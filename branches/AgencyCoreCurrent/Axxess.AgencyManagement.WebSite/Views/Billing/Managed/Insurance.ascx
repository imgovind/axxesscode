﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<%  var valueCodes = Model != null ? Model.Ub04Locator39.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
<%  var occurrenceCodes = Model != null ? Model.ToOccurrenceLocatorDictionary() : new Dictionary<string, Locator>(); %>
<%  var hcfaCodes = Model != null ? Model.HCFALocators.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
<%  using (Html.BeginForm("ManagedInsuranceVerify", "Billing", FormMethod.Post, new { @id = "managedBillingInsuranceForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "ManagedInsurance_ClaimId" }) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "ManagedInsurance_PatientId" })%>
    <% var data = Model.AgencyInsurance.ToBillDataDictionary().Select(s => s.Value); %>
<div class="wrapper main">
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.ReloadInsuranceData('<%= Model.Id %>');">Reload Insurance</a></li></ul></div><div class="clear"></div>
    <% if (Model.AgencyInsurance.InvoiceType == (int)InvoiceType.UB) { %>
    <fieldset>
        <div class="wide-column">
            <div class="column narrow">
                <div class="row">
                    <div class="strong">Ub04 Locator 31:</div>
                    <div class="">
                        <label for="Ub04Locator31" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator31", "31Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("31Locator1_Code1", occurrenceCodes.ContainsKey("31Locator1") ? occurrenceCodes["31Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_31Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("31Locator1_Code2", occurrenceCodes.ContainsKey("31Locator1") ? occurrenceCodes["31Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_31Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator31" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator31", "31Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("31Locator2_Code1", occurrenceCodes.ContainsKey("31Locator2") ? occurrenceCodes["31Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_31Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("31Locator2_Code2", occurrenceCodes.ContainsKey("31Locator2") ? occurrenceCodes["31Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_31Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column narrow">
                <div class="row">
                    <div class="strong">Ub04 Locator 32:</div>
                    <div class="">
                        <label for="Ub04Locator32" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator32", "32Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("32Locator1_Code1", occurrenceCodes.ContainsKey("32Locator1") ? occurrenceCodes["32Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_32Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("32Locator1_Code2", occurrenceCodes.ContainsKey("32Locator1") ? occurrenceCodes["32Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_32Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator32" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator32", "32Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("32Locator2_Code1", occurrenceCodes.ContainsKey("32Locator2") ? occurrenceCodes["32Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_32Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("32Locator2_Code2", occurrenceCodes.ContainsKey("32Locator2") ? occurrenceCodes["32Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_32Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column narrow">
                 <div class="row">
                    <div class="strong">Ub04 Locator 33:</div>
                    <div class="">
                        <label for="Ub04Locator33" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator33", "33Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("33Locator1_Code1", occurrenceCodes.ContainsKey("33Locator1") ? occurrenceCodes["33Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_33Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("33Locator1_Code2", occurrenceCodes.ContainsKey("33Locator1") ? occurrenceCodes["33Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_33Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator33" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator33", "33Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("33Locator2_Code1", occurrenceCodes.ContainsKey("33Locator2") ? occurrenceCodes["33Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_33Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("33Locator2_Code2", occurrenceCodes.ContainsKey("33Locator2") ? occurrenceCodes["33Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_33Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column narrow">
                 <div class="row">
                    <div class="strong">Ub04 Locator 34:</div>
                    <div class="">
                        <label for="Ub04Locator34" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator34", "34Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("34Locator1_Code1", occurrenceCodes.ContainsKey("34Locator1") ? occurrenceCodes["34Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_34Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("34Locator1_Code2", occurrenceCodes.ContainsKey("34Locator1") ? occurrenceCodes["34Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_34Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator34" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator34", "34Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("34Locator2_Code1", occurrenceCodes.ContainsKey("34Locator2") ? occurrenceCodes["34Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_34Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("34Locator2_Code2", occurrenceCodes.ContainsKey("34Locator2") ? occurrenceCodes["34Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_34Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
                
        <div class="column">
                 <div class="row">
                    <div class="strong">Ub04 Locator 35:</div>
                    <div class="margin">
                        <label for="Ub04Locator34" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator34", "35Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("35Locator1_Code1", occurrenceCodes.ContainsKey("35Locator1") ? occurrenceCodes["35Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_35Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("35Locator1_Code2", occurrenceCodes.ContainsKey("35Locator1") ? occurrenceCodes["35Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_35Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                            <%= Html.TextBox("35Locator1_Code3", occurrenceCodes.ContainsKey("35Locator1") ? occurrenceCodes["35Locator1"].Code3 : string.Empty, new { @id = "ManagedClaim_35Locator1_Code3", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator34" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator34", "35Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("35Locator2_Code1", occurrenceCodes.ContainsKey("35Locator2") ? occurrenceCodes["35Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_35Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("35Locator2_Code2", occurrenceCodes.ContainsKey("35Locator2") ? occurrenceCodes["35Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_35Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                            <%= Html.TextBox("35Locator2_Code3", occurrenceCodes.ContainsKey("35Locator2") ? occurrenceCodes["35Locator2"].Code3 : string.Empty, new { @id = "ManagedClaim_35Locator2_Code3", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        <div class="column">
                 <div class="row">
                    <div class="strong">Ub04 Locator 36:</div>
                    <div class="margin">
                        <label for="Ub04Locator34" class="float-left">a:</label>
                        <%= Html.Hidden("Ub04Locator34", "36Locator1")%>
                        <div class="float-left">
                            <%= Html.TextBox("36Locator1_Code1", occurrenceCodes.ContainsKey("36Locator1") ? occurrenceCodes["36Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_36Locator1_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("36Locator1_Code2", occurrenceCodes.ContainsKey("36Locator1") ? occurrenceCodes["36Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_36Locator1_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                            <%= Html.TextBox("36Locator1_Code3", occurrenceCodes.ContainsKey("36Locator1") ? occurrenceCodes["36Locator1"].Code3 : string.Empty, new { @id = "ManagedClaim_36Locator1_Code3", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        <div class="clear"></div>
                        <label for="Ub04Locator34" class="float-left">b:</label>
                        <%= Html.Hidden("Ub04Locator34", "36Locator2")%>
                        <div class="float-left">
                            <%= Html.TextBox("36Locator2_Code1", occurrenceCodes.ContainsKey("36Locator2") ? occurrenceCodes["36Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_36Locator2_Code1", @class = "text ubo4-code", @maxlength = "3" })%>
                            <%= Html.TextBox("36Locator2_Code2", occurrenceCodes.ContainsKey("36Locator2") ? occurrenceCodes["36Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_36Locator2_Code2", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                            <%= Html.TextBox("36Locator2_Code3", occurrenceCodes.ContainsKey("36Locator2") ? occurrenceCodes["36Locator2"].Code3 : string.Empty, new { @id = "ManagedClaim_36Locator2_Code3", @class = "text input_wrapper ubo4-date date-picker", @maxlength = "10" })%>
                        </div>
                        
                    </div>
                </div>
            </div>
               
        <div class="column">
            <div class="row">
                <div class="strong">Ub04 Locator 39:</div>
                <div class="margin">
                    <label for="Ub04Locator39" class="float-left">a:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator1")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator1_Code1", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code1 : "61", new { @id = "ManagedClaim_39Locator1_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator1_Code2", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code2 : Model.CBSA.PadStringNumberWithDecimal(2), new { @id = "ManagedClaim_39Locator1_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">b:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator2")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator2_Code1", valueCodes.ContainsKey("39Locator2") ? valueCodes["39Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_39Locator2_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator2_Code2", valueCodes.ContainsKey("39Locator2") ? valueCodes["39Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_39Locator2_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">c:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator3")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator3_Code1", valueCodes.ContainsKey("39Locator3") ? valueCodes["39Locator3"].Code1 : string.Empty, new { @id = "ManagedClaim_39Locator3_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator3_Code2", valueCodes.ContainsKey("39Locator3") ? valueCodes["39Locator3"].Code2 : string.Empty, new { @id = "ManagedClaim_39Locator3_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">d:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator4")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator4_Code1", valueCodes.ContainsKey("39Locator4") ? valueCodes["39Locator4"].Code1 : string.Empty, new { @id = "ManagedClaim_39Locator4_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator4_Code2", valueCodes.ContainsKey("39Locator4") ? valueCodes["39Locator4"].Code2 : string.Empty, new { @id = "ManagedClaim_39Locator4_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                </div>
            </div>
        </div>
             
        <div class="column">
            <div class="row">
                <div class="strong">Ub04Locator81cc:</div>
                <div class="margin">
                    <label for="Ub04Locator81ccb" class="float-left">a:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator1")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator1_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator1_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator1_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccb" class="float-left">b:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator2")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator2_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator2_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator2_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccc" class="float-left">c:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator3")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator3_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator3_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator3_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccd" class="float-left">d:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator4")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator4_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator4_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator4_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear" />
        <div class="wide_column">
                <div class="strong">Ub04 Locator 76-79:</div>
                <div class="margin">
                    <label class="float-left">76  NPI:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator76")%>
                    <div>
                   <%= Html.TextBox("Locator76_Code1", taxonomyCodes.ContainsKey("Locator76") ? taxonomyCodes["Locator76"].Code1 : Model.PhysicianNPI, new { @id = "ManagedClaim_Locator76_Code1", @class = "name_short input_wrapper ub0481cc" })%>
                    Last:<%= Html.TextBox("Locator76_Code2", taxonomyCodes.ContainsKey("Locator76") ? taxonomyCodes["Locator76"].Code2 : Model.PhysicianLastName, new { @id = "ManagedClaim_Locator76_Code2", @class = "text input_wrapper ub0481cc" })%>
                    First:<%= Html.TextBox("Locator76_Code3", taxonomyCodes.ContainsKey("Locator76") ? taxonomyCodes["Locator76"].Code3 : Model.PhysicianFirstName, new { @id = "ManagedClaim_Locator76_Code3", @class = "text input_wrapper ub0481cc" })%>
                    </div> 
                    <div class="clear"></div>  
                    <label class="float-left">77 NPI:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator77")%>
                    <div>
                    <%= Html.TextBox("Locator77_Code1", taxonomyCodes.ContainsKey("Locator77") ? taxonomyCodes["Locator77"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator77_Code1", @class = "name_short input_wrapper ub0481cc" })%>
                    Last:<%= Html.TextBox("Locator77_Code2", taxonomyCodes.ContainsKey("Locator77") ? taxonomyCodes["Locator77"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator77_Code2", @class = "text input_wrapper ub0481cc" })%>
                    First:<%= Html.TextBox("Locator77_Code3", taxonomyCodes.ContainsKey("Locator77") ? taxonomyCodes["Locator77"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator77_Code3", @class = "text input_wrapper ub0481cc" })%>
                    </div> 
                    <div class="clear"></div>  
                    <label class="float-left">78 NPI:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator78")%>
                    <div>
                    <%= Html.TextBox("Locator78_Code1", taxonomyCodes.ContainsKey("Locator78") ? taxonomyCodes["Locator78"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator78_Code1", @class = "name_short input_wrapper ub0481cc" })%>
                    Last:<%= Html.TextBox("Locator78_Code2", taxonomyCodes.ContainsKey("Locator78") ? taxonomyCodes["Locator78"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator78_Code2", @class = "text input_wrapper ub0481cc" })%>
                    First:<%= Html.TextBox("Locator78_Code3", taxonomyCodes.ContainsKey("Locator78") ? taxonomyCodes["Locator78"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator78_Code3", @class = "text input_wrapper ub0481cc" })%>
                    </div> 
                    <div class="clear"></div> 
                    <label class="float-left">79 NPI:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator79")%>
                    <div>
                    <%= Html.TextBox("Locator79_Code1", taxonomyCodes.ContainsKey("Locator79") ? taxonomyCodes["Locator79"].Code1 : string.Empty, new { @id = "ManagedClaim_Locator79_Code1", @class = "name_short input_wrapper ub0481cc" })%>
                    Last:<%= Html.TextBox("Locator79_Code2", taxonomyCodes.ContainsKey("Locator79") ? taxonomyCodes["Locator79"].Code2 : string.Empty, new { @id = "ManagedClaim_Locator79_Code2", @class = "text input_wrapper ub0481cc" })%>
                    First:<%= Html.TextBox("Locator79_Code3", taxonomyCodes.ContainsKey("Locator79") ? taxonomyCodes["Locator79"].Code3 : string.Empty, new { @id = "ManagedClaim_Locator79_Code3", @class = "text input_wrapper ub0481cc" })%>
                    </div> 
                    <div class="clear"></div> 
                </div>
        </div>         
    </fieldset>
    <% } else if(Model.AgencyInsurance.InvoiceType == (int)InvoiceType.HCFA) { %>
           <fieldset>
        <div class="column">
        
                <div class="row">
                <div class="strong">HCFA 1500 Locator 10:</div>
                    <div class="margin">
                        
                        <%--<%= Html.Hidden("HCFALocators", "10Locatora")%>--%>
                        <div class="float-left">
                             <%= Html.Hidden("HCFALocators", "10Locatora")%>
                             <label for="HCFALocators" class="float-left">a. Employment?(Current Or Previous) </label>
                             <%= Html.RadioButton("10Locatora_Code1", "Yes",  hcfaCodes.ContainsKey("10Locatora") && hcfaCodes["10Locatora"].Code1=="Yes"? true: false, new { @id = "10Locatora_Code1", @class = "radio" })%>
                             <label for="10Locatora_Code1" class="inline-radio">Yes</label>
                             <%= Html.RadioButton("10Locatora_Code1", "No", hcfaCodes.ContainsKey("10Locatora") && hcfaCodes["10Locatora"].Code1 == "No" ? true : false, new { @id = "10Locatora_Code1", @class = "radio" })%>
                             <label for="10Locatora_Code1" class="inline-radio">No</label>
                             <br />
                             <%= Html.Hidden("HCFALocators", "10Locatorb")%>
                             <label for="HCFALocators" class="float-left">b. Auto Accident? </label>
                             <%= Html.RadioButton("10Locatorb_Code1", "Yes", hcfaCodes.ContainsKey("10Locatorb") && hcfaCodes["10Locatorb"].Code1=="Yes"? true: false, new { @id = "10Locatorb_Code1", @class = "radio" })%>
                             <label for="10Locatorb_Code1" class="inline-radio">Yes</label>
                             <%= Html.RadioButton("10Locatorb_Code1", "No",  hcfaCodes.ContainsKey("10Locatorb") && hcfaCodes["10Locatorb"].Code1=="No"? true: false, new { @id = "10Locatorb_Code1", @class = "radio" })%>
                             <label for="10Locatorb_Code1" class="inline-radio">No</label>
                             <label for="10Locatorb_Code2" class="inline-radio"> Place(State):<span> <%= Html.TextBox("10Locatorb_Code2", hcfaCodes.ContainsKey("10Locatorb") ? hcfaCodes["10Locatorb"].Code2 : string.Empty, new { @class = "float-right", @maxlength = "4" })%></span></label>
                             <br />
                             <%= Html.Hidden("HCFALocators", "10Locatorc")%>
                             <label for="HCFALocators" class="float-left">c. Other Accident? </label>
                             <%= Html.RadioButton("10Locatorc_Code1", "Yes", hcfaCodes.ContainsKey("10Locatorc") && hcfaCodes["10Locatorc"].Code1 == "Yes" ? true : false, new { @id = "10Locatorc_Code1", @class = "radio" })%>
                             <label for="10Locatorc_Code1" class="inline-radio">Yes</label>
                             <%= Html.RadioButton("10Locatorc_Code1", "No", hcfaCodes.ContainsKey("10Locatorc") && hcfaCodes["10Locatorc"].Code1 == "No" ? true : false, new { @id = "10Locatorc_Code1", @class = "radio" })%>
                             <label for="10Locatorc_Code1" class="inline-radio">No</label>
                        </div>
                    </div>
                             
            </div>
        </div>
        <div class="column">
        <div class="strong">HCFA 1500 Locator 18:</div>
            <div class="margin">
            <%= Html.Hidden("HCFALocators", "18Locator")%>
                <label>From:</label>
                <%= Html.TextBox("18Locator_Code1", hcfaCodes.ContainsKey("18Locator") ? hcfaCodes["18Locator"].Code1 : string.Empty, new { @class = "date-picker input_wrapper" })%>
                <label>To:</label> 
                <%= Html.TextBox("18Locator_Code2", hcfaCodes.ContainsKey("18Locator") ? hcfaCodes["18Locator"].Code2 : string.Empty, new { @class = "date-picker input_wrapper" })%>
             </div>
        <div class="strong">HCFA 1500 Locator 24j</div>
            <div class="margin">
                <%= Html.Hidden("HCFALocators", "24Locator")%>
                <%=hcfaCodes.ContainsKey("24Locator") && hcfaCodes["24Locator"].Customized ? "<input type='checkbox' name='24Locator_Customized' value='true' class='radio float-left' checked='checked'>" : "<input type='checkbox' name='24Locator_Customized' value='true' class='radio float-left'>"%>Check box if performing provider’s ID number is required on claim
            </div>
        <div class="clear" />
        <div class="strong">HCFA 1500 Locator 33:</div>
                    <div class="margin">
                        <label for="HCFALocators" class="float-left">b:</label>
                        <%= Html.Hidden("HCFALocators", "33Locatorb")%>
                        <div class="float-left">
                            <%= Html.TextBox("33Locatorb_Code1", hcfaCodes.ContainsKey("33Locatorb") ? hcfaCodes["33Locatorb"].Code1 : string.Empty, new { @class = "text input_wrapper", @maxlength = "12" })%>
                        </div>
                    </div>
        
        </div>
    </fieldset>
    <% } %>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewBillDataInBilling('<%= Model.Id %>', 'Managed');">Add Visit Information</a></li></ul></div><div class="clear"></div>
    <% var billData = Model.AgencyInsurance != null && Model.AgencyInsurance.BillData.IsNotNullOrEmpty() ? Model.AgencyInsurance.BillData.ToObject<List<ChargeRate>>() : new List<ChargeRate>(); %>
     <%= Html.Telerik().Grid<ChargeRate>(billData).HtmlAttributes(new { @style = "height:auto; position: relative;margin-bottom: 30px;" }).Name("ManagedInsurance_BillDatas")
        .DataKeys(keys =>{ keys.Add(r => r.Id).RouteKey("Id");  })
        .Columns(columns =>
        {
            columns.Bound(e => e.DisciplineTaskName).Title("Task");
            columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
            columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
            columns.Bound(e => e.Code).Title("HCPCS").Width(55);
            columns.Bound(e => e.Charge).Format("${0:#0.00}").Title("Rate").Width(45);
            columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
            columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
            columns.Bound(e => e.TimeLimitFormat).Title("Time Limit").Width(65);
            columns.Bound(e => e.Id).Template(t => string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditBillDataInBilling($('#ManagedInsurance_ClaimId').val(),'{0}', 'Managed');\" >Edit</a> | <a  href=\"javascript:void(0);\" onclick=\"ManagedBilling.DeleteBillData($('#ManagedInsurance_ClaimId').val(),'{0}');\" >Delete</a> ", t.Id)).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditBillDataInBilling($('#ManagedInsurance_ClaimId').val(),'<#=Id#>', 'Managed');\" >Edit</a> | <a  href=\"javascript:void(0);\" onclick=\"ManagedBilling.DeleteBillData($('#ManagedInsurance_ClaimId').val(),'<#=Id#>');\" >Delete</a> ").Title("Action").Width(85).Sortable(false);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimInsuranceRates", "Billing", new { ClaimId = Model.Id })).Sortable().Footer(false)%>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.NavigateBack(0);">Back</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript">
    ManagedBilling.Navigate(2, "#managedBillingInsuranceForm", '<%=Model.PatientId %>');
    $("#ManagedClaimTabStrip-2 ol").each(function() { $("li:last", $(this)).addClass("last") });
    U.BasicTabSetup($("#managedBillingInsuranceForm"));
</script>