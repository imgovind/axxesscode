﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("AddManagedClaimAdjustment", "Billing", FormMethod.Post, new { @id = "newManagedClaimAdjustmentForm" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Post Adjustment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Adjustment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("Adjustment", "", new {@class = "text input_wrapper required currency", @maxlength = "" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">Type:</label>
                <div class="float-right"><%= Html.AdjustmentCodes("TypeId", "", "-- Select Type --", new { @class = "requireddropdown" }) %></div>
            </div>
             <div class="row">
                <label class="float-left">Comment:</label>
                <div class=""><%= Html.TextArea("Comments", "") %></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Post Adjustment</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>