﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Bill>>" %>
<span class="wintitle">All Insurances/Payors <%= Current.AgencyName %></span>
<% var titleCaseType = "AllClaim"; %>
<div class="main wrapper">
<div class="buttons float-right">
    <ul>
       <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsXls', { 'branchId': $('#Billing_<%=titleCaseType%>Center_BranchCode').val(), 'insuranceid':$('#Billing_<%=titleCaseType%>Center_InsuranceId').val(), 'parentSortType': 'branch','columnSortType':'', 'claimType': $('#Billing_<%=titleCaseType%>Center_Type').val() });" >Export to Excel</a></li>
    </ul>
</div>
    <fieldset class="orders-filter">
         <span class="strong">Branch:&nbsp;</span><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "Branch", ViewData.ContainsKey("Branch") ? ViewData["Branch"].ToString() : Guid.Empty.ToString(), new { @id = "Billing_" + titleCaseType + "Center_BranchCode", @class = "report_input valid" })%>
         <span class="strong">Bill Type:&nbsp;</span><%  var billType = new SelectList(new[] { new SelectListItem { Text = ClaimTypeSubCategory.RAP.GetDescription(), Value = ClaimTypeSubCategory.RAP.ToString() }, new SelectListItem { Text = ClaimTypeSubCategory.Final.GetDescription(), Value = ClaimTypeSubCategory.Final.ToString() }, new SelectListItem { Text = ClaimTypeSubCategory.ManagedCare.GetDescription(), Value = ClaimTypeSubCategory.ManagedCare.ToString() } }, "Value", "Text", ViewData.ContainsKey("Type") ? ViewData["Type"].ToString() : string.Empty); %><span><%= Html.DropDownList("Type", billType, new { @id = "Billing_" + titleCaseType + "Center_Type" })%></span>
         <br />
         <span class="strong">Insurance:&nbsp;</span><span><%= Html.InsuranceListByBranch("PrimaryInsurance", ViewData.ContainsKey("Insurance") ? ViewData["Insurance"].ToString() : string.Empty, ViewData.ContainsKey("Branch") ? ViewData["Branch"].ToString() : Guid.Empty.ToString(), true, "All", new { @id = "Billing_" + titleCaseType + "Center_InsuranceId", @class = "Insurances requireddropdown" })%></span>
         <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Billing.ReLoadUnProcessedClaim('#Billing_CenterContent<%=titleCaseType%>',$('#Billing_<%=titleCaseType%>Center_BranchCode').val(), $('#Billing_<%=titleCaseType%>Center_InsuranceId').val(),'AllClaimGrid', $('#Billing_<%=titleCaseType%>Center_Type').val());">Refresh</a></li>
                <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': $('#Billing_<%=titleCaseType%>Center_BranchCode').val(), 'insuranceid':$('#Billing_<%=titleCaseType%>Center_InsuranceId').val(), 'parentSortType': 'branch','columnSortType':'', 'claimType': $('#Billing_<%=titleCaseType%>Center_Type').val() });" >Print</a></li>
                </ul>
        </div>
    </fieldset>
    <div id="Billing_CenterContent<%=titleCaseType%>" style="min-height:200px;">
       <% if (Model != null && Model.Count > 0)
          { %><% foreach (var data in Model)
                                                    { %><% Html.RenderPartial(data.ClaimType != ClaimTypeSubCategory.ManagedCare ? data.ClaimType.ToString().ToTitleCase() + "Grid" : "Managed/ManagedGrid", data); %><%} %><%}
          else
          { %>No Data found for current filter.<%} %>
    </div>
 </div>
<script type="text/javascript">
    $("#Billing_<%=titleCaseType%>Center_BranchCode").change(function() {Insurance.loadInsuarnceDropDown("Billing_<%=titleCaseType%>Center", "All", true, function() { eval("Billing.ReLoadUnProcessedClaim('" + '#Billing_CenterContent<%=titleCaseType%>' + "','" + $('#Billing_<%=titleCaseType%>Center_BranchCode').val() + "'," + $('#Billing_<%=titleCaseType%>Center_InsuranceId').val() + ",'AllClaimGrid','" + $('#Billing_<%=titleCaseType%>Center_Type').val() + "')"); });});
    $("#Billing_<%=titleCaseType%>Center_InsuranceId").change(function() {eval("Billing.ReLoadUnProcessedClaim('" + '#Billing_CenterContent<%=titleCaseType%>' + "','" + $('#Billing_<%=titleCaseType%>Center_BranchCode').val() + "'," + $(this).val() + ",'AllClaimGrid','" + $('#Billing_<%=titleCaseType%>Center_Type').val() + "')");});
    $("#Billing_<%=titleCaseType%>Center_Type").change(function() {eval("Billing.ReLoadUnProcessedClaim('" + '#Billing_CenterContent<%=titleCaseType%>' + "','" + $('#Billing_<%=titleCaseType%>Center_BranchCode').val() + "','" + $('#Billing_<%=titleCaseType%>Center_InsuranceId').val() + "','AllClaimGrid','" + $(this).val() + "')");});
    $("#window_allbillingclaims_content").css({
        "background-color": "#d6e5f3"
    });
    
</script>