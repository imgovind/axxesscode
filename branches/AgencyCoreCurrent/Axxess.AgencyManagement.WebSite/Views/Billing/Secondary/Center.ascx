﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>"%>
<div id="secondaryBillingMainResult" class="ui-layout-center">
    <div class="top">
        
            <% Html.RenderPartial("Secondary/PrimaryClaimInfo", Model.PrimaryClaimData); %>
        
        <div id="secondaryBillingClaimData"></div>
    </div>
    <div class="bottom" style="top:300px">
        <% Html.RenderPartial("~/Views/Billing/Secondary/Claims.ascx", Model); %>
    </div>   
</div>




