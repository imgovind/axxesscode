﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<% using (Html.BeginForm("AddMissedVisit", "Schedule", FormMethod.Post, new { @id = "newMissedVisitForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "New_MissedVisit_EventId" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "New_MissedVisit_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "New_MissedVisit_PatientId" })%>
<%= Html.Hidden("Date", Model.EventDate, new { @id = "New_MissedVisit_Date" })%>
<div class="wrapper main">
    <div><span id="New_MissedVisit_Patient" class="bigtext"><%=Model.PatientName %></span></div>
    <fieldset>
        <legend>Missed Visit Details</legend>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_TaskName" class="float-left">Type of Visit:</label>
                <div class="float-right"><span id="New_MissedVisit_TaskName"><%=Model.DisciplineTaskName %></span></div>
            </div><div class="row">
                <label for="New_MissedVisit_Date" class="float-left">Date of Visit:</label>
                <div class="float-right"><span id="New_MissedVisit_Date"><%=Model.EventDate %></span></div>
            </div><div class="row">
                <label for="New_MissedVisit_Order_Yes" class="float-left">Order Generated:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsOrderGenerated", "true", new { @id = "IsOrderGenerated_Y", @class = "radio" })%>
                    <label for="IsOrderGenerated_Y" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsOrderGenerated", "false", new { @id = "IsOrderGenerated_N", @class = "radio" })%>
                    <label for="IsOrderGenerated_N" class="inline-radio">No</label>
                </div>
            </div><div class="row">
                <label for="New_MissedVisit_PhysicianNotified_Yes" class="float-left">Physician Office Notified:</label>
                <div class="float-right">
                    <%= Html.RadioButton("IsPhysicianOfficeNotified", "true", new { @id = "IsPhysicianOfficeNotified_Y", @class = "radio" })%>
                    <label for="IsPhysicianOfficeNotified_Y" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsPhysicianOfficeNotified", "false", new { @id = "IsPhysicianOfficeNotified_N", @class = "radio" })%>
                    <label for="IsPhysicianOfficeNotified_N" class="inline-radio">No</label>
                </div>
            </div><div class="row">
                <label for="New_MissedVisit_Reason" class="float-left">Reason:</label>
                <div class="float-right">
                    <%  var reasons = new SelectList(new[] {
                                        new SelectListItem { Text = "** Select **", Value = "" },
                                        new SelectListItem { Text = "Cancellation of Care", Value = "Cancellation of Care" },
                                        new SelectListItem { Text = "Doctor - Clinic Appointment", Value = "Doctor - Clinic Appointment" },
                                        new SelectListItem { Text = "Family - Caregiver Able to Assist Patient", Value = "Family - Caregiver Able to Assist Patient" },
                                        new SelectListItem { Text = "No Answer to Locked Door", Value = "No Answer to Locked Door" },
                                        new SelectListItem { Text = "No Answer to Phone Call", Value = "No Answer to Phone Call" },
                                        new SelectListItem { Text = "Patient - Family Uncooperative", Value = "Patient - Family Uncooperative" },
                                        new SelectListItem { Text = "Patient Hospitalized", Value = "Patient Hospitalized" },
                                        new SelectListItem { Text = "Patient Unable to Answer Door", Value = "Patient Unable to Answer Door" },
                                        new SelectListItem { Text = "Therapy on Hold", Value = "Therapy on Hold" },
                                        new SelectListItem { Text = "Other (Specify)", Value = "Other" }
                                    }, "Value", "Text", Model.Reason);%>
                                <%= Html.DropDownList("Reason", reasons, new { @id = "New_MissedVisit_Reason", @class = "" })%>
                    
                </div>
            </div>
        </div>
        <table class="form">
            <tbody>
                <tr class="line-seperated vert">
                    <td>
                        <label for="Comment">Comments:</label>
                        <div><%= Html.TextArea("Comments", Model.Comments, new { @id = "New_MissedVisit_Comments" })%></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignature" class="bigtext float-left">Staff Signature:</label>
                <div class="float-right"><%= Html.Password("Signature", "", new { @id = "New_MissedVisit_ClinicianSignature", @class = "required" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignatureDate" class="bigtext float-left">Signature Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="SignatureDate" value="<%= Model.EventDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="New_MissedVisit_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
        </ul>
    </div>
</div>
<% } %>