﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<fieldset>
    <div class="wide-column">
        <div class="row"><label for="Missed_Visit_Info_Name" class="float-left">Patient Name:</label><div class="float-right"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_TypeofVisit" class="float-left">Type of Visit:</label><div class="float-right"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_DateofVisit" class="float-left">Date of Visit:</label><div class="float-right"><%= Model != null ? Model.Date.ToShortDateString():string.Empty %></div></div>
        <div class="row"><label for="Missed_Visit_Info_UserName" class="float-left">Assigned To:</label><div class="float-right"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_OrderGenerated" class="float-left">Order Generated:</label><div class="float-right"><%= (Model != null && Model.IsOrderGenerated) ? "Yes" : "No"%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianName" class="float-left">Physician:</label><div class="float-right"><%= (Model != null && Model.PhysicianName.IsNotNullOrEmpty()) ? Model.PhysicianName : string.Empty%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianPhone" class="float-left">Physician Phone:</label><div class="float-right"><%= (Model != null && Model.PhysicianPhone.IsNotNullOrEmpty()) ? Model.PhysicianPhone : string.Empty%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianFax" class="float-left">Physician Fax:</label><div class="float-right"><%= (Model != null && Model.PhysicianFax.IsNotNullOrEmpty()) ? Model.PhysicianFax : string.Empty%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianOfficeNotified" class="float-left">Physician Office Notified:</label><div class="float-right"><%= (Model != null && Model.IsPhysicianOfficeNotified) ? "Yes" : "No"%></div></div>
        <div class="row"><label for="Missed_Visit_Info_Reason" class="float-left">Reason:</label><div class="float-right"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_Comments" class="float-left">Comments:</label><div class="float-right"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div></div>
    </div>
</fieldset>