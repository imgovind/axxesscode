﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FrequenciesViewData>" %>
<div class="window-top">
    <span class="abs">Episode Frequencies</span>
</div>
<div>
    <br />
</div>
<div id="Episode_Frequencies" background: url('/Content/Office2007/sprite.png') repeat-x scroll 0 -352px #C3D8F1;" class="payroll">
<% if (Model.Visits.Count > 0)
   { %>
    <ul style="margin-top:0px;">
        <li>
            <span class="" style="width:30%">Visit Type</span>
            <span class="strong" style="width:45%">Frequency</span>
            <span class="strong" style="width:15%">Visit Count</span>
        </li>
    </ul>
    <ol>
    <%  int i = 1; %>
    
    <%  foreach (var pair in Model.Visits)
        { %>
        <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>"%>
            <span style="width:30%"><%= pair.Key%></span>
            <span style="width:45%"><%= pair.Value.Frequency%> </span>
            <span style="width:15%"><%= pair.Value.Count%> </span>
        </li>
        <%  i++; %>
    <%  }
   }
   else
   { %> 
   <span class="strong"> There are no frequencies set</span>
  
   <% } %>
</div>

<div class="buttons" style="margin-top: 50px;">
    <ul>
        <li>
            <a class="close">Close</a>
        </li>
    </ul>
</div>