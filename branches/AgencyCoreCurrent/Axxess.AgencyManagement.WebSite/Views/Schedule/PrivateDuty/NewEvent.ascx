﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PDCalendarViewData>" %>
<%  DateTime[] startdate = new DateTime[2], enddate = new DateTime[2], currentdate = new DateTime[2]; %>
<%  startdate[0] = Model.StartDate; %>
<%  enddate[0] = Model.EndDate; %>
<%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
<%  startdate[1] = enddate[0].AddDays(1); %>
<%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
<%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
<%  var dictionary = new Dictionary<string,string>(); %>
<%  dictionary.Add("class", "non-date-field"); %>
<div class="wrapper main">
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "PDMultiDaySchedule_PatientId", @class = "non-date-field" })%>
    <fieldset>
        <legend>Quick Employee Scheduler</legend>
        <div class="wide column">
            <div class="narrowest row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.DisplayNameWithMi %></div>
            </div>
            <div class="narrowest row">
                <label class="fl strong" for="PDMultiDaySchedule_Task">Task</label>
                <div class="fr"><%= Html.MultipleDisciplineTasks("Task", "", dictionary) %></div>
            </div>
            <div class="narrowest row">
                <label class="fl strong" for="PDMultiDaySchedule_UserId">User/Employee</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", "", new { @id = "PDMultiDaySchedule_UserId", @class = "non-date-field" }) %></div>
            </div>
            <div class="ac row">
                <div class="strong">To schedule visits for the selected user, click on the desired dates in the calendar below</div>
                <div class="trical">
                    <%= Html.Hidden("EventDate", DateTime.MinValue, new { @id = "PDMultiDaySchedule_EventDate" })%>
<%  for (int c = 0; c < 2; c++) { %>
    <%  if (c == 0 || (c == 1 && (((Model.EndDate.Date.Day - Model.StartDate.Date.Day) + 1) < DateTime.DaysInMonth(Model.StartDate.Year, Model.StartDate.Month)))) { %>
                    <div class="cal">
                        <table>
                            <thead class="ac">
                                <tr>
                                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                                </tr>
                                <tr>
                                    <th>Su</th>
                                    <th>Mo</th>
                                    <th>Tu</th>
                                    <th>We</th>
                                    <th>Th</th>
                                    <th>Fr</th>
                                    <th>Sa</th>
                                </tr>
                            </thead>
                            <tbody>
        <%  var maxWeek = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
        <%  for (int i = 0; i <= maxWeek; i++) { %>
                                <tr>
            <%  int addedDate = (i) * 7; %>
            <%  if (currentdate[c].AddDays(7 + addedDate).Date >= Model.StartDate.Date && currentdate[c].AddDays(addedDate) <= enddate[c]) for (int j = 0; j <= 6; j++) { %>
                <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
                <%  if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c]) { %>
                                    <td class="inactive"></td>
                <%  } else { %>
                                    <td>
                                        <label><%= specificDate.Day %></label>
                                        <%= string.Format("<input title='' name='EventDate' value='{0}' type='checkbox' />", specificDate) %>
                                    </td>
                <%  } %>
            <%  } %>
                                </tr>
        <%  } %>
                            </tbody>
                        </table>
                    </div>
    <%  } %>
<%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="add-schedule another">Schedule</a></li>
            <li><a class="add-schedule">Schedule and Close</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>