﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
<span class="wintitle">Home Maker Note | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HomeMakerNoteForm" }))
    { %>
    <%= Html.Hidden("HomeMakerNote_PatientId", Model.PatientId)%>
    <%= Html.Hidden("HomeMakerNote_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("HomeMakerNote_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "115")%>
    <%= Html.Hidden("Type", "HomeMakerNote")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                    Home Maker Note
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="HomeMakerNote_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox("HomeMakerNote_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "HomeMakerNote_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HomeMakerNote_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="HomeMakerNote_VisitDate" value="<%= Model.VisitDate.IsValidDate() ? Model.VisitDate : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="HomeMakerNote_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HomeMakerNote_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox("HomeMakerNote_TimeIn", data.AnswerOrEmptyString("TimeIn"), new { @id = "HomeMakerNote_TimeIn", @class = "time-picker" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HomeMakerNote_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox("HomeMakerNote_TimeOut", data.AnswerOrEmptyString("TimeOut"), new { @id = "HomeMakerNote_TimeOut", @class = "time-picker" })%></div>
                    </div>
                </td>
                <td colspan="2">
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <label for="HomeMakerNote_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "HomeMakerNote_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                     <div class="clear"></div>
                     <div>
                        <label for="HomeMakerNote_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("HomeMakerNote_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = "HomeMakerNote_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                     <div class="clear"></div>
                    <div>
                        <label for="HomeMakerNote_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox("HomeMakerNote_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = "HomeMakerNote_Surcharge", @class = "text number input_wrapper", @maxlength = "7"  })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="HomeMakerNote_Content"><% Html.RenderPartial("~/Views/Schedule/HHA/HomeMakerNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>       
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="HomeMakerNote_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("HomeMakerNote_Clinician", "", new { @id = "HomeMakerNote_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="HomeMakerNote_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="HomeMakerNote_SignatureDate" value="<%= date %>" id="HomeMakerNote_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", "HomeMakerNote")%>
                    <label for="HomeMakerNote_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HomeMakerNote_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.HomeMakerNote.Submit($(this))" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.HomeMakerNote.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Visit.HomeMakerNote.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.HomeMakerNote.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
    <%  } %>
</div>