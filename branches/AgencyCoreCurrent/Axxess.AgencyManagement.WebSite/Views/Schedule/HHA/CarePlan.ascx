﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Home Health Aide Care Plan | <%= Model.Patient.DisplayName %></span>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHACarePlanForm" })) { 
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Hidden("HHACarePlan_PatientId", Model.PatientId)%>
<%= Html.Hidden("HHACarePlan_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
<%= Html.Hidden("HHACarePlan_EventId", Model.EventId)%>
<%= Html.Hidden("DisciplineTask", "75")%>
<%= Html.Hidden("Type", "HHACarePlan")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    Home Health Aide Care Plan
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr><td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span></td><td><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl%></li></ul></div><% } %></td></tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="HHACarePlan_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.PatientEpisodes("HHACarePlan_SelectedEpisodeId", data.ContainsKey("SelectedEpisodeId") && data["SelectedEpisodeId"] != null && data["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? data["SelectedEpisodeId"].Answer : string.Empty, Model.PatientId, "-- Select Episode --", new { @id = "HHACarePlan_SelectedEpisodeId", @class = "requireddropdown" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="HHACarePlan_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="HHACarePlan_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="HHACarePlan_VisitDate" /></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="HHACarePlan_HHAFrequency" class="float-left">Frequency:</label>
                        <div class="float-right"><%= Html.TextBox("HHACarePlan_HHAFrequency", data.ContainsKey("HHAFrequency") ? data["HHAFrequency"].Answer : string.Empty, new { @id = "HHACarePlan_HHAFrequency" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td colspan="2"> 
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <label for="HHACarePlan_PreviousNotes" class="float-left">Previous Care Plans:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "HHACarePlan_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <label for="HHACarePlan_DNR" class="float-left">DNR:</label>
                        <div class="float-right"><%= Html.RadioButton("HHACarePlan_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "HHACarePlan_DNR1", @class = "radio" })%><label for="HHACarePlan_DNR1" class="inline-radio">Yes</label><%= Html.RadioButton("HHACarePlan_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "HHACarePlan_DNR2", @class = "radio" })%><label for="HHACarePlan_DNR2" class="inline-radio">No</label></div>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <% string[] diet = data.ContainsKey("IsDiet") && data["IsDiet"].Answer != "" ? data["IsDiet"].Answer.Split(',') : null; %><input name="HHACarePlan_IsDiet" value="" type="hidden" />
                        <div class="float-left"><%= string.Format("<input class='radio' id='HHACarePlan_IsDiet' name='HHACarePlan_IsDiet' value='1' type='checkbox' {0} />", diet != null && diet.Contains("1") ? "checked='checked'" : "" )%><label for="HHACarePlan_IsDiet" class="strong">Diet:</label></div>
                        <div class="float-right"><%= Html.TextBox("HHACarePlan_Diet", data.ContainsKey("Diet") ? data["Diet"].Answer : string.Empty, new { @id = "HHACarePlan_Diet" })%></div>
                    </div>
                    <div class="clear"/>
                    <div style="height:26px;">
                        <% string[] allergies = data.ContainsKey("Allergies") && data["Allergies"].Answer != "" ? data["Allergies"].Answer.Split(',') : null; %><input name="HHACarePlan_Allergies" value="" type="hidden" />
                        <div class="float-left"><%= string.Format("<input class='radio' id='HHACarePlan_Allergies' name='HHACarePlan_Allergies' value='Yes' type='checkbox' {0} />", allergies != null && allergies.Contains("Yes") ? "checked='checked'" : "")%><label for="HHACarePlan_Allergies" class="strong">Allergies:</label></div>
                        <div class="float-right"><%= Html.TextBox("HHACarePlan_AllergiesDescription", data.ContainsKey("AllergiesDescription") ? data["AllergiesDescription"].Answer:string.Empty, new { @id = "HHACarePlan_AllergiesDescription" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <div id="HHACarePlan_Content"><% Html.RenderPartial("~/Views/Schedule/HHA/CarePlanContent.ascx", Model); %></div>
        <table class="fixed nursing">
            <tr>
                <th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="HHACarePlan_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("HHACarePlan_Clinician", "", new { @id = "HHACarePlan_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="HHACarePlan_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="HHACarePlan_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="HHACarePlan_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HHACarePlan_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="HHACarePlanRemove(); hhaCarePlan.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="HHACarePlanAdd(); hhaCarePlan.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="HHACarePlanRemove(); hhaCarePlan.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="HHACarePlanRemove(); hhaCarePlan.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="HHACarePlanRemove(); UserInterface.CloseWindow('hhaCarePlan');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#HHACarePlan_IsVitalSignParameter").click(function() {
        if ($('#HHACarePlan_IsVitalSignParameter').is(':checked')) $(".vitalsigns").each(function() { $(this).hide(); });
        else $(".vitalsigns").each(function() { $(this).show(); });
    });
    if ($('#HHACarePlan_IsVitalSignParameter').is(':checked')) $(".vitalsigns").each(function() { $(this).hide(); });
    else $(".vitalsigns").each(function() { $(this).show(); });

    $("#HHACarePlan_MR").attr('readonly', true);
    $("#HHACarePlan_EpsPeriod").attr('readonly', true);
    function HHACarePlanAdd() {
        $("#HHACarePlan_Clinician").removeClass('required').addClass('required');
        $("#HHACarePlan_SignatureDate").removeClass('required').addClass('required');
        if ($("input[@id=HHACarePlan_ReviewedWithHHA]:checked").length == 0) {
            $("#HHACarePlan_ReviewedWithHHA").parent().append("<label class='error' style='display: block;'>* Required</label>");
        }
        if ($("input[@id=HHACarePlan_ReviewedWithPatient]:checked").length == 0) {
            $("#HHACarePlan_ReviewedWithPatient").parent().append("<label class='error' style='display: block;'>* Required</label>");
        } 
    }
    function HHACarePlanRemove() {
        $("#HHACarePlan_Clinician").removeClass('required');
        $("#HHACarePlan_SignatureDate").removeClass('required');
    }
    $('.requirednotification').css("position", "relative");
</script>