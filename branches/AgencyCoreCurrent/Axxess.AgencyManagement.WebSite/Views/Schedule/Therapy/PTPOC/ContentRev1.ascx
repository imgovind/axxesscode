﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<table class="fixed nursing">
    <tbody>
        
        <tr>
            <th colspan="5">DME
            <%= string.Format("<input class='radio' id='{0}IsDMEApply' name='{0}_IsDMEApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDMEApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Medical Diagnosis
            <%= string.Format("<input class='radio' id='{0}IsMedicalApply' name='{0}_IsMedicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMedicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>DMEContainer">
                <div class="float-left">
                <label for="<%= Model.Type %>_POCGenericDMEAvailable" class="float-left">Available:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMEAvailable", data.AnswerOrEmptyString("POCGenericDMEAvailable"), new { @class = "", @id = Model.Type + "_POCGenericDMEAvailable" }) %>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_POCGenericDMENeeds" class="float-left">Needs:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMENeeds", data.AnswerOrEmptyString("POCGenericDMENeeds"), new { @class = "", @id = Model.Type + "_POCGenericDMENeeds" })%>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_POCGenericDMESuggestion" class="float-left">Suggestion:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMESuggestion", data.AnswerOrEmptyString("POCGenericDMESuggestion"), new { @class = "", @id = Model.Type + "_POCGenericDMESuggestion" })%>
                </div>
                </div>
            </td>
            <td colspan="5"><div id="<%=Model.Type %>MedicalContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev3.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Treatment Plan
            <%= string.Format("<input class='radio' id='{0}IsTreatmentApply' name='{0}_IsTreatmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTreatmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>TreatmentContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev5.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">Modalities<%= string.Format("<input class='radio' id='{0}IsModalitiesApply' name='{0}_IsModalitiesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsModalitiesApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>ModalitiesContainer">
            <%= Html.Templates(Model.Type + "_POCGenericModalitiesTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericModalitiesComment" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericModalitiesComment", data.ContainsKey("POCGenericModalitiesComment") ? data["POCGenericModalitiesComment"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCGenericModalitiesComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="10">PT Goals
            <%= string.Format("<input class='radio' id='{0}IsGoalsApply' name='{0}_IsGoalsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGoalsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>GoalsContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PTGoals/FormRev3.ascx", Model); %></div></td>
        </tr>
        <tr>
            <th colspan="10">Other Discipline Recommendation
            <%= string.Format("<input class='radio' id='{0}IsRecommendationApply' name='{0}_IsRecommendationApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRecommendationApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>RecommendationContainer">
                <%  string[] genericDisciplineRecommendation = data.AnswerArray("POCGenericDisciplineRecommendation"); %>
                <input type="hidden" name="<%= Model.Type %>_POCGenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <div><label for="<%= Model.Type %>_POCGenericDisciplineRecommendation1" class="float-left">Disciplines</label></div>
                                <div class="margin">
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation1' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation1">OT</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation2' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation2">MSW</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation3' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation3">ST</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation4' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_POCGenericDisciplineRecommendation4">Podiatrist</label>
                                    <span>Other</span>
                                    <%= Html.TextBox(Model.Type + "_POCGenericDisciplineRecommendationOther", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther"), new { @id = Model.Type + "_POCGenericDisciplineRecommendationOther" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_POCGenericDisciplineRecommendationReason" class="strong">Reason</label>
                                    <%= Html.Templates(Model.Type + "_DisciplineRecommendationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                                    <%= Html.TextArea(Model.Type + "_POCGenericDisciplineRecommendationReason", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason"), new { @class = "fill", @id = Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                                </div>
                            </td>
                        </tr>
                        
                        
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Rehab <%= string.Format("<input class='radio' id='{0}IsRehabApply' name='{0}_IsRehabApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRehabApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Discharge Plan<%= string.Format("<input class='radio' id='{0}IsDCPlanApply' name='{0}_IsDCPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDCPlanApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>RehabContainer">
                <div class="float-left"><label for="<%= Model.Type %>_POCGenericDisciplineRehabDiagnosis" class="strong">Rehab Diagnosis:</lable><%= Html.TextBox(Model.Type + "_POCGenericDisciplineRehabDiagnosis", data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis"), new { @class = "", @id = Model.Type + "_POCGenericDisciplineRehabDiagnosis" })%></div>
                    <div class="clear" />        
                <div class="float-left">
                    <label for="<%= Model.Type %>_GenericRehabPotential" class="strong">Rehab Potential</label>
                    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "0", data.AnswerOrEmptyString("POCRehabPotential").Equals("0"), new { @id = Model.Type + "_POCRehabPotential0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential0" class="inline-radio">Good</label>
                    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "1", data.AnswerOrEmptyString("POCRehabPotential").Equals("1"), new { @id = Model.Type + "_POCRehabPotential1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential1" class="inline-radio">Fair</label>
                    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "2", data.AnswerOrEmptyString("POCRehabPotential").Equals("2"), new { @id = Model.Type + "_POCRehabPotential2", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential2" class="inline-radio">Poor</label>
                </div><div class="clear" />
                <%= Html.Templates(Model.Type + "_OtherRehabPotentialTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCOtherRehabPotential" })%>
                <%= Html.TextArea(Model.Type + "_POCOtherRehabPotential", data.ContainsKey("POCOtherRehabPotential") ? data["POCOtherRehabPotential"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCOtherRehabPotential", @class = "fill" })%>
            
               </div> 
            </td>
            <td colspan="5"><div id="<%=Model.Type %>DCPlanContainer">
                <label class="strong float-left">Patient to be discharged to the care of:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf1' class='radio' name='{1}_POCDCPlanCareOf' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCDCPlanCareOf1">Physician</label>
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf2' class='radio' name='{1}_POCDCPlanCareOf' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCDCPlanCareOf1">Caregiver</label>
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf3' class='radio' name='{1}_POCDCPlanCareOf' value='3' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCDCPlanCareOf1">Self care</label>
                </div>
                <div class="clear" />
                <label class="strong float-left">Discharge Plans:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCDCPlanPlans1">Discharge when caregiver willing and able to manage all aspects of patient’s care</label>
                    <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_POCDCPlanPlans1">Discharge when goals met.</label>
                </div>
            <div class="clear" />
                <%= Html.Templates(Model.Type + "_POCDCPlanAdditionalTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCDCPlanAdditional" })%>
                <%= Html.TextArea(Model.Type + "_POCDCPlanAdditional", data.ContainsKey("POCDCPlanAdditional") ? data["POCDCPlanAdditional"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCDCPlanAdditional", @class = "fill" })%>
            
            </div>
            </td>
            
        </tr>
        
        <tr>
            <th colspan="10">Skilled Care Provided
            <%= string.Format("<input class='radio' id='{0}IsSkilledCareApply' name='{0}_IsSkilledCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSkilledCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5" valign="top">
                <div id="<%=Model.Type %>SkilledCareContainer1">
                        <span class="float-left">Training Topics:</span>
                        <%= Html.Templates(Model.Type + "_POCSkilledCareTrainingTopicsTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTrainingTopics" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCareTrainingTopics", data.AnswerOrEmptyString("POCSkilledCareTrainingTopics"), 4,20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTrainingTopics" })%>
                        <div class="clear" />
                        <span class="float-left">Trained:</span>
                        <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained0' name='{0}_POCSkilledCareTrained' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToChecked())%>Patient
                        <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained1' name='{0}_POCSkilledCareTrained' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToChecked())%>Caregiver
                    
                    </div>
                    </td>
                    <td colspan="5">
                    <div id="<%=Model.Type %>SkilledCareContainer2">
                        <span class="float-left">Treatment Performed:</span>
                        <%= Html.Templates(Model.Type + "_POCSkilledCareTreatmentPerformedTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCareTreatmentPerformed", data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed"),4,20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
                        <div class="clear" />
                        <span class="float-left">Patient Response:</span>
                        <%= Html.Templates(Model.Type + "_POCSkilledCarePatientResponseTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCarePatientResponse" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCarePatientResponse", data.AnswerOrEmptyString("POCSkilledCarePatientResponse"),4,20, new { @class = "fill", @id = Model.Type + "_POCSkilledCarePatientResponse" })%>
                    </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Care Coordination
            <%= string.Format("<input class='radio' id='{0}IsCareApply' name='{0}_IsCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Safety Issues/Instruction/Education 
            <%= string.Format("<input class='radio' id='{0}IsSafetyIssueApply' name='{0}_IsSafetyIssueApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSafetyIssueApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>CareContainer">
            <%= Html.Templates(Model.Type + "_POCGenericCareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericCareCoordination" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericCareCoordination", data.ContainsKey("POCGenericCareCoordination") ? data["POCGenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericCareCoordination", @class = "fill" })%>
            </div></td>
            <td colspan="5"><div id="<%=Model.Type %>SafetyIssuesInstructionContainer">
            <%= Html.Templates(Model.Type + "_POCGenericSafetyIssueTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericSafetyIssue" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericSafetyIssue", data.ContainsKey("POCGenericSafetyIssue") ? data["POCGenericSafetyIssue"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericSafetyIssue", @class = "fill" })%>
            </div></td>
        </tr>  
        <tr>
            <th colspan="10">Notification</th>
        </tr>
        <tr>
            <td colspan="10">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev3.ascx", Model); %>
            </td>
        </tr>     
    </tbody>
</table>

<script type="text/javascript">
    U.HideIfChecked($("#<%= Model.Type %>IsDMEApply"), $("#<%= Model.Type %>DMEContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMedicalApply"), $("#<%= Model.Type %>MedicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLivingApply"), $("#<%= Model.Type %>LivingContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalApply"), $("#<%= Model.Type %>PhysicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsLOFApply"), $("#<%= Model.Type %>LOFContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalAssessmentApply"), $("#<%= Model.Type %>PhysicalAssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsHomeboundApply"), $("#<%= Model.Type %>HomeboundContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPTRApply"), $("#<%= Model.Type %>PTRContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsBedApply"), $("#<%= Model.Type %>BedContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGaitApply"), $("#<%= Model.Type %>GaitContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTransferApply"), $("#<%= Model.Type %>TransferContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsWBApply"), $("#<%= Model.Type %>WBContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsAssessmentApply"), $("#<%= Model.Type %>AssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTreatmentApply"), $("#<%= Model.Type %>TreatmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGoalsApply"), $("#<%= Model.Type %>GoalsContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRecommendationApply"), $("#<%= Model.Type %>RecommendationContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsCareApply"), $("#<%= Model.Type %>CareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsDCPlanApply"), $("#<%= Model.Type %>DCPlanContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsModalitiesApply"), $("#<%= Model.Type %>ModalitiesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSafetyIssueApply"), $("#<%= Model.Type %>SafetyIssuesInstructionContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRehabApply"), $("#<%= Model.Type %>RehabContainer"));
</script>