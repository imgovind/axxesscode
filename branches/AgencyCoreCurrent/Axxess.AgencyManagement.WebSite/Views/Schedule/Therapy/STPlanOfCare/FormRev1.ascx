﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<%  string sendAsOrder = data.ContainsKey("SendAsOrder") && data["SendAsOrder"].Answer.IsNotNullOrEmpty() ? data["SendAsOrder"].Answer : "1";%>

<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("Version", Model.Version)%>
    <%= Html.Hidden("DisciplineTask",Model.DisciplineTask) %>
    <%=Html.Hidden(Model.Type+"_VisitDate",Model.VisitDate) %>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="10">
                    <%= string.Format("{0}", Model.TypeName) %> 
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                 </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="10" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            
            <tr>
                <td colspan="5">
                    <div>
                        <label for="<%= Model.Type %>_PatientName" class="float-left">Patient Name:</label>
                        <label class="float-right" id="<%= Model.Type %>_PatientName"><%=Model.Patient.DisplayName%></label>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td colspan="5">
                    <div class="float-right hidden">
                        <%= string.Format("<input id='{1}_SendAsOrder' class='radio' name='{1}_SendAsOrder' value='1' type='checkbox' {0} />", sendAsOrder.Contains("1").ToChecked(), Model.Type)%>Send as an order
                    </div>
                    <div class="clear" />
                    <div>
                    <label for="<%= Model.Type %>_OrderNumber" class="float-left">Order Number:</label>
                    <div class="fr"><%= Html.Label(data.AnswerOrEmptyString("OrderNumber"))%></div>
                    </div>
                    <div class="clear" />
                    <div>
                        <label for="<%= Model.Type %>_PhysicianId" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_PhysicianId", data.ContainsKey("PhysicianId") ? data.AnswerOrEmptyString("PhysicianId") : Model.PhysicianId.ToString(), new { @id = Model.Type + "_PhysicianId", @class = "physicians" })%></div>
                     </div>
                    <div class="clear"></div>
                    
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="10">
                    <em>Click on the button below to refresh this page from the evaluation/Re-evaluation note associated with this episode.</em>
                    <div class="buttons">
                        <ul>
                            <li>
                                <a href="javascript:void(0);" onclick="UserInterface.RefreshTherapyPlanOfCare('<%= Model.Type  %>','<%= Model.EventId  %>','<%= Model.Version %>'); ">Refresh Data</a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_Content"><% Html.RenderPartial("~/Views/Schedule/Therapy/STPlanOfCare/ContentRev1.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>  
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"> <%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td>
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.STPlanOfCare.Submit($(this),false,'<%= Model.Type %>')" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.STPlanOfCare.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Visit.STPlanOfCare.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.STPlanOfCare.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>