﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericHomeboundReason = data.AnswerArray("GenericHomeboundReason"); %>
<%  string[] liquids = data.AnswerArray("GenericLiquids"); %>
<%  string[] genericReferralFor = data.AnswerArray("GenericReferralFor"); %>
<%  string[] POCgenericPlanOfCare = data.AnswerArray("POCGenericPlanOfCare"); %>
<%  string[] genericDischargeDiscussedWith = data.AnswerArray("GenericDischargeDiscussedWith"); %>
<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<table class="fixed nursing">
    <tbody>
        
        <tr>
            <th colspan="5">DME
            <%= string.Format("<input class='radio' id='{0}IsDMEApply' name='{0}_IsDMEApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDMEApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Medical Diagnosis
            <%= string.Format("<input class='radio' id='{0}IsMedicalApply' name='{0}_IsMedicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMedicalApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>DMEContainer">
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">Available:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMEAvailable", data.AnswerOrEmptyString("POCGenericDMEAvailable"), new { @class = "", @id = Model.Type + "_POCGenericDMEAvailable" })%>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">Needs:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMENeeds", data.AnswerOrEmptyString("POCGenericDMENeeds"), new { @class = "", @id = Model.Type + "_POCGenericDMENeeds" })%>
                </div>
                <div class="clear" />
                <div class="float-left">
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">Suggestion:</label>
                </div>
                <div class="float-right">
                <%= Html.TextBox(Model.Type + "_POCGenericDMESuggestion", data.AnswerOrEmptyString("POCGenericDMESuggestion"), new { @class = "", @id = Model.Type + "_POCGenericDMESuggestion" })%>
                </div>
                </div>
            </td>
            <td colspan="5"><div id="<%=Model.Type %>MedicalContainer">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev3.ascx", Model); %></div>
            </td>
        </tr>
        <tr>
            <th colspan="10">Treatment Plan
            <%= string.Format("<input class='radio' id='{0}IsTreatmentApply' name='{0}_IsTreatmentApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsTreatmentApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td  colspan="10" >
                  
                    <input type="hidden"  name="<%= Model.Type %>_GenericPlanOfCare" value="" />
                    <table class="fixed align-left" id="<%= Model.Type %>TreatmentContainer">
                        <tbody>
                             <tr>
                                 <td>
                                    <div class="float-left">
                                    <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="float-left">ST Frequency & Duration</label>
                                    <div class="float-left"><%= Html.TextBox(Model.Type + "_POCGenericFrequencyAndDuration", data.AnswerOrEmptyString("POCGenericFrequencyAndDuration"), new { @class = "", @id = Model.Type + "_POCGenericFrequencyAndDuration" })%></div>
                                    </div>
                                </td>
                            </tr>  
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare1' class='radio' name='{1}_POCGenericPlanOfCare' value='1' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare1">Evaluation (C1)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare2' class='radio' name='{1}_POCGenericPlanOfCare' value='2' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare2">Establish Rehab Program</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare3' class='radio' name='{1}_POCGenericPlanOfCare' value='3' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare3">Given to Patient</label>
                                </td>  
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare4' class='radio' name='{1}_POCGenericPlanOfCare' value='4' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare4">Attached to Chart</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare5' class='radio' name='{1}_POCGenericPlanOfCare' value='5' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("5").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare5">Patient/Family Education</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare6' class='radio' name='{1}_POCGenericPlanOfCare' value='6' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("6").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare6">Voice Disorders</label>
                                </td>    
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare7' class='radio' name='{1}_POCGenericPlanOfCare' value='7' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("7").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare7">Speech Articulation Disorders</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare8' class='radio' name='{1}_POCGenericPlanOfCare' value='8' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("8").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare8">Dysphagia Treatments</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare9' class='radio' name='{1}_POCGenericPlanOfCare' value='9' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("9").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare9">Language Disorders</label>
                                </td>                                
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare10' class='radio' name='{1}_POCGenericPlanOfCare' value='10' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("10").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare10">Aural Rehabilitation (C6)</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare11' class='radio' name='{1}_POCGenericPlanOfCare' value='11' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("11").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare11">Non-Oral Communication (C8)</label>
                                </td>
                                <td >
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare12' class='radio' name='{1}_POCGenericPlanOfCare' value='12' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("12").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare12">Alaryngeal Speech Skills</label>
                                </td>                               
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare15' class='radio' name='{1}_POCGenericPlanOfCare' value='15' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("15").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare15">Language Processing</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare16' class='radio' name='{1}_POCGenericPlanOfCare' value='16' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("16").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare16">Food Texture Recommendations</label>
                                </td> 
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare17' class='radio' name='{1}_POCGenericPlanOfCare' value='17' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("17").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare17">Safe Swallowing Evaluation</label>
                                </td>                              
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare18' class='radio' name='{1}_POCGenericPlanOfCare' value='18' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("18").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare18">Therapy to Increase Articulation, Proficiency, Verbal Expression</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare19' class='radio' name='{1}_POCGenericPlanOfCare' value='19' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("19").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare19">Lip, Tongue, Facial Exercises to Improve Swallowing/Vocal Skills</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare20' class='radio' name='{1}_POCGenericPlanOfCare' value='20' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("20").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare20">Pain Management</label>
                                </td>
                            </tr>
                            <tr>                               
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare21' class='radio' name='{1}_POCGenericPlanOfCare' value='21' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("21").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare21">Speech Dysphagia Instruction Program</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare22' class='radio' name='{1}_POCGenericPlanOfCare' value='22' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("22").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare22">Care of Voice Prosthesis &#8212; Removal, Cleaning, Site Maint</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare23' class='radio' name='{1}_POCGenericPlanOfCare' value='23' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("23").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare23">Teach/Develop Comm. System</label>
                                </td>
                            </tr>                            
                            <tr>                                
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare24' class='radio' name='{1}_POCGenericPlanOfCare' value='24' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("24").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare24">Trach Inst. and Care</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='{1}_POCGenericPlanOfCare25' class='radio' name='{1}_POCGenericPlanOfCare' value='25' type='checkbox' {0} />", POCgenericPlanOfCare.Contains("25").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare25">Other</label>
                                </td>
                            </tr>  
                            <tr>
                                 <td>
                                    <div class="float-left">
                                    <label for="<%= Model.Type %>_GenericEquipmentRecommendations" class="float-left">Equipment Recommendations</label>
                                    <%= Html.TextArea(Model.Type + "_POCGenericEquipmentRecommendations", data.AnswerOrEmptyString("POCGenericEquipmentRecommendations"), new { @id = Model.Type + "_POCGenericEquipmentRecommendations", @class = "fill" })%>
                                    </div>
                                </td>
                                <td>
                                    <div class="float-left">
                                    <label for="<%= Model.Type %>" class="float-left">Comments/ Additional Information</label>
                                    <%= Html.TextArea(Model.Type + "_POCGenericAdditionalInformation", data.AnswerOrEmptyString("POCGenericAdditionalInformation"), new { @id = Model.Type + "_POCGenericAdditionalInformation", @class = "fill" })%>
                                    </div>
                                </td>
                                <td>
                                   <div class="float-left">
                                   <label for="<%= Model.Type %>_GenericPlanForNextVisit" class="float-left">Plan for Next Visit</label>
                                   <%= Html.TextArea(Model.Type + "_POCGenericPlanForNextVisit", data.AnswerOrEmptyString("POCGenericPlanForNextVisit"), new { @id = Model.Type + "_POCGenericPlanForNextVisit", @class = "fill" })%>
                                   </div>  
                                </td>
                            </tr>  
                            <tr>  
                                <td>
                                 <div class="float-left">
                                 <label for="<%= Model.Type %>" class="float-left">Assessment</label>
                                 <%= Html.TextArea(Model.Type + "_POCGenericPatientDesiredOutcomes", data.AnswerOrEmptyString("POCGenericPatientDesiredOutcomes"), new { @id = Model.Type + "_POCGenericPatientDesiredOutcomes", @class = "fill" })%>
                                 </div>
                                </td>
                                
                            </tr>                 
                        </tbody>
                     </table>                  
            </td>
        </tr>
        <tr>
            <th colspan="10">Modalities<%= string.Format("<input class='radio' id='{0}IsModalitiesApply' name='{0}_IsModalitiesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsModalitiesApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>ModalitiesContainer">
            <%= Html.Templates(Model.Type + "_POCGenericModalitiesTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericModalitiesComment" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericModalitiesComment", data.ContainsKey("POCGenericModalitiesComment") ? data["POCGenericModalitiesComment"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCGenericModalitiesComment", @class = "fill" })%>
            </div>
            </td>
        </tr>
        
        <tr>
            <th colspan="10">ST Goals
            <%= string.Format("<input class='radio' id='{0}IsGoalsApply' name='{0}_IsGoalsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGoalsApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr id="<%= Model.Type %>GoalsContainer1">
            <td colspan="5">
                <div class="align-center strong">Short term goals</div>
                <%= Html.Templates(Model.Type + "_POCGenericShortTermGoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericShortTermGoalsComment" })%>
                    <%= Html.TextArea(Model.Type + "_POCGenericShortTermGoalsComment", data.AnswerOrEmptyString("POCGenericShortTermGoalsComment"), 8, 20, new { @id = Model.Type + "_POCGenericShortTermGoalsComment", @class = "fill" })%>
            </td>
            <td colspan="5">
                <div class="align-center strong">Long term goals</div>
                <%= Html.Templates(Model.Type + "_POCGenericLongTermGoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericLongTermGoalsComment" })%>
                    <%= Html.TextArea(Model.Type + "_POCGenericLongTermGoalsComment", data.AnswerOrEmptyString("POCGenericLongTermGoalsComment"), 8, 20, new { @id = Model.Type + "_POCGenericLongTermGoalsComment", @class = "fill" })%>
            </td>     
        </tr>
        <tr id="<%= Model.Type %>GoalsContainer2">
            <td colspan="10">
                <%= string.Format("<input class='radio' id='{0}POCSTGoalsDesired0' name='{0}_POCSTGoalsDesired' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSTGoalsDesired").Contains("0").ToChecked())%>Patient
                <%= string.Format("<input class='radio' id='{0}POCSTGoalsDesired1' name='{0}_POCSTGoalsDesired' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSTGoalsDesired").Contains("1").ToChecked())%>Caregiver
                <span> desired outcomes:</span>
                <%= Html.TextBox(Model.Type + "_POCSTGoalsDesiredOutcomes", data.AnswerOrEmptyString("POCSTGoalsDesiredOutcomes"), new { @class = "", @id = Model.Type + "_POCSTGoalsDesiredOutcomes" })%> 
            </td>
        </tr>
        <tr>
            <th colspan="10">Other Discipline Recommendation
            <%= string.Format("<input class='radio' id='{0}IsRecommendationApply' name='{0}_IsRecommendationApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRecommendationApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="10"><div id="<%=Model.Type %>RecommendationContainer">
                <%  string[] POCgenericDisciplineRecommendation = data.AnswerArray("POCGenericDisciplineRecommendation"); %>
                <input type="hidden" name="<%= Model.Type %>_POCGenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <div><label for="<%= Model.Type %>_GenericDisciplineRecommendation1" class="float-left">Disciplines</label></div>
                                <div class="margin">
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation1' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='1' type='checkbox' {0} />", POCgenericDisciplineRecommendation.Contains("1").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation1">OT</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation2' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='2' type='checkbox' {0} />", POCgenericDisciplineRecommendation.Contains("2").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation2">MSW</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation3' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='3' type='checkbox' {0} />", POCgenericDisciplineRecommendation.Contains("3").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation3">PT</label>
                                    <%= string.Format("<input id='{1}_POCGenericDisciplineRecommendation4' class='radio' name='{1}_POCGenericDisciplineRecommendation' value='4' type='checkbox' {0} />", POCgenericDisciplineRecommendation.Contains("4").ToChecked(), Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendation4">Podiatrist</label>
                                    <span>Other</span>
                                    <%= Html.TextBox(Model.Type + "_POCGenericDisciplineRecommendationOther", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther"), new { @id = Model.Type + "_POCGenericDisciplineRecommendationOther" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div>
                                    <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="strong">Reason</label>
                                    <%= Html.Templates(Model.Type + "_DisciplineRecommendationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                                    <%= Html.TextArea(Model.Type + "_POCGenericDisciplineRecommendationReason", data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason"), new { @class = "fill", @id = Model.Type + "_POCGenericDisciplineRecommendationReason" })%>
                                </div>
                            </td>
                        </tr>
                        
                        
                    </tbody>
                </table>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Rehab <%= string.Format("<input class='radio' id='{0}IsRehabApply' name='{0}_IsRehabApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRehabApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Discharge Plan<%= string.Format("<input class='radio' id='{0}IsDCPlanApply' name='{0}_IsDCPlanApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsDCPlanApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>RehabContainer">
                <div class="float-left"><label for="<%= Model.Type %>_GenericDisciplineRehabDiagnosis" class="strong">Rehab Diagnosis:</label><%= Html.TextBox(Model.Type + "_POCGenericDisciplineRehabDiagnosis", data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis"), new { @class = "", @id = Model.Type + "_POCGenericDisciplineRehabDiagnosis" })%></div>
                    <div class="clear" />        
                <div class="float-left">
                    <label for="<%= Model.Type %>_GenericRehabPotential" class="strong">Rehab Potential</label>
                    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "0", data.AnswerOrEmptyString("POCRehabPotential").Equals("0"), new { @id = Model.Type + "_POCRehabPotential0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential0" class="inline-radio">Good</label>
                    <%= Html.RadioButton(Model.Type + "_POCRehabPotential", "1", data.AnswerOrEmptyString("POCRehabPotential").Equals("1"), new { @id = Model.Type + "_POCRehabPotential1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential1" class="inline-radio">Fair</label>
                    <%= Html.RadioButton(Model.Type + "_RehabPotential", "2", data.AnswerOrEmptyString("POCRehabPotential").Equals("2"), new { @id = Model.Type + "_POCRehabPotential2", @class = "radio" })%>
                    <label for="<%= Model.Type %>_RehabPotential2" class="inline-radio">Poor</label>
                </div><div class="clear" />
                <%= Html.Templates(Model.Type + "_OtherRehabPotentialTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCOtherRehabPotential" })%>
                <%= Html.TextArea(Model.Type + "_POCOtherRehabPotential", data.ContainsKey("POCOtherRehabPotential") ? data["POCOtherRehabPotential"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCOtherRehabPotential", @class = "fill" })%>
            
               </div> 
            </td>
            <td colspan="5"><div id="<%=Model.Type %>DCPlanContainer">
                <label class="strong float-left">Patient to be discharged to the care of:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf1' class='radio' name='{1}_POCDCPlanCareOf' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Physician</label>
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf2' class='radio' name='{1}_POCDCPlanCareOf' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Caregiver</label>
                    <%= string.Format("<input id='{1}_POCDCPlanCareOf3' class='radio' name='{1}_POCDCPlanCareOf' value='3' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanCareOf1">Self care</label>
                </div>
                <div class="clear" />
                <label class="strong float-left">Discharge Plans:</label>
                <div class="margin">
                    <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanPlans1">Discharge when caregiver willing and able to manage all aspects of patient’s care</label>
                    <%= string.Format("<input id='{1}_POCDCPlanPlans1' class='radio' name='{1}_POCDCPlanPlans' value='2' type='checkbox' {0} />", data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_DCPlanPlans1">Discharge when goals met.</label>
                </div>
            <div class="clear" />
                <%= Html.Templates(Model.Type + "_DCPlanAdditionalTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCDCPlanAdditional" })%>
                <%= Html.TextArea(Model.Type + "_POCDCPlanAdditional", data.ContainsKey("POCDCPlanAdditional") ? data["POCDCPlanAdditional"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_POCDCPlanAdditional", @class = "fill" })%>
            
            </div>
            </td>
            
        </tr>
        
        <tr>
            <th colspan="10">Skilled Care Provided
            <%= string.Format("<input class='radio' id='{0}IsSkilledCareApply' name='{0}_IsSkilledCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSkilledCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5" valign="top">
                <div id="<%=Model.Type %>SkilledCareContainer1">
                        <span class="float-left">Training Topics:</span>
                        <%= Html.Templates(Model.Type + "_SkilledCareTrainingTopicsTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTrainingTopics" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCareTrainingTopics", data.AnswerOrEmptyString("POCSkilledCareTrainingTopics"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTrainingTopics" })%>
                        <div class="clear" />
                        <span class="float-left">Trained:</span>
                        <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained0' name='{0}_POCSkilledCareTrained' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToChecked())%>Patient
                        <%= string.Format("<input class='radio' id='{0}POCSkilledCareTrained1' name='{0}_POCSkilledCareTrained' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToChecked())%>Caregiver
                    
                    </div>
                    </td>
                    <td colspan="5">
                    <div id="<%=Model.Type %>SkilledCareContainer2">
                        <span class="float-left">Treatment Performed:</span>
                        <%= Html.Templates(Model.Type + "_SkilledCareTreatmentPerformedTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCareTreatmentPerformed", data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCareTreatmentPerformed" })%>
                        <div class="clear" />
                        <span class="float-left">Patient Response:</span>
                        <%= Html.Templates(Model.Type + "_SkilledCarePatientResponseTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_POCSkilledCarePatientResponse" })%>
                        <%= Html.TextArea(Model.Type + "_POCSkilledCarePatientResponse", data.AnswerOrEmptyString("POCSkilledCarePatientResponse"), 4, 20, new { @class = "fill", @id = Model.Type + "_POCSkilledCarePatientResponse" })%>
                    </div>
            </td>
        </tr>
        <tr>
            <th colspan="5">Care Coordination
            <%= string.Format("<input class='radio' id='{0}IsCareApply' name='{0}_IsCareApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsCareApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
            <th colspan="5">Safety Issues/Instruction/Education 
            <%= string.Format("<input class='radio' id='{0}IsSafetyIssueApply' name='{0}_IsSafetyIssueApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsSafetyIssueApply").Contains("1").ToChecked())%>
                <label>N/A</label>
            </th>
        </tr>
        <tr>
            <td colspan="5"><div id="<%=Model.Type %>CareContainer">
            <%= Html.Templates(Model.Type + "_GenericCareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericCareCoordination" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericCareCoordination", data.ContainsKey("POCGenericCareCoordination") ? data["POCGenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericCareCoordination", @class = "fill" })%>
            </div></td>
            <td colspan="5"><div id="<%=Model.Type %>SafetyIssuesInstructionContainer">
            <%= Html.Templates(Model.Type + "_GenericSafetyIssueTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_POCGenericSafetyIssue" })%>
            <%= Html.TextArea(Model.Type + "_POCGenericSafetyIssue", data.ContainsKey("POCGenericSafetyIssue") ? data["POCGenericSafetyIssue"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_POCGenericSafetyIssue", @class = "fill" })%>
            </div></td>
        </tr>  
        <tr>
            <th colspan="10">Notification</th>
        </tr>
        <tr>
            <td colspan="10">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Notification/FormRev3.ascx", Model); %>
            </td>
        </tr>     
  
        
        
    </tbody>
</table>

<script type="text/javascript">
    //U.HideIfChecked($("#<%= Model.Type %>IsVitalSignsApply"), $("#<%= Model.Type %>VitalSignsContainer"));
    //U.HideIfChecked($("#<%= Model.Type %>IsMentalApply"), $("#<%= Model.Type %>MentalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMedicalApply"), $("#<%= Model.Type %>MedicalContainer"));
    //U.HideIfChecked($("#<%= Model.Type %>IsPainApply"), $("#<%= Model.Type %>PainContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsDMEApply"), $("#<%= Model.Type %>DMEContainer"));
    //U.HideIfChecked($("#<%= Model.Type %>IsLivingApply"), $("#<%= Model.Type %>LivingContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalApply"), $("#<%= Model.Type %>PhysicalContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsLOFApply"), $("#<%= Model.Type %>LOFContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsPhysicalAssessmentApply"), $("#<%= Model.Type %>PhysicalAssessmentContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsHomeboundApply"), $("#<%= Model.Type %>HomeboundContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsPTRApply"), $("#<%= Model.Type %>PTRContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsBedApply"), $("#<%= Model.Type %>BedContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsGaitApply"), $("#<%= Model.Type %>GaitContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsTransferApply"), $("#<%= Model.Type %>TransferContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsWBApply"), $("#<%= Model.Type %>WBContainer"));
//    U.HideIfChecked($("#<%= Model.Type %>IsAssessmentApply"), $("#<%= Model.Type %>AssessmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTreatmentApply"), $("#<%= Model.Type %>TreatmentContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGoalsApply"), $("#<%= Model.Type %>GoalsContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsGoalsApply"), $("#<%= Model.Type %>GoalsContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsRecommendationApply"), $("#<%= Model.Type %>RecommendationContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsTestApply"), $("#<%= Model.Type %>TestContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsCareApply"), $("#<%= Model.Type %>CareContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer1"));
    U.HideIfChecked($("#<%= Model.Type %>IsSkilledCareApply"), $("#<%= Model.Type %>SkilledCareContainer2"));
    U.HideIfChecked($("#<%= Model.Type %>IsDCPlanApply"), $("#<%= Model.Type %>DCPlanContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsModalitiesApply"), $("#<%= Model.Type %>ModalitiesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsSafetyIssueApply"), $("#<%= Model.Type %>SafetyIssuesInstructionContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRehabApply"), $("#<%= Model.Type %>RehabContainer"));
</script>
