﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">COTA Supervisory Visit | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "OTSupervisoryVisitForm" })) { %>
    <%= Html.Hidden("OTSupervisoryVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("OTSupervisoryVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("OTSupervisoryVisit_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("DisciplineTask", "122")%>
    <%= Html.Hidden("Type", "OTSupervisoryVisit")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    COTA Supervisory Visit
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="OTSupervisoryVisit_Therapist" class="float-left">COTA:</label>
                        <div class="float-right"><%= Html.OTTherapists("Therapist", data.ContainsKey("Therapist") ? data["Therapist"].Answer : "", new { @id = "OTSupervisoryVisit_Therapist" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left">COTA Present:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("OTSupervisoryVisit_TherapistPresent", "1", data.ContainsKey("TherapistPresent") && data["TherapistPresent"].Answer == "1" ? true : false, new { @id="OTSupervisoryVisit_TherapistPresentY", @class="radio" })%>
                            <label class="inline-radio" for="OTSupervisoryVisit_TherapistPresentY">Yes</label>
                            <%= Html.RadioButton("OTSupervisoryVisit_TherapistPresent", "0", data.ContainsKey("TherapistPresent") && data["TherapistPresent"].Answer == "0" ? true : false, new { @id="OTSupervisoryVisit_TherapistPresentN", @class="radio" })%>
                            <label class="inline-radio" for="OTSupervisoryVisit_TherapistPresentN">No</label>
                        </div>
                    </div>
                </td>
                <td colspan="2">
                    <div>
                        <label for="OTSupervisoryVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="OTSupervisoryVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="OTSupervisoryVisit_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="OTSupervisoryVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("OTSupervisoryVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digitd", @maxlength=6, @id = "OTSupervisoryVisit_AssociatedMileage" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Evaluation</th>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">1.</span>
                        Arrives for assigned visits as scheduled:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_ArriveOnTimeY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_ArriveOnTimeY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_ArriveOnTimeN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_ArriveOnTimeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">2.</span>
                        Follows client&#8217;s plan of care:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_FollowPOCY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_FollowPOCY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_FollowPOCN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_FollowPOCN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">3.</span>
                        Demonstrates positive and helpful attitude towards the client and others:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_HasPositiveAttitudeY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_HasPositiveAttitudeY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_HasPositiveAttitudeN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_HasPositiveAttitudeN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">4.</span>
                        Informs OT/Nurse Supervisor of client needs and changes in condition as appropriate:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_InformChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_InformChangesY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_InformChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_InformChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">5.</span>
                        COTA Implements Universal Precautions per agency policy:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_IsUniversalPrecautionsY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_IsUniversalPrecautionsY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_IsUniversalPrecautionsN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_IsUniversalPrecautionsN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">6.</span>
                        Any changes made to client plan of care at this time:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_POCChangesY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_POCChangesY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_POCChangesN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_POCChangesN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="float-left">
                        <span class="alphali">7.</span>
                        Patient/CG satisfied with care and services provided by COTA:
                    </div>
                </td>
                <td>
                    <div class="float-left">
                        <%= Html.RadioButton("OTSupervisoryVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "OTSupervisoryVisit_IsServicesSatisfactoryY", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_IsServicesSatisfactoryY">Yes</label>
                        <%= Html.RadioButton("OTSupervisoryVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "OTSupervisoryVisit_IsServicesSatisfactoryN", @class = "radio" })%>
                        <label class="inline-radio" for="OTSupervisoryVisit_IsServicesSatisfactoryN">No</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="float-left">
                        <span class="alphali">8.</span>
                        Additional Comments/Findings:
                    </div>
                    <div class="clear"></div>
                    <div class="float-left" style="width:99%"><%= Html.TextArea("OTSupervisoryVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @style="height:80px;width:100%" })%></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="OTSupervisoryVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password("OTSupervisoryVisit_Clinician", "", new { @class = "", @id = "OTSupervisoryVisit_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="OTSupervisoryVisit_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="OTSupervisoryVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="OTSupervisoryVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="OTSupervisoryVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.OTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.OTSupervisoryVisit.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Visit.OTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.OTSupervisoryVisit.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#window_OTSupervisoryVisit form label.error").css({ 'position': 'relative', 'float': 'left' });
    function OTSupervisoryVisitAdd() {
        $("#OTSupervisoryVisit_Clinician").removeClass('required').addClass('required');
        $("#OTSupervisoryVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function OTSupervisoryVisitRemove() {
        $("#OTSupervisoryVisit_Clinician").removeClass('required');
        $("#OTSupervisoryVisit_SignatureDate").removeClass('required');
    }
</script>