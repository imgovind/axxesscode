﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (data.AnswerOrEmptyString("POCIsSafetyIssueApply").Equals("1"))
   {%>

<script type="text/javascript">
        printview.addsubsection(
        printview.checkbox("N/A", true),
        "Safety Issues/Instruction/Education");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericSafetyIssue").Clean() %>",0,3),"Safety Issues/Instruction/Education");
</script>

<%} %>