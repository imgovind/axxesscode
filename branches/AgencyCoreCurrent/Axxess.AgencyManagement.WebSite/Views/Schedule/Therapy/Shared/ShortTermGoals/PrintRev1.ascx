﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal1").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal1TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal2").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal2TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal3").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal3TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal4").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal4TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal5").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal5TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Freq.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShortTermFrequency").Clean() %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShortTermDuration").Clean() %>",0,1)),
        "Short Term Goals");
</script>