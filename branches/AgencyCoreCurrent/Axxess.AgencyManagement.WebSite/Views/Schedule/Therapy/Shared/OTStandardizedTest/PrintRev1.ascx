﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span(""),
        "Standardized test");
    printview.addsubsection(
        printview.col(2,
            printview.span("Katz Index:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericKatzIndex").Clean() %>",0,1)+
            printview.span("9 Hole Peg Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("Generic9HolePeg").Clean() %>",0,1)+
            printview.span("Lawton & Brody IADL Scale:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericLawtonBrody").Clean() %>",0,1)+
            printview.span("Mini-Mental State Exam:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMiniMentalState").Clean() %>",0,1)+
            printview.span("Other:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericOtherTest").Clean() %>:<%= data.AnswerOrEmptyString("GenericStandardizedTestOther").Clean() %>",0,1)),
            "Prior",2);
    printview.addsubsection(
        printview.col(2,
            printview.span("Katz Index:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericKatzIndex1").Clean() %>",0,1)+
            printview.span("9 Hole Peg Test:",true)+
            printview.span("<%= data.AnswerOrEmptyString("Generic9HolePeg1").Clean() %>",0,1)+
            printview.span("Lawton & Brody IADL Scale:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericLawtonBrody1").Clean() %>",0,1)+
            printview.span("Mini-Mental State Exam:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericMiniMentalState1").Clean() %>",0,1)+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericOtherTest1").Clean() %>:<%= data.AnswerOrEmptyString("GenericStandardizedTestOther1").Clean()%>",0,1)),
            "Current");
</script>