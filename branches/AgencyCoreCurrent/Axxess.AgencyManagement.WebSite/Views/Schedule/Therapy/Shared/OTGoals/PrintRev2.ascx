﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var type=Model.Type; %>
<%  string[] genericOTGoals = data.AnswerArray("POCGenericOTGoals"); %>
<script type="text/javascript">
    printview.addsection(
        <%if(genericOTGoals.Contains("1")){ %>
        printview.checkbox("Patient will be able to perform upper body dressing with <%= data.AnswerOrEmptyString("POCGenericOTGoals1With").Clean() %> assist  utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals1With").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals1Weeks").Clean() %> weeks",<%= genericOTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("2")){ %>
        printview.checkbox("Patient will be able to perform lower body dressing with <%= data.AnswerOrEmptyString("POCGenericOTGoals2With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals2Utilize").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals2Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform toilet hygiene with <%= data.AnswerOrEmptyString("POCGenericOTGoals3Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals3Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("4")){ %>
        printview.checkbox("Patient will be able to perform grooming with <%= data.AnswerOrEmptyString("POCGenericOTGoals4Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals4Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("5")){ %>
        printview.checkbox("Patient will be able to perform <%= data.AnswerOrEmptyString("POCGenericOTGoals5With").Clean() %> (task)  with <%= data.AnswerOrEmptyString("POCGenericOTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("POCGenericOTGoals5Within").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals5Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("6")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("POCGenericOTGoals6Of").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericOTGoals6To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals6Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals6Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("7")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("POCGenericOTGoals7Skill").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericOTGoals7Assist").Clean() %> grade to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals7Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("POCGenericOTGoals7Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("8")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericOTGoals8To").Clean() %> to improve postural control and balance necessary to perform <%= data.AnswerOrEmptyString("POCGenericOTGoals8Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("POCGenericOTGoals8Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("9")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericOTGoals9Within").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("POCGenericOTGoals9Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("10")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("POCGenericOTGoals10Device").Clean() %> joint to <%= data.AnswerOrEmptyString("POCGenericOTGoals10Feet").Clean() %> degree of <%= data.AnswerOrEmptyString("POCGenericOTGoals10Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals10Weeks").Clean() %> weeks to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals10DegreeOf").Clean() %> (task).",<%= genericOTGoals.Contains("10").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("11")){ %>
        printview.checkbox("Patient/caregiver will be able to perform HEP safely and effectively within <%= data.AnswerOrEmptyString("POCGenericOTGoals11Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("12")){ %>
        printview.checkbox("Patient will learn <%= data.AnswerOrEmptyString("POCGenericOTGoals12Device").Clean() %> techniques to <%= data.AnswerOrEmptyString("POCGenericOTGoals12Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals12Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("13")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("POCGenericOTGoals13Of").Clean() %> standardized test score to <%= data.AnswerOrEmptyString("POCGenericOTGoals13To").Clean() %> to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals13Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("14")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of brace/splint within <%= data.AnswerOrEmptyString("POCGenericOTGoals14Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("15")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of wheelchair with <%= data.AnswerOrEmptyString("POCGenericOTGoals15To").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals15Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("16")){ %>
        printview.checkbox("Patient will be able to perform upper body bathing with <%= data.AnswerOrEmptyString("POCGenericOTGoals16With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals16To").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals16Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("17")){ %>
        printview.checkbox("Patient will be able to perform lower body bathing with <%= data.AnswerOrEmptyString("POCGenericOTGoals17Joint").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals17Degree").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals17Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        printview.span("Frequency:<%= data.AnswerOrEmptyString("POCGenericOTGoalsFrequency").Clean() %>")+
        printview.span("Duration:<%= data.AnswerOrEmptyString("POCGenericOTGoalsDuration").Clean() %>")+
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTGoalsComments").Clean() %>",0,2)+
        printview.span("OT Short Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTShortTermGoalsComments").Clean() %>",0,2)+
        printview.span("OT Long Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTLongTermGoalsComments").Clean() %>",0,2)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver desired outcomes:<%=data.AnswerOrEmptyString("POCOTGoalsDesiredOutcomes").Clean() %>",<%= data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("1").ToString().ToLower() %>)),
        "OT Goals");
        
</script>