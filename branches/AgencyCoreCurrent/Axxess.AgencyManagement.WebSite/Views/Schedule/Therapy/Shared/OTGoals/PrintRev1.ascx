﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var type=Model.Type; %>
<%  string[] genericOTGoals = data.AnswerArray("GenericOTGoals"); %>
<script type="text/javascript">
    printview.addsection(
        <%if(genericOTGoals.Contains("1")){ %>
        printview.checkbox("Patient will be able to perform upper body dressing with <%= data.AnswerOrEmptyString("GenericOTGoals1With").Clean() %> assist  utilizing <%= data.AnswerOrEmptyString("GenericOTGoals1With").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals1Weeks").Clean() %> weeks",<%= genericOTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("2")){ %>
        printview.checkbox("Patient will be able to perform lower body dressing with <%= data.AnswerOrEmptyString("GenericOTGoals2With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals2Utilize").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals2Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform toilet hygiene with <%= data.AnswerOrEmptyString("GenericOTGoals3Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals3Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("4")){ %>
        printview.checkbox("Patient will be able to perform grooming with <%= data.AnswerOrEmptyString("GenericOTGoals4Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals4Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("5")){ %>
        printview.checkbox("Patient will be able to perform <%= data.AnswerOrEmptyString("GenericOTGoals5With").Clean() %> (task)  with <%= data.AnswerOrEmptyString("GenericOTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("GenericOTGoals5Within").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals5Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("6")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("GenericOTGoals6Of").Clean() %> to <%= data.AnswerOrEmptyString("GenericOTGoals6To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("GenericOTGoals6Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals6Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("7")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("GenericOTGoals7Skill").Clean() %> to <%= data.AnswerOrEmptyString("GenericOTGoals7Assist").Clean() %> grade to improve <%= data.AnswerOrEmptyString("GenericOTGoals7Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("GenericOTGoals7Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("8")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericOTGoals8To").Clean() %> to improve postural control and balance necessary to perform <%= data.AnswerOrEmptyString("GenericOTGoals8Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("GenericOTGoals8Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("9")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("GenericOTGoals9Within").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("GenericOTGoals9Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("10")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("GenericOTGoals10Device").Clean() %> joint to <%= data.AnswerOrEmptyString("GenericOTGoals10Feet").Clean() %> degree of <%= data.AnswerOrEmptyString("GenericOTGoals10Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals10Weeks").Clean() %> weeks to improve <%= data.AnswerOrEmptyString("GenericOTGoals10DegreeOf").Clean() %> (task).",<%= genericOTGoals.Contains("10").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("11")){ %>
        printview.checkbox("Patient/caregiver will be able to perform HEP safely and effectively within <%= data.AnswerOrEmptyString("GenericOTGoals11Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("12")){ %>
        printview.checkbox("Patient will learn <%= data.AnswerOrEmptyString("GenericOTGoals12Device").Clean() %> techniques to <%= data.AnswerOrEmptyString("GenericOTGoals12Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals12Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("13")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("GenericOTGoals13Of").Clean() %> standardized test score to <%= data.AnswerOrEmptyString("GenericOTGoals13To").Clean() %> to improve <%= data.AnswerOrEmptyString("GenericOTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("GenericOTGoals13Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("14")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of brace/splint within <%= data.AnswerOrEmptyString("GenericOTGoals14Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("15")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of wheelchair with <%= data.AnswerOrEmptyString("GenericOTGoals15To").Clean() %> assist within <%= data.AnswerOrEmptyString("GenericOTGoals15Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("16")){ %>
        printview.checkbox("Patient will be able to perform upper body bathing with <%= data.AnswerOrEmptyString("GenericOTGoals16With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals16To").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals16Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("17")){ %>
        printview.checkbox("Patient will be able to perform lower body bathing with <%= data.AnswerOrEmptyString("GenericOTGoals17Joint").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("GenericOTGoals17Degree").Clean() %> device within <%= data.AnswerOrEmptyString("GenericOTGoals17Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        printview.span("Frequency:<%= data.AnswerOrEmptyString("GenericOTGoalsFrequency").Clean() %>")+
        printview.span("Duration:<%= data.AnswerOrEmptyString("GenericOTGoalsDuration").Clean() %>")+
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericOTGoalsComments").Clean() %>",0,2)+
        printview.span("Rehab Potential:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericRehabPotential").Clean() %>",0,2),
        "OT Goals");
        
</script>