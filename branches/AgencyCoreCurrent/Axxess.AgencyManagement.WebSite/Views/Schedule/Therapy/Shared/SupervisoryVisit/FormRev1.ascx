﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] superVisory = data.AnswerArray("SuperVisory"); %>
<div class="float-left">
                <%= string.Format("<input id='{1}_SuperVisory1' class='radio' name='{1}_SuperVisory' value='1' type='checkbox' {0} />", superVisory != null && superVisory.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_SuperVisory1">
                                        OT Assistant</label>
                <%= string.Format("<input id='{1}_SuperVisory2' class='radio' name='{1}_SuperVisory' value='2' type='checkbox' {0} />", superVisory != null && superVisory.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_SuperVisory2">
                                        Aide</label>
                <%= string.Format("<input id='{1}_SuperVisory3' class='radio' name='{1}_SuperVisory' value='3' type='checkbox' {0} />", superVisory != null && superVisory.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_SuperVisory3">
                                        Present</label>
                <%= string.Format("<input id='{1}_SuperVisory4' class='radio' name='{1}_SuperVisory' value='4' type='checkbox' {0} />", superVisory != null && superVisory.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_SuperVisory4">
                                        Not Present</label>
                </div>
                <div class="clear"></div>
                <div class="float-left">Observation of:<%= Html.TextBox(Model.Type + "_GenericSupervisoryObservation", data.AnswerOrEmptyString("GenericSupervisoryObservation"), new { @class = "", @id = Model.Type + "_GenericSupervisoryObservation" })%></div>
                <div class="clear"></div>
                <div class="float-left">Teaching/Training of:<%= Html.TextBox(Model.Type + "_GenericSupervisoryTeaching", data.AnswerOrEmptyString("GenericSupervisoryTeaching"), new { @class = "", @id = Model.Type + "_GenericSupervisoryTeaching" })%></div>
                <div class="clear"></div>
                <div class="float-left">Care plan reviewed/revised with assistant/aide during this visit:</div>
                <div class="clear"></div>
                <div class="float-left"><%= Html.RadioButton(Model.Type + "_GenericSupervisoryCarePLanReviewed", "1", data.AnswerOrEmptyString("GenericSupervisoryCarePLanReviewed").Equals("1"), new { @id = Model.Type + "_GenericSupervisoryCarePLanReviewed1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSupervisoryCarePLanReviewed", "0", data.AnswerOrEmptyString("GenericSupervisoryCarePLanReviewed").Equals("0"), new { @id = Model.Type + "_GenericSupervisoryCarePLanReviewed0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs0" class="inline-radio">No</label>
                    If yes(specify)<%= Html.TextBox(Model.Type + "_GenericSupervisorySpecify", data.AnswerOrEmptyString("GenericSupervisorySpecify"), new { @class = "", @id = Model.Type + "_GenericSupervisorySpecify" })%>
                </div>
                <div class="clear"></div>
                <div class="float-left">If OT assistant/aide not present, specify date he/she was contacted regarding updated care plan:
                    <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_GenericSupervisoryDate"
                    value="<%= data.AnswerOrEmptyString("GenericSupervisoryDate") %>" id="<%= Model.Type %>_GenericSupervisoryDate" />
                </div>