﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%=Model.Type %>DMEContainer">
    <div class="float-left">
        <label for="<%= Model.Type %>_GenericDMEAvailable" class="float-left">
            Available:</label>
    </div>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_POCGenericDMEAvailable", data.AnswerOrEmptyString("POCGenericDMEAvailable"), new { @class = "", @id = Model.Type + "_POCGenericDMEAvailable" })%>
    </div>
    <div class="clear" />
    <div class="float-left">
        <label for="<%= Model.Type %>_GenericDMENeeds" class="float-left">
            Needs:</label>
    </div>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_POCGenericDMENeeds", data.AnswerOrEmptyString("POCGenericDMENeeds"), new { @class = "", @id = Model.Type + "_POCGenericDMENeeds" })%>
    </div>
    <div class="clear" />
    <div class="float-left">
        <label for="<%= Model.Type %>_GenericDMESuggestion" class="float-left">
            Suggestion:</label>
    </div>
    <div class="float-right">
        <%= Html.TextBox(Model.Type + "_POCGenericDMESuggestion", data.AnswerOrEmptyString("POCGenericDMESuggestion"), new { @class = "", @id = Model.Type + "_POCGenericDMESuggestion" })%>
    </div>
</div>
