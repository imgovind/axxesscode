﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSensoryAcuity = data.AnswerArray("GenericSensoryAcuity"); %>
<%  string[] genericSensoryTracking = data.AnswerArray("GenericSensoryTracking"); %>
<%  string[] genericSensoryVisual = data.AnswerArray("GenericSensoryVisual"); %>

<table>
        <tr>
            <td style="width:50%">
                <table>
                    <tr>
                        <td rowspan="2">Area</td>
                        <td colspan="2">Sharp/Dull</td>
                        <td colspan="2">Light/Firm Touch</td>
                        <td colspan="2">Proprioception</td>
                    </tr>
                    <tr>
                        <td>Right</td>
                        <td>Left</td>
                        <td>Right</td>
                        <td>Left</td>
                        <td>Right</td>
                        <td>Left</td>
                    </tr>
                    <tr>
                        <td><%= Html.TextBox(Model.Type + "_GenericSensoryArea1", data.AnswerOrEmptyString("GenericSensoryArea1"), new { @id = Model.Type + "_GenericSensoryArea1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpRight1", data.AnswerOrEmptyString("GenericSensorySharpRight1"), new { @id = Model.Type + "_GenericSensorySharpRight1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft1", data.AnswerOrEmptyString("GenericSensorySharpLeft1"), new { @id = Model.Type + "_GenericSensorySharpLeft1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightRight1", data.AnswerOrEmptyString("GenericSensoryLightRight1"), new { @id = Model.Type + "_GenericSensoryLightRight1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft1", data.AnswerOrEmptyString("GenericSensoryLightLeft1"), new { @id = Model.Type + "_GenericSensoryLightLeft1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight1", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight1"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight1", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft1", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft1"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft1", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <td><%= Html.TextBox(Model.Type + "_GenericSensoryArea2", data.AnswerOrEmptyString("GenericSensoryArea2"), new { @id = Model.Type + "_GenericSensoryArea2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpRight2", data.AnswerOrEmptyString("GenericSensorySharpRight2"), new { @id = Model.Type + "_GenericSensorySharpRight2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft2", data.AnswerOrEmptyString("GenericSensorySharpLeft2"), new { @id = Model.Type + "_GenericSensorySharpLeft2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightRight2", data.AnswerOrEmptyString("GenericSensoryLightRight2"), new { @id = Model.Type + "_GenericSensoryLightRight2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft2", data.AnswerOrEmptyString("GenericSensoryLightLeft2"), new { @id = Model.Type + "_GenericSensoryLightLeft2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight2", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight2"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight2", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft2", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft2"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft2", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <td><%= Html.TextBox(Model.Type + "_GenericSensoryArea3", data.AnswerOrEmptyString("GenericSensoryArea3"), new { @id = Model.Type + "_GenericSensoryArea3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpRight3", data.AnswerOrEmptyString("GenericSensorySharpRight3"), new { @id = Model.Type + "_GenericSensorySharpRight3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft3", data.AnswerOrEmptyString("GenericSensorySharpLeft3"), new { @id = Model.Type + "_GenericSensorySharpLeft3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightRight3", data.AnswerOrEmptyString("GenericSensoryLightRight3"), new { @id = Model.Type + "_GenericSensoryLightRight3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft3", data.AnswerOrEmptyString("GenericSensoryLightLeft3"), new { @id = Model.Type + "_GenericSensoryLightLeft3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight3", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight3"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight3", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft3", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft3"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft3", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <td><%= Html.TextBox(Model.Type + "_GenericSensoryArea4", data.AnswerOrEmptyString("GenericSensoryArea4"), new { @id = Model.Type + "_GenericSensoryArea4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpRight4", data.AnswerOrEmptyString("GenericSensorySharpRight4"), new { @id = Model.Type + "_GenericSensorySharpRight4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft4", data.AnswerOrEmptyString("GenericSensorySharpLeft4"), new { @id = Model.Type + "_GenericSensorySharpLeft4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightRight4", data.AnswerOrEmptyString("GenericSensoryLightRight4"), new { @id = Model.Type + "_GenericSensoryLightRight4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft4", data.AnswerOrEmptyString("GenericSensoryLightLeft4"), new { @id = Model.Type + "_GenericSensoryLightLeft4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight4", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight4"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight4", @class = "fill" })%></td>
                        <td><%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft4", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft4"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft4", @class = "fill" })%></td>
                    </tr>
                </table>
            </td>
            <td>
                <div><span class="float-left"><b>Visual Skills: Acuity:</b></span>
                <%= string.Format("<input class='radio' id='{1}_GenericSensoryAcuity1' name='{1}_GenericSensoryAcuity' value='1' type='checkbox' {0} />", genericSensoryAcuity.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericSensoryAcuity" >Intact</label>
                <%= string.Format("<input class='radio' id='{1}_GenericSensoryAcuity2' name='{1}_GenericSensoryAcuity' value='2' type='checkbox' {0} />", genericSensoryAcuity.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericSensoryAcuity" >Impaired</label>
                <%= string.Format("<input class='radio' id='{1}_GenericSensoryAcuity3' name='{1}_GenericSensoryAcuity' value='3' type='checkbox' {0} />", genericSensoryAcuity.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericSensoryAcuity" >Double</label>
                <%= string.Format("<input class='radio' id='{1}_GenericSensoryAcuity4' name='{1}_GenericSensoryAcuity' value='4' type='checkbox' {0} />", genericSensoryAcuity.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericSensoryAcuity" >Blurred</label>
                </div>
                <div class="clear"></div>
                <div>
                    <span class="float-left"><b>Tracking:</b></span>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryTracking1' name='{1}_GenericSensoryTracking' value='1' type='checkbox' {0} />", genericSensoryTracking.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryTracking1">Unilaterally</label>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryTracking2' name='{1}_GenericSensoryTracking' value='2' type='checkbox' {0} />", genericSensoryTracking.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryTracking2">Bilaterally</label>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryTracking3' name='{1}_GenericSensoryTracking' value='3' type='checkbox' {0} />", genericSensoryTracking.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryTracking3">Smooth</label>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryTracking4' name='{1}_GenericSensoryTracking' value='4' type='checkbox' {0} />", genericSensoryTracking.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryTracking4">Jumpy</label>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryTracking5' name='{1}_GenericSensoryTracking' value='5' type='checkbox' {0} />", genericSensoryTracking.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryTracking5">Not Tracking</label>
                </div>
                <div class="clear"></div>
                <div>
                    <span class="float-left">Visual Field Cut or Neglect Suspected:</span>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryVisual1' name='{1}_GenericSensoryVisual' value='1' type='checkbox' {0} />", genericSensoryVisual.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryVisual1">Right</label>
                    <%= string.Format("<input class='radio' id='{1}_GenericSensoryVisual2' name='{1}_GenericSensoryVisual' value='2' type='checkbox' {0} />", genericSensoryVisual.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSensoryVisual1">Left</label>
                </div>
                <div class="clear"></div>
                <div>
                    <span class="float-left">Impacting Function?</span>
                    <%= Html.RadioButton(Model.Type + "_GenericSensoryImpactingFunction", "1", data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("1"), new { @id = Model.Type + "_GenericSensoryImpactingFunction1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericSensoryImpactingFunction1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSensoryImpactingFunction", "0", data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("0"), new { @id = Model.Type + "_GenericSensoryImpactingFunction0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericSensoryImpactingFunction0" class="inline-radio">No(Specify)</label>
                    <%= Html.TextBox(Model.Type + "_GenericSensoryImpactingFunctionSpecify", data.AnswerOrEmptyString("GenericSensoryImpactingFunctionSpecify"), new { @id = Model.Type + "_GenericSensoryImpactingFunctionSpecify" })%>
                </div>
                <div class="clear"></div>
                <div>
                    <span class="float-left">Referral Needed?</span>
                    <%= Html.RadioButton(Model.Type + "_GenericSensoryReferralNeeded", "1", data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("1"), new { @id = Model.Type + "_GenericSensoryReferralNeeded1", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericSensoryReferralNeeded1" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSensoryReferralNeeded", "0", data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("0"), new { @id = Model.Type + "_GenericSensoryReferralNeeded0", @class = "radio" })%>
                    <label for="<%= Model.Type %>_GenericSensoryReferralNeeded0" class="inline-radio">No</label>
                    Who contacted?
                    <%= Html.TextBox(Model.Type + "_GenericSensoryReferralNeededContact", data.AnswerOrEmptyString("GenericSensoryReferralNeededContact"), new { @id = Model.Type + "_GenericSensoryReferralNeededContact" })%>
                </div>
                
            </td>
        </tr>
   
</table>