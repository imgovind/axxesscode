﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <tbody>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GenericTransferTraining" class="float-left">
                    Transfer Training</label>
            </td>
            <td>
                x
                <%= Html.TextBox(Model.Type + "_GenericTransferTraining", data.AnswerOrEmptyString("GenericTransferTraining"), new { @id = Model.Type + "_GenericTransferTraining", @class = "sn" })%>
                reps
            </td>
        </tr>
        <tr>
            <td>
                <label class="float-left">
                    Assistive Device</label>
            </td>
            <td>
                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTrainingAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTrainingAssistiveDevice"), new { @id = Model.Type + "_GenericTransferTrainingAssistiveDevice", @class = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GenericBedChairAssist" class="float-left">
                    Bed &#8212; Chair</label>
            </td>
            <td>
                <%= Html.TherapyAssistance(Model.Type + "_GenericBedChairAssist", data.AnswerOrEmptyString("GenericBedChairAssist"), new { @id = Model.Type + "_GenericBedChairAssist", @class = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GenericChairToiletAssist" class="float-left">
                    Chair &#8212; Toilet</label>
            </td>
            <td>
                <%= Html.TherapyAssistance(Model.Type + "_GenericChairToiletAssist", data.AnswerOrEmptyString("GenericChairToiletAssist"), new { @id = Model.Type + "_GenericChairToiletAssist", @class = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="<%= Model.Type %>_GenericChairCarAssist" class="float-left">
                    Chair &#8212; Car</label>
            </td>
            <td>
                <%= Html.TherapyAssistance(Model.Type + "_GenericChairCarAssist", data.AnswerOrEmptyString("GenericChairCarAssist"), new { @id = Model.Type + "_GenericChairCarAssist", @class = "" })%>
            </td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td>
                <label for="" class="float-left">
                    Sitting Balance Activities</label>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericSittingStaticAssist">
                    Static:</label><%= Html.StaticBalance(Model.Type + "_GenericSittingStaticAssist", data.AnswerOrEmptyString("GenericSittingStaticAssist"), new { @id = Model.Type + "_GenericSittingStaticAssist", @class = "" })%>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericSittingDynamicAssist">
                    Dynamic:</label>
                <%= Html.DynamicBalance(Model.Type + "_GenericSittingDynamicAssist", data.AnswerOrEmptyString("GenericSittingDynamicAssist"), new { @id = Model.Type + "_GenericSittingDynamicAssist", @class = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label for="" class="float-left">
                    Standing Balance Activities</label>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericStandingStaticAssist">
                    Static:</label>
                <%= Html.StaticBalance(Model.Type + "_GenericStandingStaticAssist", data.AnswerOrEmptyString("GenericStandingStaticAssist"), new { @id = Model.Type + "_GenericStandingStaticAssist", @class = "" })%>
            </td>
            <td>
                <label for="<%= Model.Type %>_GenericStandingDynamicAssist">
                    Dynamic:</label>
                <%= Html.DynamicBalance(Model.Type + "_GenericStandingDynamicAssist", data.AnswerOrEmptyString("GenericStandingDynamicAssist"), new { @id = Model.Type + "_GenericStandingDynamicAssist", @class = "" })%>
            </td>
        </tr>
        <tr>
            <td colspan="3">Comments:
            <%= Html.Templates(Model.Type + "_TransferTrainingTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTransferTrainingComment" })%>
            </td>
        </tr>
        <tr>
            <td colspan="3"><%= Html.TextArea(Model.Type + "_GenericTransferTrainingComment", data.AnswerOrEmptyString("GenericTransferTrainingComment"), new { @id = Model.Type + "_GenericTransferTrainingComment", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>
