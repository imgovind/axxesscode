﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <thead class="strong">
        <tr>
            <th colspan="2"></th>
            <th colspan="2">ROM</th>
            <th colspan="2">Manual Muscle Test</th>
        </tr>
        <tr>
            <th class="align-left">Part</th>
            <th class="align-left">Action</th>
            <th>Right</th>
            <th>Left</th>
            <th>Right</th>
            <th>Left</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="align-left strong">Shoulder</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMRight", data.AnswerOrEmptyString("GenericShoulderFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMLeft", data.AnswerOrEmptyString("GenericShoulderFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthRight", data.AnswerOrEmptyString("GenericShoulderFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMRight", data.AnswerOrEmptyString("GenericShoulderExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMLeft", data.AnswerOrEmptyString("GenericShoulderExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthRight", data.AnswerOrEmptyString("GenericShoulderExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Abduction</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMRight", data.AnswerOrEmptyString("GenericShoulderAbductionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMLeft", data.AnswerOrEmptyString("GenericShoulderAbductionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthRight", data.AnswerOrEmptyString("GenericShoulderAbductionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderAbductionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Int Rot</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMRight", data.AnswerOrEmptyString("GenericShoulderIntRotROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMLeft", data.AnswerOrEmptyString("GenericShoulderIntRotROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthRight", data.AnswerOrEmptyString("GenericShoulderIntRotStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthLeft", data.AnswerOrEmptyString("GenericShoulderIntRotStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Ext Rot</td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMRight", data.AnswerOrEmptyString("GenericShoulderExtRotROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMLeft", data.AnswerOrEmptyString("GenericShoulderExtRotROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthRight", data.AnswerOrEmptyString("GenericShoulderExtRotStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthLeft", data.AnswerOrEmptyString("GenericShoulderExtRotStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthLeft" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Elbow</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMRight", data.AnswerOrEmptyString("GenericElbowFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMLeft", data.AnswerOrEmptyString("GenericElbowFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthRight", data.AnswerOrEmptyString("GenericElbowFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthLeft", data.AnswerOrEmptyString("GenericElbowFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMRight", data.AnswerOrEmptyString("GenericElbowExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMLeft", data.AnswerOrEmptyString("GenericElbowExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthRight", data.AnswerOrEmptyString("GenericElbowExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthLeft", data.AnswerOrEmptyString("GenericElbowExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Finger</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMRight", data.AnswerOrEmptyString("GenericFingerFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMLeft", data.AnswerOrEmptyString("GenericFingerFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthRight", data.AnswerOrEmptyString("GenericFingerFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthLeft", data.AnswerOrEmptyString("GenericFingerFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMRight", data.AnswerOrEmptyString("GenericFingerExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMLeft", data.AnswerOrEmptyString("GenericFingerExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthRight", data.AnswerOrEmptyString("GenericFingerExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthLeft", data.AnswerOrEmptyString("GenericFingerExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Wrist</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristFlexionROMRight", data.AnswerOrEmptyString("GenericWristFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristFlexionROMLeft", data.AnswerOrEmptyString("GenericWristFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthRight", data.AnswerOrEmptyString("GenericWristFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthLeft", data.AnswerOrEmptyString("GenericWristFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristExtensionROMRight", data.AnswerOrEmptyString("GenericWristExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristExtensionROMLeft", data.AnswerOrEmptyString("GenericWristExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthRight", data.AnswerOrEmptyString("GenericWristExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthLeft", data.AnswerOrEmptyString("GenericWristExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Trunk</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMRight", data.AnswerOrEmptyString("GenericTrunkFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMLeft", data.AnswerOrEmptyString("GenericTrunkFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthRight", data.AnswerOrEmptyString("GenericTrunkFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthLeft", data.AnswerOrEmptyString("GenericTrunkFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Rotation</td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMRight", data.AnswerOrEmptyString("GenericTrunkRotationROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMLeft", data.AnswerOrEmptyString("GenericTrunkRotationROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthRight", data.AnswerOrEmptyString("GenericTrunkRotationStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthLeft", data.AnswerOrEmptyString("GenericTrunkRotationStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMRight", data.AnswerOrEmptyString("GenericTrunkExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMLeft", data.AnswerOrEmptyString("GenericTrunkExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthRight", data.AnswerOrEmptyString("GenericTrunkExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthLeft", data.AnswerOrEmptyString("GenericTrunkExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td class="align-left strong">Neck</td>
            <td class="align-left">Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMRight", data.AnswerOrEmptyString("GenericNeckFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Extension</td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMRight", data.AnswerOrEmptyString("GenericNeckExtensionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMLeft", data.AnswerOrEmptyString("GenericNeckExtensionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthRight", data.AnswerOrEmptyString("GenericNeckExtensionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthLeft", data.AnswerOrEmptyString("GenericNeckExtensionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Lat Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMRight", data.AnswerOrEmptyString("GenericNeckLatFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckLatFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Long Flexion</td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMRight", data.AnswerOrEmptyString("GenericNeckLongFlexionROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckLongFlexionROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthLeft" })%></td>
        </tr>
        <tr>
            <td></td>
            <td class="align-left">Rotation</td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckRotationROMRight", data.AnswerOrEmptyString("GenericNeckRotationROMRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckRotationROMLeft", data.AnswerOrEmptyString("GenericNeckRotationROMLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMLeft" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthRight", data.AnswerOrEmptyString("GenericNeckRotationStrengthRight"), new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthRight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthLeft", data.AnswerOrEmptyString("GenericNeckRotationStrengthLeft"), new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthLeft" })%></td>
        </tr>
    </tbody>
</table>
<div>
    <label for="<%= Model.Type %>_GenericPhysicalAssessmentComment" class="strong">Comment</label>
    <%= Html.Templates(Model.Type + "_GenericPhysicalAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPhysicalAssessmentComment" })%>
    <%= Html.TextArea(Model.Type + "_GenericPhysicalAssessmentComment", data.ContainsKey("GenericPhysicalAssessmentComment") ? data["GenericPhysicalAssessmentComment"].Answer : string.Empty,6,20, new { @id = Model.Type + "_GenericPhysicalAssessmentComment", @class = "fill" })%>
</div>