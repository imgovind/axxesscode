﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Medical Diagnosis",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosis").Clean() %>",0,1) +
            printview.checkbox("Onset",<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisOnset").Equals("1").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("MedicalDiagnosisDate").Clean() %>",0,1) +
            printview.span("Therapy Diagnosis",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapyDiagnosis").Clean() %>",0,1) +
            printview.checkbox("Onset",<%= data.AnswerOrEmptyString("GenericTherapyDiagnosisOnset").Equals("1").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("TherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.col(8,
            printview.span("Precautions",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPrecautions").Clean() %>",0,1) +
            printview.span("Sensation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensation").Clean() %>",0,1) +
            printview.span("Muscle Tone",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMuscleTone").Clean() %>",0,1) +
            printview.span("ADL",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADL").Clean() %>",0,1) +
            printview.span("Endurance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericEndurance").Clean() %>",0,1) +
            printview.span("Edema",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericEdema").Clean() %>",0,1) +
            printview.span("Coordination",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericCoordination").Clean() %>",0,1)),
        "Diagnosis");
</script>