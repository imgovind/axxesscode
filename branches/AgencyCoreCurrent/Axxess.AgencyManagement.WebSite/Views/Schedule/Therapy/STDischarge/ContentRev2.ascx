﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericHomeboundReason = data.AnswerArray("GenericHomeboundReason"); %>
<%  string[] liquids = data.AnswerArray("GenericLiquids"); %>
<%  string[] genericReferralFor = data.AnswerArray("GenericReferralFor"); %>
<%  string[] genericPlanOfCare = data.AnswerArray("GenericPlanOfCare"); %>
<%  string[] genericDischargeDiscussedWith = data.AnswerArray("GenericDischargeDiscussedWith"); %>
<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
<table class="fixed nursing">
    <tbody>
        
        <tr>
            <th colspan="2">
                Patient's status upon discharge<br />
                <label style="font-size:xx-small;font-style:italic;">4 &#8211; WFL (Within Functional Limits) &#160; 3 &#8211; Mild Impairment &#160; 2 &#8211; Moderate Impairment &#160; 1 &#8211; Severe Impairment &#160; 0 &#8211; Unable to Assess/Did Not Test</label>
            </th>
            
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Cognition Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrientationScore" class="float-left">Orientation (Person/Place/Time)</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOrientationScore", data.AnswerOrEmptyString("GenericOrientationScore"), new { @class = "vitals", @id = Model.Type + "_GenericOrientationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAttentionSpanScore" class="float-left">Attention Span</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAttentionSpanScore", data.AnswerOrEmptyString("GenericAttentionSpanScore"), new { @class = "vitals", @id = Model.Type + "_GenericAttentionSpanScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericShortTermMemoryScore" class="float-left">Short Term Memory</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericShortTermMemoryScore", data.AnswerOrEmptyString("GenericShortTermMemoryScore"), new { @class = "vitals", @id = Model.Type + "_GenericShortTermMemoryScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericLongTermMemoryScore" class="float-left">Long Term Memory</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLongTermMemoryScore", data.AnswerOrEmptyString("GenericLongTermMemoryScore"), new { @class = "vitals", @id = Model.Type + "_GenericLongTermMemoryScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericJudgmentScore" class="float-left">Judgment</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericJudgmentScore", data.AnswerOrEmptyString("GenericJudgmentScore"), new { @class = "vitals", @id = Model.Type + "_GenericJudgmentScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProblemSolvingScore" class="float-left">Problem Solving</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericProblemSolvingScore", data.AnswerOrEmptyString("GenericProblemSolvingScore"), new { @class = "vitals", @id = Model.Type + "_GenericProblemSolvingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrganizationScore" class="float-left">Organization</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOrganizationScore", data.AnswerOrEmptyString("GenericOrganizationScore"), new { @class = "vitals", @id = Model.Type + "_GenericOrganizationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedOtherScore" class="float-left">Other:</label><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOther"), new { @class = "", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOther" })%>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericCognitionFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Speech/Voice Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralFacialExamScore" class="float-left">Oral/Facial Exam</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOralFacialExamScore", data.AnswerOrEmptyString("GenericOralFacialExamScore"), new { @class = "vitals", @id = Model.Type + "_GenericOralFacialExamScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericArticulationScore" class="float-left">Articulation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericArticulationScore", data.AnswerOrEmptyString("GenericArticulationScore"), new { @class = "vitals", @id = Model.Type + "_GenericArticulationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProsodyScore" class="float-left">Prosody</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericProsodyScore", data.AnswerOrEmptyString("GenericProsodyScore"), new { @class = "vitals", @id = Model.Type + "_GenericProsodyScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVoiceRespirationScore" class="float-left">Voice/Respiration</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVoiceRespirationScore", data.AnswerOrEmptyString("GenericVoiceRespirationScore"), new { @class = "vitals", @id = Model.Type + "_GenericVoiceRespirationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechIntelligibilityScore" class="float-left">Speech Intelligibility</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechIntelligibilityScore", data.AnswerOrEmptyString("GenericSpeechIntelligibilityScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechIntelligibilityScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedOtherScore" class="float-left">Other: <%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOther"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOther" })%></label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSpeechFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Auditory Comprehension Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWordDiscriminationScore" class="float-left">Word Discrimination</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWordDiscriminationScore", data.AnswerOrEmptyString("GenericWordDiscriminationScore"), new { @class = "vitals", @id = Model.Type + "_GenericWordDiscriminationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOneStepDirectionsScore" class="float-left">One Step Directions</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOneStepDirectionsScore", data.AnswerOrEmptyString("GenericOneStepDirectionsScore"), new { @class = "vitals", @id = Model.Type + "_GenericOneStepDirectionsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericTwoStepDirectionsScore" class="float-left">Two Step Directions</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTwoStepDirectionsScore", data.AnswerOrEmptyString("GenericTwoStepDirectionsScore"), new { @class = "vitals", @id = Model.Type + "_GenericTwoStepDirectionsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericComplexSentencesScore", data.AnswerOrEmptyString("GenericComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericConversationScore" class="float-left">Conversation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericConversationScore", data.AnswerOrEmptyString("GenericConversationScore"), new { @class = "vitals", @id = Model.Type + "_GenericConversationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechReadingScore" class="float-left">Speech Reading</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSpeechReadingScore", data.AnswerOrEmptyString("GenericSpeechReadingScore"), new { @class = "vitals", @id = Model.Type + "_GenericSpeechReadingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericACFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericACFEComment", data.AnswerOrEmptyString("GenericACFEComment"), new { @id = Model.Type + "_GenericACFEComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Swallowing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericChewingAbilityScore" class="float-left">Chewing Ability</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChewingAbilityScore", data.AnswerOrEmptyString("GenericChewingAbilityScore"), new { @class = "vitals", @id = Model.Type + "_GenericChewingAbilityScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralStageManagementScore" class="float-left">Oral Stage Management</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericOralStageManagementScore", data.AnswerOrEmptyString("GenericOralStageManagementScore"), new { @class = "vitals", @id = Model.Type + "_GenericOralStageManagementScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericPharyngealStageManagementScore" class="float-left">Pharyngeal Stage Management</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPharyngealStageManagementScore", data.AnswerOrEmptyString("GenericPharyngealStageManagementScore"), new { @class = "vitals", @id = Model.Type + "_GenericPharyngealStageManagementScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericReflexTimeScore" class="float-left">Reflex Time</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericReflexTimeScore", data.AnswerOrEmptyString("GenericReflexTimeScore"), new { @class = "vitals", @id = Model.Type + "_GenericReflexTimeScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedOtherScore" class="float-left">Other:</label><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOther"), new { @class = "", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOther" })%>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOtherScore"), new { @class = "vitals", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSwallowingFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align-center strong">Verbal Expression Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAugmentativeMethodsScore" class="float-left">Augmentative Methods</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAugmentativeMethodsScore", data.AnswerOrEmptyString("GenericAugmentativeMethodsScore"), new { @class = "vitals", @id = Model.Type + "_GenericAugmentativeMethodsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericNamingScore" class="float-left">Naming</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericNamingScore", data.AnswerOrEmptyString("GenericNamingScore"), new { @class = "vitals", @id = Model.Type + "_GenericNamingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAppropriateScore" class="float-left">Appropriate</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericAppropriateScore", data.AnswerOrEmptyString("GenericAppropriateScore"), new { @class = "vitals", @id = Model.Type + "_GenericAppropriateScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVEFEComplexSentencesScore", data.AnswerOrEmptyString("GenericVEFEComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericVEFEComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEConversationScore" class="float-left">Conversation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericVEFEConversationScore", data.AnswerOrEmptyString("GenericVEFEConversationScore"), new { @class = "vitals", @id = Model.Type + "_GenericVEFEConversationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericVEFEComment", data.AnswerOrEmptyString("GenericVEFEComment"), new { @id = Model.Type + "_GenericVEFEComment", @class = "fill" })%></div>
                </div>
            </td>
            <td>
                <div class="align-center strong">Reading Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFELettersNumbersScore" class="float-left">Letters/Numbers</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFELettersNumbersScore", data.AnswerOrEmptyString("GenericRFELettersNumbersScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFELettersNumbersScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEWordsScore" class="float-left">Words</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFEWordsScore", data.AnswerOrEmptyString("GenericRFEWordsScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFEWordsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFESimpleSentencesScore" class="float-left">Simple Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFESimpleSentencesScore", data.AnswerOrEmptyString("GenericRFESimpleSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFESimpleSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComplexSentencesScore" class="float-left">Complex Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericRFEComplexSentencesScore", data.AnswerOrEmptyString("GenericRFEComplexSentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericRFEComplexSentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericParagraphScore" class="float-left">Paragraph</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericParagraphScore", data.AnswerOrEmptyString("GenericParagraphScore"), new { @class = "vitals", @id = Model.Type + "_GenericParagraphScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericRFEComment", data.AnswerOrEmptyString("GenericRFEComment"), new { @id = Model.Type + "_GenericRFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="align-center strong">Writing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFELettersNumbersScore" class="float-left">Letters/Numbers</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFELettersNumbersScore", data.AnswerOrEmptyString("GenericWFELettersNumbersScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFELettersNumbersScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEWordsScore" class="float-left">Words</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFEWordsScore", data.AnswerOrEmptyString("GenericWFEWordsScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFEWordsScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESentencesScore" class="float-left">Sentences</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFESentencesScore", data.AnswerOrEmptyString("GenericWFESentencesScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFESentencesScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESpellingScore" class="float-left">Spelling</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericWFESpellingScore", data.AnswerOrEmptyString("GenericWFESpellingScore"), new { @class = "vitals", @id = Model.Type + "_GenericWFESpellingScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericFormulationScore" class="float-left">Formulation</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericFormulationScore", data.AnswerOrEmptyString("GenericFormulationScore"), new { @class = "vitals", @id = Model.Type + "_GenericFormulationScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSimpleAdditionSubtractionScore" class="float-left">Simple Addition/Subtraction</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericSimpleAdditionSubtractionScore", data.AnswerOrEmptyString("GenericSimpleAdditionSubtractionScore"), new { @class = "vitals", @id = Model.Type + "_GenericSimpleAdditionSubtractionScore" })%></div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericWFEComment", data.AnswerOrEmptyString("GenericWFEComment"), new { @id = Model.Type + "_GenericWFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>  
        <tr>
            <th>Reason for discharge</th>
            <th>Condition of patient upon discharge</th>
        </tr> 
        <tr>
            <td>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge2">Expired</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per pt/family request</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge4">No longer Homebound</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge5">Goals met</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge6">Prolonged on hold status</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge7">Moved out of service area</label></span>
                </div>
                <div class="halfOfTd"><span class="float-left">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge8' class='radio' name='{1}_GenericReasonForDischarge' value='8' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge8">Hospitalized</label></span>
                </div>
            </td>
            <td>
                <em>(Include VS, BS, Functional and Overall Status)</em>
                <div class="float-right"><label for="<%= Model.Type %>_ConditionOfDischargeTemplates">Templates:</label><%= Html.Templates(Model.Type + "_ConditionOfDischargeTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_ConditionOfDischarge" })%></div>
                <%= Html.TextArea(Model.Type + "_ConditionOfDischarge", data.AnswerOrEmptyString("ConditionOfDischarge"), 6, 20, new { @id = Model.Type + "_ConditionOfDischarge", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td>
                <label class="strong">Skilled treatment provided this visit</label>
                <%= Html.Templates(Model.Type + "_TreatmentProvidedTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_TreatmentProvided" })%>
                <%= Html.TextArea(Model.Type + "_TreatmentProvided", data.AnswerOrEmptyString("TreatmentProvided"), 6, 20, new { @id = Model.Type + "_TreatmentProvided", @class = "fill" })%>
            </td>
            <td>
                <label class="strong">Summary of treatment provided</label>
                <%= Html.Templates(Model.Type + "_SummaryTreatmentProvidedTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_SummaryTreatmentProvided" })%>
                <%= Html.TextArea(Model.Type + "_SummaryTreatmentProvided", data.AnswerOrEmptyString("SummaryTreatmentProvided"), 6, 20, new { @id = Model.Type + "_SummaryTreatmentProvided", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label class="strong">Summary of Progress Made</label>
                <%= Html.Templates(Model.Type + "_ProgressMadeTemplate", new { @class = "Templates", @template = "#" + Model.Type + "_ProgressMade" })%>
                <%= Html.TextArea(Model.Type + "_ProgressMade", data.AnswerOrEmptyString("ProgressMade"), 6, 20, new { @id = Model.Type + "_ProgressMade", @class = "fill" })%>
            </td>
        </tr>
    </tbody>
</table>