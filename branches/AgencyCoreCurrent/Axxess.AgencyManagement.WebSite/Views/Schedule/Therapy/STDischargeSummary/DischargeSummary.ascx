﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">ST Discharge Summary | <%= Model.Patient.DisplayName %></span>
<% var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<% using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "STDischargeSummaryForm" })) { %>
    <%= Html.Hidden("STDischargeSummary_PatientId", Model.PatientId)%>
    <%= Html.Hidden("STDischargeSummary_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("STDischargeSummary_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("DisciplineTask", "133")%>
    <%= Html.Hidden("Type", "STDischargeSummary")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    <%= string.Format("{0}", Model.TypeName) %>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</td>
                <td>
                <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
                <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="STDischargeSummary_EpsPeriod" class="float-left">Episode/Period:</label>
                        <div class="float-right"><%= Html.TextBox("STDischargeSummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "STDischargeSummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="STDischargeSummary_DateCompleted" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="STDischargeSummary_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="STDischargeSummary_VisitDate" /></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="STDischargeSummary_DischargeDate" class="float-left">Discharge Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="STDischargeSummary_DischargeDate" value="<%= data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() && data["DischargeDate"].Answer.IsValidDate() ? data["DischargeDate"].Answer : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="STDischargeSummary_DischargeDate" /></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
                <td colspan="2">
                    <div>
                        <label for="STDischargeSummary_Physician" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox("STDischargeSummary_PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : (data.ContainsKey("Physician") && data["Physician"].Answer.IsNotNullOrEmpty() && data["Physician"].Answer.IsGuid() ?data["Physician"].Answer:Guid.Empty.ToString() ), new { @id = "STDischargeSummary_PhysicianId", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label class="float-left">Notification of Discharge given to patient:</label>
                        <div class="float-right">
                            <%= Html.Hidden("STDischargeSummary_IsNotificationDC", " ", new { @id = "" })%>
                            <%= Html.RadioButton("STDischargeSummary_IsNotificationDC", "1", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? true : false, new { @id = "STDischargeSummary_IsNotificationDCY", @class = "radio" })%>
                            <label for="STDischargeSummary_IsNotificationDCY" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("STDischargeSummary_IsNotificationDC", "0", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "0" ? true : false, new { @id = "STDischargeSummary_IsNotificationDCN", @class = "radio" })%>
                            <label for="STDischargeSummary_IsNotificationDCN" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float-right">
                            <label for="STDischargeSummary_NotificationDate">If Yes:</label>
                            <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 day", Value = "1" },
                                    new SelectListItem { Text = "2 day", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                            <%= Html.DropDownList("STDischargeSummary_NotificationDate", patientReceived, new { @id = "STDischargeSummary_NotificationDate" }) %>
                            <%= Html.TextBox("STDischargeSummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "STDischargeSummary_NotificationDateOther", @style = "display:none;"}) %>
                        </div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="STDischargeSummary_ReasonForDC" class="float-left">Reason for Discharge:</label>
                        <%  var reasonForDC = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Goals Met", Value = "1" },
                                new SelectListItem { Text = "To Nursing Home", Value = "2" },
                                new SelectListItem { Text = "Deceased", Value = "3" },
                                new SelectListItem { Text = "Noncompliant", Value = "4" },
                                new SelectListItem { Text = "To Hospital", Value = "5" },
                                new SelectListItem { Text = "Moved from Service Area", Value = "6" },
                                new SelectListItem { Text = "Refused Care", Value = "7" },
                                new SelectListItem { Text = "No Longer Homebound", Value = "8" },
                                new SelectListItem { Text = "Other", Value = "9" }
                            }, "Value", "Text", data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : "0"); %>
                        <%= Html.DropDownList("STDischargeSummary_ReasonForDC", reasonForDC, new { @id = "STDischargeSummary_ReasonForDC", @class = "float-right" })%>
                        <%= Html.TextBox("STDischargeSummary_ReasonForDCOther",data.ContainsKey("ReasonForDCOther") ? data["ReasonForDCOther"].Answer : "", new { @id = "STDischargeSummary_ReasonForDCOther", @style = "display:none;", @class = "float-right" })%>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Patient Condition and Outcomes</th>
                <th colspan="2">Service(s) Provided</th>
            </tr>
            <tr>
                <td colspan="2">
                    <%  string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
                    <input name="STDischargeSummary_PatientCondition" value=" " type="hidden" />
                    <table class="fixed align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionStable' name='STDischargeSummary_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionStable" class="radio">Stable</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionImproved' name='STDischargeSummary_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionImproved" class="radio">Improved</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionUnchanged' name='STDischargeSummary_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionUnchanged" class="radio">Unchanged</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionUnstable' name='STDischargeSummary_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionUnstable" class="radio">Unstable</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionDeclined' name='STDischargeSummary_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionDeclined" class="radio">Declined</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionGoalsMet' name='STDischargeSummary_PatientCondition' value='6' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("6") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionGoalsMet" class="radio">Goals Met</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionGoalsPartiallyMet' name='STDischargeSummary_PatientCondition' value='7' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("7") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionGoalsPartiallyMet" class="radio">GoalsNot Met</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_PatientConditionGoalsNotMet' name='STDischargeSummary_PatientCondition' value='8' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("8") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_PatientConditionGoalsNotMet" class="radio">Goals Partially Met</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="2">
                    <%  string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
                    <input name="STDischargeSummary_ServiceProvided" value=" " type="hidden" />
                    <table class="fixed align-left">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedSN' name='STDischargeSummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedSN" class="radio">SN</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedPT' name='STDischargeSummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedPT" class="radio">PT</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedOT' name='STDischargeSummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedOT" class="radio">OT</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedST' name='STDischargeSummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedST" class="radio">ST</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedMSW' name='STDischargeSummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedMSW" class="radio">MSW</label>
                                </td>
                                <td>
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedHHA' name='STDischargeSummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedHHA" class="radio">HHA</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%= string.Format("<input id='STDischargeSummary_ServiceProvidedOther' name='STDischargeSummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "") %>
                                    <label for="STDischargeSummary_ServiceProvidedOther" class="radio">Other</label>
                                    <%= Html.TextBox("STDischargeSummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "", new { @id = "STDischargeSummary_ServiceProvidedOtherValue", @class = "oe" })%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="2">Care Summary</th>
                <th colspan="2">Condition of Discharge</th>
            </tr>
            <tr>
                <td colspan="2">
                    <em>(Care Given, Progress, Regress including Therapies)</em>
                    <div class="float-right">
                        <label for="STDischargeSummary_CareSummaryTemplates">Templates:</label>
                        <%= Html.Templates("STDischargeSummary_CareSummaryTemplates", new { @class = "Templates", @template = "#STDischargeSummary_CareSummary" })%>
                    </div>
                    <%= Html.TextArea("STDischargeSummary_CareSummary",data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : "", new { @class = "fill", @id = "STDischargeSummary_CareSummary", @style="height:150px" })%>
                </td>
                <td colspan="2">
                    <em>(Include VS, BS, Functional and Overall Status)</em>
                    <div class="float-right"><label for="STDischargeSummary_ConditionOfDischargeTemplates">Templates:</label><%= Html.Templates("STDischargeSummary_ConditionOfDischargeTemplates", new { @class = "Templates", @template = "#STDischargeSummary_ConditionOfDischarge" })%></div>
                    <%= Html.TextArea("STDischargeSummary_ConditionOfDischarge", data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer : "", new { @class = "fill", @id = "STDischargeSummary_ConditionOfDischarge", @style = "height:150px" })%>
                </td>
            </tr>
            <tr>
                <th colspan="4">Discharge Details</th>
            </tr>
            <tr class="align-left">
                <td colspan="2">
                    <label class="strong">Discharge Disposition: Where is the Patient after Discharge from your Agency?</label>
                    <%= Html.Hidden("STDischargeSummary_DischargeDisposition", " ", new { @id = "" })%>
                    <div>
                        <%= Html.RadioButton("STDischargeSummary_DischargeDisposition", "01", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? true : false, new { @id = "STDischargeSummary_DischargeDisposition1", @class = "radio float-left" })%>
                        <label for="STDischargeSummary_DischargeDisposition1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (without formal assistive services)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("STDischargeSummary_DischargeDisposition", "02", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? true : false, new { @id = "STDischargeSummary_DischargeDisposition2", @class = "radio float-left" })%>
                        <label for="STDischargeSummary_DischargeDisposition2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (with formal assistive services)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("STDischargeSummary_DischargeDisposition", "03", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? true : false, new { @id = "STDischargeSummary_DischargeDisposition3", @class = "radio float-left" })%>
                        <label for="STDischargeSummary_DischargeDisposition3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient transferred to a non-institutional hospice)</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("STDischargeSummary_DischargeDisposition", "04", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? true : false, new { @id = "STDischargeSummary_DischargeDisposition4", @class = "radio float-left" })%>
                        <label for="STDischargeSummary_DischargeDisposition4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span>
                        </label>
                    </div>
                    <div>
                        <%= Html.RadioButton("STDischargeSummary_DischargeDisposition", "UK", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? true : false, new { @id = "STDischargeSummary_DischargeDispositionUK", @class = "radio float-left" })%>
                        <label for="STDischargeSummary_DischargeDispositionUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Other unknown</span>
                        </label>
                    </div>
                </td>
                <td colspan="2">
                    <%  string[] dischargeInstructionsGivenTo = data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null; %>
                    <input name="STDischargeSummary_DischargeInstructionsGivenTo" value=" " type="hidden" />
                    <label class="float-left">Discharge Instructions Given To:</label>
                    <div class="float-left">
                        <div class="float-left">
                            <%= string.Format("<input id='STDischargeSummary_DischargeInstructionsGivenTo1' name='STDischargeSummary_DischargeInstructionsGivenTo' value='1' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "checked='checked'" : "")%>
                            <label for="STDischargeSummary_DischargeInstructionsGivenTo1" class="fixed radio">Patient</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='STDischargeSummary_DischargeInstructionsGivenTo2' name='STDischargeSummary_DischargeInstructionsGivenTo' value='2' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "checked='checked'" : "")%>
                            <label for="STDischargeSummary_DischargeInstructionsGivenTo2" class="fixed radio">Caregiver</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='STDischargeSummary_DischargeInstructionsGivenTo3' name='STDischargeSummary_DischargeInstructionsGivenTo' value='3' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "checked='checked'" : "")%>
                            <label for="STDischargeSummary_DischargeInstructionsGivenTo3" class="fixed radio">N/A</label>
                        </div>
                        <div class="float-left">
                            <%= string.Format("<input id='STDischargeSummary_DischargeInstructionsGivenTo4' name='STDischargeSummary_DischargeInstructionsGivenTo' value='4' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "checked='checked'" : "")%>
                            <label for="STDischargeSummary_DischargeInstructionsGivenTo4" class="radio">Other:</label>
                            <%= Html.TextBox("STDischargeSummary_DischargeInstructionsGivenToOther", data.ContainsKey("DischargeInstructionsGivenToOther") ? data["DischargeInstructionsGivenToOther"].Answer : "", new { @id = "STDischargeSummary_DischargeInstructionsGivenToOther", @class="oe" })%>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <label class="strong" for="STDischargeSummary_DischargeInstructions">Discharge Instructions:</label>
                    <%= Html.TextArea("STDischargeSummary_DischargeInstructions", data.AnswerOrEmptyString("DischargeInstructions"), new { @id = "STDischargeSummary_DischargeInstructions", @class="fill" }) %>
                    <div class="clear"></div>
                    <label class="float-left">Verbalized understanding:</label>
                    <div class="float-left">
                        <%= Html.Hidden("STDischargeSummary_IsVerbalizedUnderstanding", " ", new { @id = "" })%>
                        <%= Html.RadioButton("STDischargeSummary_IsVerbalizedUnderstanding", "1", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? true : false, new { @id = "STDischargeSummary_IsVerbalizedUnderstandingY", @class = "radio" })%>
                        <label for="STDischargeSummary_IsVerbalizedUnderstandingY" class="inline-radio">Yes</label>
                        <%= Html.RadioButton("STDischargeSummary_IsVerbalizedUnderstanding", "0", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? true : false, new { @id = "STDischargeSummary_IsVerbalizedUnderstandingN", @class = "radio" })%>
                        <label for="STDischargeSummary_IsVerbalizedUnderstandingN" class="inline-radio">No</label>
                    </div>
                    <div class="clear"></div>
                    <%  string[] differentTasks = data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null; %>
                    <input name="STDischargeSummary_DifferentTasks" value=" " type="hidden" />
                    <div>
                        <%= string.Format("<input id='STDischargeSummary_DifferentTasks1' name='STDischargeSummary_DifferentTasks' value='1' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("1") ? "checked='checked'" : "")%>
                        <label for="STDischargeSummary_DifferentTasks1" style="margin-left:20px">All services notified and discontinued</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='STDischargeSummary_DifferentTasks2' name='STDischargeSummary_DifferentTasks' value='2' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("2") ? "checked='checked'" : "")%>
                        <label for="STDischargeSummary_DifferentTasks2" style="margin-left:20px">Order and summary completed</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='STDischargeSummary_DifferentTasks3' name='STDischargeSummary_DifferentTasks' value='3' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("3") ? "checked='checked'" : "")%>
                        <label for="STDischargeSummary_DifferentTasks3" style="margin-left:20px">Information provided to patient for continuing needs</label>
                    </div>
                    <div>
                        <%= string.Format("<input id='STDischargeSummary_DifferentTasks4' name='STDischargeSummary_DifferentTasks' value='4' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("4") ? "checked='checked'" : "")%>
                        <label for="STDischargeSummary_DifferentTasks4" style="margin-left:20px">Physician notified</label>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="STDischargeSummary_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("STDischargeSummary_Clinician", "", new { @id = "STDischargeSummary_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="STDischargeSummary_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="STDischargeSummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : "" %>" id="STDischargeSummary_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div>
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        Return to Clinician for Signature
                    </div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="STDischargeSummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="STDischargeSummaryRemove(); STDischargeSummary.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="STDischargeSummaryAdd(); STDischargeSummary.Submit($(this));">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="STDischargeSummaryRemove(); STDischargeSummary.Submit($(this));">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="STDischargeSummaryRemove(); STDischargeSummary.Submit($(this));">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="STDischargeSummaryRemove(); UserInterface.CloseWindow('STDischargeSummary');">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#STDischargeSummary_MR").attr('readonly', true);
    $("#STDischargeSummary_EpsPeriod").attr('readonly', true);
    function STDischargeSummaryAdd() {
        $("#STDischargeSummary_Clinician").removeClass('required').addClass('required');
        $("#STDischargeSummary_SignatureDate").removeClass('required').addClass('required');
    }
    function STDischargeSummaryRemove() {
        $("#STDischargeSummary_Clinician").removeClass('required');
        $("#STDischargeSummary_SignatureDate").removeClass('required');
    }
</script>