﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.TransferSummary.ToString(), "Transfer Summary" },
    { DisciplineTasks.CoordinationOfCare.ToString(), "Coordination of Care" }
     }; %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %> <%= dictonary.ContainsKey(Model.Type) ? dictonary[Model.Type] : string.Empty %><%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= dictonary.ContainsKey(Model.Type) ? dictonary[Model.Type] : ""%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>' +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3ECompletion Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("CompletedDate") && data["CompletedDate"].Answer.IsNotNullOrEmpty() ? data["CompletedDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.StartDate.IsValid() && Model.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null ? Model.PhysicianDisplayName.Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDate Of Transfer:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TransferDate") ? data["TransferDate"].Answer.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician Phone:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PhysicianPhone") ? data["PhysicianPhone"].Answer.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEmergency Contact:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EmergencyContact") ? data["EmergencyContact"].Answer.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EReport Recipient:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("ReceivingName") ? data["ReceivingName"].Answer.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EReceiving Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("ReceivingDate") ? data["ReceivingDate"].Answer.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer.Clean() : string.Empty %>" +
        
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3ECoordination of Care/%3Cbr /%3ETransfer Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.col(3,
            printview.checkbox("Amputation",<%= functionLimitations != null && functionLimitations.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Legally Blind",<%= functionLimitations != null && functionLimitations.Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Endurance",<%= functionLimitations != null && functionLimitations.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Contracture",<%= functionLimitations != null && functionLimitations.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Hearing",<%= functionLimitations != null && functionLimitations.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Paralysis",<%= functionLimitations != null && functionLimitations.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations != null && functionLimitations.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Dyspnea with Minimal Exertion",<%= functionLimitations != null && functionLimitations.Contains("A") ? "true" : "false"%>) +
            printview.checkbox("Ambulation",<%= functionLimitations != null && functionLimitations.Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Speech",<%= functionLimitations != null && functionLimitations.Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Other",<%= functionLimitations != null && functionLimitations.Contains("B") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer.Clean() : string.Empty %>",0,1) ),
        "Functional Limitations");
    printview.addsection(
        printview.col(5,
            printview.checkbox("Stable",<%= patientCondition != null && patientCondition.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Improved",<%= patientCondition != null && patientCondition.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Unchanged",<%= patientCondition != null && patientCondition.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Unstable",<%= patientCondition != null && patientCondition.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Declined",<%= patientCondition != null && patientCondition.Contains("5") ? "true" : "false"%>) ),
        "Patient Condition");
    printview.addsection(
        printview.col(3,
            printview.checkbox("SN",<%= serviceProvided != null && serviceProvided.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("PT",<%= serviceProvided != null && serviceProvided.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("OT",<%= serviceProvided != null && serviceProvided.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("ST",<%= serviceProvided != null && serviceProvided.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("MSW",<%= serviceProvided != null && serviceProvided.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("HHA",<%= serviceProvided != null && serviceProvided.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Other",<%= serviceProvided != null && serviceProvided.Contains("7") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer.Clean() : string.Empty %>",0,1) ),
        "Service(s) Provided");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3EBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3Cth%3EBG%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3ELowest%3C/th%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer.Clean() : string.Empty %>" + 
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3EHighest%3C/th%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3Ctd class=%22dual%22%3E" +
        "<%= data != null && data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer.Clean() : string.Empty %>" +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
        "Vital Sign Ranges");
        printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/HomeBoundStatus.ascx", Model); %>);
    
    printview.addsection(
        printview.col(6,
            printview.span("Facility:",true) +
            printview.span("<%= data != null && data.ContainsKey("Facility") ? data["Facility"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Phone:",true) +
            printview.span("<%= data != null && data.ContainsKey("Phone1") && data.ContainsKey("Phone2") && data.ContainsKey("Phone3") ? data["Phone1"].Answer.Clean() + '-' + data["Phone2"].Answer.Clean() + '-' + data["Phone3"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Contact:",true) +
            printview.span("<%= data != null && data.ContainsKey("Contact") ? data["Contact"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.span("Services Providing:",true) +
        printview.span("<%= data != null && data.ContainsKey("ServicesProviding") && data["ServicesProviding"].Answer.IsNotNullOrEmpty() ? data["ServicesProviding"].Answer.Clean() : string.Empty %>",0,10),
        "Transfer Facility Information");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer.Clean() : string.Empty %>",0,10),"Summary of Care Provided by HHA");
        printview.addsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/MedicareReview.ascx", Model); %>);
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("SignificantHealthHistory") ? data["SignificantHealthHistory"].Answer.Clean() : string.Empty%>",0,10),"Significant Health History");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("TransferOrderInstructions") ? data["TransferOrderInstructions"].Answer.Clean() : string.Empty%>",0,10),"Transfer Orders And Instructions");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("ServicesProvided") ? data["ServicesProvided"].Answer.Clean() : string.Empty%>",0,10),"Description Of Services Provided And Ongoing Needs That Cannot Be Met");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("Narrative") ? data["Narrative"].Answer.Clean() : string.Empty%>",0,10),"Narrative");
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>
