﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Appetite:</label>
<div class="float-right">
    <%  var nutritionStatus = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Decreased", Value = "Decreased" },
        }, "Value", "Text", data.AnswerOrDefault("GenericNutritionStatus1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericNutritionStatus1", nutritionStatus, new { @id = Model.Type + "_GenericNutritionStatus1", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Fluid Intake:</label>
<div class="float-right">
    <%  var nutritionStatus2 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Decreased", Value = "Decreased" },
        }, "Value", "Text", data.AnswerOrDefault("GenericNutritionStatus2", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericNutritionStatus2", nutritionStatus2, new { @id = Model.Type + "_GenericNutritionStatus2", @class = "oe" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericNutritionStatusComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericNutritionStatusComment", data.AnswerOrEmptyString("GenericNutritionStatusComment"), new { @id = Model.Type + "_GenericNutritionStatusComment", @class = "fill" })%></div>

