﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Temp:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericTemp").Clean() %> <%= data.AnswerOrEmptyString("GenericTempType").Clean() %>") +
    printview.span("Resp:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericResp").Clean() %> <%= data.AnswerOrEmptyString("GenericRespType").Clean() %>") +
    printview.span("Apical Pulse:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericPulseApical").Clean() %> <%= data.AnswerOrEmptyString("GenericPulseApicalRegular").Clean() %>") +
    printview.span("Radial Pulse:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericPulseRadial").Clean() %> <%= data.AnswerOrEmptyString("GenericPulseRadialRegular").Clean() %>")) +
printview.col(4,
    printview.span("BP",true) +
    printview.span("Lying",true) +
    printview.span("Sitting",true) +
    printview.span("Standing",true) +
    printview.span("Left:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftLying").Clean() %>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftSitting").Clean() %>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftStanding").Clean() %>") +
    printview.span("Right:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPRightLying").Clean() %>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPRightSitting").Clean() %>") +
    printview.span("<%= data.AnswerOrEmptyString("GenericBPRightStanding").Clean() %>")) +
printview.col(2,
    printview.span("Weight:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericWeight").Clean() %> <%= data.AnswerOrEmptyString("GenericWeightUnit").Clean() %>") +
    printview.span("Pulse Oximetry:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericPulseOximetry").Clean() %> <%= data.AnswerOrEmptyString("GenericPulseOximetryUnit").Clean() %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericVitlaSignComment").Clean() %>"),
"Vital Signs"