﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Overall ADL Status:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericADL1","____________").ToString()%>")) +
printview.col(2,
    printview.span("Dressing:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericADL2").ToString()%>")) +
printview.col(2,
    printview.span("Motivation:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericADL3").ToString()%>")) +
printview.col(2,
    printview.span("Personal Hygiene:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericADL4").ToString()%>")) +
printview.col(2,
    printview.span("Sleeping Habits:",true) + 
    printview.span("<%= data.AnswerOrEmptyString("GenericADL5").ToString()%>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericADLComment").Clean()%>"),
   "ADL Level"
