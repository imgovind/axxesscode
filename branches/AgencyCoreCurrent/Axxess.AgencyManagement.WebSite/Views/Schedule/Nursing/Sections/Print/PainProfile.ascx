﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Pain Intensity:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericIntensityOfPain").Clean() %>") +
    printview.span("Description:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericQualityOfPain").Clean() %>") +
    printview.span("Duration:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDurationOfPain").Clean() %>") +
    printview.span("Primary Site:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericLocationOfPain").Clean() %>") +
    printview.span("Freq. of Interfering Pain",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericFrequencyOfPain").Clean() %>") +
    printview.span("Pain Mngmt. Effectiveness",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericMedicationEffectiveness").Clean() %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPainProfileComment").Clean() %>"),
"Pain Profile"