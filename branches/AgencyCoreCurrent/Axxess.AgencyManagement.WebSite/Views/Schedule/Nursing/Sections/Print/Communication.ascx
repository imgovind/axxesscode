﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Overall Communication Status:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericCommunication1", "____________").Clean()%>")) +
printview.col(2,
    printview.span("Socialization:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericCommunicationSocialization", "____________").Clean()%>")) +
printview.col(2,
    printview.span("Somatization:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericCommunicationSomatization", "____________").Clean()%>")) +
printview.col(2,
    printview.span("Ventilates Feelings:",true) + 
    printview.span("<%= data.AnswerOrDefault("GenericCommunicationVentilatesFeelings", "____________").Clean()%>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericCommunicationComment").Clean()%>"),
   "Communication"
