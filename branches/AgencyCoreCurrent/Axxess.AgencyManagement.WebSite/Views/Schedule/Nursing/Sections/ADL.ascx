﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label class="float-left">Overall ADL Status:</label>
<div class="float-right">
    <%  var adl = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericADL1", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericADL1", adl, new { @id = Model.Type + "_GenericADL1", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Dressing:</label>
<div class="float-right">
    <%  var adl2 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericADL2", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericADL2", adl2, new { @id = Model.Type + "_GenericADL2", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Motivation:</label>
<div class="float-right">
    <%  var adl3 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericADL3", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericADL3", adl3, new { @id = Model.Type + "_GenericADL3", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Personal Hygiene:</label>
<div class="float-right">
    <%  var adl4 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Regressed", Value = "Regressed" },
        }, "Value", "Text", data.AnswerOrDefault("GenericADL4", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericADL4", adl4, new { @id = Model.Type + "_GenericADL4", @class = "oe" })%>
</div>
<div class="clear"></div>
<label class="float-left">Sleeping Habits:</label>
<div class="float-right">
    <%  var adl5 = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Improved", Value = "Improved" },
            new SelectListItem { Text = "Same", Value = "Same" },
            new SelectListItem { Text = "Insomnia", Value = "Insomnia" },
        }, "Value", "Text", data.AnswerOrDefault("GenericADL5", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericADL5", adl5, new { @id = Model.Type + "_GenericADL5", @class = "oe" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericADLComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericADLComment", data.AnswerOrEmptyString("GenericADLComment"), new { @id = Model.Type + "_GenericADLComment", @class = "fill" })%></div>
