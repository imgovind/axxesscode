﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Skilled Nurse Pediatric Assessment Note<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Skilled Nurse Pediatric Assessment Note" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.Patient.LastName.Clean()%>, <%= Model.Patient.FirstName.Clean()%> <%= Model.Patient.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.Patient.DOBFormatted.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.Patient.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.StartDate.ToShortDateString().ToZeroFilled(), Model.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        
        printview.addsubsection(
            printview.span("Newborn screen results<%=data.AnswerOrEmptyString("NewbornScreenResults").Clean()%>")+
            printview.span("Gestational age at birth <%=data.AnswerOrEmptyString("GestationalAge").Clean()%> weeks")+
            printview.span("Birth wt.<%=data.AnswerOrEmptyString("BirthWeight").Clean() %> lb.<%=data.AnswerOrEmptyString("BirthOz").Clean() %> oz.")+
            printview.span("Length <%=data.AnswerOrEmptyString("BirthLength").Clean() %> in.")+
            printview.span("Head circumference <%=data.AnswerOrEmptyString("HeadCircumference").Clean() %> in")+
            printview.col(3,
                printview.span("Fontanels:",true)+
                printview.checkbox("Anterior",<%= data.AnswerOrEmptyString("Fontanels").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Posterior",<%= data.AnswerOrEmptyString("Fontanels").Contains("1").ToString().ToLower()%>)) +
            printview.col(5,
                printview.span("Umbilicus:",true)+
                printview.checkbox("Healed",<%= data.AnswerOrEmptyString("Umbilicus").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Hernia",<%= data.AnswerOrEmptyString("Umbilicus").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Inverted",<%= data.AnswerOrEmptyString("Umbilicus").Contains("2").ToString().ToLower()%>) +
                printview.checkbox("Everted",<%= data.AnswerOrEmptyString("Umbilicus").Contains("3").ToString().ToLower()%>)),
            "NewbornInfant",2);
        printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model); %>);
        printview.addsection(
            printview.col(8,
                printview.span("Thrush",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHThrush").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHThrush").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHThrush").Contains("2").ToString().ToLower()%>)) +
                printview.span("Apnea",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHApnea").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHApnea").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHApnea").Contains("2").ToString().ToLower()%>)) +
                printview.span("Conjunctivitis",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHConjunctivitis").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHConjunctivitis").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHConjunctivitis").Contains("2").ToString().ToLower()%>)) +
                printview.span("Croup",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHCroup").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHCroup").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHCroup").Contains("2").ToString().ToLower()%>)) +
                printview.span("Pica",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHPica").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHPica").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHPica").Contains("2").ToString().ToLower()%>)) +
                printview.span("Rubella",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHRubella").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHRubella").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHRubella").Contains("2").ToString().ToLower()%>)) +
                printview.span("Rubeola",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHRubeola").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHRubeola").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHRubeola").Contains("2").ToString().ToLower()%>)) +
                printview.span("Scarlet Fever",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHScarlet").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHScarlet").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHScarlet").Contains("2").ToString().ToLower()%>)) +
                printview.span("Mumps",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHMumps").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHMumps").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHMumps").Contains("2").ToString().ToLower()%>)) +
                printview.span("Chickenpox",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHChickenpox").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHChickenpox").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHChickenpox").Contains("2").ToString().ToLower()%>)) +
                printview.span("Hepatitis",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHHepatitis").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHHepatitis").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHHepatitis").Contains("2").ToString().ToLower()%>)) +
                printview.span("Sickle Cell",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHSickleCell").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHSickleCell").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHSickleCell").Contains("2").ToString().ToLower()%>)) +
                printview.span("Lead Poisoning",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHLeadPoisoning").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHLeadPoisoning").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHLeadPoisoning").Contains("2").ToString().ToLower()%>)) +
                printview.span("HIV",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHHIV").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHHIV").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHHIV").Contains("2").ToString().ToLower()%>)) +
                printview.span("Pneumonia",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHPneumonia").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHPneumonia").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHPneumonia").Contains("2").ToString().ToLower()%>)) +
                printview.span("Asthma",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHAsthma").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHAsthma").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHAsthma").Contains("2").ToString().ToLower()%>))+
                printview.span("Strep throat",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHStrep").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHStrep").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHStrep").Contains("2").ToString().ToLower()%>)) +
                printview.span("Sinusitis",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHSinusitis").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHSinusitis").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHSinusitis").Contains("2").ToString().ToLower()%>)) +
                printview.span("Nosebleeds",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHNosebleeds").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHNosebleeds").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHNosebleeds").Contains("2").ToString().ToLower()%>)) +
                printview.span("Fracture(s)",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHFractures").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHFractures").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHFractures").Contains("2").ToString().ToLower()%>)) +
                printview.span("Burn(s)",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHBurns").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHBurns").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHBurns").Contains("2").ToString().ToLower()%>)) +
                printview.span("Otitis media",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHOtitismedia").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHOtitismedia").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHOtitismedia").Contains("2").ToString().ToLower()%>)) +
                printview.span("Frequent ear infection",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHFrequentEar").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHFrequentEar").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHFrequentEar").Contains("2").ToString().ToLower()%>)) +
                printview.span("Tonsillitis",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHTonsillitis").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHTonsillitis").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHTonsillitis").Contains("2").ToString().ToLower()%>)) +
                printview.span("Frequent sore throat",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHFrequentSore").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHFrequentSore").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHFrequentSore").Contains("2").ToString().ToLower()%>)) +
                printview.span("Bleeding problems",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHBleedingProblems").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHBleedingProblems").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHBleedingProblems").Contains("2").ToString().ToLower()%>)) +
                printview.span("Rheumatic fever",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHRheumaticFever").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHRheumaticFever").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHRheumaticFever").Contains("2").ToString().ToLower()%>)) +
                printview.span("Headaches",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHHeadaches").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHHeadaches").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHHeadaches").Contains("2").ToString().ToLower()%>)) +
                printview.span("Seizures-grand mal",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHSeizuresGrand").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHSeizuresGrand").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHSeizuresGrand").Contains("2").ToString().ToLower()%>)) +
                printview.span("Seizures-petit mal",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHSeizuresPetit").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHSeizuresPetit").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHSeizuresPetit").Contains("2").ToString().ToLower()%>)) +
                printview.span("FrequentColds",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHFrequentColds").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHFrequentColds").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHFrequentColds").Contains("2").ToString().ToLower()%>)) +
                printview.span("Other:<%=data.AnswerOrEmptyString("ChildhoodHistoryOther").Clean()%>",true)+
                printview.col(3,
                printview.checkbox("H",<%= data.AnswerOrEmptyString("CHOther").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("N",<%= data.AnswerOrEmptyString("CHOther").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("P",<%= data.AnswerOrEmptyString("CHOther").Contains("2").ToString().ToLower()%>))),
            "Childhood History(H-history of; N-negative; P-present problem)");
        printview.addsubsection(
                printview.span("<%=Model.Allergies %>"),
                "Allergies",3);
        printview.addsubsection(
            printview.col(3,
                printview.checkbox("DPT",<%= data.AnswerOrEmptyString("Immunizations").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Measles",<%= data.AnswerOrEmptyString("Immunizations").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Polio",<%= data.AnswerOrEmptyString("Immunizations").Contains("2").ToString().ToLower()%>) +
                printview.checkbox("DT",<%= data.AnswerOrEmptyString("Immunizations").Contains("3").ToString().ToLower()%>) +
                printview.checkbox("Mumps",<%= data.AnswerOrEmptyString("Immunizations").Contains("4").ToString().ToLower()%>) +
                printview.checkbox("HBV",<%= data.AnswerOrEmptyString("Immunizations").Contains("5").ToString().ToLower()%>) +
                printview.checkbox("MMR",<%= data.AnswerOrEmptyString("Immunizations").Contains("6").ToString().ToLower()%>) +
                printview.checkbox("Rubella",<%= data.AnswerOrEmptyString("Immunizations").Contains("7").ToString().ToLower()%>) +
                printview.checkbox("Hib",<%= data.AnswerOrEmptyString("Immunizations").Contains("8").ToString().ToLower()%>))+
           printview.col(2,
                printview.checkbox("Other",<%= data.AnswerOrEmptyString("Immunizations").Contains("9").ToString().ToLower()%>)+ 
                printview.span("<%= data.AnswerOrEmptyString("Immunizations").Contains("9")?":"+data.AnswerOrEmptyString("ImmunizationsOther").Clean():""%>")),
           "Immunizations");
        printview.addsubsection(
            printview.span("TB skin test:",true)+
            printview.col(2,
                printview.checkbox("No",<%= data.AnswerOrEmptyString("SkinTest").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Yes<%= data.AnswerOrEmptyString("SkinTest").Contains("1")?":"+data.AnswerOrEmptyString("SkinTestYes"):string.Empty%>",<%= data.AnswerOrEmptyString("SkinTest").Contains("1").ToString().ToLower()%>)) +
                
            printview.span("Lead screening:",true)+
            printview.col(2,
                printview.checkbox("No",<%= data.AnswerOrEmptyString("LeadScreening").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Yes<%= data.AnswerOrEmptyString("LeadScreening").Contains("1") ? ":" + data.AnswerOrEmptyString("LeadScreeningYes") : string.Empty%>",<%= data.AnswerOrEmptyString("LeadScreening").Contains("1").ToString().ToLower()%>))+
            printview.col(2,
                printview.span("Other:",true)+
                printview.span("<%=data.AnswerOrEmptyString("ScreeningOther")%>")),
            "Screening/Early detection");
        <%if (data.AnswerOrEmptyString("IsEyesApply").Equals("1"))
          { %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Eyes",2);
        <%}
          else
          { %>
        printview.addsubsection(
        printview.col(2,
            printview.checkbox("Glasses",<%= data.AnswerOrEmptyString("Eyes").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Contacts R",<%= data.AnswerOrEmptyString("Eyes").Contains("CR").ToString().ToLower()%>) +
            printview.checkbox("Contacts L",<%= data.AnswerOrEmptyString("Eyes").Contains("CL").ToString().ToLower()%>) +
            printview.checkbox("Prosthesis R",<%= data.AnswerOrEmptyString("Eyes").Contains("PR").ToString().ToLower()%>) +
            printview.checkbox("Prosthesis L",<%= data.AnswerOrEmptyString("Eyes").Contains("PL").ToString().ToLower()%>) +
            printview.checkbox("Jaundice",<%= data.AnswerOrEmptyString("Eyes").Contains("3").ToString().ToLower()%>) +
            printview.checkbox("Blurred vision",<%= data.AnswerOrEmptyString("Eyes").Contains("4").ToString().ToLower()%>) +
            printview.checkbox("Legally blind",<%= data.AnswerOrEmptyString("Eyes").Contains("5").ToString().ToLower()%>) +
            printview.checkbox("Infections<%= data.AnswerOrEmptyString("Eyes").Contains("6") ? ":" + data.AnswerOrEmptyString("EyesInfections").Clean() : string.Empty%>",<%= data.AnswerOrEmptyString("Eyes").Contains("6").ToString().ToLower()%>) +
            printview.checkbox("Other:<%= data.AnswerOrEmptyString("Eyes").Contains("7") ? ":" + data.AnswerOrEmptyString("EyesOther").Clean() : string.Empty%>",<%= data.AnswerOrEmptyString("Eyes").Contains("7").ToString().ToLower()%>)),
            "Eyes",2);
            <%} %>
        
        <%if (data.AnswerOrEmptyString("IsNoseApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Nose/Throat/Mouth");
        <%}else { %>
        printview.addsubsection(
            printview.col(4,
                printview.checkbox("Congestio",<%= data.AnswerOrEmptyString("Nose").Contains("0").ToString().ToLower()%>) + 
                printview.checkbox("Dysphagla",<%= data.AnswerOrEmptyString("Nose").Contains("1").ToString().ToLower()%>) +  
                printview.checkbox("Hoarseness",<%= data.AnswerOrEmptyString("Nose").Contains("2").ToString().ToLower()%>) +  
                printview.checkbox("Lesions",<%= data.AnswerOrEmptyString("Nose").Contains("3").ToString().ToLower()%>) +   
                printview.checkbox("Sore throat",<%= data.AnswerOrEmptyString("Nose").Contains("4").ToString().ToLower()%>) +   
                printview.checkbox("Masses/Tumors",<%= data.AnswerOrEmptyString("Nose").Contains("5").ToString().ToLower()%>) +   
                printview.checkbox("Palate intact",<%= data.AnswerOrEmptyString("Nose").Contains("6").ToString().ToLower()%>) +  
                printview.span(""))+
            printview.col(3,
                printview.span("Teeth present",true)+
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("Teeth").Contains("0").ToString().ToLower()%>) +   
                printview.checkbox("No",<%= data.AnswerOrEmptyString("Teeth").Contains("1").ToString().ToLower()%>)) + 
            printview.col(2,
                printview.span("Oral hygiene practices",true)+
                printview.span("<%=data.AnswerOrEmptyString("OralHygiene").Clean()%>")+
                printview.span("Dentist visits:frequency",true)+   
                printview.span("<%=data.AnswerOrEmptyString("DentistVisit").Clean()%>")+
                printview.span("Other",true)+
                printview.span("<%=data.AnswerOrEmptyString("NoseOther").Clean()%>")),
            "Nose/Throat/Mouth");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsEarsApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Ears");
        <%}else { %>
        printview.addsubsection(
            printview.col(4,
                printview.checkbox("HOH R",<%= data.AnswerOrEmptyString("Ear").Contains("HOHR").ToString().ToLower()%>) +   
                printview.checkbox("HOH L",<%= data.AnswerOrEmptyString("Ear").Contains("HOHL").ToString().ToLower()%>) +
                printview.checkbox("Deaf R",<%= data.AnswerOrEmptyString("Ear").Contains("DeafR").ToString().ToLower()%>) +   
                printview.checkbox("Deaf L",<%= data.AnswerOrEmptyString("Ear").Contains("DeafL").ToString().ToLower()%>) +
                printview.checkbox("Hearing aid R",<%= data.AnswerOrEmptyString("Ear").Contains("AidR").ToString().ToLower()%>) +   
                printview.checkbox("Hearing aid L",<%= data.AnswerOrEmptyString("Ear").Contains("AidL").ToString().ToLower()%>) +
                printview.checkbox("Vertigo",<%= data.AnswerOrEmptyString("Ear").Contains("3").ToString().ToLower()%>) +   
                printview.checkbox("Tinnitus",<%= data.AnswerOrEmptyString("Ear").Contains("4").ToString().ToLower()%>))+
            printview.span("Infections:",true)+
            printview.col(2,
                <%if (data.AnswerOrEmptyString("EarInfections").Contains("0"))
                  { %>
                  printview.col(2,
                    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("EarInfections").Contains("0").ToString().ToLower()%>)+ 
                    printview.span("Frequency:<%=data.AnswerOrEmptyString("EarInfectionsYes").Clean()%>"))+
                <%}
                  else
                  { %>
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("EarInfections").Contains("0").ToString().ToLower()%>)+ 
                <%} %>
                printview.checkbox("No",<%= data.AnswerOrEmptyString("EarInfections").Contains("1").ToString().ToLower()%>))+ 
                
                printview.col(2,
                    printview.checkbox("P.E. tubes present",<%= data.AnswerOrEmptyString("Ear").Contains("5").ToString().ToLower()%>) +  
                    printview.checkbox("Other<%=data.AnswerOrEmptyString("Ear").Contains("6")?":"+data.AnswerOrEmptyString("EarOther").Clean():string.Empty%>",<%= data.AnswerOrEmptyString("Ear").Contains("6").ToString().ToLower()%>)),   
                   
                "Ears",2); 
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsHeadApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Head/Neck");
        <%}else { %>
        printview.addsubsection(
        <%if (data.AnswerOrEmptyString("Head").Contains("0"))
          { %>
                printview.checkbox("Injuries/Wounds:",<%= data.AnswerOrEmptyString("Head").Contains("0").ToString().ToLower()%>) + 
                printview.span("<%=data.AnswerOrEmptyString("HeadInjuries").Clean()%>")+
        <%}
          else
          { %>
            printview.checkbox("Injuries/Wounds:",<%= data.AnswerOrEmptyString("Head").Contains("0").ToString().ToLower()%>) + 
        <%} %>
        <%if (data.AnswerOrEmptyString("Head").Contains("1"))
          { %>
            printview.checkbox("Masses/Nodes:",<%= data.AnswerOrEmptyString("Head").Contains("0").ToString().ToLower()%>) + 
            printview.col(2,
                printview.span("Site:<%=data.AnswerOrEmptyString("HeadMassesSite").Clean()%>")+
                printview.span("Size:<%=data.AnswerOrEmptyString("HeadMassesSize").Clean()%>"))+
        <%}
          else
          { %>
            printview.checkbox("Masses/Nodes",false) + 
        <%} %>
        <%if (data.AnswerOrEmptyString("Head").Contains("2"))
          { %>
            printview.checkbox("Other:<%=data.AnswerOrEmptyString("HeadOther").Clean()%>",<%= data.AnswerOrEmptyString("Head").Contains("0").ToString().ToLower()%>) ,
             
        <%}
          else
          { %>
            printview.checkbox("Other",false), 
        <%} %>
        "Head/Neck");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsEndocrineApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Endocrine",2);
        <%}else { %>
        printview.addsubsection(
            printview.col(3,
                printview.checkbox("Hypothyroidism",<%= data.AnswerArray("Endocrine").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Hypethyroidism",<%= data.AnswerArray("Endocrine").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Fatigue",<%= data.AnswerArray("Endocrine").Contains("2").ToString().ToLower()%>)) +
            printview.checkbox("Intolerance to heat/cold",<%= data.AnswerArray("Endocrine").Contains("3").ToString().ToLower()%>) +
            <%if (data.AnswerArray("Endocrine").Contains("4"))
              { %>
            printview.col(2,
                printview.checkbox("Diabetes",true) +
                printview.span("Onset:<%=data.AnswerOrEmptyString("EndocrineDiabetesDate").Clean()%>"))+
            <%}
              else
              { %>
            printview.checkbox("Diabetes",false) +
            <%} %>
            <%if (data.AnswerArray("Endocrine").Contains("5"))
              { %>
            printview.col(2,
                printview.checkbox("Diet/Oral",true)+
                printview.span("Control:<%=data.AnswerOrEmptyString("EndocrineDietControl").Clean()%>"+" "+ "<%=data.AnswerOrEmptyString("DateUnitLevel").Clean() %>"))+
            <%}
              else
              { %>
            printview.checkbox("Diet/Oral",false)+
            <%} %>
            <%if (data.AnswerArray("Endocrine").Contains("6"))
              { %>
            printview.col(2,
                printview.checkbox("Med/dose/freq:",true)+
                printview.span("<%=data.AnswerOrEmptyString("EndocrineMed").Clean() %>"))+
             <%}
              else
              { %>
            printview.checkbox("Med/dose/freq",false)+
            <%} %>
            <%if (data.AnswerArray("Endocrine").Contains("7"))
              { %>
            printview.col(2,
                printview.checkbox("Insulin/dose/freq:",true)+
                printview.span("<%=data.AnswerOrEmptyString("EndocrineInsulin").Clean() %>"))+
             <%}
              else
              { %>
            printview.checkbox("Insulin/dose/freq",false)+
            <%} %>
            printview.checkbox("Hyperglycemia",<%= data.AnswerArray("Endocrine").Contains("8").ToString().ToLower()%>) +
            printview.checkbox("Hypoglycemia",<%= data.AnswerArray("Endocrine").Contains("9").ToString().ToLower()%>) +
            <%if (data.AnswerArray("Endocrine").Contains("10"))
              { %>
            printview.col(2,
                printview.checkbox("Blood sugar range:",true)+
                printview.span("<%=data.AnswerOrEmptyString("EndocrineBlood").Clean() %>"))+
             <%}
              else
              { %>
            printview.checkbox("Blood sugar range",false)+
            <%} %>
            <%if (data.AnswerArray("Endocrine").Contains("11"))
              { %>
            printview.col(2,
                printview.checkbox("Self-care/self observational tasks:",true)+
                printview.span("<%=data.AnswerOrEmptyString("EndocrineSelf").Clean() %>"))+
             <%}
              else
              { %>
            printview.checkbox("Self-care/self observational tasks",false)+
            <%} %>
            <%if (data.AnswerArray("Endocrine").Contains("12"))
              { %>
            printview.col(2,
                printview.checkbox("Other:",true)+
                printview.span("<%=data.AnswerOrEmptyString("EndocrineOther").Clean() %>")),
             <%}
              else
              { %>
            printview.checkbox("Other",false),
            <%} %>
            "Endocrine",2);
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsCardiovascularApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Cardiovascular");
        <%}else { %>
        printview.addsubsection(
            printview.span("HEART SOUNDS:",true)+
            printview.col(2,
                printview.checkbox("Reg.",<%= data.AnswerArray("HeartSounds").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Irreg.<%=data.AnswerArray("HeartSounds").Contains("1") ? ":" + data.AnswerOrEmptyString("HeartSoundsIrreg").Clean() : string.Empty%>",<%= data.AnswerArray("HeartSounds").Contains("1").ToString().ToLower()%>) +
        
                printview.checkbox("Palpitations",<%= data.AnswerArray("Cardiovascular").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Pulse deficit<%=data.AnswerArray("Cardiovascular").Contains("1") ? ":" + data.AnswerOrEmptyString("CardiovascularPulseDeficit").Clean() : string.Empty%>",<%= data.AnswerArray("Cardiovascular").Contains("1").ToString().ToLower()%>)) +
            printview.col(3,
                printview.checkbox("Edema",<%= data.AnswerArray("Cardiovascular").Contains("2").ToString().ToLower()%>) +
                printview.checkbox("JVD",<%= data.AnswerArray("Cardiovascular").Contains("3").ToString().ToLower()%>) +
                printview.checkbox("Fatigue",<%= data.AnswerArray("Cardiovascular").Contains("4").ToString().ToLower()%>)) +
            printview.col(2,
                printview.checkbox("Cyanosis(site)<%=data.AnswerArray("Cardiovascular").Contains("5") ? ":" + data.AnswerOrEmptyString("CardiovascularCyanosis") : string.Empty%>",<%= data.AnswerArray("Cardiovascular").Contains("5").ToString().ToLower()%>) +
                printview.checkbox("Cap refill<%=data.AnswerArray("Cardiovascular").Contains("6") ? ":" + data.AnswerOrEmptyString("CardiovascularCap") : string.Empty%>",<%= data.AnswerArray("Cardiovascular").Contains("6").ToString().ToLower()%>) +
                printview.checkbox("Pulses<%=data.AnswerArray("Cardiovascular").Contains("7") ? ":" + data.AnswerOrEmptyString("CardiovascularPulses") : string.Empty%>",<%= data.AnswerArray("Cardiovascular").Contains("7").ToString().ToLower()%>) +
                printview.checkbox("Other<%=data.AnswerArray("Cardiovascular").Contains("8") ? ":" + data.AnswerOrEmptyString("CardiovascularOther") : string.Empty%>",<%= data.AnswerArray("Cardiovascular").Contains("8").ToString().ToLower()%>)),
            "Cardiovascular");
        <%} %>
        <%if (data.AnswerOrEmptyString("IsRespiratoryApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Respiratory");
        <%}else { %>
        printview.addsubsection(
            printview.col(2,
                printview.span("Chest circumference",true)+
                printview.span("<%=data.AnswerOrEmptyString("ChestCircumference").Clean()%>")+
                printview.checkbox("Retractions",<%= data.AnswerArray("RespiratoryChest").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Dyspnea",<%= data.AnswerArray("RespiratoryChest").Contains("1").ToString().ToLower()%>)) +
            printview.span("BREATH SOUNDS",true)+
                printview.col(2,
                    printview.checkbox("Clear",<%= data.AnswerArray("RespiratoryBreath").Contains("0").ToString().ToLower()%>) +
                    printview.checkbox("Crackles",<%= data.AnswerArray("RespiratoryBreath").Contains("1").ToString().ToLower()%>) +
                    printview.checkbox("Wheeze",<%= data.AnswerArray("RespiratoryBreath").Contains("2").ToString().ToLower()%>) +
                    printview.checkbox("Absent",<%= data.AnswerArray("RespiratoryBreath").Contains("3").ToString().ToLower()%>)) +
                printview.checkbox("Cough<%=data.AnswerArray("RespiratoryBreath").Contains("4") ? ":" + data.AnswerOrEmptyString("RespiratoryCough").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratoryBreath").Contains("4").ToString().ToLower()%>) +
                printview.checkbox("Productive<%=data.AnswerArray("RespiratoryBreath").Contains("5") ? ":" + data.AnswerOrEmptyString("RespiratoryProductive").Clean() + " & " + "Color:" + data.AnswerOrEmptyString("RespiratoryProductiveColor").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratoryBreath").Contains("5").ToString().ToLower()%>) +
                printview.span("SKIN",true)+
                printview.col(2,
                    printview.checkbox("Temp.change",<%= data.AnswerArray("RespiratorySkin").Contains("0").ToString().ToLower()%>) +
                    printview.checkbox("Color change<%=data.AnswerArray("RespiratorySkin").Contains("1") ? ":" + data.AnswerOrEmptyString("RespiratorySkinSpecify").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratorySkin").Contains("1").ToString().ToLower()%>)) +
                printview.checkbox("Percussion<%=data.AnswerArray("RespiratorySkin").Contains("2") ? ":" + data.AnswerOrEmptyString("RespiratoryPercussion").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratorySkin").Contains("2").ToString().ToLower()%>) +
                printview.col(2,
                    printview.checkbox("Chart lobe:",<%= data.AnswerArray("RespiratorySkin").Contains("3").ToString().ToLower()%>)+
                    printview.col(4,
                        printview.checkbox("R",<%= data.AnswerArray("RespiratoryChart").Contains("0").ToString().ToLower()%>) +
                        printview.checkbox("L",<%= data.AnswerArray("RespiratoryChart").Contains("1").ToString().ToLower()%>) +
                        printview.checkbox("Lat",<%= data.AnswerArray("RespiratoryChart").Contains("2").ToString().ToLower()%>) +
                        printview.checkbox("Ant",<%= data.AnswerArray("RespiratoryChart").Contains("3").ToString().ToLower()%>) +
                        printview.checkbox("Post",<%= data.AnswerArray("RespiratoryChart").Contains("4").ToString().ToLower()%>)+
                        printview.span("",true)) +
                    printview.checkbox("O2 Sat.<%=data.AnswerArray("RespiratorySkin").Contains("4") ? ":" + data.AnswerOrEmptyString("RespiratoryO2Sat").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratorySkin").Contains("4").ToString().ToLower()%>)+
                    printview.checkbox("O2 Use<%=data.AnswerArray("RespiratorySkin").Contains("5") ? ":" + data.AnswerOrEmptyString("RespiratoryO2Use").Clean() + "L/min by " + data.AnswerOrEmptyString("RespiratoryO2UseLevel").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratorySkin").Contains("5").ToString().ToLower()%>))+
                printview.checkbox("Other<%=data.AnswerArray("RespiratorySkin").Contains("6") ? ":" + data.AnswerOrEmptyString("RespiratoryOther").Clean() : string.Empty%>",<%= data.AnswerArray("RespiratorySkin").Contains("6").ToString().ToLower()%>),
            "Respiratory",2);
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsGenitourinaryApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Genitourinary");
        <%}else { %>
        printview.addsubsection(
            printview.checkbox("Diapers/day<%=data.AnswerArray("Genitourinary").Contains("0") ? ":" + data.AnswerOrEmptyString("GenitourinaryDiapers").Clean() : string.Empty%>",<%= data.AnswerArray("Genitourinary").Contains("0").ToString().ToLower()%>) +
            printview.checkbox("Toilt trained<%=data.AnswerArray("Genitourinary").Contains("1") ? ":" + data.AnswerOrEmptyString("ToiletTrainedTime").Clean() + " " + data.AnswerOrEmptyString("ToiletTrainedUnit").Clean() : string.Empty%>",<%= data.AnswerArray("Genitourinary").Contains("1").ToString().ToLower()%>) +
            printview.span("Urine:",true)+
            printview.col(2,
                printview.span("Color:<%=data.AnswerOrEmptyString("GenitourinaryColor").Clean()%>")+
                printview.span("Amt:<%=data.AnswerOrEmptyString("GenitourinaryAmt").Clean()%>")+
                printview.span("Odor:<%=data.AnswerOrEmptyString("GenitourinaryOdor").Clean()%>")+
                printview.span("Frequency:<%=data.AnswerOrEmptyString("GenitourinaryFreq").Clean()%>")+
                printview.checkbox("Burning",<%= data.AnswerArray("GenitourinaryUrine").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Itching",<%= data.AnswerArray("GenitourinaryUrine").Contains("1").ToString().ToLower()%>)) +
            <%if (data.AnswerArray("Genitourinary").Contains("2")){ %>
            printview.col(2,
                printview.checkbox("Enuresis:",true)+
                printview.span("bedtime ritual:<%=data.AnswerOrEmptyString("GenitourinaryEnuresis").Clean()%>"))+
            <%}else{ %>
                printview.checkbox("Enuresis",false)+
            <%} %>
            <%if (data.AnswerArray("Genitourinary").Contains("3")){ %>
            printview.col(2,
                printview.checkbox("Catheter:",true)+
                printview.span("type/brand:<%=data.AnswerOrEmptyString("GenitourinaryCatheter").Clean()%>"))+
            printview.col(3,
                printview.checkbox("Foley",<%= data.AnswerArray("GenitourinaryCatheterType").Contains("0").ToString().ToLower()%>) +
                printview.checkbox("Straight catheter",<%= data.AnswerArray("GenitourinaryCatheterType").Contains("1").ToString().ToLower()%>) +
                printview.checkbox("External",<%= data.AnswerArray("GenitourinaryCatheterType").Contains("2").ToString().ToLower()%>))+
            <%}else{ %>
                printview.checkbox("Catheter",false)+
            <%} %>
            printview.checkbox("Other<%=data.AnswerArray("Genitourinary").Contains("4") ? ":" + data.AnswerOrEmptyString("GenitourinaryOther").Clean() : string.Empty%>",<%= data.AnswerArray("Genitourinary").Contains("4").ToString().ToLower()%>),
        "Genitourinary");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsGastrointestinalApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Gastrointestinal",2);
        <%}else { %>
        printview.addsubsection(
            printview.span("Nutritional requirements for age (diet)",true)+
            printview.span("<%=data.AnswerOrEmptyString("GastrointestinalRequirements").Clean()%>",2)+
            printview.col(2,
                printview.span("Meal patterns",true)+
                printview.span("<%=data.AnswerOrEmptyString("GastrointestinalMealPattern").Clean()%>")+
                printview.span("Eatting behaviors",true)+
                printview.span("<%=data.AnswerOrEmptyString("GastrointestinalEatting").Clean()%>"))+
            <%if (data.AnswerArray("GastrointestinalEatting").Contains("0")) { %>
                printview.checkbox("Eatting disorder:",true) +
                printview.col(4,
                    printview.span("")+
                    printview.checkbox("Anorexia",<%=data.AnswerArray("GastrointestinalEattingDisorder").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Bulimia",<%=data.AnswerArray("GastrointestinalEattingDisorder").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Other<%=data.AnswerArray("GastrointestinalEattingDisorder").Contains("2") ? ":" + data.AnswerOrEmptyString("EattingDisorderOther").Clean() : string.Empty%>",<%=data.AnswerArray("GastrointestinalEattingDisorder").Contains("2").ToString().ToLower()%>))+
            <%}else{ %>
                printview.checkbox("Eatting disorder",false) +
            <%} %>
            printview.col(2,
                printview.span("Appetite:",true)+
                printview.span("<%=data.AnswerOrEmptyString("AppetiteLevel").Clean()%>"))+
            
            <%if (data.AnswerArray("GastrointestinalAppetite").Contains("0"))
              { %>   
            printview.col(2,
                printview.checkbox("Weight change",<%=data.AnswerArray("GastrointestinalAppetite").Contains("0").ToString().ToLower() %>)+
                printview.span("<%=data.AnswerOrEmptyString("WeightChangeLevel").Clean()%>"+" "+"<%=data.AnswerOrEmptyString("WeightChange").Clean()%>"+" lb. x "+"<%=data.AnswerOrEmptyString("WeightChangeTime").Clean()%>"+" "+"<%=data.AnswerOrEmptyString("WeightChangeTimeLevel").Clean()%>"))+
            <%}
              else
              { %>  
                printview.checkbox("Weight change",false)+
            <%} %>
            printview.col(2,
                printview.checkbox("Increase fluids",<%=data.AnswerArray("GastrointestinalAppetite").Contains("1").ToString().ToLower() %>)+
                printview.span("<%=data.AnswerArray("GastrointestinalAppetite").Contains("1") ? data.AnswerOrEmptyString("IncreaseFluids").Clean()+" amt" : string.Empty%>")+
                printview.checkbox("Restrict fluids",<%=data.AnswerArray("GastrointestinalAppetite").Contains("2").ToString().ToLower() %>)+
                printview.span("<%=data.AnswerArray("GastrointestinalAppetite").Contains("2") ? data.AnswerOrEmptyString("RestrictFluids").Clean()+" amt" : string.Empty%>")+
                printview.checkbox("Nausea/Vomiting",<%=data.AnswerArray("GastrointestinalAppetite").Contains("3").ToString().ToLower() %>)+
                printview.span("<%=data.AnswerArray("GastrointestinalAppetite").Contains("3") ? "Frequency:" + data.AnswerOrEmptyString("NauseaFrequency").Clean() + " amt:" + data.AnswerOrEmptyString("NauseaAmt").Clean() : string.Empty%>")+
                printview.span("Last BM",true)+
                printview.span("<%=data.AnswerOrEmptyString("LastBMDate").Clean() %>")+
                printview.span("Usual frequency",true)+
                printview.span("<%=data.AnswerOrEmptyString("GastrointestinalFrequency").Clean()%>"))+
            <%if (data.AnswerArray("Gastrointestinal").Contains("0"))
              { %>    
                printview.checkbox("Diarrhea:",true)+
                printview.col(3,
                    printview.checkbox("Black/Watery",<%=data.AnswerArray("GastrointestinalDiarrhea").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Less than 3x/day",<%=data.AnswerArray("GastrointestinalDiarrhea").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("More than 3x/day",<%=data.AnswerArray("GastrointestinalDiarrhea").Contains("2").ToString().ToLower()%>))+
                printview.col(2,
                    printview.span("<%=data.AnswerOrEmptyString("DiarrheaLevel").Clean()%>")+
                    printview.span("Amount:<%=data.AnswerOrEmptyString("DiarrheaAmount").Clean()%>"))+
            <%}
              else
              { %>
                printview.checkbox("Diarrhea",false)+
            <%} %>
            <%if (data.AnswerArray("Gastrointestinal").Contains("1"))
              {%>
                printview.col(2,
                    printview.checkbox("Abnormal stools:",true)+
                    printview.span("<%=data.AnswerOrEmptyString("AbnormalStoolsLevel").Clean() + ":" + data.AnswerOrEmptyString("AbnormalStoolsDescribe").Clean()%>"))+
                    
            <%}
              else
              { %>
                printview.checkbox("Abnormal stools",false)+
            <%} %>
            <%if (data.AnswerArray("Gastrointestinal").Contains("2"))
              {%>
            printview.col(2,
                printview.checkbox("Constipation:",true)+
                printview.span("<%=data.AnswerOrEmptyString("ConstipationLevel").Clean()%>"))+
                <%if (data.AnswerOrEmptyString("Constipation").Contains("lax"))
                  { %>
                printview.col(2,
                    printview.checkbox("Lax/enema use:",true)+
                    printview.span("Type:<%=data.AnswerOrEmptyString("EnemaType").Clean()%> Freq.:<%=data.AnswerOrEmptyString("EnemaFreq").Clean()%>"))+
                <%} %>
            <%}
              else
              { %>
                printview.checkbox("Constipation",false)+
            <%} %>
            
            <%if (data.AnswerArray("Gastrointestinal").Contains("3"))
              { %>
            printview.col(2,
                printview.checkbox("Flatulence:",true)+
                printview.span("Abdominal distention:<%=data.AnswerOrEmptyString("AbdominalDistentionLevel").Clean()%>"+" Freq.:"+"<%=data.AnswerOrEmptyString("AbdominalDistentionFreq").Clean()%>"))+
            <%}
              else
              { %>
                printview.checkbox("Flatulence:",false)+
            <%} %>
            <%if (data.AnswerArray("Gastrointestinal").Contains("4"))
              { %>
              printview.col(2,
                printview.checkbox("Impaction:",true)+
                printview.span("Flatulence <%=data.AnswerOrEmptyString("GastrointestinalImpaction").Contains("0") ? "Freq.:" + data.AnswerOrEmptyString("FlatulenceFreq").Clean() : string.Empty%>")+
                printview.span("")+
                printview.span("Ascites<%=data.AnswerOrEmptyString("GastrointestinalImpaction").Contains("1") ? ":Girth:" + data.AnswerOrEmptyString("AscitesGirth").Clean() + " inches " + data.AnswerOrEmptyString("AscitesLevel").Clean() + " x " + data.AnswerOrEmptyString("AscitesQuads").Clean()+"quads" : string.Empty%>"))+
            <%}
              else
              { %>  
              printview.checkbox("Impaction:",false)+
            <%} %>
              printview.col(2,
                printview.span("Bowel sounds",true)+
                printview.span("<%=data.AnswerOrEmptyString("BowelSoundsLevel").Clean() + " x " + data.AnswerOrEmptyString("BowelSoundsQuads").Clean() + " quads"%>")+
                printview.span("Colostomy:",true)+
                printview.span("<%=data.AnswerOrEmptyString("ColostomyLevel").Clean()%>" + " " + "<%=data.AnswerOrEmptyString("ColostomyDate").Clean()%>" + " " + "<%=data.AnswerOrEmptyString("ColostomyLevel2").Clean()%>")),                
              "Gastrointestinal",2);
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsNeurologicalApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Neurological");
        <%}else { %>
        printview.addsubsection(
            printview.span("Reflexes: N-normal, A-abnormal, NA-not applicable",true)+
            printview.col(4,
                printview.span("Rooting",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEURooting").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEURooting").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEURooting").Contains("2").ToString().ToLower()%>))+
                printview.span("Sucking",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUSucking").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUSucking").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUSucking").Contains("2").ToString().ToLower()%>))+
                printview.span("Orienting",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUOrienting").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUOrienting").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUOrienting").Contains("2").ToString().ToLower()%>))+
                printview.span("Babinski's",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUBabinski").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUBabinski").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUBabinski").Contains("2").ToString().ToLower()%>))+
                printview.span("Blinking",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUBlinking").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUBlinking").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUBlinking").Contains("2").ToString().ToLower()%>))+
                printview.span("Palmar",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUPalmar").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUPalmar").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUPalmar").Contains("2").ToString().ToLower()%>))+
                printview.span("Plantar",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUPlantar").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUPlantar").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUPlantar").Contains("2").ToString().ToLower()%>))+
                printview.span("Stepping/Dancing",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUSteppingDancing").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUSteppingDancing").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUSteppingDancing").Contains("2").ToString().ToLower()%>))+
                printview.span("Moros's/Startle",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUMoros").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUMoros").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUMoros").Contains("2").ToString().ToLower()%>))+
                printview.span("Tonic neck",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUTonicneck").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUTonicneck").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUTonicneck").Contains("2").ToString().ToLower()%>))+
                printview.span("Knee jerk",true)+
                printview.col(3,
                    printview.checkbox("N",<%=data.AnswerOrEmptyString("NEUKneejerk").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("A",<%=data.AnswerOrEmptyString("NEUKneejerk").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("NA",<%=data.AnswerOrEmptyString("NEUKneejerk").Contains("2").ToString().ToLower()%>)))+
            printview.col(2,
                printview.span("Other:",true)+
                printview.span("<%=data.AnswerOrEmptyString("ReflexOther").Clean()%>")+
                printview.checkbox("Oriented<%=data.AnswerOrEmptyString("NeurologicalOrient").Contains("0")?" x "+data.AnswerOrEmptyString("NeurologicalOrientContent").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("NeurologicalOrient").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Disoriented",<%=data.AnswerOrEmptyString("NeurologicalOrient").Contains("1").ToString().ToLower()%>))+
                printview.span("Cognitive development problems",true)+
                printview.col(2,
                    printview.checkbox("Concepts",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Logic",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Impaired decision-making ability",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("2").ToString().ToLower()%>)+
                
                    printview.checkbox("Memory loss<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("3") ? ":" + data.AnswerOrEmptyString("MemoryLossLevel").Clean() : string.Empty%>",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("3").ToString().ToLower()%>))+
                printview.checkbox("Stuporous/Hallucinations<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("4") ? ":" + data.AnswerOrEmptyString("StuporousLevel").Clean() : string.Empty%>",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Headache<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("5")?":Location:"+data.AnswerOrEmptyString("HeadacheLocation").Clean()+" Freq.:"+data.AnswerOrEmptyString("HeadacheFreq").Clean():string.Empty%>",<%=data.AnswerOrEmptyString("NeurologicalCognitive").Contains("5").ToString().ToLower()%>)+
                printview.col(3,
                    printview.span("Infant motor skills:",true)+
                    printview.checkbox("Lifts head",<%=data.AnswerOrEmptyString("InfantMotor").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Crawls/creeps",<%=data.AnswerOrEmptyString("InfantMotor").Contains("1").ToString().ToLower()%>)+
                    printview.span("Rolls over:",true)+
                    printview.checkbox("Stomach to back",<%=data.AnswerOrEmptyString("RollsOver").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Back to stomach",<%=data.AnswerOrEmptyString("RollsOver").Contains("1").ToString().ToLower()%>)+
                    printview.span("Sits:",true)+
                    printview.checkbox("With assistance",<%=data.AnswerOrEmptyString("Sits").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Without assistance",<%=data.AnswerOrEmptyString("Sits").Contains("1").ToString().ToLower()%>)+
                    printview.span("Stands:",true)+
                    printview.checkbox("With assistance",<%=data.AnswerOrEmptyString("Stands").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Without assistance",<%=data.AnswerOrEmptyString("Stands").Contains("1").ToString().ToLower()%>))+
                printview.col(4,
                    printview.span("Motor Skills:",true)+
                    printview.checkbox("Walks",<%=data.AnswerOrEmptyString("MotorSkills").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Runs",<%=data.AnswerOrEmptyString("MotorSkills").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Jumps",<%=data.AnswerOrEmptyString("MotorSkills").Contains("2").ToString().ToLower()%>)+
                    printview.span("",true)+
                    printview.checkbox("Hops",<%=data.AnswerOrEmptyString("MotorSkills").Contains("3").ToString().ToLower()%>)+
                    printview.checkbox("Skips",<%=data.AnswerOrEmptyString("MotorSkills").Contains("4").ToString().ToLower()%>)+
                    printview.checkbox("Balance",<%=data.AnswerOrEmptyString("MotorSkills").Contains("5").ToString().ToLower()%>))+
                printview.col(2,
                    printview.checkbox("Motor change<%=data.AnswerOrEmptyString("MotorSkills").Contains("6")?":"+data.AnswerOrEmptyString("MotorChangeLevel").Clean():string.Empty%>",<%=data.AnswerOrEmptyString("MotorSkills").Contains("6").ToString().ToLower()%>)+
                    printview.checkbox("Tremor<%=data.AnswerOrEmptyString("MotorSkills").Contains("7")?":"+data.AnswerOrEmptyString("TremorsLevel").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("MotorSkills").Contains("7").ToString().ToLower()%>))+
                printview.checkbox("Weakness<%=data.AnswerOrEmptyString("MotorSkills").Contains("8")?":"+data.AnswerOrEmptyString("WeaknessLevel").Clean()+" Location:"+data.AnswerOrEmptyString("WeaknessLocation").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("MotorSkills").Contains("8").ToString().ToLower()%>)+
                printview.col(3,
                    printview.span("Hand grips",true)+
                    printview.span("<%=data.AnswerOrEmptyString("HandGripsLevel").Clean() %>")+
                    printview.span("<%=data.AnswerOrEmptyString("HandGripsSpecify1").Clean() %>")+
                    printview.span("",true)+
                    printview.span("<%=data.AnswerOrEmptyString("HandGripsLevel2").Clean() %>")+
                    printview.span("<%=data.AnswerOrEmptyString("HandGripsSpecify2").Clean() %>"))+
                printview.col(2,
                    printview.checkbox("Sensory loss<%=data.AnswerOrEmptyString("HandGrips").Contains("0")?":"+data.AnswerOrEmptyString("HandGripsSpecify3").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("HandGrips").Contains("0").ToString().ToLower()%>)+  
                    printview.checkbox("Numbness<%=data.AnswerOrEmptyString("HandGrips").Contains("1")?":"+data.AnswerOrEmptyString("HandGripsSpecify4").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("HandGrips").Contains("1").ToString().ToLower()%>)+  
                    printview.span("Communication patterns/Ability",true)+
                    printview.span("<%=data.AnswerOrEmptyString("CommunicationPatternsDescription").Clean() %>"))+
                printview.checkbox("Unequal pupils<%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("0")?":"+data.AnswerOrEmptyString("UnequalPupilLevel").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Psychotropic drug use<%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("1")?":"+data.AnswerOrEmptyString("PsychotropicDrugUse").Clean()+" Dose/Freq:"+data.AnswerOrEmptyString("PsychotropicDrugUseUnit").Clean():string.Empty %>",<%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("1").ToString().ToLower()%>)+   
                printview.checkbox("Other<%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("2") ? ":" + data.AnswerOrEmptyString("CommunicationPatternsOther").Clean() : string.Empty%>", <%=data.AnswerOrEmptyString("CommunicationPatterns").Contains("2").ToString().ToLower()%>),                
                "Neurological");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsPsychosocialApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Psychosocial",2);
        <%}else { %>
        printview.addsubsection(
            printview.col(3,
                printview.checkbox("Angry",<%=data.AnswerArray("Psychosocial").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Flat affect",<%=data.AnswerArray("Psychosocial").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Discouraged",<%=data.AnswerArray("Psychosocial").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Withdrawn",<%=data.AnswerArray("Psychosocial").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Difficulty coping",<%=data.AnswerArray("Psychosocial").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Disorganized",<%=data.AnswerArray("Psychosocial").Contains("5").ToString().ToLower()%>))+
            <%if (data.AnswerArray("Psychosocial").Contains("6"))
              { %>
                printview.checkbox("Recent family change:",true)+
                   printview.col(4,
                        printview.span("",true)+
                        printview.checkbox("Birth",<%=data.AnswerOrEmptyString("RecentChange").Contains("0").ToString().ToLower()%>)+
                        printview.checkbox("Death",<%=data.AnswerOrEmptyString("RecentChange").Contains("1").ToString().ToLower()%>)+
                        printview.checkbox("Moved",<%=data.AnswerOrEmptyString("RecentChange").Contains("2").ToString().ToLower()%>)+
                        printview.span("",true)+
                        printview.checkbox("Divorce",<%=data.AnswerOrEmptyString("RecentChange").Contains("3").ToString().ToLower()%>)+
                        printview.checkbox("Other<%=data.AnswerOrEmptyString("RecentChange").Contains("4") ? ":" + data.AnswerOrEmptyString("RecentChangeOther").Clean() : string.Empty%>",<%=data.AnswerOrEmptyString("RecentChange").Contains("4").ToString().ToLower()%>)+
                        printview.span("",true))+
            <%}
              else
              { %>
                printview.checkbox("Recent family change:",false)+
            <%} %>
            <%if (data.AnswerArray("Psychosocial").Contains("7"))
              { %>
                printview.col(3,
                    printview.checkbox("Suicidal:",true)+
                    printview.checkbox("Ideation",<%=data.AnswerOrEmptyString("PsychosocialSui").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Verbalized",<%=data.AnswerOrEmptyString("PsychosocialSui").Contains("1").ToString().ToLower()%>))+
            <%}
              else
              { %>
                printview.checkbox("Suicidal",false)+
            <%} %>
            
            <%if (data.AnswerArray("Psychosocial").Contains("8"))
              { %>
                printview.checkbox("Depressed:",true)+
                printview.col(3,
                    printview.checkbox("Recent",<%=data.AnswerOrEmptyString("PsychosocialDepressed").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Long term",<%=data.AnswerOrEmptyString("PsychosocialDepressed").Contains("1").ToString().ToLower()%>)+
                    printview.span("Due to:<%=data.AnswerOrEmptyString("DepressedReason").Clean()%>"))+
            <%}
              else
              { %>
                printview.checkbox("Depressed",false)+
            <%} %>
            
            <%if (data.AnswerArray("Psychosocial").Contains("9"))
              { %>
                printview.checkbox("Substance use:",true)+
                printview.col(4,
                    printview.span("",true)+
                    printview.checkbox("Drug",<%=data.AnswerOrEmptyString("PsychosocialSubstance").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Alcohol",<%=data.AnswerOrEmptyString("PsychosocialSubstance").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Tabacco",<%=data.AnswerOrEmptyString("PsychosocialSubstance").Contains("2").ToString().ToLower()%>))+
              <%}
              else
              { %>
                printview.checkbox("Substance use",false)+
               <%} %>
               
               <%if (data.AnswerArray("Psychosocial").Contains("10"))
                 { %>
                printview.checkbox("Evidence of abuse:",true)+
                printview.col(4,
                    printview.span("",true)+
                    printview.checkbox("Potential",<%=data.AnswerOrEmptyString("PsychosocialAbuse").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Actual",<%=data.AnswerOrEmptyString("PsychosocialAbuse").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Verbal/Emotional",<%=data.AnswerOrEmptyString("PsychosocialAbuse").Contains("2").ToString().ToLower()%>)+
                    printview.span("",true)+
                    printview.checkbox("Physical",<%=data.AnswerOrEmptyString("PsychosocialAbuse").Contains("3").ToString().ToLower()%>)+
                    printview.checkbox("Financial",<%=data.AnswerOrEmptyString("PsychosocialAbuse").Contains("4").ToString().ToLower()%>)+
                    printview.span("",true))+
              <%}
                 else
                 { %>
                    printview.checkbox("Evidence of abuse",false)+
              <%} %>
              printview.span("Describe findings:",true)+
              printview.span("<%=data.AnswerOrEmptyString("PsychosocialFindings").Clean()%>",2)+
              printview.span("Describe relationships with the following:",true)+
              printview.col(2,
                printview.span("Parents:<%=data.AnswerOrEmptyString("RelationshipWithParents").Clean()%>")+
                printview.span("Siblings:<%=data.AnswerOrEmptyString("RelationshipWithSiblings").Clean()%>")+
                printview.span("Peers:<%=data.AnswerOrEmptyString("RelationshipWithPeers").Clean()%>")+
                printview.span("",true))+
              printview.span("Child care arrangements:",true)+
              printview.col(3,
                printview.checkbox("Daycare",<%=data.AnswerOrEmptyString("ChildCareArran").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Home",<%=data.AnswerOrEmptyString("ChildCareArran").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Private sitter",<%=data.AnswerOrEmptyString("ChildCareArran").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Family member",<%=data.AnswerOrEmptyString("ChildCareArran").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Other<%=data.AnswerOrEmptyString("ChildCareArran").Contains("4") ? ":" + data.AnswerOrEmptyString("ChildCareArranOther").Clean() : string.Empty%>",<%=data.AnswerOrEmptyString("ChildCareArran").Contains("4").ToString().ToLower()%>)+
                printview.span("",true))+
             printview.col(2,
                printview.span("Behavior at day care/school:",true)+
                printview.span("<%=data.AnswerOrEmptyString("SchoolBehavior").Clean() %>")+
                printview.span("Usual sleep/rest pattern:",true)+
                printview.span("<%=data.AnswerOrEmptyString("RestPattern").Clean()%>")+
                printview.span("Sleeping arrangements:",true)+
                printview.span("<%=data.AnswerOrEmptyString("SleepingArran").Clean()%>")+
                printview.span("Other:",true)+
                printview.span("<%=data.AnswerOrEmptyString("PsychosocialOther").Clean()%>")),
            "Psychosocial",2);
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsPainApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Pain");
        <%}else { %>
        printview.addsubsection(
            printview.col(2,
                printview.span("Origin",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainOrigin").Clean()%>")+
                printview.span("Onset",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainOnset").Clean()%>")+
                printview.span("Location",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainLocation").Clean()%>")+
                printview.span("Quality",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainQuality").Clean()%>")+
                printview.span("Intensity",true)+
                printview.span("<%=data.AnswerOrEmptyString("GenericPainLevel").Clean()%>")+
                printview.span("Pain management history",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainHistory").Clean()%>")+
                printview.span("Present pain management regimen",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainRegimen").Clean()%>")+
                printview.span("Effectiveness",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainEffective").Clean()%>")+
                printview.span("Other",true)+
                printview.span("<%=data.AnswerOrEmptyString("PainOther").Clean()%>")),
             "Pain");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsGenitaliaApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Genitalia",2);
        <%}else { %>
        printview.addsubsection(
            printview.col(2,
                printview.checkbox("Circumcised",<%=data.AnswerArray("GenitaliaCircumcised").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Uncircumcised",<%=data.AnswerArray("GenitaliaCircumcised").Contains("1").ToString().ToLower()%>))+
            printview.col(3,
                printview.span("Scrotum:",true)+
                printview.checkbox("WNL",<%=data.AnswerArray("GenitaliaScrotum").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Swollen",<%=data.AnswerArray("GenitaliaScrotum").Contains("1").ToString().ToLower()%>))+
            <%if (data.AnswerArray("GenitaliaTest").Contains("1"))
              { %>
                printview.span("Testes:", true)+
                printview.checkbox("Descended",<%=data.AnswerArray("GenitaliaTest").Contains("0").ToString().ToLower()%>)+
                printview.col(4,
                    printview.checkbox("Undescended:",true)+
                    printview.checkbox("Right",<%=data.AnswerArray("TestUndescended").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Left",<%=data.AnswerArray("TestUndescended").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Bilateral",<%=data.AnswerArray("TestUndescended").Contains("2").ToString().ToLower()%>))+
            <%}
              else
              {%>
                printview.col(3,
                    printview.span("Testes:", true)+
                    printview.checkbox("Descended",<%=data.AnswerArray("GenitaliaTest").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Undescended",<%=data.AnswerArray("GenitaliaTest").Contains("1").ToString().ToLower()%>))+
            <%} %>
            printview.col(2,
                printview.checkbox("Puberty",<%=data.AnswerArray("Puberty").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Menarche",<%=data.AnswerArray("Puberty").Contains("1").ToString().ToLower()%>))+
            <%if (data.AnswerArray("Puberty").Contains("1"))
              { %>
                printview.col(2,
                    printview.span("",true)+
                    printview.col(2,
                        printview.span("Age:<%=data.AnswerOrEmptyString("MenarcheAge").Clean()%>")+
                        printview.span("LMP:<%=data.AnswerOrEmptyString("MenarcheLMP").Clean()%>")))+
            <%} %>
            printview.span("Pregnancy:",true)+
            printview.col(2,
                printview.span("Gravida:<%=data.AnswerOrEmptyString("PregnancyGravida").Clean()%>")+
                printview.span("Para:<%=data.AnswerOrEmptyString("PregnancyPara").Clean()%>")+
                printview.span("EDC:<%=data.AnswerOrEmptyString("PregnancyEDC").Clean()%>")+
                printview.span("Other:<%=data.AnswerOrEmptyString("GenitaliaOther").Clean()%>")),
            "Genitalia",2);
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsMusculoskeletalApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Musculoskeletal");
        <%}else { %>
        printview.addsubsection(
            printview.col(2,
                printview.span("Posture",true)+
                printview.span("<%=data.AnswerOrEmptyString("Posture").Clean()%>")+
                printview.span("Strength",true)+
                printview.span("<%=data.AnswerOrEmptyString("Strength").Clean()%>")+
                printview.span("Endurance",true)+
                printview.span("<%=data.AnswerOrEmptyString("Endurance").Clean()%>")+
                printview.checkbox("Scoliosis",<%=data.AnswerArray("Musculoskeletal").Contains("0").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("Musculoskeletal").Contains("0") ? "type:" + data.AnswerOrEmptyString("ScoliosisType").Clean() : string.Empty%>")+
                printview.checkbox("Swollen/Painful joints",<%=data.AnswerArray("Musculoskeletal").Contains("1").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("Musculoskeletal").Contains("1") ? "specify:" + data.AnswerOrEmptyString("Swollen").Clean() : string.Empty%>")+
                printview.checkbox("Fracture",<%=data.AnswerArray("Musculoskeletal").Contains("2").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("Musculoskeletal").Contains("2") ? "location:" + data.AnswerOrEmptyString("Fracture").Clean() : string.Empty%>")+
                printview.checkbox("Decreased ROM",<%=data.AnswerArray("Musculoskeletal").Contains("3").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("Musculoskeletal").Contains("3") ? "specify:" + data.AnswerOrEmptyString("DecreasedROM").Clean() : string.Empty%>")+
                printview.checkbox("Other",<%=data.AnswerArray("Musculoskeletal").Contains("4").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerOrEmptyString("MuscuOther").Clean() %>")),
                "Musculoskeletal");
        <%} %>
        
        <%if (data.AnswerOrEmptyString("IsWoundsApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Skin conditions/Wounds",2);
        <%}else { %>
        printview.addsubsection(
            printview.span("Check all that apply:",true)+
            printview.col(3,
                printview.checkbox("Mongolian spots",<%=data.AnswerArray("SkinWound").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Itch",<%=data.AnswerArray("SkinWound").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Rash",<%=data.AnswerArray("SkinWound").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Dry",<%=data.AnswerArray("SkinWound").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Scaling",<%=data.AnswerArray("SkinWound").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Incision",<%=data.AnswerArray("SkinWound").Contains("5").ToString().ToLower()%>)+
                printview.checkbox("Wounds",<%=data.AnswerArray("SkinWound").Contains("6").ToString().ToLower()%>)+
                printview.checkbox("Lesions",<%=data.AnswerArray("SkinWound").Contains("7").ToString().ToLower()%>)+
                printview.checkbox("Sutures",<%=data.AnswerArray("SkinWound").Contains("8").ToString().ToLower()%>)+
                printview.checkbox("Staples",<%=data.AnswerArray("SkinWound").Contains("9").ToString().ToLower()%>)+
                printview.checkbox("Abrasions",<%=data.AnswerArray("SkinWound").Contains("10").ToString().ToLower()%>)+
                printview.checkbox("Lacerations",<%=data.AnswerArray("SkinWound").Contains("11").ToString().ToLower()%>)+
                printview.checkbox("Bruises",<%=data.AnswerArray("SkinWound").Contains("12").ToString().ToLower()%>)+
                printview.checkbox("Ecchymosis",<%=data.AnswerArray("SkinWound").Contains("13").ToString().ToLower()%>)+
                printview.checkbox("Edema",<%=data.AnswerArray("SkinWound").Contains("14").ToString().ToLower()%>)+
                printview.checkbox("Hemangiomas",<%=data.AnswerArray("SkinWound").Contains("15").ToString().ToLower()%>)+
                printview.span("")+
                printview.span(""))+
            printview.col(2,
                printview.span("Pallor:",true)+
                printview.span("<%=data.AnswerOrEmptyString("PallorLevel").Clean()%>")+
                printview.span("Turgor:",true)+
                printview.span("<%=data.AnswerOrEmptyString("TurgorLevel").Clean()%>"))+
                printview.span("Other:",true)+
                printview.span("<%=data.AnswerOrEmptyString("SkinWoundOther").Clean()%>",2),
                "Skin conditions/Wounds",2);
        <%} %>
         
        <%if (data.AnswerOrEmptyString("IsAppliancesApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Appliances/Aids/Special Equipment",2);
        <%}else { %>
        printview.addsubsection(
            printview.col(2,
                printview.checkbox("Crutch(es)",<%=data.AnswerArray("Appliances").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Wheelchair",<%=data.AnswerArray("Appliances").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Cane",<%=data.AnswerArray("Appliances").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Walker",<%=data.AnswerArray("Appliances").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Brace/Orthotics<%=data.AnswerArray("Appliances").Contains("4") ? ":" + data.AnswerOrEmptyString("AppliancesBrace").Clean() : string.Empty%>",<%=data.AnswerArray("Appliances").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Transfer equipment<%=data.AnswerArray("Appliances").Contains("5") ? ":" + data.AnswerOrEmptyString("TransferEquipmentLevel").Clean() : string.Empty%>",<%=data.AnswerArray("Appliances").Contains("5").ToString().ToLower()%>)+
                printview.checkbox("Bedside commode",<%=data.AnswerArray("Appliances").Contains("6").ToString().ToLower()%>)+
                printview.checkbox("Prosthesis<%=data.AnswerArray("Appliances").Contains("7") ? ":" + data.AnswerOrEmptyString("ProsthesisLevel").Clean() + " " + data.AnswerOrEmptyString("ProsthesisOther").Clean() : string.Empty%>",<%=data.AnswerArray("Appliances").Contains("7").ToString().ToLower()%>)+
                printview.checkbox("Grab bars<%=data.AnswerArray("Appliances").Contains("8") ? ":" + data.AnswerOrEmptyString("GrabbarsLevel").Clean() + " " + data.AnswerOrEmptyString("GrabbarsOther").Clean() : string.Empty%>",<%=data.AnswerArray("Appliances").Contains("8").ToString().ToLower()%>)+
                printview.checkbox("Hospital bed<%=data.AnswerArray("Appliances").Contains("9") ? ":" + data.AnswerOrEmptyString("HospitalBedLevel").Clean() + " " + data.AnswerOrEmptyString("HospitalBedOther").Clean() : string.Empty%>",<%=data.AnswerArray("Appliances").Contains("9").ToString().ToLower()%>)+
                <%if (data.AnswerArray("Appliances").Contains("9"))
                  { %>
                printview.span("",true)+
                printview.span("Overlays:<%=data.AnswerOrEmptyString("Overlays").Clean()%>")+
                <%} %>
                printview.checkbox("Fire Alarm",<%=data.AnswerArray("Appliances").Contains("11").ToString().ToLower()%>)+
                printview.checkbox("Smoke Alarm",<%=data.AnswerArray("Appliances").Contains("12").ToString().ToLower()%>))+
                <%if(data.AnswerArray("Appliances").Contains("10")){ %>
                printview.col(2,
                    printview.checkbox("Oxygen:",true)+
                    printview.span("HME Co.:<%=data.AnswerOrEmptyString("HMECo").Clean() %>")+
                    printview.span("HME Rep.:<%=data.AnswerOrEmptyString("HMERep").Clean() %>")+
                    printview.span("HME Phone:<%=data.AnswerOrEmptyString("HMEPhone").Clean() %>"))+
                <%}else{ %>
                    printview.checkbox("Oxygen",false)+
                <%} %>
                printview.checkbox("Equipment needs<%=data.AnswerArray("Appliances").Contains("13")?":"+data.AnswerOrEmptyString("EquipmentSpecify").Clean():string.Empty %>",<%=data.AnswerArray("Appliances").Contains("13").ToString().ToLower()%>)+
                printview.span("Other",true)+
                printview.span("<%=data.AnswerOrEmptyString("EquipmentOther").Clean() %>"),
             "Appliances/Aids/Special Equipment"); 
        <%} %>
        
        printview.addsubsection(
            printview.col(3,
                printview.checkbox("House",<%=data.AnswerArray("LivingArrange").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Apartment",<%=data.AnswerArray("LivingArrange").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("New environment",<%=data.AnswerArray("LivingArrange").Contains("2").ToString().ToLower()%>))+
            printview.col(2,
                printview.span("Primary language:",true)+
                printview.span("<%=data.AnswerOrEmptyString("PrimaryLanguage").Clean()%>")+
                printview.checkbox("Language barrier",<%=data.AnswerArray("PrimaryLanguageOption").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Needs interpreter",<%=data.AnswerArray("PrimaryLanguageOption").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Learning barrier:",<%=data.AnswerArray("PrimaryLanguageOption").Contains("2").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("PrimaryLanguageOption").Contains("2") ? data.AnswerOrEmptyString("LearningBarrierLevel").Clean() : string.Empty%>")+
                printview.checkbox("Able to read/write:",<%=data.AnswerArray("PrimaryLanguageOption").Contains("3").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("PrimaryLanguageOption").Contains("3") ? "Educational level:" + data.AnswerOrEmptyString("EducationalLevel").Clean() : string.Empty%>"))+
                printview.checkbox("Spiritual/Cultural implications that impact care",<%=data.AnswerArray("PrimaryLanguageOption").Contains("4").ToString().ToLower()%>)+
                <%if (data.AnswerArray("PrimaryLanguageOption").Contains("4"))
                  { %>
                    printview.col(2,
                        printview.span("spiritual resource:",true)+
                        printview.span("<%=data.AnswerOrEmptyString("SpiritualResource").Clean()%>")+
                        printview.span("Phone No.:",true)+
                        printview.span("<%=data.AnswerOrEmptyString("SpiritualPhone").Clean()%>"))+
                <%} %>
                printview.checkbox("Siblings<%=data.AnswerArray("PrimaryLanguageOption").Contains("5") ? ":" + data.AnswerOrEmptyString("SiblingSpecify").Clean() : string.Empty%>",<%=data.AnswerArray("PrimaryLanguageOption").Contains("5").ToString().ToLower()%>)+
                printview.col(2,
                    printview.span("Primary caregiver",true)+
                    printview.span("<%=data.AnswerOrEmptyString("CaregiverName").Clean() %>"))+
                printview.span("Relationship/Health status",true)+
                printview.col(2,
                    printview.checkbox("Assists with ADLs",<%=data.AnswerArray("PrimaryCaregiver").Contains("0").ToString().ToLower()%>)+
                    printview.checkbox("Provides physical care",<%=data.AnswerArray("PrimaryCaregiver").Contains("1").ToString().ToLower()%>)+
                    printview.checkbox("Other",<%=data.AnswerArray("PrimaryCaregiver").Contains("2").ToString().ToLower()%>)+
                    printview.span("<%=data.AnswerArray("PrimaryCaregiver").Contains("2") ? data.AnswerOrEmptyString("PrimaryCaregiverOther").Clean() : string.Empty%>")),
                "Living Arrangement/Caregiver",2);
                
                printview.addsubsection(
                    printview.col(2,
                        printview.checkbox("Fire/electrical safety",<%=data.AnswerArray("Safety").Contains("0").ToString().ToLower()%>)+
                        printview.checkbox("Burn prevention",<%=data.AnswerArray("Safety").Contains("1").ToString().ToLower()%>)+
                        printview.checkbox("Poisoning prevention",<%=data.AnswerArray("Safety").Contains("2").ToString().ToLower()%>)+
                        printview.checkbox("Falls prevention",<%=data.AnswerArray("Safety").Contains("3").ToString().ToLower()%>)+
                        printview.checkbox("Water safety",<%=data.AnswerArray("Safety").Contains("4").ToString().ToLower()%>)+
                        printview.checkbox("Siderails up",<%=data.AnswerArray("Safety").Contains("5").ToString().ToLower()%>)+
                        printview.checkbox("Clear pathways",<%=data.AnswerArray("Safety").Contains("6").ToString().ToLower()%>)+
                        printview.checkbox("Suffocation precautions",<%=data.AnswerArray("Safety").Contains("7").ToString().ToLower()%>)+
                        printview.checkbox("Aspiration precautions",<%=data.AnswerArray("Safety").Contains("8").ToString().ToLower()%>)+
                        printview.checkbox("Elevate head of bed",<%=data.AnswerArray("Safety").Contains("9").ToString().ToLower()%>)+
                        printview.checkbox("Seizure precautions",<%=data.AnswerArray("Safety").Contains("10").ToString().ToLower()%>)+
                        printview.checkbox("Transfer/ambulation safety",<%=data.AnswerArray("Safety").Contains("11").ToString().ToLower()%>)+
                        printview.checkbox("Wheelchair precautions",<%=data.AnswerArray("Safety").Contains("12").ToString().ToLower()%>)+
                        printview.checkbox("Proper use of assistive devices",<%=data.AnswerArray("Safety").Contains("13").ToString().ToLower()%>)+
                        printview.checkbox("Universal precautions",<%=data.AnswerArray("Safety").Contains("14").ToString().ToLower()%>)+
                        printview.checkbox("Sharps and/or supplies disposals",<%=data.AnswerArray("Safety").Contains("15").ToString().ToLower()%>)+
                        printview.checkbox("Cardiac prevention",<%=data.AnswerArray("Safety").Contains("16").ToString().ToLower()%>)+
                        printview.checkbox("Diabetic precautions",<%=data.AnswerArray("Safety").Contains("17").ToString().ToLower()%>)+
                        printview.checkbox("Oxygen safety/precautions",<%=data.AnswerArray("Safety").Contains("18").ToString().ToLower()%>)+
                        printview.checkbox("Bleeding precautions",<%=data.AnswerArray("Safety").Contains("19").ToString().ToLower()%>))+
                   printview.checkbox("Other<%=data.AnswerArray("Safety").Contains("20")?":"+data.AnswerOrEmptyString("SafetyOther").Clean():string.Empty%>",<%=data.AnswerArray("Safety").Contains("20").ToString().ToLower()%>),
                   
                   "Safety");
        <%if (data.AnswerOrEmptyString("IsHematologyApply").Equals("1")){ %>
        printview.addsubsection(
        printview.checkbox("No Problem",true),"Hematology",2);
        <%}else { %>
        printview.addsubsection(
            printview.checkbox("Anemia",<%=data.AnswerArray("Hematology").Contains("0").ToString().ToLower()%>)+
            printview.col(2,
                printview.checkbox("Bilirubin",<%=data.AnswerArray("Hematology").Contains("1").ToString().ToLower()%>)+
                printview.span("<%=data.AnswerArray("Hematology").Contains("1") ? "result:" + data.AnswerOrEmptyString("HematologyBilirubin").Clean() : string.Empty%>"))+
            printview.checkbox("Other",<%=data.AnswerArray("Hematology").Contains("2").ToString().ToLower()%>)+
            printview.span("<%=data.AnswerOrEmptyString("HematologyOther").Clean()%>"),
            "Hematology",2)    
        <%} %>
        
        printview.addsubsection(
            printview.col(2,
                printview.checkbox("Do not resuscitate",<%=data.AnswerArray("AdvanceDirective").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Do not hospitalize",<%=data.AnswerArray("AdvanceDirective").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Organ donor",<%=data.AnswerArray("AdvanceDirective").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Education needed",<%=data.AnswerArray("AdvanceDirective").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Copies on file",<%=data.AnswerArray("AdvanceDirective").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Funeral arrangements made",<%=data.AnswerArray("AdvanceDirective").Contains("5").ToString().ToLower()%>))+
            printview.span("Comments",true)+
            printview.span("<%=data.AnswerOrEmptyString("AdvanceDirectiveOther").Clean()%>",2),
        "Advance Directives");
        
        printview.addsection(
            printview.span("I-Instruct; R-Reinstruct; D-Demonstrate; U-Understands Instructions",true)+
            printview.col(3,
                printview.span("Subject",true)+
                printview.span("Option",true)+
                printview.span("Patient/Caregiver response",true)+
                printview.span("Diet",true)+
                printview.span("<%=data.AnswerOrEmptyString("DietLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("DietResponse").Clean()%>")+
                printview.span("Medication(s)",true)+
                printview.span("<%=data.AnswerOrEmptyString("MedicationLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("MedicationResponse").Clean()%>")+
                printview.span("Disease process",true)+
                printview.span("<%=data.AnswerOrEmptyString("DiseaseLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("DiseaseResponse").Clean()%>")+
                printview.span("Safety",true)+
                printview.span("<%=data.AnswerOrEmptyString("SafetyLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("SafetyResponse").Clean()%>")+
                printview.span("S/S to report",true)+
                printview.span("<%=data.AnswerOrEmptyString("SSLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("SSResponse").Clean()%>")+
                printview.span("Well child care",true)+
                printview.span("<%=data.AnswerOrEmptyString("WellLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("WellResponse").Clean()%>")+
                printview.span("Activities",true)+
                printview.span("<%=data.AnswerOrEmptyString("ActivitiesLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("ActivitiesResponse").Clean()%>")+
                printview.span("Treatments",true)+
                printview.span("<%=data.AnswerOrEmptyString("TreatmentsLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("TreatmentsResponse").Clean()%>")+
                printview.span("Injury prevention",true)+
                printview.span("<%=data.AnswerOrEmptyString("InjuryLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("InjuryResponse").Clean()%>")+
                printview.span("Growth",true)+
                printview.span("<%=data.AnswerOrEmptyString("GrowthLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("GrowthResponse").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("TeachingOther").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("OtherLevel").Clean()%>")+
                printview.span("<%=data.AnswerOrEmptyString("OtherResponse").Clean()%>")),
            "Teaching/Training");
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("SkilledCareProvided").Clean()%>",3),
            "Skilled care provided this visit",2);
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("SummaryOfGrowth").Clean()%>",3),
            "Summary of growth and development for age");
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("LearningPotential").Clean()%>",3),
            "Prognosis/Learning potential",2);
        printview.addsubsection(
            printview.span("<%=data.AnswerOrEmptyString("DischrgePlan").Clean()%>",3),
            "Discharge plans");
        printview.addsection(
            printview.col(2,
                printview.span("Medication status: ",true)+
                printview.checkbox("Medication regimen completed/reviewed",<%=data.AnswerOrEmptyString("MedicationStatus").Contains("0").ToString().ToLower()%>))+
            printview.span("Check if any of the following were identified:",true)+
            printview.col(3,
                printview.checkbox("Potential adverse effects/drug reactions",<%=data.AnswerArray("SummaryChecklist").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("Ineffective drug therapy",<%=data.AnswerArray("SummaryChecklist").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("Significant side effects",<%=data.AnswerArray("SummaryChecklist").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("Significant drug interactions",<%=data.AnswerArray("SummaryChecklist").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("Duplicate drug therapy",<%=data.AnswerArray("SummaryChecklist").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("Non-compliance with drug therapy",<%=data.AnswerArray("SummaryChecklist").Contains("5").ToString().ToLower()%>)+
                printview.checkbox("No change",<%=data.AnswerArray("SummaryChecklist").Contains("6").ToString().ToLower()%>)+
                printview.checkbox("Order obtained",<%=data.AnswerArray("SummaryChecklist").Contains("7").ToString().ToLower()%>)+
                printview.span(""))+
            printview.col(3,
                printview.span("Billable supplies recorded?",true)+
                printview.checkbox("Yes",<%=data.AnswerOrEmptyString("BillableSupplies").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("No",<%=data.AnswerOrEmptyString("BillableSupplies").Contains("1").ToString().ToLower()%>))+
            printview.span("Care coordination:",true)+
            printview.col(7,
                printview.checkbox("Physician",<%=data.AnswerOrEmptyString("CareCoordination").Contains("0").ToString().ToLower()%>)+
                printview.checkbox("PT",<%=data.AnswerOrEmptyString("CareCoordination").Contains("1").ToString().ToLower()%>)+
                printview.checkbox("OT",<%=data.AnswerOrEmptyString("CareCoordination").Contains("2").ToString().ToLower()%>)+
                printview.checkbox("ST",<%=data.AnswerOrEmptyString("CareCoordination").Contains("3").ToString().ToLower()%>)+
                printview.checkbox("SS",<%=data.AnswerOrEmptyString("CareCoordination").Contains("4").ToString().ToLower()%>)+
                printview.checkbox("SN",<%=data.AnswerOrEmptyString("CareCoordination").Contains("5").ToString().ToLower()%>)+
                printview.checkbox("Aide",<%=data.AnswerOrEmptyString("CareCoordination").Contains("6").ToString().ToLower()%>))+
            printview.checkbox("Other<%=data.AnswerOrEmptyString("CareCoordination").Contains("7") ? ":" + data.AnswerOrEmptyString("CareCoordiOther").Clean() : string.Empty%>",<%=data.AnswerOrEmptyString("CareCoordination").Contains("7").ToString().ToLower()%>),
            "Summary checklist");
        printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>