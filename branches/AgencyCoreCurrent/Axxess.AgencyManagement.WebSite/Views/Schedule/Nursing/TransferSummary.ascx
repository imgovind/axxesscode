﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<%var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.TransferSummary.ToString(), "transferSummary" },
    { DisciplineTasks.CoordinationOfCare.ToString(), "coordinationofcare" }
     }; %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) {
        var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
<%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
<%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
<%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
<%= Html.Hidden("Type", Model.Type)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="6">
                    <%= string.Format("{0}", Model.TypeName) %>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="6" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span></td>
                <td colspan="2"><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl%></li></ul></div><% } %></td></tr>
            <tr>
                <td colspan="3">
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    </div><div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id=Model.Type + "_VisitDate" />
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c= <%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
                <td colspan="3">
                    <div>
                        <label for="<%= Model.Type %>_Physician" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Physician", Model.PhysicianId.ToString(), new { @id = Model.Type + "_Physician", @class = "Physicians" })%></div>
                    </div>
                    <div class="clear"></div>
                    
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Functional Limitations</th>
                <th colspan="2">Patient Condition</th>
                <th colspan="2">Service(s) Provided</th></tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] functionLimitations = data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null; %><input name=Model.Type + "_FunctionLimitations" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations1' name='{1}_FunctionLimitations' value='1' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations1" class="radio">Amputation</label></td>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations5' name='{1}_FunctionLimitations' value='5' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations5" class="radio">Paralysis</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations9' name='{1}_FunctionLimitations' value='9' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations9" class="radio">Legally Blind</label></td>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations2' name='{1}_FunctionLimitations' value='2' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations6' name='{1}_FunctionLimitations' value='6' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations6" class="radio">Endurance</label></td>
                                <td><%= string.Format("<input id='{1}_FunctionLimitationsA' name='{1}_FunctionLimitations' value='A' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitationsA" class="radio">Dyspnea with Minimal Exertion</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations3' name='{1}_FunctionLimitations' value='3' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations3" class="radio">Contracture</label></td>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations7' name='{1}_FunctionLimitations' value='7' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations7" class="radio">Ambulation</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations4' name='{1}_FunctionLimitations' value='4' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations4" class="radio">Hearing</label></td>
                                <td><%= string.Format("<input id='{1}_FunctionLimitations8' name='{1}_FunctionLimitations' value='8' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitations8" class="radio">Speech</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='{1}_FunctionLimitationsB' name='{1}_FunctionLimitations' value='B' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_FunctionLimitationsB" class="radio">Other (Specify)</label> <%= Html.TextBox(Model.Type + "_FunctionLimitationsOther", data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : string.Empty, new { @id = Model.Type + "_FunctionLimitationsOther", @class = "oe" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='{1}_PatientConditionStable' name='{1}_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionStable" class="radio">Stable</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionImproved' name='{1}_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionImproved" class="radio">Improved</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_PatientConditionUnchanged' name='{1}_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionUnstable' name='{1}_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionUnstable" class="radio">Unstable</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='{1}_PatientConditionDeclined' name='{1}_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionDeclined" class="radio">Declined</label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="<%= Model.Type %>_ServiceProvided" value="" type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedSN' name='{1}_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedPT' name='{1}_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedPT" class="radio">PT</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedOT' name='{1}_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedST' name='{1}_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedST" class="radio">ST</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedMSW' name='{1}_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedHHA' name='{1}_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedHHA" class="radio">HHA</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='{1}_ServiceProvidedOther' name='{1}_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedOther" class="radio">Other</label> <%= Html.TextBox(Model.Type + "_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = Model.Type + "_ServiceProvidedOtherValue", @class = "oe" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="2">Vital Sign Ranges:</th>
                <th colspan="2">Home Bound Status</th>
                <th colspan="2">Transfer Facility Information</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <thead>
                            <tr><th></th><th>BP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BG</th></tr>
                            <tr>
                                <th>Lowest</th>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignHRMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignRespMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignTempMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignWeightMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBGMin", @class = "fill" })%></td>
                            </tr><tr>
                                <th>Highest</th>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignHRMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignRespMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignTempMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignWeightMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBGMax", @class = "fill" })%></td>
                            </tr>
                        </thead>
                    </table>
                </td>
                <td colspan="2">
                    <% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/HomeBoundStatus.ascx", Model); %>
                </td>
                <td colspan="2">
                    <label for="<%= Model.Type %>_Facility" class="float-left">Facility:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_Facility", data.ContainsKey("Facility") ? data["Facility"].Answer : string.Empty, new { @id = Model.Type + "_Facility" })%></div>
                    <div class="clear"></div>
                    <label for="<%= Model.Type %>_Phone1" class="float-left">Phone:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_Phone1", data.ContainsKey("Phone1") ? data["Phone1"].Answer : string.Empty, new { @id = Model.Type + "_Phone1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3" })%> - <%= Html.TextBox(Model.Type + "_Phone2", data.ContainsKey("Phone2") ? data["Phone2"].Answer : string.Empty, new { @id = Model.Type + "_Phone2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3" })%> - <%= Html.TextBox(Model.Type + "_Phone3", data.ContainsKey("Phone3") ? data["Phone3"].Answer : string.Empty, new { @id = Model.Type + "_Phone3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4" })%></div>
                    <div class="clear"></div>
                    <label for="<%= Model.Type %>_Contact" class="float-left">Contact:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_Contact", data.ContainsKey("Contact") ? data["Contact"].Answer : string.Empty, new { @id = Model.Type + "_Contact" })%></div>
                    <div class="clear"></div>
                    <label class="float-left">Services Providing:</label>
                    <div class="clear"></div>
                    <div class="fill"><%= Html.TextArea(Model.Type + "_ServicesProviding", data.ContainsKey("ServicesProviding") ? data["ServicesProviding"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_ServicesProviding" })%></div>
                </td>
            </tr>
            <tr>
                <th colspan="3">Summary Of Care Provided By HHA</th>
                <th colspan="3">Medicare Review</th>
            </tr>
            <tr>
                <td colspan="3">
                    <%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_SummaryOfCareProvided" })%>
                </td>
                <td colspan="3">
                    <% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/MedicareReview.ascx", Model); %>
                </td>
            </tr>
            <tr><th colspan="6">Electronic Signature</th></tr>
            <tr>
                <td colspan="6">
                    <div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="6">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Save" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Submit">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Approve">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Return">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" id="<%= Model.Type %>_Cancel" >Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#<%= Model.Type %>_Physician").PhysicianInput();
    $("#<%= Model.Type %>_MR").attr('readonly', true);
    $("#<%= Model.Type %>_EpsPeriod").attr('readonly', true);
    function TransferSummaryAdd() {
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function TransferSummaryRemove() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
    $("#<%= Model.Type %>_Save").bind('click', function() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Submit").bind('click', function() {
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Approve").bind('click', function() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Return").bind('click', function() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Cancel").bind('click', function() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
        UserInterface.CloseWindow('<%=dictonary.ContainsKey(Model.Type)? dictonary[Model.Type]:"" %>');
    });
</script>