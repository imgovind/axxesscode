﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "SixtyDaySummaryForm" })) {
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string sendAsOrder = data.ContainsKey("SendAsOrder") && data["SendAsOrder"].Answer.IsNotNullOrEmpty() ? data["SendAsOrder"].Answer : "0";%>

<%= Html.Hidden(string.Format("{0}_PatientId",Model.Type), Model.PatientId)%>
<%= Html.Hidden(string.Format("{0}_EpisodeId",Model.Type), Model.EpisodeId)%>
<%= Html.Hidden(string.Format("{0}_EventId",Model.Type), Model.EventId)%>
<%= Html.Hidden(string.Format("{0}_VisitDate",Model.Type),Model.VisitDate) %>
<%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
<%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
<%= Html.Hidden("Type", Model.Type)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="2">
                    <%=Model.TypeName%>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="2" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2" ><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span>
                    <span class="float-right">
                        <%= string.Format("<input id='{1}_SendAsOrder' class='radio' name='{1}_SendAsOrder' value='1' type='checkbox' {0} />", sendAsOrder.Contains("1").ToChecked(), Model.Type)%>Send as an order
                
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td>
                    <div>
                        <label for="<%= Model.Type %>_Physician" class="float-left">Physician:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_PhysicianId", data.ContainsKey("PhysicianId") ? data.AnswerOrEmptyString("PhysicianId") : Model.PhysicianId.ToString(), new { @id = Model.Type + "_Physician", @class = "Physicians" })%>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_DNR" class="float-left">DNR:</label>
                        <div class="float-left">
                            <%= Html.RadioButton(Model.Type+"_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = Model.Type+"_DNR1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_DNR1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = Model.Type + "_DNR2", @class = "radio" })%>
                            <label for="<%= Model.Type %>_DNR2" class="inline-radio">No</label>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Homebound Status</th>
            </tr>
            <tr>
                <td colspan="2"><%
        string[] homeboundStatus = data.ContainsKey("HomeboundStatus") && data["HomeboundStatus"].Answer != "" ? data["HomeboundStatus"].Answer.Split(',') : null; %>
                    <input name="<%=Model.Type %>_HomeboundStatus" value="" type="hidden" />
                    <div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusLeave' name='{1}_HomeboundStatus' value='2' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusLeave" class="radio normal">Exhibits considerable &#38; taxing effort to leave home</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusAssistRequired' name='{1}_HomeboundStatus' value='3' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusAssistRequired" class="radio normal">Requires the assistance of another to get up and move safely</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusDyspnea' name='{1}_HomeboundStatus' value='4' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusDyspnea" class="radio normal">Severe Dyspnea</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusUnableToLeave' name='{1}_HomeboundStatus' value='5' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusUnableToLeave" class="radio normal">Unable to safely leave home unassisted</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusUnsafeToLeave' name='{1}_HomeboundStatus' value='6' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusUnsafeToLeave" class="radio normal">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusMedReason' name='{1}_HomeboundStatus' value='7' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusMedReason" class="radio normal">Unable to leave home due to medical restriction(s)</label>
                    </div><div class="third">
                        <%= string.Format("<input id='{1}_HomeboundStatusOtherCheck' name='{1}_HomeboundStatus' value='8' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                        <label for="<%= Model.Type %>_HomeboundStatusOtherCheck" class="radio normal">Other</label>
                    </div><div class="third">
                        <%= Html.TextBox(Model.Type+"_HomeboundStatusOther", data.ContainsKey("HomeboundStatusOther") ? data["HomeboundStatusOther"].Answer : "", new { @id = Model.Type+"_HomeboundStatusOther", @class = "float-left" })%>
                    </div><div class="third">&nbsp;</div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Patient Condition</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody>
                            <tr><% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="<%=Model.Type %>_PatientCondition" value=" " type="hidden" />
                                <td><%= string.Format("<input id='{1}_PatientConditionStable' name='{1}_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionStable" class="radio">Stable</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionImproved' name='{1}_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionImproved" class="radio">Improved</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionUnchanged' name='{1}_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionUnstable' name='{1}_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionUnstable" class="radio">Unstable</label></td>
                                <td><%= string.Format("<input id='{1}_PatientConditionDeclined' name='{1}_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_PatientConditionDeclined" class="radio">Declined</label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="2">Service(s) Provided</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="<%=Model.Type %>_ServiceProvided" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedSN' name='{1}_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedPT' name='{1}_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedPT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedOT' name='{1}_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedST' name='{1}_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedMSW' name='{1}_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedHHA' name='{1}_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedHHA" class="radio">HHA</label></td>
                                <td><%= string.Format("<input id='{1}_ServiceProvidedOther' name='{1}_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_ServiceProvidedOther" class="radio">Other</label> <%= Html.TextBox(Model.Type+"_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = Model.Type+"_ServiceProvidedOtherValue", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Vital Sign Ranges</th></tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <tbody>
                            <tr><th></th><th>BP Systolic</th><th>BP Diastolic</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BS</th><th>Pain</th></tr>
                            <tr>
                                <th>Lowest</th>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPDiaMin", data.ContainsKey("VitalSignBPDiaMin") ? data["VitalSignBPDiaMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPDiaMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignHRMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignRespMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignTempMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignWeightMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBGMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignPainMin", data.ContainsKey("VitalSignPainMin") ? data["VitalSignPainMin"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignPainMin", @class = "fill" })%></td>
                            </tr><tr>
                                <th>Highest</th>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBPDiaMax", data.ContainsKey("VitalSignBPDiaMax") ? data["VitalSignBPDiaMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBPDiaMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignHRMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignRespMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignTempMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignWeightMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignBGMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox(Model.Type + "_VitalSignPainMax", data.ContainsKey("VitalSignPainMax") ? data["VitalSignPainMax"].Answer : string.Empty, new { @id = Model.Type + "_VitalSignPainMax", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Summary of Care Provided</th></tr>
            <tr><td colspan="2"><%= Html.Templates(Model.Type + "_SummaryOfCareProvidedTemplates", new { @class = "Templates", @template = "#"+Model.Type + "_SummaryOfCareProvided" })%><%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_SummaryOfCareProvided", @rows = "15" })%></td></tr>
            <tr><th colspan="2">Patient&#8217;s Current Condition</th></tr>
            <tr><td colspan="2"><%= Html.Templates(Model.Type + "_PatientCurrentConditionTemplates", new { @class = "Templates", @template = "#"+Model.Type + "_PatientCurrentCondition" })%><%= Html.TextArea(Model.Type + "_PatientCurrentCondition", data.ContainsKey("PatientCurrentCondition") ? data["PatientCurrentCondition"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_PatientCurrentCondition", @rows = "10" })%></td></tr>
            <tr><th colspan="2">Goals</th></tr>
            <tr><td colspan="2"><%= Html.Templates(Model.Type + "_GoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Goals" })%><%= Html.TextArea(Model.Type + "_Goals", data.ContainsKey("Goals") ? data["Goals"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_Goals", @rows = "10" })%></td></tr>
            <tr><th colspan="2">Recomended Services</th></tr>
            <tr>
                <td colspan="2">
                    <table class="fixed align-left">
                        <tbody><% string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %><input name="<%=Model.Type %>_RecommendedService" value="" type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceSN' name='{1}_RecommendedService' value='1' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("1") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServicePT' name='{1}_RecommendedService' value='2' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("2") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServicePT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceOT' name='{1}_RecommendedService' value='3' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("3") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceST' name='{1}_RecommendedService' value='4' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("4") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceMSW' name='{1}_RecommendedService' value='5' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("5") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceHHA' name='{1}_RecommendedService' value='6' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("6") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceHHA" class="radio">HHA</label></td>
                                <td><%= string.Format("<input id='{1}_RecommendedServiceOther' name='{1}_RecommendedService' value='7' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("7") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_RecommendedServiceOther" class="radio">Other</label> <%= Html.TextBox(Model.Type+"_RecommendedServiceOtherValue", data.ContainsKey("RecommendedServiceOtherValue") ? data["RecommendedServiceOtherValue"].Answer:string.Empty, new { @id = Model.Type+"_RecommendedServiceOtherValue", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Notifications</th></tr>
            <tr><td colspan="2">
                <div class="third"><%= string.Format("<input id='{1}_SummarySentToPhysician' name='{1}_SummarySentToPhysician' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("SummarySentToPhysician") && data["SummarySentToPhysician"] != null && data["SummarySentToPhysician"].Answer.IsNotNullOrEmpty() && data["SummarySentToPhysician"].Answer.IsEqual("Yes") ? "checked='checked'" : "", Model.Type)%><label for="<%= Model.Type %>_SummarySentToPhysician" class="radio">Summary Sent To Physician</label></div>
                <div class="third"><label for="<%= Model.Type %>_SummarySentBy">Sent By:</label><div class="float-right"><%= Html.Users(Model.Type+"_SummarySentBy", data.ContainsKey("SummarySentBy") ? data["SummarySentBy"].Answer : string.Empty, new { @id = Model.Type+"_SummarySentBy", @class = "fill" })%></div></div>
                <div class="third"><label for="<%= Model.Type %>_SummarySentDate">Date Sent:</label><div class="float-right"><input type="text" class="date-picker" name="<%=Model.Type %>_SummarySentDate" value="<%= data.ContainsKey("SummarySentDate") && data["SummarySentDate"] != null && data["SummarySentDate"].Answer.IsNotNullOrEmpty() && data["SummarySentDate"].Answer.IsValidDate() ? data["SummarySentDate"].Answer : string.Empty %>" id="<%=Model.Type %>_SummarySentDate" /></div></div>
                </td>
            </tr>
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type+"_Clinician", "", new { @id = Model.Type+"_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : string.Empty %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature2" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type+"_Clinician2", "", new { @id = Model.Type+"_Clinician2" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate2" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate2" value="<%= data.ContainsKey("SignatureDate2") && data["SignatureDate2"].Answer.IsNotNullOrEmpty() && data["SignatureDate2"].Answer.IsValidDate() ? data["SignatureDate2"].Answer : string.Empty %>" id="<%= Model.Type %>_SignatureDate2" /></div>
                    </div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignature3" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type+"_Clinician3", "", new { @id = Model.Type+"_Clinician3" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_ClinicianSignatureDate3" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate3" value="<%= data.ContainsKey("SignatureDate3") && data["SignatureDate3"].Answer.IsNotNullOrEmpty() && data["SignatureDate3"].Answer.IsValidDate() ? data["SignatureDate3"].Answer : string.Empty %>" id="<%= Model.Type %>_SignatureDate3" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove();sixtyDaySummary.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryAdd();sixtyDaySummary.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); sixtyDaySummary.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); sixtyDaySummary.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); UserInterface.CloseWindow('sixtyDaySummary');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#<%= Model.Type %>_Physician").PhysicianInput();
    $("#<%= Model.Type %>_MR").attr('readonly', true);
    $("#<%= Model.Type %>_EpsPeriod").attr('readonly', true);
    function SixtyDaySummaryAdd() {
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function SixtyDaySummaryRemove() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
    U.ShowIfSelectEquals($("#<%= Model.Type %>_NotificationDate"), "3", $("#<%= Model.Type %>_NotificationDateOther"));
    U.ShowIfChecked($("#<%= Model.Type %>_HomeboundStatusOtherCheck"), $("#<%= Model.Type %>_HomeboundStatusOther"));
    U.ShowIfChecked($("#<%= Model.Type %>_ServiceProvidedOther"), $("#<%= Model.Type %>_ServiceProvidedOtherValue"));
    U.ShowIfChecked($("#<%= Model.Type %>_RecommendedServiceOther"), $("#<%= Model.Type %>_RecommendedServiceOtherValue"));
</script>