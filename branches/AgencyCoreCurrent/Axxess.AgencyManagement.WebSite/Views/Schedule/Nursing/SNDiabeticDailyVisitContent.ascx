﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Vital Signs</th>
            <th>Cardiovascular</th>
            <th>Respiratory</th>
            <th>Diabetes</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Cardiovascular.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Respiratory.ascx", Model); %></td>
            <td rowspan="3"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DiabeticCare.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="3">Comments/Teaching</th>
        </tr>
        <tr>
            <td colspan="3"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Narrative.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">HHA PRN Supervisory Visit</th>
        </tr>
        <tr>
            <td colspan="4">
                <div class="third">
                    <label class="float-left">HHA Present:</label>
                    <div class="float-right">
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= Html.RadioButton(Model.Type + "_HhaPresent", "1", data.AnswerOrEmptyString("HhaPresent").Equals("1"), new { @id = Model.Type + "_HhaPresent1" })%>
                                    <label for="<%= Model.Type %>_HhaPresent1">Yes</label>
                                </div>
                            </li>
                            <li>
                                <div class="option">
                                    <%= Html.RadioButton(Model.Type + "_HhaPresent", "0", data.AnswerOrEmptyString("HhaPresent").Equals("0"), new { @id = Model.Type + "_HhaPresent0" })%>
                                    <label for="<%= Model.Type %>_HhaPresent0">No</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="third">
                    <label class="float-left">Aide following Care Plan:</label>
                    <div class="float-right">
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "1", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("1"), new { @id = Model.Type + "_AideFollowCarePlan1" })%>
                                    <label for="<%= Model.Type %>_AideFollowCarePlan1">Yes</label>
                                </div>
                            </li>
                            <li>
                                <div class="option">
                                    <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "0", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("0"), new { @id = Model.Type + "_AideFollowCarePlan0" })%>
                                    <label for="<%= Model.Type %>_AideFollowCarePlan0">No</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="align-left">
                    <label class="strong">Comments:</label>
                    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), 3, 20, new { @class = "fill", @id = Model.Type + "_GenericComments" })%></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    U.HideOptions();
</script>