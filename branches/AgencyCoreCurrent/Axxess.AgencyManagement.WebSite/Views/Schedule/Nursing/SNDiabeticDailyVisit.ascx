﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Diabetic Daily Visit Nursing Note | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId, new { @id = Model.Type + "_Id" })%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    Diabetic Daily Visit Nursing Note
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="2"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span></td>
                <td><span class="bigtext"></span></td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <%  var scheduledPrn = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Scheduled", Value = "Scheduled" },
                            new SelectListItem { Text = "PRN", Value = "PRN" }
                        }, "Value", "Text", data.AnswerOrDefault("ScheduledPrn", "0")); %>
                        <div class="float-right">
                            <input type="text" class="date-picker oe" name="<%= Model.Type %>_VisitDate" id="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" />
                            <%= Html.DropDownList(Model.Type + "_ScheduledPrn", scheduledPrn, new { @id = Model.Type + "_ScheduledPrn", @class = "oe" })%>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="" class="float-left">Time In:</label>
                        <div class="float-right"><input type="text" class="time-picker" name="<%= Model.Type %>_TimeIn" id="<%= Model.Type %>_TimeIn" value="<%= data.AnswerOrEmptyString("TimeIn") %>" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="" class="float-left">Time Out:</label>
                        <div class="float-right"><input type="text" class="time-picker" name="<%= Model.Type %>_TimeOut" id="<%= Model.Type %>_TimeOut" value="<%= data.AnswerOrEmptyString("TimeOut") %>" /></div>
                    </div>
                    <div class="clear"></div>
                </td>
                <td colspan="2">  
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("~/Views/Schedule/Nursing/SNDiabeticDailyVisitContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>       
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="DDVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="DDVisitRemove(); snDiabeticDailyVisit.Submit($(this));" class="autosave">Save</a></li>
            <li><a href="javascript:void(0);" onclick="DDVisitAdd(); snDiabeticDailyVisit.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="DDVisitRemove(); snDiabeticDailyVisit.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="DDVisitRemove(); snDiabeticDailyVisit.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>  
</div>
<script type="text/javascript">
    //Schedule.WarnTimeInOut("<%= Model.Type %>");
    $("#<%= Model.Type %>_PreviousNotes").change(function() {
        if ($("#<%= Model.Type %>_PreviousNotes").val().length > 0) {
            window.setTimeout(function() {
                if (confirm("Are you sure you want to load this note?")) {
                    $("#<%= Model.Type %>_ContentId").load("/Schedule/SNDiabeticDailyVisitContent", { patientId: $("#<%= Model.Type %>_PatientId").val(), noteId: $("#<%= Model.Type %>_Id").val(), previousNoteId: $("#<%= Model.Type %>_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) {
                        if (textStatus == 'error') {
                        }
                        else if (textStatus == "success") {
                            Template.OnChangeInit();
                        }
                    });
                }
            }, 0);
        } else { U.Growl("Please select a previous note first", "error"); }
    });
    U.ChangeToRadio("<%= Model.Type %>_GenericCardiovascularRadialPosition");
    U.ChangeToRadio("<%= Model.Type %>_GenericCardiovascularPedalPosition");
    U.ChangeToRadio("<%= Model.Type %>_GenericEdemaNonPittingPosition");
    U.ChangeToRadio("<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition");
    U.ChangeToRadio("<%= Model.Type %>_GenericBloodSugarSite");
    function DDVisitAdd() {
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function DDVisitRemove() {
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
</script>