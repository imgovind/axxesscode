﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitLogViewData>" %>

<style type="text/css">
    .visitverified {
        background: none;
        border: none;
        box-shadow: none;
        max-width: 800px;
        margin: 0 auto;
    }
    .visitverified legend {
        font-size: 2em;
        font-weight: bold;
    }
    .visitverified .column { padding: 0; }
    .visitverified .column.twothirds { width: 63%; padding-right: 2%; }
    .visitverified .column.onethird { width: 33%; }
    .visitverified .row { max-width: none; }
    .visitverified .row img {
        position: absolute;
        left: -20px;
    }
    .visitverified div.float-left {
        margin-left: .5em;
    }
    .visitverified .signature img {
         max-width: 100%;
         max-height: 180px;
         border: 1px solid #eee;
         display: block; 
    }
    .map {
        border: 1px solid #888;
        margin: 1em auto;
        height: 400px;
        max-width: 800px;
        background-image: url("http://maps.googleapis.com/maps/api/staticmap?scale=1&center=<%= Model.VerifiedLatitude%>,<%= Model.VerifiedLongitude %>&zoom=12&size=640x400&maptype=roadmap&markers=color:red%7C<%= Model.VerifiedLatitude%>,<%= Model.VerifiedLongitude %>&markers=color:blue%7C<%= Model.PatientAddress %>&sensor=false");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .map:hover {
        background-image: url("http://maps.googleapis.com/maps/api/staticmap?scale=1&center=<%= Model.VerifiedLatitude%>,<%= Model.VerifiedLongitude %>&zoom=13&size=640x400&maptype=roadmap&markers=color:red%7C<%= Model.VerifiedLatitude%>,<%= Model.VerifiedLongitude %>&markers=color:blue%7C<%= Model.PatientAddress %>&sensor=false");
    }
</style>

<div class="wrapper main">
    <fieldset class="visitverified">
        <legend>Verified Visit Log</legend>
        <div class="column twothirds">
            <div class="row">
                <label class="float-left">Patient: </label><div class="float-left"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="float-left">MRN: </label><div class="float-left"><%= Model.PatientMRN %></div>
            </div>
            <div class="row">
                <label class="float-left">Date of Birth: </label><div class="float-left"><%= Model.DateofBirth.ToShortDateString() %></div>
            </div>
            <div class="row">
                <label class="float-left">Start of Care: </label><div class="float-left"><%= Model.StartofCareDate.ToShortDateString() %></div>
            </div>
            <div class="row">
                <label class="float-left">Visit Type: </label><div class="float-left"><%= Model.TaskName %></div>
            </div>
            <div class="row">
                <label class="float-left">Assigned To: </label><div class="float-left"><%= Model.UserName %></div>
            </div>
            <div class="row">
                <label class="float-left">Visit Date: </label><div class="float-left"><%= Model.VisitDate %></div>
            </div>
            <div class="row">
                <label class="float-left">Verified Date:</label><div class="float-left"><%= Model.VerifiedDateTime != DateTime.MinValue ? Model.VerifiedDateTime.ToString() : string.Empty %></div>
            </div>
            <div class="row">
                <img src="/Images/gmaps-marker-blue.png" alt="Patient Address" />
                <label class="float-left">Patient Address: </label><div class="float-left"><%= Model.PatientAddress %></div>
            </div>
            <div class="row">
                <img src="/Images/gmaps-marker-red.png" alt="Verified Address" />
                <label class="float-left">Verified Address: </label><div class="float-left"><%= Model.VerifiedAddress %></div>
            </div>
        </div>
        <div class="column onethird signature">
            <b>Patient Signature</b>
            <%if(Model.PatientSignatureAssetId.IsEmpty()){%>
                  <div class="row">
                  <label> Patient unable to sign:  </label><div><%=Model.PatientUnableToSignReason %></div>
                  </div>
              <%}
              else  {%> 
                  <img alt="Patient Signature" src="/Asset/<%= Model.PatientSignatureAssetId %>" />
            <%}%>
        </div>
<!--
            <div class="row">
                <label class="float-left">Verified Lat/Long:</label><div class="float-left"><%= string.Format("{0}/{1}", Model.VerifiedLatitude, Model.VerifiedLongitude) %></div>
            </div>
            <div class="row">
                <label class="float-left">Verified Time:</label><div class="float-left"><%= Model.VerifiedDateTime %></div>
            </div>
        </div>
-->
    </fieldset>
    <div class="map"></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
    </ul></div>
</div>