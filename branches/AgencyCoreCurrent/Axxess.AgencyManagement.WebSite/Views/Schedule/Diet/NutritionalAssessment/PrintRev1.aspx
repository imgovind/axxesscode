﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Nutritional Assessment<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>%3Cbr /%3E<%=location.PhoneWorkFormatted.IsNotNullOrEmpty()?"Phone:"+location.PhoneWorkFormatted:"" %><%=location.FaxNumberFormatted.IsNotNullOrEmpty()?"|Fax:"+location.FaxNumberFormatted:"" %>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "Nutritional Assessment" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.Patient.LastName.Clean()%>, <%= Model.Patient.FirstName.Clean()%> <%= Model.Patient.MiddleInitial.Clean()%>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.Patient.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.StartDate.ToShortDateString().ToZeroFilled(), Model.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
       
       printview.addsection(
        printview.col(2,
            printview.span("Height:",true)+
            printview.span("<%=data.AnswerOrEmptyString("Height1").Clean() %>'"+"<%=data.AnswerOrEmptyString("Height2").Clean() %>")+
            printview.span("Weight:",true)+
            printview.span("<%=data.AnswerOrEmptyString("Weight").Clean() %> lbs")+
            printview.span("BMI:",true)+
            printview.span("<%=data.AnswerOrEmptyString("BMI").Clean() %>")+
            printview.span("Weight Status:<%=data.AnswerOrEmptyString("WeightStatus").Clean() %>")+
            printview.span("Past Medical History:<%=data.AnswerOrEmptyString("MedicalHistory").Clean() %>")+
            printview.span("Surgical History:<%=data.AnswerOrEmptyString("SurgicalHistory").Clean() %>")+
            printview.span("Current Diagnosis:<%=data.AnswerOrEmptyString("CurrentDiagnosis").Clean() %>")+
            printview.span("Food Allergies:<%=data.AnswerOrEmptyString("FoodAllergies").Clean() %>")+
            printview.span("Food Intolerances:<%=data.AnswerOrEmptyString("FoodIntolerances").Clean() %>")+
            printview.span("Dentition:<%=data.AnswerOrEmptyString("Dentition").Clean() %>")+
            printview.span("Chewing Ability:<%=data.AnswerOrEmptyString("ChewingAbility").Clean() %>")+
            printview.span("Swallowing Ability:<%=data.AnswerOrEmptyString("SwallowingAbility").Clean() %>")+
            printview.span("Current Diet/Texture:<%=data.AnswerOrEmptyString("CurrentDiet").Clean() %>")+
            printview.span("Liquid Consistency:<%=data.AnswerOrEmptyString("LiquidConsistency").Clean() %>")+
            printview.span("Previous Diets:<%=data.AnswerOrEmptyString("PreviousDiets").Clean() %>")+
            printview.span("Has client been counseled on diet?<%=data.AnswerOrEmptyString("HasClient").Clean() %>")+
            printview.span("Is Client taking any vitamins/minerals?<%=data.AnswerOrEmptyString("IsClient").Clean() %> <%=data.AnswerOrEmptyString("FeedingType").Clean() %>")+
            printview.span("Oral nutritional supplements?<%=data.AnswerOrEmptyString("OralNutritionalSupplements").Clean() %>")+
            printview.span("Is client receiving a tube feeding?<%=data.AnswerOrEmptyString("TubeFeeding").Clean() %>")+
            
            printview.span("Breakfast:<%=data.AnswerOrEmptyString("Breakfast").Clean() %>")+
            printview.span("Lunch:<%=data.AnswerOrEmptyString("Lunch").Clean() %>")+
            printview.span("Dinner:<%=data.AnswerOrEmptyString("Dinner").Clean() %>")+
            printview.span("Snacks:<%=data.AnswerOrEmptyString("Snacks").Clean() %>")+
            printview.span("Skin condition:<%=data.AnswerOrEmptyString("SkinCondition").Clean() %>")+
            printview.span("Stasis ulcer/location:<%=data.AnswerOrEmptyString("Stasis").Clean() %>")+
            printview.span("Diabetic ulcer/location:<%=data.AnswerOrEmptyString("Diabetic").Clean() %>")+
            printview.span("Skin Turgor:<%=data.AnswerOrEmptyString("SkinTurgor").Clean() %>")+
            printview.span("Mucous Membranes:<%=data.AnswerOrEmptyString("MucousMembranes").Clean() %>")+
            printview.span("Patient Lab Data:<%=data.AnswerOrEmptyString("PatientLabData").Clean() %>")+
            printview.span("Pertinent Medications:<%=data.AnswerOrEmptyString("PertinentMedications").Clean() %>")+
            printview.span("Estimated Caloric Needs:<%=data.AnswerOrEmptyString("EstimatedCaloricNeeds").Clean() %>")+
            printview.span("Estimated Protein Needs:<%=data.AnswerOrEmptyString("EstimatedProteinNeeds").Clean() %>")+
            printview.span("Estimated Fluid Needs:<%=data.AnswerOrEmptyString("EstimatedFluidNeeds").Clean() %>")+
            printview.span("Is current diet/TF adequate to meet caloric/protein/fluid needs?<%=data.AnswerOrEmptyString("IfAdequateToMeet").Equals("1")?"Yes":data.AnswerOrEmptyString("IfAdequateToMeet").Equals("0")?"No":"" %>")+
            printview.span("<%=data.AnswerOrEmptyString("FormulaType").Clean() %> provided by formula:<%=data.AnswerOrEmptyString("ProvidedByFormula").Clean() %>")),
                       
            ""); 
        printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("Assessment").Clean() %>",3),
            "Assessment");
        printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("Plan").Clean() %>",3),
            "Plan");
        printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
<%  }).Render(); %>
</body>
</html>