﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Medical Social Worker Progress Note<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body></body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker Progress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22octocol%22%3E%3Cspan%3E%3Cstrong%3EMR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan style=%27width:20%%27%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("Surcharge") ? data["Surcharge"].Answer.Clean() : string.Empty%>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker Progress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        printview.addsection(
            printview.col(2,
                printview.checkbox("Unchanged from Last Visit",<%= data != null && data.ContainsKey("GenericIsLivingSituationChange") && data["GenericIsLivingSituationChange"].Answer == "2" ? "true" : "false"%>) +
                printview.checkbox("Changed <%= data != null && data.ContainsKey("GenericIsLivingSituationChange") && data["GenericIsLivingSituationChange"].Answer == "1" && data.ContainsKey("GenericLivingSituationChanged") && data["GenericLivingSituationChanged"].Answer.IsNotNullOrEmpty() ? data["GenericLivingSituationChanged"].Answer.Clean() : string.Empty %>",<%= data != null && data.ContainsKey("GenericIsLivingSituationChange") && data["GenericIsLivingSituationChange"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Update to Primary Caregiver",<%= data != null && data.ContainsKey("GenericLivingSituationChanges") && data["GenericLivingSituationChanges"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericLivingSituationChanges") && data["GenericLivingSituationChanges"].Answer.Split(',').Contains("1") && data.ContainsKey("GenericUpdateToPrimaryCaregiver") && data["GenericUpdateToPrimaryCaregiver"].Answer.IsNotNullOrEmpty() ? data["GenericUpdateToPrimaryCaregiver"].Answer.Clean() : string.Empty %>",0,1) +
                printview.checkbox("Changes to Environmental Conditions",<%= data != null && data.ContainsKey("GenericLivingSituationChanges") && data["GenericLivingSituationChanges"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericLivingSituationChanges") && data["GenericLivingSituationChanges"].Answer.Split(',').Contains("2") && data.ContainsKey("GenericChangesToEnvironmentalConditions") && data["GenericChangesToEnvironmentalConditions"].Answer.IsNotNullOrEmpty() ? data["GenericChangesToEnvironmentalConditions"].Answer.Clean() : string.Empty %>",0,1)),
            "Living Situation");
        printview.addsection(
            printview.span("Mental Status",true) +
            printview.col(3,
                printview.checkbox("Alert",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Forgetful",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Confused",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Disoriented",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Oriented",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Lethargic",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Poor Short Term Memory",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Unconscious",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Cannot Determine",<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>)) +
            printview.span("Emotional Status",true) +
            printview.col(3,
                printview.checkbox("Stable",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Tearful",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Stressed",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Angry",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Sad",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Withdrawn",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Fearful",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Anxious",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Flat Affect",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericEmotionalStatus") && data["GenericEmotionalStatus"].Answer.Split(',').Contains("10") && data.ContainsKey("GenericEmotionalStatusOther") && data["GenericEmotionalStatusOther"].Answer.IsNotNullOrEmpty() ? data["GenericEmotionalStatusOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.span("<%= data != null && data.ContainsKey("GenericPsychosocialAssessment") && data["GenericPsychosocialAssessment"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialAssessment"].Answer.Clean() : string.Empty %>",0,2),
            "Psychosocial Assessment");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Adequate Support System",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Improved Client/Family Coping",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Normal Grieving Process",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Appropriate Goals for Care Set by Client/Family",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Appropriate Community Resource Referrals",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Stable Placement Setting",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Mobilization of Financial Resources",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericVisitGoals") && data["GenericVisitGoals"].Answer.Split(',').Contains("8") && data.ContainsKey("GenericVisitGoalsOther") && data["GenericVisitGoalsOther"].Answer.IsNotNullOrEmpty() ? data["GenericVisitGoalsOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.span("Visit Goal Details",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericVisitGoalDetails") && data["GenericVisitGoalDetails"].Answer.IsNotNullOrEmpty() ? data["GenericVisitGoalDetails"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Visit Interventions",true) +
            printview.col(3,
                printview.checkbox("Psychosocial Assessment",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Develop Appropriate Support System",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Counseling re Disease Process &#38; Management",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Community Resource Planning &#38; Outreach",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Counseling re Family Coping",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Stabilize Current Placement",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Crisis Intervention",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Determine/Locate Alternative Placement",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Long-range Planning &#38; Decision Making",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Financial Counseling and/or Referrals",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericPlannedInterventions") && data["GenericPlannedInterventions"].Answer.Split(',').Contains("11") && data.ContainsKey("GenericPlannedInterventionsOther") && data["GenericPlannedInterventionsOther"].Answer.IsNotNullOrEmpty() ? data["GenericPlannedInterventionsOther"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.span("Intervention Details",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericInterventionDetails") && data["GenericInterventionDetails"].Answer.IsNotNullOrEmpty() ? data["GenericInterventionDetails"].Answer.Clean() : string.Empty %>",0,2),
            "Visit Goals");
        printview.addsection(
            printview.col(2,
                printview.span("Progress Towards Goals",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericProgressTowardsGoals") && data["GenericProgressTowardsGoals"].Answer.IsNotNullOrEmpty() ? data["GenericProgressTowardsGoals"].Answer.Clean() : string.Empty %>",0,1)) +
            printview.span("<%= data != null && data.ContainsKey("GenericProgressTowardsGoalsDetails") && data["GenericProgressTowardsGoalsDetails"].Answer.IsNotNullOrEmpty() ? data["GenericProgressTowardsGoalsDetails"].Answer.Clean() : string.Empty %>",0,2) +
            printview.span("Follow-up Plan (check all that apply)",true) +
            printview.col(2,
                printview.checkbox("Follow-up Visit",<%= data != null && data.ContainsKey("GenericFollowUpPlan") && data["GenericFollowUpPlan"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Confer with Team",<%= data != null && data.ContainsKey("GenericFollowUpPlan") && data["GenericFollowUpPlan"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Provide External Referral",<%= data != null && data.ContainsKey("GenericFollowUpPlan") && data["GenericFollowUpPlan"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Discharge SW Services",<%= data != null && data.ContainsKey("GenericFollowUpPlan") && data["GenericFollowUpPlan"].Answer.Split(',').Contains("4") ? "true" : "false"%>)),
            "Progress Towards Goals"); <%
    }).Render(); %>
</html>
