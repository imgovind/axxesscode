﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<%  string[] stabs = new string[] { "Nursing", "HHA", "MSW", "Therapy", "Dietitian" }; %>
<%  if (Model.Episode != null) { %>
<div class="top">
    <div id="scheduleTop"><%  Html.RenderPartial("Calendar", new CalendarViewData { Episode = Model.Episode, PatientId = Model.Episode.PatientId, IsDischarged = Model.IsDischarged, IsNotAdmitted = Model.IsNotAdmitted, IsPending=Model.IsPending, patientStatus =Model.PatientStatus}); %></div>
    <%  if (Current.HasRight(Permissions.ScheduleVisits) && !Current.IsAgencyFrozen && !Model.IsNotAdmitted && !Model.IsPending)
        { %>
    <div id="schedule-collapsed">
        <a href="javascript:void(0);" onclick="Schedule.ShowScheduler()" class="show-scheduler">Show Scheduler</a>
    </div>
        <%  Html.Telerik().TabStrip().Name("schedule-tab-strip").ClientEvents(events => events.OnSelect("Schedule.OnSelect")).Items(tabstrip => { %>
            <%  for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
                <%  string stitle = stabs[sindex]; %>
                <%  stitle = stitle == "Dietitian" ? "Dietician" : stitle; %>
                <%  string tabname = stitle == "MSW" ? "MSW / Other" : stitle; %>
                <%  tabstrip.Add().Text(tabname).HtmlAttributes(new { id = stitle + "_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                    <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
    <%= Html.Hidden("patientId") %>
    <div class="tab-contents">
        <table id="<%= stitle %>ScheduleTable" data="<%= stitle %>" class="schedule-tables purgable">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
        <input type="hidden" name="episodeId" value="" class="scheduleValue" />
        <div class="buttons">
            <ul>
                <li><%= String.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ScheduleInputFix($(this),'Patient','#{0}ScheduleTable');\">Save</a>", stitle) %></li>
                <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
            </ul>
        </div>
    </div>
                    <%  } %>
                <%  }); %>
            <%  } %>
            <%  tabstrip.Add().Text("Orders/Care Plans").HtmlAttributes(new { id = "Orders_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
    <%= Html.Hidden("patientId") %>
    <div class="tab-contents">
        <table id="OrdersScheduleTable" data="Orders" class="schedule-tables purgable">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" />
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OrdersScheduleTable');">Save</a></li>
                <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
            </ul>
        </div>
    </div>
                <%  } %>
            <%  }); %>
            <%  tabstrip.Add().Text("Daily/Outlier").ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                <%  using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post, new { @id = " ", })) { %>
    <%= Html.Hidden("patientId", "", new { @id = "" })%>
    <div class="tab-contents">
        <table id="multiple-schedule-table" data="Multiple" class="schedule-tables">
            <thead>
                <tr>
                    <th>Task</th>
                    <th>User</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <%  var htmlAttributes = new Dictionary<string, string>(); %>
                <%  htmlAttributes.Add("id", "multipleDisciplineTask"); %>
                <%  htmlAttributes.Add("class", "MultipleDisciplineTask"); %>
                    <td><%= Html.MultipleDisciplineTasks("DisciplineTask", "", htmlAttributes)%></td>
                    <td><%= Html.Users("userId", "", new { @class = "suppliesCode Users" }) %></td>
                    <td class="date-range">
                        <input type="text" name="StartDate" value="<%= Model.Episode.StartDate.ToShortDateString() %>" mindate="<%= Model.Episode.StartDate.ToShortDateString() %>" maxdate="<%= Model.Episode.EndDate.ToShortDateString() %>" id="outlierStartDate" />
                        <span>to</span>
                        <input type="text" name="EndDate" value="<%= Model.Episode.StartDate.ToShortDateString() %>" mindate="<%= Model.Episode.StartDate.ToShortDateString() %>" maxdate="<%= Model.Episode.EndDate.ToShortDateString() %>" id="outlierEndDate" />
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="episodeId" value="" class="scheduleValue" />
        <input type="hidden" name="Discipline" value="" />
        <input type="hidden" name="IsBillable" value="" />
        <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Schedule.FormSubmitMultiple($(this));">Save</a></li>
                <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
            </ul>
        </div>
    </div>
                <%  } %>
            <%  }); %>
        <%  }).SelectedIndex(0).Render(); %>
    <%  } %>
</div>
<div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Activities", new ScheduleActivityArgument { EpisodeId = Model.Episode.Id, PatientId = Model.Episode.PatientId, Discpline = "all" }); %></div>
<script type="text/javascript">
    Schedule.positionBottom();
    $("#outlierStartDate,#outlierEndDate").DatePicker();
</script>
<%  } else { %>
    <%  if (!Model.IsDischarged) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Episodes found for this patient." })).append(
            $("<div/>", { "class": "heading" }).Buttons([
            <% if (!Current.IsAgencyFrozen) { %>
            {
                Text: "Add New Episode",
                Click: function() {
                    UserInterface.ShowNewEpisodeModal("<%= Model.PatientId.ToString() %>")
                }
            },
            <% } %>
            {
                Text: "Inactive Episodes",
                Click: function() {
                    Schedule.loadInactiveEpisodes("<%= Model.PatientId %>")
                }
            }])
        )
    );
</script>
    <%  } else { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Episodes found for this discharged patient. Re-admit the patient to create new episodes.." })
        )
    );
</script>
    <%  } %>
<%  } %>