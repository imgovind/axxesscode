﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%  string[] isVitalSigns = data.AnswerArray("isVitalSigns"); %>
<input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
<input name="<%= Model.Type %>_IsVitalSigns" value=" " type="hidden" />
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>
                Vital Sign Parameters
                &#8212;
                <%= string.Format("<input class='radio' id='{0}_IsVitalSignParameter' name='{0}_IsVitalSignParameter' value='1' type='checkbox' {1} />", Model.Type, isVitalSignParameter.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
            </th>
            <th>
                Vital Signs
                &#8212;
                <%= string.Format("<input class='radio' id='{0}_IsVitalSigns' name='{0}_IsVitalSigns' value='1' type='checkbox' {1} />", Model.Type, isVitalSigns.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_IsVitalSigns">N/A</label>
            </th>
        </tr>
        <tr>
            <td>
                <table id="<%= Model.Type %>_IsVitalSignParameterMore" class="fixed vitalsigns">
                    <tbody>
                        <tr>
                            <th></th>
                            <th>SBP</th>
                            <th>DBP</th>
                            <th>HR</th>
                            <th>Resp</th>
                            <th>Temp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <th>greater than (&#62;)</th>
                            <td><%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.AnswerOrEmptyString("SystolicBPGreaterThan"), new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.AnswerOrEmptyString("DiastolicBPGreaterThan"), new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.AnswerOrEmptyString("PulseGreaterThan"), new { @id = Model.Type + "_PulseGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.AnswerOrEmptyString("RespirationGreaterThan"), new { @id = Model.Type + "_RespirationGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_TempGreaterThan", data.AnswerOrEmptyString("TempGreaterThan"), new { @id = Model.Type + "_TempGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.AnswerOrEmptyString("WeightGreaterThan"), new { @id = Model.Type + "_WeightGreaterThan", @class = "fill" })%></td>
                        </tr>
                        <tr>
                            <th>less than (&#60;)</th>
                            <td><%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.AnswerOrEmptyString("SystolicBPLessThan"), new { @id = Model.Type + "_SystolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.AnswerOrEmptyString("DiastolicBPLessThan"), new { @id = Model.Type + "_DiastolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_PulseLessThan", data.AnswerOrEmptyString("PulseLessThan"), new { @id = Model.Type + "_PulseLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_RespirationLessThan", data.AnswerOrEmptyString("RespirationLessThan"), new { @id = Model.Type + "_RespirationLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_TempLessThan", data.AnswerOrEmptyString("TempLessThan"), new { @id = Model.Type + "_TempLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_WeightLessThan", data.AnswerOrEmptyString("WeightLessThan"), new { @id = Model.Type + "_WeightLessThan", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table id="<%= Model.Type %>_IsVitalSignsMore" class="fixed vitalsignparameter">
                    <tbody>
                        <tr>
                            <th>BP</th>
                            <th>HR</th>
                            <th>Temp</th>
                            <th>Resp</th>
                            <th>Weight</th>
                        </tr>
                        <tr>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignBPVal", data.AnswerOrEmptyString("VitalSignBPVal"), new { @id = Model.Type + "_VitalSignBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignHRVal", data.AnswerOrEmptyString("VitalSignHRVal"), new { @id = Model.Type + "_VitalSignHRVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignTempVal", data.AnswerOrEmptyString("VitalSignTempVal"), new { @id = Model.Type + "_VitalSignTempVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignRespVal", data.AnswerOrEmptyString("VitalSignRespVal"), new { @id = Model.Type + "_VitalSignRespVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_VitalSignWeightVal", data.AnswerOrEmptyString("VitalSignWeightVal"), new { @id = Model.Type + "_VitalSignWeightVal", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Tasks</th>
        </tr>
        <tr>
            <td>
                <table class="fixed">
                    <thead>
                        <tr>
                            <th colspan="3" class="align-left">Assignment</th>
                            <th colspan="3">Status</th>
                        </tr>
                        <tr>
                            <th colspan="3" class="align-left">Personal Care</th>
                            <th>Completed</th>
                            <th>Refuse</th>
                            <th>N/A</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Bed Bath<%= Html.Hidden(Model.Type + "_PersonalCareBedBath", " ", new { @id = Model.Type + "_PersonalCareBedBathHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "2", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("2"), new { @id = Model.Type + "_PersonalCareBedBath2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "1", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("1"), new { @id = Model.Type + "_PersonalCareBedBath1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "0", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("0"), new { @id = Model.Type + "_PersonalCareBedBath0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Chair Bath<%= Html.Hidden(Model.Type + "_PersonalCareAssistWithChairBath", " ", new { @id = Model.Type + "_PersonalCareAssistWithChairBathHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "2", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "1", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "0", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Tub Bath<%= Html.Hidden(Model.Type + "_PersonalCareTubBath", " ", new { @id = Model.Type + "_PersonalCareTubBathHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "2", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("2"), new { @id = Model.Type + "_PersonalCareTubBath2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "1", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("1"), new { @id = Model.Type + "_PersonalCareTubBath1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "0", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("0"), new { @id = Model.Type + "_PersonalCareTubBath0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Shower<%= Html.Hidden(Model.Type + "_PersonalCareShower", " ", new { @id = Model.Type + "_PersonalCareShowerHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "2", data.AnswerOrEmptyString("PersonalCareShower").Equals("2"), new { @id = Model.Type + "_PersonalCareShower2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "1", data.AnswerOrEmptyString("PersonalCareShower").Equals("1"), new { @id = Model.Type + "_PersonalCareShower1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "0", data.AnswerOrEmptyString("PersonalCareShower").Equals("0"), new { @id = Model.Type + "_PersonalCareShower0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Shower w/Chair<%= Html.Hidden(Model.Type + "_PersonalCareShowerWithChair", " ", new { @id = Model.Type + "_PersonalCareShowerWithChairHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "2", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("2"), new { @id = Model.Type + "_PersonalCareShowerWithChair2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "1", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("1"), new { @id = Model.Type + "_PersonalCareShowerWithChair1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "0", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("0"), new { @id = Model.Type + "_PersonalCareShowerWithChair0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Shampoo Hair<%= Html.Hidden(Model.Type + "_PersonalCareShampooHair", " ", new { @id = Model.Type + "_PersonalCareShampooHairHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "2", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("2"), new { @id = Model.Type + "_PersonalCareShampooHair2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "1", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("1"), new { @id = Model.Type + "_PersonalCareShampooHair1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "0", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("0"), new { @id = Model.Type + "_PersonalCareShampooHair0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Hair Care/Comb Hair<%= Html.Hidden(Model.Type + "_PersonalCareHairCare", " ", new { @id = Model.Type + "_PersonalCareHairCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "2", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("2"), new { @id = Model.Type + "_PersonalCareHairCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "1", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("1"), new { @id = Model.Type + "_PersonalCareHairCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "0", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("0"), new { @id = Model.Type + "_PersonalCareHairCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Oral Care<%= Html.Hidden(Model.Type + "_PersonalCareOralCare", " ", new { @id = Model.Type + "_PersonalCareOralCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "2", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("2"), new { @id = Model.Type + "_PersonalCareOralCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "1", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("1"), new { @id = Model.Type + "_PersonalCareOralCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "0", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("0"), new { @id = Model.Type + "_PersonalCareOralCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Skin Care<%= Html.Hidden(Model.Type + "_PersonalCareSkinCare", " ", new { @id = Model.Type + "_PersonalCareSkinCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "2", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("2"), new { @id = Model.Type + "_PersonalCareSkinCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "1", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("1"), new { @id = Model.Type + "_PersonalCareSkinCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "0", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("0"), new { @id = Model.Type + "_PersonalCareSkinCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Pericare<%= Html.Hidden(Model.Type + "_PersonalCarePericare", " ", new { @id = Model.Type + "_PersonalCarePericareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "2", data.AnswerOrEmptyString("PersonalCarePericare").Equals("2"), new { @id = Model.Type + "_PersonalCarePericare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "1", data.AnswerOrEmptyString("PersonalCarePericare").Equals("1"), new { @id = Model.Type + "_PersonalCarePericare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "0", data.AnswerOrEmptyString("PersonalCarePericare").Equals("0"), new { @id = Model.Type + "_PersonalCarePericare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Nail Care<%= Html.Hidden(Model.Type + "_PersonalCareNailCare", " ", new { @id = Model.Type + "_PersonalCareNailCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "2", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("2"), new { @id = Model.Type + "_PersonalCareNailCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "1", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("1"), new { @id = Model.Type + "_PersonalCareNailCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "0", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("0"), new { @id = Model.Type + "_PersonalCareNailCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Shave<%= Html.Hidden(Model.Type + "_PersonalCareShave", " ", new { @id = Model.Type + "_PersonalCareShaveHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "2", data.AnswerOrEmptyString("PersonalCareShave").Equals("2"), new { @id = Model.Type + "_PersonalCareShave2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "1", data.AnswerOrEmptyString("PersonalCareShave").Equals("1"), new { @id = Model.Type + "_PersonalCareShave1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "0", data.AnswerOrEmptyString("PersonalCareShave").Equals("0"), new { @id = Model.Type + "_PersonalCareShave0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Dressing<%= Html.Hidden(Model.Type + "_PersonalCareAssistWithDressing", " ", new { @id = Model.Type + "_PersonalCareAssistWithDressingHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "2", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithDressing2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "1", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithDressing1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "0", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithDressing0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <th colspan="3" class="align-left">Nutrition</th>
                            <th>Completed</th>
                            <th>Refuse</th>
                            <th>N/A</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Meal Set-up<%= Html.Hidden(Model.Type + "_NutritionMealSetUp", " ", new { @id = Model.Type + "_NutritionMealSetUpHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "2", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("2"), new { @id = Model.Type + "_NutritionMealSetUp2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "1", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("1"), new { @id = Model.Type + "_NutritionMealSetUp1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "0", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("0"), new { @id = Model.Type + "_NutritionMealSetUp0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Feeding<%= Html.Hidden(Model.Type + "_NutritioAssistWithFeeding", " ", new { @id = Model.Type + "_NutritioAssistWithFeedingHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "2", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("2"), new { @id = Model.Type + "_NutritioAssistWithFeeding2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "1", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("1"), new { @id = Model.Type + "_NutritioAssistWithFeeding1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "0", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("0"), new { @id = Model.Type + "_NutritioAssistWithFeeding0", @class = "radio" })%></td>
                        </tr>
                    </thead>
                </table>
            </td>
            <td>
                <table class="fixed">
                    <thead>
                        <tr>
                            <th colspan="3" class="align-left">Assignment</th>
                            <th colspan="3">Status</th>
                        </tr>
                        <tr>
                            <th colspan="3" class="align-left">Elimination</th>
                            <th>Completed</th>
                            <th>Refuse</th>
                            <th>N/A</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Bed Pan/Urinal<%= Html.Hidden(Model.Type + "_EliminationAssistWithBedPan", " ", new { @id = Model.Type + "_EliminationAssistWithBedPanHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("2"), new { @id = Model.Type + "_EliminationAssistWithBedPan2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "1", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("1"), new { @id = Model.Type + "_EliminationAssistWithBedPan1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "0", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("0"), new { @id = Model.Type + "_EliminationAssistWithBedPan0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with BSC<%= Html.Hidden(Model.Type + "_EliminationAssistBSC", " ", new { @id = Model.Type + "_EliminationAssistBSCHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "2", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("2"), new { @id = Model.Type + "_EliminationAssistBSC2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "1", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("1"), new { @id = Model.Type + "_EliminationAssistBSC1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "0", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("0"), new { @id = Model.Type + "_EliminationAssistBSC0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Incontinence Care<%= Html.Hidden(Model.Type + "_EliminationIncontinenceCare", " ", new { @id = Model.Type + "_EliminationIncontinenceCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "2", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("2"), new { @id = Model.Type + "_EliminationIncontinenceCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "1", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("1"), new { @id = Model.Type + "_EliminationIncontinenceCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "0", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("0"), new { @id = Model.Type + "_EliminationIncontinenceCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Empty Drainage Bag<%= Html.Hidden(Model.Type + "_EliminationEmptyDrainageBag", " ", new { @id = Model.Type + "_EliminationEmptyDrainageBagHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "2", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("2"), new { @id = Model.Type + "_EliminationEmptyDrainageBag2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "1", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("1"), new { @id = Model.Type + "_EliminationEmptyDrainageBag1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "0", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("0"), new { @id = Model.Type + "_EliminationEmptyDrainageBag0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Record Bowel Movement<%= Html.Hidden(Model.Type + "_EliminationRecordBowelMovement", " ", new { @id = Model.Type + "_EliminationRecordBowelMovementHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "2", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("2"), new { @id = Model.Type + "_EliminationRecordBowelMovement2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "1", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("1"), new { @id = Model.Type + "_EliminationRecordBowelMovement1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "0", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("0"), new { @id = Model.Type + "_EliminationRecordBowelMovement0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Catheter Care<%= Html.Hidden(Model.Type + "_EliminationCatheterCare", " ", new { @id = Model.Type + "_EliminationCatheterCareHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "2", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("2"), new { @id = Model.Type + "_EliminationCatheterCare2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "1", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("1"), new { @id = Model.Type + "_EliminationCatheterCare1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "0", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("0"), new { @id = Model.Type + "_EliminationCatheterCare0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <th colspan="3" class="align-left">Activity</th>
                            <th>Completed</th>
                            <th>Refuse</th>
                            <th>N/A</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Dangle on Side of Bed<%= Html.Hidden(Model.Type + "_ActivityDangleOnSideOfBed", " ", new { @id = Model.Type + "_ActivityDangleOnSideOfBedHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "2", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("2"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "1", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("1"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "0", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("0"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Turn &#38; Position<%= Html.Hidden(Model.Type + "_ActivityTurnPosition", " ", new { @id = Model.Type + "_ActivityTurnPositionHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "2", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("2"), new { @id = Model.Type + "_ActivityTurnPosition2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "1", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("1"), new { @id = Model.Type + "_ActivityTurnPosition1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "0", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("0"), new { @id = Model.Type + "_ActivityTurnPosition0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Transfer<%= Html.Hidden(Model.Type + "_ActivityAssistWithTransfer", " ", new { @id = Model.Type + "_ActivityAssistWithTransferHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "2", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithTransfer2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "1", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithTransfer1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "0", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithTransfer0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Assist with Ambulation<%= Html.Hidden(Model.Type + "_ActivityAssistWithAmbulation", " ", new { @id = Model.Type + "_ActivityAssistWithAmbulationHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "2", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithAmbulation2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "1", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithAmbulation1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "0", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithAmbulation0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Range of Motion<%= Html.Hidden(Model.Type + "_ActivityRangeOfMotion", " ", new { @id = Model.Type + "_ActivityRangeOfMotionHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "2", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("2"), new { @id = Model.Type + "_ActivityRangeOfMotion2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "1", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("1"), new { @id = Model.Type + "_ActivityRangeOfMotion1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "0", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("0"), new { @id = Model.Type + "_ActivityRangeOfMotion0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <th colspan="3" class="align-left">Household Task</th>
                            <th>Completed</th>
                            <th>Refuse</th>
                            <th>N/A</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Make Bed<%= Html.Hidden(Model.Type + "_HouseholdTaskMakeBed", " ", new { @id = Model.Type + "_HouseholdTaskMakeBedHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "2", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("2"), new { @id = Model.Type + "_HouseholdTaskMakeBed2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "1", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("1"), new { @id = Model.Type + "_HouseholdTaskMakeBed1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "0", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("0"), new { @id = Model.Type + "_HouseholdTaskMakeBed0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Change Linen<%= Html.Hidden(Model.Type + "_HouseholdTaskChangeLinen", " ", new { @id = Model.Type + "_HouseholdTaskChangeLinenHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "2", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("2"), new { @id = Model.Type + "_HouseholdTaskChangeLinen2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "1", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("1"), new { @id = Model.Type + "_HouseholdTaskChangeLinen1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "0", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("0"), new { @id = Model.Type + "_HouseholdTaskChangeLinen0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Light Housekeeping<%= Html.Hidden(Model.Type + "_HouseholdTaskLightHousekeeping", " ", new { @id = Model.Type + "_HouseholdTaskLightHousekeepingHidden" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "2", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("2"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping2", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "1", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("1"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping1", @class = "radio" })%></td>
                            <td><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "0", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("0"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping0", @class = "radio" })%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="align-left">Other (Describe):</td>
                            <td colspan="3"><%= Html.TextBox(Model.Type + "_HouseholdTaskOther", data.AnswerOrEmptyString("HouseholdTaskOther"), new { @id = Model.Type + "_HouseholdTaskOther", @class = "fill" })%></td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Comments</th>
        </tr>
        <tr>
            <td colspan="2"><%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "fill" })%></td>
        </tr>
    </tbody>    
</table>