﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<span class="wintitle">Visit Log | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("EditVisitLog", "Schedule", FormMethod.Post, new { @id = "newVisitLogForm" })) { %>
<%= Html.Hidden("StartDate", Model[0].StartDate, new { disabled = "disabled" }) %>
<%= Html.Hidden("EndDate", Model[0].EndDate, new { disabled = "disabled" }) %>
<%= Html.Hidden("PatientId", Model[0].PatientId, new { disabled = "disabled" }) %>
<%= Html.Hidden("EpisodeId", Model[0].EpisodeId, new { disabled = "disabled" })%>
<div class="wrapper main">
    <fieldset>
    <div class="column">
        <div class="row">
            <span class="strong">Patient: <%= Model[0].PatientName %></span>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <span class="strong">Episode: <%= Model[0].StartDate.ToZeroFilled()%> -  <%= Model[0].EndDate.ToZeroFilled()%></span>
        </div>
    </div>
    <table class="form">
         <tr>
            <td><label class="fill"><strong>Task:</strong></label></td>
            <td><label class="fill"><strong>Task Date:</strong></label></td>
            <td><label class="fill"><strong>Assigned To</strong></label></td>
            <td><label class="fill"><strong>Status</strong></label></td>
            <td><label class="fill"><strong>Visit Date</strong></label></td>
            <td><label class="fill"><strong>Time In:</strong></label></td>
            <td><label class="fill"><strong>Time Out:</strong></label></td>
        </tr>
    
    <% if(Model[0].EventDate.IsNotNullOrEmpty()){
         for (int i = 0; i < Model.Count; i++)
          { %>
            <%= Html.Hidden( "[" + i + "]."+"PatientId", Model[0].PatientId,  new { @id = "VisitLog_PatientId" , @class="patientid" })%>
            <%= Html.Hidden( "[" + i + "]."+"EpisodeId", Model[0].EpisodeId, new { @id = "VisitLog_EpisodeId" })%>
            <%= Html.Hidden("[" + i + "]." + "EventId", Model[i].EventId, new { @id = "VisitLog_EventId" })%>
        <tr>
            <td><span id="VisitLog_TaskName_<%= i %>" name="[<%= i%>].TaskName"><%= Model[i].DisciplineTaskName != null ? Model[i].DisciplineTaskName  : string.Empty%></span></td>
            <td><span id="VisitLog_TaskDate_<%= i %>"  class="width100" name="[<%= i%>].TaskDate"><%= Model[i].EventDate != null ? Model[i].EventDate : string.Empty%></span></td>
            <td><span id="VisitLog_UserName_<%= i %>" name="[<%= i%>].UserName"><%= Model[i].UserName != null ? Model[i].UserName : string.Empty%></span></td>
            <td><%= Html.Status("[" + i + "]." + "Status", Model[i].Status, Model[i].DisciplineTask, Model[i].EventDate.ToDateTime(), new { @id = "VisitLog_Status_" + i, @style = "width:130px" ,@class="status"})%></td>
            <td><input type="text"  style="width:100px" id="VisitLog_VisitDate_<%= i %>" class="date-picker required visitdate" name="[<%= i%>].VisitDate" value="<%= Model[i].VisitDate %>" /></td>
            <td><input type="text" style="width:120px" size="10" id="VisitLog_TimeIn_<%= i %>" name="[<%= i%>].TimeIn" class="time-picker fill tasktimein" value="<%= Model[i].TimeIn %>" /></td>
            <td><input type="text" style="width:120px" size="10" id="VisitLog_TimeOut_<%= i %>" name="[<%= i %>].TimeOut" class="time-picker fill tasktimeout" value="<%= Model[i].TimeOut %>" /></td>
        </tr>
        <% }
         } %>
    </table>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" class="save close" id="savebutton">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div> 
<%  } %>
<script type="text/javascript">
    $("#savebutton").click(function() {
        Schedule.loadCalendarAndActivities("<%=Model[0].PatientId %>","<%=Model[0].EpisodeId %>");
    });
</script>


