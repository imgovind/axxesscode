﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PatientProfile>" %><%
var assessment = Model.CurrentAssessment != null ? Model.CurrentAssessment.ToDictionary() : new Dictionary<string, Question>();
String[] ethnicities = Model.Patient.Ethnicities != null && Model.Patient.Ethnicities != "" ? Model.Patient.Ethnicities.Split(';') : null;
String race = string.Empty;
if (ethnicities != null) {
    foreach(String ethnic in ethnicities) {
        int result;
        if (Int32.TryParse(ethnic, out result)) race += ((Race)Enum.ToObject(typeof(Race), (result))).GetDescription() + " ";
    }
}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Patient Profile<%= Model.Patient != null ? " | " + Model.Patient.LastName.ToLastNameTitleCase() + ", " + (Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/profile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(5)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToLastNameTitleCase().Clean() + ", " : string.Empty) + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase().Clean() + " " : string.Empty) + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper().Clean() : string.Empty) : string.Empty %>",
            "patient": "<%= Model != null && Model.Patient != null ? (Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase().Clean() : string.Empty) + (Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase().Clean() + "<br/>" : "<br/>") + (Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase().Clean() + ", " : string.Empty) + (Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode.ToString().ToUpper().Clean() + " " : string.Empty) + (Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode.Clean() : string.Empty) + (Model.Patient.PhoneHome.IsNotNullOrEmpty() ? "<br/>Phone: " + Model.Patient.PhoneHome.ToPhone().Clean() : string.Empty) : string.Empty %>",
            "ssn": "<%= Model != null && Model.Patient != null && Model.Patient.SSN.IsNotNullOrEmpty() ? Model.Patient.SSN.Clean() : string.Empty %>",
            "dnr": "<%= Model != null && Model.Patient != null && Model.Patient.IsDNR ? "Yes" : "No"%>",
            "id": "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>",
            "mcare": "<%= Model != null && Model.Patient != null && Model.Patient.MedicareNumber.IsNotNullOrEmpty() ? Model.Patient.MedicareNumber.Clean() : string.Empty %>",
            "soc": "<%= Model != null && Model.Patient != null && Model.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? Model.Patient.StartOfCareDateFormatted.Clean() : string.Empty %>",
            "bday": "<%= Model != null && Model.Patient != null && Model.Patient.DOBFormatted.IsNotNullOrEmpty() ? Model.Patient.DOBFormatted.Clean() : string.Empty %>",
            "sex": "<%= Model != null && Model.Patient != null && Model.Patient.Gender.IsNotNullOrEmpty() ? Model.Patient.Gender.Clean() : string.Empty %>",
            "marital": "<%= Model != null && Model.Patient != null && Model.Patient.MaritalStatus.IsNotNullOrEmpty() ? Model.Patient.MaritalStatus.Clean() : string.Empty %>",
            "office": "<%= Model != null && Model.Agency != null && location != null && location.Name.IsNotNullOrEmpty() ? location.Name.Clean() : string.Empty %>",
            "race": "<%= race.Clean() %>",
            "height": "<%= string.Format("{0} {1}", Model != null && Model.Patient != null ? Model.Patient.Height.ToString() : string.Empty, Model != null && Model.Patient != null ? (Model.Patient.HeightMetric == 0 ? "in" : (Model.Patient.HeightMetric == 1 ? "cm" : string.Empty)) : string.Empty)%>",
            "weight": "<%= string.Format("{0} {1}", Model != null && Model.Patient != null ? Model.Patient.Weight.ToString() : string.Empty, Model != null && Model.Patient != null ? (Model.Patient.WeightMetric == 0 ? "lb" : (Model.Patient.WeightMetric == 1 ? "kg" : string.Empty)) : string.Empty)%>",
            "cert": "<%= Model != null && Model.CurrentEpisode != null && Model.CurrentEpisode.StartDateFormatted.IsNotNullOrEmpty() && Model.CurrentEpisode.EndDateFormatted.IsNotNullOrEmpty() ? Model.CurrentEpisode.StartDateFormatted.Clean() + " - " + Model.CurrentEpisode.EndDateFormatted.Clean() : string.Empty %>",
            "triage": "<%= Model != null && Model.Patient != null ? (Model.Patient.Triage == 1 ? "1. Life threatening (or potential) and requires ongoing medical treatment." : string.Empty) + (Model.Patient.Triage == 2 ? "2. Not life threatening but would suffer severe adverse effects if visit postponed." : string.Empty) + (Model.Patient.Triage == 3 ? "3. Visits could be postponed 24-48 hours without adverse effects." : string.Empty) + (Model.Patient.Triage == 4 ? "4. Visits could be postponed 72-96 hours without adverse effects." : string.Empty) : string.Empty%>",
            "refdate": "<%= Model != null && Model.Patient != null && Model.Patient.ReferralDate.IsValid() ? Model.Patient.ReferralDate.ToShortDateString().Clean() : string.Empty %>",
            "priins": "<%= Model != null && Model.Patient != null && Model.Patient.PrimaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.PrimaryInsuranceName.Clean() : string.Empty %>",
            "priinsnum": "<%= Model != null && Model.Patient != null && Model.Patient.PrimaryHealthPlanId.IsNotNullOrEmpty() ? Model.Patient.PrimaryHealthPlanId.Clean() : string.Empty%>",
            "secins": "<%= Model != null && Model.Patient != null && Model.Patient.SecondaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.SecondaryInsuranceName.Clean() : string.Empty %>",
            "secinsnum": "<%= Model != null && Model.Patient != null && Model.Patient.SecondaryHealthPlanId.IsNotNullOrEmpty() ? Model.Patient.SecondaryHealthPlanId.Clean() : string.Empty%>",
            "terins": "<%= Model != null && Model.Patient != null && Model.Patient.TertiaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.TertiaryInsuranceName.Clean() : string.Empty %>",
            "terinsnum": "<%= Model != null && Model.Patient != null && Model.Patient.TertiaryHealthPlanId.IsNotNullOrEmpty() ? Model.Patient.TertiaryHealthPlanId.Clean() : string.Empty%>",
            "ecname": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 0 && Model.Patient.EmergencyContacts[0].DisplayName.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[0].DisplayName.Clean() : string.Empty %>",
            "ecphone": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 0 && Model.Patient.EmergencyContacts[0].PrimaryPhone.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[0].PrimaryPhone.ToPhone().Clean() : string.Empty %>",
            "ecralation": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 0 && Model.Patient.EmergencyContacts[0].Relationship.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[0].Relationship.Clean() : string.Empty %>",
            "ecname2": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 1 && Model.Patient.EmergencyContacts[1].DisplayName.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[1].DisplayName.Clean() : string.Empty %>",
            "ecphone2": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 1 && Model.Patient.EmergencyContacts[1].PrimaryPhone.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[1].PrimaryPhone.ToPhone().Clean() : string.Empty %>",
            "ecralation2": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 1 && Model.Patient.EmergencyContacts[1].Relationship.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[1].Relationship.Clean() : string.Empty %>",
            "ecname3": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 2 && Model.Patient.EmergencyContacts[2].DisplayName.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[2].DisplayName.Clean() : string.Empty %>",
            "ecphone3": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 2 && Model.Patient.EmergencyContacts[2].PrimaryPhone.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[2].PrimaryPhone.ToPhone().Clean() : string.Empty %>",
            "ecralation3": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 2 && Model.Patient.EmergencyContacts[2].Relationship.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[2].Relationship.Clean() : string.Empty %>",
            "ecname4": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 3 && Model.Patient.EmergencyContacts[3].DisplayName.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[3].DisplayName.Clean() : string.Empty %>",
            "ecphone4": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 3 && Model.Patient.EmergencyContacts[3].PrimaryPhone.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[3].PrimaryPhone.ToPhone().Clean() : string.Empty %>",
            "ecralation4": "<%= Model != null && Model.Patient != null && Model.Patient.EmergencyContacts.Count > 3 && Model.Patient.EmergencyContacts[3].Relationship.IsNotNullOrEmpty() ? Model.Patient.EmergencyContacts[3].Relationship.Clean() : string.Empty %>",
            "allergies": "<%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies.Clean() : string.Empty%>",
            "pharm": "<%= Model != null && Model.Pharmacy != null && Model.Pharmacy.Name.IsNotNullOrEmpty() ? Model.Pharmacy.Name.Clean() : string.Empty%>",
            "pharmphone": "<%= Model != null && Model.Pharmacy != null && Model.Pharmacy.Phone.IsNotNullOrEmpty() ? Model.Pharmacy.Phone.ToPhone().Clean() : string.Empty%>",
            "pridiag": "<%= assessment != null && assessment.ContainsKey("M1020PrimaryDiagnosis") && assessment["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? assessment["M1020PrimaryDiagnosis"].Answer.Clean() : string.Empty %>",
            "secdiag": "<%= assessment != null && assessment.ContainsKey("M1022PrimaryDiagnosis1") && assessment["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? assessment["M1022PrimaryDiagnosis1"].Answer.Clean() : string.Empty %>",
            "clinician": "<%= Model != null && Model.Clinician.IsNotNullOrEmpty() ? Model.Clinician.Clean() : string.Empty %>",
            "caseman": "<%= Model != null && Model.Patient != null && Model.Patient.CaseManagerName.IsNotNullOrEmpty() ? Model.Patient.CaseManagerName.Clean() : string.Empty %>",
            "startofcare": "<%= Model != null && Model.Patient != null && Model.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? Model.Patient.StartOfCareDateFormatted.Clean() : string.Empty %>",
            "freq": "<%= Model != null && Model.Frequencies.IsNotNullOrEmpty() ? Model.Frequencies.Clean() : string.Empty %>",
            "phys": "<%= Model != null && Model.Physician != null && Model.Physician.DisplayName.IsNotNullOrEmpty() ? Model.Physician.DisplayName.Clean() : string.Empty %>",
            "physaddress": "<%= Model != null && Model.Physician != null ? (Model.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Physician.AddressLine1.ToTitleCase().Clean() : string.Empty) + (Model.Physician.AddressLine2.IsNotNullOrEmpty() ? Model.Physician.AddressLine2.ToTitleCase().Clean() + "<br/>" : "<br/>") + (Model.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Physician.AddressCity.ToTitleCase().Clean() + ", " : string.Empty) + (Model.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Physician.AddressStateCode.ToString().ToUpper().Clean() + "  " : string.Empty) + (Model.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Physician.AddressZipCode.Clean() : string.Empty) : string.Empty %>",
            "physphone": "<%= Model != null && Model.Physician != null && Model.Physician.PhoneWork.IsNotNullOrEmpty() ? Model.Physician.PhoneWork.ToPhone().Clean() : string.Empty %>",
            "physfax": "<%= Model != null && Model.Physician != null && Model.Physician.FaxNumber.IsNotNullOrEmpty() ? Model.Physician.FaxNumber.ToPhone().Clean() : string.Empty %>",
            "physnpi": "<%= Model != null && Model.Physician != null && Model.Physician.NPI.IsNotNullOrEmpty() ? Model.Physician.NPI.Clean() : string.Empty %>",
            
<%  var serviceText = string.Empty; %>
<%  string[] servicesRequired = Model != null && Model.Patient != null && Model.Patient.ServicesRequired.IsNotNullOrEmpty() ? Model.Patient.ServicesRequired.Split(';') : null; %>
<%  if (servicesRequired != null && servicesRequired.Count() > 0)
    { %>
    <%  foreach (var service in servicesRequired) { %>
        <%  switch (service) { %>
            <% case "0": serviceText += "SNV, "; break; %>
            <% case "1": serviceText += "HHA, "; break; %>
            <% case "2": serviceText += "PT, "; break; %>
            <% case "3": serviceText += "OT, "; break; %>
            <% case "4": serviceText += "ST, "; break; %>
            <% case "5": serviceText += "MSW, "; break; %>
        <%  } %>
    <%  } %>
    <%  if (serviceText.Length > 0) serviceText = serviceText.Substring(0, serviceText.Length - 2); %>
<%  } %>
            "services": "<%= serviceText.Clean() %>",
<%  var dmeText = string.Empty; %>
<%  string[] DME = Model != null && Model.Patient != null && Model.Patient.DME.IsNotNullOrEmpty() ? Model.Patient.DME.Split(';') : null; %>
<%  if (DME != null && DME.Count() > 0) { %>
    <%  foreach (var dme in DME) { %>
        <%  switch (dme) { %>
            <% case "0": dmeText += "Bedside Commode, "; break; %>
            <% case "1": dmeText += "Cane, "; break; %>
            <% case "2": dmeText += "Elevated Toilet Seat, "; break; %>
            <% case "3": dmeText += "Grab Bars, "; break; %>
            <% case "4": dmeText += "Hospital Bed, "; break; %>
            <% case "5": dmeText += "Nebulizer, "; break; %>
            <% case "6": dmeText += "Oxygen, "; break; %>
            <% case "7": dmeText += "Tub/Shower Bench, "; break; %>
            <% case "8": dmeText += "Walker, "; break; %>
            <% case "9": dmeText += "Wheelchair, "; break; %>
            <% case "10": dmeText += "Other, "; break; %>
        <%  } %>
    <%  } %>
    <%  if (dmeText.Length > 0) dmeText = dmeText.Substring(0, dmeText.Length - 2); %>
<%  } %>
            "dme": "<%= dmeText.Clean()%>"
        };
        PdfPrint.BuildBasic("<%= Model != null && Model.Patient != null && Model.Patient.Comments.IsNotNullOrEmpty() ? Model.Patient.Comments.Clean() : string.Empty %>"); <%
    }).Render(); %>
</body>
</html>