﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<span class="wintitle">List Patients | <%= Current.AgencyName %></span>
<div class="wrapper grid-bg">
    <div class="buttons">
        <ul class="float-right">
            <li><%= Html.ActionLink("Export to Excel", "Patients", "Export", new { BranchId = Guid.Empty, Status = 1 }, new { @id = "List_Patient_ExportLink", @class = "excel" })%></li>
        </ul>
    </div>
    <fieldset class="orders-filter">
    
        <%  var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]); %>
        <div  class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="Patient.LoadPateintListContent('#Patient_List_MainContainer','#List_Patient_BranchId','#List_Patient_Status','<%= sortParams %>');">Generate</a></li>
            <% if(!Current.IsAgencyFrozen) { %>
                <li><a href="javascript:void(0);" onclick="UserInterface.ShowNewPatient();">New Patient</a></li>
            <% } %>
            </ul>
        </div>
        
        <label class="strong">Branch:
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "List_Patient_BranchId" })%>
        </label>
        <label for="PatientRoster_Status" class="strong">Status:
            <select id="List_Patient_Status" name="Status" class="PatientStatusDropDown" style="width:120px">
                <option value="0">All</option>
                <option value="1" selected>Active</option>
                <option value="2">Discharged</option>
                <option value="3">Pending</option>
                <option value="4">Non-Admit</option>
            </select>
        </label>
        
        <div id="List_Patient_Grid_Search" ></div> 
     </fieldset>
      
     <div id="Patient_List_MainContainer" ><% Html.RenderPartial("ListContent", Model); %></div>
</div>

<script type="text/javascript">
    $("#List_Patient_Grid_Search").append(
        $("<div/>").GridSearchById("#List_Patient_Grid")
    ).closest(".window-content").css("background-color", "#d6e5f3").find(".grid-search").css({ position: "relative", left: 0 });
</script>