﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">User Access | <%=Model.DisplayName %></span>
<%=Html.Hidden("PatientId", Model.Id, new { @id = "UserAccessPatientId" })%>
<div class="wrapper">
    <%= Html
            .Telerik()
            .Grid<SelectedUser>()
            .Name("List_PatientUserAccess")
            .ToolBar(commnds => commnds.Custom())
            .Columns(columns => {
                columns.Bound(u => u.UserId).Hidden(true);
                columns.Bound(u => u.Selected).Title("").ClientTemplate("<input type=\"checkbox\" onclick=\"Patient.UserAccess.UserClick(this)\" value=\"<#=PatientUserId#>\"/>").Width(40);
                columns.Bound(u => u.DisplayName).Title("Name");
            })
            .DataBinding(dataBinding => dataBinding.Ajax().Select("GetUserAccess","Patient", new { patientId = Model.Id }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))
            .Footer(false)
            .ClientEvents(events => events.OnRowDataBound("Patient.UserAccess.OnRowBound"))%>
</div>
<script type="text/javascript">
    $("#List_PatientUserAccess .t-grid-toolbar").html("")
    .append($("<div/>").GridSearch())
    .append("<div class='float-left'>Note: Changes are saved automatically.</div>");
    $("#List_PatientUserAccess .t-grid-content").css({ height: "auto", top: 59});
</script>
