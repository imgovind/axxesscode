﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<%  if (Model.Patient != null) { %>
    <div class="top" id="patientTop">
        <%  Html.RenderPartial("/Views/Patient/Info.ascx", Model.Patient); %>
    </div>
    <div class="bottom">
        <%  Html.RenderPartial("/Views/Patient/Activities.ascx", Model.Patient.Id); %>
    </div>
    <%  Html.RenderPartial("/Views/Patient/Popup.ascx", Model.Patient); %>
    <%= Html.Hidden("PatientCenter_PatientId", Model.Patient.Id) %>
    <script type="text/javascript">
        $("select.patient-activity-drop-down").change(function() {
            Patient._showFilter = $("select.patient-activity-drop-down").val();
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({
                patientId: $("#PatientCenter_PatientId").val(),
                discipline: $("select.patient-activity-drop-down").val(),
                dateRangeId: $("select.patient-activity-date-drop-down").val()
            })
        });
        if ($("select.patient-activity-date-drop-down").val() == "DateRange") {
            $("#date-range-text").hide();
            $("div.custom-date-range").show();
        } else if ($("select.patient-activity-date-drop-down").val() == "All") {
            $("#date-range-text").hide();
            $("div.custom-date-range").hide();
        } else {
            $("#date-range-text").show();
            $("div.custom-date-range").hide();
        }
        $("select.patient-activity-date-drop-down").change(function() {
            if ($("select.patient-activity-date-drop-down").val() == "DateRange") {
                if (!Patient._isRebind) {
                    Patient._dateFilter = "DateRange";
                }
                $("#date-range-text").hide();
                $("div.custom-date-range").show();
            } else if ($("select.patient-activity-date-drop-down").val() == "All") {
                Patient.DateRange($("select.patient-activity-date-drop-down").val());
                $("#date-range-text").hide();
                $("div.custom-date-range").hide();
            } else {
                Patient.DateRange($("select.patient-activity-date-drop-down").val());
                $("#date-range-text").show();
                $("div.custom-date-range").hide();
            }
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({
                patientId: $("#PatientCenter_PatientId").val(),
                discipline: $("select.patient-activity-drop-down").val(),
                dateRangeId: $("select.patient-activity-date-drop-down").val()
            })
        })
     </script>
<%  } else { %>
    <div class="abs center">No Patient Data Found</div>
<%  } %>
