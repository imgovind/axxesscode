﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<span class="wintitle">Episode Orders | <%= Model.DisplayName %> | <%= Model.StartDate.ToShortDateString() %>&#8211;<%= Model.EndDate.ToShortDateString()%></span>
<div class="wrapper">
<%= Html.Telerik().Grid<Order>().Name("List_EpisodeOrders").DataKeys(keys => { keys.Add(o => o.Id).RouteKey("id"); }).Columns(columns => {
    columns.Bound(o => o.Number).Title("Order Number").Width(110).Sortable(false).ReadOnly();
    columns.Bound(o => o.Text).Title("Type").Sortable(false).ReadOnly();
    columns.Bound(o => o.StatusName).Title("Status").Sortable(false).ReadOnly();
    columns.Bound(o => o.PhysicianName).Title("Physician").Width(150).Sortable(false).ReadOnly();
    columns.Bound(o => o.OrderDate).Title("Order Date").Width(90).Sortable(true).ReadOnly();
    columns.Bound(o => o.SendDateFormatted).Title("Sent Date").Width(90).Sortable(false);
    columns.Bound(o => o.ReceivedDateFormatted).Title("Received Date").Width(100).Sortable(false);
    columns.Bound(o => o.PrintUrl).Title(" ").Width(100).Sortable(false);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("EpisodeOrders", "Patient", new { episodeId = Model.Id, patientId = Model.PatientId })).Pageable(paging => paging.PageSize(50)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
%>
</div>
<script type="text/javascript">
    $("#List_EpisodeOrders .t-grid-content").css({ 'height': 'auto', 'top': '26px', 'bottom': '' });
    $("#List_EpisodeOrders .t-toolbar.t-grid-toolbar").empty();
</script>