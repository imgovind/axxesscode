<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("InsertNewMedication", "Patient", FormMethod.Post, new { @id = "newMedicationForm" })) { %>
<%= Html.Hidden("medicationProfileId", Model, new { @id = "New_Medication_ProfileId" })%>
<%= Html.Hidden("LexiDrugId", "", new { @id = "New_Medication_DrugId" })%>   
<div class="wrapper main">
    <fieldset class="newmed">
        <legend>New Medication</legend>
        <div class="wide-column">
            <div class="row">
                <div class="longstanding float-left">
                    <%= Html.CheckBox("IsLongStanding", false, new { @id = "New_Medication_IsLongStanding", @class = "bigradio" })%>
                    <label for="New_Medication_IsLongStanding" class="bold">Long Standing</label>
                </div><div id="New_Medication_StartDateRow" class="float-left">
                    <span>Start Date:</span>
                    <input type="text" class="date-picker" name="StartDate" id="New_Medication_StartDate" />
                </div>
            </div>
            <div class="row">
                <label for="New_Medication_MedicationDosage" class="float-left">Medication &#38; Dosage:</label>
                <div class="float-left">
                    <%= Html.TextBox("MedicationDosage", "", new { @id = "New_Medication_MedicationDosage", @class = "longtext input_wrapper required", @maxlength = "120" })%>
                </div>
                <label >(�Search for Medication� is required to include medication in drug interactions check.)</label>
                <div class="buttons fr">
                    <ul>
                        <li><a onclick="Medication.Search('New');">Search for Medication</a></li>
                    </ul>
                </div>
            </div>
            <div class="row"><label for="New_Medication_Classification" class="float-left">Classification:</label><div class="float-left"><%= Html.TextBox("Classification", "", new { @id = "New_Medication_Classification", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Medication_Frequency" class="float-left">Frequency:</label><div class="float-left"><%= Html.TextBox("Frequency", "", new { @id = "New_Medication_Frequency", @class = "text input_wrapper Frequency", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Medication_Route" class="float-left">Route:</label><div class="float-left"><%= Html.TextBox("Route", "", new { @id = "New_Medication_Route", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Medication_Type" class="float-left">Type:</label><div class="float-left">
                <% var medicationTypes = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "New", Value = "N" },
                       new SelectListItem { Text = "Changed", Value = "C" },               
                       new SelectListItem { Text = "Unchanged", Value = "U" }               
                       
                    }, "Value", "Text");%>
                <%= Html.DropDownList("medicationType", medicationTypes)%>
            </div></div>
         </div>   
    </fieldset><%= Html.Hidden("AddAnother", "", new { @id="New_Medication_AddAnother" })%>   
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick='$("#New_Medication_AddAnother").val("");$(this).closest("form").submit();'>Save &#38; Exit</a></li>
        <li><a href="javascript:void(0);" onclick='$("#New_Medication_AddAnother").val("AddAnother");$(this).closest("form").submit();'>Save &#38; Add Another</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>
<%} %>