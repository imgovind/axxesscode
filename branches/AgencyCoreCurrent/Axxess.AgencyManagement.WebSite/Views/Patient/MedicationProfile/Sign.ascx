﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile Snapshot | <%= Model.Patient.DisplayName %></span>
<% using (Html.BeginForm("SignMedicationHistory", "Patient", FormMethod.Post, new { @id = "newMedicationProfileSnapShotForm" })) { %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<%= Html.Hidden("ProfileId", Model.MedicationProfile.Id)%>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Medication Profile SnapShot</th></tr>
            <tr><td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td></tr>
            <tr><td colspan="2">
                <div>
                    <label for="MedProfile_EpisodeRange" class="float-left width200">Episode Associated:</label>
                    <div class="float-left"><%= Html.PatientEpisodes("EpisodeId", Model.EpisodeId.ToString(), Model.Patient.Id, "-- Select Episode --", new { @id = "MedProfileSnapShot_EpisodeRange", @class = "requireddropdown" })%></div>
                </div><div class="clear"></div>
                <div>
                    <label for="MedProfile_PrimaryDiagnosis" class="float-left width200">Primary Diagnosis:</label>
                    <div class="float-left"><%= Html.TextBox("PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") && data["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosis"].Answer : string.Empty, new { @id = "MedProfile_PrimaryDiagnosis", @Style = "width: 250px;" })%></div>
                </div><div class="clear"></div>
                <div>
                    <%  var htmlPharmacyPrimary = new Dictionary<string, string>(); %>
                    <%  htmlPharmacyPrimary.Add("id", "Edit_Patient_PrimaryPharmacy"); %>
                    <%  htmlPharmacyPrimary.Add("class", "Pharmacies longselect"); %>
                    <label for="Edit_Patient_PrimaryPharmacy" class="float-left width200">Pharmacy & Phone:</label>
                    <div class="float-left" ><%= Html.Pharmacies("PrimaryPharmacyId", Model.PrimaryPharmacyId,  true, htmlPharmacyPrimary)%></div>
                </div>
            </td><td colspan="2">
                <div>
                    <label for="MedProfile_Physician" class="float-left width200">Physician:</label>
                    <div class="float-left"><%= Html.TextBox("PhysicianId", (Model != null && !Model.PhysicianId.IsEmpty()) ? Model.PhysicianId.ToString() : "", new { @id = "MedProfile_Physician", @class = "Physicians" })%></div>
                </div><div class="clear"></div>
                <div>
                    <label for="MedProfile_SecondaryDiagnosis" class="float-left width200">Secondary Diagnosis:</label>
                    <div class="float-left"><%= Html.TextBox("SecondaryDiagnosis", data.ContainsKey("M1022PrimaryDiagnosis1") && data["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "MedProfile_SecondaryDiagnosis", @Style = "width: 250px;" })%></div>
                </div><div class="clear"></div>
                
            </td></tr>
            <tr><th colspan="4">Allergies</th></tr>
            <tr><td colspan="4"><%= Html.TextArea("Allergies", Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty, new { @id = "MedProfileSnapShot_Allergies", @class = "fill" })%></td></tr>
        </tbody>
    </table>
    <div class="medprofile">
        <ul>
            <li class="align-center"><h3>Medication(s)</h3></li>
            <li>
                <span class="longstanding">LS</span>
                <span class="startdate">Start Date</span>
                <span class="snapshotmedicationdosage">Medication & Dosage</span>
                <span class="snapshotfrequency">Frequency</span>
                <span class="route">Route</span>
                <span class="snapshottype">Type</span>
                <span class="snapshotclassification">Classification</span>
            </li>
        </ul><ol><%
        if (Model != null) {
            int i = 1;
            var medications = Model.MedicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable);
            foreach (var med in medications) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                    <span class="longstanding"><input name="LongStanding" class="radio" disabled="true" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
                    <span class="startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                    <span class="snapshotmedicationdosage"><%= med.MedicationDosage %></span>
                    <span class="snapshotfrequency"><%= med.Frequency %></span>
                    <span class="route"><%= med.Route %></span>
                    <span class="snapshottype"><%= med.MedicationType.Text %></span>
                    <span class="snapshotclassification"><%= med.Classification %></span>
            </li><%
                i++;
            }
        } %>
        </ol>
    </div>
    <table class="fixed nursing">
        <tbody>
         <tr><th colspan="4">Signature</th></tr>
            <tr><td colspan="4">
                <p class="align-left normal"><strong>Drug Regimen Review Acknowledgment:</strong> I have reviewed all the listed medications for potential adverse effects, drug reactions, including ineffective drug therapy, significant side effects, significant drug interactions, duplicate drug therapy, and noncompliance with drug therapy.</p><br />
                <div class="half">
                    <label for="Signature" class="float-left width200">Clinician Signature:</label>
                    <div class="float-left"><%= Html.Password("Signature", "", new { @id = "MedicationProfile_ClinicianSignature", @class="required" }) %></div>
                </div><div class="half">
                    <label for="SignedDate" class="float-left width200">Date:</label>
                    <div class="float-left"><input type="text" class="date-picker required" name="SignedDate" id="MedicationProfile_ClinicianSignatureDate" /></div>
                </div>
            </td></tr>
        </tbody>
    </table>
    <div class="buttons" style="margin-bottom: 100px; margin-top: 20px;">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Sign</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('medicationprofilesnapshot');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $(".medprofile ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });

    $("#Edit_Patient_PrimaryPharmacy").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('newpharmacy');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") selectList.selectedIndex = 0;

    });
</script>