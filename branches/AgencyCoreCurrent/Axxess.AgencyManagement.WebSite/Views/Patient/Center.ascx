﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<span class="wintitle">Patient Charts | <%= Current.AgencyName %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="buttons heading"><ul><% if (Current.HasRight(Permissions.ManagePatients) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="javascript:Acore.Open('newpatient');" title="Add New Patient">Add New Patient</a></li><% } %></ul></div>
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new {  @class = "PatientBranchCode" })%></div></div>
            <%--<div class="row"><label>View:</label><div><select name="list" class="PatientStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option><option value="3">Pending Patients</option><option value="4">Non-Admitted Patients</option></select></div></div>--%>
            <div class="row"><label>View:</label>
                <div>
                    <% var statusList = new List<SelectListItem>(){
                        new SelectListItem(){ Text = "Active Patients", Value = "1" },
                        new SelectListItem(){ Text = "Discharged Patients", Value = "2"},
                        new SelectListItem(){ Text = "Pending Patients", Value = "3"},
                        new SelectListItem(){ Text = "Non-Admitted Patients", Value = "4"}
                    }; %>
                    <%= Html.DropDownList("PatientStatus", statusList, new { @class = "PatientStatusDropDown" })%>
                </div>
            </div>
            <div class="row"><label>Filter:</label><div><select name="PatientPaymentSource" class="PatientPaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs</option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Patient_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("Patients", Model.PatientStatus); %></div>
    </div>
    <div id="PatientMainResult" class="ui-layout-center"><% if (Model == null || Model.Count == 0) { %><div class="abs center"><p>No Patients found that fit your search criteria.</p></div><% } %></div>
</div>