﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationLog>" %>
<span class="wintitle">Edit Hospitalization Log | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("UpdateHospitalizationLog", "Patient", FormMethod.Post, new { @id = "editHospitalizationLogForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_HospitalizationLog_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_HospitalizationLog_PatientId" })%>
<% var data = Model.ToDictionary(); %>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Hospitalization Log</th></tr>
            <tr><td colspan="2">
                <div><label for="Edit_HospitalizationLog_PatientName" class="float-left width200">Patient:</label>
                <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientName", Model.PatientId.ToString(), new { @id = "Edit_HospitalizationLog_PatientName", @class = "requireddropdown", @disabled = "disabled" })%></div></div>
                <div class="clear"></div>
                <div><label for="Edit_HospitalizationLog_EpisodeList" class="float-left width200">Episode:</label>
                <div class="float-left"><%= Html.PatientEpisodes("EpisodeId", Model.EpisodeId.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_HospitalizationLog_EpisodeList", @class = "requireddropdown" })%></div></div>
                <div class="clear"></div>
            </td><td colspan="2">
                <div><label for="Edit_HospitalizationLog_User" class="float-left width200">User:</label>
                <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Edit_HospitalizationLog_User", @class = "requireddropdown" })%></div></div>
                <div class="clear"></div>
            </td></tr>
            <tr><th colspan="2">OASIS M0903</th><th colspan="2">OASIS M0906</th></tr>
            <tr><td colspan="2">
                <div><label for="Edit_HospitalizationLog_M0903LastHomeVisitDate" class="float-left"><span class="green">(M0903)</span> Date of Last (Most Recent) Home Visit:</label></div>
                <div class="clear"></div>
                <div class="float-left">
                    <%= Html.TextBox("M0903LastHomeVisitDate", Model.LastHomeVisitDateFormatted, new { @id = "Edit_HospitalizationLog_M0903LastHomeVisitDate", @class = "date-picker" })%>
                </div>
            </td><td colspan="2">
                <div><label for="Edit_HospitalizationLog_M0906DischargeDate" class="float-left"><span class="green">(M0906)</span>Transfer Date: Enter the date of the transfer of the patient.</label></div>
                <div class="clear"></div>
                <div class="float-left">
                    <%= Html.TextBox("M0906DischargeDate", Model.HospitalizationDateFormatted, new { @id = "Edit_HospitalizationLog_M0906DischargeDate", @class = "date-picker" })%>
                </div>
            </td></tr>
            <tr><th colspan="2">OASIS M2410</th><th colspan="2">OASIS M2440</th></tr>
            <tr><td colspan="2">
                <div class="float-left strong"><span class="green">(M2410)</span> To which Inpatient Facility has the patient been admitted?</div>
                <%= Html.Hidden("M2410TypeOfInpatientFacility", " ", new { @id = "" })%>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "01", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").IsEqual("01") ? true : false, new { @id = "Edit_HospitalizationLog_M2410TypeOfInpatientFacility1", @class = "radio float-left" })%>
                        <label for="Edit_HospitalizationLog_M2410TypeOfInpatientFacility1"><span class="float-left">1 &#8211;</span><span class="normal margin">Hospital</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "02", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").IsEqual("02") ? true : false, new { @id = "M2410TypeOfInpatientFacility2", @class = "radio float-left" })%>
                        <label for="M2410TypeOfInpatientFacility2"><span class="float-left">2 &#8211;</span><span class="normal margin">Rehabilitation facility</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "03", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").IsEqual("03") ? true : false, new { @id = "M2410TypeOfInpatientFacility3", @class = "radio float-left" })%>
                        <label for="M2410TypeOfInpatientFacility3"><span class="float-left">3 &#8211;</span><span class="normal margin">Nursing home</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "04", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").IsEqual("04") ? true : false, new { @id = "M2410TypeOfInpatientFacility4", @class = "radio float-left" })%>
                        <label for="M2410TypeOfInpatientFacility4"><span class="float-left">4 &#8211;</span><span class="normal margin">Hospice</span></label>
                    </div>
                </div>
            </td><td colspan="2">
                <div class="float-left strong"><span class="green">(M2440)</span> For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)</div>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <input name="M2440ReasonPatientAdmittedTherapy" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedTherapy' class='radio float-left' name='M2440ReasonPatientAdmittedTherapy' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedTherapy").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedTherapy"><span class="float-left">1 &#8211;</span><span class="margin normal">Therapy services</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedRespite" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedRespite' class='radio float-left' name='M2440ReasonPatientAdmittedRespite' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedRespite").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedRespite"><span class="float-left">2 &#8211;</span><span class="margin normal">Respite care</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedHospice" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedHospice' class='radio float-left' name='M2440ReasonPatientAdmittedHospice' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedHospice").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedHospice"><span class="float-left">3 &#8211;</span><span class="margin normal">Hospice care</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedPermanent" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedPermanent' class='radio float-left' name='M2440ReasonPatientAdmittedPermanent' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedPermanent").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedPermanent"><span class="float-left">4 &#8211;</span><span class="margin normal">Permanent placement</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedUnsafe" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedUnsafe' class='radio float-left' name='M2440ReasonPatientAdmittedUnsafe' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnsafe").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedUnsafe"><span class="float-left">5 &#8211;</span><span class="margin normal">Unsafe for care at home</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedOther" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedOther' class='radio float-left' name='M2440ReasonPatientAdmittedOther' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedOther").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedOther"><span class="float-left">6 &#8211;</span><span class="margin normal">Other</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedUnknown" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2440ReasonPatientAdmittedUnknown' class='radio float-left' name='M2440ReasonPatientAdmittedUnknown' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnknown").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2440ReasonPatientAdmittedUnknown"><span class="float-left">UK &#8211;</span><span class="margin normal">Unknown</span></label>
                    </div>
                </div>
            </td></tr>
            <tr><th colspan="4">OASIS M2430</th></tr>
            <tr><td colspan="4">
                <div class="float-left strong"><span class="green">(M2430)</span> Reason for Hospitalization: For what reason(s) did the patient require hospitalization? (Mark all that apply.)</div>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <input name="M2430ReasonForHospitalizationMed" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationMed' class='radio float-left' name='M2430ReasonForHospitalizationMed' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMed").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationMed"><span class="float-left">1 &#8211;</span><span class="margin normal">Improper medication administration, medication side effects, toxicity, anaphylaxis</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationFall" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationFall' class='radio float-left' name='M2430ReasonForHospitalizationFall' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationFall").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationFall"><span class="float-left">2 &#8211;</span><span class="margin normal">Injury caused by fall</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationInfection" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationInfection' class='radio float-left' name='M2430ReasonForHospitalizationInfection' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationInfection").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationInfection"><span class="float-left">3 &#8211;</span><span class="margin normal">Respiratory infection (e.g., pneumonia, bronchitis)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationOtherRP" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationOtherRP' class='radio float-left' name='M2430ReasonForHospitalizationOtherRP' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationOtherRP").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationOtherRP"><span class="float-left">4 &#8211;</span><span class="margin normal">Other respiratory problem</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHeartFail" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationHeartFail' class='radio float-left' name='M2430ReasonForHospitalizationHeartFail' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartFail").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationHeartFail"><span class="float-left">5 &#8211;</span><span class="margin normal">Heart failure (e.g., fluid overload)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationCardiac" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationCardiac' class='radio float-left' name='M2430ReasonForHospitalizationCardiac' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationCardiac").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationCardiac"><span class="float-left">6 &#8211;</span><span class="margin normal">Cardiac dysrhythmia (irregular heartbeat)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationMyocardial" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationMyocardial' class='radio float-left' name='M2430ReasonForHospitalizationMyocardial' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMyocardial").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationMyocardial"><span class="float-left">7 &#8211;</span><span class="margin normal">Myocardial infarction or chest pain</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHeartDisease" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationHeartDisease' class='radio float-left' name='M2430ReasonForHospitalizationHeartDisease' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartDisease").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationHeartDisease"><span class="float-left">8 &#8211;</span><span class="margin normal">Other heart disease</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationStroke" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationStroke' class='radio float-left' name='M2430ReasonForHospitalizationStroke' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationStroke").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationStroke"><span class="float-left">9 &#8211;</span><span class="margin normal">Stroke (CVA) or TIA</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHypo" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationHypo' class='radio float-left' name='M2430ReasonForHospitalizationHypo' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHypo").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationHypo"><span class="float-left">10 &#8211;</span><span class="margin normal">Hypo/Hyperglycemia, diabetes out of control</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationGI" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationGI' class='radio float-left' name='M2430ReasonForHospitalizationGI' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationGI").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationGI"><span class="float-left">11 &#8211;</span><span class="margin normal">GI bleeding, obstruction, constipation, impaction</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationDehMal" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationDehMal' class='radio float-left' name='M2430ReasonForHospitalizationDehMal' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationDehMal").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationDehMal"><span class="float-left">12 &#8211;</span><span class="margin normal">Dehydration, malnutrition</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUrinaryInf" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationUrinaryInf' class='radio float-left' name='M2430ReasonForHospitalizationUrinaryInf' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUrinaryInf").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationUrinaryInf"><span class="float-left">13 &#8211;</span><span class="margin normal">Urinary tract infection</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationIV" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationIV' class='radio float-left' name='M2430ReasonForHospitalizationIV' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationIV").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationIV"><span class="float-left">14 &#8211;</span><span class="margin normal">IV catheter-related infection or complication</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationWoundInf" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationWoundInf' class='radio float-left' name='M2430ReasonForHospitalizationWoundInf' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationWoundInf").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationWoundInf"><span class="float-left">15 &#8211;</span><span class="margin normal">Wound infection or deterioration</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUncontrolledPain" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationUncontrolledPain' class='radio float-left' name='M2430ReasonForHospitalizationUncontrolledPain' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUncontrolledPain").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationUncontrolledPain"><span class="float-left">16 &#8211;</span><span class="margin normal">Uncontrolled pain</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationMental" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationMental' class='radio float-left' name='M2430ReasonForHospitalizationMental' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMental").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationMental"><span class="float-left">17 &#8211;</span><span class="margin normal">Acute mental/behavioral health problem</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationDVT" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationDVT' class='radio float-left' name='M2430ReasonForHospitalizationDVT' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationDVT").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationDVT"><span class="float-left">18 &#8211;</span><span class="margin normal">Deep vein thrombosis, pulmonary embolus</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationScheduled" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationScheduled' class='radio float-left' name='M2430ReasonForHospitalizationScheduled' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationScheduled").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationScheduled"><span class="float-left">19 &#8211;</span><span class="margin normal">Scheduled treatment or procedure</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationOther" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationOther' class='radio float-left' name='M2430ReasonForHospitalizationOther' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationOther").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationOther"><span class="float-left">20 &#8211;</span><span class="margin normal">Other than above reasons</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUK" type="hidden" value=" " />
                        <%= string.Format("<input id='Edit_HospitalizationLog_M2430ReasonForHospitalizationUK' class='radio float-left' name='M2430ReasonForHospitalizationUK' type='checkbox' value='1' {0} />", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUK").IsEqual("1") ? "checked='checked'" : "")%>
                        <label for="Edit_HospitalizationLog_M2430ReasonForHospitalizationUK"><span class="float-left">UK &#8211;</span><span class="margin normal">Reason unknown</span></label>
                    </div>
                </div>
            </td></tr>
            <tr><th colspan="4">Narrative</th></tr>
            <tr>
                <td colspan="4" class="align-left">
                    <div class="align-center"><%= Html.Templates("Templates", new { @class = "Templates mobile_fr", @template = "#Edit_HospitalizationLog_GenericDischargeNarrative" })%></div>
                    <div class="clear"></div>
                    <div class="align-center"><%= Html.TextArea("GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), 8, 20, new { @class = "fill", @id = "Edit_HospitalizationLog_GenericDischargeNarrative", @maxcharacters = "1500" })%><p class="charsRemaining"></p></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="buttons"><ul>
         <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edithospitalizationlog');">Exit</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    Template.OnChangeInit();
</script>
<%} %>
