﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">New Authorization | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Authorization", FormMethod.Post, new { @id = "newAuthorizationForm" })) { %>
<div class="wrapper main">
    
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row"><label for="New_Authorization_PatientId" class="float-left">Patient Name:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", Model.Id.ToString(), new { @id = "New_Authorization_PatientId", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_Authorization_StartDate" class="float-left">Start Date:</label><div class="float-right"><input type="text" class="date-picker required" name="StartDate" id="New_Authorization_StartDate" /></div></div>
            <div class="row"><label for="New_Authorization_EndDate" class="float-left">End Date:</label><div class="float-right"><input type="text" class="date-picker required" name="EndDate" id="New_Authorization_EndDate" /></div></div>
            <div class="row"><label for="New_Authorization_LocationId" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "New_Authorization_LocationId", @class = "BranchLocation" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Authorization_Status" class="float-left">Status:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AuthorizationStatus, "Status", Model.Status.ToString(), new { @id = "New_Authorization_Status" })%></div></div>
            <div class="row"><label for="New_Authorization_Insurance" class="float-left">Insurance:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Insurance, "Insurance", Model.PrimaryInsurance, new { @id = "New_Authorization_Insurance", @class = "Insurances requireddropdown" })%></div></div>
            <div class="row"><label for="New_Authorization_AuthNumber1" class="float-left">Authorization Number 1:</label><div class="float-right"><%= Html.TextBox("Number1", "", new { @id = "New_Authorization_AuthNumber1", @class = "text input_wrapper required", @maxlength = "30" })%></div></div>
            <div class="row"><label for="New_Authorization_AuthNumber2" class="float-left">Authorization Number 2:</label><div class="float-right"><%= Html.TextBox("Number2", "", new { @id = "New_Authorization_AuthNumber2", @class = "text input_wrapper ", @maxlength = "30" })%></div></div>
            <div class="row"><label for="New_Authorization_AuthNumber3" class="float-left">Authorization Number 3:</label><div class="float-right"><%= Html.TextBox("Number3", "", new { @id = "New_Authorization_AuthNumber3", @class = "text input_wrapper ", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
        <% var countVisitType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "1"); %>
        <% var countHourType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "2"); %>
        <div class="column">
            <div class="row"><label for="New_Authorization_SNVisit" class="float-left">SN Count:</label><div class="float-right"><%= Html.TextBox("SNVisit", "", new { @id = "New_Authorization_SNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("SNVisitCountType", countVisitType, new { @id = "New_Authorization_SNVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_PTVisit" class="float-left">PT Count:</label><div class="float-right"><%= Html.TextBox("PTVisit", "", new { @id = "New_Authorization_PTVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("PTVisitCountType", countVisitType, new { @id = "New_Authorization_PTVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_OTVisit" class="float-left">OT Count:</label><div class="float-right"><%= Html.TextBox("OTVisit", "", new { @id = "New_Authorization_OTVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("OTVisitCountType", countVisitType, new { @id = "New_Authorization_OTVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_STVisit" class="float-left">ST Count:</label><div class="float-right"><%= Html.TextBox("STVisit", "", new { @id = "New_Authorization_STVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("STVisitCountType", countVisitType, new { @id = "New_Authorization_STVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_MSWVisit" class="float-left">MSW Count:</label><div class="float-right"><%= Html.TextBox("MSWVisit", "", new { @id = "New_Authorization_MSWVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("MSWVisitCountType", countVisitType, new { @id = "New_Authorization_MSWVisitCountType", @class = "shortselect" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Authorization_HHAVisit" class="float-left">HHA Count:</label><div class="float-right"><%= Html.TextBox("HHAVisit", "", new { @id = "New_Authorization_HHAVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("HHAVisitCountType", countVisitType, new { @id = "New_Authorization_HHAVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_DieticianVisit" class="float-left">Dietitian Count:</label><div class="float-right"><%= Html.TextBox("DieticianVisit", "", new { @id = "New_Authorization_DieticianVisit", @class = "numeric sn", @maxlength = "4" }) %><%= Html.DropDownList("DieticianVisitCountType", countVisitType, new { @id = "New_Authorization_DieticianVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_RNVisit" class="float-left">RN Count:</label><div class="float-right"><%= Html.TextBox("RNVisit", "", new { @id = "New_Authorization_RNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("RNVisitCountType", countHourType, new { @id = "New_Authorization_RNVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_LVNVisit" class="float-left">LVN Count:</label><div class="float-right"><%= Html.TextBox("LVNVisit", "", new { @id = "New_Authorization_LVNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("LVNVisitCountType", countHourType, new { @id = "New_Authorization_LVNVisitCountType", @class = "shortselect" })%></div></div>
        </div>
    </fieldset>
    <fieldset><legend>Comments</legend><div class="wide_column"><div class="row"><p class="charsRemaining"></p><textarea id="New_Authorization_Comments" name="Comments" cols="5" rows="6" maxcharacters="500"></textarea></div></div></fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newauthorization');">Cancel</a></li></ul></div>
</div>
<%}%>
