﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="abs above">
    <div class="float-left">
        <label>Show</label>
        <select id="patient-activity-drop-down" name="patient-activity-drop-down" class="patient-activity-drop-down">
            <option value="All" selected="selected">All</option>
            <option value="Orders">Orders</option>
            <option value="Nursing">Nursing</option>
            <option value="PT">PT</option>
            <option value="OT">OT</option>
            <option value="ST">ST</option>
            <option value="HHA">HHA</option>
            <option value="MSW">MSW</option>
            <option value="Dietician">Dietitian</option>
        </select>
        <label>Date</label>
        <select id="patient-activity-date-drop-down" name="patient-activity-date-drop-down" class="patient-activity-date-drop-down">
            <!--option value="All">All</option-->
            <!--option disabled="disabled">---------------------------------------------</option-->
            <option value="DateRange">Date Range</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="ThisEpisode">This Episode</option>
            <option value="LastEpisode">Last Episode</option>
            <option value="NextEpisode">Next Episode</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="Today">Today</option>
            <option value="ThisWeek">This Week</option>
            <option value="ThisWeekToDate">This Week-to-date</option>
            <option value="LastMonth">Last Month</option>
            <option value="ThisMonth">This Month</option>
            <option value="ThisMonthToDate">This Month-to-date</option>
            <option value="ThisFiscalQuarter">This Fiscal Quarter</option>
            <option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option>
            <option value="ThisFiscalYear">This Fiscal Year</option>
            <option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option>
            <option value="Yesterday">Yesterday</option>
            <option value="LastWeek">Last Week</option>
            <option value="LastWeekToDate">Last Week-to-date</option>
            <option value="LastMonthToDate">Last Month-to-date</option>
            <option value="LastFiscalQuarter">Last Fiscal Quarter</option>
            <option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option>
            <option value="LastFiscalYear">Last Fiscal Year</option>
            <option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option>
            <option value="NextWeek">Next Week</option>
            <option value="Next4Weeks">Next 4 Weeks</option>
            <option value="NextMonth">Next Month</option>
            <option value="NextFiscalQuarter">Next Fiscal Quarter</option>
            <option value="NextFiscalYear">Next Fiscal Year</option>
        </select>
    </div>
    <div id="patient-center-activity-filter" class="float-left">
        <div id="date-range-text" class="float-right"></div>
        <div class="custom-date-range" class="hidden">
            <div class="buttons float-right" style="margin-top:-3px">
                <ul>
                    <li><a onclick="Patient.CustomDateRange();return false">Search</a></li>
                </ul>
            </div>
            <label>From</label>
            <input type="text" class="date-picker" name="From" id="patient-activity-from-date" />
            <label>To</label>
            <input type="text" class="date-picker" name="To" id="patient-activity-to-date" />
        </div>
    </div>
</div>
<% var val = Model != null && !Model.IsEmpty() ? Model : Guid.Empty;
    Html.Telerik().Grid<ScheduleEvent>().Name("PatientActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false).Width(0);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task").Width(180); 
       columns.Bound(s => s.EventDateSortable).ClientTemplate("<#=EventDateSortable#>").Title("Scheduled Date").Width(120);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(130);
       columns.Bound(s => s.StatusName).Title("Status").Width(100);
       columns.Bound(s => s.VisitVerifyUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=VisitVerifyUrl#>");
       columns.Bound(s => s.OasisProfileUrl).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" });
       columns.Bound(s => s.StatusComment).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\"><#=StatusComment#></a>");
       columns.Bound(s => s.Comments).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=CommentsCleaned#></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\"><#=EpisodeNotesCleaned#></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=PrintUrl#>").Width(22);
       columns.Bound(s => s.AttachmentUrl).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=AttachmentUrl#>").Width(22);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
       columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Patient.ActivityRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Patient", new { patientId = val, discipline = "All", dateRangeId = "ThisEpisode", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now })).Sortable().Scrollable().Footer(false).Render();
%>
<script type="text/javascript">
    var currentDateFilter = Patient._dateFilter != "" ? Patient._dateFilter : <% if (Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) || Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) || Current.IfOnlyRole(AgencyRoles.Auditor)) { %> "All" <% } else { %> "ThisEpisode" <% } %>;
    var currentTaskFilter = Patient._showFilter != "" ? Patient._showFilter : "All";
    $("#patient-activity-drop-down").val(currentTaskFilter)
    $("#patient-activity-from-date,#patient-activity-to-date").DatePicker();
    $("#patient-activity-date-drop-down").val(currentDateFilter);
    
    $("#patient-activity-from-date").val(Patient._fromDate != "" ? Patient._fromDate : '<%= DateTime.Now.AddDays(-60).ToZeroFilled() %>');
    $("#patient-activity-to-date").val(Patient._toDate != "" ? Patient._toDate : '<%= DateTime.Today.ToZeroFilled() %>');
    if (currentDateFilter == "DateRange") window.setTimeout(function() { $("#patient-center-activity-filter .custom-date-range a:contains('Search')").click() }, 50);
    else Patient.DateRange(currentDateFilter);
    Patient._isRebind = false;
</script>