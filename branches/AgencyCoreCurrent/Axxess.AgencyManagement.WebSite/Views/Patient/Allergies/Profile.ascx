﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<span class="wintitle">Allergy Profile | <%= Model.Patient.DisplayName %></span>
<%= Html.Hidden("Id", Model.AllergyProfile.Id, new {@id = "allergyProfileId" })%>
<%= Html.Hidden("PatientId", Model.AllergyProfile.PatientId)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Allergy Profile</th>
            </tr>
            <tr>
                <td colspan="4" class="bigtext"><%= Model.Patient.DisplayName %></td>
            </tr>
        </tbody>
    </table>
    <div class="buttons">
        <ul class="float-left">
            <% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %><li><a href="javascript:void(0);" onclick="Allergy.Add('<%= Model.AllergyProfile.Id %>');">Add Allergy</a></li><% } %>
            <li><a href="javascript:void(0);" onclick="Allergy.Refresh('<%= Model.AllergyProfile.Id %>', 'AllergyProfile');">Refresh Allergies</a></li>
            <li><a href="javascript:void(0);" onclick="Acore.OpenPrintView({ Url: '/Patient/AllergyProfilePrint/<%=Model.AllergyProfile.PatientId %>', PdfUrl: '/Patient/AllergyProfilePdf', PdfData: { 'id': '<%=Model.AllergyProfile.PatientId %>' } });">Print Allergy Profile</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div id="AllergyProfile_AllergyProfileList"><% Html.RenderPartial("~/Views/Patient/Allergies/List.ascx", Model.AllergyProfile); %></div>
</div>