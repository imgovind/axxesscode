﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HiqhViewData>" %>
<div class="wrapper main">
<% if (Model.XmlResult.IsNotNullOrEmpty()) { %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="U.GetAttachment('Patient/HiqhEligibilityPdf', { medicareNumber: '<%= Model.MedicareNumber %>', lastName: '<%= Model.LastName %>', firstName: '<%= Model.FirstName %>', dob: '<%= Model.Dob %>', gender: '<%= Model.Gender %>' })">Print</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a></li>
        </ul>
    </div>
<%  
       System.Xml.XPath.XPathDocument xPathDocument = new System.Xml.XPath.XPathDocument(new System.IO.StringReader(Model.XmlResult));
       System.Xml.XPath.XPathNavigator xPathNavigator = xPathDocument.CreateNavigator(); 
%>
       
    <fieldset class="half float-left"><legend>Beneficiary & Entitlement Information</legend>
<%
   var beneficiaryInfoNodes = xPathNavigator.Select("hiqhResponse/beneficiaryInformation");
   foreach (System.Xml.XPath.XPathNavigator node in beneficiaryInfoNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Provider Id:</label>
                <div class="float-right"><%= node.SelectSingleNode("providerId") != null ? node.SelectSingleNode("providerId").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Message:</label>
                <div class="float-right"><%= node.SelectSingleNode("message") != null ? node.SelectSingleNode("message").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Full Name:</label>
                <div class="float-right"><%= node.SelectSingleNode("fullName") != null ? node.SelectSingleNode("fullName").Value : string.Empty%> <%= node.SelectSingleNode("firstInitial") != null ? node.SelectSingleNode("firstInitial").Value : string.Empty%></div>
            </div>
<% } %>
<%
   var currentEntitlementNodes = xPathNavigator.Select("hiqhResponse/beneficiaryInformation/currentEntitlement");
    foreach (System.Xml.XPath.XPathNavigator node in currentEntitlementNodes) {
%>
            <div class="row">
                <label class="float-left">Part A Entitlement - Effective date:</label>
                <div class="float-right"><%= node.SelectSingleNode("partAEffectiveDate") != null ? node.SelectSingleNode("partAEffectiveDate").Value + " - Current" : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Part B Entitlement - Effective date:</label>
                <div class="float-right"><%= node.SelectSingleNode("partBEffectiveDate") != null ? node.SelectSingleNode("partBEffectiveDate").Value + " - Current" : string.Empty%></div>
            </div>
        </div>
<% } %>
    </fieldset>
    <fieldset class="half float-right"><legend>Corrected Beneficiary Information</legend><div class="clear"><span><em>Fields appear blank if no corrected information found.</em></span></div>
<%
       var correctedBeneficiaryNodes = xPathNavigator.Select("hiqhResponse/correctedBeneficiary");
       foreach (System.Xml.XPath.XPathNavigator node in correctedBeneficiaryNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">HIC Number:</label>
                <div class="float-right"><%= node.SelectSingleNode("hic") != null ? node.SelectSingleNode("hic").Value : string.Empty %></div>
            </div>
            <div class="row">
                <label class="float-left">Date of Birth:</label>
                <div class="float-right"><%= node.SelectSingleNode("dateOfBirth") != null ? node.SelectSingleNode("dateOfBirth").Value : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Name:</label>
                <div class="float-right"><%= node.SelectSingleNode("lastName") != null ? node.SelectSingleNode("lastName").Value : string.Empty%> <%= node.SelectSingleNode("firstInitial") != null ? node.SelectSingleNode("firstInitial").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right"><%= node.SelectSingleNode("sex") != null ? node.SelectSingleNode("sex").Value : string.Empty %></div>
            </div>
        </div>
<% } %>
    </fieldset>
    <div class="clear"></div>
    <fieldset><legend>HMO Plan Information</legend>
<%
       var planInformationNodes = xPathNavigator.Select("hiqhResponse/planInformation/hmoPlans/hmoPlan");
       foreach (System.Xml.XPath.XPathNavigator node in planInformationNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Type:</label>
                <div class="float-right"><%= node.SelectSingleNode("type") != null ? node.SelectSingleNode("type").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Plan Identification Code:</label>
                <div class="float-right"><%= node.SelectSingleNode("idCode") != null ? node.SelectSingleNode("idCode").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Option Code:</label>
                <div class="float-right"><%= node.SelectSingleNode("optionCode") != null ? node.SelectSingleNode("optionCode").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Effective Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("effectiveDate") != null ? node.SelectSingleNode("effectiveDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Termination Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("terminationDate") != null ? node.SelectSingleNode("terminationDate").Value : string.Empty%></div>
            </div>
        </div>
<% } %>
    </fieldset>
        <fieldset><legend>Hospice Benefit Periods</legend>
<%
       var hospiceInformationNodes = xPathNavigator.Select("hiqhResponse/hospiceInformation/hospiceBenefitPeriods/hospiceBenefitPeriod");
       foreach (System.Xml.XPath.XPathNavigator node in hospiceInformationNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Period:</label>
                <div class="float-right"><%= node.SelectSingleNode("period") != null ? node.SelectSingleNode("period").Value : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">First Provider Start Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("firstProviderStartDate") != null ? node.SelectSingleNode("firstProviderStartDate").Value : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">First Provider Id:</label>
                <div class="float-right"><%= node.SelectSingleNode("firstProviderId") != null ? node.SelectSingleNode("firstProviderId").Value : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">First Intermediary Id:</label>
                <div class="float-right"><%= node.SelectSingleNode("firstIntermediaryId") != null ? node.SelectSingleNode("firstIntermediaryId").Value : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">First Owner Change Start Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("firstOwnerChangeStartDate") != null ? node.SelectSingleNode("firstOwnerChangeStartDate").Value : string.Empty%></div>
            </div>
        </div>
<% } %>
    </fieldset>
    <fieldset><legend>Home Health PPS Benefit Periods</legend>
<%
    var homeHealthBenefitNodes = xPathNavigator.Select("hiqhResponse/homeHealthBenefitPeriods/homeHealthBenefitPeriods/homeHealthBenefitPeriod");
    foreach (System.Xml.XPath.XPathNavigator node in homeHealthBenefitNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Spell Number:</label>
                <div class="float-right"><%= node.SelectSingleNode("spellNumber") != null ? node.SelectSingleNode("spellNumber").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Qualifying Indicator:</label>
                <div class="float-right"><%= node.SelectSingleNode("qualifyingIndicator") != null ? node.SelectSingleNode("qualifyingIndicator").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">PartA Visits Remaining:</label>
                <div class="float-right"><%= node.SelectSingleNode("partAVisitsRemaining") != null ? node.SelectSingleNode("partAVisitsRemaining").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Earliest Billing Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("earliestBillingDate") != null ? node.SelectSingleNode("earliestBillingDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Latest Billing Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("latestBillingDate") != null ? node.SelectSingleNode("latestBillingDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">PartB Visits Applied:</label>
                <div class="float-right"><%= node.SelectSingleNode("partBVisitsApplied") != null ? node.SelectSingleNode("partBVisitsApplied").Value : string.Empty%></div>
            </div>
        </div>
<% } %>
    </fieldset>
    <fieldset><legend>Home Health Episode Information</legend>
<%
    var homeHealthEpisodeNodes = xPathNavigator.Select("hiqhResponse/homeHealthEpisodes/homeHealthEpisodes/homeHealthEpisode");
    foreach (System.Xml.XPath.XPathNavigator node in homeHealthEpisodeNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Start Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("startDate") != null ? node.SelectSingleNode("startDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">End Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("endDate") != null ? node.SelectSingleNode("endDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Intermediary Number:</label>
                <div class="float-right"><%= node.SelectSingleNode("intermediaryNumber") != null ? node.SelectSingleNode("intermediaryNumber").Value : string.Empty%></div>
            </div>
<%  if (node.SelectSingleNode("providerNumber") != null) { %>
<%
    var medicareProviderNumber = node.SelectSingleNode("providerNumber").Value;
    if (medicareProviderNumber.IsNotNullOrEmpty()) {%>
            <div class="row">
                <label class="float-left">Provider Number:</label>
                <div class="float-right"><%= medicareProviderNumber %></div>
            </div>
 <%           
        var medicareProvider = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetMedicareProvider(medicareProviderNumber);
        if (medicareProvider != null) {
%>
            <div class="row">
                <label class="float-left">Provider Name:</label>
                <div class="float-right"><%= medicareProvider.Name %></div>
            </div>
            <div class="row">
                <label class="float-left">Provider Address:</label>
                <div class="float-right"><%= medicareProvider.Address %></div>
            </div>
            <div class="row">
                <label class="float-left">Provider City/State/Zip:</label>
                <div class="float-right"><%= string.Format("{0}, {1} {2}", medicareProvider.City, medicareProvider.StateCode, medicareProvider.ZipCode) %></div>
            </div>
            <div class="row">
                <label class="float-left">Provider Phone:</label>
                <div class="float-right"><%= medicareProvider.Phone.ToPhone() %></div>
            </div>
        <% } %>
    <% } %>
            <div class="row">
                <label class="float-left">Earliest Billing Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("earliestBillingDate") != null ? node.SelectSingleNode("earliestBillingDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Latest Billing Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("latestBillingDate") != null ? node.SelectSingleNode("latestBillingDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Patient Status Code:</label>
                <div class="float-right"><%= node.SelectSingleNode("patientStatusCode") != null ? node.SelectSingleNode("patientStatusCode").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Valid Cancel Indicator:</label>
                <div class="float-right"><%= node.SelectSingleNode("validCancelIndicator") != null ? node.SelectSingleNode("validCancelIndicator").Value : string.Empty%></div>
            </div>
<% } %>

        </div>
<% } %>
    </fieldset>
    <fieldset><legend>Medicare Secondary Payer (MSP) Information</legend>
<%
    var medicareSecondaryPayerNodes = xPathNavigator.Select("hiqhResponse/medicareSecondaryPayers/medicareSecondaryPayers/medicareSecondaryPayer");
    foreach (System.Xml.XPath.XPathNavigator node in medicareSecondaryPayerNodes) {
%>
        <div class="column">
            <div class="row">
                <label class="float-left">Record:</label>
                <div class="float-right"><%= node.SelectSingleNode("record") != null ? node.SelectSingleNode("record").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">MSP Code:</label>
                <div class="float-right"><%= node.SelectSingleNode("mspCode") != null ? node.SelectSingleNode("mspCode").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">MSP Code Description:</label>
                <div class="float-right"><%= node.SelectSingleNode("mspCodeDescription") != null ? node.SelectSingleNode("mspCodeDescription").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Effective Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("effectiveDate") != null ? node.SelectSingleNode("effectiveDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Termination Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("terminationDate") != null ? node.SelectSingleNode("terminationDate").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Intermediary Number:</label>
                <div class="float-right"><%= node.SelectSingleNode("intermediaryNumber") != null ? node.SelectSingleNode("intermediaryNumber").Value : string.Empty%></div>
            </div>
            <div class="row">
                <label class="float-left">Accretion Date:</label>
                <div class="float-right"><%= node.SelectSingleNode("accretionDate") != null ? node.SelectSingleNode("accretionDate").Value : string.Empty%></div>
            </div>
        </div>
<% } %>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="U.GetAttachment('Patient/HiqhEligibilityPdf', { medicareNumber: '<%= Model.MedicareNumber %>', lastName: '<%= Model.LastName %>', firstName: '<%= Model.FirstName %>', dob: '<%= Model.Dob %>', gender: '<%= Model.Gender %>' })">Print</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a></li>
        </ul>
    </div>
<%} else { %>
   <fieldset>
        <div class="wide-column">
            <div class="row align-center">No Medicare Eligibility Report found for this patient!</div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a></li>
        </ul>
    </div>
<% } %>
</div>
