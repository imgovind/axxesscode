﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<span class="wintitle">Patient Vital Signs | <%= ViewData.ContainsKey("DisplayName") && ViewData["DisplayName"] != null ? ViewData["DisplayName"].ToString().Clean() : string.Empty %></span>
<%  var pagename = "Patient_VitalSignsCharts"; %>
<div id="<%= pagename %>_Log"><% Html.RenderPartial("VitalSigns/Report", Model); %></div>