﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.VisitDate.ToDateTime()).Take(9).ToList() : new List<VitalSign>(); %>
<%  var arrayResp = new List<double>(); %>
<%  var arrayRespDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.RespGraph > 0) { %>
        <%  arrayResp.Add(v.RespGraph); %>
        <%  arrayRespDate.Add(v.VisitDate); %>
    <%  } %>
<%  }); %>
<%  var respJson = arrayResp.ToJavascrptArray(); %>
<%  var dateRespJson = arrayRespDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Respiratory Rate
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Respiratory" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Respiratory Rate</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.VisitDate %></span>
                <span class="vitals-single-value"><%= item.Resp %></span>
                <span class="vitals-task"><%= item.DisciplineTask %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Respiratory").Graph({
        Type: "line",
        Title: "Respiratory Rate",
        YAxisTitle: "Resp Rate",
        XAxisData: <%= dateRespJson %>,
        YAxisData: [{ name: "Resp Rate", data: <%= respJson %> }]
    });
</script>