﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Grant Clinician Access | <%=Model.DisplayName %></span>
<% using (Html.BeginForm("AddUserAccess", "Patient", FormMethod.Post, new { @id = "AddUserAccess_Form"})) { %>
<%=Html.Hidden("PatientId", Model.Id, new { @id = "UserAccessPatientId" })%>
<div class="wrapper">
    <fieldset class="medication">
    <%= Html
            .Telerik()
            .Grid<SelectedUser>()
            .Name("List_AddPatientUserAccess")
            .ToolBar(commnds => commnds.Custom())
            .Columns(columns => {
                columns.Bound(u => u.UserId).Hidden(true);
                columns.Bound(u => u.Selected).Title("").ClientTemplate("<input name=\"selectedUser\"type=\"checkbox\" value=\"<#=UserId#>\"/>").Width(40);
                columns.Bound(u => u.DisplayName).Title("Clinician Name");
            })
            .DataBinding(dataBinding => dataBinding.Ajax().Select("GetAddUserAccess","Patient", new { patientId = Model.Id }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))
            .Footer(false) %>
    </fieldset>
    <div class="buttons">
        <ul>
            <li>
                <a href="javascript:void(0);" onclick="$('#AddUserAccess_Form').submit();">Add Selected</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Close</a>
            </li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#List_AddPatientUserAccess .t-grid-toolbar").html("")
    .append($("<div/>").GridSearch());
    $("#List_AddPatientUserAccess .t-grid-content").css({ height: "auto", top: 59});
</script>