﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Clinicians That Currently Have Access | <%=Model.DisplayName %></span>
<% using (Html.BeginForm("RemoveUserAccess", "Patient", FormMethod.Post, new { @id = "UserAccess_Form"})) { %>
<%=Html.Hidden("PatientId", Model.Id, new { @id = "UserAccessPatientId" })%>
<div class="wrapper">
<fieldset class="medication">
    <%= Html
            .Telerik()
            .Grid<SelectedUser>()
            .Name("List_PatientUserAccess")
            .ToolBar(commnds => commnds.Custom())
            .Columns(columns => {
                columns.Bound(u => u.UserId).Hidden(true);
                columns.Bound(u => u.Selected).Title("").ClientTemplate("<input name=\"selectedUsers\" type=\"checkbox\" value=\"<#=PatientUserId#>\"/>").Width(40);
                columns.Bound(u => u.DisplayName).Title("Clinician Name");
                columns.Bound(u => u.UserId).Title("Action").ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.UserAccess.RemoveUserAccess('<#=PatientUserId#>');\">Remove Access</a>").Width(120);
            })
            .DataBinding(dataBinding => dataBinding.Ajax().Select("GetUserAccess","Patient", new { patientId = Model.Id }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))
            .Footer(true)%>
</fieldset>
</div>
<div class= "buttons">
    <ul>
        <li>
            <a href="javascript:void(0);" onclick="$('#UserAccess_Form').submit();">Remove Selected</a>
        </li>
    </ul>
</div>
<% } %>
<script type="text/javascript">    
    $("#List_PatientUserAccess .t-grid-toolbar").html("")
    .append($("<div/>").GridSearch())
    .append($("<div/>").addClass("float-right").Buttons([{ Text: "Add Clinicians", Click: function() { UserInterface.ShowAddUserAccess($("#UserAccessPatientId")[0].value); } }]));
    $("#List_PatientUserAccess .t-grid-content").css({ height: "auto", top: 59});
</script>
