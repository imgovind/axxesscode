﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserNonVisitTaskRate>>" %>
<ul>
    <li><h3 class="ac strong">Task Pay Rates</h3></li>
    <li>
        <span class="grid-quarter">Task</span>
        <span class="grid-fifth"></span>
        <span class="grid-fifth">Rate Type</span>
        <span class="grid-tenth">User Rate</span>
        <span class="grid-tenth"></span>
        <span>Action</span>
    </li>
</ul>
<%  if (Model != null && Model.Count > 0) { %>
<ol>
    <%  foreach(UserNonVisitTaskRate item in Model) { %>
    <li>
        <span class="grid-quarter"><%= item.TaskTitle%></span>
        <span class="grid-fifth"></span>
        <span class="grid-fifth"><%= item.RateTypeName%></span>
        <span class="grid-tenth"><%= item.Rate%></span>
        <span class="grid-tenth"></span>
        <span>
            <a class="link edit-nonvisit-rate" guid="<%= item.Id %>">Edit</a>
            |
            <a class="link delete-nonvisit-rate" guid="<%= item.Id %>">Delete</a>
        </span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h1 class="blue">No Task Pay Rates Found</h1>
<%  } %>