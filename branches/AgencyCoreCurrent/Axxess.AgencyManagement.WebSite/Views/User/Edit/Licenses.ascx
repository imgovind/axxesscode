﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-license" guid="<%= Model.Id %>">New License</a></li>
        </ul>
        <ul class="fr">
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div id="EditUser_LicenseGrid" class="acore-grid" guid="<%= Model.Id %>"><% Html.RenderPartial("License/List", Model.LicensesArray); %></div>
</div>