﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Rate/Add", "User", FormMethod.Post, new { @id = "NewUserPayRate_Form" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "NewUserPayRate_UserId" })%>
    <fieldset>
        <legend>New Pay Rate</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewUserPayRate_Insurance" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", string.Empty, false, new { @id = "NewUserPayRate_Insurance", @class = "Insurances required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_Task" class="fl strong">Task</label>
                <div class="fr"><%= Html.UserDisciplineTask("Id", 0, Model, string.Empty, true, new { @id = "NewUserPayRate_Task", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_RateType" class="fl strong">Rate Type</label>
                <div class="fr"><%= Html.UserRateTypes("RateType", "0", new { @id = "NewUserPayRate_RateType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_VisitRate" class="fl strong">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", string.Empty, new { @id = "NewUserPayRate_VisitRate", @class = "floatnum required", @maxlength = "8" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_MileageRate" class="fl strong">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", string.Empty, new { @id = "NewUserPayRate_MileageRate", @class = "floatnum", @maxlength = "8" })%></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>