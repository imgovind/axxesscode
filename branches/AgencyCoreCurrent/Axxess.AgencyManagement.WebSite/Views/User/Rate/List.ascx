﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserRate>>" %>
<ul>
    <li><h3 class="ac strong">Pay Rates</h3></li>
    <li>
        <span class="grid-quarter">Task</span>
        <span class="grid-fifth">Insurance</span>
        <span class="grid-fifth">Rate Type</span>
        <span class="grid-tenth">User Rate</span>
        <span class="grid-tenth">Mileage Rate</span>
        <span>Action</span>
    </li>
</ul>
<%  if (Model != null && Model.Count() > 0) { %>
<ol>
    <%  foreach(var rate in Model) { %>
    <li>
        <span class="grid-quarter"><%= rate.DisciplineTaskName %></span>
        <span class="grid-fifth"><%= Html.InsuranceName(rate.Insurance) %></span>
        <span class="grid-fifth"><%= rate.RateTypeName %></span>
        <span class="grid-tenth"><%= rate.Rate %></span>
        <span class="grid-tenth"><%= rate.MileageRate %></span>
        <span>
            <a class="link edit-rate" guid="<%= rate.Id %>" insurance="<%= rate.Insurance %>">Edit</a>
            |
            <a class="link delete-rate" guid="<%= rate.Id %>" insurance="<%= rate.Insurance %>">Delete</a>
        </span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h1 class="blue">No Pay Rates Found</h1>
<%  } %>