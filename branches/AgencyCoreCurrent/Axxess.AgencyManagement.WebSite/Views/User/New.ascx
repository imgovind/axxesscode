﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SubscriptionPlanViewData>" %>
<span class="wintitle">New User | <%= Current.AgencyName %></span>
<div id="newUserContent" class="wrapper main">
<%  using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "NewUser_Form" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewUser_EmailAddress" class="fl strong">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewUser_EmailAddress", @class = "email required", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewUser_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewUser_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_MiddleName" class="fl strong">Middle Initial</label>
                <div class="fr"><%= Html.TextBox("MiddleName", string.Empty, new { @id = "NewUser_MiddleName", @class = "mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewUser_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_Suffix" class="fl strong">Suffix</label>
                <div class="fr"><%= Html.TextBox("Suffix", string.Empty, new { @id = "NewUser_Suffix" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr">
                    <%= Html.RadioButton("Profile.Gender", "Female", false, new { @id = "NewUser_Gender_F", @class = "required radio" })%>
                    <label for="NewUser_Gender_F" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Profile.Gender", "Male", false, new { @id = "NewUser_Gender_M", @class = "required radio" })%>
                    <label for="NewUser_Gender_M" class="inline-radio">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_DOB" class="fl strong">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker" name="DOB" id="NewUser_DOB" /></div>
            </div>
            <div class="row">
                <label for="NewUser_Credentials" class="fl strong">Credentials</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", string.Empty, new { @id = "NewUser_Credentials", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUser_CredentialsMore">
                    <label for="NewUser_OtherCredentials"><em>(Specify)</em></label>
                    <%= Html.TextBox("CredentialsOther", string.Empty, new { @id = "NewUser_OtherCredentials", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_TitleType" class="fl strong">Title</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", string.Empty, new { @id = "NewUser_TitleType", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUser_TitleTypeMore">
                    <label for="NewUser_OtherTitleType"><em>(Specify)</em></label>
                    <%= Html.TextBox("TitleTypeOther", string.Empty, new { @id = "NewUser_OtherTitleType", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Employment Type</label>
                <div class="fr">
                    <%= Html.RadioButton("EmploymentType", "Employee", new { @id = "NewUser_EmploymentType_E", @class = "required radio" })%>
                    <label for="NewUser_EmploymentType_E" class="inline-radio">Employee</label>
                    <%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "NewUser_EmploymentType_C", @class = "required radio" })%>
                    <label for="NewUser_EmploymentType_C" class="inline-radio">Contractor</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Hire Date</label>
                <div class="fr">
                    <input type="text" class="date-picker" name="HireDate" id="NewUser_HireDate" value="<%=DateTime.Now.ToShortDateString() %>" />
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Date of Termination</label>
                <div class="fr"><input type="text" class="date-picker" name="TerminationDate" id="NewUser_TerminationDate" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewUser_SSN" class="fl strong">Social Security Number</label>
                <div class="fr"><%= Html.TextBox("SSN", string.Empty, new { @id = "NewUser_SSN", @class = "digits", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_CustomId" class="fl strong">Agency Custom Employee Id</label>
                <div class="fr"><%= Html.TextBox("CustomId", "", new { @id = "NewUser_CustomId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_ClinicianProviderId" class="fl strong">Clinician Provider NPI#</label>
                <div class="fr"><%= Html.TextBox("ClinicianProviderId", "", new { @id = "NewUser_ClinicianProviderId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_LocationId" class="fl strong">Agency Branch</label>
                <%  var htmlAttributes = new Dictionary<string, string>(); %>
                <%  htmlAttributes.Add("id", "NewUser_LocationId"); %>
                <%  htmlAttributes.Add("multiple", "multiple"); %>
                <div class="fr required"><%= Html.MultiSelectBranchList("LocationList", null, htmlAttributes)%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", string.Empty, new { @id = "NewUser_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", string.Empty, new { @id = "NewUser_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", string.Empty, new { @id = "NewUser_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", string.Empty, new { @id = "NewUser_AddressStateCode", @class = "state" })%>
                <%= Html.TextBox("Profile.AddressZipCode", string.Empty, new { @id = "NewUser_AddressZipCode", @class = "numeric zip", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_HomePhoneArray1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("HomePhoneArray", string.Empty, new { @id = "NewUser_HomePhoneArray1", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", string.Empty, new { @id = "NewUser_HomePhoneArray2", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", string.Empty, new { @id = "NewUser_HomePhoneArray3", @class = "numeric phone_long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_MobilePhoneArray1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("MobilePhoneArray", string.Empty, new { @id = "NewUser_MobilePhoneArray1", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", string.Empty, new { @id = "NewUser_MobilePhoneArray2", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", string.Empty, new { @id = "NewUser_MobilePhoneArray3", @class = "numeric phone_long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_FaxPhoneArray1" class="fl strong">Fax Phone</label>
                <div class="fr">
                    <%= Html.TextBox("FaxPhoneArray", string.Empty, new { @id = "NewUser_FaxPhoneArray1", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", string.Empty, new { @id = "NewUser_FaxPhoneArray2", @class = "numeric phone_short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", string.Empty, new { @id = "NewUser_FaxPhoneArray3", @class = "numeric phone_long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Duplicate Roles/Permissions/Restrictions from Previous User</legend>
        <div class="column">
            <div class="row">
                <label for="NewUser_Previous" class="fl strong">Duplicate from</label>
                <div class="fr"><%= Html.Users("PreviousUser", string.Empty, new { @id = "NewUser_Previous" })%></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Partial("NewContent", new User()) %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="NewUser_Comments" name="Comments" maxcharacters="500" class="taller"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add User</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
(function() {
    var Model = <%= Model.ToJson() %>, Plans = {}, Messages = {};
    for (var i=0; i<Model.Plans.length; i++) {
        Plans[Model.Plans[i].Key] = Model.Plans[i].Value;
    }
    var NewUserWindow = $('#window_newuser');
    NewUserWindow.delegate('input[name="LocationList"]', 'change', function(event) {
        var el = $(event.target),
            LocationId = el.val(),
            checked = el.prop('checked'),
            LocationName = el.parent().text();
        if (checked && Plans[LocationId]) {
            if (Plans[LocationId].Count+1 > Plans[LocationId].Max) {
                Messages[LocationId] = 
                    '<p style="margin-bottom:.5em">You have reached your maximum number of user accounts for the <b>' +
                    Plans[LocationId].CurrentPlan + 
                    '</b> user subscription plan at <b>' + LocationName + '</b>.<br>Creating this user will automatically upgrade your subscription to the <b>' +
                    Plans[LocationId].NextPlan + '</b> user subscription plan.</p>';
            }
        } else {
            // Remove Message
            delete Messages[LocationId];
        }
        var NewUserContent = NewUserWindow.find('#newUserContent'),
            UpgradeNotice = NewUserContent.find('.upgradeNotice');
        if (!UpgradeNotice.length) {
            UpgradeNotice = $('<div class="upgradeNotice" style="'+
                'background-color: #fccac3;'+
                'border:.1em solid #dbb3ad;'+
                'border-radius:.3em;'+
                'color:#594542;'+
                'padding:1em;'+
                'margin:10px;'+
                'font-size:1.1em"></div>');
            NewUserContent.prepend(UpgradeNotice);
        }
        UpgradeNotice.empty();
        if ($.isEmptyObject(Messages)) {
            UpgradeNotice.remove();
        } else {
            for (var key in Messages) {
                UpgradeNotice.append(Messages[key]);
            }
        }
    });
})()
   </script>