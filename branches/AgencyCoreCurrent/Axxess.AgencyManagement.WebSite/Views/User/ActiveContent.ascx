﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<% var activeAction = string.Empty;
   var visible = false;
   if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen)
   {
       visible = true;
       activeAction = "<a href=\"javascript:void(0);\" onclick=\"User.Edit('<#=Id#>')\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Deactivate('<#=Id#>');\">Deactivate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('<#=Id#>');\">Delete</a>";
   }    
 %>
 <%= Html.Telerik().Grid(Model).Name("List_User")
    .HtmlAttributes(new {@style ="top:55px;bottom:13px"})
    .ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.Id).Template(u => string.Format("<input name='UserId' type='checkbox' value='{0}'/>", u.Id)).Title("").Width(4).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20);
    columns.Bound(u => u.DisplayTitle).Title("Title").Sortable(true).Width(15);
    columns.Bound(u => u.EmailAddress).Template(u => string.Format("<a href='mailto:{0}'>{0}</a>", u.EmailAddress)).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(20).Sortable(true);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(8);
    columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(8);
    columns.Bound(u => u.EmploymentType).Sortable(true).Width(10);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(8);
    columns.Bound(u => u.Profile.Gender).Title("Gender").Width(6).Sortable(true);
    columns.Bound(u => u.Created).Format("{0:MM/dd/yyyy}").Title("Created").Width(8).Sortable(true);
    columns.Bound(u => u.Comments).Template(u => string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\">{0}</a>", u.Comments)).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=Comments#></a>").Title("").Sortable(false).Width(3);
    columns.Bound(u => u.Id).Width(15).Sortable(false).Template(u => visible ? string.Format("<a href=\"javascript:void(0);\" onclick=\"User.Edit('{0}')\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Deactivate('{0}');\">Deactivate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('{0}');\">Delete</a>",u.Id) : string.Empty).ClientTemplate(activeAction).Visible(visible).Title("Action");
    }).ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound"))
 .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "User", new { status = (int)UserStatus.Active }))
       .Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(user =>
       {
           var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
           var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
           if (sortName == "DisplayName")
           {
               if (sortDirection == "ASC") user.Add(u => u.DisplayName).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.DisplayName).Descending();
           }
           else if (sortName == "DisplayTitle")
           {
               if (sortDirection == "ASC") user.Add(u => u.DisplayTitle).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.DisplayTitle).Descending();
           }
           else if (sortName == "EmailAddress")
           {
               if (sortDirection == "ASC") user.Add(u => u.EmailAddress).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.EmailAddress).Descending();
           }
           else if (sortName == "EmploymentType")
           {
               if (sortDirection == "ASC") user.Add(u => u.EmploymentType).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.EmploymentType).Descending();
           }
           else if (sortName == "Gender")
           {
               if (sortDirection == "ASC") user.Add(u => u.Profile.Gender).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.Profile.Gender).Descending();
           }
           else if (sortName == "Created")
           {
               if (sortDirection == "ASC") user.Add(u => u.Created).Ascending();
               else if (sortDirection == "DESC") user.Add(u => u.Created).Descending();
           }
       }))
 .Sortable().Footer(false)
 .Scrollable(scrolling => scrolling.Enabled(true))

%>
<div id="DeleteMultiple_BottomPanel" style="position:absolute;right:0;bottom:0;left:0;background-color:#C3D8F1;" >
<div class="buttons" >
    <% if(!Current.IsAgencyFrozen) { %>
    <ul>
        <li><a href="javascript:void(0);" onclick='User.MultiActiveDelete();'>Delete</a></li>
        <li><a href="javascript:void(0);" onclick='User.MultiDeactivate();'>Deactivate</a></li>
    </ul>
    <% } %>
    </div> 

<script type="text/javascript">
    $('#List_User').css({"background-color": "#C3D8F1"});
    $("#List_User .t-grid-toolbar").html("");
    $("#List_User .t-grid-content").css({ 'height': 'auto','background-color': '#FFFFFF' });
    
    
   
   var newUser = "", activeExport = "";
<% var newUser = string.Empty; %>
<% var activeExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ManageUsers) && !Current.IsAgencyFrozen) { %>
    newUser = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewUser(); return false;%22%3ENew User%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    activeExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22U.GetAttachment('Export/ActiveUsers', {});%22%3EExcel Export (Active)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_User .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E" + newUser + "%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EActive Users%3C/div%3E%3Cdiv id=%22ActiveUser_Search%22%3E%3C/div%3E" + activeExport + "%3C/div%3E"));
 $("#ActiveUser_Search").append($("<div/>").GridSearchById("#List_User"));
    $(".grid-search").css("margin-left","-370px");
 $("#List_User div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "User.RebindActiveUser('" + U.ParameterByName(link, 'List_User-orderBy') + "');");
    });
    Agency.ToolTip("#List_User");
</script>
