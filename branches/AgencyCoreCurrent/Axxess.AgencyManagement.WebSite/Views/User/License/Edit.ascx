<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<License>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("License/Update", "User", FormMethod.Post, new { @id = "EditUserLicense_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUserLicense_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserLicense_UserId" })%>
    <fieldset>
        <legend>Edit License</legend>
        <div class="wide-column">
            <div class="row">
                <label for="EditUserLicense_Type" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", Model.LicenseType, new { @id = "EditUserLicense_Type", @class = "required notzero" })%></div>
                <div class="clr"></div>
                <div class="fr" id="EditUserLicense_TypeMore">
                    <label for="EditUserLicense_TypeOther"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", Model.OtherLicenseType, new { @id = "EditUserLicense_TypeOther", @maxlength = "25" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUserLicense_LicenseNumber" class="fl strong">License Number</label>
                <div class="fr"><input type="text" name="LicenseNumber" id="EditUserLicense_LicenseNumber" value="<%= Model.LicenseNumber %>" /></div>
            </div>
            <div class="row">
                <label for="EditUserLicense_InitDate" class="fl strong">Issue Date</label>
                <div class="fr"><input type="text" class="date-picker" name="InitiationDate" id="EditUserLicense_InitDate" value="<%= Model.InitiationDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label for="EditUserLicense_ExpDate" class="fl strong">Expiration Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpirationDate" id="EditUserLicense_ExpDate" value="<%= Model.ExpirationDate.ToShortDateString() %>" /></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%  } %>
</div>