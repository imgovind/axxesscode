﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Link Account - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="linkaccount-wrapper">
        <div id="linkaccount-window">
            <% if (Model.UserId != Guid.Empty) { %>
                <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Account Link</span></div>
                <div class="box">
                    <div id="messages"></div>
                    <h1>Axxess&#8482; Home Health Management System Account Link.</h1>
                    <%= string.Format("{0} has been linked to your account.", Model.AgencyName) %>
                    <div class="row">&#160;</div>
                    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Link_User_Id" })%>
                    <%= Html.Hidden("LoginId", Model.LoginId, new { @id = "Link_User_LoginId" })%>
                    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Link_User_AgencyId" })%>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Name) %>
                        <%= Model.Name %>
                    </div>
                    <div class="row">
                        <%= Html.LabelFor(a => a.EmailAddress) %>
                        <%= Model.EmailAddress %>
                    </div>
                    <div class="row">
                        <%= Html.LabelFor(a => a.AgencyName)%>
                        <%= Model.AgencyName %>
                    </div>
                    <div class="row">&#160;</div>
                    <div class="row">Note: Your Password and Electronic Signature will remain the same.</div>
                    <div class="bottom-right">
                        <input type="button" onclick="window.location.replace('/Login');" value="Login" class="button" style="clear: both; width: 120px!important;" />
                    </div>
                </div>
            <% } else { %>
                <div class="box-header"><span class="img icon axxess"></span><span class="title">The page you requested was not found.</span></div>
                <div class="box">
                    <div class="notification warning">You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.
                    </div>
                </div>
            <% } %>
        </div>
    </div>
    <div id="Agency_Selection_Container" class="agencySelection hidden"></div>
    <% Html.Telerik().ScriptRegistrar().jQuery(false)
         .DefaultGroup(group => group
             .Add("jquery-1.7.1.min.js")
             .Add("Plugins/Other/blockui.min.js")
             .Add("Plugins/Other/form.min.js")
             .Add("Plugins/Other/validate.min.js")
             .Add("Plugins/Other/jgrowl.min.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
             .Compress(true).Combined(true)
             .CacheDurationInDays(1)
             .Version(Current.AssemblyVersion))
        .OnDocumentReady(() =>
        { 
    %>
    Link.Init();
    <% 
        }).Render(); %>
</body>
</html>