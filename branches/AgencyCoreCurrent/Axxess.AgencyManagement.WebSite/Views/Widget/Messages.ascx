﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="mail-controls">
    <span class="float-left">
        <a href="javascript:void(0);" onclick="Message.Inbox();">Inbox</a> 
<%  if (!Current.IsAgencyFrozen) { %> 
        &#8211;
        <a href="javascript:void(0);" onclick="Message.New();">Compose Mail</a>
<%  } %>
    </span>
<%  if (!Current.IsAgencyFrozen) { %> 
    <select class="float-right">
        <option>Actions...</option>
        <option>Delete</option>
    </select>
<%  } %>
    <div class="clear"></div>
</div>
<%  using (Html.BeginForm("DeleteMany", "Message", FormMethod.Post, new { @id = "MessageWidget_Form" })) { %>
<div style="position:relative;height:181px" id="messages-widget-content"></div>
<%  } %>
<div onclick="Message.Inbox()" class="widget-more"><a href="javascript:void(0);">More &#187;</a></div>
