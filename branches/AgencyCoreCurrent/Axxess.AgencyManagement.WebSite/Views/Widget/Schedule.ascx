﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th style="width: 40%;">Patient Name</th>
				<th style="width: 40%;">Task</th>
				<th style="width: 20%;">Date</th>
			</tr>
        </thead>
	    <tbody id="scheduleWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div onclick="UserInterface.ShowUserTasks();" id="userScheduleWidgetMore" class="widget-more"><a href="javascript:void(0);">More &#187;</a></div>
