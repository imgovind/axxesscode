﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCare>" %>
<% var patient = Model.PatientData.IsNotNullOrEmpty() ? Model.PatientData.ToObject<Patient>() : null; %>
<span class="wintitle">Edit 485 - Plan of Care (From Assessment) | <%= patient != null ? patient.DisplayName : "" %></span>
<% using (Html.BeginForm("Save485", "Oasis", FormMethod.Post, new { @id = "edit485Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_485_Id" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_485_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_485_PatientId" })%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="3">
                    Plan of Care
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="3" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">Return Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td>
                    <label for="Edit_485_PatientName" class="float-left">Patient&#8217;s Name</label>
                    <div class="float-right"><input readonly="readonly" id="Edit_485_PatientName" value="<%= patient != null ? patient.DisplayName : "" %>" /></div>
                    <div class="clear"></div>
                    <label for="Edit_485_EpisodePeriod" class="float-left">Episode Period</label>
                    <div class="float-right"><input readonly="readonly" id="Edit_485_EpisodePeriod" value="<%= Model != null && Model.EpisodeStart.IsNotNullOrEmpty() && Model.EpisodeEnd.IsNotNullOrEmpty() ? string.Format("{0} - {1}", Model.EpisodeStart, Model.EpisodeEnd) : "" %>" /></div>
                </td>
                <td>
                <% if (ViewData.ContainsKey("IsLinkedToAssessment") && ViewData["IsLinkedToAssessment"].ToString().ToBoolean()) { %>
                    <em>Click on the button below to refresh this page from the OASIS Assessment associated with this episode.</em>
                    <div class="buttons">
                        <ul>
                            <li>
                                <a href="javascript:void(0);" onclick="UserInterface.RefreshPlanofCare('<%= Model.EpisodeId  %>','<%= Model.PatientId  %>','<%= Model.Id  %>'); ">Refresh Data</a>
                            </li>
                        </ul>
                    </div>
                    <% } else { %>
                        <em>This Plan of Care is not associated to an OASIS Assessment.</em>
                    <% } %>
                </td>
                <td>
                    <label for="Edit_485_PhysicianId" class="float-left">Physician:</label>
                    <div class="float-right"><%= Html.TextBox("PhysicianId", Model != null ? Model.PhysicianId.ToString() : string.Empty, new { @id = "Edit_485_PhysicianId", @class = "Physicians requiredphysician" })%></div>
                    <div class="clear"></div>
                    <div class="float-right ancillary-button align-left"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
                </td>
    
                
            </tr>
            
    <%  if ( Current.HasRight(Permissions.ViewPreviousNotes))
    { %>
    <%--fieldset>
        <legend>Previous Assessments</legend>--%>
    <tr>
    <td></td>
    <td >
        <div>
            <div class="row">
                <label class="float-left">Select Plan of Care:</label>
                <div class="float-right">
                    <%= Html.PreviousCarePlans(Model.PatientId, Model.Id, "HCFA485", Model.Created, new { @id = "Previous_485" })%>
                </div>
            </div>
        </div>
        <div class="">
            <div class="">
                <div class="buttons">
                    <ul>
                        <li><a href="javascript:void(0)" id="Previous_485_Button">Load Plan of Care</a></li>
                    </ul>
                </div>
            </div>
        </div>
        </td>
        <td></td>
        </tr>
    <%--</fieldset>--%>
<%  } %>

        </tbody>
    </table>

<div id="planofCareContentId"><% Html.RenderPartial("~/Views/Oasis/485/LocatorQuestions.ascx", Model); %></div>
<%= Html.Hidden("Status", "", new { @id = "Edit_485_Status" })%>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('110');PlanOfCare.Submit($(this),'editplanofcare');">Save</a></li>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('110');PlanOfCare.Submit($(this),'editplanofcare');">Save &#38; Close</a></li>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('115');PlanOfCare.Submit($(this),'editplanofcare');">Complete</a></li>
    <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editplanofcare');">Close</a></li>
</ul></div>
<% } %>
</div>
<script type="text/javascript">
    $("#Previous_485_Button").click(function() {
        var previousAssessmentArray = $("#Previous_485").val().split('_');
        if (previousAssessmentArray.length > 1) {
            if (confirm("Are you sure you want to load this plan of care?")) {
                var episodeId = previousAssessmentArray[1],
                    patientId = $("#Edit_485_PatientId").val(),
                    eventId = previousAssessmentArray[0];
                $("#planofCareContentId").Load("Oasis/PlanofCareContent", {
                    episodeId: episodeId,
                    patientId: patientId,
                    eventId: eventId
                }, function(result) {
                    U.Growl("Loading of the 485 has completed", "success");
                });
            }
        } else U.Growl("Please select a previous assessment first", "error");
    });
</script>
