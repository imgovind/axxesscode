<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] behaviorInterventions = data.AnswerArray("485BehaviorInterventions"); %>
<%  string[] behaviorGoals = data.AnswerArray("485BehaviorGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485BehaviorInterventions", " ", new { @id = Model.TypeName + "_485BehaviorInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions1' name='{0}_485BehaviorInterventions' value='1' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions1" class="radio">SN to notify physician this patient was screened for depression using PHQ-2 scale and meets criteria for further evaluation for depression.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions2' name='{0}_485BehaviorInterventions' value='2' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("2").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions2" class="radio">SN to perform a neurological assessment each visit.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions3' name='{0}_485BehaviorInterventions' value='3' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("3").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions3" class="radio">SN to assess/instruct on seizure disorder signs &#38; symptoms and appropriate actions during seizure activity.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions4' name='{0}_485BehaviorInterventions' value='4' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("4").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions4">SN to instruct the</label>
                        <%  var instructSeizurePrecautionPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructSeizurePrecautionPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions4">on seizure precautions.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions5' name='{0}_485BehaviorInterventions' value='5' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("5").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions5" class="radio">SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.</label>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT"|| Model.Discipline == "ST") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions7' name='{0}_485BehaviorInterventions' value='7' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("7").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions7" class="radio">Notify SN or Physician that this patient was screened for depression using the PHQ-2 scale and meets criteria for further evaluation for depression.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions8' name='{0}_485BehaviorInterventions' value='8' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("8").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorInterventions8" class="radio">SN to evaluate patient for signs and symptoms of depression.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions9' name='{0}_485BehaviorInterventions' value='9' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("9").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions9">MSW:</label>
                        <%= Html.Hidden(Model.TypeName + "_485MSWProviderServicesVisits", " ", new { @id = Model.TypeName + "_485MSWProviderServicesVisitsHidden" })%>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWProviderServicesVisits", "1", data.AnswerOrEmptyString("485MSWProviderServicesVisits").Equals("1"), new { @id = Model.TypeName + "_485MSWProviderServicesVisits1", @class = "no_float deselectable" })%>
                        <label for="<%= Model.TypeName %>_485MSWProviderServicesVisits1">1-2 OR</label>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWProviderServicesVisits", "0", data.AnswerOrEmptyString("485MSWProviderServicesVisits").Equals("0"), new { @id = Model.TypeName + "_485MSWProviderServicesVisits0", @class = "no_float deselectable" })%>
                        <%= Html.TextBox(Model.TypeName + "_485MSWProviderServicesVisitsAmount", data.AnswerOrEmptyString("485MSWProviderServicesVisitsAmount"), new { @id = Model.TypeName + "_485MSWProviderServicesVisitsAmount", @class = "zip", @maxlength = "5" })%>
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions9">visits, every 60 days for provider services.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions10' name='{0}_485BehaviorInterventions' value='10' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("10").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions10">MSW:</label>
                        <%= Html.Hidden(Model.TypeName + "_485MSWLongTermPlanningVisits", " ", new { @id = Model.TypeName + "_485MSWLongTermPlanningVisitsHidden" })%>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWLongTermPlanningVisits", "1", data.AnswerOrEmptyString("485MSWLongTermPlanningVisits").Equals("1"), new { @id = Model.TypeName + "_485MSWLongTermPlanningVisits1", @class = "no_float deselectable" })%>
                        <label for="<%= Model.TypeName %>_485MSWLongTermPlanningVisits1">1-2 OR</label>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWLongTermPlanningVisits", "0", data.AnswerOrEmptyString("485MSWLongTermPlanningVisits").Equals("0"), new { @id = Model.TypeName + "_485MSWLongTermPlanningVisits0", @class = "no_float deselectable" })%>
                        <%= Html.TextBox(Model.TypeName + "_485MSWLongTermPlanningVisitsAmount", data.AnswerOrEmptyString("485MSWLongTermPlanningVisitsAmount"), new { @id = Model.TypeName + "_485MSWLongTermPlanningVisitsAmount", @class = "zip", @maxlength = "5" })%>
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions10">visits, every 60 days for long term planning.</label>
                    </span>
                </div>
<%  } %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions6' name='{0}_485BehaviorInterventions' value='6' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("6").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions6">MSW:</label>
                        <%= Html.Hidden(Model.TypeName + "_485MSWCommunityAssistanceVisits", " ", new { @id = Model.TypeName + "_485MSWCommunityAssistanceVisitsHidden" })%>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWCommunityAssistanceVisits", "1", data.AnswerOrEmptyString("485MSWCommunityAssistanceVisits").Equals("1"), new { @id = Model.TypeName + "_485MSWCommunityAssistanceVisits1", @class = "no_float deselectable" })%>
                        <label for="<%= Model.TypeName %>_485MSWCommunityAssistanceVisits1">1-2 OR</label>
                        <%= Html.RadioButton(Model.TypeName + "_485MSWCommunityAssistanceVisits", "0", data.AnswerOrEmptyString("485MSWCommunityAssistanceVisits").Equals("0"), new { @id = Model.TypeName + "_485MSWCommunityAssistanceVisits0", @class = "no_float deselectable" })%>
                        <%= Html.TextBox(Model.TypeName + "_485MSWCommunityAssistanceVisitAmount", data.AnswerOrEmptyString("485MSWCommunityAssistanceVisitAmount"), new { @id = Model.TypeName + "_485MSWCommunityAssistanceVisitAmount", @class = "zip", @maxlength = "5" })%>
                        <label for="<%= Model.TypeName %>_485BehaviorInterventions6">visits, every 60 days for community resource assistance.</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485BehaviorComments">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485BehaviorOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485BehaviorComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485BehaviorComments", data.AnswerOrEmptyString("485BehaviorComments"), 5, 70, new { @id = Model.TypeName + "_485BehaviorComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485BehaviorGoals", " ", new { @id = Model.TypeName + "_485BehaviorGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals1' name='{0}_485BehaviorGoals' value='1' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals1" class="radio">Neuro status will be within normal limits and free of S&#38;S of complications or further deterioration.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals2' name='{0}_485BehaviorGoals' value='2' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("2").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals2" class="radio">Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals3' name='{0}_485BehaviorGoals' value='3' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("3").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals3" class="radio">Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals4' name='{0}_485BehaviorGoals' value='4' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("4").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals4" class="radio">Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals5' name='{0}_485BehaviorGoals' value='5' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("5").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals5" class="radio">Patient will remain free from increased confusion during the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals6' name='{0}_485BehaviorGoals' value='6' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("6").ToChecked())%>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485BehaviorGoals6">The</label>
                        <%  var verbalizeSeizurePrecautionsPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485VerbalizeSeizurePrecautionsPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                        <label for="<%= Model.TypeName %>_485BehaviorGoals6">will verbalize understanding of seizure precautions.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals7' name='{0}_485BehaviorGoals' value='7' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("7").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals7" class="radio">Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.</label>
                </div>
<%  } %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals8' name='{0}_485BehaviorGoals' value='8' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("8").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485BehaviorGoals8" class="radio">Patient&#8217;s community resource needs will be met with assistance of social worker.</label>
                </div>
            </div>
        </div>        
        <div class="row">
            <label for="<%= Model.TypeName %>_485BehaviorGoalComments">Additional Goals:</label>
            <%= Html.Templates(Model.TypeName + "_485BehaviorGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485BehaviorGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485BehaviorGoalComments", data.AnswerOrEmptyString("485BehaviorGoalComments"), 5, 70, new { @id = Model.TypeName + "_485BehaviorGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">    
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>