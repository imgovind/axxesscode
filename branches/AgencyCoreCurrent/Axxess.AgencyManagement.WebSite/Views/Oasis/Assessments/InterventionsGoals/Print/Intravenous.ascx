<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var IntegumentaryIVInterventions = data.AnswerArray("485IntegumentaryIVInterventions"); %>
<%  var IntegumentaryIVGoals = data.AnswerArray("485IntegumentaryIVGoals"); %>
<%  if (IntegumentaryIVInterventions.Length > 0 || (data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryIVInterventions.Contains("1")) { %>
    printview.checkbox("SN to instruct <%= data.AnswerOrDefault("485InstructInfectionSignsSymptomsPerson", "Patient/Caregiver")%> on signs and symptoms of infection and infiltration.",true) +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("2")) { %>
    printview.checkbox("SN to change central line dressing every <%= data.AnswerOrDefault("485ChangeCentralLineEvery", "<span class='blank'></span>")%> using sterile technique.",true) +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructChangeCentralLinePerson", "Patient/Caregiver")%> to change central line dressing every <%= data.AnswerOrDefault("485InstructChangeCentralLineEvery", "<span class='blank'></span>")%> using sterile technique.",true) +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("4")) { %>
    printview.checkbox("SN to change <%= data.AnswerOrDefault("485ChangePortDressingType", "<span class='blank'></span>")%> port dressing using sterile technique every <%= data.AnswerOrDefault("485ChangePortDressingEvery", "<span class='blank'></span>")%>.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty())
        { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryIVInterventionComments").Clean()%>") +
    <%  } %>
    "","IV Interventions");
<%  } %>
<%  if (IntegumentaryIVGoals.Length > 0 || (data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryIVGoals.Contains("1")) { %>
    printview.checkbox("IV will remain patent and free from signs and symptoms of infection.",true) +
    <%  } %>
    <%  if (IntegumentaryIVGoals.Contains("2")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485InstructChangeDress", "Patient/Caregiver")%> will demonstrate understanding of changing <%= data.AnswerOrDefault("485InstructChangeDressSterile", "<span class='blank'></span>")%> dressing using sterile technique.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryIVGoalComments") && data["485IntegumentaryIVGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryIVGoalComments").Clean()%>") +
    <%  } %>
    "","IV Goals");
<%  } %>