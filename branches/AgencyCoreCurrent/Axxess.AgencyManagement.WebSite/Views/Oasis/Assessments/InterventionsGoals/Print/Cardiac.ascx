<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var CardiacStatusInterventions = data.AnswerArray("485CardiacStatusInterventions"); %>
<%  var CardiacStatusGoals = data.AnswerArray("485CardiacStatusGoals"); %>
<%  if (CardiacStatusInterventions.Length > 0 || (data.ContainsKey("485CardiacInterventionComments") && data["485CardiacInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (CardiacStatusInterventions.Contains("1")) { %>
    printview.checkbox("SN to instruct on daily/weekly weights and recordings.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("2")) { %>
    printview.checkbox("SN to perform weekly weights.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct on application of <%= data.AnswerOrDefault("485ApplicationWrap","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("4")) { %>
    printview.checkbox("SN to instruct patient on daily weight self-monitoring program, and to report weight <%= data.AnswerOrEmptyString("485WeightSelfMonitorGain")%> of <%= data.AnswerOrDefault("485WeightSelfMonitorGainDay","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> lbs/day, <%= data.AnswerOrDefault("485WeightSelfMonitorGainWeek","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %> lbs/week.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("5")) { %>
    printview.checkbox("SN to assess patient&#8217;s weight log every visit.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("6")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructRecognizeCardiacDysfunctionPerson", "Patient/Caregiver") %> on measures to recognize cardiac dysfunction and relieve complications.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("7")) { %>
    printview.checkbox("SN to instruct patient on measures to detect and alleviate edema.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("8")) { %>
    printview.checkbox("SN to instruct patient when (s)he starts feeling chest pain, tightness, or squeezing in the chest to take nitroglycerin. Patient may take nitroglycerin one time every 5 minutes. If no relief after 3 doses, call 911.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("9")) { %>
    printview.checkbox("SN to instruct the patient the following symptoms could be signs of a heart attack: chest discomfort, discomfort in one or both arms, back, neck, jaw, stomach, shortness of breath, cold sweat, nausea, or dizziness. Instruct patient on signs and symptoms that necessitate calling 911.",true) +
    <%  } %>
    <%  if (CardiacStatusInterventions.Contains("10")) { %>
    printview.checkbox("No blood pressure or venipuncture in <%= data.AnswerOrDefault("485NoBloodPressureArm", "%3Cspan class=%22blank%22%3E%3C/span%3E") %> arm.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485CardiacInterventionComments") && data["485CardiacInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485CardiacInterventionComments").Clean() %>",false,2) +
    <%  } %>
    "","Cardiovascular Interventions");
<%  } %>
<%  if (CardiacStatusGoals.Length > 0 || (data.ContainsKey("485CardiacGoalComments") && data["485CardiacGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (CardiacStatusGoals.Contains("1")) { %>
    printview.checkbox("Patient weight will be maintained between <%= data.AnswerOrDefault("485WeightMaintainedMin","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>lbs and <%= data.AnswerOrDefault("485WeightMaintainedMax","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>lbs during the episode.",true) +
    <%  } %>
    <%  if (CardiacStatusGoals.Contains("2")) { %>
    printview.checkbox("Patient will remain free from chest pain, or chest pain will be relieved with nitroglycerin, during the episode.",true) +
    <%  } %>
    <%  if (CardiacStatusGoals.Contains("3")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485VerbalizeCardiacSymptomsPerson","Patient/Caregiver").Clean() %> will verbalize understanding of symptoms of cardiac complications and when to call 911 by: <%= data.AnswerOrDefault("485VerbalizeCardiacSymptomsDate","%3Cspan class=%22blank%22%3E%3C/span%3E").Clean() %>.",true) +
    <%  } %>
    <%  if (CardiacStatusGoals.Contains("4")) { %>
    printview.checkbox("The <%= data.AnswerOrDefault("485VerbalizeEdemaRelieverPerson","Patient/Caregiver").Clean() %> will verbalize and demonstrate edema-relieving measures by the episode.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485CardiacGoalComments") && data["485CardiacGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485CardiacGoalComments").Clean() %>",false,2) +
    <%  } %>
    "","Cardiovascular Goals");
<%  } %>