<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var SensoryInterventions = data.AnswerArray("485SensoryStatusIntervention"); %>
<%  if (SensoryInterventions.Length > 0 || (data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (SensoryInterventions.Contains("1")) { %>
    printview.checkbox("ST to evaluate.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485SensoryStatusInterventionComments") && data["485SensoryStatusInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SensoryStatusInterventionComments").Clean()%>") +
    <%  } %>
    "","Sensory Interventions");
<%  } %>
<%  if (data.ContainsKey("485SensoryStatusGoalComments") && data["485SensoryStatusGoalComments"].Answer.IsNotNullOrEmpty()) { %>
printview.addsection(
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SensoryStatusGoalComments").Clean()%>"),
    "Sensory Status Goals");
<%  } %>