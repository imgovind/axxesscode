<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%  string[] eliminationInterventions = data.AnswerArray("485EliminationInterventions"); %>
    <%= Html.Hidden(Model.TypeName + "_485EliminationInterventions", "", new { @id = Model.TypeName + "_485EliminationInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions1' name='{0}_485EliminationInterventions' value='1' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions1" class="radio">SN to instruct on establishing bowel regimen.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions9' name='{0}_485EliminationInterventions' value='9' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("9").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions9" class="radio">SN to instruct on establishing bladder regimen.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions2' name='{0}_485EliminationInterventions' value='2' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions2" class="radio">SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions3' name='{0}_485EliminationInterventions' value='3' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions3" class="radio">SN to instruct on care of stoma, surrounding skin and use of skin barrier.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions4' name='{0}_485EliminationInterventions' value='4' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("4").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions4" class="radio">SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions5' name='{0}_485EliminationInterventions' value='5' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("5").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions5" class="radio">SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions6' name='{0}_485EliminationInterventions' value='6' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("6").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationInterventions6">SN to change catheter every month and PRN using sterile technique</label>
                        <%= Html.TextBox(Model.TypeName + "_485EliminationFoleyCatheterType", data.AnswerOrEmptyString("485EliminationFoleyCatheterType"), new { @id = Model.TypeName + "_485EliminationFoleyCatheterType" })%>
                        <label for="<%= Model.TypeName %>_485EliminationInterventions6">Fr. catheter.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions7' name='{0}_485EliminationInterventions' value='7' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("7").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationInterventions7" class="radio">SN to instruct on intermittent catheterizations.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions8' name='{0}_485EliminationInterventions' value='8' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("8").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationInterventions8">SN to perform intermittent catheterization every</label>
                        <%= Html.TextBox(Model.TypeName + "_485EliminationCatheterizationNumber", data.AnswerOrEmptyString("485EliminationCatheterizationNumber"), new { @id = Model.TypeName + "_485EliminationCatheterizationNumber" })%>
                        <label for="<%= Model.TypeName %>_485EliminationInterventions8">&#38; PRN using sterile technique.</label>
                    </span>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT" || Model.Discipline == "ST")
    { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions10' name='{0}_485EliminationInterventions' value='8' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("8").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationInterventions10">No blood pressure in</label>
                        <%= Html.TextBox(Model.TypeName + "_485EliminationBloodPressureArm", data.AnswerOrEmptyString("485EliminationBloodPressureArm"), new { @id = Model.TypeName + "_485EliminationBloodPressureArm" })%>
                        <label for="<%= Model.TypeName %>_485EliminationInterventions10">arm.</label>
                    </span>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EliminationOrderTemplates" class="strong">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485CardiacOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485EliminationInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485EliminationInterventionComments", data.AnswerOrEmptyString("485EliminationInterventionComments"), 2, 70, new { @id = Model.TypeName + "_485EliminationInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%  string[] eliminationGoals = data.AnswerArray("485EliminationGoals"); %>
    <%= Html.Hidden(Model.TypeName + "_485EliminationGoals", "", new { @id = Model.TypeName + "_485EliminationGoalsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals1' name='{0}_485EliminationGoals' value='1' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationGoals1" class="radio">Foley will remain patent during this episode and patient will be free of signs and symptoms of UTI.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals2' name='{0}_485EliminationGoals' value='2' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationGoals2" class="radio">Suprapubic tube will remain patent during this episode and patient will be free of signs and symptoms of UTI.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals3' name='{0}_485EliminationGoals' value='3' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationGoals3" class="radio">Patient will be without signs/symptoms of UTI (pain, foul odor, cloudy or blood-tinged urine and fever) during this episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals4' name='{0}_485EliminationGoals' value='4' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("4").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationGoals4">The</label>
                        <%  var eliminationOstomyManagementIndependent = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485EliminationOstomyManagementIndependent", "Patient/Caregiver")); %>
                        <%= Html.DropDownList(Model.TypeName + "_485EliminationOstomyManagementIndependent", eliminationOstomyManagementIndependent)%>
                        <label for="<%= Model.TypeName %>_485EliminationGoals4">will be independent in ostomy management by:</label>
                         <%= Html.TextBox(Model.TypeName + "_485EliminationOstomyManagementIndependentDate", data.AnswerOrEmptyString("485EliminationOstomyManagementIndependentDate"), new { @id = Model.TypeName + "_485EliminationOstomyManagementIndependentDate", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals5' name='{0}_485EliminationGoals' value='5' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("5").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationGoals5" class="radio">Patient will be free from signs and symptoms of constipation during the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals6' name='{0}_485EliminationGoals' value='6' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("6").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationGoals6">The</label>
                        <%  var eliminationAcidFoodVerbalizedGivenTo = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485EliminationAcidFoodVerbalizedGivenTo", "Patient/Caregiver")); %>
                        <%= Html.DropDownList(Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenTo", eliminationAcidFoodVerbalizedGivenTo)%>
                        <label for="<%= Model.TypeName %>_485EliminationGoals6">will be independent in ostomy management by:</label>
                         <%= Html.TextBox(Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenToDate", data.AnswerOrEmptyString("485EliminationAcidFoodVerbalizedGivenToDate"), new { @id = Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenToDate", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals7' name='{0}_485EliminationGoals' value='7' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("7").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485EliminationGoals7">Patient will verbalize understanding not to eat 4 hours before bedtime to reduce acid reflux/indigestion by:</label>
                         <%= Html.TextBox(Model.TypeName + "_485EliminationEatingTimeVerbalizedGivenDate", data.AnswerOrEmptyString("485EliminationEatingTimeVerbalizedGivenDate"), new { @id = Model.TypeName + "_485EliminationEatingTimeVerbalizedGivenDate", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='' id='{0}_485EliminationGoals8' name='{0}_485EliminationGoals' value='8' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("8").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EliminationGoals8" class="radio">Patient will not develop any signs and symptoms of dehydration during the episode.</label>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EliminationGoalsComments" class="float-left">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485EliminationGoalsTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485EliminationGoalsComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485EliminationGoalsComments", data.AnswerOrEmptyString("485EliminationGoalsComments"), 5, 70, new { @id = Model.TypeName + "_485EliminationGoalsComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>