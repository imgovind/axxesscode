<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
<%  var data = Model.ToDictionary(); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%  string[] integumentaryIVInterventions = data.AnswerArray("485IntegumentaryIVInterventions"); %>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryIVInterventions", "", new { @id = Model.TypeName + "_485IntegumentaryIVInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions1' name='{0}_485IntegumentaryIVInterventions' value='1' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("1").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions1">SN to instruct the</label>
                        <%  var instructInfectionSignsSymptomsPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructInfectionSignsSymptomsPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructInfectionSignsSymptomsPerson", instructInfectionSignsSymptomsPerson)%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions1">on signs and symptoms of infection and infiltration.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions2' name='{0}_485IntegumentaryIVInterventions' value='2' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("2").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions2">SN to change central line dressing every</label>
                        <%= Html.TextBox(Model.TypeName + "_485ChangeCentralLineEvery", data.AnswerOrEmptyString("485ChangeCentralLineEvery"), new { @id = Model.TypeName + "_485ChangeCentralLineEvery", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions2">using sterile technique.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions3' name='{0}_485IntegumentaryIVInterventions' value='3' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("3").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">SN to instruct the</label>
                        <%  var instructChangeCentralLinePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructChangeCentralLinePerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructChangeCentralLinePerson", instructChangeCentralLinePerson)%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">to change central line dressing every</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructChangeCentralLineEvery", data.AnswerOrEmptyString("485InstructChangeCentralLineEvery"), new { @id = Model.TypeName + "_485InstructChangeCentralLineEvery", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">using sterile technique.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions4' name='{0}_485IntegumentaryIVInterventions' value='4' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("4").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions4">SN to change</label>
                        <%= Html.TextBox(Model.TypeName + "_485ChangePortDressingType", data.AnswerOrEmptyString("485ChangePortDressingType"), new { @id = Model.TypeName + "_485ChangePortDressingType", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions4">port dressing using sterile technique every</label>
                        <%= Html.TextBox(Model.TypeName + "_485ChangePortDressingEvery", data.AnswerOrEmptyString("485ChangePortDressingEvery"), new { @id = Model.TypeName + "_485ChangePortDressingEvery", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventionComments" class="strong">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485IntegumentaryIVOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485IntegumentaryIVInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485IntegumentaryIVInterventionComments", data.AnswerOrEmptyString("485IntegumentaryIVInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485IntegumentaryIVInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%  string[] integumentaryIVGoals = data.AnswerArray("485IntegumentaryIVGoals"); %>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryIVGoals", "", new { @id = Model.TypeName + "_485IntegumentaryIVGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485IntegumentaryIVGoals1' type='checkbox' name='{0}_485IntegumentaryIVGoals' value='1' {1} />", Model.TypeName, integumentaryIVGoals.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals1" class="radio">IV will remain patent and free from signs and symptoms of infection.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485IntegumentaryIVGoals2' type='checkbox' name='{0}_485IntegumentaryIVGoals' value='2' {1} />", Model.TypeName, integumentaryIVGoals.Contains("2").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">The</label>
                        <%  var instructChangeDress = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485InstructChangeDress", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructChangeDress", instructChangeDress)%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">will demonstrate understanding of changing</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructChangeDressSterile", data.AnswerOrEmptyString("485InstructChangeDressSterile"), new { @id = Model.TypeName + "_485InstructChangeDressSterile", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">dressing using sterile technique.</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryIVGoalComments" class="strong">Additional Goals:</label>
            <%= Html.Templates(Model.TypeName + "_485IntegumentaryIVGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485IntegumentaryIVGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485IntegumentaryIVGoalComments", data.AnswerOrEmptyString("485IntegumentaryIVGoalComments"), 5, 70, new { @id = Model.TypeName + "_485IntegumentaryIVGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>
<%  } %>