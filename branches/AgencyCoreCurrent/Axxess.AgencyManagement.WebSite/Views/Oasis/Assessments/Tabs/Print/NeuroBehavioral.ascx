<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var NeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
<%  var NeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
<%  var NeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
<%  var MentalStatus = data.AnswerArray("485MentalStatus"); %>
<%  var Behaviour = data.AnswerArray("GenericBehaviour"); %>
<%  var Psychosocial = data.AnswerArray("GenericPsychosocial"); %>
<%  var PatientStrengths = data.AnswerArray("485PatientStrengths"); %>
<script type="text/javascript">
    printview.addsection(
    "","Neuro/Behavioral");
<%  if (Model.AssessmentTypeNum < 5 || Model.AssessmentTypeNum % 10 == 9|| Model.AssessmentTypeNum==11) { %>
    <%  if (Model.AssessmentTypeNum < 5|| Model.AssessmentTypeNum==11) { %>
    printview.addsection(
    <%if(NeurologicalStatus.Contains("1")){ %>
        printview.col(6,
            printview.checkbox("LOC:",<%= NeurologicalStatus.Contains("1").ToString().ToLower() %>,true) +
            printview.span("<%= NeurologicalStatus.Contains("1") ? (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("1") ? "Alert" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("2") ? "Lethargic" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("3") ? "Comatose" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("4") ? "Disoriented" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("5") ? "Forgetful" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalLOC").Equals("6") ? "Other" : string.Empty) : string.Empty %>",0,1) +
            printview.span("Oriented to:") +
            printview.checkbox("Person",<%= NeurologicalOriented.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Place",<%= NeurologicalOriented.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Time",<%= NeurologicalOriented.Contains("3").ToString().ToLower() %>)) +
    <%}else{ %>   
        printview.checkbox("LOC",false)+
    <%} %>  
    <%if(NeurologicalStatus.Contains("2")){ %> 
      printview.col(6,
            printview.checkbox("Pupils:",<%= NeurologicalStatus.Contains("2").ToString().ToLower() %>,true) +
            printview.span("<%= NeurologicalStatus.Contains("2") ? (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("1") ? "PERRLA/WNL (Within Normal Limits)" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("2") ? "Sluggish" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("3") ? "Non-Reactive" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalPupils").Equals("4") ? "Other" : string.Empty) : string.Empty %>",0,1) +
            printview.span("") +
            printview.checkbox("Bilateral",<%= (NeurologicalStatus.Contains("2") && data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (NeurologicalStatus.Contains("2") && data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (NeurologicalStatus.Contains("2") && data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("2")).ToString().ToLower() %>)) +
     <%}else{ %>   
        printview.checkbox("Pupils",false)+
    <%} %>         
    printview.checkbox("Vision:",<%= NeurologicalStatus.Contains("3").ToString().ToLower() %>,true) +
    <%if(NeurologicalStatus.Contains("3")){ %>
        printview.col(6,
            printview.checkbox("WNL",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("Blurred Vision",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("2")).ToString().ToLower() %>) +
            printview.checkbox("Cataracts",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Corrective Lenses",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("4")).ToString().ToLower() %>) +
            printview.checkbox("Glaucoma",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("5")).ToString().ToLower() %>) +
            printview.checkbox("Legally Blind",<%= (NeurologicalStatus.Contains("3") && NeurologicalVisionStatus.Contains("6")).ToString().ToLower() %>)) +
    <%} %>   
        printview.col(6,     
            printview.checkbox("Speech:",<%= NeurologicalStatus.Contains("4").ToString().ToLower() %>,true) +
            printview.span("<%= NeurologicalStatus.Contains("4") ? (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("1") ? "Clear" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("2") ? "Slurred" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("3") ? "Aphasic" : string.Empty) + (data.AnswerOrEmptyString("GenericNeurologicalSpeech").Equals("4") ? "Other" : string.Empty) : string.Empty %>",false,1) +
            printview.checkbox("Paralysis:",<%= NeurologicalStatus.Contains("5").ToString().ToLower() %>,true) +
            printview.span("<%= NeurologicalStatus.Contains("5") ? "Location: " + data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation").Clean() : string.Empty %>",false,1) +
            printview.checkbox("Tremors:",<%= NeurologicalStatus.Contains("9").ToString().ToLower() %>,true) +
            printview.span("<%= NeurologicalStatus.Contains("9") ? "Location: " + data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation").Clean() : string.Empty %>",false,1)) +
        printview.col(5,
            printview.checkbox("Quadriplegia",<%= NeurologicalStatus.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("Paraplegia",<%= NeurologicalStatus.Contains("7").ToString().ToLower() %>,true) +
            printview.checkbox("Seizures",<%= NeurologicalStatus.Contains("8").ToString().ToLower() %>,true) +
            printview.checkbox("Dizziness",<%= NeurologicalStatus.Contains("10").ToString().ToLower() %>,true) +
            printview.checkbox("Headache",<%= NeurologicalStatus.Contains("11").ToString().ToLower() %>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNeuroEmoBehaviorComments").Clean() %>",false,2),
        "Neurological");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Oriented",<%= MentalStatus.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Comatose",<%= MentalStatus.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Forgetful",<%= MentalStatus.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Agitated",<%= MentalStatus.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Depressed",<%= MentalStatus.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Disoriented",<%= MentalStatus.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Lethargic",<%= MentalStatus.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Other <%= MentalStatus.Contains("8") ? data.AnswerOrEmptyString("485MentalStatusOther").Clean() : string.Empty %>",<%= MentalStatus.Contains("8").ToString().ToLower() %>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485MentalStatusComments").Clean() %>",false,2),
        "Mental Status");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= Behaviour.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Withdrawn",<%= Behaviour.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Expresses Depression",<%= Behaviour.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Impaired Decision Making",<%= Behaviour.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Difficulty coping w/Health Status",<%= Behaviour.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Combative",<%= Behaviour.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Irritability",<%= Behaviour.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Other <%= Behaviour.Contains("8") ? data.AnswerOrEmptyString("GenericBehaviourOther").Clean() : string.Empty %>",<%= Behaviour.Contains("8").ToString().ToLower() %>)) + 
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericBehaviourComments").Clean() %>",false,2),
        "Behavior Status");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (No Issues Identified)",<%= Psychosocial.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Poor Home Environment",<%= Psychosocial.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("No emotional support from family/CG",<%= Psychosocial.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Verbalize desire for spiritual support",<%= Psychosocial.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Other <%= Psychosocial.Contains("5") ? data.AnswerOrEmptyString("GenericPsychosocialOther").Clean() : string.Empty %>",<%= Psychosocial.Contains("5").ToString().ToLower() %>)) + 
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPsychosocialComments").Clean() %>",false,2),
        "Psychosocial");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Motivated Learner",<%= PatientStrengths.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Strong Support System",<%= PatientStrengths.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Absence of Multiple Diagnosis",<%= PatientStrengths.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Enhanced Socioeconomic Status",<%= PatientStrengths.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Other",<%= PatientStrengths.Contains("5").ToString().ToLower() %>) +
            printview.span("<%= PatientStrengths.Contains("5") ? data.AnswerOrEmptyString("485PatientStrengthOther").Clean() : string.Empty %>",false,2)),
        "Patient Strengths");
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 > 8 ||Model.AssessmentTypeNum==11) { %>
    printview.addsection(
    "","<%=isOasis ?"OASIS M1700-M1720":string.Empty%>");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1700) " : string.Empty %>Cognitive Functioning: Patient&#8217;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.",true) +
        printview.checkbox("0 &#8211; Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.",<%= data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.",<%= data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.",<%= data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.",<%= data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.",<%= data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("04").ToString().ToLower() %>),
        "Cognition");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1710) " : string.Empty %>When Confused (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; Never",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; In new or complex situations only",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; On awakening or at night only",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; During the day and evening, but not constantly",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Constantly",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("04").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Patient nonresponsive",<%= data.AnswerOrEmptyString("M1710WhenConfused").Equals("NA").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1720) " : string.Empty %>When Anxious (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; None of the time",<%= data.AnswerOrEmptyString("M1720WhenAnxious").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Less often than daily",<%= data.AnswerOrEmptyString("M1720WhenAnxious").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Daily, but not constantly",<%= data.AnswerOrEmptyString("M1720WhenAnxious").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; All of the time",<%= data.AnswerOrEmptyString("M1720WhenAnxious").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Patient nonresponsive",<%= data.AnswerOrEmptyString("M1720WhenAnxious").Equals("NA").ToString().ToLower() %>)));
        <%  if (Model.AssessmentTypeNum % 10 != 9) { %>
    printview.addsection(
    "","<%=isOasis ?"OASIS M1730":string.Empty%>");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1730) " : string.Empty %>Depression Screening: Has the patient been screened for depression, using a standardized depression screening tool?",true) +
        printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1730DepressionScreening").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Yes, patient was screened using the PHQ-2&#169;* scale.",<%= data.AnswerOrEmptyString("M1730DepressionScreening").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Yes, with a different standardized assessment-and the patient meets criteria for further evaluation.",<%= data.AnswerOrEmptyString("M1730DepressionScreening").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Yes, patient was screened with a different standardized assessment-and the patient does not meet criteria for further evaluation.",<%= data.AnswerOrEmptyString("M1730DepressionScreening").Equals("03").ToString().ToLower() %>),
        "Depression Screening");
        <%  } %>
    printview.addsection(
    "","<%=isOasis ?"OASIS M1740-M1745":string.Empty%>");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1740) " : string.Empty %>Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): (Mark all that apply.)",true) +
        printview.checkbox("1 &#8211; Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsImpDes").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsVerbal").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsPhysical").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsSIB").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("6 &#8211; Delusional, hallucinatory, or paranoid behavior",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsDelusional").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("7 &#8211; None of the above behaviors demonstrated",<%= data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsNone").Equals("1").ToString().ToLower() %>),
        "Cognition");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1745) " : string.Empty %>Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; Never",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Less than once a month",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Once a month",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Several times each month",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Several times a week",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("04").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; At least daily",<%= data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("05").ToString().ToLower() %>)));
        <%  if (Model.AssessmentTypeNum % 10 != 9) { %>
    printview.addsection(
    "","<%=isOasis ?"OASIS M1750":string.Empty%>");
    printview.addsection(
        printview.span("<%= isOasis ? "(M1750) " : string.Empty %>Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("1").ToString().ToLower() %>)));
        <%  } %>
        <%  if (data.AnswerOrEmptyString("M1730DepressionScreening").Equals("01")) { %>
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth style=%22width:240px;%22%3E%3C/th%3E%3Cth%3ENot at all%3Cbr /%3E(0 &#8211; 1 day)%3C/th%3E%3Cth%3EServeral Days%3Cbr /%3E(2 &#8211; 6 days)%3C/th%3E%3Cth%3EMore than half of the days%3Cbr /%3E(7 &#8211; 11 days)%3C/th%3E%3Cth%3ENearly every day%3Cbr /%3E(12 &#8211; 14 days)%3C/th%3E%3Cth%3EN/A Unable to respond%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Ea) Little interest or pleasure in doing things%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data.AnswerOrEmptyString("M1730DepressionScreeningInterest").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Eb) Feeling down, depressed, or hopeless?%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("02").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("03").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data.AnswerOrEmptyString("M1730DepressionScreeningHopeless").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E","PHQ-2&#169;");
        <%  } %>
    <%  } %>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/NeuroBehavioral.ascx", Model); %>
<%  } %>
</script>