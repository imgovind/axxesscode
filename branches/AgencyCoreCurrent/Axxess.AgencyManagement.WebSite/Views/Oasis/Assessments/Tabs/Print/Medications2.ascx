﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum != 8) { %>
   
    printview.addsection(
        printview.col(8,
            printview.span("Time:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecTime").Clean() %>",false,1) +
            printview.span("Medication:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecMedication").Clean() %>",false,1) +
            printview.span("Dose:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecDose").Clean() %>",false,1) +
            printview.span("Route:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecRoute").Clean() %>",false,1) +
            printview.span("Frequency:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecFrequency").Clean() %>",false,1) +
            printview.span("PRN Reason:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecPRN").Clean() %>",false,1) +
            printview.span("Location:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecLocation").Clean() %>",false,1) +
            printview.span("Response:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedRecResponse").Clean() %>",false,1)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMedRecComments").Clean() %>",false,2),
        "Medication Administration Records");
    
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2000) " : string.Empty %>Drug Regimen Review: Does a complete drug regimen review indicate potential clinically significant medication issues, e.g., drug reactions, ineffective drug therapy, side effects, drug interactions, duplicate therapy, omissions, dosage errors, or noncompliance? ",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; Not assessed/reviewed",<%= data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; No problems found during review",<%= data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Problems found during review",<%= data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Patient is not taking any medications",<%= data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("NA").ToString().ToLower() %>)));
        <%  if (data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("02")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2002) " : string.Empty %>Medication Follow-up: Was a physician or the physician—designee contacted within one calendar day to resolve clinically significant medication issues, including reconciliation? ",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("1").ToString().ToLower() %>)));
        <%  } %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 > 5) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2004) " : string.Empty %>Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("01").ToString().ToLower() %>)) +
        printview.checkbox("NA &#8211; No clinically significant medication issues identified since the previous OASIS assessment",<%= data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 && !data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("NA")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2010) " : string.Empty %>Patient/Caregiver High Risk Drug Education: Has the patient/caregiver received instruction on special precautions for all high-risk medications (such as hypoglycemics, anticoagulants, etc.) and how and when to report problems that may occur? ",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("01").ToString().ToLower() %>)) +
        printview.checkbox("NA &#8211; Patient not taking any high risk drugs OR patient/caregiver fully knowledgeable about special precautions with all high-risk medications",<%= data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum % 10 > 5) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2015) " : string.Empty %>Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("01").ToString().ToLower() %>)) +
        printview.checkbox("NA &#8211; Patient not taking any drugs",<%= data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2020) " : string.Empty %>Management of Oral Medications: Patient&#8217;s current ability to prepare and take all oral medications reliably and safely, including administration of the correct dosage at the appropriate times/intervals. Excludes injectable and IV medications. (NOTE: This refers to ability, not compliance or willingness.) ",true) +
        printview.checkbox("0 &#8211; Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times.",<%= data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to take medication(s) at the correct times if:<ul><li>(a) individual dosages are prepared in advance by another person; OR</li><li>(b) another person develops a drug diary or chart.</li></ul>",<%= data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Able to take medication(s) at the correct times if given reminders by another person at the appropriate times",<%= data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Unable to take medication unless administered by another person.",<%= data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("NA &#8211; No oral medications prescribed.",<%= data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 6 || Model.AssessmentTypeNum % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2030) " : string.Empty %>Management of Injectable Medications: Patient&#8217;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.",true) +
        printview.checkbox("0 &#8211; Able to independently take the correct medication(s) and proper dosage(s) at the correct times.",<%= data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Able to take injectable medication(s) at the correct times if: %3Cul%3E%3Cli%3E(a) individual syringes are prepared in advance by another person; OR%3C/li%3E%3Cli%3E(b) another person develops a drug diary or chart.%3C/li%3E%3C/ul%3E",<%= data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection",<%= data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Unable to take injectable medication unless administered by another person.",<%= data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("NA &#8211; No injectable medications prescribed.",<%= data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("NA").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2040) " : string.Empty %>Prior Medication Management: Indicate the patient&#8217;s usual ability with managing oral and injectable medications prior to this current illness, exacerbation, or injury. Check only one box in each row.",true) +
        printview.col(5,
            printview.span("a. Oral medications") +
            printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Needed Some Help",<%= data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Not Applicable",<%= data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("NA").ToString().ToLower() %>) +
            printview.span("b. Injectable medications") +
            printview.checkbox("0 &#8211; Independent",<%= data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Needed Some Help",<%= data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Dependent",<%= data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Not Applicable",<%= data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("NA").ToString().ToLower() %>)));
    <%  } %>
<%  } %>
</script>