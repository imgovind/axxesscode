﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<span class="wintitle">Payroll Summary | <%= Current.AgencyName %></span>
<div id="Payroll_SummaryContent" class="main wrapper align-center">
<%  using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
    <fieldset class="orders-filter">
    <div class="buttons float-right">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate</a></li>
            </ul>
        </div>
    <div class="float-left">
        <span class="strong">Payroll Summary</span> &#151;
        <label for="payrollStartDate">From: </label>
        <input type="text" class="date-picker required" id="payrollStartDate" name="payrollStartDate" value="<%= Model.ToShortDateString() %>" />
        <label for="payrollEndDate">To: </label>
        <input type="text" class="date-picker required" id="payrollEndDate" name="payrollEndDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Today.ToShortDateString() %>" />
   </div>
   <div class="float-left">       
        <label for="strong">Status: </label>
        <select name="payrollStatus" id="payrollStatus">
            <option value="All">All</option>
            <option value="true">Paid</option>
            <option value="false" selected="selected">Unpaid</option>
        </select>
   </div> 
        
    </fieldset>
    <div id="payrollLoading" style="position:absolute; top:87px; bottom:0px; width:99%;z-index:-1;"> </div>
    <div id="payrollSearchResult" class="payroll align-left"></div>
    <div id="payrollSearchResultDetails" class="align-left"></div>
    <div id="payrollSearchResultDetail" class="align-left"></div>
    <%= Html.Hidden("PayrollSearchResultView", string.Empty, new { @id = "markAsPaidButtonId"} ) %>
    <% if(!Current.IsAgencyFrozen) { %>
    <div id="payrollMarkAsPaidButton" class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Payroll.MarkAsPaid()">Mark As Paid</a></li>
            <li><a href="javascript:void(0);" onclick="Payroll.MarkAsUnpaid()">Mark As Unpaid</a></li>
        </ul>
    </div>
    <% } %>
<%  } %>
</div>

<script type="text/javascript">
    $("#window_payrollsummary_content").css({
        "background-color": "#d6e5f3"
    });
</script>