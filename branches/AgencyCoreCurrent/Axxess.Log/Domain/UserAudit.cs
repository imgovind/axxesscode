﻿namespace Axxess.Log.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;


   public class UserAudit
    {
       public string ActionType { get; set; }
       public string ActionDescription { get; set; }
       public DateTime Date { get; set; }
    }
}
