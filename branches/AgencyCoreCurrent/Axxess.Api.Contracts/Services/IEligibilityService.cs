﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2014/01/")]
    public interface IEligibilityService : IService
    {
        [OperationContract]
        PatientEligibility EligibilityCheck(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
    }
}
