﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2011/10/")]
    public interface ICacheService : IService
    {
        [OperationContract]
        void RefreshAgency(Guid agencyId);
        [OperationContract]
        string GetAgencyXml(Guid agencyId);


        [OperationContract]
        void RefreshPhysicians(Guid agencyId);
        [OperationContract]
        string GetPhysicianXml(Guid physicianId, Guid agencyId);
        [OperationContract]
        List<string> GetPhysicians(Guid agencyId);


        [OperationContract]
        void RefreshUsers(Guid agencyId);
        [OperationContract]
        string GetUserDisplayName(Guid userId, Guid agencyId);
        [OperationContract]
        List<UserData> GetUsers();
    }
}
