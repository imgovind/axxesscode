﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2013/01/")]
    public interface IAbilityNetworkService : IService
    {
        [OperationContract]
        string GetHiqhRequest(Guid patientId, string hic, string lastName, string firstInitial, DateTime dateOfBirth, string sex, string providerId, string stateCode);
        [OperationContract]
        bool GetPasswordChangeRequest(string userId, string oldPassword, string newPassword);
    }
}
