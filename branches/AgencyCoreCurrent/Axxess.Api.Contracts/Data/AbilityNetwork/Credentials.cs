﻿namespace Axxess.Api.Contracts
{
    public class Credentials
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}