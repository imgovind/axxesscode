﻿namespace Axxess.Api.Contracts
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class passwordChangeRequest : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string newPassword { get; set; }

        [XmlElement("medicareMainframe", Form = XmlSchemaForm.Unqualified)]
        public passwordChangeRequestMedicareMainframe[] medicareMainframe { get; set; }
       
        [XmlAttribute()]
        public string mockResponse { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class passwordChangeRequestMedicareMainframe : BaseApiEntity
    {
        [XmlElement("application", Form = XmlSchemaForm.Unqualified)]
        public passwordChangeRequestMedicareMainframeApplication[] application { get; set; }
       
        [XmlElement("credential", Form = XmlSchemaForm.Unqualified)]
        public passwordChangeRequestMedicareMainframeCredential[] credential { get; set; }

        [XmlElement("clerkCredential", Form = XmlSchemaForm.Unqualified)]
        public passwordChangeRequestMedicareMainframeClerkCredential[] clerkCredential { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class passwordChangeRequestMedicareMainframeApplication : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string facilityState { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string lineOfBusiness { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string name { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dataCenter { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string appId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string pptnRegion { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class passwordChangeRequestMedicareMainframeCredential : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string userId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string password { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class passwordChangeRequestMedicareMainframeClerkCredential : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string userId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string password { get; set; }
    }

    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class NewDataSet : BaseApiEntity
    {
        [XmlElement("passwordChangeRequest")]
        public passwordChangeRequest[] Items { get; set; }
    }
}