﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class Subscriber
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string last_name { get; set; }
        [DataMember]
        public string first_name { get; set; }
        [DataMember]
        public string middle_name { get; set; }
        [DataMember]
        public string identification_code { get; set; }
        [DataMember]
        public string gender { get; set; }
    }
}
