﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class MedicarePart
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string eligibility_or_benefit_information { get; set; } //EB01
        [DataMember]
        public string insurance_type_code { get; set; }  //EB04
    }
}
