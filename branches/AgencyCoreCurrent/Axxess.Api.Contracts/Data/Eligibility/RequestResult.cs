﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class RequestResult
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string response { get; set; }
    }
}
