﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class PatientEligibility
    {
        [DataMember]
        public RequestResult Request_Result { get; set; }
        [DataMember]
        public RequestValidation Request_Validation { get; set; }
        [DataMember]
        public FunctionalAcknowledgment Functional_Acknowledgment { get; set; }
        [DataMember]
        public MedicarePart Medicare_Part_A { get; set; }
        [DataMember]
        public MedicarePart Medicare_Part_B { get; set; }
        [DataMember]
        public Subscriber Subscriber { get; set; }
        [DataMember]
        public GenericResult Dependent { get; set; }
        [DataMember]
        public HealthBenefitPlanCoverage Health_Benefit_Plan_Coverage { get; set; }
        [DataMember]
        public Episode Episode { get; set; }
        [DataMember]
        public OtherAgencyData Other_Agency_Data { get; set; }
        
    }
}
