﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Eligibility/2014/01/")]
    public class Episode
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string payer { get; set; } //NM101
        [DataMember]
        public string name { get; set; } //NM103
        [DataMember]
        public string reference_id_qualifier { get; set; } //PRV02
        [DataMember]
        public string reference_id { get; set; } //PRV03
        [DataMember]
        public string period_date_range { get; set; }
        [DataMember]
        public string period_start { get; set; } // DTP01 193 DTP03
        [DataMember]
        public string period_end { get; set; } //DTP01 194 DTP03
    }
}
