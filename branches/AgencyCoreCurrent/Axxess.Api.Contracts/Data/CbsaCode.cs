﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/01/")]
    public class CbsaCode
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Zip { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public string CountyCode { get; set; }
        [DataMember]
        public string CountyTitle { get; set; }
        [DataMember]
        public string StateCode { get; set; }
        [DataMember]
        public string StateName { get; set; }
        [DataMember]
        public string CBSA { get; set; }
        [DataMember]
        public double WITwoSeven { get; set; }
        [DataMember]
        public double WITwoEight { get; set; }
        [DataMember]
        public double WITwoNine { get; set; }
        [DataMember]
        public double WITwoTen { get; set; }
        [DataMember]
        public double WITwoEleven { get; set; }
        [DataMember]
        public double WITwoTwelve { get; set; }
        [DataMember]
        public double WITwoThirteen { get; set; }
        [DataMember]
        public double Variance { get; set; }
    }
}
