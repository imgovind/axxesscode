﻿namespace Axxess.Api.Contracts
{
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/webinaruser/2013/07/")]
    public class WebinarUser
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string AgencyName { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .AppendFormat("Last: {0} ", this.LastName)
                .AppendFormat("First: {0} ", this.FirstName)
                .AppendFormat("Agency: {0} ", this.AgencyName)
                .AppendFormat("Email: {0} ", this.EmailAddress)
                .ToString();
        }
    }
}
