﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class CaliAgencyScript
    {
        private static ISheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\CaliAgencies.txt");
        private static string output = Path.Combine(App.Root, string.Format("Files\\CaliAgencies_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();

            using (var fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    int i = 1;
                    while (streamReader.Peek() >= 0)
                    {
                        var line = streamReader.ReadLine();
                        if (line.IsNotNullOrEmpty())
                        {
                            if (line.Trim().StartsWith("("))
                            {
                                if (line.Length >= 15)
                                {
                                    var excelRow = sheet.CreateRow(i);
                                    excelRow.CreateCell(0).SetCellValue("");

                                    var faxNumber = line.Substring(0, 14);
                                    excelRow.CreateCell(1).SetCellValue(faxNumber.Trim().ToPhoneDB());
                                    i++;
                                }
                            }
                            else
                            {
                                var lineArray = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                if (lineArray != null && lineArray.Length > 0)
                                {
                                    lineArray.ForEach(word =>
                                    {
                                        if (word.Length > 3 && word.Contains("@"))
                                        {
                                            var excelRow = sheet.CreateRow(i);
                                            excelRow.CreateCell(0).SetCellValue(word.ToLower());

                                            var startIndex = word.Length + 1;
                                            if (line.Length >= startIndex + 15)
                                            {
                                                var faxNumber = line.Substring(startIndex, 14);
                                                excelRow.CreateCell(1).SetCellValue(faxNumber.Trim().ToPhoneDB());
                                            }

                                            i++;
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                Write();
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("California State Agencies");

            var headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            var headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(1).SetCellValue("Fax Number");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
