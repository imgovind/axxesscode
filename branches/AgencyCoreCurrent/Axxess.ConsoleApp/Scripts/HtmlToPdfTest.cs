﻿using System;
using System.IO;
using System.Net;

using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

using HtmlAgilityPack;

using Axxess.ConsoleApp.Common;

namespace Axxess.ConsoleApp.Tests
{
    public static class HtmlToPdfTest
    {
        private static string output = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", ""), string.Format("Files\\html_{0}.pdf", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            try
            {
                using (FileStream fileStream = new FileStream(output, FileMode.Create))
                {
                    Document document = new Document();
                    PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
                    document.Open();
                    var reader = new StringReader(GetData());
                    var parser = new HTMLWorker(document);
                    parser.Parse(reader);
                    document.Close();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            //HtmlToPdfBuilder builder = new HtmlToPdfBuilder(PageSize.LETTER);
            //HtmlPdfPage firstPage = builder.AddPage();
            //HtmlPdfPage secondPage = builder.AddPage();
            //firstPage.AppendHtml("<h1>Hello World</h1>");
            //secondPage.AppendHtml("<h1>{0}</h1><span>{1}</span>", "Hello Second Page", "Another Param");
            //builder.AddStyle("H1", "color:#F00");
            //builder.AddStyle("p", "font-weight:bold;text-decoration:underline;");
            //byte[] pdfFile = builder.RenderPdf();
            //File.WriteAllBytes(output, pdfFile);
        }

        private static string GetData()
        {
            string html = string.Empty;
            using (WebClient client = new WebClient())
            {
                using (StringWriter stringWriter = new StringWriter())
                {
                    var raw = client.DownloadString("http://dotnetperls.com/streamwriter");
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(raw);
                    foreach (HtmlNode imageNode in doc.DocumentNode.SelectNodes("//img"))
                    {
                        imageNode.Remove();
                    }
                    doc.Save(stringWriter);
                    html = stringWriter.ToString();
                }
            }
            return html;
        }
    }
}
