﻿namespace Axxess.ConsoleApp
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    public static class LoadAllergyProfiles
    {
        private static IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();
        private static string output = Path.Combine(App.Root, string.Format("Files\\Allergies_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                var patients = GetAllPatients();
                if (patients != null && patients.Count > 0)
                {
                    patients.ForEach(patient =>
                    {
                        var patientEpisodes = GetPatientEpisodes(patient.AgencyId.ToGuid(), patient.PatientId);
                        if (patientEpisodes != null && patientEpisodes.Count > 0)
                        {
                            var episodeData = patientEpisodes.OrderBy(e => e.StartDate).FirstOrDefault();
                            if (episodeData != null)
                            {
                                episodeData.PatientId = patient.PatientId;
                                episodeData.AgencyId = patient.AgencyId.ToGuid();

                                var sampleAssessment = GetEpisodeAssessment(episodeData);
                                if (sampleAssessment != null)
                                {
                                    var data = sampleAssessment.ToDictionary();

                                    if (data.AnswerOrEmptyString("485Allergies").IsNotNullOrEmpty() && data.AnswerOrEmptyString("485Allergies").IsEqual("yes"))
                                    {
                                        if (data.AnswerOrEmptyString("485AllergiesDescription").IsNotNullOrEmpty())
                                        {
                                            var allergyArray = data.AnswerOrEmptyString("485AllergiesDescription").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                            if (allergyArray != null && allergyArray.Length > 0)
                                            {
                                                var allergies = new List<Allergy>();
                                                allergyArray.ForEach(a =>
                                                {
                                                    allergies.Add(new Allergy
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        Type = string.Empty,
                                                        IsDeprecated = false,
                                                        ProfileId = patient.AllergyProfileId,
                                                        Name = Regex.Replace(a.Trim(), @"<(.|\n)*?>", string.Empty)
                                                    });
                                                });

                                                if (allergies.Count > 0)
                                                {
                                                    patient.Allergies = allergies.ToXml();
                                                }
                                            }
                                        }
                                        Console.WriteLine(data.AnswerOrEmptyString("485AllergiesDescription"));
                                        Console.WriteLine(patient.Allergies);
                                        Console.WriteLine();
                                    }
                                }
                            }
                        }
                        textWriter.WriteLine(new PatientAllergyProfileScript(patient).ToString());
                    });
                }
            }
        }

        internal static List<PatientData> GetAllPatients()
        {
            var patients = new List<PatientData>();
            var script = @"SELECT `Id`,`FirstName`,`LastName`, `AgencyId` FROM `patients`;";

            using (var cmd = new FluentCommand<PatientData>(script))
            {
                patients = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new PatientData
                {
                    PatientId = reader.GetGuid("Id"),
                    AgencyId = reader.GetString("AgencyId"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName")
                })
                .AsList();
            }

            return patients;
        }

        internal static List<EpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            var list = new List<EpisodeData>();
            var script = @"SELECT patientepisodes.Id, patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
                "AND patientepisodes.IsActive = 1";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static Assessment GetEpisodeAssessment(EpisodeData episode)
        {
            Assessment assessment = null;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var assessmentEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date < episode.EndDate.AddDays(-5).Date
                       && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment()))
                    .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                if (assessmentEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentEvent.EventId, assessmentEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), episode.AgencyId);
                }
                else
                {
                    var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.Id);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var previousEpisodeRecertEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= previousEpisode.EndDate.AddDays(-5).Date
                           && e.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && e.IsRecertificationAssessment())
                            .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                        if (previousEpisodeRecertEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousEpisodeRecertEvent.EventId, previousEpisodeRecertEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), episode.AgencyId);
                        }
                        else
                        {
                            var previousEpisodeRocEvent = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= previousEpisode.EndDate.AddDays(-5).Date
                               && e.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && e.IsResumptionofCareAssessment())
                                .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                            if (previousEpisodeRocEvent != null)
                            {
                                assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousEpisodeRocEvent.EventId, previousEpisodeRocEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), episode.AgencyId);
                            }
                        }
                    }
                }
            }
            return assessment;
        }

        internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var episodes = GetPatientEpisodes(agencyId, patientId);
            if (episodes != null && episodes.Count > 0)
            {
                var episode = episodes.Where(e => e.Id == episodeId).FirstOrDefault();
                if (episode != null)
                {
                    return episodes.Where(e => e.EndDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                }
            }
            return null;
        }

    }
}
