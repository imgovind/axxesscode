﻿namespace Axxess.ConsoleApp
{
    using System;

    public class PatientAllergyProfileScript
    {
        private PatientData patientData;
        private const string ALLERGYPROFILE_INSERT = "INSERT INTO `allergyprofiles`(`Id`,`AgencyId`,`PatientId`,`Allergies`,`Created`,`Modified`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', {4}, {5});";

        public PatientAllergyProfileScript(PatientData patientData)
        {
            this.patientData = patientData;
        }

        public override string ToString()
        {
            return string.Format(ALLERGYPROFILE_INSERT, patientData.AllergyProfileId, patientData.AgencyId, patientData.PatientId, patientData.Allergies, patientData.Created, patientData.Modified);
        }
    }
}
