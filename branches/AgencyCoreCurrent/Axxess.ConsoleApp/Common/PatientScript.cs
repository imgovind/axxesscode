﻿namespace Axxess.ConsoleApp
{
    public class PatientScript
    {
        private PatientData patientData;
        private const string PATIENT_INSERT = "INSERT INTO `patients`(`Id`,`AgencyId`,`AgencyLocationId`,`PatientIdNumber`,`MedicareNumber`," +
            "`MedicaidNumber`,`FirstName`,`LastName`,`MiddleInitial`,`DOB`,`Gender`,`Ethnicities`,`MaritalStatus`,`AddressLine1`,`AddressLine2`," +
            "`AddressCity`,`AddressStateCode`,`AddressZipCode`,`PhoneHome`,`StartofCareDate`,`Status`,`Created`,`Modified`,`DischargeDate`," +
            "`Comments`,`SSN`,`PaymentSource`, `PrimaryInsurance`, `AddressCounty`, `IsTransferred`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', {20}, {21}, {22}, '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', {29});";

        public PatientScript(PatientData patientData)
        {
            this.patientData = patientData;
        }

        public override string ToString()
        {
            return string.Format(PATIENT_INSERT, patientData.PatientId, patientData.AgencyId, patientData.AgencyLocationId, patientData.PatientNumber, patientData.MedicareNumber, patientData.MedicaidNumber,
               patientData.FirstName.Replace("'", "\\'"), patientData.LastName.Replace("'", "\\'"), patientData.MiddleInitial, patientData.BirthDate, patientData.Gender, patientData.Ethnicity, patientData.MaritalStatus, patientData.AddressLine1.Replace("'", "\\'"),
               patientData.AddressLine2, patientData.AddressCity.Replace("'", "\\'"), patientData.AddressState, patientData.AddressZipCode, patientData.Phone, patientData.StartofCareDate, patientData.PatientStatusId, patientData.Created,
               patientData.Modified, patientData.DischargeDate, patientData.Comments.Replace("'", "\\'"), patientData.SSN, patientData.PaymentSource, patientData.PrimaryInsurance, patientData.AddressCounty, patientData.IsHosptialized ? "1" : "0");
        }
    }
}
