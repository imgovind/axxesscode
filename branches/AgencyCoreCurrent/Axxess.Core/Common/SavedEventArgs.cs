﻿namespace Axxess.Core
{
    using System;

    public class SavedEventArgs : EventArgs
    {
        public SaveAction Action { get; set;}
        public SavedEventArgs(SaveAction action)
        {
            this.Action = action;
        }
    }
}
