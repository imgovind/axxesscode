﻿
namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;
   public class CustomDescriptionAttribute : DescriptionAttribute
    {
       private string shortDesc;

       public CustomDescriptionAttribute(string text, string shortDesc)
           : base(text)
       {
           this.shortDesc = shortDesc;
       }
       public string ShortDescription
       {
           get
           {
               return shortDesc;
           }
       }
    }
}
