﻿namespace Axxess.Core
{
    using System;

    using Extension;

    public class Time
    {
        #region Time Members

        public Time(string time)
        {
            Check.Argument.IsNotEmpty(time, "time");

            if (time.Length == 7 || time.Length == 8)
            {
                this.Hour = time.Substring(0, 2).ToInteger();
                this.Minute = time.Substring(3, 2).ToInteger();
                if (time.EndsWith("AM"))
                {
                    this.Meridiem = Meridiem.AM;
                    if (this.Hour == 12)
                    {
                        this.Hour = 0;
                    }
                }

                if (time.EndsWith("PM"))
                {
                    this.Meridiem = Meridiem.PM;
                    if (this.Hour < 12)
                    {
                        this.Hour = this.Hour + 12;
                    }
                }
            }
        }

        public int Hour { get; set; }
        public int Minute { get; set; }
        public Meridiem Meridiem { get; set; }

        public TimeSpan TimeOfDay
        {
            get
            {
                return new TimeSpan(this.Hour, this.Minute, 0);
            }
        }

        #endregion
        
    }

    public enum Meridiem
    {
        AM = 1,
        PM = 2
    }
}
