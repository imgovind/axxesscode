﻿namespace Axxess.Core
{
    using System;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    public class SafeDictionary<K, V>
    {
        private readonly Dictionary<K, V> baseDictionary = null;
        private readonly ReaderWriterLockSlim readWriteLockSlim = new ReaderWriterLockSlim();

        public SafeDictionary()
        {
            this.baseDictionary = new Dictionary<K, V>();
        }

        public V this[K key] 
        {
            get
            {
                using (readWriteLockSlim.AcquireReadLock())
                {
                    return this.baseDictionary[key];
                }
            }
            set
            {
                using (readWriteLockSlim.AcquireWriteLock())
                {
                    this.baseDictionary[key] = value;
                }
            }
        }

        public void Add(K key, V value)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                if (!this.baseDictionary.ContainsKey(key))
                {
                    this.baseDictionary.Add(key, value);
                }
            }
        }

        public void Clear()
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseDictionary.Clear();
            }
        }

        public bool TryGetValue(K key, out V value)
        {
            using (readWriteLockSlim.AcquireReadLock())
            {
                return this.baseDictionary.TryGetValue(key, out value);
            }
        }

        public bool ContainsKey(K key)
        {
            using (readWriteLockSlim.AcquireReadLock())
            {
                return this.baseDictionary.ContainsKey(key);
            }
        }

        public int Count
        {
            get
            {
                using (readWriteLockSlim.AcquireReadLock())
                {
                    return this.baseDictionary.Count;
                }
            }
        }

        public void Remove(K key)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseDictionary.Remove(key);
            }
        }

        public void ForEach(Action<K, V> action)
        {
            using (readWriteLockSlim.AcquireWriteLock())
            {
                this.baseDictionary.ForEach(action);
            }
        }
    }
}
