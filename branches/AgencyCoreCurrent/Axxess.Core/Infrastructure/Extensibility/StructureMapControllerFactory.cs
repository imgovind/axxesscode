﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    using StructureMap;

    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            try
            {
                if (controllerType == null)
                {
                    return base.GetControllerInstance(requestContext, controllerType);
                }
                return ObjectFactory.GetInstance(controllerType) as Controller;
            }
            catch (StructureMapException ex)
            {
                System.Diagnostics.Debug.WriteLine(ObjectFactory.WhatDoIHave());
                throw new HttpException(404, string.Format("The path '{0}' does not exist.", requestContext.HttpContext.Request.CurrentExecutionFilePath));
            }
            catch (Exception)
            {
                throw new HttpException(404, string.Format("The path '{0}' does not exist.", requestContext.HttpContext.Request.CurrentExecutionFilePath));
            }
        }
    }
}
