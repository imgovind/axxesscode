﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web.Mvc;
    using System.Threading;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class AuditAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = filterContext.RequestContext.HttpContext;
            context.Items["AuditSilence"] = context.Request.Params.Get("AuditSilence").IsNotNullOrEmpty() ? context.Request.Params.Get("AuditSilence").ToBoolean() : false;
            
            base.OnActionExecuting(filterContext);
        }
    }
}
