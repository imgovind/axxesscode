﻿namespace Axxess.Core.Infrastructure
{
    using System;

    using Axxess.Core.Extension;

    public static class AppSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static bool CompressHtmlResponse
        {
            get
            {
                return configuration.AppSettings["CompressHtmlResponse"].ToBoolean();
            }
        }

        public static bool UseSSL
        {
            get
            {
                return configuration.AppSettings["UseSSL"].ToBoolean();
            }
        }

        public static double CachingIntervalInMinutes
        {
            get
            {
                return configuration.AppSettings["CachingIntervalInMinutes"].ToDouble();
            }
        }

        public static double SessionTimeoutInMinutes
        {
            get
            {
                return configuration.AppSettings["SessionTimeoutInMinutes"].ToDouble();
            }
        }

        public static string AuthenticationType
        {
            get
            {
                return configuration.AppSettings["AuthenticationType"];
            }
        }

        public static string GoogleAPIKey
        {
            get
            {
                return configuration.AppSettings["GoogleApiKey"];
            }
        }

        public static string AuthenticationDomain
        {
            get
            {
                return configuration.AppSettings["AuthenticationDomain"];
            }
        }

        public static bool UsePersistentCookies
        {
            get
            {
                return configuration.AppSettings["UsePersistentCookies"].ToBoolean();
            }
        }

        public static string[] MemcacheServerUriArray
        {
            get
            {
                return configuration.AppSettings["MemcacheServerUris"].Split(';');
            }
        }

        public static string NoReplyEmail
        {
            get
            {
                return configuration.AppSettings["NoReplyEmail"];
            }
        }

        public static string NoReplyDisplayName
        {
            get
            {
                return configuration.AppSettings["NoReplyDisplayName"];
            }
        }

        public static bool EnableSSLMail
        {
            get
            {
                return configuration.AppSettings["EnableSSLMail"].ToBoolean();
            }
        }

        public static string MailTemplate
        {
            get
            {
                return configuration.AppSettings["TemplateDirectory"];
            }
        }

        public static string NewsFeedUrl
        {
            get
            {
                return configuration.AppSettings["NewsFeedUrl"];
            }
        }

        public static string AllowedImageExtensions
        {
            get
            {
                return configuration.AppSettings["AllowedImageExtensions"];
            }
        }

        public static string ANSIGeneratorUrl
        {
            get
            {
                return configuration.AppSettings["ANSIGeneratorUrl"];
            }
        }

        public static string PatientEligibilityUrl
        {
            get
            {
                return configuration.AppSettings["PatientEligibilityUrl"];
            }
        }

        public static string UBOFourPath
        {
            get
            {
                return configuration.AppSettings["UBOFourPath"];
            }
        }

        public static string CryptoPassPhrase
        {
            get
            {
                return configuration.AppSettings["CryptoPassPhrase"];
            }
        }

        public static string CryptoSaltValue
        {
            get
            {
                return configuration.AppSettings["CryptoSaltValue"];
            }
        }

        public static string CryptoHashAlgorithm
        {
            get
            {
                return configuration.AppSettings["CryptoHashAlgorithm"];
            }
        }

        public static string CryptoInitVector
        {
            get
            {
                return configuration.AppSettings["CryptoInitVector"];
            }
        }

        public static int CryptoPasswordIterations
        {
            get
            {
                return configuration.AppSettings["CryptoPasswordIterations"].ToInteger();
            }
        }

        public static int CryptoKeySize
        {
            get
            {
                return configuration.AppSettings["CryptoKeySize"].ToInteger();
            }
        }

        public static string WeatherImageUriFormat
        {
            get
            {
                return configuration.AppSettings["WeatherImageUriFormat"];
            }
        }

        public static string WeatherForecastImageUriFormat
        {
            get
            {
                return configuration.AppSettings["WeatherForecastImageUriFormat"];
            }
        }

        public static string WeatherImageThumbnailUriFormat
        {
            get
            {
                return configuration.AppSettings["WeatherImageThumbnailUriFormat"];
            }
        }

        public static string YahooWeatherUri
        {
            get
            {
                return configuration.AppSettings["YahooWeatherUri"];
            }
        }
    }
}
