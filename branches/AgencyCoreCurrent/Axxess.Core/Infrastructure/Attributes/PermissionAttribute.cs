﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class PermissionAttribute : DescriptionAttribute
    {
        public PermissionAttribute(string description, string longDescription, string category, string tip) : base(description)
        {
            this.Category = category;
            this.LongDescription = longDescription;
            this.Tip = tip;
        }

        public string Category { get; private set; }
        public string LongDescription { get; private set; }
        public string Tip { get; private set; }
    }
}
