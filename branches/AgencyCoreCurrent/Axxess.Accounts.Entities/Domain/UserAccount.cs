﻿namespace Axxess.Accounts.Entities
{
    using System;

    public class UserAccount
    {
        public int Status { get; set; }
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public string Title { get; set; }
        public Guid AgencyId { get; set; }
        public int ClusterId { get; set; }
        public DateTime Created { get; set; }
        public string AgencyName { get; set; }
        public DateTime ReadOnlyDate { get; set; }
        public bool IsAgencySuspended { get; set; }
    }
}
