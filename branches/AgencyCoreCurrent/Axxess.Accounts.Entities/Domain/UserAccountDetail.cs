﻿namespace Axxess.Accounts.Entities
{
    using System;

    public class UserAccountDetail : UserAccount
    {
        public string AgencyPhone { get; set; }
        public string AgencyAddressCity { get; set; }
        public string AgencyAddressLine1 { get; set; }
        public string AgencyAddressLine2 { get; set; }
        public string AgencyAddressZipCode { get; set; }
        public string AgencyAddressStateCode { get; set; }
    }
}
