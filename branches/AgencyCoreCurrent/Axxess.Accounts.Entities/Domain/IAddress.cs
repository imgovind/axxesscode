﻿namespace Axxess.Accounts.Entities
{
    public interface IAddress
    {
        string AddressCity { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressZipCode { get; set; }
        string AddressStateCode { get; set; }
    }
}
