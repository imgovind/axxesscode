﻿namespace Axxess.Accounts.Entities
{
    using System;

    public class LoginToken : IDataModel
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public bool IsUsed { get; set; }
        public Guid AgencyId { get; set; }
        public DateTime Created { get; set; }

        #endregion
    }
}
