﻿namespace Axxess.Accounts.Entities
{
    using System;

    public class Login : IDataModel
    {
        #region Members

        public Guid Id { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
        public DateTime Created { get; set; }
        public bool IsAxxessAdmin { get; set; }
        public bool IsAxxessSupport { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordHash { get; set; }
        public string SignatureHash { get; set; }
        public string SignatureSalt { get; set; }
        public DateTime LastLoginDate { get; set; }

        #endregion
    }
}
