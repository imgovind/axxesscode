﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Data;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;

    public class UserAccountMapper : ISelectable<UserAccount>
    {
        public UserAccount ApplySelect(DataReader reader)
        {
            return new UserAccount()
            {
                Status = reader.GetInt("Status"),
                UserId = reader.GetGuid("UserId"),
                LoginId = reader.GetGuid("LoginId"),
                AgencyId = reader.GetGuid("AgencyId"),
                ClusterId = reader.GetInt("ClusterId"),
                Created = reader.GetDateTime("Created"),
                Title = reader.GetStringNullable("Title"),
                AgencyName = reader.GetString("AgencyName"),
                ReadOnlyDate = reader.GetDateTime("FrozenDate"),
                IsAgencySuspended = reader.GetBoolean("IsSuspended")
            };
        }
    }
}
