﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Data;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;

    public class LoginMapper : ISelectable<Login>
    {
        public Login ApplySelect(DataReader reader)
        {
            return new Login()
            {
                Id = reader.GetGuid("Id"),
                Role = reader.GetStringNullable("Role"),
                Created = reader.GetDateTime("Created"),
                IsActive = reader.GetBoolean("IsActive"),
                IsLocked = reader.GetBoolean("IsLocked"),
                IsAxxessAdmin = reader.GetBoolean("IsAxxessAdmin"),
                LastLoginDate = reader.GetDateTime("LastLoginDate"),
                DisplayName = reader.GetStringNullable("DisplayName"),
                IsAxxessSupport = reader.GetBoolean("IsAxxessSupport"),
                EmailAddress = reader.GetStringNullable("EmailAddress"),
                PasswordSalt = reader.GetStringNullable("PasswordSalt"),
                PasswordHash = reader.GetStringNullable("PasswordHash"),
                SignatureHash = reader.GetStringNullable("SignatureHash"),
                SignatureSalt = reader.GetStringNullable("SignatureSalt")
            };
        }
    }
}
