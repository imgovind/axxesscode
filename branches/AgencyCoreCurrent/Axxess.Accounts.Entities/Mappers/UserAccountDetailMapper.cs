﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Data;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;

    public class UserAccountDetailMapper : ISelectable<UserAccountDetail>
    {
        public UserAccountDetail ApplySelect(DataReader reader)
        {
            return new UserAccountDetail()
            {
                UserId = reader.GetGuid("UserId"),
                AgencyId = reader.GetGuid("AgencyId"),
                Created = reader.GetDateTime("Created"),
                Title = reader.GetStringNullable("Title"),
                AgencyName = reader.GetString("AgencyName"),
                AgencyPhone = reader.GetString("PhoneWork").ToPhone(),
                AgencyAddressCity = reader.GetStringNullable("AddressCity"),
                AgencyAddressLine1 = reader.GetStringNullable("AddressLine1"),
                AgencyAddressLine2 = reader.GetStringNullable("AddressLine2"),
                AgencyAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                AgencyAddressStateCode = reader.GetStringNullable("AddressStateCode")
            };
        }
    }
}
