﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Data;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;

    public class StringVariableMapper : ISelectable<StringVariable>
    {
        public StringVariable ApplySelect(DataReader reader)
        {
            return new StringVariable
            {
                Name = reader.GetStringNullable("Name"),
                Value = reader.GetStringNullable("Value")
            };
        }
    }
}
