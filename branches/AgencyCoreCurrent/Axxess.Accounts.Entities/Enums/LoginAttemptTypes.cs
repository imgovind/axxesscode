﻿namespace Axxess.Accounts.Entities.Enums
{
    using System;

    public enum LoginAttemptType
    {
        Failed,
        Success,
        Locked,
        Deactivated,
        TrialPeriodOver,
        AccountSuspended,
        TooManyAttempts,
        WeekendAccessRestricted,
        AccountInUse,
        AgencyFrozen,
        TrialAccountExpired,
        AgencyReadOnlyPeriodExpired
    }
}
