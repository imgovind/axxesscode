﻿namespace Axxess.Accounts.Entities.Enums
{
    using System;
    using System.ComponentModel;

    public enum UserStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Inactive")]
        Inactive = 2
    }
}
