﻿namespace Axxess.Accounts.Entities.Enums
{
    using System;

    public enum ApplicationRoles : byte
    {
        None = 0,
        AxxessAdmin = 1,
        ApplicationUser = 2,
        Physician = 3
    }
}
