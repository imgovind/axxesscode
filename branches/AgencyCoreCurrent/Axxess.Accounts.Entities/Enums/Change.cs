﻿namespace Axxess.Accounts.Entities.Enums
{
    using System;

    public enum Change
    {
        Password,
        Signature,
        Nothing
    }
}
