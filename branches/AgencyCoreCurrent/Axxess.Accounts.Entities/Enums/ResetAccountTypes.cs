﻿namespace Axxess.Accounts.Entities.Enums
{
    using System;

    public enum ResetAccountTypes
    {
        None = 0,
        Password = 1,
        Signature = 2
    }
}
