﻿namespace Axxess.Accounts.Entities
{
    using System;

    public interface IDataCache<T> where T : class, new()
    {
        int Size();
        void Purge();
        T Get(Guid key);
        bool Exist(Guid key);
        void Delete(Guid key);
        void Add(Guid key, T val);
    }
}
