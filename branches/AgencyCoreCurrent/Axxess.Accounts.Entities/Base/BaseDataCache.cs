﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Framework.Extensions;

    public abstract class BaseDataCache<T> : IDataCache<T> where T : class, new()
    {
        protected Dictionary<Guid, T> innerCache = new Dictionary<Guid, T>();
        protected ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();

        public virtual void Add(Guid key, T val)
        {
            using (cacheLock.AcquireWriteLock())
            {
                innerCache[key] = val;
            }
        }

        public virtual T Get(Guid key)
        {
            using (cacheLock.AcquireReadLock())
            {
                T val;
                innerCache.TryGetValue(key, out val);
                return val;
            }
        }

        public virtual void Delete(Guid key)
        {
            using (cacheLock.AcquireWriteLock())
            {
                innerCache.Remove(key);
            }
        }

        public virtual void Purge()
        {
            using (cacheLock.AcquireWriteLock())
            {
                innerCache.Clear();
            }
        }

        public virtual bool Exist(Guid key)
        {
            using (cacheLock.AcquireReadLock())
            {
                return innerCache.ContainsKey(key);
            }
        }

        public virtual int Size()
        {
            using (cacheLock.AcquireReadLock())
            {
                return innerCache.Count;
            }
        }
    }
}
