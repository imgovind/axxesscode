﻿namespace Axxess.Accounts.Entities
{
    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Axxess.Framework;
    using Axxess.Framework.Validation;

    [Serializable]
    public abstract class EntityBase : Disposable
    {
        #region Validation

        private List<Validation> rules = new List<Validation>();

        protected virtual void AddValidationRule(Validation validation)
        {
            if (!rules.Contains(validation))
            {
                rules.Add(validation);
            }
        }

        protected abstract void AddValidationRules();

        [XmlIgnore]
        [ScriptIgnore]
        public bool IsValid
        {
            get
            {
                ClearRules();
                AddValidationRules();
                return AreRulesValid();
            }
        }

        private void ClearRules()
        {
            rules.Clear();
        }

        [XmlIgnore]
        [ScriptIgnore]
        private EntityValidator validator;

        private bool AreRulesValid()
        {
            validator = new EntityValidator(
                  this.rules.ToArray()
                   );
            validator.Validate();
            return validator.IsValid;
        }

        [XmlIgnore]
        [ScriptIgnore]
        public virtual string ValidationMessage
        {
            get
            {
                return validator.Message;
            }
        }

        [XmlIgnore]
        [ScriptIgnore]
        public List<Validation> Rules
        {
            get
            {
                return rules;
            }
        }

        public void AddMoreRules(Validation validation)
        {
            if (!rules.Contains(validation))
            {
                rules.Add(validation);
            }
        }

        #endregion

        #region IDisposable

        private bool isDisposed;
        protected bool IsDisposed
        {
            get { return isDisposed; }
        }

        protected override void Dispose(bool disposing)
        {
            if (this.IsDisposed)
                return;

            if (disposing)
            {
                rules.Clear();
                isDisposed = true;
            }
        }

        #endregion
    }
}
