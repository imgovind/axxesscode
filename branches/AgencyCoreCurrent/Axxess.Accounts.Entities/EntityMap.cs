﻿namespace Axxess.Accounts.Entities
{
    using System;
    
    using Axxess.Framework.DataAccess;

    public static class EntityMap
    {
        public static void Register()
        {
            Mapper.Register<Login>(new LoginMapper());
            Mapper.Register<UserAccount>(new UserAccountMapper());
            Mapper.Register<StringVariable>(new StringVariableMapper());
            Mapper.Register<UserAccountDetail>(new UserAccountDetailMapper());
        }
    }
}
