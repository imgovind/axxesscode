﻿namespace Axxess.Api.Oasis
{
    using System;
    using System.Diagnostics;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;
    using HomeHealthGold.Audits;

    static class Program
    {
        static void Main()
        {
            try
            {
                OasisAudits.SetLicenseCode("NgAAAEF38/4Tq84B+0R0duXJzwE+tKDfsibLs2J7DIWWQnFe6SHoyhMuBbSMdoIzgoT2N70g7xpAmVYI57dd+Z866F8=");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
		        { 
			        new ValidationWindowsService()
		        };
                ServiceBase.Run(ServicesToRun);
                
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
