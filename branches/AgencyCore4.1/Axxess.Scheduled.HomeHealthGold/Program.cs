﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Domain;
    
    class Program
    {
        #region Private Members

        private static Guid AgencyId;
        private static string oasisOutput;
        private static string servicesOutput;
        private static string admissionsOutput;
        private static Dictionary<Guid, string> assessmentStatusList = null;
        private static Dictionary<string, string> cbsaLookup = new Dictionary<string, string>();

        #endregion

        #region Main Methods

        static void Main(string[] args)
        {
            Bootstrapper.Run();
            LoopThroughAgencyData();
            Console.ReadLine();
        }

        private static void Initialize(Guid agencyId)
        {
            AgencyId = agencyId;

            assessmentStatusList = new Dictionary<Guid, string>();

            var ticks = DateTime.Now.Ticks;
            oasisOutput = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("oasis_{0}_{1}.txt", agencyId, ticks));
            if (File.Exists(oasisOutput))
            {
                File.Delete(oasisOutput);
            }

            servicesOutput = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("services_{0}_{1}.txt", agencyId, ticks));
            if (File.Exists(servicesOutput))
            {
                File.Delete(servicesOutput);
            }

            admissionsOutput = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("admissions_{0}_{1}.txt", agencyId, ticks));
            if (File.Exists(admissionsOutput))
            {
                File.Delete(admissionsOutput);
            }
        }

        private static void LoopThroughAgencyData()
        {
            var hhgCustomers = Database.GetHhgCustomers();
            if (hhgCustomers != null && hhgCustomers.Count > 0)
            {
                hhgCustomers.ForEach(customer =>
                {
                    Initialize(customer.AgencyId);

                    var agency = Database.GetAgency(AgencyId);
                    if (agency != null)
                    {
                        var users = Database.GetAgencyUsers(AgencyId);
                        var patients = Database.GetAgencyPatients(AgencyId);
                        var physicians = Database.GetAgencyPhysicians(AgencyId);
                        if (patients != null && patients.Count > 0)
                        {
                            using (TextWriter oasisWriter = new StreamWriter(oasisOutput, false))
                            {
                                using (TextWriter servicesWriter = new StreamWriter(servicesOutput, false))
                                {
                                    using (TextWriter admissionDataWriter = new StreamWriter(admissionsOutput, false))
                                    {
                                        int admissionCounter = 1;
                                        patients.ForEach(patient =>
                                        {
                                            if (patient != null)
                                            {
                                                var patientAdmissions = Database.GetPatientAdmissions(AgencyId, patient.Id);
                                                if (patientAdmissions != null && patientAdmissions.Count > 0)
                                                {
                                                    patientAdmissions.ForEach(patientAdmission =>
                                                    {
                                                        var primaryPhysician = Database.GetPatientPhysician(patient.Id);

                                                        var physician = physicians != null
                                                            && primaryPhysician != null
                                                            ? physicians.Find(u => u.Id == primaryPhysician.PhysicianId)
                                                            : null;

                                                        var admissionNumber = GetAdmissionId(patientAdmissions, patient.Id, patient.AdmissionId);

                                                        var clinician = users != null
                                                            && !patient.UserId.IsEmpty()
                                                            ? users.Find(u => u.Id == patient.UserId)
                                                            : null;

                                                        var admissionLine = CreatePatientAdmissionLine(admissionNumber, patientAdmission, clinician, physician);

                                                        var adminBatch = Database.GetHhgDataBatch(patientAdmission.Id, AgencyId, patient.Id);
                                                        if (adminBatch != null)
                                                        {
                                                            if (patientAdmission.IsDeprecated)
                                                            {
                                                                adminBatch.IsDeprecated = true;
                                                                adminBatch.LastModified = patientAdmission.Modified;
                                                                if (Database.Update<HhgBatch>(adminBatch))
                                                                {
                                                                    Console.WriteLine("Deleted Admin Batch");
                                                                    admissionDataWriter.WriteLine("D{0}", admissionLine);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (patientAdmission.Modified != adminBatch.LastModified
                                                                        || patientAdmission.Status != adminBatch.Status)
                                                                {
                                                                    adminBatch.Status = patientAdmission.Status;
                                                                    adminBatch.LastModified = patientAdmission.Modified;
                                                                    if (Database.Update<HhgBatch>(adminBatch))
                                                                    {
                                                                        Console.WriteLine("Updated Admin Batch");
                                                                        admissionDataWriter.WriteLine("C{0}", admissionLine);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            adminBatch = new HhgBatch
                                                            {
                                                                PatientId = patient.Id,
                                                                Id = patientAdmission.Id,
                                                                AgencyId = customer.AgencyId,
                                                                Status = patientAdmission.Status,
                                                                LastModified = patientAdmission.Modified,
                                                                Created = DateTime.Now
                                                            };
                                                            if (Database.Add<HhgBatch>(adminBatch))
                                                            {
                                                                Console.WriteLine("New Admin Batch");
                                                                admissionDataWriter.WriteLine("N{0}", admissionLine);
                                                            }
                                                        }

                                                        var admissionEpisodes = Database.GetPatientAdmissionEpisodes(AgencyId, patient.Id, patientAdmission.Id);
                                                        if (admissionEpisodes != null && admissionEpisodes.Count > 0)
                                                        {
                                                            admissionEpisodes.ForEach(admissionEpisode =>
                                                            {
                                                                if (admissionEpisode != null && admissionEpisode.Schedule.IsNotNullOrEmpty())
                                                                {
                                                                    var schedule = admissionEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                                                    if (schedule != null && schedule.Count > 0)
                                                                    {
                                                                        var billableVisits = schedule.Where(s =>
                                                                            !s.EventId.IsEmpty() && s.VisitDate.IsDate()
                                                                            && !s.IsMissedVisit && s.IsBillable).ToList();
                                                                        if (billableVisits != null && billableVisits.Count > 0)
                                                                        {
                                                                            billableVisits.ForEach(visit =>
                                                                            {
                                                                                var serviceLine = CreateServiceLine(patient.PatientIdNumber, admissionNumber, visit.VisitDate, Clean(visit.DisciplineTaskName));
                                                                                var serviceBatch = Database.GetHhgDataBatch(visit.EventId, AgencyId, patient.Id);
                                                                                if (serviceBatch != null)
                                                                                {
                                                                                    if (visit.IsDeprecated)
                                                                                    {
                                                                                        serviceBatch.IsDeprecated = true;
                                                                                        if (Database.Update<HhgBatch>(serviceBatch))
                                                                                        {
                                                                                            Console.WriteLine("Deleted Service Batch");
                                                                                            servicesWriter.WriteLine("D{0}", serviceLine);
                                                                                            assessmentStatusList.Add(visit.EventId, "D");
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (visit.Status != adminBatch.Status.ToString())
                                                                                        {
                                                                                            serviceBatch.Status = visit.Status.ToInteger();
                                                                                            if (Database.Update<HhgBatch>(serviceBatch))
                                                                                            {
                                                                                                Console.WriteLine("Updated Service Batch");
                                                                                                servicesWriter.WriteLine("C{0}", serviceLine);
                                                                                                assessmentStatusList.Add(visit.EventId, "C");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (!visit.IsDeprecated)
                                                                                    {
                                                                                        serviceBatch = new HhgBatch
                                                                                        {
                                                                                            Id = visit.EventId,
                                                                                            PatientId = patient.Id,
                                                                                            AgencyId = customer.AgencyId,
                                                                                            Status = visit.Status.ToInteger(),
                                                                                            LastModified = DateTime.Now,
                                                                                            Created = DateTime.Now
                                                                                        };
                                                                                        if (Database.Add<HhgBatch>(serviceBatch))
                                                                                        {
                                                                                            Console.WriteLine("New Service Batch");
                                                                                            servicesWriter.WriteLine("N{0}", serviceLine);
                                                                                            assessmentStatusList.Add(visit.EventId, "N");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }

                                                                        var assessmentEvents = schedule.Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate()
                                                                            && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.IsAssessment()
                                                                            && (s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedExportReady
                                                                            || s.Status.ToInteger() == (int)ScheduleStatus.OasisExported
                                                                            || s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedNotExported)).ToList();
                                                                        if (assessmentEvents != null && assessmentEvents.Count > 0)
                                                                        {
                                                                            assessmentEvents.ForEach(assessmentVisit =>
                                                                            {
                                                                                var status = assessmentStatusList != null ? assessmentStatusList[assessmentVisit.EventId] : " ";
                                                                                if (status.IsNotNullOrEmpty())
                                                                                {
                                                                                    var assessingClinician = users != null
                                                                                        && !assessmentVisit.UserId.IsEmpty()
                                                                                        ? users.Find(u => u.Id == assessmentVisit.UserId)
                                                                                        : null;
                                                                                    var assessment = Database.GetAssessment(AgencyId, patient.Id, assessmentVisit.EventId, assessmentVisit.GetAssessmentType());
                                                                                    var oasisLine = CreateOasisLine(patient.PatientIdNumber, admissionNumber, assessingClinician, assessment);

                                                                                    Console.WriteLine("Assessment Batch");
                                                                                    oasisWriter.WriteLine("{0}{1}", status, oasisLine);
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        admissionCounter++;
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }

        #endregion

        #region Helper Functions

        private static string CreatePatientAdmissionLine(string admissionNumber, PatientAdmissionDate patientAdmission, User clinician, AgencyPhysician physician)
        {
            if (patientAdmission != null && patientAdmission.PatientData.IsNotNullOrEmpty())
            {
                var patient = patientAdmission.PatientData.ToObject<Patient>();
                if (patient != null)
                {
                    var cbsaCode = GetCbsaCode(patient.AddressZipCode);

                    return new StringBuilder(160)
                        .Append(patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(string.Empty.PadRight(25))
                        .Append((clinician != null ? clinician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append((physician != null ? physician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append(cbsaCode.Trim().PadRight(5).PartOfString(0, 5))
                        .ToString();
                }
            }
            return string.Empty;
        }

        private static string CreateServiceLine(string patientId, string admissionNumber, string visitDate, string taskName)
        {
            return new StringBuilder(80)
                        .Append(patientId.IsNotNullOrEmpty() ? patientId.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(visitDate.IsNotNullOrEmpty() && visitDate.IsDate() ? visitDate.ToZeroFilled().Trim().PadRight(10).PartOfString(0, 10) : string.Empty.PadRight(10))
                        .Append(taskName.IsNotNullOrEmpty() ? taskName.ToUpperCase().Trim().PadRight(30).PartOfString(0, 30) : string.Empty.PadRight(30))
                        .ToString();
        }

        private static string CreateOasisLine(string patientId, string admissionNumber, User clinician, Assessment assessment)
        {
            return new StringBuilder(1513)
                        .Append(patientId.IsNotNullOrEmpty() ? patientId.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append((clinician != null ? clinician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append((assessment != null && assessment.SubmissionFormat.IsNotNullOrEmpty() ? assessment.SubmissionFormat : string.Empty).Trim().PadRight(1448).PartOfString(0, 1448))
                        .ToString();
        }

        private static string GetAdmissionId(List<PatientAdmissionDate> patientAdmissions, Guid patientId, Guid patientAdmissionId)
        {
            string admissionNumber = string.Empty;
            if (patientAdmissions != null && patientAdmissions.Count > 0)
            {
                int index = patientAdmissions.FindIndex(p => p.Id == patientAdmissionId);
                if (index >= 0)
                {
                    admissionNumber = (index + 1).ToString().PadLeft(4, '0');
                }
            }

            return admissionNumber;
        }

        private static string GetCbsaCode(string zipCode)
        {
            var cbsa = string.Empty;
            if (zipCode.IsNotNullOrEmpty())
            {
                if (cbsaLookup.ContainsKey(zipCode))
                {
                    cbsa = cbsaLookup[zipCode];
                }
                else
                {
                    var cbsaCode = Database.GetCbsaCode(zipCode);
                    if (cbsaCode != null && cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        cbsa = cbsaCode.CBSA;
                        cbsaLookup.Add(zipCode, cbsa);
                    }
                }
            }
            return cbsa;
        }

        private static string Clean(string taskName)
        {
            var text = taskName;
            if (taskName.IsNotNullOrEmpty())
            {
                if (taskName.ToLowerInvariant().StartsWith("oasis", StringComparison.CurrentCultureIgnoreCase)
                    && !taskName.ToLowerInvariant().Contains("(pt)")
                    && !taskName.ToLowerInvariant().Contains("(ot)"))
                {
                    text = string.Format("SN {0}", taskName);
                }
            }
            return text;
        }
        #endregion
    }
}
