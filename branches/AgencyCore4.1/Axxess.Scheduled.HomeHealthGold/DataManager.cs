﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using System.Collections.Generic;
    using System;

    internal sealed class DataManager
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.Initialize();
            }

            internal static readonly DataManager instance = new DataManager();
        }

        #endregion

        #region Private Members

        private int batchId;

        #endregion

        #region Public Instance

        public static DataManager Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private DataManager() { }

        private void Initialize()
        {
        }

        private void Flush()
        {
        }

        #endregion

        #region Public Methods

        public void Add(string providerNo, string activationCode, string headerFormat, string oasisFormat, string trailerFormat, string clinician, string clinicianSpecialty)
        {
            
        }

        #endregion
    }
}
