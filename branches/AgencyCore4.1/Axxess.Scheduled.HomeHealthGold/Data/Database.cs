﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    using Axxess.LookUp.Domain;

    using SubSonic.Repository;

    public static class Database
    {
        #region Private Members

        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);
        
        private const string SELECT_OASIS_HEADERFORMAT = @"SELECT * FROM `submissionheaderformats`";
        private const string SELECT_AGENCY_PATIENTS = @"SELECT * FROM `patients` WHERE agencyid = @agencyid AND IsDeprecated = 0;";
        private const string SELECT_AGENCY_LOCATION = @"SELECT * FROM `agencylocations` WHERE agencyid = @agencyid AND IsDeprecated = 0 LIMIT 0,1;";
        private const string SELECT_AGENCY_USERS = @"SELECT Id, FirstName, LastName, Credentials, CredentialsOther FROM `users` WHERE AgencyId = @agencyid;";
        private const string SELECT_PATIENT_EPISODES = 
                "SELECT Id, Schedule FROM patientepisodes WHERE AgencyId = @agencyid AND PatientId = @patientid AND IsActive = 1;";

        private const string SELECT_PATIENTADMISSION_EPISODES =
                "SELECT Id, Schedule FROM patientepisodes WHERE AgencyId = @agencyid AND PatientId = @patientid " +
                "AND AdmissionId = @admissionid AND patientepisodes.IsActive = 1;";

        private const string SELECT_OASIS_ASSESSMENT = @"SELECT SubmissionFormat, `Status`, Modified FROM `{0}` WHERE AgencyId = @agencyid AND PatientId = @patientid AND id = @id AND IsDeprecated = 0 LIMIT 0, 1;";

        private const string SELECT_HHG_BATCH = @"SELECT * FROM `hhgbatches` WHERE Id = @id AND AgencyId = @agencyid AND PatientId = @patientid AND IsDeprecated = 0 LIMIT 0, 1;";
        private const string SELECT_HHG_CUSTOMERS = @"SELECT * FROM `hhglogins` WHERE IsDeprecated = 0;";

        #endregion

        #region Internal Methods

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<HhgLogin> GetHhgCustomers()
        {
            var hhgCustomers = new List<HhgLogin>();
            using (var cmd = new FluentCommand<HhgLogin>(SELECT_HHG_CUSTOMERS))
            {
                hhgCustomers = cmd
                    .SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new HhgLogin()
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId")
                }).AsList();
            }
            return hhgCustomers;
        }

        internal static List<Patient> GetAgencyPatients(Guid agencyId)
        {
            var patients = new List<Patient>();
            using (var cmd = new FluentCommand<Patient>(SELECT_AGENCY_PATIENTS))
            {
                patients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AsList();
            }
            return patients;
        }

        internal static List<PatientEpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            var list = new List<PatientEpisodeData>();

            using (var cmd = new FluentCommand<PatientEpisodeData>(SELECT_PATIENT_EPISODES))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static List<PatientEpisodeData> GetPatientAdmissionEpisodes(Guid agencyId, Guid patientId, Guid admissionId)
        {
            var list = new List<PatientEpisodeData>();

            using (var cmd = new FluentCommand<PatientEpisodeData>(SELECT_PATIENTADMISSION_EPISODES))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("admissionid", admissionId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static Assessment GetAssessment(Guid agencyId, Guid patientId, Guid assessmentId, AssessmentType assessmentType)
        {
            Assessment assessment = null;
            var sql = string.Format(SELECT_OASIS_ASSESSMENT, GetAssessmentTableName(assessmentType));
            using (var cmd = new FluentCommand<Assessment>(sql))
            {
                assessment = cmd.SetConnection("OasisCConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", assessmentId)
                .SetMap(reader => new Assessment
                {
                    Status = reader.GetInt("Status"),
                    Modified = reader.GetDateTime("Modified"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                })
                .AsSingle();
            }
            return assessment;
        }

        internal static HhgBatch GetHhgDataBatch(Guid id, Guid agencyId, Guid patientId)
        {
            HhgBatch hhgBatch = null;
            using (var cmd = new FluentCommand<HhgBatch>(SELECT_HHG_BATCH))
            {
                hhgBatch = cmd
                    .SetConnection("AgencyManagementConnectionString")
                    .AddGuid("id", id)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new HhgBatch
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        PatientId = reader.GetGuid("PatientId"),
                        Status = reader.GetInt("Status"),
                        LastModified = reader.GetDateTime("LastModified"),
                        Created = reader.GetDateTime("Created")
                    }).AsSingle();
            }
            return hhgBatch;
        }

        internal static Dictionary<string, SubmissionHeaderFormat> GetOasisHeaderInstructionsNew()
        {
            var format = GetSubmissionHeaderFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionHeaderFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        internal static List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions()
        {
            var list = new List<SubmissionHeaderFormat>();

            using (var cmd = new FluentCommand<SubmissionHeaderFormat>(SELECT_OASIS_HEADERFORMAT))
            {
                list = cmd.SetConnection("OasisCConnectionString")
                .SetMap(reader => new SubmissionHeaderFormat
                {
                    Item = reader.GetStringNullable("Item"),
                    Length = reader.GetDouble("Length"),
                    Start = reader.GetDouble("Start"),
                    End = reader.GetDouble("End"),
                    PadType = reader.GetStringNullable("PadType"),
                    DataType = reader.GetStringNullable("DataType"),
                    DefaultValue = reader.GetStringNullable("DefaultValue")
                })
                .AsList();
            }
            return list;
        }

        internal static List<User> GetAgencyUsers(Guid agencyId)
        {
            var list = new List<User>();

            using (var cmd = new FluentCommand<User>(SELECT_AGENCY_USERS))
            {
                list = cmd
                    .SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new User
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetString("FirstName"),
                        LastName = reader.GetString("LastName"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        CredentialsOther = reader.GetStringNullable("CredentialsOther")
                    })
                    .AsList();
            }
            return list;
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId)
        {
            AgencyLocation location = agencyManagementDatabase.Single<AgencyLocation>(a => a.AgencyId == agencyId);

            if (location != null && !location.IsLocationStandAlone)
            {
                var agency = agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
                if (agency != null)
                {
                    location.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    location.NationalProviderNumber = agency.NationalProviderNumber;
                }
            }
            return location;

        }

        internal static Agency GetAgency(Guid agencyId)
        {
            return agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
        }

        internal static List<AgencyPhysician> GetAgencyPhysicians(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyPhysician>(a => a.AgencyId == agencyId).ToList();
        }

        internal static PatientPhysician GetPatientPhysician(Guid patientId)
        {
            return agencyManagementDatabase.Single<PatientPhysician>(a => a.PatientId == patientId && a.IsPrimary == true);
        }

        internal static CBSACode GetCbsaCode(string zipCode)
        {
            return lookupDatabase.Single<CBSACode>(c => c.Zip == zipCode);
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissions(Guid agencyId, Guid patientId)
        {
            return agencyManagementDatabase.Find<PatientAdmissionDate>(a => a.AgencyId == agencyId && a.PatientId == patientId).OrderBy(p => p.StartOfCareDate.ToZeroFilled()).ToList();
        }

        #endregion

        #region Private Methods

        private static string GetAssessmentTableName(AssessmentType assessmentType)
        {
            var tableName = string.Empty;
            switch (assessmentType)
            {
                case AssessmentType.StartOfCare:
                    tableName = "startofcareassessments";
                    break;
                case AssessmentType.DischargeFromAgency:
                    tableName = "dischargefromagencyassessments";
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    tableName = "deathathomeassessments";
                    break;
                case AssessmentType.FollowUp:
                    tableName = "followupassessments";
                    break;
                case AssessmentType.Recertification:
                    tableName = "recertificationassessments";
                    break;
                case AssessmentType.ResumptionOfCare:
                    tableName = "resumptionofcareassessments";
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    tableName = "transferdischargeassessments";
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    tableName = "transfernotdischargedassessments";
                    break;
                default:
                    break;
            }
            return tableName;
        }

        #endregion
        
    }
}
