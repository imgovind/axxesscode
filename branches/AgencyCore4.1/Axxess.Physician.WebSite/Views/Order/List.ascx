﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Signed Orders</span>
<div class="wrapper">
    <%= Html.Telerik().Grid<Order>().Name("List_CompleteOrders").Columns(columns => {
        columns.Bound(o => o.Number).Title("Order #").Sortable(true).Width(10);
        columns.Bound(o => o.TypeDescription).Title("Document").Sortable(true).Width(25);
        columns.Bound(o => o.AgencyName).Title("Agency").Sortable(true).Width(25);
        columns.Bound(o => o.PatientName).Title("Patient").Sortable(true).Width(25);
        columns.Bound(o => o.OrderDate).Title("Order Date").Sortable(true).Width(10);
        columns.Bound(o => o.PrintUrl).Title(" ").Width(3);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CompleteGrid", "Order")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>