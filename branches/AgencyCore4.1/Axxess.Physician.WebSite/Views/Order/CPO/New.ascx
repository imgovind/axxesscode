﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CarePlanOversight>" %>
<span class="wintitle">New Care Plan Oversight Log</span>
<% using (Html.BeginForm("AddCPO", "Order", FormMethod.Post, new { @id = "newCPO" })) { %>
<div class="wrapper main">
    <fieldset>
    <div class="column">
        <div class="row">
            <label for="New_CPO_Agency" class="float-left width200">Agency:</label>
            <div class="float-right"><%=Html.LookupSelectList("agency", "AgencyId","",new{@id="New_CPO_AgencyId", @class="requireddropdown"}) %></div>
        </div>
        <div class="clear" />
        <div class="row">
            <label for="New_CPO_Patient" class="float-left width200">Patient:</label>
            <div class="float-right"><%=Html.AgencyPhysicianPatient("PatientId","",Guid.Empty,new {@id="New_CPO_PatientId", @class="requireddropdown"}) %></div>
        </div>
        <div class="clear"></div>
        <div class="row">
            <label for="New_CPO_LogDate" class="float-left">Date:</label>
            <div class="float-right"><input type="date" class="data-picker required" name="LogDate" value="<%=DateTime.Now.ToShortDateString() %>" id="New_CPO_LogDate" /></div>
        </div>
        <div class="clear" />
    </div>
    <div class="column">
        <div class="row">
            <label for="New_CPO_Activity" class="float-left width200">CPO Type:</label>
            <div class="float-right"><%=Html.LookupSelectList("CPTCode", "CptCode", "", new { @id = "New_CPO_CptCode", @class = "requireddropdown" })%></div>
        </div>
        <div class="clear" />
        <div class="row">
            <label for="New_CPO_Activity" class="float-left width200">CPO Activity:</label>
            <div class="float-right"><%=Html.LookupSelectList("CPOActivity", "Activity", "", new { @id = "New_CPO_Activity", @class = "requireddropdown" })%></div>
        </div>
        <div class="clear" />
        <div class="row">
            <label for="New_CPO_Duration" class="float-left width200">Duration <em>(in minutes)</em>:</label>
            <div class="float-right"><%=Html.TextBox("Duration", "", new { @id = "New_CPO_Duration", @class = "numeric required", @style = "width: 100px;", @maxlength="4" })%></div>
        </div>
    </div>
    <div class="wide_column">
        <div class="row">
            <label for="New_CPO_Comments" class="float-left">Comments:</label>
        </div>
        <div class="row">
            <%=Html.TextArea("Comments", "", 16, 30, new { @id = "New_CPO_Comments", @class = "fill", @maxcharacters = "5000", @style="height: 160px;" })%>
        </div>
    </div>
</fieldset>
    <input type="hidden" name="button" id="CarePlanOversight_Button" />
    <%= Html.Hidden("Status", "", new { @id = "New_CPO_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_CPO_Status').val('415');Remove();CarePlanOversight.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_CPO_Status').val('425');Add();CarePlanOversight.Submit($(this));">Complete</a></li>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $(".numeric").numeric();
    $('#New_CPO_AgencyId').change(function() { CarePlanOversight.LoadPatientsDropDown('New_CPO_PatientId', $(this)); });
    function Remove() {
        $("#New_CPO_Activity").removeClass('requireddropdown');
    }
    function Add() {
        $("#New_CPO_Activity").removeClass('requireddropdown').addClass('requireddropdown');
    }
</script>