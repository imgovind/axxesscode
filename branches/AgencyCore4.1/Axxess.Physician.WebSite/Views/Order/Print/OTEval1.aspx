﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  string[] homeboundStatusAssist = data.AnswerArray("GenericHomeboundStatusAssist"); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E" +
        "<%= Model.TypeName %>" + 
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PhysicianDisplayName.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn")%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EOccupational Therapy <%= Model.Type == "OTDischarge" ? "Discharge" : (Model.Type == "OTReEvaluation" ? "Re-" : "") + "Evaluation" %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
printview.addsection(
        printview.col(4,
            printview.span("Medical Diagnosis",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedicalDiagnosis").Clean() %>",0,1) +
            printview.checkbox("Onset",<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisOnset").Equals("1").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("MedicalDiagnosisDate").Clean() %>",0,1) +
            printview.span("Therapy Diagnosis",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapyDiagnosis").Clean() %>",0,1) +
            printview.checkbox("Onset",<%= data.AnswerOrEmptyString("GenericTherapyDiagnosisOnset").Equals("1").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("TherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.col(8,
            printview.span("Precautions",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPrecautions").Clean() %>",0,1) +
            printview.span("Sensation",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSensation").Clean() %>",0,1) +
            printview.span("Muscle Tone",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMuscleTone").Clean() %>",0,1) +
            printview.span("ADL",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADL").Clean() %>",0,1) +
            printview.span("Endurance",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericEndurance").Clean() %>",0,1) +
            printview.span("Edema",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericEdema").Clean() %>",0,1) +
            printview.span("Coordination",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericCoordination").Clean() %>",0,1)),
        "Diagnosis");
printview.addsection(
        printview.span("Prior Level of Function",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorFunctionalStatus").Clean() %>",0,1) +
        
        printview.span("Pertinent Medical History",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalHistory").Clean() %>",0,1),
       
        "PLOF and Medical History");
printview.addsection(
        printview.col(4,
            printview.span("Pain Location",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentLocation").Clean() %>",0,1) +
            printview.span("Pain Level",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainLevel").Clean() %>",0,1) +
            printview.span("Increased by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy").Clean() %>",0,1) +
            printview.span("Relieved by",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy").Clean() %>",0,1)),
        "Pain Assessment");
printview.addsection(
        printview.col(4,
            printview.checkbox("Gait",<%= homeboundStatusAssist.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Leaving the Home",<%= homeboundStatusAssist.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfers",<%= homeboundStatusAssist.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("SOB/Endurance",<%= homeboundStatusAssist.Contains("4").ToString().ToLower() %>)),
        "Homebound Status");
printview.addsection(
        printview.col(3,
            printview.span("") +
            printview.span("%3Cspan class=%22align-center fill%22%3EROM%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3EStrength%3C/span%3E",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderAbductionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderIntRotStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShoulderExtRotStrengthLeft").Clean() %>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericElbowExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFingerExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWristExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Hip",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipAbductionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipIntRotStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHipExtRotStrengthLeft").Clean() %>",0,1) +
            printview.span("Knee",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericKneeExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Ankle",true) +
            printview.span("Plantarflexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnklePlantFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Dorsiflexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAnkleFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkRotationStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTrunkExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckExtensionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Lat Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Long Flexion") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthLeft").Clean() %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationROMLeft").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthRight").Clean() %>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericNeckRotationStrengthLeft").Clean() %>",0,1)),
        "Physical Assessment");
printview.addsection(
        printview.col(6,
            printview.span("Bathing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLBathing").Clean() %>",0,1) +
            printview.span("UE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLUEDressing").Clean() %>",0,1) +
            printview.span("LE Dressing",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLLEDressing").Clean() %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLGrooming").Clean() %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLToileting").Clean() %>",0,1) +
            printview.span("Feeding",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLFeeding").Clean() %>",0,1) +
            printview.span("Meal Prep",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLMealPrep").Clean() %>",0,1) +
            printview.span("House Cleaning",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLHouseCleaning").Clean() %>",0,1) +
            printview.span("Adapt. Equip.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLAdaptiveEquipment").Clean() %>",0,1)) +
        printview.col(2,
            printview.col(2,
                printview.span("Bed Mobility",true) +
                printview.col(2,
                    printview.span("Rolling:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilityRolling").Clean() %>",0,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Supine&#8594;Sit:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilitySupineToSit").Clean() %>",0,1)) +
                printview.col(2,
                    printview.span("Sit&#8594;Stand:",true) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericADLBedMobilitySitToStand").Clean() %>",0,1)))) +
        printview.col(5,
            printview.span("Transfers",true) +
            printview.span("Tub/Shower/Toilet:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTransfersTubShowerToilet").Clean() %>",0,1) +
            printview.span("Bed&#8594;Chair:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLTransfersBedToChairWheelchair").Clean() %>",0,1)) +
        printview.col(2,
            printview.span("W/C Mobility:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericADLWCMobility").Clean() %>",0,1) +
            printview.col(2,
                printview.span("Sitting Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("Standing Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("0").ToString().ToLower() %>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("0").ToString().ToLower() %>))),
        "ADLs / Functional Mobility Level / Level of Assist");
    printview.addsection(
        printview.col(5,
            printview.span("Sitting Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist").Clean() %>% Assist",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist").Clean() %>% Assist",0,1) +
            printview.span("Standing Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesStaticAssist").Clean() %>% Assist",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data.AnswerOrEmptyString("GenericStandingBalanceActivitiesDynamicAssist").Clean() %>% Assist",0,1)) +
        printview.checkbox("UE Weight-Bearing Activities",<%= data.AnswerOrEmptyString("GenericUEWeightBearing").Equals("1").ToString().ToLower() %>),
        "Neuromuscular Reeducation");
</script>
<%  string[] genericTreatmentCodes = data.AnswerArray("GenericTreatmentCodes"); %>
<script type="text/javascript">
printview.addsection(
        printview.col(4,
            printview.checkbox("B1 Evaluation",<%= genericTreatmentCodes.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("B2 Thera Ex",<%= genericTreatmentCodes.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("B3 Transfer Training",<%= genericTreatmentCodes.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("B4 Home Program",<%= genericTreatmentCodes.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("B5 Gait Training",<%= genericTreatmentCodes.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("B6 Chest PT",<%= genericTreatmentCodes.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("B7 Ultrasound",<%= genericTreatmentCodes.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("B8 Electrother",<%= genericTreatmentCodes.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("B9 Prosthetic Training",<%= genericTreatmentCodes.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("B10 Muscle Re-ed",<%= genericTreatmentCodes.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("B11 Muscle Re-ed",<%= genericTreatmentCodes.Contains("11").ToString().ToLower() %>) +
            printview.span("Other: <%= data.AnswerOrEmptyString("GenericTreatmentCodesOther").Clean() %>")),
        "Treatment Codes");
</script>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("Decreased Strength",<%= genericAssessment.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Decreased ROM",<%= genericAssessment.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Decreased ADL",<%= genericAssessment.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Ind",<%= genericAssessment.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Decreased Mobility",<%= genericAssessment.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Pain",<%= genericAssessment.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Decreased Endurance",<%= genericAssessment.Contains("7").ToString().ToLower() %>) +
            printview.span("") +
            printview.span("Rehab Potential",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericRehabPotential").Clean() %>",0,1) +
            printview.span("Prognosis",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPrognosis").Clean() %>",0,1)) +
        printview.span("Comments",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericAssessmentMore").Clean() %>",0,2),
        "Assessment");
printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal1").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal1TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal2").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal2TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal3").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal3TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal4").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal4TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal5").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericShortTermGoal5TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Freq.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShortTermFrequency").Clean() %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericShortTermDuration").Clean() %>",0,1)),
        "Short Term Goals");
printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal1").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal1TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal2").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal2TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal3").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal3TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal4").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal4TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal5").Clean()%>",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericLongTermGoal5TimeFrame").Clean()%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Freq.",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericLongTermFrequency").Clean() %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericLongTermDuration").Clean() %>",0,1)),
        "Long Term Goals");
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>
