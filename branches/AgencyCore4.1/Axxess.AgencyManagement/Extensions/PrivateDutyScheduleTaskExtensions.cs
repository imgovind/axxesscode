﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.OasisC.Enums;

    public static class PrivateDutyScheduleTaskExtensions
    {
        public static bool IsAssessment(this PrivateDutyScheduleTask scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.GetCustomCategory().IsEqual("OASIS")).ToList();

                if (oasis != null && oasis.Exists(d => (int)d == scheduleEvent.DisciplineTask))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsNote(this PrivateDutyScheduleTask task)
        {
            if (task != null && task.DisciplineTask > 0)
            {
                return task.TypeOfEvent() == "Notes";
            }
            return false;
        }


        public static bool IsSkilledNurseNote(this PrivateDutyScheduleTask scheduleEvent)
        {
            var result = false;

            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && Enum.IsDefined(typeof(DisciplineTasks),scheduleEvent.DisciplineTask))
            {
                if (IsSkilledNurseType((DisciplineTasks)scheduleEvent.DisciplineTask))
                {
                    result = true;
                }
            }

            return result;
        }

        private static bool IsSkilledNurseType(DisciplineTasks disciplineTasks)
        {
            var result = false;

            switch (disciplineTasks)
            {
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.SNInsulinHS:
                case DisciplineTasks.SNInsulinNoon:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNPsychAssessment:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }



        public static bool IsCompleted(this PrivateDutyScheduleTask task)
        {
            if (task != null && task.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = task.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(task.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), task.Status) : ScheduleStatus.NoStatus;

                if (status == ScheduleStatus.OrderReturnedWPhysicianSignature
                    || status == ScheduleStatus.OrderSentToPhysician
                    || status == ScheduleStatus.OrderToBeSentToPhysician
                    || status == ScheduleStatus.OrderSubmittedPendingReview
                    || status == ScheduleStatus.OrderSentToPhysicianElectronically

                    || status == ScheduleStatus.EvalSentToPhysician
                    || status == ScheduleStatus.EvalToBeSentToPhysician
                    || status == ScheduleStatus.EvalReturnedWPhysicianSignature
                    || status == ScheduleStatus.EvalSentToPhysicianElectronically

                    || status == ScheduleStatus.NoteCompleted
                    || status == ScheduleStatus.NoteMissedVisit
                    || status == ScheduleStatus.NoteMissedVisitComplete
                    || status == ScheduleStatus.NoteSubmittedWithSignature
                    || status == ScheduleStatus.OasisCompletedExportReady
                    || status == ScheduleStatus.OasisCompletedPendingReview
                    || status == ScheduleStatus.OasisExported
                    || status == ScheduleStatus.OasisCompletedNotExported
                    || status == ScheduleStatus.ReportAndNotesCompleted
                    || status == ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompletelyFinished(this PrivateDutyScheduleTask task)
        {
            if (task != null && task.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = task.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(task.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), task.Status) : ScheduleStatus.NoStatus;

                if (status == ScheduleStatus.NoteCompleted
                    || status == ScheduleStatus.OrderSentToPhysician
                    || status == ScheduleStatus.OrderToBeSentToPhysician
                    || status == ScheduleStatus.OrderReturnedWPhysicianSignature
                    || status == ScheduleStatus.OrderSentToPhysicianElectronically
                    || status == ScheduleStatus.EvalSentToPhysician
                    || status == ScheduleStatus.EvalToBeSentToPhysician
                    || status == ScheduleStatus.EvalReturnedWPhysicianSignature
                    || status == ScheduleStatus.EvalSentToPhysicianElectronically
                    || status == ScheduleStatus.OasisCompletedExportReady
                    || status == ScheduleStatus.OasisExported
                    || status == ScheduleStatus.OasisCompletedNotExported
                    || status == ScheduleStatus.NoteMissedVisitComplete)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOrder(this PrivateDutyScheduleTask task)
        {
            if (task != null)
            {
                Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), task.Discipline);
                if (discipline == Disciplines.Orders)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsReport(this PrivateDutyScheduleTask task)
        {
            if (task != null)
            {
                Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), task.Discipline);
                if (discipline == Disciplines.ReportsAndNotes)
                {
                    return true;
                }
            }
            return false;
        }

        public static string TypeOfEvent(this PrivateDutyScheduleTask task)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), task.DisciplineTask))
            {
                var type = ((DisciplineTasks)task.DisciplineTask).ToString();
                switch (type)
                {
                    case "OASISCDeath":
                    case "OASISCDeathOT":
                    case "OASISCDeathPT":
                    case "OASISCDischarge":
                    case "OASISCDischargeOT":
                    case "OASISCDischargePT":
                    case "NonOASISDischarge":
                    case "OASISCFollowUp":
                    case "OASISCFollowupPT":
                    case "OASISCFollowupOT":
                    case "OASISCRecertification":
                    case "OASISCRecertificationPT":
                    case "OASISCRecertificationOT":
                    case "NonOASISRecertification":
                    case "OASISCResumptionofCare":
                    case "OASISCResumptionofCarePT":
                    case "OASISCResumptionofCareOT":
                    case "OASISCStartofCare":
                    case "OASISCStartofCarePT":
                    case "OASISCStartofCareOT":
                    case "NonOASISStartofCare":
                    case "OASISCTransfer":
                    case "OASISCTransferPT":
                    case "OASISCTransferOT":
                    case "OASISCTransferDischarge":
                    case "OASISCTransferDischargePT":
                    case "SNAssessment":
                    case "SNAssessmentRecert":
                        return "OASIS";
                    case "SkilledNurseVisit":
                    case "Labs":
                    case "InitialSummaryOfCare":
                    case "SNInsulinAM":
                    case "SNInsulinPM":
                    case "SNInsulinHS":
                    case "SNInsulinNoon":
                    case "FoleyCathChange":
                    case "SNB12INJ":
                    case "SNBMP":
                    case "SNCBC":
                    case "SNHaldolInj":
                    case "PICCMidlinePlacement":
                    case "PRNFoleyChange":
                    case "PRNSNV":
                    case "PRNVPforCMP":
                    case "PTWithINR":
                    case "PTWithINRPRNSNV":
                    case "SkilledNurseHomeInfusionSD":
                    case "SkilledNurseHomeInfusionSDAdditional":
                    case "SNDC":
                    case "SNEvaluation":
                    case "SNFoleyLabs":
                    case "SNFoleyChange":
                    case "SNInjection":
                    case "SNInjectionLabs":
                    case "SNLabsSN":
                    case "SNVPsychNurse":
                    case "SNVwithAideSupervision":
                    case "SNVDCPlanning":
                    case "LVNSupervisoryVisit":
                    case "DieticianVisit":
                    case "DischargeSummary":
                    case "SixtyDaySummary":
                    case "TransferSummary":
                    case "SNVTeachingTraining":
                    case "SNVManagementAndEvaluation":
                    case "SNVObservationAndAssessment":
                    case "PTEvaluation":
                    case "PTVisit":
                    case "PTDischarge":
                    case "PTReEvaluation":
                    case "PTReassessment":
                    case "PTAVisit":
                    case "PTMaintenance":
                    case "OTEvaluation":
                    case "OTReEvaluation":
                    case "OTReassessment":
                    case "OTDischarge":
                    case "OTVisit":
                    case "OTMaintenance":
                    case "STVisit":
                    case "STEvaluation":
                    case "STReEvaluation":
                    case "STReassessment":
                    case "STDischarge":
                    case "STMaintenance":
                    case "MSWEvaluationAssessment":
                    case "MSWVisit":
                    case "MSWDischarge":
                    case "MSWAssessment":
                    case "MSWProgressNote":
                    case "HHAideSupervisoryVisit":
                    case "HHAideVisit":
                    case "HomeMakerNote":
                    case "HHAideCarePlan":
                    case "COTAVisit":
                    case "PASVisit":
                    case "PASTravel":
                    case "PASCarePlan":
                    case "SNDiabeticDailyVisit":
                    case "PTDischargeSummary":
                    case "OTDischargeSummary":
                    case "UAPWoundCareVisit":
                    case "UAPInsulinPrepAdminVisit":
                    case "CoordinationOfCare":
                    case "SNPediatricVisit":
                    case "SNPediatricAssessment":
                    case "PTSupervisoryVisit":
                    case "OTSupervisoryVisit":
                    case "DriverOrTransportationNote":
                    case "SNPsychAssessment":
                        return "Notes";
                    case "PhysicianOrder":
                        return "PhysicianOrder";
                    case "HCFA485":
                    case "NonOasisHCFA485":
                    case "HCFA485StandAlone" :
                        return "PlanOfCare";
                    case "FaceToFaceEncounter":
                        return "FaceToFaceEncounter";
                    case "IncidentAccidentReport":
                        return "IncidentAccident";
                    case "InfectionReport":
                        return "Infection";
                    case "CommunicationNote":
                        return "CommunicationNote";
                }
            }
            return string.Empty;
        }
    }
}
