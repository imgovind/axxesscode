﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;


    public static class ClaimExtensions
    {
        public static bool IsRapDischage(this Rap rap)
        {
            var result = false;
            if (rap != null && rap.UB4PatientStatus.IsNotNullOrEmpty() && rap.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), rap.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)rap.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsFinalDischage(this Final final)
        {
            var result = false;
            if (final != null && final.UB4PatientStatus.IsNotNullOrEmpty() && final.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), final.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)final.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsManagedClaimDischarge(this ManagedClaim managedClaim)
        {
            var result = false;
            if (managedClaim != null && managedClaim.UB4PatientStatus.IsNotNullOrEmpty() && managedClaim.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), managedClaim.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)managedClaim.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsSecondaryClaimDischarge(this SecondaryClaim secondaryClaim)
        {
            var result = false;
            if (secondaryClaim != null && secondaryClaim.UB4PatientStatus.IsNotNullOrEmpty() && secondaryClaim.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), secondaryClaim.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)secondaryClaim.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static IDictionary<string, Locator> ToUb04Locator81Dictionary(this string ub04Locator81cca)
        {
            var questions = new Dictionary<string, Locator>();
            if (ub04Locator81cca.IsNotNullOrEmpty())
            {
                var locatorQuestions = ub04Locator81cca.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        questions.Add(n.LocatorId, n);
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToLocatorDictionary(this string ub04Locator)
        {
            var questions = new Dictionary<string, Locator>();
            if (ub04Locator.IsNotNullOrEmpty())
            {
                var locatorQuestions = ub04Locator.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        questions.Add(n.LocatorId, n);
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this Final final)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (final.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = final.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (final.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = final.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (final.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = final.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (final.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = final.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    questions.Add(n.LocatorId, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this ManagedClaim managedClaim)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (managedClaim.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = managedClaim.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (managedClaim.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = managedClaim.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (managedClaim.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = managedClaim.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (managedClaim.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = managedClaim.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    questions.Add(n.LocatorId, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this SecondaryClaim secondaryClaim)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (secondaryClaim.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = secondaryClaim.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (secondaryClaim.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = secondaryClaim.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (secondaryClaim.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = secondaryClaim.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (secondaryClaim.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = secondaryClaim.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    questions.Add(n.LocatorId, n);
                });
            }
            return questions;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  AgencyInsurance agencyInsurance)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (agencyInsurance!=null && agencyInsurance.BillData.IsNotNullOrEmpty())
            {
                var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                if (rates != null && rates.Count > 0)
                {
                    rates.ForEach(n =>
                    {
                        if (!chargeRates.ContainsKey(n.Id))
                        {
                            chargeRates.Add(n.Id, n);
                        }
                    });
                }
            }
            return chargeRates;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  Final final)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (final != null && final.Insurance.IsNotNullOrEmpty())
            {
                var insurance = final.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null )
                {
                    chargeRates = insurance.ToInsurancBillDataDictionary();
                }
            }
            return chargeRates;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  ManagedClaim managedClaim)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (managedClaim != null && managedClaim.Insurance.IsNotNullOrEmpty())
            {
                var insurance = managedClaim.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null)
                {
                    chargeRates = insurance.ToInsurancBillDataDictionary();
                }
            }
            return chargeRates;
        }
    }
}
