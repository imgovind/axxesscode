﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class PhysicianExtensions
    {
        public static List<PhysicianSelection> ForSelection(this IList<AgencyPhysician> patients)
        {
            var result = new List<PhysicianSelection>();

            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    result.Add(new PhysicianSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName, DisplayName = p.DisplayName });
                });
            }

            return result.OrderBy(u => u.DisplayName).ToList();
        }
    }
}
