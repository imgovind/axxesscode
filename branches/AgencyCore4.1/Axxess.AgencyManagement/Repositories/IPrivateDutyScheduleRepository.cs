﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using Axxess.Api.Contracts;
    using Axxess.AgencyManagement.Domain;

    public interface IPrivateDutyScheduleRepository
    {
        List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDates(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate);
        List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid agencyId, Guid userId, Guid patientId, DateTime startDate, DateTime endDate);

        bool AddTask(PrivateDutyScheduleTask task);
        bool UpdateTask(PrivateDutyScheduleTask editedTask);
        bool DeleteTask(Guid agencyId, Guid patientId, Guid id);
        bool DeleteTask(PrivateDutyScheduleTask task);
        bool RestoreTask(Guid agencyId, Guid patientId, Guid id);
        bool RestoreTask(PrivateDutyScheduleTask task);

        PrivateDutyScheduleTask GetTask(Guid agencyId, Guid patientId, Guid id);

        bool AddVisitNote(PrivateDutyPatientVisitNote note);
        bool UpdateVisitNoteStatus(Guid agencyId, Guid patientId, Guid id, bool isDeprecated);
        bool UpdateVisitNoteStatus(PrivateDutyPatientVisitNote visitNote, bool isDeprecated);
        PrivateDutyPatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid id);
    }
}
