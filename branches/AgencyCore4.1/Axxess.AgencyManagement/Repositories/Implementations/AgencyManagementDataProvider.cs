﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class AgencyManagementDataProvider : IAgencyManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public AgencyManagementDataProvider()
        {
            this.database = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region AgencyCoreDataProvider Members

        private IReferralRepository referralRepository;
        public IReferralRepository ReferralRepository
        {
            get
            {
                if (referralRepository == null)
                {
                    referralRepository = new ReferralRepository(this.database);
                }
                return referralRepository;
            }
        }

        private IPatientRepository patientRepository;
        public IPatientRepository PatientRepository
        {
            get
            {
                if (patientRepository == null)
                {
                    patientRepository = new PatientRepository(this.database);
                }
                return patientRepository;
            }
        }

        private IPhysicianRepository physicianRepository;
        public IPhysicianRepository PhysicianRepository
        {
            get
            {
                if (physicianRepository == null)
                {
                    physicianRepository = new PhysicianRepository(this.database);
                }
                return physicianRepository;
            }
        }

        private IUserRepository employeeRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (employeeRepository == null)
                {
                    employeeRepository = new UserRepository(this.database);
                }
                return employeeRepository;
            }
        }

        private IAgencyRepository agencyRepository;
        public IAgencyRepository AgencyRepository
        {
            get
            {
                if (agencyRepository == null)
                {
                    agencyRepository = new AgencyRepository(this.database);
                }
                return agencyRepository;
            }
        }

        private IBillingRepository billingRepository;
        public IBillingRepository BillingRepository
        {
            get
            {
                if (billingRepository == null)
                {
                    billingRepository = new BillingRepository(this.database);
                }
                return billingRepository;
            }
        }

        private IMessageRepository messageRepository;
        public IMessageRepository MessageRepository
        {
            get
            {
                if (messageRepository == null)
                {
                    messageRepository = new MessageRepository(this.database);
                }
                return messageRepository;
            }
        }

        private IAssetRepository assetRepository;
        public IAssetRepository AssetRepository
        {
            get
            {
                if (assetRepository == null)
                {
                    assetRepository = new AssetRepository(this.database);
                }
                return assetRepository;
            }
        }

        private IPrivateDutyScheduleRepository scheduleRepository;
        public IPrivateDutyScheduleRepository ScheduleRepository
        {
            get
            {
                if (scheduleRepository == null)
                {
                    scheduleRepository = new PrivateDutyScheduleRepository(this.database);
                }
                return scheduleRepository;
            }
        }


        #endregion
    }

}

