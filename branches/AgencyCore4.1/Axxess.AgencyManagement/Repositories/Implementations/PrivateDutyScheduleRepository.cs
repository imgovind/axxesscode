﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
using Axxess.Api.Contracts;
    using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Domain;

    public class PrivateDutyScheduleRepository : IPrivateDutyScheduleRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PrivateDutyScheduleRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        public List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDates(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            string script = @"SELECT * 
                                FROM privatedutyscheduletasks
                                    WHERE 
                                        AgencyId = @agencyId AND
                                        PatientId = @patientId AND
                                        EventStartTime BETWEEN DATE(@startDate) AND DATE(@endDate)";

            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyId", agencyId)
                     .AddGuid("patientId", patientId)
                     .AddDateTime("startDate", startDate)
                     .AddDateTime("endDate", endDate)
                     .AsList();
            }

            return list;
        }

        public List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            string script = @"SELECT * 
                                FROM privatedutyscheduletasks
                                    WHERE 
                                        AgencyId = @agencyId AND
                                        PatientId = @patientId AND
                                        UserId = @userId AND
                                        EventStartTime BETWEEN DATE(@startDate) AND DATE(@endDate)";

            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyId", agencyId)
                     .AddGuid("patientId", patientId)
                     .AddGuid("userId", userId)
                     .AddDateTime("startDate", startDate)
                     .AddDateTime("endDate", endDate)
                     .AsList();
            }

            return list;
        }

        public List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate)
        {
            string script = @"SELECT * 
                                FROM privatedutyscheduletasks
                                    WHERE 
                                        AgencyId = @agencyId AND
                                        UserId = @userId AND
                                        EventStartTime BETWEEN DATE(@startDate) AND DATE(@endDate)";

            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyId", agencyId)
                     .AddGuid("userId", userId)
                     .AddDateTime("startDate", startDate)
                     .AddDateTime("endDate", endDate)
                     .AsList();
            }

            return list;
        }


        public bool AddTask(PrivateDutyScheduleTask task)
        {
            bool result = false;
            try
            {
                if (task != null)
                {
                    database.Add<PrivateDutyScheduleTask>(task);
                    result = true;
                }
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public PrivateDutyScheduleTask GetTask(Guid agencyId, Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            try
            {
                return database.Single<PrivateDutyScheduleTask>(v => v.AgencyId == agencyId && v.PatientId == patientId && v.Id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public bool UpdateTask(PrivateDutyScheduleTask editedTask)
        {
            bool result = false;
            if (editedTask != null)
            {
                try
                {
                    int i = database.Update<PrivateDutyScheduleTask>(editedTask);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteTask(Guid agencyId, Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            bool result = false;
            try
            {
                var task = this.GetTask(agencyId, patientId, id);
                task.IsDeprecated = true;
                int i = database.Update<PrivateDutyScheduleTask>(task);
                result = true;
            }
            catch (Exception)
            {
            }
            return result;
        }

        public bool DeleteTask(PrivateDutyScheduleTask task)
        {
            bool result = false;
            if (task != null)
            {
                try
                {
                    task.IsDeprecated = true;
                    int i = database.Update<PrivateDutyScheduleTask>(task);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool RestoreTask(Guid agencyId, Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            bool result = false;
            try
            {
                var task = this.GetTask(agencyId, patientId, id);
                task.IsDeprecated = false;
                int i = database.Update<PrivateDutyScheduleTask>(task);
                result = true;
            }
            catch (Exception)
            {
            }
            return result;
        }

        public bool RestoreTask(PrivateDutyScheduleTask task)
        {
            bool result = false;
            if (task != null)
            {
                try
                {
                    task.IsDeprecated = false;
                    int i = database.Update<PrivateDutyScheduleTask>(task);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }


        public bool AddVisitNote(PrivateDutyPatientVisitNote note)
        {
            bool result = false;
            try
            {
                if (note != null)
                {
                    database.Add<PrivateDutyPatientVisitNote>(note);
                    result = true;
                }
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public PrivateDutyPatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            try
            {
                return database.Single<PrivateDutyPatientVisitNote>(v => v.AgencyId == agencyId && v.PatientId == patientId && v.Id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateVisitNoteStatus(Guid agencyId, Guid patientId, Guid id, bool isDeprecated)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            bool result = false;
            try
            {
                var visitNote = this.GetVisitNote(agencyId, patientId, id);
                visitNote.IsDeprecated = isDeprecated;
                int i = database.Update<PrivateDutyPatientVisitNote>(visitNote);
                result = true;
            }
            catch (Exception)
            {
            }
            return result;
        }

        #endregion

        #region IPrivateDutyScheduleRepository Members


        

        public bool UpdateVisitNoteStatus(PrivateDutyPatientVisitNote visitNote, bool isDeprecated)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
