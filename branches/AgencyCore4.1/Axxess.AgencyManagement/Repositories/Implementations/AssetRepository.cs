﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public Asset Get(Guid id, Guid agencyId)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId && a.IsDeprecated == false);
        }

        public bool Delete(Guid id)
        {
            var asset = database.Single<Asset>(a => a.Id == id);
            if (asset != null)
            {
                asset.IsDeprecated = true;
                asset.Modified = DateTime.Now;
                database.Update<Asset>(asset);
                return true;
            }
            return false;
        }

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }

        public List<PatientDocument> GetPatientDocuments(Guid patientId)
        {
            List<PatientDocument> list = new List<PatientDocument>();
            list = database.Find<PatientDocument>(p => p.PatientId == patientId && p.IsDeprecated == false).ToList();
            return list;
        }

        public bool DeletePatientDocument(Guid id, Guid patientId)
        {
            var result = false;
            var patientDocument= database.Single<PatientDocument>(pa => pa.Id == id && pa.PatientId == patientId);
            if (patientDocument != null)
            {
                patientDocument.IsDeprecated = true;
                patientDocument.Modified = DateTime.Now;
                database.Update<PatientDocument>(patientDocument);
                if (Delete(patientDocument.AssetId))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool AddPatientDocument(Asset asset, PatientDocument patientDocument)
        {
            bool result = false;
            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                patientDocument.AssetId = asset.Id;
                patientDocument.Id = Guid.NewGuid();
                patientDocument.Created = DateTime.Now;
                patientDocument.Modified = DateTime.Now;
                database.Add<PatientDocument>(patientDocument);
                result = true;
            }
            return result;
        }
        public bool UpdatePatientDocument(PatientDocument patientDocument)
        {
            var result = false;
            try
            {
                if (patientDocument != null)
                {
                    database.Update<PatientDocument>(patientDocument);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId)
        {
            return database.Single<PatientDocument>(a => a.Id == id && a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);
        }
        #endregion
    }
}
