﻿using System;
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Enums;
    using Extensions;

    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
using Axxess.Core;
    using Axxess.Core.Infrastructure;

    
    [XmlRoot()]
    [DataContract]
    public class PrivateDutyScheduleTask : EntityBase {
        //public DateTime StartTime;
        //public DateTime EndTime;

        public PrivateDutyScheduleTask()
        {
            this.UserId = Guid.Empty;
            this.Id = Guid.Empty;
            this.Assets = new List<Guid>();
        }

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public int DisciplineTask { get; set; }

        public DateTime EventStartTime { get; set; }
        public DateTime EventEndTime { get; set; }
        public DateTime VisitStartTime { get; set; }
        public DateTime VisitEndTime { get; set; }

        public string Status { get; set; }
        public string Discipline { get; set; }
        
        public string Surcharge { get; set; }
        public string AssociatedMileage { get; set; }
        public string Comments { get; set; }

        public int Version { get; set; }
        public string SendAsOrder { get; set; }
        public bool InPrintQueue { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMissedVisit { get; set; }
        public bool IsOrderForNextEpisode { get; set; }
        public bool IsVisitPaid { get; set; }
        public bool IsAllDay { get; set; }
        public bool IsDeprecated { get; set; }
        public List<Guid> Assets { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string UserName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status.IsNotNullOrEmpty())
                {
                    int check = -1;
                    ScheduleStatus status = ScheduleStatus.NoStatus;
                    if (int.TryParse(this.Status, out check)) { status = Enum.IsDefined(typeof(ScheduleStatus), check) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status) : ScheduleStatus.NoStatus; }
                    if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.EventStartTime.Date <= DateTime.Now.Date)
                    {
                        return ScheduleStatus.CommonNotStarted.GetDescription();
                    }
                    else
                    {
                        return status.GetDescription();
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), this.DisciplineTask)).GetDescription(); } else { return string.Empty; };
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Url { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string ActionUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid PhysicianId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string AttachmentUrl
        {
            get;
            set;
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string MissedVisitComments { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string EpisodeNotes { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusComment
        {
            get
            {
                int check = -1;
                ScheduleStatus status = ScheduleStatus.NoStatus;
                if (int.TryParse(this.Status, out check)) { status = Enum.IsDefined(typeof(ScheduleStatus), check) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status) : ScheduleStatus.NoStatus; }
                var result = string.Empty;
                if (this.IsMissedVisit && this.MissedVisitComments.IsNotNullOrEmpty()) result += this.MissedVisitComments.Clean() + "\r\n";
                //if (this.ReturnReason.IsNotNullOrEmpty() && !this.IsCompletelyFinished() && status != ScheduleStatus.ReportAndNotesCompleted) result += this.ReturnReason;
                return result;
            }
        }

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public bool IsPastDue
        //{
        //    get
        //    {
        //        return (this.EventDate.IsNotNullOrEmpty() && this.EventDate.IsValidDate() && EventDate.ToDateTime().Date < DateTime.Now.Date && (this.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString()));
        //    }
        //}

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public bool IsComplete
        //{
        //    get
        //    {
        //        return this.IsCompletelyFinished() || this.IsCompleted();
        //    }
        //}

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public bool IsOrphaned
        //{
        //    get
        //    {
        //        var result = false;
        //        if (this.EventDate.IsValidDate())
        //        {
        //            var eventDate = this.EventDate.ToDateTime();
        //            if ((eventDate.Date < this.StartDate.Date || eventDate.Date > this.EndDate.Date))
        //            {
        //                if (this.DisciplineTask != 102)
        //                {
        //                    result = true;
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //}

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string EventDateSortable
        //{
        //    get
        //    {
        //        return EventDate.IsNotNullOrEmpty() ? "<div class='float-left'><span class='float-right'>" + EventDate.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + EventDate.Split('/')[0] + "/" + EventDate.Split('/')[1] + "</span></div>" : "";
        //    }
        //}

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public int Unit
        //{
        //    get
        //    {
        //        var timeIn = DateTime.Now;
        //        var timeOut = DateTime.Now;
        //        if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn) && timeOut >= timeIn)
        //        {
        //            var min = (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
        //            if (min > 0)
        //            {
        //                return (int)Math.Ceiling((double)min / 15);
        //            }
        //            return 0;
        //        }
        //        return 0;
        //    }
        //}

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public int MinSpent
        //{
        //    get
        //    {
        //        var timeIn = DateTime.Now;
        //        var timeOut = DateTime.Now;
        //        if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn))
        //        {
        //            if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
        //            {
        //                var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
        //                var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
        //                return outi + outO;
        //            }
        //            else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
        //            {
        //                var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
        //                var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
        //                return outi + outO + 12 * 60;

        //            }
        //            else
        //            {
        //                if (timeOut >= timeIn)
        //                {
        //                    return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
        //                }
        //            }
        //        }
        //        return 0;
        //    }
        //}

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid NewEpisodeId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsEpisodeReassiged { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string OasisProfileUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusCommentCleaned
        {
            get
            {
                return this.StatusComment.IsNotNullOrEmpty() ? this.StatusComment.Clean() : string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string CommentsCleaned
        {
            get
            {
                return this.Comments.IsNotNullOrEmpty() ? this.Comments.Clean() : string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string EpisodeNotesCleaned
        {
            get
            {
                return this.EpisodeNotes.IsNotNullOrEmpty() ? this.EpisodeNotes.Clean() : string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsUserDeleted { get; set; }


        protected override void AddValidationRules()
        {
            Rules.Add(new Validation(() => this.EventStartTime > this.EventEndTime, "The end date must be after the start date."));
        }
    }
}
