﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class RapBill : Claim
    {
        public bool IsFirstBillableVisit { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsVerified { get; set; }
    }
}
