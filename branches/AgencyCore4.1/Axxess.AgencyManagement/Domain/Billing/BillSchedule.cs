﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;

    public class BillSchedule
    {
        public Guid EventId { get; set; }
        public string DisciplineTaskName { get; set; }
        public string PereferredName { get; set; }
        public string EventDate { get; set; }
        public string VisitDate { get; set; }
        public string RevenueCode { get; set; }
        public string HCPCSCode { get; set; }
        public string Modifier { get; set; }
        public string Modifier2 { get; set; }
        public string Modifier3 { get; set; }
        public string Modifier4 { get; set; }
        public string StatusName { get; set; }
        public int Unit { get; set; }
        public double Charge { get; set; }
        public bool IsExtraTime  { get; set; }
        public List<BillSchedule> UnderlyingVisits { get; set; }
        public List<ServiceAdjustment> Adjustments { get; set; }
        public string ViewId 
        { 
            get 
            {
                if (!EventId.IsEmpty())
                {
                    return "Visit" + this.EventId.ToString();
                }
                else
                {
                    return "Visit" + (this.VisitDate.IsNotNullOrEmpty() ? this.VisitDate.Replace("\\", "") : (this.EventDate.IsNotNullOrEmpty() ? this.EventDate.Replace("\\", "") : string.Empty)); 
                }
                
            } 
        }
    }
}
