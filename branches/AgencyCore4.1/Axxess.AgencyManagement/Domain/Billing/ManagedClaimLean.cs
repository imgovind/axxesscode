﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
   
    using System.ComponentModel.DataAnnotations;

    public class ManagedClaimLean
    {

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public bool IsVerified { get; set; }
        public DateTime Modified { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string VerifiedVisit { get; set; }
        public int PrimaryInsuranceId { get; set; }

        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }

        public int Status { get; set; }
        public double Balance
        {
            get
            {
                return this.ClaimAmount - this.PaymentAmount - AdjustmentAmount;
            }
        }

        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public string ClaimDate { get; set; }
        public string ClaimDateFormatted
        {
            get
            {
                return ClaimDate.IsNotNullOrEmpty() && ClaimDate.IsDate() && ClaimDate.ToDateTime() != DateTime.MinValue ? ClaimDate : string.Empty;
            }
        }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public double AdjustmentAmount { get; set; }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(ManagedClaimStatus), this.Status) ? ((ManagedClaimStatus) this.Status).GetDescription() : string.Empty;

            }
        }

        public string ClaimRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }


        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public bool IsHMO { get; set; }
        public int InvoiceType { get; set; }

        public string PrintUrl 
        {
            get
            {
                var url = string.Empty;
                if (this.IsVisitVerified && this.IsInfoVerified && this.IsSupplyVerified)
                {
                    if (this.InvoiceType == (int)Enums.InvoiceType.UB)
                    {
                        url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/ManagedUB04Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">UB-04</a>", this.PatientId, this.Id);
                    }
                    else if (this.InvoiceType == (int)Enums.InvoiceType.HCFA)
                    {
                        url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/ManagedHCFA1500Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">HCFA-1500</a> ", this.PatientId, this.Id);
                    }
                    else if (this.InvoiceType == (int)Enums.InvoiceType.Invoice)
                    {
                        url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/InvoicePdf', {{ 'patientId': '{1}', 'Id': '{0}', 'isForPatient': '{2}' }});\" >Invoice</a>", this.Id, this.PatientId, false);
                    }
                    
                    if (url.IsNotNullOrEmpty())
                    {
                        url += string.Format(" | <a href=\"javascript:void(0);\" onclick=\"ManagedBilling.LoadGeneratedSingleManagedClaim('{0}');\">Download</a>", this.Id);
                    }
                    else
                    {
                        url = string.Format("<a href=\"javascript:void(0);\" onclick=\"ManagedBilling.LoadGeneratedSingleManagedClaim('{0}');\">Download</a>", this.Id);
                    }
                }
                return url;
            }
        }
    }
}
