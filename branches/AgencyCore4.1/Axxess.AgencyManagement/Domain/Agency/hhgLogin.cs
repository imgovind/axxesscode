﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class HhgLogin
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }

        [SubSonicIgnore]
        public string CreatedFormatted
        {
            get
            {
                return Created.ToShortDateString().ToZeroFilled();
            }
        }
    }
}
