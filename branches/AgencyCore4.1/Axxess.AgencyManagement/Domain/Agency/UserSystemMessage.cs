﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserMessage
    {
        public Guid Id { get; set; }
        public Guid MessageId { get; set; }
        public int MessageType { get; set; }
        public Guid UserId { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeprecated { get; set; }
        public Guid FolderId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ThreadId { get; set; }
    }
}
