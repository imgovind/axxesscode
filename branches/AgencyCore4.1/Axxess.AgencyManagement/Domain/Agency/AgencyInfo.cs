﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;
    public class AgencyInfo
    {
        public string Name { get; set; }
    }
}
