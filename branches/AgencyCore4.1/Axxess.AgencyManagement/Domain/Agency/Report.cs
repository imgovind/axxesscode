﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class Report : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid AssetId { get; set; }
        public string Type { get; set; }
        public string Format { get; set; }
        public string Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Completed { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Type), "Report Type is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Status), "Report Status is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Format), "Report Format is required. <br />"));
        }

        #endregion
    }
}
