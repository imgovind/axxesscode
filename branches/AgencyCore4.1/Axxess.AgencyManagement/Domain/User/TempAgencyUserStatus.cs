﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    public class TempAgencyUserStatus
    {
        public Guid AgencyId { get; set; }
        [SubSonicPrimaryKey]
        public Guid UserId { get; set; }
        public int Status { get; set; }
    }
}
