﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserVisitWidget
    {
        public Guid PatientId { get; set; }
        public string TaskName { get; set; }
        public string EventDate { get; set; }
        public bool IsDischarged { get; set; }
        public string PatientName { get; set; }
    }
}
