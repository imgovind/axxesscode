﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class RecertEvent
    {
        public Guid Id { get; set; }
        public string Task { get; set; }
        public string Status { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string AssignedTo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime TargetDate { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime ScheduledDate { get; set; }
        public string Schedule { get; set; }
        public string TargetDateFormatted { get {return this.TargetDate.ToString("MM/dd/yyyy") ;}}
        public string ScheduledDateFormatted { get { return ScheduledDate != DateTime.MinValue ? this.ScheduledDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public int DateDifference { get; set; }
    }
}
