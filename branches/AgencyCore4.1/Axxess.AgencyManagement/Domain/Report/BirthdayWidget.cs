﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    using SubSonic.SqlGeneration.Schema;

    public class BirthdayWidget
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string BirthDay
        {
            get
            {
                return string.Format("{0:MMMM d}", this.Date);
            }
        }
        public string Age
        {
            get
            {
                int age = DateTime.Now.Year - this.Date.Year;
                if (DateTime.Now.Month < this.Date.Month || (DateTime.Now.Month == this.Date.Month && DateTime.Now.Day < this.Date.Day))
                    age--;
                return (age).ToString();
            }
        }
        public string PhoneHome { get; set; }
        public bool IsDischarged { get; set; }
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
    }
}
