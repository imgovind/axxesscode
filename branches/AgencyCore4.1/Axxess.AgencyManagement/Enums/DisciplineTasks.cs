﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum DisciplineTasks
    {
        [CustomDescription("No Discipline", "No Discipline", "None", "None", "None", "None")]
        NoDiscipline = 0,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN", "Skilled Nurse Visit")]
        SkilledNurseVisit = 1,
        [CustomDescription("OASIS-B Follow-up", "Follow-up", "OASIS", "Assessment", "OASISBFollowUp", "")]
        OASISBFollowUp = 2,
        [CustomDescription("OASIS-B Recertification", "Recert.", "OASIS", "Assessment", "OASISBRecertification", "")]
        OASISBRecertification = 3,
        [CustomDescription("OASIS-B Resumption of Care", "ROC", "OASIS", "Assessment", "OASISBResumptionofCare", "")]
        OASISBResumptionofCare = 4,
        [CustomDescription("OASIS-B Discharge", "Discharge", "OASIS", "Assessment", "OASISBDischarge", "")]
        OASISBDischarge = 10,
        [CustomDescription("OASIS-B Start of Care", "SOC", "OASIS", "Assessment", "OASISBStartofCare", "")]
        OASISBStartofCare = 11,
        [CustomDescription("OASIS-B Transfer", "Transfer", "OASIS", "Assessment", "OASISBTransfer", "")]
        OASISBTransfer = 12,
        [CustomDescription("OASIS B Death at Home", "Death", "OASIS", "Assessment", "OASISBDeathatHome", "")]
        OASISBDeathatHome = 15,
        [CustomDescription("OASIS-C Death", "Death", "OASIS", "Assessment", "OASISCDeath", "OASIS-C Death")]
        OASISCDeath = 5,
        [CustomDescription("OASIS-C Discharge", "Discharge", "OASIS", "Assessment", "OASISCDischarge", "OASIS-C Discharge")]
        OASISCDischarge = 6,
        [CustomDescription("OASIS-C Follow-up", "Follow-up", "OASIS", "Assessment", "OASISCFollowUp", "OASIS-C Follow-up")]
        OASISCFollowUp = 7,
        [CustomDescription("OASIS-C Recertification", "Recert.", "OASIS", "Assessment", "OASISCRecertification", "OASIS-C Recertification")]
        OASISCRecertification = 8,
        [CustomDescription("OASIS-C Resumption of Care", "ROC", "OASIS", "Assessment", "OASISCResumptionofCare", "OASIS-C Resumption of Care")]
        OASISCResumptionofCare = 9,
        [CustomDescription("OASIS-C Start of Care", "SOC", "OASIS", "Assessment", "OASISCStartofCare", "OASIS-C Start of Care")]
        OASISCStartofCare = 13,
        [CustomDescription("OASIS-C Transfer", "Transfer", "OASIS", "Assessment", "OASISCTransfer", "OASIS-C Transfer")]
        OASISCTransfer = 14,
        [CustomDescription("OASIS-C Transfer Discharge", "Transfer", "OASIS", "Assessment", "OASISCTransferDischarge", "OASIS-C Transfer Discharge")]
        OASISCTransferDischarge = 88,
        [CustomDescription("SN Insulin AM Visit", "SNV", "SN", "Notes", "SN", "SN Insulin - AM")]
        SNInsulinAM = 16,
        [CustomDescription("SN Insulin PM Visit", "SNV", "SN", "Notes", "SN", "SN Insulin - PM")]
        SNInsulinPM = 17,
        [CustomDescription("SN Discharge Summary", "DS", "SN", "Notes", "DS", "SN Discharge Summary")]
        DischargeSummary = 18,
        [CustomDescription("SN Foley Cath Change", "SNV", "SN", "Notes", "SN", "Foley Cath Change")]
        FoleyCathChange = 19,
        [CustomDescription("LVN/LPN Supervisory Visit", "LVN Sup Visit", "SN", "Notes", "LVNSupervisoryVisit", "LVN Supervisory Visit")]
        LVNSupervisoryVisit = 20,
        [CustomDescription("SN B12 Injection Visit", "SNV", "SN", "Notes", "SN", "SN B12 INJ")]
        SNB12INJ = 21,
        [CustomDescription("SN BMP Visit", "SNV", "SN", "Notes", "SN", "SN BMP")]
        SNBMP = 22,
        [CustomDescription("SN CBC Visit", "SNV", "SN", "Notes", "SN", "SN CBC")]
        SNCBC = 23,
        [CustomDescription("SN Haldol Injection Visit", "SNV", "SN", "Notes", "SN", "SN Haldol Injection")]
        SNHaldolInj = 24,
        [CustomDescription("SN PICC/Midline Placement Visit", "SNV", "SN", "Notes", "SN", "PICC/Midline Placement")]
        PICCMidlinePlacement = 25,
        [CustomDescription("SN PRN Foley Change Visit", "SNV", "SN", "Notes", "SN", "PRN Foley Change")]
        PRNFoleyChange = 26,
        [CustomDescription("SN PRN Visit", "SNV", "SN", "Notes", "SN", "PRN SNV")]
        PRNSNV = 27,
        [CustomDescription("SN PRN VP for CMP Visit", "SNV", "SN", "Notes", "SN", "PRN VP for CMP")]
        PRNVPforCMP = 28,
        [CustomDescription("PT w/ INR", "PT", "PT", "Notes", "PTWithINR", "PT w/ INR")]
        PTWithINR = 29,
        [CustomDescription("PT w/ INR PRN", "SNV", "SN", "Notes", "SN", "PT w/ INR PRN SNV")]
        PTWithINRPRNSNV = 30,
        [CustomDescription("SN Home Infusion SD Visit", "SNV", "SN", "Notes", "SN")]
        SkilledNurseHomeInfusionSD = 31,
        [CustomDescription("SN Home Infusion SD Additional Visit", "SNV", "SN", "Notes", "SN")]
        SkilledNurseHomeInfusionSDAdditional = 32,
        [CustomDescription("SN Assessment (Start of Care)", "SNV", "SN", "Notes", "SNAssessment")]
        SNAssessment = 33,
        [CustomDescription("SN DC Visit", "SNV", "SN", "Notes", "SN")]
        SNDC = 34,
        [CustomDescription("SN Evaluation Visit", "SNV", "SN", "Notes", "SN")]
        SNEvaluation = 35,
        [CustomDescription("SN Foley Labs Visit", "SNV", "SN", "Notes", "SN")]
        SNFoleyLabs = 36,
        [CustomDescription("SN Foley Change Visit", "SNV", "SN", "Notes", "SN")]
        SNFoleyChange = 37,
        [CustomDescription("SN Injection Visit", "SNV", "SN", "Notes", "SN")]
        SNInjection = 38,
        [CustomDescription("SN Injection/Labs Visit", "SNV", "SN", "Notes", "SN")]
        SNInjectionLabs = 39,
        [CustomDescription("SN Labs Visit", "SNV", "SN", "Notes", "SN")]
        SNLabsSN = 40,
        [CustomDescription("SN Psychiatric Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNVPsychNurse = 41,
        [CustomDescription("SN with Aide Supervision Visit", "SNV", "SN", "Notes", "SN")]
        SNVwithAideSupervision = 42,
        [CustomDescription("SN DC Planning Visit", "SNV", "SN", "Notes", "SN")]
        SNVDCPlanning = 43,
        [CustomDescription("PT Evaluation", "PT", "PT", "Notes", "PTNotes", "PT Evaluation")]
        PTEvaluation = 44,
        [CustomDescription("PT Visit", "PT", "PT", "Notes", "PTVisit", "PT Visit")]
        PTVisit = 45,
        [CustomDescription("PT Discharge", "PT", "PT", "Notes", "PTDischarge", "PT Discharge")]
        PTDischarge = 46,
        [CustomDescription("OT Evaluation", "OT", "OT", "Notes", "OTNotes", "OT Evaluation")]
        OTEvaluation = 47,
        [CustomDescription("OT Re-Evaluation", "OT", "OT", "Notes", "OTNotes", "OT Re-Evaluation")]
        OTReEvaluation = 48,
        [CustomDescription("OT Visit", "OT", "OT", "Notes", "OTVisit", "OT Visit")]
        OTVisit = 49,
        [CustomDescription("ST Visit", "ST", "ST", "Notes", "STVisit", "ST Visit")]
        STVisit = 50,
        [CustomDescription("ST Evaluation", "ST", "ST", "Notes", "STNotes", "ST Evaluation")]
        STEvaluation = 51,
        [CustomDescription("ST Discharge", "ST", "ST", "Notes", "STNotes", "ST Discharge")]
        STDischarge = 52,
        [CustomDescription("MSW Evaluation", "MSW", "MSW", "Notes", "MSWEvaluationAssessment", "MSW Evaluation")]
        MSWEvaluationAssessment = 53,
        [CustomDescription("HHA Visit", "HHA", "HHA", "Notes", "HHAideVisit", "HHA Visit")]
        HHAideVisit = 54,
        [CustomDescription("HHA Supervisory Visit", "HHA Sup Visit", "SN", "Notes", "HHAideSupervisoryVisit", "HHA Supervisory Visit")]
        HHAideSupervisoryVisit = 55,
        [CustomDescription("MSW Visit", "MSW", "MSW", "Notes", "MSWVisit", "MSW Visit")]
        MSWVisit = 56,
        [CustomDescription("MSW Discharge", "MSW", "MSW", "Notes", "MSWDischarge", "MSW Discharge")]
        MSWDischarge = 57,
        [CustomDescription("Dietician Visit", "DV", "DV", "Notes", "DieticianVisit", "Dietician Visit")]
        DieticianVisit = 58,
        [CustomDescription("PTA Visit", "PT", "PT", "Notes", "PTVisit", "PTA Visit")]
        PTAVisit = 59,
        [CustomDescription("PT Re-Evaluation", "PT", "PT", "Notes", "PTNotes", "PT Re-Evaluation")]
        PTReEvaluation = 60,
        [CustomDescription("OASIS-C Start of Care (PT)", "SOC", "OASIS", "Assessment", "OASISCStartofCarePT", "OASIS-C Start of Care (PT)")]
        OASISCStartofCarePT = 61,
        [CustomDescription("OASIS-C Resumption of Care (PT)", "ROC", "OASIS", "Assessment", "OASISCResumptionofCarePT", "OASIS-C Resumption of Care (PT)")]
        OASISCResumptionofCarePT = 62,
        [CustomDescription("OASIS-C Death (PT)", "Death", "OASIS", "Assessment", "OASISCDeathPT", "OASIS-C Death (PT)")]
        OASISCDeathPT = 63,
        [CustomDescription("OASIS-C Discharge (PT)", "Discharge", "OASIS", "Assessment", "OASISCDischargePT", "OASIS-C Discharge (PT)")]
        OASISCDischargePT = 64,
        [CustomDescription("OASIS-C Follow-up (PT)", "Follow-up", "OASIS", "Assessment", "OASISCFollowupPT", "OASIS-C Follow-up (PT)")]
        OASISCFollowupPT = 65,
        [CustomDescription("OASIS-C Recertification (PT)", "Recert.", "OASIS", "Assessment", "OASISCRecertificationPT", "OASIS-C Recertification (PT)")]
        OASISCRecertificationPT = 66,
        [CustomDescription("OASIS-C Transfer (PT)", "Transfer", "OASIS", "Assessment", "OASISCTransferPT", "OASIS-C Transfer (PT)")]
        OASISCTransferPT = 67,
        [CustomDescription("COTA Visit", "OT", "OT", "Notes", "OTVisit", "COTA Visit")]
        COTAVisit = 68,
        [CustomDescription("OASIS-C Start of Care (OT)", "SOC", "OASIS", "Assessment", "OASISCStartofCareOT", "OASIS-C Start of Care (OT)")]
        OASISCStartofCareOT = 112,
        [CustomDescription("OASIS-C Resumption of Care (OT)", "ROC", "OASIS", "Assessment", "OASISCResumptionofCareOT", "OASIS-C Resumption of Care (OT)")]
        OASISCResumptionofCareOT = 69,
        [CustomDescription("OASIS-C Recertification (OT)", "Recert.", "OASIS", "Assessment", "OASISCRecertificationOT", "OASIS-C Recertification (OT)")]
        OASISCRecertificationOT = 73,
        [CustomDescription("OASIS-C Death (OT)", "Death", "OASIS", "Assessment", "OASISCDeathOT", "OASIS-C Death (OT)")]
        OASISCDeathOT = 70,
        [CustomDescription("OASIS-C Discharge (OT)", "Discharge", "OASIS", "Assessment", "OASISCDischargeOT", "OASIS-C Discharge (OT)")]
        OASISCDischargeOT = 71,
        [CustomDescription("OASIS-C Follow-up (OT)", "Follow-up", "OASIS", "Assessment", "OASISCFollowupOT", "OASIS-C Follow-up (OT)")]
        OASISCFollowupOT = 72,
        [CustomDescription("OASIS-C Transfer (OT)", "Transfer", "OASIS", "Assessment", "OASISCTransferOT", "OASIS-C Transfer (OT)")]
        OASISCTransferOT = 74,
        [CustomDescription("HHA Care Plan", "HHA", "HHA", "Notes", "HHAideCarePlan", "HHAide Care Plan")]
        HHAideCarePlan = 75,
        [CustomDescription("MSW Assessment", "MSW", "MSW", "Notes", "MSWAssessment", "MSW Assessment")]
        MSWAssessment = 76,
        [CustomDescription("MSW Progress Note", "MSW", "MSW", "Notes", "MSWProgressNote", "MSW Progress Note")]
        MSWProgressNote = 77,
        [CustomDescription("485 Plan of Care (From Assessment)", "485", "485", "Orders", "HCFA485", "485 Plan of Care (From OASIS Assessment)")]
        HCFA485 = 78,
        [CustomDescription("486 Plan Of Care", "486", "486", "Orders", "HCFA486", "HCFA 486")]
        HCFA486 = 79,
        [CustomDescription("Physician Order", "Order", "Order", "Orders", "PhysicianOrder", "Physician Order")]
        PhysicianOrder = 80,
        [CustomDescription("Physician Order", "Order", "Order", "Orders", "PostHospitalizationOrder", "Post Hospitalization Order")]
        PostHospitalizationOrder = 81,
        [CustomDescription("Medicaid POC", "POC", "Order", "Orders", "MedicaidPOC", "Medicaid POC")]
        MedicaidPOC = 82,
        [CustomDescription("Rap", "Rap", "Claim", "Claims", "Rap", "RAP")]
        Rap = 83,
        [CustomDescription("Final", "Final", "Claim", "Claims", "Final", "Final")]
        Final = 84,
        [CustomDescription("60-Day Summary/Case Conference", "SDS", "SN", "Notes", "SixtyDaySummary", "60 Day Summary/Case Conference")]
        SixtyDaySummary = 85,
        [CustomDescription("Transfer Summary", "TS", "SN", "Notes", "TransferSummary", "Transfer Summary")]
        TransferSummary = 86,
        [CustomDescription("Communication Note", "Com Note", "Notes", "Notes", "CommunicationNote", "Communication Note")]
        CommunicationNote = 87,
        [CustomDescription("Non-OASIS Start of Care", "NonOasisSOC", "NonOASIS", "Assessment", "NonOASISStartofCare", "Non-OASIS Start of Care")]
        NonOASISStartofCare = 89,
        [CustomDescription("Non-OASIS Recertification", "NonOasisRecert", "NonOASIS", "Assessment", "NonOASISRecertification", "Non-OASIS Recertification")]
        NonOASISRecertification = 90,
        [CustomDescription("Non-OASIS Discharge", "NonOasisDischarge", "NonOASIS", "Assessment", "NonOASISDischarge", "Non-OASIS Discharge")]
        NonOASISDischarge = 91,
        [CustomDescription("Non-OASIS Plan of Care", "NonOasis485", "NonOasis485", "Orders", "NonOasisHCFA485", "Non-OASIS HCFA 485")]
        NonOasisHCFA485 = 92,
        [CustomDescription("Incident/Accident Log", "Incident/Accident", "ReportsAndNotes", "ReportsAndNotes", "IncidentAccidentReport", "Incident / Accident Log")]
        IncidentAccidentReport = 93,
        [CustomDescription("Infection Log", "InfectionReport", "ReportsAndNotes", "ReportsAndNotes", "InfectionReport", "Infection Log")]
        InfectionReport = 94,
        [CustomDescription("SN Teaching/Training Visit", "SNV", "SN", "Notes", "SN")]
        SNVTeachingTraining = 95,
        [CustomDescription("SN Observation & Assessment Visit", "SNV", "SN", "Notes", "SN")]
        SNVObservationAndAssessment = 96,
        [CustomDescription("SN Management & Evaluation Visit", "SNV", "SN", "Notes", "SN")]
        SNVManagementAndEvaluation = 97,
        [CustomDescription("Plan of Treatment/Care", "485", "485", "Orders", "HCFA485StandAlone", "Plan of Treatment/Care")]
        HCFA485StandAlone = 98,
        [CustomDescription("PAS Note", "HHA", "HHA", "Notes", "PASVisit", "PAS Note")]
        PASVisit = 99,
        [CustomDescription("PAS Care Plan", "HHA", "HHA", "Notes", "PASCarePlan", "PAS Care Plan")]
        PASCarePlan = 100,
        [CustomDescription("Skilled Nurse Assessment (Recertification)", "SNV", "SN", "Notes", "SNAssessmentRecert", "SN Assessment (Recertification)")]
        SNAssessmentRecert = 101,
        [CustomDescription("Physician Face-to-face Encounter", "Order", "Order", "Orders", "FaceToFaceEncounter", "FaceToFaceEncounter")]
        FaceToFaceEncounter = 102,
        [CustomDescription("OT Discharge", "OT", "OT", "Notes", "OTNotes", "OT Discharge")]
        OTDischarge = 103,
        [CustomDescription("ST Re-Evaluation", "ST", "ST", "Notes", "STNotes", "ST Re-Evaluation")]
        STReEvaluation = 104,
        [CustomDescription("PT Maintenance Visit", "PT", "PT", "Notes", "PTNotes", "PT Maintenance Visit")]
        PTMaintenance = 105,
        [CustomDescription("OT Maintenance Visit", "OT", "OT", "Notes", "OTNotes", "OT Maintenance Visit")]
        OTMaintenance = 106,
        [CustomDescription("ST Maintenance Visit", "ST", "ST", "Notes", "STNotes", "ST Maintenance Visit")]
        STMaintenance = 107,
        [CustomDescription("Medicare Eligibility Report", "MER", "MER", "ReportsAndNotes", "MedicareEligibilityReport", "Medicare Eligibility Report")]
        MedicareEligibilityReport = 108,
        [CustomDescription("Coordination of Care", "COC", "SN", "Notes", "CoordinationOfCare", "Coordination Of Care")]
        CoordinationOfCare = 109,
        [CustomDescription("Driver/Transportation Log", "MSW", "MSW", "Notes", "DriverOrTransportationNote", "Driver/Transportation Log")]
        DriverOrTransportationNote = 110,
        [CustomDescription("SN Diabetic Daily Visit", "SNV", "SN", "Notes", "SN", "SN Diabetic Daily Visit")]
        SNDiabeticDailyVisit = 111,
        [CustomDescription("UAP Wound Care Note", "HHA", "HHA", "Notes", "UAP", "UAP Wound Care Note")]
        UAPWoundCareVisit = 113,
        [CustomDescription("UAP Insulin Prep-Aministration Note", "HHA", "HHA", "Notes", "UAP", "UAP Insulin Prep-Aministration Note")]
        UAPInsulinPrepAdminVisit = 114,
        [CustomDescription("Home Maker Note", "HHA", "HHA", "Notes", "HomeMakerNote", "Home Maker Note")]
        HomeMakerNote = 115,
        [CustomDescription("PT Discharge Summary", "PT DS", "PT", "Notes", "PTDS", "PT Discharge Summary")]
        PTDischargeSummary = 116,
        [CustomDescription("OT Discharge Summary", "OT DS", "OT", "Notes", "OTDS", "OT Discharge Summary")]
        OTDischargeSummary = 117,
        [CustomDescription("OASIS-C Transfer Discharge (PT)", "TransferDischarge", "OASIS", "Assessment", "OASISCTransferDischargePT", "OASIS-C Transfer Discharge (PT)")]
        OASISCTransferDischargePT = 118,
        [CustomDescription("SN Pediatric Visit", "SNV", "SN", "Notes", "SN", "Skilled Nursing Pediatric Visit")]
        SNPediatricVisit = 119,
        [CustomDescription("Lab", "SNV", "SN", "Notes", "Lab", "Lab")]
        Labs = 120,
        [CustomDescription("PTA Supervisory Visit", "PTA Sup Visit", "PT", "Notes", "PTSupervisoryVisit", "PTA Supervisory Visit")]
        PTSupervisoryVisit = 121,
        [CustomDescription("COTA Supervisory Visit", "COTA Sup Visit", "OT", "Notes", "OTSupervisoryVisit", "COTA Supervisory Visit")]
        OTSupervisoryVisit = 122,
        [CustomDescription("PT Reassessment", "PT Reassessment", "PT", "Notes", "PTReassessment", "PT Reassessment")]
        PTReassessment = 123,
        [CustomDescription("OT Reassessment", "OT Reassessment", "OT", "Notes", "OTReassessment", "OT Reassessment")]
        OTReassessment = 124,
        [CustomDescription("Initial Summary of Care", "ISOC", "SN", "Notes", "SN", "Initial Summary of Care")]
        InitialSummaryOfCare = 125,
        [CustomDescription("SN Insulin Noon Visit", "SNV", "SN", "Notes", "SN", "SN Insulin - Noon")]
        SNInsulinNoon = 126,
        [CustomDescription("SN Insulin HS Visit", "SNV", "SN", "Notes", "SN", "SN Insulin - HS")]
        SNInsulinHS = 127,
        [CustomDescription("SN Pediatric Assessment", "SNV", "SN", "Notes", "SN","SN Pediatric Assessment")]
        SNPediatricAssessment = 128,
        [CustomDescription("SN Psychiatric Nurse Assessment", "SNV", "SN", "Notes", "SN", "SN Psychiatric Nurse Assessment")]
        SNPsychAssessment = 129,
        [CustomDescription("ST Reassessment", "ST", "ST", "Notes", "STNotes", "ST Reassessment")]
        STReassessment = 130,
        [CustomDescription("PAS Travel Note", "HHA", "HHA", "Notes", "PASTravel", "PAS Travel Note")]
        PASTravel = 131
    }
}
