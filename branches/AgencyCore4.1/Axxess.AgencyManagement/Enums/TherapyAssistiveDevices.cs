﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum TherapyAssistiveDevices 
    {
        [Description("Rolling walker")]
        RollingWalker = 1,
        [Description("Standard walker")]
        StandardWalker = 2,
        [Description("Platform walker")]
        PlatformWalker = 3,
        [Description("4 wheeled walker")]
        FourWheeledWalker = 4,
        [Description("Wide based quad cane")]
        WideBasedQuadCane = 5,
        [Description("Short based quad cane")]
        ShortBasedQuadCane = 6,
        [Description("Standard cane")]
        StandardCan = 7,
        [Description("Crutches")]
        Crutches = 8,
        [Description("None")]
        None = 9,
        [Description("Other")]
        Other = 10
    }
}
