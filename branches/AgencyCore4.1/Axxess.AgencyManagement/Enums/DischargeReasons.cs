﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum DischargeReasons
    {
        [Description("Admitted to Hospital")]
        AdmittedToHospital = 1,
        [Description("Admitted to SN / IC Facility")]
        AdmittedToSN = 2,
        [Description("Death")]
        Death = 3,
        [Description("Family / Friends Assumed Responsibility")]
        FamilyAssumedResponsibility = 4,
        [Description("Lack of Funds")]
        LackOfFunds = 5,
        [Description("Lack of Progress")]
        LackOfProgress = 6,
        [Description("No Further Home Health Care Needed")]
        NoFurtherCareNeeded = 7,
        [Description("Patient Moved out of Area")]
        PatientMoved = 8,
        [Description("Patient Refused Service")]
        PatientRefusedService = 9,
        [Description("Physician Request")]
        PhysicianRequest = 10,
        [Description("Transferred to Another HHA")]
        TransferredToAnotherHHA = 11,
        [Description("Transferred to Home Care (Personal Care)")]
        TransferredToHomeCare = 12,
        [Description("Transferred to Hospice")]
        TransferredToHospice = 13,
        [Description("Transferred to Outpatient Rehabilitation")]
        TransferredToOutpatientRehabilitation = 14,
        [Description("Other")]
        Other = 15
    }
}
