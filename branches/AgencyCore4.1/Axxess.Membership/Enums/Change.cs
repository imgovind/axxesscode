﻿namespace Axxess.Membership.Enums
{
    public enum Change
    {
        Password,
        Signature,
        Nothing
    }
}
