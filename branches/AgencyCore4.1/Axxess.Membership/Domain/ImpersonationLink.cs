﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Xml.Linq;

    public class ImpersonationLink
    {
        public Guid Id { get; set; }
        public bool IsUsed { get; set; }
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string RepName { get; set; }
        public Guid RepLoginId { get; set; }
        public DateTime Created { get; set; }
    }
}
