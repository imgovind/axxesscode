﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData; 

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;

    public interface IPrivateDutyScheduleService
    {
        bool AddTask(PrivateDutyScheduleTask task);
        bool UpdateTaskDetails(PrivateDutyScheduleTask editedTask, HttpFileCollectionBase httpFiles);
        bool DeleteTask(Guid patientId, Guid id);
        bool RestoreTask(Guid patientId, Guid id);

        List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDates(Guid patientId, DateTime startDate, DateTime endDate);
        List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid userId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid patientId, DateTime startDate, DateTime endDate);
    }
}
