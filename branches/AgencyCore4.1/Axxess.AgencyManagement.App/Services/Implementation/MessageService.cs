﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    public class MessageService : IMessageService
    {
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IMessageRepository messageRepository;

        public MessageService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
        }

        public bool SendMessage(Message message, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            Asset asset = null;
            var recipientNames = new StringBuilder();
            var carbonCopyNames = new StringBuilder();
            var recipients = new Dictionary<Guid, string>();
            var emailAddresses = new Dictionary<Guid, string>();

            message.FromId = Current.UserId;
            message.FromName = Current.UserFullName;
            message.AgencyId = Current.AgencyId;

            if (httpFiles != null && httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Attachment1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var binaryReader = new BinaryReader(file.InputStream);
                    asset = new Asset
                    {
                        FileName = file.FileName,
                        AgencyId = Current.AgencyId,
                        ContentType = file.ContentType,
                        FileSize = file.ContentLength.ToString(),
                        Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                    };
                    if (assetRepository.Add(asset))
                    {
                        message.AttachmentId = asset.Id;
                    }
                }
            }

            message.Recipients.ForEach(u =>
            {
                var user = userRepository.Get(u, Current.AgencyId);
                if (user != null)
                {
                    if (!user.LoginId.IsEmpty())
                    {
                        var login = loginRepository.Find(user.LoginId);
                        if (login != null && login.IsActive)
                        {
                            emailAddresses.Add(u, login.EmailAddress);
                            recipients.Add(u, user.FirstName);
                            recipientNames.AppendFormat("{0}; ", user.DisplayName);
                        }
                    }
                }
            });

            if (message.CarbonCopyRecipients != null)
            {
                message.CarbonCopyRecipients.ForEach(u =>
                {
                    var user = userRepository.Get(u, Current.AgencyId);
                    if (user != null)
                    {
                        if (!user.LoginId.IsEmpty())
                        {
                            var login = loginRepository.Find(user.LoginId);
                            if (login != null && login.IsActive)
                            {
                                emailAddresses.Add(u, login.EmailAddress);
                                recipients.Add(u, user.FirstName);
                                carbonCopyNames.AppendFormat("{0}; ", user.DisplayName);
                            }
                        }
                    }
                });
            }

            message.Recipients.ForEach(r =>
            {
                message.RecipientId = r;
                message.RecipientNames = recipientNames.ToString();
                message.CarbonCopyNames = carbonCopyNames.ToString();
                if (messageRepository.Add(message))
                {
                    if (recipients.ContainsKey(r))
                    {
                        var parameters = new string[4];
                        parameters[0] = "recipientfirstname";
                        parameters[1] = recipients[r];
                        parameters[2] = "senderfullname";
                        parameters[3] = Current.UserFullName;
                        var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                        Notify.User(CoreSettings.NoReplyEmail, emailAddresses[r], string.Format("{0} sent you a message.", Current.UserFullName), bodyText);
                    }
                    result = true;
                }
            });

            return result;
        }

        public IList<Message> GetMessages(string inboxType, int pageNumber)
        {
            var messages = new List<Message>();
            if (inboxType.IsNotNullOrEmpty())
            {
                if (inboxType.IsEqual("sent"))
                {
                    var sentMessages = messageRepository.GetSentMessages(Current.UserId, Current.AgencyId, pageNumber);
                    sentMessages.ForEach(sm =>
                    {
                        sm.FromName = sm.RecipientNames.IsNotNullOrEmpty() ? sm.RecipientNames.Length > 25 ? sm.RecipientNames.Substring(0, 25) + " ..." : sm.RecipientNames : string.Empty;
                        sm.Subject = sm.Subject.IsNotNullOrEmpty() ? sm.Subject.Length > 35 ? sm.Subject.Substring(0, 35) + " ..." : sm.Subject : string.Empty;
                        messages.Add(sm);
                    });
                }
                else if (inboxType.IsEqual("bin"))
                {
                    var regularMessages = messageRepository.GetDeletedMessagesAndSystemMessages(Current.UserId, Current.AgencyId, pageNumber).ToList();
                    if (regularMessages != null && regularMessages.Count > 0)
                    {
                        var user = userRepository.GetUserOnly(Current.UserId, Current.AgencyId);
                        if (user != null)
                        {
                            var userMessages = user.Messages.IsNotNullOrEmpty() ? user.Messages.ToObject<List<MessageState>>().Where(m => m.IsDeprecated == true).ToList() : new List<MessageState>();
                            if (userMessages != null && userMessages.Count > 0)
                            {
                                userMessages.ForEach(m =>
                                {
                                    var systemMessage = regularMessages.Find(s => s.Id == m.Id);
                                    if (systemMessage != null)
                                    {
                                        messages.Add(new Message
                                        {
                                            Id = systemMessage.Id,
                                            AgencyId = Current.AgencyId,
                                            Subject = systemMessage.Subject,
                                            Body = systemMessage.Body.ReplaceTokens(),
                                            Created = systemMessage.Created,
                                            FromName = "Axxess",
                                            MarkAsRead = m.IsRead,
                                            RecipientId = Current.UserId,
                                            RecipientNames = Current.UserFullName,
                                            Type = MessageType.System
                                        });
                                        regularMessages.Remove(systemMessage);
                                    }
                                });
                            }
                        }
                        messages.AddRange(regularMessages);
                    }
                }
                else
                {
                    var regularMessages = messageRepository.GetUserMessagesAndSystemMessages(Current.UserId, Current.AgencyId, pageNumber).ToList();
                    if (regularMessages != null && regularMessages.Count > 0)
                    {
                        var user = userRepository.GetUserOnly(Current.UserId, Current.AgencyId);
                        if (user != null)
                        {
                            var userMessages = user.Messages.IsNotNullOrEmpty() ? user.Messages.ToObject<List<MessageState>>().Where(m => m.IsDeprecated == false).ToList() : new List<MessageState>();
                            if (userMessages != null && userMessages.Count > 0)
                            {
                                userMessages.ForEach(m =>
                                {
                                    var systemMessage = regularMessages.Find(s => s.Id == m.Id);
                                    if (systemMessage != null)
                                    {
                                        messages.Add(new Message
                                        {
                                            Id = systemMessage.Id,
                                            AgencyId = Current.AgencyId,
                                            Subject = systemMessage.Subject,
                                            Body = systemMessage.Body.ReplaceTokens(),
                                            Created = systemMessage.Created,
                                            FromName = "Axxess",
                                            MarkAsRead = m.IsRead,
                                            RecipientId = Current.UserId,
                                            RecipientNames = Current.UserFullName,
                                            Type = MessageType.System
                                        });
                                        regularMessages.Remove(systemMessage);
                                    }
                                });
                            }
                            messages.AddRange(regularMessages);
                        }
                    }
                }
            }
            return messages.OrderByDescending(m => m.Created).ToList();
        }

        public int GetTotalMessagesCount(string inboxType)
        {
            if (inboxType.IsNotNullOrEmpty())
            {
                if (inboxType.IsEqual("sent"))
                {
                    return messageRepository.GetTotalCountOfSentMessages(Current.UserId, Current.AgencyId);
                }
                else if (inboxType.IsEqual("bin"))
                {
                    return messageRepository.GetTotalCountOfDeletedMessagesAndSystemMessages(Current.UserId, Current.AgencyId); ;
                }
                else
                {
                    return messageRepository.GetTotalCountOfUserMessagesAndSystemMessages(Current.UserId, Current.AgencyId);
                }
            }
            return 0;
        }

        public Message GetSystemMessage(Guid id)
        {
            var systemMessage = messageRepository.GetSystemMessage(id);
            if (systemMessage != null)
            {
                var user = userRepository.GetUserOnly(Current.UserId, Current.AgencyId);
                if (user != null)
                {
                    var userMessages = user.Messages.ToObject<List<MessageState>>();
                    if (userMessages != null && userMessages.Count > 0)
                    {
                        var m = userMessages.Find(u => u.Id == id);
                        if (m != null)
                        {
                            if (!m.IsRead)
                            {
                                m.IsRead = true;
                                user.Messages = userMessages.ToXml();
                                userRepository.UpdateModel(user);
                            }

                            return new Message
                            {
                                Id = systemMessage.Id,
                                Subject = systemMessage.Subject,
                                Body = systemMessage.Body.ReplaceTokens(),
                                Created = systemMessage.Created,
                                FromName = "Axxess",
                                MarkAsRead = m.IsRead,
                                RecipientId = Current.UserId,
                                RecipientNames = Current.UserFullName,
                                Type = MessageType.System,
                                IsDeprecated = m.IsDeprecated
                            };
                        }
                    }
                }
            }
            return new Message();
        }

        public bool DeleteSystemMessage(Guid id)
        {
             var user = userRepository.GetUserOnly(Current.UserId, Current.AgencyId);
             if (user != null)
             {
                 var userMessages = user.Messages.ToObject<List<MessageState>>();
                 if (userMessages != null && userMessages.Count > 0)
                 {
                     var m = userMessages.Find(u => u.Id == id);
                     if (m != null)
                     {
                         m.IsDeprecated = true;
                         user.Messages = userMessages.ToXml();
                         return userRepository.UpdateModel(user);
                     }
                 }
             }

             return false;
        }

    }
}
