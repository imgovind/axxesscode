﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Linq;

    using Domain;
    using ViewData; 

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.Log.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.OasisC.Repositories;
    using Axxess.AgencyManagement.Extensions;

    public class PrivateDutyScheduleService : IPrivateDutyScheduleService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPrivateDutyScheduleRepository scheduleRepository;
        #endregion

        #region Constructor

        public PrivateDutyScheduleService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
        }

        #endregion

        public List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDates(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var tasks = scheduleRepository.GetScheduleTasksBetweenDates(Current.AgencyId, patientId, startDate, endDate);
            if (tasks.IsNotNullOrEmpty())
            {
                List<Guid> userIds = tasks.Select(s => s.UserId).ToList();
                var users = userRepository.GetUsersByIds(Current.AgencyId, userIds);
                foreach (var task in tasks)
                {
                    var user = users.FirstOrDefault(u => u.Id == task.UserId);
                    if (user != null)
                    {
                        task.UserName = user.DisplayName;
                    }
                }
            }
            return tasks;
        }

        public List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid userId, DateTime startDate, DateTime endDate)
        {
            var tasks = scheduleRepository.GetUsersScheduleTasksBetweenDates(Current.AgencyId, userId, startDate, endDate);
            if (tasks.IsNotNullOrEmpty())
            {
                string userName = UserEngine.GetName(userId, Current.AgencyId);
                if(userName.IsNotNullOrEmpty())
                {
                    foreach (var task in tasks)
                    {
                        task.UserName = userName;
                    }
                }
            }
            return tasks;
        }

        public List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDates(Guid userId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var tasks = scheduleRepository.GetUsersScheduleTasksBetweenDates(Current.AgencyId, userId, patientId, startDate, endDate);
            if (tasks.IsNotNullOrEmpty())
            {
                string userName = UserEngine.GetName(userId, Current.AgencyId);
                if (userName.IsNotNullOrEmpty())
                {
                    foreach (var task in tasks)
                    {
                        task.UserName = userName;
                    }
                }
            }
            return tasks;
        }

        public bool AddTask(PrivateDutyScheduleTask task)
        {
            bool result = false;
            if (task != null)
            {
                task.AgencyId = Current.AgencyId;
                task.Id = Guid.NewGuid();
                task.UserName = UserEngine.GetName(task.UserId, Current.AgencyId);
                task.VisitStartTime = task.EventStartTime;
                task.VisitEndTime = task.EventEndTime;
                ProcessTask(task);
                result = scheduleRepository.AddTask(task);
            }
            return result;
        }

        public bool DeleteTask(Guid patientId, Guid id)
        {
            bool result = false;
            var task = scheduleRepository.GetTask(Current.AgencyId, patientId, id);
            if (task != null)
            {
                if (ProcessTaskStatusUpdate(task, true))
                {
                    if (scheduleRepository.DeleteTask(task))
                    {
                        result = true;
                    }
                    else
                    {
                        ProcessTaskStatusUpdate(task, false);
                    }
                }
            }
            return result;
        }

        public bool RestoreTask(Guid patientId, Guid id)
        {
            bool result = false;
            var task = scheduleRepository.GetTask(Current.AgencyId, patientId, id);
            if (task != null)
            {
                if (ProcessTaskStatusUpdate(task, false))
                {
                    if (scheduleRepository.RestoreTask(task))
                    {
                        result = true;
                    }
                    else
                    {
                        ProcessTaskStatusUpdate(task, true);
                    }
                }
            }
            return result;
        }

        public bool UpdateTaskDetails(PrivateDutyScheduleTask editedTask, HttpFileCollectionBase httpFiles)
        {
            bool result = false;
            if (editedTask != null)
            {
                var task = scheduleRepository.GetTask(Current.AgencyId, editedTask.PatientId, editedTask.Id);

                //TODO Update assets

                if (!editedTask.UserId.IsEmpty())
                {
                    task.UserId = editedTask.UserId;
                }
                task.Comments = editedTask.Comments;
                //task.Status = editedTask.Status;
                task.EventStartTime = editedTask.EventStartTime;
                task.EventEndTime = editedTask.EventEndTime;
                task.VisitStartTime = editedTask.VisitStartTime;
                task.VisitEndTime = editedTask.VisitEndTime;
                //task.IsBillable = editedTask.IsBillable;
                //task.Surcharge = editedTask.Surcharge;
                //task.AssociatedMileage = editedTask.AssociatedMileage;
                //task.IsMissedVisit = editedTask.IsMissedVisit;
                task.IsAllDay = editedTask.IsAllDay;
                //task.DisciplineTask = editedTask.DisciplineTask;
                //task.PhysicianId = editedTask.PhysicianId;
                if (ProcessDetailUpdate(task))
                {
                    if (scheduleRepository.UpdateTask(task))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }


        private void ProcessTask(PrivateDutyScheduleTask task)
        {
            var discipline = lookupRepository.GetPrivateDutyDisciplineTask(task.DisciplineTask);
            if (discipline != null)
            {
                task.Discipline = discipline.Discipline;
                task.IsBillable = discipline.IsBillable;
                task.Version = discipline.Version;
                if (task.IsNote())
                {
                    task.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var patientVisitNote = new PrivateDutyPatientVisitNote { AgencyId = Current.AgencyId, Id = task.Id, PatientId = task.PatientId, NoteType = ((DisciplineTasks)task.DisciplineTask).ToString(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = task.UserId, IsBillable = task.IsBillable };
                    scheduleRepository.AddVisitNote(patientVisitNote);
                }
                else if (task.IsAssessment())
                {
                    task.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                }
                else if (task.IsOrder())
                {
                    task.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                }
                else if (task.IsReport())
                {
                    task.Status = ((int)ScheduleStatus.ReportAndNotesCreated).ToString();
                }
            }
        }

        private bool ProcessTaskStatusUpdate(PrivateDutyScheduleTask task, bool isDeprecated)
        {
            bool result = false;
            if (task != null)
            {
                if (task.IsNote())
                {
                    result = scheduleRepository.UpdateVisitNoteStatus(Current.AgencyId, task.PatientId, task.Id, isDeprecated);
                }
                else if (task.IsAssessment())
                {
                    //assessmentService.MarkAsDeleted(id, Guid.Empty, patientId, task.DisciplineTaskName, isDeprecated);
                }
                else if (task.IsOrder())
                {

                }
                else if (task.IsReport())
                {

                }
            }
            return result;
        }

        private bool ProcessDetailUpdate(PrivateDutyScheduleTask task)
        {
            bool result = false;
            var type = ((DisciplineTasks)task.DisciplineTask).ToString();
            var discipline = lookupRepository.GetPrivateDutyDisciplineTask(task.DisciplineTask);
            if (discipline != null)
            {
                if (task.IsNote())
                {
                    var visitNote = patientRepository.GetVisitNote(Current.AgencyId, task.PatientId, task.Id);
                    if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                    {
                        //var notesQuestions = visitNote.ToDictionary();
                        //if (notesQuestions != null)
                        //{
                            //if (notesQuestions.ContainsKey("VisitStartTime")) { notesQuestions["VisitStartTime"].Answer = task.VisitStartTime; } else { notesQuestions.Add("VisitStartTime", new NotesQuestion { Answer = task.VisitStartTime.ToJavascriptFormat(), Name = "VisitStartTime", Type = type }); }
                            //if (notesQuestions.ContainsKey("VisitEndTime")) { notesQuestions["VisitEndTime"].Answer = task.VisitEndTime; } else { notesQuestions.Add("VisitEndTime", new NotesQuestion { Answer = task.VisitEndTime.ToJavascriptFormat(), Name = "TimeIn", Type = type }); }
                            //if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = task.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = task.Surcharge, Name = "Surcharge", Type = type }); }
                            //if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = task.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = task.AssociatedMileage, Name = "AssociatedMileage", Type = type }); }

                            //if (discipline.IsTypeChangeable)
                            //{
                            //    visitNote.NoteType = type;
                            //}

                            //visitNote.Note = notesQuestions.Values.ToList().ToXml();
                            //visitNote.IsDeprecated = task.IsDeprecated;

                            //visitNote.Status = task.Status.IsNotNullOrEmpty() && task.Status.IsInteger() ? task.Status.ToInteger() : visitNote.Status;
                            result = patientRepository.UpdateVisitNote(visitNote);
                        //}
                        //else
                        //{
                        //    result = true;
                        //}
                    }
                    else
                    {
                        result = true;
                    }

                }
                else if (task.IsAssessment())
                {

                }
                else if (task.IsOrder())
                {

                }
                else if (task.IsReport())
                {
                }
            }
            return result;
        }
    }
}
