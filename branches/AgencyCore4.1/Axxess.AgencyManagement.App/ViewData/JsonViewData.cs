﻿namespace Axxess.AgencyManagement.App.ViewData
{
    public class JsonViewData
    {
        public bool isSuccessful
        {
            get;
            set;
        }

        public string errorMessage
        {
            get;
            set;
        }

        public string redirectUrl
        {
            get;
            set;
        }
    }
}
