﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class DisciplineOccurenceViewData
    {
        public int MSWCount { get; set; }
        public int STCount { get; set; }
        public int OTCount { get; set; }
        public int PTCount { get; set; }
        public int HHACount { get; set; }
        public int SNCount { get; set; }
    }
}
