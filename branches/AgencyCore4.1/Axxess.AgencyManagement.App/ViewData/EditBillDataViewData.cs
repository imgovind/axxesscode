﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App
{
    public class EditBillDataViewData
    {
        public Guid Id { get; set; }
        public ChargeRate ChargeRate { get; set; }
        public string TypeOfClaim { get; set; }
    }
}
