﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class FrequenciesViewData
    {
        public FrequenciesViewData()
        {
            Visits = new Dictionary<string, VisitData>();
        }
        public Dictionary<String, VisitData> Visits { get; set; }

      
    }
}
