﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;    
    using System.Xml.Linq;

    public class PhysicianViewData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string MailingAddressLine1 { get; set; }
        public string MailingAddressLine2 { get; set; }
        public string MailingAddressCity { get; set; }
        public string MailingAddressStateCode { get; set; }
        public string MailingAddressZipCode { get; set; }
        public string PracticeLocationAddressLine1 { get; set; }
        public string PracticeLocationAddressLine2 { get; set; }
        public string PracticeLocationAddressCity { get; set; }
        public string PracticeLocationAddressStateCode { get; set; }
        public string PracticeLocationAddressZipCode { get; set; }
        public string PhoneWork { get; set; }
        public string EmailAddress { get; set; }
        public string FaxNumber { get; set; }
        public string NPI { get; set; }
        public string UPIN { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseStatedId { get; set; }
        public string Credentials { get; set; }
        public string Comments { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Primary { get; set; }
        public string[] PhysicianContactPrimaryPhoneArray { get; set; }
        public string[] PhysicianContactAltPhoneArray { get; set; }
        public string[] PhysicianContactFaxArray { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
    }
}
