﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.LookUp.Domain;

namespace Axxess.AgencyManagement.App
{
    public class ClaimEditSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Supply Supply { get; set; }
        public string Type { get; set; }

        public ClaimEditSupplyViewData()
        {

        }

        public ClaimEditSupplyViewData(Guid id, Guid patientId, string type, Supply supply)
        {
            Id = id;
            PatientId = patientId;
            Type = type;
            Supply = supply;
        }
    }
}
