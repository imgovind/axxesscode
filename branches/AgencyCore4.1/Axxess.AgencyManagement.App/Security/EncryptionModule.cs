﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.App.Logging;

    public class EncryptionModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += new EventHandler(this.OnBeginRequest);
        }

        #endregion

        #region Private Methods

        void OnBeginRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpRequest currentRequest = currentContext.Request;

            if (currentRequest.RawUrl.Contains("?") && !currentRequest.RawUrl.ToLower().Contains("lookup") && !currentRequest.RawUrl.Contains(".axd") && !currentRequest.RawUrl.ToLower().Contains(".htm"))
            {
                string query = currentRequest.Url.Query.Replace("?", "");
                string path = currentRequest.Url.AbsolutePath.Substring(1);

                if (query.StartsWith("enc=", StringComparison.OrdinalIgnoreCase))
                {
                    string decryptedQuery = Crypto.Decrypt(query.Replace("enc=", ""));

                    if (!string.IsNullOrEmpty(decryptedQuery))
                    {
                        currentContext.RewritePath(string.Format("/{0}", path), string.Empty, decryptedQuery);
                    }
                    else
                    {
                        string msg = string.Format("Decryption failure in the Encryption Module. URL = {0}.", currentRequest.Url.PathAndQuery);
                        Logger.Warning(msg);
                    }
                }
                else if (currentRequest.HttpMethod == "GET")
                {
                    string encryptedQuery = Crypto.Encrypt(query);

                    if (!string.IsNullOrEmpty(encryptedQuery))
                    {
                        currentContext.Response.Redirect(string.Format("/{0}?enc={1}", path, encryptedQuery));
                    }
                    else
                    {
                        string msg = string.Format("Encryption failure in the Encryption Module. URL = {0}.", currentRequest.Url.PathAndQuery);
                        Logger.Warning(msg);
                    }
                }
            }
        }

        #endregion
    }
}
