﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Threading;

    using Enums;
    using Domain;
    using Exports;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using System.Net;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Xml.XPath;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class RequestController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;

        public RequestController(IAgencyManagementDataProvider agencyManagementDataProvider, IReportService reportService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.reportService = reportService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region RequestController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TherapyManagementReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Therapy Management\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Therapy Management Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddTherapyManagementReport(BranchId, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                    viewData.errorMessage = "You will be notified when the \"Therapy Management\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HHRGReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"HHRG\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "HHRG Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddHHRGReport(BranchId, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                    viewData.errorMessage = "You will be notified when the \"HHRG\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UnbilledVisitsForManagedClaimsReport(Guid BranchId, int InsuranceId, int Status, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Unbilled Visits for Managed Claims Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Unbilled Visits for Managed Claims Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (InsuranceId != 0)
                {
                    if (agencyRepository.AddReport(report))
                    {
                        new Thread(() => ReportManager.AddUnbilledVisitsReport(BranchId, InsuranceId, Status, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                        viewData.errorMessage = "You will be notified when the Unbilled Visits for Managed Claims Report is completed.";
                        viewData.isSuccessful = true;
                    }
                }
                else
                {
                    viewData.errorMessage = "Please select an Insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsAndVisitsByAgeReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patients & Visits By Age\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patients & Visits By Age Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientsAndVisitsByAgeReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Patients & Visits By Age\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DischargesByReasonReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Discharges By Reason\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Discharges By Reason Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddDischargesByReasonReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Discharges By Reason\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Visits By Primary Payment Source\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Visits By Primary Payment Source Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddVisitsByPrimaryPaymentSourceReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Visits By Primary Payment Source\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByStaffTypeReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Visits By Staff Type\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Visits By Staff Type Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddVisitsByStaffTypeReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Visits By Staff Type\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AdmissionsByReferralSourceReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Admissions By Referral Source\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Admissions By Referral Source Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddAdmissionsByReferralSourceReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Admissions By Referral Source\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patients & Visits By Principal Diagnosis\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patients & Visits By Principal Diagnosis Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientsVisitsByPrincipalDiagnosisReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Patients & Visits By Principal Diagnosis\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CostReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Cost Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Cost Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddCostReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the Cost Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReport(Guid completedReportId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Report could not be deleted." };
            var report = agencyRepository.GetReport(Current.AgencyId, completedReportId);
            if (report != null)
            {
                report.IsDeprecated = true;
                if (agencyRepository.UpdateReport(report))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "This report has been successfully deleted.";
                }
            }
            return Json(viewData);

        }

        #endregion
    }
}
