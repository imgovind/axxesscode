﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Services;
    using iTextExtension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.ObjectModel;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MessageController : BaseController
    {
        #region Private Members

        private readonly IMessageService messageService;
        private readonly IUserRepository userRepository;
        private readonly IMessageRepository messageRepository;
        private readonly IAgencyRepository agencyRepository;

        #endregion

        #region Constructor

        public MessageController(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider, IMessageService messageService)
        {
            Check.Argument.IsNotNull(messageService, "messageService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.messageService = messageService;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.messageRepository = agencyManagementProvider.MessageRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region MessageController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Inbox()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CustomWidget()
        {
            var message = messageRepository.GetCurrentDashboardMessage();
            if (message != null && message.Text.IsNotNullOrEmpty())
            {
                return Json(message);
            }
            return Json(new Message());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult List(string inboxType)
        {
            var pageNumber = HttpContext.Request.Params["page"] != null ? int.Parse(HttpContext.Request.Params["page"]) : 1;

            var messages = messageService.GetMessages(inboxType, pageNumber).Select(s => new MessageItem()
            { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type  }).ToList();
            var messagesCount = messageService.GetTotalMessagesCount(inboxType);
            if (messages != null && messages.Count > 0)
            {
                var gridModel = new GridModel(messages);
                gridModel.Data = messages;
                gridModel.Total = messagesCount;
                return Json(gridModel);
            }

            return Json(new GridModel(new List<MessageItem>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WidgetList()
        {
            var messages = messageService.GetMessages("inbox", 1).Take(5).Select(s => new MessageItem()
            { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type  }).ToList();
            return Json(new GridModel(messages));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id)
        {
            return Json(messageRepository.GetMessage(id, Current.AgencyId, true));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MessagePdf(Guid id, int type)
        {
            Message message = null;
            if (type == (int)MessageType.System)
            {
                message = messageService.GetSystemMessage(id);
            }
            else
            {
                message = messageRepository.GetMessage(id, Current.AgencyId, true);
                if (message != null)
                {
                    if (Current.UserId == message.FromId)
                    {
                        message.Type = MessageType.Sent;
                    }
                }
            }
            var doc = new MessagePdf(message, agencyRepository.GetWithBranches(Current.AgencyId));
            var stream = doc.GetFormattedStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Message_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid id, int type, string inboxType)
        {
            if (type == (int)MessageType.System)
            {
                return PartialView("Info", messageService.GetSystemMessage(id));
            }
            var message = messageRepository.GetMessage(id, Current.AgencyId, true);
            if (message != null)
            {
                if (Current.UserId == message.FromId)
                {
                    message.Type = MessageType.Sent;
                }
            }
            else
            {
                message = new Message();
            }
            if (inboxType.IsNotNullOrEmpty() && inboxType == "inbox")
            {
                message.IsInInbox = true;
            }
            return PartialView("Info", message);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New([Bind] Message message)
        {
            Check.Argument.IsNotNull(message, "message");

            var viewData = new JsonViewData { errorMessage = "Your message could not be sent. Please try again." };

            if (message.IsValid)
            {
                if (messageService.SendMessage(message, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Message has been sent!";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error sending your message.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }

            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Recipients(string searchTerm)
        {
            var recipients = new List<Recipient>();
            var query = userRepository.GetAgencyUsers(searchTerm, Current.AgencyId);
            query.ForEach(e =>
            {
                recipients.Add(new Recipient { id = e.Id.ToString(), name = string.Concat(e.LastName + ", " + e.FirstName) });
            });
            return Json(recipients);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, int type, string inboxType)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Message could not be deleted!" };

            if (type == (int)MessageType.User || (type == (int)MessageType.Sent && inboxType.IsNotNullOrEmpty() && inboxType.Equals("inbox")))
            {
                if (messageRepository.Delete(id, Current.AgencyId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Message was deleted successfully";
                }
            }
            else
            {
                if (messageService.DeleteSystemMessage(id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Message was deleted successfully";
                }
            }

            return Json(viewData);
        }

        #endregion
    }
}
