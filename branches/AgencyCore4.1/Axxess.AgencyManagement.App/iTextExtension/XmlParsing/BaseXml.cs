﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    abstract class BaseXml {
        protected String Type = string.Empty;
        protected List<XmlPrintSection> Layout = null;
        private XElement XmlFile;
        public static String BlankChar = "_";
        public int SectionCount() {
            return this.Layout.Count();
        }
        public String GetType() {
            return this.Type != null ? this.Type : string.Empty;
        }
        public List<XmlPrintSection> GetLayout() {
            return this.Layout;
        }
        protected BaseXml(PdfDoc doc) {
            this.XmlFile = XElement.Load(doc.GetXmlFile());
        }
        protected BaseXml(PdfDoc doc, int rev) {
            this.XmlFile = XElement.Load(doc.GetXmlFile(rev));
        }
        protected void Init() {
            this.Layout = this.ExtractSections(this.XmlFile);
        }

        protected void Init(PdfDoc doc)
        {
            this.XmlFile = XElement.Load(doc.GetXmlFile());
            this.Layout = this.ExtractSections(this.XmlFile);
        }
        
        protected void NotaFilter()
        {
            for (int tabI = 0; tabI < this.Layout.Count(); tabI++)
            {
                for (int fieldsetI = 0; fieldsetI < this.Layout[tabI].Subsection.Count(); fieldsetI++)
                {
                    for (int questionI = 0; questionI < this.Layout[tabI].Subsection[fieldsetI].Question.Count(); questionI++)
                    {
                        if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Type == "checkgroup")
                        {
                            for (int optionI = 0; optionI < this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option.Count(); optionI++) if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion != null && !(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Data.Split(',').Contains(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Value) || this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Data.Equals(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Value))) this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion.RemoveRange(0, this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Option[optionI].Subquestion.Count());
                        }
                        else if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Type == "multiple")
                        {
                            for (int subquestionI = 0; subquestionI < this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count(); subquestionI++)
                            {
                                if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Type == "notacheck" && this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Data.Equals(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Value))
                                {
                                    this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.RemoveRange(0, subquestionI);
                                    if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count() > 0) this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.RemoveRange(1, this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion.Count() - 1);
                                    subquestionI = 0;
                                }
                                if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Type == "checkgroup")
                                {
                                    for (int optionI = 0; optionI < this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option.Count(); optionI++) if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Subquestion != null && !(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Data.Split(',').Contains(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Value) || this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Data.Equals(this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Value))) this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Subquestion.RemoveRange(0, this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Subquestion[subquestionI].Option[optionI].Subquestion.Count());
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual string GetData(String Index) {
            return Index;
        }
        protected int RemoveUnusedOrdersGoals(int Tab, int Fieldset) {
            for (int questionI = 0; questionI < this.Layout[Tab].Subsection[Fieldset].Question.Count(); questionI++) {
                if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Type == "checkgroup") {
                    for (int optionI = 0; optionI < this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.Count(); optionI++) {
                        if (!this.Layout[Tab].Subsection[Fieldset].Question[questionI].Data.Split(',').Contains(this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option[optionI].Value)) this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.RemoveAt(optionI--);
                    }
                    if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.Count == 0) this.Layout[Tab].Subsection[Fieldset].Question.RemoveAt(questionI--);
                }
                else if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Type == "multiple")
                {
                   for (int questionI2 = 0; questionI2 < this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion.Count(); questionI2++) 
                   {
                       if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Type == "checkgroup")
                       {
                           for (int optionI = 0; optionI < this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Option.Count(); optionI++)
                           {
                               if (!this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Data.Split(',').Contains(this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Option[optionI].Value)) this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Option.RemoveAt(optionI--);
                           }
                           if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Option.Count == 0) this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion.RemoveAt(questionI2--);
                       }
                       else if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Type == "textarea")
                       {

                       }
                       else
                       {
                           if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion[questionI2].Data.Trim().Length == 0) this.Layout[Tab].Subsection[Fieldset].Question[questionI].Subquestion.RemoveAt(questionI2--);
                       }
                    }
                }
                else if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Type == "textarea")
                {

                }
                else if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Data.Trim().Length == 0) this.Layout[Tab].Subsection[Fieldset].Question.RemoveAt(questionI--);
            }
            if (this.Layout[Tab].Subsection[Fieldset].Question.Count == 0) this.Layout[Tab].Subsection.RemoveAt(Fieldset--);
            return Fieldset;
        }
        private List<XmlPrintSection> ExtractSections(XElement Node)
        {
            return (from section in Node.Elements("section")
                    where (section.HasAttributes || section.HasElements) && (this.Type == null || section.Attribute("membership") == null || section.Attribute("membership").Value.Split(',').Contains(this.Type))
                    select new XmlPrintSection(this, section)).ToList();
        }

        public string GetJson() {
            StringBuilder Json = new StringBuilder();
            Json.Append("[");
            foreach (XmlPrintSection section in this.Layout)
                Json.Append(section.GetJson());
            Json.Append("]");
            Json.Replace("[],","");
            Json.Replace("}{","},{");
            return Json.ToString();
        }
    }
}