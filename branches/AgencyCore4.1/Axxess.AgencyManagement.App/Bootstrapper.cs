﻿namespace Axxess.AgencyManagement.App
{
    using System.Web.Mvc;

    using StructureMap;

    using OpenForum.Core;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC;

    using Modules;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
                x.AddRegistry<ApplicationRegistry>();
                x.AddRegistry<ForumRegistry>();
            });

            AreaRegistration.RegisterAllAreas();

        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Container.ResolveAll<IStartupTask>().ForEach(t => t.Execute());
            Axxess.Core.Infrastructure.Module.Register(new ReportModule());
            Axxess.Core.Infrastructure.Module.Register(new UserModule());
            Axxess.Core.Infrastructure.Module.Register(new ScheduleModule());
            Axxess.Core.Infrastructure.Module.Register(new AssetModule());
            Axxess.Core.Infrastructure.Module.Register(new OasisModule());
            Axxess.Core.Infrastructure.Module.Register(new PatientModule());
            Axxess.Core.Infrastructure.Module.Register(new MapModule());
            Axxess.Core.Infrastructure.Module.Register(new AgencyModule());
            Axxess.Core.Infrastructure.Module.Register(new AccountModule());
            Axxess.Core.Infrastructure.Module.Register(new DefaultModule());
            OpenForumManager.SimpleInitialize();
        }
    }
}
