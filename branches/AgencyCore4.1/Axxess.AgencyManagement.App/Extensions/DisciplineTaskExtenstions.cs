﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    using Axxess.Core.Extension;

    using Axxess.LookUp.Domain;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class DisciplineTaskExtenstions
    {
        public static string BillDisciplineIdentify(this DisciplineTask task)
        {
            switch (task.Id)
            {
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                    return "SkilledNurseSOC";

                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                    return "SkilledNurseRecet";

                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                    return "SkilledNurse";

                case (int)DisciplineTasks.SNVTeachingTraining:
                    return "SkilledNurseTeaching";

                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                    return "SkilledNurseManagement";

                case (int)DisciplineTasks.SNVObservationAndAssessment:
                    return "SkilledNurseObservation";

                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                    return "PhysicalTherapy";

                case (int)DisciplineTasks.PTAVisit:
                    return "PhysicalTherapyAssistance";

                case (int)DisciplineTasks.PTMaintenance:
                    return "PhysicalTherapyMaintenance";

                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                    return "OccupationalTherapy";

                case (int)DisciplineTasks.OTMaintenance:
                    return "OccupationalTherapyMaintenance";

                case (int)DisciplineTasks.STMaintenance:
                    return "SpeechTherapyMaintenance";

                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                    return "SpeechTherapy";

                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                    return "MedicareSocialWorker";

                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASTravel:
                    return "HomeHealthAide";

                case (int)DisciplineTasks.COTAVisit:
                    return "OccupationalTherapyAssistance";

                case (int)DisciplineTasks.PhysicianOrder:
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                case (int)DisciplineTasks.FaceToFaceEncounter:
                case (int)DisciplineTasks.IncidentAccidentReport:
                case (int)DisciplineTasks.InfectionReport:
                case (int)DisciplineTasks.CommunicationNote:
                    return string.Empty;
            }
            return string.Empty;
        }
    }
}
