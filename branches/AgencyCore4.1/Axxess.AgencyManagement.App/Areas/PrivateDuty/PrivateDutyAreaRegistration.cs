﻿using System.Web.Mvc;

namespace Axxess.AgencyManagement.WebSite.Areas.PrivateDuty
{
    public class PrivateDutyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrivateDuty";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            
            context.MapRoute(
               "PrivateDutyScheduleTask",
               "PrivateDuty/Task/{action}/{id}",
               new { controller = "Schedule", action = "New", id = UrlParameter.Optional },
               null,
               new string[] { "Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers" }
           );

            context.MapRoute(
             "PrivateDutyScheduleDefault",
             "PrivateDuty/Schedule/{action}/{id}",
             new { controller = "Schedule", action = "Center", id = UrlParameter.Optional },
             null,
             new string[] { "Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers" }
         );

            //context.MapRoute(
            //    "PrivateDutyScheduleTaskList",
            //    "PrivateDuty/Schedule/TaskList/{id}/{startDate}/{endDate}",
            //    new { controller = "Schedule", action = "TaskList", id = UrlParameter.Optional, startDate = UrlParameter.Optional, endDate = UrlParameter.Optional },
            //    null,
            //    new string[] { "Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers" }
            //);

            //context.MapRoute(
            //    "PrivateDutySchedulePatientList",
            //    "PrivateDuty/Schedule/CenterPatientGrid/{id}",
            //    new { controller = "Schedule", action = "CenterPatientGrid", id = UrlParameter.Optional },
            //    null,
            //    new string[] { "Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers" }
            //);

            //#region Private Duty
            //context.MapRoute(
            //    "PrivateDutyCenter",
            //    "Schedule/PrivateDuty/Center",
            //    new { controller = this.Name, action = "PrivateDutyCenter" });

            //context.MapRoute(
            //    "PrivateDutyEventList",
            //    "Schedule/PrivateDuty/Event/List",
            //    new { controller = this.Name, action = "PrivateDutyEventList", patientId = new IsGuid() });

            //routes.MapRoute(
            //    "PrivateDutyEventNew",
            //    "Schedule/PrivateDuty/Event/New",
            //    new { controller = this.Name, action = "PrivateDutyEventNew", patientId = new IsGuid() });

            //routes.MapRoute(
            //    "PrivateDutyEventCreate",
            //    "Schedule/PrivateDuty/Event/Create",
            //    new { controller = this.Name, action = "PrivateDutyEventCreate" });

            //routes.MapRoute(
            //    "PrivateDutyEventEdit",
            //    "Schedule/PrivateDuty/Event/Edit",
            //    new { controller = this.Name, action = "PrivateDutyEventEdit", id = new IsGuid(), patientId = new IsGuid() });

            //routes.MapRoute(
            //    "PrivateDutyEventUpdate",
            //    "Schedule/PrivateDuty/Event/Update",
            //    new { controller = this.Name, action = "PrivateDutyEventUpdate" });

            //routes.MapRoute(
            //    "PrivateDutyEventDelete",
            //    "Schedule/PrivateDuty/Event/Delete",
            //    new { controller = this.Name, action = "PrivateDutyEventDelete", id = new IsGuid(), patientId = new IsGuid() });
            //#endregion

        }
    }
}
