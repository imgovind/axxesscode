﻿namespace Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using ViewData;
    using Services;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Workflows;

    using Telerik.Web.Mvc;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.LookUp.Domain;

    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.App.Areas.PrivateDuty.Models;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IPrivateDutyScheduleService scheduleService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPrivateDutyScheduleRepository scheduleRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IReportService reportService, IPrivateDutyScheduleService scheduleService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = dataProvider.UserRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.scheduleRepository = dataProvider.ScheduleRepository;
            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.scheduleService = scheduleService;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            return PartialView("Center/Layout");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId, "");
            else if (Current.IsClinicianOrHHA)
                patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult List(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var events = scheduleService.GetScheduleTasksBetweenDates(patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            if (events.IsNotNullOrEmpty())
            {
                var jsonEvents = events.Select(s => new
                {
                    Id = s.Id,
                    s.IsAllDay,
                    EventStartTime = s.EventStartTime,
                    EventEndTime = s.EventEndTime,
                    DisciplineTaskName = s.DisciplineTaskName,
                    UserName = s.UserName,
                    Status = s.Status
                });
                return Json(jsonEvents);
            }
            else
            {
                return Json(events);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return PartialView("Center/Grid", scheduleService.GetScheduleTasksBetweenDates(patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            return PartialView("Task/New", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create(PrivateDutyScheduleTask newTask)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved." };
            if (newTask != null)
            {
                if (newTask.IsValid)
                {
                    if (scheduleService.AddTask(newTask))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Task was saved successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult NoteEdit(Guid id, Guid patientId)
        //{
        //    var visitNote = new PrivateDutyPatientVisitNote();
        //    if (!id.IsEmpty() && !patientId.IsEmpty())
        //    {
        //        visitNote = scheduleRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //    }

        //    return PartialView("PrivateDuty/Event/Edit", new PrivateDutyPatientVisitNote());
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id, Guid patientId)
        {
            var task = new PrivateDutyScheduleTask();
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                task = scheduleRepository.GetTask(Current.AgencyId, patientId, id);
            }
            return PartialView("Task/Edit", task);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update([Bind] PrivateDutyScheduleTask editTask)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved." };
            if (editTask != null)
            {
                if (scheduleService.UpdateTaskDetails(editTask, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was updated successfully.";
                }
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult SaveNote([Bind] PrivateDutyPatientVisitNote editVisitNote)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved." };
        //    return Json(viewData);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be deleted." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (scheduleService.DeleteTask(patientId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was deleted successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be restored." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (scheduleService.RestoreTask(patientId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was restored successfully.";
                }
            }
            return Json(viewData);
        }


        #endregion
    }
}
