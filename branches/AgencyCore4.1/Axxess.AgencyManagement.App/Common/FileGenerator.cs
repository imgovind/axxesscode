﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net;
    using System.IO;
    using Axxess.Core.Extension;

    public static class FileGenerator
    {
        public static string RemoteDownload(string serverIP, string remoteDir, string file)
        {
            string result = string.Empty;
            try
            {
                if (serverIP.IsNotNullOrEmpty() && remoteDir.IsNotNullOrEmpty() && file.IsNotNullOrEmpty())
                {
                    string uri = string.Format("ftp://{0}/{1}/{2}", serverIP, remoteDir, file);
                    Uri serverUri = new Uri(uri);
                    if (serverUri.Scheme != Uri.UriSchemeFtp)
                    {
                        return result;
                    }
                    FtpWebRequest reqFTP;
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(serverUri);
                    reqFTP.Credentials = new NetworkCredential("AxxessAdmin", "29Eiyztygt");
                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.Proxy = null;
                    reqFTP.UsePassive = false;
                    FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    var readStream = new StreamReader(responseStream);
                    result = readStream.ReadToEnd();
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Download Error");
            }
            return result;
        }



    }
}
