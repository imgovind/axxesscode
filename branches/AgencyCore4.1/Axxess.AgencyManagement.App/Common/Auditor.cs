﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;

    public static class Auditor
    {
        private static readonly ILogRepository logRepository = Container.Resolve<ILogDataProvider>().LogRepository;

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask)
        {
            return Log(episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, string.Empty);
        }

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask, string description)
        {
            return Log(episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, description);
        }

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, ScheduleStatus status, DisciplineTasks disciplineTask, string description)
        {
            var log = new TaskLog
            {
                UserId = Current.UserId,
                Description = description,
                AgencyId = Current.AgencyId,
                UserName = Current.UserFullName,
                Action = action.ToString(),
                Status = (int)status,
                Date = DateTime.Now
            };

            return SaveAudit(episodeId, patientId, entityId, disciplineTask, log);
        }

        public static IList<string> Trail(Guid patientId, Guid entityId, Disciplines disciplineTask)
        {
            var trail = new List<string>();
            var audit = logRepository.GetTaskAudit(Current.AgencyId, patientId, entityId, (int)disciplineTask);

            if (audit != null)
            {
                var logs = audit.Log.ToObject<List<TaskLog>>();
                logs.ForEach(l =>
                {
                    trail.Add(l.ToString());
                });
            }

            return trail;
        }

        private static bool SaveAudit(Guid episodeId, Guid patientId, Guid entityId, DisciplineTasks disciplineTask, TaskLog log)
        {
            var result = false;
            var audit = logRepository.GetTaskAudit(Current.AgencyId, patientId, entityId, (int)disciplineTask);

            if (audit == null)
            {
                audit = new TaskAudit
                {
                    EntityId = entityId,
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    AgencyId = Current.AgencyId,
                    Log = new List<TaskLog> { log }.ToXml(),
                    DisciplineTaskId = (int)disciplineTask
                };

                result = logRepository.AddTaskAudit(audit);
            }
            else
            {
                var logs = audit.Log.ToObject<List<TaskLog>>();
                logs.Add(log);
                audit.Log = logs.ToXml();
                audit.EpisodeId = episodeId;
                result = logRepository.UpdateTaskAudit(audit);
            }

            return result;
        }

        public static bool AddGeneralLog(LogDomain domain, Guid domainId, string entityId, LogType logType, LogAction logAction, string description)
        {
            var audit = new AppAudit { AgencyId = Current.AgencyId, LogDomain = domain.ToString(), DomainId = domainId, EntityId = entityId, UserId = Current.UserId, LogType = logType.ToString(), Action = logAction.ToString(), Date = DateTime.Now, Description = description };
            return logRepository.AddGeneralAudit(audit);
        }
    }
}
