﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public static class AppList
    {
        private static IDictionary<Guid, IDictionary<Guid, string>> userCache;
        public static IDictionary<Guid, IDictionary<Guid, string>> UserCache
        {
            get
            {
                if (userCache == null)
                {
                    LoadAgencyUsers();
                }
                return userCache;
            }
        }

        private static void LoadAgencyUsers()
        {
            userCache = new Dictionary<Guid, IDictionary<Guid, string>>();

            IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
            if (dataProvider != null)
            {
                var users = dataProvider.AgencyRepository.GetUserNames();

                users.ForEach(u => {
                    if (userCache.ContainsKey(u.AgencyId))
                    {
                        var agencylist = userCache[u.AgencyId];
                        if (!agencylist.ContainsKey(u.UserId))
                        {
                            agencylist.Add(u.UserId, u.DisplayName);
                        }
                    }
                    else
                    {
                        var newAgencyList = new Dictionary<Guid, string>();
                        newAgencyList.Add(u.UserId, u.DisplayName);
                        userCache.Add(u.AgencyId, newAgencyList);
                    }
                });
            }
        }

        public static string GetUserDisplayName(Guid userId)
        {
            var agencyCache = UserCache[Current.AgencyId];

            if (agencyCache != null && agencyCache.Count > 0)
            {
                if (agencyCache.ContainsKey(userId))
                {
                    return agencyCache[userId];
                }
            }
            return string.Empty;
        }

        public static void Refresh()
        {
            userCache = null;
        }
    }
}
