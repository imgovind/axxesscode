﻿namespace Axxess.AgencyManagement.App.Exports
{
     using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Enums;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Domain;
   public class SubmittedBatchClaimsExport :BaseExporter
    {
        private IList<ClaimDataLean> batchClaims;
         private DateTime StartDate;
         private DateTime EndDate;
         private string ClaimType;
         public SubmittedBatchClaimsExport(IList<ClaimDataLean> batchClaims, DateTime StartDate, DateTime EndDate, string ClaimType)
            : base()
        {
            this.batchClaims = batchClaims;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.ClaimType = ClaimType;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Submitted Batch Claims";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("SubmittedBatchClaims");
            
            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Submitted Batch Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase() == "ALL" ? "ALL" : Enum.IsDefined(typeof(ClaimType), ClaimType) ? ((ClaimType)Enum.Parse(typeof(ClaimType), ClaimType)).GetDescription() : string.Empty));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Batch Id");
            headerRow.CreateCell(1).SetCellValue("Submission Date");
            headerRow.CreateCell(2).SetCellValue("# of claims");
            headerRow.CreateCell(3).SetCellValue("# of RAPs");
            headerRow.CreateCell(4).SetCellValue("# of Finals");
           
            if (this.batchClaims.Count > 0)
            {
                int i = 2;
                this.batchClaims.ForEach(claim =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(claim.Id);
                    if (claim.Created != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(1);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(claim.Created);
                    }
                    else
                    {
                        row.CreateCell(1).SetCellValue(string.Empty);
                    }
                    row.CreateCell(2).SetCellValue(claim.Count);
                    if(string.IsNullOrEmpty(claim.RAPCount))
                    {
                        row.CreateCell(3).SetCellValue("");
                    }
                    else
                    {
                        if (claim.RAPCount.IsInteger())
                        {
                            row.CreateCell(3).SetCellValue(claim.RAPCount.ToInteger());
                        }
                        else
                        {
                            row.CreateCell(3).SetCellValue(claim.RAPCount);
                        }
                    }
                    if (string.IsNullOrEmpty(claim.FinalCount))
                    {
                        row.CreateCell(4).SetCellValue("");
                    }
                    else
                    {
                        if (claim.FinalCount.IsInteger())
                        {
                            row.CreateCell(4).SetCellValue(claim.FinalCount.ToInteger());
                        }
                        else
                        {
                            row.CreateCell(4).SetCellValue(claim.FinalCount);
                        }
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Submitted Batch Claims: {0}", batchClaims.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5);
        }
    }
}
