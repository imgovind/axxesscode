﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Extensions;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class CommunicationNoteExporter : BaseExporter
    {
        private IList<CommunicationNote> communicationNotes;
        private DateTime StartDate;
        private DateTime EndDate;
        public CommunicationNoteExporter(IList<CommunicationNote> communicationNotes, DateTime StartDate,DateTime EndDate)
            : base()
        {
            this.communicationNotes = communicationNotes;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Communication Notes";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("CommunicationNotes");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Communication Notes");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Employee");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Status");
            int i = 2;
            if (this.communicationNotes.Count > 0)
            {
                this.communicationNotes.ForEach(note =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(note.DisplayName);
                    dataRow.CreateCell(1).SetCellValue(note.UserDisplayName);

                    if (note.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(note.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }   
                    dataRow.CreateCell(3).SetCellValue(note.StatusName);
                    i++;
                });
                var totalRow = sheet.CreateRow(i+2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Communication Notes: {0}", communicationNotes.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(4);
        }
    }
}
