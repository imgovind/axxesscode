﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Extensions;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class PayrollDetailExport : BaseExporter
    {
       private PayrollDetail payrollDetail;
       public PayrollDetailExport(PayrollDetail payrollDetail)
            : base()
        {
            this.payrollDetail = payrollDetail;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Detail";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Payroll Detail");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Detail");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", payrollDetail.StartDate.ToString("MM/dd/yyyy"), payrollDetail.EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Employee Name: {0}", payrollDetail.Name));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Visit Date");
            headerRow.CreateCell(3).SetCellValue("Time In");
            headerRow.CreateCell(4).SetCellValue("Time Out");
            headerRow.CreateCell(5).SetCellValue("Hours");
            headerRow.CreateCell(6).SetCellValue("Pay Rate");
            headerRow.CreateCell(7).SetCellValue("Miles");
            headerRow.CreateCell(8).SetCellValue("Mileage Rate");
            headerRow.CreateCell(9).SetCellValue("Visit Pay");
            headerRow.CreateCell(10).SetCellValue("Surcharge");
            headerRow.CreateCell(11).SetCellValue("Total Pay");

            if (this.payrollDetail != null && this.payrollDetail.Visits!=null && this.payrollDetail.Visits.Count > 0)
            {
                int i = 2;
                var totalTime = 0;
                this.payrollDetail.Visits.ForEach(visit =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(visit.PatientName);
                    row.CreateCell(1).SetCellValue(visit.TaskName);
                    DateTime scheduleDate = DateTime.Parse(visit.ScheduleDate);
                    if (scheduleDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(scheduleDate);
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    row.CreateCell(3).SetCellValue(visit.TimeIn.IsNotNullOrEmpty() ? visit.TimeIn : string.Empty);
                    row.CreateCell(4).SetCellValue(visit.TimeOut.IsNotNullOrEmpty() ? visit.TimeOut : string.Empty);
                    row.CreateCell(5).SetCellValue(string.Format("{0:#0.00}", (double)visit.MinSpent / 60) );
                    row.CreateCell(6).SetCellValue(visit.VisitRate);
                    row.CreateCell(7).SetCellValue(visit.AssociatedMileage);
                    row.CreateCell(8).SetCellValue(visit.MileageRate);
                    row.CreateCell(9).SetCellValue(visit.VisitPayment);
                    row.CreateCell(10).SetCellValue(visit.Surcharge);
                    row.CreateCell(11).SetCellValue(visit.TotalPayment);
                    
                    i++;
                    totalTime += visit.MinSpent;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Payroll Detail: {0}", this.payrollDetail.Visits.Count));
                totalRow.CreateCell(1).SetCellValue(string.Format("Total Time for this employee: {0}", string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)));
            }
            workBook.FinishWritingToExcelSpreadsheet(8);
        }
    }
}
