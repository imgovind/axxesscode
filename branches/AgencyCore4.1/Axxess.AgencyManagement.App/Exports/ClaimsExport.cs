﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Enums;
    public class ClaimsExport : BaseExporter
    {
        private List<Bill> bills;
        public ClaimsExport(List<Bill> bills)
            : base()
        {
            this.bills = bills;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims Summary";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            ISheet sheet = base.workBook.CreateSheet("Claims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var rowNum = 2;
            var sectionHeaders = new List<string>();
            if (bills != null && bills.Count > 0)
            {
                foreach (var bill in bills)
                {
                    var headerStarterRow = sheet.CreateRow(rowNum - 1);
                    sectionHeaders.Add(string.Format("{0}(s)", bill.ClaimType.GetDescription()));
                    headerStarterRow.CreateCell(0).SetCellValue(sectionHeaders[sectionHeaders.Count - 1]);

                    var titleRow2 = sheet.CreateRow(rowNum);
                    titleRow2.CreateCell(0).SetCellValue(bill.BranchName + "|" + bill.InsuranceName);
                    rowNum = ContentBuliderHelper(sheet, bill, rowNum + 1);
                    rowNum++;
                }
            }
            
            workBook.FinishWritingToExcelSpreadsheetWithMultipleHeaders(9, sectionHeaders, 3);

        }

        private int ContentBuliderHelper(ISheet sheet, Bill bill, int rowNum)
        {
            int i = rowNum + 1;
            if (bill.ClaimType == ClaimTypeSubCategory.RAP)
            {
                IRow headerRow = sheet.CreateRow(rowNum);
                headerRow.CreateCell(0).SetCellValue("MRN");
                headerRow.CreateCell(1).SetCellValue("Patient");
                headerRow.CreateCell(2).SetCellValue("Case Manager");
                headerRow.CreateCell(3).SetCellValue("Clinician");
                headerRow.CreateCell(4).SetCellValue("Episode Date");
                headerRow.CreateCell(5).SetCellValue("OASIS");
                headerRow.CreateCell(6).SetCellValue("Billable Visit");
                headerRow.CreateCell(7).SetCellValue("Verfied");
                if (bill.Claims != null && bill.Claims.Count > 0)
                {
                    bill.Claims.ForEach(claim =>
                    {
                        var rap = (RapBill)claim;
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                        row.CreateCell(1).SetCellValue(string.Format("{0}, {1}", rap.LastName, rap.FirstName));
                        row.CreateCell(2).SetCellValue(rap.CaseManager);
                        row.CreateCell(3).SetCellValue(rap.Clinician);
                        row.CreateCell(4).SetCellValue(rap.DateRange);
                        row.CreateCell(5).SetCellValue(rap.IsOasisComplete ? "X" : "");
                        row.CreateCell(6).SetCellValue(rap.IsFirstBillableVisit ? "X" : "");
                        row.CreateCell(7).SetCellValue(rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "X" : "");
                        i++;
                    });
                }
                rowNum = i;
            }
            else if (bill.ClaimType == ClaimTypeSubCategory.Final)
            {
                IRow headerRow = sheet.CreateRow(rowNum);
                headerRow.CreateCell(0).SetCellValue("MRN");
                headerRow.CreateCell(1).SetCellValue("Patient");
                headerRow.CreateCell(2).SetCellValue("Case Manager");
                headerRow.CreateCell(3).SetCellValue("Clinician");
                headerRow.CreateCell(4).SetCellValue("Episode Date");
                headerRow.CreateCell(5).SetCellValue("RAP");
                headerRow.CreateCell(6).SetCellValue("Visit");
                headerRow.CreateCell(7).SetCellValue("Order");
                headerRow.CreateCell(8).SetCellValue("Verfied");
                //int i = rowNum+1;
                if (bill.Claims != null && bill.Claims.Count > 0)
                {
                    bill.Claims.ForEach(claim =>
                    {
                        var final = (FinalBill)claim;
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                        row.CreateCell(1).SetCellValue(string.Format("{0}, {1}", final.LastName, final.FirstName));
                        row.CreateCell(2).SetCellValue(final.CaseManager);
                        row.CreateCell(3).SetCellValue(final.Clinician);
                        row.CreateCell(4).SetCellValue(final.DateRange);
                        row.CreateCell(5).SetCellValue(final.IsRapGenerated ? "X" : "");
                        row.CreateCell(6).SetCellValue(final.AreVisitsComplete ? "X" : "");
                        row.CreateCell(7).SetCellValue(final.AreOrdersComplete ? "X" : "");
                        row.CreateCell(8).SetCellValue(final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "X" : "");
                        i++;
                    });
                }
                rowNum = i;
            }
            else if (bill.ClaimType == ClaimTypeSubCategory.ManagedCare)
            {
                IRow headerRow = sheet.CreateRow(rowNum);
                headerRow.CreateCell(0).SetCellValue("MRN");
                headerRow.CreateCell(1).SetCellValue("Patient Name");
                headerRow.CreateCell(2).SetCellValue("Case Manager");
                headerRow.CreateCell(3).SetCellValue("Clinician");
                headerRow.CreateCell(4).SetCellValue("Episode Date");
                headerRow.CreateCell(5).SetCellValue("Detail");
                headerRow.CreateCell(6).SetCellValue("Visit");
                headerRow.CreateCell(7).SetCellValue("Supply");
                headerRow.CreateCell(8).SetCellValue("Verfied");
                //int i = rowNum+1;
                if (bill.Claims != null && bill.Claims.Count > 0)
                {
                    bill.Claims.ForEach(claim =>
                    {
                        var managedClaim = (ManagedBill)claim;
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(managedClaim.PatientIdNumber);
                        row.CreateCell(1).SetCellValue(string.Format("{0}, {1}", managedClaim.LastName, managedClaim.FirstName));
                        row.CreateCell(2).SetCellValue(managedClaim.CaseManager);
                        row.CreateCell(3).SetCellValue(managedClaim.Clinician);
                        row.CreateCell(4).SetCellValue(managedClaim.DateRange);
                        row.CreateCell(5).SetCellValue(managedClaim.IsInfoVerified ? "X" : "");
                        row.CreateCell(6).SetCellValue(managedClaim.IsVisitVerified ? "X" : "");
                        row.CreateCell(7).SetCellValue(managedClaim.IsSupplyVerified ? "X" : "");
                        row.CreateCell(8).SetCellValue(managedClaim.IsVisitVerified && managedClaim.IsSupplyVerified && managedClaim.IsInfoVerified ? "X" : "");
                        i++;
                    });
                }
                rowNum = i;
            }
            return rowNum;
        }
    }
}