﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public class InsuranceEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly InsuranceEngine instance = new InsuranceEngine();
        }

        #endregion

        #region Public Instance

        public static InsuranceEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private static object syncLock = new object();
        private static SafeDictionary<Guid, IDictionary<int, InsuranceCache>> cache = new SafeDictionary<Guid, IDictionary<int, InsuranceCache>>();
        private static readonly IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
        private static readonly ILookUpDataProvider lookUpdataProvider = Container.Resolve<ILookUpDataProvider>();

        #endregion

        #region Private Constructor / Methods

        private InsuranceEngine()
        {
        }

        private void Load(Guid agencyId)
        {

            var insurances = new List<InsuranceCache>();
            var standardInsurances = lookUpdataProvider.LookUpRepository.Insurances().Select(i => new InsuranceCache { Id = i.Id, Name = i.Name , PayorType=(int) PayerTypes.MedicareTraditional}).ToList();
            if (standardInsurances != null && standardInsurances.Count > 0)
            {
                insurances.AddRange(standardInsurances);
            }
            var agencyInsurances = dataProvider.AgencyRepository.GetInsurances(agencyId).Select(i => new InsuranceCache { Id = i.Id, Name = i.Name ,PayorType=i.PayorType}).ToList();
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                insurances.AddRange(agencyInsurances);
            }

            if (insurances != null && insurances.Count > 0)
            {
                var newInsuranceList = new Dictionary<int, InsuranceCache>();
                insurances.ForEach(insurance =>
                {
                    newInsuranceList.Add(insurance.Id, insurance);
                });

                if (cache.ContainsKey(agencyId))
                {
                    cache[agencyId] = newInsuranceList;
                }
                else
                {
                    if (!cache.ContainsKey(agencyId))
                    {
                        cache.Add(agencyId, newInsuranceList);
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        public InsuranceCache Get(int Id, Guid agencyId)
        {
            if (cache.Count > 0 && cache.ContainsKey(agencyId))
            { 
                
                var insuranceCache = cache[agencyId];

                if (insuranceCache != null && insuranceCache.Count > 0)
                {
                    if (insuranceCache.ContainsKey(Id))
                    {
                        return insuranceCache[Id];
                    }
                }
                else
                {
                    this.Load(agencyId);

                    if (cache.Count > 0 && cache.ContainsKey(agencyId))
                    {
                        var newInsuranceCache = cache[agencyId];

                        if (newInsuranceCache != null && newInsuranceCache.Count > 0)
                        {
                            if (newInsuranceCache.ContainsKey(Id))
                            {
                                return newInsuranceCache[Id];
                            }
                        }
                    }
                }
            }
            else
            {
                this.Load(agencyId);

                if (cache.Count > 0 && cache.ContainsKey(agencyId))
                {

                    var newInsuranceCache = cache[agencyId];

                    if (newInsuranceCache != null && newInsuranceCache.Count > 0)
                    {
                        if (newInsuranceCache.ContainsKey(Id))
                        {
                            return newInsuranceCache[Id];
                        }
                    }
                }
            }
            return null;
        }

        public void Refresh(Guid agencyId)
        {
            Load(agencyId);
        }

        public SafeDictionary<Guid, IDictionary<int, InsuranceCache>> Cache
        {
            get { return cache; }
        }

        #endregion
    }
}
