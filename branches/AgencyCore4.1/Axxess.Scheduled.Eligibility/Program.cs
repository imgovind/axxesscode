﻿using System;
using System.Text;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.AgencyManagement.Domain;

using Axxess.Scheduled.Eligibility.Data;
using Axxess.Scheduled.Eligibility.Model;
using Axxess.Scheduled.Eligibility.Service;
using Axxess.Scheduled.Eligibility.Helpers;

namespace Axxess.Scheduled.Eligibility
{
    class Program
    {
        const int MAX_THREADS = 10;
        static Queue<PatientEligibilityData> itemList = Database.GetPatients();
        static List<UserData> recipients = Database.GetNotificationRecipients();
        static List<User> userMessages = Database.GetUserMessages();
        static List<PatientMessagesPair> PatientMessages = new List<PatientMessagesPair>();

        static void Main(string[] args)
        {
            Bootstrapper.Run();
            Run();

            Console.Read();
        }

        private static void Run()
        {
            int counter = 0;
            var threadList = new List<Thread>();
            int takeAmount = (int)Math.Ceiling(itemList.Count / (double)MAX_THREADS);
            do
            {
                var workList = itemList.Dequeue(takeAmount).ToList();
                var thread = new Thread(() => Work(workList));
                thread.Name = string.Format("Thread{0}", counter);
                threadList.Add(thread);
                counter++;
            } while (counter < MAX_THREADS);

            threadList.ForEach(thread =>
            {
                thread.Start();
            });

            threadList.ForEach(thread =>
            {
                thread.Join();
            });

            //SendMessages(PatientMessages);
        }

        private static void Work(List<PatientEligibilityData> list)
        {
           
            list.ForEach(patient =>
            {
                Console.WriteLine("Patient: {0} {1} working on {2}", patient.FirstName, patient.LastName, Thread.CurrentThread.Name);
                var jsonResult = MedicareService.Verify(patient);
                if (jsonResult.IsNotNullOrEmpty())
                {
                    var result = jsonResult.ToEligibilityResult();
                    PatientEligibilityResult previousResult = patient.PreviousResult.IsNotNullOrEmpty() ? patient.PreviousResult.ToEligibilityResult() : null;
                    
                    List<string> messages = new List<string>();
                    if (result != null)
                    {
                        if (result.Request_Validation != null && result.Request_Validation.success.IsNotNullOrEmpty() && result.Request_Validation.success.IsEqual("yes"))
                        {
                            Console.WriteLine("Rejection Reason: {0}", result.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? result.Request_Validation.reject_reason_code : string.Empty);
                            Console.WriteLine("Follow up Action Code: {0}", result.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? result.Request_Validation.follow_up_action_code : string.Empty);
                            messages.Add(result.Request_Validation.reject_reason_code);
                        }
                        else
                        {
                            if (result.Subscriber != null && result.Subscriber.success.IsNotNullOrEmpty() && result.Subscriber.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Identification Code: {0}", result.Subscriber.identification_code.IsNotNullOrEmpty() ? result.Subscriber.identification_code : string.Empty);
                                Console.WriteLine("Date of Birth: {0}", result.Subscriber.date.IsNotNullOrEmpty() ? result.Subscriber.date.ToDateTimeString() : string.Empty);
                                Console.WriteLine("Subscriber Name: {0}, {1} {2}", result.Subscriber.last_name.IsNotNullOrEmpty() ? result.Subscriber.last_name : string.Empty, result.Subscriber.first_name.IsNotNullOrEmpty() ? result.Subscriber.first_name : string.Empty, result.Subscriber.middle_name.IsNotNullOrEmpty() ? result.Subscriber.middle_name : string.Empty);
                                Console.WriteLine("Gender: {0}", result.Subscriber.gender.IsNotNullOrEmpty() ? result.Subscriber.gender : string.Empty);
                            }
                            if (result.Medicare_Part_A != null && result.Medicare_Part_A.success.IsNotNullOrEmpty() && result.Medicare_Part_A.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Medicare Part A: {0}", result.Medicare_Part_A.date.IsNotNullOrEmpty() ? result.Medicare_Part_A.date.ToDateTimeString() + " - Current" : string.Empty);
                            }
                            if (result.Medicare_Part_B != null && result.Medicare_Part_B.success.IsNotNullOrEmpty() && result.Medicare_Part_B.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Medicare Part B: {0}", result.Medicare_Part_B.date.IsNotNullOrEmpty() ? result.Medicare_Part_B.date.ToDateTimeString() + " - Current" : string.Empty);
                            }
                            
                            if (result.Episode != null && result.Episode.name.IsNotNullOrEmpty())
                            {
                                Console.WriteLine("Last Home Care Episode Payer: {0}", result.Episode.name.IsNotNullOrEmpty() ? result.Episode.name : string.Empty);
                                Console.WriteLine("Last Home Care Episode NPI#: {0}", result.Episode.reference_id.IsNotNullOrEmpty() ? result.Episode.reference_id : string.Empty);
                                Console.WriteLine("Last Home Care Episode Episode Date Range: {0} - {1}", result.Episode.period_start.IsNotNullOrEmpty() ? result.Episode.period_start.ToDateTimeString() : string.Empty,
                                    result.Episode.period_end.IsNotNullOrEmpty() ? result.Episode.period_end.ToDateTimeString() : string.Empty);

                                if (result.Episode.reference_id.IsNotNullOrEmpty() && result.Episode.reference_id != patient.NPI)
                                {
                                    messages.Add(string.Format("{0} is under a different NPI#({1}) than your agency({2})", patient.DisplayNameWithMi, result.Episode.reference_id, patient.NPI));
                                }
                            }
                            bool isMedicare = false;
                            if (result.Health_Benefit_Plan_Coverage != null && result.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty())
                            {
                                if (result.Health_Benefit_Plan_Coverage.success.IsEqual("yes"))
                                {
                                    Console.WriteLine("Health Benefit Plan Coverage: {0}", result.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.name : string.Empty);
                                    Console.WriteLine("Insurance Type: {0}", result.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.insurance_type : string.Empty);
                                    Console.WriteLine("Reference Id Qualifier: {0}", result.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty);
                                    Console.WriteLine("Address: {0}", result.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.address1 : string.Empty);
                                    Console.WriteLine("Phone Number: {0}", result.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.phone : string.Empty);
                                    Console.WriteLine("Health Benefit Plan Payer: {0}", result.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.payer : string.Empty);
                                    Console.WriteLine("Reference Id: {0}", result.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.reference_id : string.Empty);
                                    Console.WriteLine("Date: {0}", result.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.date.ToDateTimeString() : string.Empty);
                                    Console.WriteLine("City: {0}", result.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.city : string.Empty);
                                    Console.WriteLine("State: {0}", result.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.state : string.Empty);
                                }
                                else
                                {
                                    Console.WriteLine("Primary Payer: Medicare");
                                    isMedicare = true;
                                }

                                if (previousResult != null)
                                {
                                    if (previousResult.Health_Benefit_Plan_Coverage != null
                                        && !previousResult.Health_Benefit_Plan_Coverage.success.IsEqual(result.Health_Benefit_Plan_Coverage.success))
                                    {
                                        if (isMedicare)
                                        {
                                            messages.Add(string.Format("The primary payer of {0} has changed to Medicare", patient.DisplayNameWithMi));
                                        }
                                        else
                                        {
                                            messages.Add(string.Format("The primary payer of {0} has changed to {1}", patient.DisplayNameWithMi, result.Health_Benefit_Plan_Coverage.name));
                                        }
                                    }
                                }
                            }
                        }
                        if (MedicareService.Insert(patient, jsonResult))
                        {
                            Console.WriteLine("Result Saved into the Database.");
                        }
                        if (messages.Count > 0)
                        {
                            PatientMessageData p = new PatientMessageData()
                            {
                                AgencyId = patient.AgencyId,
                                FirstName = patient.FirstName,
                                LastName = patient.LastName,
                                MiddleInitial = patient.MiddleInitial,
                                PatientIdNumber = patient.PatientIdNumber,
                                MedicareNumber = patient.MedicareNumber,
                                InsuranceName = result.Health_Benefit_Plan_Coverage != null && result.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.name : result.Episode != null ? result.Episode.name : "",
                                State = result.Health_Benefit_Plan_Coverage.state,
                                City = result.Health_Benefit_Plan_Coverage.city,
                                AddressLineOne = result.Health_Benefit_Plan_Coverage.address1,
                                StartDate = result.Episode.period_start,
                                EndDate = result.Episode.period_end,
                                PlanNumber = result.Health_Benefit_Plan_Coverage.reference_id,
                                Id = patient.Id
                            };
                            PatientMessages.Add(new PatientMessagesPair(p, messages));
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Result.");
                    }
                }
                else
                {
                    Console.WriteLine("No Result.");
                }
                Console.WriteLine("========================================================================");
            });
            
        }

        public static void SendMessages(List<PatientMessagesPair> messages)
        {
            foreach (var r in recipients)
            {
                var userEmails = r.Emails.Split(';');
                var userFirstNames = r.FirstNames.Split(';');
                for (int count = 0; count < userEmails.Length; count++)
                {
                    var parameters = new string[4];
                    parameters[0] = "recipientfirstname";
                    parameters[1] = userFirstNames[count];
                    parameters[2] = "senderfullname";
                    parameters[3] = "Axxess Technology Solutions";
                    var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                    Notify.User(CoreSettings.NoReplyEmail, userEmails[count], "Axxess Technology Solutions sent you a message.", bodyText);
                }
                string messageBody = "";
                string report = "";
                var agencyUsers = userMessages.Where(um => um.AgencyId == r.AgencyId).ToList();
                if (messages.Count > 0)
                {
                    var agencyMessages = messages.Where(m => m.Patient.AgencyId == r.AgencyId).ToList();
                    if(agencyMessages.Count > 0)
                    {
                        report = BuildReport(agencyMessages);
                        messageBody = BuildMessage(agencyMessages);
                    }
                    else
                    {
                        report = BuildEmptyReport();
                        messageBody = BuildEmptyMessage();
                    }
                }
                else
                {
                    report = BuildEmptyReport();
                    messageBody = BuildEmptyMessage();
                }
                Database.InsertMedicareEligibilityReport(r.AgencyId, report);
                agencyUsers.ForEach(au =>
                {
                    Message message = new Message();
                    message.Id = Guid.NewGuid();
                    message.Subject = "Axxess - Medicare Elegibility Summary";
                    message.Created = DateTime.Now;
                    message.Body = messageBody;
                    Database.InsertSystemMessage(message);  
                    Database.AddMessage(au, message.Id);
                });
            }
            Console.WriteLine("========================================================================");
            Console.WriteLine("Finished Sending Messages");
            Console.WriteLine("========================================================================");
        }

        private static string BuildMessage(List<PatientMessagesPair> agencyMessages)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"content\">");
            sb.Append("<h2 class=\"\">Medicare Eligibility Summary Report</h2>");


            sb.Append("<h3 class=\"\">Errors</h3>");
            sb.Append("<table class=\"noborders\">");
            sb.Append(HtmlHelper.BuildTableHeaderRow("MRN", "Name", "Medicare Number", "Error"));
            foreach (var am in agencyMessages)
            {
                sb.Append(HtmlHelper.BuildTableRow(am.Patient.PatientIdNumber, am.Patient.DisplayNameWithMi, am.Patient.MedicareNumber, am.Messages.ToArray().Join(";")));
            }
            sb.Append("</table>");

            sb.Append("<h3 class=\"\">Primary Payors</h3>");
            sb.Append("<table class=\"noborders\">");
            sb.Append(HtmlHelper.BuildTableHeaderRow("MRN", "Name", "Medicare Number", "Payor", "Plan Number", "Start Date", "End Date"));
            foreach (var am in agencyMessages)
            {
                sb.Append(HtmlHelper.BuildTableRow(am.Patient.PatientIdNumber, am.Patient.DisplayNameWithMi,
                    am.Patient.MedicareNumber, am.Patient.Payor, am.Patient.PlanNumber,
                    am.Patient.StartDate.IsNotNullOrEmpty() && am.Patient.StartDate.IsValidPHPDate() ? am.Patient.StartDate.ToDateTimePHP().ToShortDateString() : "",
                    am.Patient.EndDate.IsNotNullOrEmpty() && am.Patient.EndDate.IsValidPHPDate() ? am.Patient.EndDate.ToDateTimePHP().ToShortDateString() : ""));
            }
            sb.Append("</table>");
            sb.Append("</div>");
            return sb.ToString();
        }

        private static string BuildEmptyMessage()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"content\">");
            sb.Append("<h2 class=\"\">Medicare Eligibility Summary Report</h2>");
            sb.Append("<p>No changes were found.</p>");
            sb.Append("</div>");
            return sb.ToString();
        }

        private static string BuildReport(List<PatientMessagesPair> agencyMessages)
        {
            MedicareEligibilitySummaryData data = new MedicareEligibilitySummaryData();
            List<MedicareEligibilityError> errors = new List<MedicareEligibilityError>();
            foreach (var am in agencyMessages)
            {
                errors.Add(new MedicareEligibilityError()
                {
                    Error = am.Messages.ToArray().Join(";"),
                    FirstName = am.Patient.FirstName,
                    LastName = am.Patient.LastName,
                    MiddleInitial = am.Patient.MiddleInitial,
                    PatientIdNumber = am.Patient.PatientIdNumber,
                    MedicareNumber = am.Patient.MedicareNumber
                });
            }
            List<MedicareEligibilityPayor> payors = new List<MedicareEligibilityPayor>();
            foreach (var am in agencyMessages)
            {
                if (am.Patient != null)
                {
                    payors.Add(new MedicareEligibilityPayor()
                    {
                        FirstName = am.Patient.FirstName,
                        LastName = am.Patient.LastName,
                        MiddleInitial = am.Patient.MiddleInitial,
                        PatientIdNumber = am.Patient.PatientIdNumber,
                        MedicareNumber = am.Patient.MedicareNumber,
                        PlanNumber = am.Patient.PlanNumber,
                        Payor = am.Patient.InsuranceName,
                        AddressLineOne = am.Patient.AddressLineOne,
                        AddressLineTwo = am.Patient.AddressLineTwo,
                        StartDate = am.Patient.StartDate.IsNotNullOrEmpty() && am.Patient.StartDate.IsValidPHPDate() ? am.Patient.StartDate.ToDateTimePHP().ToShortDateString() : "",
                        EndDate = am.Patient.EndDate.IsNotNullOrEmpty() && am.Patient.EndDate.IsValidPHPDate() ? am.Patient.EndDate.ToDateTimePHP().ToShortDateString() : "",
                    });
                }
            }
            data.Errors = errors;
            data.Payors = payors;
            return data.ToXml();
        }

        private static string BuildEmptyReport()
        {
            MedicareEligibilitySummaryData data = new MedicareEligibilitySummaryData();
            data.Errors = new List<MedicareEligibilityError>();
            data.Payors = new List<MedicareEligibilityPayor>();
            return data.ToXml();
        }
    }
}
