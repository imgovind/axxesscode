﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using StructureMap;
using Axxess.Core;
using System.Web.Mvc;

namespace Axxess.Scheduled.Eligibility
{
    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
            });
        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Container.ResolveAll<IStartupTask>().ForEach(t => t.Execute());
        }
    }
}
