﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Scheduled.Eligibility.Model;
using Axxess.Core.Extension;

namespace Axxess.Scheduled.Eligibility.Helpers
{
    public static class HtmlHelper
    {
        public static string BuildTableHeaderRow(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<tr>");
            for (int i = 0; i < parameters.Length; i++)
            {
                builder.Append("<td class=\"col" + i + "\"><strong>").Append(parameters[i]).Append("</strong></td>");
            }
            builder.Append("</tr>");
            return builder.ToString();
        }

        public static string BuildTableRow(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<tr>");
            for (int i = 0; i < parameters.Length; i++)
            {
                builder.Append("<td>").Append(parameters[i]).Append("</td>");
            }
            builder.Append("</tr>");
            return builder.ToString();
        }

        public static string BuildTableRowWithColClass(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<tr>");
            for (int i = 0; i < parameters.Length; i++)
            {
                builder.Append("<td class=\"col" + i + "\">").Append(parameters[i]).Append("</td>");
            }
            builder.Append("</tr>");
            return builder.ToString();
        }

        public static string BuildTableWithOneRow(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table>");
            builder.Append(BuildTableRow(parameters));
            builder.Append("</table>");
            return builder.ToString();
        }

        public static string BuildTableWithOneRowWithColClass(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table>");
            builder.Append(BuildTableRowWithColClass(parameters));
            builder.Append("</table>");
            return builder.ToString();
        }

        public static string BuildTableWithOneHeaderRow(params string[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table>");
            builder.Append(BuildTableHeaderRow(parameters));
            builder.Append("</table>");
            return builder.ToString();
        }

        public static string BuildSectionHeader(string parameter)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table>");
            builder.Append(BuildTableRow(parameter));
            builder.Append("</table>");
            return builder.ToString();
        }
    }
}
