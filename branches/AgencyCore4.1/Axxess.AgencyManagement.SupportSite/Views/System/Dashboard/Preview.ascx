﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul id="widget-column-1" class="widgets">
    <li class="widget" id="intro-widget">
        <div class="widget-head"><h5>Hello,&nbsp;<%= Current.DisplayName %>!</h5></div>
        <div class="widget-content"></div>
    </li>
</ul>