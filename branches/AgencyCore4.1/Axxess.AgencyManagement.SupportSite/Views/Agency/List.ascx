﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var actionUrlWidth = 230;
   var actionUrl = "<a href=\"javascript:void(0);\" onclick=\"Agency.Templates('<#=Id#>');\">Templates</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Physicians('<#=Id#>');\">Physicians</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Notes('<#=Id#>');\">Notes</a>";
   if (Current.IsAxxessAdmin) {
       actionUrlWidth = 390;
       actionUrl = "<a href=\"javascript:void(0);\" onclick=\"Agency.Edit('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Locations('<#=Id#>');\">Locations</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Templates('<#=Id#>');\">Templates</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Supplies('<#=Id#>');\">Supplies</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Physicians('<#=Id#>');\">Physicians</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Notes('<#=Id#>');\">Notes</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.Delete('<#=Id#>');\"><#=ActionText#></a>";
   }
 %>
<% using (Html.BeginForm("Agencies", "Export", FormMethod.Post)) { %>
    <div class="wrapper">
        <%= Html.Telerik().Grid<AgencyLite>().Name("List_Agencies").ToolBar(commnds => commnds.Custom()).Columns(columns => {
        columns.Bound(a => a.Name).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Agency.Users('<#=Id#>');\"><#=Name#></a>").Title("Name").Sortable(true);
        columns.Bound(a => a.ContactPersonDisplayName).ClientTemplate("<a href=\"mailto:<#=ContactPersonEmail#>\"><#=ContactPersonDisplayName#></a>").Title("Contact Person").Sortable(true);
        columns.Bound(a => a.ContactPersonPhoneFormatted).Title("Contact Phone").Width(120).Sortable(false);
        columns.Bound(a => a.Location).Title("Location").Sortable(false).Width(150);
        columns.Bound(a => a.DateSortable).Title("Created").Sortable(true).Width(105);
        columns.Bound(a => a.Id).ClientTemplate(actionUrl).Title("Action").Sortable(false).Width(actionUrlWidth);
        }).ClientEvents(c => c.OnRowDataBound("Agency.ListRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Agency")).Selectable().Sortable().Scrollable(scrolling => scrolling.Enabled(true))
        %>
    </div>
<% } %>
<script type="text/javascript">
    $("#List_Agencies .t-grid-toolbar").html("");
    <% if (Current.IsAxxessAdmin) { %>
    $("#List_Agencies .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22acore.open('newagency'); return false;%22%3ENew Agency%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E  %3Cdiv class=%22float-left%22%3E%3Cspan class=%22redrow%22%3ERed - TLC%3C/span%3E | %3Cspan class=%22bluerow%22%3EBlue - Over 90 days%3C/span%3E | %3Cspan class=%22blackrow%22%3EBlack - Veterans%3C/span%3E%3C/div%3E"));
    <% } else { %>
    $("#List_Agencies .t-grid-toolbar").append(unescape("%3Cdiv class=%22float-left%22%3E%3Cspan class=%22redrow%22%3ERed - TLC%3C/span%3E | %3Cspan class=%22bluerow%22%3EBlue - Over 90 days%3C/span%3E | %3Cspan class=%22blackrow%22%3EBlack - Veterans%3C/span%3E%3C/div%3E"));
    <% } %>
    $("#List_Agencies .t-grid-toolbar").append(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3E%3Clabel for=%22Agency_FilterBox%22%3ESearch:&nbsp;%3C/label%3E%3Cinput type=%22text%22 id=%22Agency_FilterBox%22 /%3E%3C/div%3E%3C/div%3E"));
    <% if (Current.IsAxxessAdmin) { %>
    $("#List_Agencies .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    <% } %>
    $(".t-grid-content").css({ 'height': 'auto' });
    
    $("#Agency_FilterBox").keyup(function() {
        if ($(this).val()) {
            Agency.FilterTGrid($(this).val())
        } else {
            $("tr", "#List_Agencies .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#List_Agencies .t-grid-content").addClass("t-alt");
        }
    });
</script>