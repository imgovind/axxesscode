﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%= string.Format("<script type='text/javascript'>acore.renamewindow('Agency Users | {0}','listagencyusers');</script>", Model.Name)%>
<% var actionUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Agency.EditUser('<#=LoginId#>');\">Edit</a> | <a href='/Agency/Impersonate/{0}/<#=Id#>' target='_blank'>Login As</a> | <a href='javascript:void(0);' onclick=\"UserInterface.ResendActivationLink('<#=Id#>','{0}');\">Activate Link</a>", Model.Id.ToString()); %>
<div class="wrapper">
    <%= Html.Telerik().Grid<UserSelection>().Name("List_Agencies_Users").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.EmailAddress);
    columns.Bound(u => u.DisplayName).Title("Name");
    columns.Bound(u => u.Credential).Sortable(false);
    columns.Bound(u => u.IsLoginActive).Title("Login Active").Sortable(false);
    columns.Bound(u => u.LoginCreated).Title("Login Created").Sortable(false);
    columns.Bound(u => u.Id).Sortable(false).ClientTemplate(actionUrl).Title("Action").Width(180);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("UserList", "Agency", new { agencyId = Model.Id })).Pageable(paging => paging.PageSize(500)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Agencies_Users .t-grid-toolbar").html(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3E%3Clabel for=%22User_FilterBox%22%3ESearch:&nbsp;%3C/label%3E%3Cinput type=%22text%22 id=%22User_FilterBox%22 /%3E%3C/div%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
    $("#User_FilterBox").keyup(function() {
        if ($(this).val()) {
            FilterTGrid($(this).val())
            if ($("tr.match:even", "#List_Agencies_Users .t-grid-content").length) $("tr.match:first", "#List_Agencies_Users .t-grid-content").click();
        } else {
            $("tr", "#List_Agencies_Users .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#List_Agencies_Users .t-grid-content").addClass("t-alt");
            $("tr:first", "#List_Agencies_Users .t-grid-content").click();
        }
    });
    function FilterTGrid(text) {
        search = text.split(" ");
        $("tr", "#List_Agencies_Users .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#List_Agencies_Users .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#List_Agencies_Users .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#List_Agencies_Users .t-grid-content").addClass("t-alt");
    }
</script>