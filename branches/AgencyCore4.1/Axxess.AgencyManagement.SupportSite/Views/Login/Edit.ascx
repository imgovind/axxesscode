﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Axxess.Membership.Domain.Login>" %>
<% using (Html.BeginForm("Update", "Login", FormMethod.Post, new { @id = "editLoginForm" })) {%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Login_Id" }) %>
<div class="form-wrapper">
    <fieldset>
        <legend>Login Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Login_EmailAddress">E-mail Address:</label><div class="float-right"><span class="bigtext"><%= Model.EmailAddress %></span></div></div>
            <div class="row"><label for="Edit_Login_DisplayName">Display Name:</label><div class="float-right"><%=Html.TextBox("DisplayName", Model.DisplayName, new { @id = "Edit_Login_DisplayName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Login_IsAxxessAdmin">Is Axxess Admin?</label><div class="float-right"><%= Html.CheckBox("IsAxxessAdmin", Model.IsAxxessAdmin ? true : false, new { @id = "Edit_Login_IsAxxessAdmin" })%></div></div>
            <div class="row"><label for="Edit_Login_IsAxxessSupport">Is Axxess Support?</label><div class="float-right"><%= Html.CheckBox("IsAxxessSupport", Model.IsAxxessSupport ? true : false, new { @id = "Edit_Login_IsAxxessSupport" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editlogin');">Close</a></li>
    </ul></div>
</div>
<% } %>
