﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    class AxxessContentTable : PdfPTable {
        public AxxessContentTable(String[,] data, Font font, float fontSize) : base(data.GetLength(1)) {
            font.Size = fontSize;
            this.WidthPercentage = 100;
            for (int i = 0; i < data.GetLength(0); i++) {
                for (int j = 0; j < data.GetLength(1); j++) {
                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidth = 0;
                    cell.AddElement(new Paragraph(data[i, j], font));
                    this.AddCell(cell);
                }
            }
        }
        public AxxessContentTable(IElement[,] data, bool Split) : base(data.GetLength(1)) {
            if (!Split) {
                this.SplitLate = true;
                this.SplitRows = false;
            }
            this.WidthPercentage = 100;
            for (int i = 0; i < data.GetLength(0); i++) {
                for (int j = 0; j < data.GetLength(1); j++) {
                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidth = 0;
                    cell.Padding = 1;
                    cell.AddElement(data[i, j]);
                    this.AddCell(cell);
                }
            }
        }
    }
}