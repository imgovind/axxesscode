﻿namespace Axxess.Physician.App.iTextExtension {
    using System.Linq;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    class AxxessCell : PdfPCell {
        public AxxessCell() : base() {
            this.__construct(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 }, BaseColor.BLACK);
        }
        public AxxessCell(float[] padding) : base() {
            this.__construct(padding, new float[] { 0, 0, 0, 0 }, BaseColor.BLACK);
        }
        public AxxessCell(float[] padding, float[] borders) : base() {
            this.__construct(padding, borders, BaseColor.BLACK);
        }
        public AxxessCell(float[] padding, float[] borders, BaseColor borderColor) : base() {
            this.__construct(padding, borders, borderColor);
        }
        public AxxessCell(PdfPTable table) : base(table) {
            this.__construct(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 }, BaseColor.BLACK);
        }
        private void __construct(float[] padding, float[] borders, BaseColor borderColor) {
            this.BorderColor = borderColor;
            this.BorderWidthTop = borders.Count() > 0 ? borders[0] : 0;
            this.BorderWidthRight = borders.Count() > 1 ? borders[1] : 0;
            this.BorderWidthBottom = borders.Count() > 2 ? borders[2] : 0;
            this.BorderWidthLeft = borders.Count() > 3 ? borders[3] : 0;
            this.PaddingTop = padding.Count() > 0 ? padding[0] : 0;
            this.PaddingRight = padding.Count() > 1 ? padding[1] : 0;
            this.PaddingBottom = padding.Count() > 2 ? padding[2] : 0;
            this.PaddingLeft = padding.Count() > 3 ? padding[3] : 0;
        }
    }
}