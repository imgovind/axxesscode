﻿namespace Axxess.Physician.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;

    using Enums;
    using Services;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;
    using Axxess.AgencyManagement;
    using Axxess.OasisC.Domain;
    using Axxess.Physician.App.ViewData;
    using Axxess.AgencyManagement.App.iTextExtension;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrderController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly IAssessmentService assessmentService;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        public OrderController(IAgencyManagementDataProvider agencyManagementDataProvider, IPhysicianService physicianService, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.assessmentService = assessmentService;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Order Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ListHeader()
        {
            ViewData["GroupName"] = "EventDate";
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListView(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView();
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListGrid()
        {
            return View(new GridModel(physicianService.GetOrders()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewCPO()
        {
            
            return PartialView("CPO/New", null);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddCPO(CarePlanOversight cpo)
        {
            Check.Argument.IsNotNull(cpo, "cpo");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New care plan oversight could not be saved." };
            if (cpo.IsValid)
            {
                cpo.PhysicianLoginId = Current.LoginId;
                if (patientRepository.AddCPO(cpo))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Care plan oversight has been saved successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgencyPatientList(Guid agencyId)
        {
            var viewData = new List<AgencyPatientViewData>();
            if (agencyId!=null)
            {
                var patients = patientRepository.GetPatientByAgencyPhysician(agencyId, Current.LoginId);
                if (patients != null && patients.Count > 0)
                {
                    viewData = patients.Select(p => new AgencyPatientViewData { PatientId = p.Id, Name = string.Format("{0} {1}", p.FirstName, p.LastName) }).ToList();
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateCPO([Bind] CarePlanOversight cpo)
        {
            Check.Argument.IsNotNull(cpo, "cpo");

            var viewData = new JsonViewData { errorMessage = "Care plan oversight could not be updated. Please try again.", isSuccessful = false };

            if (cpo.IsValid)
            {
                cpo.PhysicianLoginId = Current.LoginId;
                if (physicianService.SaveCPO(cpo))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Care plan oversight log has been updated successfully.";
                }
            }
            else
            {
                viewData.errorMessage = cpo.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteCPO(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this oversight. Please try again." };
            if (patientRepository.DeleteCPO(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Care plan oversight was successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error happened in deleting the oversight.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, agencyId);
            if (order != null)
            {
                order.Agency = AgencyEngine.Get(order.AgencyId);
                order.Patient = patientRepository.Get(order.PatientId, order.AgencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, order.AgencyId);

                var episode = patientRepository.GetEpisodeById(order.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    var allergies = assessmentService.GetAllergies(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (allergies != null && allergies.Count > 0 && allergies.ContainsKey("485Allergies") && allergies["485Allergies"].Answer.IsNotNullOrEmpty() && allergies["485Allergies"].Answer == "Yes" && allergies.ContainsKey("485AllergiesDescription") && allergies["485AllergiesDescription"].Answer.IsNotNullOrEmpty()) order.Allergies = allergies["485AllergiesDescription"].Answer;
                    else order.Allergies = "NKA (Food/Drugs/Latex)";
                    var diagnosis = assessmentService.GetDiagnoses(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (diagnosis != null && diagnosis.Count > 0)
                    {
                        if (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisText = diagnosis["M1020PrimaryDiagnosis"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1020ICD9M") && diagnosis["M1020ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisCode = diagnosis["M1020ICD9M"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisText = diagnosis["M1022PrimaryDiagnosis1"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022ICD9M1") && diagnosis["M1022ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisCode = diagnosis["M1022ICD9M1"].Answer;
                        }
                    }
                }
                return View("Print/Physician", order);
            }
            return View("Print/Physician", new PhysicianOrder());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PhysicianPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var order = patientRepository.GetOrder(orderId, patientId, agencyId);
            if (order != null) {
                order.Agency = AgencyEngine.Get(order.AgencyId);
                order.Patient = patientRepository.Get(order.PatientId, order.AgencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, order.AgencyId);
                var episode = patientRepository.GetEpisodeById(order.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null) {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    var allergies = assessmentService.GetAllergies(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (allergies != null && allergies.Count > 0 && allergies.ContainsKey("485Allergies") && allergies["485Allergies"].Answer.IsNotNullOrEmpty() && allergies["485Allergies"].Answer == "Yes" && allergies.ContainsKey("485AllergiesDescription") && allergies["485AllergiesDescription"].Answer.IsNotNullOrEmpty()) order.Allergies = allergies["485AllergiesDescription"].Answer;
                    else order.Allergies = "NKA (Food/Drugs/Latex)";
                    var diagnosis = assessmentService.GetDiagnoses(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (diagnosis != null && diagnosis.Count > 0) {
                        if (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) order.PrimaryDiagnosisText = diagnosis["M1020PrimaryDiagnosis"].Answer;
                        if (diagnosis.ContainsKey("M1020ICD9M") && diagnosis["M1020ICD9M"].Answer.IsNotNullOrEmpty()) order.PrimaryDiagnosisCode = diagnosis["M1020ICD9M"].Answer;
                        if (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) order.SecondaryDiagnosisText = diagnosis["M1022PrimaryDiagnosis1"].Answer;
                        if (diagnosis.ContainsKey("M1022ICD9M1") && diagnosis["M1022ICD9M1"].Answer.IsNotNullOrEmpty()) order.SecondaryDiagnosisCode = diagnosis["M1022ICD9M1"].Answer;
                    }
                }
            }
            var doc = new PhysicianOrderPdf(order);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Order_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanofCareOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var planofCare = assessmentService.GetPlanofCare(agencyId, episodeId, patientId, orderId);
            if (planofCare != null && planofCare.Data.IsNotNullOrEmpty()) {
                var agency = agencyRepository.Get(planofCare.AgencyId);
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                var episode = patientRepository.GetEpisode(planofCare.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                if (episode != null) {
                    if (planofCare.AssessmentType.IsNotNullOrEmpty() && planofCare.AssessmentType.ToLower().Contains("recert")) {
                        planofCare.EpisodeEnd = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(60).ToShortDateString().ToZeroFilled() : string.Empty;
                        planofCare.EpisodeStart = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(1).ToShortDateString().ToZeroFilled() : string.Empty;
                    } else {
                        planofCare.EpisodeEnd = episode.EndDateFormatted;
                        planofCare.EpisodeStart = episode.StartDateFormatted;
                    }
                }
                if (!planofCare.PhysicianId.IsEmpty()) {
                    var physician = physicianRepository.Get(planofCare.PhysicianId, planofCare.AgencyId);
                    planofCare.PhysicianData = physician != null ? physician.ToXml() : string.Empty;
                } else {
                    if (planofCare.PhysicianData.IsNotNullOrEmpty()) {
                        var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty()) {
                            var physician = physicianRepository.Get(oldPhysician.Id, planofCare.AgencyId);
                            if (physician != null) planofCare.PhysicianData = physician.ToXml();
                        }
                    }
                }
                return View("Print/485", planofCare);
            }
            return View("Print/485", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PlanofCarePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var planofCare = assessmentService.GetPlanofCare(agencyId, episodeId, patientId, orderId);
            if (planofCare != null && planofCare.Data.IsNotNullOrEmpty()) {
                var agency = agencyRepository.Get(planofCare.AgencyId);
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                var episode = patientRepository.GetEpisode(planofCare.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                if (episode != null) {
                    if (planofCare.AssessmentType.IsNotNullOrEmpty() && planofCare.AssessmentType.ToLower().Contains("recert")) {
                        planofCare.EpisodeEnd = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(60).ToShortDateString().ToZeroFilled() : string.Empty;
                        planofCare.EpisodeStart = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(1).ToShortDateString().ToZeroFilled() : string.Empty;
                    } else {
                        planofCare.EpisodeEnd = episode.EndDateFormatted;
                        planofCare.EpisodeStart = episode.StartDateFormatted;
                    }
                }
                if (!planofCare.PhysicianId.IsEmpty()) {
                    var physician = physicianRepository.Get(planofCare.PhysicianId, planofCare.AgencyId);
                    planofCare.PhysicianData = physician != null ? physician.ToXml() : string.Empty;
                } else {
                    if (planofCare.PhysicianData.IsNotNullOrEmpty()) {
                        var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty()) {
                            var physician = physicianRepository.Get(oldPhysician.Id, planofCare.AgencyId);
                            if (physician != null) planofCare.PhysicianData = physician.ToXml();
                        }
                    }
                }
            } else planofCare = new PlanofCare();
            var doc = new PlanOfCarePdf(planofCare, agencyId);
            var stream = doc.GetPlanOfCareStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=HCFA-485_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CarePlanOversights(Guid id)
        {
            var cpo = physicianRepository.GetCPOById(id);
            if (cpo == null)
            {
                cpo = new CarePlanOversight();
            }
            return View("CPO/Edit", cpo);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid id)
        {
            var order = patientRepository.GetFaceToFaceEncounter(id, patientId, agencyId);
            if (order != null)
            {
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(order.PatientId, agencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, agencyId);
                var episode = patientRepository.GetEpisodeById(agencyId, order.EpisodeId, order.PatientId);
                order.EpisodeEndDate = episode.EndDateFormatted;
                order.EpisodeStartDate = episode.StartDateFormatted;
            }
            else
            {
                order = new FaceToFaceEncounter();
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(patientId, agencyId);
            }
            return View("FaceToFace/Edit", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SendCpoBill(string list)
        {
            Check.Argument.IsNotNull(list, "list");
            var viewData = new JsonViewData { errorMessage = "The cpo bill could not be sent. Please try again.", isSuccessful = false };
            
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateFaceToFace([Bind] FaceToFaceEncounter faceToFace)
        {
            Check.Argument.IsNotNull(faceToFace, "faceToFace");

            var viewData = new JsonViewData { errorMessage = "Face-To-Face Encounter could not be updated. Please try again.", isSuccessful = false };

            if (faceToFace.IsValid)
            {
                if (physicianService.UpdateFaceToFaceEncounter(faceToFace))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Face-To-Face Encounter has been updated successfully.";
                }
            }
            else
            {
                viewData.errorMessage = faceToFace.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CpoListHeader()
        {
            ViewData["GroupName"] = "LogDate";
            return PartialView("CPO/ListHeader");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CpoListView(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView("CPO/ListView");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CPOGrid()
        {
            return View(new GridModel(physicianService.GetPhysicianCPO()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceList()
        {
            return PartialView("FaceToFace/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceGrid()
        {
            return View(new GridModel(physicianService.GetFaceToFaceEncounters()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceEncounterPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
            if (order != null)
            {
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(order.PatientId, agencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, agencyId);
                var episode = patientRepository.GetEpisodeById(agencyId, order.EpisodeId, order.PatientId);
                order.EpisodeEndDate = episode.EndDateFormatted;
                order.EpisodeStartDate = episode.StartDateFormatted;
                if (order.Patient != null)
                {
                    order.Location = agencyRepository.FindLocation(agencyId, order.Patient.AgencyLocationId);
                }
            }
            else
            {
                order = new FaceToFaceEncounter();
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(patientId, agencyId);
            }
            var xml = new PhysFaceToFaceXml(order);
            order.PrintViewJson = xml.GetJson();
            return View("Print/FaceToFace", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult FaceToFacePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
            if (order != null) {
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(order.PatientId, agencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, agencyId);
                var episode = patientRepository.GetEpisodeById(agencyId, order.EpisodeId, order.PatientId);
                order.EpisodeEndDate = episode.EndDateFormatted;
                order.EpisodeStartDate = episode.StartDateFormatted;
            } else {
                order = new FaceToFaceEncounter();
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(patientId, agencyId);
            }
            var doc = new PhysFaceToFacePdf(order);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=FaceToFace_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWEval(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWEvaluationPrint", assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTEval(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var OTVisitNoteViewData=assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId);
            return PartialView(string.Format("Print/OTEval{0}",OTVisitNoteViewData!=null && OTVisitNoteViewData.Version>0?OTVisitNoteViewData.Version:1), OTVisitNoteViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischarge(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var PTDischargeNote = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId);
            return PartialView("Print/PTDischarge", PTDischargeNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTDischargePdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummary(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId);
            var xml = new VisitNoteXml(note, PdfDocs.SixtyDaySummary);
            note.PrintViewJson = xml.GetJson();
            return View("Print/SixtyDaySummaryPrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SixtyDaySummaryPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            VisitNoteViewData viewdata = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId);
            var doc = new SixtyDaySummaryPdf(viewdata);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTEval(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var PTEvaluationNote=assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId);
            
            return PartialView(string.Format("Print/PTEval{0}",PTEvaluationNote!=null&&PTEvaluationNote.Version>0?PTEvaluationNote.Version:1), PTEvaluationNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STEval(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Print/STEval", assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STEvalPdf(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(string action, Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, int orderType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This order could not been updated." };
            if (orderType == (int)OrderType.PhysicianOrder)
            {
                if (physicianService.UpdatePhysicianOrder(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.HCFA485 || orderType == (int)OrderType.HCFA485StandAlone)
            {
                if (assessmentService.UpdatePlanofCare(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The plan of care has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.FaceToFaceEncounter)
            {
                if (physicianService.UpdateFaceToFaceEncounter(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Face-to-Face encounter has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.PtEvaluation
                || orderType == (int)OrderType.PtReEvaluation
                || orderType == (int)OrderType.StEvaluation
                || orderType == (int)OrderType.StReEvaluation
                || orderType == (int)OrderType.OtEvaluation
                || orderType == (int)OrderType.OtReEvaluation
                || orderType == (int)OrderType.PTDischarge
                || orderType == (int)OrderType.SixtyDaySummary
                )
            {
                if (physicianService.UpdateEvalOrder(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Eval has been updated successfully.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CompleteList()
        {
            return PartialView("List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompleteGrid()
        {
            return View(new GridModel(physicianService.GetOrdersCompleted()));
        }

        #endregion
    }
}
