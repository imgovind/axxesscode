﻿namespace Axxess.Physician.App.Security
{
    using System;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Domain;
    
    [Serializable]
    public class AxxessPhysicianPrincipal : IPrincipal
    {
        #region Private Members

        private Roles roleId;
        private AxxessPhysicianIdentity identity;

        #endregion

        #region Constructor

        public AxxessPhysicianPrincipal(AxxessPhysicianIdentity identity)
        {
            this.identity = identity;
        }

        #endregion

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return identity; }
        }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
