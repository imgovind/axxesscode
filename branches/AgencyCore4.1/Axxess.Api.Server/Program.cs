﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace Axxess.Api.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MultiInstantiation();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }

        static void MultiInstantiation()
        {
            var mappings = new Dictionary<string, Type> 
            { 
               {"GrouperService", typeof (GrouperService)},
               {"ValidationService", typeof (OasisValidationService)}, 
               {"AuthenticationService", typeof (AuthenticationService)}
            };

            foreach (var map in mappings)
            {
                ServiceHost serviceHost = new ServiceHost(map.Value);
                serviceHost.Open();
                Console.WriteLine("{0} started", map.Key);
            }

            Console.ReadKey();
        }
    }
}
