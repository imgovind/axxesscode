﻿var Agency = {
    Init: function() {
        $('#txtAdd_Agency_Phone1').autotab({ target: 'txtAdd_Agency_Phone2', format: 'numeric' });
        $('#txtAdd_Agency_Phone2').autotab({ target: 'txtAdd_Agency_Phone3', format: 'numeric', previous: 'txtAdd_Agency_Phone1' });

        $('#txtAdd_Agency_ContactPhone1').autotab({ target: 'txtAdd_Agency_ContactPhone2', format: 'numeric' });
        $('#txtAdd_Agency_ContactPhone2').autotab({ target: 'txtAdd_Agency_ContactPhone3', format: 'numeric', previous: 'txtAdd_Agency_ContactPhone1' });
        $('#txtAdd_Agency_ContactPhone3').autotab({ target: 'txtAdd_Agency_ContactPersonFirstName', format: 'numeric', previous: 'txtAdd_Agency_ContactPhone2' });

        $(".names").alpha({ nocaps: false });
        $(".numeric").numeric();

        $("#btnAdd_Agency_GeneratePassword").click(function() {
            U.postUrl("/LookUp/NewShortGuid", null, function(data) {
                $("#txtAdd_Agency_AdminPassword").val(data.text);
            });
        });

        $("input[name=txtAdd_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#txtAdd_Agency_ContactPersonEmail").val($("#txtAdd_Agency_AdminUsername").val());
                $('#txtAdd_Agency_ContactPersonEmail').attr("disabled", true);
                $("#txtAdd_Agency_ContactPersonFirstName").val($("#txtAdd_Agency_AdminFirstName").val());
                $('#txtAdd_Agency_ContactPersonFirstName').attr("disabled", true);
                $("#txtAdd_Agency_ContactPersonLastName").val($("#txtAdd_Agency_AdminLastName").val());
                $('#txtAdd_Agency_ContactPersonLastName').attr("disabled", true); 
            }
            else {
                $("#txtAdd_Agency_ContactPersonEmail").val('');
                $('#txtAdd_Agency_ContactPersonEmail').removeAttr("disabled");
                $("#txtAdd_Agency_ContactPersonFirstName").val('');
                $('#txtAdd_Agency_ContactPersonFirstName').removeAttr("disabled");
                $("#txtAdd_Agency_ContactPersonLastName").val('');
                $('#txtAdd_Agency_ContactPersonLastName').removeAttr("disabled"); 
            }
        });

        $("#newAgencyForm").validate({
            messages: {
                AgencyAdminUsername: "",
                AgencyAdminPassword: "",
                AgencyAdminFirstName: "",
                AgencyAdminLastName: "",
                Name: "",
                TaxId: "",
                AddressLine1: "",
                AddressCity: "",
                AddressZipCode: "",
                AddressStateCode: "",
                PhoneArray: "",
                ContactPersonEmail: "",
                ContactPersonFirstName: "",
                ContactPersonLastName: "",
                ContactPhoneArray: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newAgencyValidaton").empty();
                            $("#newAgencyValidaton").hide();
                            var text = "<div style=\"color:green;\"><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully saved</strong></li></ul></div>";
                            $("#newAgencyValidaton").append(text);
                            $("#newAgencyValidaton").show();
                            //Agency.RebindList();
                            Agency.Close($("#newAgencyForm"));
                        }
                        else {
                            $("#newAgencyValidaton").empty();
                            $("#newAgencyValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage'><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newAgencyValidaton").append(text);
                            $("#newAgencyValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editAgencyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {

                    },
                    success: function(result) {
                        var resultObject = eval(result);

                        if (resultObject.isSuccessful) {
                            $("#editAgencyValidaton").empty();
                            $("#editAgencyValidaton").hide();
                            var text = "<div><ul class='system_messages'><li class='green sent'><span class='ico'></span><strong class='system_title'>Your data is successfully edited !</strong></li></ul></div>";
                            $("#editAgencyValidaton").append(text);
                            $("#editAgencyValidaton").show();

                        }
                        else {
                            $("#editAgencyValidaton").empty();
                            $("#editAgencyValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage'><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#editAgencyValidaton").append(text);
                            $("#editAgencyValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    New: function() {
        $("#newAgencyValidaton").empty();
        $("#newAgencyValidaton").hide();
        $("#newAgencyForm label.error").hide();
        $("#newAgencyForm").clearForm();
        U.postUrl("/LookUp/NewShortGuid", null, function(data) {
            $("#txtAdd_Agency_AdminPassword").val(data.text);
        });
    },
    Delete: function(cont, id) {
        var row = cont.parents('tr:first');
        if (confirm("Are you sure you want to delete this Agency?")) {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Agency/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        $(row).remove();
                    }
                }
            });
        }
    },
    Edit: function(id) {
        $("#editAgencyForm label.error").hide();
        $("#editAgencyForm").clearForm();
        $("#editAgencyValidaton").empty();
        $("#editAgencyValidaton").hide();
        var data = 'id=' + id;
        $.ajax({
            url: '/Agency/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                getNewRows(reportObject);
            }
        });

        getNewRows = function(data) {
            $("#txtEdit_AgencyID").val(id);
            $("#txtEdit_PhysicianAgency").val((data.PhysicianAgency !== null ? data.PhysicianAgency : " "));
            $("#txtEdit_Agency_Date").val((data.PhysicianAgencyDate !== null ? new Date(data.PhysicianAgencyDate).getMonth() + "/" + new Date(data.PhysicianAgencyDate).getDate() + "/" + new Date(data.PhysicianAgencyDate).getFullYear() : " "));
            $("#txtEdit_Agency_AgencySource").val((data.AgencySource !== null ? data.AgencySource : " "));
            $("#txtEdit_Referrer_FirstName").val((data.ReferrerFirstName !== null ? data.ReferrerFirstName : " "));
            $("#txtEdit_Referrer_LastName").val((data.ReferrerLastName !== null ? data.ReferrerLastName : " "));
            $('input[name=Gender][value=' + data.Gender.toString() + ']').attr('checked', true);
        };
    },
    NewAgency: function() {
        var refer = $('#List_Agency_NewButton');
        href = "javascript:void(0);";
        refer.attr('href', href);
    },
    RebindList: function() {
        var AgencyGrid = $('#ExistingAgencyGrid').data('tGrid');
        AgencyGrid.rebind();
    },
    Close: function(control) {
        control.closest('div.window').hide();
    }
}