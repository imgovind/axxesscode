﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareSupportiveAssistanceForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M1100) Patient Living Situation: Which of the following best describes the patient's
                residential circumstance and availability of assistance? (Check one box only.)
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0" border="0">
        <thead>
            <tr>
                <th colspan="1" rowspan="2">
                    Living Arrangement
                </th>
                <th colspan="5">
                    Availability of Assistance
                </th>
            </tr>
            <tr>
                <th>
                    Around the clock
                </th>
                <th>
                    Regular daytime
                </th>
                <th>
                    Regular nighttime
                </th>
                <th>
                    Occasional / short-term assistance
                </th>
                <th>
                    No assistance available
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    a. Patient lives alone
                </td>
                <td>
                    <%=Html.Hidden("StartOfCare_M1100LivingSituation", " ", new { @id = "" })%>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "01", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "01" ? true : false, new { @id = "" })%>
                    &nbsp; 01
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "02", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "02" ? true : false, new { @id = "" })%>
                    &nbsp; 02
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "03", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "03" ? true : false, new { @id = "" })%>
                    &nbsp; 03
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "04", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "04" ? true : false, new { @id = "" })%>
                    &nbsp; 04
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "05", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "05" ? true : false, new { @id = "" })%>
                    &nbsp; 05
                </td>
            </tr>
            <tr>
                <td>
                    b. Patient lives with other person(s) in the home
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "06", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "06" ? true : false, new { @id = "" })%>
                    &nbsp; 06
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "07", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "07" ? true : false, new { @id = "" })%>
                    &nbsp; 07
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "08", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "08" ? true : false, new { @id = "" })%>
                    &nbsp; 08
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "09", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "09" ? true : false, new { @id = "" })%>
                    &nbsp; 09
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "10", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "10" ? true : false, new { @id = "" })%>
                    &nbsp; 10
                </td>
            </tr>
            <tr>
                <td>
                    c. Patient lives in congregate situation (e.g., assisted living)
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "11", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "11" ? true : false, new { @id = "" })%>
                    &nbsp; 11
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "12", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "12" ? true : false, new { @id = "" })%>
                    &nbsp; 12
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "13", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "13" ? true : false, new { @id = "" })%>
                    &nbsp; 13
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "14", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "14" ? true : false, new { @id = "" })%>
                    &nbsp; 14
                </td>
                <td>
                    <%=Html.RadioButton("StartOfCare_M1100LivingSituation", "15", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "15" ? true : false, new { @id = "" })%>
                    &nbsp; 15
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="5">
                Type of Assistance Patient Receives - other than from home health agency staff&nbsp;
                <i>(Select all that apply)</i>
            </th>
        </tr>
        <tr>
            <th>
                Type of Assistance
            </th>
            <th>
                Family/Friends
            </th>
            <th>
                Provider Services
            </th>
            <th>
                Paid Caregiver
            </th>
            <th>
                Volunteer Organizations
            </th>
        </tr>
        <tr>
            <td>
                ADL (bathing, dressing, toileting, bowel/bladder, eating/feeding)
            </td>
            <td>
                <input name="StartOfCare_GenericADLTypeOfAssistance" value=" " type="hidden" />
                <input type="checkbox" name="StartOfCare_GenericADLTypeOfAssistance" value="Family/Friends" '<% if( data.ContainsKey("GenericADLTypeOfAssistance") && data["GenericADLTypeOfAssistance"].Answer == "Family/Friends" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericADLTypeOfAssistance" value="Provider Services" '<% if( data.ContainsKey("GenericADLTypeOfAssistance") && data["GenericADLTypeOfAssistance"].Answer == "Provider Services" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericADLTypeOfAssistance" value="Paid Caregiver" '<% if( data.ContainsKey("GenericADLTypeOfAssistance") && data["GenericADLTypeOfAssistance"].Answer == "Paid Caregiver" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericADLTypeOfAssistance" value="Volunteer Organizations" '<% if( data.ContainsKey("GenericADLTypeOfAssistance") && data["GenericADLTypeOfAssistance"].Answer == "Volunteer Organizations" ){ %>checked="checked"<% }%>'" />
            </td>
        </tr>
        <tr>
            <td>
                IADL (meds, meals, housekeeping, laundry, telephone, shopping, finances)
            </td>
            <td>
                <input name="StartOfCare_GenericIADLTypeOfAssistance" value=" " type="hidden" />
                <input type="checkbox" name="StartOfCare_GenericIADLTypeOfAssistance" value="Family/Friends" '<% if( data.ContainsKey("GenericIADLTypeOfAssistance") && data["GenericIADLTypeOfAssistance"].Answer == "Family/Friends" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericIADLTypeOfAssistance" value="Provider Services" '<% if( data.ContainsKey("GenericIADLTypeOfAssistance") && data["GenericIADLTypeOfAssistance"].Answer == "Provider Services" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericIADLTypeOfAssistance" value="Paid Caregiver" '<% if( data.ContainsKey("GenericIADLTypeOfAssistance") && data["GenericIADLTypeOfAssistance"].Answer == "Paid Caregiver" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericIADLTypeOfAssistance" value="Volunteer Organizations" '<% if( data.ContainsKey("GenericIADLTypeOfAssistance") && data["GenericIADLTypeOfAssistance"].Answer == "Volunteer Organizations" ){ %>checked="checked"<% }%>'" />
            </td>
        </tr>
        <tr>
            <td>
                Psychosocial Support
            </td>
            <td>
                <input name="StartOfCare_GenericPsychoSupportTypeOfAssistance" value=" " type="hidden" />
                <input type="checkbox" name="StartOfCare_GenericPsychoSupportTypeOfAssistance" value="Family/Friends" '<% if( data.ContainsKey("GenericPsychoSupportTypeOfAssistance") && data["GenericPsychoSupportTypeOfAssistance"].Answer == "Family/Friends" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericPsychoSupportTypeOfAssistance" value="Provider Services" '<% if( data.ContainsKey("GenericPsychoSupportTypeOfAssistance") && data["GenericPsychoSupportTypeOfAssistance"].Answer == "Provider Services" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericPsychoSupportTypeOfAssistance" value="Paid Caregiver" '<% if( data.ContainsKey("GenericPsychoSupportTypeOfAssistance") && data["GenericPsychoSupportTypeOfAssistance"].Answer == "Paid Caregiver" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericPsychoSupportTypeOfAssistance" value="Volunteer Organizations" '<% if( data.ContainsKey("GenericPsychoSupportTypeOfAssistance") && data["GenericPsychoSupportTypeOfAssistance"].Answer == "Volunteer Organizations" ){ %>checked="checked"<% }%>'" />
            </td>
        </tr>
        <tr>
            <td>
                Assistance with Medical Appointments, Delivery of Medications
            </td>
            <td>
                <input name="StartOfCare_GenericMedicalApptTypeOfAssistance" value=" " type="hidden" />
                <input type="checkbox" name="StartOfCare_GenericMedicalApptTypeOfAssistance" value="Family/Friends" '<% if( data.ContainsKey("GenericMedicalApptTypeOfAssistance") && data["GenericMedicalApptTypeOfAssistance"].Answer == "Family/Friends" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericMedicalApptTypeOfAssistance" value="Provider Services" '<% if( data.ContainsKey("GenericMedicalApptTypeOfAssistance") && data["GenericMedicalApptTypeOfAssistance"].Answer == "Provider Services" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericMedicalApptTypeOfAssistance" value="Paid Caregiver" '<% if( data.ContainsKey("GenericMedicalApptTypeOfAssistance") && data["GenericMedicalApptTypeOfAssistance"].Answer == "Paid Caregiver" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericMedicalApptTypeOfAssistance" value="Volunteer Organizations" '<% if( data.ContainsKey("GenericMedicalApptTypeOfAssistance") && data["GenericMedicalApptTypeOfAssistance"].Answer == "Volunteer Organizations" ){ %>checked="checked"<% }%>'" />
            </td>
        </tr>
        <tr>
            <td>
                Management of Finances
            </td>
            <td>
                <input name="StartOfCare_GenericFinanceManagmentTypeOfAssistance" value=" " type="hidden" />
                <input type="checkbox" name="StartOfCare_GenericFinanceManagmentTypeOfAssistance"
                    value="Family/Friends" '<% if( data.ContainsKey("GenericFinanceManagmentTypeOfAssistance") && data["GenericFinanceManagmentTypeOfAssistance"].Answer == "Family/Friends" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericFinanceManagmentTypeOfAssistance"
                    value="Provider Services" '<% if( data.ContainsKey("GenericFinanceManagmentTypeOfAssistance") && data["GenericFinanceManagmentTypeOfAssistance"].Answer == "Provider Services" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericFinanceManagmentTypeOfAssistance"
                    value="Paid Caregiver" '<% if( data.ContainsKey("GenericFinanceManagmentTypeOfAssistance") && data["GenericFinanceManagmentTypeOfAssistance"].Answer == "Paid Caregiver" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_GenericFinanceManagmentTypeOfAssistance"
                    value="Volunteer Organizations" '<% if( data.ContainsKey("GenericFinanceManagmentTypeOfAssistance") && data["GenericFinanceManagmentTypeOfAssistance"].Answer == "Volunteer Organizations" ){ %>checked="checked"<% }%>'" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                Comments:<br />
                <%=Html.TextArea("StartOfCare_GenericTypeOfAssistanceComments", data.ContainsKey("GenericTypeOfAssistanceComments") ? data["GenericTypeOfAssistanceComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericTypeOfAssistanceComments", @style = "width: 99.5%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Supportive Assistance: Names of organizations providing assistance
            </th>
        </tr>
        <tr>
            <td>
                <%=Html.TextArea("StartOfCare_GenericOrgProvidingAssistanceNames", data.ContainsKey("GenericOrgProvidingAssistanceNames") ? data["GenericOrgProvidingAssistanceNames"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericOrgProvidingAssistanceNames", @style = "width: 99.5%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th width="45%">
                Community Agencies/Social Service Screening
            </th>
            <th width="5%">
                Yes
            </th>
            <th width="5%">
                No
            </th>
            <th>
                Ability of patient to handle finances:
            </th>
        </tr>
        <tr>
            <td>
                Community resource info needed to manage care
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericCommunityResourceInfoNeeded", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericCommunityResourceInfoNeeded", "1", data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "1" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_GenericCommunityResourceInfoNeeded", "0", data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "0" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericAbilityHandleFinance", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Independent", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Independent" ? true : false, new { @id = "" })%>
                &nbsp;Independent
                <%=Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Dependent", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Dependent" ? true : false, new { @id = "" })%>
                &nbsp;Dependent
                <%=Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Needs assistance", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Needs assistance" ? true : false, new { @id = "" })%>
                &nbsp;Needs assistance
            </td>
        </tr>
        <tr>
            <td>
                Altered affect, e.g., expressed sadness or anxiety, grief
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericAlteredAffect", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericAlteredAffect", "Yes", data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "Yes" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_GenericAlteredAffect", "No", data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "No" ? true : false, new { @id = "" })%>
            </td>
            <td rowspan="5" valign="top">
                Comments:<br />
                <%=Html.TextArea("StartOfCare_GenericAlteredAffectComments", data.ContainsKey("GenericAlteredAffectComments") ? data["GenericAlteredAffectComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericAlteredAffectComments", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                Suicidal ideation
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericSuicidalIdeation", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericSuicidalIdeation", "Yes", data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "Yes" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_GenericSuicidalIdeation", "No", data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "No" ? true : false, new { @id = "" })%>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <ul>
                    <li>Suspected Abuse/Neglect: </li>
                    <li>&nbsp;
                        <input type="hidden" name="StartOfCare_GenericSuspected" value=" " />
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="1" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Unexplained bruises </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="2" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Inadequate food </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="3" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "3" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Fearful of family member </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="4" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "4" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Exploitation of funds </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="5" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "5" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Sexual abuse </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="6" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "6" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Neglect </li>
                    <li>&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericSuspected" value="7" '<% if( data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "7" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Left unattended if constant supervision is needed </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                MSW referral indicated for:
                <%=Html.TextBox("StartOfCare_GenericMSWIndicatedForWhat", data.ContainsKey("GenericMSWIndicatedForWhat") ? data["GenericMSWIndicatedForWhat"].Answer : "", new { @id = "StartOfCare_GenericMSWIndicatedForWhat",@size="35",@maxlength="150" })%>
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericMSWIndicatedFor", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericMSWIndicatedFor", "Yes", data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "Yes" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_GenericMSWIndicatedFor", "No", data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "No" ? true : false, new { @id = "" })%>
            </td>
        </tr>
        <tr>
            <td>
                Coordinator notified
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_GenericCoordinatorNotified", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericCoordinatorNotified", "Yes", data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "Yes" ? true : false, new { @id = "" })%>
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_GenericCoordinatorNotified", "No", data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "No" ? true : false, new { @id = "" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Safety/Sanitation Hazards affecting patient: <i>(Select all that apply)</i>
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="StartOfCare_GenericNoHazardsIdentified" value="" />
                <input type="checkbox" name="StartOfCare_GenericNoHazardsIdentified" value="1" '<% if( data.ContainsKey("GenericNoHazardsIdentified") && data["GenericNoHazardsIdentified"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                No hazards identified
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericHazardsIdentified" value="" />
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="1" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Stairs </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="2" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Narrow or obstructed walkway </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="3" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "3" ){ %>checked="checked"<% }%>'" />&nbsp;
                        No gas/electric appliance </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="4" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "4" ){ %>checked="checked"<% }%>'" />&nbsp;
                        No running water, plumbing </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="5" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "5" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Insect/rodent infestation </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="6" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "6" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cluttered/soiled living area </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="7" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "7" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Inadequate lighting, heating and cooling </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHazardsIdentified" value="8" '<% if( data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer == "8" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Lack of fire safety devices </li>
                    <li>Other: <i>(specify)</i>
                        <%=Html.TextBox("StartOfCare_GenericOtherHazards", data.ContainsKey("GenericOtherHazards") ? data["GenericOtherHazards"].Answer : "", new { @id = "StartOfCare_GenericOtherHazards", @size = "20", @maxlength = "30" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Comments:<br />
                <%=Html.TextArea("StartOfCare_GenericHazardsComments", data.ContainsKey("GenericHazardsComments") ? data["GenericHazardsComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericHazardsComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Fire Assessment for Patients with Oxygen
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="StartOfCare_GenericUsingOxygen" value="" />
                <input type="checkbox" name="StartOfCare_GenericUsingOxygen" value="1" '<% if( data.ContainsKey("GenericUsingOxygen") && data["GenericUsingOxygen"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient not using oxygen
            </td>
        </tr>
        <tr>
            <td>
                Does patient have No Smoking signs posted? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericNoSmokingSigns", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericNoSmokingSigns", "1", data.ContainsKey("GenericNoSmokingSigns") && data["GenericNoSmokingSigns"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericNoSmokingSigns", "0", data.ContainsKey("GenericNoSmokingSigns") && data["GenericNoSmokingSigns"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericNoSmokingSignsPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericNoSmokingSignsPerson" value="1" '<% if( data.ContainsKey("GenericNoSmokingSignsPerson") && data["GenericNoSmokingSignsPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericNoSmokingSignsPerson" value="2" '<% if( data.ContainsKey("GenericNoSmokingSignsPerson") && data["GenericNoSmokingSignsPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Does patient or anyone in the home smoke with oxygen in use? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericSmokeWithOxygenInUser", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericSmokeWithOxygenInUser", "1", data.ContainsKey("GenericSmokeWithOxygenInUser") && data["GenericSmokeWithOxygenInUser"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericSmokeWithOxygenInUser", "0", data.ContainsKey("GenericSmokeWithOxygenInUser") && data["GenericSmokeWithOxygenInUser"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericSmokeWithOxygenInUserPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericSmokeWithOxygenInUserPerson" value="1" '<% if( data.ContainsKey("GenericSmokeWithOxygenInUserPerson") && data["GenericSmokeWithOxygenInUserPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericSmokeWithOxygenInUserPerson" value="2" '<% if( data.ContainsKey("GenericSmokeWithOxygenInUserPerson") && data["GenericSmokeWithOxygenInUserPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Are smoke detectors present and working properly? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericSmokeDetectorsWorking", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericSmokeDetectorsWorking", "1", data.ContainsKey("GenericSmokeDetectorsWorking") && data["GenericSmokeDetectorsWorking"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericSmokeDetectorsWorking", "0", data.ContainsKey("GenericSmokeDetectorsWorking") && data["GenericSmokeDetectorsWorking"].Answer == "0" ? true : false, new { @id = "" })%>
                No<br />
                <input type="hidden" name="StartOfCare_GenericSmokeDetectorsWorkingPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericSmokeDetectorsWorkingPerson" value="1" '<% if( data.ContainsKey("GenericSmokeDetectorsWorkingPerson") && data["GenericSmokeDetectorsWorkingPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericSmokeDetectorsWorkingPerson" value="2" '<% if( data.ContainsKey("GenericSmokeDetectorsWorkingPerson") && data["GenericSmokeDetectorsWorkingPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Does patient have a properly functioning fire extinguisher? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericFireExtinguisherWorking", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericFireExtinguisherWorking", "1", data.ContainsKey("GenericFireExtinguisherWorking") && data["GenericFireExtinguisherWorking"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericFireExtinguisherWorking", "0", data.ContainsKey("GenericFireExtinguisherWorking") && data["GenericFireExtinguisherWorking"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericFireExtinguisherWorkingPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericFireExtinguisherWorkingPerson" value="1" '<% if( data.ContainsKey("GenericFireExtinguisherWorkingPerson") && data["GenericFireExtinguisherWorkingPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericFireExtinguisherWorkingPerson" value="2" '<% if( data.ContainsKey("GenericFireExtinguisherWorkingPerson") && data["GenericFireExtinguisherWorkingPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Are oxygen cylinders stored properly? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericOxygenCyclindersSafe", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericOxygenCyclindersSafe", "1", data.ContainsKey("GenericOxygenCyclindersSafe") && data["GenericOxygenCyclindersSafe"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericOxygenCyclindersSafe", "0", data.ContainsKey("GenericOxygenCyclindersSafe") && data["GenericOxygenCyclindersSafe"].Answer == "0" ? true : false, new { @id = "" })%>
                No<br />
                <input type="hidden" name="StartOfCare_GenericOxygenCyclindersSafePerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericOxygenCyclindersSafePerson" value="1" '<% if( data.ContainsKey("GenericOxygenCyclindersSafePerson") && data["GenericOxygenCyclindersSafePerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericOxygenCyclindersSafePerson" value="2" '<% if( data.ContainsKey("GenericOxygenCyclindersSafePerson") && data["GenericOxygenCyclindersSafePerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Are all electrical cords near oxygen intact and free from fraying? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericElectricalCordsIntact", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericElectricalCordsIntact", "1", data.ContainsKey("GenericElectricalCordsIntact") && data["GenericElectricalCordsIntact"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericElectricalCordsIntact", "0", data.ContainsKey("GenericElectricalCordsIntact") && data["GenericElectricalCordsIntact"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericElectricalCordsIntactPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericElectricalCordsIntactPerson" value="1" '<% if( data.ContainsKey("GenericElectricalCordsIntactPerson") && data["GenericElectricalCordsIntactPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericElectricalCordsIntactPerson" value="2" '<% if( data.ContainsKey("GenericElectricalCordsIntactPerson") && data["GenericElectricalCordsIntactPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Does patient have an evacuation plan in case of fire? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericEvacuationPlan", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericEvacuationPlan", "1", data.ContainsKey("GenericEvacuationPlan") && data["GenericEvacuationPlan"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericEvacuationPlan", "0", data.ContainsKey("GenericEvacuationPlan") && data["GenericEvacuationPlan"].Answer == "0" ? true : false, new { @id = "" })%>
                0&nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericEvacuationPlanPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericEvacuationPlanPerson" value="1" '<% if( data.ContainsKey("GenericEvacuationPlanPerson") && data["GenericEvacuationPlanPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericEvacuationPlanPerson" value="2" '<% if( data.ContainsKey("GenericEvacuationPlanPerson") && data["GenericEvacuationPlanPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Are all cleaning fluids and aerosols stored away from oxygen, and not used while
                oxygen is in use? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericAerosolsSafeWhenOxygenInUse", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericAerosolsSafeWhenOxygenInUse", "1", data.ContainsKey("GenericAerosolsSafeWhenOxygenInUse") && data["GenericAerosolsSafeWhenOxygenInUse"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericAerosolsSafeWhenOxygenInUse", "0", data.ContainsKey("GenericAerosolsSafeWhenOxygenInUse") && data["GenericAerosolsSafeWhenOxygenInUse"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericAerosolsSafeWhenOxygenInUsePerson"
                    value="" />
                <input type="checkbox" name="StartOfCare_GenericAerosolsSafeWhenOxygenInUsePerson"
                    value="1" '<% if( data.ContainsKey("GenericAerosolsSafeWhenOxygenInUsePerson") && data["GenericAerosolsSafeWhenOxygenInUsePerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericAerosolsSafeWhenOxygenInUsePerson"
                    value="2" '<% if( data.ContainsKey("GenericAerosolsSafeWhenOxygenInUsePerson") && data["GenericAerosolsSafeWhenOxygenInUsePerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Does patient refrain from using petroleum products around oxygen?&nbsp;
                <%=Html.Hidden("StartOfCare_GenericPetroleumNearOxygen", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericPetroleumNearOxygen", "1", data.ContainsKey("GenericPetroleumNearOxygen") && data["GenericPetroleumNearOxygen"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericPetroleumNearOxygen", "0", data.ContainsKey("GenericPetroleumNearOxygen") && data["GenericPetroleumNearOxygen"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericPetroleumNearOxygenPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericPetroleumNearOxygenPerson" value="1" '<% if( data.ContainsKey("GenericPetroleumNearOxygenPerson") && data["GenericPetroleumNearOxygenPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericPetroleumNearOxygenPerson" value="2" '<% if( data.ContainsKey("GenericPetroleumNearOxygenPerson") && data["GenericPetroleumNearOxygenPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Does patient only use water-based body and lip moisturizers?&nbsp;
                <%=Html.Hidden("StartOfCare_GenericWaterbasedBodyLotion", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericWaterbasedBodyLotion", "1", data.ContainsKey("GenericWaterbasedBodyLotion") && data["GenericWaterbasedBodyLotion"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericWaterbasedBodyLotion", "0", data.ContainsKey("GenericWaterbasedBodyLotion") && data["GenericWaterbasedBodyLotion"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No<br />
                <input type="hidden" name="StartOfCare_GenericWaterbasedBodyLotionPerson" value="" />
                <input type="checkbox" name="StartOfCare_GenericWaterbasedBodyLotionPerson" value="1" '<% if( data.ContainsKey("GenericWaterbasedBodyLotionPerson") && data["GenericWaterbasedBodyLotionPerson"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                Patient &nbsp;
                <input type="checkbox" name="StartOfCare_GenericWaterbasedBodyLotionPerson" value="2" '<% if( data.ContainsKey("GenericWaterbasedBodyLotionPerson") && data["GenericWaterbasedBodyLotionPerson"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                Caregiver educated&nbsp;
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <%string[] safetyMeasure = data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer != "" ? data["485SafetyMeasures"].Answer.Split(',') : null; %>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="3">
                Safety Measures (locator #15)
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="StartOfCare_485SafetyMeasures" value="" />
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="1" '<% if( safetyMeasure!=null && safetyMeasure.Contains("1") ){ %>checked="checked"<% }%>'" />&nbsp;
                Anticoagulant Precautions
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="2" '<% if( safetyMeasure!=null && safetyMeasure.Contains("2")){ %>checked="checked"<% }%>'" />&nbsp;
                Emergency Plan Developed
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="3" '<% if(safetyMeasure!=null && safetyMeasure.Contains("3") ){ %>checked="checked"<% }%>'" />&nbsp;
                Fall Precautions
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="4" '<% if( safetyMeasure!=null && safetyMeasure.Contains("4")){ %>checked="checked"<% }%>'" />&nbsp;
                Keep Pathway Clear
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="5" '<% if( safetyMeasure!=null && safetyMeasure.Contains("5") ){ %>checked="checked"<% }%>'" />&nbsp;
                Keep Side Rails Up
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="6" '<% if( safetyMeasure!=null && safetyMeasure.Contains("6")){ %>checked="checked"<% }%>'" />&nbsp;
                Neutropenic Precautions
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="7" '<% if( safetyMeasure!=null && safetyMeasure.Contains("7")){ %>checked="checked"<% }%>'" />&nbsp;
                O2 Precautions
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="8" '<% if(safetyMeasure!=null && safetyMeasure.Contains("8") ){ %>checked="checked"<% }%>'" />&nbsp;
                Proper Position During Meals
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="9" '<% if( safetyMeasure!=null && safetyMeasure.Contains("9") ){ %>checked="checked"<% }%>'" />&nbsp;
                Safety in ADLs
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="10" '<% if( safetyMeasure!=null && safetyMeasure.Contains("10") ){ %>checked="checked"<% }%>'" />&nbsp;
                Seizure Precautions
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="11" '<% if( safetyMeasure!=null && safetyMeasure.Contains("11") ){ %>checked="checked"<% }%>'" />&nbsp;
                Sharps Safety
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="12" '<% if( safetyMeasure!=null && safetyMeasure.Contains("12")){ %>checked="checked"<% }%>'" />&nbsp;
                Slow Position Change
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="13" '<% if(safetyMeasure!=null && safetyMeasure.Contains("13") ){ %>checked="checked"<% }%>'" />&nbsp;
                Standard Precautions/Infection Control
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="14" '<% if(safetyMeasure!=null && safetyMeasure.Contains("14") ){ %>checked="checked"<% }%>'" />&nbsp;
                Support During Transfer and Ambulation
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="15" '<% if(safetyMeasure!=null && safetyMeasure.Contains("15") ){ %>checked="checked"<% }%>'" />&nbsp;
                Use of Assistive Devices
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Other: <i>(specify)</i><br />
                <%=Html.TextArea("StartOfCare_485OtherSafetyMeasures", data.ContainsKey("485OtherSafetyMeasures") ? data["485OtherSafetyMeasures"].Answer : "", 5, 70, new { @id = "StartOfCare_485OtherSafetyMeasures", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="16" '<% if( safetyMeasure!=null && safetyMeasure.Contains("16") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on safe utilities management
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="17" '<% if(safetyMeasure!=null && safetyMeasure.Contains("17") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on mobility safety
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="18" '<% if( safetyMeasure!=null && safetyMeasure.Contains("18") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on DME & electrical safety
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="19" '<% if(safetyMeasure!=null && safetyMeasure.Contains("19") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on sharps container
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="20" '<% if(safetyMeasure!=null && safetyMeasure.Contains("20")){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on medical gas
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="21" '<% if( safetyMeasure!=null && safetyMeasure.Contains("21") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on disaster/emergency plan
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="22" '<% if(safetyMeasure!=null && safetyMeasure.Contains("22") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on safety measures
            </td>
            <td>
                <input type="checkbox" name="StartOfCare_485SafetyMeasures" value="23" '<% if(safetyMeasure!=null && safetyMeasure.Contains("23") ){ %>checked="checked"<% }%>'" />&nbsp;
                Instructed on proper handling of biohazard waste
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <span>Triage/Risk Code:</span>
                <input type="text" name="StartOfCare_485TriageRiskCode" id="StartOfCare_485TriageRiskCode"
                    size="5" maxlength="5" value="" />
            </td>
            <td>
                <span>Disaster Code:</span>
                <input type="text" name="StartOfCare_485DisasterCode" id="StartOfCare_485DisasterCode"
                    size="5" maxlength="5" value="" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Comments:<br />
                <%=Html.TextArea("StartOfCare_485SafetyMeasuresComments", data.ContainsKey("485SafetyMeasuresComments") ? data["485SafetyMeasuresComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485SafetyMeasuresComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="3">
                Cultural
            </th>
        </tr>
        <tr>
            <td>
                Primary language?&nbsp;
                <%=Html.TextBox("StartOfCare_GenericPrimaryLanguage", data.ContainsKey("GenericPrimaryLanguage") ? data["GenericPrimaryLanguage"].Answer : "", new { @id = "StartOfCare_GenericPrimaryLanguage",@size="40",@maxlength="40" })%>
            </td>
        </tr>
        <tr>
            <td>
                Does patient have cultural practices that influence health care?&nbsp;
                <%=Html.Hidden("StartOfCare_GenericCulturalPractices", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericCulturalPractices", "1", data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericCulturalPractices", "0", data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                No
                <br />
                If yes, please explain:<br />
                <%=Html.TextArea("StartOfCare_GenericCulturalPracticesDetails", data.ContainsKey("GenericCulturalPracticesDetails") ? data["GenericCulturalPracticesDetails"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericCulturalPracticesDetails", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                Is religion important to the patient?&nbsp;
                <%=Html.Hidden("StartOfCare_GenericReligionImportant", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericReligionImportant", "1", data.ContainsKey("GenericReligionImportant") && data["GenericReligionImportant"].Answer == "1" ? true : false, new { @id = "" })%>
                &nbsp; Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericReligionImportant", "0", data.ContainsKey("GenericReligionImportant") && data["GenericReligionImportant"].Answer == "0" ? true : false, new { @id = "" })%>
                &nbsp; No
            </td>
        </tr>
        <tr>
            <td>
                Patient's religious preference? &nbsp;
                <%=Html.TextBox("StartOfCare_GenericReligiousPreference", data.ContainsKey("GenericReligiousPreference") ? data["GenericReligiousPreference"].Answer : "", new { @id = "StartOfCare_GenericReligiousPreference", @size = "40", @maxlength = "40" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%string[] useOfInterpreter = data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer != "" ? data["GenericUseOfInterpreter"].Answer.Split(',') : null; %>
                <ul>
                    <li>Use of interpreter <i>(select patient preferences)</i>: </li>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericUseOfInterpreter" value="" />
                        <input type="checkbox" name="StartOfCare_GenericUseOfInterpreter" value="1" '<% if( useOfInterpreter!=null && useOfInterpreter.Contains("1") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Family </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericUseOfInterpreter" value="2" '<% if( useOfInterpreter!=null && useOfInterpreter.Contains("2") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Friend </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericUseOfInterpreter" value="3" '<% if( useOfInterpreter!=null && useOfInterpreter.Contains("3") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Professional </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericUseOfInterpreter" value="4" '<% if( useOfInterpreter!=null && useOfInterpreter.Contains("4") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other &nbsp;
                        <%=Html.TextBox("StartOfCare_GenericUseOfInterpreterOtherDetails", data.ContainsKey("GenericUseOfInterpreterOtherDetails") ? data["GenericUseOfInterpreterOtherDetails"].Answer : "", new { @id = "StartOfCare_GenericUseOfInterpreterOtherDetails", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Patient's primary source of emotional support:&nbsp;
                <%=Html.TextBox("StartOfCare_GenericPrimaryEmotionalSupport", data.ContainsKey("GenericPrimaryEmotionalSupport") ? data["GenericPrimaryEmotionalSupport"].Answer : "", new { @id = "StartOfCare_GenericPrimaryEmotionalSupport", @size = "30", @maxlength = "30" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Homebound?
            </th>
        </tr>
        <tr>
            <td>
                Is Homebound? &nbsp;
                <%=Html.Hidden("StartOfCare_GenericIsHomeBound", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericIsHomeBound", "Yes", data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "Yes" ? true : false, new { @id = "" })%>
                &nbsp;Yes&nbsp;
                <%=Html.RadioButton("StartOfCare_GenericIsHomeBound", "No", data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "No" ? true : false, new { @id = "" })%>&nbsp;No
            </td>
        </tr>
        <tr>
            <td>
                <%string[] homeBoundReason = data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer != "" ? data["GenericHomeBoundReason"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericHomeBoundReason" value="" />
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="1" '<% if( homeBoundReason!=null && homeBoundReason.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Residual weakness </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="2" '<% if(  homeBoundReason!=null && homeBoundReason.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Unable to safely leave home unassisted </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="3" '<% if(  homeBoundReason!=null && homeBoundReason.Contains("3") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Requires max assistance/taxing effort to leave home </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="4" '<% if( homeBoundReason!=null && homeBoundReason.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Confusion, unsafe to go out of home alone </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="5" '<% if( homeBoundReason!=null && homeBoundReason.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Severe SOB, SOB upon exertion </li>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="6" '<% if(  homeBoundReason!=null && homeBoundReason.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other
                        <%=Html.TextBox("StartOfCare_GenericOtherHomeBoundDetails", data.ContainsKey("GenericOtherHomeBoundDetails") ? data["GenericOtherHomeBoundDetails"].Answer : "", new { @id = "StartOfCare_GenericOtherHomeBoundDetails", @size = "60", @maxlength = "60" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="StartOfCare_GenericHomeBoundReason" value="7" '<% if(  homeBoundReason!=null && homeBoundReason.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                Need assistance for all activities
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
