﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpPatientHistoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1020/1022/1024) Diagnoses, Symptom Control, and Payment Diagnoses: List each diagnosis
                for which the patient is receiving home care (Column 1) and enter its ICD-9-C M
                code at the level of highest specificity (no surgical/procedure codes) (Column 2).
                Diagnoses are listed in the order that best reflect the seriousness of each condition
                and support the disciplines and services provided. Rate the degree of symptom control
                for each condition (Column 2). Choose one value that represents the degree of symptom
                control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes
                (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed
                if multiple coding is indicated for any diagnoses. If a V-code is reported in place
                of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and
                4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare
                P P S case mix group. Do not assign symptom control ratings for V- or E-codes.<br />
                <b>Code each row according to the following directions for each column:<br />
                </b>Column 1: Enter the description of the diagnosis.<br />
                Column 2: Enter the ICD-9-C M code for the diagnosis described in Column 1;<br />
                Rate the degree of symptom control for the condition listed in Column 1 using the
                following scale:<br />
                0 - Asymptomatic, no treatment needed at this time<br />
                1 - Symptoms well controlled with current therapy<br />
                2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs
                ongoing monitoring<br />
                3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and
                dose monitoring<br />
                4 - Symptoms poorly controlled; history of re-hospitalizations Note that in Column
                2 the rating for symptom control of each diagnosis should not be used to determine
                the sequencing of the diagnoses listed in Column 1. These are separate items and
                sequencing may not coincide. Sequencing of diagnoses should reflect the seriousness
                of each condition and support the disciplines and services provided.<br />
                Column 3: (OPTIONAL) If a V-code is assigned to any row in Column 2, in place of
                a case mix diagnosis, it may be necessary to complete optional item M1024 Payment
                Diagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.<br />
                Column 4: (OPTIONAL) If a V-code in Column 2 is reported in place of a case mix
                diagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,
                enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns
                3 and 4. For example, if the case mix diagnosis is a manifestation code, record
                the diagnosis description and ICD-9-C M code for the underlying condition in Column
                3 of that row and the diagnosis description and ICD-9-C M code for the manifestation
                in Column 4 of that row. Otherwise, leave Column 4 blank in that row.
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2">
                (M1020) Primary Diagnosis & (M1022) Other Diagnoses
            </th>
            <th colspan="2">
                (M1024) Payment Diagnoses (OPTIONAL)
            </th>
        </tr>
        <tr>
            <th>
                Column 1
            </th>
            <th>
                Column 2
            </th>
            <th>
                Column 3
            </th>
            <th>
                Column 4
            </th>
        </tr>
        <tr>
            <td>
                Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition
                and support the disciplines and services provided.)
            </td>
            <td>
                ICD-9-C M and symptom control rating for each condition. Note that the sequencing
                of these ratings may not match the sequencing of the diagnoses
            </td>
            <td>
                Complete if a V-code is assigned under certain circumstances to Column 2 in place
                of a case mix diagnosis.
            </td>
            <td>
                Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis
                that is a multiple coding situation (e.g., a manifestation code).
            </td>
        </tr>
        <tr>
            <td>
                Description
            </td>
            <td>
                ICD-9-C M / Symptom Control Rating
            </td>
            <td>
                Description/ ICD-9-C M
            </td>
            <td>
                Description/ ICD-9-C M
            </td>
        </tr>
        <tr>
            <td valign="top" class="ICDText">
                <u>(M1020) Primary Diagnosis<br />
                </u>a.<%=Html.TextBox("FollowUp_M1020PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1020PrimaryDiagnosis" })%>
                <br />
                <br />
            </td>
            <td valign="top">
                <u>(V-codes are allowed)<br />
                </u>a.<%=Html.TextBox("FollowUp_M1020ICD9M", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1020ICD9M" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1020SymptomControlRating" id="FollowUp_M1020SymptomControlRating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td valign="top">
                <u>(V- or E-codes NOT allowed)<br />
                </u>a.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesA3", data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesA3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MA3", data.ContainsKey("M1024ICD9MA3") ? data["M1024ICD9MA3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MA3" })%>
            </td>
            <td valign="top">
                <u>(V- or E-codes NOT allowed)</u>
                <br />
                a.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesA4", data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesA4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MA4", data.ContainsKey("M1024ICD9MA4") ? data["M1024ICD9MA4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MA4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                <u>(M1022) Other Diagnoses<br />
                </u>b.<%=Html.TextBox("FollowUp_M1022PrimaryDiagnosis1", data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis1" })%>
                <br />
            </td>
            <td>
                <u>(V- or E-codes are allowed)<br />
                </u>b.<%=Html.TextBox("FollowUp_M1022ICD9M1", data.ContainsKey("M1022ICD9M1") ? data["M1022ICD9M1"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1022ICD9M1" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1022OtherDiagnose1Rating" id="FollowUp_M1022OtherDiagnose1Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                b.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesB3", data.ContainsKey("M1024PaymentDiagnosesB3") ? data["M1024PaymentDiagnosesB3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesB3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MB3", data.ContainsKey("M1024ICD9MB3") ? data["M1024ICD9MB3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MB3" })%>
            </td>
            <td>
                b.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesB4", data.ContainsKey("M1024PaymentDiagnosesB4") ? data["M1024PaymentDiagnosesB4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesB4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MB4", data.ContainsKey("M1024ICD9MB4") ? data["M1024ICD9MB4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MB4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                c.<%=Html.TextBox("FollowUp_M1022PrimaryDiagnosis2", data.ContainsKey("M1022PrimaryDiagnosis2") ? data["M1022PrimaryDiagnosis2"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis2" })%>
                <br />
            </td>
            <td>
                c.<%=Html.TextBox("FollowUp_M1022ICD9M2", data.ContainsKey("M1022ICD9M2") ? data["M1022ICD9M2"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1022ICD9M2" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1022OtherDiagnose2Rating" id="FollowUp_M1022OtherDiagnose2Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                c.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesC3", data.ContainsKey("M1024PaymentDiagnosesC3") ? data["M1024PaymentDiagnosesC3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesC3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MC3", data.ContainsKey("M1024ICD9MC3") ? data["M1024ICD9MC3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MC3" })%>
            </td>
            <td>
                c.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesC4", data.ContainsKey("M1024PaymentDiagnosesC4") ? data["M1024PaymentDiagnosesC4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesC4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MC4", data.ContainsKey("M1024ICD9MC4") ? data["M1024ICD9MC4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MC4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                d.<%=Html.TextBox("FollowUp_M1022PrimaryDiagnosis3", data.ContainsKey("M1022PrimaryDiagnosis3") ? data["M1022PrimaryDiagnosis3"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis3" })%>
                <br />
            </td>
            <td>
                d.<%=Html.TextBox("FollowUp_M1022ICD9M3", data.ContainsKey("M1022ICD9M3") ? data["M1022ICD9M3"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1022ICD9M3" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1022OtherDiagnose3Rating" id="FollowUp_M1022OtherDiagnose3Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                d.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesD3", data.ContainsKey("M1024PaymentDiagnosesD3") ? data["M1024PaymentDiagnosesD3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesD3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MD3", data.ContainsKey("M1024ICD9MD3") ? data["M1024ICD9MD3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MD3" })%>
            </td>
            <td>
                d.
                <%=Html.TextBox("FollowUp_M1024PaymentDiagnosesD4", data.ContainsKey("M1024PaymentDiagnosesD4") ? data["M1024PaymentDiagnosesD4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesD4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MD4", data.ContainsKey("M1024ICD9MD4") ? data["M1024ICD9MD4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MD4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                e.<%=Html.TextBox("FollowUp_M1022PrimaryDiagnosis4", data.ContainsKey("M1022PrimaryDiagnosis4") ? data["M1022PrimaryDiagnosis4"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis4" })%>
                <br />
            </td>
            <td>
                e.<%=Html.TextBox("FollowUp_M1022ICD9M4", data.ContainsKey("M1022ICD9M4") ? data["M1022ICD9M4"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1022ICD9M4" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1022OtherDiagnose4Rating" id="FollowUp_M1022OtherDiagnose4Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                e.
                <%=Html.TextBox("FollowUp_M1024PaymentDiagnosesE3", data.ContainsKey("M1024PaymentDiagnosesE3") ? data["M1024PaymentDiagnosesE3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesE3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9ME3", data.ContainsKey("M1024ICD9ME3") ? data["M1024ICD9ME3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9ME3" })%>
            </td>
            <td>
                e.
                <%=Html.TextBox("FollowUp_M1024PaymentDiagnosesE4", data.ContainsKey("M1024PaymentDiagnosesE4") ? data["M1024PaymentDiagnosesE4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesE4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9ME4", data.ContainsKey("M1024ICD9ME4") ? data["M1024ICD9ME4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9ME4" })%>
            </td>
        </tr>
        <tr>
            <td class="ICDText">
                f.<%=Html.TextBox("FollowUp_M1022PrimaryDiagnosis5", data.ContainsKey("M1022PrimaryDiagnosis5") ? data["M1022PrimaryDiagnosis5"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis5" })%>
                <br />
            </td>
            <td>
                f.<%=Html.TextBox("FollowUp_M1022ICD9M5", data.ContainsKey("M1022ICD9M5") ? data["M1022ICD9M5"].Answer : "", new { @class = "ICD", @id = "FollowUp_M1022ICD9M5" })%>
                <br />
                <b>Severity:</b>
                <select name="FollowUp_M1022OtherDiagnose5Rating" id="FollowUp_M1022OtherDiagnose5Rating"
                    class="pad">
                    <option value=" " selected="true"></option>
                    <option value="00">0</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                    <option value="03">3</option>
                    <option value="04">4</option>
                </select>
            </td>
            <td>
                f.
                <%=Html.TextBox("FollowUp_M1024PaymentDiagnosesF3", data.ContainsKey("M1024PaymentDiagnosesF3") ? data["M1024PaymentDiagnosesF3"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesF3" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MF3", data.ContainsKey("M1024ICD9MF3") ? data["M1024ICD9MF3"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MF3" })%>
            </td>
            <td>
                f.<%=Html.TextBox("FollowUp_M1024PaymentDiagnosesF4", data.ContainsKey("M1024PaymentDiagnosesF4") ? data["M1024PaymentDiagnosesF4"].Answer : "", new { @class = "diagnosisM1024 ICDText", @id = "FollowUp_M1024PaymentDiagnosesF4" })%>
                <br />
                <%=Html.TextBox("FollowUp_M1024ICD9MF4", data.ContainsKey("M1024ICD9MF4") ? data["M1024ICD9MF4"].Answer : "", new { @class = "ICDM1024 pad", @id = "FollowUp_M1024ICD9MF4" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1030) Therapies the patient receives at home: (Mark all that apply.)
            </div>
        </div>
        <div class="padding">
            <input name="FollowUp_M1030HomeTherapiesInfusion" value=" " type="hidden" />
            <input name="FollowUp_M1030HomeTherapiesInfusion" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ){ %>checked="checked"<% }%>'" />1
            - Intravenous or infusion therapy (excludes TPN)<br />
            <input name="FollowUp_M1030HomeTherapiesParNutrition" value=" " type="hidden" />
            <input name="FollowUp_M1030HomeTherapiesParNutrition" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ){ %>checked="checked"<% }%>'" />2
            - Parenteral nutrition (TPN or lipids)<br />
            <input name="FollowUp_M1030HomeTherapiesEntNutrition" value=" " type="hidden" />
            <input name="FollowUp_M1030HomeTherapiesEntNutrition" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ){ %>checked="checked"<% }%>'" />3
            - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial
            entry into the alimentary canal)<br />
            <input name="FollowUp_M1030HomeTherapiesNone" value=" " type="hidden" />
            <input name="FollowUp_M1030HomeTherapiesNone" value="1" type="checkbox" '<% if( data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />4
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
