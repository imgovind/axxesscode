﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyManagementForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M2100) Types and Sources of Assistance: Determine the level of caregiver ability
                and willingness to provide assistance for the following activities, if assistance
                is needed. (Check only one box in each row.)
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                    Type of Assistance
                </th>
                <th>
                    No assistance needed in this area
                </th>
                <th>
                    Caregiver(s) currently provide assistance
                </th>
                <th>
                    Caregiver(s) need training/ supportive services to provide assistance
                </th>
                <th>
                    Caregiver(s) not likely to provide assistance
                </th>
                <th>
                    Unclear if Caregiver(s) will provide assistance
                </th>
                <th>
                    Assistance needed, but no Caregiver(s) available
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100ADLAssistance", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "00", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "01", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "02", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "03", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "04", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ADLAssistance", "05", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100IADLAssistance", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "00", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "01", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "02", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "03", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "04", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100IADLAssistance", "05", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    c. Medication administration (e.g., oral, inhaled or injectable)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100MedicationAdministration", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "00", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "01", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "02", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "03", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "04", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicationAdministration", "05", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    d. Medical procedures/ treatments (e.g., changing wound dressing)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100MedicalProcedures", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "00", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "01", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "02", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "03", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "04", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100MedicalProcedures", "05", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral
                    nutrition, ventilator therapy equipment or supplies)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100ManagementOfEquipment", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "00", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "01", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "02", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "03", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "04", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100ManagementOfEquipment", "05", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    f. Supervision and safety (e.g., due to cognitive impairment
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100SupervisionAndSafety", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "00", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "01", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "02", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "03", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "04", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100SupervisionAndSafety", "05", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
            <tr>
                <td>
                    g. Advocacy or facilitation of patient's participation in appropriate medical care
                    (includes transporta-tion to or from appointments)
                </td>
                <td>
                    <%=Html.Hidden("DischargeFromAgency_M2100FacilitationPatientParticipation", " ", new { @id = "" })%>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "00", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "01", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "02", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "03", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "04", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                </td>
                <td>
                    <%=Html.RadioButton("DischargeFromAgency_M2100FacilitationPatientParticipation", "05", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s)
                (other than home health agency staff)?
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "01", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - At least daily<br />
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "02", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Three or more times per week<br />
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "03", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - One to two times per week<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "04", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Received, but less often than weekly<br />
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "05", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - No assistance received<br />
                <%=Html.RadioButton("DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance", "UK", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unknown [Omit “UK” option on DC]<br />
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
