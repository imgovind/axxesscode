﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table cellpadding="0" cellspacing="0">
        <%string[] newMedicationsLS = data.ContainsKey("485NewMedicationsLS") && data["485NewMedicationsLS"].Answer != "" ? data["485NewMedicationsLS"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="5">
                New Medications (locator #10)
            </th>
        </tr>
        <tr>
            <th>
                LS
            </th>
            <th>
                Start Date
            </th>
            <th>
                Medication/Dosage
            </th>
            <th>
                Classification
            </th>
            <th>
                Frequency/Route
            </th>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NewMedicationsLS" value=" " type="hidden" />
                <input name="Recertification_485NewMedicationsLS" value="1" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsStartDate1", data.ContainsKey("485NewMedicationsStartDate1") ? data["485NewMedicationsStartDate1"].Answer : "", new { @id = "Recertification_485NewMedicationsStartDate1", @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsDosage1", data.ContainsKey("485NewMedicationsDosage1") ? data["485NewMedicationsDosage1"].Answer : "", new { @id = "Recertification_485NewMedicationsDosage1",  @maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="Recertification_485NewMedicationsClassification1"
                    id="Recertification_485NewMedicationsClassification1">
                </select>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsFrequency1", data.ContainsKey("485NewMedicationsFrequency1") ? data["485NewMedicationsFrequency1"].Answer : "", new { @id = "Recertification_485NewMedicationsFrequency1", @maxlength="50",@style="width: 200px;" })%>
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NewMedicationsLS" value="2" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsStartDate2", data.ContainsKey("485NewMedicationsStartDate2") ? data["485NewMedicationsStartDate2"].Answer : "", new { @id = "Recertification_485NewMedicationsStartDate2", @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsDosage2", data.ContainsKey("485NewMedicationsDosage2") ? data["485NewMedicationsDosage2"].Answer : "", new { @id = "Recertification_485NewMedicationsDosage2",@maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="Recertification_485NewMedicationsClassification2"
                    id="Recertification_485NewMedicationsClassification2">
                </select>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsFrequency2", data.ContainsKey("485NewMedicationsFrequency2") ? data["485NewMedicationsFrequency2"].Answer : "", new { @id = "Recertification_485NewMedicationsFrequency2", @style = "width: 200px;", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NewMedicationsLS" value="3" type="checkbox" '<% if( newMedicationsLS!=null && newMedicationsLS.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsStartDate3", data.ContainsKey("485NewMedicationsStartDate3") ? data["485NewMedicationsStartDate3"].Answer : "", new { @id = "Recertification_485NewMedicationsStartDate3",  @maxlength = "10" })%>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsDosage3", data.ContainsKey("485NewMedicationsDosage3") ? data["485NewMedicationsDosage3"].Answer : "", new { @id = "Recertification_485NewMedicationsDosage3",  @maxlength = "20" })%>
            </td>
            <td>
                <select style="width: 150px;" name="Recertification_485NewMedicationsClassification3"
                    id="Recertification_485NewMedicationsClassification3">
                </select>
            </td>
            <td>
                <%=Html.TextBox("Recertification_485NewMedicationsFrequency3", data.ContainsKey("485NewMedicationsFrequency3") ? data["485NewMedicationsFrequency3"].Answer : "", new { @id = "Recertification_485NewMedicationsFrequency3", @style = "width: 200px;", @maxlength = "50" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="100%">
                Medication Administration Record
            </th>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Time:</strong>&nbsp;
                <%=Html.TextBox("Recertification_GenericMedRecTime", data.ContainsKey("GenericMedRecTime") ? data["GenericMedRecTime"].Answer : "", new { @id = "Recertification_GenericMedRecTime", @size = "6" })%>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Medication</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecMedication", data.ContainsKey("GenericMedRecMedication") ? data["GenericMedRecMedication"].Answer : "", new { @id = "Recertification_GenericMedRecMedication", @size="30", @maxlength="50" })%>
            </td>
            <td>
                <strong>Dose</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecDose", data.ContainsKey("GenericMedRecDose") ? data["GenericMedRecDose"].Answer : "", new { @id = "Recertification_GenericMedRecDose", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>Route</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecRoute", data.ContainsKey("GenericMedRecRoute") ? data["GenericMedRecRoute"].Answer : "", new { @id = "Recertification_GenericMedRecRoute", @size = "30", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Frequency</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecFrequency", data.ContainsKey("GenericMedRecFrequency") ? data["GenericMedRecFrequency"].Answer : "", new { @id = "Recertification_GenericMedRecFrequency", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>PRN Reason</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecPRN", data.ContainsKey("GenericMedRecPRN") ? data["GenericMedRecPRN"].Answer : "", new { @id = "Recertification_GenericMedRecPRN", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>Location</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecLocation", data.ContainsKey("GenericMedRecLocation") ? data["GenericMedRecLocation"].Answer : "", new { @id = "Recertification_GenericMedRecLocation", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                <strong>Patient Response</strong><br />
                <%=Html.TextBox("Recertification_GenericMedRecResponse", data.ContainsKey("GenericMedRecResponse") ? data["GenericMedRecResponse"].Answer : "", new { @id = "Recertification_GenericMedRecResponse", @size = "30", @maxlength = "50" })%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                <strong>Comment</strong><br />
                <%=Html.TextArea("Recertification_GenericMedRecComments", data.ContainsKey("GenericMedRecComments") ? data["GenericMedRecComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericMedRecComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>
                IV Access
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Does patient have IV access? </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericIVAccess", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericIVAccess", "1", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;Yes
                    </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericIVAccess", "0", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;No
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Type:</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericIVAccessType", data.ContainsKey("GenericIVAccessType") ? data["GenericIVAccessType"].Answer : "", new { @id = "Recertification_GenericIVAccessType", @size = "50", @maxlength = "50" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Date of Insertion:</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericIVAccessDate", data.ContainsKey("GenericIVAccessDate") ? data["GenericIVAccessDate"].Answer : "", new { @id = "Recertification_GenericIVAccessDate", @size = "11", @maxlength = "11" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Date of Last Dressing Change:</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericIVAccessDressingChange", data.ContainsKey("GenericIVAccessDressingChange") ? data["GenericIVAccessDressingChange"].Answer : "", new { @id = "Recertification_GenericIVAccessDressingChange", @size = "11", @maxlength = "11" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2030) Management of Injectable Medications: Patient's current ability to prepare
                and take all prescribed injectable medications reliably and safely, including administration
                of correct dosage at the appropriate times/intervals. Excludes IV medications.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("Recertification_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct medication(s) and proper dosage(s) at the
            correct times.<br />
            <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take injectable medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person based on the frequency of the injection<br />
            <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take injectable medication unless administered by another person.<br />
            <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No injectable medications prescribed.
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] medicationInterventions = data.ContainsKey("485MedicationInterventions") && data["485MedicationInterventions"].Answer != "" ? data["485MedicationInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485MedicationInterventions" value=" " />
                <input name="Recertification_485MedicationInterventions" value="1" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient filling medication box to determine if patient is preparing
                correctly
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="2" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess caregiver filling medication box to determine if caregiver is preparing
                correctly
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="3" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to determine if the
                <%var determineFrequencEachMedPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DetermineFrequencEachMedPerson") && data["485DetermineFrequencEachMedPerson"].Answer != "" ? data["485DetermineFrequencEachMedPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DetermineFrequencEachMedPerson", determineFrequencEachMedPerson)%>
                is able to identify the correct dose, route, and frequency of each medication
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="4" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess if the
                <%var assessIndicationEachMedPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessIndicationEachMedPerson") && data["485AssessIndicationEachMedPerson"].Answer != "" ? data["485AssessIndicationEachMedPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485AssessIndicationEachMedPerson", assessIndicationEachMedPerson)%>
                can verbalize an understanding of the indication for each medication
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="5" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to establish reminders to alert patient to take medications at correct times
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input type="checkbox" name="Recertification_485MedicationInterventions" value="6" '<% if( medicationInterventions!=null && medicationInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess the
                <%var assessOpenMedContainersPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessOpenMedContainersPerson") && data["485AssessOpenMedContainersPerson"].Answer != "" ? data["485AssessOpenMedContainersPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485AssessOpenMedContainersPerson", assessOpenMedContainersPerson)%>
                ability to open medication containers and determine the proper dose that should
                be administered
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="7" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedicationRegimen = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedicationRegimen") && data["485InstructMedicationRegimen"].Answer != "" ? data["485InstructMedicationRegimen"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructMedicationRegimen", instructMedicationRegimen)%>
                on medication regimen dose, indications, side effects, and interactions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="8" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to remove any duplicate or expired medications to prevent confusion with medication
                regimen
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="9" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("9")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to observe patient drawing up injectable medications to determine if patient
                is able to draw up the correct dose
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485AssessAdminInjectMeds" value="10" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("10")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess the
                <%var assessAdminInjectMedsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AssessAdminInjectMedsPerson") && data["485AssessAdminInjectMedsPerson"].Answer != "" ? data["485AssessAdminInjectMedsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485AssessAdminInjectMedsPerson", assessAdminInjectMedsPerson)%>
                administering injectable medications to determine if proper technique is utilized
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="11" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("11")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to report to physician if drug therapy appears to be ineffective
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="12" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("12")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <select name="Recertification_485InstructHighRiskMedsPerson" id="Recertification_485InstructHighRiskMedsPerson">
                    <option value="Patient/Caregiver">Patient/Caregiver</option>
                    <option value="Patient">Patient</option>
                    <option value="Caregiver">Caregiver</option>
                </select>
                on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets,
                sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle
                relaxants
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="13" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("13")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSignsSymptomsIneffectiveDrugPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructSignsSymptomsIneffectiveDrugPerson") && data["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer != "" ? data["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructSignsSymptomsIneffectiveDrugPerson", instructSignsSymptomsIneffectiveDrugPerson)%>
                on signs and symptoms of ineffective drug therapy to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="14" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("14")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedSideEffectsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedSideEffectsPerson") && data["485InstructMedSideEffectsPerson"].Answer != "" ? data["485InstructMedSideEffectsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructMedSideEffectsPerson", instructMedSideEffectsPerson)%>
                on medication side effects to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="15" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("15")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructMedReactionsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMedReactionsPerson") && data["485InstructMedReactionsPerson"].Answer != "" ? data["485InstructMedReactionsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructMedReactionsPerson", instructMedReactionsPerson)%>
                on medication reactions to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="16" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("16")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer IV
                <%=Html.TextBox("Recertification_485AdministerIVType", data.ContainsKey("485AdministerIVType") ? data["485AdministerIVType"].Answer : "", new { @id = "Recertification_485AdministerIVType", @size = "15", @maxlength = "15" })%>
                at rate of
                <%=Html.TextBox("Recertification_485AdministerIVRate", data.ContainsKey("485AdministerIVRate") ? data["485AdministerIVRate"].Answer : "", new { @id = "Recertification_485AdministerIVRate", @size = "15", @maxlength = "15" })%>
                via
                <%=Html.TextBox("Recertification_485AdministerIVVia", data.ContainsKey("485AdministerIVVia") ? data["485AdministerIVVia"].Answer : "", new { @id = "Recertification_485AdministerIVVia", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485AdministerIVEvery", data.ContainsKey("485AdministerIVEvery") ? data["485AdministerIVEvery"].Answer : "", new { @id = "Recertification_485AdministerIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="17" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("17")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructAdministerIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructAdministerIVPerson") && data["485InstructAdministerIVPerson"].Answer != "" ? data["485InstructAdministerIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructAdministerIVPerson", instructAdministerIVPerson)%>
                to administer IV at rate of
                <%=Html.TextBox("Recertification_485InstructAdministerIVRate", data.ContainsKey("485InstructAdministerIVRate") ? data["485InstructAdministerIVRate"].Answer : "", new { @id = "Recertification_485InstructAdministerIVRate", @size = "15", @maxlength = "15" })%>
                via
                <%=Html.TextBox("Recertification_485InstructAdministerIVVia", data.ContainsKey("485InstructAdministerIVVia") ? data["485InstructAdministerIVVia"].Answer : "", new { @id = "Recertification_485InstructAdministerIVVia", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485InstructAdministerIVEvery", data.ContainsKey("485InstructAdministerIVEvery") ? data["485InstructAdministerIVEvery"].Answer : "", new { @id = "Recertification_485InstructAdministerIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="18" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("18")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change peripheral IV catheter every 72 hours with
                <%=Html.TextBox("Recertification_485ChangePeripheralIVGauge", data.ContainsKey("485ChangePeripheralIVGauge") ? data["485ChangePeripheralIVGauge"].Answer : "", new { @id = "Recertification_485ChangePeripheralIVGauge", @size = "15", @maxlength = "15" })%>
                gauge
                <%=Html.TextBox("Recertification_485ChangePeripheralIVWidth", data.ContainsKey("485ChangePeripheralIVWidth") ? data["485ChangePeripheralIVWidth"].Answer : "", new { @id = "Recertification_485ChangePeripheralIVWidth", @size = "15", @maxlength = "15" })%>
                inch angiocath
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="19" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("19")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to flush peripheral IV with
                <%=Html.TextBox("Recertification_485FlushPeripheralIVWith", data.ContainsKey("485FlushPeripheralIVWith") ? data["485FlushPeripheralIVWith"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("Recertification_485FlushPeripheralIVOf", data.ContainsKey("485FlushPeripheralIVOf") ? data["485FlushPeripheralIVOf"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485FlushPeripheralIVEvery", data.ContainsKey("485FlushPeripheralIVEvery") ? data["485FlushPeripheralIVEvery"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="20" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("20")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructFlushPerpheralIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructFlushPerpheralIVPerson") && data["485InstructFlushPerpheralIVPerson"].Answer != "" ? data["485InstructFlushPerpheralIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructFlushPerpheralIVPerson", instructFlushPerpheralIVPerson)%>
                to flush peripheral IV with
                <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVWith", data.ContainsKey("485InstructFlushPerpheralIVWith") ? data["485InstructFlushPerpheralIVWith"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVOf", data.ContainsKey("485InstructFlushPerpheralIVOf") ? data["485InstructFlushPerpheralIVOf"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVEvery", data.ContainsKey("485InstructFlushPerpheralIVEvery") ? data["485InstructFlushPerpheralIVEvery"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="21" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("21")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change central line dressing every
                <%=Html.TextBox("Recertification_485ChangeCentralLineEvery", data.ContainsKey("485ChangeCentralLineEvery") ? data["485ChangeCentralLineEvery"].Answer : "", new { @id = "Recertification_485ChangeCentralLineEvery", @size = "15", @maxlength = "15" })%>
                using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="22" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("22")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructChangeCentralLinePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructChangeCentralLinePerson") && data["485InstructChangeCentralLinePerson"].Answer != "" ? data["485InstructChangeCentralLinePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructChangeCentralLinePerson", instructChangeCentralLinePerson)%>
                to change central line dressing every
                <%=Html.TextBox("Recertification_485InstructChangeCentralLineEvery", data.ContainsKey("485InstructChangeCentralLineEvery") ? data["485InstructChangeCentralLineEvery"].Answer : "", new { @id = "Recertification_485InstructChangeCentralLineEvery", @size = "15", @maxlength = "15" })%>
                using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="23" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("23")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to flush central line with
                <%=Html.TextBox("Recertification_485FlushCentralLineWith", data.ContainsKey("485FlushCentralLineWith") ? data["485FlushCentralLineWith"].Answer : "", new { @id = "Recertification_485FlushCentralLineWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("Recertification_485FlushCentralLineOf", data.ContainsKey("485FlushCentralLineOf") ? data["485FlushCentralLineOf"].Answer : "", new { @id = "Recertification_485FlushCentralLineOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485FlushCentralLineEvery", data.ContainsKey("485FlushCentralLineEvery") ? data["485FlushCentralLineEvery"].Answer : "", new { @id = "Recertification_485FlushCentralLineEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="24" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("24")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructFlushCentralLinePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructFlushCentralLinePerson") && data["485InstructFlushCentralLinePerson"].Answer != "" ? data["485InstructFlushCentralLinePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructFlushCentralLinePerson", instructFlushCentralLinePerson)%>
                to flush central line with
                <%=Html.TextBox("Recertification_485InstructFlushCentralLineWith", data.ContainsKey("485InstructFlushCentralLineWith") ? data["485InstructFlushCentralLineWith"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("Recertification_485InstructFlushCentralLineOf", data.ContainsKey("485InstructFlushCentralLineOf") ? data["485InstructFlushCentralLineOf"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485InstructFlushCentralLineEvery", data.ContainsKey("485InstructFlushCentralLineEvery") ? data["485InstructFlushCentralLineEvery"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="25" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("25")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to access
                <%=Html.TextBox("Recertification_485AccessPortType", data.ContainsKey("485AccessPortType") ? data["485AccessPortType"].Answer : "", new { @id = "Recertification_485AccessPortType", @size = "15", @maxlength = "15" })%>
                port every
                <%=Html.TextBox("Recertification_485AccessPortTypeEvery", data.ContainsKey("485AccessPortTypeEvery") ? data["485AccessPortTypeEvery"].Answer : "", new { @id = "Recertification_485AccessPortTypeEvery", @size = "15", @maxlength = "15" })%>
                and flush with
                <%=Html.TextBox("Recertification_485AccessPortTypeWith", data.ContainsKey("485AccessPortTypeWith") ? data["485AccessPortTypeWith"].Answer : "", new { @id = "Recertification_485AccessPortTypeWith", @size = "15", @maxlength = "15" })%>
                cc of
                <%=Html.TextBox("Recertification_485AccessPortTypeOf", data.ContainsKey("485AccessPortTypeOf") ? data["485AccessPortTypeOf"].Answer : "", new { @id = "Recertification_485AccessPortTypeOf", @size = "15", @maxlength = "15" })%>
                every
                <%=Html.TextBox("Recertification_485AccessPortTypeFrequency", data.ContainsKey("485AccessPortTypeFrequency") ? data["485AccessPortTypeFrequency"].Answer : "", new { @id = "Recertification_485AccessPortTypeFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="26" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("26")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change
                <%=Html.TextBox("Recertification_485ChangePortDressingType", data.ContainsKey("485ChangePortDressingType") ? data["485ChangePortDressingType"].Answer : "", new { @id = "Recertification_485ChangePortDressingType", @size = "15", @maxlength = "15" })%>
                port dressing using sterile technique every
                <%=Html.TextBox("Recertification_485ChangePortDressingEvery", data.ContainsKey("485ChangePortDressingEvery") ? data["485ChangePortDressingEvery"].Answer : "", new { @id = "Recertification_485ChangePortDressingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="27" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("27")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructPortDressingPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructPortDressingPerson") && data["485InstructPortDressingPerson"].Answer != "" ? data["485InstructPortDressingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructPortDressingPerson", instructPortDressingPerson)%>
                to change
                <%=Html.TextBox("Recertification_485InstructPortDressingType", data.ContainsKey("485InstructPortDressingType") ? data["485InstructPortDressingType"].Answer : "", new { @id = "Recertification_485InstructPortDressingType", @size = "15", @maxlength = "15" })%>
                port dressing using sterile technique every
                <%=Html.TextBox("Recertification_485InstructPortDressingEvery", data.ContainsKey("485InstructPortDressingEvery") ? data["485InstructPortDressingEvery"].Answer : "", new { @id = "Recertification_485InstructPortDressingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="28" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("28")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change IV tubing every
                <%=Html.TextBox("Recertification_485ChangeIVTubingEvery", data.ContainsKey("485ChangeIVTubingEvery") ? data["485ChangeIVTubingEvery"].Answer : "", new { @id = "Recertification_485ChangeIVTubingEvery", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationInterventions" value="29" type="checkbox" '<% if( medicationInterventions!=null && medicationInterventions.Contains("29")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructInfectionSignsSymptomsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructInfectionSignsSymptomsPerson") && data["485InstructInfectionSignsSymptomsPerson"].Answer != "" ? data["485InstructInfectionSignsSymptomsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructInfectionSignsSymptomsPerson", instructInfectionSignsSymptomsPerson)%>
                on signs and symptoms of infection and infiltration
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var medicationInterventionTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485MedicationInterventionTemplates") && data["485MedicationInterventionTemplates"].Answer != "" ? data["485MedicationInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485MedicationInterventionTemplates", medicationInterventionTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485MedicationInterventionComments", data.ContainsKey("485MedicationInterventionComments") ? data["485MedicationInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485MedicationInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] medicationGoals = data.ContainsKey("485MedicationGoals") && data["485MedicationGoals"].Answer != "" ? data["485MedicationGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485MedicationGoals" value=" " />
                <input name="Recertification_485MedicationGoals" value="1" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free of adverse medication reactions during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="2" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medManagementIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedManagementIndependentPerson") && data["485MedManagementIndependentPerson"].Answer != "" ? data["485MedManagementIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485MedManagementIndependentPerson", medManagementIndependentPerson)%>
                will be independent with medication management by:
                <%=Html.TextBox("Recertification_485MedManagementIndependentDate", data.ContainsKey("485MedManagementIndependentDate") ? data["485MedManagementIndependentDate"].Answer : "", new { @id = "Recertification_485MedManagementIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="3" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeMedRegimenUnderstandingPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeMedRegimenUnderstandingPerson") && data["485VerbalizeMedRegimenUnderstandingPerson"].Answer != "" ? data["485VerbalizeMedRegimenUnderstandingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeMedRegimenUnderstandingPerson", verbalizeMedRegimenUnderstandingPerson)%>
                will verbalize understanding of medication regimen, dose, route, frequency, indications,
                and side effects by:
                <%=Html.TextBox("Recertification_485VerbalizeMedRegimenUnderstandingDate", data.ContainsKey("485VerbalizeMedRegimenUnderstandingDate") ? data["485VerbalizeMedRegimenUnderstandingDate"].Answer : "", new { @id = "Recertification_485VerbalizeMedRegimenUnderstandingDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="4" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medAdminIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedAdminIndependentPerson") && data["485MedAdminIndependentPerson"].Answer != "" ? data["485MedAdminIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485MedAdminIndependentPerson", medAdminIndependentPerson)%>
                will be independent with
                <%=Html.TextBox("Recertification_485MedAdminIndependentWith", data.ContainsKey("485MedAdminIndependentWith") ? data["485MedAdminIndependentWith"].Answer : "", new { @id = "Recertification_485MedAdminIndependentWith", @size = "10", @maxlength = "10" })%>
                administration by:
                <%=Html.TextBox("Recertification_485MedAdminIndependentDate", data.ContainsKey("485MedAdminIndependentDate") ? data["485MedAdminIndependentDate"].Answer : "", new { @id = "Recertification_485MedAdminIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="5" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var medSetupIndependentPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485MedSetupIndependentPerson") && data["485MedSetupIndependentPerson"].Answer != "" ? data["485MedSetupIndependentPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485MedSetupIndependentPerson", medSetupIndependentPerson)%>
                will be independent with setting up medication boxes by:
                <%=Html.TextBox("Recertification_485MedSetupIndependentDate", data.ContainsKey("485MedSetupIndependentDate") ? data["485MedSetupIndependentDate"].Answer : "", new { @id = "Recertification_485MedSetupIndependentDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="6" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeEachMedIndicationPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeEachMedIndicationPerson") && data["485VerbalizeEachMedIndicationPerson"].Answer != "" ? data["485VerbalizeEachMedIndicationPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeEachMedIndicationPerson", verbalizeEachMedIndicationPerson)%>
                will be able to verbalize an understanding of the indications for each medication
                by:
                <%=Html.TextBox("Recertification_485VerbalizeEachMedIndicationDate", data.ContainsKey("485VerbalizeEachMedIndicationDate") ? data["485VerbalizeEachMedIndicationDate"].Answer : "", new { @id = "Recertification_485VerbalizeEachMedIndicationDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="7" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var correctDoseIdentifyPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485CorrectDoseIdentifyPerson") && data["485CorrectDoseIdentifyPerson"].Answer != "" ? data["485CorrectDoseIdentifyPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485CorrectDoseIdentifyPerson", correctDoseIdentifyPerson)%>
                will be able to identify the correct dose, route, and frequency of each medication
                by:
                <%=Html.TextBox("Recertification_485CorrectDoseIdentifyDate", data.ContainsKey("485CorrectDoseIdentifyDate") ? data["485CorrectDoseIdentifyDate"].Answer : "", new { @id = "Recertification_485CorrectDoseIdentifyDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="8" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                IV will remain patent and free from signs and symptoms of infection
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="9" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("9")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateCentralLineFlushPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateCentralLineFlushPerson") && data["485DemonstrateCentralLineFlushPerson"].Answer != "" ? data["485DemonstrateCentralLineFlushPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DemonstrateCentralLineFlushPerson", demonstrateCentralLineFlushPerson)%>
                will demonstrate understanding of flushing central line
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="10" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("10")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstratePeripheralIVLineFlushPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstratePeripheralIVLineFlushPerson") && data["485DemonstratePeripheralIVLineFlushPerson"].Answer != "" ? data["485DemonstratePeripheralIVLineFlushPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DemonstratePeripheralIVLineFlushPerson", demonstratePeripheralIVLineFlushPerson)%>
                will demonstrate understanding of flushing peripheral IV line
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="11" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("11")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateSterileDressingTechniquePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateSterileDressingTechniquePerson") && data["485DemonstrateSterileDressingTechniquePerson"].Answer != "" ? data["485DemonstrateSterileDressingTechniquePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DemonstrateSterileDressingTechniquePerson", demonstrateSterileDressingTechniquePerson)%>
                will demonstrate understanding of changing
                <%=Html.TextBox("Recertification_485DemonstrateSterileDressingTechniqueType", data.ContainsKey("485DemonstrateSterileDressingTechniqueType") ? data["485DemonstrateSterileDressingTechniqueType"].Answer : "", new { @id = "Recertification_485DemonstrateSterileDressingTechniqueType", @size = "10", @maxlength = "10" })%>
                dressing using sterile technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485MedicationGoals" value="12" type="checkbox" '<% if( medicationGoals!=null && medicationGoals.Contains("12")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateAdministerIVPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateAdministerIVPerson") && data["485DemonstrateAdministerIVPerson"].Answer != "" ? data["485DemonstrateAdministerIVPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DemonstrateAdministerIVPerson", demonstrateAdministerIVPerson)%>
                will demonstrate understanding of administering IV
                <%=Html.TextBox("Recertification_485DemonstrateAdministerIVType", data.ContainsKey("485DemonstrateAdministerIVType") ? data["485DemonstrateAdministerIVType"].Answer : "", new { @id = "Recertification_485DemonstrateAdministerIVType", @size = "10", @maxlength = "10" })%>
                at rate of
                <%=Html.TextBox("Recertification_485DemonstrateAdministerIVRate", data.ContainsKey("485DemonstrateAdministerIVRate") ? data["485DemonstrateAdministerIVRate"].Answer : "", new { @id = "Recertification_485DemonstrateAdministerIVRate", @size = "10", @maxlength = "10" })%>
                via
                <%=Html.TextBox("Recertification_485DemonstrateAdministerIVVia", data.ContainsKey("485DemonstrateAdministerIVVia") ? data["485DemonstrateAdministerIVVia"].Answer : "", new { @id = "Recertification_485DemonstrateAdministerIVVia", @size = "10", @maxlength = "10" })%>
                every
                <%=Html.TextBox("Recertification_485DemonstrateAdministerIVEvery", data.ContainsKey("485DemonstrateAdministerIVEvery") ? data["485DemonstrateAdministerIVEvery"].Answer : "", new { @id = "Recertification_485DemonstrateAdministerIVEvery", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var medicationGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485MedicationGoalTemplates") && data["485MedicationGoalTemplates"].Answer != "" ? data["485MedicationGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485MedicationGoalTemplates", medicationGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485MedicationGoalComments", data.ContainsKey("485MedicationGoalComments") ? data["485MedicationGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485MedicationGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
