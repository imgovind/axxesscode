﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationTherapyNeedForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode
                for which this assessment will define a case mix group, what is the indicated need
                for therapy visits (total of reasonable and necessary physical, occupational, and
                speech-language pathology visits combined)? (Enter zero [ “000” ] if no therapy
                visits indicated.)
            </div>
        </div>
        <div class="padding">
            <%=Html.TextBox("Recertification_M2200NumberOfTherapyNeed", data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : "", new { @id = "Recertification_M2200NumberOfTherapyNeed"})%>
            &nbsp;Number of therapy visits indicated (total of physical, occupational and speech-language
            pathology combined).<br />
            <input name="Recertification_M2200TherapyNeed" value="" type="hidden" />
            <input name="Recertification_M2200TherapyNeed" value="1" type="checkbox" '<% if(data.ContainsKey("M2200TherapyNeed") && data["M2200TherapyNeed"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;NA
            - Not Applicable: No case mix group defined by this assessment.
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
