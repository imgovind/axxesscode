﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editTransfernotdischarge" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="TransferInPatientNotDischargedTitle" class="float_left">Edit Transfer to an
                Inpatient Facility - patient not discharged from an agency </span><span class="float_right">
                    <a href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);"
                        class="window_resize"></a><a href="javascript:void(0);" class="window_close">
                    </a></span>
        </div>
        <div class="abs window_content general_form oasisAssWindowContent">
            <div class="oasisAssWindowContainer">
                <div id="editTransferInPatientNotDischargedTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
                    <ul>
                        <li><a href="#editClinicalRecord_nottransfer">Clinical Record Items</a></li>
                        <li><a href="#editRiskassessment_nottransfer">Risk Assessment</a></li>
                        <li><a href="#editCardiacstatus_nottransfer">Cardiac Status</a></li>
                        <li><a href="#editMedications_nottransfer">Medications</a></li>
                        <li><a href="#editEmergentcare_nottransfer">Emergent Care</a></li>
                        <li><a href="#editDischardeAdd_nottransfer">Transfer</a></li>
                    </ul>
                     <div style="width: 179px;">
                        <input id="transferInPatientNotDischargedValidation" type="button" value="Validate"
                            onclick="TransferNotDischarge.Validate(); JQD.open_window('#validation');" /></div>
                    <div id="editClinicalRecord_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedDemographicsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0010) CMS Certification Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0010CertificationNumber" name="TransferInPatientNotDischarged_M0010CertificationNumber"
                                        type="text" class="text" value="" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0014) Branch State:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="TransferInPatientNotDischarged_M0014BranchState"
                                        id="TransferInPatientNotDischarged_M0014BranchState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0016) Branch ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0016BranchId" name="TransferInPatientNotDischarged_M0016BranchId"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0018) National Provider Identifier (NPI)</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0018NationalProviderId" name="TransferInPatientNotDischarged_M0018NationalProviderId"
                                        type="text" class="text" /><br />
                                    <input name="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown" type="hidden"
                                        value=" " />
                                    <input type="checkbox" id="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown"
                                        name="TransferInPatientNotDischarged_M0018NationalProviderIdUnknown" value="1" />&nbsp;UK
                                    – Unknown or Not Available</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0020) Patient ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0020PatientIdNumber" name="TransferInPatientNotDischarged_M0020PatientIdNumber"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0030) Start of Care Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0030SocDate" name="TransferInPatientNotDischarged_M0030SocDate"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0040) Patient Name:</div>
                                </div>
                                <div class="right marginOasis">
                                    <div class="padding">
                                        Suffix :
                                        <input id="TransferInPatientNotDischarged_M0040Suffix" name="TransferInPatientNotDischarged_M0040Suffix"
                                            style="width: 20px;" type="text" class="text" />
                                        &nbsp; First :
                                        <input id="TransferInPatientNotDischarged_M0040FirstName" name="TransferInPatientNotDischarged_M0040FirstName"
                                            type="text" class="text" />
                                        <br />
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MI :
                                        <input id="TransferInPatientNotDischarged_M0040MI" name="TransferInPatientNotDischarged_M0040MI"
                                            style="width: 20px;" type="text" class="text" />&nbsp; &nbsp;Last:
                                        <input id="TransferInPatientNotDischarged_M0040LastName" name="TransferInPatientNotDischarged_M0040LastName"
                                            type="text" class="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0050) Patient State of Residence:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="TransferInPatientNotDischarged_M0050PatientState"
                                        id="TransferInPatientNotDischarged_M0050PatientState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0060) Patient Zip Code:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0060PatientZipCode" name="TransferInPatientNotDischarged_M0060PatientZipCode"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0063) Medicare Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0063PatientMedicareNumber" name="TransferInPatientNotDischarged_M0063PatientMedicareNumber"
                                        type="text" class="text" /><br />
                                    <input name="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown" name="TransferInPatientNotDischarged_M0063PatientMedicareNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicare</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0064) Social Security Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0064PatientSSN" name="TransferInPatientNotDischarged_M0064PatientSSN"
                                        type="text" class="text" /><br />
                                    <input name="TransferInPatientNotDischarged_M0064PatientSSNUnknown" type="hidden"
                                        value=" " />
                                    <input id="TransferInPatientNotDischarged_M0064PatientSSNUnknown" name="TransferInPatientNotDischarged_M0064PatientSSNUnknown"
                                        type="checkbox" value="1" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0065) Medicaid Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0065PatientMedicaidNumber" name="TransferInPatientNotDischarged_M0065PatientMedicaidNumber"
                                        type="text" class="text" /><br />
                                    <input name="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown" name="TransferInPatientNotDischarged_M0065PatientMedicaidNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicaid</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0066) Birth Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0066PatientDoB" name="TransferInPatientNotDischarged_M0066PatientDoB"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow  title">
                                    <div class="padding">
                                        (M0069) Gender:</div>
                                </div>
                                <div class="right marginOasis">
                                    <%=Html.Hidden("TransferInPatientNotDischarged_M0069Gender", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0069Gender", "1", new { @id = "" })%>Male
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0069Gender", "2", new { @id = "" })%>Female
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0080) Discipline of Person Completing Assessment:
                                    </div>
                                </div>
                                <div class="padding">
                                    <%=Html.Hidden("TransferInPatientNotDischarged_M0080DisciplinePerson", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "01", new { @id = "" })%>&nbsp;1
                                    - RN
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "02", new { @id = "" })%>&nbsp;2
                                    - PT
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "03", new { @id = "" })%>&nbsp;3
                                    - SLP/ST
                                    <%=Html.RadioButton("TransferInPatientNotDischarged_M0080DisciplinePerson", "04", new { @id = "" })%>&nbsp;4
                                    - OT
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0090) Date Assessment Completed:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="TransferInPatientNotDischarged_M0090AssessmentCompleted" name="TransferInPatientNotDischarged_M0090AssessmentCompleted"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis assessmentType">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow margin">
                                    <u>Start/Resumption of Care</u></div>
                                <div class="insiderow margin">
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="01" />&nbsp;1
                                    – Start of care—further visits planned<br />
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="03" />&nbsp;3
                                    – Resumption of care (after inpatient stay)<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow">
                                    <u>Follow-Up</u></div>
                                <div class="insiderow">
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="04" />&nbsp;4
                                    – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="05" />&nbsp;5
                                    – Other follow-up [ Go to M0110 ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Transfer to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="06" />&nbsp;6
                                    – Transferred to an inpatient facility—patient not discharged from agency [ Go to
                                    M1040]<br />
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="07" />&nbsp;7
                                    – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="08" />&nbsp;8
                                    – Death at home [ Go to M0903 ]<br />
                                    <input name="TransferInPatientNotDischarged_M0100AssessmentType" type="radio" value="09" />&nbsp;9
                                    – Discharge from agency [ Go to M1040 ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0102) Date of Physician-ordered Start of Care (Resumption of Care): If the physician
                                        indicated a specific start of care (resumption of care) date when the patient was
                                        referred for home health services, record the date specified.</div>
                                </div>
                                <div class="padding">
                                    <input id="TransferInPatientNotDischarged_M0102PhysicianOrderedDate" name="TransferInPatientNotDischarged_M0102PhysicianOrderedDate"
                                        type="text" class="text" />
                                    [ Go to M0110, if date entered ]<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable"
                                        value="" />
                                    <input id="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable"
                                        name="TransferInPatientNotDischarged_M0102PhysicianOrderedDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA –No specific SOC date ordered by physician
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0104) Date of Referral: Indicate the date that the written or verbal referral
                                        for initiation or resumption of care was received by the HHA.
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="TransferInPatientNotDischarged_M0104ReferralDate" name="TransferInPatientNotDischarged_M0104ReferralDate"
                                        type="text" class="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this
                                        assessment will define a case mix group an “early” episode or a “later” episode
                                        in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    </div>
                                </div>
                                <div class="padding">
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0110EpisodeTiming" value="" />
                                    <input name="TransferInPatientNotDischarged_M0110EpisodeTiming" type="radio" value="01" />&nbsp;1
                                    - Early
                                    <input name="TransferInPatientNotDischarged_M0110EpisodeTiming" type="radio" value="02" />&nbsp;2
                                    - Later
                                    <input name="TransferInPatientNotDischarged_M0110EpisodeTiming" type="radio" value="UK" />&nbsp;UK
                                    - Unknown
                                    <input name="TransferInPatientNotDischarged_M0110EpisodeTiming" type="radio" value="NA" />&nbsp;NA
                                    - Not Applicable: No Medicare case mix group
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0140) Race/Ethnicity: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceAMorAN" value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceAMorAN" type="checkbox" value="1" />&nbsp;1
                                    - American Indian or Alaska Native<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceAsia" value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceAsia" type="checkbox" value="1" />&nbsp;2
                                    - Asian<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceBalck" value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceBalck" type="checkbox" value="1" />&nbsp;3
                                    - Black or African-American<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceHispanicOrLatino"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceHispanicOrLatino" type="checkbox"
                                        value="1" />&nbsp;4 - Hispanic or Latino<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceNHOrPI" value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceNHOrPI" type="checkbox" value="1" />&nbsp;5
                                    - Native Hawaiian or Pacific Islander<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0140RaceWhite" value="" />
                                    <input name="TransferInPatientNotDischarged_M0140RaceWhite" type="checkbox" value="1" />&nbsp;6
                                    - White
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceNone"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceNone" type="checkbox"
                                        value="1" />&nbsp;0 - None; no charge for current services<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCREFFS"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCREFFS" type="checkbox"
                                        value="1" />&nbsp;1 - Medicare (traditional fee-for-service)<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCREHMO"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCREHMO" type="checkbox"
                                        value="1" />&nbsp;2 - Medicare (HMO/managed care/Advantage plan)<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMCAIDFFS"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceMCAIDFFS" type="checkbox"
                                        value="1" />&nbsp;3 - Medicaid (traditional fee-for-service)<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceMACIDHMO"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceMACIDHMO" type="checkbox"
                                        value="1" />&nbsp;4 - Medicaid (HMO/managed care)
                                    <br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceWRKCOMP"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceWRKCOMP" type="checkbox"
                                        value="1" />&nbsp;5 - Workers' compensation<br />
                                    <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceTITLPRO"
                                        value="" />
                                    <input name="TransferInPatientNotDischarged_M0150PaymentSourceTITLPRO" type="checkbox"
                                        value="1" />&nbsp;6 - Title programs (e.g., Title III, V, or XX)<br />
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceOTHGOVT"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourceOTHGOVT" type="checkbox"
                                    value="1" />&nbsp;7 - Other government (e.g., TriCare, VA, etc.)<br />
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourcePRVINS"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourcePRVINS" type="checkbox"
                                    value="1" />&nbsp;8 - Private insurance<br />
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourcePRVHMO"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourcePRVHMO" type="checkbox"
                                    value="1" />&nbsp;9 - Private HMO/managed care<br />
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceSelfPay"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourceSelfPay" type="checkbox"
                                    value="1" />&nbsp;10 - Self-pay<br />
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceOtherSRS"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourceOtherSRS" type="checkbox"
                                    value="1" />&nbsp;11 - Other (specify)&nbsp;&nbsp;&nbsp;<input type="text" name="TransferInPatientNotDischarged_M0150PaymentSourceOther"
                                        id="TransferInPatientNotDischarged_M0150PaymentSourceOther" /><br />
                                <input type="hidden" name="TransferInPatientNotDischarged_M0150PaymentSourceUnknown"
                                    value="" />
                                <input name="TransferInPatientNotDischarged_M0150PaymentSourceUnknown" type="checkbox"
                                    value="1" />&nbsp;UK - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editRiskassessment_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedRiskAssessmentForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your
                                        agency for this year’s influenza season (October 1 through March 31) during this
                                        episode of care?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1040InfluenzaVaccine" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M1040InfluenzaVaccine" type="radio" value="00" />&nbsp;0
                                    - No<br />
                                    <input name="TransferInPatientNotDischarged_M1040InfluenzaVaccine" type="radio" value="01" />&nbsp;1
                                    - Yes [ Go to M1050 ]<br />
                                    <input name="TransferInPatientNotDischarged_M1040InfluenzaVaccine" type="radio" value="NA" />&nbsp;NA
                                    - Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge)
                                    is outside this influenza season. [ Go to M1050 ]<br />
                                </div>
                            </div>
                            <div class="insideCol" id="transferNotDischarge_M1045">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1045) Reason Influenza Vaccine not received: If the patient did not receive the
                                        influenza vaccine from your agency during this episode of care, state reason:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="hidden" value=" " />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="01" />&nbsp;1 - Received from another health care provider
                                    (e.g., physician)<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="02" />&nbsp;2 - Received from your agency previously during
                                    this year’s flu season<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="03" />&nbsp;3 - Offered and declined<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="04" />&nbsp;4 - Assessed and determined to have medical contraindication(s)<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="05" />&nbsp;5 - Not indicated; patient does not meet age/condition
                                    guidelines for influenza vaccine<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="06" />&nbsp;6 - Inability to obtain vaccine due to declared
                                    shortage<br />
                                    <input name="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason"
                                        type="radio" value="07" />&nbsp;7 - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide
                                        vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1050PneumococcalVaccine" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M1050PneumococcalVaccine" type="radio"
                                        value="0" />&nbsp;0 - No<br />
                                    <input name="TransferInPatientNotDischarged_M1050PneumococcalVaccine" type="radio"
                                        value="1" />&nbsp;1 - Yes [ Go to M1500 at TRN; Go to M1230 at DC ]
                                </div>
                            </div>
                            <div class="insideCol" id="transferNotDischarge_M1055">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide
                                        vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge),
                                        state reason:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="radio"
                                        value="01" />&nbsp;1 - Patient has received PPV in the past<br />
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="radio"
                                        value="02" />&nbsp;2 - Offered and declined<br />
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="radio"
                                        value="03" />&nbsp;3 - Assessed and determined to have medical contraindication(s)<br />
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="radio"
                                        value="04" />&nbsp;4 - Not indicated; patient does not meet age/condition guidelines
                                    for PPV<br />
                                    <input name="TransferInPatientNotDischarged_M1055PPVNotReceivedReason" type="radio"
                                        value="05" />&nbsp;5 - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editCardiacstatus_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedCardiacStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart
                                        failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines
                                        (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous
                                        OASIS assessment?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1500HeartFailureSymptons" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M1500HeartFailureSymptons" type="radio"
                                        value="00" />&nbsp;0 - No [ Go to M2004 at TRN; Go to M1600 at DC ]<br />
                                    <input name="TransferInPatientNotDischarged_M1500HeartFailureSymptons" type="radio"
                                        value="01" />&nbsp;1 - Yes<br />
                                    <input name="TransferInPatientNotDischarged_M1500HeartFailureSymptons" type="radio"
                                        value="02" />&nbsp;2 - Not assessed [Go to M2004 at TRN; Go to M1600 at DC ]<br />
                                    <input name="TransferInPatientNotDischarged_M1500HeartFailureSymptons" type="radio"
                                        value="NA" />&nbsp;NA - Patient does not have diagnosis of heart failure [Go
                                    to M2004 at TRN; Go to M1600 at DC ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="transferNotDischarge_M1510">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure
                                        and has exhibited symptoms indicative of heart failure since the previous OASIS
                                        assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="00" />&nbsp;0 - No action taken<br />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="01" />&nbsp;1 - Patient’s physician (or other primary care practitioner)
                                    contacted the same day<br />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="02" />&nbsp;2 - Patient advised to get emergency treatment (e.g., call
                                    911 or go to emergency room)<br />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="03" />&nbsp;3 - Implemented physician-ordered patient-specific established
                                    parameters for treatment<br />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="04" />&nbsp;4 - Patient education or other clinical interventions<br />
                                    <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowup" type="checkbox"
                                        value="05" />&nbsp;5 - Obtained change in care plan orders (e.g., increased monitoring
                                    by agency, change in visit frequency, telehealth, etc.)
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editMedications_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedMedicationsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2004) Medication Intervention: If there were any clinically significant medication
                                        issues since the previous OASIS assessment, was a physician or the physician-designee
                                        contacted within one calendar day of the assessment to resolve clinically significant
                                        medication issues, including reconciliation?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M2004MedicationIntervention" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M2004MedicationIntervention" type="radio"
                                        value="00" />&nbsp;0 - No<br />
                                    <input name="TransferInPatientNotDischarged_M2004MedicationIntervention" type="radio"
                                        value="01" />&nbsp;1 - Yes<br />
                                    <input name="TransferInPatientNotDischarged_M2004MedicationIntervention" type="radio"
                                        value="NA" />&nbsp;NA - No clinically significant medication issues identified
                                    since the previous OASIS assessment
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS
                                        assessment, was the patient/caregiver instructed by agency staff or other health
                                        care provider to monitor the effectiveness of drug therapy, drug reactions, and
                                        side effects, and how and when to report problems that may occur?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="hidden" value=" " />
                                    <input name="TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="00" />&nbsp;0 - No<br />
                                    <input name="TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="01" />&nbsp;1 - Yes<br />
                                    <input name="TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="NA" />&nbsp;NA - Patient not taking any drugs
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editEmergentcare_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedEmergentCareForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2300) Emergent Care: Since the last time OASIS data were collected, has the patient
                                        utilized a hospital emergency department (includes holding/observation)?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M2300EmergentCare" type="hidden" value=" " />
                                    <input name="TransferInPatientNotDischarged_M2300EmergentCare" type="radio" value="00" />&nbsp;0
                                    - No [ Go to M2400 ]<br />
                                    <input name="TransferInPatientNotDischarged_M2300EmergentCare" type="radio" value="01" />&nbsp;1
                                    - Yes, used hospital emergency department WITHOUT hospital admission<br />
                                    <input name="TransferInPatientNotDischarged_M2300EmergentCare" type="radio" value="02" />&nbsp;2
                                    - Yes, used hospital emergency department WITH hospital admission<br />
                                    <input name="TransferInPatientNotDischarged_M2300EmergentCare" type="radio" value="UK" />&nbsp;UK
                                    - Unknown [ Go to M2400 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="transferNotDischarge_M2310">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent
                                        care (with or without hospitalization)? (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMed" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMed" type="checkbox"
                                            value="1" />&nbsp;1 - Improper medication administration, medication side effects,
                                        toxicity, anaphylaxis<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareFall" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareFall" type="checkbox"
                                            value="1" />&nbsp;2 - Injury caused by fall<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareResInf" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareResInf" type="checkbox"
                                            value="1" />&nbsp;3 - Respiratory infection (e.g., pneumonia, bronchitis)<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOtherResInf"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOtherResInf"
                                            type="checkbox" value="1" />&nbsp;4 - Other respiratory problem<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartFail"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartFail"
                                            type="checkbox" value="1" />&nbsp;5 - Heart failure (e.g., fluid overload)<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareCardiac" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareCardiac" type="checkbox"
                                            value="1" />&nbsp;6 - Cardiac dysrhythmia (irregular heartbeat)<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMyocardial"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMyocardial"
                                            type="checkbox" value="1" />&nbsp;7 - Myocardial infarction or chest pain<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartDisease"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHeartDisease"
                                            type="checkbox" value="1" />&nbsp;8 - Other heart disease<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareStroke" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareStroke" type="checkbox"
                                            value="1" />&nbsp;9 - Stroke (CVA) or TIA<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHypo" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareHypo" type="checkbox"
                                            value="1" />&nbsp;10 - Hypo/Hyperglycemia, diabetes out of control
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareGI" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareGI" type="checkbox"
                                            value="1" />&nbsp;11 - GI bleeding, obstruction, constipation, impaction<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDehMal" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDehMal" type="checkbox"
                                            value="1" />&nbsp;12 - Dehydration, malnutrition<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUrinaryInf"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUrinaryInf"
                                            type="checkbox" value="1" />&nbsp;13 - Urinary tract infection<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareIV" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareIV" type="checkbox"
                                            value="1" />&nbsp;14 - IV catheter-related infection or complication<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareWoundInf" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareWoundInf" type="checkbox"
                                            value="1" />&nbsp;15 - Wound infection or deterioration<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUncontrolledPain"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUncontrolledPain"
                                            type="checkbox" value="1" />&nbsp;16 - Uncontrolled pain<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMental" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareMental" type="checkbox"
                                            value="1" />&nbsp;17 - Acute mental/behavioral health problem<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDVT" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareDVT" type="checkbox"
                                            value="1" />&nbsp;18 - Deep vein thrombosis, pulmonary embolus<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOther" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareOther" type="checkbox"
                                            value="1" />&nbsp;19 - Other than above reasons<br />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUK" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2310ReasonForEmergentCareUK" type="checkbox"
                                            value="1" />&nbsp;UK - Reason unknown<br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editDischardeAdd_nottransfer" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editwOasisTransferInPatientNotDischargedAddForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Id", "")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
                        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous
                                        OASIS assessment, were the following interventions BOTH included in the physician-ordered
                                        plan of care AND implemented?
                                    </div>
                                </div>
                                <div class="margin">
                                    <table class="agency-data-table" id="Table7">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Plan / Intervention
                                                </th>
                                                <th>
                                                    No
                                                </th>
                                                <th>
                                                    Yes
                                                </th>
                                                <th colspan="2">
                                                    Not Applicable
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    a. Diabetic foot care including monitoring for the presence of skin lesions on the
                                                    lower extremities and patient/caregiver education on proper foot care
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DiabeticFootCare" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400DiabeticFootCare" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DiabeticFootCare" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DiabeticFootCare" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Patient is not diabetic or is bilateral amputee
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    b. Falls prevention interventions
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400FallsPreventionInterventions" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400FallsPreventionInterventions" type="radio"
                                                        value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400FallsPreventionInterventions" type="radio"
                                                        value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400FallsPreventionInterventions" type="radio"
                                                        value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for
                                                    falls since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    c. Depression intervention(s) such as medication, referral for other treatment,
                                                    or a monitoring plan for current treatment
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DepressionIntervention" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400DepressionIntervention" type="radio"
                                                        value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DepressionIntervention" type="radio"
                                                        value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400DepressionIntervention" type="radio"
                                                        value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment indicates patient did not meet criteria for depression AND patient
                                                    did not have diagnosis of depression since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    d. Intervention(s) to monitor and mitigate pain
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PainIntervention" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400PainIntervention" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PainIntervention" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PainIntervention" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment did not indicate pain since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    e. Intervention(s) to prevent pressure ulcers
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerIntervention" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerIntervention" type="radio"
                                                        value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerIntervention" type="radio"
                                                        value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerIntervention" type="radio"
                                                        value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment indicates the patient was not at risk of pressure ulcers since
                                                    the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    f. Pressure ulcer treatment based on principles of moist wound healing
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerTreatment" type="hidden"
                                                        value=" " />
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerTreatment" type="radio"
                                                        value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerTreatment" type="radio"
                                                        value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="TransferInPatientNotDischarged_M2400PressureUlcerTreatment" type="radio"
                                                        value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Dressings that support the principles of moist wound healing not indicated for this
                                                    patient’s pressure ulcers OR patient has no pressure ulcers with need for moist
                                                    wound healing
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2410) To which Inpatient Facility has the patient been admitted?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="radio"
                                        value="01" />&nbsp;1 - Hospital [ Go to M2430 ]<br />
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="radio"
                                        value="02" />&nbsp;2 - Rehabilitation facility [ Go to M0903 ]<br />
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="radio"
                                        value="03" />&nbsp;3 - Nursing home [ Go to M2440 ]<br />
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="radio"
                                        value="04" />&nbsp;4 - Hospice [ Go to M0903 ]<br />
                                    <input name="TransferInPatientNotDischarged_M2410TypeOfInpatientFacility" type="radio"
                                        value="NA" />&nbsp;NA - No inpatient facility admission [Omit “NA” option on
                                    TRN]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="transferNotDischarge_M2430">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2430) Reason for Hospitalization: For what reason(s) did the patient require hospitalization?
                                        (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMed" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMed" type="checkbox"
                                            value="1" />&nbsp;1 - Improper medication administration, medication side effects,
                                        toxicity, anaphylaxis<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationFall" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationFall" type="checkbox"
                                            value="1" />&nbsp;2 - Injury caused by fall<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationInfection"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationInfection"
                                            type="checkbox" value="1" />&nbsp;3 - Respiratory infection (e.g., pneumonia,
                                        bronchitis)<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOtherRP"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOtherRP"
                                            type="checkbox" value="1" />&nbsp;4 - Other respiratory problem<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartFail"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartFail"
                                            type="checkbox" value="1" />&nbsp;5 - Heart failure (e.g., fluid overload)<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationCardiac"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationCardiac"
                                            type="checkbox" value="1" />&nbsp;6 - Cardiac dysrhythmia (irregular heartbeat)<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMyocardial"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMyocardial"
                                            type="checkbox" value="1" />&nbsp;7 - Myocardial infarction or chest pain<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartDisease"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartDisease"
                                            type="checkbox" value="1" />&nbsp;8 - Other heart disease<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationStroke"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationStroke"
                                            type="checkbox" value="1" />&nbsp;9 - Stroke (CVA) or TIA<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHypo" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHypo" type="checkbox"
                                            value="1" />&nbsp;10 - Hypo/Hyperglycemia, diabetes out of control
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationGI" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationGI" type="checkbox"
                                            value="1" />&nbsp;11 - GI bleeding, obstruction, constipation, impaction<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDehMal"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDehMal"
                                            type="checkbox" value="1" />&nbsp;12 - Dehydration, malnutrition<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUrinaryInf"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUrinaryInf"
                                            type="checkbox" value="1" />&nbsp;13 - Urinary tract infection<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationIV" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationIV" type="checkbox"
                                            value="1" />&nbsp;14 - IV catheter-related infection or complication<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationWoundInf"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationWoundInf"
                                            type="checkbox" value="1" />&nbsp;15 - Wound infection or deterioration<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUncontrolledPain"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUncontrolledPain"
                                            type="checkbox" value="1" />&nbsp;16 - Uncontrolled pain<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMental"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMental"
                                            type="checkbox" value="1" />&nbsp;17 - Acute mental/behavioral health problem<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDVT" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDVT" type="checkbox"
                                            value="1" />&nbsp;18 - Deep vein thrombosis, pulmonary embolus<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationScheduled"
                                            type="hidden" value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationScheduled"
                                            type="checkbox" value="1" />&nbsp;19 - Scheduled treatment or procedure<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOther" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOther" type="checkbox"
                                            value="1" />&nbsp;20 - Other than above reasons<br />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUK" type="hidden"
                                            value=" " />
                                        <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUK" type="checkbox"
                                            value="1" />&nbsp;UK - Reason unknown<br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="transferNotDischarge_M2440">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M2440) For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all
                                    that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedTherapy" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedTherapy" type="checkbox"
                                        value="1" />&nbsp;1 - Therapy services<br />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedRespite" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedRespite" type="checkbox"
                                        value="1" />&nbsp;2 - Respite care<br />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedHospice" type="hidden"
                                        value=" " />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedHospice" type="checkbox"
                                        value="1" />&nbsp;3 - Hospice care<br />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedPermanent"
                                        type="hidden" value=" " />
                                    <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedPermanent"
                                        type="checkbox" value="1" />&nbsp;4 - Permanent placement
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnsafe" type="hidden"
                                    value=" " />
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnsafe" type="checkbox"
                                    value="1" />&nbsp;5 - Unsafe for care at home<br />
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedOther" type="hidden"
                                    value=" " />
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedOther" type="checkbox"
                                    value="1" />&nbsp;6 - Other<br />
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnknown" type="hidden"
                                    value=" " />
                                <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnknown" type="checkbox"
                                    value="1" />&nbsp;UK - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0903) Date of Last (Most Recent) Home Visit:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input id="TransferInPatientNotDischarged_M0903LastHomeVisitDate" name="TransferInPatientNotDischarged_M0903LastHomeVisitDate"
                                        type="text" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                                        or death (at home) of the patient.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input id="TransferInPatientNotDischarged_M0906TransferDate" name="TransferInPatientNotDischarged_M0906TransferDate"
                                        type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
            Patient Landing Screen
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/TransferNotDischarge.js"))
       .OnDocumentReady(() =>
        {%>
TransferNotDischarge.Init();
<%}); 
%>
