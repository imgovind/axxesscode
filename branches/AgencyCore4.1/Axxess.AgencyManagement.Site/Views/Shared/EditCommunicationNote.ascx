﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editCommunicationNote" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left" id="edit_CommunicationNote_Title">Edit Communication Note
            </span><span class="float_right"><a href="javascript:void(0);" class="window_close">
            </a></span>
        </div>
        <div class="abs window_content general_form">
            <% using (Html.BeginForm("EditCommunicationNote", "Patient", FormMethod.Post, new { @id = "editCommunicationNoteForm" }))%>
            <%  { %>
            <%=Html.Hidden("PatientId", "", new { @id = "txtEdit_CommunicationNote_PatientID" })%>
            <%=Html.Hidden("Id", "", new { @id = "edit_CommunicationNote_ID" })%>
            <div id="editCommunicationNoteValidaton" class="marginBreak " style="display: none">
            </div>
            <div class="row">
                <div class="contentDivider">
                    <!--[if !IE]>start row<![endif]-->
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Patient Name:&nbsp;&nbsp;&nbsp;</label>
                        <span name="CommunicationNotePatient" id="txtEdit_CommunicationNote_Patient"></span>
                    </div>
                    <!--[if !IE]>end row<![endif]-->
                </div>
                <div class="contentDivider">
                    <!--[if !IE]>start row<![endif]-->
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp;Date:&nbsp;&nbsp;&nbsp;</label>
                        <%= Html.Telerik().DatePicker()
                                        .Name("CommunicationNoteDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtEdit_CommunicationNote_Date" })
                                                                         
                        %>
                    </div>
                    <!--[if !IE]>end row<![endif]-->
                </div>
            </div>
            <!--[if !IE]>start row<![endif]-->
            <div class="row">
                <label for="FirstName">
                    &nbsp;&nbsp;&nbsp; Communication Text&nbsp;&nbsp;&nbsp;</label>
                <div class="marginBreakNotes">
                    <%=Html.TextArea("CommunicationNoteText", new { @id = "txtEdit_CommunicationNote_text", @class = "notesTextArea required" })%>
                </div>
            </div>
            <!--[if !IE]>end row<![endif]-->
            <div class="row">
                <div class="buttons buttonfix">
                    <ul>
                        <li>
                            <input type="submit" value="Save" /></li>
                        <li>
                            <input type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</div>
