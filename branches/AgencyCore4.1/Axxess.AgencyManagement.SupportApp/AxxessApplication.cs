﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;


    public class AxxessApplication : HttpApplication
    {
        public AxxessApplication()
        {
            //this.AuthenticateRequest += new EventHandler(AxxessApplication_AuthenticateRequest);
        }

        protected void AxxessApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            //if (Context.User != null)
            //{
            //    string username = HttpContext.Current.User.Identity.Name;
            //    IMembershipService membershipService = Container.Resolve<IMembershipService>();
            //    AxxessPrincipal principal = membershipService.Get(username);

            //    Thread.CurrentPrincipal = principal;
            //    HttpContext.Current.User = principal;
            //}
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            //Bootstrapper.Run();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Exception exception = Server.GetLastError();
            //if (exception != null)
            //{
            //    Logger.Exception(exception);
            //}
        }
    }
}
