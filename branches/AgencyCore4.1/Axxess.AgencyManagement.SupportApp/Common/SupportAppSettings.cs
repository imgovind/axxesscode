﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class SupportAppSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static string ImpersonateUrl
        {
            get
            {
                return configuration.AppSettings["ImpersonateUrl"];
            }
        }

        public static string ImpersonatePhysicianUrl
        {
            get
            {
                return configuration.AppSettings["ImpersonatePhysicianUrl"];
            }
        }

        public static string RememberMeCookie
        {
            get
            {
                return configuration.AppSettings["RememberMeCookie"];
            }
        }

        public static int RememberMeForTheseDays
        {
            get
            {
                return configuration.AppSettings["RememberMeForTheseDays"].ToInteger();
            }
        }

        public static string AuthenticationType
        {
            get
            {
                return configuration.AppSettings["AuthenticationType"];
            }
        }

        public static bool UsePersistentCookies
        {
            get
            {
                return configuration.AppSettings["UsePersistentCookies"].ToBoolean();
            }
        }
    }
}
