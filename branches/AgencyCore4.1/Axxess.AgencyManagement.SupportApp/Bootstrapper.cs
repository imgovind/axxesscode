﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System.Web.Mvc;

    using StructureMap;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Modules;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
                x.AddRegistry<SupportApplicationRegistry>();
            });

            AreaRegistration.RegisterAllAreas();
        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Module.Register(new AgencyModule());
            Axxess.Core.Infrastructure.Module.Register(new AccountModule());
            Axxess.Core.Infrastructure.Module.Register(new DefaultModule());
        }
    }
}
