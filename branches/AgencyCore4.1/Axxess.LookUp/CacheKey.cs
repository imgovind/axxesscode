﻿namespace Axxess.LookUp
{
    internal enum CacheKey
    {
        Npi,
        Races,
        States,
        Supplies,
        Insurances,
        MedicareRates,
        PaymentSources,
        DiagnosisCodes,
        ProcedureCodes,
        DisciplineTasks,
        ReferralSources,
        SupplyCategories,
        AdmissionSources,
        DrugClassifications,
        PPSStandards,
        CBSACode,
        HippsAndHhrg,
        Relationships,
        ZipCodes,
        PrivateDutyDisciplineTask
    }
}
