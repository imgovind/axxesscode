﻿namespace Axxess.LookUp.Domain
{  
    public class SupplyCategory
    {
        public byte Id { get; set; }
        public string Description { get; set; }
    }
}
