﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class AmericanState
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
