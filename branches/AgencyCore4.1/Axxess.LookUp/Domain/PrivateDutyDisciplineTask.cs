﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class PrivateDutyDisciplineTask
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string Discipline { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsCreatable { get; set; }
        public bool IsTypeChangeable { get; set; }
        public string RevenueCode { get; set; }
        public string GCode { get; set; }
        public int Unit { get; set; }
        public double Rate { get; set; }
        public int Version { get; set; }
    }
}
