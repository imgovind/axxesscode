﻿namespace Axxess.LookUp.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    public class Drug
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string LexiDrugId { get; set; }

        //[SubSonicIgnore]
        //public string Url
        //{
        //    get
        //    {
        //        return string.Format("http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c={0}", Code);
        //    }
        //}

        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0}: {1}", Code, Name);
        }
        #endregion
    }
}
