﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class ProcedureCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public string FormatCode
        {
            get
            {
                return string.Format("{0}.{1}", this.Code.Substring(0, 2).PadLeft(2, '0'), this.Code.Substring((2)));
            }
        }
    }
}
