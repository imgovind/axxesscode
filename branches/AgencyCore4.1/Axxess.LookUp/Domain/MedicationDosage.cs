﻿namespace Axxess.LookUp.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

  public class MedicationDosage
    {
        public int Id { get; set; }
        public string CommonName { get; set; }
        public string ProperName { get; set; }
        public string Dose { get; set; }
        public string Image { get; set; }
    }
}
