﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
    public class PPSStandard
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public double UrbanRate { get; set; }
        public double RuralRate { get; set; }
        public double Labor { get; set; }
        public double NonLabor { get; set; }
        public double S { get; set; }
        public double RuralS { get; set; }
        public double T { get; set; }
        public double RuralT { get; set; }
        public double U { get; set; }
        public double RuralU { get; set; }
        public double V { get; set; }
        public double RuralV { get; set; }
        public double W { get; set; }
        public double RuralW { get; set; }
        public double X { get; set; }
        public double RuralX { get; set; }
    }
}
