﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceProcess;

using Axxess.Api.Contracts;

namespace Axxess.Api.Services
{
    public partial class CacheWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public CacheWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "CacheService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(CacheService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("Cache Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("Cache Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
