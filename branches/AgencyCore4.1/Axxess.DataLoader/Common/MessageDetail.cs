﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.DataLoader.Common
{
    public class MessageDetail
    {
        public Guid Id { get; set; }
        public Guid FromId { get; set; }
        public string FromName { get; set; }
        public string RecipientNames { get; set; }
        public Guid PatientId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Guid AttachmentId { get; set; }
        public DateTime Created { get; set; }
        public string CarbonCopyNames { get; set; }
    }
}
