﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    

    using Axxess.LookUp.Domain;

    using Axxess.AgencyManagement.Domain;

    using Axxess.OasisC.Domain;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public static class Database
    {
        internal class tempProcessedMessage 
        {             
            public Guid ID { get; set; }
            public Guid NewMessageId { get; set; }
        };
       
        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository oasisDatabase = new SimpleRepository("OasisCConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);

        internal static List<User> GetAllUsers()
        {
            return agencyManagementDatabase.All<User>().ToList();            
        }

        
        internal static List<Agency> GetAgencies()
        {
            return agencyManagementDatabase.All<Agency>().ToList();
        }

        internal static Agency GetAgency(Guid agencyId)
        {
            return agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyLocation>(a => a.AgencyId == agencyId);
        }

        internal static List<AgencyLocation> GetAgencyLocations(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyLocation>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<AgencyLocation> GetAgencyLocations()
        {
            return agencyManagementDatabase.All<AgencyLocation>().ToList();
        }

        internal static List<PhysicianOrder> GetOrders()
        {
            return agencyManagementDatabase.All<PhysicianOrder>().ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters()
        {
            return agencyManagementDatabase.All<FaceToFaceEncounter>().ToList();
        }

        internal static Patient GetPatient(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static Patient GetPatientByMR(string mr, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.PatientIdNumber == mr && p.AgencyId == agencyId);
        }

        internal static PatientAdmissionDate GetPatientAdminPeriod(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<PatientAdmissionDate>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static User GetUser(Guid userId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<User>(p => p.Id == userId && p.AgencyId == agencyId);
        }

        internal static List<User> GetUsers(Guid agencyId)
        {
            return agencyManagementDatabase.Find<User>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Rap> GetRaps(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Rap>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Final> GetFinals(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Final>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Patient> GetPatients(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<AllergyProfile> GetAllergyProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AllergyProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<MedicationProfile> GetMedicationProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<MedicationProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PhysicianOrder> GetPhysicianOrders(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PhysicianOrder>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId)
        {
            return agencyManagementDatabase.Find<FaceToFaceEncounter>(p => p.AgencyId == agencyId).ToList();
        }

        internal static bool UpdatePatient(Patient patient)
        {
            if (patient != null)
            {
                agencyManagementDatabase.Update<Patient>(patient);
                return true;
            }
            return false;
        }

        internal static AgencyPhysician GetPhysician(string npi, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi && p.AgencyId == agencyId);
        }

        internal static AgencyPhysician GetPhysician(Guid physicianId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId);
        }

        internal static List<AgencyPhysician> GetPhysicians(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyPhysician>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientEpisode> GetEpisodes(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientEpisode>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientAdmissionDate>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId)
        {
            var list = new List<PatientVisitNote>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `WoundNote` from patientvisitnotes where agencyid = '{0}' and IsWoundCare = 1 and IsDeprecated = 0;", agencyId);
            using (var cmd = new FluentCommand<PatientVisitNote>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    WoundNote = reader.GetStringNullable("WoundNote")
                })
                .AsList();
            }
            return list;
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId, DateTime startTime, DateTime endTime)
        {
            return agencyManagementDatabase.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.Modified >= startTime && p.Modified <= endTime).OrderByDescending(p => p.Modified).ToList();
        }

        internal static Npi GetNpiData(string npi)
        {
            return lookupDatabase.Single<Npi>(p => p.Id == npi);
        }

        internal static List<PlanofCare> GetPlanofCareByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<PlanofCare>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<T> GetAssessments<T>(Guid agencyId) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `SubmissionFormat` from `{0}s` where agencyid = '{1}' and IsDeprecated = 0;", typeof(T).Name.ToLower(), agencyId);
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("OasisCConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                    //OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();
            }
            return list;
        }

        internal static Assessment GetAssessment<T>(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId) where T : Assessment, new()
        {
            return oasisDatabase.Single<T>(a => a.EpisodeId == episodeId && a.PatientId == patientId && a.Id == assessmentId && a.AgencyId == agencyId);
            //return oasisDatabase.Find<RecertificationAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool AddForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateMany<T>(IEnumerable<T> items) where T : class, new()
        {
            if (items != null)
            {
                agencyManagementDatabase.UpdateMany<T>(items);
                return true;
            }
            return false;
        }

        internal static bool UpdateForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForOasisC<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                oasisDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<CBSACode> GetCbsaCodes()
        {
            return lookupDatabase.All<CBSACode>().ToList();
        }

        internal static List<DisciplineTask> GetDisciplineTasks()
        {
            return lookupDatabase.All<DisciplineTask>().ToList();
        }

        internal static List<User> GetUsers()
        {
            return agencyManagementDatabase.All<User>().ToList();
        }

        internal static List<PatientEpisode> GetPatientEpisodes()
        {
            return agencyManagementDatabase.All<PatientEpisode>().ToList();
        }

        internal static void AddMany<T>(IEnumerable<T> items) where T : class, new()
        {
                agencyManagementDatabase.AddMany<T>(items);
        }
        internal static List<TempAgencyUserStatus> GetTempAgencyUsers()
        {
            var list = new List<TempAgencyUserStatus>();
            var sql = @"SELECT Id,AgencyId FROM users where IsDeprecated=0 order by AgencyId";
            using (var cmd = new FluentCommand<TempAgencyUserStatus>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new TempAgencyUserStatus
                    {
                        AgencyId=reader.GetGuid("AgencyId"),
                        UserId=reader.GetGuid("Id")
                    })
                    .AsList();
            }
            return list;
        }
        internal static List<ManagedClaim> GetPaidManagedClaims(Guid agencyId)
        {
            var list = new List<ManagedClaim>();
            var sql = @"SELECT `Id`, `AgencyId`, `PatientId`, `Payment`, `PrimaryInsuranceId`, `PaymentDate` " +
                       "FROM managedclaims WHERE agencyid = @agencyid AND `Status` = 3030;";
            using (var cmd = new FluentCommand<ManagedClaim>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ManagedClaim
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Payment = reader.GetDouble("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                })
                .AsList();
            }
            return list;
        }

        internal static List<ManagedClaimPayment> GetManagedClaimPayments()
        {
            return agencyManagementDatabase.Find<ManagedClaimPayment>(payment => payment.IsDeprecated == false).ToList();
        }

        internal static List<Message> GetAllMessages()
        {
            IList<Message> allMessages = agencyManagementDatabase.Find<Message>(m => m.Id != Guid.Empty);
            return allMessages.ToList();

        }

        internal static int GetAgencyMessageCount(Guid agencyId)
        {
            int count = agencyManagementDatabase.Find<Message>(m => m.AgencyId == agencyId).Count;
            return count;
        }

        internal static List<Message> GetMessagesByAgencyAndUser( Guid agencyId, Guid userId )
        {
            int count = agencyManagementDatabase.Find<Message>(m => m.AgencyId == agencyId).Count;

            string script = string.Format(
                "select * from messages where AgencyId = '{0}' and FromId = '{1}' order by FromId asc, Subject asc, RecipientNames asc, Body asc, RecipientId asc, Created asc", 
                agencyId.ToString(), 
                userId.ToString());

            var sortedMessages = new FluentCommand<Message>(script)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new Message
                {   
                    Id = reader.GetGuid("Id"),
                    Body = reader.GetString("Body"),
                    Subject = reader.GetString("Subject"),
                    MarkAsRead = reader.GetBoolean("MarkAsRead"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    FromName = reader.GetString("FromName"),
                    FromId = reader.GetGuidIncludeEmpty("FromId"),
                    Created = reader.GetDateTime("Created"),
                    AttachmentId = reader.GetGuidIncludeEmpty("AttachmentId"),
                    RecipientNames = reader.GetStringNullable("RecipientNames"),
                    CarbonCopyNames = reader.GetStringNullable("CarbonCopyNames"),
                    RecipientId = reader.GetGuidIncludeEmpty("RecipientId"),
                    PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                    AgencyId = reader.GetGuidIncludeEmpty("AgencyId")
                }).AsList();

            return sortedMessages;
        }

        internal static TempAgencyUserStatus GetTempAgencyUserStatus(Guid userId)
        {       
            return agencyManagementDatabase.Find<TempAgencyUserStatus>(t => t.UserId == userId).FirstOrDefault();
        }

        
        internal static List<tempProcessedMessage> GetProcessedMessageIdsForUser(Guid userId)
        {
            string sql = string.Empty;
            sql += "select ID, NewMessageId from tempprocessedmessages where NewMessageId in (";
            sql += string.Format("select MessageId from usermessages where userid = '{0}')", userId.ToString());
            var processedMessages = new FluentCommand<tempProcessedMessage>(sql)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new tempProcessedMessage
                {
                    ID = reader.GetGuid("ID"),
                    NewMessageId = reader.GetGuid("NewMessageId")
                }).AsList();
            return processedMessages;
        }


        internal static bool DeleteMessageByUserId(Guid userId)
        {
            string script = string.Format("delete from usermessages where UserId ='{0}'", userId.ToString());
                int result = new FluentCommand<UserMessage>(script)
                    .SetConnection("AgencyManagementConnectionString").AsNonQuery();
                return (result > 0);
        }

        internal static bool DeleteMessageDetails(Guid Id)
        {            
            string script = string.Format("delete from messagedetails where Id='{0}'", Id.ToString());
            int result = new FluentCommand<MessageDetail>(script)
                    .SetConnection("AgencyManagementConnectionString").AsNonQuery();

            return (result > 0);
        }

        internal static List<UserMessage> GetUserMessageByUserId(Guid userId)
        {
            return agencyManagementDatabase.Find<UserMessage>(u => u.UserId == userId).ToList();
        }

        internal static List<TempAgencyUserStatus> GetTempUsers(int status)
        {
            return agencyManagementDatabase.Find<TempAgencyUserStatus>(t => t.Status == status).ToList();
        }

        internal static int UpdateTempAgencyUserStatus(TempAgencyUserStatus t)
        {
            return agencyManagementDatabase.Update<TempAgencyUserStatus>(t);
        }

        internal static bool MigrateUserMessage(Message message, Guid linkId, bool addToMessageDetails )
        {
            bool success = true;

            // check to see if this message was processed on a prior migration
            tempProcessedMessage oldMessage = agencyManagementDatabase.Single<tempProcessedMessage>(temp => temp.ID == message.Id);

            if (oldMessage == null) // message not processed, proceed
            {
                if (addToMessageDetails) // this is a totally new message, add details.
                {
                    Common.MessageDetail md = new Common.MessageDetail()
                    {
                        Id = linkId,
                        FromId = message.FromId,
                        FromName = message.FromName,
                        AttachmentId = message.AttachmentId,
                        RecipientNames = message.RecipientNames,
                        CarbonCopyNames = message.CarbonCopyNames,
                        PatientId = message.PatientId,
                        Subject = message.Subject,
                        Body = message.Body,
                        Created = message.Created
                    };
                    success = Database.Add<Common.MessageDetail>(md);
                }

                if (success)
                {
                    UserMessage um = new UserMessage()
                    {
                        AgencyId = message.AgencyId,
                        FolderId = Guid.Empty,
                        Id = Guid.NewGuid(),
                        IsDeprecated = message.IsDeprecated,
                        IsRead = message.MarkAsRead,
                        MessageId = linkId,
                        MessageType = 0,
                        UserId = message.RecipientId
                    };
                    success = Database.Add<UserMessage>(um);
                }

                if (success) // message added. record this into the completed message table
                {
                    string sqlString = string.Format("insert into tempprocessedmessages (ID, NewMessageId) values ('{0}', '{1}')", message.Id.ToString(), linkId.ToString());
                    var cmd = new FluentCommand<tempProcessedMessage>(sqlString);
                    int result = cmd.SetConnection("AgencyManagementConnectionString").AsScalar();
                }
            }
            else
            {
                success = false;                
            }
            return success;
        }

        internal static List<SystemMessage> GetAllSystemMessages()
        {
            List<SystemMessage> sysMessages = agencyManagementDatabase.All<SystemMessage>().ToList();
            return sysMessages;
        }

    }
}
