﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Data;

    using HtmlAgilityPack;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;


    public static class DataExtensions
    {
        enum TableOneFields : int
        {
            Names = 2,
            StartOfCare = 3,
            EpisodeRange = 4,
            PhoneNumber = 5,
            PrimaryInsuranceName = 7,
            DoctorName = 9,
            DoctorPhone = 10
        };

        enum TableTwoFields : int
        {
            MedicalRecordNumber = 2,
            FullAddress = 5,
            MedicareNumber = 6,
            ICD9Code = 7,
            PrimaryDiagnosis = 8,
            PrimaryCaseWorker = 9,
            CaseWorkerPhone = 10
        }

        enum TableThreeFields : int
        {
            DOB = 2,
            SN = 5,
            SNValue = 6,
            HA = 7,
            HAValue = 8,
            PT = 9,
            PTValue = 10,
            MS = 11,
            MSValue = 12,
            OT = 13,
            OTValue = 14,
            ST = 15,
            STValue = 15,
            OTH = 17,
            OTHValue = 18
        }

            
        public static string GetValue(this DataRow dataRow, int columnIndex)
        {
            if (dataRow != null && columnIndex < dataRow.ItemArray.Length)
            {
                if (dataRow[columnIndex] != null && dataRow[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRow[columnIndex].ToString().Trim();
                }
            }
            return string.Empty;
        }

        public static string ToText(this DataRecord dataRow)
        {
            if (dataRow != null && dataRow.Values.Count > 0)
            {
                var text = new StringBuilder();
                dataRow.Values.ForEach(value =>
                {
                    text.AppendFormat("{0} ", value);
                });
                return text.ToString().Trim();
            }
            return string.Empty;
        }

        public static string ToText(this DataRow dataRow)
        {
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                var i = 0;
                var text = new StringBuilder();
                dataRow.ItemArray.ForEach(item =>
                {
                    text.AppendFormat("{0} ", dataRow.GetValue(i));
                    i++;
                });
                return text.ToString().Trim();
            }
            return string.Empty;
        }

        public static string GetValue(this DataRecord dataRecord, int columnIndex)
        {
            if (dataRecord != null && columnIndex < dataRecord.Values.Count)
            {
                if (dataRecord[columnIndex] != null && dataRecord[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRecord[columnIndex].ToString().Trim().Replace("'", "").Replace("\"", "").Replace("\\", "");
                }
            }
            return string.Empty;
        }

        public static bool IsEmpty(this DataRow dataRow)
        {
            var result = true;
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                {
                    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public static bool WithAtLeastXColumn(this DataRow dataRow, int columns)
        {
            var counter = 0;
            var result = false;
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                {
                    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                    {
                        counter++;
                    }
                }
            }
            if (counter >= columns)
            {
                result = true;
            }
            return result;
        }

        public static string ToPhoneDB(this string text)
        {
            if (text.IsNotNullOrEmpty() && text.Length >= 10)
            {
                return text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            }
            return string.Empty;
        }

        private static void AddPatientComment(string CommentPrefix, HtmlNode node, Patient patient)
        {
            if (node.InnerText.Trim() != null)
            {
                patient.Comments += CommentPrefix + ": " + node.InnerText.Trim() + ", ";
            }
        }

        private static void ProcessTableOne(HtmlNode node, int columnNumber, Patient patient)
        {
            switch (columnNumber)
            {
                case (int)TableOneFields.Names:
                    {
                        patient.LastName = node.InnerText.Split(',')[0].Trim();

                        var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (firstName.Length > 1)
                        {
                            patient.FirstName = StringExtensions.StripNewLines(firstName[0].Trim());
                            patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                        }
                        else
                        {
                            patient.FirstName = StringExtensions.StripNewLines(node.InnerText.Split(',')[1].Trim());
                        }
                    }
                    break;
                case (int)TableOneFields.StartOfCare:
                    {
                        if (node.InnerText.IsNotNullOrEmpty())
                        {
                            patient.StartofCareDate = node.InnerText.ToMySqlDate();
                        }
                    }
                    break;
                case (int)TableOneFields.EpisodeRange:
                    {
                        if (node.InnerText.IsNotNullOrEmpty() && !node.InnerText.Trim().IsEqual("-"))
                        {
                            AddPatientComment("Last Cert Period", node, patient);
                        }
                    }
                    break;
                case (int)TableOneFields.PhoneNumber:
                    {
                        patient.PhoneHome = StringExtensions.StripNewLines(node.InnerText.ToPhoneDB());
                        
                    }
                    break;

                case (int)TableOneFields.PrimaryInsuranceName:
                    {
                        if (node.InnerText.Trim().IsNotNullOrEmpty())
                        {
                            if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                            {
                                patient.PrimaryInsurance = "1";
                            }
                            else
                            {
                                AddPatientComment("Insurance", node, patient);
                            }
                        }
                    }
                    break;

                case (int)TableOneFields.DoctorName:
                    {
                        if (node.InnerText.Trim().IsNotNullOrEmpty())
                        {
                            AddPatientComment("Dr. Name", node, patient);
                        }
                    }
                    break;

                case (int)TableOneFields.DoctorPhone:
                    {
                        if (node.InnerText.Trim().IsNotNullOrEmpty())
                        {
                            AddPatientComment("Dr. Phone", node, patient);
                        }
                    }
                    break;
            }
        }
        private static void ProcessTableTwo(HtmlNode node, int columnNumber, Patient patient)
        {
            switch (columnNumber)
            {
                case (int)TableTwoFields.MedicalRecordNumber:
                    patient.PatientIdNumber = node.InnerText.Trim().Replace("MR# ", "");
                    break;

                case (int)TableTwoFields.FullAddress:
                    var address = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Replace("<Font Color=\"#000000\" Face=\"Arial\" Size=\"1\">", "").Replace("<font color=\"#000000\" face=\"ARIAL\" size=\"1\">", "").Replace("<Font Size=\"1\" Color=\"#000000\" Face=\"Arial\">", "");

                    var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                    if (addressArray.Length > 1)
                    {
                        patient.AddressLine1 = StringExtensions.StripNewLines(addressArray[0]);
                        var cityStateZip = addressArray[1].Split(',');
                        if (cityStateZip.Length > 1)
                        {
                            patient.AddressCity = StringExtensions.StripNewLines(cityStateZip[0]);

                            var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (stateZip.Length > 1)
                            {
                                patient.AddressStateCode = StringExtensions.StripNewLines(stateZip[0]);
                                patient.AddressZipCode = StringExtensions.StripNewLines(stateZip[1]);
                            }
                        }
                    }
                    break;

                case (int)TableTwoFields.MedicareNumber:
                    if (patient.PrimaryInsurance.IsEqual("1"))
                    {
                        patient.MedicareNumber = node.InnerText.Trim();
                    }
                    else
                    {
                        AddPatientComment("Health  Insurance Number", node, patient);
                    }
                    break;

                case (int)TableTwoFields.ICD9Code:
                    if (node.InnerText.Trim().IsNotNullOrEmpty())
                    {
                        AddPatientComment("ICD9 Code", node, patient);
                    }
                    break;

                case (int)TableTwoFields.PrimaryDiagnosis:
                    if (node.InnerText.Trim().IsNotNullOrEmpty())
                    {
                        AddPatientComment("Diagnosis Desc", node, patient);
                    }
                    break;

                case (int)TableTwoFields.PrimaryCaseWorker:
                    if (node.InnerText.Trim().IsNotNullOrEmpty())
                    {
                        AddPatientComment("CM", node, patient);
                    }
                    break;

                case (int)TableTwoFields.CaseWorkerPhone:
                    if (node.InnerText.Trim().IsNotNullOrEmpty())
                    {
                        AddPatientComment("CM Phone", node, patient);
                    }
                    break;
            }
        }
        private static void ProcessTableThree(HtmlNode node, int columnNumber, Patient patient)
        {
            switch (columnNumber)
            {
                case (int)TableThreeFields.DOB:
                    if (node.InnerText.Trim().IsNotNullOrEmpty())
                    {
                        patient.DOB = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                    }
                    else
                    {
                        patient.DOB = DateTime.Now;
                    }
                    break;
                case (int) TableThreeFields.HAValue:
                    AddPatientComment("HA", node,  patient);
                    break;

                case (int) TableThreeFields.MSValue:
                    AddPatientComment("MS", node, patient);
                    break;

                case (int) TableThreeFields.OTValue:
                    AddPatientComment("OT", node, patient);
                    break;
                case(int) TableThreeFields.OTHValue:
                    AddPatientComment("OTH", node, patient);
                    break;
                case (int) TableThreeFields.PTValue:
                    AddPatientComment("PT", node, patient);
                    break;
                case (int) TableThreeFields.SNValue:
                    AddPatientComment("SN", node, patient);
                    break;
                case (int) TableThreeFields.STValue:
                    AddPatientComment("ST", node, patient);
                    break;
            }
        }

        public static void SetVisiTrakPatientThree(this HtmlNode node, int tableNumber, int columnNumber, Patient patient)
        {
            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        {
                            ProcessTableOne(node, columnNumber, patient);
                        }
                        break;
                    case 2:
                        {
                            ProcessTableTwo(node, columnNumber, patient);
                        }
                        break;
                    case 3:
                        {
                            ProcessTableThree(node, columnNumber, patient);
                        }
                        break;
                }
            }
        }


        public static void SetVisiTrakPatient(this HtmlNode node, int tableNumber, int columnNumber, Patient patient)
        {
            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        if (columnNumber == 2)
                        {
                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (firstName.Length > 1)
                            {
                                patient.FirstName = firstName[0].Trim();
                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                            }
                            else
                            {
                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                            }
                        }
                        if (columnNumber == 3)
                        {
                            if (node.InnerText.IsNotNullOrEmpty())
                            {
                                patient.StartofCareDate = node.InnerText.ToMySqlDate();
                            }
                        }
                        if (columnNumber == 4)
                        {
                            if (node.InnerText.IsNotNullOrEmpty() && !node.InnerText.Trim().IsEqual("-"))
                            {
                                //var dateRange = node.InnerText.Trim().Split('-');
                                //if (dateRange.Length > 1)
                                //{
                                //    var episodeStart = dateRange[0].ToMySqlDate();
                                //    var episodeEnd = dateRange[1].ToMySqlDate();

                                //    if (episodeEnd.Year < 2011)
                                //    {
                                //        patient.Status = 2;
                                //    }
                                //}
                                patient.Comments += string.Format("Last Certification Period: {0}. ", node.InnerText);
                            }
                        }
                        if (columnNumber == 5)
                        {
                            patient.PhoneHome = node.InnerText.ToPhoneDB();
                        }

                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                {
                                    patient.PrimaryInsurance = "1";
                                }
                                else
                                {
                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Trim());
                                }
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Acuity Level: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        break;
                    case 2:
                        if (columnNumber == 2)
                        {
                            patient.PatientIdNumber = node.InnerText.Trim().Replace("MR# ", "");
                        }
                        if (columnNumber == 5)
                        {
                            var address = node.InnerHtml.IsNotNullOrEmpty()? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase):string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Replace("<Font Color=\"#000000\" Face=\"Arial\" Size=\"1\">", "").Replace("<font color=\"#000000\" face=\"ARIAL\" size=\"1\">", "").Replace("<Font Size=\"1\" Color=\"#000000\" Face=\"Arial\">", "");

                            var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (addressArray.Length > 1)
                            {
                                patient.AddressLine1 = addressArray[0];
                                var cityStateZip = addressArray[1].Split(',');
                                if (cityStateZip.Length > 1)
                                {
                                    patient.AddressCity = cityStateZip[0];

                                    var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    if (stateZip.Length > 1)
                                    {
                                        patient.AddressStateCode = stateZip[0];
                                        patient.AddressZipCode = stateZip[1];
                                    }
                                }
                            }
                        }
                        if (columnNumber == 6)
                        {
                            if (patient.PrimaryInsurance.IsEqual("1"))
                            {
                                patient.MedicareNumber = node.InnerText.Trim();
                            }
                            else
                            {
                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Code: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Trim());
                            }
                        }
                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("CM Phone: {0}. ", node.InnerText.Trim());
                            }
                        }

                        break;
                    case 3:
                        if (columnNumber == 2)
                        {
                            patient.DOB = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                        }

                        break;
                }
            }
        }

        public static void SetVisiTrakPatientTwo(this HtmlNode node, int tableNumber, int columnNumber, Patient patient)
        {
            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        if (columnNumber == 2)
                        {
                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (firstName.Length > 1)
                            {
                                patient.FirstName = firstName[0].Trim();
                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                            }
                            else
                            {
                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                            }
                        }
                        if (columnNumber == 4)
                        {
                            if (node.InnerText.IsNotNullOrEmpty())
                            {
                                //patient.StartofCareDate = node.InnerText.ToMySqlDate();
                                string startOfCare = node.InnerText.Trim();
                                string[] soc_array = startOfCare.Split('-');
                                patient.StartofCareDate = DateTime.Parse(soc_array[0]);
                            }
                        }
                        if (columnNumber == 6)
                        {
                            if (node.InnerText.IsNotNullOrEmpty() && !node.InnerText.Trim().IsEqual("-"))
                            {
                                //var dateRange = node.InnerText.Trim().Split('-');
                                //if (dateRange.Length > 1)
                                //{
                                //    var episodeStart = dateRange[0].ToMySqlDate();
                                //    var episodeEnd = dateRange[1].ToMySqlDate();

                                //    if (episodeEnd.Year < 2011)
                                //    {
                                //        patient.Status = 2;
                                //    }
                                //}
                                patient.Comments += string.Format("Last Certification Period: {0}. ", node.InnerText);
                            }
                        }
                        if (columnNumber == 8)
                        {
                            patient.PhoneHome = node.InnerText.ToPhoneDB();
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                {
                                    patient.PrimaryInsurance = "1";
                                }
                                else
                                {
                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Trim());
                                }
                            }
                        }

                        if (columnNumber == 12)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Acuity Level: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 14)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 15)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }
                        break;
                    case 2:
                        if (columnNumber == 2)
                        {
                            patient.PatientIdNumber = node.InnerText.Trim().Replace("MR# ", "");
                        }
                        if (columnNumber == 6)
                        {
                            var address = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "");
                            var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (addressArray.Length > 1)
                            {
                                patient.AddressLine1 = addressArray[0];
                                var cityStateZip = addressArray[1].Split(',');
                                if (cityStateZip.Length > 1)
                                {
                                    patient.AddressCity = cityStateZip[0];

                                    var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    if (stateZip.Length > 1)
                                    {
                                        patient.AddressStateCode = stateZip[0];
                                        patient.AddressZipCode = stateZip[1];
                                    }
                                }
                            }
                        }
                        if (columnNumber == 8)
                        {
                            if (patient.PrimaryInsurance.IsEqual("1"))
                            {
                                patient.MedicareNumber = node.InnerText.Trim();
                            }
                            else
                            {
                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Code: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 11)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Trim());
                            }
                        }
                        if (columnNumber == 13)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 14)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("CM Phone: {0}. ", node.InnerText.Trim());
                            }
                        }
                        break;
                    case 3:
                        if (columnNumber == 2)
                        {
                            patient.DOB = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                        }
                        break;
                }
            }
        }

        public static bool DoesHtmlRowHaveData(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    rowNode.ChildNodes.ForEach(tableBodyNode =>
                    {
                        if (tableBodyNode.Name.IsEqual("td"))
                        {
                            /*
                            tableBodyNode.ChildNodes.ToList().ForEach(dataNode =>
                            {*/
                                if (tableBodyNode.Name.IsEqual("td"))
                                {
                                    if (count == 1)
                                    {
                                        name = tableBodyNode.InnerText;
                                    }
                                    count++;
                                }
                            /*
                            });
                            */
                        }
                    });
                });

                if (count >= 10 && !name.IsEqual("Name"))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool DoesHtmlRowHaveDataSecond(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                var tbody = node.SelectSingleNode("tbody");
                if (tbody != null && tbody.Name.IsEqual("tbody"))
                {
                    tbody.ChildNodes.ToList().ForEach(rowNode =>
                    {

                        if (rowNode.Name.IsEqual("tr"))
                        {
                            rowNode.ChildNodes.ToList().ForEach(dataNode =>
                            {
                                if (dataNode.Name.IsEqual("td"))
                                {
                                    if (count == 1)
                                    {
                                        name = dataNode.InnerText;
                                    }
                                    count++;
                                }
                            });
                        }
                    });

                    if (count >= 10 && !name.IsEqual("Name"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool DoesHtmlRowHaveTBody(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("tbody"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Name"))
                {
                    return true;
                }
            }
            return false;
        }

        public static void SetVisiTrakPhysician(this HtmlNode node, int columnNumber, AgencyPhysician physician)
        {
            if (node != null && physician != null)
            {
                var nodeValue = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Trim();
                if (columnNumber == 2)
                {
                    physician.LastName = nodeValue.Split(',')[0].Trim();

                    var firstName = nodeValue.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (firstName.Length > 1)
                    {
                        physician.FirstName = firstName[0].Trim();
                        physician.MiddleName = firstName[1].Trim().Replace(".", "");
                    }
                    else
                    {
                        physician.FirstName = nodeValue.Split(',')[1].Trim();
                    }
                }
                if (columnNumber == 4)
                {
                    physician.AddressLine1 = nodeValue;
                }
                if (columnNumber == 5)
                {
                    physician.AddressCity = nodeValue;
                }
                if (columnNumber == 6)
                {
                    physician.AddressStateCode = nodeValue;
                }
                if (columnNumber == 7)
                {
                    physician.AddressZipCode = nodeValue;
                }
                if (columnNumber == 8)
                {
                    physician.PhoneWork = nodeValue.ToPhoneDB();
                }
                if (columnNumber == 9)
                {
                    physician.FaxNumber = nodeValue.ToPhoneDB();
                }

                if (columnNumber == 10 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("UPIN: {0}. ", nodeValue.Trim());
                }

                if (columnNumber == 11 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.NPI = nodeValue;
                }

                if (columnNumber == 12 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.LicenseNumber = nodeValue;
                }

                if (columnNumber == 13 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("Tax ID: {0}. ", nodeValue);
                }

            }
        }

        public static void SetVisiTrakPhysicianTwo(this HtmlNode node, int columnNumber, AgencyPhysician physician)
        {
            if (node != null && physician != null)
            {
                var nodeValue = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Trim();
                if (columnNumber == 2)
                {
                    physician.LastName = nodeValue.Split(',')[0].Trim();

                    var firstName = nodeValue.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (firstName.Length > 1)
                    {
                        physician.FirstName = firstName[0].Trim();
                        physician.MiddleName = firstName[1].Trim().Replace(".", "");
                    }
                    else
                    {
                        physician.FirstName = nodeValue.Split(',')[1].Trim();
                    }
                }
                if (columnNumber == 4)
                {
                    physician.AddressLine1 = nodeValue;
                }
                if (columnNumber == 6)
                {
                    physician.AddressCity = nodeValue;
                }
                if (columnNumber == 8)
                {
                    physician.AddressStateCode = nodeValue;
                }
                if (columnNumber == 10)
                {
                    physician.AddressZipCode = nodeValue;
                }
                if (columnNumber == 12)
                {
                    physician.PhoneWork = nodeValue.ToPhoneDB();
                }
                if (columnNumber == 14)
                {
                    physician.FaxNumber = nodeValue.ToPhoneDB();
                }

                if (columnNumber == 16 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("UPIN: {0}. ", nodeValue.Trim());
                }

                if (columnNumber == 18)
                {
                    physician.NPI = nodeValue;
                }

                if (columnNumber == 20 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.LicenseNumber = nodeValue;
                }

                if (columnNumber == 22 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("Tax ID: {0}. ", nodeValue);
                }

            }
        }

        public static bool DoesHtmlRowHavePhysicianData(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Doctor"))
                {
                    return true;
                }
            }
            return false;
        }

        public static IDictionary<string, NotesQuestion> ToDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.Note != null)
            {
                var noteQuestions = patientVisitNote.Note.ToObject<List<NotesQuestion>>();
                noteQuestions.ForEach(n =>
                {
                    questions.Add(n.Name, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToWoundDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.WoundNote != null)
            {
                var noteQuestions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                noteQuestions.ForEach(n =>
                {
                    questions.Add(n.Name, n);
                });
            }
            return questions;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty()) return questions[key].Answer.Unclean();
            else return string.Empty;
        }

        public static Guid AnswerOrEmptyGuid(this IDictionary<string, NotesQuestion> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty() && questions[key].Answer.IsGuid())
            {
                return questions[key].Answer.ToGuid();
            }

            return Guid.Empty;
        }
    }
}
