﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Collections.Generic;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class GenericExcelPatientScript
    {
        private static string input = Path.Combine(App.Root, "Files\\generic_patient.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\LoadLog_{0}{1}{2}.log", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year ));

        /// <summary>
        /// Field list for ANY excel patient spreadsheet that contains 1 data item per colum.
        /// Simple adjust this field list to have the correct column for the data item,
        /// then go adjust the case statement in ExtractPatientData() and your spreadsheet
        /// should import flawlessly into our databases.
        /// </summary>

        enum FieldListT1 : int
        {
            FullName        = 0,
            MRNumber        = 4,
            PolicyHicNumber = 5,
            DOB             = 7,
            SOC             = 10,
            admStatus       = 14,
            adm             = 17,
            Icd9Code        = 20,
            drName          = 25,
            drPhone         = 35,
            upin            = 40
        }

        /*********************************************************************/

        enum FieldListT2 : int
        {
            fullAddress     = 1,
            patientPhone    = 14,
            insName         = 32
        }

        /*********************************************************************/
        
        // set this to the ACTUAL number of rows that contain data
        // on your excel spreadsheet. DO NOT include empty rows
        // that are between records.

        private const int ROWS_PER_RECORD = 2;
        private const bool FIRST_ROW_AS_COL_NAMES = true;

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = FIRST_ROW_AS_COL_NAMES;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    int currentRow          = 1;
                                    Patient patientData     = null;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            switch (currentRow)
                                            {
                                                case 1:
                                                    patientData = CreatePatientObject(agencyId, locationId);
                                                    ExtractRowOne(dataRow, patientData);
                                                    currentRow++;
                                                    break;
                                                case 2:
                                                    ExtractRowTwo(dataRow, patientData);
                                                    break;
                                                default:
                                                    break;
                                            }

                                        }
                                        else
                                        {
                                            if (currentRow >= ROWS_PER_RECORD)
                                            {
                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;
                                                AddPatientToDatabase(agencyId, textWriter, patientData);
                                                currentRow = 1;
                                            }                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }

        /*********************************************************************/

        private static Patient CreatePatientObject(Guid agencyId, Guid locationId)
        {
            Patient p = new Patient()
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                AgencyLocationId = locationId,
                Status = 1,
                Ethnicities = string.Empty,
                MaritalStatus = string.Empty,
                IsDeprecated = false,
                IsHospitalized = false,
                PatientIdNumber = string.Empty,
                Gender = ""
            };
            return p;
        }

        /*********************************************************************/

        private static bool AddPatientToDatabase(Guid agencyId, TextWriter textWriter, Patient patientData)
        {
            bool bSuccess = false;

            var medicationProfile = new MedicationProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Medication = "<ArrayOfMedication />"
            };

            var allergyProfile = new AllergyProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Allergies = "<ArrayOfAllergy />"
            };

            bSuccess = Database.Add(patientData);
            if (bSuccess)
            {
                bSuccess = Database.Add(medicationProfile);
                if (bSuccess)
                {
                    bSuccess = Database.Add(allergyProfile);
                    if (!bSuccess)
                    {
                        textWriter.WriteLine("Allergy profile add failed for: " + patientData.DisplayName);
                    }
                }
                else
                {
                    textWriter.WriteLine("Medication profile add failed for: " + patientData.DisplayName);
                }
            }
            else
            {
                textWriter.WriteLine("Add failed for patient: " + patientData.DisplayName);
            }

            if (bSuccess)
            {
                var admissionPeriod = new PatientAdmissionDate
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    Created = DateTime.Now,
                    DischargedDate = DateTime.MinValue,
                    IsActive = true,
                    IsDeprecated = false,
                    Modified = DateTime.Now,
                    PatientData = patientData.ToXml().Replace("'", ""),
                    PatientId = patientData.Id,
                    Reason = string.Empty,
                    StartOfCareDate = patientData.StartofCareDate,
                    Status = patientData.Status
                };

                bSuccess = Database.Add(admissionPeriod);
                if (bSuccess)
                {
                    var patient = Database.GetPatient(patientData.Id, agencyId);
                    if (patient != null)
                    {
                        patient.AdmissionId = admissionPeriod.Id;
                        bSuccess = Database.Update(patient);
                        if (bSuccess)
                        {
                            Console.WriteLine("{0}", patientData.DisplayName);
                            textWriter.WriteLine("{0}", patientData.DisplayName);
                        }
                    }
                }
                else
                {
                    textWriter.WriteLine("Failed adding admission period for: " + patientData.DisplayName);
                }
            }

            return bSuccess;
        }

        /*********************************************************************/

        private static void ExtractRowOne( DataRow dataRow, Patient patientData )
        {
            for (int col = (int)FieldListT1.FullName; col != ((int)FieldListT1.upin) + 1; col++)
            {
                string value = string.Empty;
                string[] names;

 
                switch (col)
                {
                    case (int)FieldListT1.MRNumber:
                        value = dataRow.GetValue( (int)FieldListT1.MRNumber ).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.PatientIdNumber = value;
                        break;
                    case (int)FieldListT1.FullName:
                        value = dataRow.GetValue((int)FieldListT1.FullName).Trim();
                        if( value.IsNotNullOrEmpty())
                            ParseName(patientData, value);
                        break;
                    case (int)FieldListT1.DOB:
                        value = dataRow.GetValue((int)FieldListT1.DOB).Trim();
                        if( value.IsNotNullOrEmpty ())
                            patientData.DOB = DateTime.FromOADate(dataRow.GetValue((int)FieldListT1.DOB).ToDouble());
                        break;
                    case (int)FieldListT1.PolicyHicNumber:
                        value = dataRow.GetValue((int)FieldListT1.PolicyHicNumber).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.MedicareNumber = value;
                        break;
                    case (int) FieldListT1.SOC:
                        patientData.StartofCareDate = DateTime.FromOADate(dataRow.GetValue((int) FieldListT1.SOC).ToDouble());
                        break;
                    case (int)FieldListT1.drName:
                        value = dataRow.GetValue((int)FieldListT1.drName).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", Dr.: " + value;
                        break;
                    case (int)FieldListT1.drPhone:
                        value = dataRow.GetValue((int)FieldListT1.drPhone).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.Comments += ", Dr. Phone: " + value;
                        break;
                    case (int)FieldListT1.adm:
                        value = dataRow.GetValue((int)FieldListT1.adm).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", Admission Months: " + value;
                        break;
                    case (int)FieldListT1.admStatus:
                        value = dataRow.GetValue((int)FieldListT1.admStatus).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", Admission Status: " + value;
                        break;
                    case (int) FieldListT1.Icd9Code:
                        value = dataRow.GetValue((int)FieldListT1.Icd9Code).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", Diagnosis Code: " + value;
                        break;
                    case (int) FieldListT1.upin:
                        value = dataRow.GetValue((int)FieldListT1.upin).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", UPIN: " + value;
                        break;
                }
            }
        }

        /*********************************************************************/

        private static void ExtractRowTwo( DataRow dataRow, Patient patientData )
        {
            for (int col = (int)FieldListT2.fullAddress; col != ((int)FieldListT2.insName) + 1; col++)
            {
                string value = string.Empty;
                string[] names;

                switch (col)
                {
                    case (int) FieldListT2.fullAddress:
                        value = dataRow.GetValue((int)FieldListT2.fullAddress).Trim();
                        if( value.IsNotNullOrEmpty())
                            ParseFullAddress(value, patientData);
                        
                        break;

                    case (int) FieldListT2.patientPhone:
                        value = dataRow.GetValue((int)FieldListT2.patientPhone).Trim().ToPhoneDB();
                        if( value.IsNotNullOrEmpty())
                            patientData.PhoneHome = value;
                        break;

                    case (int) FieldListT2.insName:
                        value = dataRow.GetValue((int)FieldListT2.insName).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.PrimaryInsurance = value;
                        break;
                }
            }
        }

        /*********************************************************************/

        private static void ParseName(Patient patientData, string value)
        {
            string[] names;
            if (value.IsNotNullOrEmpty())
            {
                names = value.Split(',');
                if (names[0].IsNotNullOrEmpty()) patientData.LastName = names[0];
                if (names[1].Trim().Contains(" "))
                {
                    string[] splitChars = { " " };

                    string[] firstMiddle = names[1].Trim().Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                    if (firstMiddle[0].IsNotNullOrEmpty()) patientData.FirstName = firstMiddle[0];
                    if (firstMiddle[1].IsNotNullOrEmpty()) patientData.MiddleInitial = firstMiddle[1];
                }
                else if (names[1].IsNotNullOrEmpty()) patientData.FirstName = names[1];
            }
        }

        /*********************************************************************/

        private static void ParseFullAddress(string src, Patient p)
        {
            string[] address        = src.Trim().Split(',');
            string[] stateAndZip    = address[1].Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int cityLoc             = address[0].LastIndexOf(' ');
            p.AddressCity           = address[0].Substring(cityLoc, address[0].Length - cityLoc);
            p.AddressLine1          = address[0].Remove(cityLoc);
            p.AddressStateCode      = stateAndZip[0];
            p.AddressZipCode        = stateAndZip[1];
        }
    }
}
