﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using HtmlAgilityPack;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class VisiTrakPhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\DoctorListing.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\DoctorListing_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.Load(input);

                    int i = 1;
                    AgencyPhysician physicianData = null;
                    htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                    {
                        if (htmlNode.Name.IsEqual("html"))
                        {
                            htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                            {
                                if (bodyNode.Name.IsEqual("body"))
                                {
                                    bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                    {
                                        if (tableNode.DoesHtmlRowHavePhysicianData())
                                        {
                                            int dataCounter = 1;
                                            physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;

                                            tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                            {
                                                if (tRowNode.Name.IsEqual("tr"))
                                                {
                                                    tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                    {
                                                        if (dataNode.Name.IsEqual("td"))
                                                        {
                                                            dataNode.SetVisiTrakPhysician(dataCounter, physicianData);
                                                            dataCounter++;
                                                        }
                                                    });
                                                }
                                            });

                                            if (physicianData.Comments.IsNotNullOrEmpty())
                                            {
                                                physicianData.Comments = physicianData.Comments.Trim();
                                            }

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
