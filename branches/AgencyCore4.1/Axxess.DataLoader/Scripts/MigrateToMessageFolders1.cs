﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Threading;

    public static class MigrateToMessageFolders1
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\Result_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string output1 = Path.Combine(App.Root, string.Format("Files\\Writing_{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                //List<Agency> agencies = Database.GetAgencies();
                //try
                //{
                //    MigrateUserMessages(a);
                //}
                //catch (Exception e)
                //{

                //    textWriter.Write(e.ToString());
                //}
                int available;
                int maxLimit;
                using (ManualResetEvent resetEvent = new ManualResetEvent(false))
                {
                    var start = DateTime.Now;
                    List<Agency> agencies = Database.GetAgencies();
                    int total = agencies.Count;
                    agencies.ForEach(a =>
                        {
                            //ThreadPool.QueueUserWorkItem(delegate
                            ThreadPool.SetMaxThreads(8, 4);
                            //{
                                try
                                {
                                    MigrateUserMessages(a);
                                }
                                catch (Exception e)
                                {

                                    textWriter.Write(e.ToString());
                                }
                            //    if (Interlocked.Decrement(ref total) == 0)
                            //        resetEvent.Set();
                            //});
                            Console.WriteLine();
                            Console.WriteLine(string.Format("processing message for :{0}", a.Name));
                            ThreadPool.GetAvailableThreads(out available, out maxLimit);
                            Console.WriteLine((maxLimit - available).ToString() + " threads are running");

                        });



                        var users = Database.GetAllUsers();
                        int userNumbers = users.Count;
                        users.ForEach(u =>
                            {
                                ThreadPool.QueueUserWorkItem(delegate
                                {
                                    try
                                    {
                                        MigrateToMessageFolders.LoadSystemMessages(u);
                                    }
                                    catch (Exception ex)
                                    {
                                        textWriter.Write(ex.ToString());
                                    }
                                    if (Interlocked.Decrement(ref userNumbers) == 0)
                                        resetEvent.Set();
                                });
                                Console.WriteLine();
                                Console.WriteLine(string.Format("processing message for user:{0}", u.DisplayName));
                                ThreadPool.GetAvailableThreads(out available, out maxLimit);
                                Console.WriteLine((maxLimit - available).ToString() + " threads are running");
                            });
                        resetEvent.WaitOne();
                        var timeSpan = (DateTime.Now - start).TotalMinutes;
                        Console.WriteLine();
                        Console.WriteLine(string.Format("Total Time spent:{0} mins ",timeSpan.ToString() ));
                    }
                
            }
                
                
                Console.WriteLine("-----------------------------------------DONE--------------------------------------------");
            
        }


        public static void LoadSystemMessages(User user)
        {
            var userSystemMessages = new List<UserMessage>();
            
                    List<MessageState> messageState = user.Messages.ToObject<List<MessageState>>();

                    if (messageState != null)
                    {
                        messageState.ForEach(ms =>
                            {
                                UserMessage um = new UserMessage();
                                um.Id = Guid.NewGuid();
                                um.MessageId = ms.Id;
                                um.UserId = user.Id;
                                um.MessageType = (int)MessageType.System;
                                um.IsRead = ms.IsRead;
                                um.IsDeprecated = ms.IsDeprecated;
                                um.FolderId = Guid.Empty;
                                um.ThreadId = Guid.Empty;
                                userSystemMessages.Add(um);
                            }
                        );
                    }
                   
            Database.AddMany<UserMessage>(userSystemMessages);
        }


        public static bool MigrateUserMessages(Agency agency)
        {
            bool success = false;

            Console.WriteLine();
            using (TextWriter textWriter = new StreamWriter(output1, true))
            {
                List<User> users = Database.GetUsers(agency.Id);
                if (users != null)
                {
                    users.ForEach(u =>
                        {
                            textWriter.WriteLine(string.Format("Process message for user id:{0} in agency:{1}",u.Id,u.AgencyId));
                            List<Message> userMessages = Database.GetMessagesByAgencyAndUser(u.AgencyId,u.Id);
                            if (userMessages.IsNotNullOrEmpty())
                            {
                                Message lastMessage = new Message();
                                lastMessage.FromId = Guid.Empty;
                                Guid linkGuid = Guid.Empty;
                                userMessages.ForEach(message =>
                                {
                                    bool makeNew = true;
                                    bool body = false;
                                    bool receipt = false;
                                    //if (message.Subject.IsNotNullOrEmpty()&&lastMessage.Subject.IsNotNullOrEmpty())
                                    //{
                                    //    if (lastMessage.Subject.CompareTo(message.Subject) != 0)
                                    //    {
                                    //        subject = false;
                                    //    }
                                    //}
                                    if (message.Body.IsNotNullOrEmpty() && lastMessage.Body.IsNotNullOrEmpty())
                                    {
                                        if (lastMessage.Body.CompareTo(message.Body) == 0)
                                        {
                                            body = true;
                                        }
                                    }
                                    if (message.RecipientNames.IsNotNullOrEmpty() && lastMessage.RecipientNames.IsNotNullOrEmpty())
                                    {
                                        if (lastMessage.RecipientNames.CompareTo(message.RecipientNames) == 0)
                                        {
                                            receipt = true;
                                        }
                                    }

                                    if (body && receipt)
                                    {
                                        makeNew = false;
                                    };
                                    linkGuid = (makeNew ? Guid.NewGuid() : linkGuid);
                                    Database.MigrateUserMessage(message, linkGuid, makeNew);
                                    lastMessage = (makeNew ? message : lastMessage);

                                }
                            );
                            }
                        });
                }
            }
            return success;
        }
    }
}
