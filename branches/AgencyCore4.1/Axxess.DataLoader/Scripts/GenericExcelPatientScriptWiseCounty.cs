﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Collections.Generic;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class GenericExcelPatientScriptWiseCounty
    {
        private static string input = Path.Combine(App.Root, "Files\\generic_patient.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\LoadLog_{0}{1}{2}.log", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year ));

        /// <summary>
        /// Field list for ANY excel patient spreadsheet that contains 1 data item per colum.
        /// Simple adjust this field list to have the correct column for the data item,
        /// then go adjust the case statement in ExtractPatientData() and your spreadsheet
        /// should import flawlessly into our databases.
        /// </summary>

        enum FieldListT1 : int
        {
            MRNumber        = 0,
            LastName        = 1,
            FirstName       = 2,
            Gender          = 3,
            DOB             = 4,
            Phone           = 5,
            Address1        = 6,
            City            = 7,
            State           = 8,
            Zip             = 9,
            StartDate       = 10,
            ICD9_Primary    = 11,
            ICD9_Secondary  = 12,
            DocName         = 13,
            InsName         = 14
        }

        /*********************************************************************/

        enum FieldListT2 : int
        {
            fullAddress     = 1,
            patientPhone    = 14,
            insName         = 32
        }

        /*********************************************************************/
        
        // set this to the ACTUAL number of rows that contain data
        // on your excel spreadsheet. DO NOT include empty rows
        // that are between records.

        private const int ROWS_PER_RECORD = 1;
        private const bool FIRST_ROW_AS_COL_NAMES = true;

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = FIRST_ROW_AS_COL_NAMES;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    int currentRow          = 1;
                                    Patient patientData     = null;
                                    int count = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            switch (currentRow)
                                            {
                                                case 1:
                                                    patientData = CreatePatientObject(agencyId, locationId);
                                                    ExtractRowOne(dataRow, patientData);
                                                    currentRow++;
                                                    break;
                                                case 2:
                                                    //ExtractRowTwo(dataRow, patientData);
                                                    break;
                                                default:
                                                    break;
                                            }
                                            if (currentRow >= ROWS_PER_RECORD)
                                            {
                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;
                                                AddPatientToDatabase(agencyId, textWriter, patientData);
                                                Console.WriteLine("{0}) {1}", count, patientData.DisplayName);
                                                currentRow = 1;
                                                count++;
                                            }                                            
                                        }
                                        else
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }

        /*********************************************************************/

        private static Patient CreatePatientObject(Guid agencyId, Guid locationId)
        {
            Patient p = new Patient()
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                AgencyLocationId = locationId,
                Status = 1,
                Ethnicities = string.Empty,
                MaritalStatus = string.Empty,
                IsDeprecated = false,
                IsHospitalized = false,
                PatientIdNumber = string.Empty,
                Gender = ""
            };
            return p;
        }

        /*********************************************************************/

        private static bool AddPatientToDatabase(Guid agencyId, TextWriter textWriter, Patient patientData)
        {
            bool bSuccess = false;

            var medicationProfile = new MedicationProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Medication = "<ArrayOfMedication />"
            };

            var allergyProfile = new AllergyProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Allergies = "<ArrayOfAllergy />"
            };

            bSuccess = Database.Add<Patient>(patientData);
            if (bSuccess)
            {
                textWriter.WriteLine("Patient record added for: " + patientData.DisplayName);
                bSuccess = Database.Add < MedicationProfile>(medicationProfile);
                if (bSuccess)
                {
                    textWriter.WriteLine("Medication profile added for: " + patientData.DisplayName);
                    bSuccess = Database.Add<AllergyProfile>(allergyProfile);
                    if (bSuccess)
                    {
                        textWriter.WriteLine("Allergy profile added for: " + patientData.DisplayName);
                    }
                    else
                    {
                        textWriter.WriteLine("Allergy profile add failed for: " + patientData.DisplayName);
                    }
                }
                else
                {
                    textWriter.WriteLine("Medication profile add failed for: " + patientData.DisplayName);
                }
            }
            else
            {
                textWriter.WriteLine("Add failed for patient: " + patientData.DisplayName);
            }

            if (bSuccess)
            {
                var admissionPeriod = new PatientAdmissionDate
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    Created = DateTime.Now,
                    DischargedDate = DateTime.MinValue,
                    IsActive = true,
                    IsDeprecated = false,
                    Modified = DateTime.Now,
                    PatientData = patientData.ToXml().Replace("'", ""),
                    PatientId = patientData.Id,
                    Reason = string.Empty,
                    StartOfCareDate = patientData.StartofCareDate,
                    Status = patientData.Status
                };

                bSuccess = Database.Add < PatientAdmissionDate>(admissionPeriod);
                if (bSuccess)
                {
                    var patient = Database.GetPatient(patientData.Id, agencyId);
                    if (patient != null)
                    {
                        patient.AdmissionId = admissionPeriod.Id;
                        bSuccess = Database.Update(patient);
                        if (bSuccess)
                        {
                            //Console.WriteLine("{0}", patientData.DisplayName);
                            textWriter.WriteLine("{0}", patientData.DisplayName);
                        }
                    }
                }
                else
                {
                    textWriter.WriteLine("Failed adding admission period for: " + patientData.DisplayName);
                }
            }

            return bSuccess;
        }

        /*********************************************************************/

        private static void ExtractRowOne( DataRow dataRow, Patient patientData )
        {
            for (int col = (int)FieldListT1.MRNumber; col != ((int)FieldListT1.InsName) + 1; col++)
            {
                string value = string.Empty;
                string[] names;

 
                switch (col)
                {
                    case (int)FieldListT1.MRNumber:
                        value = dataRow.GetValue( (int)FieldListT1.MRNumber ).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.PatientIdNumber = value;
                        break;
                    case (int)FieldListT1.LastName:
                        value = dataRow.GetValue((int)FieldListT1.LastName).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.LastName = value;
                        break;

                    case (int)FieldListT1.FirstName:
                        value = dataRow.GetValue((int)FieldListT1.FirstName).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.FirstName = value;
                        break;

                    case (int)FieldListT1.DOB:
                        value = dataRow.GetValue((int)FieldListT1.DOB).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.DOB = DateTime.FromOADate(value.ToDouble());
                        break;
                    case (int)FieldListT1.Gender:
                        value = dataRow.GetValue((int)FieldListT1.Gender).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Gender = value;
                        break;
                    case (int) FieldListT1.StartDate:
                        value = dataRow.GetValue((int)FieldListT1.StartDate);
                        if( value.IsNotNullOrEmpty())
                            patientData.StartofCareDate = DateTime.FromOADate(value.ToDouble());
                        break;
                    case (int)FieldListT1.DocName:
                        value = dataRow.GetValue((int)FieldListT1.DocName).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.Comments += ", Dr.: " + value;
                        break;
                    case (int)FieldListT1.ICD9_Primary:
                        value = dataRow.GetValue((int)FieldListT1.ICD9_Primary).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.Comments += ", Primary Diagnosis: " + value;
                        break;
                    case (int)FieldListT1.ICD9_Secondary:
                        value = dataRow.GetValue((int)FieldListT1.ICD9_Secondary).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.Comments += ", Secondary Diagnosis: " + value;
                        break;
                    case (int)FieldListT1.InsName:
                        value = dataRow.GetValue((int)FieldListT1.InsName).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.PrimaryInsurance = value;
                        break;
                    case (int)FieldListT1.Address1:
                        value = dataRow.GetValue((int)FieldListT1.Address1).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.AddressLine1 = value;
                        break;
                    case (int)FieldListT1.City:
                        value = dataRow.GetValue((int)FieldListT1.City).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.AddressCity = value;
                        break;
                    case (int)FieldListT1.State:
                        value = dataRow.GetValue((int)FieldListT1.State).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.AddressStateCode= value;
                        break;
                    case (int)FieldListT1.Zip:
                        value = dataRow.GetValue((int)FieldListT1.Zip).Trim();
                        if (value.IsNotNullOrEmpty())
                            patientData.AddressZipCode = value;
                        break;

                }
            }
        }

        /*********************************************************************/

        private static void ExtractRowTwo( DataRow dataRow, Patient patientData )
        {
            for (int col = (int)FieldListT2.fullAddress; col != ((int)FieldListT2.insName) + 1; col++)
            {
                string value = string.Empty;
                string[] names;

                switch (col)
                {
                    case (int) FieldListT2.fullAddress:
                        value = dataRow.GetValue((int)FieldListT2.fullAddress).Trim();
                        if( value.IsNotNullOrEmpty())
                            ParseFullAddress(value, patientData);
                        
                        break;

                    case (int) FieldListT2.patientPhone:
                        value = dataRow.GetValue((int)FieldListT2.patientPhone).Trim().ToPhoneDB();
                        if( value.IsNotNullOrEmpty())
                            patientData.PhoneHome = value;
                        break;

                    case (int) FieldListT2.insName:
                        value = dataRow.GetValue((int)FieldListT2.insName).Trim();
                        if( value.IsNotNullOrEmpty())
                            patientData.PrimaryInsurance = value;
                        break;
                }
            }
        }

        /*********************************************************************/

        private static void ParseName(Patient patientData, string value)
        {
            string[] names;
            if (value.IsNotNullOrEmpty())
            {
                names = value.Split(',');
                if (names[0].IsNotNullOrEmpty()) patientData.LastName = names[0];
                if (names[1].Trim().Contains(" "))
                {
                    string[] splitChars = { " " };

                    string[] firstMiddle = names[1].Trim().Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                    if (firstMiddle[0].IsNotNullOrEmpty()) patientData.FirstName = firstMiddle[0];
                    if (firstMiddle[1].IsNotNullOrEmpty()) patientData.MiddleInitial = firstMiddle[1];
                }
                else if (names[1].IsNotNullOrEmpty()) patientData.FirstName = names[1];
            }
        }

        /*********************************************************************/

        private static void ParseFullAddress(string src, Patient p)
        {
            string[] address        = src.Trim().Split(',');
            string[] stateAndZip    = address[1].Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int cityLoc             = address[0].LastIndexOf(' ');
            p.AddressCity           = address[0].Substring(cityLoc, address[0].Length - cityLoc);
            p.AddressLine1          = address[0].Remove(cityLoc);
            p.AddressStateCode      = stateAndZip[0];
            p.AddressZipCode        = stateAndZip[1];
        }
    }
}
