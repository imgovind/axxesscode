﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using System.Threading;

    public static class AgencyAddUploadTypeScript
    {
        public static void Run()
        {
            var agencies = Database.GetAgencies();
            if (agencies != null && agencies.Count > 0)
            {
                Console.WriteLine("Agency List Count: {0}", agencies.Count);
                int count = 0;
                List<UploadType> types = new List<UploadType>();
                agencies.ForEach(agency =>
                {
                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Admission Documents"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Consent to Photograph"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Lab Results"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Advanced Directives"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Consent Form"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Patient Choice Statment"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Encounter Details"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Family Medical History"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Individual Medical History"
                    });

                    types.Add(new UploadType
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agency.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsDeprecated = false,
                        Type = "Hospital Discharge Summary"
                    });

                    count++;
                    Console.SetCursorPosition(0, 2);
                    Console.WriteLine(count + "/" + agencies.Count);
                });
                Console.WriteLine("Adding classifications to all agencies.");
                Database.AddMany<UploadType>(types);

            }
        }
    }
}
