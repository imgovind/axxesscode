﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class SynergyThreeScript
    {
        private static string input = Path.Combine(App.Root, "Files\\synergy_report.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\synergy_report_{0}.txt", DateTime.Now.Ticks.ToString()));

        enum RowOneFields : int
        {
            Name = 1,
            PatientCode = 3,
            PolicyNumber = 4,
            Dob = 5,
            StartOfCare = 6,
            Admitted = 7,
            Adm = 8,
            DXCode = 9,
            PhysicianName = 10

        }
        /*********************************************************************/


        private static void ProcessRowOne(DataRow dataRow, Patient patientData)
        {
            throw new NotImplementedException();
        }

        /*********************************************************************/


        private static void ProcessRowTwo(DataRow dataRow, Patient patientData)
        {
            var names = dataRow.GetValue(0).Split(',');
            patientData.LastName = names[0].Trim();
            if (names.Length > 1)
                patientData.FirstName = names[1].Trim();
        }

        /*********************************************************************/

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i               = 0;
                                    int dataCounter     = 1;
                                    Patient patientData = null;
                                    int rowCount        = 0;

                                    foreach (DataRow row in dataTable.Rows)
                                    {
                                        bool bSuccess = false;

                                        switch (rowCount)
                                        {
                                            case 0: // process row 1 columns
                                                patientData = InitPatient(agencyId, locationId);
                                                ProcessRowOne(row, patientData);
                                                rowCount++;
                                                break;
                                            case 1: // process row 2 columns
                                                ProcessRowTwo(row, patientData);
                                                rowCount++;
                                                break;
                                            case 2: // blank row. do nothing.
                                                rowCount = 0;
                                                break;
                                        }
                                        patientData.Created = DateTime.Now;
                                        patientData.Modified = DateTime.Now;
                                        bSuccess = LinkPatientProfiles(agencyId, patientData);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }

        /*********************************************************************/

        private static bool LinkPatientProfiles(Guid agencyId, Patient patientData)
        {
            bool bSuccess = false;

            var medicationProfile = CreateMedicationProfile( agencyId, patientData);
            var allergyProfile = CreateAllergyProfile( agencyId, patientData);

            bSuccess = Database.Add(patientData);
            if (bSuccess)
            {
                bSuccess = Database.Add(medicationProfile);
                if (bSuccess)
                {
                    bSuccess = Database.Add(allergyProfile);
                }
            }

            if (bSuccess)
            {
                var admissionPeriod = CreateAdmissionPeriod( agencyId, patientData);
                bSuccess = Database.Add(admissionPeriod);
                if ( bSuccess )
                {
                    var patient = Database.GetPatient(patientData.Id, agencyId);
                    if (patient != null)
                    {
                        patient.AdmissionId = admissionPeriod.Id;
                        bSuccess = Database.Update(patient);
                        if (bSuccess)
                        {
                            Console.WriteLine("{0}) Added Successfully.", patientData.DisplayName);
                        }
                    }
                }
            }
            return bSuccess;
        }

        /*********************************************************************/

        private static PatientAdmissionDate CreateAdmissionPeriod( Guid agencyId, Patient patientData)
        {
            var admissionPeriod = new PatientAdmissionDate()
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                Created = DateTime.Now,
                DischargedDate = DateTime.MinValue,
                IsActive = true,
                IsDeprecated = false,
                Modified = DateTime.Now,
                PatientData = patientData.ToXml().Replace("'", ""),
                PatientId = patientData.Id,
                Reason = string.Empty,
                StartOfCareDate = patientData.StartofCareDate,
                Status = patientData.Status
            };
            return admissionPeriod;
        }

        /*********************************************************************/

        private static AllergyProfile CreateAllergyProfile( Guid agencyId, Patient patientData)
        {
            var allergyProfile = new AllergyProfile()
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Allergies = "<ArrayOfAllergy />"
            };
            return allergyProfile;
        }

        /*********************************************************************/

        private static MedicationProfile CreateMedicationProfile( Guid agencyId, Patient patientData)
        {
            var medicationProfile = new MedicationProfile()
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Medication = "<ArrayOfMedication />"
            };
            return medicationProfile;
        }

        /*********************************************************************/

        private static Patient InitPatient(Guid agencyId, Guid locationId)
        {
            Patient p = new Patient();
            p.Id = Guid.NewGuid();
            p.AgencyId = agencyId;
            p.AgencyLocationId = locationId;
            p.Status = 1;
            p.Ethnicities = string.Empty;
            p.MaritalStatus = string.Empty;
            p.IsDeprecated = false;
            p.IsHospitalized = false;
            p.Status = 1;
            p.Gender = string.Empty;
            p.AddressLine1 = string.Empty;
            p.AddressCity = string.Empty;
            p.AddressStateCode = string.Empty;
            p.AddressZipCode = string.Empty;
            p.PhoneHome = string.Empty;
            p.PrimaryRelationship = string.Empty;
            p.SecondaryRelationship = string.Empty;
            p.TertiaryRelationship = string.Empty;
            return p;
        }
    }
}
										
