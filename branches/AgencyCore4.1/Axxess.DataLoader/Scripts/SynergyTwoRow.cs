﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class SynergyTwoRow
    {
        private static string input = Path.Combine(App.Root, "Files\\patient-active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\patient-active_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 2;
                                int rowCounter = 1;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (dataRow.GetValue(0).ToLower().Contains("patient"))
                                            continue;
                                        else if (dataRow.GetValue(0).ToLower().Contains("soc"))
                                            continue;
                                        if (rowCounter % numberOfRows == 1)
                                        {
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 2;
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;

                                            var nameRow = dataRow.GetValue(0);
                                            var nameSplit = nameRow.Split(',');
                                            var namePart = nameSplit[0];
                                            var idPart = nameSplit[1];
                                            var nameArray = namePart.Split(' ');
                                            if (nameArray.Length > 3)
                                            {
                                                patientData.FirstName = nameArray[2].Trim();
                                                patientData.LastName = nameArray[0].Trim();
                                            }
                                            else if (nameArray.Length > 2)
                                            {
                                                patientData.FirstName = nameArray[1].Trim();
                                                patientData.LastName = nameArray[0].Trim();
                                            }
                                            var id = idPart.Split(')')[0];
                                            patientData.PatientIdNumber = id.Trim();

                                            var addressRow = dataRow.GetValue(1);
                                            var addressArray = addressRow.Split(new string[] {"  "},StringSplitOptions.None);
                                            if (addressArray.Length >= 2)
                                            {
                                                patientData.AddressLine1 = addressArray[0].Trim();
                                                patientData.AddressZipCode = addressArray[2].Trim();
                                                var cityState = addressArray[1].Split(',');
                                                if (cityState.Length >= 2)
                                                {
                                                    patientData.AddressCity = cityState[0].Trim();
                                                    patientData.AddressStateCode = cityState[1].Trim();
                                                }
                                            }

                                            var phone = dataRow.GetValue(2).ToPhoneDB();
                                            patientData.PhoneHome = phone;
                                            var hic = dataRow.GetValue(3);
                                            if (hic.Length == 10 && !hic.IsDigitsOnly())
                                            {
                                                patientData.MedicareNumber = hic;
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("Insurance: {0}. ",hic);
                                            }
                                        }
                                        else if (rowCounter % numberOfRows == 0)
                                        {

                                            var startOfCare = dataRow.GetValue(0);
                                            //patientData.StartofCareDate = startOfCare.IsValidDate() ? startOfCare.ToDateTime() : DateTime.MinValue;
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(startOfCare));
                                            var statCode = dataRow.GetValue(1);
                                            if (statCode.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Stat Code: {0}. ", statCode);
                                            }
                                            var statDate = dataRow.GetValue(2);
                                            if (statDate.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Stat Date: {0}. ", DateTime.FromOADate(double.Parse(statDate)));
                                            }
                                            var medicalRecord = dataRow.GetValue(4);
                                            if (medicalRecord.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Medical Record: {0}. ", medicalRecord);
                                            }
                                            var primaryDiagnosis = dataRow.GetValue(5);
                                            if (primaryDiagnosis.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis: {0}. ", primaryDiagnosis);
                                            }
                                            var physician = dataRow.GetValue(6);
                                            if (physician.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician: {0}. ", physician);
                                            }
                                            var caseAdmin = dataRow.GetValue(7);
                                            if (caseAdmin.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Case Admin: {0}. ", caseAdmin);
                                            }

                                            patientData.Gender = "";

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            var a = 5;
                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                            }
                                            textWriter.Write(textWriter.NewLine);
                                            i++;
                                        }
                                        rowCounter++;
                                    }
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }
    }
}
