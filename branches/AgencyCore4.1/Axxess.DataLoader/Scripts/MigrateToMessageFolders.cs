﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Threading;

    public static class MigrateToMessageFolders
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\Result_{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                int available;
                int maxLimit;
                using (ManualResetEvent resetEvent = new ManualResetEvent(false))
                {
                    var start = DateTime.Now;
                    List<Agency> agencies = Database.GetAgencies();
                    int total = agencies.Count;
                    var a = Database.GetAgency(new Guid("ff85de54-aa66-4ad0-8bfe-5b2a5f772e16"));

                    //agencies.ForEach(a =>
                    //{
                        List<User> userList = Database.GetUsers(a.Id);
                        textWriter.WriteLine("Migrating agency: {0}", a.Id.ToString());

                        userList.ForEach(u =>
                        {
                            ThreadPool.QueueUserWorkItem(delegate
                            {
                                ThreadPool.SetMaxThreads(8, 6);
                                {
                                    try
                                    {                                        
                                        MigrateUserMessages(a, u);
                                    }
                                    catch (Exception e)
                                    {
                                        textWriter.Write(e.ToString());
                                    }
                                    //if (Interlocked.Decrement(ref total) == 0)
                                    //    resetEvent.Set();
                                }
                            });
                            Console.WriteLine();
                            Console.WriteLine(string.Format("processing message for :{0}", a.Name));
                            ThreadPool.GetAvailableThreads(out available, out maxLimit);
                            Console.WriteLine((maxLimit - available).ToString() + " threads are running");
                        });

                    //});



                    var users = Database.GetAllUsers();
                    int userNumbers = users.Count;
                    users.ForEach(u =>
                    {
                        ThreadPool.QueueUserWorkItem(delegate
                        {
                            try
                            {
                                MigrateToMessageFolders.LoadSystemMessages(u);
                            }
                            catch (Exception ex)
                            {
                                textWriter.Write(ex.ToString());
                            }
                            //if (Interlocked.Decrement(ref userNumbers) == 0)
                            //    resetEvent.Set();
                        });
                        Console.WriteLine();
                        Console.WriteLine(string.Format("processing message for user:{0}", u.DisplayName));
                        ThreadPool.GetAvailableThreads(out available, out maxLimit);
                        Console.WriteLine((maxLimit - available).ToString() + " threads are running");
                    });
                    //resetEvent.WaitOne();
                    var timeSpan = (DateTime.Now - start).TotalMinutes;
                    Console.WriteLine();
                    Console.WriteLine(string.Format("Total Time spent:{0} mins ", timeSpan.ToString()));
                }

            }
            Console.WriteLine("-----------------------------------------DONE--------------------------------------------");
        }


        public static void LoadSystemMessages(User user)
        {
            var userSystemMessages = new List<UserMessage>();
            
                    List<MessageState> messageState = user.Messages.ToObject<List<MessageState>>();

                    if (messageState != null)
                    {
                        messageState.ForEach(ms =>
                            {
                                UserMessage um = new UserMessage();
                                um.Id = Guid.NewGuid();
                                um.MessageId = ms.Id;
                                um.UserId = user.Id;
                                um.MessageType = (int)MessageType.System;
                                um.IsRead = ms.IsRead;
                                um.IsDeprecated = ms.IsDeprecated;
                                um.FolderId = Guid.Empty;
                                um.ThreadId = Guid.Empty;
                                userSystemMessages.Add(um);
                            }
                        );
                    }                   
            Database.AddMany<UserMessage>(userSystemMessages);
        }


        public static bool MigrateUserMessages(Agency agency, User user)
        {
            bool success = false;

            Console.WriteLine("Starting agency: {0}", agency.Name);

            Message lastMessage = new Message();
            lastMessage.FromId = Guid.Empty;
            Guid linkGuid = Guid.Empty;


            List<Message> userMessages = Database.GetMessagesByAgencyAndUser(agency.Id, user.Id);

            if (userMessages.IsNotNullOrEmpty())
            {
                userMessages.ForEach(message =>
                    {
                        bool makeNew = false;

                        if (lastMessage.FromId.CompareTo(message.FromId) == 0) // continue check
                        {
                            if (lastMessage.Subject.CompareTo(message.Subject) == 0) // continue check
                            {
                                if (lastMessage.RecipientNames.CompareTo(message.RecipientNames) == 0) // continue check
                                {
                                    if (lastMessage.Body.CompareTo(message.Body) == 0)
                                    {
                                        if (lastMessage.RecipientId.CompareTo(message.RecipientId) != 0) // recipient only changed
                                        {
                                            // add entry to UserMessages
                                            makeNew = false;
                                        }
                                    }
                                    else // body changed. new message and usermessage
                                    {
                                        makeNew = true;
                                    }
                                }
                                else // subject changed, new message and usermessage
                                {
                                    makeNew = true;
                                }
                            }
                            else // recipient names changed. new message and usermessage
                            {
                                makeNew = true;
                            }
                        }
                        else // new sender. new message and usermessage
                        {
                            makeNew = true;
                        }

                        //if (makeNew)
                        //{
                        //    Console.WriteLine("Sender: {0}\t Recipients: {1}", message.FromName, message.RecipientNames);
                        //}

                        linkGuid = (makeNew ? Guid.NewGuid() : linkGuid);
                        Database.MigrateUserMessage(message, linkGuid, makeNew);
                        lastMessage = (makeNew ? message : lastMessage);
                    }
                );
            }
               
            return success;
        }
    }
}
