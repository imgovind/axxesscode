﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class VisitrackExcelPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\WiseCountyHomeHealthPhysicians.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AddedToAgencyPhysicians{0}.txt", DateTime.Now.Ticks.ToString()));
        
        public class PhysicianAddress {
            public string streetAddress1    { get; set; }
            public string streetAddress2    { get; set; }
            public string city              { get; set; }
            public string state             { get; set; }
            public string zipCode           { get; set; }
        }


        public static void Run( Guid agencyId )
        {
            int NAME = 0,
                SPECIALTY = 1,
                NPI = 2,
                ADDRESS = 3,
                OFFICE_PHONE = 4,
                FAX_PHONE = 5,
                PECOS = 6;

            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if (dataRow.GetValue(NPI).Trim().IsNotNullOrEmpty())
                                            {
                                                var physicianData = new AgencyPhysician();

                                                physicianData.Id = Guid.NewGuid();
                                                physicianData.AgencyId = agencyId;
                                                physicianData.IsDeprecated = false;

                                                string unparsedString = dataRow.GetValue(NAME);
                                                string[] splitString = unparsedString.Split(',');

                                                physicianData.LastName = splitString[0].Trim();

                                                if (splitString.Length > 0)
                                                    unparsedString = splitString[1].Trim();
                                                else
                                                    unparsedString = string.Empty;

                                                splitString = unparsedString.Trim().Split(' ');

                                                if (splitString.Length > 0)
                                                    physicianData.FirstName = splitString[0].Trim(); 

                                                if (splitString.Length > 1)
                                                {
                                                    if (splitString[1].IsNotNullOrEmpty())
                                                        physicianData.MiddleName = splitString[1].Trim();
                                                }
                                                physicianData.Credentials = dataRow.GetValue(SPECIALTY).Trim();


                                                PhysicianAddress address = ParseAddress(dataRow.GetValue(ADDRESS));
                                                physicianData.AddressLine1 = address.streetAddress1;
                                                physicianData.AddressCity = address.city;
                                                physicianData.AddressStateCode = address.state;
                                                physicianData.AddressZipCode = address.zipCode;

                                                physicianData.PhoneWork = dataRow.GetValue(FAX_PHONE).ToPhoneDB();
                                                physicianData.FaxNumber = dataRow.GetValue(OFFICE_PHONE).ToPhoneDB();

                                                physicianData.NPI = dataRow.GetValue(NPI).Trim();
                                                physicianData.IsPecosVerified = (dataRow.GetValue(PECOS).ToLower().Trim().CompareTo("yes") == 0);

                                                physicianData.PhysicianAccess = true;
                                                physicianData.Comments = string.Empty;
                                                physicianData.Created = DateTime.Now;
                                                physicianData.Modified = DateTime.Now;

                                                
                                                var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                                string strDebug = string.Empty;

                                                if (physician == null)
                                                {
                                                    bool success = Database.Add(physicianData);
                                                    if (success)
                                                    {
                                                        strDebug = string.Format(
                                                            "FName: {0}\tLName {1}\t MName: {2}\n" +
                                                            "Addr1: {3}\tAddr2 {4}\n" +
                                                            "City: {5} State: {6} Zip: {7}\n" +
                                                            "Phone: {8}\n",
                                                            physicianData.FirstName,
                                                            physicianData.LastName,
                                                            physicianData.MiddleName,
                                                            physicianData.AddressLine1,
                                                            physicianData.AddressLine2,
                                                            physicianData.AddressCity,
                                                            physicianData.AddressStateCode,
                                                            physicianData.AddressZipCode,
                                                            physicianData.PhoneWork);

                                                        Console.WriteLine(strDebug);
                                                        textWriter.WriteLine(strDebug);
                                                    }
                                                }
                                                else
                                                {
                                                    strDebug = string.Format("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                                    Console.WriteLine(strDebug);
                                                    textWriter.WriteLine(strDebug);
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                    Console.WriteLine("{0} records added.", i);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.Message, ex.StackTrace);
                    System.Diagnostics.Trace.WriteLine(ex.Message, ex.StackTrace);
                }
                finally
                {
                    textWriter.Flush();
                    textWriter.Close();
                }
            }
        }

        public static PhysicianAddress ParseAddress(string strAddress)
        {
            // note: this will need to be changed for different
            // file formats. this assumes the address, city, state, and zip
            // are separated by commas in the source file.
            PhysicianAddress address = new PhysicianAddress();
            int streetAddress = 0,
                city = 1,
                state = 2,
                zip = 3;

            try
            {

                string[] temp = strAddress.Split(',');

                address.streetAddress1 = temp[streetAddress].Trim();
                address.streetAddress2 = string.Empty;
                address.city = temp[city].Trim();

                switch (temp[state].ToLower().Trim())
                {
                    case "texas":
                        address.state = "TX";
                        break;
                    case "alabama":
                        address.state = "AL";
                        break;
                    case "massachusetts":
                        address.state = "MA";
                        break;
                    default:
                        address.state = temp[state].Trim();
                        break;
                }

                if (temp[zip].Trim().Length > 0)
                    address.zipCode = temp[zip].Trim().Substring(0, 5);
                else
                    address.zipCode = string.Empty;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message, e.StackTrace);
            }

            return address;
            
        }
    }
}
