﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class GenericExcelScript3
    {
        private static string input = Path.Combine(App.Root, "Files\\patient_data.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PatientData_{0}.txt", DateTime.Now.ToFileTime().ToString()));

        enum DataFields : int
        {
            MedicalRecordNumber	= 0,
            LastName = 1,
            FirstName = 2,
            Gender	= 3,
            HIC = 4,
            MedicaidNumber = 5,
            DateOfBirth = 6,
            Phone	= 7,
            Address	= 8,
            City	= 9,
            State	= 10,
            ZIPCode	= 11,
            EmailAddress	= 12,
            StartOfCare	= 13,
            StartDate	= 14,
            EndDate	= 15,
            PrimaryDiagnosis = 16,
            SecondaryDiagnosis	= 17,
            Disciplines	= 18,
            DisciplineFrequencies	= 19,
            Triage	= 20,
            PhysicianName	= 21,
            NPI	= 22, 
            PhysicianPhone	= 23,
            PhysicianFacsimile	= 24,
            PrimaryClinician	= 25,
            Branch	= 26,
            Insurance	= 27
        }

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            Patient patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.PatientIdNumber = dataRow.GetValue((int)DataFields.MedicaidNumber);
                                            patientData.LastName = dataRow.GetValue((int)DataFields.LastName);
                                            patientData.FirstName = dataRow.GetValue((int)DataFields.FirstName);
                                            patientData.Gender = dataRow.GetValue((int)DataFields.Gender);
                                            patientData.MedicaidNumber = dataRow.GetValue((int)DataFields.MedicaidNumber);


                                            patientData.DOB = patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue((int)DataFields.DateOfBirth)));

                                            patientData.PhoneHome = dataRow.GetValue((int)DataFields.Phone);
                                            patientData.AddressLine1 = dataRow.GetValue((int)DataFields.Address);
                                            patientData.AddressCity = dataRow.GetValue((int)DataFields.City);
                                            patientData.AddressStateCode = dataRow.GetValue((int)DataFields.State);
                                            patientData.AddressZipCode = dataRow.GetValue((int)DataFields.ZIPCode);


                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue((int)DataFields.StartOfCare)));
                                            patientData.Comments =
                                                string.Format("Primary DX: {0}. ", dataRow.GetValue((int)DataFields.PrimaryDiagnosis)) +
                                                string.Format("Secondary DX: {0}. ", dataRow.GetValue((int)DataFields.SecondaryDiagnosis)) +
                                                string.Format("Disciplines: {0}. ", dataRow.GetValue((int)DataFields.Disciplines)) +
                                                string.Format("Primary Clinician: {0}. ", dataRow.GetValue((int)DataFields.PrimaryClinician)) +
                                                string.Format("Discipline Frequencies: {0}. ", dataRow.GetValue((int)DataFields.DisciplineFrequencies)) +
                                                string.Format("Triage: {0}. ", dataRow.GetValue((int)DataFields.Triage)) +
                                                string.Format("PhyscianName: {0}. ", dataRow.GetValue((int)DataFields.PhysicianName));
                                            patientData.PrimaryInsurance = dataRow.GetValue((int)DataFields.Insurance);
                                            patientData.SecondaryInsurance = null;
                                            patientData.TertiaryInsurance = null;

                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                             
                                            
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;
                                                 

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            
                                            bool patientLoaded = Database.Add<Patient>(patientData);
                                            bool medicationLoaded = Database.Add<MedicationProfile>(medicationProfile);
                                            bool allergyLoaded = Database.Add<AllergyProfile>(allergyProfile);

                                            if ( patientLoaded && medicationLoaded && allergyLoaded )
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add<PatientAdmissionDate>(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
