﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    enum DataFields : int
    {
        FirstName = 0,
        LastName = 1,
        Credentials = 2,
        Address = 3,
        City = 4,
        State = 5,
        Zip = 6,
        Phone = 7,
        Fax = 8,
        NPI = 10,
    }

    public static class VisitrackExcelPhysician2
    {
        private static string input = Path.Combine(App.Root, "Files\\DoctorListing.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AddedToAgencyPhysicians{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run( Guid agencyId )
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 0;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if (dataRow.GetValue((int) DataFields.NPI).Trim().IsNotNullOrEmpty())
                                            {
                                                //string name = dataRow.GetValue((int) DataFields.Name);
                                                //string [] firstAndLast = name.Trim().Split(',');
                                                //string middleName = string.Empty;
                                                
                                                //firstAndLast[0] = firstAndLast[0].Trim();
                                                //firstAndLast[1] = firstAndLast[1].Trim();

                                                //if( firstAndLast[1].Contains(' ') )
                                                //{
                                                //    middleName = firstAndLast[1].Trim().Split(' ')[1];
                                                //    firstAndLast[1] = firstAndLast[1].Remove(firstAndLast[1].IndexOf(' '), firstAndLast[1].Length - firstAndLast[1].IndexOf(' '));
                                                //}

                                                var physicianData = new AgencyPhysician()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    FirstName = dataRow.GetValue((int) DataFields.FirstName),
                                                    LastName = dataRow.GetValue((int) DataFields.LastName),
                                                    AddressLine1 = dataRow.GetValue((int)DataFields.Address),
                                                    AddressCity = dataRow.GetValue((int)DataFields.City),
                                                    AddressStateCode = dataRow.GetValue((int)DataFields.State),
                                                    AddressZipCode = dataRow.GetValue((int)DataFields.Zip),
                                                    PhoneWork = CleanPhoneNumberString(dataRow.GetValue((int)DataFields.Phone)),
                                                    FaxNumber = CleanPhoneNumberString(dataRow.GetValue((int)DataFields.Fax)),
                                                    NPI = dataRow.GetValue((int)DataFields.NPI),
                                                    IsPecosVerified = false,
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    PhysicianAccess = false,
                                                    Credentials = dataRow.GetValue((int) DataFields.Credentials)
                                                };
                                                
                                                
                                                var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                                string strDebug = string.Empty;

                                                if (physician == null)
                                                {
                                                    bool success = Database.Add(physicianData);
                                                    if (success)
                                                    {
                                                        strDebug = string.Format(
                                                            "FName: {0}\tLName {1}\t MName: {2}\n" +
                                                            "Addr1: {3}\tAddr2 {4}\n" +
                                                            "City: {5} State: {6} Zip: {7}\n" +
                                                            "Phone: {8}\n",
                                                            physicianData.FirstName,
                                                            physicianData.LastName,
                                                            physicianData.MiddleName,
                                                            physicianData.AddressLine1,
                                                            physicianData.AddressLine2,
                                                            physicianData.AddressCity,
                                                            physicianData.AddressStateCode,
                                                            physicianData.AddressZipCode,
                                                            physicianData.PhoneWork);

                                                        Console.WriteLine(strDebug);
                                                        textWriter.WriteLine(strDebug);
                                                    }
                                                }
                                                else
                                                {
                                                    strDebug = string.Format("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                                    Console.WriteLine(strDebug);
                                                    textWriter.WriteLine(strDebug);
                                                }
                                                i++;
                                            }
                                        }
                                    }                                    
                                    Console.WriteLine("{0} records added.", i);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.Message, ex.StackTrace);
                    System.Diagnostics.Trace.WriteLine(ex.Message, ex.StackTrace);
                }
                finally
                {
                    textWriter.Flush();
                    textWriter.Close();
                }
            }
        }
        public static string CleanPhoneNumberString(string number)
        {
            string cleanNumber = string.Empty;

            foreach (char c in number)
            {
                if (Char.IsDigit(c))
                {
                    cleanNumber += c;
                }
            }

            return cleanNumber;
        }
    }
}
