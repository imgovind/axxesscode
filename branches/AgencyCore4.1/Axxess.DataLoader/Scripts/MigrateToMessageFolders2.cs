﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Threading;

    public static class MigrateToMessageFolders2
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\Result_{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                int available;
                int maxLimit;
                DeleteFailedMessage();
                using (ManualResetEvent resetEvent = new ManualResetEvent(false))
                {
                    var start = DateTime.Now;
                    List<TempAgencyUserStatus> unProcessedUsers = Database.GetTempUsers(0);
                    if (unProcessedUsers.IsNotNullOrEmpty())
                    {
                        ThreadPool.SetMaxThreads(10, 5);
                        int total = unProcessedUsers.Count;
                        unProcessedUsers.ForEach(u =>
                            {
                                ThreadPool.QueueUserWorkItem(delegate
                                {
                                    var current = Database.GetTempAgencyUserStatus(u.UserId);
                                    if (current.Status == 0)
                                    {
                                        current.Status = 1;//set the status to be processing
                                        if (Database.Update<TempAgencyUserStatus>(current))
                                        {
                                            Console.WriteLine();
                                            Console.WriteLine(string.Format("processing message for userId:{0}", current.UserId));
                                            ThreadPool.GetAvailableThreads(out available, out maxLimit);
                                            Console.WriteLine((maxLimit - available).ToString() + " threads are running");
                                            try
                                            {
                                                bool migrationSuccessful = MigrateUserMessages(current.AgencyId, current.UserId);
                                                if (migrationSuccessful)
                                                {
                                                    current.Status = 2;//set the status to be finished
                                                    //Console.WriteLine(("Agency:"+current.AgencyId+" User:"+current.UserId+" Date:"+DateTime)ToString());
                                                }
                                                else
                                                {
                                                    current.Status = 3;//set the status to be failure
                                                    //Console.WriteLine(("Agency:"+current.AgencyId+" User:"+current.UserId+" Date:"+DateTime+" Date:")ToString());
                                                }
                                                Database.Update<TempAgencyUserStatus>(current);
                                            }
                                            catch (Exception e)
                                            {
                                                textWriter.Write(e.ToString());
                                            }
                                        }
                                    }
                                    if (Interlocked.Decrement(ref total) == 0)
                                        resetEvent.Set();
                                });
                            });

                        resetEvent.WaitOne();
                        var timeSpan = (DateTime.Now - start).TotalMinutes;
                        Console.WriteLine();
                        Console.WriteLine(string.Format("Total Time spent:{0} mins ", timeSpan.ToString()));
                    }
                    else
                    {
                        Console.WriteLine("All users are in successful status in tempagencyuserstatuses table.");
                    }
                }

            }
            Console.WriteLine("-----------------------------------------DONE--------------------------------------------");
        }


        public static void LoadSystemMessages(User user)
        {
            var userSystemMessages = new List<UserMessage>();
            
                    List<MessageState> messageState = user.Messages.ToObject<List<MessageState>>();

                    if (messageState != null)
                    {
                        messageState.ForEach(ms =>
                            {
                                UserMessage um = new UserMessage();
                                um.Id = Guid.NewGuid();
                                um.MessageId = ms.Id;
                                um.UserId = user.Id;
                                um.MessageType = (int)MessageType.System;
                                um.IsRead = ms.IsRead;
                                um.IsDeprecated = ms.IsDeprecated;
                                um.FolderId = Guid.Empty;
                                um.ThreadId = Guid.Empty;
                                userSystemMessages.Add(um);
                            }
                        );
                    }
                   
            Database.AddMany<UserMessage>(userSystemMessages);
        }


        public static bool MigrateUserMessages(Guid agencyId, Guid userId)
        {
            bool success = true;

            //Console.WriteLine("Starting agency: {0}", agency.Name);

            Message lastMessage = new Message();
            lastMessage.FromId = Guid.Empty;
            Guid linkGuid = Guid.Empty;


            List<Message> userMessages = Database.GetMessagesByAgencyAndUser(agencyId, userId);
            List<Database.tempProcessedMessage> processedMessages = Database.GetProcessedMessageIdsForUser(userId);

            if (userMessages.IsNotNullOrEmpty())
            {
                userMessages.ForEach(message =>
                    {
                        bool makeNew = false;

                        if (lastMessage.FromId.CompareTo(message.FromId) == 0) // continue check
                        {
                            if (lastMessage.Subject.CompareTo(message.Subject) == 0) // continue check
                            {
                                if (lastMessage.RecipientNames.CompareTo(message.RecipientNames) == 0) // continue check
                                {
                                    if (lastMessage.Body.CompareTo(message.Body) == 0)
                                    {
                                        if (lastMessage.RecipientId.CompareTo(message.RecipientId) != 0) // recipient only changed
                                        {
                                            // add entry to UserMessages
                                            makeNew = false;
                                        }
                                    }
                                    else // body changed. new message and usermessage
                                    {
                                        makeNew = true;
                                    }
                                }
                                else // subject changed, new message and usermessage
                                {
                                    makeNew = true;
                                }
                            }
                            else // recipient names changed. new message and usermessage
                            {
                                makeNew = true;
                            }
                        }
                        else // new sender. new message and usermessage
                        {
                            makeNew = true;
                        }

                        // if the message has been processed, in a previous run,
                        // then do not add it again.
                        bool skipMessage = false;
                        foreach( Database.tempProcessedMessage tpm in processedMessages )
                        {
                            if (tpm.ID.ToString().CompareTo( message.Id.ToString()) == 0 )
                            {
                                skipMessage = true;
                                break;
                            }
                        }

                        if (!skipMessage)
                        {
                            linkGuid = (makeNew ? Guid.NewGuid() : linkGuid);
                            if (Database.MigrateUserMessage(message, linkGuid, makeNew))
                            {
                                lastMessage = (makeNew ? message : lastMessage);
                            }
                            else
                            {
                                success = false;
                            }
                        }
                    }
                );
            }
               
            return success;
        }

        public static void DeleteFailedMessage()
        {
            List<TempAgencyUserStatus> failedUsers = Database.GetTempUsers(1);
            if (failedUsers.IsNotNullOrEmpty())
            {
                bool result = false;
                failedUsers.ForEach(f =>
                    {
                        var userMessage = Database.GetUserMessageByUserId(f.UserId);
                        if (userMessage.IsNotNullOrEmpty())
                        {
                            Console.WriteLine(string.Format("Deleting message details for user Id:{0}", f.UserId));
                            userMessage.ForEach(u =>
                                {
                                    
                                    try
                                    {
                                        Database.DeleteMessageDetails(u.MessageId);//delete from messagedetails
                                        result = true;
                                        
                                    }
                                    catch (Exception)
                                    {
                                        result = false;
                                    }
                                });
                            if (result)
                            {
                                Database.DeleteMessageByUserId(f.UserId);//delete from usermassages
                                Console.WriteLine(string.Format("Deleting user message for user Id:{0}", f.UserId));
                                f.Status = 0;
                                Database.Update<TempAgencyUserStatus>(f);
                            }
                        }
                        else
                        {
                            f.Status = 0;
                            Database.Update<TempAgencyUserStatus>(f);
                            Console.WriteLine(string.Format("Update status for user:{0}", f.UserId));
                        }
                        
                    });
                
            }
        }
    }
}
