﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class PhysicianCsvScript
    {
        private static string input = Path.Combine(App.Root, "Files\\AllCare.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AllCare_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    var physicianData = new AgencyPhysician();
                                    physicianData.Id = Guid.NewGuid();
                                    physicianData.AgencyId = agencyId;
                                    physicianData.IsDeprecated = false;
                                    physicianData.AddressLine1 = "";
                                    physicianData.AddressCity = "";
                                    physicianData.AddressStateCode = "";
                                    physicianData.AddressZipCode = "";

                                    physicianData.LastName = dataRow.GetValue(2);
                                    physicianData.FirstName = dataRow.GetValue(3);
                                    if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                    {
                                        var addressRow = dataRow.GetValue(4).Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                        if (addressRow.Length > 2)
                                        {
                                            physicianData.AddressLine1 = addressRow[0];
                                            physicianData.AddressLine2 = addressRow[1];
                                            physicianData.AddressCity = addressRow[2];
                                        }
                                        else if (addressRow.Length > 1)
                                        {
                                            physicianData.AddressLine1 = addressRow[0];
                                            physicianData.AddressCity = addressRow[1];
                                        }
                                        else
                                        {
                                            physicianData.AddressLine1 = addressRow[0];
                                        } 
                                    }
                                    if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                    {
                                        var stateZip = dataRow.GetValue(5).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                        if (stateZip.Length == 2)
                                        {
                                            physicianData.AddressStateCode = stateZip[0];
                                            physicianData.AddressZipCode = stateZip[1];
                                        }
                                        else if (stateZip[0].Length < 5)
                                        {
                                            physicianData.AddressStateCode = stateZip[0];
                                        }
                                        else if (stateZip[0].Length == 5)
                                        {
                                            physicianData.AddressZipCode = stateZip[0];
                                        } 
                                    }
                                    if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                    {
                                        physicianData.PhoneWork = dataRow.GetValue(6).ToPhoneDB(); 
                                    }
                                    if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                    {
                                        physicianData.FaxNumber = dataRow.GetValue(7).ToPhoneDB(); 
                                    }
                                    if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                    {
                                        physicianData.UPIN = dataRow.GetValue(10);
                                    }
                                    if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                    {
                                        physicianData.NPI = dataRow.GetValue(12);
                                    }
                                    if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                    {
                                        physicianData.Comments += string.Format("Registration: {0}. ", dataRow.GetValue(9)); 
                                    }

                                    if (physicianData.NPI.IsNotNullOrEmpty())
                                    {
                                        var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                        if (physician == null)
                                        {
                                            if (Database.Add(physicianData))
                                            {
                                                Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("{0}) {1} ALREADY EXISTS",i,physicianData.DisplayName);
                                        }
                                    }

                                    i++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
