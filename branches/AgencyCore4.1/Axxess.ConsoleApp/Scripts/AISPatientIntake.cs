﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class AISPatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\tristan.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\tristan_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "964690fb-ddd0-47db-b263-72bd52df47c0";
                                patientData.AgencyLocationId = "62d1e255-3956-4cbc-9e11-0b410ba63825";
                                patientData.FirstName = record.GetValue(0).ToTitleCase();
                                patientData.LastName = record.GetValue(1).ToTitleCase();
                                patientData.MiddleInitial = record.GetValue(2);
                                patientData.Gender = record.GetValue(3) == "M" ? "Male" : "Female";
                                patientData.MedicareNumber = record.GetValue(4);
                                patientData.PatientNumber = record.GetValue(5);
                                patientData.StartofCareDate = record.GetValue(6).ToMySqlDate(0);
                                if (record.GetValue(7).IsNotNullOrEmpty())
                                {
                                    patientData.DischargeDate = record.GetValue(7).ToMySqlDate(0);
                                    patientData.PatientStatusId = "2";
                                }
                                else
                                {
                                    patientData.PatientStatusId = "1";
                                }
                                patientData.BirthDate = record.GetValue(8).ToMySqlDate(0);
                                patientData.AddressLine1 = record.GetValue(9).ToTitleCase();
                                patientData.AddressLine2 = record.GetValue(10).ToTitleCase();
                                patientData.AddressCity = record.GetValue(11).ToTitleCase();
                                patientData.AddressState = record.GetValue(12);
                                patientData.AddressZipCode = record.GetValue(13).Substring(0, 5);
                                patientData.Comments = "Physician UPIN: " + record.GetValue(18) + ". HIPPS: " + record.GetValue(19) + ". ";
                                patientData.Comments += "Matching Key: " + record.GetValue(20) + ". ";
                                patientData.Comments += string.Format("Diagnosis Code 1: {0}. ", record.GetValue(21));
                                patientData.Comments += string.Format("Diagnosis Code 2: {0}. ", record.GetValue(22));
                                patientData.Comments += string.Format("Diagnosis Code 3: {0}. ", record.GetValue(23));
                                patientData.Comments += string.Format("Diagnosis Code 4: {0}. ", record.GetValue(24));
                                patientData.Comments += string.Format("Diagnosis Code 5: {0}. ", record.GetValue(25));
                                patientData.Comments += string.Format("Diagnosis Code 6: {0}.", record.GetValue(26));


                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
