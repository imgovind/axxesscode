﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;

using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;

using Axxess.LookUp.Domain;
using Axxess.LookUp.Repositories;

using Excel;
using Kent.Boogaart.KBCsv;

namespace Axxess.ConsoleApp.Tests
{
    public static class AddPatientPhysicians
    {
        private static Guid AgencyId = new Guid("b19d9289-8c18-467a-abfa-4bb6763324c5");
        private static readonly IAgencyManagementDataProvider agencyDataProvider = new AgencyManagementDataProvider();

        private static string input = "Ropheka_Physicians.xls";
        private static string output = string.Format("Emcare_Physicians_{0}.txt", DateTime.Now.Ticks.ToString());

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    foreach (DataRow record in dataTable.Rows)
                                    {
                                        var agencyPhysicians = agencyDataProvider.PhysicianRepository.GetAgencyPhysicians(AgencyId);
                                        Console.WriteLine("Agency Physician Count: {0}", agencyPhysicians != null ? agencyPhysicians.Count.ToString() : "0");
                                        textWriter.WriteLine("Agency Physician Count: {0}", agencyPhysicians != null ? agencyPhysicians.Count.ToString() : "0");

                                        var physicianNPI = record.GetValue(4);
                                        var patientId = record.GetValue(0).ToGuid();
                                        var lastName = record.GetValue(2).ToTitleCase();
                                        var firstName = record.GetValue(3).ToTitleCase();

                                        if (!patientId.IsEmpty())
                                        {
                                            var patientPhysicians = agencyDataProvider.PhysicianRepository.GetPatientPhysicians(patientId, AgencyId);
                                            if (patientPhysicians != null)
                                            {
                                                if (patientPhysicians.Count == 0)
                                                {
                                                    var physician = agencyPhysicians.Where(p => p.NPI.IsEqual(physicianNPI)).FirstOrDefault();
                                                    if (physician != null)
                                                    {
                                                        if (agencyDataProvider.PhysicianRepository.Link(patientId, physician.Id, true))
                                                        {
                                                            Console.WriteLine("Patient: {0} {1} linked to Physician: {2} {3}", firstName, lastName, physician.FirstName, physician.LastName);
                                                            textWriter.WriteLine("Patient: {0} {1} linked to Physician: {2} {3}", firstName, lastName, physician.FirstName, physician.LastName);
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Could not link Patient: {0} {1} to Physician: {2} {3}", firstName, lastName, physician.FirstName, physician.LastName);
                                                            textWriter.WriteLine("Could not link Patient: {0} {1} to Physician: {2} {3}", firstName, lastName, physician.FirstName, physician.LastName);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var agencyPhysician = new AgencyPhysician
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AddressCity = record.GetValue(10),
                                                            AddressLine1 = record.GetValue(9),
                                                            AddressLine2 = "",
                                                            AddressStateCode = record.GetValue(11),
                                                            AddressZipCode = record.GetValue(12),
                                                            AgencyId = AgencyId,
                                                            Credentials = record.GetValue(5),
                                                            EmailAddress = "",
                                                            FaxNumberArray = GetPhoneArray(record.GetValue(14).ToPhoneDB()),
                                                            FirstName = record.GetValue(7),
                                                            Gender = "",
                                                            IsDeprecated = false,
                                                            LastName = record.GetValue(6),
                                                            LicenseNumber = "",
                                                            LicenseStateCode = "",
                                                            MiddleName = "",
                                                            NPI = physicianNPI,
                                                            PhoneWorkArray = GetPhoneArray(record.GetValue(13).ToPhoneDB()),
                                                            Primary = true,
                                                            UPIN = ""
                                                        };

                                                        if (agencyDataProvider.PhysicianRepository.Add(agencyPhysician))
                                                        {
                                                            Console.WriteLine("Physician {0} {1} added to DB", agencyPhysician.FirstName, agencyPhysician.LastName);
                                                            textWriter.WriteLine("Physician {0} {1} added to DB", agencyPhysician.FirstName, agencyPhysician.LastName);

                                                            if (agencyDataProvider.PhysicianRepository.Link(patientId, agencyPhysician.Id, true))
                                                            {
                                                                Console.WriteLine("Patient: {0} {1} linked to Physician: {2} {3}", firstName, lastName, agencyPhysician.FirstName, agencyPhysician.LastName);
                                                                textWriter.WriteLine("Patient: {0} {1} linked to Physician: {2} {3}", firstName, lastName, agencyPhysician.FirstName, agencyPhysician.LastName);
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Could not link Patient: {0} {1} to new Physician: {2} {3}", firstName, lastName, agencyPhysician.FirstName, agencyPhysician.LastName);
                                                                textWriter.WriteLine("Could not link Patient: {0} {1} to new Physician: {2} {3}", firstName, lastName, agencyPhysician.FirstName, agencyPhysician.LastName);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Patient {0} {1} has physician(s)", firstName, lastName);
                                                            textWriter.WriteLine("Patient {0} {1} has physician(s)", firstName, lastName);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Patient {0} {1} has physician(s)", firstName, lastName);
                                                    textWriter.WriteLine("Patient {0} {1} has physician(s)", firstName, lastName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Patient: {0} {1} ", firstName, lastName);
                                                textWriter.WriteLine("Patient: {0} {1}", firstName, lastName);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("patientPhysicians == null");
                                            textWriter.Write("patientPhysicians == null");
                                        }
                                        Console.WriteLine();
                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }

        private static List<string> GetPhoneArray(string phone)
        {
            var phoneArray = new List<string>();
            if (phone.IsNotNullOrEmpty() && phone.Length == 10)
            {
                phoneArray.Add(phone.Substring(0, 3));
                phoneArray.Add(phone.Substring(3, 3));
                phoneArray.Add(phone.Substring(6, 4));
            }
            return phoneArray;
        }
    }
}
