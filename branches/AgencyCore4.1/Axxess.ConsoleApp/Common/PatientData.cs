﻿namespace Axxess.ConsoleApp
{
    using System;

    public class PatientData
    {
        #region Constructor

        public PatientData()
        {
            Gender = "";
            Ethnicity = "";
            BirthDate = "0001-01-01";
            StartofCareDate = "0001-01-01";
            Created = "CURDATE()";
            Modified = "CURDATE()";
            PatientStatusId = "1";
            ClaimStatusId = "300";
            MaritalStatus = "";
            Medication = "<ArrayOfMedication />";
            EpisodeDetails = "<EpisodeDetail />";
            EpisodeSchedule = "<ArrayOfScheduleEvent />";
            Diagnosis = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

            SSN = string.Empty;
            Phone = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            MiddleInitial = string.Empty;
            MedicareNumber = string.Empty;
            MedicaidNumber = string.Empty;
            PaymentSource = string.Empty;
            AddressCity = string.Empty;
            AddressLine1 = string.Empty;
            AddressLine2 = string.Empty;
            AddressState = string.Empty;
            AddressZipCode = string.Empty;
            AddressCounty = string.Empty;

            Comments = string.Empty;
            PatientId = Guid.NewGuid();
            EpisodeId = Guid.NewGuid();
            MedProfileId = Guid.NewGuid();
            AllergyProfileId = Guid.NewGuid();
            Allergies = "<ArrayOfAllergy />";
            FirstBillableDate = DateTime.MinValue.ToString().ToMySqlDate();
            DischargeDate = DateTime.MinValue.ToString().ToMySqlDate();
        }

        #endregion

        #region Members

        public string AgencyId { get; set; }
        public string AgencyLocationId { get; set; }
        public Guid PatientId { get; set; }
        public string PatientNumber { get; set; }
        public string PatientStatusId { get; set; }
        public string Gender { get; set; }
        public string Ethnicity { get; set; }
        public string MaritalStatus { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string BirthDate { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressCounty { get; set; }
        public string StartofCareDate { get; set; }
        public string Phone { get; set; }
        public string SSN { get; set; }
        public string DischargeDate { get; set; }
        public string Comments { get; set; }
        public string PaymentSource { get; set; }
        public string PrimaryInsurance { get; set; }
        public bool IsHosptialized { get; set; }

        public Guid MedProfileId { get; set; }
        public string Medication { get; set; }
        
        public Guid AllergyProfileId { get; set; }
        public string Allergies { get; set; }
        
        public Guid EpisodeId { get; set; }
        public string EpisodeStart { get; set; }
        public string EpisodeEnd { get; set; }
        public string FirstBillableDate { get; set; }
        public string EpisodeDetails { get; set; }
        public string EpisodeSchedule { get; set; }

        public string Diagnosis { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public string ClaimStatusId { get; set; }

        public string Created { get; set; }
        public string Modified { get; set; }

        #endregion
    }
}
