﻿namespace Axxess.ConsoleApp
{
    public class PhysicianScript
    {
        private NpiData npiData;
        private const string NPI_INSERT = "INSERT INTO `npis_copy`(`Id`,`EntityTypeCode`,`ProviderOrganizationName`,`ProviderFirstName`,`ProviderMiddleName`,`ProviderLastName`," +
            "`ProviderCredentialText`,`ProviderFirstLineBusinessMailingAddress`,`ProviderSecondLineBusinessMailingAddress`,`ProviderBusinessMailingAddressCityName`," +
            "`ProviderBusinessMailingAddressStateName`,`ProviderBusinessMailingAddressPostalCode`,`ProviderBusinessMailingAddressCountryCode`," +
            "`ProviderBusinessMailingAddressTelephoneNumber`,`ProviderBusinessMailingAddressFaxNumber`,`ProviderFirstLineBusinessPracticeLocationAddress`," +
            "`ProviderSecondLineBusinessPracticeLocationAddress`,`ProviderBusinessPracticeLocationAddressCityName`," +
            "`ProviderBusinessPracticeLocationAddressStateName`,`ProviderBusinessPracticeLocationAddressPostalCode`,`ProviderBusinessPracticeLocationAddressCountryCode`," +
            "`ProviderBusinessPracticeLocationAddressTelephoneNumber`,`ProviderBusinessPracticeLocationAddressFaxNumber`) " +
            "VALUES ('{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}');";

        public PhysicianScript(NpiData npiData)
        {
            this.npiData = npiData;
        }

        public override string ToString()
        {
            return string.Format(NPI_INSERT, npiData.Id, npiData.EntityTypeCode, npiData.ProviderOrganizationName, npiData.ProviderFirstName,
                npiData.ProviderMiddleName, npiData.ProviderLastName, npiData.ProviderCredentialText, npiData.ProviderFirstLineBusinessMailingAddress,
                npiData.ProviderSecondLineBusinessMailingAddress, npiData.ProviderBusinessMailingAddressCityName, npiData.ProviderBusinessMailingAddressStateName,
                npiData.ProviderBusinessMailingAddressPostalCode, npiData.ProviderBusinessMailingAddressCountryCode, npiData.ProviderBusinessMailingAddressTelephoneNumber,
                npiData.ProviderBusinessMailingAddressFaxNumber, npiData.ProviderFirstLineBusinessPracticeLocationAddress, npiData.ProviderSecondLineBusinessPracticeLocationAddress,
                npiData.ProviderBusinessPracticeLocationAddressCityName, npiData.ProviderBusinessPracticeLocationAddressStateName, npiData.ProviderBusinessPracticeLocationAddressPostalCode,
                npiData.ProviderBusinessPracticeLocationAddressCountryCode, npiData.ProviderBusinessPracticeLocationAddressTelephoneNumber, npiData.ProviderBusinessPracticeLocationAddressFaxNumber);
        }
    }
}
