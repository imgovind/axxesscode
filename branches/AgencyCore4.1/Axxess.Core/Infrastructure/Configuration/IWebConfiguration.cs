﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Specialized;

    public interface IWebConfiguration
    {
        NameValueCollection AppSettings
        {
            get;
        }

        string ConnectionStrings(string name);

        T GetSection<T>(string sectionName);
    }
}
