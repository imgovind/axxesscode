﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Data;
    using System.Data.Common;

    using MySql.Data.Types;
    using MySql.Data.MySqlClient;

    public partial class FluentCommand<T>
    {
        public DbParameter AddParameter(string name, MySqlDbType dataType, object value)
        {
            var param = new MySqlParameter(name, dataType);
            param.Value = SetParameterValue(value);
            command.Parameters.Add(param);
            return param;
        }

        public DbParameter AddParameter(string name, MySqlDbType dataType)
        {
            var param = new MySqlParameter(name, dataType);
            command.Parameters.Add(param);
            return param;
        }

        public FluentCommand<T> Add<TV>(string name, TV value)
        {
            var param = new MySqlParameter(name, value);
            command.Parameters.Add(param);
            return this;
        }

        public FluentCommand<T> AddInt(string name, int value)
        {
            AddParameter(name, MySqlDbType.Int32, value);
            return this;
        }

        public FluentCommand<T> AddInt(string name, int? value)
        {
            AddParameter(name, MySqlDbType.Int32, value);
            return this;
        }

        public FluentCommand<T> AddDecimal(string name, decimal value)
        {
            AddParameter(name, MySqlDbType.Decimal, value);
            return this;
        }

        public FluentCommand<T> AddDecimal(string name, decimal? value)
        {
            AddParameter(name, MySqlDbType.Decimal, value);
            return this;
        }

        public FluentCommand<T> AddDateTime(string name, DateTime value)
        {
            AddParameter(name, MySqlDbType.DateTime, value);
            return this;
        }

        public FluentCommand<T> AddDateTime(string name, DateTime? value)
        {
            AddParameter(name, MySqlDbType.DateTime, value);
            return this;
        }

        public FluentCommand<T> AddString(string name, int size, string value)
        {
            AddParameter(name, MySqlDbType.VarChar, size);
            return this;
        }

        public FluentCommand<T> AddString(string name, string value)
        {
            AddParameter(name, MySqlDbType.VarChar, value);
            return this;
        }

        public FluentCommand<T> AddGuid(string name, Guid value)
        {
            var param = AddParameter(name, MySqlDbType.VarChar);
            param.Value = SetGuidParamValue(value);
            return this;
        }

        public FluentCommand<T> AddBoolean(string name, bool value)
        {
            AddParameter(name, MySqlDbType.Int16, value);
            return this;
        }

        public FluentCommand<T> SetDictonaryId(string Id)
        {
            this.dictionaryId = Id;
            return this;
        }
    }

}
