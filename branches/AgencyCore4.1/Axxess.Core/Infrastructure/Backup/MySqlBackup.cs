﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Collections.Generic;

    using Extension;

    using MySql.Data.MySqlClient;

    public class MySqlBackup : IDatabaseOperation
    {
        #region Constructor and Private Members

        private string fileName;
        private string filePath;
        private string databaseName;
        private bool enableEncryption;
        private string connectionString;
        private DateTime startTime;
        private Dictionary<string, string> tableStatements;

        private static StreamWriter textWriter = null;

        public const string Version = "2.0";

        public MySqlBackup(string databaseName, string connectionString, bool enableEncryption, string filePath)
        {
            this.databaseName = databaseName;
            this.enableEncryption = enableEncryption;
            this.connectionString = connectionString;

            this.startTime = DateTime.Now;
            this.filePath = filePath;
            this.fileName = Path.Combine(filePath, string.Format("{0}-{1}.sql", this.databaseName, startTime.ToString("yyyy-MM-dd-HHmmss")));
        }

        #endregion

        #region IDatabaseOperation Members

        public void Execute()
        {
            if (this.tableStatements == null || this.tableStatements.Count == 0)
            {
                List<StringVariable> tables = null;
                using (var fluentCommand = new FluentCommand<StringVariable>(MySqlAdministration.SQL_GET_TABLE_NAMES))
                { 
                    tables = fluentCommand
                        .UseConnectionString(connectionString, int.MaxValue)
                        .SetMap(reader => new StringVariable
                        {
                            Value = reader.GetString(string.Format("Tables_in_{0}", this.databaseName.ToLower()))
                        }).AsList();
                }

                this.tableStatements = new Dictionary<string, string>();
                tables.ForEach(v =>
                {
                    if (!v.Value.ToLower().Contains("_copy")
                        && !v.Value.ToLower().Contains("_backup")
                        && !v.Value.ToLower().Contains("_bk")
                        && !v.Value.ToLower().Contains("_tmp")
                        && !v.Value.ToLower().Contains("_new")
                        && !v.Value.ToLower().Contains("npi")
                        && !v.Value.ToLower().Contains("assets")
                        && !v.Value.ToLower().Contains("dave")
                        && !v.Value.ToLower().Contains("error")
                        && !v.Value.ToLower().Contains("taskaudits")
                        && !v.Value.ToLower().Contains("imperson")
                        && !v.Value.ToLower().Contains("scheduleevents")
                        && !v.Value.ToLower().Contains("claimresponse")
                        && !v.Value.ToLower().Contains("wdup")
                        && !v.Value.ToLower().Contains("remit")
                        && !v.Value.ToLower().Contains("withdup")
                        && !v.Value.ToLower().Contains("test"))
                    {
                        this.tableStatements.Add(v.Value, string.Format("SELECT * FROM `{0}`", v.Value));
                    }
                });
            }

            using (textWriter = new StreamWriter(this.fileName, false, Encoding.UTF8))
            {
                textWriter.WriteLine(WriteLine("-- MySqlBackup dump " + MySqlBackup.Version));
                textWriter.WriteLine(WriteLine("-- Dump time: " + startTime.ToString("yyyy-MM-dd HH:mm:ss")));
                textWriter.WriteLine(WriteLine("-- ------------------------------------------------------"));
                textWriter.WriteLine(WriteLine("-- Server version	" + this.GetServerVersion()));
                textWriter.WriteLine(WriteLine("-- ------------------------------------------------------"));
                textWriter.WriteLine(WriteLine("--"));
                textWriter.WriteLine(WriteLine(string.Format("-- Create schema {0}", this.databaseName)));
                textWriter.WriteLine(WriteLine("--"));
                textWriter.WriteLine(WriteLine(""));
                textWriter.WriteLine(WriteLine(string.Format("CREATE DATABASE IF NOT EXISTS {0};", this.databaseName)));
                textWriter.WriteLine(WriteLine(string.Format("USE {0};", this.databaseName)));
            }

            foreach (KeyValuePair<string, string> keyValuePair in this.tableStatements)
            {
                string tableName = keyValuePair.Key;
                this.fileName = Path.Combine(this.filePath, string.Format("{0}-{1}-{2}.sql", this.databaseName, tableName, startTime.ToString("yyyy-MM-dd-HHmmss")));
                using (textWriter = new StreamWriter(this.fileName, true, Encoding.UTF8))
                {
                    textWriter.WriteLine(WriteLine(""));
                    textWriter.WriteLine(WriteLine(""));
                    textWriter.WriteLine(WriteLine(""));
                    textWriter.WriteLine(WriteLine("--"));
                    textWriter.WriteLine(WriteLine(string.Format("-- Definition of table `{0}`", tableName)));
                    textWriter.WriteLine(WriteLine("--"));
                    textWriter.WriteLine(WriteLine(""));
                    textWriter.WriteLine(WriteLine(string.Format("DROP TABLE IF EXISTS `{0}`;", tableName)));
                    textWriter.WriteLine(WriteLine(this.GetCreateTableSyntax(tableName)));
                    textWriter.WriteLine(WriteLine(""));
                    textWriter.WriteLine(WriteLine("--"));
                    textWriter.WriteLine(WriteLine(string.Format("-- Dumping data for table `{0}`", tableName)));
                    textWriter.WriteLine(WriteLine("--"));
                    textWriter.WriteLine(WriteLine(""));
                }

                var tableCount = this.GetTableRowCount(tableName);
                var columnNames = this.GetTableColumnNames(tableName);
                string insertStatementHeader = this.GetTableInsertSyntax(tableName, columnNames);

                if (this.DoesTableFitConditionForSplitting(tableCount, columnNames))
                {
                    var offset = 0;
                    var counter = 1;
                    var chunk = tableCount;

                    do
                    {
                        var sqlQuery = string.Format("{0} LIMIT {1}, {2}", this.tableStatements[tableName], offset, MySqlAdministration.MIN_ROW_COUNT_FOR_SPLITTING);
                        this.fileName = Path.Combine(this.filePath, string.Format("{0}-{1}-{2}-{3}.sql", this.databaseName, tableName, startTime.ToString("yyyy-MM-dd-HHmmss"), counter));
                        Console.WriteLine("Writing to Table {0} getting {1} rows", tableName, offset);
                        
                        using (textWriter = new StreamWriter(this.fileName, true, Encoding.UTF8))
                        {
                            textWriter.WriteLine(WriteLine("SET foreign_key_checks = 0;"));
                            RetrieveData(tableName, sqlQuery, insertStatementHeader);
                        }
                        offset += MySqlAdministration.MIN_ROW_COUNT_FOR_SPLITTING;
                        chunk -= MySqlAdministration.MIN_ROW_COUNT_FOR_SPLITTING;
                        counter++;
                    }
                    while (chunk >= 0);
                }
                else
                {
                    Console.WriteLine("Writing to Table {0}", tableName);
                    using (textWriter = new StreamWriter(this.fileName, true, Encoding.UTF8))
                    {
                        textWriter.WriteLine(WriteLine("SET foreign_key_checks = 0;"));
                        RetrieveData(tableName, this.tableStatements[tableName], insertStatementHeader);
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private string WriteLine(string input)
        {
            var result = input;
            if (input.IsNotNullOrEmpty())
            {
                if (this.enableEncryption)
                {
                    result = Crypto.Encrypt(input);
                }
            }
            return result;
        }

        private string GetTableInsertSyntax(string tableName, Dictionary<string, string> columnNames)
        {
            var stringBuilder = new StringBuilder().AppendFormat("INSERT INTO `{0}` (", tableName);
            foreach (KeyValuePair<string, string> keyValuePair in columnNames)
            {
                stringBuilder.AppendFormat("`{0}`,", keyValuePair.Key);
            }

            return stringBuilder
                .Remove(stringBuilder.Length - 1, 1)
                .Append(") VALUES")
                .ToString();
        }

        private string GetServerVersion()
        {
            string serverVersion = string.Empty;
            using (var fluentCommand = new FluentCommand<StringVariable>(MySqlAdministration.SQL_GET_MYSQL_VERSION))
            {
                var variable = fluentCommand
                    .UseConnectionString(connectionString, int.MaxValue)
                    .SetMap(reader => new StringVariable
                    {
                        Value = reader.GetString("Value")
                    }).AsSingle();
                if (variable != null)
                {
                    serverVersion = variable.Value;
                }
            }
            return serverVersion;
        }

        private string GetCreateTableSyntax(string tableName)
        {
            string tableSyntax = string.Empty;
            string sql = string.Format(MySqlAdministration.SQL_GET_TABLE_CREATE_SYNTAX, tableName);
            using (var fluentCommand = new FluentCommand<StringVariable>(sql))
            {
                var variable = fluentCommand
                    .UseConnectionString(connectionString, int.MaxValue)
                    .SetMap(reader => new StringVariable
                    {
                        Value = reader.GetString("Create Table")
                    }).AsSingle();
                if (variable != null && variable.Value.IsNotNullOrEmpty())
                {
                    tableSyntax = variable.Value.Replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS") + ";";
                }
            }
            return tableSyntax;
        }

        private int GetTableRowCount(string tableName)
        {
            int rowCount = 0;
            string sql = string.Format(MySqlAdministration.SQL_GET_TABLE_ROW_COUNT, tableName);
            using (var fluentCommand = new FluentCommand<int>(sql))
            {
                rowCount = fluentCommand
                    .UseConnectionString(connectionString, int.MaxValue)
                    .AsScalar();
            }
            return rowCount;
        }

        private Dictionary<string, string> GetTableColumnNames(string tableName)
        {
            var columnNames = new Dictionary<string, string>();
            string sql = string.Format(MySqlAdministration.SQL_GET_TABLE_COLUMN_NAMES, tableName);
            using (var fluentCommand = new FluentCommand<StringVariable>(sql))
            {
                var columnVariables = fluentCommand
                    .UseConnectionString(connectionString, int.MaxValue)
                    .SetMap(reader => new StringVariable
                    {
                        Name = reader.GetString("Field"),
                        Value = reader.GetString("Type")
                    }).AsList();

                if (columnVariables != null)
                {
                    columnVariables.ForEach(v => 
                    {
                        columnNames.Add(v.Name, v.Value);
                    });
                }
            }
            return columnNames;
        }

        private bool DoesTableFitConditionForSplitting(int tableCount, Dictionary<string, string> columnNames)
        {
            var result = false;
            var largeColumnList = new string[] { "text", "blob" };

            if (tableCount >= MySqlAdministration.MIN_ROW_COUNT_FOR_SPLITTING)
            {
                result = true;
            }

            return result;
        }

        private void RetrieveData(string tableName, string sqlStatement, string insertStatementHeader)
        {
            using (var command = new MySqlCommand(sqlStatement + ";"))
            {
                command.Connection = new MySqlConnection(this.connectionString);
                command.Connection.Open();
                command.CommandTimeout = int.MaxValue;

                using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        object[] objectArray = new object[reader.FieldCount];

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            objectArray[i] = reader[i];
                        }

                        string insertString = string.Format("{0}{1};\r\n",
                            insertStatementHeader,
                            GetSqlValueString(objectArray));
                        textWriter.WriteLine(WriteLine(insertString));
                    }
                }
                command.Connection.Close();
            }
        }

        private string GetSqlValueString(object[] obs)
        {
            var stringBuilder = new StringBuilder()
                .Append("(");

            foreach (object ob in obs)
            {
                stringBuilder.Append(GetDataString(ob) + ",");
            }

           return stringBuilder
                .Remove(stringBuilder.Length - 1, 1)
                .Append(")")
                .ToString();
        }

        private string GetDataString(object ob)
        {
            if (ob is System.DBNull)
            {
                return "NULL";
            }
            else if (ob is System.DateTime)
            {
                return "'" + ((DateTime)ob).ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            else if (ob is System.Boolean)
            {
                return Convert.ToInt32(ob) + "";
            }
            else if (ob is System.Byte[])
            {
                return this.GetBLOBSqlDataStringFromBytes(((byte[])ob));
            }
            else if (ob is System.String)
            {
                string data = ob.ToString();

                EscapeStringSequence(ref data);

                return ("'" + data + "'");
            }
            else
            {
                string a = ob.ToString();
                double d = 0;
                if (double.TryParse(a, out d))
                {
                    return a;
                }
                else
                {
                    EscapeStringSequence(ref a);
                    return "'" + a + "'";
                }
            }
        }

        private void EscapeStringSequence(ref string data)
        {
            data = data.Replace("\\", "\\\\"); // Backslash
            data = data.Replace("\r", "\\r");  // Carriage return
            data = data.Replace("\n", "\\n");  // New Line
            data = data.Replace("\a", "\\a");  // Vertical tab
            data = data.Replace("\b", "\\b");  // Backspace
            data = data.Replace("\f", "\\f");  // Formfeed
            data = data.Replace("\t", "\\t");  // Horizontal tab
            data = data.Replace("\v", "\\v");  // Vertical tab
            data = data.Replace("\"", "\\\""); // Double quotation mark
            data = data.Replace("'", "\\'");   // Single quotation mark
        }

        public string GetBLOBSqlDataStringFromBytes(byte[] ba)
        {
            char[] c = new char[ba.Length * 2 + 2];
            byte b;
            c[0] = '0'; c[1] = 'x';
            for (int y = 0, x = 2; y < ba.Length; ++y, ++x)
            {
                b = ((byte)(ba[y] >> 4));
                c[x] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(ba[y] & 0xF));
                c[++x] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }

        #endregion
    }
}
