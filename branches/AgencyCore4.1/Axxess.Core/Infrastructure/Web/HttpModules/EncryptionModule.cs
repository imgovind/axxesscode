﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;

    using Extension;

    public class EncryptionModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += new EventHandler(this.OnBeginRequest);
        }

        #endregion

        #region Private Methods
        
        void OnBeginRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpRequest currentRequest = currentContext.Request;

            if (currentRequest.RawUrl.Contains("?"))
            {
                string query = currentRequest.Url.Query.Replace("?", "");
                string path = currentRequest.Url.AbsolutePath.Substring(1);

                if (!path.ContainsAny(CoreSettings.NoEncryptFiles) &&
                    !path.StartsWithAny(CoreSettings.NoEncryptPaths))
                {
                    if (query.StartsWith("enc=", StringComparison.OrdinalIgnoreCase))
                    {
                        string decryptedQuery = Crypto.Decrypt(query.Replace("enc=", ""));

                        if (!string.IsNullOrEmpty(decryptedQuery))
                        {
                            currentContext.RewritePath(string.Format("/{0}", path), string.Empty, decryptedQuery);
                        }
                    }
                    else if (currentRequest.HttpMethod == "GET")
                    {
                        string encryptedQuery = Crypto.Encrypt(query);

                        if (!string.IsNullOrEmpty(encryptedQuery))
                        {
                            currentContext.Response.Redirect(string.Format("/{0}?enc={1}", path, encryptedQuery));
                        }
                    }
                }
            }
            
        }

        #endregion
    }
}
