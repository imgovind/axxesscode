﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Configuration;

    public class ApplicationModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += new EventHandler(this.BeginRequest);

            httpApp.EndRequest += new EventHandler(this.EndRequest);

        }

        #endregion

        #region Private Methods

        void BeginRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpRequest currentRequest = currentContext.Request;
            if (currentRequest.ContentLength > CoreSettings.MaxFileSizeInBytes)
            {
                throw new ConfigurationErrorsException("FileUploadMoreThanMaxSize");
            }
        }

        void EndRequest(object sender, EventArgs e)
        {
            var currentContext = HttpContext.Current;
            var application = (HttpApplication)sender;
            var lastError = application.Server.GetLastError();
            if (lastError != null && lastError.GetType() == typeof(ConfigurationErrorsException) && lastError.Message == "FileUploadMoreThanMaxSize")
            {
                currentContext.Response.TrySkipIisCustomErrors = false;
                currentContext.Response.StatusCode = 999;
                currentContext.Response.StatusDescription = "Error in file size";
                currentContext.Response.ClearContent();
                currentContext.Response.Write("The File is more than the allowed size.");
            }
        }



        #endregion
    }
}
