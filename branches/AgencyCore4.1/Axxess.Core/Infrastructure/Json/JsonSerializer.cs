﻿namespace Axxess.Core.Infrastructure
{
    using System.IO;
    using System.Text;
    using System.Runtime.Serialization.Json;

    public static class JsonSerializer
    {
        public static string Serialize<T>(T instance)
        {
            if (instance != null)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(instance.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, instance);
                    return Encoding.Default.GetString(ms.ToArray());
                }
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
