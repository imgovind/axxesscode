﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Most Recent Immunizations",true)+
        printview.col(2,
            printview.span("Pneumonia",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPneumonia").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPneumonia").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericPneumonia").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericPneumoniaDate").Clean() %>",0,1))+
            printview.span("Flu",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericFlu").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericFlu").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericFlu").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericFluDate").Clean() %>",0,1)) +
            printview.span("Tetanus",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericTetanus").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericTetanus").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericTetanus").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericTetanusDate").Clean() %>",0,1)) +
            printview.span("TB",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericTB").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericTB").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericTB").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericTBDate").Clean() %>",0,1)) +
            printview.span("TB Exposure",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericTBExposure").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericTBExposure").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericTBExposure").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericTBExposureDate").Clean() %>",0,1)) +
            printview.span("Hepatitis B",true)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericHepatitisB").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericHepatitisB").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericHepatitisB").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericHepatitisBDate").Clean() %>",0,1))) +
        printview.span("Additional Immunizations",true)+
        printview.col(2,
            printview.span("<%= data.AnswerOrEmptyString("GenericAdditionalImmunization1").Clean() %>",0,1) +
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericImmunization1").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericImmunization1").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericImmunization1").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericImmunization1Date").Clean() %>",0,1)) +
            printview.span("<%= data.AnswerOrEmptyString("GenericAdditionalImmunization2").Clean() %>",0,1)+
            printview.col(4,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericImmunization2").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericImmunization2").Equals("No").ToString().ToLower() %>) +
                printview.checkbox("Unknown",<%= data.AnswerOrEmptyString("GenericImmunization2").Equals("Unknown").ToString().ToLower() %>)+
                printview.span("<%= data.AnswerOrEmptyString("GenericImmunization2Date").Clean() %>",0,1))) +
        printview.span("Comments:",true)+
        printview.span("<%= data.AnswerOrEmptyString("GenericImmunizationComments").Clean() %>",0,2),
        "Immunizations");
        
               
<%  if (Model.AssessmentTypeNum % 10 != 4 || Model.AssessmentTypeNum % 10 != 5) { %>
    <%  if (Model.AssessmentTypeNum < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1032) " : string.Empty %>Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization?",true) +
        printview.col(2,
            printview.checkbox("1 &#8211; Recent decline in mental, emotional, or behavioral status",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskRecentDecline").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Multiple hospitalizations (2 or more) in the past 12 months",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskMultipleHosp").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; History of falls (2+ falls or any fall with an injury in the past year)",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskHistoryOfFall").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Taking five or more medications",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskMedications").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Frailty indicators, e.g., weight loss, self-reported exhaustion",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskFrailty").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; Other",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskOther").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1032HospitalizationRiskNone").Equals("1").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1034) " : string.Empty %>Overall Status: Which description best fits the patient&#8217;s overall status?",true) +
        printview.checkbox("0 &#8211; The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient&#8217;s age).",<%= data.AnswerOrEmptyString("M1034OverallStatus").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications.",<%= data.AnswerOrEmptyString("M1034OverallStatus").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death.",<%= data.AnswerOrEmptyString("M1034OverallStatus").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; The patient has serious progressive conditions that could lead to death within a year.",<%= data.AnswerOrEmptyString("M1034OverallStatus").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("UK &#8211; The patient&#8217;s situation is unknown or unclear.",<%= data.AnswerOrEmptyString("M1034OverallStatus").Equals("UK").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1036) " : string.Empty %>Risk Factors, either present or past, likely to affect current health status and/or outcome:",true) +
        printview.col(3,
            printview.checkbox("1 &#8211; Smoking",<%= data.AnswerOrEmptyString("M1036RiskFactorsSmoking").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Obesity",<%= data.AnswerOrEmptyString("M1036RiskFactorsObesity").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Alcohol dependency",<%= data.AnswerOrEmptyString("M1036RiskFactorsAlcoholism").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Drug dependency",<%= data.AnswerOrEmptyString("M1036RiskFactorsDrugs").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1036RiskFactorsNone").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M1036RiskFactorsUnknown").Equals("1").ToString().ToLower() %>)));
    <%  } %>
    <%  if (Model.AssessmentTypeNum > 5) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1040) " : string.Empty %>Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&#8217;s influenza season (October 1 through March 31) during this episode of care?",true) +
        printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("NA &#8211; Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season.",<%= data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("NA").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1045) " : string.Empty %>Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:",true) +
        printview.checkbox("1 &#8211; Received from another health care provider (e.g., physician)",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Received from your agency previously during this year&#8217;s flu season",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Offered and declined",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Assessed and determined to have medical contraindication(s)",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Not indicated; patient does not meet age/condition guidelines for influenza vaccine",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("05").ToString().ToLower() %>) +
        printview.checkbox("6 &#8211; Inability to obtain vaccine due to declared shortage",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("06").ToString().ToLower() %>) +
        printview.checkbox("7 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("07").ToString().ToLower() %>));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1050) " : string.Empty %>Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("0").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("1").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("<%= isOasis ? "(M1055) " : string.Empty %>Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:",true) +
        printview.checkbox("1 &#8211; Patient has received PPV in the past",<%= data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Offered and declined",<%= data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Assessed and determined to have medical contraindication(s)",<%= data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Not indicated; patient does not meet age/condition guidelines for PPV",<%= data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("05").ToString().ToLower() %>));
    <%  } %>
    <% if (Model.AssessmentTypeNum % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/RiskAssessment.ascx", Model); %>
<%  } %>
</script>