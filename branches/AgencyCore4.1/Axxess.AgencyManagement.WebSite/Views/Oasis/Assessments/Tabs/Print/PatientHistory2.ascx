﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<script type="text/javascript">
  printview.addsection(
            printview.col(2,
                printview.span("Last Physician Visit Date:",true) +
                printview.span("<%=  data.AnswerOrEmptyString("GenericLastVisitDate").Clean() %>",false,1)));
    printview.addsection(
            printview.span("<%=data.AnswerOrEmptyString("485AllergiesDescription").Clean() %>"),
            "Allergies");
    printview.addsection(
        printview.col(4,
            printview.span("Apical Pulse:",true) +
            printview.col(3,
                printview.span("<%= data.AnswerOrEmptyString("GenericPulseApical").Clean() %>",false,1) +
                printview.checkbox("Reg",<%= data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Irreg",<%= data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("2").ToString().ToLower() %>)) +
            printview.span("Radial Pulse:",true) +
            printview.col(3,
                printview.span("<%= data.AnswerOrEmptyString("GenericPulseRadial").Clean() %>",false,1) +
                printview.checkbox("Reg",<%= data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Irreg",<%= data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("2").ToString().ToLower() %>)) +
            printview.span("<strong>Height:</strong><%= data.AnswerOrEmptyString("GenericHeight").Clean() %>") +
            printview.span("<strong>Weight:</strong><%= data.AnswerOrEmptyString("GenericWeight").Clean() %> <%= data.AnswerOrEmptyString("GenericWeightActualStated").Equals("1") ? "(Actual)" : string.Empty %><%= data.AnswerOrEmptyString("GenericWeightActualStated").Equals("2") ? "(Stated)" : string.Empty %>") +
            printview.span("<strong>Temperature</strong><%= data.AnswerOrEmptyString("GenericTemp").Clean() %>") +
            printview.span("<strong>Respirations</strong><%= data.AnswerOrEmptyString("GenericResp").Clean() %>") +
            printview.span("BP",true) +
            printview.span("Lying",true) +
            printview.span("Sitting",true) +
            printview.span("Standing",true) +
            printview.span("Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftLying").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftSitting").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftStanding").Clean() %>",false,1) +
            printview.span("Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightLying").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightSitting").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightStanding").Clean() %>",false,1)),
        "Vital Signs");
    
    

</script>