﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>

<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "NutritionForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Nutrition.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
     <% Html.RenderPartial("Action", Model); %>
     <fieldset>
        <legend>Nutrition</legend>
        <% string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
        <% string[] genericProblem = data.AnswerArray("GenericProblem"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutrition", "", new { @id = Model.TypeName + "_GenericNutritionHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Within Normal Limits' id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Dysphagia' id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition2" class="radio">Dysphagia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Appetite' id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition3" class="radio">Appetite</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Weight' id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition4" class="inline-radio">Weight</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition4More" class="float-right">
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionWeightGainLoss", " ", new { @id = Model.TypeName + "_GenericNutritionWeightGainLossHidden" })%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionWeightGainLoss", "Loss", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss"), new { @id = Model.TypeName + "_GenericNutritionWeightLoss", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Weight Loss" })%>
                            <label for="GenericNutritionWeightLoss" class="inline-radio">Loss</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionWeightGainLoss", "Gain", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain"), new { @id = Model.TypeName + "_GenericNutritionWeightGain", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Weight Gain" })%>
                            <label for="GenericNutritionWeightGain" class="inline-radio">Gain</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Diet' id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition5" class="inline-radio">Diet</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition5More" class="float-right">
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionDietAdequate", "", new { @id = Model.TypeName + "_GenericNutritionDietAdequateHidden" })%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionDietAdequate", "Adequate", data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate"), new { @id = Model.TypeName + "_GenericNutritionDietAdequate", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Adequate Diet" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionDietAdequate" class="inline-radio">Adequate</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionDietAdequate", "Inadequate", data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate"), new { @id = Model.TypeName + "_GenericNutritionDietInadequate", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Inadequate Diet" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionDietInadequate" class="inline-radio">Inadequate</label>
                            <%  string[] genericNutritionDiet = data.AnswerArray("GenericNutritionDiet"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionDiet", "", new { @id = Model.TypeName + "_GenericNutritionDietHidden" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='Diet Type' id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition9" class="inline-radio">Diet Type</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition9More" class="float-right">
                            <%= Html.TextBox(Model.TypeName + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.TypeName + "_GenericNutritionDietType", @class = "text", @maxlength = "100", @title = " Diet Type " })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding' id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("6").ToChecked()) %>
                        <label class="radio" for="<%= Model.TypeName %>_GenericNutrition6" class="inline-radio">Enteral Feeding</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericNutrition6More" class="float-right">
                            <%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionEnteralFeeding", "", new { @id = Model.TypeName + "_GenericNutritionEnteralFeedingHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, NG' id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding1" class="inline-radio fixed">NG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, PEG' id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding2" class="inline-radio fixed">PEG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, Dobhoff' id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding3" class="inline-radio fixed">Dobhoff</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Tube Placement Checked' id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition7" class="radio">Tube Placement Checked</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Residual Checked' id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition8" class="inline-radio">Residual Checked</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition8More" class="float-right">
                            <div class="float-right">
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">Amount:</label>
                            <%= Html.TextBox(Model.TypeName + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.TypeName + "_GenericNutritionResidualCheckedAmount", @class = "vitals numeric", @maxlength = "5", @title = "(Optional) Nutrition, Residual Checked Amount" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">ml</label>
                            </div><div class="clear"></div>
                            <div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem1' name='{0}_GenericProblem' value='1' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("1").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem1" class="inline-radio">Throat problems?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem2' name='{0}_GenericProblem' value='2' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("2").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem2" class="inline-radio">Sore throat?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem3' name='{0}_GenericProblem' value='3' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("3").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem3" class="inline-radio">Dentures?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem4' name='{0}_GenericProblem' value='4' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("4").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem4" class="inline-radio">Hoarseness?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem5' name='{0}_GenericProblem' value='5' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("5").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem5" class="inline-radio">Dental problems?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem6' name='{0}_GenericProblem' value='6' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("6").ToChecked())%>
                                <label for="<%= Model.TypeName %>_GenericProblem6" class="inline-radio">Problems chewing?</label>
                                </div>
                                <div class="float-left">
                                <%= string.Format("<input id='{0}_GenericProblem7' name='{0}_GenericProblem' value='7' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("7").ToChecked())%>
                                <label for="<%= Model.TypeName %>__GenericProblem7" class="inline-radio">Other:</label>
                                <%= Html.TextBox(Model.TypeName + "_GenericNutritionOther", data.AnswerOrEmptyString("GenericNutritionOther"), new { @id = Model.TypeName + "_GenericNutritionOther", @class = "text", @maxlength = "100", @title = " Diet Type " })%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionComments" class="strong">Comments:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericNutritionComments", data.AnswerOrEmptyString("GenericNutritionComments"), 10, 50, new { @id = Model.TypeName + "_GenericNutritionCommentsComments", @title = "(Optional) Nutrition Comments" })%>
            </div>
        </div>
    </fieldset>
     <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>

<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    Oasis.CalculateNutritionScore("<%= Model.TypeName %>");
    $("input[name=<%= Model.TypeName %>_GenericNutritionalHealth]").change(function() {
        Oasis.CalculateNutritionScore("<%= Model.TypeName %>")
    });
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNutrition4"),
        $("#<%= Model.TypeName %>_GenericNutrition4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNutrition5"),
        $("#<%= Model.TypeName %>_GenericNutrition5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNutrition6"),
        $("#<%= Model.TypeName %>_GenericNutrition6More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNutrition8"),
        $("#<%= Model.TypeName %>_GenericNutrition8More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericNutrition9"),
        $("#<%= Model.TypeName %>_GenericNutrition9More"));
    U.EnableIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs6"),
        $("#<%= Model.TypeName %>_485NutritionalReqs6More"));
    U.EnableIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs8"),
        $("#<%= Model.TypeName %>_485NutritionalReqs8More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs12"),
        $("#<%= Model.TypeName %>_485NutritionalReqs12More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs13"),
        $("#<%= Model.TypeName %>_485NutritionalReqs13More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs14"),
        $("#<%= Model.TypeName %>_485NutritionalReqs14More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs15"),
        $("#<%= Model.TypeName %>_485NutritionalReqs15More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqsEnteralPer4"),
        $("#<%= Model.TypeName %>_485NutritionalReqsEnteralPer4More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_485NutritionalReqs16"),
        $("#<%= Model.TypeName %>_485NutritionalReqs16More"));
</script>