﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Statistical Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report-links">
                <ul class="float-left half"> 
                    <li class="link"><a href="/Report/Statistical/License" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patients By Insurance Company</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/PatientVisits" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patient Visit History</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/EmployeeVisits" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Employee Visit History</span></a></li> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>