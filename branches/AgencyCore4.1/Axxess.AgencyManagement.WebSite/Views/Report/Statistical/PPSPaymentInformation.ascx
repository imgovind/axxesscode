﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("PPSPaymentInformationReport", "Export", FormMethod.Post)) { %>
    <div class="wrapper">
        <fieldset>
            <legend>Payment Information Report (PPS Log)</legend>
            <div class="column">
                <div class="row"><label for="PPSPaymentInformation_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "PPSPaymentInformation_BranchId" })%></div></div>
                <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="PPSPaymentInformation_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="PPSPaymentInformation_EndDate" /></div></div>
            </div>
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate Report</a></li></ul></div>
        </fieldset>
    </div>
<%} %>
