﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "CensusByPrimaryInsurance"; %>

         <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Sortable(false).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Sortable(false).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Sortable(false).Title("State").Width(50);
                columns.Bound(r => r.PatientAddressZipCode).Sortable(false).Title("Zip Code").Width(60);
                columns.Bound(r => r.PatientPhone).Sortable(false).Title("Home Phone").Width(110);
                columns.Bound(r => r.PatientGender).Sortable(false).Title("Gender").Width(60);
            })
                   //  .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchCode = Guid.Empty, StatuId = 1, InsuranceId = 0 }))
                                         .Sortable(sorting =>
                                                              sorting.SortMode(GridSortMode.SingleColumn)
                                                                  .OrderBy(order =>
                                                                  {
                                                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                      if (sortName == "PatientDisplayName")
                                                                      {
                                                                          if (sortDirection == "ASC")
                                                                          {
                                                                              order.Add(o => o.PatientDisplayName).Ascending();
                                                                          }
                                                                          else if (sortDirection == "DESC")
                                                                          {
                                                                              order.Add(o => o.PatientDisplayName).Descending();
                                                                          }
                                                                      }
                                                                  })
                                                          )
                                       .Scrollable()
                                                .Footer(false)
        %>
    
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','CensusByPrimaryInsuranceContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", InsuranceId : \"" + $('#<%= pagename %>_InsuranceId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>