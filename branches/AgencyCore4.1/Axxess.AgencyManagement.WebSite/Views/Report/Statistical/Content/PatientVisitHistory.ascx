﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "StatisticalPatientVisitHistory"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.DisciplineTaskName).Title("Task").Sortable(false).ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").Sortable(false).ReadOnly();
               columns.Bound(s => s.EventDate).Title("Schedule Date").ReadOnly();
               columns.Bound(s => s.VisitDate).Title("Visit Date").ReadOnly();
               columns.Bound(s => s.UserName).Title("Employee");
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                               .Sortable(sorting =>
                                     sorting.SortMode(GridSortMode.SingleColumn)
                                         .OrderBy(order =>
                                         {
                                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                             if (sortName == "EventDate")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.EventDate).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.EventDate).Descending();
                                                 }
                                             }
                                             else if (sortName == "VisitDate")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.VisitDate).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.VisitDate).Descending();
                                                 }
                                             }
                                             else if (sortName == "UserName")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.UserName).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.UserName).Descending();
                                                 }
                                             }
                                         })
                                 )
                                   .Scrollable()
                                           .Footer(false)
        %>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','StatisticalPatientVisitHistoryContent',{   PatientId : \"" + $('#<%= pagename %>_PatientId').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
