﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AddressBookEntry>>" %>
<% string pagename = "PatientAddressList"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Address Listing</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="column">
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientAddressListContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientAddressList", new { BranchCode = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Patient/Content/AddressListContent", Model); %>
    </div>
</div>
