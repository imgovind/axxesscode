﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Emergency Contact</legend>
        <div class="column">
           <div class="row"><label for="Report_Patient_EC_BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = "Report_Patient_EC_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
           <div class="row"><label for="Report_Patient_EC_Status" class="float-left">Status:</label><div class="float-right"><select id="Report_Patient_EC_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1">Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientEmergencyListing();">Generate Report</a></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<EmergencyContactInfo>().Name("Report_Patient_EC_Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientName).Title("Patient");
               columns.Bound(p => p.ContactName).Title("Contact");
               columns.Bound(p => p.ContactRelation).Title("Relationship");
               columns.Bound(p => p.ContactPhoneHome).Title("Contact Phone");
               columns.Bound(p => p.ContactEmailAddress).Title("Contact E-mail");

           }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientEmergencyListing", "Report", new { StatusId = 0, AddressBranchCode = Guid.Empty }))
            .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $("#Report_Patient_EC_Grid .t-grid-content").css({ 'height': 'auto' });</script>