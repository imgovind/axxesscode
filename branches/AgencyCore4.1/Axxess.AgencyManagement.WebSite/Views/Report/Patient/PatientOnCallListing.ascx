﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientOnCallListing>>" %>
<% string pagename = "PatientOnCallListing"; %>
<h3>
    Patient On Call Listing</h3>
<fieldset>
    <div class="row">
        <label for="AddressStateCode">
            Patient Name:</label><%= Html.LookupSelectList(SelectListTypes.Patients, pagename + "_Name", "", new { @id = pagename + "_Name", @class = "Name report_input valid" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            State:</label><%= Html.LookupSelectList(SelectListTypes.States, pagename + "_AddressStateCode", "", new { @id = pagename + "_AddressStateCode", @class = "AddressStateCode report_input valid" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            Branch:</label><%= Html.LookupSelectList(SelectListTypes.Branches, pagename + "_AddressBranchCode", "", new { @id = pagename + "_AddressBranchCode", @class = "AddressBranchCode report_input valid" })%>
    </div>
    <input id="report_step" type="hidden" name="step" class="report_input" value="2" />
    <input id="report_action" type="hidden" name="action" value="<%= pagename %>" />
    <p>
        <input type="button" value="Generate Report" onclick="Report.populateReport($(this));" />
    </p>
</fieldset>
<div id="<%= pagename %>Result" class="report-grid">
    <h3>
    <% =Html.Telerik().Grid<PatientSocCertPeriod>()
                                 .Name(pagename + "Grid")        
         .Columns(columns =>
     {
     columns.Bound(p => p.PatientPatientID).Title("Patient ID");
     columns.Bound(p => p.PatientLastName).Title("Last Name");
     columns.Bound(p => p.PatientFirstName).Title("First Name");
     columns.Bound(p => p.PatientSoC).Title("SOC Date");
     columns.Bound(p => p.SocCertPeriod).Title("SOC Cert Period");
     columns.Bound(p => p.PhysicianName);
     columns.Bound(p => p.respEmp);
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename+"Result", "Report", new { Name = Guid.Empty, AddressStateCode = " ", AddressBranchCode = Guid.Empty, ReportStep = "" }))
   .Sortable()
   .Selectable()
   .Scrollable()
   .Footer(false)
  
    %>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>