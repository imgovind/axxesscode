﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "UnearnedRevenue"; %>
<div class="wrapper">
    <fieldset>
        <legend>Unearned Revenue</legend>
        <div class="column">
              <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
              <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.InsurancesMedicare("InsuranceId", "0", true, "All", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div>
              <div class="row"><label class="float-left">Date:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','UnearnedRevenueContent',{{ BranchCode: $('#{0}_BranchCode').val(),InsuranceId: $('#{0}_InsuranceId').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportUnearnedRevenue", new { BranchCode = Guid.Empty, InsuranceId = 0, EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
     <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/UnearnedRevenue", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadMedicareWithHMOInsuarnceDropDown('<%= pagename %>', 'All', true, function() { }); });
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
</script>
