﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleVisitsByType"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                 columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(150);
                 columns.Bound(m => m.PatientName).Title("Patient");
                 columns.Bound(m => m.StatusName).Title("Status");
                 //columns.Bound(m => m.DisciplineTaskName).Sortable(false).Title("Task");
                 columns.Bound(m => m.EventDateSortable).Title("Schedule Date").Template(t => t.EventDateSortable).Width(110);
                 columns.Bound(p => p.UserName).Title("Assigned To");
               })
                           .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }
                      }
                      else if (sortName == "PatientName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientName).Descending();
                          }

                      }
                      else if (sortName == "UserName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.UserName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.UserName).Descending();
                          }
                      }
                      else if (sortName == "StatusName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.StatusName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.StatusName).Descending();
                          }
                      }
                      else if (sortName == "EventDateSortable")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.EventDateSortable).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.EventDateSortable).Descending();
                          }
                      }

                  })
                  )
               .Scrollable().Footer(false)  %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ScheduleVisitsByTypeContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", PatientId : \"" + $('#<%= pagename %>_Patient').val() + "\", ClinicianId : \"" + $('#<%= pagename %>_Clinician').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\", Type : \"" + $('#<%= pagename %>_Type').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
