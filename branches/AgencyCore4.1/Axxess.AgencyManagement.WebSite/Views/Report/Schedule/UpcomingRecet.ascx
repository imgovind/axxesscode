﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "ScheduleUpcomingRecet"; %>
<div class="wrapper main">
    <fieldset>
        <legend> Upcoming Recet.</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"] != null ? ViewData["ManLocationId"].ToString() : Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("InsuranceId", ViewData.ContainsKey("Payor") && ViewData["Payor"] != null ? ViewData["Payor"].ToString() : "0", new { @id = pagename + "_InsuranceId", @class = "Insurances" })%></div></div>
            <div class="row"><label class="float-left">Due Date:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To:<input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(7).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','UpcomingRecetContent',{{ BranchCode: $('#{0}_BranchCode').val(), InsuranceId: $('#{0}_InsuranceId').val(), StartDate:$('#{0}_StartDate').val(), EndDate:$('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleUpcomingRecet", new { BranchCode = ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"] != null ? ViewData["ManLocationId"].ToString() : Guid.Empty.ToString(), InsuranceId = ViewData.ContainsKey("Payor") && ViewData["Payor"] != null ? ViewData["Payor"].ToString() : "0", StartDate= DateTime.Now, EndDate=DateTime.Now.AddDays(7)}, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/UpcomingRecet", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>','All',true); });
</script>


