﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "SchedulePatientWeeklySchedule"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Weekly Schedule</legend>
        <div class="column">
              <div class="row"><label for="BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport,"BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label for="SchedulePatientWeeklySchedule_Status" class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
              <div class="row"><label for="" class="float-left">Patient:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_PatientId", @class = "report_input valid" })%></div> </div>
        </div>
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientWeeklyScheduleContent',{{ PatientId: $('#{0}_PatientId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePatientWeeklySchedule", new { PatientId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/PatientWeeklySchedule", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
</script>
