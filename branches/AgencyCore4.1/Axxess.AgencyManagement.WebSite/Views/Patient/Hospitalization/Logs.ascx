﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<HospitalizationLog>>" %>
<ul>
    <li>
        <span class="source">Source</span>
        <span class="user">User</span>
        <span class="date">Date</span>
        <span class="printicon"></span>
        <% if(!Current.IsAgencyFrozen) { %>
        <span class="action">Action</span>
        <% } %>
    </li>
</ul><ol><%
if (Model != null) {
    int i = 1;
    var logs = Model.OrderBy(l => l.Created).ToList();
    if (logs != null && logs.Count > 0)
    {
        foreach (var log in logs) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                <span class="source"><%= log.Source %></span>
                <span class="user"><%= log.User %></span>
                <span class="date"><%= log.HospitalizationDateFormatted %></span>
                <span class="printicon"><%= log.PrintUrl %></span>
                <% if(!Current.IsAgencyFrozen) { %>
                <span class="action"><a href="javascript:void(0);" onclick="HospitalizationLog.Edit('<%=log.PatientId %>', '<%= log.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="HospitalizationLog.Delete('<%=log.PatientId %>', '<%= log.Id %>');">Delete</a></span>
                <% } %>
        </li><%
            i++;
        }
    } else { %>
        <li class="align-center"><span class="darkred">None</span></li>
<% } } %>
</ol>