﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% Html.Telerik().Grid<PatientSelection>().Name("PatientSelectionGrid").Columns(columns => {
       columns.Bound(p => p.LastName);
       columns.Bound(p => p.ShortName).Title("First Name");
       columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
       columns.Bound(p => p.PatientIdNumber).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
   }).DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { branchId=Guid.Empty , statusId = 1, paymentSourceId = 0 }))
   .Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events.OnDataBound("Patient.PatientListDataBound").OnRowSelected("Patient.OnPatientRowSelected")).Render(); %>