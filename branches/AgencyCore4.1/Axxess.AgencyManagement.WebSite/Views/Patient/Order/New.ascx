﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OrderViewData>" %>

<span class="wintitle">New Order | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Order", FormMethod.Post, new { @id = "newOrderForm" })) { %>
<div class="wrapper main">
    
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Order_PatientName" class="float-left">Patient Name:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", Model!=null && Model.PatientId!=Guid.Empty ? Model.PatientId.ToString() : "", new { @id = "New_Order_PatientName", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Order_EpisodeList", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label for="New_Order_PhysicianDropDown" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Model!=null && Model.PhysicianId!=Guid.Empty?Model.PhysicianId.ToString():"", new { @id = "New_Order_PhysicianDropDown", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div><div class="row">
                <label for="New_Order_Date" class="float-left">Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="OrderDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="New_Order_Date" /></div>
            </div><div class="row">
                <label for="New_Order_IsOrderForNextEpisode" class="float-left">Check here if this order is for the next episode</label>
                <div class="float-right"><%= Html.CheckBox("IsOrderForNextEpisode", false, new { @id = "New_Order_IsOrderForNextEpisode", @class = "radio float-left" }) %></div>
            </div>
        </div><div class="wide-column">
            <div class="row">
                <label for="New_Order_Summary" class="float-left">Summary/Title</label>
                <div class="mobile_fr"><%= Html.TextBox("Summary", "", new { @id = "New_Order_Summary", @class = "required longtext", @maxlength = "100" })%></div>
            </div><div class="row">
                <label for="New_Order_Text" class="strong">Order Description</label>
                <%= Html.Templates("New_Order_OrderTemplates", new { @class = "Templates mobile_fr", @template = "#New_Order_Text" })%>
                <div class="align-center"><textarea id="New_Order_Text" name="Text" cols="5" rows="12" style="height: auto;"></textarea></div>
            </div><div class="row">
                <%= Html.CheckBox("IsOrderReadAndVerified", false, new { @id = "New_Order_IsOrderReadAndVerified", @class = "radio float-left" })%>
                <label for="New_Order_IsOrderReadAndVerified" class="float-left">Order read back and verified.</label>
            </div>
       </div>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignature" class="bigtext float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_Order_ClinicianSignature", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignatureDate" class="bigtext float-left">Signature Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="SignatureDate" id="New_Order_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Order_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Order_ClinicianSignatureDate').removeClass('required');$('#New_Order_Status').val('110');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Order_ClinicianSignatureDate').addClass('required');$('#New_Order_Status').val('115');$(this).closest('form').submit();">Create Order</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('neworder');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Order_EpisodeList', $('#New_Order_PatientName'));
    $('#New_Order_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_Order_EpisodeList',$(this)); });
</script>
<% } %>