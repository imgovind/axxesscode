﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div id="patient-info" class="hidden">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Patient_Info_Id" class="float-left">Id:</label>
                <div class="float-right"><%= Model.PatientIdNumber %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Name" class="float-left">Name:</label>
                <div class="float-right"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Gender" class="float-left">Gender:</label>
                <div class="float-right"><%= Model.Gender %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Address1" class="float-left">Address:</label>
                <div class="float-right"><%= Model.AddressFirstRow.ToTitleCase() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Address2" class="float-left">City, State, Zip:</label>
                <div class="float-right"><%= Model.AddressSecondRow.IsNotNullOrEmpty() ? Model.AddressSecondRow.ToTitleCase() : string.Empty %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_HomePhone" class="float-left">Home Phone:</label>
                <div class="float-right"><%= Model.PhoneHome.ToPhone() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_AltPhone" class="float-left">Mobile Phone:</label>
                <div class="float-right"><%= Model.PhoneMobile.ToPhone() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_SocDate" class="float-left">Start of Care Date:</label>
                <div class="float-right"><%= Model.StartOfCareDateFormatted %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_DOB" class="float-left">Date of Birth:</label>
                <div class="float-right"><%= Model.DOBFormatted %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Medicare" class="float-left">Medicare #:</label>
                <div class="float-right"><%= Model.MedicareNumber %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Medicaid" class="float-left">Medicaid #:</label>
                <div class="float-right"><%= Model.MedicaidNumber %></div>
            </div>
            <%var physician = Model.Physician; %>
            <%  if (physician != null )
                { %>
            <div class="row">
                <label for="Patient_Info_Physician" class="float-left">Physician Name:</label>
                <div class="float-right"><%= physician.DisplayName%></div>
            </div>
            <div class="row">
                <label for="Patient_Info_PhysicianPhone" class="float-left">Physician Phone:</label>
                <div class="float-right"><%= physician.PhoneWork.ToPhone()%></div>
            </div>
            <div class="row">
                <label for="Patient_Info_PhysicianFaxNumber" class="float-left">Physician Fax:</label>
                <div class="float-right"><%= physician.FaxNumber.ToPhone()%></div>
            </div>
            <%  } %>
             <%var emergencyContact = Model.EmergencyContact; %>
            <%  if (emergencyContact != null )
                { %>
            <div class="row">
                <label for="Patient_Info_EmergencyContact" class="float-left">Emergency Name:</label>
                <div class="float-right"><%= emergencyContact.DisplayName%></div>
            </div>
            <div class="row">
                <label for="Patient_Info_EmergencyContactPhone" class="float-left">Emergency Phone:</label>
                <div class="float-right"><%= emergencyContact.PrimaryPhone.ToPhone()%></div>
            </div>
            <%  } %>
            <div class="row">
                <label for="Patient_Info_IsDNR" class="float-left">DNR:</label>
                <div class="float-right"><%= Model.IsDNR ? "Yes" : "No" %></div>
            </div>
        </div>
    </fieldset>
</div>