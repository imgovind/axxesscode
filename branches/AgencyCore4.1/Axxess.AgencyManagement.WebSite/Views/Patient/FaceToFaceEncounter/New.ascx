﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">Physician Face-to-face Encounter | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "FaceToFaceEncounter", FormMethod.Post, new { @id = "newFaceToFaceEncounterForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_FaceToFaceEncounter_PatientName" class="float-left">Patient Name:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", !Model.IsEmpty() ? Model.ToString() : "" , new { @id = "New_FaceToFaceEncounter_PatientName", @class="requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="New_FaceToFaceEncounter_EpisodeId" class="float-left">Episode Associated:</label>
                <div class="float-right"> <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_FaceToFaceEncounter_EpisodeId", @class = "requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="New_FaceToFaceEncounter_PhysicianDropDown" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_FaceToFaceEncounter_PhysicianDropDown", @class = "Physicians requiredphysician" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_FaceToFaceEncounter_Date" class="float-left">Request Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="RequestDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="New_FaceToFaceEncounter_Date" /></div>
            </div>
        </div>
        <div class="wide-column"> 
            <table>
                <tbody>
                    <tr>  
                        <td>
                            <%= Html.RadioButton("Certification", "1", true, new { @id = "New_FaceToFaceEncounter_Certification1", @class = "radio" }) %>
                            <label for="New_FaceToFaceEncounter_Certification1">POC Certifying Physician</label>
                        </td>
                        <td>
                            <%= Html.RadioButton("Certification", "2", new { @id = "New_FaceToFaceEncounter_Certification2", @class = "radio" }) %>
                            <label for="New_FaceToFaceEncounter_Certification2">Non POC Certifying Physician</label>
                        </td>
                     </tr>
                </tbody>
            </table>
            <p>*Note:<em>Completing this document creates a Physician Face to Face Encounter request document that must be submitted to the physician. The physician will be required to certify that the patient is homebound and the home health services provided are medically necessary. The physician will certify by signing the face to face encounter document and returning to the home health agency.</em></p>
        </div>
        <div class="clear"></div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_FaceToFaceEncounter_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$('#New_FaceToFaceEncounter_Status').val('115');$(this).closest('form').submit();">Submit</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newfacetofaceencounter');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#New_FaceToFaceEncounter_PhysicianDropDown").PhysicianInput();
    Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $('#New_FaceToFaceEncounter_PatientName'));
    $('#New_FaceToFaceEncounter_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $(this)) }); 
</script>
<%} %>
