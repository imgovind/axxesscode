﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Hospitalized Patients | <%= Current.AgencyName %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PatientHospitalizationData>()
        .Name("List_Patient_Hospitalized_Grid")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(90);
            columns.Bound(p => p.DisplayName).Title("Patient");
            columns.Bound(p => p.Source).Title("Source");
            columns.Bound(p => p.HospitalizationDate).Title("Hospitalization Date").Width(150);
            columns.Bound(p => p.LastHomeVisitDate).Title("Last Home Visit Date").Width(150);
            columns.Bound(p => p.User).Title("User");
            columns.Bound(p => p.Id).Width(100).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowPatientHospitalizationLogs('<#=PatientId#>');\" class=\"\">Show Logs</a>").Title("Action");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("HospitalizationList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Patient_Hospitalized_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>
        <% if (!Current.IsAgencyFrozen) { %>
        .append($("<div/>").addClass("float-left").Buttons([ { Text: "New Hospitalization", Click: function() { HospitalizationLog.Add('<%= Guid.Empty %>'); } } ]))
        <% } %>
        .append($("<div/>").addClass("float-right").Buttons([ { Text:"Export to Excel",Click:function(){U.GetAttachment('Export/HospitalizationLog', {}); } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>