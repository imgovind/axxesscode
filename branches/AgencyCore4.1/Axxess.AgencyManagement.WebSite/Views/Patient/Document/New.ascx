﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using(Html.BeginForm("AddDocument", "Patient", FormMethod.Post, new { @id="UploadDocument_Form"})) { %>
<%= Html.Hidden("PatientId", Model.Id, new { @name = "PatientId" })%>
<div class="wrapper main">
    <fieldset>
        <legend>Upload Patient Document</legend>
        <div class="wide column">
            <div class="row align-center">
                <h1><%= Model.DisplayName %></h1>
            </div>
            <div class="row align-center">
                <label for="UploadDocument_Asset" class="float-left strong">New Document</label>
                <div><input id="UploadDocument_Asset" type="file" class="required" name="Asset" multiple="multiple" /></div>
            </div>
            <div class="row align-center">
                <label class="float-left strong">Document Name</label>
                <div><input type="text" class="required" name="Filename" id="UploadDocument_Filename" maxlength="200" /></div>
            </div>
            <div class="row align-center">
                <label for="UploadDocument_Cassification" class="float-left strong">Upload Type</label>
                <div id="UploadDocument_UploadType"> 
                   <%=Html.UploadTypes("UploadTypeId","0","--Select Type--", new { @class="required notzero"}) %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li>
                <a id="UploadDocument_Submit" href="javascript:void(0);" onclick="$('#UploadDocument_Form').submit()">Submit</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a>
            </li>
        </ul>
    </div>
</div>
<% } %>