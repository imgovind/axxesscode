﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Documents | <%= Model.DisplayName %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PatientDocument>()
        .Name("List_PatientDocuments")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(c => c.Filename).ClientTemplate("<a href=\"/Asset/<#=AssetId#>\"><#=Filename#></a>").Title("Document Name").Sortable(true);
            columns.Bound(c => c.TypeName).Title("Type");            
            columns.Bound(c => c.CreatedDateFormatted).Title("Created").Width(120);
            columns.Bound(c => c.ModifiedDateFormatted).Title("Modified").Width(120);
            columns.Bound(c => c.Id).Title("Action").ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditDocumentModal('<#=PatientId#>','<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteDocument('<#=PatientId#>','<#=Id#>');\" class=\"deletePatientDocument\">Delete</a>");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("GetDocuments", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
    $("#List_PatientDocuments .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $("#List_PatientDocuments .t-grid-content").css({ height: "auto", top: 59 });
</script>
