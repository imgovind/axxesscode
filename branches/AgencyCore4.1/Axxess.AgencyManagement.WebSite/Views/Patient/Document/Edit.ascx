﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDocument>" %>
<% using(Html.BeginForm("UpdateDocument", "Patient", FormMethod.Post, new { @id="EditDocument_Form"})) { %>
<%= Html.Hidden("PatientId", Model.PatientId, new { @name = "PatientId" })%>
<%= Html.Hidden("Id", Model.Id, new { @name = "Id" })%>
<div class="wrapper main">
    <fieldset>
        <legend>Edit Patient Document</legend>
        <div class="row align-center">
            <h1><%= Model.PatientName %></h1>
        </div>
        <div class="row align-center">
            <label class="float-left strong">Document Name</label>
            <%= Html.TextBox("Filename", Model.Filename, new{ @class="required", @maxlength="200"}) %>
        </div>
        <div class="row align-center">
            <label class="float-left strong">Upload Type</label>
            <div id="EditPatient_Type">
                <%=Html.UploadTypes("UploadTypeId",Model.UploadTypeId.ToString(),"--Select Type--", new{ @class="required notzero" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li>
                <a id="EditDocument_Submit" href="javascript:void(0);" onclick="$('#EditDocument_Form').submit()">Save</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a>
            </li>
        </ul>
    </div>
</div>
<% } %>

