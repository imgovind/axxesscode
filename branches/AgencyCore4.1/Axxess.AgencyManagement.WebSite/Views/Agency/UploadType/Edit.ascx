﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UploadType>" %>
<span class="wintitle">Edit Upload Type | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Update", "UploadType", FormMethod.Post, new { @id = "editUploadTypeForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_UploadType_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_UploadType_Type" class="strong">Type</label>
                <div class="float-right">
                    <%= Html.TextBox("Type", Model.Type, new { @id = "Edit_UploadType_Type", @class = "required", @maxlength = "100"}) %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadUploadTypeLog('{0}');\">Activity Logs</a>", Model.Id) %></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
