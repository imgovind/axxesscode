﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Upload Type | <%= Current.AgencyName %></span>
<% using(Html.BeginForm("Add", "UploadType", FormMethod.Post, new { @id = "newUploadTypeForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="New_UploadType_Type" class="strong">Type</label>
                <div class="float-right">
                    <%= Html.TextBox("Type", string.Empty, new { @id = "New_UploadType_Type", @class = "required", @maxlength = "200"}) %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>