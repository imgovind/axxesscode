﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Incident>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Incident/Accident Log<%= Model.PatientName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
    <div class="page largerfont">
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
                        <% if (location == null) location = Model.Agency.GetMainOffice(); %>
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "" %>
                        <%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : ""%> <%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() : ""%><br />
                        <%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : ""%>
                    </td><th class="h1">Incident/Accident Log</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="bicol">
                            <span class="big">Patient Name: <%= Model.Patient != null ? Model.Patient.DisplayName : string.Empty %></span>
                            <span class="big">MR: <%= Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty %></span><br />
                        </span>
                        <span class="quadcol">
                            <span><strong>Physician:</strong></span>
                            <span><%= Model.PhysicianName.ToTitleCase()  %></span>
                            <span><strong>Type of Incident:</strong></span>
                            <span><%= Model.IncidentType %></span>
                            <span><strong>Date of Incident:</strong></span>
                            <span><%= Model.IncidentDate.ToString("MM/dd/yyyy")%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Individual(s) Involved</h3>
        <div class="quadcol"><%string[] individualInvolved = Model.IndividualInvolved.IsNotNullOrEmpty() ? Model.IndividualInvolved.Split(';') : null; %>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Patient") ? "X" : string.Empty %></span><strong></strong>Patient</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Caregiver") ? "X" : string.Empty %></span><strong></strong>Caregiver</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Employee/Contractor") ? "X" : string.Empty %></span><strong></strong>Employee/Contractor</span>
            <span class="checklabel"><span class="checkbox"><%= individualInvolved != null && individualInvolved.Contains("Other") ? "X" : string.Empty %></span><strong></strong>Other (Specify)<%= Model.IndividualInvolvedOther%></span>
        </div><div >
            <span><strong>Describe Incident/Accident:</strong></span>
            <span><%= Model.Description.IsNotNullOrEmpty() ? Model.Description : "<br /><br /><br /><br />"%></span>
        </div><div>
            <span><strong>Action Taken/Interventions Performed:</strong></span>
            <span><%= Model.ActionTaken.IsNotNullOrEmpty() ? Model.ActionTaken : "<br /><br /><br /><br />"%></span>
        </div>
        <div>
            <span><strong>Orders:</strong></span>
            <span><%= Model.Orders.IsNotNullOrEmpty() ? Model.Orders : "<br /><br /><br /><br />"%></span>
        </div>
        <div>
            <span><strong>Follow Up:</strong></span>
            <span><%= Model.FollowUp.IsNotNullOrEmpty() ? Model.FollowUp : "<br /><br /><br /><br />"%></span>
        </div>
        <h3>Notifications</h3>
        <div class="quadcol">
            <span><strong>M.D. Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.MDNotified.IsNotNullOrEmpty() && Model.MDNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>Family/CG Notified?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.FamilyNotified.IsNotNullOrEmpty() && Model.FamilyNotified == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.FamilyNotified.IsNotNullOrEmpty() && Model.FamilyNotified == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.FamilyNotified.IsNotNullOrEmpty() && Model.FamilyNotified == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
            <span><strong>New Orders:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "Yes" ? "X" : string.Empty %></span><strong></strong>Yes</span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "No" ? "X" : string.Empty %></span><strong></strong>No</span>
            <span class="checklabel"><span class="checkbox"><%= Model.NewOrdersCreated.IsNotNullOrEmpty() && Model.NewOrdersCreated == "NA" ? "X" : string.Empty %></span><strong></strong>N/A</span>
        </div><div class="quadcol">
            <span><strong>Signature:</strong></span>
            <span><%= Model.SignatureText %></span>
            <span><strong>Date:</strong></span>
            <span><%= Model.SignatureText.IsNotNullOrEmpty()?Model.SignatureDate.ToString("MM/dd/yyyy"):string.Empty %></span>
        </div>
    </div>
</body>
</html>
