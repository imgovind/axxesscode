﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyPhysician>>" %>
<span class="wintitle">Physician List | <%= Current.AgencyName %></span>
<div class="wrapper">
<div class="t-toolbar t-grid-toolbar" style="height:50px;">
    <div class="buttons">
        <ul class="float-right">
            <li><%= Html.ActionLink("Excel Export", "Physicians", "Export", new { }, new { @id = "List_Physicians_ExportLink", @class = "excel" })%></li>
        </ul>
    </div>
       
            <div class="buttons float-left">
                <ul>
                    <li> <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowNewPhysician()\">New Physician</a>")%> </li>
                </ul>
            </div>
            
        <div id="List_Physicians_Grid_Search" ></div>
    
</div>
<div id="List_PhysiciansGridContainer">
<% Html.RenderPartial("Physician/ListContent", Model); %>
</div>
</div>
<script type="text/javascript">
    $("#List_Physicians_Grid_Search").append($("<div/>").GridSearchById("#List_PhysicianGrid"));
</script>
