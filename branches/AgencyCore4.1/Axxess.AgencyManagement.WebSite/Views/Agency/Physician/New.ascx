﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Physician | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Physician", FormMethod.Post, new { @id = "newPhysicianForm" })) { %>
    <fieldset>
        <legend>Search Physician</legend>
        <div class="column">
            <div class="row">
                <label for="New_Physician_NpiSearch" class="float-right strong">NPI Number:</label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float-left"><input type="text" id="New_Physician_NpiSearch" name="New_Physician_NpiSearch" maxlength="10" style="width:400px" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Physician_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", "", new { @id = "New_Physician_FirstName", @class = "text input_wrapper required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_MiddleIntial" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleName", "", new { @id = "New_Physician_MiddleIntial", @class = "text input_wrapper mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", "", new { @id = "New_Physician_LastName", @class = "text input_wrapper required", @maxlength = "50" })%></div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="New_Physician_Credentials" class="float-left">Credentials:</label>
                <div class="float-right"><%= Html.TextBox("Credentials", "", new { @id = "New_Physician_Credentials", @class = "text input_wrapper", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_NpiNumber" class="float-left">NPI No:</label>
                <div class="float-right"><%= Html.TextBox("NPI", "", new { @id = "New_Physician_NpiNumber", @class = "text input_wrapper digits required", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_PecosCheck" class="float-left">PECOS Verification:</label>
                <div id="New_Physician_PecosCheck" class="float-right" style="width:200px;text-align:left">Not Checked</div>
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row">
                <label for="New_Physician_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Physician_AddressLine1", @class = "text input_wrapper required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Physician_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Physician_AddressCity", @class = "text input_wrapper required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Physician_AddressStateCode", @class = "AddressStateCode required valid" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "New_Physician_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Physician_Phone1" class="float-left">Primary Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneWorkArray" id="New_Physician_Phone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneWorkArray" id="New_Physician_Phone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_long" name="PhoneWorkArray" id="New_Physician_Phone3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Physician_AltPhone1" class="float-left">Alternate Phone:</label>
                <div class="float-right">
                    <input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneAltArray" id="New_Physician_AltPhone1" maxlength="3" />
                    -
                    <input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneAltArray" id="New_Physician_AltPhone2" maxlength="3" />
                    -
                    <input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneAltArray" id="New_Physician_AltPhone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="New_Physician_Fax1" class="float-left">Fax:</label>
                <div class="float-right">
                    <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Physician_Fax1" maxlength="3" />
                    -
                    <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Physician_Fax2" maxlength="3" />
                    -
                    <input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Physician_Fax3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="New_Physician_Email" class="float-left">E-mail:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Physician_Email", @class = "text email input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_Physician_PhysicianAccess" class="float-left">Grant Physician Access:</label>
                <div class="float-right">
                    <%= Html.CheckBox("PhysicianAccess", false, new { @id = "New_Physician_PhysicianAccess", @class = "bigradio" }) %>
                    <label for="New_Physician_PhysicianAccess">(E-mail Address Required)</label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#New_Physician_PhysicianAccess").change(function() {
        if ($("#New_Physician_PhysicianAccess").prop("checked")) $("#New_Physician_Email").addClass("required");
        else $("#New_Physician_Email").removeClass("required");
    })
</script>