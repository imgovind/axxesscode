﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<span class="wintitle">Manage Company Information | <%= Model.Name %></span>
<div class="wrapper main">
    <div class="form-wrapper">
        <div class="bigtext align-center"><%= Current.AgencyName%></div>
        <%  using (Html.BeginForm("UpdateAgency", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
        <fieldset>
            <legend>Company Information</legend>
            <div class="column strong">
                <div class="row"><label for="Edit_Agency_CompanyName">Company Name:</label><div class="float-right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Agency_CompanyName", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_TaxId">Tax Id:</label><div class="float-right"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_TaxIdType">Tax Id Type:</label><div class="float-right"><%var taxIdTypes = new SelectList(new[] { new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" }, new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" }, new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" } }, "Value", "Text", Model.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "Edit_Agency_TaxIdType", @class = "input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float-right"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = "Edit_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Agency_ContactPhoneArray1">Contact Person Phone:</label><div class="float-right"><%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : "", new { @id = "Edit_Agency_ContactPhoneArray1", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : "", new { @id = "Edit_Agency_ContactPhoneArray2", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : "", new { @id = "Edit_Agency_ContactPhoneArray3", @class = "input_wrappermultible autotext required numeric phone_long", @maxlength = "4", @size = "5" })%></div></div>
                <div class="row"><label for="Edit_Agency_CahpsVendor">CAHPS Vendor:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.CahpsVendors, "CahpsVendor", Model.CahpsVendor.ToString(), new { @id = "Edit_Agency_CahpsVendor", @class = "valid" })%></div></div>
            </div>
            <div class="column strong">
                <div class="row"><label for="Edit_Agency_NationalProviderNo">National Provider Number:</label><div class="float-right"><%=Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float-right"><%=Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
                <div class="row"><label for="Edit_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Location</legend>
            <div class="align-center"><label for="Edit_AgencyInfo_LocationId">Agency Branch: </label><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "Edit_AgencyInfo_LocationId", @class = "BranchLocation required" })%></div>
            <div id="Edit_AgencyInfo_Container" class="">
                <% Html.RenderPartial("~/Views/Agency/InfoContent.ascx", new AgencyLocation()); %>
            </div>
        </fieldset>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
                <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('agencyinfo');">Close</a></li>
            </ul>
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    Agency.LoadBranchContent($('#Edit_AgencyInfo_LocationId').val());
    $('#Edit_AgencyInfo_LocationId').change(function() { Agency.LoadBranchContent($(this).val()); });
</script>