﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<span class="wintitle">Edit Insurance | <%= Model.Name %></span>
<% using (Html.BeginForm("Update", "Insurance", FormMethod.Post, new { @id = "editInsuranceForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Insurance_Id" })%>
<% var data = Model != null ? Model.ToChargeRateDictionary() : new Dictionary<string, ChargeRate>(); %>
<% var locator = Model != null ? Model.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
<div class="wrapper main">
    
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_Name" class="float-left">Insurance/Payor Name:</label><div class="float-right"><%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Insurance_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Insurance_PayorType" class="float-left">Payor Type:</label><div class="float-right"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", Convert.ToString(Model.PayorType), new { @id = "Edit_Insurance_PayorType", @class = "requireddropdown valid" })%></div></div>
            <div class="row"><label for="Edit_Insurance_InvoiceType" class="float-left">Invoice Type:</label><div class="float-right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", Convert.ToString(Model.InvoiceType)); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "Edit_Insurance_InvoiceType" })%></div></div>
            <div class="row"><label for="Edit_Insurance_BillType" class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "Institutional", Value = "institutional" }, new SelectListItem { Text = "Professional", Value = "professional" } }, "Value", "Text", Model.BillType); %><%= Html.DropDownList("BillType", billType, new { @id = "Edit_Insurance_BillType" })%></div></div>
            <div class="row">
                <div class="float-left">
                    <%= Html.CheckBox("HasContractWithAgency", Model.HasContractWithAgency, new { @id = "Edit_Insurance_HasContractWithAgency", @class = "radio float-left" })%>&nbsp;
                    <label for="Edit_Insurance_HasContractWithAgency"> Check here if you have a contract with this insurance.</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_PayorId" class="float-left">Payor Id:</label><div class="float-right"><%= Html.TextBox("PayorId", Convert.ToString(Model.PayorId), new { @id = "Edit_Insurance_PayorId", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ProviderId" class="float-left">Provider ID/Code:</label><div class="float-right"><%= Html.TextBox("ProviderId", Model.ProviderId, new { @id = "Edit_Insurance_ProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_OtherProviderId" class="float-left">Other Provider ID:</label><div class="float-right"><%= Html.TextBox("OtherProviderId", Model.OtherProviderId, new { @id = "Edit_Insurance_OtherProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ProviderSubscriberId" class="float-left">Provider Subscriber ID:</label><div class="float-right"><%= Html.TextBox("ProviderSubscriberId", Model.ProviderSubscriberId, new { @id = "Edit_Insurance_ProviderSubscriberId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_SubmitterId" class="float-left">Submitter ID:</label><div class="float-right"><%= Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "Edit_Insurance_SubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
        </div><div class="clear"></div>
        
        <div class="wide-column">
            <div id="Edit_Insurance_Ub04Locators" class="<%= Model.InvoiceType == (int)InvoiceType.UB ? "" : "hidden" %>">
                <label for="Edit_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCa:</label><%= Html.Hidden("Ub04Locator81cca", "Locator1")%><div class="float-left"><%= Html.TextBox("Locator1_Code1", locator.ContainsKey("Locator1") ? locator["Locator1"].Code1 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator1_Code2", locator.ContainsKey("Locator1") ? locator["Locator1"].Code2 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator1_Code3", locator.ContainsKey("Locator1") ? locator["Locator1"].Code3 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
                <label for="Edit_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCb:</label><%= Html.Hidden("Ub04Locator81cca", "Locator2")%><div class="float-left"><%= Html.TextBox("Locator2_Code1", locator.ContainsKey("Locator2") ? locator["Locator2"].Code1 : string.Empty, new { @id = "Edit_Insurance_Locator2_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator2_Code2", locator.ContainsKey("Locator2") ? locator["Locator2"].Code2 : string.Empty, new { @id = "Edit_Insurance_Locator2_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator2_Code3", locator.ContainsKey("Locator2") ? locator["Locator2"].Code3 : string.Empty, new { @id = "Edit_Insurance_Locator2_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
                <label for="Edit_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCc:</label><%= Html.Hidden("Ub04Locator81cca", "Locator3")%><div class="float-left"><%= Html.TextBox("Locator3_Code1", locator.ContainsKey("Locator3") ? locator["Locator3"].Code1 : string.Empty, new { @id = "Edit_Insurance_Locator3_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator3_Code2", locator.ContainsKey("Locator3") ? locator["Locator3"].Code2 : string.Empty, new { @id = "Edit_Insurance_Locator3_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator3_Code3", locator.ContainsKey("Locator3") ? locator["Locator3"].Code3 : string.Empty, new { @id = "Edit_Insurance_Locator3_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
                <label for="Edit_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCd:</label><%= Html.Hidden("Ub04Locator81cca", "Locator4")%><div class="float-left"><%= Html.TextBox("Locator4_Code1", locator.ContainsKey("Locator4") ? locator["Locator4"].Code1 : string.Empty, new { @id = "Edit_Insurance_Locator4_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator4_Code2", locator.ContainsKey("Locator4") ? locator["Locator4"].Code2 : string.Empty, new { @id = "Edit_Insurance_Locator4_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator4_Code3", locator.ContainsKey("Locator4") ? locator["Locator4"].Code3 : string.Empty, new { @id = "Edit_Insurance_Locator4_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column"> <div class="float-left"><%= Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = "Edit_Insurance_IsAxxessTheBiller", @class = "radio float-left" })%>&nbsp;<label for="Edit_Insurance_IsAxxessTheBiller"> Check here if claims are electronically submitted to your clearing house through Axxess™.</label></div><div class="row"><label for="Edit_Insurance_ClearingHouse" class="float-left">Clearing House:</label><div class="float-left"><%  var clearingHouse = new SelectList(new[] { new SelectListItem { Text = string.Empty, Value = "0" },new SelectListItem { Text = "ZirMed", Value = "ZirMed" }}, "Value", "Text",Model.ClearingHouse); %><%= Html.DropDownList("ClearingHouse", clearingHouse)%></div></div></div><div class="clear"></div>
        <div id="Edit_Insurance_EdiInformation">
           <div class="column">
            <div class="row"><label for="Edit_Insurance_InterchangeReceiverId" class="float-left">Interchange Receiver ID:</label><div class="float-right"><%  var interchangeReceiverId = new SelectList(new[] {
                            new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                            new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                            new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                            new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                            new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                            new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                            new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                            new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                            new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                        }, "Value", "Text",Model.InterchangeReceiverId); %>
            
            <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "New_Insurance_InterchangeReceiverId", @class = "valid" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ClearingHouseSubmitterId" class="float-left">Clearing House Submitter ID:</label><div class="float-right"><%= Html.TextBox("ClearingHouseSubmitterId", Model.ClearingHouseSubmitterId, new { @id = "Edit_Insurance_ClearingHouseSubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
           </div>
           <div class="column">
            <div class="row"><label for="Edit_Insurance_SubmitterName" class="float-left">Submitter Name:</label><div class="float-right"><%= Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "Edit_Insurance_SubmitterName", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Insurance_SubmitterPhone1" class="float-left">Submitter Phone:</label><div class="float-right"><%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(0, 3) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(3, 3) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(6, 4) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        </div>
    </fieldset>
     <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Insurance_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Insurance_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Insurance_AddressCity", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_ContactPersonFirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactPersonLastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Insurance_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactEmailAddress" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("ContactEmailAddress", Model.ContactEmailAddress, new { @id = "Edit_Insurance_ContactEmailAddress", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_PhoneNumberArray1" class="float-left">Phone:</label><div class="float-right"><%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() && Model.PhoneNumber.Length >= 3 ? Model.PhoneNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() && Model.PhoneNumber.Length >= 6 ? Model.PhoneNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() && Model.PhoneNumber.Length >= 10 ? Model.PhoneNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_PhoneNumberArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Insurance_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 3 ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 6 ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 10 ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_FaxNumberArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_CurrentBalance" class="float-left">Current Balance:</label><div class="float-right"><%= Html.TextBox("CurrentBalance", Model.CurrentBalance, new { @id = "Edit_Insurance_CurrentBalance", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_WorkWeekStartDay" class="float-left">Work Week Begins:</label><div class="float-right"><%var workWeekStartDay = new SelectList(new[]{ new SelectListItem { Text = "Sunday", Value = "1" },new SelectListItem { Text = "Monday", Value = "2" }}, "Value", "Text", Convert.ToString(Model.WorkWeekStartDay));%><%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "Edit_Insurance_WorkWeekStartDay" })%></div></div>
            <br />
            <div class="row"><label for="Edit_Insurance_IsAuthReq" class="float-left">Visit Authorization Required:</label><div class="float-right"><%= Html.RadioButton("IsVisitAuthorizationRequired", true, Model.IsVisitAuthorizationRequired, new { @id = "Edit_Insurance_IsAuthReq", @class = "radio" })%><label class="inline-radio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired",false ,!Model.IsVisitAuthorizationRequired, new { @class = "radio" })%><label class="inline-radio">No</label></div></div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Load Visit Information from existing Insurance</legend>
        <div class="float-left"><em>To apply the visit information from the selected insurance, click on the "Apply" button on the left.</em></div>
        <div class="clear"></div>
        <div class="wide-column"><label for="Edit_Insurance_OldInsuranceId" class="float-left">Choose existing Insurance: </label><div class="float-left"><%= Html.InsurancesNoneMedicare("OldInsuranceId", "0", true, "-- Select Insurance --", new { @id = "Edit_Insurance_OldInsuranceId" })%></div><div class="buttons"><ul class="float-left"><li><a href="javascript:void(0);" onclick="Insurance.VisitInfoReplace('<%=Model.Id %>',$('#Edit_Insurance_OldInsuranceId').val());">Apply</a></li></ul></div></div>
     </fieldset>
     <div class="buttons"><ul class="align-center"><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewBillData('<%= Model.Id %>');">Add Visit Information</a></li></ul></div><div class="clear"></div>
     <%= Html.Telerik().Grid<ChargeRate>().HtmlAttributes(new { @style = "height:auto; position: relative;margin-bottom: 30px;" }).Name("Edit_Insurance_BillDatas")
            .DataKeys(keys =>{ keys.Add(r => r.Id).RouteKey("Id");  })
            .Columns(columns =>
            {
                columns.Bound(e => e.DisciplineTaskName).Title("Task");
                columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
                columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                columns.Bound(e => e.ExpectedRate).Format("${0:#0.00}").Title("Expected Rate").Width(45);
                columns.Bound(e => e.Charge).Format("${0:#0.00}").Title("Rate").Width(45).HeaderHtmlAttributes(new { @class = "rate-header" });
                columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
                columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
                columns.Bound(e => e.TimeLimitFormat).Title("Time Limit").Width(65);
                columns.Bound(e => e.Id).ClientTemplate("<a  href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditBillData($('#Edit_Insurance_Id').val(),'<#=Id#>');\" >Edit</a> | <a  href=\"javascript:void(0);\" onclick=\"Agency.DeleteBillData($('#Edit_Insurance_Id').val(),'<#=Id#>');\" >Delete</a> ").Title("Action").Width(85);
                
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("BillDatas", "Agency", new { InsuranceId = Model.Id })).Sortable().Footer(false)%>
     <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editinsurance');">Exit</a></li></ul></div>
     <fieldset>
        <legend>Activity Logs</legend>
        <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadInsuranceLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    </fieldset>
</div>
<% } %>
<script type="text/javascript">
    U.HideIfChecked($("#Edit_Insurance_IsAxxessTheBiller"), $("#Edit_Insurance_EdiInformation"));
    U.DoIfChecked(true, $("#Edit_Insurance_HasContractWithAgency"),
        function() {
            $("#Edit_Insurance_BillDatas").data('tGrid').showColumn("ExpectedRate");
            $("th.rate-header a").text("Billed Rate");
        },
        function() {
            $("#Edit_Insurance_BillDatas").data('tGrid').hideColumn("ExpectedRate");
            $("th.rate-header a").text("Rate");
        });
        $("#Edit_Insurance_InvoiceType").change(function() {
            var selectedValue = $(this).val();
            if (selectedValue == 1) {
                $("#Edit_Insurance_Ub04Locators").removeClass("hidden");
                $("#Edit_Insurance_HCFALocators").addClass("hidden");
            } else if (selectedValue == 2) {
                $("#Edit_Insurance_Ub04Locators").addClass("hidden");
                $("#Edit_Insurance_HCFALocators").removeClass("hidden");
            } else {
                $("#Edit_Insurance_Ub04Locators").addClass("hidden");
                $("#Edit_Insurance_HCFALocators").addClass("hidden");
            }
        });
 </script>
