﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<div class="wrapper main">
 <% var isMedicareHMO = Model.PayorType == (int)PayerTypes.MedicareHMO; %>
<%  using (Html.BeginForm("SaveBillData", "Agency", FormMethod.Post, new { @id = "newInsuranceBillData" })) { %>
    <%= Html.Hidden("InsuranceId", Model.Id, new { @id = "New_BillData_InsuranceId" })%>
    <fieldset class="newmed">
        <legend>New Visit Information</legend>
        <div class="wide_column">
            <div class="row">
                <label for="New_BillData_Task" class="float-left">Task:</label>
                <div class="float-right"><%=Html.InsuranceDisciplineTask("Id",0,Model.Id,true, new { @id = "New_BillData_Task", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_Description" class="float-left">Preferred Description:</label>
                <div class="float-right"><%= Html.TextBox("PreferredDescription", "", new { @id = "New_BillData_Description", @class = "text input_wrapper required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_RevenueCode" class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", "", new { @id = "New_BillData_RevenueCode", @class = "text input_wrapper Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_HCPCS" class="float-left">HCPCS Code:</label>
                <div class="float-right"><%= Html.TextBox("Code", "", new { @id = "New_BillData_HCPCS", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row" id="New_BillData_ExpectedRate_Div">
                <label for="New_BillData_ExpectedRate" class="float-left">Expected Rate:</label>
                <div class="float-right"><%= Html.TextBox("ExpectedRate", "", new { @id = "New_BillData_ExpectedRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_ChargeRate" id="New_BillData_ChargeRate_Label" class="float-left">Rate:</label>
                <div class="float-right"><%= Html.TextBox("Charge", "", new { @id = "New_BillData_ChargeRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_Modifier" class="float-left">Modifier:</label>
                <div class="float-right">
                    <%= Html.TextBox("Modifier", "", new { @id = "New_BillData_Modifier", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", "", new { @id = "New_BillData_Modifier2", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", "", new { @id = "New_BillData_Modifier3", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", "", new { @id = "New_BillData_Modifier4", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_BillData_ChargeType" class="float-left">Service Unit Type:</label>
                <div class="float-right"><%=Html.UnitType("ChargeType", "0", new { @id = "New_BillData_ChargeType", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <% if (isMedicareHMO){ %>
             <div class="row hidden" id="New_BillData_MedicareHMORateContent">
                <label for="New_BillData_ChargeRate" class="float-left">Medicare HMO Rate:</label>
                <div class="float-right"><%= Html.TextBox("MedicareHMORate", "", new { @id = "New_BillData_MedicareHMORate", @class = "text input_wrapper", @maxlength = "100" })%></div>
                 <div class="clear"></div>
                <em>This rate applies for the Medicare final claim per visit.</em>
            </div>
            <%} %>
            <div class="row" id="New_BillData_PerVisitUnitContent">
                <label for="New_BillData_PerVisitUnit" class="float-left">Service Units per Visit:</label>
                <div class="float-right"><%= Html.TextBox("Unit", "1", new { @id = "New_BillData_PerVisitUnit", @class = "text input_wrapper sn", @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit.</em>
            </div>
            <div class="row">
                <%= Html.CheckBox("IsUnitsPerDayOnSingleLineItem", false, new { @id = "New_BillData_IsUnitsPerDayOnSingleLineItem", @class = "radio float-left" })%>
                <label for="New_BillData_IsUnitsPerDayOnSingleLineItem">Check if the units of this visit type are totaled per day on a single line.</label>
            </div>
            <div id="New_BillData_IsTimeLimitContent" class="hidden">
                <div class="row">
                    <%= Html.CheckBox("IsTimeLimit",false, new { @id = "New_BillData_IsTimeLimit", @class = "radio float-left" })%>
                    <label for="New_BillData_IsTimeLimit">Check if a time limit applies per unit.</label>
                </div>
                <div class="margin hidden" id="New_BillData_TimeLimitContent">
                    <div class="row">
                        <label for="New_BillData_TimeLimit" class="float-left">Time Limit:</label>
                        <div class="float-right">
                            Hour:<%= Html.TextBox("TimeLimitHour", "", new { @id = "New_BillData_TimeLimitHour", @class = "text input_wrapper sn", @maxlength = "2" })%>
                            MIN:<%= Html.TextBox("TimeLimitMin", "", new { @id = "New_BillData_TimeLimitMin", @class = "text input_wrapper sn", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="New_BillData_SecondDescription" class="float-left">Description:</label>
                        <div class="float-right"><%= Html.TextBox("SecondDescription", "", new { @id = "New_BillData_SecondDescription", @class = "text", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_BillData_SecondRevenueCode" class="float-left">Revenue Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondRevenueCode", "", new { @id = "New_BillData_SecondRevenueCode", @class = "text input_wrapper Frequency", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_BillData_SecondHCPCS" class="float-left">HCPCS Code:</label>
                        <div class="float-right"><%= Html.TextBox("SecondCode", "", new { @id = "New_BillData_SecondHCPCS", @class = "text input_wrapper", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsSecondChargeDifferent", false, new { @id = "New_BillData_IsSecondChargeDifferent", @class = "radio float-left" })%>
                         <label for="New_BillData_IsSecondChargeDifferent">Check if a rate is different.</label>
                    </div>
                    <div id="New_BillData_SecondRateContent" class="hidden">
                        <div class='row' id="New_BillData_SecondExpectedRateContent">
                             <label for="New_BillData_SecondExpectedRate" class="float-left">Expected Rate:</label>
                             <div class="float-right"><%= Html.TextBox("SecondExpectedRate", "", new { @id = "New_BillData_SecondExpectedRate", @class = "text input_wrapper", @maxlength = "100" })%></div>
                        </div>
                        <div class="row" id="New_BillData_SecondChargeContent">
                             <label for="New_BillData_SecondCharge" class="float-left" id="New_BillData_SecondChargeLabel">Rate:</label>
                             <div class="float-right"><%= Html.TextBox("SecondCharge", "", new { @id = "New_BillData_SecondCharge", @class = "text input_wrapper", @maxlength = "100" })%></div>
                        </div>
                    </div>
                    <div class="row">
                        <label for="New_BillData_SecondModifier" class="float-left">Modifier:</label>
                        <div class="float-right">
                            <%= Html.TextBox("SecondModifier", "", new { @id = "New_BillData_SecondModifier", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", "", new { @id = "New_BillData_SecondModifier2", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", "", new { @id = "New_BillData_SecondModifier3", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", "", new { @id = "New_BillData_SecondModifier4", @class = "text input_wrapper insurance-modifier", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="New_BillData_SecondChargeType" class="float-left">Unit Type:</label>
                        <div class="float-right"><%=Html.UnitType("SecondChargeType", "0", new { @id = "New_BillData_SecondChargeType", @class = "text input_wrapper" })%></div>
                   </div>
                    <div class="row" id="New_BillData_SecondPerVisitUnitContent">
                        <label for="New_BillData_SecondPerVisitUnit" class="float-left">Service Units per Visit:</label>
                        <div class="float-right"><%= Html.TextBox("SecondUnit", "1", new { @id = "New_BillData_SecondPerVisitUnit", @class = "text input_wrapper sn", @maxlength = "1" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsUnitPerALineItem", false, new { @id = "New_BillData_IsUnitPerALineItem", @class = "radio float-left" })%>
                         <label for="New_BillData_IsUnitPerALineItem">Check if a unit apply per a line item.</label>
                    </div>
                </div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false'>Save &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#New_BillData_ChargeType").change(function() {
         var chargeTypevalue=$(this).val();
        if (chargeTypevalue == 3 || chargeTypevalue == 2) {$("#New_BillData_IsTimeLimitContent").show();}else {$("#New_BillData_IsTimeLimitContent").hide();}
        if (chargeTypevalue == 1) {
            $("#New_BillData_PerVisitUnitContent").show();
            $("#New_BillData_PerVisitUnit").addClass("required");
             <% if(isMedicareHMO){ %>$("#New_BillData_MedicareHMORateContent").hide().find("input[name=MedicareHMORate]").removeClass("required");<%} %>
        } else {
            $("#New_BillData_PerVisitUnitContent").hide();
            $("#New_BillData_PerVisitUnit").removeClass("required");
             if(chargeTypevalue!=0){<% if(isMedicareHMO){ %>$("#New_BillData_MedicareHMORateContent").show().find("input[name=MedicareHMORate]").addClass("required");<%} %>}
        }
    });
    $("#New_BillData_IsTimeLimit").click(function() {
        if ($(this).is(":checked")) {
            $("#window_ModalWindow").animate({ height: 540, "margin-top": -52 }, 500, function() {$("#New_BillData_TimeLimitContent").addClass("required").show();})
        } else {
            $("#New_BillData_TimeLimitContent").removeClass("required").hide();
            $("#window_ModalWindow").animate({ height: 385, "margin-top": 0 }, 500)
        }
    });

    $("#New_BillData_IsSecondChargeDifferent").click(function() {
        if ($(this).is(":checked")) {
            $("#window_ModalWindow").animate({ height: 570, "margin-top": -52 }, 500, function() {$("#New_BillData_SecondRateContent").addClass("required").show();})
        } else {
            $("#New_BillData_SecondRateContent").removeClass("required").hide();
            $("#window_ModalWindow").animate({ height: 540, "margin-top": -52 }, 500)
        }
    });
    $("#New_BillData_Task").change(function (e) { var value = $("#New_BillData_Task option:selected").text();$("#New_BillData_Description").val(value);});
     $("#New_BillData_SecondChargeType").change(function() {
         var secondChargeTypevalue=$(this).val();
        if (secondChargeTypevalue == 1) {
            $("#New_BillData_SecondPerVisitUnitContent").show();
            $("#New_BillData_SecondPerVisitUnit").addClass("required");
        } else {
            $("#New_BillData_SecondPerVisitUnitContent").hide();
            $("#New_BillData_SecondPerVisitUnit").removeClass("required");
        }
    });
      $("#New_BillData_Task").change(function (e) { 
       var value = $("#New_BillData_Task option:selected").text();
       $("#New_BillData_Description").val(value);
    });
    U.ShowIfChecked($("#Edit_Insurance_HasContractWithAgency"), $("#New_BillData_ExpectedRate_Div"));
    U.ShowIfChecked($("#Edit_Insurance_HasContractWithAgency"), $("#New_BillData_SecondExpectedRateContent"));
    if($("#New_BillData_ExpectedRate_Div:visible").length > 0)
    {
        $("#New_BillData_ChargeRate_Label").text("Billed Rate");
        $("#New_BillData_SecondChargeLabel").text("Billed Rate");
    }
</script>
