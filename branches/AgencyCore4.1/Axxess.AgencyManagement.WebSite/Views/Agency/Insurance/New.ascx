﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Insurance/Payor | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Insurance", FormMethod.Post, new { @id = "newInsuranceForm" })) { %>
<div class="wrapper main">
    
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_Name" class="float-left">Insurance/Payor Name:</label><div class="float-right"><%= Html.TextBox("Name", "", new { @id = "New_Insurance_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Insurance_PayorType" class="float-left">Payor Type:</label><div class="float-right"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", "", new { @id = "New_Insurance_PayorType", @class = "requireddropdown valid" })%></div></div>
            <div class="row"><label for="New_Insurance_InvoiceType" class="float-left">Invoice Type:</label><div class="float-right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "New_Insurance_InvoiceType" })%></div></div>
            <div class="row"><label for="New_Insurance_BillType" class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "Institutional", Value = "institutional" }, new SelectListItem { Text = "Professional", Value = "professional" } }, "Value", "Text", "institutional"); %><%= Html.DropDownList("BillType", billType, new { @id = "New_Insurance_BillType" })%></div></div>
             <div class="row">
                <div class="float-left">
                    <%= Html.CheckBox("HasContractWithAgency", false, new { @id = "New_Insurance_HasContractWithAgency", @class = "radio float-left" })%>&nbsp;
                    <label for="New_Insurance_HasContractWithAgency"> Check here if you have a contract with this insurance.</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_PayorId" class="float-left">Payor ID:</label><div class="float-right"><%= Html.TextBox("PayorId", "", new { @id = "New_Insurance_PayorId", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_ProviderId" class="float-left">Provider ID/Code:</label><div class="float-right"><%= Html.TextBox("ProviderId", "", new { @id = "New_Insurance_ProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_OtherProviderId" class="float-left">Other Provider ID:</label><div class="float-right"><%= Html.TextBox("OtherProviderId", "", new { @id = "New_Insurance_OtherProviderId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_ProviderSubscriberId" class="float-left">Provider Subscriber ID:</label><div class="float-right"><%= Html.TextBox("ProviderSubscriberId", "", new { @id = "New_Insurance_ProviderSubscriberId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_SubmitterId" class="float-left">Submitter ID:</label><div class="float-right"><%= Html.TextBox("SubmitterId", "", new { @id = "New_Insurance_SubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
        </div><div class="clear"></div>
        <div class="wide-column">
            <label for="New_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCa:</label><%= Html.Hidden("Ub04Locator81cca", "Locator1")%><div class="float-left"><%= Html.TextBox("Locator1_Code1", "", new { @id = "New_Insurance_Locator1_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator1_Code2", "", new { @id = "New_Insurance_Locator1_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator1_Code3", "", new { @id = "New_Insurance_Locator1_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
            <label for="New_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCb:</label><%= Html.Hidden("Ub04Locator81cca", "Locator2")%><div class="float-left"><%= Html.TextBox("Locator2_Code1", "", new { @id = "New_Insurance_Locator2_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator2_Code2", "", new { @id = "New_Insurance_Locator2_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator2_Code3", "", new { @id = "New_Insurance_Locator2_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
            <label for="New_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCc:</label><%= Html.Hidden("Ub04Locator81cca", "Locator3")%><div class="float-left"><%= Html.TextBox("Locator3_Code1", "", new { @id = "New_Insurance_Locator3_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator3_Code2", "", new { @id = "New_Insurance_Locator3_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator3_Code3", "", new { @id = "New_Insurance_Locator3_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div><div class="clear"></div>
            <label for="New_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCd:</label><%= Html.Hidden("Ub04Locator81cca", "Locator4")%><div class="float-left"><%= Html.TextBox("Locator4_Code1", "", new { @id = "New_Insurance_Locator4_Code1", @class = "text sn", @maxlength = "2" })%> <%= Html.TextBox("Locator4_Code2", "", new { @id = "New_Insurance_Locator4_Code2", @class = "text input_wrapper", @maxlength = "10" })%> <%= Html.TextBox("Locator4_Code3", "", new { @id = "New_Insurance_Locator4_Code3", @class = "text input_wrapper", @maxlength = "12" })%></div>
       </div>
    </fieldset>
     <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column"> <div class="float-left"><%= Html.CheckBox("IsAxxessTheBiller",false, new { @id = "New_Insurance_IsAxxessTheBiller", @class = "radio float-left" })%>&nbsp;<label for="New_Insurance_IsAxxessTheBiller"> Check here if claims are electronically submitted to your clearing house through Axxess™.</label></div><div class="row"><label for="New_Insurance_ClearingHouse" class="float-left">Clearing House:</label><div class="float-left"><%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                        }, "Value", "Text","0"); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "New_Insurance_ClearingHouse" })%></div></div></div><div class="clear"></div>
        <div id="New_Insurance_EdiInformation">
          <div class="column">
            <div class="row"><label for="New_Insurance_InterchangeReceiverId" class="float-left">Interchange Receiver ID:</label><div class="float-right"><%  var interchangeReceiverId = new SelectList(new[] {
                            new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                            new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                            new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                            new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                            new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                            new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                            new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                            new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                            new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                        }, "Value", "Text","28"); %>
            
            <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "New_Insurance_InterchangeReceiverId", @class = "valid" })%></div></div>
            <div class="row"><label for="New_Insurance_ClearingHouseSubmitterId" class="float-left">Clearing House Submitter ID:</label><div class="float-right"><%= Html.TextBox("ClearingHouseSubmitterId", "", new { @id = "New_Insurance_ClearingHouseSubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
        </div>
          <div class="column">
            <div class="row"><label for="New_Insurance_SubmitterName" class="float-left">Submitter Name:</label><div class="float-right"><%= Html.TextBox("SubmitterName", "", new { @id = "New_Insurance_SubmitterName", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Insurance_SubmitterPhone1" class="float-left">Submitter Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short"name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone3" maxlength="4" /></div></div>
        </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Insurance_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Insurance_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Insurance_AddressCity", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_ContactPersonFirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactPersonLastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "New_Insurance_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactEmailAddress" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("ContactEmailAddress", "", new { @id = "New_Insurance_ContactEmailAddress", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_PhoneNumberArray1" class="float-left">Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short"name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Insurance_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Insurance_FaxNumberArray3" maxlength="4" /></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_CurrentBalance" class="float-left">Current Balance:</label><div class="float-right"><%= Html.TextBox("CurrentBalance", "", new { @id = "New_Insurance_CurrentBalance", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Insurance_WorkWeekStartDay" class="float-left">Work Week Begins:</label><div class="float-right"><% var workWeekStartDay = new SelectList(new[]{ new SelectListItem { Text = "Sunday", Value = "1" },new SelectListItem { Text = "Monday", Value = "2" } }, "Value", "Text");%><%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "New_Insurance_WorkWeekStartDay" })%></div></div><br />
            <div class="row"><label for="New_Insurance_VisitAuthReq" class="float-left">Visit Authorization Required:</label><div class="float-right"><%= Html.RadioButton("IsVisitAuthorizationRequired", "1", new {  @class = "radio" })%><label class="inline-radio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired", "0", true, new { @id = "New_Insurance_VisitAuthReq", @class = "radio" })%><label class="inline-radio">No</label></div></div><br />
        </div>
    </fieldset>
     <fieldset>
        <legend>Load Visit Information from existing Insurance</legend>
         <div class="column">  
               <label for="New_Insurance_OldInsuranceId" class="float-left">Choose existing Insurance: </label><div class="float-left"><%= Html.InsurancesNoneMedicare("OldInsuranceId", "0", true, "-- Select Insurance --", new  { })%></div>
         </div>
     </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newinsurance');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    U.HideIfChecked($("#New_Insurance_IsAxxessTheBiller"), $("#New_Insurance_EdiInformation"));
</script>
