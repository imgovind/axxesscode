﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Infection Log | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Infection", FormMethod.Post, new { @id = "newInfectionReportForm" })) { %>
<div class="wrapper main">
    
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_PatientId" class="float-left">Patient:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Infection_PatientId", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Infection_EpisodeList", @class = "requireddropdown" })%></div>
            </div>
            <div class="row">
                <div>
                    <label for="PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                    <div class="float-right">
                        <span class="bigtext" id="PrimaryDiagnosis"></span>
                        <%--<span class="bigtext"><%= Model.Diagnosis.ContainsKey("PrimaryDiagnosis") ? Model.Diagnosis["PrimaryDiagnosis"] : "" %></span>
                        <%  if (Model.Diagnosis.ContainsKey("ICD9M") && Model.Diagnosis["ICD9M"].IsNotNullOrEmpty()) { %>
                         <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= Model.Diagnosis["ICD9M"] %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                        <%  } %>--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="SecondaryDiagnosis" class="float-left">Secondary Diagnosis:</label>
                    <div class="float-right">
                        <span class="bigtext" id="SecondaryDiagnosis"></span>
                       <%-- <span class="bigtext"><%= Model.Diagnosis.ContainsKey("SecondaryDiagnosis") ? Model.Diagnosis["SecondaryDiagnosis"] : ""%></span>
                        <%  if (Model.Diagnosis.ContainsKey("ICD9M2") && Model.Diagnosis["ICD9M2"].IsNotNullOrEmpty()) { %>
                         <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= Model.Diagnosis["ICD9M2"] %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                        <%  } %>--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="New_Infection_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Infection_PhysicianId", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>                
         </div><div class="column">
            <div class="row">
                <label for="New_Infection_InfectionDate" class="float-left">Date of Infection:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="InfectionDate" id="New_Infection_InfectionDate" /></div>
            </div><div class="row">
                <label for="New_Infection_TreatmentPrescribedYes" class="float-left">Treatment Prescribed?</label>
                <div class="float-right">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", new { @id = "New_Infection_TreatmentPrescribedYes", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", new { @id = "New_Infection_TreatmentPrescribedNo", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", new { @id = "New_Infection_TreatmentPrescribedNA", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "New_Infection_MDNotifiedYes", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "New_Infection_MDNotifiedNo", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "New_Infection_MDNotifiedNA", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "New_Infection_NewOrdersYes", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "New_Infection_NewOrdersNo", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "New_Infection_NewOrdersNA", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNA" class="inline-radio">N/A</label>
                </div>
            </div>
         </div>
         <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%= Guid.Empty %>');" title="Add New Order">Add New Order</a></li>
            </ul>
        </div>
    </fieldset><fieldset>
        <legend>Type of Infection</legend>
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <input id="New_Infection_InfectionType1" type="checkbox" value="Gastrointestinal" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType1" class="radio">Gastrointestinal</label>
                    </td><td>
                        <input id="New_Infection_InfectionType2" type="checkbox" value="Respiratory" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType2" class="radio">Respiratory</label>
                    </td><td>
                        <input id="New_Infection_InfectionType3" type="checkbox" value="Skin" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType3" class="radio">Skin</label>
                    </td><td>
                        <input id="New_Infection_InfectionType4" type="checkbox" value="Wound" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType4" class="radio">Wound</label>
                    </td>
                </tr><tr>
                    <td>
                        <input id="New_Infection_InfectionType5" type="checkbox" value="Urinary" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType5" class="radio">Urinary</label>
                    </td><td>
                        <input id="New_Infection_InfectionType6" type="checkbox" value="Other" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType6" class="radio">Other (specify)</label>
                    </td><td colspan="2">
                        <%= Html.TextBox("InfectionTypeOther", "", new { @id = "New_Infection_InfectionTypeOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <legend>Treatment</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Treatment" class="strong">Treatment/Antibiotic:</label>
                <%= Html.Templates("TreatmentTemplates", new { @class = "Templates", @template = "#New_Infection_Treatment" })%>
                <div><%= Html.TextArea("Treatment", new { @id="New_Infection_Treatment", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="Orders" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "Templates", @template = "#New_Infection_Narrative" })%>
                <div><%= Html.TextArea("Orders", new { @id = "New_Infection_Narrative", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", new { @id = "New_Infection_FollowUp", @style = "height: 180px;" })%></div>
            </div>
        </div>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Infection_ClinicianSignature" class="bigtext float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_Infection_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Infection_SignatureDate" class="bigtext float-left">Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="SignatureDate" id="New_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Infection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Infection_Status').val('515');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Infection_Status').val('520');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newinfectionreport');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $('#New_Infection_PatientId'));
    $('#New_Infection_PatientId').change(function() {
        Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $(this));
        $("#PrimaryDiagnosis").text('');
        $("#SecondaryDiagnosis").text('');
        $("#ICD9M").remove();
        $("#ICD9M1").remove();
    });
    Template.OnChangeInit();
    $('#New_Infection_EpisodeList').change(function() { Schedule.loadDiagnosisData($('#New_Infection_PatientId').val(), $(this).val()); });
    
</script>
<% } %>