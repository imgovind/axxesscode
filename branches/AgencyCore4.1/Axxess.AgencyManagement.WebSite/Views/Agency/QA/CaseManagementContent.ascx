﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%  var data = ViewData["GroupName"].ToString();%>
<%= Html.Hidden("CaseManagement_GroupName",data)%>
<%= Html.Telerik().Grid(Model).Name("caseManagementGrid").Columns(columns =>
{
        columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}'/>", s.CustomValue)).Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
        columns.Bound(s => s.PatientName);
        columns.Bound(s => s.EventDate).Width(100);
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").Title("Task").Width(250);
        columns.Bound(s => s.Status).Width(200).Sortable(false);
        columns.Bound(s => s.RedNote).Title(" ").Width(30).Template(s => s.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip red-note\" href=\"javascript:void(0);\">{0}</a>", s.RedNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.YellowNote).Title(" ").Width(30).Template(s => s.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\">{0}</a>", s.YellowNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.BlueNote).Title(" ").Width(30).Template(s => s.BlueNote.IsNotNullOrEmpty() ? string.Format("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\">{0}</a>", s.BlueNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
    }).Groupable(settings => settings.Groups(groups => {
        if (data == "PatientName") groups.Add(s => s.PatientName);
        else if (data == "EventDate") groups.Add(s => s.EventDate);
        else if (data == "DisciplineTaskName") groups.Add(s => s.TaskName);
        else if (data == "UserName") groups.Add(s => s.UserName);
        else groups.Add(s => s.EventDate);
    })).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).Scrollable().Footer(false) %>
<script type="text/javascript">
    $("#caseManagementGrid .t-group-indicator").hide();
    $("#caseManagementGrid .t-grouping-header").remove();
    $(".t-grid-content", "#window_caseManagement #caseManagementGrid").css({
        "height": "auto",
        "position": "absolute",
        "top": "45px"
    });
    $("#window_caseManagement_content").css({
        "background-color": "#d6e5f3"
    });
    $("#caseManagementGrid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Agency.LoadCaseManagement('<%=data %>','" + U.ParameterByName(link, 'caseManagementGrid-orderBy') + "');");
    });
    Agency.ToolTip("#caseManagementGrid");
</script>