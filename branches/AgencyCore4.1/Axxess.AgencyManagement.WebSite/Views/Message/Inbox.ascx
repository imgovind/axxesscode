﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Messages | <%= Current.AgencyName %></span>
<div id="messaging-container" class="wrapper layout">
    <input type="hidden" value="<%= Current.IsAgencyFrozen %>" id="Messages_IsAgencyFrozen"/>
    <div class="layout-left" style="min-width: 300px;">
        <% Html.RenderPartial("~/Views/Message/Grid.ascx"); %>
    </div>
    <div id="messageInfoResult" class="layout-main">
        <% Html.RenderPartial("~/Views/Message/Info.ascx"); %>
    </div>
</div>
<script type="text/javascript">
    $('#window_messageinbox .layout').layout({ west: { paneSelector: '.layout-left', size: 340 } });
</script>

