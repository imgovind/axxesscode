﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Managed Claim Payments | <%= Model.DisplayNameWithMi %></span>
 <% Html.Telerik().Grid<ManagedClaimPayment>().DataKeys(keys => { keys.Add(c => c.Id).RouteKey("Id"); })
        .Name("AllManagedBillingPaymentsGrid").Columns(columns =>
    {
        columns.Bound(p => p.Payment).Format("{0:C}");
        columns.Bound(p => p.PaymentDate).Format("{0:MM/dd/yyyy}");
        columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalUpdatePaymentManagedClaim('<#=Id#>');\">Update</a> | <a href=\"javascript:void(0);\" onclick=\"ManagedBilling.DeletePayment('<#=PatientId#>','<#=ClaimId#>','<#=Id#>');\">Delete</a>").Title("Actions").Width(200).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AllManagedClaimPaymentsGrid", "Billing", new { patientId = Model.Id }))
    .Sortable().Groupable(settings => settings.Groups(groups =>
    {
        groups.Add(s => s.ClaimDateRange);
    })).Selectable().Scrollable().Footer(false).Render(); %>
<script type="text/javascript"> $("#AllManagedBillingPaymentsGrid .t-grid-content").css({ 'height': 'auto' }); </script>

