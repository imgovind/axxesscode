﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  using (Html.BeginForm("ManagedVisitVerify", "Billing", FormMethod.Post, new { @id = "managedBillingVisitForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id= "ManagedVisit_PatientId"})%>
<div class="wrapper main">
    <div class="billing">
        <h3 class="align_center">Date Range: <%= Model.EpisodeStartDate.ToShortDateString()%> &#8211; <%= Model.EpisodeEndDate.ToShortDateString()%></h3>
    <%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
        <%  foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
            <%  var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
            <%  var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
            <%  if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
        <ul>
            <li class="align_center">
                <h3><%= billCategoryKey.GetDescription()%></h3>
            </li>
            <li>
                <span class="rap-checkbox"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Scheduled Date</span>
                <span class="visitdate">Visit Date</span>
                <span class="visithcpcs">HCPCS</span>
                <span class="visitmodifiers">Modifiers</span>
                <span class="visitstatus">Status</span>
                <span class="visitunits">Units</span>
                <span class="visitcharge">Charge</span>
            </li>
        </ul>
                <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
                    <%  var visits = billCategoryVisits[disciplineVisitKey]; %>
                    <%  if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="discpiline-title">
                <span><%= disciplineVisitKey.GetDescription() %></span>
            </li>
                        <%  int i = 1; %>
                        <%  foreach (var visit in visits) { %> 
            <li class="<%= i % 2 != 0 ? "odd notready" : "even notready" %> main-line-item" onmouseover="<%= !visit.EventId.IsEmpty() ? "$(this).addClass('hover');" : "" %>" onmouseout="$(this).removeClass('hover');">
                <label for="<%= visit.ViewId + "_" + billCategoryKey.GetCustomShortDescription() %>">
                    <span class="rap-checkbox">
                            <%  if (!visit.IsExtraTime) { %>
                        <%= i%>. <input class="radio <%= visit.UnderlyingVisits != null && visit.UnderlyingVisits.Count > 0 ? "day-per-line" : "" %>" name="Visit" type="checkbox" id="<%= visit.ViewId  + "_" + billCategoryKey.GetCustomShortDescription()%>" value="<%= isBillable ? visit.EventId.ToString() + "\" checked=\"checked"  : visit.EventId.ToString() %>" />
                            <%  } %>
                    </span>
                    <span class="visittype"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName %></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "" %></span>
                    <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "") %></span>
                    <span class="visithcpcs"><%= visit.HCPCSCode %></span>
                    <span class="visitmodifiers"><%= visit.Modifier %>&nbsp;<%= visit.Modifier2 %>&nbsp;<%= visit.Modifier3 %>&nbsp;<%= visit.Modifier4 %></span>
                    <span class="visitstatus"><%= visit.StatusName %></span>
                    <span class="visitunits"><%= visit.Unit %></span>
                    <span class="visitcharge"><%= string.Format("${0:#0.00}", visit.Charge) %></span>
                </label>
                <% if (visit.UnderlyingVisits != null && visit.UnderlyingVisits.Count > 1) { %>
                <ol>
                <%  int i2 = 1; %> 
                <%  foreach (var visit2 in visit.UnderlyingVisits)
                    { %> 
                    <li class="<%= i2 % 2 != 0 ? "odd notready" : "even notready" %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                        <label for="<%= visit2.ViewId %>">
                            <span class="rap-checkbox">
                            <%  if (!visit2.IsExtraTime) { %>
                                <%= i2 %>. <input class="radio" name="Visit" type="checkbox" id="<%= visit2.ViewId %>" value="<%= isBillable ? visit2.EventId.ToString() + "\" checked=\"checked"  : visit2.EventId.ToString() %>" />
                            <%  } %>
                            </span>
                            <span class="visittype"><%= visit2.PereferredName.IsNotNullOrEmpty() ? visit2.PereferredName : visit2.DisciplineTaskName%></span>
                            <span class="visitdate"><%= visit2.EventDate.IsNotNullOrEmpty() && visit2.EventDate.IsValidDate() ? visit2.EventDate.ToDateTime().ToString("MM/dd/yyy") : ""%></span>
                            <span class="visitdate"><%= visit2.VisitDate.IsNotNullOrEmpty() && visit2.VisitDate.IsValidDate() ? visit2.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit2.EventDate.IsNotNullOrEmpty() && visit2.EventDate.IsValidDate() ? visit2.EventDate.ToDateTime().ToString("MM/dd/yyy") : "")%></span>
                            <span class="visithcpcs"><%= visit2.HCPCSCode%></span>
                            <span class="visitmodifiers"><%= visit2.Modifier%>&nbsp;<%= visit2.Modifier2%>&nbsp;<%= visit2.Modifier3%>&nbsp;<%= visit2.Modifier4%></span>
                            <span class="visitstatus"><%= visit2.StatusName%></span>
                            <span class="visitunits"><%= visit2.Unit%></span>
                            <span class="visitcharge"><%= string.Format("${0:#0.00}", visit2.Charge)%></span>
                        </label>
                    </li>
                <%  if (!visit2.IsExtraTime) i2++; %>
                <% } %>
                </ol>
               <% } %>
            </li>
                
                            <%  if (!visit.IsExtraTime) i++; %>
                        <%  } %>
        </ol>
                    <%  } %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ManagedBilling.NavigateBack(1);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
</div>
<%  } %>
<script type="text/javascript">
    ManagedBilling.InitVisits();
    ManagedBilling.Navigate(3, "#managedBillingVisitForm", $("#ManagedVisit_PatientId").val());
    $("#ManagedClaimTabStrip-3 ol").each(function() { $("li:last", $(this)).addClass("last") });
</script>