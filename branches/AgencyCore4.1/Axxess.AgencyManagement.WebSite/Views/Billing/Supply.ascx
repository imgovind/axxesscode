﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimSupplyViewData>" %>
<div id="supplyTab" class="wrapper main">
<%  using (Html.BeginForm("SupplyVerify", "Billing", FormMethod.Post, new { @id = "finalBillingSupplyForm" })){ %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "FinalSupply_PatientId" })%>
    <%= Html.Hidden("episodeId",Model.EpisodeId) %>
    <div class="billing">
        <h3 class="align-center">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy") %> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy") %></h3>
        <div class="row">
            <%= Html.CheckBox("IsSupplyNotBillable", Model.IsSupplyNotBillable, new { @id = "Final_Supply_IsSupplyNotBillable", @class = "radio float-left" }) %>
            <label for="Final_Supply_IsSupplyNotBillable">
                Check this box if you do not want to bill for supplies.
                <em>(This removes all supplies from the claim)</em>
            </Label>
        </div>
        <div id="FinalBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>(Model.BilledSupplies).Name("FinalBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).ToolBar(commands => {commands.Custom();}).DataBinding(dataBinding => { dataBinding.Ajax()
                    .Select("SupplyBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId, Type = "Final" });
                }).HtmlAttributes(new { style = "position:relative;" }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").Template(s => s.UniqueIdentifierCheckbox).Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '<#= UniqueIdentifier #>', 'Final', true);\">Edit</a>").
                        Template(s => "<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '" + s.UniqueIdentifier + "', 'Final', true);\">Edit</a>").Width(140).Title("Action");
                }).ClientEvents(events => events
                    .OnRowSelect("Supply.RowSelected").OnDataBinding("Supply.OnDataBinding")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
        <div id="FinalUnBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Non-Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>(Model.UnbilledSupplies).Name("FinalUnBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).HtmlAttributes(new { style = "position:relative;" }).ToolBar(commands => { commands.Custom(); }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").Template(s => s.UniqueIdentifierCheckbox).ReadOnly().Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '<#= UniqueIdentifier #>', 'Final', false);\">Edit</a>").
                        Template(s => "<a href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingEditSupply('" + Model.Id + "','" + Model.PatientId + "', '" + s.UniqueIdentifier + "', 'Final', false);\">Edit</a>").Width(140).Title("Action");
                }).DataBinding(dataBinding =>{ dataBinding.Ajax()
                    .Select("SupplyUnBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId, Type = "Final" });
                }).ClientEvents(events => events
                    .OnRowSelect("Supply.RowSelected").OnDataBinding("Supply.OnDataBinding")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Billing.NavigateBack(2);">Back</a></li>
            <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Billing.Navigate(3, '#finalBillingSupplyForm', $("#FinalSupply_PatientId").val());
    $("#FinalBillingSupplyGrid .t-grid-toolbar").empty();
    $("#FinalBillingSupplyGrid .t-grid-toolbar").append("<a class=\"t-button float-left\" href='javascript:void(0);' onclick=\"UserInterface.ShowModalBillingNewSupply('<%=Model.Id%>','<%=Model.PatientId%>', 'Final');\">Add New Supply</a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalBillingSupplyGrid'),false, 'Final');\"> Mark As Non-Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalBillingSupplyGrid'), 'Final');\"> Delete </a> <lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#FinalUnBillingSupplyGrid .t-grid-toolbar").empty().append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalUnBillingSupplyGrid'),true, 'Final');\"> Mark As Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Supply.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalUnBillingSupplyGrid'), 'Final');\"> Delete </a><lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#finalBillingSupplyForm #FinalBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
    $("#finalBillingSupplyForm #FinalUnBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
</script>