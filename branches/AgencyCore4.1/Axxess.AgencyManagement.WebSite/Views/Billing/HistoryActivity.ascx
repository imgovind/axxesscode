﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientInsuranceViewData>" %>
<script type="text/javascript">

    function onEditSnapShotDataBound(e) {
        if (e != undefined && e.dataItem != undefined && e.dataItem.PaymentDate != undefined) {
            var day = new Date(e.dataItem.PaymentDate);
            if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[3].innerHTML = '&#160;';
        }
    }
 </script>
<%  var val = Model != null && !Model.PatientId.IsEmpty() ? Model.PatientId : Guid.Empty;
    Html.Telerik().Grid<ClaimHistoryLean>().Name("BillingHistoryActivityGrid").Columns(columns =>
    {
        columns.Bound(p => p.Type).Title("Type").Width(50);
        columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width(150);
        columns.Bound(p => p.StatusName).Title("Status");
        columns.Bound(p => p.ClaimAmount).Title("Claim Amount").Format("${0:#0.00}").Width(90);
        columns.Bound(p => p.PaymentAmount).Title("Payment Amount").Format("${0:#0.00}").Width(110);
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(100);
        columns.Bound(p => p.Id).Title("").ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/UB04Pdf', { 'patientId': '<#=PatientId#>', 'Id': '<#=Id#>', 'type': '<#=Type#>' });\">UB-04</a>").Width(75);
        columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalEditClaim('<#=PatientId#>','<#=Id#>','<#=Type#>');\">Update</a>&nbsp;|&nbsp;<a href=\"javascript:void(0);\" onclick=\"Billing.DeleteClaim('<#=PatientId#>','<#=Id#>','<#=Type==\"FINAL\"?\"Final\":Type#>');\">Delete</a>").Title("Action").Width(120).Visible(!Current.IsAgencyFrozen);
        columns.Bound(p => p.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DetailView(details => details.ClientTemplate(
                    Html.Telerik().Grid<ClaimSnapShotViewData>().HtmlAttributes(new { @style = "position:relative;" })
                                    .Name("ClaimSnapShot_<#= Id #><#= Type #>")
                                         .DataKeys(keys =>
                                         {
                                             keys.Add(r => r.Id).RouteKey("Id");
                                             keys.Add(r => r.BatchId).RouteKey("BatchId");
                                             keys.Add(r => r.Type).RouteKey("Type");
                                         })
                                    .Columns(columns =>
                                    {
                                        columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
                                        columns.Bound(o => o.EpisodeRange).Width(150);
                                        columns.Bound(o => o.ClaimDate).Width(140).Title("Claim Date").ReadOnly();
                                        columns.Bound(o => o.PaymentDateFormatted).Format("{0:MM/dd/yyyy}").Width(90).Title("Payment Date");
                                        columns.Bound(o => o.PaymentAmount).Format("${0:#0.00}").Title("Payment Amount").Width(130);
                                        columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>");
                                        columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalEditClaimSnapShot('<#=Id#>','<#=BatchId#>','<#=Type#>');\">Update</a>").Title("Action").Width(60).Visible(!Current.IsAgencyFrozen);
                                    })
                                    .DataBinding(dataBinding => dataBinding.Ajax()
                                            .Select("SnapShotClaims", "Billing", new
                                            {
                                                Id = "<#= Id #>",
                                                Type = "<#= Type #>"
                                            })).ClientEvents(events => events.OnEdit("Billing.onEditClaim").OnRowDataBound("onEditSnapShotDataBound")).Footer(false).ToHtmlString()

                                            ))
    .DataBinding(dataBinding => dataBinding.Ajax().Select("HistoryActivity", "Billing", new { patientId = val, insuranceId = Model.InsuranceId })).ClientEvents(events => events.OnDataBound("Billing.OnClaimDataBound").OnRowSelected("Billing.OnClaimRowSelected").OnDetailViewCollapse("Billing.onClaimDetailViewCollapse").OnDetailViewExpand("Billing.onClaimDetailViewExpand")).Sortable().Selectable().Scrollable().Footer(false).HtmlAttributes(new { Style = "min-width:100px;top:0px;" }).Render(); %>
