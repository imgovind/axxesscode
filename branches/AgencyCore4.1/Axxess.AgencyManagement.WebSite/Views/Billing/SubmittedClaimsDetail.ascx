﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoDetailList>" %>
<span class="wintitle">Submitted Claims Detail | <%= Current.AgencyName %></span>
<div class="wrapper main">
<div class="trical">
    <div class="buttons float-right">
        <ul>
            <%= string.Format("<li><a href='/Export/SubmittedClaimDetails?Id={0}&StartDate={1}&EndDate={2}'>Export</a></li>", Model.Id, Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString())%>
        </ul>
    </div>
</div>

<%= Html.Telerik().Grid(Model.Details)
        .Name("ClaimInfoDetail_List")
        .Columns(columns =>
        {
            columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(200);
            columns.Bound(o => o.DisplayName).Title("Patient");
            columns.Bound(o => o.Range).Title("Episode");
            columns.Bound(o => o.BillType).Format("{0:MM/dd/yyyy}").Width(120);
        }).Footer(false)
%>
</div>
<script type="text/javascript">
    $("#ClaimInfoDetail_List .t-grid-content").css({ 'height': 'auto' });
    $("#ClaimInfoDetail_List").css({ 'top': '60px' });
</script>