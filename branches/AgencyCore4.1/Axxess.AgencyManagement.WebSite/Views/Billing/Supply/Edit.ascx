﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimEditSupplyViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("SupplyBillableUpdate", "Billing", FormMethod.Post, new { @id = "edit" + Model.Type + "ClaimSupplyForm" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <%= Html.Hidden("UniqueIdentifier", Model.Supply.UniqueIdentifier)%>
    <fieldset>
        <legend>New Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Description:</label>
                <div class="float-right"><%= Html.TextBox("Description", Model.Supply.Description, new { @class = "text input_wrapper required", @style = "width:320px;" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Revenue Code:</label>
                <div class="float-right"><%= Html.TextBox("RevenueCode", Model.Supply.RevenueCode, new {@class = "text input_wrapper required", @maxlength = "" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">HCPCS:</label>
                <div class="float-right"><%= Html.TextBox("Code", Model.Supply.Code, new { @class = "text input_wrapper required" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Date:</label>
                <div class="float-right"><%= Html.TextBox("Date", Model.Supply.Date, new { @class = "date-picker input_wrapper required" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Unit:</label>
                <div class="float-right"><%= Html.TextBox("Quantity", Model.Supply.Quantity, new { @class = "text input_wrapper required numeric" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Unit Cost:</label>
                <div class="float-right">$<%= Html.TextBox("UnitCost", Model.Supply.UnitCost, new { @class = "text input_wrapper required floatnum" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Total Cost:</label>
                <div class="float-right">
                    $<%= Html.TextBox("EditSupply_TotalCost", Model.Supply.TotalCost, new { @class = "text input_wrapper", @id = "EditSupply_TotalCost", disabled = "disabled" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script>
    $("#edit<%= Model.Type %>ClaimSupplyForm #Quantity").bind('input', function(e) {
        var units = $(this).val();
        var cost = $("#edit<%= Model.Type %>ClaimSupplyForm #UnitCost").val();
        
        if(!isNaN(units) && !isNaN(cost))
        {
            var total = units * cost;
            $("#NewSupply_TotalCost").val(U.FormatMoney(total));
        }
    });

    $("#edit<%= Model.Type %>ClaimSupplyForm #UnitCost").bind('input', function(e) {
        var cost = $(this).val();
        var units = $("#edit<%= Model.Type %>ClaimSupplyForm #Quantity").val();
        
        if(!isNaN(units) && !isNaN(cost))
        {
            var total = units * cost;
            $("#EditSupply_TotalCost").val(U.FormatMoney(total));
        }
    });
</script>