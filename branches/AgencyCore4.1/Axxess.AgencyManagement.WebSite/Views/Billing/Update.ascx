﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("SaveUpdate", "Billing", FormMethod.Post, new { @id = "updateClaimForm" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>Update Information</legend>
        <div class="column">
            <div class="row">
                <label for="Status" class="float-left">Claim Status:</label>
                <div class="float-right"><%= Html.BillStatus("Status", Model.Status.ToString(), false, new { })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="PaymentAmount" class="float-left">Payment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("PaymentAmount", Model.PaymentAmount > 0 ? Model.PaymentAmount.ToString() : "", new { @class = "text input_wrapper", @maxlength = "" })%></div>
            </div>
            <div class="row">
                <label for="PaymentDateValue" class="float-left">Payment Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="PaymentDateValue" value="<%= Model.PaymentDate.Date <= DateTime.MinValue.Date ? DateTime.Now.ToShortDateString() : Model.PaymentDate.ToShortDateString() %>" /></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment" class="strong">Comment:</label>
                <div><%= Html.TextArea("Comment", Model.Comment)%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>