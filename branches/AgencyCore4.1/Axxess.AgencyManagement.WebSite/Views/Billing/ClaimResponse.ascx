﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Claim Response | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <pre><%= Model.Replace('\u0009'.ToString(), "    ") %></pre>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick="$(this).closest('.window').Close();return false">Close</a></li>
        </ul>
    </div>
</div>