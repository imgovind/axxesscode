﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<span class="wintitle">Final (EOE) Claims | <%= Model.DisplayName %></span>
<% Html.Telerik().TabStrip().HtmlAttributes(new { @style = "height:auto" }).Name("final-tabs")
        .Items(parent => {
            parent.Add().Text("Step 1 of 4:^Demographics").LoadContentFrom("Info", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId }).Selected(true);
            parent.Add().Text("Step 2 of 4:^Verify Visits").LoadContentFrom("Visit", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId }).Enabled(Model.IsFinalInfoVerified);
            parent.Add().Text("Step 3 of 4:^Verify Supplies").LoadContentFrom("Supply", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId }).Enabled(Model.IsVisitVerified);
            parent.Add().Text("Step 4 of 4:^Summary").LoadContentFrom("Summary", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId }).Enabled(Model.IsSupplyVerified);
        }).ClientEvents(events => events.OnLoad("Billing.finalTabStripOnLoad").OnSelect("Billing.finalTabStripOnSelect")).Render(); %>
<script type="text/javascript">
    $("#final-tabs li.t-item a").each(function() { if ($(this).closest("li").hasClass("t-state-disabled")) { $(this).click( U.BlockClick) } $(this).html("<span><strong>" + ($(this).html().substring(0, $(this).html().indexOf(":^") + 1) + "</strong><br/>" + $(this).html().substring($(this).html().indexOf(":^") + 2)) + "</span>") });
</script>