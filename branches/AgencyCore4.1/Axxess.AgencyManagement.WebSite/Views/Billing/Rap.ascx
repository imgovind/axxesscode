﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rap>" %>
<span class="wintitle">RAP | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("RapVerify", "Billing", FormMethod.Post, new { @id = "rapVerification" })) { %>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<%  var valueCodes = Model != null ? Model.Ub04Locator39.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
    <div class="buttons">
        <ul>
            <%= String.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId)%>
        </ul>
    </div>
    <fieldset>
        <legend>Patient</legend>
        <%= Html.Hidden("Id",Model.Id) %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Rap_PatientId" })%>
        <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO)%>
        <div class="column">
            <div class="row">
                <label for="FirstName" class="float-left">Patient First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "text {validate:{required:true}}", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="LastName" class="float-left">Patient Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @class = "text {validate:{required:true}}", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="MedicareNumber" class="float-left">Medicare #:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @class = "text required", @maxlength = "12" }) %></div>
            </div>
            <div class="row">
                <label for="PrimaryInsuranceId" class="float-left">Insurance:</label>
                <div class="float-right"><%= Html.InsurancesMedicareByBranch("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"),Model.AgencyLocationId, true, "-- Select Insurnace --", new { @class = "requireddropdown", @id = "Rap_PrimaryInsuranceId" })%></div>
            </div>
            <div id="RAPHMOInfo_InsuranceContent"><%  if (Model.PrimaryInsuranceId >= 1000) Html.RenderPartial("~/Views/Billing/InsuranceInfoContent.ascx", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "RAPHMOInfo" }); %></div> 
            <div class="row">
                <label for="PatientIdNumber" class="float-left">Patient ID/MR Number: </label>
                <div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text required", @maxlength = "30" }) %></div>
            </div>
            <div class="row">
                <label for="Gender" class="float-left">Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "GenderFemale", @class = "radio required" })%>
                    <label for="GenderFemale" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "GenderMale", @class = "radio required" })%>
                    <label for="GenderMale" class="inline-radio">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="DOB" class="float-left">Date of Birth:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Rap_DOB" /></div>
            </div>
            <div class="row">
                <label for="Type" class="float-left">Bill Type:</label>
                <%  var billType = new SelectList(new[] {
                        new SelectListItem { Text = "Initial Rap", Value = "0" }
                    }, "Value", "Text", Model.Type); %>
                <div class="float-right"><%= Html.DropDownList("Type", billType) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="StartOfCareDate" class="float-left">Admission Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Rap_StartOfCareDate" /></div>
            </div>
            <div class="row">
                <label for="AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : "", new { @class = "AdmissionSource requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="PatientStatus" class="float-left">Patient Status:</label>
                <div class="float-right"><%= Html.UB4PatientStatus("UB4PatientStatus", ((int)UB4PatientStatus.StillPatient).ToString(), new { @id = "Rap_PatientStatus", @class = "requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @class = "text required" }) %></div>
            </div>
            <div class="row">
                <label for="AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text" }) %></div>
            </div>
            <div class="row">
                <label for="AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required" }) %></div>
            </div>
            <div class="row">
                <label for="AddressStateCode" class="float-left">State, Zip Code:</label>
                <div class="float-right">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @class = "input_wrapper AddressStateCode" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip zip", @maxlength = "9" }) %>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <label class="float-left">Condition Codes:</label>
            <div class="clear"></div>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <label class="float-left">18.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">19.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">20.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">21.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">22.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">23.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">24.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">25.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">26.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">27.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                        <td>
                            <label class="float-left">28.</label>
                            <div class="float-left"><%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : "", new { @class = "sn", @maxlength = "2" })%></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
    <fieldset>
        <legend>Episode</legend>
        <div class="column">
            <%= Html.Hidden("AssessmentType", Model.AssessmentType)%>
            <div class="row">
                <label for="HippsCode" class="float-left">HIPPS Code:</label>
                <div class="float-right"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @class = "text required", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="ClaimKey" class="float-left">OASIS Matching Key:</label>
                <div class="float-right"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "text required", @maxlength = "18" }) %></div>
            </div>
            <div class="row">
                <label for="EpisodeStartDate" class="float-left">Episode Start Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="Rap_EpisodeStartDate" /></div>
            </div>
            <div class="row">
                <span class="float-right">Recommended / Previously Entered First Billable Date: <%=Model.FirstBillableVisitDateFormat %></span>
                <div class="clear"></div>
                <label for="FirstBillableVisitDateFormatInput" class="float-left">Date Of First Billable Visit:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="FirstBillableVisitDateFormatInput" value="<%= Model.FirstBillableVisitDateFormat %>" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="Rap_FirstBillableVisitDateFormat" /></div>
                <br />
                <em>Please Verifiy the first billable visit date from the schedule.</em>
                <div class="buttons float-right">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="UserInterface.ShowScheduleCenter('<%= Model.PatientId %>', '<%= Model.PatientStatus %>');">View Schedule</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <label for="PhysicianLastName" class="float-left">Physician Last Name:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="PhysicianFirstName" class="float-left">Physician First Name:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "text required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="PhysicianNPI" class="float-left">Physician NPI #:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="ProspectivePay" class="float-left">HIPPS Code Payment:</label>
                <div class="float-right"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @class = "text", @maxlength = "20" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="strong">Diagnosis Codes:</div>
                <div class="margin">
                    <label for="Primary" class="float-left">Primary</label>
                    <div class="float-right"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Second" class="float-left">Second</label>
                    <div class="float-right"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Third" class="float-left">Third</label>
                    <div class="float-right"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Fourth" class="float-left">Fourth</label>
                    <div class="float-right"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Fifth" class="float-left">Fifth</label>
                    <div class="float-right"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Sixth" class="float-left">Sixth</label>
                    <div class="float-right"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : "")%></div>
                    <div class="clear"></div>
                </div>
                <div class="strong">UB04 Locator 81cc:</div>
                <div class="margin">
                    <label for="Ub04Locator81cca" class="float-left">a:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator1")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "Rap_Locator1_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "Rap_Locator1_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "Rap_Locator1_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccb" class="float-left">b:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator2")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "Rap_Locator2_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "Rap_Locator2_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "Rap_Locator2_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccc" class="float-left">c:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator3")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "Rap_Locator3_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "Rap_Locator3_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "Rap_Locator3_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator81ccd" class="float-left">d:</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator4")%>
                    <div class="float-left">
                        <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "Rap_Locator4_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "Rap_Locator4_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "Rap_Locator4_Code3", @class = "text input_wrapper ub0481cc", @maxlength = "12" })%>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="strong">UB04 Locator 39:</div>
                <div class="margin">
                    <label for="Ub04Locator39" class="float-left">a:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator1")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator1_Code1", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code1 : "61", new { @id = "Rap_39Locator1_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator1_Code2", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code2 : Model.CBSA.PadStringNumberWithDecimal(2), new { @id = "Rap_39Locator1_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">b:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator2")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator2_Code1", valueCodes.ContainsKey("39Locator2") ? valueCodes["39Locator2"].Code1 : string.Empty, new { @id = "Rap_39Locator2_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator2_Code2", valueCodes.ContainsKey("39Locator2") ? valueCodes["39Locator2"].Code2 : string.Empty, new { @id = "Rap_39Locator2_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">c:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator3")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator3_Code1", valueCodes.ContainsKey("39Locator3") ? valueCodes["39Locator3"].Code1 : string.Empty, new { @id = "Rap_39Locator3_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator3_Code2", valueCodes.ContainsKey("39Locator3") ? valueCodes["39Locator3"].Code2 : string.Empty, new { @id = "Rap_39Locator3_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                    <div class="clear"></div>
                    <label for="Ub04Locator39" class="float-left">d:</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator4")%>
                    <div class="float-left">
                        <%= Html.TextBox("39Locator4_Code1", valueCodes.ContainsKey("39Locator4") ? valueCodes["39Locator4"].Code1 : string.Empty, new { @id = "Rap_39Locator4_Code1", @class = "text sn", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator4_Code2", valueCodes.ContainsKey("39Locator4") ? valueCodes["39Locator4"].Code2 : string.Empty, new { @id = "Rap_39Locator4_Code2", @class = "text input_wrapper ub0481cc", @maxlength = "10" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide_column">
            <div class="row">
                <label for="RapRemark">Remark:</label>
                <%= Html.TextArea("Remark", Model.Remark, new {  }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify</a></li>
            <%= String.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId) %>
        </ul>
    </div>
    <%  } %>
</div>
<script type="text/javascript">
    if ($("#Rap_PrimaryInsuranceId").val() > 1000) {
        $("#Rap_MedicareHMOInformations").show();
        $("#Rap_MedicareHMOInformations input.hmoflag").addClass("required");
    } else {
        $("#Rap_MedicareHMOInformations").hide();
        $("#Rap_MedicareHMOInformations input.hmoflag").removeClass("required");
    }
    $("#Rap_PrimaryInsuranceId").change(function() {
        if ($(this).val() >= 1000) Billing.LoadInsuranceContent("#RAPHMOInfo_InsuranceContent", $("#Rap_PatientId").val(), $(this).val(), $("#Rap_EpisodeStartDate").val(), "<%=Model.EpisodeEndDate %>", "RAPHMOInfo");
        else $("#RAPHMOInfo_InsuranceContent").empty()
    });
</script>