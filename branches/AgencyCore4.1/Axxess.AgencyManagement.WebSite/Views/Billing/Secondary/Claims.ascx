﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>" %>

<div class='SecondaryClaims-FINAL-DetailGrid'>

    <%= Html.Telerik().Grid<SecondaryClaimLean>().HtmlAttributes(new { Style = "min-width:100px;" })
        .Name("SecondaryClaimsGrid")
             .DataKeys(keys =>
             {
                 keys.Add(r => r.Id).RouteKey("Id");
                 keys.Add(r => r.PatientId).RouteKey("PatientId");
             })
        .Columns(columns =>
        {
            columns.Bound(p => p.Id).Hidden(true);
            columns.Bound(o => o.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowSecondaryClaim('<#=Id#>');\">Open Claim</a>&nbsp;").Title("").Width(90);
            columns.Bound(o => o.ClaimRange).Title("Claim Range").Width(150).ReadOnly();
            columns.Bound(o => o.StatusName).Title("Status").Width(70);
            columns.Bound(p => p.Balance).Title("Balance").Format("${0:#0.00}").Width(90);
            columns.Bound(p => p.IsInfoVerified).Title("Details").ClientTemplate("<span class=\"img icon <#= IsInfoVerified ? 'success-small' : 'error-small' #>\">").Width(50);
            columns.Bound(p => p.IsInsuranceVerified).Title("Insurance").ClientTemplate("<span class=\"img icon <#= IsInsuranceVerified ? 'success-small' : 'error-small' #>\">").Width(75);
            columns.Bound(p => p.IsVisitVerified).Title("Visits").ClientTemplate("<span class=\"img icon <#= IsVisitVerified ? 'success-small' : 'error-small' #>\">").Width(50);
            columns.Bound(p => p.IsRemittanceVerified).Title("Remittance").ClientTemplate("<span class=\"img icon <#= IsRemittanceVerified ? 'success-small' : 'error-small' #>\">").Width(75);
            columns.Bound(p => p.IsSupplyVerified).Title("Supply").ClientTemplate("<span class=\"img icon <#= IsSupplyVerified ? 'success-small' : 'error-small' #>\">").Width(50);
            
            columns.Bound(p => p.PrintUrl).Title("").Width(120);
            columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalUpdateStatusSecondaryClaim('<#=Id#>');\">Update</a>&#160;|&#160;<a href=\"javascript:void(0);\" onclick=\"SecondaryBilling.DeleteClaim('<#=PatientId#>','<#=Id#>');\">Delete</a>").Title("Action").Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("SecondaryClaims", "Billing", new { patientId = Model.PatientId, primaryClaimId = Model.PrimaryClaimId }))
        .ClientEvents(events => events.OnRowSelected("SecondaryBilling.OnClaimRowSelected").OnDataBound("SecondaryBilling.OnCliamDataBound")).Selectable().Scrollable().Footer(false)%>
                

<script type="text/javascript"> $("#SecondaryClaimsGrid .t-grid-content").css({ height: 'auto',top:'26px','overflow-x': 'auto' });</script>