﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>" %>
<% using (Html.BeginForm("CreateSecondaryClaim", "Billing", FormMethod.Post, new { @id = "createSecondaryClaimForm" }))%>
<%  { %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("PrimaryClaimId", Model.PrimaryClaimId)%>
 <div class="form-wrapper">
      <fieldset>
        <legend>Claim Information</legend>
            <div class="row">
                <label class="float-left">Episode:</label>
                <div class="float-right"> <%= Model.EpisodeRange %></div>
            </div>
            <div class="row">
                <label class="float-left">Date Range:</label>
                <div class="float-right"> 
                    <input type="date" class="date-picker shortdate required" name="StartDate" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="StartDate" />
                    To
                    <input type="date" class="date-picker shortdate required" name="EndDate" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" value="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="EndDate" />
                </div>
            </div>
     </fieldset>
     <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add Secondary Claim</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<%} %>

<script type="text/javascript">
    
</script>