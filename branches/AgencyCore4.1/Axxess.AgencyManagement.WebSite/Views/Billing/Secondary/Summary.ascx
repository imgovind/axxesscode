﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<div class="wrapper main">
    <fieldset><%= Html.Hidden("Id", Model.Id)%>
        <div class="column">
            <div class="row"><div class="float-left">Patient First Name:</div><div class="float-right light"><%= Model.FirstName%></div></div>
            <div class="row"><div class="float-left">Patient Last Name:</div><div class="float-right light"><%= Model.LastName%></div></div>
            <div class="row"><div class="float-left">Medicare #:</div><div class="float-right light"><%= Model.IsuranceIdNumber%></div></div>
            <div class="row"><div class="float-left">Patient Record #:</div><div class="float-right light"><%= Model.PatientIdNumber%></div></div>
            <div class="row"><div class="float-left">Gender:</div><div class="float-right light"><%= Model.Gender%></div></div>
            <div class="row"><div class="float-left">Date of Birth:</div><div class="float-right light"><%= Model.DOB != null ? Model.DOB.ToString("MM/dd/yyyy") : string.Empty %></div></div>
        </div>
        <div class="column">
            <div class="row"><div class="float-left">Episode Start Date:</div><div class="float-right light"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Admission Date:</div><div class="float-right light"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Address Line 1:</div><div class="float-right light"><%= Model.AddressLine1%></div></div>
            <div class="row"><div class="float-left">Address Line 2:</div><div class="float-right light"><%= Model.AddressLine2%></div></div>
            <div class="row"><div class="float-left">City:</div><div class="float-right light"><%= Model.AddressCity%></div></div>
            <div class="row"><div class="float-left">State, Zip Code:</div><div class="float-right light"><%= Model.AddressStateCode + Model.AddressZipCode%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column"><% var total = 0.0; %>
            <div class="row"><div class="float-left">HIPPS Code:</div><div class="float-right light"><%= Model.HippsCode%></div></div>
            <div class="row"><div class="float-left">OASIS Matching Key:</div><div class="float-right light"><%= Model.ClaimKey%></div></div>
            <div class="row"><div class="float-left">Date Of First Billable Visit:</div><div class="float-right light"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Physician Last Name, F.I.:</div><div class="float-right light"><%= (Model.PhysicianLastName )+" "+ ( Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1)+"." : "")%></div></div>
            <div class="row"><div class="float-left">Physician NPI #:</div><div class="float-right light"><%= Model.PhysicianNPI%></div></div>
            <div class="row"><div>Remark:</div><div class="margin light"><p><%= Model.Remark%></p></div></div>
        </div>
        <div class="column">
            <div class="row"><% var diagnosis = XElement.Parse(Model.DiagnosisCode); var val = (diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : ""); %>
                <div>Diagonasis Codes:</div>
                <div class="margin">
                    <div class="float-left">Primary</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Second</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Third</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Fourth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Fifth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Sixth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="billing">
        <ul><li><span class="rap-checkbox"></span><span class="managed-description">Description</span><span class="visittype">HCPCS/HIPPS Code</span><span class="visitdate">Service Date</span><span class="icon-column">Service Unit</span><span class="icon-column">Total Charges</span></li></ul>
        <ol>
            <% if(Model != null && Model.IsHomeHealthServiceIncluded) { %>
                <li class="even">
                    <span class="rap-checkbox">0023</span>
                    <span class="managed-description">Home Health Services</span>
                    <span class="visittype"><%= Model.HippsCode %></span>
                    <span class="visitdate"><%= Model.FirstBillableVisitDate != DateTime.MinValue ? Model.FirstBillableVisitDate.ToZeroFilled() : string.Empty %></span>
                    <span class="icon-column"></span>
                    <span class="icon-column"></span>
                </li>
            <% } %>
              <% var i = Model != null && Model.IsHomeHealthServiceIncluded ? 1 : 0;
                if(Model!=null && Model.Supply.IsNotNullOrEmpty())
               {
                   var supplies = Model.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && !s.IsDeprecated).ToList() ?? new List<Supply>();
                  
                   if (supplies != null && supplies.Count > 0)
               {
                   foreach (var supply in supplies)
                   { %>
            <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
                  <span class="rap-checkbox"><%= supply.RevenueCode%></span>
                  <span class="managed-description"><%= supply.Description%></span>
                  <span class="visittype"><%= supply.Code%></span>
                  <span class="visitdate"><%= supply.DateForEdit.ToString("MM/dd/yyy")%></span>
                  <span class="icon-column"><%= supply.Quantity%> </span>
                  <span class="icon-column"><%= string.Format("${0:#0.00}", supply.TotalCost)%></span>
            </li>
            <% i++; total += supply.TotalCost;
                   }
               }
           }%>
           
            <%  
           if(Model!=null && Model.BillVisitSummaryDatas!=null && Model.BillVisitSummaryDatas.Count>0){
               foreach (var visit in Model.BillVisitSummaryDatas)
               { %>
            <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
                  <span class="rap-checkbox"><%= visit.RevenueCode %></span>
                  <span class="managed-description"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName%></span>
                  <span class="visittype"><% =string.Format("{0} {1} {2} {3} {4}", visit.HCPCSCode, visit.Modifier, visit.Modifier2,visit.Modifier3,visit.Modifier4)%></span>
                  <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "")%></span>
                  <span class="icon-column"><% = visit.Unit %> </span>
                  <span class="icon-column"><% = string.Format("${0:#0.00}", visit.Charge)%></span>
                <% if (visit.Adjustments != null && visit.Adjustments.Count > 0) { %>
                    <div>
                    <% int adjustmentCount = 0; 
                       foreach (var adjustment in visit.Adjustments) { %> 
                        <div>
                            <span class="rap-checkbox"></span>
                            <span class="managed-description strong">Adjustment <%= adjustmentCount + 1 %></span>
                            <span class="visittype">
                                <label for="AdjGroupCode" class="strong">Group Code:</label>
                                <label><%= adjustment.AdjGroup%></label>
                            </span>
                            <span style="width:18%;">
                                <label for="AdjReasonCode" class="strong">Reason Code:</label>
                                <label><%= adjustment.AdjData.AdjReason %></label>
                            </span>
                            <span class="icon-column"><%= String.Format("{0:C}", adjustment.AdjData.AdjAmount.ToNegativeDouble()) %></span>
                        </div>
                        
                        
                    <% total += adjustment.AdjData.AdjAmount.ToNegativeDouble();
                           adjustmentCount++;
                       } %>
                   </div>
                <% } %>
            
            <% i++; total +=  visit.Charge;
               }
           }%>
        </ol>
        <ul class="total"><li class="align-right"><label for="Total">Total:</label> <label> <%= string.Format("${0:#0.00}", total)%></label></li></ul>
        <%= Html.Hidden("Claim_Total", total, new { id = "Claim_Total" }) %>
    </div>
    <div class="buttons">
        <ul>
            <li>
                <a href="javascript:void(0);" onclick="SecondaryBilling.NavigateBack(4);">Back</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="SecondaryBilling.SecondaryClaimComplete('<%=Model.Id%>','<%=Model.PatientId%>');">Complete</a>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $("#SecondaryClaimTabStrip-6 ol li:first").addClass("first");
    $("#SecondaryClaimTabStrip-6 ol li:last").addClass("last");
</script>