﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reset Password - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="forgotPassword-wrapper">
        <div id="forgotpassword-window">
            <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Forgot Password</span></div>
            <div class="box">
                <div id="messages"></div>
                <div id="forgotPasswordFormContainer">
                    <% using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "forgotPasswordForm", @class = "forgotPassword" })) %>
                    <% { %>
                    <div class="row">
                        <label for="" class="fl">Email Address</label>
                        <div class="fr"><%= Html.TextBoxFor(a => a.EmailAddress, new { @class = "required" })%></div>
                    </div>
                    <div class="row">
                        <label for="" class="fl">Security Check</label>
                        <div class="fr">Enter both words below, separated by a space.</div>
                        <div class="clr"></div>
                        <div>
                            <script type="text/javascript">var RecaptchaOptions = { theme: "clean", tabindex : 0 }</script>
                            <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LemQrsSAAAAAM5b7-XCi9VUDVIu2dMG49YlEo4-"></script>
                            <noscript>
                                <iframe src="https://www.google.com/recaptcha/api/noscript?k=6LemQrsSAAAAAM5b7-XCi9VUDVIu2dMG49YlEo4-" width="500" height="300" frameborder="0"></iframe>
                                <br />
                                <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                                <input name="recaptcha_response_field" value="manual_challenge" type="hidden" />
                            </noscript>
                        </div> 
                    </div>
                    <div class="row tr">
                        <input type="submit" value="Send" class="button" style="width: 90px!important;" />
                        <br />
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(false)
         .DefaultGroup(group => group
             .Add("jquery-1.7.1.min.js")
             .Add("Plugins/Other/blockui.min.js")
             .Add("Plugins/Other/form.min.js")
             .Add("Plugins/Other/validate.min.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
             .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
             .Compress(true)
             .Combined(true)
             .CacheDurationInDays(1)
             .Version(Current.AssemblyVersion))
        .OnDocumentReady(() =>
        { 
    %>
    ResetPassword.Init();
    <% 
        }).Render(); %>
</body>
</html>
