﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div class="wrapper">
        <div class="main fl">
            <fieldset class="title">
                <legend class="no-text">Axxess</legend>
                <h1 class="no-text">AgencyCore</h1>
            </fieldset>
            <% var browser = HttpContext.Current.Request.Browser; %>
            <% if (AppSettings.BrowserCompatibilityCheck == "false" || browser.IsBrowserAllowed()) { %>
            <div class="login-box">
                <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login", @returnUrl = Request.QueryString["returnUrl"] })) { %>
                <div class="row">
                    <label for="Login_UserName" class="fl sm">Email</label>
                    <input id="Login_UserName" name="UserName" class="fr required" type="text" />
                </div>
                <div class="row">
                    <label for="Login_Password" class="fl sm">Password</label>
                    <input id="Login_Password" name="Password" class="fr required" type="password" />
                </div>
                <div class="row">
                    <div class="fr login-button upper sm">
                        Login
                        <span class="xl">&#187;</span>
                    </div>
                </div>
                <div class="row">
                    <input id="Login_RememberMe" name="RememberMe" checked="checked" value="true" type="checkbox" />
                    <label for="Login_RememberMe" class="sm">Remember Me</label>
                    |
                    <a class="sm" href="Forgot">Forgot your Password?</a>
                </div>
                <input id="Login_Button" type="submit" value="Login" />
                <% } %>
            </div>
            <% } else { %>
            <div class="login-box">
                <h4>Browser Compatibility Check</h4>
                <p>Your browser version does not meet our minimum browser requirements.</p>
                <p>Our software supports Internet Explorer 8 and FireFox 3+.</p>
                <p>Please download Internet Explorer 8 or Firefox by clicking on the links below.</p>
                <ul>
                    <li><a href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer Download">Download Internet Explorer</a></li>
                    <li><a href="http://www.mozilla.com/en-US/firefox" title="Firefox Download">Download Firefox</a></li>
                </ul>
            </div>
            <% } %>
            <noscript>
                <div id="login-box">
                    <h4>JavaScript Check</h4>
                    <p>Your browser version does not meet our minimum browser requirements.</p>
                    <p>Our software requires JavaScript to be enabled in your browser.</p>
                    <p>Please contact Axxess for assistance on how to enable JavaScript.</p>
                </div>
            </noscript> 
            <div class="adspace"><a href="http://axxessweb.com/referral_program" target="_blank"><img src="Images/login/referral.jpeg" /></a></div>
        </div>
        <div class="right fr">
            <div id="axxessEventsBox"></div>
            <script type="text/javascript" src="https://esign.axxessweb.com/assets/js/axxess_events_box.js"></script>
        </div>
        <div class="clr"></div>
        <div class="footer">Axxess Healthcare Consult | Copyright &#169; 2008 - <%= DateTime.Now.Year.ToString() %>. | All Rights Reserved. | Dallas, Texas</div>
    </div>
    <div id="Account_Modal_Container" class="agencySelection hidden" style="text-align: center;">
        <span id="accountInUseMessage">This user is already logged in on another computer. If you choose to proceed, the user will be automatically logged off the other computer and their work will not be saved.</span><br />
        Are you sure you want to continue?<br />
        <input id="Login_Continue_Button" type="submit" value="Continue" class="button" />
        <input id="Login_Cancel_Button" type="submit" value="Cancel" class="button" />
    </div>
    <div id="Agency_Selection_Container" class="agencySelection hidden"></div>
        <% Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Logon.Init(); 
        <% }).Render(); %>
    <% if (Model.IsLocked) { %>
    <script type="text/javascript">
        $("#Login_UserName").attr("disabled", "disabled");
        $("#Login_Password").attr("disabled", "disabled");
        $("#Login_RememberMe").hide();
        $("#Login_Forgot").hide();
        $("#Login_Button").hide();
        $("#messages").empty().removeClass().addClass("notification error").append('<span>You have made too many failed login attempts. Please contact your Agency/Companys Administrator.</span>');
    </script>
    <% } %>
</body>
</html>

