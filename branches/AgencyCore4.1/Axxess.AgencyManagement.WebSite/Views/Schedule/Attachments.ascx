﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<div class="wrapper main">
    <span class="bigtext align-center">Attachments: <%= Model.DisciplineTaskName %> | <%= Model.PatientName %></span>
    <fieldset>
        <legend></legend>
        <div class="wide-column">
            <div class="row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <span>There are <span id="scheduleEvent_Assest_Count"><%= Model.Assets.Count.ToString() %></span> attachment(s).</span>
                <div class="clear"></div>
            <%  foreach (Guid assetId in Model.Assets) { %>
                <span><%= Html.Asset(assetId) %></span>
                <div class="clear"></div>
            <%  } %>
                <br />
                <br />
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
</div>