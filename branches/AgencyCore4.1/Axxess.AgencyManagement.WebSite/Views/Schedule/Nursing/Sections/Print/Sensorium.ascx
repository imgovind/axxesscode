﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var orientationQuestions = new List<string>() { "Time", "Place", "Person" }; %>
<% var memoryQuestions = new List<string>() { "Clouding of Consciousness", "Inability to Concentrate", "Amnesia", "Poor Recent Memory", "Poor Remote Memory", "Conflabulation" }; %>
<% if (data.AnswerOrEmptyString("GenericIsSensoriumApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsSensoriumApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Sensorium"
<%}else{ %>
    printview.span("Orientation Impaired",true) + 
    <% int orientationCount = 1; %>
    <% foreach(string question in orientationQuestions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSensoriumOrientation" + orientationCount, possibleAnswers)%>")) +
    <% orientationCount++; %>
    <% } %> 
    printview.span("Memory",true) + 
    <% int memoryCount = 1; %>
    <% foreach(string question in memoryQuestions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSensoriumMemory" + memoryCount, possibleAnswers)%>")) +
    <% memoryCount++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSensoriumComment").Clean()%>"),
    "Sensorium"
<% } %>
