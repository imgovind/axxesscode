﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Increased Amount", "Decreased Amount", "Agitation", "Tics", "Tremor", "Peculiar Posturing", "Unusual Gait", "Repetitive Acts" }; %>
<% if (data.AnswerOrEmptyString("GenericIsMotorActivityApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsMotorActivityApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Motor Activity"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericMotorActivity" + count, possibleAnswers) %>")) +
    <% count++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericMotorActivityComment").Clean()%>"),
    "Motor Activity"
<% } %>
