﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] goals = data.AnswerArray("GenericGoals"); %>
<div id="<%= Model.Type %>_GoalsContainer">
    <table>
        <tr>    
            <td>
                <div class="column align-left">
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='1' name='{0}_GenericGoals' id='{0}_GenericGoals1' {1}>", Model.Type, goals.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals1">Make daily social contacts as evidenced by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals1Weeks' id='{0}_GenericGoals1Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals1Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals1Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='2' name='{0}_GenericGoals' id='{0}_GenericGoals2' {1}>", Model.Type, goals.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals2">Exhibit stable weight, nutrition, hydration status w/weight gain of </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals2Weight' id='{0}_GenericGoals2Weight' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals2Weight"))%>
                        <label for="<%= Model.Type %>_GenericGoals2Weight"> lbs by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals2Weeks' id='{0}_GenericGoals2Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals2Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals2Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='3' name='{0}_GenericGoals' id='{0}_GenericGoals3' {1}>", Model.Type, goals.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals3">Improve interpersonal relationships by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals3Weeks' id='{0}_GenericGoals3Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals3Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals3Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='4' name='{0}_GenericGoals' id='{0}_GenericGoals4' {1}>", Model.Type, goals.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals4">Demonstrate coping strategies by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals4Weeks' id='{0}_GenericGoals4Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals4Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals4Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='5' name='{0}_GenericGoals' id='{0}_GenericGoals5' {1}>", Model.Type, goals.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals5">Decrease neurotic behavior by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals5Weeks' id='{0}_GenericGoals5Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals5Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals5Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='6' name='{0}_GenericGoals' id='{0}_GenericGoals6' {1}>", Model.Type, goals.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals6">Verbalize a decrease in depression by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals6Weeks' id='{0}_GenericGoals6Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals6Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals6Weeks">wks.</label>
                    </div>
                        <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='7' name='{0}_GenericGoals' id='{0}_GenericGoals7' {1}>", Model.Type, goals.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals7">Verbalize absense of suicidal ideation, intent and plan by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals7Weeks' id='{0}_GenericGoals7Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals7Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals7Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='8' name='{0}_GenericGoals' id='{0}_GenericGoals8' {1}>", Model.Type, goals.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals8">Will not harm self as evidenced by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals8Weeks' id='{0}_GenericGoals8Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals8Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals8Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='9' name='{0}_GenericGoals' id='{0}_GenericGoals9' {1}>", Model.Type, goals.Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals9">Verbalize absense of violent ideation by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals9Weeks' id='{0}_GenericGoals9Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals9Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals9Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='10' name='{0}_GenericGoals' id='{0}_GenericGoals10' {1}>", Model.Type, goals.Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals10">Verbalize s/sx suicidality, crisis intervention, when to call physician/911 by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals10Weeks' id='{0}_GenericGoals10Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals10Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals10Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='11' name='{0}_GenericGoals' id='{0}_GenericGoals11' {1}>", Model.Type, goals.Contains("11").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals11">Exhibit elevated mood as evidenced by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals11Weeks' id='{0}_GenericGoals11Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals11Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals11Weeks">wks.</label>
                    </div>
                        <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='12' name='{0}_GenericGoals' id='{0}_GenericGoals12' {1}>", Model.Type, goals.Contains("12").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals12">Demonstrate 2 coping skills as evidenced by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals12Weeks' id='{0}_GenericGoals12Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals12Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals12Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='13' name='{0}_GenericGoals' id='{0}_GenericGoals13' {1}>", Model.Type, goals.Contains("13").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals13">Make daily social contacts as evidenced by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals13Weeks' id='{0}_GenericGoals13Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals13Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals13Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='14' name='{0}_GenericGoals' id='{0}_GenericGoals14' {1}>", Model.Type, goals.Contains("14").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals14">Exhibit goal directed thoughts as evidence by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals14Weeks' id='{0}_GenericGoals14Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals14Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals14Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='15' name='{0}_GenericGoals15' id='{0}_GenericGoals15' {1}>", Model.Type, goals.Contains("15").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals15">Maintain stable weight, nutrition, hydration status w/weight gain of </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals15Weight' id='{0}_GenericGoals15Weight' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals15Weight"))%>
                        <label for="<%= Model.Type %>_GenericGoals15Weight"> lbs by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals15Weeks' id='{0}_GenericGoals15Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals15Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals15Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='16' name='{0}_GenericGoals16' id='{0}_GenericGoals16' {1}>", Model.Type, goals.Contains("16").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals16">Achieve sx control of CV & CP status w/meds & relaxation skills AEB by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals16Weeks' id='{0}_GenericGoals16Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals16Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals16Weeks">wks.</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="column align-left">
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='17' name='{0}_GenericGoals' id='{0}_GenericGoals17' {1}>", Model.Type, goals.Contains("17").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals17">Achieve GI/GU managements as evidence by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals17Weeks' id='{0}_GenericGoals17Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals17Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals17Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='18' name='{0}_GenericGoals' id='{0}_GenericGoals18' {1}>", Model.Type, goals.Contains("18").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals18">Verbalize bowel management as evidence by: </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals18Weeks' id='{0}_GenericGoals18Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals18Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals18Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='19' name='{0}_GenericGoals' id='{0}_GenericGoals19' {1}>", Model.Type, goals.Contains("19").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals19">Verbalize and achieve symptom control of sleep disturbance of </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals19Noc' id='{0}_GenericGoals19Noc' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals19Noc"))%>
                        <label for="<%= Model.Type %>_GenericGoals19Weight"> h/noc by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals19Weeks' id='{0}_GenericGoals19Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals19Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals19Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='20' name='{0}_GenericGoals' id='{0}_GenericGoals20' {1}>", Model.Type, goals.Contains("20").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals20">Exhibit control of anxiety w/med & relaxation skills AEB by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals20Weeks' id='{0}_GenericGoals20Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals20Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals20Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='21' name='{0}_GenericGoals' id='{0}_GenericGoals21' {1}>", Model.Type, goals.Contains("21").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals21">Exhibit control of thought disorder as evidenced by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals21Weeks' id='{0}_GenericGoals21Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals21Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals21Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='22' name='{0}_GenericGoals' id='{0}_GenericGoals22' {1}>", Model.Type, goals.Contains("22").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals22">Achieve mobility/safety management as evidenced by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals22Weeks' id='{0}_GenericGoals22Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals22Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals22Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='23' name='{0}_GenericGoals' id='{0}_GenericGoals23' {1}>", Model.Type, goals.Contains("23").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals23">Achieve maximum level of self-care as evidenced by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals23Weeks' id='{0}_GenericGoals23Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals23Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals23Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='24' name='{0}_GenericGoals' id='{0}_GenericGoals24' {1}>", Model.Type, goals.Contains("24").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals24">Verbalized medications, use schedule & side effecs and patient will take as ordered by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals24Weeks' id='{0}_GenericGoals24Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals24Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals24Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='25' name='{0}_GenericGoals' id='{0}_GenericGoals25' {1}>", Model.Type, goals.Contains("25").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals25">Verbalize adequate knowledge of disease process & know when to notify physician by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals25Weeks' id='{0}_GenericGoals25Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals25Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals25Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='26' name='{0}_GenericGoals' id='{0}_GenericGoals26' {1}>", Model.Type, goals.Contains("26").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals26">97 Verbalize goal-directed thoughts, really based orientation & congruent thinking by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals26Weeks' id='{0}_GenericGoals26Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals26Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals26Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='27' name='{0}_GenericGoals' id='{0}_GenericGoals27' {1}>", Model.Type, goals.Contains("27").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals27">Exhibit decreased hyperactivity & safe behaviors as evidence by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals27Weeks' id='{0}_GenericGoals27Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals27Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals27Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='28' name='{0}_GenericGoals28' id='{0}_GenericGoals28' {1}>", Model.Type, goals.Contains("28").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals28">Refrain from boastful/delusional behaviors & from interrupting conversations by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals28Weeks' id='{0}_GenericGoals28Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals28Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals28Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='29' name='{0}_GenericGoals29' id='{0}_GenericGoals29' {1}>", Model.Type, goals.Contains("29").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals29">Demonstrate positive coping mechanisms & verbalize 2 realistic goals by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals29Weeks' id='{0}_GenericGoals29Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals29Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals29Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='30' name='{0}_GenericGoals30' id='{0}_GenericGoals30' {1}>", Model.Type, goals.Contains("30").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals30">Experience no untoward ECT complications by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals30Weeks' id='{0}_GenericGoals30Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals30Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals30Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='31' name='{0}_GenericGoals32' id='{0}_GenericGoals31' {1}>", Model.Type, goals.Contains("31").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals31">Verbalize 2 management techniques of disease by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals31Weeks' id='{0}_GenericGoals31Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals31Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals31Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='32' name='{0}_GenericGoals32' id='{0}_GenericGoals32' {1}>", Model.Type, goals.Contains("32").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals32">Verbalize 2 s/sx of illness by </label>
                        <%= string.Format("<input type='textbox' class='sn' value='{1}' name='{0}_GenericGoals32Weeks' id='{0}_GenericGoals32Weeks' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoals32Weeks"))%>
                        <label for="<%= Model.Type %>_GenericGoals32Weeks">wks.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input type='checkbox' class='radio' value='33' name='{0}_GenericGoals33' id='{0}_GenericGoals33' {1}>", Model.Type, goals.Contains("33").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericGoals33">Other</label>
                        <span class="more">
                            <%= string.Format("<input type='textbox' class='' value='{1}' name='{0}_GenericGoalsOther' id='{0}_GenericGoalsOther' {1}>", Model.Type, data.AnswerOrEmptyString("GenericGoalsOther"))%>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericGoalsComment" class="strong">Comments:</label>
                <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericGoalsComment", data.AnswerOrEmptyString("GenericGoalsComment"), new { @class = "fill", @id = Model.Type + "_GenericGoalsComment" })%></div>
            </td>
        </tr>
    </table>
</div>
<script>
    U.ShowIfChecked($("#<%= Model.Type %>_GenericGoals33"), $("#<%= Model.Type %>_GenericGoalsOther"))
</script>