﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var ss = Model.Type;%>
<%  var allergyProfile = (Model != null && Model.Allergies.IsNotNullOrEmpty()) ? Model.Allergies.ToObject<AllergyProfile>() : new AllergyProfile();
    allergyProfile.Type = Model.Type; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Newborn/Infant</th>
            <th>Vital Signs</th>
            <th colspan="2">Childhood History (H-history of; N-negative; P-present problem)</th>
        </tr>
        <tr>
            <td>
                <label class="float-left">Newborn screen results</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.Type + "_NewbornScreenResults", data.AnswerOrEmptyString("NewbornScreenResults"), new { @id = Model.Type + "_NewbornScreenResults" })%>
                </div>
                <div class="clear" />
                <div class="float-left">
                    <label>Gestational age at birth</label>
                    <%= Html.TextBox(Model.Type + "_GestationalAge", data.AnswerOrEmptyString("GestationalAge"), new { @id = Model.Type + "_GestationalAge", @class = "numeric sn" })%>
                    <label>weeks</label>
                    <div class="clear" />
                    <label>Birth wt.</label>
                    <%= Html.TextBox(Model.Type + "_BirthWeight", data.AnswerOrEmptyString("BirthWeight"), new { @id = Model.Type + "_BirthWeight", @class = "numeric sn" })%>
                    <label>lb.</label>
                    <%= Html.TextBox(Model.Type + "_BirthOz", data.AnswerOrEmptyString("BirthOz"), new { @id = Model.Type + "_BirthOz", @class = "numeric sn" })%>
                    <label>oz.</label>
                    <div class="clear" />
                    <label>Length</label>
                    <%= Html.TextBox(Model.Type + "_BirthLength", data.AnswerOrEmptyString("BirthLength"), new { @id = Model.Type + "_BirthLength", @class = "numeric sn" })%>
                    <label>in.</label>
                    <div class="clear" />
                    <label>Head circumference</label>
                    <%= Html.TextBox(Model.Type + "_HeadCircumference", data.AnswerOrEmptyString("HeadCircumference"), new { @id = Model.Type + "_HeadCircumference" })%>
                    
               </div> 
               <label class="float-left">Fontanels:</label>
               <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Fontanels0' name='{0}_Fontanels' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Fontanels").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_Fontanels0">Anterior</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Fontanels1' name='{0}_Fontanels' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Fontanels").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_Fontanels1">Posterior</label>
                            </div>
                        </li>
                    </ul>
               </div>
               <div class="clear" />
               <label class="float-left">Umbilicus:</label>
               <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Umbilicus0' name='{0}_Umbilicus' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Umbilicus").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_Umbilicus0">Healed</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Umbilicus1' name='{0}_Umbilicus' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Umbilicus").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_Umbilicus1">Hernia</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Umbilicus2' name='{0}_Umbilicus' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Umbilicus").Contains("2").ToChecked())%>
                                <label for="<%= Model.Type %>_Umbilicus2">Inverted</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Umbilicus3' name='{0}_Umbilicus' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Umbilicus").Contains("3").ToChecked())%>
                                <label for="<%= Model.Type %>_Umbilicus3">Everted</label>
                            </div>
                        </li>
                    </ul>
               </div>
            </td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
            <td>
                <label class="float-left">Thrush</label>
                <div class="float-right">
                    <%  var cHThrush = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHThrush", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHThrush", cHThrush, new { @id = Model.Type + "_CHThrush", @class = "oe" })%>
                </div>
                <div class="clear" />
                <label class="float-left">Apnea</label>
                <div class="float-right">
                    <%  var cHApnea = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHApnea", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHApnea", cHApnea, new { @id = Model.Type + "_CHApnea", @class = "oe" })%>
                </div>
                <div class="clear" />
                <label class="float-left">Conjunctivitis</label>
                <div class="float-right">
                    <%  var cHConjunctivitis = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHConjunctivitis", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHConjunctivitis", cHConjunctivitis, new { @id = Model.Type + "_CHConjunctivitis", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Croup</label>
                <div class="float-right">
                    <%  var cHCroup = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHCroup", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHCroup", cHCroup, new { @id = Model.Type + "_CHCroup", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Pica</label>
                <div class="float-right">
                    <%  var cHPica = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHPica", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHPica", cHPica, new { @id = Model.Type + "_CHPica", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Rubella</label>
                <div class="float-right">
                    <%  var cHRubella = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHRubella", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHRubella", cHRubella, new { @id = Model.Type + "_CHRubella", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Rubeola</label>
                <div class="float-right">
                    <%  var cHRubeola = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHRubeola", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHRubeola", cHRubeola, new { @id = Model.Type + "_CHRubeola", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Scarlet Fever</label>
                <div class="float-right">
                    <%  var cHScarlet = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHScarlet", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHScarlet", cHScarlet, new { @id = Model.Type + "_CHScarlet", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Mumps</label>
                <div class="float-right">
                    <%  var cHMumps = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHMumps", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHMumps", cHMumps, new { @id = Model.Type + "_CHMumps", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Chickenpox</label>
                <div class="float-right">
                    <%  var cHChickenpox = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHChickenpox", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHChickenpox", cHChickenpox, new { @id = Model.Type + "_CHChickenpox", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Hepatitis</label>
                <div class="float-right">
                    <%  var cHHepatitis = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHHepatitis", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHHepatitis", cHHepatitis, new { @id = Model.Type + "_CHHepatitis", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Sickle Cell</label>
                <div class="float-right">
                    <%  var cHSickleCell = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHSickleCell", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHSickleCell", cHSickleCell, new { @id = Model.Type + "_CHSickleCell", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Lead Poisoning</label>
                <div class="float-right">
                    <%  var cHLeadPoisoning = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHLeadPoisoning", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHLeadPoisoning", cHLeadPoisoning, new { @id = Model.Type + "_CHLeadPoisoning", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">HIV</label>
                <div class="float-right">
                    <%  var cHHIV = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHHIV", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHHIV", cHHIV, new { @id = Model.Type + "_CHHIV", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Pneumonia</label>
                <div class="float-right">
                    <%  var cHPneumonia = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHPneumonia", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHPneumonia", cHPneumonia, new { @id = Model.Type + "_CHPneumonia", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Asthma</label>
                <div class="float-right">
                    <%  var cHAsthma = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHAsthma", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHAsthma", cHAsthma, new { @id = Model.Type + "_CHAsthma", @class = "oe" })%>
                    
                </div>
            </td>
            <td>
                <label class="float-left">Strep throat</label>
                <div class="float-right">
                    <%  var cHStrep = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHStrep", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHStrep", cHStrep, new { @id = Model.Type + "_CHStrep", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Sinusitis</label>
                <div class="float-right">
                    <%  var cHSinusitis = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHSinusitis", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHSinusitis", cHSinusitis, new { @id = Model.Type + "_CHSinusitis", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Nosebleeds</label>
                <div class="float-right">
                    <%  var cHNosebleeds = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHNosebleeds", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHNosebleeds", cHNosebleeds, new { @id = Model.Type + "_CHNosebleeds", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Fracture(s)</label>
                <div class="float-right">
                    <%  var cHFractures = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHFractures", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHFractures", cHFractures, new { @id = Model.Type + "_CHFractures", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Burn(s)</label>
                <div class="float-right">
                    <%  var cHBurns = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHBurns", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHBurns", cHBurns, new { @id = Model.Type + "_CHBurns", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Otitis media</label>
                <div class="float-right">
                    <%  var cHOtitismedia = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHOtitismedia", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHOtitismedia", cHOtitismedia, new { @id = Model.Type + "_CHOtitismedia", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Frequent ear infection</label>
                <div class="float-right">
                    <%  var cHFrequentEar = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHFrequentEar", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHFrequentEar", cHFrequentEar, new { @id = Model.Type + "_CHFrequentEar", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Tonsillitis</label>
                <div class="float-right">
                    <%  var cHTonsillitis = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHTonsillitis", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHTonsillitis", cHTonsillitis, new { @id = Model.Type + "_CHTonsillitis", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Frequent sore throat</label>
                <div class="float-right"> 
                    <%  var cHFrequentSore = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHFrequentSore", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHFrequentSore", cHFrequentSore, new { @id = Model.Type + "_CHFrequentSore", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Bleeding problems</label>
                <div class="float-right">
                    <%  var cHBleedingProblems = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHBleedingProblems", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHBleedingProblems", cHBleedingProblems, new { @id = Model.Type + "_CHBleedingProblems", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Rheumatic fever</label>
                <div class="float-right">
                    <%  var cHRheumaticFever = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHRheumaticFever", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHRheumaticFever", cHRheumaticFever, new { @id = Model.Type + "_CHRheumaticFever", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Headaches</label>
                <div class="float-right">
                    <%  var cHHeadaches = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHHeadaches", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHHeadaches", cHHeadaches, new { @id = Model.Type + "_CHHeadaches", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Seizures-grand mal</label>
                <div class="float-right">
                    <%  var cHSeizuresGrand = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHSeizuresGrand", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHSeizuresGrand", cHSeizuresGrand, new { @id = Model.Type + "_CHSeizuresGrand", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Seizures-petit mal</label>
                <div class="float-right">
                    <%  var cHSeizuresPetit = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHSeizuresPetit", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHSeizuresPetit", cHSeizuresPetit, new { @id = Model.Type + "_CHSeizuresPetit", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <label class="float-left">Frequent colds</label>
                <div class="float-right">
                    <%  var cHFrequentColds = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHFrequentColds", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHFrequentColds", cHFrequentColds, new { @id = Model.Type + "_CHFrequentColds", @class = "oe" })%>
                    
                </div>
                <div class="clear" />
                <div class="float-left strong">Other:
                <%= Html.TextBox(Model.Type + "_ChildhoodHistoryOther", data.AnswerOrEmptyString("ChildhoodHistoryOther"), new { @id = Model.Type + "_ChildhoodHistoryOther",@class="oe" })%></div>
                <div class="float-right">
                    <%  var cHOther = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "H", Value = "0" },
                            new SelectListItem { Text = "N", Value = "1" },
                            new SelectListItem { Text = "P", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("CHOther", ""));%>
                    <%= Html.DropDownList(Model.Type + "_CHOther", cHOther, new { @id = Model.Type + "_CHOther", @class = "oe" })%>
                    
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="2">Allergies</th>
            
            <th>Eyes
                <%= string.Format("<input class='radio' id='{0}IsEyesApply' name='{0}_IsEyesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsEyesApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Nose/Throat/Mouth
                <%= string.Format("<input class='radio' id='{0}IsNoseApply' name='{0}_IsNoseApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsNoseApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            <td rowspan="3" colspan="2">
                <div class="buttons">
                <ul class="float-left">
                    <% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %><li><a href="javascript:void(0);" onclick="Allergy.Add('<%= allergyProfile.Id %>', '<%= Model.Type %>');">Add Allergy</a></li><% } %>
                    <li><a href="javascript:void(0);" onclick="Allergy.Refresh('<%= allergyProfile.Id %>','<%= Model.Type  %>');">Refresh Allergies</a></li>
                </ul>
                </div>
                <div class="clear"></div>
                <div id="<%= Model.Type %>_AllergyProfileList"><% Html.RenderPartial("~/Views/Patient/Allergies/List.ascx", allergyProfile); %></div>
            </td>
            
            <td>
                <div id="<%= Model.Type %>EyesContainer">
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes0' name='{0}_Eyes' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("0").ToChecked())%>
                            <label>Glasses</label>
                        </div>
                    </li>
                    <li>
                            <div class="option">
                            <%= string.Format("<input id='{0}_EyesCR' name='{0}_Eyes' value='CR' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("CR").ToChecked())%>
                            <label>Contacts R</label>
                            </div></li>
                            <li><div class="option">
                            <%= string.Format("<input id='{0}_EyesCL' name='{0}_Eyes' value='CL' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("CL").ToChecked())%>
                            <label>Contacts L</label>
                            </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_EyesPR' name='{0}_Eyes' value='PR' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("PR").ToChecked())%>
                            <label>Prosthesis R</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_EyesPL' name='{0}_Eyes' value='PL' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("PL").ToChecked())%>
                            <label>Prosthesis L</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes3' name='{0}_Eyes' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("3").ToChecked())%>
                            <label>Jaundice</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes4' name='{0}_Eyes' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("4").ToChecked())%>
                            <label>Blurred vision</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes5' name='{0}_Eyes' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("5").ToChecked())%>
                            <label>Legally blind</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes6' name='{0}_Eyes' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("6").ToChecked())%>
                            <label>Infections</label>
                        </div>
                        <div class="extra">
                            <%= Html.TextBox(Model.Type + "_EyesInfections", data.AnswerOrEmptyString("EyesInfections"), new { @id = Model.Type + "_EyesInfections" })%>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Eyes7' name='{0}_Eyes' value='7' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Eyes").Contains("7").ToChecked())%>
                            <label>Other</label>
                        </div>
                        <div class="extra">
                            (specify)<%= Html.TextBox(Model.Type + "_EyesOther", data.AnswerOrEmptyString("EyesOther"), new { @id = Model.Type + "_EyesOther" })%>
                        </div>
                    </li>
                </ul>
                </div>
            </td>
            <td>
                <div id="<%= Model.Type %>NoseContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose0' name='{0}_Nose' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("0").ToChecked())%>
                                <label>Congestio</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose1' name='{0}_Nose' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("1").ToChecked())%>
                                <label>Dysphagla</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose2' name='{0}_Nose' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("2").ToChecked())%>
                                <label>Hoarseness</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose3' name='{0}_Nose' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("3").ToChecked())%>
                                <label>Lesions</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose4' name='{0}_Nose' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("4").ToChecked())%>
                                <label>Sore throat</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose5' name='{0}_Nose' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("5").ToChecked())%>
                                <label>Masses/Tumors</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Nose6' name='{0}_Nose' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Nose").Contains("6").ToChecked())%>
                                <label>Palate intact</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Teeth present</label>
                    <div class="float-right">
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_Teeth0' name='{0}_Teeth' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Teeth").Contains("0").ToChecked())%>
                                    <label>Yes</label>
                                </div>
                            </li>
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_Teeth1' name='{0}_Teeth' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Teeth").Contains("1").ToChecked())%>
                                    <label>No</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clear" />
                    <label class="float-left">Oral hygiene practices</label>
                    <%= Html.TextBox(Model.Type + "_OralHygiene", data.AnswerOrEmptyString("OralHygiene"), new { @id = Model.Type + "_OralHygiene",@class="float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Dentist visits:frequency</label>
                    <%= Html.TextBox(Model.Type + "_DentistVisit", data.AnswerOrEmptyString("DentistVisit"), new { @id = Model.Type + "_DentistVisit", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Other</label>
                    <%= Html.TextBox(Model.Type + "_NoseOther", data.AnswerOrEmptyString("NoseOther"), new { @id = Model.Type + "_NoseOther", @class = "float-right" })%>
                    
                </div>
            </td>
        </tr>
        <tr>
            
            <th>Ears
                <%= string.Format("<input class='radio' id='{0}IsEarsApply' name='{0}_IsEarsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsEarsApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Head/Neck
                <%= string.Format("<input class='radio' id='{0}IsHeadApply' name='{0}_IsHeadApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsHeadApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            
            <td>
                <div id="<%= Model.Type %>EarContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarHOHR' name='{0}_Ear' value='HOHR' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("HOHR").ToChecked())%>
                                <label>HOH R</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarHOHL' name='{0}_Ear' value='HOHL' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("HOHL").ToChecked())%>
                                <label>HOH L</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarDeafR' name='{0}_Ear' value='DeafR' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("DeafR").ToChecked())%>
                                <label>Deaf R</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarDeafL' name='{0}_Ear' value='DeafL' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("DeafL").ToChecked())%>
                                <label>Deaf L</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarAidR' name='{0}_Ear' value='AidR' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("AidR").ToChecked())%>
                                <label>Hearing aid R</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarAidL' name='{0}_Ear' value='AidL' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("AidL").ToChecked())%>
                                <label>Hearing aid L</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Ear3' name='{0}_Ear' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("3").ToChecked())%>
                                <label>Vertigo</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Ear4' name='{0}_Ear' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("4").ToChecked())%>
                                <label>Tinnitus</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Infections</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarInfections0' name='{0}_EarInfections' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("EarInfections").Contains("0").ToChecked())%>
                                <label>Yes</label>
                            </div>
                            <div class="extra">
                                   Frequency:<%= Html.TextBox(Model.Type + "_EarInfectionsYes", data.AnswerOrEmptyString("EarInfectionsYes"), new { @id = Model.Type + "_EarInfectionsYes" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_EarInfections1' name='{0}_EarInfections' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("EarInfections").Contains("1").ToChecked())%>
                                <label>No</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Ear5' name='{0}_Ear' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("5").ToChecked())%>
                                <label>P.E. tubes present</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Ear6' name='{0}_Ear' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Ear").Contains("6").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EarOther", data.AnswerOrEmptyString("EarOther"), new { @id = Model.Type + "_EarOther" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <div id="<%= Model.Type %>HeadContainer">
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Head0' name='{0}_Head' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Head").Contains("0").ToChecked())%>
                                <label>Injuries/Wounds</label>
                            </div>
                            <div class="extra">
                                (specify)<%= Html.TextBox(Model.Type + "_HeadInjuries", data.AnswerOrEmptyString("HeadInjuries"), new { @id = Model.Type + "_HeadInjuries" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Head1' name='{0}_Head' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Head").Contains("1").ToChecked())%>
                                <label>Masses/Nodes</label>
                            </div>
                            <div class="extra">
                                <label class="float-left">Site</label>
                                <%= Html.TextBox(Model.Type + "_HeadMassesSite", data.AnswerOrEmptyString("HeadMassesSite"), new { @id = Model.Type + "_HeadMassesSite",@class="float-right" })%>
                                <div class="clear" />
                                <label class="float-left">Size</label>
                                <%= Html.TextBox(Model.Type + "_HeadMassesSize", data.AnswerOrEmptyString("HeadMassesSize"), new { @id = Model.Type + "_HeadMassesSize", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Head2' name='{0}_Head' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Head").Contains("2").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                (specify, incl.history)<%= Html.TextBox(Model.Type + "_HeadOther", data.AnswerOrEmptyString("HeadOther"), new { @id = Model.Type + "_HeadOther" })%>
                            </div>
                        </li>
                    </ul>        
                </div>
            </td>
        </tr>
        <tr>
            <th>Endocrine
                <%= string.Format("<input class='radio' id='{0}IsEndocrineApply' name='{0}_IsEndocrineApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsEndocrineApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Cardiovascular
                <%= string.Format("<input class='radio' id='{0}IsCardiovascularApply' name='{0}_IsCardiovascularApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsCardiovascularApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Respiratory
                <%= string.Format("<input class='radio' id='{0}IsRespiratoryApply' name='{0}_IsRespiratoryApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsRespiratoryApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Genitourinary
                <%= string.Format("<input class='radio' id='{0}IsGenitourinaryApply' name='{0}_IsGenitourinaryApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGenitourinaryApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            <td>
                <div id="<%=Model.Type %>EndocrineContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine0' name='{0}_Endocrine' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("0").ToChecked())%>
                                <label>Hypothyroidism</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine1' name='{0}_Endocrine' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("1").ToChecked())%>
                                <label>Hyperthyroidism</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine2' name='{0}_Endocrine' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("2").ToChecked())%>
                                <label>Fatigue</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine3' name='{0}_Endocrine' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("3").ToChecked())%>
                                <label>Intolerance to heat/cold</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine4' name='{0}_Endocrine' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("4").ToChecked())%>
                                <label>Diabetes</label>
                            </div>
                            <div class="extra">
                                Onset<input type="text" class="date-picker" name="<%= Model.Type %>_EndocrineDiabetesDate" value="<%= data.AnswerOrEmptyString("EndocrineDiabetesDate") %>" id="<%= Model.Type %>_EndocrineDiabetesDate" />
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine5' name='{0}_Endocrine' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("5").ToChecked())%>
                                <label>Diet/Oral control</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineDietControl", data.AnswerOrEmptyString("EndocrineDietControl"), new { @id = Model.Type + "_EndocrineDietControl", @class = "numeric sn float-left" })%>
                                <div class="float-right">
                                    <%  var dateUnitLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "months", Value = "months" },
                                            new SelectListItem { Text = "Years", Value = "Years" }
                                        }, "Value", "Text", data.AnswerOrDefault("DateUnitLevel", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_DateUnitLevel", dateUnitLevel, new { @id = Model.Type + "_DateUnitLevel", @class = "oe" })%>
                                </div>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine6' name='{0}_Endocrine' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("6").ToChecked())%>
                                <label>Med/dose/freq</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineMed", data.AnswerOrEmptyString("EndocrineMed"), new { @id = Model.Type + "_EndocrineMed" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine7' name='{0}_Endocrine' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("7").ToChecked())%>
                                <label>Insulin/dose/freq</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineInsulin", data.AnswerOrEmptyString("EndocrineInsulin"), new { @id = Model.Type + "_EndocrineInsulin" })%>
                            </div>
                        </li>
                        <div class="clear"></div>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine8' name='{0}_Endocrine' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("8").ToChecked())%>
                                <label>Hyperglycemia</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine9' name='{0}_Endocrine' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("9").ToChecked())%>
                                <label>Hypoglycemia</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine10' name='{0}_Endocrine' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("10").ToChecked())%>
                                <label>Blood sugar range</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineBlood", data.AnswerOrEmptyString("EndocrineBlood"), new { @id = Model.Type + "_EndocrineBlood" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine11' name='{0}_Endocrine' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("11").ToChecked())%>
                                <label>Self-care/self observational tasks(specify)</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineSelf", data.AnswerOrEmptyString("EndocrineSelf"), new { @id = Model.Type + "_EndocrineSelf" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Endocrine12' name='{0}_Endocrine' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("Endocrine").Contains("12").ToChecked())%>
                                <label>Other(specify incl. history)</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_EndocrineOther", data.AnswerOrEmptyString("EndocrineOther"), new { @id = Model.Type + "_EndocrineOther" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <div id="<%=Model.Type %>CardiovascularContainer">
                    <label class="float-left">Heart sounds:</label>
                    <ul class="checkgroup inline float-right">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_HeartSounds0' name='{0}_HeartSounds' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("HeartSounds").Contains("0").ToChecked())%>
                                <label>Reg.</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_HeartSounds1' name='{0}_HeartSounds' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("HeartSounds").Contains("1").ToChecked())%>
                                <label>Irreg.(specify)</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_HeartSoundsIrreg", data.AnswerOrEmptyString("HeartSoundsIrreg"), new { @id = Model.Type + "_HeartSoundsIrreg" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular0' name='{0}_Cardiovascular' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("0").ToChecked())%>
                                <label>Palpitations</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular1' name='{0}_Cardiovascular' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("1").ToChecked())%>
                                <label>Pulse deficit</label>
                            </div>
                            <div class="extra">
                                (specify)<%= Html.TextBox(Model.Type + "_CardiovascularPulseDeficit", data.AnswerOrEmptyString("CardiovascularPulseDeficit"), new { @id = Model.Type + "_CardiovascularPulseDeficit" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular2' name='{0}_Cardiovascular' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("2").ToChecked())%>
                                <label>Edema</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular3' name='{0}_Cardiovascular' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("3").ToChecked())%>
                                <label>JVD</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular4' name='{0}_Cardiovascular' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("4").ToChecked())%>
                                <label>Fatigue</label>
                            </div>
                        </li>
                        
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular5' name='{0}_Cardiovascular' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("5").ToChecked())%>
                                <label>Cyanosis(site)</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_CardiovascularCyanosis", data.AnswerOrEmptyString("CardiovascularCyanosis"), new { @id = Model.Type + "_CardiovascularCyanosis" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular6' name='{0}_Cardiovascular' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("6").ToChecked())%>
                                <label>Cap refill:</label>
                            </div>
                            <div class="extra float-right">
                                    <%  var genericCapLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Less than 3 sec", Value = "Less than 3 sec" },
                                            new SelectListItem { Text = "More than 3 sec", Value = "More than 3 sec" }
                                        }, "Value", "Text", data.AnswerOrDefault("CardiovascularCap", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_CardiovascularCap", genericCapLevel, new { @id = Model.Type + "_CardiovascularCap", @class = "oe" })%>
                                
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular7' name='{0}_Cardiovascular' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("7").ToChecked())%>
                                <label>Pulses:</label>
                            </div>
                            <div class="extra float-right">
                                    <%  var genericPulsesLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "LDP", Value = "LDP" },
                                            new SelectListItem { Text = "LPT", Value = "LPT" },
                                            new SelectListItem { Text = "RDP", Value = "RDP" },
                                            new SelectListItem { Text = "RPT", Value = "RPT" }
                                        }, "Value", "Text", data.AnswerOrDefault("CardiovascularPulses", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_CardiovascularPulses", genericPulsesLevel, new { @id = Model.Type + "_CardiovascularPulses", @class = "oe" })%>
                                
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Cardiovascular8' name='{0}_Cardiovascular' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Cardiovascular").Contains("8").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                (specify incl. history)<%= Html.TextBox(Model.Type + "_CardiovascularOther", data.AnswerOrEmptyString("CardiovascularOther"), new { @id = Model.Type + "_CardiovascularOther" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <div id="<%=Model.Type %>RespiratoryContainer">
                    <label class="float-left">Chest circumference</label>
                    <%= Html.TextBox(Model.Type + "_ChestCircumference", data.AnswerOrEmptyString("ChestCircumference"), new { @id = Model.Type + "_ChestCircumference", @class="float-right" })%>
                    <div class="clear" />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryChest0' name='{0}_RespiratoryChest' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChest").Contains("0").ToChecked())%>
                                <label>Retractions</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryChest1' name='{0}_RespiratoryChest' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChest").Contains("1").ToChecked())%>
                                <label>Dyspnea</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Breath sounds:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath0' name='{0}_RespiratoryBreath' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("0").ToChecked())%>
                                <label>Clear</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath1' name='{0}_RespiratoryBreath' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("1").ToChecked())%>
                                <label>Crackles</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath2' name='{0}_RespiratoryBreath' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("2").ToChecked())%>
                                <label>Wheeze</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath3' name='{0}_RespiratoryBreath' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("3").ToChecked())%>
                                <label>Absent</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath4' name='{0}_RespiratoryBreath' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("4").ToChecked())%>
                                <label>Cough:</label>
                            </div>
                            <div class="extra float-right">
                                    <%  var genericCoughLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Dry", Value = "Dry" },
                                            new SelectListItem { Text = "Acute", Value = "Acute" },
                                            new SelectListItem { Text = "Chronic", Value = "Chronic" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryCough", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_RespiratoryCough", genericCoughLevel, new { @id = Model.Type + "_RespiratoryCough", @class = "oe" })%>
                                
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratoryBreath5' name='{0}_RespiratoryBreath' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryBreath").Contains("5").ToChecked())%>
                                <label>Productive:</label>
                            </div>
                            <div class="extra float-right">
                                    <%  var genericProductiveLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Thick", Value = "Thick" },
                                            new SelectListItem { Text = "Thin", Value = "Thin" },
                                            new SelectListItem { Text = "Difficult", Value = "Difficult" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryProductive", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_RespiratoryProductive", genericProductiveLevel, new { @id = Model.Type + "_RespiratoryProductive", @class = "oe" })%>
                                    Color<%= Html.TextBox(Model.Type + "_RespiratoryProductiveColor", data.AnswerOrEmptyString("RespiratoryProductiveColor"), new { @id = Model.Type + "_RespiratoryProductiveColor" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Skin:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin0' name='{0}_RespiratorySkin' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("0").ToChecked())%>
                                <label>Temp.change</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin1' name='{0}_RespiratorySkin' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("1").ToChecked())%>
                                <label>Color change</label>
                            </div>
                            <div class="extra">
                                Specify:<%= Html.TextBox(Model.Type + "_RespiratorySkinSpecify", data.AnswerOrEmptyString("RespiratorySkinSpecify"), new { @id = Model.Type + "_RespiratorySkinSpecify" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin2' name='{0}_RespiratorySkin' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("2").ToChecked())%>
                                <label>Percussion</label>
                            </div>
                            <div class="extra float-right">
                                    <%  var genericPercussionLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Resonant", Value = "Resonant" },
                                            new SelectListItem { Text = "Tympanic", Value = "Tympanic" },
                                            new SelectListItem { Text = "Dull", Value = "Dull" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryPercussion", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_RespiratoryPercussion", genericPercussionLevel, new { @id = Model.Type + "_RespiratoryPercussion", @class = "oe" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin3' name='{0}_RespiratorySkin' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("3").ToChecked())%>
                                <label>Chart lobe</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_RespiratoryChart0' name='{0}_RespiratoryChart' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChart").Contains("0").ToChecked())%>
                                            <label>R</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_RespiratoryChart1' name='{0}_RespiratoryChart' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChart").Contains("1").ToChecked())%>
                                            <label>L</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_RespiratoryChart2' name='{0}_RespiratoryChart' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChart").Contains("2").ToChecked())%>
                                            <label>Lat.</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_RespiratoryChart3' name='{0}_RespiratoryChart' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChart").Contains("3").ToChecked())%>
                                            <label>Ant.</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_RespiratoryChart4' name='{0}_RespiratoryChart' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratoryChart").Contains("4").ToChecked())%>
                                            <label>Post.</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin4' name='{0}_RespiratorySkin' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("4").ToChecked())%>
                                <label>O2 Sat.</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_RespiratoryO2Sat", data.AnswerOrEmptyString("RespiratoryO2Sat"), new { @id = Model.Type + "_RespiratoryO2Sat" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin5' name='{0}_RespiratorySkin' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("5").ToChecked())%>
                                <label>O2 Use:</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_RespiratoryO2Use", data.AnswerOrEmptyString("RespiratoryO2Use"), new { @id = Model.Type + "_RespiratoryO2Use",@class="sn numeric" })%>L/min. by
                                <%  var genericO2UseLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Mask", Value = "Mask" },
                                            new SelectListItem { Text = "Nasal", Value = "Nasal" },
                                            new SelectListItem { Text = "Trach", Value = "Trach" },
                                            new SelectListItem { Text = "Gas", Value = "Gas" },
                                            new SelectListItem { Text = "Liquid", Value = "Liquid" },
                                            new SelectListItem { Text = "Concentrator", Value = "Concentrator" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryO2UseLevel", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_RespiratoryO2UseLevel", genericO2UseLevel, new { @id = Model.Type + "_RespiratoryO2UseLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RespiratorySkin6' name='{0}_RespiratorySkin' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("RespiratorySkin").Contains("6").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                (specify incl.history)<%= Html.TextBox(Model.Type + "_RespiratoryOther", data.AnswerOrEmptyString("RespiratoryOther"), new { @id = Model.Type + "_RespiratoryOther" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
                <div id="<%=Model.Type %>GenitourinaryContainer">
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Genitourinary0' name='{0}_Genitourinary' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Genitourinary").Contains("0").ToChecked())%>
                                <label>Diapers/day</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_GenitourinaryDiapers", data.AnswerOrEmptyString("GenitourinaryDiapers"), new { @id = Model.Type + "_GenitourinaryDiapers", @class="sn numeric float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Genitourinary1' name='{0}_Genitourinary' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Genitourinary").Contains("1").ToChecked())%>
                                <label>Toilet trained</label>
                            </div>
                            <div class="extra">
                                <div class="float-right">
                                    <%  var toiletTrainedTimeLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Day", Value = "Day" },
                                            new SelectListItem { Text = "Night", Value = "Night" },
                                            new SelectListItem { Text = "Both", Value = "Both" }
                                        }, "Value", "Text", data.AnswerOrDefault("ToiletTrainedTime", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_ToiletTrainedTime", toiletTrainedTimeLevel, new { @id = Model.Type + "_ToiletTrainedTime", @class = "oe" })%>
                                
                                    <%  var toiletTrainedUnitLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Bladder", Value = "Bladder" },
                                            new SelectListItem { Text = "Bowel", Value = "Bowel" },
                                            new SelectListItem { Text = "Both", Value = "Both" }
                                        }, "Value", "Text", data.AnswerOrDefault("ToiletTrainedUnit", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_ToiletTrainedUnit", toiletTrainedUnitLevel, new { @id = Model.Type + "_ToiletTrainedUnit", @class = "oe" })%>
                                </div>
                            </div>
                        </li>
                        <div class="clear" />
                        <div class="float-left">
                        Urine: Color<%= Html.TextBox(Model.Type + "_GenitourinaryColor", data.AnswerOrEmptyString("GenitourinaryColor"), new { @id = Model.Type + "_GenitourinaryColor" })%>
                        <br />Amt <%= Html.TextBox(Model.Type + "_GenitourinaryAmt", data.AnswerOrEmptyString("GenitourinaryAmt"), new { @id = Model.Type + "_GenitourinaryAmt" })%>
                        <br />Odor <%= Html.TextBox(Model.Type + "_GenitourinaryOdor", data.AnswerOrEmptyString("GenitourinaryOdor"), new { @id = Model.Type + "_GenitourinaryOdor" })%>
                        <br />Frequency <%= Html.TextBox(Model.Type + "_GenitourinaryFreq", data.AnswerOrEmptyString("GenitourinaryFreq"), new { @id = Model.Type + "_GenitourinaryFreq", @class="sn numeric" })%>
                        <div class="clear" />
                        <ul class="checkgroup inline float-right">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenitourinaryUrine0' name='{0}_GenitourinaryUrine' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitourinaryUrine").Contains("0").ToChecked())%>
                                    <label>Burning</label>
                                </div>
                            </li>
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_GenitourinaryUrine1' name='{0}_GenitourinaryUrine' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitourinaryUrine").Contains("1").ToChecked())%>
                                    <label>Itching</label>
                                </div>
                            </li>
                        </ul>
                        </div>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Genitourinary2' name='{0}_Genitourinary' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Genitourinary").Contains("2").ToChecked())%>
                                <label>Enuresis: </label>
                            </div>
                            <div class="extra">
                                bedtime ritual<%= Html.TextBox(Model.Type + "_GenitourinaryEnuresis", data.AnswerOrEmptyString("GenitourinaryEnuresis"), new { @id = Model.Type + "_GenitourinaryEnuresis" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Genitourinary3' name='{0}_Genitourinary' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Genitourinary").Contains("3").ToChecked())%>
                                <label>Catheter: </label>
                            </div>
                            <div class="extra">
                                type/brand <%= Html.TextBox(Model.Type + "_GenitourinaryCatheter", data.AnswerOrEmptyString("GenitourinaryCatheter"), new { @id = Model.Type + "_GenitourinaryCatheter" })%>
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GenitourinaryCatheterType0' name='{0}_GenitourinaryCatheterType' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitourinaryCatheterType").Contains("0").ToChecked())%>
                                            <label>Foley</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GenitourinaryCatheterType1' name='{0}_GenitourinaryCatheterType' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitourinaryCatheterType").Contains("1").ToChecked())%>
                                            <label>Straight catheter</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GenitourinaryCatheterType2' name='{0}_GenitourinaryCatheterType' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitourinaryCatheterType").Contains("2").ToChecked())%>
                                            <label>External</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Genitourinary4' name='{0}_Genitourinary' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Genitourinary").Contains("4").ToChecked())%>
                                <label>Other: </label>
                            </div>
                            <div class="extra">
                                (specify incl. pertinent history)<%= Html.TextBox(Model.Type + "_GenitourinaryOther", data.AnswerOrEmptyString("GenitourinaryOther"), new { @id = Model.Type + "_GenitourinaryOther" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th>Gastrointestinal
                <%= string.Format("<input class='radio' id='{0}IsGastrointestinalApply' name='{0}_IsGastrointestinalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGastrointestinalApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Neurological
                <%= string.Format("<input class='radio' id='{0}IsNeurologicalApply' name='{0}_IsNeurologicalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsNeurologicalApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Psychosocial
                <%= string.Format("<input class='radio' id='{0}IsPsychosocialApply' name='{0}_IsPsychosocialApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPsychosocialApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Immunizations</th>
            
        </tr>
        <tr>
            <td rowspan="5">
                <div id="<%=Model.Type %>GastrointestinalContainer">
                    <label class="float-left">Nutritional requirements for age (diet)</label>
                    <%= Html.TextArea(Model.Type + "_GastrointestinalRequirements", data.AnswerOrEmptyString("GastrointestinalRequirements"), new { @id = Model.Type + "_GastrointestinalRequirements", @class="fill" })%>
                    <label class="float-left">Meal patterns</label>
                    <%= Html.TextBox(Model.Type + "_GastrointestinalMealPattern", data.AnswerOrEmptyString("GastrointestinalMealPattern"), new { @id = Model.Type + "_GastrointestinalMealPattern", @class = "float-right" })%>
                    <label class="float-left">Eatting behaviors</label>
                    <%= Html.TextBox(Model.Type + "_GastrointestinalEatting", data.AnswerOrEmptyString("GastrointestinalEatting"), new { @id = Model.Type + "_GastrointestinalEatting", @class = "float-right" })%>
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GastrointestinalEatting0' name='{0}_GastrointestinalEatting' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalEatting").Contains("0").ToChecked())%>
                                <label>Eatting disorder:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline float-right">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalEattingDisorder0' name='{0}_GastrointestinalEattingDisorder' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalEattingDisorder").Contains("0").ToChecked())%>
                                            <label>Anorexia</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalEattingDisorder1' name='{0}_GastrointestinalEattingDisorder' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalEattingDisorder").Contains("1").ToChecked())%>
                                            <label>Bulimia</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalEattingDisorder2' name='{0}_GastrointestinalEattingDisorder' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalEattingDisorder").Contains("2").ToChecked())%>
                                            <label>Other</label>
                                        </div>
                                        <div class="extra">
                                            (specify)<%= Html.TextBox(Model.Type + "_EattingDisorderOther", data.AnswerOrEmptyString("EattingDisorderOther"), new { @id = Model.Type + "_EattingDisorderOther" })%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Appetite:</label>
                    <div class="float-right">
                        <%  var appetiteLevel = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Good", Value = "Good" },
                                new SelectListItem { Text = "Fair", Value = "Fair" },
                                new SelectListItem { Text = "Poor", Value = "Poor" }
                            }, "Value", "Text", data.AnswerOrDefault("AppetiteLevel", ""));%>
                        <%= Html.DropDownList(Model.Type + "_AppetiteLevel", appetiteLevel, new { @id = Model.Type + "_AppetiteLevel", @class = "oe" })%>
                    </div>
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GastrointestinalAppetite0' name='{0}_GastrointestinalAppetite' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalAppetite").Contains("0").ToChecked())%>
                                <label>Weight change</label>
                            </div>
                            <div class="extra">
                                <div class="inline">
                                    <%  var weightChangeLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Gain", Value = "Gain" },
                                            new SelectListItem { Text = "Loss", Value = "Loss" }
                                        }, "Value", "Text", data.AnswerOrDefault("WeightChangeLevel", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_WeightChangeLevel", weightChangeLevel, new { @id = Model.Type + "_WeightChangeLevel", @class = "oe" })%>
                                    <%= Html.TextBox(Model.Type + "_WeightChange", data.AnswerOrEmptyString("WeightChange"), new { @id = Model.Type + "_WeightChange", @class = "sn numeric" })%>
                                    lb. x<%= Html.TextBox(Model.Type + "_WeightChangeTime", data.AnswerOrEmptyString("WeightChangeTime"), new { @id = Model.Type + "_WeightChangeTime", @class = "sn numeric" })%>
                                    <%  var weightChangeTimeLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "wk", Value = "wk" },
                                        new SelectListItem { Text = "mo", Value = "mo" },
                                        new SelectListItem { Text = "yr", Value = "yr" }
                                    }, "Value", "Text", data.AnswerOrDefault("WeightChangeTimeLevel", ""));%>
                                    <%= Html.DropDownList(Model.Type + "_WeightChangeTimeLevel", weightChangeTimeLevel, new { @id = Model.Type + "_WeightChangeTimeLevel", @class = "oe" })%>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GastrointestinalAppetite1' name='{0}_GastrointestinalAppetite' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalAppetite").Contains("1").ToChecked())%>
                                <label>Increase fluids</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_IncreaseFluids", data.AnswerOrEmptyString("IncreaseFluids"), new { @id = Model.Type + "_IncreaseFluids", @class = "sn numeric" })%>amt
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GastrointestinalAppetite2' name='{0}_GastrointestinalAppetite' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalAppetite").Contains("2").ToChecked())%>
                                <label>Restrict fluids</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_RestrictFluids", data.AnswerOrEmptyString("RestrictFluids"), new { @id = Model.Type + "_RestrictFluids", @class = "sn numeric" })%>amt
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GastrointestinalAppetite3' name='{0}_GastrointestinalAppetite' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalAppetite").Contains("3").ToChecked())%>
                                <label>Nausea/Vomiting:</label>
                            </div>
                            <div class="extra">
                                Frequency<%= Html.TextBox(Model.Type + "_NauseaFrequency", data.AnswerOrEmptyString("NauseaFrequency"), new { @id = Model.Type + "_NauseaFrequency", @class = "sn numeric" })%>
                                amt<%= Html.TextBox(Model.Type + "_NauseaAmt", data.AnswerOrEmptyString("NauseaAmt"), new { @id = Model.Type + "_NauseaAmt", @class = "sn numeric" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Last BM:</label>
                    <div class="float-right">
                        <input type="text" class="date-picker" name="<%= Model.Type %>_LastBMDate" value="<%= data.AnswerOrEmptyString("LastBMDate") %>" id="<%= Model.Type %>_LastBMDate" />
                    </div>
                    <div class="clear" />
                    <label class="float-left">Usual frequency</label>
                    <%= Html.TextBox(Model.Type + "_GastrointestinalFrequency", data.AnswerOrEmptyString("GastrointestinalFrequency"), new { @id = Model.Type + "_GastrointestinalFrequency", @class = "sn numeric float-right" })%>
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Gastrointestinal0' name='{0}_Gastrointestinal' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Gastrointestinal").Contains("0").ToChecked())%>
                                <label>Diarrhea:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalDiarrhea0' name='{0}_GastrointestinalDiarrhea' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalDiarrhea").Contains("0").ToChecked())%>
                                            <label>Black/Watery</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalDiarrhea1' name='{0}_GastrointestinalDiarrhea' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalDiarrhea").Contains("1").ToChecked())%>
                                            <label>Less than 3x/day</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalDiarrhea2' name='{0}_GastrointestinalDiarrhea' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("GastrointestinalDiarrhea").Contains("2").ToChecked())%>
                                            <label>More than 3x/day</label>
                                        </div>
                                    </li>
                                </ul>
                                <%  var diarrheaLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Mucus", Value = "Mucus" },
                                    new SelectListItem { Text = "Pain", Value = "Pain" },
                                    new SelectListItem { Text = "Foul odor", Value = "Foul odor" },
                                    new SelectListItem { Text = "Frothy", Value = "Frothy" }
                                }, "Value", "Text", data.AnswerOrDefault("DiarrheaLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_DiarrheaLevel", diarrheaLevel, new { @id = Model.Type + "_DiarrheaLevel", @class = "oe" })%>
                                Amount:<%= Html.TextBox(Model.Type + "_DiarrheaAmount", data.AnswerOrEmptyString("DiarrheaAmount"), new { @id = Model.Type + "_DiarrheaAmount", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Gastrointestinal1' name='{0}_Gastrointestinal' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Gastrointestinal").Contains("1").ToChecked())%>
                                <label>Abnormal stools:</label>
                            </div>
                            <div class="extra">
                                <%  var abnormalStoolsLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Clay", Value = "Clay" },
                                    new SelectListItem { Text = "Tarry", Value = "Tarry" },
                                    new SelectListItem { Text = "Fresh blood", Value = "Fresh blood" }
                                }, "Value", "Text", data.AnswerOrDefault("AbnormalStoolsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_AbnormalStoolsLevel", abnormalStoolsLevel, new { @id = Model.Type + "_AbnormalStoolsLevel", @class = "oe" })%>
                                Describe:<%= Html.TextBox(Model.Type + "_AbnormalStoolsDescribe", data.AnswerOrEmptyString("AbnormalStoolsDescribe"), new { @id = Model.Type + "_AbnormalStoolsDescribe", @class = "oe" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Gastrointestinal2' name='{0}_Gastrointestinal' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Gastrointestinal").Contains("2").ToChecked())%>
                                <label>Constipation:</label>
                            </div>
                            <div class="extra">
                                <%  var constipationLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Chronic", Value = "Chronic" },
                                    new SelectListItem { Text = "Acute", Value = "Acute" },
                                    new SelectListItem { Text = "Occasional", Value = "Occasional" }
                                }, "Value", "Text", data.AnswerOrDefault("ConstipationLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ConstipationLevel", constipationLevel, new { @id = Model.Type + "_ConstipationLevel", @class = "oe inline" })%>
                                <ul class="checkgroup">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_Constipation0' name='{0}_Constipation' value='lax' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Constipation").Contains("lax").ToChecked())%>
                                            <label>Lax/enema use:</label>
                                        </div>
                                        <div class="extra">
                                            Type:<%= Html.TextBox(Model.Type + "_EnemaType", data.AnswerOrEmptyString("EnemaType"), new { @id = Model.Type + "_EnemaType" })%>
                                            Freq.:<%= Html.TextBox(Model.Type + "_EnemaFreq", data.AnswerOrEmptyString("EnemaFreq"), new { @id = Model.Type + "_EnemaFreq", @class = "sn numeric" })%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Gastrointestinal3' name='{0}_Gastrointestinal' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Gastrointestinal").Contains("3").ToChecked())%>
                                <label>Flatulence:</label>
                            </div>
                            <div class="extra">
                                Abdominal distention:
                                <%  var abdominalDistentionLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Cramping", Value = "Cramping" },
                                    new SelectListItem { Text = "Pain", Value = "Pain" }
                                }, "Value", "Text", data.AnswerOrDefault("AbdominalDistentionLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_AbdominalDistentionLevel", abdominalDistentionLevel, new { @id = Model.Type + "_AbdominalDistentionLevel", @class = "oe inline" })%>
                                Freq.:<%= Html.TextBox(Model.Type + "_AbdominalDistentionFreq", data.AnswerOrEmptyString("AbdominalDistentionFreq"), new { @id = Model.Type + "_AbdominalDistentionFreq", @class = "sn numeric" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Gastrointestinal4' name='{0}_Gastrointestinal' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Gastrointestinal").Contains("4").ToChecked())%>
                                <label>Impaction:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup float-right">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalImpaction0' name='{0}_GastrointestinalImpaction' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GastrointestinalImpaction").Contains("0").ToChecked())%>
                                            <label>Flatulence:</label>
                                        </div>
                                        <div class="extra">
                                            Freq.:<%= Html.TextBox(Model.Type + "_FlatulenceFreq", data.AnswerOrEmptyString("FlatulenceFreq"), new { @id = Model.Type + "_FlatulenceFreq", @class = "sn numeric" })%>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_GastrointestinalImpaction1' name='{0}_GastrointestinalImpaction' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GastrointestinalImpaction").Contains("1").ToChecked())%>
                                            <label>Ascites:</label>
                                        </div>
                                        <div class="extra">
                                            Girth:<%= Html.TextBox(Model.Type + "_AscitesGirth", data.AnswerOrEmptyString("AscitesGirth"), new { @id = Model.Type + "_AscitesGirth", @class = "sn numeric" })%>inches
                                            <%  var ascitesLevel = new SelectList(new[] {
                                                new SelectListItem { Text = "------", Value = "" },
                                                new SelectListItem { Text = "Firm", Value = "Firm" },
                                                new SelectListItem { Text = "Tender", Value = "Tender" }
                                            }, "Value", "Text", data.AnswerOrDefault("AscitesLevel", ""));%>
                                            <%= Html.DropDownList(Model.Type + "_AscitesLevel", ascitesLevel, new { @id = Model.Type + "_AscitesLevel", @class = "oe inline" })%>
                                            x<%= Html.TextBox(Model.Type + "_AscitesQuads", data.AnswerOrEmptyString("AscitesQuads"), new { @id = Model.Type + "_AscitesQuads", @class = "sn numeric" })%>quads
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Bowel sounds:</label>
                    <%  var bowelSoundsLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "Active", Value = "Active" },
                        new SelectListItem { Text = "Hyperactive", Value = "Hyperactive" }
                    }, "Value", "Text", data.AnswerOrDefault("BowelSoundsLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_BowelSoundsLevel", bowelSoundsLevel, new { @id = Model.Type + "_BowelSoundsLevel", @class = "oe inline" })%>
                    x<%= Html.TextBox(Model.Type + "_BowelSoundsQuads", data.AnswerOrEmptyString("BowelSoundsQuads"), new { @id = Model.Type + "_BowelSoundsQuads", @class = "sn numeric" })%>quads
                    <div class="clear" />
                    <label class="float-left">Colostomy:</label>
                    <%  var colostomyLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "Sigmoid", Value = "Sigmoid" },
                        new SelectListItem { Text = "Transverse", Value = "Transverse" }
                    }, "Value", "Text", data.AnswerOrDefault("ColostomyLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_ColostomyLevel", colostomyLevel, new { @id = Model.Type + "_ColostomyLevel", @class = "oe inline" })%>
                    Date<input type="text" class="date-picker" name="<%= Model.Type %>_ColostomyDate" value="<%= data.AnswerOrEmptyString("ColostomyDate") %>" id="<%= Model.Type %>_ColostomyDate" />
                    <%  var colostomyLevel2 = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "Rebound", Value = "Rebound" },
                        new SelectListItem { Text = "Hot", Value = "Hot" },
                        new SelectListItem { Text = "Red", Value = "Red" },
                        new SelectListItem { Text = "Discolored", Value = "Discolored" }
                    }, "Value", "Text", data.AnswerOrDefault("ColostomyLevel2", ""));%>
                    <%= Html.DropDownList(Model.Type + "_ColostomyLevel2", colostomyLevel2, new { @id = Model.Type + "_ColostomyLevel2", @class = "oe inline" })%>
                </div>
            </td>
            <td rowspan="5">
                <div id="<%=Model.Type %>NeurologicalContainer">
                    <label class="float-left">Reflexes: N-normal, A-abnormal, NA-not applicable</label>
                    <div class="halfOfTd">
                        <label class="float-left">Rooting</label>
                        <div class="float-right">
                        <%  var nEURooting = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEURooting", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEURooting", nEURooting, new { @id = Model.Type + "_NEURooting", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Sucking</label>
                        <div class="float-right">
                        <%  var nEUSucking = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUSucking", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUSucking", nEUSucking, new { @id = Model.Type + "_NEUSucking", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Orienting</label>
                        <div class="float-right">
                        <%  var nEUOrienting = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUOrienting", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUOrienting", nEUOrienting, new { @id = Model.Type + "_NEUOrienting", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Babinski's</label>
                        <div class="float-right">
                        <%  var nEUBabinski = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUBabinski", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUBabinski", nEUBabinski, new { @id = Model.Type + "_NEUBabinski", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Blinking</label>
                        <div class="float-right">
                        <%  var nEUBlinking = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUBlinking", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUBlinking", nEUBlinking, new { @id = Model.Type + "_NEUBlinking", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Palmar</label>
                        <div class="float-right">
                        <%  var nEUPalmar = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUPalmar", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUPalmar", nEUPalmar, new { @id = Model.Type + "_NEUPalmar", @class = "shortOption" })%>
                        </div>
                    </div>
                    <div class="halfOfTd">
                        <label class="float-left">Plantar</label>
                        <div class="float-right">
                        <%  var nEUPlantar = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUPlantar", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUPlantar", nEUPlantar, new { @id = Model.Type + "_NEUPlantar", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Stepping/Dancing</label>
                        <div class="float-right">
                        <%  var nEUSteppingDancing = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUSteppingDancing", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUSteppingDancing", nEUSteppingDancing, new { @id = Model.Type + "_NEUSteppingDancing", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Moro's/Startle</label>
                        <div class="float-right">
                        <%  var nEUMoros = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUMoros", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUMoros", nEUMoros, new { @id = Model.Type + "_NEUMoros", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Tonic neck</label>
                        <div class="float-right">
                        <%  var nEUTonicneck = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUTonicneck", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUTonicneck", nEUTonicneck, new { @id = Model.Type + "_NEUTonicneck", @class = "shortOption" })%>
                        </div>
                        <div class="clear" />
                        <label class="float-left">Knee jerk</label>
                        <div class="float-right">
                        <%  var nEUKneejerk = new SelectList(new[] {
                            new SelectListItem { Text = "----", Value = "" },
                            new SelectListItem { Text = "N", Value = "0" },
                            new SelectListItem { Text = "A", Value = "1" },
                            new SelectListItem { Text = "NA", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("NEUKneejerk", ""));%>
                        <%= Html.DropDownList(Model.Type + "_NEUKneejerk", nEUKneejerk, new { @id = Model.Type + "_NEUKneejerk", @class = "shortOption" })%>
                        </div>
                    </div>
                    <div class="clear" />
                    Other(list with results)<%= Html.TextBox(Model.Type + "_ReflexOther", data.AnswerOrEmptyString("ReflexOther"), new { @id = Model.Type + "_ReflexOther" })%>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalOrient0' name='{0}_NeurologicalOrient' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalOrient").Contains("0").ToChecked())%>
                                <label>Oriented</label>
                            </div>
                            <div class="extra">
                                x<%= Html.TextBox(Model.Type + "_NeurologicalOrientContent", data.AnswerOrEmptyString("NeurologicalOrientContent"), new { @id = Model.Type + "_NeurologicalOrientContent", @class="oe" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalOrient1' name='{0}_NeurologicalOrient' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalOrient").Contains("1").ToChecked())%>
                                <label>Disoriented</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    Cognitive development problems:
                    <div class="clear" />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive0' name='{0}_NeurologicalCognitive' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("0").ToChecked())%>
                                <label>Concepts</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive1' name='{0}_NeurologicalCognitive' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("1").ToChecked())%>
                                <label>Logic</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive2' name='{0}_NeurologicalCognitive' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("2").ToChecked())%>
                                <label>Impaired decision-making ability</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive3' name='{0}_NeurologicalCognitive' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("3").ToChecked())%>
                                <label>Memory loss:</label>
                            </div>
                            <div class="extra">
                                <%  var memoryLossLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Short term", Value = "Short term" },
                                        new SelectListItem { Text = "Long term", Value = "Long term" }
                                    }, "Value", "Text", data.AnswerOrDefault("MemoryLossLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_MemoryLossLevel", memoryLossLevel, new { @id = Model.Type + "_MemoryLossLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive4' name='{0}_NeurologicalCognitive' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("4").ToChecked())%>
                                <label>Stuporous/Hallucinations:</label>
                            </div>
                            <div class="extra">
                                <%  var stuporousLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Visual", Value = "Visual" },
                                        new SelectListItem { Text = "Auditory", Value = "Auditory" }
                                    }, "Value", "Text", data.AnswerOrDefault("StuporousLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_StuporousLevel", stuporousLevel, new { @id = Model.Type + "_StuporousLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_NeurologicalCognitive5' name='{0}_NeurologicalCognitive' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalCognitive").Contains("5").ToChecked())%>
                                <label>Headache:</label>
                            </div>
                            <div class="extra">
                                Location:<%= Html.TextBox(Model.Type + "_HeadacheLocation", data.AnswerOrEmptyString("HeadacheLocation"), new { @id = Model.Type + "_HeadacheLocation" })%>
                                Freq.:<%= Html.TextBox(Model.Type + "_HeadacheFreq", data.AnswerOrEmptyString("HeadacheFreq"), new { @id = Model.Type + "_HeadacheFreq",@class="sn numeric" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Infant motor skills:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_InfantMotor0' name='{0}_InfantMotor' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("InfantMotor").Contains("0").ToChecked())%>
                                <label>Lifts head</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_InfantMotor1' name='{0}_InfantMotor' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("InfantMotor").Contains("1").ToChecked())%>
                                <label>Crawls/creeps</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Rolls over:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RollsOver0' name='{0}_RollsOver' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("RollsOver").Contains("0").ToChecked())%>
                                <label>Stomach to back</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_RollsOver1' name='{0}_RollsOver' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("RollsOver").Contains("1").ToChecked())%>
                                <label>Back to stomach</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Sits:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Sits0' name='{0}_Sits' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Sits").Contains("0").ToChecked())%>
                                <label>With assistance</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Sits1' name='{0}_Sits' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Sits").Contains("1").ToChecked())%>
                                <label>Without assistance</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Stands:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Stands0' name='{0}_Stands' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Stands").Contains("0").ToChecked())%>
                                <label>With assistance</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Stands1' name='{0}_Stands' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Stands").Contains("1").ToChecked())%>
                                <label>Without assistance</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Motor Skills:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills0' name='{0}_MotorSkills' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("0").ToChecked())%>
                                <label>Walks</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills1' name='{0}_MotorSkills' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("1").ToChecked())%>
                                <label>Runs</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills2' name='{0}_MotorSkills' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("2").ToChecked())%>
                                <label>Jumps</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills3' name='{0}_MotorSkills' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("3").ToChecked())%>
                                <label>Hops</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills4' name='{0}_MotorSkills' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("4").ToChecked())%>
                                <label>Skips</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills5' name='{0}_MotorSkills' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("5").ToChecked())%>
                                <label>Balance</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills6' name='{0}_MotorSkills' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("6").ToChecked())%>
                                <label>Motor change:</label>
                            </div>
                            <div class="extra inline">
                                <%  var motorChangeLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Fine", Value = "Fine" },
                                        new SelectListItem { Text = "Gross", Value = "Gross" }
                                    }, "Value", "Text", data.AnswerOrDefault("MotorChangeLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_MotorChangeLevel", motorChangeLevel, new { @id = Model.Type + "_MotorChangeLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills7' name='{0}_MotorSkills' value='7' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("7").ToChecked())%>
                                <label>Tremors:</label>
                            </div>
                            <div class="extra inline">
                                <%  var tremorsLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Fine", Value = "Fine" },
                                        new SelectListItem { Text = "Gross", Value = "Gross" },
                                        new SelectListItem { Text = "Paralysis", Value = "Paralysis" }
                                    }, "Value", "Text", data.AnswerOrDefault("TremorsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_TremorsLevel", tremorsLevel, new { @id = Model.Type + "_TremorsLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_MotorSkills8' name='{0}_MotorSkills' value='8' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MotorSkills").Contains("8").ToChecked())%>
                                <label>Weakness:</label>
                            </div>
                            <div class="extra inline">
                                <%  var weaknessLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "UE", Value = "UE" },
                                        new SelectListItem { Text = "LE", Value = "LE" }
                                    }, "Value", "Text", data.AnswerOrDefault("WeaknessLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_WeaknessLevel", weaknessLevel, new { @id = Model.Type + "_WeaknessLevel", @class = "oe" })%>
                                Location:<%= Html.TextBox(Model.Type + "_WeaknessLocation", data.AnswerOrEmptyString("WeaknessLocation"), new { @id = Model.Type + "_WeaknessLocation" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Hand grips</label>
                    <div class="float-right">
                        <%  var handGripsLevel = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Equal", Value = "Equal" },
                                new SelectListItem { Text = "Unequal", Value = "Unequal" }
                            }, "Value", "Text", data.AnswerOrDefault("HandGripsLevel", ""));%>
                        <%= Html.DropDownList(Model.Type + "_HandGripsLevel", handGripsLevel, new { @id = Model.Type + "_HandGripsLevel", @class = "oe" })%>
                        Specify:<%= Html.TextBox(Model.Type + "_HandGripsSpecify1", data.AnswerOrEmptyString("HandGripsSpecify1"), new { @id = Model.Type + "_HandGripsSpecify1" })%>
                        <br />
                        <%  var handGripsLevel2 = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Strong", Value = "Strong" },
                                new SelectListItem { Text = "Weak", Value = "Weak" }
                            }, "Value", "Text", data.AnswerOrDefault("HandGripsLevel2", ""));%>
                        <%= Html.DropDownList(Model.Type + "_HandGripsLevel2", handGripsLevel2, new { @id = Model.Type + "_HandGripsLevel2", @class = "oe" })%>
                        Specify:<%= Html.TextBox(Model.Type + "_HandGripsSpecify2", data.AnswerOrEmptyString("HandGripsSpecify2"), new { @id = Model.Type + "_HandGripsSpecify2" })%>
                        <br />
                        <ul class="checkgroup inline">
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_HandGrips0' name='{0}_HandGrips' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("HandGrips").Contains("0").ToChecked())%>
                                    <label>Sensory loss</label>
                                </div>
                                <div class="extra">
                                    (specify):<%= Html.TextBox(Model.Type + "_HandGripsSpecify3", data.AnswerOrEmptyString("HandGripsSpecify3"), new { @id = Model.Type + "_HandGripsSpecify3" })%>
                                </div>
                            </li>
                            <li>
                                <div class="option">
                                    <%= string.Format("<input id='{0}_HandGrips1' name='{0}_HandGrips' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("HandGrips").Contains("1").ToChecked())%>
                                    <label>Numbness</label>
                                </div>
                                <div class="extra">
                                    (specify):<%= Html.TextBox(Model.Type + "_HandGripsSpecify4", data.AnswerOrEmptyString("HandGripsSpecify4"), new { @id = Model.Type + "_HandGripsSpecify4" })%>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clear" />
                    <label class="float-left">Communication patterns/Ability</label>
                    <%= Html.TextBox(Model.Type + "_CommunicationPatternsDescription", data.AnswerOrEmptyString("CommunicationPatternsDescription"), new { @id = Model.Type + "_CommunicationPatternsDescription", @class = "float-right" })%>
                    <div class="clear" />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CommunicationPatterns0' name='{0}_CommunicationPatterns' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CommunicationPatterns").Contains("0").ToChecked())%>
                                <label>Unequal pupils</label>
                            </div>
                            <div class="extra">
                                <%  var unequalPupilLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "R", Value = "R" },
                                        new SelectListItem { Text = "L", Value = "L" },
                                        new SelectListItem { Text = "Perrla", Value = "Perrla" }
                                    }, "Value", "Text", data.AnswerOrDefault("UnequalPupilLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_UnequalPupilLevel", unequalPupilLevel, new { @id = Model.Type + "_UnequalPupilLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <li>
                             <div class="option">
                                <%= string.Format("<input id='{0}_CommunicationPatterns1' name='{0}_CommunicationPatterns' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CommunicationPatterns").Contains("1").ToChecked())%>
                                <label>Psychotropic drug use</label>
                            </div>
                            <div class="extra">
                                Specify:<%= Html.TextBox(Model.Type + "_PsychotropicDrugUse", data.AnswerOrEmptyString("PsychotropicDrugUse"), new { @id = Model.Type + "_PsychotropicDrugUse" })%>
                                Dose/Freq.:<%= Html.TextBox(Model.Type + "_PsychotropicDrugUseUnit", data.AnswerOrEmptyString("PsychotropicDrugUseUnit"), new { @id = Model.Type + "_PsychotropicDrugUseUnit", @class="sn numeric" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_CommunicationPatterns2' name='{0}_CommunicationPatterns' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CommunicationPatterns").Contains("2").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                Specify:<%= Html.TextBox(Model.Type + "_CommunicationPatternsOther", data.AnswerOrEmptyString("CommunicationPatternsOther"), new { @id = Model.Type + "_CommunicationPatternsOther", @class = "oe" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td rowspan="5">
                <div id="<%=Model.Type %>PsychosocialContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial0' name='{0}_Psychosocial' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("0").ToChecked())%>
                                <label>Angry</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial1' name='{0}_Psychosocial' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("1").ToChecked())%>
                                <label>Flat affect</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial2' name='{0}_Psychosocial' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("2").ToChecked())%>
                                <label>Discouraged</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial3' name='{0}_Psychosocial' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("3").ToChecked())%>
                                <label>Withdrawn</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial4' name='{0}_Psychosocial' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("4").ToChecked())%>
                                <label>Difficulty coping</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial5' name='{0}_Psychosocial' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("5").ToChecked())%>
                                <label>Disorganized</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial6' name='{0}_Psychosocial' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("6").ToChecked())%>
                                <label>Recent family change:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup innline">
                                    <li><div class="option">
                                        <%= string.Format("<input id='{0}_RecentChange0' name='{0}_RecentChange' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("0").ToChecked())%>
                                        <label>Birth</label></div>
                                    </li>
                                    <li><div class="option">
                                        <%= string.Format("<input id='{0}_RecentChange1' name='{0}_RecentChange' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("1").ToChecked())%>
                                        <label>Death</label></div>
                                    </li>
                                    <li><div class="option">
                                        <%= string.Format("<input id='{0}_RecentChange2' name='{0}_RecentChange' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("2").ToChecked())%>
                                        <label>Moved</label></div>
                                    </li>
                                    <li><div class="option">
                                        <%= string.Format("<input id='{0}_RecentChange3' name='{0}_RecentChange' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("3").ToChecked())%>
                                        <label>Divorce</label></div>
                                    </li>
                                    <li><div class="option">
                                        <%= string.Format("<input id='{0}_RecentChange4' name='{0}_RecentChange' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("4").ToChecked())%>
                                        <label>Other</label></div>
                                        <div class="extra">
                                            (specify)<%= Html.TextBox(Model.Type + "_RecentChangeOther", data.AnswerOrEmptyString("RecentChangeOther"), new { @id = Model.Type + "_RecentChangeOther" })%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial7' name='{0}_Psychosocial' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("7").ToChecked())%>
                                <label>Suicidal:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialSui0' name='{0}_PsychosocialSui' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSui").Contains("0").ToChecked())%>
                                            <label>Ideation</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialSui1' name='{0}_PsychosocialSui' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSui").Contains("1").ToChecked())%>
                                            <label>Verbalized</label>
                                        </div>
                                    </li>
                                </ul>
                                
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial8' name='{0}_Psychosocial' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("8").ToChecked())%>
                                <label>Depressed:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialDepressed0' name='{0}_PsychosocialDepressed' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialDepressed").Contains("0").ToChecked())%>
                                            <label>Recent</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialDepressed1' name='{0}_PsychosocialDepressed' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialDepressed").Contains("1").ToChecked())%>
                                            <label>Long term</label>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear" />
                                Due to<%= Html.TextBox(Model.Type + "_DepressedReason", data.AnswerOrEmptyString("DepressedReason"), new { @id = Model.Type + "_DepressedReason" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial9' name='{0}_Psychosocial' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("9").ToChecked())%>
                                <label>Substance use:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialSubstance0' name='{0}_PsychosocialSubstance' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("0").ToChecked())%>
                                            <label>Drugs</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialSubstance1' name='{0}_PsychosocialSubstance' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("1").ToChecked())%>
                                            <label>Alcohol</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialSubstance2' name='{0}_PsychosocialSubstance' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("2").ToChecked())%>
                                            <label>Tobacco</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Psychosocial10' name='{0}_Psychosocial' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Psychosocial").Contains("10").ToChecked())%>
                                <label>Evidence of abuse:</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialAbuse0' name='{0}_PsychosocialAbuse' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("0").ToChecked())%>
                                            <label>Potential</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialAbuse1' name='{0}_PsychosocialAbuse' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("1").ToChecked())%>
                                            <label>Actual</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialAbuse2' name='{0}_PsychosocialAbuse' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("2").ToChecked())%>
                                            <label>Verbal/Emotional</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialAbuse3' name='{0}_PsychosocialAbuse' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("3").ToChecked())%>
                                            <label>Physical</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_PsychosocialAbuse4' name='{0}_PsychosocialAbuse' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("4").ToChecked())%>
                                            <label>Financial</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Describe findings:</label>
                    <%= Html.TextArea(Model.Type + "_PsychosocialFindings", data.AnswerOrEmptyString("PsychosocialFindings"), new { @id = Model.Type + "_PsychosocialFindings", @class = "fill" })%>
                    <div class="clear" />
                    <label class="float-left">Describe relationships with the following:</label>
                    <div class="clear" />
                    Parents:<%= Html.TextBox(Model.Type + "_RelationshipWithParents", data.AnswerOrEmptyString("RelationshipWithParents"), new { @id = Model.Type + "_RelationshipWithParents" })%>
                    <div class="clear" />
                    Siblings:<%= Html.TextBox(Model.Type + "_RelationshipWithSiblings", data.AnswerOrEmptyString("RelationshipWithSiblings"), new { @id = Model.Type + "_RelationshipWithSiblings" })%>
                    <div class="clear" />
                    Peers:<%= Html.TextBox(Model.Type + "_RelationshipWithPeers", data.AnswerOrEmptyString("RelationshipWithPeers"), new { @id = Model.Type + "_RelationshipWithPeers" })%>
                    <div class="clear" />
                    <label class="float-left">Child care arrangements:</label>
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_ChildCareArran0' name='{0}_ChildCareArran' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ChildCareArran").Contains("0").ToChecked())%>
                                <label>Daycare</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_ChildCareArran1' name='{0}_ChildCareArran' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ChildCareArran").Contains("1").ToChecked())%>
                                <label>Home</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_ChildCareArran2' name='{0}_ChildCareArran' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ChildCareArran").Contains("2").ToChecked())%>
                                <label>Private sitter</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_ChildCareArran3' name='{0}_ChildCareArran' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ChildCareArran").Contains("3").ToChecked())%>
                                <label>Family member</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_ChildCareArran4' name='{0}_ChildCareArran' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ChildCareArran").Contains("4").ToChecked())%>
                                <label>Other:</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextBox(Model.Type + "_ChildCareArranOther", data.AnswerOrEmptyString("ChildCareArranOther"), new { @id = Model.Type + "_ChildCareArranOther" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Behavior at day care/school</label>
                    <%= Html.TextBox(Model.Type + "_SchoolBehavior", data.AnswerOrEmptyString("SchoolBehavior"), new { @id = Model.Type + "_SchoolBehavior", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Usual sleep/rest pattern</label>
                    <%= Html.TextBox(Model.Type + "_RestPattern", data.AnswerOrEmptyString("RestPattern"), new { @id = Model.Type + "_RestPattern", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Sleeping arrangements</label>
                    <%= Html.TextBox(Model.Type + "_SleepingArran", data.AnswerOrEmptyString("SleepingArran"), new { @id = Model.Type + "_SleepingArran", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Other:</label>
                    <%= Html.TextArea(Model.Type + "_PsychosocialOther", data.AnswerOrEmptyString("PsychosocialOther"), new { @id = Model.Type + "_PsychosocialOther", @class = "fill" })%>
                            
                </div>
            </td>
            <td>
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations0' name='{0}_Immunizations' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("0").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">DPT</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations1' name='{0}_Immunizations' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Measles</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations2' name='{0}_Immunizations' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Polio</label>
                        </div>
                    </li>
                    <div class="clear" />
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations3' name='{0}_Immunizations' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">DT</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations4' name='{0}_Immunizations' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Mumps</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations5' name='{0}_Immunizations' value='5' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">HBV</label>
                        </div>
                    </li>
                    <div class="clear" />
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations6' name='{0}_Immunizations' value='6' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">MMR</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations7' name='{0}_Immunizations' value='7' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Rubella</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations8' name='{0}_Immunizations' value='8' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Hib</label>
                        </div>
                    </li>
                    <div class="clear" />
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Immunizations9' name='{0}_Immunizations' value='9' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("Immunizations").Contains("9").ToChecked())%>
                            <label for="<%= Model.Type %>_Immunizations0">Other</label>
                        </div>
                        <div class="extra">
                            <div class="clear" />
                            (specify)<%= Html.TextBox(Model.Type + "_ImmunizationsOther", data.AnswerOrEmptyString("ImmunizationsOther"), new { @id = Model.Type + "_ImmunizationsOther" })%>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <th>Screening/Early detection</th>
        </tr>
        <tr>
            <td>
                <label class="float-left">TB skin test:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinTest0' name='{0}_SkinTest' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkinTest").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_SkinTest0">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinTest1' name='{0}_SkinTest' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SkinTest").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_SkinTest1">Yes</label>
                            </div>
                            <div class="extra">
                                <div class="clear" />
                                &nbsp;When/Results:<%= Html.TextBox(Model.Type + "_SkinTestYes", data.AnswerOrEmptyString("SkinTestYes"), new { @id = Model.Type + "_SkinTestYes", @class = "oe" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear" />
                <label class="float-left">Lead screening:</label>
                <div class="float-right">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_LeadScreening0' name='{0}_LeadScreening' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("LeadScreening").Contains("0").ToChecked())%>
                                <label for="<%= Model.Type %>_LeadScreening0">No</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_LeadScreening1' name='{0}_LeadScreening' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("LeadScreening").Contains("1").ToChecked())%>
                                <label for="<%= Model.Type %>_LeadScreening1">Yes</label>
                            </div>
                            <div class="extra">
                                &nbsp;When/Results:<%= Html.TextBox(Model.Type + "_LeadScreeningYes", data.AnswerOrEmptyString("LeadScreeningYes"), new { @id = Model.Type + "_LeadScreeningYes", @class="oe" })%>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear" />
                <label class="float-left">Other(specify)</label>
                <div class="float-right"><%= Html.TextBox(Model.Type + "_ScreeningOther", data.AnswerOrEmptyString("ScreeningOther"), new { @id = Model.Type + "_ScreeningOther" })%>
                        </div>
            </td>
        </tr>
        <tr>
            <th>Pain
                <%= string.Format("<input class='radio' id='{0}IsPainApply' name='{0}_IsPainApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsPainApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            <td>
                <div id="<%=Model.Type %>PainContainer">
                    <label class="float-left">Origin</label>
                    <%= Html.TextBox(Model.Type + "_PainOrigin", data.AnswerOrEmptyString("PainOrigin"), new { @id = Model.Type + "_PainOrigin", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Onset</label>
                    <%= Html.TextBox(Model.Type + "_PainOnset", data.AnswerOrEmptyString("PainOnset"), new { @id = Model.Type + "_PainOnset", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Location</label>
                    <%= Html.TextBox(Model.Type + "_PainLocation", data.AnswerOrEmptyString("PainLocation"), new { @id = Model.Type + "_PainLocation", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Quality</label>
                    <%= Html.TextBox(Model.Type + "_PainQuality", data.AnswerOrEmptyString("PainQuality"), new { @id = Model.Type + "_PainQuality", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Intensity:(using scales below)</label>
                    <%  var genericPainLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "0 = No Pain", Value = "None" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "Moderate" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", ""));%>
                    <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel", @class = "float-right" }) %>
                    <div class="row">
                        <img src="/Images/painscale.png" alt="Pain Scale Image" width="100%" /><br />
                        <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
                    </div>
                    <label class="float-left">Pain management history</label>
                    <%= Html.TextBox(Model.Type + "_PainHistory", data.AnswerOrEmptyString("PainHistory"), new { @id = Model.Type + "_PainHistory", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Present pain management regimen</label>
                    <%= Html.TextBox(Model.Type + "_PainRegimen", data.AnswerOrEmptyString("PainRegimen"), new { @id = Model.Type + "_PainRegimen", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Effectiveness</label>
                    <%= Html.TextBox(Model.Type + "_PainEffective", data.AnswerOrEmptyString("PainEffective"), new { @id = Model.Type + "_PainEffective", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Other(specify)</label>
                    <%= Html.TextBox(Model.Type + "_PainOther", data.AnswerOrEmptyString("PainOther"), new { @id = Model.Type + "_PainOther", @class = "float-right" })%>
                </div>
            </td>
        </tr>
        <tr>
            <th>Genitalia
                <%= string.Format("<input class='radio' id='{0}IsGenitaliaApply' name='{0}_IsGenitaliaApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsGenitaliaApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Hematology
                <%= string.Format("<input class='radio' id='{0}IsHematologyApply' name='{0}_IsHematologyApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsHematologyApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Musculoskeletal
                <%= string.Format("<input class='radio' id='{0}IsMusculoskeletalApply' name='{0}_IsMusculoskeletalApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsMusculoskeletalApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
            <th>Appliances/Aids/Special Equipment
                <%= string.Format("<input class='radio' id='{0}IsAppliancesApply' name='{0}_IsAppliancesApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsAppliancesApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            <td rowspan="3">
                <div id="<%=Model.Type %>GenitaliaContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaCircumcised0' name='{0}_GenitaliaCircumcised' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaCircumcised").Contains("0").ToChecked())%>
                                <label>Circumcised</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaCircumcised1' name='{0}_GenitaliaCircumcised' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaCircumcised").Contains("1").ToChecked())%>
                                <label>Uncircumcised</label>
                            </div>
                        </li>
                    </ul>  
                    <div class="clear" />
                    <label class="float-left">Scrotum:</label>  
                    <ul class="checkgroup inline float-right">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaScrotum0' name='{0}_GenitaliaScrotum' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaScrotum").Contains("0").ToChecked())%>
                                <label>WNL</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaScrotum1' name='{0}_GenitaliaScrotum' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaScrotum").Contains("1").ToChecked())%>
                                <label>Swollen</label>
                            </div>
                        </li>
                    </ul> 
                    <div class="clear" />
                    <label class="float-left">Testes:</label>
                    <ul class="checkgroup inline float-right">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaTest0' name='{0}_GenitaliaTest' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaTest").Contains("0").ToChecked())%>
                                <label>Descended</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_GenitaliaTest1' name='{0}_GenitaliaTest' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("_GenitaliaTest").Contains("1").ToChecked())%>
                                <label>Undescended</label>
                            </div>
                            <div class="extra">
                                <ul class="checkgroup inline">
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_TestUndescended0' name='{0}_TestUndescended' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("0").ToChecked())%>
                                            <label>Right</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_TestUndescended1' name='{0}_TestUndescended' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("1").ToChecked())%>
                                            <label>Left</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="option">
                                            <%= string.Format("<input id='{0}_TestUndescended2' name='{0}_TestUndescended' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("2").ToChecked())%>
                                            <label>Bilateral</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>   
                    <div class="clear" />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Puberty0' name='{0}_Puberty' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Puberty").Contains("0").ToChecked())%>
                                <label>Puberty</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Puberty1' name='{0}_Puberty' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Puberty").Contains("1").ToChecked())%>
                                <label>Menarche</label>
                            </div>
                            <div class="extra">
                                age<%= Html.TextBox(Model.Type + "_MenarcheAge", data.AnswerOrEmptyString("MenarcheAge"), new { @id = Model.Type + "_MenarcheAge", @class = "float-right" })%>
                                <div class="clear" />    
                                LMP<%= Html.TextBox(Model.Type + "_MenarcheLMP", data.AnswerOrEmptyString("MenarcheLMP"), new { @id = Model.Type + "_MenarcheLMP", @class = "float-right" })%>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Pregnancy:</label>  
                    Gravida<%= Html.TextBox(Model.Type + "_PregnancyGravida", data.AnswerOrEmptyString("PregnancyGravida"), new { @id = Model.Type + "_PregnancyGravida", @class = "float-right" })%>
                    <div class="clear" />
                    Para<%= Html.TextBox(Model.Type + "_PregnancyPara", data.AnswerOrEmptyString("PregnancyPara"), new { @id = Model.Type + "_PregnancyPara", @class = "float-right" })%>
                    <div class="clear" />
                    EDC<%= Html.TextBox(Model.Type + "_PregnancyEDC", data.AnswerOrEmptyString("PregnancyEDC"), new { @id = Model.Type + "_PregnancyEDC", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Other</label>
                    <%= Html.TextArea(Model.Type + "_GenitaliaOther", data.AnswerOrEmptyString("GenitaliaOther"), new { @id = Model.Type + "_GenitaliaOther", @class = "fill" })%>
                </div>
            </td>
            <td>
                <div id="<%=Model.Type %>HematologyContainer">
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Hematology0' name='{0}_Hematology' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("0").ToChecked())%>
                                <label>Anemia</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Hematology1' name='{0}_Hematology' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("1").ToChecked())%>
                                <label>Bilirubin</label>
                            </div>
                            <div class="extra float-right">
                                results<%= Html.TextBox(Model.Type + "_HematologyBilirubin", data.AnswerOrEmptyString("HematologyBilirubin"), new { @id = Model.Type + "_HematologyBilirubin", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Hematology2' name='{0}_Hematology' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("2").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra float-right">
                                <%= Html.TextArea(Model.Type + "_HematologyOther", data.AnswerOrEmptyString("HematologyOther"), 3,50, new { @id = Model.Type + "_HematologyOther", @class = "fill" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td rowspan="3">
                <div id="<%=Model.Type %>MusculoskeletalContainer">
                    <label class="float-left">Posture</label>
                    <%= Html.TextBox(Model.Type + "_Posture", data.AnswerOrEmptyString("Posture"), new { @id = Model.Type + "_Posture", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Strength</label>
                    <%= Html.TextBox(Model.Type + "_Strength", data.AnswerOrEmptyString("Strength"), new { @id = Model.Type + "_Strength", @class = "float-right" })%>
                    <div class="clear" />
                    <label class="float-left">Endurance</label>
                    <%= Html.TextBox(Model.Type + "_Endurance", data.AnswerOrEmptyString("Endurance"), new { @id = Model.Type + "_Endurance", @class = "float-right" })%>
                    <div class="clear" />
                    <ul class="checkgroup">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Musculoskeletal0' name='{0}_Musculoskeletal' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("0").ToChecked())%>
                                <label>Scoliosis</label>
                            </div>
                            <div class="extra">
                                type<%= Html.TextBox(Model.Type + "_ScoliosisType", data.AnswerOrEmptyString("ScoliosisType"), new { @id = Model.Type + "_ScoliosisType", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Musculoskeletal1' name='{0}_Musculoskeletal' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("1").ToChecked())%>
                                <label>Swollen/Painful joints</label>
                            </div>
                            <div class="extra">
                                specify:<%= Html.TextBox(Model.Type + "_Swollen", data.AnswerOrEmptyString("Swollen"), new { @id = Model.Type + "_Swollen", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Musculoskeletal2' name='{0}_Musculoskeletal' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("2").ToChecked())%>
                                <label>Fracture</label>
                            </div>
                            <div class="extra">
                                location:<%= Html.TextBox(Model.Type + "_Fracture", data.AnswerOrEmptyString("Fracture"), new { @id = Model.Type + "_Fracture", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Musculoskeletal3' name='{0}_Musculoskeletal' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("3").ToChecked())%>
                                <label>Decreased ROM</label>
                            </div>
                            <div class="extra">
                                specify:<%= Html.TextBox(Model.Type + "_DecreasedROM", data.AnswerOrEmptyString("DecreasedROM"), new { @id = Model.Type + "_DecreasedROM", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Musculoskeletal4' name='{0}_Musculoskeletal' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("4").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextArea(Model.Type + "_MuscuOther", data.AnswerOrEmptyString("MuscuOther"), 3, 50, new { @id = Model.Type + "_MuscuOther", @class = "fill" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
            <td rowspan="3">
                <div id="<%=Model.Type %>AppliancesContainer">
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances0' name='{0}_Appliances' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("0").ToChecked())%>
                                <label>Crutch(es)</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances1' name='{0}_Appliances' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("1").ToChecked())%>
                                <label>Wheelchair</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances2' name='{0}_Appliances' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("2").ToChecked())%>
                                <label>Cane</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances3' name='{0}_Appliances' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("3").ToChecked())%>
                                <label>Walker</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances4' name='{0}_Appliances' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("4").ToChecked())%>
                                <label>Brace/Orthotics</label>
                            </div>
                            <div class="extra">
                                (specify):<%= Html.TextBox(Model.Type + "_AppliancesBrace", data.AnswerOrEmptyString("AppliancesBrace"), new { @id = Model.Type + "_AppliancesBrace", @class = "float-right" })%>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances5' name='{0}_Appliances' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("5").ToChecked())%>
                                <label>Transfer equipment</label>
                            </div>
                            <div class="extra">
                                <%  var transferEquipmentLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Board", Value = "Board" },
                                        new SelectListItem { Text = "Lift", Value = "Lift" }
                                    }, "Value", "Text", data.AnswerOrDefault("TransferEquipmentLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_TransferEquipmentLevel", transferEquipmentLevel, new { @id = Model.Type + "_TransferEquipmentLevel", @class = "oe" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances6' name='{0}_Appliances' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("6").ToChecked())%>
                                <label>Bedside commode</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances7' name='{0}_Appliances' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("7").ToChecked())%>
                                <label>Prosthesis</label>
                            </div>
                            <div class="extra">
                                <%  var prosthesisLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "RUE", Value = "RUE" },
                                        new SelectListItem { Text = "RLE", Value = "RLE" },
                                        new SelectListItem { Text = "LUE", Value = "LUE" },
                                        new SelectListItem { Text = "LLE", Value = "LLE" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("ProsthesisLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ProsthesisLevel", prosthesisLevel, new { @id = Model.Type + "_ProsthesisLevel", @class = "oe otherOption" })%>
                                <%= Html.TextBox(Model.Type + "_ProsthesisOther", data.AnswerOrEmptyString("ProsthesisOther"), new { @id = Model.Type + "_ProsthesisOther", @class = "float-right otherTxt" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances8' name='{0}_Appliances' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("8").ToChecked())%>
                                <label>Grab bars:</label>
                            </div>
                            <div class="extra">
                                <%  var grabbarsLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Bathroom", Value = "Bathroom" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("GrabbarsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_GrabbarsLevel", grabbarsLevel, new { @id = Model.Type + "_GrabbarsLevel", @class = "oe otherOption" })%>
                                <%= Html.TextBox(Model.Type + "_GrabbarsOther", data.AnswerOrEmptyString("GrabbarsOther"), new { @id = Model.Type + "_GrabbarsOther", @class = "float-right otherTxt" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances9' name='{0}_Appliances' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("9").ToChecked())%>
                                <label>Hospital bed:</label>
                            </div>
                            <div class="extra">
                                <%  var hospitalBedLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Semi-elec", Value = "Semi-elec" },
                                        new SelectListItem { Text = "Crank", Value = "Crank" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("HospitalBedLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_HospitalBedLevel", hospitalBedLevel, new { @id = Model.Type + "_HospitalBedLevel", @class = "oe otherOption" })%>
                                <%= Html.TextBox(Model.Type + "_HospitalBedOther", data.AnswerOrEmptyString("HospitalBedOther"), new { @id = Model.Type + "_HospitalBedOther", @class = "float-right otherTxt" })%>
                                <div class="clear" />
                                <div class="float-right">
                                    Overlays<%= Html.TextBox(Model.Type + "_Overlays", data.AnswerOrEmptyString("Overlays"), new { @id = Model.Type + "_Overlays", @class = "float-right" })%>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances10' name='{0}_Appliances' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("10").ToChecked())%>
                                <label>Oxygen:</label>
                            </div>
                            <div class="extra">
                                HME Co.<%= Html.TextBox(Model.Type + "_HMECo", data.AnswerOrEmptyString("HMECo"), new { @id = Model.Type + "_HMECo", @class = "float-right" })%>
                                <div class="clear" />
                                HME Rep.<%= Html.TextBox(Model.Type + "_HMERep", data.AnswerOrEmptyString("HMERep"), new { @id = Model.Type + "_HMERep", @class = "float-right" })%>
                                <div class="clear" />
                                HME Phone<%= Html.TextBox(Model.Type + "_HMEPhone", data.AnswerOrEmptyString("HMEPhone"), new { @id = Model.Type + "_HMEPhone", @class = "float-right" })%>
                                
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances11' name='{0}_Appliances' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("11").ToChecked())%>
                                <label>Fire Alarm</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances12' name='{0}_Appliances' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("12").ToChecked())%>
                                <label>Smoke Alarm</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances13' name='{0}_Appliances' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("13").ToChecked())%>
                                <label>Equipment needs</label>
                            </div>
                            <div class="extra">
                                (specify):<%= Html.TextBox(Model.Type + "_EquipmentSpecify", data.AnswerOrEmptyString("EquipmentSpecify"), new { @id = Model.Type + "_EquipmentSpecify", @class = "float-right" })%>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_Appliances14' name='{0}_Appliances' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("14").ToChecked())%>
                                <label>Other</label>
                            </div>
                            <div class="extra">
                                <%= Html.TextArea(Model.Type + "_EquipmentOther", data.AnswerOrEmptyString("EquipmentOther"),4,30, new { @id = Model.Type + "_EquipmentOther", @class = "fill" })%>
                            </div>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th>Skin conditions/Wounds
                <%= string.Format("<input class='radio' id='{0}IsWoundsApply' name='{0}_IsWoundsApply' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("IsWoundsApply").Contains("1").ToChecked())%>
                <label>No Problem</label>
            </th>
        </tr>
        <tr>
            <td>
                <div id="<%=Model.Type %>WoundsContainer">
                    <label class="float-left">Check all that apply:</label><br />
                    <ul class="checkgroup inline">
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound0' name='{0}_SkinWound' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("0").ToChecked())%>
                                <label>Mongolian spots</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound1' name='{0}_SkinWound' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("1").ToChecked())%>
                                <label>Itch</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound2' name='{0}_SkinWound' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("2").ToChecked())%>
                                <label>Rash</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound3' name='{0}_SkinWound' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("3").ToChecked())%>
                                <label>Dry</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound4' name='{0}_SkinWound' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("4").ToChecked())%>
                                <label>Scaling</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound5' name='{0}_SkinWound' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("5").ToChecked())%>
                                <label>Incision</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound6' name='{0}_SkinWound' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("6").ToChecked())%>
                                <label>Wounds</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound7' name='{0}_SkinWound' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("7").ToChecked())%>
                                <label>Lesions</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound8' name='{0}_SkinWound' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("8").ToChecked())%>
                                <label>Sutures</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound9' name='{0}_SkinWound' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("9").ToChecked())%>
                                <label>Staples</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound10' name='{0}_SkinWound' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("10").ToChecked())%>
                                <label>Abrasions</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound11' name='{0}_SkinWound' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("11").ToChecked())%>
                                <label>Lacerations</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound12' name='{0}_SkinWound' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("12").ToChecked())%>
                                <label>Bruises</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound13' name='{0}_SkinWound' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("13").ToChecked())%>
                                <label>Ecchymosis</label>
                            </div>
                        </li>
                        <div class="clear" />
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound14' name='{0}_SkinWound' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("14").ToChecked())%>
                                <label>Edema</label>
                            </div>
                        </li>
                        <li>
                            <div class="option">
                                <%= string.Format("<input id='{0}_SkinWound15' name='{0}_SkinWound' value='15' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("15").ToChecked())%>
                                <label>Hemangiomas</label>
                            </div>
                        </li>
                    </ul>
                    <div class="clear" />
                    <label class="float-left">Pallor:</label>
                    <%  var pallorLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "Jaundice", Value = "Jaundice" },
                            new SelectListItem { Text = "Redness", Value = "Redness" }
                        }, "Value", "Text", data.AnswerOrDefault("PallorLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_PallorLevel", pallorLevel, new { @id = Model.Type + "_PallorLevel", @class = "oe" })%>
                    <div class="clear" />
                    <label class="float-left">Turgor:</label>
                    <%  var turgorLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "Good", Value = "Good" },
                            new SelectListItem { Text = "Poor", Value = "Poor" }
                        }, "Value", "Text", data.AnswerOrDefault("TurgorLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_TurgorLevel", turgorLevel, new { @id = Model.Type + "_TurgorLevel", @class = "oe" })%>
                    <div class="clear" />
                    <label class="float-left">Other</label>
                    <%= Html.TextArea(Model.Type + "_SkinWoundOther", data.AnswerOrEmptyString("SkinWoundOther"), 4, 30, new { @id = Model.Type + "_SkinWoundOther", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <th>Living Arrangements/Caregiver</th>
            <th>Safety</th>
            <th>Advance Directives</th>
            <th>Teaching/Training</th>
        </tr>
        <tr>
            <td>
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_LivingArrange0' name='{0}_LivingArrange' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("LivingArrange").Contains("0").ToChecked())%>
                            <label>House</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_LivingArrange1' name='{0}_LivingArrange' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("LivingArrange").Contains("1").ToChecked())%>
                            <label>Apartment</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_LivingArrange2' name='{0}_LivingArrange' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("LivingArrange").Contains("2").ToChecked())%>
                            <label>New environment</label>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Primary language</label>
                <%= Html.TextBox(Model.Type + "_PrimaryLanguage", data.AnswerOrEmptyString("PrimaryLanguage"), new { @id = Model.Type + "_PrimaryLanguage", @class = "float-right" })%>
                <div class="clear" />
                <ul class="checkgroup">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption0' name='{0}_PrimaryLanguageOption' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("0").ToChecked())%>
                            <label>Language barrier</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption1' name='{0}_PrimaryLanguageOption' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("1").ToChecked())%>
                            <label>Needs interpreter</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption2' name='{0}_PrimaryLanguageOption' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("2").ToChecked())%>
                            <label>Learning barrier:</label>
                        </div>
                        <div class="extra">
                            <%  var learningBarrierLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Mental", Value = "Mental" },
                                    new SelectListItem { Text = "Psychosocial", Value = "Psychosocial" },
                                    new SelectListItem { Text = "Physical", Value = "Physical" },
                                    new SelectListItem { Text = "Functional", Value = "Functional" }
                                }, "Value", "Text", data.AnswerOrDefault("LearningBarrierLevel", ""));%>
                            <%= Html.DropDownList(Model.Type + "_LearningBarrierLevel", learningBarrierLevel, new { @id = Model.Type + "_LearningBarrierLevel", @class = "oe" })%>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption3' name='{0}_PrimaryLanguageOption' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("3").ToChecked())%>
                            <label>Able to read/write</label>
                        </div>
                        <div class="extra">
                            Educational level<%= Html.TextBox(Model.Type + "_EducationalLevel", data.AnswerOrEmptyString("EducationalLevel"), new { @id = Model.Type + "_EducationalLevel", @class = "float-right" })%>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption4' name='{0}_PrimaryLanguageOption' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("4").ToChecked())%>
                            <label>Spiritual/Cultural implications that impact care.</label>
                        </div>
                        <div class="extra">
                            spiritual resource<%= Html.TextBox(Model.Type + "_SpiritualResource", data.AnswerOrEmptyString("SpiritualResource"), new { @id = Model.Type + "_SpiritualResource", @class = "" })%>
                            <div class="clear" />
                            Phone No.<%= Html.TextBox(Model.Type + "_SpiritualPhone", data.AnswerOrEmptyString("SpiritualPhone"), new { @id = Model.Type + "_SpiritualPhone", @class = "" })%>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryLanguageOption5' name='{0}_PrimaryLanguageOption' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("5").ToChecked())%>
                            <label>Siblings</label>
                        </div>
                        <div class="extra">
                            (specify)<%= Html.TextBox(Model.Type + "_SiblingSpecify", data.AnswerOrEmptyString("SiblingSpecify"), new { @id = Model.Type + "_SiblingSpecify", @class = "float-right" })%>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Primary caregiver</label>
                <%= Html.TextBox(Model.Type + "_CaregiverName", data.AnswerOrEmptyString("CaregiverName"), new { @id = Model.Type + "_CaregiverName", @class = "float-right" })%>
                <div class="clear" />
                <label class="float-left">Relationship/Health status</label>
                <ul class="checkgroup">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryCaregiver0' name='{0}_PrimaryCaregiver' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryCaregiver").Contains("0").ToChecked())%>
                            <label>Assists with ADLs</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryCaregiver1' name='{0}_PrimaryCaregiver' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryCaregiver").Contains("1").ToChecked())%>
                            <label>Provides physical care</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_PrimaryCaregiver2' name='{0}_PrimaryCaregiver' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryCaregiver").Contains("2").ToChecked())%>
                            <label>Other</label>
                        </div>
                        <div class="extra">
                            (specify)<%= Html.TextBox(Model.Type + "_PrimaryCaregiverOther", data.AnswerOrEmptyString("PrimaryCaregiverOther"), new { @id = Model.Type + "_PrimaryCaregiverOther", @class = "float-right" })%>
                        </div>
                    </li>
                </ul>
            </td>
            <td>
                <label class="float-left">Safety Measures:</label><br />
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety0' name='{0}_Safety' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("0").ToChecked())%>
                            <label>Fire/electrical safety</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety1' name='{0}_Safety' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("1").ToChecked())%>
                            <label>Burn prevention</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety2' name='{0}_Safety' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("2").ToChecked())%>
                            <label>Poisoning prevention</label>
                        </div>
                    </li> 
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety3' name='{0}_Safety' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("3").ToChecked())%>
                            <label>Falls prevention</label>
                        </div>
                    </li>   
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety4' name='{0}_Safety' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("4").ToChecked())%>
                            <label>Water safety</label>
                        </div>
                    </li>   
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety5' name='{0}_Safety' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("5").ToChecked())%>
                            <label>Siderails up</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety6' name='{0}_Safety' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("6").ToChecked())%>
                            <label>Clear pathways</label>
                        </div>
                    </li>    
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety7' name='{0}_Safety' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("7").ToChecked())%>
                            <label>Suffocation precautions</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety8' name='{0}_Safety' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("8").ToChecked())%>
                            <label>Aspiration precautions</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety9' name='{0}_Safety' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("9").ToChecked())%>
                            <label>Elevate head of bed</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety10' name='{0}_Safety' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("10").ToChecked())%>
                            <label>Seizure precautions</label>
                        </div>
                    </li> 
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety11' name='{0}_Safety' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("11").ToChecked())%>
                            <label>Transfer/ambulation safety</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety12' name='{0}_Safety' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("12").ToChecked())%>
                            <label>Wheelchair precautions</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety13' name='{0}_Safety' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("13").ToChecked())%>
                            <label>Proper use of assistive devices</label>
                        </div>
                    </li> 
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety14' name='{0}_Safety' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("14").ToChecked())%>
                            <label>Universal precautions</label>
                        </div>
                    </li>   
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety15' name='{0}_Safety' value='15' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("15").ToChecked())%>
                            <label>Sharps and/or supplies disposals</label>
                        </div>
                    </li>   
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety16' name='{0}_Safety' value='16' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("16").ToChecked())%>
                            <label>Cardiac prevention</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety17' name='{0}_Safety' value='17' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("17").ToChecked())%>
                            <label>Diabetic precautions</label>
                        </div>
                    </li>    
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety18' name='{0}_Safety' value='18' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("18").ToChecked())%>
                            <label>Oxygen safety/precautions</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety19' name='{0}_Safety' value='19' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("19").ToChecked())%>
                            <label>Bleeding precautions</label>
                        </div>
                    </li>  
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_Safety20' name='{0}_Safety' value='20' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("20").ToChecked())%>
                            <label>Other</label>
                        </div>
                        <div class="extra">
                            <%= Html.TextBox(Model.Type + "_SafetyOther", data.AnswerOrEmptyString("SafetyOther"), new { @id = Model.Type + "_SafetyOther", @class = "float-right" })%>
                        </div>
                    </li> 
                </ul>
            </td>
            <td>
                <ul class="checkgroup">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective0' name='{0}_AdvanceDirective' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("0").ToChecked())%>
                            <label>Do not resuscitate</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective1' name='{0}_AdvanceDirective' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("1").ToChecked())%>
                            <label>Do not hospitalize</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective2' name='{0}_AdvanceDirective' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("2").ToChecked())%>
                            <label>Organ donor</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective3' name='{0}_AdvanceDirective' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("3").ToChecked())%>
                            <label>Education needed</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective4' name='{0}_AdvanceDirective' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("4").ToChecked())%>
                            <label>Copies on file</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_AdvanceDirective5' name='{0}_AdvanceDirective' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("AdvanceDirective").Contains("5").ToChecked())%>
                            <label>Funeral arrangements made</label>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Comments:</label>
                <%= Html.TextArea(Model.Type + "_AdvanceDirectiveOther", data.AnswerOrEmptyString("AdvanceDirectiveOther"), new { @id = Model.Type + "_AdvanceDirectiveOther", @class = "fill" })%>
            </td>
            <td>
                <label class="float-left">I-Instruct; R-Reinstruct; D-Demonstrate; U-Understands Instructions</label>
                <div class="quarterCol">Subject</div>
                <div class="quarterCol">Option</div>
                <div class="halfOfTd">Patient/Caregiver response</div>
                <div class="clear" />
                <div class="quarterCol"><span class="float-left">Diet</span></div>
                <div class="quarterCol">
                <%  var dietLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("DietLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_DietLevel", dietLevel, new { @id = Model.Type + "_DietLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_DietResponse", data.AnswerOrEmptyString("DietResponse"), new { @id = Model.Type + "_DietResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Medication(s)</span></div>
                <div class="quarterCol">
                <%  var MedicationLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("MedicationLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_MedicationLevel", MedicationLevel, new { @id = Model.Type + "_MedicationLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_MedicationResponse", data.AnswerOrEmptyString("MedicationResponse"), new { @id = Model.Type + "_MedicationResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Disease process</span></div>
                <div class="quarterCol">
                    <%  var DiseaseLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "I", Value = "I" },
                            new SelectListItem { Text = "R", Value = "R" },
                            new SelectListItem { Text = "D", Value = "D" },
                            new SelectListItem { Text = "U", Value = "U" }
                        }, "Value", "Text", data.AnswerOrDefault("DiseaseLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_DiseaseLevel", DiseaseLevel, new { @id = Model.Type + "_DiseaseLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_DiseaseResponse", data.AnswerOrEmptyString("DiseaseResponse"), new { @id = Model.Type + "_DiseaseResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Safety</span></div>
                <div class="quarterCol">
                <%  var SafetyLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("SafetyLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_SafetyLevel", SafetyLevel, new { @id = Model.Type + "_SafetyLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_SafetyResponse", data.AnswerOrEmptyString("SafetyResponse"), new { @id = Model.Type + "_SafetyResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">S/S to report</span></div>
                <div class="quarterCol">
                <%  var SSLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("SSLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_SSLevel", SSLevel, new { @id = Model.Type + "_SSLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_SSResponse", data.AnswerOrEmptyString("SSResponse"), new { @id = Model.Type + "_SSResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Well child care</span></div>
                <div class="quarterCol">
                <%  var WellLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("WellLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_WellLevel", WellLevel, new { @id = Model.Type + "WellLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_WellResponse", data.AnswerOrEmptyString("WellResponse"), new { @id = Model.Type + "_WellResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Activities</span></div>
                <div class="quarterCol">
                <%  var ActivitiesLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("ActivitiesLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_ActivitiesLevel", ActivitiesLevel, new { @id = Model.Type + "_ActivitiesLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_ActivitiesResponse", data.AnswerOrEmptyString("ActivitiesResponse"), new { @id = Model.Type + "_ActivitiesResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Treatments</span></div>
                <div class="quarterCol">
                <%  var TreatmentsLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("TreatmentsLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_TreatmentsLevel", TreatmentsLevel, new { @id = Model.Type + "_TreatmentsLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_TreatmentsResponse", data.AnswerOrEmptyString("TreatmentsResponse"), new { @id = Model.Type + "_TreatmentsResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Injury prevention</span></div>
                <div class="quarterCol">
                <%  var InjuryLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("InjuryLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_InjuryLevel", InjuryLevel, new { @id = Model.Type + "_InjuryLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_InjuryResponse", data.AnswerOrEmptyString("InjuryResponse"), new { @id = Model.Type + "_InjuryResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol"><span class="float-left">Growth</span></div>
                <div class="quarterCol">
                <%  var GrowthLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("GrowthLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_GrowthLevel", GrowthLevel, new { @id = Model.Type + "_GrowthLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_GrowthResponse", data.AnswerOrEmptyString("GrowthResponse"), new { @id = Model.Type + "_GrowthResponse", @class = "fill" })%>     
                </div> <div class="clear" />
                <div class="quarterCol">
                    <%= Html.TextBox(Model.Type + "_TeachingOther", data.AnswerOrEmptyString("TeachingOther"), new { @id = Model.Type + "_TeachingOther", @class = "fill" })%>  
                </div>
                <div class="quarterCol">
                <%  var OtherLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "I", Value = "I" },
                        new SelectListItem { Text = "R", Value = "R" },
                        new SelectListItem { Text = "D", Value = "D" },
                        new SelectListItem { Text = "U", Value = "U" }
                    }, "Value", "Text", data.AnswerOrDefault("OtherLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_OtherLevel", OtherLevel, new { @id = Model.Type + "_OtherLevel", @class = "shortOption" })%>
                </div>
                <div class="halfOfTd">
                    <%= Html.TextBox(Model.Type + "_OtherResponse", data.AnswerOrEmptyString("OtherResponse"), new { @id = Model.Type + "_OtherResponse", @class = "fill" })%>     
                </div> 
            </td>
        </tr>
        <tr>
            <th colspan="2">Skilled care provided this visit</th>
            <th colspan="2">Summary of growth and development for age</th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_SkilledCareProvidedTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_SkilledCareProvided" })%>
                <%= Html.TextArea(Model.Type + "_SkilledCareProvided", data.AnswerOrEmptyString("SkilledCareProvided"),6,20, new { @id = Model.Type + "_SkilledCareProvided", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_SummaryOfGrowthTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_SummaryOfGrowth" })%>
                <%= Html.TextArea(Model.Type + "_SummaryOfGrowth", data.AnswerOrEmptyString("SummaryOfGrowth"), 6, 20, new { @id = Model.Type + "_SummaryOfGrowth", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="2">Prognosis/Learning potential</th>
            <th colspan="2">Discharge plans</th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_LearningPotentialTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_LearningPotential" })%>
                <%= Html.TextArea(Model.Type + "_LearningPotential", data.AnswerOrEmptyString("LearningPotential"), 6, 20, new { @id = Model.Type + "_LearningPotential", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_DischrgePlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_DischrgePlan" })%>
                <%= Html.TextArea(Model.Type + "_DischrgePlan", data.AnswerOrEmptyString("DischrgePlan"), 6, 20, new { @id = Model.Type + "_DischrgePlan", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="4">Summary checklist</th>
        </tr>
        <tr>
            <td colspan="4">
                <label class="float-left">Medication status:</label>
                <ul class="checkgroup">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_MedicationStatus0' name='{0}_MedicationStatus' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("MedicationStatus").Contains("0").ToChecked())%>
                            <label>Medication regimen completed/reviewed</label>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Check if any of the following were identified:</label>
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist0' name='{0}_SummaryChecklist' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("0").ToChecked())%>
                            <label>Potential adverse effects/drug reactions</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist1' name='{0}_SummaryChecklist' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("1").ToChecked())%>
                            <label>Ineffective drug therapy</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist2' name='{0}_SummaryChecklist' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("2").ToChecked())%>
                            <label>Significant side effects</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist3' name='{0}_SummaryChecklist' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("3").ToChecked())%>
                            <label>Significant drug interactions</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist4' name='{0}_SummaryChecklist' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("4").ToChecked())%>
                            <label>Duplicate drug therapy</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist5' name='{0}_SummaryChecklist' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("5").ToChecked())%>
                            <label>Non-compliance with drug therapy</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist6' name='{0}_SummaryChecklist' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("6").ToChecked())%>
                            <label>No change</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_SummaryChecklist7' name='{0}_SummaryChecklist' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("SummaryChecklist").Contains("7").ToChecked())%>
                            <label>Order obtained</label>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Billable supplies recorded?</label>
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_BillableSupplies0' name='{0}_BillableSupplies' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("BillableSupplies").Contains("0").ToChecked())%>
                            <label>Yes</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_BillableSupplies1' name='{0}_BillableSupplies' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("BillableSupplies").Contains("1").ToChecked())%>
                            <label>No</label>
                        </div>
                    </li>
                </ul>
                <div class="clear" />
                <label class="float-left">Care coordination:</label>
                <ul class="checkgroup inline">
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination0' name='{0}_CareCoordination' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("0").ToChecked())%>
                            <label>Physician</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination1' name='{0}_CareCoordination' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("1").ToChecked())%>
                            <label>PT</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination2' name='{0}_CareCoordination' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("2").ToChecked())%>
                            <label>OT</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination3' name='{0}_CareCoordination' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("3").ToChecked())%>
                            <label>ST</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination4' name='{0}_CareCoordination' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("4").ToChecked())%>
                            <label>SS</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination5' name='{0}_CareCoordination' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("5").ToChecked())%>
                            <label>SN</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination6' name='{0}_CareCoordination' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("6").ToChecked())%>
                            <label>Aide</label>
                        </div>
                    </li>
                    <li>
                        <div class="option">
                            <%= string.Format("<input id='{0}_CareCoordination7' name='{0}_CareCoordination' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("CareCoordination").Contains("7").ToChecked())%>
                            <label>Other</label>
                        </div>
                        <div class="extra">
                            (specify)<%= Html.TextBox(Model.Type + "_CareCoordiOther", data.AnswerOrEmptyString("CareCoordiOther"), new { @id = Model.Type + "_CareCoordiOther" })%>
            
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>


<script type="text/javascript">
    U.HideOptions();
    U.HideIfChecked($("#<%= Model.Type %>IsEyesApply"), $("#<%= Model.Type %>EyesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsNoseApply"), $("#<%= Model.Type %>NoseContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsEarsApply"), $("#<%= Model.Type %>EarContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsHeadApply"), $("#<%= Model.Type %>HeadContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsEndocrineApply"), $("#<%= Model.Type %>EndocrineContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsCardiovascularApply"), $("#<%= Model.Type %>CardiovascularContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsRespiratoryApply"), $("#<%= Model.Type %>RespiratoryContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGenitourinaryApply"), $("#<%= Model.Type %>GenitourinaryContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGastrointestinalApply"), $("#<%= Model.Type %>GastrointestinalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsNeurologicalApply"), $("#<%= Model.Type %>NeurologicalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPsychosocialApply"), $("#<%= Model.Type %>PsychosocialContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsPainApply"), $("#<%= Model.Type %>PainContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsGenitaliaApply"), $("#<%= Model.Type %>GenitaliaContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsHematologyApply"), $("#<%= Model.Type %>HematologyContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsMusculoskeletalApply"), $("#<%= Model.Type %>MusculoskeletalContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsAppliancesApply"), $("#<%= Model.Type %>AppliancesContainer"));
    U.HideIfChecked($("#<%= Model.Type %>IsWoundsApply"), $("#<%= Model.Type %>WoundsContainer"));
    U.ShowTxtWhenChooseOther();
    
</script>