﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%= Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Driver/Transportation Note<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/MSW/transportation.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (
                Model != null && Model.Agency != null ?
                    (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                : string.Empty).Clean() %>",
            "patientname": "<%= (
                Model != null && Model.Patient != null ?
                    (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) +
                    (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) +
                    (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br/>" : "<br/>")
                : string.Empty).Clean() %>",
            "mr": "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber : string.Empty %>",
            "visitdate": "<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.Clean() : string.Empty %>",
            "timein": "<%= Model != null && Model.Questions != null && Model.Questions.ContainsKey("TimeIn") && Model.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? Model.Questions["TimeIn"].Answer.Clean() : string.Empty %>",
            "timeout": "<%= Model != null && Model.Questions != null && Model.Questions.ContainsKey("TimeOut") && Model.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? Model.Questions["TimeOut"].Answer.Clean() : string.Empty %>",
            "mileage": "<%= Model != null && Model.Questions.ContainsKey("AssociatedMileage") ? Model.Questions["AssociatedMileage"].Answer.Clean() : ""%>",
            "episode": "<%= Model != null && Model.StartDate.IsValid() && Model.EndDate.IsValid() ? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() : ""%>",
            "surcharge": "<%= Model != null && Model.Questions.ContainsKey("Surcharge") ? Model.Questions["Surcharge"].Answer.Clean() : ""%>",
            "sign": "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>",
            "signdate": "<%= Model != null && Model.SignatureDate.IsDate() ? Model.SignatureDate.Clean() : string.Empty %>"
            
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>