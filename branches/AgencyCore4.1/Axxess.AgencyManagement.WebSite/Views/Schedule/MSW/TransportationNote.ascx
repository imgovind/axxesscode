﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Driver/Transportation Log | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "TransportationNoteForm" })) { var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
    <%= Html.Hidden("DriverOrTransportationNote_PatientId", Model.PatientId, new { @id = "DriverOrTransportationNote_PatientId" })%>
    <%= Html.Hidden("DriverOrTransportationNote_EpisodeId", Model.EpisodeId, new { @id = "DriverOrTransportationNote_EpisodeId" })%>
    <%= Html.Hidden("DriverOrTransportationNote_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", "DriverOrTransportationNote")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="8">
                    Driver/Transportation Log
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
                <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="8" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="5" class="bigtext"><%= Model.Patient.DisplayName %></td>
                <td colspan="3" class="bigtext"><%= string.Format("MR# : {0}", Model.Patient.PatientIdNumber) %></td>
            </tr>
            <tr>
                <td colspan="4">
                    <div>
                        <label for="<%= Model.Type %>_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="DriverOrTransportationNote_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker oe" name="DriverOrTransportationNote_VisitDate" value="<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="Text1" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="DriverOrTransportationNote_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @id = "DriverOrTransportationNote_TimeIn", @class = "time-picker oe" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="DriverOrTransportationNote_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @id = "DriverOrTransportationNote_TimeOut", @class = "time-picker oe" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="DriverOrTransportationNote_PickUpDate" class="float-left">Pick Up Date:</label>
                        <div class="float-right"><input type="text" class="date-picker oe" name="DriverOrTransportationNote_PickUpDate" value="<%= data.ContainsKey("PickUpDate") && data["PickUpDate"].Answer.IsNotNullOrEmpty() && data["PickUpDate"].Answer .IsValidDate()? data["PickUpDate"].Answer.ToDateTime().ToString("MM/dd/yyyy") : "" %>" id="DriverOrTransportationNote_PickUpDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="DriverOrTransportationNote_PickUpTime" class="float-left">Pick Up Time:</label>
                        <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_PickUpTime", data.ContainsKey("PickUpTime") ? data["PickUpTime"].Answer : "", new { @id = "DriverOrTransportationNote_PickUpTime", @class = "time-picker oe" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
                <td colspan="4">
                    <div>&nbsp;
                        <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %><label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div><% } %>
                    </div>  
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = Model.Type + "_AssociatedMileage", @class = "text digits input_wrapper oe", @maxlength = "5" })%></div>
                    </div>
                    <div class="clear"></div>
                     <div>
                        <label for="<%= Model.Type %>_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type + "_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = Model.Type + "_Surcharge", @class = "text digits input_wrapper oe", @maxlength = "5" })%></div>
                    </div>
                     <div class="clear"></div>
                     <div>
                        <label for="DriverOrTransportationNote_AppointmentTime" class="float-left">Appointment Time:</label>
                        <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_AppointmentTime", data.ContainsKey("AppointmentTime") ? data["AppointmentTime"].Answer : "", new { @id = "DriverOrTransportationNote_AppointmentTime", @class = "time-picker oe" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="DriverOrTransportationNote_AppointmentType" class="float-left">Appointment Type:</label>
                        <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_AppointmentType", data.ContainsKey("AppointmentType") ? data["AppointmentType"].Answer : "", new { @id = "DriverOrTransportationNote_AppointmentType",  @class = "oe" })%></div>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th>Pick Up Location</th>
                <th>Drop Off Location</th>
            </tr>
            <tr>
                <td class="align-left">
                    <label for="DriverOrTransportationNote_PickUpAddressLine1" class="float-left">Address Line 1:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_PickUpAddressLine1", data.ContainsKey("PickUpAddressLine1") ? data["PickUpAddressLine1"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_PickUpAddressLine1", @class = "text input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_PickUpAddressLine2" class="float-left"> Address Line 2:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_PickUpAddressLine2", data.ContainsKey("PickUpAddressLine2") ? data["PickUpAddressLine2"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_PickUpAddressLine2", @class = "text  input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_PickUpAddressCity" class="float-left">City:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_PickUpAddressCity", data.ContainsKey("PickUpAddressCity") ? data["PickUpAddressCity"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_PickUpAddressCity", @class = "text  input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_PickUpAddressStateCode" class="float-left">State:</label>
                    <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "DriverOrTransportationNote_PickUpAddressStateCode", data.ContainsKey("PickUpAddressStateCode") ? data["PickUpAddressStateCode"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_PickUpAddressStateCode", @class = "AddressStateCode  valid" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_PickUpAddressStateCode" class="float-left">Zip:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_PickUpAddressZipCode", data.ContainsKey("PickUpAddressZipCode") ? data["PickUpAddressZipCode"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_PickUpAddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "9" })%></div>
                </td>
                <td class="align-left">
                    <label for="DriverOrTransportationNote_DropOffAddressLine1" class="float-left">Address Line 1:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_DropOffAddressLine1", data.ContainsKey("DropOffAddressLine1") ? data["DropOffAddressLine1"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_DropOffAddressLine1", @class = "text  input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_DropOffAddressLine2" class="float-left"> Address Line 2:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_DropOffAddressLine2", data.ContainsKey("DropOffAddressLine2") ? data["DropOffAddressLine2"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_DropOffAddressLine2", @class = "text  input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_DropOffAddressCity" class="float-left">City:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_DropOffAddressCity", data.ContainsKey("DropOffAddressCity") ? data["DropOffAddressCity"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_DropOffAddressCity", @class = "text  input_wrapper", @maxlength = "50" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_DropOffAddressStateCode" class="float-left">State:</label>
                    <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "DriverOrTransportationNote_DropOffAddressStateCode", data.ContainsKey("DropOffAddressStateCode") ? data["DropOffAddressStateCode"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_DropOffAddressStateCode", @class = "AddressStateCode  valid" })%></div>
                    <div class="clear"></div>
                    <label for="DriverOrTransportationNote_DropOffAddressStateCode" class="float-left">Zip:</label>
                    <div class="float-right"><%= Html.TextBox("DriverOrTransportationNote_DropOffAddressZipCode", data.ContainsKey("DropOffAddressZipCode") ? data["DropOffAddressZipCode"].Answer : string.Empty, new { @id = "DriverOrTransportationNote_DropOffAddressZipCode", @class = "text  digits isValidUSZip zip", @maxlength = "9" })%></div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Mileage Log</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="fixed">
                        <tbody>
                            <tr>
                                <th></th>
                                <th>Start</th>
                                <th>Pick up home</th>
                                <th>Drop off appt.</th>
                                <th>Return pick up</th>
                                <th>Return home</th>
                                <th>Return office</th>
                                <th>Total</th>
                            </tr>
                            <tr>
                                <th>Miles</th>
                                <td><%= Html.TextBox("DriverOrTransportationNote_StartMileage", data.ContainsKey("StartMileage") ? data["StartMileage"].Answer : "", new { @id = "DriverOrTransportationNote_StartMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_PickUpHomeMileage", data.ContainsKey("PickUpHomeMileage") ? data["PickUpHomeMileage"].Answer : "", new { @id = "DriverOrTransportationNote_PickUpHomeMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_DropOffApptMileage", data.ContainsKey("DropOffApptMileage") ? data["DropOffApptMileage"].Answer : "", new { @id = "DriverOrTransportationNote_DropOffApptMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_ReturnPickUpMileage", data.ContainsKey("ReturnPickUpMileage") ? data["ReturnPickUpMileage"].Answer : "", new { @id = "DriverOrTransportationNote_ReturnPickUpMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_ReturnHomeMileage", data.ContainsKey("ReturnHomeMileage") ? data["ReturnHomeMileage"].Answer : "", new { @id = "DriverOrTransportationNote_ReturnHomeMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_ReturnOfficeMileage", data.ContainsKey("ReturnOfficeMileage") ? data["ReturnOfficeMileage"].Answer : "", new { @id = "DriverOrTransportationNote_ReturnOfficeMileage", @class = "sn" })%></td>
                                <td><%= Html.TextBox("DriverOrTransportationNote_TotalMileage", data.ContainsKey("TotalMileage") ? data["TotalMileage"].Answer : "", new { @id = "DriverOrTransportationNote_TotalMileage", @class = "sn" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="DriverOrTransportationNote_Clinician" class="float-left">Driver Signature:</label>
                        <div class="float-right"><%= Html.Password("DriverOrTransportationNote_Clinician", "", new { @id = "DriverOrTransportationNote_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="DriverOrTransportationNote_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right">
                            <input type="text" class="date-picker" name="DriverOrTransportationNote_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : "" %>" id="DriverOrTransportationNote_SignatureDate" />
                        </div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="DriverOrTransportationNote_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="DriverOrTransportationNoteRemove();transportationLog.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="DriverOrTransportationNoteAdd(); transportationLog.Submit($(this));">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="DriverOrTransportationNoteRemove();transportationLog.Submit($(this));>Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="DriverOrTransportationNoteRemove();transportationLog.Submit($(this));>Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="DriverOrTransportationNoteRemove(); UserInterface.CloseWindow('transportationnote');">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    //Schedule.WarnTimeInOut("DriverOrTransportationNote");
    function DriverOrTransportationNoteAdd() {
        $("#DriverOrTransportationNote_Clinician").removeClass('required').addClass('required');
        $("#DriverOrTransportationNote_SignatureDate").removeClass('required').addClass('required');
    }
    function DriverOrTransportationNoteRemove() {
        $("#DriverOrTransportationNote_TimeIn").removeClass('required');
        $("#DriverOrTransportationNote_TimeOut").removeClass('required');
        $("#DriverOrTransportationNote_Clinician").removeClass('required');
        $("#DriverOrTransportationNote_SignatureDate").removeClass('required');
    }
</script>