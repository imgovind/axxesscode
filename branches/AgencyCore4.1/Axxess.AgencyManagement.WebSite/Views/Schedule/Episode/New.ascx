﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<%= Html.Hidden("New_Episode_PreviousEndDate", Model.EndDate != DateTime.MinValue ? Model.EndDate.ToShortDateString() : "")%>
<% using (Html.BeginForm("AddEpisode", "Schedule", FormMethod.Post, new { @id = "newEpisodeForm" })){%>
<div class="wrapper main">
<span class="bigtext align-center">New Episode<%= Model.PatientId != Guid.Empty ? ": " + Model.DisplayName : "" %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column"><div class="row"><label for="New_Episode_PatientId" class="float-left">Patient:</label><div class="float-right"><%= Html.Hidden("PatientId", Model.PatientId.ToString(), new { @id = "New_Episode_PatientId"})%><%=Model.DisplayName %></div></div></div>
        <div class="column"><div class="row"><label for="New_Episode_AdmissionId" class="float-left">Start Of Care Date:</label><div class="float-right"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "New_Episode_AdmissionId" })%> </div></div></div>
    </fieldset>
    <span id="newEpisodeTip"><% if (Model != null && Model.EndDate != DateTime.MinValue){  %><label class="bold" >Tip:</label><em> Last Episode end date is :- <%= Model.EndDate.ToString("MM/dd/yyyy") %></em><% } %></span>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row"><label for="New_Episode_TargetDate" class="float-left">Episode Start Date:</label><div class="float-right"><input type="text" class="date-picker required" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>" onchange="Schedule.newEpisodeStartDateOnChange()" id="New_Episode_StartDate" /></div></div>
            <div class="row"><label for="New_Episode_EndDate" class="float-left">Episode End Date:</label><div class="float-right"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="New_Episode_EndDate" /></div></div>
            <div class="row"><label for="New_Episode_CaseManager" class="float-left">Case Manager:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), new { @id = "New_Episode_CaseManager", @class = "Users required valid" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Episode_PrimaryPhysician" class="float-left">Primary Physician:</label><div class="float-right"><%= Html.TextBox("Detail.PrimaryPhysician", Model.PrimaryPhysician, new { @id = "New_Episode_PrimaryPhysician", @class = "Physicians" })%></div></div>
            <div class="row"><label for="New_Episode_PrimaryInsurance" class="float-left">Primary Insurance:</label><div class="float-right"><%= Html.InsurancesByBranch("Detail.PrimaryInsurance", Model.PrimaryInsurance, Model.AgencyLocationId, false, new { @id = "New_Episode_PrimaryInsurance", @class = "Insurances requireddropdown valid" })%></div></div>
            <div class="row"><label for="New_Episode_SecondaryInsurance" class="float-left">Secondary Insurance:</label><div class="float-right"><%= Html.InsurancesByBranch("Detail.SecondaryInsurance", Model.SecondaryInsurance, Model.AgencyLocationId, false, new { @id = "New_Episode_SecondaryInsurance", @class = "Insurances" })%></div></div>
        </div>
    </fieldset>
    <fieldset><legend>Comments <span class="img icon note-blue"></span><span style="font-weight: normal;">(Blue Sticky Note)</span></legend><div class="wide-column"><div class="row"><textarea id="New_Episode_Comments" name="Detail.Comments" rows="10"  cols=""></textarea></div></div></fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<%} %>
<script type="text/javascript">
//    var endEpisodeDate= Date.parse("<%= Model.EndDateFormatted %>");
//    var admissionDate = Date.parse("<%= Model.AdmissionDates.Count > 0 ? Model.AdmissionDates[0].Text : string.Empty %>");
    Schedule.WarnEpisodeGap("New_Episode");
    $("#New_Episode_PrimaryPhysician").PhysicianInput(); 
</script>