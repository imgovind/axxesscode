﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody> 
        <tr>
            <th colspan="4">
                Vital Signs
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Diagnosis
            </th>
            <th colspan="2">
                PLOF and Medical History
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Living Situation
            </th>
            <th colspan="2">
                Homebound Reason
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %>                
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">
                Functional Mobility Key
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                ADLs/Functional Mobility Level/Level of Assist
            </th>
            <th colspan="2">
                Physical Assessment
            </th>
        </tr>
        <tr>
            <td colspan="2" rowspan="3">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Pain Assessment
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
            </td>
            
        </tr>
       <tr>
            <th colspan="4">Sensory/Perceptual Skills</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Cognitive Status/Comprehension</th>
            <th colspan="2">Motor Components (Enter Appropriate Response)</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/FormRev1.ascx", Model); %></td>
        </tr>
       <tr>
            <th colspan="4">
                Assessment
            </th>
       </tr>
       <tr>
            <td colspan="4">
                <%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
                <%= Html.Templates(Model.Type + "_GenericAssessmentMoreTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericAssessmentMore" })%>
                <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"),4,20, new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" })%>
            </td>
       </tr>
        <tr>
            <td colspan="4">
                <div>Indicate all factors influencing the patient's progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.</div>
                <div><%= Html.TextArea(Model.Type + "_GenericFactors", data.AnswerOrEmptyString("GenericFactors"), new { @class = "fill", @id = Model.Type + "_GenericFactors" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div>Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician's professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)</div>
                <div><%= Html.TextArea(Model.Type + "_GenericExpectations", data.AnswerOrEmptyString("GenericExpectations"), new { @class = "fill", @id = Model.Type + "_GenericExpectations" })%></div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div>Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.</div>
                <div><%= Html.TextArea(Model.Type + "_GenericRecommendations", data.AnswerOrEmptyString("GenericRecommendations"), new { @class = "fill", @id = Model.Type + "_GenericRecommendations" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="4">Standardized test</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTStandardizedTest/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">
                Care Coordination
            </th>
            <th colspan="2">
                Skilled Treatment Provided This Visit
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericCareCoordinationTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCareCoordination" })%>
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.Templates(Model.Type + "_GenericSkilledTreatmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSkilledTreatmentProvided" })%>
                <%= Html.TextArea(Model.Type + "_GenericSkilledTreatmentProvided", data.ContainsKey("GenericSkilledTreatmentProvided") ? data["GenericSkilledTreatmentProvided"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericSkilledTreatmentProvided", @class = "fill" })%>
            </td>
        </tr>
        
    </tbody>
</table>
