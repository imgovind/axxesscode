﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody> 
        <tr>
            <th colspan="4">
                Vital Signs
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Diagnosis
            </th>
            <th colspan="2">
                Status and History
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Pain Assessment
            </th>
            <th colspan="2">
                Homebound Reason
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">
                Functional Mobility Key
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                ADLs/Functional Mobility Level/Level of Assist
            </th>
            <th colspan="2">
                Physical Assessment
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev2.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/OTPhysicalAssessment/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Living Situation
            </th>
            <th colspan="2">
                Neuromuscular Reeducation
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LivingSituation/FormRev1.ascx", Model); %>
            </td>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/NeuromuscularReeducation/FormRev1.ascx", Model); %>
            </td>
        </tr>
       
        <tr>
            <th colspan="4">
                Assessment
            </th>
        </tr>
        <tr>
            <td colspan="4">
                <%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
                <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"), new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" })%>
            </td>
        </tr>
       
        <tr>
            <th colspan="2">
                Care Coordination
            </th>
            <th colspan="2">
                Skilled Treatment Provided This Visit
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%>
            </td>
            <td colspan="2">
                <%= Html.TextArea(Model.Type + "_GenericSkilledTreatmentProvided", data.ContainsKey("GenericSkilledTreatmentProvided") ? data["GenericSkilledTreatmentProvided"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericSkilledTreatmentProvided", @class = "fill" })%>
            </td>
        </tr>
        <tr>
            <th colspan="4">Sensory/Perceptual Skills</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Sensory/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Cognitive Status/Comprehension</th>
            <th colspan="2">Motor Components (Enter Appropriate Response)</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/CognitiveStatus/FormRev1.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MotorComponents/FormRev1.ascx", Model); %></td>
        </tr>
    </tbody>
</table>
