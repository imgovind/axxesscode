﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericMotorComponents = data.AnswerArray("GenericMotorComponents"); %>
<table>
    <tbody>
        <tr>
            <th>
                Fine Motor Coordination
            </th>
            <td>
                Right<%= Html.TextBox(Model.Type + "_GenericFineMotorR", data.AnswerOrEmptyString("GenericFineMotorR"), new { @class = "vitals", @id = Model.Type + "_GenericFineMotorR" })%>
            </td>
            <td>
                <%= Html.Sensory(Model.Type + "_GenericFineMotorSensoryR", data.AnswerOrEmptyString("GenericFineMotorSensoryR"), new { @id = Model.Type + "_GenericFineMotorSensoryR", @class = "sensory" })%>
            </td>
        </tr>
        
        <tr>
            <td></td>
            <td>
                Left<%= Html.TextBox(Model.Type + "_GenericFineMotorL", data.AnswerOrEmptyString("GenericFineMotorL"), new { @class = "vitals", @id = Model.Type + "_GenericFineMotorL" })%>
            </td>
            <td>
                <%= Html.Sensory(Model.Type + "_GenericFineMotorSensoryL", data.AnswerOrEmptyString("GenericFineMotorSensoryL"), new { @id = Model.Type + "_GenericFineMotorSensoryL", @class = "sensory" })%>
            </td>
        </tr>
        <tr>
            <th>
                Gross Motor Coordination
            </th>
            <td>
                Right<%= Html.TextBox(Model.Type + "_GenericGrossMotorR", data.AnswerOrEmptyString("GenericGrossMotorR"), new { @class = "vitals", @id = Model.Type + "_GenericGrossMotorR" })%>
            </td>
            <td>
                <%= Html.Sensory(Model.Type + "_GenericGrossMotorSensoryR", data.AnswerOrEmptyString("GenericGrossMotorSensoryR"), new { @id = Model.Type + "_GenericGrossMotorSensoryR", @class = "sensory" })%>
            </td>
        </tr>
        
        <tr>
            <td></td>
            <td>
                Left<%= Html.TextBox(Model.Type + "_GenericGrossMotorL", data.AnswerOrEmptyString("GenericGrossMotorL"), new { @class = "vitals", @id = Model.Type + "_GenericGrossMotorL" })%>
            </td>
            <td>
                <%= Html.Sensory(Model.Type + "_GenericGrossMotorSensoryL", data.AnswerOrEmptyString("GenericGrossMotorSensoryL"), new { @id = Model.Type + "_GenericGrossMotorSensoryL", @class = "sensory" })%>
            </td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericMotorComponents' class='radio' name='{1}_GenericMotorComponents' value='1' type='checkbox' {0} />", genericMotorComponents.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericMotorComponents">
                    Right handed</label>
                <%= string.Format("<input id='{1}_GenericMotorComponents' class='radio' name='{1}_GenericMotorComponents' value='2' type='checkbox' {0} />", genericMotorComponents.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericMotorComponents">
                    Left handed</label>
                <%= string.Format("<input id='{1}_GenericMotorComponents' class='radio' name='{1}_GenericMotorComponents' value='3' type='checkbox' {0} />", genericMotorComponents.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericMotorComponents">
                    Orthosis</label>
                <%= string.Format("<input id='{1}_GenericMotorComponents' class='radio' name='{1}_GenericMotorComponents' value='4' type='checkbox' {0} />", genericMotorComponents.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericMotorComponents">
                    Used</label>
                <%= string.Format("<input id='{1}_GenericMotorComponents' class='radio' name='{1}_GenericMotorComponents' value='5' type='checkbox' {0} />", genericMotorComponents.Contains("5").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericMotorComponents">
                    Needed(specify)</label>
                <%= Html.TextBox(Model.Type + "_GenericMotorComponentsSpecify", data.AnswerOrEmptyString("GenericMotorComponentsSpecify"), new { @class = "", @id = Model.Type + "_GenericMotorComponentsSpecify" })%>
            </td>
        </tr>
        <tr>
            <td>
                Comments
                <%= Html.Templates(Model.Type + "_GenericMotorTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericMotorComments" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%= Html.TextArea(Model.Type + "_GenericMotorComments", data.AnswerOrEmptyString("GenericMotorComments"),4,20, new { @class = "fill", @id = Model.Type + "_GenericMotorComments" })%>
            </td>
        </tr>
    </tbody>
</table>
