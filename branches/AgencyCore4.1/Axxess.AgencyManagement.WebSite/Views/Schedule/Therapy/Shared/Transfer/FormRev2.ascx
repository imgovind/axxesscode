﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <tbody>
        <tr>
            <th></th>
            <th class="strong">Assistance</th>
            <th class="strong">Assistive Device</th>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferBedChairAssistance" class="strong">Bed-Chair</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferBedChairAssistance", data.AnswerOrEmptyString("GenericTransferBedChairAssistance"), new { @id = Model.Type + "_GenericTransferBedChairAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferBedChairAssistiveDevice", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice"), new { @id = Model.Type + "_GenericTransferBedChairAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairBedAssistance" class="strong">Chair-Bed</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairBedAssistance", data.AnswerOrEmptyString("GenericTransferChairBedAssistance"), new { @id = Model.Type + "_GenericTransferChairBedAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairBedAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairBedAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairToWCAssistance" class="strong">Chair to W/C</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairToWCAssistance", data.AnswerOrEmptyString("GenericTransferChairToWCAssistance"), new { @id = Model.Type + "_GenericTransferChairToWCAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairToWCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistance" class="strong">Toilet or BSC</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferToiletOrBSCAssistance", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferCanVanAssistance" class="strong">Car/Van</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferCanVanAssistance", data.AnswerOrEmptyString("GenericTransferCanVanAssistance"), new { @id = Model.Type + "_GenericTransferCanVanAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferCanVanAssistiveDevice", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice"), new { @id = Model.Type + "_GenericTransferCanVanAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferTubShowerAssistance" class="strong">Tub/Shower</label></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericTransferTubShowerAssistance", data.AnswerOrEmptyString("GenericTransferTubShowerAssistance"), new { @id = Model.Type + "_GenericTransferTubShowerAssistance", @class = "fill" })%></td>
            <td><%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTubShowerAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice"), new { @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice", @class = "fill" })%></td>
        </tr>
        <tr>
            <th></th>
            <th class="strong">Static</th>
            <th class="strong">Dynamic</th>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic" class="strong">Sitting Balance</label></td>
            <td><%= Html.StaticBalance(Model.Type + "_GenericTransferSittingBalanceStatic", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic"), new { @id = Model.Type + "_GenericTransferSittingBalanceStatic", @class = "fill" })%></td>
            <td><%= Html.DynamicBalance(Model.Type + "_GenericTransferSittingDynamicAssist", data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist"), new { @id = Model.Type + "_GenericTransferSittingDynamicAssist", @class = "fill" })%></td>
        </tr>
        <tr>
            <td class="align-left"><label for="<%= Model.Type %>_GenericTransferStandBalanceStatic" class="strong">Stand Balance</label></td>
            <td><%= Html.StaticBalance(Model.Type + "_GenericTransferStandBalanceStatic", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic"), new { @id = Model.Type + "_GenericTransferStandBalanceStatic", @class = "fill" })%></td>
            <td><%= Html.DynamicBalance(Model.Type + "_GenericTransferStandBalanceDynamic", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic"), new { @id = Model.Type + "_GenericTransferStandBalanceDynamic", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>
<div>
    <label for="<%= Model.Type %>_GenericTransferComment" class="strong">Comment</label>
    <%= Html.Templates(Model.Type + "_GenericTransferTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTransferComment" })%>
    <%= Html.TextArea(Model.Type + "_GenericTransferComment", data.ContainsKey("GenericTransferComment") ? data["GenericTransferComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericTransferComment", @class = "fill" })%>
</div>
