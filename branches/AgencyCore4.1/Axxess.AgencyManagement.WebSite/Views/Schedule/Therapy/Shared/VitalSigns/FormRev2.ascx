﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed">
    <thead>
        <tr>
            <th class="strong"><label for="<%= Model.Type %>_GenericBloodPressure">SBP</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericBloodPressurePer">DBP</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericPulse">HR</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericResp">Resp</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericTemp">Temp</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericWeight">Weight</label></th>
            <th class="strong"><label for="<%= Model.Type %>_GenericWeight">O<sub>2</sub> Saturation (optional)</label></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><%= Html.TextBox(Model.Type + "_GenericBloodPressure", data.AnswerOrEmptyString("GenericBloodPressure"), new { @class = "fill", @id = Model.Type + "_GenericBloodPressure" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericBloodPressurePer", data.AnswerOrEmptyString("GenericBloodPressurePer"), new { @class = "fill", @id = Model.Type + "_GenericBloodPressurePer" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericPulse", data.AnswerOrEmptyString("GenericPulse"), new { @class = "fill", @id = Model.Type + "_GenericPulse" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @class = "fill", @id = Model.Type + "_GenericResp" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @class = "fill", @id = Model.Type + "_GenericTemp" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @class = "fill", @id = Model.Type + "_GenericWeight" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericO2Sat", data.AnswerOrEmptyString("GenericO2Sat"), new { @class = "fill", @id = Model.Type + "_GenericO2Sat" })%></td>
        </tr>
    </tbody>
</table>