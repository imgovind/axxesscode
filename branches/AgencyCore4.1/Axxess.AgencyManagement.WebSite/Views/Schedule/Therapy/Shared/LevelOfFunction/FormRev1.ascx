﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div>
    <label for="<%= Model.Type %>_GenericPriorFunctionalStatus" class="strong">Prior Level Of Function:</label>
    <%= Html.Templates(Model.Type + "_PriorFunctionalTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPriorFunctionalStatus" })%>
    <%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus", @class = "fill" })%>
</div>
<div class="clear"></div>

<div>
    <label for="<%= Model.Type %>_GenericMedicalHistory" class="strong">Current Level Of Function:</label>
    <%= Html.Templates(Model.Type + "_CurrentFunctionalTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericCurrentFunctionalStatus" })%>
    <%= Html.TextArea(Model.Type + "_GenericCurrentFunctionalStatus", data.AnswerOrEmptyString("GenericCurrentFunctionalStatus"), new { @id = Model.Type + "_GenericCurrentFunctionalStatus", @class = "fill" })%>
</div>
