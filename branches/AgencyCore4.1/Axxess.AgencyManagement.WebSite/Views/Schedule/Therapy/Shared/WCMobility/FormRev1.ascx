﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var noteDiscipline = data.AnswerOrEmptyString("DisciplineTask"); %>
<table class="fixed">
    <tbody>
        <tr>
            <th><label for="<%= Model.Type %>_GenericWCMobilityLevel">Level</label></th>
            <th><label for="<%= Model.Type %>_GenericWCMobilityRamp">Uneven</label></th>
        </tr>
        <tr>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityLevel", data.AnswerOrEmptyString("GenericWCMobilityLevel"), new { @id = Model.Type + "_GenericWCMobilityLevel", @class = "" })%></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityUneven", data.AnswerOrEmptyString("GenericWCMobilityUneven"), new { @id = Model.Type + "_GenericWCMobilityUneven", @class = "" })%></td>
        </tr>
        <tr>
            <th><label for="<%= Model.Type %>_GenericWCMobilityManeuver">Maneuver</label></th>
            <th><label for="<%= Model.Type %>_GenericWCMobilityADL" class="strong">ADL</label></th>
        </tr>
        <tr>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityManeuver", data.AnswerOrEmptyString("GenericWCMobilityManeuver"), new { @id = Model.Type + "_GenericWCMobilityManeuver", @class = "" })%></td>
            <td><%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityADL", data.AnswerOrEmptyString("GenericWCMobilityADL"), new { @id = Model.Type + "_GenericWCMobilityADL", @class = "" })%></td>
        </tr>
    </tbody>
</table>
