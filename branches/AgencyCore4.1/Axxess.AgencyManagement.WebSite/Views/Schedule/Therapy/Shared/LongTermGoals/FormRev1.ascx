﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table>
    <thead>
        <tr>
            <th colspan="2"></th>
            <th class="strong">Time Frame</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal1", data.AnswerOrEmptyString("GenericLongTermGoal1"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal1" }) %></td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal1TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal1TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal1TimeFrame" })%></td>
        </tr>
        <tr>
            <td>2.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal2", data.AnswerOrEmptyString("GenericLongTermGoal2"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal2" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal2TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal2TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal2TimeFrame" })%></td>
        </tr>
        <tr>
            <td>3.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal3", data.AnswerOrEmptyString("GenericLongTermGoal3"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal3" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal3TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal3TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal3TimeFrame" })%></td>
        </tr>
        <tr>
            <td>4.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal4", data.AnswerOrEmptyString("GenericLongTermGoal4"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal4" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal4TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal4TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal4TimeFrame" })%></td>
        </tr>
        <tr>
            <td>5.</td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal5", data.AnswerOrEmptyString("GenericLongTermGoal5"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal5" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericLongTermGoal5TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal5TimeFrame"), new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal5TimeFrame" })%></td>
        </tr>
    </tbody>
</table>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericLongTermFrequency" class="float-left">Frequency:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLongTermFrequency", data.AnswerOrEmptyString("GenericLongTermFrequency"), new { @id = Model.Type + "_GenericLongTermFrequency" })%></div>
</div>
<div class="padnoterow">
    <label for="<%= Model.Type %>_GenericLongTermDuration" class="float-left">Duration:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericLongTermDuration", data.AnswerOrEmptyString("GenericLongTermDuration"), new { @class = "", @id = Model.Type + "_GenericLongTermDuration" })%></div>
</div>