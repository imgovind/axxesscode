﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>


<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">Vital Signs</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/FormRev2.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Subjective</th>
        </tr>
        <tr>
            <td colspan="4">
            <%= Html.Templates(Model.Type + "_GenericSubjectiveTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericSubjective" })%>
            <%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"),4,20, new { @id = Model.Type + "_GenericSubjective", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="2">Homebound</th>
            <th colspan="2">Functional Limitations/Problem Areas</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev2.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/FormRev3.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">Pain Assessment</th>
            <th colspan="2">Teaching</th>
        </tr>
        <tr>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %></td>
            <td colspan="2">
                <%  string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
                <input type="hidden" name=Model.Type + "_GenericTeaching" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching1' class='float-left radio' name='{1}_GenericTeaching' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching1" class="radio">Patient/Family</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching2' class='float-left radio' name={1}_GenericTeaching' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching2" class="radio">Caregiver</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching3' class='float-left radio' name='{1}_GenericTeaching' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching3" class="radio">Correct Use of Adaptive Equipment</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching4' class='float-left radio' name='{1}_GenericTeaching' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching4" class="radio">Safety Technique</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching5' class='float-left radio' name='{1}_GenericTeaching' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching5" class="radio">ADLs</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching6' class='float-left radio' name='{1}_GenericTeaching' value='6' type='checkbox' {0} />", genericTeaching.Contains("6").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching6" class="radio">HEP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching7' class='float-left radio' name='{1}_GenericTeaching' value='7' type='checkbox' {0} />", genericTeaching.Contains("7").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching7" class="radio">Correct Use of Assistive Device</label>
                            </td>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other: (modalities, DME/AE need, consults, etc)</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther" })%></div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><%= Html.Templates(Model.Type + "_GenericTeachingCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTeachingComment" })%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <%= Html.TextArea(Model.Type + "_GenericTeachingComment", data.AnswerOrEmptyString("GenericTeachingComment"), 4, 20, new { @id = Model.Type + "_GenericTeachingComment", @class = "fill" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="4">Functional mobility key</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/MobilityKey/FormRev1.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="2">ADL Training</th>
            <th colspan="2">Therapeutic/Dynamic Activities</th>
        </tr>
        <tr>
            <td colspan="2" rowspan="5">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/FormRev3.ascx", Model); %>
            </td>
            <td colspan="2">
                <table>
                    <tbody>
                        <tr>
                            <th></th>
                            <th colspa="2">Assistance x Reps</th>
                            <th>Assistive Device</th>
                        </tr>
                        <tr>
                            <td>Bed Mobility</td>
                            <td colspa="2">
                                <%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistance", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistance"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistance", @class = "" })%>
                                x<%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssistanceReps", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssistanceReps" })%>
                            </td>
                            <td>
                                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDevice", @class = "" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>Bed/WC transfer</td>
                            <td colspa="2">
                                <%= Html.TherapyAssistance(Model.Type + "_GenericBedWCTransferAssistance", data.AnswerOrEmptyString("GenericBedWCTransferAssistance"), new { @id = Model.Type + "_GenericBedWCTransferAssistance", @class = "" })%>
                                x<%= Html.TextBox(Model.Type + "_GenericBedWCTransferAssistanceReps", data.AnswerOrEmptyString("GenericBedWCTransferAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericBedWCTransferAssistanceReps" })%>
                            </td>
                            <td>
                                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedWCTransferAssistiveDevice", data.AnswerOrEmptyString("GenericBedWCTransferAssistiveDevice"), new { @id = Model.Type + "_GenericBedWCTransferAssistiveDevice", @class = "" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>Toilet transfer</td>
                            <td colspa="2">
                                <%= Html.TherapyAssistance(Model.Type + "_GenericToiletTransferAssistance", data.AnswerOrEmptyString("GenericToiletTransferAssistance"), new { @id = Model.Type + "_GenericToiletTransferAssistance", @class = "" })%>
                                x<%= Html.TextBox(Model.Type + "_GenericToiletTransferAssistanceReps", data.AnswerOrEmptyString("GenericToiletTransferAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericToiletTransferAssistanceReps" })%>
                            </td>
                            <td>
                                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericToiletTransferAssistiveDevice"), new { @id = Model.Type + "_GenericToiletTransferAssistiveDevice", @class = "" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>Tub/Shower transfer</td>
                            <td colspa="2">
                                <%= Html.TherapyAssistance(Model.Type + "_GenericTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericTubShowerTransferAssistance"), new { @id = Model.Type + "_GenericTubShowerTransferAssistance", @class = "" })%>
                                x<%= Html.TextBox(Model.Type + "_GenericTubShowerTransferAssistanceReps", data.AnswerOrEmptyString("GenericTubShowerTransferAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericTubShowerTransferAssistanceReps" })%>
                            </td>
                            <td>
                                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericTubShowerTransferAssistiveDevice"), new { @id = Model.Type + "_GenericTubShowerTransferAssistiveDevice", @class = "" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>Other</td>
                            <td colspa="2">
                                <%= Html.TherapyAssistance(Model.Type + "_GenericTherapeuticOtherAssistance", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistance"), new { @id = Model.Type + "_GenericTherapeuticOtherAssistance", @class = "" })%>
                                x<%= Html.TextBox(Model.Type + "_GenericTherapeuticOtherAssistanceReps", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistanceReps"), new { @class = "sn", @id = Model.Type + "_GenericTherapeuticOtherAssistanceReps" })%>
                            </td>
                            <td>
                                <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTherapeuticOtherAssistiveDevice", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistiveDevice"), new { @id = Model.Type + "_GenericTherapeuticOtherAssistiveDevice", @class = "" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">Therapeutic Exercise</th>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <label for="<%= Model.Type %>_GenericROMTo" class="float-left">ROM To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericROMTo", data.AnswerOrEmptyString("GenericROMTo"), new { @id = Model.Type + "_GenericROMTo", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericROMToReps", data.AnswerOrEmptyString("GenericROMToReps"), new { @id = Model.Type + "_GenericROMToReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericActiveTo" class="float-left">Active To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericActiveTo", data.AnswerOrEmptyString("GenericActiveTo"), new { @id = Model.Type + "_GenericActiveTo", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericActiveToReps", data.AnswerOrEmptyString("GenericActiveToReps"), new { @id = Model.Type + "_GenericActiveToReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericAssistive" class="float-left">Active/Assistive To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericAssistive", data.AnswerOrEmptyString("GenericAssistive"), new { @id = Model.Type + "_GenericAssistive", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericAssistiveReps", data.AnswerOrEmptyString("GenericAssistiveReps"), new { @id = Model.Type + "_GenericAssistiveReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericManual" class="float-left">Resistive, Manual, To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericManual", data.AnswerOrEmptyString("GenericManual"), new { @id = Model.Type + "_GenericManual", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericManualReps", data.AnswerOrEmptyString("GenericManualReps"), new { @id = Model.Type + "_GenericManualReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericResistiveWWeights" class="float-left">Resistive, w/Weights, To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericResistiveWWeights", data.AnswerOrEmptyString("GenericResistiveWWeights"), new { @id = Model.Type + "_GenericResistiveWWeights", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericResistiveWWeightsReps", data.AnswerOrEmptyString("GenericResistiveWWeightsReps"), new { @id = Model.Type + "_GenericResistiveWWeightsReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericStretchingTo" class="float-left">Stretching To:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericStretchingTo", data.AnswerOrEmptyString("GenericStretchingTo"), new { @id = Model.Type + "_GenericStretchingTo", @class = "loc" })%>
                        x
                        <%= Html.TextBox(Model.Type + "_GenericStretchingToReps", data.AnswerOrEmptyString("GenericStretchingToReps"), new { @id = Model.Type + "_GenericStretchingToReps", @class = "loc" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericObjectiveComment" class="float-left">Comment:</label>
                </div>
                <div>
                    <%= Html.Templates(Model.Type + "_GenericObjectiveTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericObjectiveComment" })%>
                    <%= Html.TextArea(Model.Type + "_GenericObjectiveComment", data.AnswerOrEmptyString("GenericObjectiveComment"),5,20, new { @id = Model.Type + "_GenericObjectiveComment", @class = "fill" })%>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="2">Supervisory Visit (Complete if applicable)</th>
        </tr>
        <tr>
            <td colspan="2">
                <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SupervisoryVisit/FormRev1.ascx", Model); %>
            </td>
        </tr>
        <tr>
            <th colspan="4">Assessment</th>
        </tr>
        <tr>
            
            <td colspan="4">
            <%= Html.Templates(Model.Type + "_GenericAssessmentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericAssessment" })%>
            <%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"),8,20, new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="2">W/C Mobility &#8212;
                <%  string[] isWCMobilityApplied = data.AnswerArray("GenericIsWCMobilityApplied"); %>
                <%= Html.Hidden(Model.Type + "isWCMobilityApplied", string.Empty, new { @id = Model.Type + "_GenericIsWCMobilityAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsWCMobilityApplied1' name='{0}_GenericIsWCMobilityApplied' value='1' type='checkbox' {1} />", Model.Type, isWCMobilityApplied.Contains("1").ToChecked())%>
                <label for="<%= Model.Type %>_GenericIsWCMobilityApplied1">N/A</label></th>
            <th colspan="2">Plan</th>
        </tr>
        <tr>
            <td colspan="2">
                <div id="<%= Model.Type %>_WCMobilityContainer"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %></div>
            </td>
            <td colspan="2">
                <div>
                    <label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="float-left">Continue Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @id = Model.Type + "_GenericContinuePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="float-left">Change Prescribed Plan:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @id = Model.Type + "_GenericChangePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericPlanDischarge" class="float-left">Plan Discharge:</label>
                    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @id = Model.Type + "_GenericPlanDischarge", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Comment:</label>
                <%= Html.Templates(Model.Type + "_GenericPlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericPlanComment" })%>
                <div><%= Html.TextArea(Model.Type + "_GenericPlanComment", data.AnswerOrEmptyString("GenericPlanComment"), new { @id = Model.Type + "_GenericPlanComment", @class = "fill" })%></div>
            
            </td>
        </tr>
        <tr>
            <th colspan="4">Progress made towards goals</th>
        </tr>
        <tr>
            <td colspan="4">
            <%= Html.Templates(Model.Type + "_GenericProgressTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericProgress" })%>
            <%= Html.TextArea(Model.Type + "_GenericProgress", data.AnswerOrEmptyString("GenericProgress"),5,20, new { @id = Model.Type + "_GenericProgress", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="4">Skilled treatment provided this visit</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev3.ascx", Model); %></td>
        </tr>
    </tbody>
</table>