<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.ViewViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
<div class="post_view">
    <div class="title"><%= Html.Encode(Model.Post.Title) %></div>
    <div class="content"><%= Model.Post.Body %></div>
    <div class="creation">
        <span class="block"><strong>Post by:</strong> <%= Html.Encode(Model.Post.CreatedBy)%></span>
        <span class="block"><strong>On:</strong> <%= Model.Post.CreatedDate.ToString("MM/dd/yyyy HH:mm tt") %></span>
    </div>
    <div class="reply_section">
        <div class="title">Replies</div>
        <div class="replies"><%
        foreach (var item in Model.Post.Replies) { %>
            <div class="reply">
                <div class="content"><%= item.Body%></div>
                <div class="creation">
                    <span class="block"><strong>Reply by:</strong> <%= Html.Encode(item.CreatedBy)%></span>
                    <span class="block"><strong>On:</strong> <%= item.CreatedDate.ToString("MM/dd/yyyy HH:mm tt") %></span>
                </div>
            </div>
        <% } %>
        </div>
    </div>
    <ul class="post_controls">
        <li class="first"><%= Html.ActionLink("Back", "index") %></li>
        <li><%= Html.ActionLink("Reply", "Reply", new { postId = Model.Post.Id, title = ForumViewHelper.ToUrlFriendlyTitle(Model.Post.Title) })%></li>
    </ul>
</div>
</asp:Content>