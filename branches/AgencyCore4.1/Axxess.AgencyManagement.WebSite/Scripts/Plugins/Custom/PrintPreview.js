﻿(function($) {
    $.extend($.fn, {
        Chart: function(Options) {
            return this.each(function() {
                var $chart = $(this),
                    Count = 0;
                $chart.append(
                    $("<tr/>")
                );
                if (Options.Label) {
                    $chart.find("tr:last").append(
                        $("<td/>", { "colspan": (Options.Display == "block" ? String(Options.Cols) : "1"), "class": "label", "text": Options.Label })
                    )
                    if (Options.Display == "block") $chart.append("<tr/>")
                }
                if (Options.ColWidths) {
                    var sum = 0;
                    for (var i = 0; i < Options.Cols; i++) sum += Options.ColWidths[i];
                    var multiplier = 100 / sum;
                    for (var i = 0; i < Options.Cols; i++) Options.ColWidths[i] = Options.ColWidths[i] * multiplier;
                }
                for (var i = 0; i < Options.Subquestion.length; i++) {
                    if (!i && !i % Options.Cols) $chart.append(
                        $("<tr/>")
                    );
                    $chart.find("tr:last").append(
                        $("<td/>", { "style": "width:" + (Options.ColWidths ? Options.ColWidths[Count] + "%" : "auto") }).Question(Options.Subquestion[i])
                    );
                }
            })
        },
        Checkbox: function(Options) {
            return this.each(function() {
                var $checkbox = $(this),
                    $render;
                if (Options.Data === Options.Value) var Checked = true;
                else var Checked = false;
                if (Options.Subquestion && Checked) {
                    var $subquestion = new Array();
                    for (var i = 0; i < Options.Subquestion.length; i++) $subquestion[i] = $("<td/>").Question(Options.Subquestion[i]);
                }
                $render = $("<span/>", { "class": "checkbox", "html": (Checked ? "X" : "&#160;") }).after(
                    $("<label/>", { "html": Options.Label })
                );
                if ($subquestion) {
                    $checkbox.append(
                        $("<table/>").append(
                            $("<tbody/>").append(
                                $("<tr/>").append(
                                    $("<td/>").append(
                                        $render
                                    )
                                )
                            )
                        )
                    );
                    for (var i = 0; i < $subquestion.length; i++) {
                        if (Options.Display != "inline") $checkbox.find("tbody").append(
                            $("<tr/>")
                        );
                        $checkbox.find("tr:last").append(
                            $subquestion[i].attr("colspan", (Options.Label ? "2" : "1"))
                        )
                    }
                } else $checkbox.append($render);
            })
        },
        CheckGroup: function(Options) {
            return this.each(function() {
                var $checkgroup = $(this)
                if (Options.Label) {
                    if (Options.Oasis && $("body").hasClass("oasis")) Options.Label = "(" + Options.Oasis + ") " + Options.Label;
                    $checkgroup.append(
                        $("<tr/>").append(
                            $("<td/>", { "class": "label", "text": Options.Label })
                        )
                    );
                    if (Options.Display != "inline") $checkgroup.append(
                        $("<tr/>")
                    );
                } else $checkgroup.append(
                    $("<tr/>")
                );
                $checkgroup.find("tr:last").append(
                    $("<td/>").append(
                        $("<table/>").append(
                            $("<tbody/>").OptionsField(Options)
                        )
                    )
                )
            })
        },
        Label: function(Options) {
            return this.each(function() {
                var $label = $(this);
                $label.append(
                    $("<tr/>").append(
                        $("<td/>", { "class": "label" + (Options.Align == "center" ? " ac" : (Options.Align == "right" ? " ar" : "")), "text": Options.Label })
                    )
                )
                if (Options.Instructions) $label.append(
                    $("<tr/>").append(
                        $("<td/>", { "class": "instructions" + (Options.Align == "center" ? " ac" : (Options.Align == "right" ? " ar" : "")), "text": Options.Label })
                    )
                )
            })
        },
        Loading: function(Start) {
            return this.each(function() {
                if (Start) $(this).append(
                    $("<div/>", { "id": "loading" })
                );
                else $(this).find("#loading").remove();
            })
        },
        OptionsField: function(Options) {
            return this.each(function() {
                var $optionfield = $(this);
                if (Options.Display == "inline") $optionfield.append("<tr/>");
                for (var i = 0; i < Options.Option.length; i++) {
                    if (Options.Display != "inline" && i % Options.Cols == 0) $optionfield.append("<tr/>");
                    if (!Options.Option[i].Data) Options.Option[i].Data = Options.Data;
                    $optionfield.find("tr:last").append(
                        $("<td/>").Checkbox(Options.Option[i])
                    )
                }
            })
        },
        Multiple: function(Options) {
            return this.each(function() {
                var $multiple = $(this),
                    Count = 0;
                $multiple.append(
                    $("<tr/>")
                );
                if (Options.Oasis && $("body").hasClass("oasis")) Options.Label = "(" + Options.Oasis + ") " + Options.Label;
                if (Options.Label) $multiple.find("tr:last").append(
                    $("<td/>", { "rowspan": (Options.Display == "onecol" ? "1" : String(Options.Subquestion.Length)), "class": "label", "text": Options.Label })
                );
                $.each(Options.Subquestion, function(Index, Value) {
                    if (Count) $multiple.append(
                        $("<tr/>")
                    );
                    $multiple.find("tr:last").append(
                        $("<td/>").Question(Value)
                    );
                    Count++;
                })
            })
        },
        Page: function(Options) {
            return this.each(function() {
                var $page = $(this);
                $page.addClass("page");
                $.each(Options, function(Index, Value) {
                    $page.append(
                        $("<div/>", { "class": Index, "html": Value })
                    )
                })
            })
        },
        PrintPreview: function(Options) {
            return this.each(function() {
                var $print = $(this),
                    $content = $("<div/>", { "class": "content" });
                if (Options.IsOasis) $print.addClass("oasis");
                for (var i = 0; i < Options.Xml.length; i++) $content.append(
                    $("<table/>").Section(Options.Xml[i])
                );
                $print.append(
                    $("<div/>").Page(Options.Fields)
                );
                $content.clone().appendTo(".page");
                var PageHeight = $print.find(".content").height();
                do {
                    var Split = -1,
                        Total = $content.find(".section .section").length;
                    $print.find(".content:last .section .section").each(function() {
                        if ($(this).position().top < PageHeight) Split++;
                        else return;
                    });
                    for (var i = 0; i < Total; i++) {
                        if (i < Split) $content.find(".section .section:first").remove();
                        else $print.find(".page:last .content").find(".section .section").eq(Split).remove();
                    }
                    $print.find(".content:last").find(".section").not(".section .section").each(function() {
                        if ($(this).find(".section").length == 0) $(this).remove();
                    });
                    $content.find(".section").not(".section .section").each(function() {
                        if ($(this).find(".section").length == 0) $(this).remove();
                    });
                    if ($content) {
                        $print.append(
                            $("<div/>").Page(Options.Fields)
                        );
                        $content.clone().appendTo(".page:last");
                    }
                } while ($content);
                $print.find(".page").each(function() {
                    $(this).append(
                        $("<div/>", { "class": "pagenum", "text": "Page " + parseInt($(".page").index($(this)) + 1) + " of " + $print.find(".page").length })
                    )
                })
            })
        },
        Question: function(Options) {
            return this.each(function() {
                var $question = $(this);
                $question.addClass("question pad").append(
                    $("<table/>").append(
                        $("<tbody/>")
                    )
                );
                if (Options.Type == "text" || Options.Type == "date" || Options.Type == "state" || Options.Type == "phone") $question.find("tbody").Text(Options);
                if (Options.Type == "select") $question.find("tbody").Select(Options);
                if (Options.Type == "textarea") $question.find("tbody").TextArea(Options);
                if (Options.Type == "radio" || Options.Type == "checkgroup") $question.find("tbody").CheckGroup(Options);
                if (Options.Type == "multiple") $question.find("tbody").Multiple(Options);
                if (Options.Type == "chart") $question.removeClass("pad").find("tbody").Chart(Options);
                if (Options.Type == "label") $question.find("tbody").Label(Options);
                if (Options.Type == "title") $question.find("tbody").Title(Options);
                if (Options.Type == "notacheck" || Options.Type == "checkbox" || Options.Type == "radioopt") $question.find("tbody").Checkbox(Options);
            })
        },
        Section: function(Options) {
            return this.each(function() {
                var $section = $(this), $current;
                $section.addClass("section").append("<tbody/>");
                if (Options.Label) $section.prepend(
                    $("<thead/>").append(
                        $("<tr/>").append(
                            $("<th/>", { "colspan": Options.Cols, "text": Options.Label })
                        )
                    )
                )
                if (Options.Subsection) {
                    for (var i = 0; i < Options.Subsection.length; i++) {
                        if (i % Options.Cols == 0) {
                            $section.find("tbody").append("<tr/>");
                            $current = $section.find("tbody:first tr:last");
                        }
                        $current.append(
                            $("<td/>").append(
                                $("<table/>").Section(Options.Subsection[i])
                            )
                        )
                    }
                    if (!parseInt(Options.Subsection.length / Options.Cols)) for (var i = Options.Subsection.length % Options.Cols; i > -1; i--) $current.append(
                        $("<td/>")
                    )
                }
                if (Options.Question) {
                    for (var i = 0; i < Options.Question.length; i++) {
                        if (i % Options.Cols == 0) {
                            $section.find("tbody").append("<tr/>");
                            $current = $section.find("tbody:first tr:last");
                        }
                        $current.append(
                            $("<td/>").Question(Options.Question[i])
                        )
                    }
                    if (!parseInt(Options.Question.length / Options.Cols)) for (var i = Options.Question.length % Options.Cols; i > -1; i--) $current.append(
                        $("<td/>")
                    )
                }
            })
        },
        Select: function(Options) {
            return this.each(function() {
                var $select = $(this),
                    $subquestion = new Array();
                $.each(Options.Option, function(Index, Value) {
                    if (Options.Data === Value.Value) {
                        if (Value.Subquestion) for (var i = 0; i < Value.Subquestion.length; i++) $subquestion[i] = $("<td/>").Question(Value.Subquestion[i]);
                        Options.Data = Value.Label;
                    }
                });
                $select.Text(Options);
                if ($subquestion.length) for (var i = 0; i < $subquestion.length; i++) $select.append(
                    $("<tr/>").append(
                        $subquestion[i].attr("colspan", (Optiosn.Label ? "2" : "1"))
                    )
                )
            })
        },
        TextArea: function(Options) {
            return this.each(function() {
                var $textarea = $(this);
                if (Options.Data == null || !Options.Data.length) {
                    Options.Data = $("<span/>", { "class": "blank", "html": "&#160;" }).after(
                        $("<span/>", { "class": "blank", "html": "&#160;" }));
                    for (var i = 2; i < Options.Length; i++) Options.Data.after(
                        $("<span/>", { "class": "blank", "html": "&#160;" }));
                }
                $textarea.append(
                    $("<tr/>").append(
                        $("<td/>", { "class": "label", "html": Options.Label })
                    )
                ).append(
                    $("<tr/>").append(
                        $("<td/>", { "class": "textarea data", "html": Options.Data })
                    )
                )
            })
        },
        Text: function(Options) {
            return this.each(function() {
                var $text = $(this);
                if (Options.Oasis && $("body").hasClass("oasis")) Options.Label = "(" + Options.Oasis + ") " + Options.Label;
                if (Options.Data == null || !Options.Data.length) Options.Data = $("<span/>", { "class": "blank", "html": "&#160;" });
                $text.append(
                    $("<tr/>")
                );
                if (Options.Label) $text.find("tr").append(
                        $("<td/>", { "class": "label", "html": Options.Label })
                );
                $text.find("tr").append(
                    $("<td/>", { "class": "data", "html": Options.Data })
                );
            })
        },
        Title: function(Options) {
            return this.each(function() {
                Options.Align = "center";
                $(this).Label(Options);
            })
        }
    })
})(jQuery);