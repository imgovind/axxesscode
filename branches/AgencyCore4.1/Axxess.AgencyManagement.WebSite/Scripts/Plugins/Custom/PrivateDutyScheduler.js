(function($) {
    $.extend($.fn, {
        PrivateDutyScheduler: function(command) {
            return this.each(function() {
                // Refresh Tasks
                if (command === "refresh") {
                    if ($(this).fullCalendar("getView").title != "month") $(this).fullCalendar("changeView", "month");
                    if ($(this).fullCalendar("getDate").getMonth() != new Date().getMonth()) $(this).fullCalendar("today");
                    else $(this).fullCalendar("refetchEvents");
                // New Scheduler
                } else {
                    // Find Aspect Ratio for available space
                    var ratio = $(this).closest(".ui-layout-center").width() / ($(this).closest(".ui-layout-center").height() - 110);
                    // Initialize the Plugin
                    $(this).fullCalendar({
                        aspectRatio: ratio,
                        dragOpacity: 0.5,
                        editable: true,
                        eventAfterRender: function(event, element, view) {
                            $(".fc-event-title", element).html($(".fc-event-title", element).text());
                            $(element).attr("guid", event.id);
                            $(element).append(
                                $("<div/>").addClass("task-controls").append(
                                    $("<span/>").addClass("img icon edit-comment").click(function() {
                                        PrivateDuty.Task.Edit($(this).closest("[guid]").attr("guid"))
                                    })).append(
                                    $("<span/>").addClass("img icon delete-comment").click(function() {
                                        PrivateDuty.Task.Delete($(this).closest("[guid]").attr("guid"))
                                    })).append(
                                    $("<span/>").addClass("img icon print-comment")
                                )
                            ).mouseover(function() { $(".task-controls", element).show() }).mouseout(function() { $(".task-controls", element).hide() });
                            $(".task-controls", element).hide();
                        },
                        eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
                            if (Acore.Confirm("Are you sure you want to move this task?")) {
                                U.PostUrl("PrivateDuty/Task/Update", {
                                    Id: event.id,
                                    PatientId: PrivateDuty.ScheduleCenter.PatientId,
                                    IsAllDay: event.allDay,
                                    EventStartTime: { Date: U.FormatDate(event.start), Time: U.FormatTime(event.start) },
                                    EventEndTime: { Date: U.FormatDate(event.end), Time: U.FormatTime(event.end) }
                                }, function(result) {
                                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error")
                                    if (!result.isSuccessful) revertFunc();
                                })
                            } else revertFunc();
                        },
                        eventResize: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
                            if (Acore.Confirm("Are you sure you want change the scheduled time on this task?")) {
                                U.PostUrl("PrivateDuty/Task/Update", {
                                    Id: event.id,
                                    PatientId: PrivateDuty.ScheduleCenter.PatientId,
                                    EventStartTime: { Date: U.FormatDate(event.start), Time: U.FormatTime(event.start) },
                                    EventEndTime: { Date: U.FormatDate(event.end), Time: U.FormatTime(event.end) }
                                }, function(result) {
                                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error")
                                    if (!result.isSuccessful) revertFunc();
                                })
                            } else revertFunc();
                        },
                        events: function(start, end, callback) {
                            // Get Task Data
                            if (U.IsGuid(PrivateDuty.ScheduleCenter.PatientId)) U.PostUrl("PrivateDuty/Task/List", {
                                startDate: U.FormatDate(start),
                                endDate: U.FormatDate(end),
                                patientId: PrivateDuty.ScheduleCenter.PatientId
                            }, function(result) {
                                // Parse Data and Pass it to Events JSON
                                var events = [];
                                $(result).each(function() {
                                    var s = parseInt($(this).attr("Status"));
                                    if (s == 425 || s == 330 || s == 220 || s == 225 || s == 135 || s == 455) back = "#3ea123", border = "#173b0d";
                                    else if (s == 400) back = "#b20e0e", border = "#4d0606";
                                    else back = "#3e799c", border = "#152a36";
                                    events.push({
                                        id: $(this).attr("Id"),
                                        title: "<strong>" + $(this).attr("DisciplineTaskName") + "</strong> &#8211; " + $(this).attr("UserName"),
                                        allDay: $(this).attr("IsAllDay"),
                                        start: U.ConvertJsonDateToJavascriptDate($(this).attr("EventStartTime")),
                                        end: U.ConvertJsonDateToJavascriptDate($(this).attr("EventEndTime")),
                                        backgroundColor: back,
                                        borderColor: border
                                    });
                                });
                                callback(events);
                            }, function() {
                                // If Error on Getting Data
                                U.Growl("There was an error in getting your events.  Please try again.", "error")
                            });
                        },
                        header: { left: "prev,next today", center: "title", right: "month,agendaWeek,agendaDay" },
                        loading: function(isLoading, view) {
                            // Toggle Loading Class
                            if (isLoading) $(view).addClass("loading");
                            else $(view).removeClass("loading");
                        },
                        titleFormat: {
                            // Overload Title Format to Allow Patient Name
                            month: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMMM yyyy",
                            week: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMM d[ yyyy]{'&#8211;'[ MMM]d, yyyy}",
                            day: "'<span class=\"fc-patient-name\"></span> &#8212; 'dddd, MMM d, yyyy"
                        },
                        viewDisplay: function(view) {
                            // Set Patient Name
                            $(view.element).closest(".pd-calendar").find(".fc-patient-name").text(PrivateDuty.ScheduleCenter.PatientName);
                        },
                        windowResize: function(view) {
                            // Reset Aspect Ratio upon Resize
                            $(this).fullCalendar("option", "aspectRatio", $("#PDScheduleMainResult").width() / ($("#PDScheduleMainResult").height() - 110));
                        }
                    });
                    // Fix Styling
                    $(".fc-button-effect", this).remove();
                    $(".fc-button-content", this).each(function() {
                        $(this).text($(this).text().replace(/\b./g, function(m) { return m.toUpperCase(); }))
                    });
                }
            })
        }
    })
})(jQuery);