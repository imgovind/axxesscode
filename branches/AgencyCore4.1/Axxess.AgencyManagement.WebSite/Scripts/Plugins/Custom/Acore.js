﻿var Acore = {
    AlertId: "Alert",                   // Default acore id for alert window
    AgencyId: "",                       // Current Agency ID
    Animated: false,                    // Toggle animations on and off
    AnimationSpeed: 500,                // Set the speed of the animations
    AutocompleteId: 0,                  // Autocomplete iterator
    CascadeGap: 25,                     // Pixel gap for window cascading
    Cookies: false,                     // Toggle cookies on and off
    CssTop: 0,                          // CSS top iterator, used for window cascading
    CssLeft: 0,                         // CSS left iterator, used for window cascading
    DefaultWindow: "",                  // Window to open automatically on activate
    DefaultWinHeight: "85%",            // Default window height
    DefaultWinWidth: "85%",             // Default window width
    DialogId: "Dialog",                 // Default acore id for dialog window
    GetRemoteContent: true,             // Enable remote content loading
    IdleTime: 0,                        // System idle timer
    MaxWindows: 8,                      // Maximum allowed number of concurrent open windows
    MinWinHeight: 350,                  // Minimum height of any window (may be overloaded per window)
    MinWinWidth: 1000,                  // Minimum width of any window (may be overloaded per window)
    Mobile: false,                      // Mobile site switch
    OpenWindows: 0,                     // Open window counter
    OpenWindowsCookie: "OpenWindows",   // Name for Open Windows cookie
    PrintId: 0,                         // Print lightbox iterator
    ReturnReasonId: "ReturnReason",     // Default acore id for return reason
    Timeout: 45,                        // Auto-logout to occur at this cycle number
    TimeoutCount: 5,                    // Number of cycles to display auto-logout warning
    TimeoutInterval: (60 * 1000),       // Auto-logout interval (cycle length in milliseconds)
    UserId: "",                         // Current User ID
    WindowBorder: "0",                  // Window border ## TO BE .25em WHEN NEW STYLE IMPLEMENTED ##
    Windows: {},                        // Registry of available windows
    PingTimer: null,                    // Timer for keep-alive pings
    TimeoutTimer: null,                 // Timer for idle timeout counting
    AddMenu: function(Options) {
        //  Options:
        //      Name    Display name for the menu
        //      Id      DOM identifier for the menu
        //      Parent  DOM identifier for the parent menu
        //      IconX   Pixel position in sprite for menu icon X axis
        //      IconY   Pixel position in sprite for menu icon Y axis
        if (Options.Name != null && (Options.Parent == null || $("#" + Options.Parent).length)) {
            // Create basic menu option
            var menu =
                $("<li/>").append(
                    $("<a/>", { "class": "menu-trigger", text: Options.Name })).append(
                    $("<ul/>", { id: Options.Id == undefined ? Options.Name.replace(/[^0-9a-zA-Z]/, "") : Options.Id, "class": "menu" })
                );
            // If icon x position in sprite defined, add icon (default y is 86)
            if (Options.IconX != null) $("a", menu).prepend(
                $("<span/>", { "class": "img icon", style: "background-position:-" + (parseInt(Options.IconX) > 0 ? parseInt(Options.IconX) : 0) + "px -" + (parseInt(Options.IconY) > 0 ? parseInt(Options.IconY) : "86") + "px;" })
            );
            // If it is located in a submenu, add the more icon
            if (Options.Parent != null && Options.Parent != "mainmenu") $("a", menu).append(
                $("<span/>", { "class": "img icon more" })
            );
            // Add menu into main or submenu depending upon the Parent option
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(menu);
        }
    },
    AddMenuItem: function(Options) {
        //  Options:
        //      Id      Window identifier
        //      Name    Name for menu item
        //      Href    URL for menu link
        //      Parent  DOM identifier for parent menu
        // If external link
        if (Options.Name != null && Options.Href != null) $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
            $("<li/>").append(
                $("<a/>", { href: Options.Href, target: "_blank", text: Options.Name })
            )
        );
        // If internal window load
        if (Options.Id != null && this.Windows[Options.Id] != null) $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
            $("<li/>").click(function() {
                Acore.Open(Options.Id, Acore.Windows[Options.Id].Url, Acore.Windows[Options.Id].OnLoad)
            }).append(
                $("<a/>", { text: this.Windows[Options.Id].MenuName })
            )
        );
    },
    AddWindow: function(Options) {
        //  Options:
        //      AlwaysOnTop     Bool switch to keep window above all other windows
        //      Center          Bool switch to display window centered as opposed to cascading
        //      Content         Alternate to URL, pass DOM object to append to window content
        //      Height          Overloaded window default height
        //      IconX           Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY           Pixel position in sprite for Window/Taskbar icon Y axis
        //      Id              Identifier for window in acore's window registry
        //      IgnoreMinSize   Bool switch to ignore the minimum height and width for window
        //      Input           Variables to post to Url
        //      Maximizable     Bool switch to allow window to be maximized
        //      MaxOnMobile     Bool switch to auto-maximize windows on mobile devices
        //      Menu            DOM identifier for parent menu (leave null if not on menu, pass array if in multiple menus)
        //      MenuName        Name as seen on the top navigation bar menus
        //      MinHeight       Minimum height for the window
        //      MinWidth        Minimum width for the window
        //      Modal           Bool switch to shade out background
        //      Name            Master window name
        //      OnLoad          Window onLoad/Callback event
        //      Status          Default text seen on the status bar of the window
        //      StatusBar       Bool switch to display window with or without the status bar
        //      TaskName        Name as seen on the bottom task bar
        //      Resize          Bool switch to allow or disallow window resizing
        //      Url             AJAX URL for window's content
        //      VersitileHeight Bool switch to adjust height dynamically to content size
        //      Width           Overloaded window default width
        //      WindowFrame     Bool switch to display window with or without the standard frame
        //      WindowName      Name as seen on the top of the window
        // Ensure Id and Name are set and that the Id isn't already used
        if (Options.Id != null && Options.Name != null && this.Windows[Options.Id] == null) {
            // If more specific names are not set, set them to the default name
            Options.WindowName = Options.WindowName == null ? Options.Name : Options.WindowsName;
            Options.MenuName = Options.MenuName == null ? Options.Name : Options.MenuName;
            Options.TaskName = Options.TaskName == null ? Options.Name : Options.TaskName;
            // Set defaults
            Options = $.extend({
                AlwaysOnTop: false,
                Center: false,
                Height: Acore.DefaultWinHeight,
                IconX: 220,
                IconY: 12,
                IgnoreMinSize: false,
                Maximizable: true,
                MaxOnMobile: true,
                MinHeight: Acore.MinWinHeight,
                MinWidth: Acore.MinWinWidth,
                Modal: false,
                StatusBar: true,
                Resize: true,
                VersitileHeight: false,
                Width: Acore.DefaultWinWidth,
                WindowFrame: true
            }, Options);
            // Set options
            this.Windows[Options.Id] = Options;
            // Add the items to the menu accordingly
            if (Options.Menu != null && Options.Menu.constructor == Array) for (i = 0; i < Options.Menu.length; i++) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu[i] });
            else if (Options.Menu != null) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu });
        }
    },
    Alert: function(Message) {
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId + "_content").html($("<div/>").AcoreAlert(Message)).parent().SetHeight();
        else {
            Acore.Modal({
                Alert: true,
                Name: "Notice",
                Width: 400,
                Content: $("<div/>").AcoreAlert(Message)
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    Close: function(Window) {
        var AcoreId = Window.GetAcoreId();
        // If CKEditor found in window, remove instance
        if ($(".cke_editor", Window).length) $(".cke_editor", Window).each(function() {
            var TextArea = $(this).parent().parent().parent().attr("id").replace(/cke_/, "");
            CKEDITOR.remove(CKEDITOR.instances[TextArea]);
        })
        // Set IsOpen flag to false
        Acore.Windows[AcoreId].IsOpen = false;
        // Decrement number of open windows
        Acore.OpenWindows--;
        // If temporary modal window, remove window from registry
        if (AcoreId == Acore.DialogId) Acore.RemoveWindow(Acore.DialogId);
        else if (AcoreId == Acore.AlertId) Acore.RemoveWindow(Acore.AlertId);
        else if (AcoreId == Acore.ReturnReasonId) Acore.RemoveWindow(Acore.ReturnReasonId);
        // If not temporary window, and cookies are enabled remove from open windows cookie
        else if (Acore.Cookies) {
            var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
            for (var i = 0; i < OpenWindows.length; i++) if (OpenWindows[i].Id == AcoreId) OpenWindows.splice(i--, 1);
            if (OpenWindows.length) U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
            else U.Cookie.Delete(Acore.OpenWindowsCookie);
        }
        // Remove task bar from DOM
        if (Acore.Animated) Window.GetTask().animate({ width: 0 }, Acore.AnimationSpeed, function() { Window.GetTask().remove() });
        else Window.GetTask().remove();
        // Remove window from DOM
        if (Acore.Animated) {
            var CenterX = Window.width() / 2 + Window.position().left, CenterY = Window.height() / 2 + Window.position().top;
            Window.find(".window-content").empty();
            Window.animate({ width: 0, height: 0, left: CenterX, top: CenterY }, Acore.AnimationSpeed, function() { Window.remove() });
        } else Window.remove();
        // If background is shaded due to window currently being closed, remove shade
        if ($("#shade").length) {
            if ((Acore.Windows[Acore.DialogId] == undefined && Acore.Windows[Acore.AlertId] == undefined) || (Acore.Windows[Acore.DialogId] != undefined && Acore.Windows[Acore.AlertId] == undefined && !Acore.Windows[Acore.DialogId].Modal)) {
                if (Acore.Animated) $("#shade").animate({ opacity: 0 }, Acore.AnimationSpeed, function() { $("#shade").remove() });
                else $("#shade").remove();
            } else $("#window_" + Acore.DialogId).show().addClass("active");
        }
    },
    CloseAlert: function() {
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId).Close();
    },
    CloseDialog: function() {
        if (Acore.Windows[Acore.DialogId] != undefined) $("#window_" + Acore.DialogId).Close();
    },
    Confirm: function(Options) {
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId + "_content").html($("<div/>").AcoreConfirm(Options)).parent().SetHeight();
        else {
            Acore.Modal({
                Alert: true,
                Name: Options.Name ? Options.Name : "Question",
                Width: Options.Width ? Options.Width : 400,
                Content: $("<div/>").AcoreConfirm(Options)
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    Dialog: function(Options) {
        //  Options, standard window plus:
        //      Alert           Bool switch to use alert id
        //      ReturnReason    Bool switch to use return reason id
        // Ensure that Modal option is always set
        if (Options.Modal == undefined) Options.Modal = false;
        // Set defaults based upon style of dialog
        if (Options.Alert || Options.ReturnReason) Options.Id = Options.Alert ? Acore.AlertId : Acore.ReturnReasonId, Options.AlwaysOnTop = true, Options.Alert = null, Options.ReturnReason = null;
        else Options.Id = Acore.DialogId;
        if (Options.Height == null) Options.VersitileHeight = true;
        // Set defaults and overwrite where applicable
        Options = $.extend({
            Center: true,
            IgnoreMinSize: true,
            Name: "Axxess Agencycore",
            Resize: false,
            StatusBar: false,
            Width: 400,
            WindowFrame: !Options.Modal
        }, Options);
        // Add window
        Acore.AddWindow(Options);
        // Open modal window
        Acore.Open(Options.Id, Options.Url, Options.OnLoad, Options.Input);
    },
    IncrementIdle: function() {
        // Add to idle time
        Acore.IdleTime++;
        // If idle for long enough to start the logout count down
        if (Acore.IdleTime + Acore.TimeoutCount >= Acore.Timeout) {
            // If there is already a dialog window open, close it first
            if (Acore.Windows[Acore.AlertId] != null) $("#window_" + Acore.AlertId).Close();
            // Open AcoreIdle Modal
            Acore.Modal({
                Alert: true,
                Name: "Idle Logout",
                Content: $("<div/>").AcoreIdle()
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    Modal: function(Options) {
        Acore.Dialog($.extend(Options, { Modal: true }));
    },
    OnLoad: function(Element) {
        // Global OnLoad Function
        if (Element.hasClass("window")) var Window = Element;
        else var Window = Element.closest(".window");
        // Rename Window
        Element.Rename();
        // Initialize special inputs
        Element.InitForms();
        // Add hover functions on mouseover fields
        if ($(".reports li,.acore-grid ol li,.cal tbody td:not(.inactive),.hover-toggle", Element).length) $(".reports li,.acore-grid ol li,.cal tbody td:not(.inactive),.hover-toggle", Element).mouseover(function() {
            $(this).addClass("hover")
        }).mouseout(function() {
            $(this).removeClass("hover")
        });
        // Set Status Messages
        if ($("input[type=text]:not([status]),input[type=password]:not([status]),select:not([status])", Element).length) $("input[type=text]:not([status]),input[type=password]:not([status]),select:not([status])", Element).each(function() {
            $(this).attr("status", $(this).closest(".row").find("label").text() + ($(this).hasClass("required") ? " (Required)" : ""))
        });
        if ($(".buttons a:not([status])", Element).length) $(".buttons a:not([status])", Element).each(function() {
            $(this).attr("status", $(this).text())
        });
        if ($(".winstatus", Element).length) Window.Status($(".winstatus", Element).text());
        if ($("[status]", Element).length) $("[status]", Element).mouseover(function() {
            $(this).closest(".window").Status($(this).attr("status"))
        }).mouseout(function() {
            $(this).closest(".window").Status($(this).closest(".window").find(".winstatus").length ? $(this).closest(".window").find(".winstatus").text() : "")
        });
        // Init Tooltips
        if ($("[tooltip]", Element).length) ToolTip.Init(Element);
        // Telerick Grid placement and manipulations
        if ($(".t-group-indicator", Element).length) $(".t-group-indicator", Element).hide();
        if ($(".t-grouping-header", Element).length) $(".t-grouping-header", Element).remove();
        if ($(".grid-search", Element).length) $(".grid-search", Element).GridSearch();
        if ($(".grid-controls", Window).length && $(".t-grid", Window).length) setTimeout(function() {
            if (Acore.Animated) $(".t-grid", Window).animate({ top: $(".wrapper", Window).prop("offsetHeight") }, Acore.AnimationSpeed);
            else $(".t-grid", Window).css("top", $(".wrapper", Window).prop("offsetHeight"));
        }, Acore.Animated ? Acore.AnimationSpeed : 10);
        // Acore Grid Zebra Striping
        if ($(".acore-grid", Element).length || (Element.hasClass("acore-grid") && $("ol", Element).length)) {
            if (Element.hasClass("acore-grid") && $("ol", Element).length) var grid = Element;
            else var grid = $(".acore-grid", Element);
            $("ol", grid).Zebra();
            if ($("ul .sortable", grid).length) $("ul .sortable", grid).click(function() {
                var numbered = $(this).closest("li").find(".grid-number").length,
                    invert = $(this).hasClass("sorted"),
                    eq = $(this).index(),
                    list = $(this).closest("ul").next("ol");
                list.addClass("loading");
                if ($(this).hasClass("sorted")) $(this).removeClass("sorted").addClass("inverted-sorted");
                else if ($(this).hasClass("inverted-sorted")) $(this).removeClass("inverted-sorted").addClass("sorted");
                else {
                    $(this).closest("li").find(".sorted,.inverted-sorted").removeClass("sorted inverted-sorted");
                    $(this).addClass("sorted");
                }
                $(this).closest("li").find(".ui-icon-circle-triangle-n,.ui-icon-circle-triangle-s").remove();
                if (invert) $(this).append($("<span/>").addClass("fr img icon ui-icon-circle-triangle-n"));
                else $(this).append($("<span/>").addClass("fr img icon ui-icon-circle-triangle-s"));
                setTimeout(function() {
                    var items = list.children("li").get();
                    items.sort(function(a, b) {
                        var child = $(a).children("span:eq(" + eq + ") .sort-value").length ? "span:eq(" + eq + ") .sort-value" : "span:eq(" + eq + ")";
                        return $(a).children(child).text().trim().toUpperCase().localeCompare($(b).children(child).text().trim().toUpperCase());
                    });
                    if (invert) items.reverse();
                    $.each(items, function(index, item) { list.append(item) });
                    if (numbered) $("li", list).each(function(index, item) { $(".grid-number", this).text(String(index + 1) + ".") });
                    list.Zebra().removeClass("loading");
                }, 50);
            });
        }
    },
    Open: function(Id, Url, OnLoad, Inputs) {
        if (typeof Id == "object") var Parameters = Id;
        else {
            var Parameters = { Id: Id };
            if (typeof Url == "function") Parameters.OnLoad = Url, Parameters.Url = undefined;
            if (typeof Url == "object") Parameters.Inputs = Url, Parameters.Url = undefined;
            if (typeof Inputs == "object") Parameters.Inputs = Inputs, Parameters.Url = Url;
            if (typeof OnLoad == "object") Parameters.Inputs = OnLoad, Parameters.OnLoad = undefined;
        }
        // Ensure the top menus are all collapsed
        $("ul.menu").hide();
        // If window does not exists
        if (this.Windows[Id] == null) U.Growl("Not able to find window: " + Id + ".  Please contact Axxess for assistance in troubleshooting.", "error");
        // If window exists in registry
        else {
            // If window is already open, focus and growl
            if (this.Windows[Id].IsOpen) {
                U.Growl("You already have a open window for " + this.Windows[Id].Name + ". Please close down this window before opening another one like it.", "error");
                $("#window_" + Id).WinFocus();
                // If opening window will not hit max window threshold
            } else if (this.OpenWindows >= this.MaxWindows) {
                U.Growl("You can only have " + this.MaxWindows + " concurrent windows at once. Please close down some of your windows to proceed", "error");
                // Good to open
            } else {
                // Set window options
                var Options = {};
                $.extend(Options, Acore.Windows[Parameters.Id]);
                Options.Id = Parameters.Id;
                // Set window overrides
                if (typeof Parameters.Url == "string") Options.Url = Parameters.Url;
                if (typeof Parameters.OnLoad == "function") Options.OnLoad = Parameters.OnLoad;
                if (typeof Parameters.Inputs == "object") Options.Inputs = Parameters.Inputs;
                /* ## UNCOMMENT TO FORCE ALL WINDOWS TO USE POST INSTEAD OF GET ##
                else Options.Inputs = {}; */
                // Add window onto the desktop
                $("#desktop").append(
                    $("<div/>").AcoreWindow(Options)
                );
                // If not a temporary window and cookies are enabled, add to Open Windows cookie
                if (Id != Acore.AlertId && Id != Acore.DialogId && Id != Acore.ReturnReasonId && Acore.Cookies) {
                    var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
                    if (!OpenWindows) OpenWindows = new Array();
                    var push = true;
                    for (var i = 0; i < OpenWindows.length; i++) if (OpenWindows[i].Id == Parameters.Id) push = false;
                    if (push) OpenWindows.push(Parameters);
                    U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
                }
                if (!Acore.Mobile || !Options.MaxOnMobile) {
                    // If window is smaller than minimum sizes, set to minimum size
                    if (!Options.IgnoreMinSize && $("#window_" + Options.Id).width() < Options.MinWidth) $("#window_" + Options.Id).css("width", Options.MinWidth);
                    if (!Options.IgnoreMinSize && $("#window_" + Options.Id).height() < Options.MinHeight) $("#window_" + Options.Id).css("height", Options.MinHeight);
                    // If window is larger than desktop, auto-maximize window
                    if (($("#window_" + Options.Id).width() > $("#desktop").width()) || ($("#window_" + Options.Id).height() > $("#desktop").height())) $("#window_" + Options.Id).Maximize();
                    // If window is to be cascaded, set top and left accordingly
                    if (!Options.Center) {
                        // Increment left and top counters, and if window can't fit reset to zero
                        var Width = Options.Width, Height = Options.Height;
                        if (!Width) Width = parseInt(Acore.DefaultWinWidth.indexOf("%") > 0 ? parseInt($("#desktop").width() * (parseInt(Acore.DefaultWinWidth.replace(/[^0-9]/, "")) / 100)) : parseInt(Acore.DefaultWinWidth.replace(/[^0-9]/, "")));
                        if (!Height) Height = parseInt(Acore.DefaultWinHeight.indexOf("%") > 0 ? parseInt($("#desktop").height() * (parseInt(Acore.DefaultWinHeight.replace(/[^0-9]/, "")) / 100)) : parseInt(Acore.DefaultWinHeight.replace(/[^0-9]/, "")));
                        if (++Acore.CssLeft * Acore.CascadeGap + Width > $("#desktop").width()) Acore.CssLeft = 0;
                        if (++Acore.CssTop * Acore.CascadeGap + Height > $("#desktop").height()) Acore.CssTop = 0;
                        // Set top and left accordingly
                        $("#window_" + Options.Id).css({
                            top: Acore.CssTop * Acore.CascadeGap,
                            left: Acore.CssLeft * Acore.CascadeGap
                        });
                    }
                }
                // Set IsOpen flag to true
                this.Windows[Id].IsOpen = true;
            }
        }
    },
    OpenPrintView: function(Options) {
        //  Options:
        //      Url         URL to print preview
        //      PdfUrl      URL to fetch PDF
        //      PdfData     Data for PDF Print POST
        //      ReturnClick Function for return button
        //      Buttons     Buttons array
        //          Text        Button text
        //          Click       Button click function
        // Open AcorePrintView modal
        Acore.Modal({
            Name: "Print View",
            Height: "95%",
            Width: 950,
            WindowFrame: false,
            Content: $("<div/>").AcorePrintView(Options)
        })
        $("#window_" + Acore.DialogId).Center();
    },
    Ping: function() {
        // Make AJAX POST to ping server
        U.PostUrl("Ping", null, function(data) {
            if (!data.isSuccessful) $(location).attr("href", "SessionExpired");
        })
    },
    RemoveWindow: function(Id) {
        // If window is found, remove from Acore window information registry
        if (this.Windows[Id].Menu == undefined) this.Windows[Id] = null;
        // Else growl to user that window cannot be found
        else U.Growl("Cannot remove a window which is referenced in the main menu.", "error");
    },
    ReturnReason: function(EventId, EpisodeId, PatientId, ReturnFunction) {
        if (Acore.Windows[Acore.ReturnReasonId] != undefined) $("#window_" + Acore.ReturnReasonId + "_content").ReturnComments("Refresh");
        else {
            Acore.AddWindow({
                AlwaysOnTop: true,
                Center: false,
                Content: $("<div/>", { "class": "wrapper main" }).ReturnComments({ EventId: EventId, EpisodeId: EpisodeId, PatientId: PatientId, ReturnFunction: ReturnFunction }),
                Height: 375,
                Id: Acore.ReturnReasonId,
                IgnoreMinSize: true,
                Name: "Return Reason",
                Minimizable: typeof ReturnFunction != "function",
                Modal: false,
                Resize: false,
                ReturnReason: true,
                StatusBar: false,
                Width: 400
            });
            Acore.Open(Acore.ReturnReasonId);
        }
    }
};
(function($) {
    $.extend($.fn, {
        AcoreAlert: function(Message) {
            return this.each(function() {
                $(this).AcoreDialog(Message, [
                    {
                        Text: "OK",
                        Click: function() {
                            $(this).closest(".window").Close();
                            return false;
                        }
                    }
                ])
            })
        },
        AcoreConfirm: function(Options) {
            return this.each(function() {
                $(this).AcoreDialog(Options.Message, [
                    {
                        Text: "Yes",
                        Click: function() {
                            if (typeof Options.Yes == "function") try {
                                Options.Yes();
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error")
                            }
                            $(this).closest(".window").Close();
                            return false;
                        }
                    }, {
                        Text: "No",
                        Click: function() {
                            if (typeof Options.No == "function") try {
                                Options.No();
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error")
                            }
                            $(this).closest(".window").Close();
                            return false;
                        }
                    }
                ])
            })
        },
        AcoreDialog: function(Message, Buttons) {
            return this.each(function() {
                $(this).append(
                    $("<div/>", { id: "dialog-modal", "class": "wrapper main ac strong", html: Message }).prepend(
                        $("<div/>").addClass("fl").append(
                            $("<span/>").addClass("img icon info")
                        )).append(
                        $("<div>").addClass("clr")).append(
                        $("<div/>").Buttons(Buttons)
                    )
                ).closest(".window").SetHeight();
            })
        },
        AcoreDesktop: function() {
            return this.each(function() {
                // If loaded in frame, reload out of the frame
                if (window.location !== window.top.location) window.top.location = window.location;
                // If mobile device detected, set mobile flag to true and import stylesheet
                var browser = navigator.userAgent;
                if (browser.match(/Android/i) || browser.match(/webOS/i) || browser.match(/iPhone/i) || browser.match(/iPod/i) || browser.match(/iPad/i)) Acore.Mobile = true;
                if (Acore.Mobile)
                    $("head").append(
                        $("<link/>", { rel: "stylesheet", type: "text/css", href: "Content/handheld.css" })
                    );
                // Build DOM for desktop
                $(this).append(
                    $("<div/>", { id: "desktop" }).bind("contextmenu", function(Event) {
                        $("#mainmenu").clone(true).ContextMenu(Event);
                    })).append(
                    $("<div/>", { id: "bar-top" }).css({ "z-index": parseInt(Acore.MaxWindows + 1), top: -64 }).append(
                        $("<span/>", { "class": "fr" }).append(
                            $("<a/>", { "text": "Logout" }).click(function() {
                                Acore.Confirm({
                                    Message: "Are your sure you want to logout?",
                                    Yes: function() {
                                        $(location).attr("href", "Logout");
                                    }
                                });
                                return false;
                            }).prepend(
                                $("<span/>", { "class": "img icon", style: "background-position:-208px -84px" })))).append(
                        $("<ul/>", { "id": "mainmenu" })).append(
                        $("<div/>", { "id": "tab", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) + ";" })).append(
                    )).append(
                    $("<div/>", { "id": "bar-bottom" }).css({ "z-index": parseInt(Acore.MaxWindows + 1), bottom: -32 }).append(
                        $("<ul/>", { "id": "task" })).append(
                        $("<span/>", { "class": "copy", "html": "&#169; 2009 &#8211; " + (new Date).getFullYear() + " Axxess&#8482; Technology Solutions, All Rights Reserved" })
                    )
                );
                // Set keyboard shortcuts
                $(document).keypress(function(e) {
                    // ALT+R JavaScript run prompt
                    if (e.which == 114 && e.altKey) eval(prompt("Run:\nRun the following JavaScript command:", ""));
                    // ALT+W Open acore window by id
                    if (e.which == 119 && e.altKey) eval("Acore.Open('" + prompt("Load Window:\nOpen the following AgencyCore window:", "") + "')");
                    // ALT+L Logout of software
                    if (e.which == 108 && e.altKey) if (confirm("Are you sure you want to logout?")) $(location).attr(href, "/Logout");
                    // ALT+X Close current acore window
                    if (e.which == 120 && e.altKey) if ($(".window.active").length) $(".window.active").Close();
                });
                // Bind mouse and keyboard events to trigger resetting idle counter
                $(document).bind("mousemove keydown DOMMouseScroll mousewheel mousedown", function() {
                    Acore.IdleTime = 0;
                });
                // Set thread to keep server connection alive
                Acore.PingTimer = setInterval(Acore.Ping, Acore.TimeoutInterval);
                // Set thread to count idle time
                Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                // Set Resize Event to Reposition Grids Under Controls
                $(window).resize(function() {
                    $(".grid-controls").closest(".wrapper").each(function() {
                        $(".t-grid", this).css("top", $(this).prop("offsetHeight"))
                    })
                });
                // Finalize desktop
                $("#mainmenu").Menu();
                if (Acore.Animated) {
                    $("#bar-top").animate({ top: 0 }, Acore.AnimationSpeed);
                    $("#bar-bottom").animate({ bottom: 0 }, Acore.AnimationSpeed);
                } else {
                    $("#bar-top").css("top", 0);
                    $("#bar-bottom").css("bottom", 0);
                }
                // If Cookies Enabled, Load Previous State
                if (Acore.Cookies) {
                    var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
                    if (OpenWindows) for (var i = 0; i < OpenWindows.length; i++) Acore.Open(OpenWindows[i]);
                    else if (Acore.DefaultWindow) Acore.Open(Acore.DefaultWindow);
                }
            })
        },
        // Initialize idle modal
        AcoreIdle: function() {
            return this.each(function() {
                // Stop thread to count idle time
                clearInterval(Acore.TimeoutTimer);
                // Build idle countdown modal
                $(this).AcoreDialog("Due to inactivity, you will automatically be logged off in <span id=\"timeoutsec\"></span>", [
                    {
                        Text: "Stay Logged In",
                        Click: function() {
                            // Stop thread for second by second updates
                            clearInterval(IdleCountdown);
                            // Close idle countdown modal
                            $(this).closest(".window").Close();
                            // Reset thread to count idle time
                            Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                            return false;
                        }
                    }, {
                        Text: "Logout",
                        Click: function() {
                            // Logout of software
                            $(location).attr("href", "Logout");
                            return false;
                        }
                    }
                ]);
                // Calculate auto-logout time
                var logout = new Date();
                logout.setTime(logout.getTime() + Acore.TimeoutCount * Acore.TimeoutInterval);
                // Set thread for second by second updates
                IdleCountdown = setInterval(function() {
                    // Calulate time remaining till auto-logout
                    var currentTime = new Date();
                    var remaining = new Date();
                    remaining.setTime(logout.getTime() - currentTime.getTime());
                    // If time is up, logout of software
                    if (parseInt(remaining.getMinutes()) == 59) $(location).attr("href", "SessionExpired");
                    // Else display amount of time remaining
                    else $("#timeoutsec").html(remaining.getMinutes() + ":" + (remaining.getSeconds() < 10 ? "0" + remaining.getSeconds() : remaining.getSeconds()));
                }, 1000);
            })
        },
        AcorePrintView: function(Options) {
            return this.each(function() {
                // Build basic print view
                $(this).css("overflow", "hidden").append(
                    $("<div/>", { id: "print-box" }).append(
                        $("<iframe/>", { name: "printview" + Acore.PrintId, id: "printview", "class": "loading", src: Options.Url }).load(function() {
                            $(this).removeClass("loading");
                        })
                    )).append(
                    $("<div/>", { id: "print-controls", "class": "buttons" }).append(
                        $("<ul/>").append(
                            $("<li/>").append(
                                $("<a/>", { id: "printbutton", text: "Print" })
                            )).append(
                            $("<li/>").append(
                                $("<a/>", { href: "javascript:void(0)", text: "Close" }).click(function() {
                                    $(this).closest(".window").Close();
                                })
                            )
                        )
                    )
                ); 
                // If PDF Print, set click function
                if (Options.PdfUrl != null) $("#printbutton").click(function() {
                    U.GetAttachment(Options.PdfUrl, Options.PdfData);
                });
                // If HTML Print, set click function
                else
                    $("#printbutton").attr("href", "javascript:void(0)").click(function() {
                        window.frames["printview" + Acore.PrintId].focus();
                        window.frames["printview" + Acore.PrintId].print();
                        Acore.PrintId++;
                    });
                // If return button is enabled, add return reason field and proper button functionality
                if (Options.ReturnClick != null && typeof (Options.ReturnClick) == "function") $("#print-controls ul").prepend(
                    $("<li/>").append(
                        $("<a/>", { id: "printreturn", text: "Return" }).click(function() {
                            $("ul", "#print-controls").hide();
                            Acore.ReturnReason(Options.PdfData.eventId, Options.PdfData.episodeId, Options.PdfData.patientId, Options.ReturnClick);
                        })
                    )
                )
                // If custom buttons are defined, add them to the start of the button list
                if (Options.Buttons != null) for (i = Options.Buttons.length; i > 0; i--) {
                    $("#print-controls ul").prepend(
                        $("<li/>").append(
                            $("<a/>", { href: "javascript:void(0)", text: Options.Buttons[i - 1].Text }).click(Options.Buttons[i - 1].Click)
                        )
                    )
                }
            });
        },
        AcoreTask: function(Options) {
            return this.each(function() {
                // Build task bar item
                $(this).attr({
                    id: "task_" + Options.Id,
                    "class": "task active",
                    style: "width:" + 100 / Acore.MaxWindows + "%"
                }).append(
                    $("<a/>", { href: "javascript:void(0)", title: Options.TaskName, text: Options.TaskName }).click(function() {
                        if ($(this).closest(".task").GetWindow().hasClass("active")) $(this).closest(".task").GetWindow().Minimize();
                        else $(this).closest(".task").GetWindow().WinFocus();
                    }).prepend(
                        $("<span/>", { "class": "img icon", style: "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                    )
                ).bind("contextmenu", function(Event) {
                    // Build context menu
                    var Menu = $("<ul/>");
                    var AcoreWindow = $(this).GetWindow();
                    // If not active window, add option to bring to front
                    if (!AcoreWindow.hasClass("active")) Menu.append(
                        $("<li/>", { text: "Bring to Front" }).click(function() {
                            AcoreWindow.WinFocus();
                        })
                    );
                    // If not minimized, add option to minimize
                    if (!AcoreWindow.hasClass("minimized")) Menu.append(
                        $("<li/>", { text: "Minimize" }).click(function() {
                            AcoreWindow.Minimize();
                        })
                    );
                    // If not maximized, add option to maximize
                    if (!AcoreWindow.hasClass("maximized")) Menu.append(
                        $("<li/>", { text: "Maximize" }).click(function() {
                            AcoreWindow.Maximize();
                        })
                    );
                    // If maximized, add option to restore
                    else Menu.append(
                        $("<li/>", { text: "Restore" }).click(function() {
                            AcoreWindow.Restore();
                        })
                    );
                    // Add option to close
                    Menu.append(
                        $("<li/>", { text: "Close" }).click(function() {
                            AcoreWindow.Close();
                        })
                    );
                    // Enable context menu
                    Menu.ContextMenu(Event);
                })
            })
        },
        AcoreWindow: function(Options) {
            return this.each(function() {
                if (Acore.Mobile) {
                    // Minimize all windows
                    $(".window").Minimize();
                    // Build basic mobile window
                    $(this).attr({ id: "window_" + Options.Id, "class": "window" }).append(
                        $("<div/>", { "class": "window-top" }).append(
                            $("<span/>", { "class": "float-left" }).append(
                                $("<span/>", { "class": "img icon", style: "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                            )).append(
                            $("<span/>", { "class": "abs", text: Options.WindowName })).append(
                            $("<span/>", { "class": "float-right" }).append(
                                $("<a/>", { href: "javascript:void(0)", "class": "window-min" }).click(function() { $(this).closest(".window").Minimize() })).append(
                                $("<a/>", { href: "javascript:void(0)", "class": "window-close" }).click(function() { $(this).closest(".window").Close() })
                            )
                        )).append(
                        $("<div/>", { id: "window_" + Options.Id + "_content", "class": "abs window-content loading" })
                    );
                    if (Options.MaxOnMobile) $(this).addClass("mobile-max");
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).Load(Options.Url, Options.Inputs, Options.OnLoad);
                    else $(".window-content", this).removeClass("loading");
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                } else {
                    // Build basic window
                    $(this).attr({
                        id: "window_" + Options.Id,
                        "class": "window hidden"
                    }).css("z-index", ++Acore.OpenWindows).mousedown(function() {
                        if (!$(this).hasClass("active")) $(this).WinFocus();
                    }).append(
                        $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "window-content loading" + (Options.Error ? " error" : "") })
                    );
                    // If window has frame, add top bar
                    if (Options.WindowFrame) $(".window-content", this).before(
                        $("<div/>", { "class": "window-top" }).dblclick(function() {
                            if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                            else $(this).closest(".window").Maximize();
                        }).append(
                            $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" }).dblclick(function() {
                                $(this).closest(".window").Close();
                            })
                            ).append(
                            $("<span/>", { "class": "title", "text": Options.WindowName })).append(
                            $("<span/>", { "class": "fr" }).append(
                                $("<a/>", { "class": "window-min" }).click(function() {
                                    $(this).closest(".window").Minimize();
                                    return false;
                                })).append(
                                $("<a/>", { "class": "window-resize" }).click(function() {
                                    if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                    else $(this).closest(".window").Maximize();
                                    return false;
                                })).append(
                                $("<a/>", { "class": "window-close" }).click(function() {
                                    if (Options.Id == Acore.ReturnReasonId && $("#print-controls").length) $("ul", "#print-controls").show();
                                    $(this).closest(".window").Close();
                                    return false;
                                })
                            )
                        )
                    );
                    // If unframed window, set content top accordingly
                    else $(".window-content", this).css({
                        top: Acore.WindowBorder,
                        right: Acore.WindowBorder,
                        bottom: Acore.WindowBorder,
                        left: Acore.WindowBorder
                    });
                    // If window is resizable, enable resizing
                    if (Options.Resize) $(this).resizable({
                        containment: "parent",
                        handles: "n, ne, e, se, s, sw, w, nw",
                        minWidth: Acore.MinWinWidth,
                        minHeight: Acore.MinWinHeight
                    });
                    // If window is not resizable and framed, remove resize button
                    else if (Options.WindowFrame) $(".window-resize", this).remove();
                    // If framed window has status bar, add in status bar and set content bottom accordingly
                    if (Options.WindowFrame && Options.StatusBar) {
                        $(this).append($("<div/>", { "class": "window-bottom", "text": Options.Status }));
                        $(".window-content", this).css("bottom", "21px");
                    }
                    // Set window size
                    if (parseInt(Options.Width) > 0) $(this).css("width", Options.Width);
                    if (parseInt(Options.Height) > 0) $(this).css("height", Options.Height);
                    // If modal window, shade out the background and increase the z-index
                    if (Options.Modal) {
                        if ($("#shade").length == 0) $("body").append(
                            $("<div/>", { "id": "shade", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) }).bind("contextmenu", function(Event) { Event.preventDefault() })
                        );
                        else $(".window.active").hide();
                        $(this).css({ "z-index": parseInt(Acore.MaxWindows) + 2 }).removeClass("standard");
                        // If framed non-modal window, enable draggable
                    } else if (Options.WindowFrame) $(this).draggable({
                        containment: "parent",
                        handle: ".window-top .title"
                    });
                    if (Options.MaxOnMobile) $(this).addClass("mobile-max");
                    // Fade In Animation
                    if (Acore.Animated) $(this).fadeIn(Acore.AnimationSpeed, function() { $(this).removeClass("hidden") });
                    else $(this).removeClass("hidden");
                    // Load window content
                    if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).Load(Options.Url, Options.Inputs, Options.OnLoad);
                    else $(".window-content", this).removeClass("loading");
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // If always on top, fix z-index and set class
                    if (Options.AlwaysOnTop) $(this).css("z-index", parseInt(Acore.MaxWindows + ($("#shade").length ? 2 : 1))).addClass("top-window");
                    // Bring window to focus
                    $(this).WinFocus();
                }
            })
        },
        Center: function(Animated) {
            return this.each(function() {
                if ($(this).hasClass("window")) {
                    var Window = $(this),
                        Top = ($("#desktop").height() - $(this).height()) / 2,
                        Left = ($("#desktop").width() - $(this).width()) / 2,
                        Width = $(this).css("width"),
                        Height = $(this).css("height");
                    Animated = Animated == false ? false : Acore.Animated;
                    if (Acore.Windows[Window.GetAcoreId()].VersitileHeight) {
                        if (Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
                        else Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
                        if (Animated) Window.animate({ top: Top, left: Left, width: Width, height: Height }, Acore.AnimationSpeed, function() { Window.find(".window-content").removeClass("loading") });
                        else Window.css({ top: Top, left: Left, width: Width, height: Height }).find(".window-content").removeClass("loading");
                    } else Window.css({ top: Top, left: Left });
                } else U.Growl("Error: Unexpected call to center non-window element. Cannot center element that is not a window.", "error");
            })
        },
        Close: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window && Acore.Windows[Window.GetAcoreId()].HasUnsavedChages) Acore.Confirm({
                    Message: "All unsaved changes will be lost.  Are you sure you want to proceed with closing this window?",
                    Yes: function() { Acore.Close(Window) }
                });
                else if (Window) Acore.Close(Window);
            })
        },
        ContextMenu: function(Event) {
            return this.each(function() {
                // If no other context menus are open and the DOM focus is not upon an input of type text nor textarea
                if ($(".context-menu").length == 0 && $("input.t-input:focus,input[type=text]:focus,textarea:focus").length == 0) {
                    // If no text is highlighted
                    if ((document.getSelection && document.getSelection() == "") || (document.selection && document.selection.createRange().text == "")) {
                        // Prevent the default context menu from popping up
                        Event.preventDefault();
                        // Build context menu into the DOM
                        $("body").append(
                            $("<div/>").css({
                                position: "fixed",
                                top: 0,
                                left: 0,
                                width: "100%",
                                height: "100%",
                                "z-index": 999
                            }).click(function() {
                                $(this).remove();
                            }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                                // If menu is too tall to display below the cursor, then display above
                                if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css({ bottom: $("body").height() - Event.pageY, top: "auto" });
                                else $(".context-menu").css({ top: Event.pageY, bottom: "auto" });
                                // If menu is too wide to display to the right of the cursor, then display to the left
                                if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css({ right: $("body").width() - Event.pageX, left: "auto" });
                                else $(".context-menu").css({ left: Event.pageX, right: "auto" });
                            }).append(
                                $(this).addClass("context-menu").css("z-index", "1000")
                            )
                        );
                        // If menu is too tall to display below the cursor, then display above
                        if (Event.pageY + $(".context-menu").height() > $("body").height()) $(".context-menu").css("bottom", $("body").height() - Event.pageY);
                        else $(".context-menu").css("top", Event.pageY);
                        // If menu is too wide to display to the right of the cursor, then display to the left
                        if (Event.pageX + $(".context-menu").width() > $("body").width()) $(".context-menu").css("right", $("body").width() - Event.pageX);
                        else $(".context-menu").css("left", Event.pageX);
                        // If context menu has submenus, enable superfish plugin
                        if ($(".context-menu ul").length) $(".context-menu").Menu();
                        // Apply hover class on mouse over and remove on mouse out
                        $(".context-menu li").each(function() { $(this).mouseover(function() { $(this).addClass("hover") }) });
                        $(".context-menu li").each(function() { $(this).mouseout(function() { $(this).removeClass("hover") }) });
                    }
                }
            })
        },
        GetAcoreId: function() {
            // If object is window or task, extrapolate the string literal for the Acore Id and return it
            if ($(this).hasClass("window") || $(this).hasClass("task")) return $(this).attr("id").replace(/^window_/, "");
            else if ($(this).closest(".window").length) return $(this).closest(".window").attr("id").replace(/^window_/, "");
            else return $(".window.active").attr("id").replace(/^window_/, "");
        },
        GetTask: function() {
            // If object is a window, find and return its corresponding task bar item
            if ($(this).hasClass("window") && $(this).attr("id") != null) {
                var task = "#task_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(task).length) return $(task);
            }
            return false;
        },
        GetWindow: function() {
            // If object is a task bar item, find and return its corresponding window
            if (($(this).hasClass("context") || $(this).hasClass("task")) && $(this).attr("id") != null) {
                var window = "#window_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(window).length) return $(window);
            }
            return false;
        },
        InitForms: function() {
            return this.each(function() {
                if ($(".date-picker", this).length) $(".date-picker", this).DatePicker();
                if ($(".time-picker", this).length) $(".time-picker", this).TimePicker();
                if ($(".physician-picker", this).length) $(".physician-picker", this).PhysicianInput();
                // $("input[type=file]", Element).StyledUploader();
                if ($(".diagnosis,.icd,.procedureICD,.procedureDiagnosis", this).length) $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis", this).IcdInput();
                if ($(".row", this).find(".phone-short:eq(0)").length) $(".row", this).find(".phone-short:eq(0)").PhoneAutoTab();
                if ($(".numeric", this).length) $(".numeric", this).numeric();
                if ($(".decimal", this).length) $(".decimal", this).Decimal();
                if ($(".currency", this).length) $(".currency", this).Currency();
                if ($(".template-selector select", this).length) $(".template-selector select", this).TemplateSelect();
                if ($("textarea[maxcharacters]", this).length) {
                    $("textarea[maxcharacters]", this).after($("<em/>").addClass("characters-remaining"));
                    $("textarea[maxcharacters]", this).limitMaxlength({
                        onEdit: function(remaining) {
                            $(this).siblings(".characters-remaining").text("You have " + remaining + " characters remaining");
                            if (remaining > 0) $(this).css("background-color", "#fff")
                        },
                        onLimit: function() {
                            $(this).css("background-color", "#ecbab3")
                        }
                    })
                }
                if ($("input[type=radio]", this).length) $("input[type=radio]", this).DeselectableRadio();
                // Append red asterisk to required fields
                if ($(".row .required", this).length) {
                    $(".row .required", this).closest(".row").prepend("<span class='required-red abs-right'>*</span>");
                    $("form", this).prepend(
                        $("<div/>", { "class": "abs required-legend", "text": " = Required Field" }).prepend(
                            $("<span/>", { "class": "required-red", "text": "*" })));
                }
                // Set events for button processing
                $(".buttons a", this).bind("ProcessingStart", function() {
                    $(this).addClass("processing-button").hide().parent().after(
                        $("<li/>", { "class": "processing", text: "Processing..." }).append(
                            $("<span/>").addClass("img icon processing")));
                }).bind("ProcessingComplete", function() {
                    $(this).show().parent().siblings("li.processing").remove();
                });
                // Set checkgoup functionality
                var Checkgroups = $(".checkgroup .option > input[type=checkbox],.checkgroup .option > input[type=radio]", this);
                if (Checkgroups.length) Checkgroups.each(function() {
                    if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                    else $(this).closest(".option").removeClass("selected");
                    $(this).change(function() {
                        Checkgroups.each(function() {
                            if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                            else $(this).closest(".option").removeClass("selected");
                        })
                    })
                });
                if ($(".checkgroup .more", this).length) $(".checkgroup .more", this).each(function() { U.ShowIfChecked($(this).siblings("[type=checkbox],[type=radio]"), $(this)) });
                // Set standard buttons
                if ($(".buttons .close", this).not(".save").length) $(".buttons .close", this).not(".save").click(function() {
                    $(this).closest(".window").Close();
                    return false;
                });
                if ($(".buttons .clear", this).not(".save").length) $(".buttons .clear", this).not(".save").click(function() {
                    $(this).closest("form").clearForm();
                    return false;
                });
                if ($(".buttons .save", this).length) $(".buttons .save", this).click(function() {
                    var Form = $(this).closest("form"), Button = $(this);
                    $(".complete-required", Form).removeClass("required error");
                    if (Button.hasClass("close")) Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                        $(this).closest(".window").Close();
                    });
                    else if ($(this).hasClass("clear")) Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                        $(this).clearForm();
                        $(".processing-button", this).trigger("ProcessingComplete");
                    });
                    else if ($(this).hasClass("next") && $(this).closest(".ui-tabs").length) Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                        $(this).closest(".ui-tabs").find(".ui-tabs-selected").next("li").find("a").click();
                    });
                    else Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                        $(".processing-button", this).trigger("ProcessingComplete");
                    });
                    Form.unbind("SubmitFailure").bind("SubmitFailure", function() {
                        $(".processing-button", this).trigger("ProcessingComplete");
                    });
                    if ($(this).closest("form").valid()) Button.trigger("ProcessingStart").closest("form").submit();
                    return false;
                });
                if ($(".buttons .complete", this).length) $(".buttons .complete", this).click(function() {
                    var Form = $(this).closest("form");
                    $(".complete-required", Form).addClass("required");
                    Form.unbind("SubmitSuccess").bind("SubmitSuccess", function() {
                        $(this).closest(".window").Close();
                    }).unbind("SubmitFailure").bind("SubmitFailure", function() {
                        $(".buttons li:hidden a").trigger("ProcessComplete");
                    });
                    if ($(this).closest("form").valid()) $(this).trigger("ProcessingStart").closest("form").submit();
                    return false;
                });
                if ($(".button-with-arrow .new-physician", this).length) $(".button-with-arrow .new-physician", this).click(function() {
                    Agency.Physician.NewModal();
                    return false;
                })
            })
        },
        IsDialog: function() {
            return $(this).GetAcoreId() == Acore.DialogId;
        },
        Load: function(Url, Data, Callback) {
            // If load is on window, set element to content div, detect versitile height
            var Element = $(this).hasClass("window") ? $(".window-content", this) : $(this),
                Window = Element.closest(".window"),
                VersitileHeight = ($(this).hasClass("window") && Acore.Windows[Element.GetAcoreId()].VersitileHeight);
            // Adjust variables is optional variable Data is skipped, but optional variable Callback is specified
            if (typeof Data === "function") Callback = Data, Data = undefined;
            // If object scales to fit, set too small default height/width
            if (VersitileHeight && Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
            else if (VersitileHeight) Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
            if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ top: ($("#desktop").height() - Window.height()) / 2, left: ($("#desktop").width() - Window.width()) / 2 });
            // Empty element, set to loading and make AJAX request for content
            Element.empty().addClass("loading").load(Url, Data, function(r, t, x) {
                // If session is expired, send to login page
                if (/AgencyCore Login/.test(r)) $(location).attr("href", "SessionExpired");
                // If there was an error loading page, display error message
                else if (t == "error" || /HTTP [45]0[034]/.test(r)) {
                    Element.html(U.MessageErrorAjax());
                    if (Acore.Windows[Window.GetAcoreId()].WindowFrame == false && Element.attr("id") != "printview") Element.append(
                        $("<div/>").addClass("abs-bottom").Buttons([{ Text: "Close", Click: function() { Window.Close() } }]));
                    if (VersitileHeight) {
                        Element.find(".error-box").css("top", "3.5em");
                        var Height = Acore.Windows[Element.GetAcoreId()].WindowFrame == false ? 175 : 195;
                        if (Acore.Windows[Element.GetAcoreId()].Center) Window.css({ height: Height, width: 550 }).Center();
                        else if (Acore.Animated) Window.animate({ height: Height, width: 550 }, Acore.AnimationSpeed, function() { Element.removeClass("loading") });
                        else Window.css({ height: Height, width: 550 }).find(".window-content").removeClass("loading");
                    } else Element.removeClass("loading");
                }
                // If No Server Errors
                else {
                    // Run global init script
                    Acore.OnLoad(Element);
                    if (VersitileHeight) Window.SetHeight();
                    else Element.removeClass("loading");
                    // Run callback function, if specified
                    if (typeof Callback === "function") {
                        try {
                            Callback(r, t, x, Element)
                        } catch (e) {
                            Element.html(U.MessageErrorJS());
                            $.error(e)
                        }
                    }
                }
            })
        },
        Maximize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // Maximize window
                    Window.attr({
                        "restore-top": $(this).css("top"),
                        "restore-left": $(this).css("left"),
                        "restore-right": $(this).css("right"),
                        "restore-bottom": $(this).css("bottom"),
                        "restore-width": $(this).css("width"),
                        "restore-height": $(this).css("height")
                    }).addClass("maximized").removeClass("minimized");
                    if (Acore.Animated) Window.animate({ "top": 0, "left": 0, "width": "100%", "height": "100%" }, Acore.AnimationSpeed, function() { $(window).resize() });
                    else {
                        Window.css({ "top": 0, "left": 0, "width": "100%", "height": "100%" });
                        $(window).resize();
                    }
                    // Raise window resize event
                    $(window).resize();
                    // If not the active window, then bring to front
                    if (!Window.hasClass("active")) Window.WinFocus();
                }
            })
        },
        Minimize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // Minimize window
                if (Window) {
                    Window.attr({
                        "minimize-restore-top": Window.css("top"),
                        "minimize-restore-left": Window.css("left"),
                        "minimize-restore-width": Window.css("width"),
                        "minimize-restore-height": Window.css("height")
                    });
                    if (Acore.Animated) Window.animate({ top: $("#desktop").height(), left: 0, height: 0, width: 0 }, Acore.AnimationSpeed, function() {
                        Window.removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active")
                    });
                    else Window.css({ top: 0, left: 0, height: 0, width: 0 }).removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active");
                }
            })
        },
        Rename: function(NewName, TaskRename) {
            return this.each(function() {
                // If pulling name from the DOM
                if (NewName == undefined && $(".wintitle", this).html() != undefined) {
                    NewName = $(".wintitle", this).html();
                    $(".wintitle", this).remove();
                }
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If window has frame, rename at the top of the window
                    if ($(".window-top .title", Window).length) $(".window-top .title", Window).html(NewName);
                    // Rename task bar item
                    if (TaskRename) Window.GetTask().find("a").html(
                            Window.GetTask().find(".img")
                        ).append(NewName).attr("title", Window.GetTask().text());
                }
            })
        },
        Restore: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // If window is maximized, restore css to old numbers
                if (Window && Window.hasClass("maximized")) {
                    if (Acore.Animated) $(this).removeClass("maximized").animate({
                        "top": $(this).attr("maximize-restore-top"),
                        "left": $(this).attr("maximize-restore-left"),
                        "width": $(this).attr("maximize-restore-width"),
                        "height": $(this).attr("maximize-restore-height")
                    }, Acore.AnimationSpeed, function() { $(window).resize() });
                    else {
                        $(this).removeClass("maximized").css({
                            "top": $(this).attr("maximize-restore-top"),
                            "left": $(this).attr("maximize-restore-left"),
                            "width": $(this).attr("maximize-restore-width"),
                            "height": $(this).attr("maximize-restore-height")
                        });
                        $(window).resize();
                    }
                }
            })
        },
        SetHeight: function(Animated) {
            return this.each(function() {
                // Set variables
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window"),
                    Content = Window.find(".window-content"),
                    Width = Acore.Windows[Window.GetAcoreId()].Width ? Acore.Windows[Window.GetAcoreId()].Width : Acore.DefaultWinWidth,
                    TempWidth = Window.css("width"),
                    TempHeight = Acore.Windows[Window.GetAcoreId()].IgnoreMinSize ? 200 : Acore.MinWinHeight;
                Animated = Animated == false ? false : Acore.Animated;
                // Shrink window to smaller size
                if (Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
                else Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
                if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ top: ($("#desktop").height() - Window.height()) / 2, left: ($("#desktop").width() - Window.width()) / 2 });
                // Temporarily adjust window to calculate height, then revert
                Window.css({ width: Width, height: 50 });
                Content.removeClass("loading")
                var Height = Content.prop("scrollHeight");
                Content.addClass("loading")
                Window.css({ width: TempWidth, height: TempHeight });
                // Add to content height for window element heights
                if (Acore.Windows[Window.GetAcoreId()].WindowFrame == false) Height += 9;
                else if (Acore.Windows[Window.GetAcoreId()].StatusBar == false) Height += 32;
                else Height += 48;
                // If height is larger than desktop, set to default
                if (Height > $("#desktop").height()) Height = Acore.DefaultWinHeight;
                // If window is placed out of desktop boundries, move it up some
                var Top = Window.position().top;
                if (Top + Height > $("#desktop").height()) Top = $("#desktop").height() - Height - 3;
                // If centered, send to center function, resize window sizing
                if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ width: Width, height: Height }).Center(Animated);
                else if (Animated) Window.animate({ width: Width, height: Height, top: Top }, Acore.AnimationSpeed, function() { Content.removeClass("loading") });
                else Window.css({ width: Width, height: Height, top: Top }).find(".window-content").removeClass("loading");
            })
        },
        Status: function(Message) {
            return this.each(function() {
                // Find window and status bar
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window"),
                        StatusBar = Window.find(".window-bottom");
                // Set status bar message
                if (StatusBar) StatusBar.text(Message);
            })
        },
        WinFocus: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // Clear current active window
                    $(".window.active", "#desktop").removeClass("active");
                    $(".active", "#task").removeClass("active");
                    // Hide menus
                    $(".ui-menu").hide();
                    // Make current window active
                    if (Window.css("z-index") <= Acore.MaxWindows) Window.css("z-index", Acore.OpenWindows);
                    Window.addClass("active");
                    Window.GetTask().addClass("active");
                    if (Window.hasClass("minimized")) {
                        if (Acore.Animated) Window.removeClass("minimized").animate({
                            top: Window.attr("minimize-restore-top"),
                            left: Window.attr("minimize-restore-left"),
                            height: Window.attr("minimize-restore-height"),
                            width: Window.attr("minimize-restore-width")
                        }, Acore.AnimationSpeed);
                        else Window.css({
                            top: Window.attr("minimize-restore-top"),
                            left: Window.attr("minimize-restore-left"),
                            height: Window.attr("minimize-restore-height"),
                            width: Window.attr("minimize-restore-width")
                        }).removeClass("minimized");
                    }
                    // Recalculate other windows' z-index
                    $(".window:not(.active):not(.top-window)").each(function() {
                        if (!Acore.Windows[$(this).GetAcoreId()].Modal && $(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    });
                }
            })
        }
    })
})(jQuery);