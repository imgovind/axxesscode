(function($) {
    $.extend($.fn, {
        DatePicker: function(Function, Argument) {
            return this.each(function() {
                if ($(this).hasClass("hasDatepicker") && Function == "Rebuild" && typeof Argument == "string") {
                    var Input = $(this),
                        Selections = new Array(),
                        Title = $("<span/>", { "class": "ui-datepicker-year" }),
                        TitleClick = function() { },
                        NextPrevClick = function() { },
                        CalendarClick = function() { };
                    if (Argument == "Month") {
                        var Year = parseInt($(".ui-datepicker-year").text().trim().substr(0, 4));
                        Selections = [
                            ["Jan", "Feb", "Mar", "Apr"],
                            ["May", "Jun", "Jul", "Aug"],
                            ["Sep", "Oct", "Nov", "Dec"]
                        ];
                        Title.html(Year);
                        TitleClick = function() {
                            Input.DatePicker("Rebuild", "Year")
                        };
                        NextPrevClick = function() {
                            if ($(this).hasClass("ui-datepicker-next")) Year++;
                            else Year--;
                            $(".ui-datepicker-year").html(String(Year));
                            Input.DatePicker("Rebuild", "Month");
                        };
                        CalendarClick = function() {
                            var Month = $(".ui-datepicker-calendar").find("a").index(this),
                                CurrentDate = Input.datepicker("getDate");
                            if (CurrentDate == null) CurrentDate = new Date();
                            CurrentDate.setMonth(Month);
                            CurrentDate.setFullYear(Year);
                            Input.datepicker("setDate", CurrentDate)
                        };
                    }
                    else if (Argument == "Year") {
                        var Year = parseInt($(".ui-datepicker-year").text().trim().substr(0, 3) + "0");
                        Selections = [
                            [String(Year - 1), String(Year), String(Year + 1), String(Year + 2)],
                            [String(Year + 3), String(Year + 4), String(Year + 5), String(Year + 6)],
                            [String(Year + 7), String(Year + 8), String(Year + 9), String(Year + 10)]
                        ];
                        Title.html(String(Year) + "&#8211;" + String(Year + 9));
                        TitleClick = function() { Input.DatePicker("Rebuild", "Decade") };
                        NextPrevClick = function() {
                            if ($(this).hasClass("ui-datepicker-next")) Year += 10;
                            else Year -= 10;
                            $(".ui-datepicker-year").html(String(Year));
                            Input.DatePicker("Rebuild", "Year");
                        };
                        CalendarClick = function() {
                            $(".ui-datepicker-year").html($(this).text());
                            Input.DatePicker("Rebuild", "Month");
                        };
                    } else if (Argument == "Decade") {
                        var Year = parseInt($(".ui-datepicker-year").text().trim().substr(0, 2) + "00");
                        Selections = [
                            [(Year - 10) + "-\n" + (Year - 1), (Year) + "-\n" + (Year + 9), (Year + 10) + "-\n" + (Year + 19), (Year + 20) + "-\n" + (Year + 29)],
                            [(Year + 30) + "-\n" + (Year + 39), (Year + 40) + "-\n" + (Year + 49), (Year + 50) + "-\n" + (Year + 59), (Year + 60) + "-\n" + (Year + 69)],
                            [(Year + 70) + "-\n" + (Year + 79), (Year + 80) + "-\n" + (Year + 89), (Year + 90) + "-\n" + (Year + 99), (Year + 100) + "-\n" + (Year + 109)]
                        ];
                        Title.html(String(Year) + "&#8211;" + String(Year + 99));
                        NextPrevClick = function() {
                            if ($(this).hasClass("ui-datepicker-next")) Year += 100;
                            else Year -= 100;
                            $(".ui-datepicker-year").html(String(Year));
                            Input.DatePicker("Rebuild", "Decade");
                        };
                        CalendarClick = function() {
                            $(".ui-datepicker-year").html($(this).text().trim().substr(0, 4));
                            Input.DatePicker("Rebuild", "Year");
                        };
                    }
                    // Set calendar header
                    $(".ui-datepicker-header").empty().append(
                        $("<a/>", { "class": "ui-datepicker-prev ui-corner-all", "title": "Prev" }).click(NextPrevClick).append(
                            $("<span/>", { "class": "ui-icon ui-icon-circle-triangle-w", "text": "Prev" }))).append(
                        $("<a/>", { "class": "ui-datepicker-next ui-corner-all", "title": "Next" }).click(NextPrevClick).append(
                            $("<span/>", { "class": "ui-icon ui-icon-circle-triangle-e", "text": "Next" }))).append(
                        $("<div/>", { "class": "ui-datepicker-title" }).click(TitleClick).append(Title)
                    );
                    // Empty calendar table
                    $(".ui-datepicker-calendar").empty().append(
                        $("<tbody/>")
                    );
                    // Cycle through selections array to build table on calendar object
                    for (var i = 0; i < Selections.length; i++) {
                        $("tbody", ".ui-datepicker-calendar").append(
                            $("<tr/>").addClass(Argument.toLowerCase())
                        );
                        for (var j = 0; j < Selections[i].length; j++) $("tr:last", ".ui-datepicker-calendar").append(
                            $("<td/>").append(
                                $("<a/>", { "class": "ui-state-default", "text": Selections[i][j] }).click(CalendarClick)
                            )
                        )
                    }
                } else {
                    // If re-initializing with new name
                    var $input = $(this),
                        Options = Function;
                    if ($input.hasClass("hasDatepicker") && Options == "Rename" && typeof Argument == "string") {
                        // Pull existing min/max dates
                        Options = new Object();
                        if ($input.datepicker("option", "minDate")) Options.minDate = $input.datepicker("option", "minDate");
                        if ($input.datepicker("option", "maxDate")) Options.maxDate = $input.datepicker("option", "maxDate");
                        // Remove current datepicker instance
                        $input.datepicker("destroy").next("a").remove();
                        // Rename input accordingly
                        $input.attr({
                            id: Argument,
                            name: Argument
                        })
                    }
                    // If of input type date, change to type text
                    if ($input.attr("type") == "date") {
                        Options = new Object();
                        if (U.IsDate($input.attr("mindate"))) Options.minDate = new Date($input.attr("mindate"));
                        if (U.IsDate($input.attr("maxdate"))) Options.maxDate = new Date($input.attr("maxdate"));
                        var $newinput = $("<input/>", {
                            "type": "text",
                            "name": $input.attr("name") ? $input.attr("name") : "",
                            "value": $input.val() ? $input.val() : "",
                            "size": $input.attr("size") ? $input.attr("size") : "",
                            "maxlength": $input.attr("maxlength") ? $input.attr("maxlength") : "",
                            "class": $input.attr("class") ? $input.attr("class") : "",
                            "id": $input.attr("id") ? $input.attr("id") : "",
                            "title": $input.attr("title") ? $input.attr("title") : "",
                            "style": $input.attr("style") ? $input.attr("style") : ""
                        });
                        $input.after($newinput);
                        $input.remove();
                        $input = $newinput;
                    }
                    if (!$input.hasClass("hasDatepicker")) $input.addClass("date").datepicker({
                        // Use custom defined min/max dates or use default range 1900/01/01 - 2099/12/31
                        minDate: (Options && Options.minDate ? Options.minDate : new Date(1900, 0, 1)),
                        maxDate: (Options && Options.maxDate ? Options.maxDate : new Date(2099, 11, 31)),
                        // Show dates from other months and make them selectable
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        // When opening the calendar, set title click to switch to Month Selector
                        beforeShow: function(input, inst) {
                            setTimeout(function() {
                                $(".ui-datepicker-title").click(function() {
                                    $(input).DatePicker("Rebuild", "Month");
                                })
                            }, 500);
                        },
                        // When date selected from calendar, set entry to valid
                        onSelect: function() {
                            $(this).removeClass("error").addClass("valid");
                        }
                        // When input is blurred, check to make sure it is either blank or a valid date within selectable range
                    }).bind("ValueSet", function() { }).blur(function() {
                        var d = $(this).val();
                        // If value is entered
                        if (d && d.length > 3) {
                            if (!U.IsDate(d)) {
                                d = d.replace(/[^0-9]/g, "");
                                if (d.length == 8) d = d.substr(0, 2) + "/" + d.substr(2, 2) + "/" + d.substr(4, 4);
                                else if (d.length == 7) {
                                    if (d.substr(0, 1) == "1") d = d.substr(0, 2) + "/0" + d.substr(2, 1) + "/" + d.substr(3, 4);
                                    else d = "0" + d.substr(0, 1) + "/" + d.substr(1, 2) + "/" + d.substr(3, 4);
                                } else if (d.length == 6) {
                                    if (parseInt(d.substr(2, 2)) + 1 == parseInt(new Date().getFullYear() / 100) || parseInt(d.substr(2, 2)) == parseInt(new Date().getFullYear() / 100))
                                        d = "0" + d.substr(0, 1) + "/0" + d.substr(1, 1) + "/" + d.substr(2, 4);
                                    else d = d.substr(0, 2) + "/" + d.substr(2, 2) + "/" + U.Y2KConvert(d.substr(4, 2));
                                } else if (d.length == 5) {
                                    if (d.substr(0, 1) == "1") d = d.substr(0, 2) + "/0" + d.substr(2, 1) + "/" + U.Y2KConvert(d.substr(3, 2));
                                    else d = "0" + d.substr(0, 1) + "/" + d.substr(1, 2) + "/" + U.Y2KConvert(d.substr(3, 2));
                                } else if (d.length == 4) d = "0" + d.substr(0, 1) + "/0" + d.substr(1, 1) + "/" + U.Y2KConvert(d.substr(2, 2));
                                // If parsed to a valid date, set it
                                if (U.IsDate(d)) $(this).datepicker("setDate", d).keyup();
                                // Otherwise error out
                                else $(this).removeClass("valid").addClass("error");
                            }
                            // If not compliant with zero-fill and Y2K compliance
                            else if (d.length < 10) {
                                var ex = d.split("/");
                                if (ex[0].length == 1) ex[0] = "0" + ex[0];
                                if (ex[1].length == 1) ex[1] = "0" + ex[1];
                                if (ex[2].length == 2) ex[2] = U.Y2KConvert(ex[2]);
                                d = ex.join("/");
                                $(this).datepicker("setDate", d).keyup();
                            }
                            // If set prior to minimum date
                            else if (!d || $(this).datepicker("option", "minDate") && new Date(d).getTime() < $(this).datepicker("option", "minDate").getTime()) $(this).removeClass("valid").addClass("error");
                            // If set after maximum date
                            else if (!d || $(this).datepicker("option", "maxDate") && new Date(d).getTime() > $(this).datepicker("option", "maxDate").getTime()) $(this).removeClass("valid").addClass("error");
                            // If valid
                            else $(this).keyup();
                        } else if (d.length || ($(this).hasClass("required") && !d)) $(this).removeClass("valid").addClass("error");
                        $(this).trigger("ValueSet");
                        // After input append calendar icon
                    }).after(
                        $("<a/>", { "href": "javascript:void(0)", "class": "t-link t-icon t-icon-calendar", "text": "select date" }).click(function() {
                            if ($(this).prev().is("label.error")) $(this).prev().prev().datepicker("show");
                            else $(this).prev().datepicker("show");
                        })
                    );
                    if ($input.val()) $input.blur();
                    // Ensure calendar is hidden upon default
                    $("#ui-datepicker-div").hide();
                }
            })
        }
    })
})(jQuery);