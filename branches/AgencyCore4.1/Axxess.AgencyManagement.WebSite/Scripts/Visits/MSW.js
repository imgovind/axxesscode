﻿$.extend(Visit, {
    MSWEvaluationAssessment: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            $(".Physicians").PhysicianInput();
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.ShowIfChecked($(Prefix + "GenericLivingSituation10"), $(Prefix + "GenericLivingSituationOther"));
            U.ShowIfChecked($(Prefix + "GenericReasonForReferral11"), $(Prefix + "GenericReasonForReferralOther"));
            U.ShowIfChecked($(Prefix + "GenericEmotionalStatus10"), $(Prefix + "GenericEmotionalStatusOther"));
            U.ShowIfChecked($(Prefix + "GenericPlannedInterventions11"), $(Prefix + "GenericPlannedInterventionsOther"));
            U.ShowIfChecked($(Prefix + "GenericGoals8"), $(Prefix + "GenericGoalsOther"));
            Visit.Shared.Init(Type, Visit.MSWEvaluationAssessment.Init);
        },
        Submit: function(Button, Completing, Type) { Visit.Shared.Submit(Button, Completing, Type) },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("MSWEvaluationAssessment", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "MSWEvaluationAssessment") }
    },
    MSWAssessment: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("MSWAssessment", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "MSWAssessment") }
    },
    MSWDischarge: {
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("MSWDischarge", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "MSWDischarge") }
    }
});