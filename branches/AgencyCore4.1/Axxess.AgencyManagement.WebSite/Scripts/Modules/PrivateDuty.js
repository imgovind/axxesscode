﻿var PrivateDuty = {
    ScheduleCenter: {
        AcoreId: "PrivateDutyScheduleCenter",   // Acore Window ID for Private Duty Schedule Center
        PatientId: "",                          // Current Active Patient's GUID
        PatientName: "",                        // Current Active Patient's Name
        // Initialize Private Duty Schedule Center
        HideList: function(e) {
            $(".hide-list,.show-list").toggle();
            if (Acore.Animated) {
                $(".list-collapsed", e).animate({ bottom: 0 }, Acore.AnimationSpeed);
                $(".schedule-list", e).animate({ height: 0 }, Acore.AnimationSpeed, function() { $(this).empty() });
            } else {
                $(".list-collapsed", e).css("bottom", 0);
                $(".schedule-list", e).css("height", 0).empty();
            }
        },
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set new patient button
            $(".new-patient", e).click(function() {
                Patient.New();
                return false;
            });
            $(".new-task", e).click(function() {
                PrivateDuty.Task.New();
                return false;
            });
            $(".show-list").click(function() {
                PrivateDuty.ScheduleCenter.ShowList(e);
                return false;
            });
            $(".hide-list").click(function() {
                PrivateDuty.ScheduleCenter.HideList(e);
                return false;
            });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { PrivateDuty.ScheduleCenter.PatientSelector.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { PrivateDuty.ScheduleCenter.PatientSelector.Rebind(e) });
        },
        // External Loader
        Load: function(patientId) {
            if (Acore.Windows[Schedule.PD.Center.AcoreId].IsOpen) {
                PrivateDuty.ScheduleCenter.LoadContent(patientId, episodeId);
                $("#window_" + PrivateDuty.ScheduleCenter.AcoreId).WinFocus();
            } else {
                PrivateDuty.ScheduleCenter.PatientId = patientId;
                Acore.Open(PrivateDuty.ScheduleCenter.AcoreId);
            }
        },
        // Patient Selection Loader
        LoadContent: function(patientId) {
            var e = $("#window_" + PrivateDuty.ScheduleCenter.AcoreId + "_content");
            if (U.IsGuid(patientId)) {
                var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
                row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, 500);
                PrivateDuty.ScheduleCenter.PatientName = row.find("td:eq(2)").text();
                PrivateDuty.ScheduleCenter.PatientId = patientId;
                PrivateDuty.ScheduleCenter.Refresh();
            }
        },
        PatientSelector: {
            // Initialize Patient Selector
            Init: function() {
                var e = $("#window_" + PrivateDuty.ScheduleCenter.AcoreId + "_content");
                $(".ui-layout-west .t-grid-content", e).css("height", "auto");
                PrivateDuty.ScheduleCenter.PatientSelector.Filter(e);
                if (U.IsGuid(PrivateDuty.ScheduleCenter.PatientId) && $(".ui-layout-west .t-last:contains(" + PrivateDuty.ScheduleCenter.PatientId + ")", e).length) PrivateDuty.ScheduleCenter.LoadContent(PrivateDuty.ScheduleCenter.PatientId);
                else if ($(".ui-layout-west .t-last", e).length) PrivateDuty.ScheduleCenter.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
            },
            // Text Filter Functionality
            Filter: function(e) {
                var text = $(".ui-layout-west .top input", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            // Selecting Patient from Patient Selector
            Select: function(e) {
                PrivateDuty.ScheduleCenter.LoadContent($(".t-last", e.row).text());
            }
        },
        // Refresh Calendar Data
        Refresh: function() {
            var e = $("#window_" + PrivateDuty.ScheduleCenter.AcoreId + "_content");
            if ($(".fc", e).length) $(".fc", e).PrivateDutyScheduler("refresh");
            else $(".pd-calendar", e).PrivateDutyScheduler();
        },
        ShowList: function(e) {
            $(".hide-list,.show-list").toggle();
            $(".schedule-list", e).Load("PrivateDuty/Task/Grid", {
                patientId: PrivateDuty.ScheduleCenter.PatientId,
                startDate: U.FormatDate($(".pd-calendar", e).fullCalendar("getView").start),
                endDate: U.FormatDate($(".pd-calendar", e).fullCalendar("getView").end)
            });
            if (Acore.Animated) {
                $(".list-collapsed", e).animate({ bottom: $(".main", e).height() + 9 }, Acore.AnimationSpeed);
                $(".schedule-list", e).animate({ height: $(".main", e).height() + 9 }, Acore.AnimationSpeed);
            } else {
                $(".list-collapsed", e).css("bottom", $(".main", e).height() + 9);
                $(".schedule-list", e).css("height", $(".main", e).height() + 9);
            }
        }
    },
    Task: {
        // Delete Private Duty Event
        Delete: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this event?",
                Yes: function() {
                    U.PostUrl("PrivateDuty/Task/Delete", { id: id, patientId: PrivateDuty.ScheduleCenter.PatientId }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) PrivateDuty.ScheduleCenter.Refresh();
                    })
                }
            })
        },
        // Open Edit Private Event Window
        Edit: function(id) {
            Acore.Modal({
                Name: "Task Editor",
                Url: "PrivateDuty/Task/Edit",
                Input: { id: id, patientId: PrivateDuty.ScheduleCenter.PatientId },
                OnLoad: PrivateDuty.Task.InitEdit,
                Width: 575,
                WindowFrame: false,
                Height: 375
            })
        },
        // Initialize Edit Private Event Window
        InitEdit: function(r, t, x, e) {
            PrivateDuty.Task.InitShared("EditPrivateDutyTask_");
        },
        // Initialize New Private Event Window
        InitNew: function(r, t, x, e) {
            PrivateDuty.Task.InitShared("NewPrivateDutyTask_");
        },
        InitShared: function(prefix) {
            U.HideIfChecked($(prefix + "AllDay"), $(prefix + "End"));
            $(prefix + "Form").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) { },
                        success: function(result) {
                            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                            if (result.isSuccessful) PrivateDuty.ScheduleCenter.Refresh();
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        },
        // Open New Private Event Window
        New: function() {
            Acore.Modal({
                Name: "Employee Scheduler",
                Url: "PrivateDuty/Task/New",
                Input: { patientId: PrivateDuty.ScheduleCenter.PatientId },
                OnLoad: PrivateDuty.Task.InitNew,
                Width: 575,
                WindowFrame: false,
                Height: 375
            })
        }
    }
};