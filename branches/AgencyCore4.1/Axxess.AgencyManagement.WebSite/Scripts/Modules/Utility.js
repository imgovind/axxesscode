var U = {
    TGridAjax: function(url, input, grid) {
        U.PostUrl(url, input, function(result) {
            if (result.isSuccessful) {
                U.RebindTGrid(grid);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    DisplayDate: function() {
        var currentDate = new Date(),
            days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            dateSuffix = "th";
        if (currentDate.getDate() % 10 == 1 && currentDate.getDate() != 11) dateSuffix = "st";
        else if (currentDate.getDate() % 10 == 2 && currentDate.getDate() != 12) dateSuffix = "nd";
        else if (currentDate.getDate() % 10 == 3 && currentDate.getDate() != 13) dateSuffix = "rd";
        return days[currentDate.getDay()] + ", " + months[currentDate.getMonth()] + " " + currentDate.getDate() + dateSuffix + ", " + currentDate.getFullYear()
    },
    FormatMoney: function(num) {
        if (isNaN(Number(num))) return "";
        var result = String(Math.round(Math.abs(num) * 100));
        result = result.substr(0, result.length - 2) + "." + result.substr(result.length - 2);
        for (var i = 0; i * 4 + 6 < result.length; i++)
            result = result.substr(0, result.length - (i * 4 + 6)) + "," + result.substr(result.length - (i * 4 + 6));
        result = "$" + result;
        if (num < 0) result = "-" + result;
        return result;
    },
    IsTime: function(Time) {
        return /^(0[1-9]|1[012]):[0-5][0-9] [AP]M$/.test(Time)
    },
    IsGuid: function(Guid) {
        return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(Guid) && !(/^[0-]*$/.test(Guid))
    },
    IsEmail: function(Email) {
        return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(Email)
    },
    IsDate: function(date) {
        if (date && date.match(/^(0?[1-9]|1[012])\/(0?[1-9]|[12]\d|3[01])\/(19|20)?\d\d$/)) {
            date = date.split("/");
            if (date[0].match(/^(0?[469]|11)$/) && parseInt(date[1]) > 30) return false; // More than 30 days in April, June, September, or November
            if (date[0].match(/^(0?2)$/)) {
                if (parseInt(date[2]) % 4 && parseInt(date[1]) > 28) return false; // More than 28 days in February on non-Leap Year
                if (parseInt(date[2]) % 4 == 0 && parseInt(date[1]) > 29) return false; // More than 29 days in February on a Leap Year
            }
            return true;
        }
        return false;
    },
    Y2KConvert: function(ShortYear) {
        var CurrentYear = String(new Date().getFullYear()),
            CurrentPrefix = parseInt(CurrentYear.substr(0, 2)),
            CurrentShortYear = parseInt(CurrentYear.substr(2, 2));
        if (parseInt(ShortYear) > CurrentShortYear) return String(--CurrentPrefix) + String(ShortYear);
        else return String(CurrentPrefix) + String(ShortYear);
    },
    EnableDatePicker: function(e) { $(".date", e.row).DatePicker(); },
    ValidationError: function(control) {
        if (control.closest(".window-content").find(".error:first").closest("fieldset").length) var scroll = control.closest(".window-content").find(".error:first").closest("fieldset").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("td").length) var scroll = control.closest(".window-content").find(".error:first").closest("td").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("div").length) var scroll = control.closest(".window-content").find(".error:first").closest("div").position().top;
        else var scroll = 0;
        control.closest(".window-content").scrollTop(scroll);
        U.Growl("Error: There was a problem validating your form, please review your information and try again.", "error");
    },
    GetAttachment: function(Url, Data) {
        $("body").append(
            $("<form/>", { "id": "AjaxAttachmentForm", "method": "post", "action": Url })
        );
        $.each(Data, function(key, val) {
            $("#AjaxAttachmentForm").append(
                $("<input/>", { "type": "hidden", "name": key, "value": val })
            );
        });
        $("#AjaxAttachmentForm").submit().remove();
    },
    HideOptions: function() {
        $(".option").each(function() {
            if ($(this).closest("li").find(".extra").length) {
                if ($(this).find("input").is(":not(:checked)")) {
                    $(this).closest("li").find(".extra").addClass("form-omitted");
                    $(this).closest("li").find(".extra").hide();
                }
                $(this).find("input").change(function() {
                    $(this).closest("li").find(".extra").toggle().find(".checkgroup .extra").each(function() {
                        $(this).hide();
                        $(this).addClass("form-omitted");
                        if ($(this).closest("li").find(".option input").is(":checked")) {
                            $(this).show();
                            $(this).removeClass("form-omitted");
                        }
                    });
                });
            }
        });
    },
    ShowTxtWhenChooseOther: function() {
        $(".otherOption").each(function() {
            if ($(this).closest("div").find(".otherTxt").length) {
                if ($(this).val().toLowerCase() != "other") {
                    $(this).closest("div").find(".otherTxt").hide();
                }
                $(this).change(function() {
                    if ($(this).val().toLowerCase() == "other") {
                        $(this).closest("div").find(".otherTxt").show();
                    } else {
                        $(this).closest("div").find(".otherTxt").hide();
                    }
                });
            }
        });
    },
    DeselectableRadio: function(control) {
        if (control == undefined) control = $("#desktop");
        $("input[type=radio].deselectable", control).mouseup(function() {
            if ($(this).prop("checked")) $(this).addClass("deselect");
        });
        $("input[type=radio].deselectable", control).click(function() {
            if ($(this).hasClass("deselect")) {
                $(this).removeClass("deselect").prop("checked", false).change();
            } else return true;
        });
    },
    AjaxError: function() {
        return U.Error("There was an error loading this window.", "Please try again later, if problem persists, contact Axxess for assistance.");
    },
    Error: function(title, text) {
        return $("<div/>", { "id": "window_error" }).append(
            $("<span/>", { "class": "img icon error float-left" })).append(
            $("<h1/>", { "text": title })).append(
            $("<p/>", { "text": text })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "href": "javascript:void(0)", "text": "Close Window", "onclick": "$(this).closest('.window').Close()" })
                    )
                )
            )
        )
    },
    Message: function(Title, Text, Class) {
        return $("<div/>", { "class": "error-box" }).addClass(Class).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon float-left" }).addClass(Class)).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageError: function(Title, Text) {
        return $("<div/>", { "class": "error-box" }).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon error float-left" })).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageErrorAjax: function() {
        return U.MessageError("Request Error", "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.");
    },
    MessageErrorJS: function(Growl) {
        var Title = "Browser Error",
            Text = "An error has been detected in your browser. Please refresh the page and try again, if problem persists, contact Axxess for assistance.";
        if (Growl) return Title + "<br />" + Text;
        else return U.MessageError(Title, Text);
    },
    MessageInfo: function(Title, Text) {
        return U.Message(Title, Text, "info")
    },
    MessageSuccess: function(title, text) {
        return U.Message(Title, Text, "success")
    },
    MessageWarn: function(Title, Text) {
        return U.Message(Title, Text, "warning")
    },
    ToolTip: function(o, c) { o.each(function() { if ($(this).attr("tooltip") != undefined) { $(this).tooltip({ track: true, showURL: false, top: 10, left: 10, extraClass: c, bodyHandler: function() { return $(this).attr("tooltip"); } }); } }); },
    RebindTGrid: function(g, a) { if (g.data('tGrid') != null) g.data('tGrid').rebind(a); },
    Growl: function(message, theme) {
        if (typeof message == "object") message = message.toSource();
        else if (typeof message != "string") message = String(message);
        $.jGrowl($.trim(message), { theme: theme, life: 5000 })
    },
    PhoneAutoTab: function(name) {
        $('#' + name + '1').autotab({ target: name + '2', format: 'numeric' });
        $('#' + name + '2').autotab({ target: name + '3', format: 'numeric', previous: name + '1' });
        $('#' + name + '3').autotab({ format: 'numeric', previous: name + '2' });
    },
    FilterResults: function(type) {
        $('#' + type + 'MainResult').empty().addClass("loading");
        U.RebindTGrid($('#' + type + 'SelectionGrid'), { branchId: $("select." + type + "BranchCode").val(), statusId: $("select." + type + "StatusDropDown").val(), paymentSourceId: $("select." + type + "PaymentDropDown").val() });
    },
    HandlerHelperTemplate: function(AssessmentType, $control, actionType, Action) {
        var $form = $control.closest("form");
        if ($form != null && $form != undefined) {
            $('#' + AssessmentType + '_Button', $form).val(actionType);
            $form.validate();
            var options = {
                dataType: "json",
                beforeSubmit: function(values, form, options) { },
                success: function(result) {
                    if ($.trim(result.responseText) == "Success") {
                        if (actionType == "Save") {
                            U.Growl("Your assessment has been saved.", "success");
                        }
                        else if (actionType == "Approve") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been approved.", "success");
                        }
                        else if (actionType == "Return") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been returned.", "success");
                        }
                        else if (actionType == "SaveExit") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been saved successfully.", "success");
                        }
                        else if (actionType == "SaveContinue") {
                            U.Growl("Your assessment has been saved.", "success");
                            Oasis.NextTab("#" + AssessmentType + "_Tabs");
                        }
                        else if (actionType == "SaveCheckError") {
                            U.Growl("Your assessment has been saved.", "success");
                            if (typeof Action == "function")
                                Action();
                        }
                        else if (actionType == "CheckError") {
                            Action();
                        }
                        else if (actionType == "SaveComplete") {
                            if (typeof Action == "function")
                                Action();
                        }
                    } else U.Growl(result.responseText, "error");
                },
                error: function(result) {
                    if ($.trim(result.responseText) == "Success") {
                        if (actionType == "Save") {
                            U.Growl("Your assessment has been saved.", "success");
                        }
                        else if (actionType == "Approve") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been approved.", "success");
                        }
                        else if (actionType == "Return") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been returned.", "success");
                        }
                        else if (actionType == "SaveExit") {
                            UserInterface.CloseAndRefresh(AssessmentType);
                            U.Growl("Your assessment has been saved successfully.", "success");
                        }
                        else if (actionType == "SaveContinue") {
                            U.Growl("Your assessment has been saved.", "success");
                            Oasis.NextTab("#" + AssessmentType + "_Tabs");
                        }
                        else if (actionType == "SaveCheckError") {
                            U.Growl("Your assessment has been saved.", "success");
                            if (typeof Action == "function")
                                Action();
                        }
                        else if (actionType == "CheckError") {
                            Action();
                        }
                        else if (actionType == "SaveComplete") {
                            if (typeof Action == "function")
                                Action();
                        }
                    }
                    else U.Growl(result.responseText, "error");
                }
            };
            $form.find(".form-omitted :input,.form-omitted:input").val("");
            $form.ajaxSubmit(options);
            return false;
        }
    },
    Delete: function(name, url, data, callback, bypassAlerts) {
        if (bypassAlerts || confirm("Are you sure you want to delete this " + name.toLowerCase() + "?")) {
            U.PostUrl(url, data, function(result) {
                if (result.isSuccessful) {
                    if (callback != undefined && typeof (callback) == 'function') callback();
                    if (!bypassAlerts) U.Growl(name + " has been successfully deleted.", "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    ShowIfOtherSelected: function(selectList, field) {
        var required = false;
        if ($("option:selected", selectList).val().toLowerCase() != "other") field.prop("disabled", true);
        if (field.hasClass("required")) {
            required = true;
            if (field.prop("disabled")) field.removeClass("required");
        }
        selectList.change(function() {
            if ($("option:selected", selectList).val().toLowerCase() == "other") {
                if (required) field.addClass("required");
                field.prop("disabled", false).focus();
            } else field.prop("disabled", true).val("").removeClass("required");
        });
    },
    ShowIfOtherChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) {
            field.show();
            U.ToggleValidation(field, true);
        } else {
            field.hide();
            U.ToggleValidation(field, false);
        }
        checkbox.bind('click', function() {
            if ($(this).is(":checked")) {
                field.show();
                U.ToggleValidation(field);
            } else {
                field.hide().val("");
                U.ToggleValidation(field);
            }
        });
    },
    EnableIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) {
            field.attr("disabled", false).removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.attr("disabled", true).addClass('form-omitted');
            U.ToggleValidation(field);
        }
        checkbox.bind('click', function() {
            if (checkbox.is(":checked")) {
                field.attr("disabled", false).removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.attr("disabled", true).addClass('form-omitted');
                U.ToggleValidation(field);
            }
        });
    },
    ShowIfChecked: function(checkbox, field) {
        this.IfChecked(true, checkbox, field)
    },
    HideIfChecked: function(checkbox, field) {
        this.IfChecked(false, checkbox, field)
    },
    IfChecked: function(show, checkbox, field) {
        if (checkbox.prop("checked") == show) {
            field.show().removeClass("form-omitted");
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass("form-omitted");
            U.ToggleValidation(field);
        }
        checkbox.change(function() {
            if (checkbox.prop("checked") == show) {
                field.show().removeClass("form-omitted");
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass("form-omitted");
                U.ToggleValidation(field);
            }
        });
    },
    EnableIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.attr("disabled", false).removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.attr("disabled", true).addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.attr("disabled", false).removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.attr("disabled", true).addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    ShowIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    HideIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        } else {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            } else {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            }
        });
    },
    ShowIfSelectEquals: function(select, value, field) {
        if (select.val() == value) {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        select.bind('change', function() {
            if (select.val() == value) {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    NoneOfTheAbove: function(checkbox, group) {
        if (checkbox.prop("checked")) group.each(function() {
            if ($(this).prop("id") != checkbox.prop("id")) $(this).prop("checked", false).change();
        });
        group.change(function() {
            if ($(this).prop("id") != checkbox.prop("id") && $(this).prop("checked")) checkbox.prop("checked", false).change();
        });
        checkbox.change(function() {
            if ($(this).prop("checked")) group.each(function() {
                if ($(this).prop("id") != checkbox.prop("id")) $(this).prop("checked", false).change();
            });
        });
    },
    ToggleValidation: function(obj, enable) {
        if ((enable != undefined && !enable) || obj.hasClass('required-disabled') || $(".required-disabled", obj).length || obj.hasClass('requireddropdown-disabled') || $(".requireddropdown-disabled", obj).length) {
            if (obj.hasClass('required-disabled')) obj.removeClass('required-disabled').addClass('required');
            if ($(".required-disabled", obj).length) $(".required-disabled", obj).removeClass('required-disabled').addClass('required');
            if (obj.hasClass('requireddropdown-disabled')) obj.removeClass('requireddropdown-disabled').addClass('requireddropdown');
            if ($(".requireddropdown-disabled", obj).length) $(".requireddropdown-disabled", obj).removeClass('requireddropdown-disabled').addClass('requireddropdown');
        } else {
            if (obj.hasClass('required')) obj.removeClass('required').addClass('required-disabled');
            if ($(".required", obj).length) $(".required", obj).removeClass('required').addClass('required-disabled');
            if (obj.hasClass('requireddropdown')) obj.removeClass('requireddropdown').addClass('requireddropdown-disabled');
            if ($(".requireddropdown", obj).length) $(".requireddropdown", obj).removeClass('requireddropdown').addClass('requireddropdown-disabled');
        }
    },
    DeleteTemplateWithInput: function(type, input, callback, name) {
        if (name == undefined) name = type;
        U.Delete(name, type + "/Delete", { Id: input.Id, PatientId: input.PatientId, EpisodeId: input.EpisodeId },
            function() {
                eval(type + ".RebindList()");
                if (callback != undefined && typeof (callback) == 'function') callback();
            });
    },
    DeleteTemplate: function(type, id, callback, name) {
        if (name == undefined) name = type;
        U.Delete(name, type + "/Delete", { Id: id },
             function() {
                 eval(type + ".RebindList()");
                 if (callback != undefined && typeof (callback) == 'function') callback();
             });
    },
    InitTemplate: function(formobj, callback, message, before) {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        formobj.validate({
            submitHandler: function(form) {
                if (before != undefined && typeof (before) == 'function') before();
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(message, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $(formobj.selector + " .zip").ZipInput(formobj);
    },
    InitEditTemplate: function(type, callback, literalType) {
        if (literalType == undefined) literalType = type.toLowerCase();
        U.InitTemplate($("#edit" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("edit" + type.toLowerCase());
        }, literalType + " successfully updated");
    },
    InitNewTemplate: function(type, callback, literalType) {
        if (literalType == undefined) literalType = type.toLowerCase();
        U.InitTemplate($("#new" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("new" + type.toLowerCase());
        }, "New " + literalType + " successfully added");
    },
    ShowDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    CloseDialog: function() {
        $.unblockUI();
    },
    GetUrl: function(url, input, onSuccess) {
        //Acore.Abort(url);
        //Acore.RequestArray[url] = 
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    PostUrl: function(url, input, onSuccess, onFailure) {
        //Acore.Abort(url);
        //Acore.RequestArray[url] = 
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    PostUrlNoAbort: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    Block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: "<img src='/Images/loading.gif' style='padding:4em;'>",
            css: {
                "background-color": "#fff",
                "border": "0 none",
                "top": "50%",
                "left": "50%",
                "margin": "-10em",
                "text-align": "center",
                "width": "20em",
                "height": "20em",
                "border-radius": "20em",
                "-moz-border-radius": "20em",
                "-webkit-border-radius": "20em",
                "-o-border-radius": "20em",
                "-khtml-border-radius": "20em",
                "box-shadow": "-.3em -.5em 3em #000 inset",
                "-moz-box-shadow": "-.3em -.5em 3em #000 inset",
                "-webkit-box-shadow": "-.3em -.5em 3em #000 inset",
                "-o-box-shadow": "-.3em -.5em 3em #000 inset",
                "-khtml-box-shadow": "-.3em -.5em 3em #000 inset"
            }
        });
    },
    UnBlock: function(selector) {
        $.unblockUI();
    },
    ToTitleCase: function(text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    },
    ClearRadio: function(selector) {
        $("input[name=" + selector + "]").each(function() { $(this).removeAttr('checked'); });
    },
    ChangeToRadio: function(selector) {
        var $checkbox = $("input[name=" + selector + "]");
        $checkbox.click(function() {
            if ($(this).attr('checked')) {
                $checkbox.removeAttr('checked');
                $(this).attr('checked', true);
            }
        });
    },
    TimePicker: function(selector) {
        $(selector).TimePicker();
    },
    ParameterByName: function(url, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null) return "";
        else return decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    RebindDataGridContent: function(id, url, jsonData, sortParams) {
        var input = jsonData;
        if (sortParams != null && sortParams != undefined) {
            $.extend(input, { SortParams: sortParams });
        }
        $("#" + id + "GridContainer").empty().addClass("loading").css({ "position": "absolute", "width": "100%", "height": "85%" }).load(url, input, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("This page can't be loaded. Please close this window and try again.", 'error');
            }
            else if (textStatus == "success") {
                $(this).removeClass("loading");
                $(this).css({ "position": "", "width": "", "height": "" })
                var $exportLink = $('#' + id + '_ExportLink');
                if ($exportLink != null && $exportLink != undefined) {
                    var href = $exportLink.attr('href');
                    if (href != null && href != undefined) {
                        $.each(jsonData, function(key, value) {
                            var filter = new RegExp(key + "=([^&]*)");
                            href = href.replace(filter, key + '=' + value);
                        });
                        $exportLink.attr('href', href);
                    }
                }
            }
        });
    },
    RebindPageContent: function(Id, url, jsonData) {
        $("#" + Id + "Container").empty().addClass("loading").load(url, jsonData, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl("This page can't be loaded. Please close this window and try again.", 'error'); }
            else if (textStatus == "success") { $(this).removeClass("loading"); }
        });
    },
    BlockButton: function(btn) {
        btn.attr("old-onclick", btn.attr("onclick"));
        btn.removeAttr("onclick");
        btn.attr("old-text", btn.text());
        btn.addClass("blocked-button");
        btn.text("Please Wait...");
    },
    UnBlockButton: function(btn) {
        btn.attr("onclick", btn.attr("old-onclick"));
        btn.removeAttr("old-onclick");
        btn.removeClass("blocked-button");
        btn.text(btn.attr("old-text"));
        btn.removeAttr("old-text");
    },
    BlockClick: function() {
        return false;
    },
    PullMultiComboBoxValues: function(comboBx) {
        if (comboBx.val() != "") {
            return comboBx.val();
        }
        var checkedOptions = comboBx.next().children(".checked");
        var inputs = checkedOptions.find("input");
        return inputs.map(function() { return this.value; }).get().join(',');
    },
    HtmlEncode: function(value) {
        if (value) {
            return jQuery('<div />').text(value).html();
        } else {
            return '';
        }
    },
    HtmlDecode: function(value) {
        if (value) {
            return $('<div />').html(value).text();
        } else {
            return '';
        }
    },
    DoIfChecked: function(show, checkbox, doFunction, undoFunction) {
        if (checkbox.prop("checked") == show) {
            doFunction();
        } else {
            undoFunction();
        }
        checkbox.change(function() {
            if (checkbox.prop("checked") == show) {
                doFunction();
            } else {
                undoFunction();
            }
        });
    },
    BasicTabSetup: function(Element) {
        $("input[type=radio]", Element).DeselectableRadio();
        // Append red asterisk to required fields
        $(".row .required,.row .requireddropdown", Element).closest(".row").prepend("<span class='required-red abs-right'>*</span>");

        if ($(".row .required", Element).length) Element.prepend(
            $("<div/>", { "class": "abs required-legend", text: " = Required Field" }).prepend(
                $("<span/>", { "class": "required-red", text: "*" })));

        // Initialize special inputs
        $(".date-picker", Element).DatePicker();
        $(".time-picker", Element).TimePicker();
        // $("input[type=file]", Element).StyledUploader();
        $(".physicians", Element).PhysicianInput();
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        // $(".row .phone-short:eq(0)", Element).PhoneAutoTab();
        $(".numeric", Element).numeric();
        $(".floatnum", Element).floatnum();

        $("[status]").mouseover(function() {
            $(this).closest(".window").Status($(this).attr("status"))
        }).mouseout(function() {
            $(this).closest(".window").Status("")
        });
        // Set checkgoup functionality
        $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", Element).each(function() {
            if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
            else $(this).closest(".option").removeClass("selected");
            $(this).change(function() {
                $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", Element).each(function() {
                    if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                    else $(this).closest(".option").removeClass("selected");
                })
            })
        });
    },
    FormatDate: function(date) {
        var dateToFormat = new Date(date);
        var month = dateToFormat.getMonth() + 1;
        var day = dateToFormat.getDate().toString();
        if (day.length == 1) day = "0" + day;
        var year = dateToFormat.getFullYear();
        return month + "/" + day + "/" + year;
    },
    FormatTime: function(date) {
        var dateToFormat = new Date(date);
        var hours = dateToFormat.getHours();
        if (hours >= 12) {
            hours -= 12;
            var meridian = "PM";
        } else var meridian = "AM";
        if (hours == 0) hours = 12;
        var minutes = dateToFormat.getMinutes().toString();
        if (minutes.length == 1) minutes = "0" + minutes;
        return hours + ":" + minutes + " " + meridian;
    },
    ConvertJsonDateToJavascriptDate: function(jsonDate) {
        if (jsonDate.indexOf("Date") != -1 && jsonDate.length > 6) {
            return new Date(parseInt(jsonDate.substr(6)));
        }
        return new Date();
    }
};
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }
