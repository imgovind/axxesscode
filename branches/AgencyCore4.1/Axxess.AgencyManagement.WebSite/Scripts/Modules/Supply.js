﻿var Supply = {
    _isRebinding: false,
    Delete: function(id) { U.DeleteTemplate("Supply", id); },
    InitEdit: function() {
        U.InitEditTemplate("Supply");
    },
    InitNew: function() {
        U.InitNewTemplate("Supply");
    },
    RebindList: function() { Supply._isRebinding = true; U.RebindTGrid($('#List_Supply')); Supply._isRebinding = false; },
    OnSupplyEdit: function(e) {
        if (e != null && e != undefined && e.mode == 'edit') {
            $(e.form).find("input[name=RevenueCode]").val(e.dataItem.RevenueCode);
            $(e.form).find("input[name=Code]").val(e.dataItem.Code);
        }
        $(e.form).find("input[name=Description]").AjaxAutocomplete({
            ExtraParameters: { 'limit': 50 },
            minLength: 1,
            SourceUrl: "Agency/SuppliesSearch",
            Format: function(json) { return json.Description },
            Select: function(json, input) {
                input.val(json.Description);
                $(e.form).find("input[name=Code]").val(json.Code);
                $(e.form).find("input[name=UnitCost]").val(json.UnitCost);
                $(e.form).find("input[name=UniqueIdentifier]").val(json.Id);
                $(e.form).find("input[name=RevenueCode]").val(json.RevenueCode);
            }
        });
    },
    RowSelected: function(e) {
        $("[type=checkbox]", e.row).click()
    },
    InitNewBillingSupply: function(responseText, textStatus, XMLHttpRequest, Element) {
        var Type = Element.find("#Type").val();
        Supply.InitBillingSupply(Element, "New", true, Type, "Supply created successfully.");
    },
    InitEditBillingSupply: function(responseText, textStatus, XMLHttpRequest, Element) {
        var Type = Element.find("#Type").val();
        Supply.InitBillingSupply(Element, "Edit", true, Type, "Supply updated successfully.");
    },
    InitEditUnBillingSupply: function(responseText, textStatus, XMLHttpRequest, Element) {
        var Type = Element.find("#Type").val();
        Supply.InitBillingSupply(Element, "Edit", false, Type, "Supply updated successfully.");
    },
    RebindBillingSupplyList: function(locator) {
        Supply._isRebinding = true;
        U.RebindTGrid($(locator));
        Supply._isRebinding = false;
    },
    InitBillingSupply: function(Element, Page, isBillable, Type, Message) {
        var pageLower = Page.toLowerCase()
        $("#" + pageLower + Type + "ClaimSupplyForm").find("input[name=Description]").AjaxAutocomplete({
            ExtraParameters: { 'limit': 50 },
            minLength: 1,
            SourceUrl: "Agency/SuppliesSearch",
            Format: function(json) { return json.Description },
            Select: function(json, input) {
                input.val(json.Description);
                Element.find("input[name=Code]").val(json.Code);
                Element.find("input[name=UnitCost]").val(json.UnitCost);
                Element.find("input[name=UniqueIdentifier]").val(json.Id);
                Element.find("input[name=RevenueCode]").val(json.RevenueCode);
            }
        });
        $("#" + pageLower + Type + "ClaimSupplyForm #Quantity").bind('input', function(e) {
            var units = $(this).val();
            var cost = $("#" + pageLower + Type + "ClaimSupplyForm #UnitCost").val();

            if (!isNaN(units) && !isNaN(cost)) {
                var total = units * cost;
                $("#" + Page + "Supply_TotalCost").val(U.FormatMoney(total));
            }
        });

        $("#" + pageLower + Type + "ClaimSupplyForm #UnitCost").bind('input', function(e) {
            var cost = $(this).val();
            var units = $("#" + pageLower + Type + "ClaimSupplyForm #Quantity").val();

            if (!isNaN(units) && !isNaN(cost)) {
                var total = units * cost;
                $("#" + Page + "Supply_TotalCost").val(U.FormatMoney(total));
            }
        });
        var billingGridLocator = "#" + Type + (isBillable ? "" : "Un") + "BillingSupplyGrid";
        U.InitTemplate($("#" + pageLower + Type + "ClaimSupplyForm"),
            function() {
                UserInterface.CloseModal();
                Supply.RebindBillingSupplyList(billingGridLocator);
            }, Message);
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable, Type) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
                inputs[inputs.length] = { name: "Type", value: Type };
                U.PostUrl("Billing/ChangeSupplyBillStatus", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        Supply._isRebinding = true;
                        U.RebindTGrid($("#" + Type + "BillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#" + Type + "UnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        Supply._isRebinding = false;
                    }
                })
            }
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control, Type) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else {
            if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
                if (inputs != null) {
                    inputs[inputs.length] = { name: "Id", value: Id };
                    inputs[inputs.length] = { name: "PatientId", value: PatientId };
                    inputs[inputs.length] = { name: "Type", value: Type };
                    U.PostUrl("Billing/SuppliesDelete", inputs, function(result) {
                        if (result != null && result.isSuccessful) {
                            Supply._isRebinding = true;
                            U.RebindTGrid(control, { Id: Id, patientId: PatientId });
                            Supply._isRebinding = false;
                        }
                    })
                }
            }
        }
    },
    OnDataBinding: function(e) {
        if (!Supply._isRebinding) {
            e.preventDefault();
        }
    }
}