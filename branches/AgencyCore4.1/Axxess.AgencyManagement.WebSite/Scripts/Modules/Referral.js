﻿var Referral = {
    Delete: function(Id) { U.DeleteTemplate("Referral", Id); },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Referral_HomePhone");
        U.PhoneAutoTab("Edit_Referral_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("Edit_Referral_EmergencyContactPhoneAlternate");
        U.InitEditTemplate("Referral");
        Template.OnChangeInit();
        $("#window_editreferral .Physicians").PhysicianInput();
        $("#EditReferral_NewPhysician").click(function() {
            var PhysId = $("#EditReferral_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Referral.AddPhysician(PhysId, $("#Edit_Referral_Id").val());
            else U.Growl("Please Select a Physician", "error");
        });
    },
    InitNew: function() {
        U.PhoneAutoTab("New_Referral_HomePhone");
        U.PhoneAutoTab("New_Referral_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("New_Referral_EmergencyContactPhoneAlternate");
        U.ShowIfChecked($("#New_Referral_DMEOther"), $("#New_Referral_OtherDME"));
        $("#window_newreferral .Physicians").PhysicianInput();
        U.InitNewTemplate("Referral");
        Template.OnChangeInit();
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Referral_Date").datepicker("hide");
        $("#newNonAdmitReferralForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("Referral non-admission successful.", "success");
                            Referral.RebindList();
                            UserInterface.CloseModal();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    RebindList: function() { U.RebindTGrid($('#List_Referral')); },
    Admit: function(id) {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); }, { referralId: id });
    },
    AddPhysician: function(id, referralId) {
        U.TGridAjax("/Referral/AddPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
        $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, referralId) {
        if (confirm("Are you sure you want to remove this physician?")) U.TGridAjax("/Referral/DeletePhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    },
    SetPrimaryPhysician: function(id, referralId) {
        if (confirm("Are you sure you want to set this physician as primary?")) U.TGridAjax("/Referral/SetPrimaryPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    }
}