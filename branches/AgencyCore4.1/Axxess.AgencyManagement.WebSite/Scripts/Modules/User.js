﻿var User = {
    InitNewRate: function() { U.InitTemplate($("#newUserRateForm"), function() { User.RebindRates(); UserInterface.CloseModal(); }, "Rate successfully added"); },
    InitEditRate: function() { U.InitTemplate($("#editUserRateForm"), function() { User.RebindRates(); UserInterface.CloseModal(); }, "Rate successfully updated"); },
    InitNew: function() {
        if ($("#newUserForm").length) {
            $(".numeric").numeric();
            $(".names").alpha({ nocaps: false });
            U.PhoneAutoTab("New_User_HomePhoneArray");
            U.PhoneAutoTab("New_User_MobilePhoneArray");
            U.PhoneAutoTab("New_User_FaxPhoneArray");
            U.ShowIfOtherSelected($("#New_User_TitleType"), $("#New_User_OtherTitleType"));
            U.ShowIfOtherSelected($("#New_User_Credentials"), $("#New_User_OtherCredentials"));
            $('#New_User_AllPermissions').change(function() {
                if ($('#New_User_AllPermissions').attr('checked')) {
                    $('input[name="PermissionsArray"]').each(function() {
                        if (!$(this).attr('checked')) {
                            $(this).attr('checked', true);
                        }
                    });
                }
                else {
                    $('input[name="PermissionsArray"]').each(function() {
                        if ($(this).attr('checked')) {
                            $(this).attr('checked', false);
                        }
                    });
                }
            });
            $("#newUserForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        clearForm: false,
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                User.RebindList();
                                U.Growl("New user successfully added.", "success");
                                UserInterface.CloseWindow('newuser');
                            } else U.Growl(resultObject.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return true;
                }
            })
        }
    },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_User_HomePhoneArray");
        U.PhoneAutoTab("Edit_User_MobilePhoneArray");
        U.PhoneAutoTab("Edit_User_FaxPhoneArray");
        $('#Edit_User_AllPermissions').change(function() {
            if ($('#Edit_User_AllPermissions').attr('checked')) {
                $('input[name="PermissionsArray"]').each(function() {
                    if (!$(this).attr('checked')) {
                        $(this).attr('checked', true);
                    }
                });
            }
            else {
                $('input[name="PermissionsArray"]').each(function() {
                    if ($(this).attr('checked')) {
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        U.ShowIfOtherSelected($("#Edit_User_TitleType"), $("#Edit_User_OtherTitleType"));
        U.ShowIfOtherSelected($("#Edit_User_Credentials"), $("#Edit_User_OtherCredentials"));

        $("#editUserForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            User.RebindList();
                            U.Growl("User successfully updated.", "success");
                            UserInterface.CloseWindow('edituser');
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        var file_input_index = 0;
        $('#editUserLicenseForm input[type=file]').each(function() {
            file_input_index++;
            $(this).wrap('<div id="Edit_UserLicense_Attachment_Container_' + file_input_index + '"></div>');
        });
        $("#editUserLicenseForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editUserPermissionsForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("User permissions updated successfully.", "success");
                        } else U.Growl($.trim(result.errorMessage), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitProfile: function() {
        U.PhoneAutoTab("Edit_Profile_PhonePrimary");
        U.PhoneAutoTab("Edit_Profile_PhoneAlternate");
        $("#editProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Profile updated successfully.", "success");
                            UserInterface.CloseWindow('editprofile');
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").click(function() {
            U.PostUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Your request to reset your signature was successful. Please check your e-mail.", "success");
                    UserInterface.CloseWindow('forgotsignature');
                }
                else {
                    $("#resetSignatureMessage").addClass("error-message").html(result.errorMessage);
                }
            });
        });
    },
    InitMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Missed Visit successfully created.", "success");
                            UserInterface.CloseWindow('newmissedvisit');
                            User.RebindScheduleList();
                            UserInterface.CloseModal();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitEditMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Missed Visit successfully created.", "success");
                            UserInterface.CloseAndRefresh('editmissedvisit');
                            //User.RebindScheduleList();
                            UserInterface.CloseModal();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    ListUserRowDataBound: function(e) {
        $("a.tooltip", e.row).each(function() {
            if ($(this).html().length) $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), "") }).tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: -15,
                bodyHandler: function() {
                    return $(this).html();
                }
            });
            else $(this).remove();
        })
    },
    SelectPermissions: function(control, className) {
        if (control.attr("checked")) $(className).each(function() {
            if (!$(this).attr("checked")) $(this).attr("checked", true);
        });
        else $(className).each(function() {
            if ($(this).attr("checked")) $(this).attr("checked", false);
        })
    },
    RebindList: function() {
        User.RebindActiveUser({ SortParams: "DisplayName" });
        User.RebindInactiveUser({ SortParams: "DisplayName" });
    },
    RebindActiveUser: function(SortParams) {
        $("#ActiveUsers-GridContainer").empty().addClass("loading").load('User/ActiveContent', { SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error')
            { U.Growl('Active Users List could not be grouped. Please close this window and try again.', "error"); }
            else if (textStatus == "success")
            { $("#ActiveUsers-GridContainer").removeClass("loading"); }
        });
    },
    RebindInactiveUser: function(SortParams) {
        $("#InactiveUsers-GridContainer").empty().addClass("loading").load('User/InActiveContent', { SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error')
            { U.Growl('Inactive Users List could not be loaded. Please close this window and try again.', "error"); }
            else if (textStatus == "success")
            { $("#InactiveUsers-GridContainer").removeClass("loading"); }
        });
    }
,
    RebindScheduleList: function() { User.LoadUserSchedule('VisitDate', 'PatientName-ASC'); },
    LoadUserSchedule: function(groupName, SortParams) {
        $("#myScheduledTasksContentId").empty().addClass("loading").load('User/ScheduleGrouped', { groupName: groupName, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl('Schedule List could not be grouped. Please close this window and try again.', "error"); }
            else if (textStatus == "success") { $("#myScheduledTasksContentId").removeClass("loading"); }
        });

    },
    Delete: function(userId) {
        if (confirm("Are you sure you want to delete this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Delete", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Activate: function(userId) {
        if (confirm("Are you sure you want to activate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Activate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Deactivate: function(userId) {
        if (confirm("Are you sure you want to deactivate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Deactivate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    NavigateMonthlyCalendar: function(month, year) {
        $("#window_userschedulemonthlycalendar_content").Load("User/UserCalendarNavigate", { month: month, year: year }, function(r, t, x, e) {
            if (t == "error") $("#window_userschedulemonthlycalendar_content").html(U.AjaxError);
        })
    },
    LoadUserCalendar: function(month, year, SortParams) {
        $("#UserCalendar_GridContainer").empty().addClass("loading").load('User/UserVisits', { month: month, year: year, SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl('User calendar could not be grouped. Please close this window and try again.', "error"); }
            else if (textStatus == "success") { $("#UserCalendar_GridContainer").removeClass("loading"); }
        });
    },
    LoadRate: function(fromId, toId) {
        if (confirm("Are you sure you want to apply the same rate as the selected user?")) {
            U.PostUrl("/User/LoadUserRate", { fromId: fromId, toId: toId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    User.RebindRates();
                }
                else {
                    $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    NewRate: function(userId) {
        Acore.Modal({
            Name: "New User Rate",
            Url: "User/NewUserRate",
            Input: { userId: userId },
            OnLoad: User.InitNewRate,
            Width: "500px",
            Height: "265px",
            WindowFrame: false
        });
    },
    EditRate: function(userId, id, insurance) {
        Acore.Modal({
            Name: "Edit User Rate",
            Url: "User/EditUserRate",
            Input: { userId: userId, id: id, insurance: insurance },
            OnLoad: User.InitEditRate,
            Width: "500px",
            Height: "265px",
            WindowFrame: false
        });
    },
    DeleteRate: function(userId, id, insurance) {
        if (confirm("Are you sure you want to delete this rate?")) {
            U.PostUrl("/User/DeleteUserRate", { userId: userId, id: id, insurance: insurance }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    User.RebindRates();
                }
                else {
                    $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    RebindRates: function() {
        var grid = $('#List_User_Rates').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}