﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PrivateDutyScheduleTask>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PrivateDuty/Event/Update", "Schedule", FormMethod.Post, new { @id = "EditPrivateDutyTask_Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditPrivateDutyTask_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditPrivateDutyTask_PatientId" })%>
    <fieldset>
        <legend>Edit Task</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left strong">Patient</label>
                <div class="float-right"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="float-left strong">Task</label>
                <div class="float-right"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyTask_UserId">User/Employee</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserName, new { @id = "EditPrivateDutyTask_UserId" })%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyTask_StartDate">Start Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="EventStartTime.Date" id="EditPrivateDutyTask_StartDate" value="<%= Model.EventStartTime.ToShortDateString() %>" />
                    <input type="text" class="time-picker required" name="EventStartTime.Time" id="EditPrivateDutyTask_StartTime" value="<%= Model.EventStartTime.ToShortTimeString() %>" />
                </div>
                <div class="clr"></div>
                <div class="float-right">
                    <%= string.Format("<input type=\"checkbox\" class=\"radio\" name=\"AllDay\" id=\"EditPrivateDutyTask_AllDay\" {0} />", Model.IsAllDay.ToChecked()) %>
                    <label for="EditPrivateDutyTask_AllDay">All Day Event</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left strong" for="EditPrivateDutyTask_EndDate">End Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="EventEndTime.Date" id="EditPrivateDutyTask_EndDate" value="<%= Model.EventStartTime.ToShortDateString() %>" />
                    <input type="text" class="time-picker required" name="EventEndTime.Time" id="EditPrivateDutyTask_EndTime" value="<%= Model.EventEndTime.ToShortTimeString() %>" />
                </div>
            </div>
            <div class="row">
                <label class="fl strong" for="EditPrivateDutyTask_Comments">Comments</label>
                <div class="fr"><%= Html.TextArea("Comments", Model.Comments, new { @id = "EditPrivateDutyTask_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>