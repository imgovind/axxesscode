﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Schedule", FormMethod.Post, new { @id = "NewPrivateDutyTask_Form" })) { %>
<%= Html.Hidden("PatientId", Model.Id, new { @id = "NewPrivateDutyTask_PatientId" })%>
    <fieldset>
        <legend>Employee Scheduler</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left strong">Patient</label>
                <div class="float-right"><%= Model.DisplayNameWithMi %></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyTask_DisciplineTask">Task</label>
                <div class="float-right"><%= Html.MultipleDisciplineTasks("DisciplineTask", "", new Dictionary<string, string>() { { "class", "requireddropdown" } })%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyTask_UserId">User/Employee</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", "", new { @id = "NewPrivateDutyTask_UserId", @class = "required" })%></div>
            </div>
            <div class="row">
                <label class="float-left strong" for="NewPrivateDutyTask_StartDate">Start Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker required" name="EventStartTime.Date" id="NewPrivateDutyTask_StartDate" />
                    <input type="text" class="time-picker required" name="EventStartTime.Time" id="NewPrivateDutyTask_StartTime" />
                    <br />
                    <input type="checkbox" class="radio" name="AllDay" id="NewPrivateDutyTask_AllDay" />
                    <label for="NewPrivateDutyTask_AllDay">All Day Event</label>
                </div>
            </div>
            <div id="NewPrivateDutyTask_End" class="row">
                <label class="float-left strong" for="NewPrivateDutyTask_EndDate">End Time</label>
                <div class="float-right">
                    <input type="text" class="date-picker " name="EventEndTime.Date" id="NewPrivateDutyTask_EndDate" />
                    <input type="text" class="time-picker " name="EventEndTime.Time" id="NewPrivateDutyTask_EndTime" />
                </div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewPrivateDutyTask_Comments">Comments</label>
                <div class="fr"><%= Html.TextArea("Comments", new { @id = "NewPrivateDutyTask_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("AddAnother", "0", new { @id = "NewPrivateDutyTask_AddAnother" })%>
    <div class="buttons">
        <ul>
            <li><a class="save">Save and Close</a></li>
            <li><a class="save clear">Save and Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>