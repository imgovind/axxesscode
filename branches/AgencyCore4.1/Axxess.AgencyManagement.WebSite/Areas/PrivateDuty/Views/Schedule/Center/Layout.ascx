﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Private Duty Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west layout-left">
        <div class="top"><%  Html.RenderPartial("Center/PatientFilters"); %></div>
        <div class="bottom"><%  Html.RenderPartial("Center/PatientSelector"); %></div>
    </div>
    <div id="PDScheduleMainResult" class="ui-layout-center layout-main">
        <div class="window-menu">
            <ul>
                <li><a class="new-task">New Task</a></li>
                <li><a class="schedule-employee">Schedule Employee</a></li>
                <li><a class="reassign-schedule">Reassign Schedules</a></li>
                <li><a class="master-calendar">Master Calendar</a></li>
            </ul>
        </div>
        <div class="main">
            <div class="pd-calendar"></div>
            <fieldset class="calendar-legend">
                <ul>
                    <li><div class="scheduled">&#160;</div> Scheduled</li>
                    <li><div class="completed">&#160;</div> Completed</li>
                    <li><div class="missed">&#160;</div> Missed</li>
                </ul>
            </fieldset>
            <div class="list-collapsed">
                <a class="show-list">List Tasks</a>
                <a class="hide-list hidden">Hide List</a>
            </div>
            <div class="schedule-list"><% /* Html.RenderPartial("Center/List");*/ %></div>
        </div>
    </div>
</div>