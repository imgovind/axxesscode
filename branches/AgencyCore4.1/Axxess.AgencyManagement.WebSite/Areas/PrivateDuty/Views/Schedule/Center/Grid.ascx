﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PrivateDutyScheduleTask>>" %>
<%  Html.Telerik().Grid(Model).Name("PrivateDutyTask_Grid").Columns(columns => {
        columns.Bound(s => s.Id).Visible(false).Title("");
        columns.Bound(s => s.DisciplineTaskName).Title("Task").Width(180);
        columns.Bound(s => s.EventStartTime).Title("Scheduled Time").Width(120);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(130);
        columns.Bound(s => s.StatusName).Title("Status").Width(100);
        columns.Bound(s => s.StatusComment).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\"><#=StatusComment#></a>");
        columns.Bound(s => s.Comments).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\"><#=CommentsCleaned#></a>");
        columns.Bound(s => s.PrintUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=PrintUrl#>");
        columns.Bound(s => s.AttachmentUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<#=AttachmentUrl#>");
        columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
    }).ClientEvents(c => c.OnDataBound("PrivateDuty.InitList")).Sortable().Scrollable().Footer(false).Render(); %>
<script type="text/javascript">
    $("#PrivateDutyTask_Grid .t-grid-content").css("height", "auto");
</script>