﻿<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css" media="screen">
		p {
			margin: 0 0 10px 0;
			font-family: arial, sans-serif;
			font-size: 12px;
		}

		body, div, td, th, textarea, input, h2, h3 {
			font-family: arial, sans-serif;
			font-size: 11px;
		}
	</style>
	<body style="font-family: arial, sans-serif; font-size: 12px;">
		<p>Hello <%=firstname%>,</p>
		<p>You requested a password change. If you did not request this change, please contact Axxess support immediately at support@axxessweb.com.</p>
		<p>To complete your password change request, go to:<br />
		<a href='<%=baseurl%>Reset<%=encryptedQueryString%>'><%=baseurl%>Reset<%=encryptedQueryString%></a>
		</p>
		<p>You will be required to create a new password to complete the password change request. Please choose a password you can easily remember.</p>
		<p>Thanks for choosing Axxess&trade;,<br />
		The Axxess&trade; team</p>
		<p>This is an automated e-mail, please do not reply.</p>
		<p>This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>