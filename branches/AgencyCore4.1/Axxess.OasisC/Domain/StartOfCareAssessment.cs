﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class StartOfCareAssessment : Assessment
    {
        public StartOfCareAssessment()
        {
            this.Type = AssessmentType.StartOfCare;
        }

        //public override string ToString()
        //{
        //    return string.Format("StartofCare: AgencyId: {0}  EpisodeId: {1}  PatientId: {2}",
        //        this.AgencyId, this.EpisodeId, this.PatientId);
        //}
    }
}
