﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/01/")]
    public class PPSStandard
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime Time { get; set; }
        [DataMember]
        public double UrbanRate { get; set; }
        [DataMember]
        public double RuralRate { get; set; }
        [DataMember]
        public double Labor { get; set; }
        [DataMember]
        public double NonLabor { get; set; }
        [DataMember]
        public double S { get; set; }
        [DataMember]
        public double RuralS { get; set; }
        [DataMember]
        public double T { get; set; }
        [DataMember]
        public double RuralT { get; set; }
        [DataMember]
        public double U { get; set; }
        [DataMember]
        public double RuralU { get; set; }
        [DataMember]
        public double V { get; set; }
        [DataMember]
        public double RuralV { get; set; }
        [DataMember]
        public double W { get; set; }
        [DataMember]
        public double RuralW { get; set; }
        [DataMember]
        public double X { get; set; }
        [DataMember]
        public double RuralX { get; set; }
    }
}
