﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Cache/2011/11/")]
    public class UniqueItem
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Xml { get; set; }
    }
}
