﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/11/")]
    public class ScheduleEvent
    {
        public ScheduleEvent()
        {
            this.UserId = Guid.Empty;
            this.EventId = Guid.Empty;
            this.EventDate = string.Empty;
            this.Assets = new List<Guid>();
        }

        public Guid EventId { get; set; }
        public int DisciplineTask { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string EventDate { get; set; }
        public string VisitDate { get; set; }
        public string Status { get; set; }
        public string Discipline { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMissedVisit { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string Surcharge { get; set; }
        public string AssociatedMileage { get; set; }
        public string ReturnReason { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsOrderForNextEpisode { get; set; }
        public bool IsVisitPaid { get; set; }
        public bool InPrintQueue { get; set; }
        public List<Guid> Assets { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}