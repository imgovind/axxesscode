﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class WebinarUser
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AgencyName { get; set; }
    }
}
