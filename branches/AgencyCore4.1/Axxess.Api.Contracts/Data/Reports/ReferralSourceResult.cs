﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/ReferralSourceResult/2012/03/")]
    public class ReferralSourceResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Admissions { get; set; }
    }
}
