﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/BaseCaliforniaReportResult/2012/03/")]
    public abstract class BaseCaliforniaReportResult
    {
        [DataMember]
        public string LineNumber { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}