﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/AgencyCacheFault/2011/10/", Name = "AgencyCacheFault")]
    public class AgencyCacheFault : DefaultFault
    {
        public AgencyCacheFault() : base() {  }

        public AgencyCacheFault(string message) : base((int)FaultReasons.AgencyCacheFault, message) { }
    }
}
