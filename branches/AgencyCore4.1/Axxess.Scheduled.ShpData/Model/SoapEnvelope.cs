﻿namespace Axxess.Scheduled.ShpData
{
    using System.Xml;
    using System.Xml.Serialization;

    [XmlType(Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SoapEnvelope
    {
        [XmlAttribute(AttributeName = "soap", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string soapenva { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2001/XMLSchema")]
        public string xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string xsi { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public ResponseBody<AcceptSHPDataResponse> body { get; set; }
        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
        public SoapEnvelope()
        {
            xmlns.Add("soap", "http://schemas.xmlsoap.org/soap/envelope/");
        }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ResponseBody<T>
    {
        [XmlElement(Namespace = "http://www.shpdata.com/SHPDataAcceptor")]
        public T AcceptSHPDataResponse { get; set; }
    }

    [System.SerializableAttribute()]
    public class AcceptSHPDataResponse
    {
        public AcceptSHPDataResult AcceptSHPDataResult { get; set; }
    }

    [System.SerializableAttribute()]
    public class AcceptSHPDataResult
    {
        public SHP_DATA_RETURN SHP_DATA_RETURN { get; set; }
    } 
}
