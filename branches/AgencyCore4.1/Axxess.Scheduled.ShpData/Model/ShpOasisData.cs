﻿namespace Axxess.Scheduled.ShpData
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SHP_OASIS_DATA
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string PROVIDER_ID { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string ACTIVATION_CODE { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string EMAIL_CONFIRMATION { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public int BATCH { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public int TOTAL_BATCHES { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public System.DateTime BATCH_KEY_TIME_STAMP { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public SHP_OASIS_DATAOASIS_DATA OASIS_DATA { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class SHP_OASIS_DATAOASIS_DATA
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public SHP_OASIS_DATAOASIS_DATAOASIS_HEADER OASIS_HEADER { get; set; }

        [XmlElementAttribute("OASIS_BODY", Form = XmlSchemaForm.Unqualified)]
        public SHP_OASIS_DATAOASIS_DATAOASIS_BODY[] OASIS_BODY { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public SHP_OASIS_DATAOASIS_DATAOASIS_TRAILER OASIS_TRAILER { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class SHP_OASIS_DATAOASIS_DATAOASIS_HEADER
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string HEADER_VALUE { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class SHP_OASIS_DATAOASIS_DATAOASIS_BODY
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string BODY_VALUE { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string CLINICIAN_NAME { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string CLINICIAN_SPECIALTY { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string CASE_MANAGER { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string TEAM_NAME { get; set; }

        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string CUSTOMER_DEFINED { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class SHP_OASIS_DATAOASIS_DATAOASIS_TRAILER
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string TRAILER_VALUE { get; set; }
    }
}
