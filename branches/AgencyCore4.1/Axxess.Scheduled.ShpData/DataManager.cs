﻿namespace Axxess.Scheduled.ShpData
{
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using System.Collections.Generic;
    using System;

    internal sealed class DataManager
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly DataManager instance = new DataManager();
        }

        #endregion

        #region Private Members

        private int batchId;
        private SHP_OASIS_DATA batch = null;
        private List<SHP_OASIS_DATAOASIS_DATAOASIS_BODY> body;

        #endregion

        #region Public Instance

        public static DataManager Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private DataManager()
        {
            this.Initialize();
        }

        private void Initialize()
        {
            batchId = DateTime.Now.ToInteger();
            batch = new SHP_OASIS_DATA();
            batch.BATCH = batchId;
            batch.OASIS_DATA = new SHP_OASIS_DATAOASIS_DATA();
            batch.OASIS_DATA.OASIS_HEADER = new SHP_OASIS_DATAOASIS_DATAOASIS_HEADER();
            batch.OASIS_DATA.OASIS_TRAILER = new SHP_OASIS_DATAOASIS_DATAOASIS_TRAILER();
            body = new List<SHP_OASIS_DATAOASIS_DATAOASIS_BODY>();
        }

        private void Flush()
        {
            if (body != null && body.Count > 0)
            {
                batch.OASIS_DATA.OASIS_BODY = body.ToArray();
                Console.WriteLine("Batch Id: {0} ProviderNo: {1} Activation Code: {2} Body: {3}", batchId, batch.PROVIDER_ID, batch.ACTIVATION_CODE, batch.OASIS_DATA.OASIS_BODY.Length, batch.BATCH);
                body.ForEach(b =>
                {
                    Console.WriteLine(b.BODY_VALUE);
                });
            }
        }

        #endregion

        #region Public Methods / Properties

        public int BatchId
        {
            get
            {
                return batchId;
            }
        }

        public void Add(string providerNo, string activationCode, string headerFormat, string oasisFormat, string trailerFormat, string clinician, string clinicianSpecialty)
        {
            if (batch.PROVIDER_ID.IsNullOrEmpty() && batch.ACTIVATION_CODE.IsNullOrEmpty())
            {
                batch.PROVIDER_ID = providerNo;
                batch.ACTIVATION_CODE = activationCode;
                batch.OASIS_DATA.OASIS_HEADER.HEADER_VALUE = headerFormat;
                batch.OASIS_DATA.OASIS_TRAILER.TRAILER_VALUE = trailerFormat;
                body.Add(new SHP_OASIS_DATAOASIS_DATAOASIS_BODY
                {
                    BODY_VALUE = oasisFormat,
                    CLINICIAN_NAME = clinician,
                    CLINICIAN_SPECIALTY = clinicianSpecialty,
                    TEAM_NAME = "",
                    CASE_MANAGER = "",
                    CUSTOMER_DEFINED = ""
                });
            }
            else 
            {
                if (batch.PROVIDER_ID.IsEqual(providerNo) && batch.ACTIVATION_CODE.IsEqual(activationCode))
                {
                    if (body.Count > 1000)
                    {
                        this.Flush();
                        this.Initialize();
                    }

                    body.Add(new SHP_OASIS_DATAOASIS_DATAOASIS_BODY
                    {
                        BODY_VALUE = oasisFormat,
                        CLINICIAN_NAME = clinician,
                        CLINICIAN_SPECIALTY = clinicianSpecialty,
                        TEAM_NAME = "",
                        CASE_MANAGER = "",
                        CUSTOMER_DEFINED = ""
                    });
                }
                else
                {
                    this.Flush();
                    this.Initialize();

                    batch.PROVIDER_ID = providerNo;
                    batch.ACTIVATION_CODE = activationCode;
                    batch.OASIS_DATA.OASIS_HEADER.HEADER_VALUE = headerFormat;
                    batch.OASIS_DATA.OASIS_TRAILER.TRAILER_VALUE = trailerFormat;
                    body.Add(new SHP_OASIS_DATAOASIS_DATAOASIS_BODY
                    {
                        BODY_VALUE = oasisFormat,
                        CLINICIAN_NAME = clinician,
                        CLINICIAN_SPECIALTY = clinicianSpecialty,
                        TEAM_NAME = "",
                        CASE_MANAGER = "",
                        CUSTOMER_DEFINED = ""
                    });
                }
            }
        }

        #endregion
    }
}
