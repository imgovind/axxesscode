﻿namespace Axxess.Scheduled.ShpData
{
    using System;
    using System.Net;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Enums;
    using System.IO;

    class Program
    {
        #region Private Members

        private static SHP_OASIS_DATA batch = null;
        private static List<SHP_OASIS_DATAOASIS_DATAOASIS_BODY> body;

        #endregion

        #region Main Methods

        static void Main(string[] args)
        {
            try
            {
                Bootstrapper.Run();
                LoopThroughAgencyData();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                var log = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("log_{0}.txt", DateTime.Now.ToString("yyyyMMdd-hhmmsstt")));
                using (TextWriter logWriter = new StreamWriter(log, false))
                {
                    logWriter.WriteLine(ex.ToString());
                }
            }
        }

        private static void InitializeBatch(string providerNo, string activationCode, string headerFormat, string trailerFormat)
        {
            body = new List<SHP_OASIS_DATAOASIS_DATAOASIS_BODY>();

            batch = new SHP_OASIS_DATA();
            batch.PROVIDER_ID = providerNo;
            batch.ACTIVATION_CODE = activationCode;

            batch.OASIS_DATA = new SHP_OASIS_DATAOASIS_DATA();
            batch.OASIS_DATA.OASIS_HEADER = new SHP_OASIS_DATAOASIS_DATAOASIS_HEADER();
            batch.OASIS_DATA.OASIS_HEADER.HEADER_VALUE = headerFormat;
            
            //batch.OASIS_DATA.OASIS_BODY = new SHP_OASIS_DATAOASIS_DATAOASIS_BODY[1000];

            batch.OASIS_DATA.OASIS_TRAILER = new SHP_OASIS_DATAOASIS_DATAOASIS_TRAILER();
            batch.OASIS_DATA.OASIS_TRAILER.TRAILER_VALUE = trailerFormat;
        }

        private static void AddToBatch(string oasisFormat, string clinician, string specialty)
        {
            body.Add(new SHP_OASIS_DATAOASIS_DATAOASIS_BODY
            {
                BODY_VALUE = oasisFormat,
                CLINICIAN_NAME = clinician,
                CLINICIAN_SPECIALTY = specialty,
                TEAM_NAME = "",
                CASE_MANAGER = "",
                CUSTOMER_DEFINED = ""
            });
        }

        private static void FinalizeBatch(Guid agencyId)
        {
            if (batch != null && body != null && body.Count > 0)
            {
                batch.BATCH = 1;
                batch.BATCH_KEY_TIME_STAMP = DateTime.Now;
                var shpDataAcceptor = new SHPDataAcceptor.SHPDataAcceptor();

                if (body.Count <= 1000)
                {
                    batch.TOTAL_BATCHES = 1;
                    batch.OASIS_DATA.OASIS_BODY = body.ToArray();

                    XmlNode xmlNode = shpDataAcceptor.AcceptSHPData(batch.ToXml());
                    var output = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("output_{0}_{1}.txt", agencyId, DateTime.Now.Ticks));
                    using (TextWriter logWriter = new StreamWriter(output, true))
                    {
                        logWriter.WriteLine(batch.ToXml());
                    }
                    if (xmlNode != null && xmlNode.OuterXml.IsNotNullOrEmpty())
                    {
                        var result = xmlNode.OuterXml.ToObject<SHP_DATA_RETURN>();
                        if (result != null)
                        {
                            Console.WriteLine("Return Number: {0}", result.RETURN_NUMBER);
                            result.RETURN_MESSAGE.ForEach(m =>
                            {
                                Console.WriteLine("Return Message: {0}", m.IsNotNullOrEmpty() ? m : string.Empty);
                            });
                        }
                    }
                }
                else
                {
                    int counter = 0;
                    var totalBatches = Math.Ceiling(body.Count / 1000d);
                    do
                    {
                        batch.BATCH = batch.BATCH + counter;
                        batch.TOTAL_BATCHES = (int)totalBatches;
                        batch.OASIS_DATA.OASIS_BODY = body.Skip(1000 * counter).Take(1000).ToArray();
                        counter++;

                        XmlNode xmlNode = shpDataAcceptor.AcceptSHPData(batch.ToXml());
                        if (xmlNode != null && xmlNode.OuterXml.IsNotNullOrEmpty())
                        {
                            var result = xmlNode.OuterXml.ToObject<SHP_DATA_RETURN>();
                            if (result != null)
                            {
                                Console.WriteLine("Return Number: {0}", result.RETURN_NUMBER);
                                result.RETURN_MESSAGE.ForEach(m =>
                                {
                                    Console.WriteLine("Return Message: {0}", m.IsNotNullOrEmpty() ? m : string.Empty);
                                });
                            }
                        }
                    } while (1000 * counter <= body.Count);
                }
            }
            else
            {
                Console.WriteLine("Batch or Body is null");
            }
        }

        private static void LoopThroughAgencyData()
        {
            var customers = Database.GetShpCustomers();
            if (customers != null && customers.Count > 0)
            {
                customers.ForEach(customer =>
                {
                    var agencyLocation = Database.GetAgencyLocation(customer.AgencyId);
                    var trailerFormat = OasisFooter(3);
                    var headerFormat = OasisHeader(agencyLocation);
                    var users = Database.GetAgencyUsers(customer.AgencyId);
                    var patientList = Database.GetPatients(customer.AgencyId);
                    
                    InitializeBatch(customer.ProviderNo, customer.ActivationCode, headerFormat, trailerFormat);

                    patientList.ForEach(patient =>
                    {
                        var patientAdmissions = Database.GetPatientAdmissions(customer.AgencyId, patient.Id);
                        if (patientAdmissions != null && patientAdmissions.Count > 0)
                        {
                            patientAdmissions.ForEach(patientAdmission =>
                            {
                                var admissionEpisodes = Database.GetPatientAdmissionEpisodes(customer.AgencyId, patient.Id, patientAdmission.Id);
                                if (admissionEpisodes != null && admissionEpisodes.Count > 0)
                                {
                                    admissionEpisodes.ForEach(admissionEpisode =>
                                    {
                                        if (admissionEpisode != null && admissionEpisode.Schedule.IsNotNullOrEmpty())
                                        {
                                            var schedule = admissionEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                            if (schedule != null && schedule.Count > 0)
                                            {
                                                var assessmentEvents = schedule.Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate()
                                                    && !s.IsDeprecated && !s.IsMissedVisit && s.IsAssessment()
                                                    && (s.Status.ToInteger() == (int) ScheduleStatus.OasisCompletedPendingReview
                                                    || s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedExportReady
                                                    || s.Status.ToInteger() == (int)ScheduleStatus.OasisExported
                                                    || s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedNotExported)).ToList();
                                                if (assessmentEvents != null && assessmentEvents.Count > 0)
                                                {
                                                    assessmentEvents.ForEach(assessmentEvent =>
                                                    {
                                                        var assessment = Database.GetAssessment(customer.AgencyId, patient.Id, assessmentEvent.EventId, assessmentEvent.GetAssessmentType());
                                                        if (assessment != null && assessment.SubmissionFormat.IsNotNullOrEmpty())
                                                        {
                                                            var batch = Database.GetShpDataBatch(assessmentEvent.EventId, customer.AgencyId, patient.Id);
                                                            if (batch != null)
                                                            {
                                                                if (assessment.Modified != batch.LastModified
                                                                    || assessment.Status != batch.Status)
                                                                {
                                                                    batch.LastModified = assessment.Modified;
                                                                    batch.Status = assessment.Status;
                                                                    if (Database.Update<ShpDataBatch>(batch))
                                                                    {
                                                                        AddToBatch(assessment.SubmissionFormat, GetClinicianName(users, assessmentEvent.UserId), GetClinicianSpecialty(users, assessmentEvent.UserId));
                                                                        Console.WriteLine("Changed Batch: {0}", batch.ToString());
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Console.WriteLine("Existing Batch: {0}", batch.ToString());
                                                                }
                                                            }
                                                            else
                                                            {
                                                                batch = new ShpDataBatch
                                                                {
                                                                    Id = assessmentEvent.EventId,
                                                                    AgencyId = customer.AgencyId,
                                                                    PatientId = patient.Id,
                                                                    Status = assessment.Status,
                                                                    LastModified = assessment.Modified,
                                                                    Created = DateTime.Now
                                                                };
                                                                if (Database.Add<ShpDataBatch>(batch))
                                                                {
                                                                    AddToBatch(assessment.SubmissionFormat, GetClinicianName(users, assessmentEvent.UserId), GetClinicianSpecialty(users, assessmentEvent.UserId));
                                                                    Console.WriteLine("New Batch: {0}", batch.ToString());
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                    });

                    // Send Web Service
                    FinalizeBatch(customer.AgencyId);
                });
            } 
        }

        private static string GetClinicianSpecialty(List<User> users, Guid userId)
        {
            var clinician = users.Find(u => u.Id == userId);

            if (clinician.Credentials.IsEqual("None"))
            {
                return string.Empty;
            }

            if (clinician.CredentialsOther.IsNotNullOrEmpty())
            {
                return clinician.CredentialsOther;
            }

            return clinician.Credentials;
        }

        private static string GetClinicianName(List<User> users, Guid userId)
        {
            var clinician = users.Find(u => u.Id == userId);

            return clinician != null ? string.Format("{0} {1}", clinician.FirstName, clinician.LastName) : string.Empty;
        }

        private static string OasisHeader(AgencyLocation agencyLocation)
        {
            var submissionGuide = Database.GetOasisHeaderInstructionsNew();
            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1448;
            if (submissionGuide != null && submissionGuide.Count > 0)
            {
                submissionFormat.Append("A1"); //REC_ID :2

                if (submissionGuide.ContainsKey("FED_ID")) //FED_ID :8
                {
                    if (agencyLocation != null && agencyLocation.MedicareProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicareProviderNumber.PadRight(6).Trim().PartOfString(0, 6).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(6));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(6));
                }

                submissionFormat.Append(string.Empty.PadRight(4)); //FILLER1 :12

                if (submissionGuide.ContainsKey("ST_ID")) //ST_ID :27
                {
                    if (agencyLocation.MedicaidProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicaidProviderNumber.Trim().PadRight(15).PartOfString(0, 15).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(15));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(15));
                }

                if (submissionGuide.ContainsKey("HHA_AGENCY_ID")) //HHA_AGENCY_ID :43
                {
                    if (agencyLocation.HomeHealthAgencyId.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.HomeHealthAgencyId.Trim().PadRight(16).PartOfString(0, 16).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(16));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(16));
                }

                if (submissionGuide.ContainsKey("ACY_NAME")) //ACY_NAME :73
                {
                    if (agencyLocation.Name.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.Name.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_1")) //ACY_ADDR_1 :103
                {
                    if (agencyLocation != null && agencyLocation.AddressLine1.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine1.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_2")) //ACY_ADDR_2 :133
                {
                    if (agencyLocation != null && agencyLocation.AddressLine2.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine2.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }


                if (submissionGuide.ContainsKey("ACY_CITY")) //ACY_CITY :153
                {
                    if (agencyLocation != null && agencyLocation.AddressCity.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressCity.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(20));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("ACY_ST")) //ACY_ST :155
                {
                    if (agencyLocation != null && agencyLocation.AddressStateCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressStateCode.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("ACY_ZIP")) //ACY_ZIP :166
                {
                    if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressZipCode.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(11));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("ACY_CNTCT")) //ACY_CNTCT :196
                {
                    submissionFormat.Append(string.Format("{0}, {1}", agencyLocation.ContactPersonLastName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonLastName : string.Empty, agencyLocation.ContactPersonFirstName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonFirstName : string.Empty).Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_PHONE")) //ACY_PHONE :206
                {
                    if (agencyLocation.ContactPersonPhone.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.ContactPersonPhone.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("ACY_EXTEN")) //ACY_EXTEN :211
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("AGT_ID")) //AGT_ID :220
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("AGT_NAME")) //AGT_NAME :250
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_1")) //AGT_ADDR_1 :280
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_2")) //AGT_ADDR_2 :310
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_CITY")) //AGT_CITY :330
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("AGT_ST")) //AGT_ST :332
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("AGT_ZIP")) //AGT_ZIP :343
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("AGT_CNTCT")) //AGT_CNTCT :373
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_PHONE")) //AGT_PHONE :383
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("AGT_EXTEN")) //AGT_EXTEN :388
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("SFW_ID") && submissionGuide["SFW_ID"] != null && submissionGuide["SFW_ID"].DefaultValue.IsNotNullOrEmpty())//SFW_ID :397
                {
                    submissionFormat.Append(submissionGuide["SFW_ID"].DefaultValue.Trim().Trim().PadRight(9).PartOfString(0, 9).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("SFW_NAME") && submissionGuide["SFW_NAME"] != null && submissionGuide["SFW_NAME"].DefaultValue.IsNotNullOrEmpty()) //SFW_NAME : 427
                {
                    submissionFormat.Append(submissionGuide["SFW_NAME"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_1") && submissionGuide["SFW_ADDR_1"] != null && submissionGuide["SFW_ADDR_1"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 : 457
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_1"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_2") && submissionGuide["SFW_ADDR_2"] != null && submissionGuide["SFW_ADDR_2"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 :487
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_2"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_CITY") && submissionGuide["SFW_CITY"] != null && submissionGuide["SFW_CITY"].DefaultValue.IsNotNullOrEmpty()) //SFW_CITY : 507
                {
                    submissionFormat.Append(submissionGuide["SFW_CITY"].DefaultValue.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("SFW_ST") && submissionGuide["SFW_ST"] != null && submissionGuide["SFW_ST"].DefaultValue.IsNotNullOrEmpty()) //SFW_ST : 509
                {
                    submissionFormat.Append(submissionGuide["SFW_ST"].DefaultValue.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("SFW_ZIP") && submissionGuide["SFW_ZIP"] != null && submissionGuide["SFW_ZIP"].DefaultValue.IsNotNullOrEmpty()) //SFW_ZIP : 520
                {
                    submissionFormat.Append(submissionGuide["SFW_ZIP"].DefaultValue.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }


                if (submissionGuide.ContainsKey("SFW_CNTCT") && submissionGuide["SFW_CNTCT"] != null && submissionGuide["SFW_CNTCT"].DefaultValue.IsNotNullOrEmpty()) //SFW_CNTCT : 550
                {
                    submissionFormat.Append(submissionGuide["SFW_CNTCT"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_PHONE") && submissionGuide["SFW_PHONE"] != null && submissionGuide["SFW_PHONE"].DefaultValue.IsNotNullOrEmpty()) //SFW_PHONE :560
                {
                    submissionFormat.Append(submissionGuide["SFW_PHONE"].DefaultValue.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("SFW_EXTEN")) //SFW_EXTEN :565
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("FILE_DT")) //FILE_DT :573
                {
                    submissionFormat.Append(DateTime.Now.ToString("yyyyMMdd").Trim().ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                }

                if (submissionGuide.ContainsKey("TEST_SW") && submissionGuide["TEST_SW"] != null && submissionGuide["TEST_SW"].DefaultValue.IsNotNullOrEmpty()) //TEST_SW :574
                {
                    submissionFormat.Append(submissionGuide["TEST_SW"].DefaultValue.PadRight(1).PartOfString(0, 1).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(1));
                }


                if (submissionGuide.ContainsKey("NATL_PROV_ID")) //NATL_PROV_ID :584
                {
                    if (agencyLocation.NationalProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.NationalProviderNumber.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("HDR_FL")) //HDR_FL : 1445
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }

                if (submissionGuide.ContainsKey("DATA_END")) //HDR_FL :1446
                {
                    submissionFormat.Append("%".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("%".PadRight(1));
                }

                if (submissionGuide.ContainsKey("CRG_RTN")) //CRG_RTN :1447
                {
                    submissionFormat.Append("\r".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\r".PadRight(1));
                }

                if (submissionGuide.ContainsKey("LN_FD")) //LN_FD :1448
                {
                    submissionFormat.Append("\n".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\n".PadRight(1));
                }

            }
            return submissionFormat.ToString();
        }

        private static string OasisFooter(int totalNumberOfRecord)
        {
            var footerString = new string(' ', 1446);
            var footerStringWithRecId = footerString.Remove(0, 2).Insert(0, "Z1");
            var footerStringWithTotalRec = footerStringWithRecId.Remove(2, 6).Insert(2, totalNumberOfRecord.ToString().PadLeft(6, '0'));
            var footerStringWithDataEnd = footerStringWithTotalRec.Remove(1445, 1).Insert(1445, "%");
            return footerStringWithDataEnd;
        }

        private static void AddShpDataLogin()
        {
            var shpLogin = new ShpDataLogin
            {
                Id = Guid.NewGuid(),
                AgencyId = "824e13c1-7cc6-43f1-aa5c-81190c45cb54".ToGuid(),
                Username = "aolowu@axxessconsult.com",
                PasswordHash = Crypto.Encrypt("@gencyC0re"),
                ProviderNo = "32860",
                ActivationCode = "805C",
                IsDeprecated = false,
                Created = DateTime.Now
            };

            if (Database.Add<ShpDataLogin>(shpLogin))
            {
                Console.WriteLine("Shp Data Login Added");
            }
        }

        #endregion

        #region Helper Functions

        #endregion
    }
}
