﻿namespace Axxess.Scheduled.ShpData
{
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using StructureMap;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
            });
        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Container.ResolveAll<IStartupTask>().ForEach(t => t.Execute());
        }
    }
}
