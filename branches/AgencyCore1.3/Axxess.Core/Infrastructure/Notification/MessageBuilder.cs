﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class MessageBuilder
    {
        #region Static Constructor

        private static readonly string templateDirectory;
        static MessageBuilder()
        {
            templateDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.MailTemplate);
        }

        #endregion

        #region Public Methods

        public static string PrepareTextFrom(string templateName, params string[] pairs)
        {
            string body = ReadTextFrom(templateName);

            for (var i = 0; i < pairs.Length; i += 2)
            {
                body = body.Replace("<%={0}%>".FormatWith(pairs[i]), pairs[i + 1]);
            }

            return body;
        }

        #endregion

        #region Private Methods

        private static string ReadTextFrom(string templateName)
        {
            string filePath = string.Concat(Path.Combine(templateDirectory, templateName), ".txt");
            return File.ReadAllText(filePath);
        }

        #endregion
    }
}
