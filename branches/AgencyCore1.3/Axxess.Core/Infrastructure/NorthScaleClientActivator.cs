﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Net;

    using NorthScale.Store;
    using NorthScale.Store.Configuration;

    public static class NorthScaleClientActivator
    {
        #region Members

        private static NorthScaleClient northScaleClient;

        public static NorthScaleClient Cache
        {
            get
            {
                if (northScaleClient == null)
                {
                    LoadConfig();
                }
                return northScaleClient;
            }
        }

        private static void LoadConfig()
        {
            var northScaleClientConfiguration = new NorthScaleClientConfiguration();

            northScaleClientConfiguration.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 2);
            northScaleClientConfiguration.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);

            if (AppSettings.IsProdServer)
            {
                northScaleClientConfiguration.Urls.Add(new Uri("http://10.0.5.41:8091/pools/default"));
                northScaleClientConfiguration.Urls.Add(new Uri("http://10.0.5.42:8091/pools/default"));
            }
            else
            {
                northScaleClientConfiguration.Urls.Add(new Uri("http://10.0.1.6:8091/pools/default"));

            }
            northScaleClientConfiguration.Credentials = new NetworkCredential("Administrator", "@gencyC0re");

            northScaleClient = new NorthScaleClient(northScaleClientConfiguration, "default");
        }

        #endregion
    }
}
