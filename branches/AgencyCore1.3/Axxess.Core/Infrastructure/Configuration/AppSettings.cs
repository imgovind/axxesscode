﻿namespace Axxess.Core.Infrastructure
{
    using System;

    using Axxess.Core.Extension;

    public static class AppSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static bool CompressHtmlResponse
        {
            get
            {
                return configuration.AppSettings["CompressHtmlResponse"].ToBoolean();
            }
        }


        public static bool UseSSL
        {
            get
            {
                return configuration.AppSettings["UseSSL"].ToBoolean();
            }
        }

        public static double CachingIntervalInMinutes
        {
            get
            {
                return configuration.AppSettings["CachingIntervalInMinutes"].ToDouble();
            }
        }

        public static double SessionTimeoutInMinutes
        {
            get
            {
                return configuration.AppSettings["SessionTimeoutInMinutes"].ToDouble();
            }
        }

        public static string AuthenticationType
        {
            get
            {
                return configuration.AppSettings["AuthenticationType"];
            }
        }

        public static string AuthenticationDomain
        {
            get
            {
                return configuration.AppSettings["AuthenticationDomain"];
            }
        }

        public static bool UsePersistentCookies
        {
            get
            {
                return configuration.AppSettings["UsePersistentCookies"].ToBoolean();
            }
        }

        public static bool IsProdServer
        {
            get
            {
                return configuration.AppSettings["IsProdServer"].ToBoolean();
            }
        }

        public static string NoReplyEmail
        {
            get
            {
                return configuration.AppSettings["NoReplyEmail"];
            }
        }

        public static string NoReplyDisplayName
        {
            get
            {
                return configuration.AppSettings["NoReplyDisplayName"];
            }
        }

        public static bool EnableSSLMail
        {
            get
            {
                return configuration.AppSettings["EnableSSLMail"].ToBoolean();
            }
        }

        public static string MailTemplate
        {
            get
            {
                return configuration.AppSettings["TemplateDirectory"];
            }
        }

        public static string NewsFeedUrl
        {
            get
            {
                return configuration.AppSettings["NewsFeedUrl"];
            }
        }

        public static string AllowedImageExtensions
        {
            get
            {
                return configuration.AppSettings["AllowedImageExtensions"];
            }
        }
        public static string ANSIGeneraterUrl
        {
            get
            {
                return configuration.AppSettings["ANSIGeneraterUrl"];
            }
        }
    }
}
