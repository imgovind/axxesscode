﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class Insurance
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}