﻿using System;
using System.ServiceProcess;
using System.ComponentModel;
using System.Configuration.Install;

namespace Axxess.Api.Services
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller grouperInstaller;
        private ServiceInstaller validationInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.validationInstaller = new ServiceInstaller();
            this.validationInstaller.StartType = ServiceStartMode.Automatic;
            this.validationInstaller.ServiceName = "ValidationService";
            this.validationInstaller.DisplayName = "Validation Service";
            this.validationInstaller.Description = "Provides OASIS Validation to the AgencyCore Application.";

            this.grouperInstaller = new ServiceInstaller();
            this.grouperInstaller.StartType = ServiceStartMode.Automatic;
            this.grouperInstaller.ServiceName = "GrouperService";
            this.grouperInstaller.DisplayName = "Grouper Service";
            this.grouperInstaller.Description = "Provides Grouper HIPPS Code to the AgencyCore Application.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.validationInstaller, this.grouperInstaller });
        }
    }
}
