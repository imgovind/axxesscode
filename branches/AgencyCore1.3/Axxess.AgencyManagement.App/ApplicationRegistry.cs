﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web.Mvc;

    using AutoMapper;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Services;
    using Security;
    using Logging;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Repositories;
    using Axxess.Membership.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public class ApplicationRegistry : Registry
    {
        public ApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });

            For<ILog>().Use<DatabaseLog>();
            For<IOasisCDataProvider>().Use<OasisCDataProvider>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            For<IAgencyManagementDataProvider>().Use<AgencyManagementDataProvider>();
            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<IAssessmentService>().Use<AssessmentService>();
            For<IMembershipService>().Use<MembershipService>();
            For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();
            For<BaseAgent<IGrouperService>>().Use<GrouperAgent>();
            For<BaseAgent<IValidationService>>().Use<ValidationAgent>();
        }
    }
}
