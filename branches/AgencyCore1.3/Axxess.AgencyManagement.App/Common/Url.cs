﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    
    using Enums;
    using Axxess.AgencyManagement.Extensions;

    public static class Url
    {
        public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string onclick = string.Empty;
                string reopenUrl = string.Empty;
                string printUrl = Print(scheduleEvent, usePrintIcon);
                string detailUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskDetails('{0}', '{1}', '{2}');\">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                string deleteUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}');\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.UserId);
                string reassignUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this), '{0}','{1}','{2}','{3}');\" class=\"reassign\">Reassign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.UserId);

                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                        onclick = string.Format("SOC.loadSoc('{0}','{1}','StartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        onclick = string.Format("ROC.loadRoc('{0}','{1}','ResumptionOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        onclick = string.Format("FollowUp.loadFollowUp('{0}','{1}','FollowUp');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        onclick = string.Format("Recertification.loadRecertification('{0}','{1}','Recertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        onclick = string.Format("TransferNotDischarge.loadTransferNotDischarge('{0}','{1}','TransferInPatientNotDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        onclick = string.Format("TransferForDischarge.loadTransferForDischarge('{0}','{1}','TransferInPatientDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        onclick = string.Format("DeathAtHome.loadDeathAtHome('{0}','{1}','DischargeFromAgencyDeath');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        onclick = string.Format("Discharge.loadDischarge('{0}','{1}','DischargeFromAgency');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        onclick = string.Format("Patient.EditCommunicationNote('{0}');", scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DischargeSummary:
                        onclick = string.Format("Schedule.loadDischargeSummary('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                        onclick = string.Format("Schedule.loadSnVisit('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        onclick = string.Format("Schedule.loadSixtyDaySummary('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.TransferSummary:
                        onclick = string.Format("Schedule.loadTransferSummary('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        onclick = string.Format("Schedule.loadLVNSVisit('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        onclick = string.Format("Schedule.loadHHASVisit('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideVisit:
                        onclick = string.Format("Schedule.loadHHAVisitNote('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        onclick = string.Format("Schedule.loadHHACarePlan('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                }

                if (Current.HasRight(Permissions.EditTaskDetails))
                {
                    scheduleEvent.ActionUrl = detailUrl;
                }

                if (Current.HasRight(Permissions.DeleteTasks))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + deleteUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = deleteUrl;
                    }
                }

                if (addReassignLink)
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + reassignUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = reassignUrl;
                    }
                }

                if (Current.HasRight(Permissions.PrintClinicalDocuments))
                {
                    scheduleEvent.PrintUrl = printUrl;
                }

                if (scheduleEvent.IsCompleted())
                {
                    scheduleEvent.ActionUrl = string.Empty;
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                }

                if (scheduleEvent.IsCompletelyFinished() 
                    && scheduleEvent.DisciplineTask != (int)DisciplineTasks.PhysicianOrder 
                    && scheduleEvent.DisciplineTask != (int)DisciplineTasks.HCFA485)
                {
                    if (Current.HasRight(Permissions.ReopenDocuments))
                    {
                        reopenUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReOpen('{0}','{1}','{2}');{3}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                        scheduleEvent.ActionUrl = reopenUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = string.Empty;
                    }
                }

                if (scheduleEvent.IsMissedVisit)
                {
                    scheduleEvent.ActionUrl = string.Empty;
                    scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                }
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status.ToString()
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon)
            {
                linkText = "<img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" />";
            }
            if (scheduleEvent.IsMissedVisit)
            {
                printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/MissedVisit/View/{0}/{1}');\">{2}</a>", scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
            }
            else
            {

                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/StartofCare/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/StartofCare/View/{0}/{1}/{2}', function() {{ SOC.loadSoc('{2}','{1}','StartOfCare'); acore.closeprintview(); }}, function () {{ Oasis.OasisStatusAction('{2}','{1}','{0}','StartOfCare','Approve','startofcare'); Agency.RebindCaseManagement(); }}, function () {{ Oasis.OasisStatusAction('{2}','{1}','{0}','StartOfCare','Return','startofcare'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/ResumptionOfCare/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/ResumptionOfCare/View/{0}/{1}/{2}',function(){{ ROC.loadRoc('{2}','{1}','ResumptionOfCare');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','ResumptionOfCare','Approve','resumptionofcare'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','ResumptionOfCare','Return','resumptionofcare'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Followup/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Followup/View/{0}/{1}/{2}',function(){{ FollowUp.loadFollowUp('{2}','{1}','FollowUp');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','FollowUp','Approve','followup'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','FollowUp','Return','followup'); Agency.RebindCaseManagement(); }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Recertification/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Recertification/View/{0}/{1}/{2}',function(){{ Recertification.loadRecertification('{2}','{1}','Recertification');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','Recertification','Approve','recertification'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','Recertification','Return','recertification'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferNotDischarge/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferNotDischarge/View/{0}/{1}/{2}',function(){{ TransferNotDischarge.loadTransferNotDischarge('{2}','{1}','TransferInPatientNotDischarged');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','TransferInPatientNotDischarged','Approve','transfernotdischarge'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','TransferInPatientNotDischarged','Return','transfernotdischarge'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferDischarge/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferDischarge/View/{0}/{1}/{2}',function(){{ TransferForDischarge.loadTransferForDischarge('{2}','{1}','TransferInPatientDischarged');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','TransferInPatientDischarged','Approve','transferfordischarge'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','TransferInPatientDischarged','Return','transferfordischarge'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Death/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Death/View/{0}/{1}/{2}',function(){{ DeathAtHome.loadDeathAtHome('{2}','{1}','DischargeFromAgencyDeath');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgencyDeath','Approve','deathathome'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgencyDeath','Return','deathathome'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Discharge/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Discharge/View/{0}/{1}/{2}',function(){{ Discharge.loadDischarge('{2}','{1}','DischargeFromAgency');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgency','Approve','discharge'); Agency.RebindCaseManagement(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgency','Return','discharge'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        break;
                    case DisciplineTasks.HCFA485:
                        printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/485/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        break;
                    case DisciplineTasks.DischargeSummary:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeSummary/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeSummary/View/{0}/{1}/{2}',function(){{ Schedule.loadDischargeSummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{ Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SNVisit/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SNVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadSnVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{ Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SixtyDaySummary/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SixtyDaySummary/View/{0}/{1}/{2}',function(){{ Schedule.loadSixtyDaySummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.TransferSummary:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferSummary/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferSummary/View/{0}/{1}/{2}',function(){{ Schedule.loadTransferSummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/LVNSupervisoryVisit/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/LVNSupervisoryVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadLVNSVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}');  }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAideSupervisoryVisit/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAideSupervisoryVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadHHASVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.HHAideVisit:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAVisitNote/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAVisitNote/View/{0}/{1}/{2}',function(){{ Schedule.loadHHAVisitNote('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        if (usePrintIcon)
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHACarePlan/View/{0}/{1}/{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        else
                        {
                            printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHACarePlan/View/{0}/{1}/{2}',function(){{ Schedule.loadHHACarePlan('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); Agency.RebindCaseManagement(); }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                        }
                        break;
                }
            }
            return printUrl;
        }
    }
}
