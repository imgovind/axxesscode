﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public static class VisitRateExtensions
    {
        public static IDictionary<string, ChargeRate> ToChargeRateDictionary(this AgencyInsurance agencyInsurance)
        {
            IDictionary<string, ChargeRate> questions = new Dictionary<string, ChargeRate>();
            if (agencyInsurance != null && agencyInsurance.Charge.IsNotNullOrEmpty())
            {
                var chargeRateQuestions = agencyInsurance.Charge.ToObject<List<ChargeRate>>();
                chargeRateQuestions.ForEach(n =>
                {
                    questions.Add(n.RateDiscipline, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, CostRate> ToCostRateDictionary(this AgencyLocation agencyLocation)
        {
            IDictionary<string, CostRate> questions = new Dictionary<string, CostRate>();
            if (agencyLocation != null && agencyLocation.Cost.IsNotNullOrEmpty())
            {
                var costRateQuestions = agencyLocation.Cost.ToObject<List<CostRate>>();
                costRateQuestions.ForEach(n =>
                {
                    questions.Add(n.RateDiscipline, n);
                });
            }
            return questions;
        }

    }
}
