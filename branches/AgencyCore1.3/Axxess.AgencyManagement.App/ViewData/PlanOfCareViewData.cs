﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using Axxess.OasisC.Domain;

    public class PlanofCareViewData
    {
        public PlanofCareViewData()
        {
            this.Agency = new PlanofCareDetails.Agency();
            this.Patient = new PlanofCareDetails.Patient();
            this.Questions = new PlanofCareDetails.Questions();
            this.Physician = new PlanofCareDetails.Physician();
            this.Signature = new PlanofCareDetails.Signatures();
        }

        public PlanofCareDetails.Agency Agency { get; set; }
        public PlanofCareDetails.Patient Patient { get; set; }
        public PlanofCareDetails.Questions Questions { get; set; }
        public PlanofCareDetails.Physician Physician { get; set; }
        public PlanofCareDetails.Signatures Signature { get; set; }
    }
}