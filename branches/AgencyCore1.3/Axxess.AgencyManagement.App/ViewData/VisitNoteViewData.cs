﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public class VisitNoteViewData
    {
        public Agency Agency { get; set; }
        public Patient Patient { get; set; }
        public IDictionary<string, NotesQuestion> Questions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EventDate { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EventId { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public bool IsWoundCareExist { get; set;}
        public bool IsSupplyExist { get; set; }
        public string SignatureText { get; set; }
        public string SignatureDate { get; set; }
    }
}
