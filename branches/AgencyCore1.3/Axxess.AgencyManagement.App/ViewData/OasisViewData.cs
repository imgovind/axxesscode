﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Web;
    using Axxess.OasisC.Domain;

    public class OasisViewData : JsonViewData
    {
        public Guid assessmentId
        {
            get;
            set;
        }
        public IAssessment Assessment
        {
            get;
            set;
        }
    }
}