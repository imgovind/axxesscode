﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

   public class MedicationProfileViewData
    {
       public MedicationProfile MedicationProfile { get; set; }
       public Patient Patient { get; set; }
       public Guid EpisodeId { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public IDictionary<string, Question> Questions { get; set; }
       public Guid PhysicianId { get; set; }
       public string PhysicianDisplayName { get; set; }
       public AssessmentType AssessmentType { get; set; }
       public string PharmacyName { get; set; }
       public string PharmacyPhone { get; set; }
       public Agency Agency { get; set; }
       public string SignatureName { get; set; }
       public DateTime SignatureDate { get; set; }
    } 
}
