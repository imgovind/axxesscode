﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Web;
    using Enums;
    using Domain;
    using ViewData;
    using Security;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IMembershipService membershipService;

        public UserController(IAgencyManagementDataProvider dataProvider, IUserService userService, IMembershipService membershipService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.userRepository = dataProvider.UserRepository;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult New()
        {
            return PartialView();
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Add([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();

            if (user.IsValid)
            {
                if (userService.IsEmailAddressUnique(user.EmailAddress))
                {
                    if (!userService.CreateUser(user))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the new User.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User was saved successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The e-mail address provided is already in use.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(userRepository.Get(Id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Update([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();

            if (user.IsValid)
            {
                if (userRepository.Update(user))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User was saved successfully";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the new User.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult AddLicense([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.AddLicense(license, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return PartialView("JsonResult", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult UpdateLicense(Guid Id, Guid userId, DateTime ExpirationDate)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.UpdateLicense(Id, userId, ExpirationDate))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
            }
            return View(new GridModel(new List<License>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult DeleteLicense(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (userService.DeleteLicense(Id, userId))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
            }
            return View(new GridModel(new List<License>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public JsonResult UpdatePermissions(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (userService.UpdatePermissions(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permissions updated successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Deactivate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            bool result = userRepository.Deactivate(Current.AgencyId, userId);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Delete(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Try Again." };
            if (membershipService.DeactivateLogin(userId) && userRepository.Delete(Current.AgencyId, userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Schedule()
        {
            ViewData["UserScheduleGroupName"] = "VisitDate";
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessCaseManagement)]
        public ActionResult ScheduleGrouped(string groupName)
        {
            ViewData["UserScheduleGroupName"] = groupName;
            return PartialView("ScheduleGrid", userService.GetSchedule(Current.UserId).OrderBy(m => m.VisitDate).ToList());
        }
        
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewLists)]
        public ActionResult ScheduleList()
        {
            return View(new GridModel(userService.GetSchedule(Current.UserId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewLists)]
        public JsonResult ScheduleWidget()
        {
            return Json(userService.GetSchedule(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessPersonalProfile)]
        public ActionResult Profile()
        {
            return PartialView("Profile/Edit", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPersonalProfile)]
        public ActionResult Profile([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = true };

            if (viewData.isSuccessful && userService.UpdateProfile(user))
            {
                viewData.isSuccessful = true;

                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }

                if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !userService.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The signature provided does not match the one on file.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your profile could not be saved.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewLists)]
        public JsonResult All()
        {
            return Json(userRepository.GetAgencyUsers(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Grid()
        {
            return PartialView("List");
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult List()
        {
            return View(new GridModel(userRepository.GetAgencyUsers(Current.AgencyId)));
        }

        [GridAction]
        [Demand(Permissions.ManageUsers)]
        public ActionResult LicenseList(Guid userId)
        {
            return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotSignature()
        {
            return PartialView("Signature/Forgot", Current.User.Name);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailSignature()
        {
            var viewData = new JsonViewData();

            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Forgot signature failed. Please try again later.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ResetSignature([Bind]SignatureChange signature)
        {
            Check.Argument.IsNotNull(signature, "signature");

            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(signature.NewSignature), "Temporary signature is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(signature.NewSignature), "New signature is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(signature.NewSignatureConfirm), "Confirm new signature is required.<br/>"),
                           new Validation(() => !signature.NewSignature.IsEqual(signature.NewSignatureConfirm), "The signatures you have entered do not match.")
                      );

            if (viewData.isSuccessful)
            {
                if (!userService.IsSignatureCorrect(Current.UserId, signature.CurrentSignature))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The temporary signature provided does not match the one on file.";
                }
                else
                {
                    if (membershipService.ChangeSignature(Current.LoginId, signature))
                    {
                        viewData.isSuccessful = true;
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Reset signature failed. Please try again later.";
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ChangeSignature()
        {
            return PartialView("Signature/Change");
        }

        #endregion

    }
}
