﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;

    using Web;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AssetController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssetRepository assetRepository;

        public AssetController(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assetRepository = agencyManagementDataProvider.AssetRepository;
        }

        #endregion

        #region AssetController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Serve(Guid assetId)
        {
            var asset = assetRepository.Get(assetId, Current.AgencyId);
            return File(asset.Bytes, asset.ContentType, asset.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assetRepository.Delete(assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }
        
        #endregion
    }
}
