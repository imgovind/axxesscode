﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.App.Web;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.AgencyManagement.App.ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    using Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PayrollController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IPayrollService payrollService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;

        public PayrollController(IAgencyManagementDataProvider agencyManagementDataProvider, IPayrollService payrollService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(payrollService, "payrollService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.payrollService = payrollService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region PayrollController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePayroll)]
        public ActionResult Search()
        {
            DateTime agencyCreated = DateTime.Today;

            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                agencyCreated = agency.Created;
            }

            return View(agencyCreated);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePayroll)]
        public ActionResult Search(DateTime startDate, DateTime endDate)
        {
            return View("Results", payrollService.GetSummary(startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePayroll)]
        public ActionResult Detail(Guid userId, DateTime startDate, DateTime endDate)
        {
            var detail = new PayrollDetail();
            if (!userId.IsEmpty())
            {
                detail.Name = userRepository.Get(userId, Current.AgencyId).DisplayName;
                detail.Visits = userService.GetSchedule(userId, startDate, endDate);
            }
            return View(detail);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePayroll)]
        public ActionResult Details(DateTime startDate, DateTime endDate)
        {
            return View(payrollService.GetDetails(startDate, endDate));
        }

        #endregion

    }
}
