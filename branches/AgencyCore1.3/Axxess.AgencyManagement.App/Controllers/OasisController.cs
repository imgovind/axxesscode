﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using System.Text;

    using AutoMapper;

    using Web;
    using Enums;
    using Services;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.LookUp.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Constructor / Member

        private readonly IUserService userService;
        private readonly IDateService dateService;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientService patientService;
        private readonly IPatientRepository patientRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly ICachedDataRepository cachedDataRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;

        public OasisController(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IAssessmentService assessmentService, IPatientService patientService, IUserService userService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.cachedDataRepository = oasisDataProvider.CachedDataRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region OasisController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Assessment(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new OasisViewData();
            Assessment oasisAssessment = assessmentService.SaveAssessment(formCollection, Request.Files);
            if (oasisAssessment.ValidationError.IsNullOrEmpty())
            {
                viewData.assessmentId = oasisAssessment.Id;
                viewData.Assessment = oasisAssessment;
                viewData.isSuccessful = true;
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public JsonResult Get(Guid Id, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return Json(assessmentService.GetAssessment(Id, assessmentType).ToDictionary());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Soc(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");

            Assessment assessment = assessmentService.GetAssessment(Id, AssessmentType);
            if (assessment != null)
            {
                var medicationProfile = patientRepository.GetLatestMedicationProfileHistory(assessment.PatientId, Current.AgencyId, false);
                if (medicationProfile != null)
                {
                    assessment.MedicationProfile = medicationProfile.ToXml();
                }
            }
            return PartialView("StartofCare", assessment);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult SocBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("StartofCare/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult SocPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var assessment = oasisAssessmentRepository.Get(eventId, patientId, episodeId, "StartOfCare", Current.AgencyId);
            if (assessment != null)
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    assessment.AgencyData = agency.ToXml();
                }
            }
            return View("StartofCare/Print", assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult SocCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");

            Assessment assessment = assessmentService.GetAssessment(Id, AssessmentType);

            if (assessment != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    assessment.MedicationProfile = medicationProfile.ToXml();
                }
            }
            return PartialView("StartOfCare/" + ContentIdentifier(Category), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "mooCode")]
        public JsonResult Guide(string mooCode)
        {
            if (mooCode.IsNullOrEmpty())
            {
                return Json(new OasisGuide());
            }
            return Json(cachedDataRepository.GetOasisGuide(mooCode));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult View485Medication(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var medProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
                if (medProfile != null)
                {
                    return View("485/Medication", medProfile);
                }
            }
            return View("485/Medication", new MedicationProfile());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult View485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null && planofCare.Data.IsNotNullOrEmpty())
            {
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                return View("485/Print", planofCare);
            }
            return View("485/Print", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult Edit485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                if (planofCare.Data.IsNotNullOrEmpty())
                {
                    planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                }
                return View("485/Edit", planofCare);
            }
            return View("485/Edit", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public JsonResult Save485(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved" };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty())
            {
                int statusId = int.Parse(status);

                if (statusId == (int) ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCare(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderToBeSentToPhysician)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || signatureDate.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this Plan of Care.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCare(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
                else
                {
                    // do nothing
                }
            }           
           
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Validate(Guid Id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return View("~/Views/Oasis/Validation.aspx", assessmentService.Validate(Id, patientId, episodeId, assessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Recertification(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");

            Assessment assessment = assessmentService.GetAssessment(Id, AssessmentType);
            if (assessment != null)
            {
                var medicationProfile = patientRepository.GetLatestMedicationProfileHistory(assessment.PatientId, Current.AgencyId, false);
                if (medicationProfile != null)
                {
                    assessment.MedicationProfile = medicationProfile.ToXml();
                }
            }

            return PartialView("Recertification", assessment);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult RecertBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("Recertification/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult RecertPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var assessment = oasisAssessmentRepository.Get(eventId, patientId, episodeId, "Recertification", Current.AgencyId);
            if (assessment != null)
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    assessment.AgencyData = agency.ToXml();
                }
            }
            return View("Recertification/Print", assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult RecertificationCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");

            Assessment assessment = assessmentService.GetAssessment(Id, AssessmentType);
            if (assessment != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    assessment.MedicationProfile = medicationProfile.ToXml();
                }
            }

            return PartialView("Recertification/" + ContentIdentifier(Category), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Roc(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("ResumptionofCare", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult RocBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("ResumptionOfCare/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult RocPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("ResumptionOfCare/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "ResumptionOfCare", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult RocCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("ResumptionOfCare/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult FollowUp(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("FollowUp", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult FollowUpBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("Followup/Print", viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult FollowUpPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("Followup/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "FollowUp", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult FollowUpCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Followup/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Death(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Death", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DeathBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("Death/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DeathPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("Death/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "DischargeFromAgencyDeath", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DeathCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Death/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Discharge(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Discharge", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DischargeBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("Discharge/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("Discharge/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "DischargeFromAgency", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DischargeCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Discharge/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult TransferForDischarge(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("TransferDischarge", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferDischargeBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("TransferDischarge/Print", viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("TransferDischarge/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "TransferInPatientDischarged", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult TransferForDischargeCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("TransferDischarge/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult TransferNotDischarge(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("TransferNotDischarge", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferNotDischargeBlank()
        {
            var viewData = new StartOfCareAssessment();
            viewData.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            return View("TransferNotDischarge/Print", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferNotDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            return View("TransferNotDischarge/Print", oasisAssessmentRepository.Get(eventId, patientId, episodeId, "TransferInPatientNotDischarged", Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult TransferNotDischargeCategory(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("TransferNotDischarge/" + ContentIdentifier(Category), assessmentService.GetAssessment(Id, AssessmentType));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult Medication(Guid AssessmentId, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(AssessmentId, "AssessmentId");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            var assessment = assessmentService.GetAssessment(AssessmentId, AssessmentType);
            if (assessment != null && assessment.MedicationProfile != null)
            {
                var list = assessment.MedicationProfile.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.Discharge('" + assessment.Id + "','" + assessment.PatientId + "','" + assessment.Type + "','" + l.Id + "');";
                });
                return View(new GridModel(list.OrderBy(l => l.MedicationCategory).ThenByDescending(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult InsertMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType)
        {
            var assessment = assessmentService.InsertMedication(AssessmentId, PatientId, AssessmentType, medication, MedicationType);
            if (assessment != null && assessment.MedicationProfile != null)
            {
                var list = assessment.MedicationProfile.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.Discharge('" + assessment.Id + "','" + assessment.PatientId + "','" + assessment.Type + "','" + l.Id + "');";
                });
                return View(new GridModel(list.OrderBy(l => l.MedicationCategory).ThenByDescending(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult UpdateMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType)
        {
            assessmentService.UpdateMedication(AssessmentId, PatientId, AssessmentType, medication, MedicationType);
            var assessment = assessmentService.GetAssessment(AssessmentId, AssessmentType);
            if (assessment != null && assessment.MedicationProfile != null)
            {
                var list = assessment.MedicationProfile.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.Discharge('" + assessment.Id + "','" + assessment.PatientId + "','" + assessment.Type + "','" + l.Id + "');";
                });
                return View(new GridModel(list.OrderBy(l => l.MedicationCategory).ThenByDescending(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public JsonResult UpdateMedicationForDischarge(Guid AssessmentId, Guid PatientId, string AssessmentType, DateTime DischargeDate, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your discharge medication is not changed." };
            if (assessmentService.UpdateMedicationForDischarge(AssessmentId, PatientId, AssessmentType, Id, DischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your discharge medication is changed.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult DeleteMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication)
        {
            assessmentService.DeleteMedication(AssessmentId, PatientId, AssessmentType, medication);
            var assessment = assessmentService.GetAssessment(AssessmentId, AssessmentType);
            if (assessment != null && assessment.MedicationProfile != null)
            {
                var list = assessment.MedicationProfile.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.Discharge('" + assessment.Id + "','" + assessment.PatientId + "','" + assessment.Type + "','" + l.Id + "');";
                });
                return View(new GridModel(list.OrderBy(l => l.MedicationCategory).ThenByDescending(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public JsonResult Submit(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string actionType , string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Assessment is not submitted." };
            if (actionType == "Submit")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedPendingReview).ToString(), string.Empty))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment is submitted.";
                }
            }
            else if (actionType == "Approve")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), string.Empty))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment is Approved.";
                }
            }
            else if (actionType == "Return")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(), reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment is Returned.";
                }
            }
            else if (actionType == "Exported")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisExported).ToString(), string.Empty))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment is exported.";
                }
            }
            else if (actionType == "ReOpen")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReopened).ToString(), reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment is reopened.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult MarkExported(List<string> OasisSelected)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data not marked as exported." };
            if (assessmentService.MarkAsExported(OasisSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data successfully marked as exported.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult ExportView()
        {
            return PartialView("OasisExport");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.CreateOasisSubmitFile)]
        public ActionResult Export()
        {
            var data = oasisAssessmentRepository.GetAllByStatus(Current.AgencyId, ((int)ScheduleStatus.OasisCompletedExportReady));
            data.ForEach(a =>
            {
                var patient = patientService.GetProfile(a.PatientId);
                if (patient != null)
                {
                    a.PatientName = patient.Patient.DisplayName;
                    a.Insurance = patient.Patient.PrimaryInsuranceName;
                }
            });
            return View(new GridModel(data));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult ExportedView()
        {
            return PartialView("Exported");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult Exported()
        {
            var data = oasisAssessmentRepository.GetAllByStatus(Current.AgencyId, ((int)ScheduleStatus.OasisExported));
            data.ForEach(a =>
            {
                var patient = patientService.GetProfile(a.PatientId);
                if (patient != null)
                {
                    a.PatientName = patient.Patient.DisplayName;
                    a.Insurance = patient.Patient.PrimaryInsuranceName;
                }
            });
            return View(new GridModel(data));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.CreateOasisSubmitFile)]
        public FileStreamResult Generate(List<string> OasisSelected)
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null) { 
                agencyRepository.GetMainLocation(Current.AgencyId); }
            string generateOasisHeadre = assessmentService.OasisHeader(agencyRepository.Get(Current.AgencyId));
            var generateJsonOasis = string.Empty;
            int count = 0;
            var hl = generateOasisHeadre.Length;
            OasisSelected.ForEach(o =>
            {
                string[] data = o.Split('|');
                var assessment = oasisAssessmentRepository.Get(data[0].ToGuid(), data[1], Current.AgencyId);
                if (assessment != null && assessment.SubmissionFormat != null)
                {
                    generateJsonOasis += assessment.SubmissionFormat + "\r\n";
                    count++;
                }
            });
            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(count + 2);
            var fl = generateOasisFooter.Length;
            UTF8Encoding encoding = new UTF8Encoding();
            string allString = generateOasisHeadre + generateJsonOasis + generateOasisFooter;
            byte[] buffer = encoding.GetBytes(allString);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, allString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis{0}.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assessmentService.DeleteWoundCareAsset(episodeId, patientId, eventId, assessmentType, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult Supply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity, string date)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNegativeOrZero(supplyId, "supplyId");
            Check.Argument.IsNotNull(quantity, "quantity");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The supply could not added. Please try again!" };
            if (assessmentService.AddSupply(episodeId, patientId, eventId, assessmentType, supplyId, quantity, date))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Supply Successfully Added.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.UpdateSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.DeleteSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult BlankMasterCalendar(Guid episodeId, Guid patientId)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            var modelData = new EpisodeDateViewData();
            if (episode != null)
            {
                modelData.EndDate = episode.EndDate;
                modelData.StartDate = episode.StartDate;
            }
            return PartialView("BlankMasterCalendar", modelData);
        }

        #endregion

        #region Private Methods

        private string ContentIdentifier(string content)
        {
            string result = "";
            switch (content)
            {
                case "#demographics_soc":
                case "#clinicalRecord_recertification":
                case "#clinicalRecord_roc":
                case "#clinicalRecord_death":
                case "#clinicalRecord_followUp":
                case "#clinicalRecord_discharge":
                case "#clinicalRecord_transfer":
                case "#clinicalRecord_nottransfer":
                    result = "Demographics";
                    break;
                case "#patienthistory_soc":
                case "#patienthistory_recertification":
                case "#patienthistory_roc":
                case "#patienthistory_followUp":
                    result = "PatientHistory";
                    break;
                case "#riskassessment_soc":
                case "#riskassessment_recertification":
                case "#riskassessment_roc":
                case "#riskassessment_discharge":
                case "#riskassessment_transfer":
                case "#riskassessment_nottransfer":
                    result = "RiskAssessment";
                    break;
                case "#prognosis_soc":
                case "#prognosis_recertification":
                case "#prognosis_roc":
                    result = "Prognosis";
                    break;
                case "#supportiveassistance_soc":
                case "#supportiveassistance_recertification":
                case "#supportiveassistance_roc":
                    result = "SupportiveAssistance";
                    break;
                case "#sensorystatus_soc":
                case "#sensorystatus_recertification":
                case "#sensorystatus_roc":
                case "#sensorystatus_followUp":
                case "#sensorystatus_discharge":
                    result = "SensoryStatus";
                    break;
                case "#pain_soc":
                case "#pain_recertification":
                case "#pain_roc":
                case "#pain_followUp":
                case "#pain_discharge":
                    result = "Pain";
                    break;
                case "#integumentarystatus_soc":
                case "#integumentarystatus_recertification":
                case "#integumentarystatus_roc":
                case "#integumentarystatus_followUp":
                case "#integumentarystatus_discharge":
                    result = "IntegumentaryStatus";
                    break;
                case "#respiratorystatus_soc":
                case "#respiratorystatus_recertification":
                case "#respiratorystatus_roc":
                case "#respiratorystatus_followUp":
                case "#respiratorystatus_discharge":
                    result = "RespiratoryStatus";
                    break;
                case "#endocrine_soc":
                case "#endocrine_recertification":
                case "#endocrine_roc":
                    result = "EndocrineStatus";
                    break;
                case "#cardiacstatus_soc":
                case "#cardiacstatus_recertification":
                case "#cardiacstatus_roc":
                case "#cardiacstatus_discharge":
                case "#cardiacstatus_transfer":
                case "#cardiacstatus_nottransfer":
                    result = "CardiacStatus";
                    break;
                case "#eliminationstatus_soc":
                case "#eliminationstatus_recertification":
                case "#eliminationstatus_roc":
                case "#eliminationstatus_followUp":
                case "#eliminationstatus_discharge":
                    result = "EliminationStatus";
                    break;
                case "#nutrition_soc":
                case "#nutrition_recertification":
                case "#nutrition_roc":
                    result = "Nutrition";
                    break;
                case "#behaviourialstatus_soc":
                case "#behaviourialstatus_recertification":
                case "#behaviourialstatus_roc":
                case "#behaviourialstatus_discharge":
                    result = "BehaviourStatus";
                    break;
                case "#adl_soc":
                case "#adl_recertification":
                case "#adl_roc":
                case "#adl_followUp":
                case "#adl_discharge":
                    result = "Adl";
                    break;
                case "#suppliesworksheet_soc":
                case "#suppliesworksheet_recertification":
                case "#suppliesworksheet_roc":
                    result = "SuppliesWorkSheet";
                    break;
                case "#medications_soc":
                case "#medications_recertification":
                case "#medications_roc":
                case "#medications_followUp":
                case "#medications_discharge":
                case "#medications_transfer":
                case "#medications_nottransfer":
                    result = "Medication";
                    break;
                case "#caremanagement_soc":
                case "#caremanagement_roc":
                case "#caremanagement_discharge":
                    result = "CareManagement";
                    break;
                case "#therapyneed_soc":
                case "#therapyneed_recertification":
                case "#therapyneed_roc":
                case "#therapyneed_followUp":
                    result = "TherapyNeed";
                    break;
                case "#ordersdisciplinetreatment_soc":
                case "#ordersdisciplinetreatment_recertification":
                case "#ordersdisciplinetreatment_roc":
                    result = "OrdersDiscipline";
                    break;
                case "#emergentcare_discharge":
                case "#emergentcare_transfer":
                case "#emergentcare_nottransfer":
                    result = "EmergentCare";
                    break;
                case "#deathAdd_death":
                    result = "DeathData";
                    break;
                case "#dischargeAdd_discharge":
                case "#dischargeAdd_transfer":
                case "#dischargeAdd_nottransfer":
                    result = "DischargeData";
                    break;
            }
            return result;
        }

        #endregion

    }
}
