﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Web;
    using Enums;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
  
    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReferralController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReferralService referralService;
        private readonly IReferralRepository referralRepository;

        public ReferralController(IAgencyManagementDataProvider agencyManagementDataProvider, IReferralService referralService)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.referralService = referralService;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
        }

        #endregion

        #region ReferralController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageReferrals)]
        public ActionResult Get(Guid id)
        {
            return Json(referralRepository.Get( Current.AgencyId,id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult Grid()
        {
            return View(new GridModel(referralService.GetPending(Current.AgencyId)));
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManageReferrals)]
        public ActionResult New()
        {
            return PartialView();
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageReferrals)]
        public JsonResult Add([Bind]Referral referral)
        {
            Check.Argument.IsNotNull(referral, "referral");

            var viewData = new JsonViewData();

            if (referral.IsValid)
            {
                referral.AgencyId = Current.AgencyId;
                if (!referralRepository.Add(referral))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the referral.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Referral was created successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = referral.ValidationMessage;
            }

            return Json(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageReferrals)]
        public ActionResult Edit(Guid id)
        {
            return PartialView("Edit", referralRepository.Get(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageReferrals)]
        public ActionResult Update([Bind] Referral referral)
        {
            Check.Argument.IsNotNull(referral, "referral");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(referral.FirstName), ". Patient first name is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.LastName), ". Patient last name is required. <br>"),
                           new Validation(() => string.IsNullOrEmpty(referral.DOB.ToString()), ". Patient date of birth is required.<br/>"),
                           new Validation(() => !referral.DOB.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.Gender), ". Patient gender has to be selected.<br/>"),
                           new Validation(() => (referral.EmailAddress == null ? !string.IsNullOrEmpty(referral.EmailAddress) : !referral.EmailAddress.IsEmail()), ". Patient e-mail is not in a valid  format.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressLine1), ". Patient address line is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressCity), ". Patient city is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressStateCode), ". Patient state is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressZipCode), ". Patient zip is required.<br/>")
                      );

            if (viewData.isSuccessful)
            {
                referral.AgencyId = Current.AgencyId;
                bool result = referralRepository.Edit(referral);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your referral has been updated.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Could not update the referral. Please try again.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageReferrals)]
        public ActionResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this Referral. Please try again." };
            bool result = referralRepository.Delete(Current.AgencyId, id);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Referral has been deleted.";
            }
            return Json(viewData);
        }

        #endregion
    }
}
