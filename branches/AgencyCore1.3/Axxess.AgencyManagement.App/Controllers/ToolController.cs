﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;

    using Web;
    using Axxess.Core.Infrastructure;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ToolController : BaseController
    {
        public ToolController()
        {

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Smoke()
        {
            return PartialView("Data", Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Ping()
        {
            return PartialView("Ping");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Who()
        {
            var who = string.Format("{0} ({1})", Environment.MachineName, Request.ServerVariables["LOCAL_ADDR"]); 
            return PartialView("Data", who);
        }
    }
}
