﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    
    using Web;
    using Enums;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Services;
    
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PhysicianController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        public PhysicianController(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, IPhysicianService physicianService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region PhysicianController Actions
        
        
        
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public JsonResult CheckPecos(string npi)
        {
            var viewData = new JsonViewData();
            if (npi.IsNotNullOrEmpty())
            {
                if (!lookupRepository.VerifyPecos(npi))
                {
                    viewData.isSuccessful = false;
                }
                else
                {
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public ActionResult SetPrimary(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "PatientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your physicain is not successfully set primary." };

            bool result = physicianRepository.SetPrimary(patientId, id);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your physicain is successfully set primary.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your physicain is not successfully set primary.";
            }
            return Json(viewData);
        }

        #endregion

    }
}
