﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Web;
    using Enums;
    using Services;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.OasisC.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;

        public BillingController(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, IPatientService patientService, IBillingService billingService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.referrralRepository = dataProvider.ReferralRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.patientService = patientService;
            this.billingService = billingService;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Center()
        {
            return PartialView("Center", billingService.AllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult FinalGrid()
        {
            return PartialView("FinalGrid", billingService.AllUnProcessedFinal());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult RapGrid()
        {
            return PartialView("RapGrid", billingService.AllUnProcessedRap());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Rap(Guid episodeId, Guid patientId)
        {
            return PartialView("Rap", billingService.GetRap(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Final(Guid episodeId, Guid patientId)
        {
            if (episodeId.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Final", new Final());
            }
            return PartialView("Final", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Info(Guid episodeId, Guid patientId)
        {
            return PartialView("Info", billingService.GetFinalInfo(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Visit(Guid episodeId, Guid patientId)
        {
            var claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            if (claim != null)
            {
                var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                claim.Visits = episode != null ? episode.Schedule : "";
                claim.BillInformations = billingService.GetInsuranceUnit(0);
            }
            return PartialView("Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Supply(Guid episodeId, Guid patientId)
        {
            Final claim = null;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
                if (claim != null && claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                    var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                    visits.ForEach(v =>
                    {

                        var episodeSupply = billingService.GetSupply(v);
                        episodeSupply.ForEach(s => {
                            if(!supplyList.Exists(l=>l.UniqueIdentifier==s.UniqueIdentifier)){
                                supplyList.Add(s);
                            }
                        });
                    });
                    claim.Supply = supplyList.ToXml();
                   
                }
            }
            return PartialView("Supply", claim );
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Summary(Guid episodeId, Guid patientId)
        {
            var claim = billingService.GetFinalInfo(patientId, episodeId);
            if (claim != null)
            {
                claim.BillInformations = billingService.GetInsuranceUnit(0);
            }
            return PartialView("Summary", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public JsonResult RapVerify(Rap claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Rap is not verified." };
            if (billingRepository.VerifyRap(claim, Current.AgencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Rap is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult ClaimSummary(List<Guid> RapSelected, List<Guid> FinalSelected)
        {
            var viewData = new Bill { Finals = billingRepository.Finals(FinalSelected, Current.AgencyId), Raps = billingRepository.Raps(RapSelected, Current.AgencyId) };
            return PartialView("ClaimSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Generate(List<Guid> RapSelected, List<Guid> FinalSelected)
        {
            string generateJsonClaim = billingService.Generate(RapSelected, FinalSelected);
            
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(generateJsonClaim);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, generateJsonClaim.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=billing{0}.txt", DateTime.Now.ToString("yyyyMMddHH:mm:ss:ff")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult UpdateStatus(List<Guid> RapSelected, List<Guid> FinalSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Status Update is not Successful." };
            bool rapStatus =billingService.UpdateRapStatus(RapSelected, Current.AgencyId, StatusType);
            bool finalStatus = billingService.UpdateFinalStatus(FinalSelected, Current.AgencyId, StatusType);
            if (rapStatus && finalStatus)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Status Update is Successfull.";
            }
            else if (rapStatus && !finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your Final Status Update is not Successfull.";
            }

            else if (!rapStatus && finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your rap Status Update is not Successfull.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult FinalComplete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final complete is not Successful." };
            if (!id.IsEmpty())
            {
                if (billingService.FinalComplete(id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Final Successfully Complete.";
                }
            }
            return Json(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult History()
        {
            var viewData = new PatientCenterViewData();
            viewData.Patients = patientRepository.GetAllByAgencyId(Current.AgencyId).ToList().ForSelection();
            if (viewData.Patients != null && viewData.Patients.Count > 0)
            {
                viewData.ScheduleEvents = patientRepository.GetScheduledEvents(Current.AgencyId, viewData.Patients.FirstOrDefault().Id);
            }
            return PartialView("History", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult PendingClaims()
        {
            return PartialView("PendingClaims", billingService.PendingClaims());
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult HistoryActivity(Guid patientId)
        {
            return View(new GridModel(billingService.Activity(patientId)));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult InfoVerify(Final claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final Basic Info is  not verified." };
            if (billingRepository.VerifyInfo(claim, Current.AgencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Basic Info is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final is not verified." };
            if (billingService.VisitVerify(Id, episodeId, patientId, Visit))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Visit is successfully verified.";
            }
            return Json(viewData);
        }


         [AcceptVerbs(HttpVerbs.Post)]
         public ActionResult SupplyVerify(FormCollection  formCollection, List<Guid> SupplyId)
         {
               var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Rap is not verified." };
                 var keys= formCollection.AllKeys;
                 if (keys != null)
                 {
                     var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                     var episodeId = keys.Contains("episodeId") && formCollection["episodeId"].IsNotNullOrEmpty() ? formCollection["episodeId"].ToGuid() : Guid.Empty;
                     var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                     if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                     {
                         if (billingService.VisitSupply(Id, episodeId, patientId, formCollection, SupplyId))
                         {
                             viewData.isSuccessful = true;
                             viewData.errorMessage = "Final supply is successfully verified.";
                         }
                     }
                 }
             return Json(viewData);
         }
       
        #endregion
    }
}
