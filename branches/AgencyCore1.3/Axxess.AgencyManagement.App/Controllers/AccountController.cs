﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Web.Mvc;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Web;
    using Enums;
    using Domain;
    using Security;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using MvcReCaptcha;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : BaseController
    {
        #region Constructor

        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsAuthentication, IMembershipService membershipService)
        {
            this.MembershipService = membershipService;
            this.AuthenticationService = formsAuthentication;
        }

        #endregion

        #region Properties

        public IMembershipService MembershipService
        {
            get;
            private set;
        }

        public IFormsAuthenticationService AuthenticationService
        {
            get;
            private set;
        }

        #endregion

        #region AccountController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public JsonResult LogOn([Bind] Logon logon)
        {
            var viewData = Validate<JsonViewData>(
                             new Validation(() => string.IsNullOrEmpty(logon.UserName.Trim()), "You must specify a username. <br/>"),
                             new Validation(() => string.IsNullOrEmpty(logon.Password.Trim()), "You must specify a password.  <br/>"),
                             new Validation(() => !logon.UserName.Trim().IsEmail(), "E-mail Address is not valid.<br/>")
                        );

            if (viewData.isSuccessful)
            {
                LoginAttempt loginAttempt = MembershipService.Validate(logon.UserName.Trim(), logon.Password.Trim());
                switch (loginAttempt)
                {
                    case LoginAttempt.Success:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        viewData.redirectUrl = "/";
                        if (Current.IsAxxessAdmin)
                        {
                            viewData.redirectUrl = "/Admin";
                        }
                        break;
                    case LoginAttempt.ChangePasswordRequired:
                    case LoginAttempt.ChangeSignatureRequired:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        viewData.redirectUrl = "/";
                        if (Current.IsAxxessAdmin)
                        {
                            viewData.redirectUrl = "/Admin";
                        }
                        break;
                    case LoginAttempt.Locked:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "This user has been locked. Please contact your Agency/Company's Administrator.";
                        break;
                    case LoginAttempt.Deactivated:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "This user is deactivated. Please contact your Agency/Company's Administrator.";
                        break;
                    case LoginAttempt.TrialPeriodOver:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Company's trial period is over. Please contact your Agency/Company's Administrator.";
                        break;
                    case LoginAttempt.AccountSuspended:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "You do not have access to your account at this time. Please contact your Agency/Company's Administrator.";
                        break;
                    default:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username / Password combination failed.";
                        break;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(Current.User.Name);
                this.AuthenticationService.SignOut();
            }

            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public ActionResult ForgotPassword()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(Current.User.Name);
                this.AuthenticationService.SignOut();
            }
            return View();
        }

        [CaptchaValidator]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ForgotPassword(string UserName, bool captchaValid)
        {
            var viewData = Validate<JsonViewData>(
                            new Validation(() => string.IsNullOrEmpty(UserName.Trim()), "You must specify your E-mail Address. <br/>"),
                            new Validation(() => !(captchaValid), "The text you entered didn't match the security check. Please try again. <br/>")
                       );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.ResetPassword(UserName.Trim()))
                {
                    viewData.errorMessage = "Your password has been reset. A link to reset your password has been sent to your e-mail address.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Reset password failed. Please try again later.";
                }
            }

            return Json(viewData);
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "Exceptions result in password not being changed.")]
        public JsonResult ChangePassword([Bind] PasswordChange passwordChange)
        {
            var viewData = Validate<JsonViewData>(
                            new Validation(() => string.IsNullOrEmpty(passwordChange.CurrentPassword), "You must specify your current password. <br/>"),
                            new Validation(() => string.IsNullOrEmpty(passwordChange.NewPassword), "You must specify a new password.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(passwordChange.NewPasswordConfirm), "Please confirm your new password.  <br/>"),
                            new Validation(() => !(passwordChange.NewPassword.IsEqual(passwordChange.NewPasswordConfirm)), "The new password and confirmation password do not match.  <br/>"),
                            new Validation(() => passwordChange.NewPassword.Length < 8, "You must specify a new password of 8 or more characters.  <br/>")

                       );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.ChangePassword(Current.User.Name, passwordChange.CurrentPassword, passwordChange.NewPassword))
                {
                    viewData.redirectUrl = "/";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Change password attempt failed.";
                }
            }

            return Json(viewData);
        }

        #endregion
        
    }

}
