﻿namespace Axxess.AgencyManagement.App.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using System.Reflection;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using System.Web;

    public class DatabaseLog : ILog
    {
        #region ILog Members

        public void Info(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Info.ToString()
                });
        }

        public void Warning(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Warn.ToString()
                });
        }

        public void Error(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Error.ToString()
                });
        }

        public void Exception(Exception e)
        {
            var type = LogPriority.Fatal.ToString();
            if (e is HttpException)
            {
                HttpException httpException = e as HttpException;
                type = httpException.GetHttpCode().ToString();
            }

            var innerError = string.Empty;
            if (e.InnerException != null)
            {
                innerError = e.InnerException.ToString();
            }

            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();

            var detail = new ErrorDetail
            {
                InnerError = innerError,
                MethodName = methodBase.Name,
                FileName = stackFrame.GetFileName(),
                ExceptionType = e.GetType().ToString(),
                LineNumber = stackFrame.GetFileLineNumber()
            };

            LogEngine.Instance.Add(
                new Error()
                {
                    Type = type,
                    Detail = detail,
                    Message = e.Message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName
                });
        }

        #endregion
    }
}
