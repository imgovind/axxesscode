﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Threading;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    public class MembershipService : IMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;

        #endregion

        #region Constructor

        public MembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public AxxessPrincipal Get(string userName)
        {
            Check.Argument.IsNotEmpty(userName, "userName");

            AxxessPrincipal principal = Cacher.Get<AxxessPrincipal>(userName);
            if (principal == null)
            {
                Login login = loginRepository.Find(userName);
                AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                {
                    DisplayName = login.DisplayName
                };

                identity.IsAxxessAdmin = login.IsAxxessAdmin;

                var permissionsArray = new List<string>();
                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                if (!login.IsAxxessAdmin)
                {
                    if (role == Roles.ApplicationUser)
                    {
                        var user = userRepository.GetByLoginId(login.Id);

                        if (user != null)
                        {
                            var agency = agencyRepository.Get(user.AgencyId);
                            identity.Session =
                                new UserSession
                                {
                                    UserId = user.Id,
                                    LoginId = login.Id,
                                    AgencyId = agency.Id,
                                    AgencyName = agency.Name,
                                    FullName = user.DisplayName,
                                    SignatureSalt = login.SignatureSalt,
                                    SignatureHash = login.SignatureHash,
                                    AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty
                                };

                            permissionsArray = user.PermissionsArray;
                        }
                    }
                }

                identity.ChangePassword = login.IsPasswordChangeRequired;
                identity.ChangeSignature = login.IsSignatureChangeRequired;

                principal = new AxxessPrincipal(identity, role, permissionsArray);
                Cacher.Set<AxxessPrincipal>(userName, principal);
            }

            return principal;
        }

        public LoginAttempt Validate(string userName, string password)
        {
            var loginAttempt = LoginAttempt.Failed;
            var login = loginRepository.Find(userName);

            if (login != null)
            {
                if (login.IsLocked)
                {
                    loginAttempt = LoginAttempt.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                        {
                            AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                            {
                                DisplayName = login.DisplayName,
                                IsAxxessAdmin = login.IsAxxessAdmin
                            };

                            if (!login.IsAxxessAdmin)
                            {
                                var user = userRepository.GetByLoginId(login.Id);
                                if (user != null)
                                {
                                    var agency = agencyRepository.Get(user.AgencyId);
                                    if (agency != null)
                                    {
                                        if (!agency.IsSuspended)
                                        {
                                            if (!agency.HasTrialPeriodExpired())
                                            {
                                                var permissionsArray = new List<string>();
                                                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                                                if (role == Roles.ApplicationUser)
                                                {
                                                    identity.Session =
                                                        new UserSession
                                                        {
                                                            UserId = user.Id,
                                                            LoginId = login.Id,
                                                            AgencyId = agency.Id,
                                                            AgencyName = agency.Name,
                                                            FullName = user.DisplayName,
                                                            SignatureSalt = login.SignatureSalt,
                                                            SignatureHash = login.SignatureHash,
                                                            AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty
                                                        };
                                                    permissionsArray = user.PermissionsArray;
                                                }

                                                if (login.IsPasswordChangeRequired)
                                                {
                                                    identity.ChangePassword = login.IsPasswordChangeRequired;
                                                    loginAttempt = LoginAttempt.ChangePasswordRequired;
                                                }
                                                else if (login.IsSignatureChangeRequired)
                                                {
                                                    identity.ChangeSignature = login.IsSignatureChangeRequired;
                                                    loginAttempt = LoginAttempt.ChangeSignatureRequired;
                                                }
                                                else
                                                {
                                                    login.LastLoginDate = DateTime.Now;
                                                    if (loginRepository.Update(login))
                                                    {
                                                        loginAttempt = LoginAttempt.Success;
                                                    }
                                                }

                                                var principal = new AxxessPrincipal(identity, role, permissionsArray);
                                                Thread.CurrentPrincipal = principal;
                                                HttpContext.Current.User = principal;
                                                Cacher.Set<AxxessPrincipal>(userName, principal);
                                            }
                                            else
                                            {
                                                loginAttempt = LoginAttempt.TrialPeriodOver;
                                            }
                                        }
                                        else
                                        {
                                            loginAttempt = LoginAttempt.AccountSuspended;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                login.LastLoginDate = DateTime.Now;
                                if (loginRepository.Update(login))
                                {
                                    loginAttempt = LoginAttempt.Success;
                                }
                                var principal = new AxxessPrincipal(identity);
                                Thread.CurrentPrincipal = principal;
                                HttpContext.Current.User = principal;
                                Cacher.Set<AxxessPrincipal>(userName, principal);
                            }
                        }
                    }
                    else
                    {
                        loginAttempt = LoginAttempt.Deactivated;
                    }
                }
            }

            return loginAttempt;
        }

        public bool Impersonate(string userName, Guid agencyId, Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(userName, "userName");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            AxxessPrincipal principal = null;
            if (Cacher.TryGet<AxxessPrincipal>(userName, out principal))
            {
                LogOff(userName);
            }

            var user = userRepository.Get(userId, Current.AgencyId);

            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                {
                    DisplayName = login.DisplayName
                };

                identity.IsAxxessAdmin = false;

                var permissionsArray = new List<string>();
                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                if (role == Roles.ApplicationUser && !login.IsAxxessAdmin)
                {
                    var agency = agencyRepository.Get(agencyId);
                    identity.Session =
                        new UserSession
                        {
                            UserId = user.Id,
                            LoginId = login.Id,
                            AgencyId = agency.Id,
                            AgencyName = agency.Name,
                            FullName = user.DisplayName
                        };

                    permissionsArray = user.PermissionsArray;
                }

                identity.ChangePassword = login.IsPasswordChangeRequired;
                identity.ChangeSignature = login.IsSignatureChangeRequired;

                principal = new AxxessPrincipal(identity, role, permissionsArray);
                Cacher.Set<AxxessPrincipal>(userName, principal);
                return true;
            }

            return false;
        }

        public bool ResetPassword(string userName)
        {
            var login = loginRepository.Find(userName);
            if (login != null)
            {
                string newSalt = string.Empty;
                string newPasswordHash = string.Empty;
                string newPassword = ShortGuid.NewId().ToString();

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(newPassword, out newPasswordHash, out newSalt);
                if (loginRepository.ChangePassword(login.Id, newPasswordHash, newSalt, true))
                {
                    AxxessPrincipal principal = null;
                    if (Cacher.TryGet<AxxessPrincipal>(login.EmailAddress, out principal))
                    {
                        AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                        identity.ChangePassword = true;
                        Cacher.Set<AxxessPrincipal>(login.EmailAddress, principal);
                    }

                    var bodyText = MessageBuilder.PrepareTextFrom("PasswordResetInstructions", "password", newPassword);
                    Notify.User(AppSettings.NoReplyEmail, userName, "Reset Password for Axxess Home Health Management Software", bodyText);
                        
                    return true;
                }
            }
            return false;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            var login = loginRepository.Find(userName);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(oldPassword, login.PasswordHash, login.PasswordSalt))
                {
                    string newSalt = string.Empty;
                    string newPasswordHash = string.Empty;

                    saltedHash.GetHashAndSalt(newPassword, out newPasswordHash, out newSalt);
                    if (loginRepository.ChangePassword(Current.User.Id, newPasswordHash, newSalt, false))
                    {
                        AxxessPrincipal principal = null;
                        if (Cacher.TryGet<AxxessPrincipal>(userName, out principal))
                        {
                            AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                            identity.ChangePassword = false;
                            Cacher.Set<AxxessPrincipal>(userName, principal);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool ResetSignature(Guid loginId)
        {
            var login = loginRepository.Find(loginId);

            if (login != null)
            {
                string signatureSalt = string.Empty;
                string signatureHash = string.Empty;
                var newSignature = ShortGuid.NewId().ToString();

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(newSignature, out signatureHash, out signatureSalt);
                login.SignatureSalt = signatureSalt;
                login.SignatureHash = signatureHash;
                login.IsSignatureChangeRequired = true;

                if (loginRepository.Update(login))
                {
                    AxxessPrincipal principal = null;
                    if (Cacher.TryGet<AxxessPrincipal>(login.EmailAddress, out principal))
                    {
                        AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                        identity.ChangeSignature = true;
                        Cacher.Set<AxxessPrincipal>(login.EmailAddress, principal);
                    }

                    var bodyText = MessageBuilder.PrepareTextFrom("ForgotSignature", "firstname", login.DisplayName, "signature", newSignature);
                    Notify.User(AppSettings.NoReplyEmail, login.EmailAddress, "Reset Signature - Axxess Home Health Management", bodyText);
                    return true;
                }
            }

            return false;
        }

        public bool ChangeSignature(Guid loginId, SignatureChange signature)
        {
            var login = loginRepository.Find(loginId);
            if (login != null)
            {
                string signaturesalt = string.Empty;
                string signatureHash = string.Empty;
                var newSignature = ShortGuid.NewId().ToString();

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(signature.NewSignature, out signatureHash, out signaturesalt);
                login.SignatureSalt = signaturesalt;
                login.SignatureHash = signatureHash;
                login.IsSignatureChangeRequired = false;

                if (loginRepository.Update(login))
                {
                    AxxessPrincipal principal = null;
                    if (Cacher.TryGet<AxxessPrincipal>(login.EmailAddress, out principal))
                    {
                        AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                        identity.ChangeSignature = false;
                        Cacher.Set<AxxessPrincipal>(login.EmailAddress, principal);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool DeactivateLogin(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    login.IsActive = false;
                    if (loginRepository.Update(login))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void LogOff(string userName)
        {
            SessionStore.Abandon();
            Cacher.Remove(userName);
        }

        #endregion
    }
}
