﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;

    using Enums;
    using Axxess.AgencyManagement.Domain;

    public interface IMembershipService
    {
        void LogOff(string userName);
        AxxessPrincipal Get(string userName);
        LoginAttempt Validate(string userName, string password);
        bool Impersonate(string userName, Guid agencyId, Guid userId);
        bool DeactivateLogin(Guid userId);

        bool ResetPassword(string userName);
        bool ChangePassword(string userName, string oldPassword, string newPassword);

        bool ResetSignature(Guid loginId);
        bool ChangeSignature(Guid loginId, SignatureChange signature);
    }
}
