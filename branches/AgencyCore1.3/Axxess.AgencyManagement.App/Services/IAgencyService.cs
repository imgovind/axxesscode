﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    
    using Enums;
    using Domain;
    using ViewData;

    public interface IAgencyService
    {
        Agency GetAgency(Guid Id);
        bool CreateAgency(Agency agency);
        bool CreateContact(AgencyContact contact);
        List<UserVisit> GetSchedule();
        List<ScheduleEvent> GetCaseManagerSchedule();
        bool CreateLocation(AgencyLocation location);
        List<RecertEvent> GetRecertsPastDue();
        List<RecertEvent> GetRecertsUpcoming();
        List<InsuranceViewData> GetInsurances();
        List<Order> GetOrdersToBeSent();
        List<Order> GetOrdersPendingSignature();
        bool MarkOrdersAsSent(FormCollection formCollection);
        void MarkOrderAsReturned(Guid id, OrderType type, DateTime dateReceived);
    }
}
