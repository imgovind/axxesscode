﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain; 

    public interface IPatientService
    {
        PatientProfile GetProfile(Guid id);
        void DeleteEpisodeAndClaims(Patient patient);
        bool CreateEpisodeAndClaims(Patient patient);
        bool SendOrderElectronically(PhysicianOrder order);
        bool NewPhysicianContact(AgencyPhysician physician, Guid patientId);
        bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId);
        bool UpdateEpisode(ScheduleEvent scheduleEvent, Guid oldEmployeeId);
        bool UpdateEpisode(Guid episodeId, Guid patientId, string jsonString);
        bool UpdateEpisode(Guid episodeId, Guid patientId, string disciplineTask, string discipline, Guid userId, bool IsBillable, DateTime startDate, DateTime endDate);
        bool UpdateEpisode(Guid episodeId, Guid patientId, ScheduleEvent scheduleEvent);
        bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId);
        bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid userId);
        PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline);
        PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId);
        bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        bool SaveNotes(string button, FormCollection formCollection);
        bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason);
        bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate);
        bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);
        bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId);
        PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent);
        PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode, ScheduleEvent scheduleEvent);
        Rap CreateRap(Patient patient, PatientEpisode episode);
        Final CreateFinal(Patient patient, PatientEpisode episode);
        bool CreateMedicationProfile(Patient patient, Guid medId);
        bool LinkPhysicians(Patient patient);
        bool UpdateEpisode(PatientEpisode episode);

        //bool UpdateMedicationProfileHistory(string button, MedicationProfileHistory medicationProfile);
        List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory);
        bool SignMedicationHistory(Guid medId, MedicationProfileHistory medicationProfileHistory);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId);

        bool AddMissedVisit(MissedVisit missedVisit);
        bool IsValidImage(HttpFileCollectionBase httpFiles);

        bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles);

        bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, int supplyId, string quantity , string date);
        bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId);

        bool AdmitPatient(PendingPatient pending);
        bool NonAdmitPatient(PendingPatient patient);

        bool Reopen(Guid episodeId, Guid patientId, Guid eventId);
    }
}
