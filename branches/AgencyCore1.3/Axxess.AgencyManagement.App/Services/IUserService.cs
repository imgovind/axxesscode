﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public interface IUserService
    {
        bool CreateUser(User user);
        List<UserVisit> GetSchedule(Guid userId);
        List<UserVisit> GetSchedule(Guid userId, DateTime start, DateTime end);
        bool IsEmailAddressUnique(string emailAddress);

        bool UpdateProfile(User user);
        bool IsPasswordCorrect(Guid userId, string password);
        bool IsSignatureCorrect(Guid userId, string signature);

        bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles);
        bool UpdatePermissions(FormCollection formCollection);

        bool DeleteLicense(Guid Id, Guid userId);

        bool UpdateLicense(Guid Id, Guid userId, DateTime ExpirationDate);
    }
}
