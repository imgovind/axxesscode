﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using ViewData;
    using Axxess.AgencyManagement.Domain;

    public interface IReportService
    {
        #region Patient Reports

        List<Birthday> GetCurrentBirthdays();
        List<Birthday> GetPatientBirthdays(Guid addressBranchCode);
        List<AddressBookEntry> GetPatientAddressListing(Guid addressBranchCode);
        List<PatientRoster> GetPatientRoster(int statusId, Guid addressBranchCode);
        List<EmergencyContactInfo> GetPatientEmergencyContacts(int statusId, Guid addressBranchCode);
        List<PatientSocCertPeriod> GetPatientSocCertPeriod(DateTime startDate, DateTime endDate);
        List<PatientOnCallListing> GetPatientOnCallListing(DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId);

        #endregion

        #region Oasis Reports

        IList<OpenOasis> GetAllOpenOasis();
        IList<ClinicalOrder> GetOrders(int statusId);

        #endregion

    }
}
