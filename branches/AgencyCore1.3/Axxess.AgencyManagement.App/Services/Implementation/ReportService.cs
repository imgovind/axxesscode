﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    public class ReportService : IReportService
    {
        #region Constructor and Private Members

        private readonly IUserRepository userRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        public ReportService(IAgencyManagementDataProvider agencyManagementDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Patient Reports

        public List<Birthday> GetPatientBirthdays(Guid addressBranchCode)
        {
            IList<Patient> patients = null;
            var birthdays = new List<Birthday>();
            if (addressBranchCode.IsEmpty())
            {
                patients = patientRepository.Find((int)PatientStatus.Active, Current.AgencyId);
            }
            else
            {
                patients = patientRepository.Find((int)PatientStatus.Active, addressBranchCode, Current.AgencyId);
            }
            patients.ForEach(patient =>
            {
                birthdays.Add(new Birthday
                {
                    Id = patient.Id,
                    Name = patient.DisplayName,
                    Date = patient.DOB,
                    AddressLine1 = patient.AddressLine1,
                    AddressLine2 = patient.AddressLine2,
                    AddressCity = patient.AddressCity,
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    PhoneHome = patient.PhoneHome,
                    PhoneMobile = patient.PhoneMobile,
                    EmailAddress = patient.EmailAddress
                });
            });

            return birthdays;
        }

        public List<Birthday> GetPatientBirthdays(DateTime startDate, DateTime endDate)
        {
            List<Birthday> birthdays = new List<Birthday>();
            var patients = patientRepository.GetAllByBirthDayRange(startDate, endDate, Current.AgencyId);

            patients.ForEach(patient =>
            {
                birthdays.Add(new Birthday
                {
                    Id = patient.Id,
                    Name = patient.DisplayName,
                    Date = patient.DOB,
                    AddressLine1 = patient.AddressLine1,
                    AddressLine2 = patient.AddressLine2,
                    AddressCity = patient.AddressCity,
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    PhoneHome = patient.PhoneHome,
                    PhoneMobile = patient.PhoneMobile,
                    EmailAddress = patient.EmailAddress
                });
            });

            return birthdays;
        }

        public List<Birthday> GetCurrentBirthdays()
        {
            var birthdays = new List<Birthday>();
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);

            patients.ForEach(patient =>
            {
                if (patient.DOB.Month == DateTime.Now.Month)
                {
                    birthdays.Add(new Birthday
                    {
                        Id = patient.Id,
                        Name = patient.DisplayName,
                        Date = patient.DOB,
                        AddressLine1 = patient.AddressLine1,
                        AddressLine2 = patient.AddressLine2,
                        AddressCity = patient.AddressCity,
                        AddressStateCode = patient.AddressStateCode,
                        AddressZipCode = patient.AddressZipCode,
                        PhoneHome = patient.PhoneHome,
                        PhoneMobile = patient.PhoneMobile,
                        EmailAddress = patient.EmailAddress
                    });
                }
            });

            return birthdays;
        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid addressBranchCode)
        {
            List<AddressBookEntry> contacts = new List<AddressBookEntry>();
            var patients = patientRepository.Find((int)PatientStatus.Active, addressBranchCode, Current.AgencyId);
            patients.ForEach(patient =>
            {
                contacts.Add(new AddressBookEntry
                {
                    Id = patient.Id,
                    Name = patient.DisplayName,
                    AddressLine1 = patient.AddressLine1.ToTitleCase(),
                    AddressLine2 = patient.AddressLine2.ToTitleCase(),
                    AddressCity = patient.AddressCity.ToTitleCase(),
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    PhoneHome = patient.PhoneHome.ToPhone(),
                    PhoneMobile = patient.PhoneMobile.ToPhone(),
                    EmailAddress = patient.EmailAddress
                });
            });

            return contacts;
        }

        public List<EmergencyContactInfo> GetPatientEmergencyContacts(int statusId, Guid branchCode)
        {
            var contacts = new List<EmergencyContactInfo>();
            var patients = patientRepository.Find(statusId, branchCode, Current.AgencyId);
            patients.ForEach(patient =>
            {
                var emergencyContact = patientRepository.GetEmergencyContacts(patient.Id).Where(c => c.IsPrimary).FirstOrDefault();
                var emergencyContactInfo = new EmergencyContactInfo();
                emergencyContactInfo.PatientName = patient.DisplayName;
                emergencyContactInfo.Id = patient.Id;
                if (emergencyContact != null)
                {
                    emergencyContactInfo.ContactName = emergencyContact.FirstName + ", " + emergencyContact.LastName;
                    emergencyContactInfo.ContactRelation = emergencyContact.Relationship;
                    emergencyContactInfo.ContactPhoneHome = emergencyContact.PrimaryPhone;
                    emergencyContactInfo.ContactEmailAddress = emergencyContact.EmailAddress;
                }
                contacts.Add(emergencyContactInfo);
            });

            return contacts;
        }

        public List<PatientSocCertPeriod> GetPatientSocCertPeriod(DateTime startDate, DateTime endDate)
        {
            List<PatientSocCertPeriod> SocCertPeriod = new List<PatientSocCertPeriod>();
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            patients.ForEach(patient =>
            {
                var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, patient.Id, DateTime.Now, "Nursing");
                var socCertPeriod = new PatientSocCertPeriod();
                if (lastEpisode != null)
                {
                    socCertPeriod.SocCertPeriod = lastEpisode.StartDateFormatted + " - "
                        + lastEpisode.EndDateFormatted;
                }
                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id).Where(p => p.Primary).FirstOrDefault();
                var patientRoster = new PatientRoster();
                socCertPeriod.Id = patient.Id;
                socCertPeriod.PatientPatientID = patient.PatientIdNumber;
                socCertPeriod.PatientLastName = patient.LastName;
                socCertPeriod.PatientFirstName = patient.FirstName;
                socCertPeriod.PatientSoC = patient.StartOfCareDateFormatted;
                if (patient.Physician != null)
                {
                    socCertPeriod.PhysicianName = patient.Physician.LastName + ", " + patient.Physician.FirstName;
                }

                var emp = userRepository.Get(patient.UserId, Current.AgencyId);
                socCertPeriod.respEmp = "";
                if (emp != null)
                {
                    socCertPeriod.respEmp = emp.DisplayName;
                }
                SocCertPeriod.Add(socCertPeriod);
            });

            return SocCertPeriod;
        }

        public List<PatientOnCallListing> GetPatientOnCallListing(DateTime startDate, DateTime endDate)
        {
            List<PatientOnCallListing> OnCall = new List<PatientOnCallListing>();
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            patients.ForEach(patient =>
            {
                var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, patient.Id, DateTime.Now, "Nursing");
                var emergencyContact = patientRepository.GetEmergencyContacts(patient.Id).Where(c => c.IsPrimary).FirstOrDefault();

                var onCall = new PatientOnCallListing();
                if (lastEpisode != null)
                {
                    onCall.SocCertPeriod = lastEpisode.StartDateFormatted + " - "
                        + lastEpisode.EndDateFormatted;
                }
                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id).Where(p => p.Primary).FirstOrDefault();
                var patientRoster = new PatientRoster();
                onCall.Id = patient.Id;
                onCall.PatientPatientID = patient.PatientIdNumber;
                onCall.PatientLastName = patient.LastName;
                onCall.PatientFirstName = patient.FirstName;
                onCall.PatientSoC = patient.StartOfCareDateFormatted;
                if (patient.Physician != null)
                {
                    onCall.PhysicianName = patient.Physician.LastName + ", " + patient.Physician.FirstName;
                    onCall.PhysicianPhone = patient.Physician.PhoneWork;
                    onCall.PhysicianFacsimile = patient.Physician.FaxNumber;
                    onCall.PhysicianPhoneHome = patient.Physician.FaxNumber;
                    onCall.PhysicianEmailAddress = patient.Physician.EmailAddress;
                }

                var emp = userRepository.Get(patient.UserId, Current.AgencyId);
                onCall.respEmp = "";
                if (emp != null)
                {
                    onCall.respEmp = emp.DisplayName;
                }
                if (emergencyContact != null)
                {
                    onCall.ContactName = emergencyContact.FirstName + ", " + emergencyContact.LastName;
                    onCall.ContactRelation = emergencyContact.Relationship;
                    onCall.ContactPhoneHome = emergencyContact.PrimaryPhone;
                    onCall.ContactEmailAddress = emergencyContact.EmailAddress;
                }
                onCall.PatientInsurance = patient.PaymentSource;
                OnCall.Add(onCall);
            });

            return OnCall;
        }

        public List<PatientRoster> GetPatientRoster(int statusId, Guid branchCode)
        {
            var rosterList = new List<PatientRoster>();
            var patients = patientRepository.Find(statusId, branchCode, Current.AgencyId);
            patients.ForEach(patient =>
            {
                var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, patient.Id, DateTime.Now, "Nursing");
                IDictionary<string, Question> lastAssessment = null;
                if (lastEpisode != null && !lastEpisode.AssessmentId.IsEmpty() && lastEpisode.AssessmentType.IsNotNullOrEmpty())
                {
                    lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                }
                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id).Where(p => p.Primary).FirstOrDefault();

                var roster = new PatientRoster();
                roster.Id = patient.Id;
                roster.PatientId = patient.PatientIdNumber;
                roster.PatientLastName = patient.LastName;
                roster.PatientFirstName = patient.FirstName;
                roster.PatientDisplayName = patient.DisplayName;
                roster.PatientGender = patient.Gender;
                roster.PatientMedicareNumber = patient.MedicareNumber;
                roster.PatientDOB = patient.DOB;
                roster.PatientPhone = patient.PhoneHome;
                roster.PatientAddressLine1 = patient.AddressLine1;
                roster.PatientAddressLine2 = patient.AddressLine2;
                roster.PatientAddressCity = patient.AddressCity;
                roster.PatientAddressStateCode = patient.AddressStateCode;
                roster.PatientAddressZipCode = patient.AddressZipCode;
                roster.PatientSoC = patient.StartOfCareDateFormatted;
                if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                {
                    roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                }
                else
                {
                    roster.PatientPrimaryDiagnosis = "";
                }
                if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                {
                    roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                }
                else
                {
                    roster.PatientSecondaryDiagnosis = "";
                }
                roster.PatientInsurance = patient.PaymentSource;
                if (patient.Physician != null)
                {
                    roster.PhysicianName = patient.Physician.DisplayName;
                    roster.PhysicianPhone = patient.Physician.PhoneWork;
                    roster.PhysicianFacsimile = patient.Physician.FaxNumber;
                    roster.PhysicianPhoneHome = patient.Physician.FaxNumber;
                    roster.PhysicianEmailAddress = patient.Physician.EmailAddress;
                }

                rosterList.Add(roster);

            });

            return rosterList;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId)
        {
            var rosterList = new List<PatientRoster>();
            IList<Patient> patients = new List<Patient>();

            if (agencyPhysicianId.IsEmpty())
            {
                patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            }
            else
            {
                patients = physicianRepository.GetPhysicanPatients(agencyPhysicianId);
            }
            patients.ForEach(patient =>
            {
                var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, patient.Id, DateTime.Now, "Nursing");
                IDictionary<string, Question> lastAssessment = null;
                if (lastEpisode != null)
                {
                    lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                }
                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id).Where(p => p.Primary).FirstOrDefault();

                var roster = new PatientRoster();
                roster.Id = patient.Id;
                roster.PatientId = patient.PatientIdNumber;
                roster.PatientLastName = patient.LastName;
                roster.PatientFirstName = patient.FirstName;
                roster.PatientDisplayName = patient.DisplayName;
                roster.PatientGender = patient.Gender;
                roster.PatientMedicareNumber = patient.MedicareNumber;
                roster.PatientDOB = patient.DOB;
                roster.PatientPhone = patient.PhoneHome;
                roster.PatientAddressLine1 = patient.AddressLine1;
                roster.PatientAddressLine2 = patient.AddressLine2;
                roster.PatientAddressCity = patient.AddressCity;
                roster.PatientAddressStateCode = patient.AddressStateCode;
                roster.PatientAddressZipCode = patient.AddressZipCode;
                roster.PatientSoC = patient.StartOfCareDateFormatted;
                if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                {
                    roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                }
                else
                {
                    roster.PatientPrimaryDiagnosis = "";
                }
                if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                {
                    roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                }
                else
                {
                    roster.PatientSecondaryDiagnosis = "";
                }
                roster.PatientInsurance = patient.PaymentSource;
                if (patient.Physician != null)
                {
                    roster.PhysicianName = patient.Physician.DisplayName;
                    roster.PhysicianPhone = patient.Physician.PhoneWork;
                    roster.PhysicianFacsimile = patient.Physician.FaxNumber;
                    roster.PhysicianPhoneHome = patient.Physician.FaxNumber;
                    roster.PhysicianEmailAddress = patient.Physician.EmailAddress;
                }

                rosterList.Add(roster);

            });

            return rosterList;
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid Name, string AddressStateCode, Guid AddressBranchCode)
        {
            List<PatientRoster> Roster = new List<PatientRoster>();
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            patients.ForEach(patient =>
            {

                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id).Where(p => p.Primary).FirstOrDefault();
                var patientRoster = new PatientRoster();
                patientRoster.Id = patient.Id;
                patientRoster.PatientId = patient.PatientIdNumber;
                patientRoster.PatientLastName = patient.LastName;
                patientRoster.PatientFirstName = patient.FirstName;
                patientRoster.PatientSoC = patient.StartOfCareDateFormatted;
                if (patient.Physician != null)
                {
                    patientRoster.PhysicianName = patient.Physician.LastName + ", " + patient.Physician.FirstName;
                }
                var emp = userRepository.Get(patient.UserId, Current.AgencyId);
                patientRoster.ResponsibleEmployee = "";
                if (emp != null)
                {
                    patientRoster.ResponsibleEmployee = emp.DisplayName;
                }
                Roster.Add(patientRoster);
            });

            return Roster.OrderBy(p => p.PhysicianName).ToList();
        }

        #endregion

        #region Clinical Reports

        public IList<OpenOasis> GetAllOpenOasis()
        {
            IList<OpenOasis> openOasisList = new List<OpenOasis>();

            var patients = patientRepository.Find((int)PatientStatus.Active, Current.AgencyId);
            patients.ForEach(patient =>
            {
                var patientEpisodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patient.Id);
                if (patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            List<ScheduleEvent> events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (events != null && events.Count > 0)
                            {
                                events.ForEach(e =>
                                {
                                    if (e.IsAssessment() && e.IsOpen())
                                    {
                                        var openOasis = new OpenOasis();
                                        openOasis.PatientIdNumber = patient.PatientIdNumber;
                                        openOasis.PatientName = patient.DisplayName.ToTitleCase();
                                        openOasis.AssessmentId = e.EventId;
                                        openOasis.AssessmentName = e.DisciplineTaskName;
                                        openOasis.Status = e.StatusName;
                                        openOasis.Date = e.TargetDate;
                                        openOasisList.Add(openOasis);
                                    }
                                });
                            }
                        }
                    });
                }
            });

            return openOasisList;
        }

        public IList<ClinicalOrder> GetOrders(int statusId)
        {
            IList<ClinicalOrder> orderList = new List<ClinicalOrder>();

            var patients = patientRepository.Find((int)PatientStatus.Active, Current.AgencyId);
            patients.ForEach(patient =>
            {
                var patientEpisodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patient.Id);
                if (patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            List<ScheduleEvent> events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (events != null && events.Count > 0)
                            {
                                events.ForEach(e =>
                                {
                                    if (e.IsOrderAndStatus(statusId))
                                    {
                                        var order = patientRepository.GetOrder(e.EventId, patient.Id, Current.AgencyId);
                                        if (order != null)
                                        {
                                            var clinicalOrder = new ClinicalOrder();
                                            clinicalOrder.Id = e.EventId.ToString();
                                            clinicalOrder.Type = e.DisciplineTaskName;
                                            clinicalOrder.Number = order.OrderNumber.ToString();
                                            clinicalOrder.PatientName = patient.DisplayName.ToTitleCase();
                                            clinicalOrder.Physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId).DisplayName;
                                            clinicalOrder.Status = e.StatusName;
                                            clinicalOrder.CreatedDate = order.Created.ToShortDateString();
                                            orderList.Add(clinicalOrder);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });

            return orderList;
        }


        #endregion

    }
}
