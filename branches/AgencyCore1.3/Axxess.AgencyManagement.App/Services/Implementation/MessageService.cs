﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    public class MessageService : IMessageService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IMessageRepository messageRepository;

        public MessageService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
        }

        public bool SendMessage(Message message)
        {
            var result = false;
            var names = new StringBuilder();
            var recipients = new Dictionary<Guid, string>();
            var emailAddresses = new Dictionary<Guid, string>();

            message.FromId = Current.UserId;
            message.FromName = Current.UserFullName;

            message.Recipients.ForEach(u =>
            {
                var user = userRepository.Get(u, Current.AgencyId);
                if (user != null)
                {
                    recipients.Add(u, user.FirstName);
                    names.AppendFormat("{0}; ", user.DisplayName);

                    if (!user.LoginId.IsEmpty())
                    {
                        var login = loginRepository.Find(user.LoginId);
                        if (login != null)
                        {
                            emailAddresses.Add(u, login.EmailAddress);
                        }
                    }
                }
            });

            message.Recipients.ForEach(r =>
            {
                message.RecipientId = r;
                message.RecipientNames = names.ToString();
                result = messageRepository.Add(message);
                if (result)
                {
                    var parameters = new string[4];
                    parameters[0] = "recipientfirstname";
                    parameters[1] = recipients[r];
                    parameters[2] = "senderfullname";
                    parameters[3] = Current.UserFullName;
                    var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                    Notify.User(AppSettings.NoReplyEmail, emailAddresses[r], string.Format("{0} sent you a message.", Current.UserFullName), bodyText);
                }
            });

            return result;
        }
    }
}
