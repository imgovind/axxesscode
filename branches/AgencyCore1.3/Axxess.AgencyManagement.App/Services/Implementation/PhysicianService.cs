﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    
    using Security;
    using Axxess.Membership.Enums;

    public class PhysicianService : IPhysicianService
    {
        private readonly IPhysicianRepository physicianRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        public bool CreatePhysician(AgencyPhysician physician)
        {
            return false;
        }

    }
}
