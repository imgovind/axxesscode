﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using Security;
    using System.Xml.Linq;
    using System.Net;
    using System.IO;
    using System.Text.RegularExpressions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.Membership.Enums;

    using Axxess.LookUp.Domain;

    using Axxess.OasisC.Repositories;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Domain.Billing;
   
    public class BillingService : IBillingService
    {

        #region Private Members

        private IUserRepository userRepository;
        private IAssessmentService assessmentService;
        private IPatientRepository patientRepository;
        private IBillingRepository billingRepository;
        private IReferralRepository referrralRepository;
        private IPhysicianRepository physicianRepository;
        private IAgencyRepository agencyRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private ILookupRepository lookUpRepository;

        #endregion

        #region Constructor

        public BillingService(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.assessmentService = assessmentService;
            this.patientRepository = dataProvider.PatientRepository;
            this.userRepository = dataProvider.UserRepository;
            this.referrralRepository = dataProvider.ReferralRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
        }

        public string Generate(List<Guid> rapToGenerate, List<Guid> finalToGenerate)
        {
            Agency agency = agencyRepository.Get(Current.AgencyId);
            string strResult = String.Empty;
            if (agency != null)
            {
                var visitRateWithUnit = GetInsuranceUnit(0);

                List<Rap> rapClaim = new List<Rap>();
                List<Final> finlaClaim = new List<Final>();
                var requestArr = "";
                
                if (rapToGenerate != null)
                {
                    rapToGenerate.ForEach(rap =>
                    {
                        Rap claim = null;
                        if (rap != Guid.Empty)
                        {
                            claim = billingRepository.GetRap(Current.AgencyId, rap);
                        }
                        if (claim != null)
                        {
                            rapClaim.Add(claim);
                        }
                    });
                }
                if (finalToGenerate != null)
                {
                    finalToGenerate.ForEach(final =>
                    {
                        Final claim = null;
                        if (final != Guid.Empty)
                        {
                            claim = billingRepository.GetFinal(Current.AgencyId, final);
                        }
                        if (claim != null)
                        {
                            finlaClaim.Add(claim);
                        }
                    });
                }

                var claimData = new ClaimData { AgencyId = Current.AgencyId };
                AgencyLocation agencyLocation = agencyRepository.GetMainLocation(agency.Id);
                List<ClaimInfo> claimInfo = new List<ClaimInfo>();
                if (agencyLocation != null)
                {
                    List<object> patients = new List<object>();
                    int rapCount = rapClaim.Count;
                    foreach (var rap in rapClaim)
                    {
                        var diganosis = XElement.Parse(rap.DiagonasisCode);
                        List<object> claims = new List<object>();

                        claimInfo.Add(new ClaimInfo { ClaimId = rap.Id, PatientId = rap.PatientId, EpisodeId = rap.EpisodeId, ClaimType = rap.Type == 0 ? "329" : "" });
                        var rapObj = new
                        {
                            claim_id = rap.Id,
                            claim_type = rap.Type == 1 ? "" : "329",
                            claim_physician_upin = rap.PhysicianNPI,
                            claim_physician_last_name = rap.PhysicianLastName,
                            claim_physician_first_name = rap.PhysicianFirstName,
                            claim_first_visit_date = rap.FirstBillableVisitDate.ToShortDateString(),
                            claim_episode_start_date = rap.EpisodeStartDate.ToShortDateString(),
                            claim_episode_end_date = rap.EpisodeStartDate.ToShortDateString(),
                            claim_hipps_code = rap.HippsCode,
                            claim_oasis_key = rap.ClaimKey,
                            claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                            claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                            claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                            claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                            claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                            claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""),
                            claim_total_charge_amount = "0",
                            claim_admission_source_code = rap.AdmissionSource.IsNotNullOrEmpty()? lookUpRepository.GetAdmissionSourceCode(int.Parse(rap.AdmissionSource)):string.Empty,
                            claim_patient_status_code ="30",//rap.PatientStatus
                        };
                        claims.Add(rapObj);
                        if (finlaClaim != null && finlaClaim.Count > 0)
                        {
                            var final = finlaClaim.Find(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                            var visitTotalAmount = 0.00;
                            if (final != null)
                            {
                                var visits = final.VerifiedVisits != null && final.VerifiedVisits != "" ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                                List<object> visistList = new List<object>();
                                if (visits.Count > 0)
                                {
                                    visits.ForEach(v =>
                                    {
                                        var discipline = v.Discipline == "Nursing" ? "SN" : v.Discipline;
                                        visitTotalAmount += double.Parse(visitRateWithUnit.ContainsKey(discipline) ? visitRateWithUnit[discipline].Amount : "0.00");
                                        visistList.Add(new { date = v.EventDate, type = discipline });
                                    });
                                }
                                claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "322" : "" });
                                var finalObj = new
                                {
                                    claim_id = final.Id,
                                    claim_type = final.Type == 1 ? "" : "322",
                                    claim_physician_upin = final.PhysicianNPI,
                                    claim_physician_last_name = final.PhysicianLastName,
                                    claim_physician_first_name = final.PhysicianFirstName,
                                    claim_first_visit_date = final.FirstBillableVisitDate.ToShortDateString(),
                                    claim_episode_start_date = final.EpisodeStartDate.ToShortDateString(),
                                    claim_episode_end_date = final.EpisodeEndDate.ToShortDateString(),
                                    claim_hipps_code = final.HippsCode,
                                    claim_oasis_key = final.ClaimKey,
                                    claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""),
                                    claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                                    claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(final.AdmissionSource)) : string.Empty,
                                    claim_patient_status_code = "30",//final.PatientStatus,
                                    claim_supply_value = final.SupplyTotal,
                                    claim_visits = visistList
                                };
                                claims.Add(finalObj);
                                finlaClaim.RemoveAll(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                            }
                        }
                        var patient = new
                        {
                            patient_gender = rap.Gender.Substring(0, 1),
                            patient_medicare_num = rap.MedicareNumber,
                            patient_record_num = rap.PatientIdNumber,
                            patient_dob = rap.DOB.ToShortDateString(),
                            patient_doa = rap.StartofCareDate.ToShortDateString(),
                            patient_dod = "",
                            patient_address = rap.AddressLine1,
                            patient_address2 = rap.AddressLine2,
                            patient_city = rap.AddressCity,
                            patient_state = rap.AddressStateCode,
                            patient_zip = rap.AddressZipCode,
                            patient_cbsa = lookUpRepository.CbsaCodeByZip(rap.AddressZipCode),
                            patient_last_name = rap.LastName,
                            patient_first_name = rap.FirstName,
                            patient_middle_initial = "",
                            claims_arr = claims
                        };
                        patients.Add(patient);
                    }

                    if (finlaClaim != null && finlaClaim.Count > 0)
                    {
                        foreach (var final in finlaClaim)
                        {
                            List<object> claims = new List<object>();
                            var diganosis = XElement.Parse(final.DiagonasisCode);
                            var visitTotalAmount = 0.00;
                            var visits = final.VerifiedVisits != null && final.VerifiedVisits != "" ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                            List<object> visistList = new List<object>();
                            if (visits.Count > 0)
                            {
                                visits.ForEach(v =>
                                {
                                    var discipline = v.Discipline == "Nursing" ? "SN" : v.Discipline;
                                    visitTotalAmount += double.Parse(visitRateWithUnit.ContainsKey(discipline) ? visitRateWithUnit[discipline].Amount : "0.00");
                                    visistList.Add(new { date = v.EventDate, type = discipline });
                                });
                            }
                            claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "322" : "" });
                            var finalObj = new
                            {
                                claim_id = final.Id,
                                claim_type = final.Type == 1 ? "" : "322",
                                claim_physician_upin = final.PhysicianNPI,
                                claim_physician_last_name = final.PhysicianLastName,
                                claim_physician_first_name = final.PhysicianFirstName,
                                claim_first_visit_date = final.FirstBillableVisitDate.ToShortDateString(),
                                claim_episode_start_date = final.EpisodeStartDate.ToShortDateString(),
                                claim_episode_end_date = final.EpisodeEndDate.ToShortDateString(),
                                claim_hipps_code = final.HippsCode,
                                claim_oasis_key = final.ClaimKey,
                                claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_total_charge_amount =  Math.Round(visitTotalAmount,2),
                                claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(final.AdmissionSource)) : string.Empty,
                                claim_patient_status_code = "30",//final.PatientStatus,
                                claim_supply_value=final.SupplyTotal,
                                claim_visits = visistList
                            };
                            claims.Add(finalObj);
                            var patient = new
                            {
                                patient_gender = final.Gender.Substring(0, 1),
                                patient_medicare_num = final.MedicareNumber,
                                patient_record_num = final.PatientIdNumber,
                                patient_dob = final.DOB.ToShortDateString(),
                                patient_doa = final.StartofCareDate.ToShortDateString(),
                                patient_dod = "",
                                patient_address = final.AddressLine1,
                                patient_address2 = final.AddressLine2,
                                patient_city = final.AddressCity,
                                patient_state = final.AddressStateCode,
                                patient_zip = final.AddressZipCode,
                                patient_cbsa = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
                                patient_last_name = final.LastName,
                                patient_first_name = final.FirstName,
                                patient_middle_initial = "",
                                claims_arr = claims
                            };
                            patients.Add(patient);
                        }
                    }

                    var visitRates = new List<object>();
                    visitRateWithUnit.ForEach(vr =>
                    {
                        var key= vr.Key;
                        var value=vr.Value;
                        var visitRate = GetVisitRateInstnace(key, value.CodeOne, value.CodeTwo, value.Amount, value.Unit);
                        if (visitRate != null)
                        {
                            visitRates.Add(visitRate);
                        }
                    });
                    var claimId = GetNextClaimId(claimData);
                    claimData.Id = claimId;
                    var agencyClaim = new
                    {
                        format = "ansi837",
                        user_login_name = Current.User.Name,
                        payer_id ="C00380" ,//agency.SubmitterId,
                        payer_name = "Palmetto GBA",// agency.SubmitterName,
                        submitter_id = agency.SubmitterId,
                        submitter_name = agency.SubmitterName,
                        submitter_phone = agency.SubmitterPhone,
                        submitter_fax = agency.SubmitterFax,
                        user_agency_name = agency.Name,
                        user_tax_id = agency.TaxId,
                        user_national_provider_id = agency.NationalProviderNumber,
                        user_address_1 = agencyLocation.AddressLine1,
                        user_address_2 = agencyLocation.AddressLine2,
                        user_city = agencyLocation.AddressCity,
                        user_state = agencyLocation.AddressStateCode,
                        user_zip = agencyLocation.AddressZipCode,
                        user_phone = agencyLocation.PhoneWork,
                        user_fax = agencyLocation.FaxNumber,
                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(agencyLocation.AddressZipCode),
                        ansi_837_id = claimId,
                        visit_rates =visitRates,
                        patients_arr = patients
                    };
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    requestArr = jss.Serialize(agencyClaim);
                }
                if (requestArr.IsNotNullOrEmpty())
                {
                    strResult = GenerateANSI(requestArr);
                    if (strResult != string.Empty)
                    {
                        claimData.Data = strResult;
                        claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                        billingRepository.UpdateClaimData(claimData);
                    }
                    else
                    {
                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                    }
                }
            }
            return strResult;
        }

        public string GenerateANSI(string jsonData)
        {
            var strResult = string.Empty;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonData);
                byte[] data = encoding.GetBytes(postData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AppSettings.ANSIGeneraterUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                Stream newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream receiveStream = response.GetResponseStream();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(receiveStream, encode);
                    strResult = readStream.ReadToEnd();
                }
                else
                {
                    strResult = string.Empty;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return strResult;
        }

        public bool VerifyFinal(int claim, string type)
        {
            Check.Argument.IsNotNegativeOrZero(claim, "claim");

            bool result = false;
            var currentClaim = billingRepository.GetClaim(Current.AgencyId, claim);
            if (currentClaim != null)
            {
                switch (type)
                {
                    case "Info":
                        currentClaim.IsFinalInfoVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                    case "Visit":
                        currentClaim.IsVisitVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                    case "Supply":
                        currentClaim.IsSupplyVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                }
            }
            return result;
        }

        public bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                if (scheduleEvents != null)
                {
                    var visitList = new List<ScheduleEvent>();
                    var claim = billingRepository.GetFinal(Current.AgencyId, Id);
                    var finalVisit = claim!=null && claim.VerifiedVisits.IsNotNullOrEmpty()? claim.VerifiedVisits.ToObject<List<ScheduleEvent>>(): new List<ScheduleEvent>();
                    Visit.ForEach(v =>
                    {
                        var visit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                        if (visit != null)
                        {
                            visit.IsBillable = true;
                            visitList.Add(visit);
                           
                        }
                    });
                    if (finalVisit != null) 
                    {
                        finalVisit.ForEach(f => {
                            if (scheduleEvents.Exists(e => e.EventId == f.EventId)&& !Visit.Contains(f.EventId) )
                            {
                                scheduleEvents.FirstOrDefault(e=>e.EventId==f.EventId).IsBillable=false;
                            }
                        });
                    }
                    claim.IsVisitVerified = true;
                    claim.VerifiedVisits = visitList.ToXml();
                    claim.Modified = DateTime.Now;
                    episode.Schedule = scheduleEvents.ToXml();
                    if (patientRepository.UpdateEpisode(Current.AgencyId, episode))
                    {
                        result = billingRepository.UpdateFinalStatus(claim);
                    }
                }
            }
            return result;
        }

        public bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, FormCollection formCollection, List<Guid> SupplyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var keys = formCollection.AllKeys;
            var claim = billingRepository.GetFinal(Current.AgencyId, Id);
            if (claim != null)
            {
                var visits = new List<ScheduleEvent>();
                var episodeSupplyList = new List<Supply>();
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                   visits= claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                   if (visits != null)
                   {
                       visits.ForEach(v =>
                       {
                           episodeSupplyList.AddRange(this.GetSupply(v));
                       });
                   }
                }
                var tempSupplyList = new List<Supply>();
                var existingSupplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                if (SupplyId != null)
                {
                    SupplyId.ForEach(v =>
                    {

                        if (existingSupplyList.Exists(s => s.UniqueIdentifier == v))
                        {
                            var currentSupply = existingSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                            if (currentSupply != null)
                            {
                                currentSupply.IsBillable = true;
                                currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                tempSupplyList.Add(currentSupply);
                            }
                        }
                        else if (episodeSupplyList.Exists(s => s.UniqueIdentifier == v))
                        {
                            var currentSupply = episodeSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                            if (currentSupply != null)
                            {
                                currentSupply.IsBillable = true;
                                currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                tempSupplyList.Add(currentSupply);
                            }
                        }
                    });
                }
                claim.SupplyTotal = formCollection["SupplyTotal"].ToDouble();
                claim.Supply = tempSupplyList.ToXml();
                claim.Modified = DateTime.Now;
                claim.IsSupplyVerified = true;
                result = billingRepository.UpdateFinalStatus(claim);
            }
            return result;
        }

        public Bill AllUnProcessedBill()
        {
            var raps = billingRepository.GetUnProcessedRaps(Current.AgencyId);
            if (raps != null)
            {
                raps.ForEach(rap =>
                {
                        rap.IsFirstBillableVisit = rap.IsFirstBillableVisit || patientRepository.IsFirstBillableVist(Current.AgencyId, rap.EpisodeId, rap.PatientId);
                        rap.IsOasisComplete = rap.IsOasisComplete || patientRepository.IsOasisComplete(Current.AgencyId, rap.EpisodeId, rap.PatientId);
                    }
               );
            }
            var finals = billingRepository.GetUnProcessedFinals(Current.AgencyId);

            if (finals != null)
            {
                finals.ForEach(final =>
                {
                        final.IsFirstBillableVisit = final.IsFirstBillableVisit || patientRepository.IsFirstBillableVist(Current.AgencyId, final.EpisodeId, final.PatientId);
                        final.IsOasisComplete = final.IsOasisComplete || patientRepository.IsOasisComplete(Current.AgencyId, final.EpisodeId, final.PatientId);
                        final.AreOrdersComplete = final.AreOrdersComplete || patientRepository.IsOrdersComplete(Current.AgencyId,final.EpisodeId,final.PatientId);
                        var rap = billingRepository.GetRap(Current.AgencyId, final.Id);
                        if (rap != null)
                        {
                            final.IsRapGenerated = final.IsRapGenerated || rap.IsGenerated;
                        }
                }
                    );
            }
            return new Bill { Raps = raps, Finals = finals };
        }

        public IList<Rap> AllUnProcessedRap()
        {
            var raps = billingRepository.GetRaps(Current.AgencyId).Where(r=>r.Status==(int) ScheduleStatus.ClaimCreated).ToList();
            if (raps != null)
            {
                raps.ForEach(rap =>
                {
                    if (!rap.IsGenerated )
                    {
                        rap.IsFirstBillableVisit = rap.IsFirstBillableVisit || patientRepository.IsFirstBillableVist(Current.AgencyId, rap.EpisodeId, rap.PatientId);
                        rap.IsOasisComplete = rap.IsOasisComplete || patientRepository.IsOasisComplete(Current.AgencyId, rap.EpisodeId, rap.PatientId);
                    }
                }
               );
            }
            return raps;
        }

        public IList<Final> AllUnProcessedFinal()
        {
            var finals = billingRepository.GetFinals(Current.AgencyId).Where(f => f.Status == (int)ScheduleStatus.ClaimCreated).ToList();
            if (finals != null)
            {
                finals.ForEach(final =>
                {
                    if (!final.IsGenerated && final.EpisodeEndDate<=DateTime.Now)
                    {
                        final.IsFirstBillableVisit = final.IsFirstBillableVisit || patientRepository.IsFirstBillableVist(Current.AgencyId, final.EpisodeId, final.PatientId);
                        final.IsOasisComplete = final.IsOasisComplete || patientRepository.IsOasisComplete(Current.AgencyId, final.EpisodeId, final.PatientId);
                        final.AreOrdersComplete = final.AreOrdersComplete || patientRepository.IsOrdersComplete(Current.AgencyId, final.EpisodeId, final.PatientId);
                        var rap = billingRepository.GetRap(Current.AgencyId, final.Id);
                        if (rap != null)
                        {
                            final.IsRapGenerated = final.IsRapGenerated || rap.IsGenerated;
                        }
                    }
                }
                    );

            }
            return finals;
        }

        public long GetNextClaimId(ClaimData claimData)
        {
            return billingRepository.AddClaimData(claimData);
        }

        public IList<ClaimViewData> Activity(Guid patientId)
        {
            var raps = billingRepository.GetRapProcessed(patientId, Current.AgencyId);
            var finals = billingRepository.GetFinalProcessed(patientId, Current.AgencyId);
            var claims = new List<ClaimViewData>();
            if (raps != null && raps.Count > 0)
            {
                raps.ForEach(rap =>
                {

                    claims.Add(
                        new ClaimViewData
                        {
                            Id = rap.Id,
                            FirstName = rap.FirstName,
                            LastName = rap.LastName,
                            MedicareNumber = rap.MedicareNumber,
                            PaymentDate = rap.PaymentDate,
                            Type = "Rap",
                            Created = rap.Created,
                            Status = rap.Status,
                            EpisodeStartDate = rap.EpisodeStartDate,
                            EpisodeEndDate = rap.EpisodeEndDate,

                        }
                        );
                });
            }
            if (finals != null && finals.Count > 0)
            {
                finals.ForEach(final =>
                {

                    claims.Add(
                        new ClaimViewData
                        {
                            Id = final.Id,
                            Type = "Final",
                            FirstName = final.FirstName,
                            LastName = final.LastName,
                            MedicareNumber = final.MedicareNumber,
                            PaymentDate = final.PaymentDate,
                            Created = final.Created,
                            Status = final.Status,
                            EpisodeStartDate = final.EpisodeStartDate,
                            EpisodeEndDate = final.EpisodeEndDate
                        }
                        );
                });
            }

            return claims;
        }

        public IList<ClaimViewData> PendingClaims()
        {
            var raps = billingRepository.GetRaps(Current.AgencyId);
            var finals = billingRepository.GetFinals(Current.AgencyId);
            var claims = new List<ClaimViewData>();
            if (raps != null && raps.Count > 0)
            {
                raps.ForEach(rap =>
                {
                    if (rap.Status == (int)ScheduleStatus.ClaimSubmitted)
                    {
                        claims.Add(
                            new ClaimViewData
                            {
                                Id = rap.Id,
                                FirstName = rap.FirstName,
                                LastName = rap.LastName,
                                MedicareNumber = rap.MedicareNumber,
                                PaymentDate = rap.PaymentDate,
                                Type = "Rap",
                                Created = rap.Created,
                                Status = rap.Status,
                                EpisodeStartDate = rap.EpisodeStartDate,
                                EpisodeEndDate = rap.EpisodeEndDate,
                            }
                        );
                    }
                });
            }
            if (finals != null && finals.Count > 0)
            {
                finals.ForEach(final =>
                {
                    if (final.Status == (int)ScheduleStatus.ClaimSubmitted)
                    {
                        claims.Add(
                            new ClaimViewData
                            {
                                Id = final.Id,
                                Type = "Final",
                                FirstName = final.FirstName,
                                LastName = final.LastName,
                                MedicareNumber = final.MedicareNumber,
                                PaymentDate = final.PaymentDate,
                                Created = final.Created,
                                Status = final.Status,
                                EpisodeStartDate = final.EpisodeStartDate,
                                EpisodeEndDate = final.EpisodeEndDate
                            }
                        );
                    }
                });
            }
            return claims;
        }

        public IList<TypeOfBill> GetAllUnProcessedBill()
        {
            IList<TypeOfBill> claims = null;
            var bill = billingRepository.AllUnProcessedBill(Current.AgencyId);
            if (bill != null)
            {
                claims = new List<TypeOfBill>();

                if (bill.Raps != null && bill.Raps.Count > 0)
                {
                    bill.Raps.ForEach(r =>
                    {
                        if (r.IsFirstBillableVisit && r.IsOasisComplete)
                        {
                            claims.Add(new TypeOfBill
                            {
                                PatientName = r.DisplayName,
                                EpisodeRange = r.EpisodeRange,
                                Type = "RAP"
                            });
                        }
                    });
                }

                if (bill.Finals != null && bill.Finals.Count > 0)
                {
                    bill.Finals.ForEach(f =>
                    {
                        if (f.IsRapGenerated && f.AreVisitsComplete && f.AreOrdersComplete)
                        {
                            claims.Add(new TypeOfBill
                            {
                                PatientName = f.DisplayName,
                                EpisodeRange = f.EpisodeRange,
                                Type = "Final"
                            });
                        }
                    });
                }
            }
            return claims;
        }

        public List<Supply> GetSupply(ScheduleEvent scheduleEvent)
        {
            var supplies = new List<Supply>();
            switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
            {
                case DisciplineTasks.OASISCStartofCare:
                case DisciplineTasks.OASISCStartofCarePT:
                case DisciplineTasks.OASISCResumptionofCare:
                case DisciplineTasks.OASISCResumptionofCarePT:
                case DisciplineTasks.OASISCResumptionofCareOT:
                case DisciplineTasks.OASISCFollowUp:
                case DisciplineTasks.OASISCFollowupPT:
                case DisciplineTasks.OASISCFollowupOT:
                case DisciplineTasks.OASISCRecertification:
                case DisciplineTasks.OASISCRecertificationPT:
                case DisciplineTasks.OASISCRecertificationOT:
                    var assessment = assessmentRepository.GetWithNoList(Current.AgencyId, scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString());
                    if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
                    {
                        supplies = assessment.Supply.ToObject<List<Supply>>();
                    }
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                    var note = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                    if (note != null && note.Supply.IsNotNullOrEmpty())
                    {
                        supplies = note.Supply.ToObject<List<Supply>>();
                    }
                    break;
            }
            return supplies;
        }

        public Rap GetRap(Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
            if (rap != null)
            {
                if (!rap.IsVerified && !rap.IsGenerated)
                {
                    var patient = patientRepository.Get(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        rap.FirstName = patient.FirstName;
                        rap.LastName = patient.LastName;
                        rap.MedicareNumber = patient.MedicareNumber;
                        rap.PatientIdNumber = patient.PatientIdNumber;
                        rap.Gender = patient.Gender;
                        rap.DOB = patient.DOB;
                        rap.StartofCareDate = patient.StartofCareDate;
                        rap.AddressLine1 = patient.AddressLine1;
                        rap.AddressLine2 = patient.AddressLine2;
                        rap.AddressCity = patient.AddressCity;
                        rap.AddressStateCode = patient.AddressStateCode;
                        rap.AddressZipCode = patient.AddressZipCode;
                        rap.AdmissionSource = patient.AdmissionSource;
                        rap.PatientStatus = patient.Status;
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                        {
                            patient.PhysicianContacts.ForEach(p => {
                                if (p.Primary) 
                                {
                                    rap.PhysicianLastName = p.LastName;
                                    rap.PhysicianFirstName = p.FirstName;
                                    rap.PhysicianNPI = p.NPI;
                                } 
                            });
                        }
                    }
                    if (patientRepository.IsFirstBillableVist(Current.AgencyId, episodeId, patientId))
                    {
                        var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId);
                        if (evnt != null && evnt.EventDate.IsValidDate())
                        {
                            rap.FirstBillableVisitDate = evnt.EventDate.ToDateTime();
                        }
                    }
                    var assessmentEvent = patientRepository.OasisCompletedForRap(Current.AgencyId, episodeId, patientId);
                    if (assessmentEvent != null)
                    {
                        var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                        var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                        if (assessment != null)
                        {
                            IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();
                            string diagnosis = "<DiagonasisCodes>";
                                diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                            diagnosis += "</DiagonasisCodes>";
                            rap.DiagonasisCode = diagnosis;
                            rap.HippsCode = assessment.HippsCode;
                            rap.ClaimKey = assessment.ClaimKey;
                        }
                    }
                }
            }
            return rap;
        }

        public Final GetFinalInfo(Guid patientId, Guid episodeId)
        {
            var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
            var final = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            if (final != null)
            {
                if (!final.IsFinalInfoVerified)
                {
                    if (rap != null && rap.IsVerified)
                    {
                        final.FirstName = rap.FirstName;
                        final.LastName = rap.LastName;
                        final.MedicareNumber = rap.MedicareNumber;
                        final.PatientIdNumber = rap.PatientIdNumber;
                        final.DiagonasisCode = rap.DiagonasisCode;
                        final.Gender = rap.Gender;
                        final.DOB = rap.DOB;
                        final.EpisodeStartDate = rap.EpisodeStartDate;
                        final.StartofCareDate = rap.StartofCareDate;
                        final.AddressLine1 = rap.AddressLine1;
                        final.AddressLine2 = rap.AddressLine2;
                        final.AddressCity = rap.AddressCity;
                        final.AddressStateCode = rap.AddressStateCode;
                        final.AddressZipCode = rap.AddressZipCode;
                        final.HippsCode = rap.HippsCode;
                        final.ClaimKey = rap.ClaimKey;
                        final.FirstBillableVisitDate = rap.FirstBillableVisitDate;
                        final.PhysicianLastName = rap.PhysicianLastName;
                        final.PhysicianFirstName = rap.PhysicianFirstName;
                        final.PhysicianNPI = rap.PhysicianNPI;
                        final.AdmissionSource = rap.AdmissionSource;
                    }
                    else if (rap == null)
                    {
                        var patient = patientRepository.Get(patientId, Current.AgencyId);
                        if (patient != null)
                        {
                            final.FirstName = patient.FirstName;
                            final.LastName = patient.LastName;
                            final.MedicareNumber = patient.MedicareNumber;
                            final.PatientIdNumber = patient.PatientIdNumber;
                            final.Gender = patient.Gender;
                            final.DOB = patient.DOB;
                            final.StartofCareDate = patient.StartofCareDate;
                            final.AddressLine1 = patient.AddressLine1;
                            final.AddressLine2 = patient.AddressLine2;
                            final.AddressCity = patient.AddressCity;
                            final.AddressStateCode = patient.AddressStateCode;
                            final.AddressZipCode = patient.AddressZipCode;

                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                            {
                                patient.PhysicianContacts.ForEach(p =>
                                {
                                    if (p.Primary)
                                    {
                                        final.PhysicianLastName = p.LastName;
                                        final.PhysicianFirstName = p.FirstName;
                                        final.PhysicianNPI = p.NPI;
                                    }
                                });
                            }
                        }

                        if (patientRepository.IsFirstBillableVist(Current.AgencyId, episodeId, patientId))
                        {
                            var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId);
                            if (evnt != null && evnt.EventDate.IsValidDate())
                            {
                                final.FirstBillableVisitDate = evnt.EventDate.ToDateTime();
                            }
                        }
                        var assessmentEvent = patientRepository.OasisCompletedForRap(Current.AgencyId, episodeId, patientId);
                        if (assessmentEvent != null)
                        {
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                            var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                            if (assessment != null)
                            {
                                IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();
                                string diagnosis = "<DiagonasisCodes>";
                                diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                diagnosis += "</DiagonasisCodes>";
                                final.DiagonasisCode = diagnosis;
                                final.HippsCode = assessment.HippsCode;
                                final.ClaimKey = assessment.ClaimKey;
                            }
                        }
                    }
                }
            }
            return final;
        }

        public object GetVisitRateInstnace(string disciplineeName , string codeOne , string codeTwo, string amount, int unit)
        {
            switch (disciplineeName){
                case "SN":
                    return new { SN = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "HHA":
                    return new { HHA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "PT":
                    return new { PT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "OT":
                    return new { OT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "ST":
                    return new { ST = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "MSW":
                    return new { MSW = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
            }
            return null;
        }

        public Dictionary<string, BillInfo> GetInsuranceUnit(int insurnaceId)
        {
            var unit = new Dictionary<string, BillInfo>();
            unit.Add("SN", new BillInfo { Discipline = "SN", Amount = "150.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0154" });
            unit.Add("HHA", new BillInfo { Discipline = "HHA", Amount = "80.00", Unit = 4, CodeOne = "0571", CodeTwo = "G0156" });
            unit.Add("PT", new BillInfo { Discipline = "PT", Amount = "200.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0151" });
            unit.Add("OT", new BillInfo { Discipline = "OT", Amount = "200.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0152" });
            unit.Add("ST", new BillInfo { Discipline = "ST", Amount = "200.00", Unit = 4, CodeOne = "0440", CodeTwo = "G0153" });
            unit.Add("MSW", new BillInfo { Discipline = "MSW", Amount = "200.00", Unit = 3, CodeOne = "0561", CodeTwo = "G0155" });
            return unit;
        }

        public List<Object> GetSupplyRate()
        {
            List<object> supplies = new List<object>();
            supplies.Add(new { S = "14.39" });
            supplies.Add(new { T = "51.96" });
            supplies.Add(new { U = "142.38" });
            supplies.Add(new { V = "211.69" });
            supplies.Add(new { W = "326.43" });
            supplies.Add(new { X = "561.42" });
            return supplies;
        }

        public bool UpdateRapStatus(List<Guid> rapToGenerate, Guid agencyId, string StatusType)
        {
            bool result = false;
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(r =>
                {
                    var rap = billingRepository.GetRap(agencyId, r);
                    if (rap != null)
                    {
                        if (StatusType == "Submit")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimSubmitted;
                            rap.IsGenerated = true;
                            rap.IsVerified = true;
                        }
                        else if (StatusType == "Cancelled")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimCancelledClaim;
                        }
                        else if (StatusType == "Rejected")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimRejected;
                        }
                        else if (StatusType == "Accepted")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimAccepted;
                        }
                        else if (StatusType == "PaymentPending")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimPaymentPending;
                        }
                        else if (StatusType == "Error")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimWithErrors;
                        }
                        else if (StatusType == "Paid")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimPaidClaim;
                        }
                        else if (StatusType == "ReOpen")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimReOpen;
                            rap.IsVerified = false;

                        }
                         if (billingRepository.UpdateRapStatus(rap))
                        {
                            var final = billingRepository.GetFinal(agencyId, rap.PatientId, rap.EpisodeId);
                            if (final != null)
                            {
                                if (StatusType == "Submit")
                                {
                                    final.IsRapGenerated = true;
                                }
                                else if (StatusType == "ReOpen")
                                {
                                    final.IsRapGenerated = false;
                                }
                            }
                             billingRepository.UpdateFinalStatus(final);
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateFinalStatus(List<Guid> finalToGenerate, Guid agencyId, string StatusType)
        {
            bool result = false;
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(f =>
                {
                    var final =billingRepository.GetFinal(agencyId, f);
                    if (final != null)
                    {
                        if (StatusType == "Submit")
                        {
                            final.Status = (int)ScheduleStatus.ClaimSubmitted;
                            final.IsGenerated = true;
                        }
                        else if (StatusType == "Cancelled")
                        {
                            final.Status = (int)ScheduleStatus.ClaimCancelledClaim;
                        }
                        else if (StatusType == "Rejected")
                        {
                            final.Status = (int)ScheduleStatus.ClaimRejected;
                        }
                        else if (StatusType == "Accepted")
                        {
                            final.Status = (int)ScheduleStatus.ClaimAccepted;
                        }
                        else if (StatusType == "PaymentPending")
                        {
                            final.Status = (int)ScheduleStatus.ClaimPaymentPending;
                        }
                        else if (StatusType == "Error")
                        {
                            final.Status = (int)ScheduleStatus.ClaimWithErrors;
                        }
                        else if (StatusType == "Paid")
                        {
                            final.Status = (int)ScheduleStatus.ClaimPaidClaim;
                        }
                        else if (StatusType == "ReOpen")
                        {
                            final.Status = (int)ScheduleStatus.ClaimReOpen;
                            final.IsFinalInfoVerified = false;
                            final.IsSupplyVerified = false;
                            final.IsVisitVerified = false;
                            final.IsGenerated = false;

                        }
                        billingRepository.UpdateFinalStatus(final);
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool FinalComplete(Guid id)
        {
            var final = billingRepository.GetFinal(Current.AgencyId, id);
            bool result = false;
            if (final != null)
            {
              result=  billingRepository.UpdateFinalStatus(final);
            }
            return result;
        }

        #endregion
    }
}
