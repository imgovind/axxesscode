﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Domain.Billing;
    using Axxess.LookUp.Domain;
   
    

    public interface IBillingService
    {
        string Generate(List<Guid> rapToGenerate, List<Guid> finalToGenerate);
        bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit);
        bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, FormCollection formCollection, List<Guid> SupplyId);
        Bill AllUnProcessedBill();
        IList<Rap> AllUnProcessedRap();
        IList<Final> AllUnProcessedFinal();
        IList<ClaimViewData> Activity(Guid patientId);
        IList<ClaimViewData> PendingClaims();
        IList<TypeOfBill> GetAllUnProcessedBill();
        List<Supply> GetSupply(ScheduleEvent scheduleEvent);
        Rap GetRap(Guid patientId, Guid episodeId);
        Final GetFinalInfo(Guid patientId, Guid episodeId);
        Dictionary<string, BillInfo> GetInsuranceUnit(int insurnaceId);
        bool UpdateRapStatus(List<Guid> rapToGenerate, Guid agencyId , string StatusType);
        bool UpdateFinalStatus(List<Guid> finalToGenerate, Guid agencyId, string StatusType);
        bool FinalComplete(Guid id);
    }
}
