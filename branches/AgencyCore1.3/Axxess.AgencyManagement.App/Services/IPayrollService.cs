﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;

    public interface IPayrollService
    {
        List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate);
        List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate);
    }
}
