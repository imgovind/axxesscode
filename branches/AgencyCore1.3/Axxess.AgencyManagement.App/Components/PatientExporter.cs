﻿namespace Axxess.AgencyManagement.App.Components
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PatientExporter : BaseExporter
    {
        private IList<Patient> patients;
        public PatientExporter(IList<Patient> patients)
            : base()
        {
            this.patients = patients;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Patients");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Id");
            headerRow.CreateCell(1).SetCellValue("Medicare Number");
            headerRow.CreateCell(2).SetCellValue("Last Name");
            headerRow.CreateCell(3).SetCellValue("First Name");
            headerRow.CreateCell(4).SetCellValue("Middle Initial");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Mobile Phone");
            headerRow.CreateCell(7).SetCellValue("Address 1");
            headerRow.CreateCell(8).SetCellValue("Address 2");
            headerRow.CreateCell(9).SetCellValue("City");
            headerRow.CreateCell(10).SetCellValue("State");
            headerRow.CreateCell(11).SetCellValue("Zip Code");
            headerRow.CreateCell(12).SetCellValue("Date of Birth");
            headerRow.CreateCell(13).SetCellValue("Gender");
            headerRow.CreateCell(14).SetCellValue("Email Address");

            headerRow.RowStyle = headerStyle;

            if (this.patients.Count > 0)
            {
                int i = 1;
                this.patients.ForEach(p =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(p.MedicareNumber);
                    dataRow.CreateCell(2).SetCellValue(p.LastName.ToTitleCase());
                    dataRow.CreateCell(3).SetCellValue(p.FirstName.ToTitleCase());
                    dataRow.CreateCell(4).SetCellValue(p.MiddleInitial.ToTitleCase());
                    dataRow.CreateCell(5).SetCellValue(p.PhoneHome.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(p.PhoneMobile.ToPhone());
                    dataRow.CreateCell(7).SetCellValue(p.AddressLine1.ToTitleCase());
                    dataRow.CreateCell(8).SetCellValue(p.AddressLine2.ToTitleCase());
                    dataRow.CreateCell(9).SetCellValue(p.AddressCity.ToTitleCase());
                    dataRow.CreateCell(10).SetCellValue(p.AddressStateCode);
                    dataRow.CreateCell(11).SetCellValue(p.AddressZipCode);
                    dataRow.CreateCell(12).SetCellValue(p.DOB.ToShortDateString());
                    dataRow.CreateCell(13).SetCellValue(p.Gender);
                    dataRow.CreateCell(14).SetCellValue(p.EmailAddress);
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
            sheet.AutoSizeColumn(14);
        }
    }
}
