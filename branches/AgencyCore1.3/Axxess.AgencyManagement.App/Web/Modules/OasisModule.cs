﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class OasisModule : Module
    {
        public override string Name
        {
            get { return "Oasis"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "485Print",
                "485/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "View485", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "485Medication",
                "485/Medication/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "View485Medication", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "SocBlank",
               "StartofCare/View/Blank",
               new { controller = this.Name, action = "SocBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "SocPrint",
                "StartofCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SocPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "RocBlank",
               "ResumptionOfCare/View/Blank",
               new { controller = this.Name, action = "RocBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "RocPrint",
                "ResumptionOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "RocPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "RecertBlank",
               "Recertification/View/Blank",
               new { controller = this.Name, action = "RecertBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "RecertPrint",
                "Recertification/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "RecertPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferNotDischargeBlank",
                "TransferNotDischarge/View/Blank",
                new { controller = this.Name, action = "TransferNotDischargeBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "TransferNotDischargePrint",
                "TransferNotDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferNotDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "Blank",
                "TransferDischarge/View/Blank",
                new { controller = this.Name, action = "TransferDischargeBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "TransferDischargePrint",
                "TransferDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargeBlank",
                "Discharge/View/Blank",
                new { controller = this.Name, action = "DischargeBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "DischargePrint",
                "Discharge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DeathBlank",
                "Death/View/Blank",
                new { controller = this.Name, action = "DeathBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "DeathPrint",
                "Death/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DeathPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "FollowUpBlank",
                "Followup/View/Blank",
                new { controller = this.Name, action = "FollowUpBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "FollowUpPrint",
                "Followup/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "FollowUpPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
        "Validate",
        "Validate/{Id}/{patientId}/{episodeId}/{assessmentType}",
         new { controller = this.Name, action = "Validate", Id = new IsGuid(), patientId = new IsGuid(), episodeId = new IsGuid(), assessmentType = UrlParameter.Optional });
        }
    }
}
