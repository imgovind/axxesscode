﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.App.Logging;

    public class AxxessApplication : HttpApplication
    {
        public AxxessApplication()
        {
            this.AuthenticateRequest += new EventHandler(AxxessApplication_AuthenticateRequest);
        }

        protected void AxxessApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Context.User != null)
            {
                string username = HttpContext.Current.User.Identity.Name;
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                AxxessPrincipal principal = membershipService.Get(username);

                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Exception(exception);
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (context.Request.IsAuthenticated && custom == "Agency")
            {
                return Current.AgencyId.ToString();
            }
            else
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }
    }
}
