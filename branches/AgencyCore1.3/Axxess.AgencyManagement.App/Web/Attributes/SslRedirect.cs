﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.IO.Compression;
    using System.Web.Security;

    using Axxess.Core.Infrastructure;

    public class SslRedirect : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (AppSettings.UseSSL)
            {
                if (!filterContext.HttpContext.Request.IsSecureConnection)
                {
                    filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.Url.ToString().Replace("http:", "https:"));
                    filterContext.Result.ExecuteResult(filterContext);
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }

}
