﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public class PayrollDetail
    {
        public string Name { get; set; }
        public List<UserVisit> Visits { get; set; }
    }
}
