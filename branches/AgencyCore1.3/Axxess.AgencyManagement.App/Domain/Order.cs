﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Enums;

    [KnownType(typeof(Order))]
    public class Order
    {
        public Guid Id { get; set; }
        public string PatientName { get; set; }
        public string PhysicianName { get; set; }
        public string Text { get; set; }
        public OrderType Type { get; set; }
        public long Number { get; set; }
        public string SentDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ReceivedDate { get; set; }
        public string CreatedDate { get; set; }
        public string PrintUrl { get; set; }
    }
}
