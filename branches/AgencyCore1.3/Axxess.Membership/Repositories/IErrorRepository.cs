﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Domain;

    public interface IErrorRepository
    {
        //PagedResult<Error> GetHostErrors(byte hostId, int start, int max);
        //PagedResult<Error> GetErrorsByDate(DateTime createdDate, int start, int max);
        //PagedResult<Error> GetErrorsByDateRange(DateTime startDate, DateTime endDate, int start, int max);

        bool Add(Error error);
        IList<Error> GetSome();
        //bool Upadte(int errorId, byte hostId, string type, string importance, short impressions, XElement details);
        //bool Delete(int errorId);
    }
}
