﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using SubSonic.Repository;

    public class ErrorRepository : IErrorRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ErrorRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IErrorRepository Members

        public bool Add(Error error)
        {
            if (error != null)
            {
                error.Details = error.Detail.ToXml();
                database.Add<Error>(error);
                return true;
            }
            return false;
        }

        public IList<Error> GetSome()
        {
            IList<Error> errorList = new List<Error>();
            var errors = database.GetPaged<Error>(e => e.Server == Environment.MachineName, "Created desc", 0, 50);
            errors.ForEach(e =>
            {
                errorList.Add(e);
            });
            return errorList;
        }

        #endregion
    }
}
