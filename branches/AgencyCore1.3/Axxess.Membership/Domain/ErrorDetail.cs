﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Text;

    public class ErrorDetail
    {
        public int Impression { get; set; }
        public int LineNumber { get; set; }
        public string FileName { get; set; }
        public string MethodName { get; set; }
        public string ExceptionType { get; set; }
        public string InnerError { get; set; }

        public override string ToString()
        {
            var text = new StringBuilder();
            text.AppendFormat("{0} ", LineNumber);
            text.AppendFormat("{0} ", FileName);
            text.AppendFormat("{0} ", MethodName);
            text.AppendFormat("{0} ", ExceptionType);
            return text.ToString();
        }
    }
}
