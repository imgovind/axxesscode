﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IAuthenticationService : IService
    {
        [OperationContract]
        bool ValidateUser(string username, string password);
        [OperationContract]
        void LogOff(string username);
    }
}
