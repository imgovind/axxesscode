﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenForum.Core.Models;
using SubSonic.Repository;

using Axxess.Core.Extension;

namespace OpenForum.Core.DataAccess
{
    public class DefaultPostRepository : IPostRepository
    {
        private readonly SimpleRepository database;

        public DefaultPostRepository()
        {
            this.database = new SimpleRepository("AxxessForumConnectionString", SimpleRepositoryOptions.None);
        }

        public IQueryable<Post> Find()
        {
            var query = from p in database.All<Post>()
                   orderby p.LastPostDate descending
                   select p;
            return query;
        }

        public List<Reply> FindReplies(int postId)
        {
            return database.Find<Reply>(r => r.PostId == postId).ToList();
        }

        public IQueryable<Post> Search(string query)
        {
            List<int> searchResults = new List<int>();
            return Find().Where(x => searchResults.Contains(x.Id));
        }

        public Post FindById(int id)
        {
            return database.Single<Post>(p => p.Id == id);
        }

        public void SubmitPost(Post post)
        {
            if (post.Id == 0)
            {
                database.Add<Post>(post);
            }
            else
            {
                database.Update<Post>(post);
            }
        }

        public void SubmitReply(Reply reply)
        {
            if (reply.Id == 0)
            {
                database.Add<Reply>(reply);
            }
            else
            {
                database.Update<Reply>(reply);
            }
        }

        public Reply FindReplyById(int replyId)
        {
            return database.Single<Reply>(r => r.Id == replyId);
        }

    }
}
