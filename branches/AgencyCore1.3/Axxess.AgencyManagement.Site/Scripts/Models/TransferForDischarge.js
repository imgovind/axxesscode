﻿var TransferForDischarge = {
    _TransferForDischargeId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return TransferForDischarge._patientId;
    },
    SetId: function(patientId) {
        TransferForDischarge._patientId = patientId;
    },
    GetTransferForDischargeId: function() {
        return TransferForDischarge._TransferForDischargeId;
    },
    SetTransferForDischargeId: function(TransferForDischargeId) {
        TransferForDischarge._TransferForDischargeId = TransferForDischargeId;
    },
    GetSOCEpisodeId: function() {
        return TransferForDischarge._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        TransferForDischarge._EpisodeId = EpisodeId;
    },
    Init: function() {

        $("input[name=TransferInPatientDischarged_M1040InfluenzaVaccine]").click(function() {
            if ($(this).val() == "01" || $(this).val() == "NA") {

                $("#transfer_M1045").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1045InfluenzaVaccineNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == "00") {
                $("#transfer_M1045").unblock();
            }
        });


        $("input[name=TransferInPatientDischarged_M1050PneumococcalVaccine]").click(function() {
            if ($(this).val() == 1) {

                $("#transfer_M1055").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1055PPVNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == 0) {
                $("#transfer_M1055").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M1500HeartFailureSymptons]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02" || $(this).val() == "NA") {

                $("#transfer_M1510").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1510HeartFailureFollowup ]").attr('checked', false);

            }
            else if ($(this).val() == "01") {
                $("#transfer_M1510").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M2300EmergentCare]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "UK") {

                $("#transfer_M2310").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2310ReasonForEmergentCare ]").attr('checked', false);

            }
            else if ($(this).val() == "01" || $(this).val() == "02") {
                $("#transfer_M2310").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M2410TypeOfInpatientFacility]").click(function() {
            if ($(this).val() == 1) {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2430").unblock();
                $("input[name=TransferInPatientDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);
            }
            else if ($(this).val() == "02" || $(this).val() == "04") {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2430ReasonForHospitalization]").attr('checked', false);
                $("input[name=TransferInPatientDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);

            }
            else if ($(this).val() == "03") {
                $("#transfer_M2440").unblock();
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2430ReasonForHospitalization]").attr('checked', false);

            }

        });

    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='TransferInPatientDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientDischarged_Action']").val('Edit');
                        Oasis.NextTab("#editTransferInPatientDischargedTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='TransferInPatientDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientDischarged_Action']").val('Edit');
                    }
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, formType) {
        var form = control.closest("form");
        form.validate();
        TransferForDischarge.HandlerHelper(form, control);
    },
    Validate: function() {
        OasisValidation.Validate(TransferForDischarge._TransferForDischargeId, "TransferInPatientDischarged");
    }
    ,
    TransferInPatientDischarged: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#editTransferdischarge").clearForm();
        $("#editTransferdischarge div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#TransferInPatientDischargedTitle").text("New Transfer to an Inpatient Facility  - " + patientName);
                $("input[name='TransferInPatientDischarged_Id']").val("");
                $("input[name='TransferInPatientDischarged_PatientGuid']").val(id);
                $("input[name='TransferInPatientDischarged_Action']").val('New');
                $("#TransferInPatientDischarged_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#TransferInPatientDischarged_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#TransferInPatientDischarged_M0040FirstName").val(patient.FirstName);
                $("#TransferInPatientDischarged_M0040MI").val(patient.MiddleInitial);
                $("#TransferInPatientDischarged_M0040LastName").val(patient.LastName);
                $("#TransferInPatientDischarged_M0050PatientState").val(patient.AddressStateCode);
                $("#TransferInPatientDischarged_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#TransferInPatientDischarged_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#TransferInPatientDischarged_M0064PatientSSN").val(patient.SSN);
                $("#TransferInPatientDischarged_M0065PatientMedicaidNumber").val(patient.MedicaidNumber);
                $("#TransferInPatientDischarged_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=TransferInPatientDischarged_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='TransferInPatientDischarged_M0100AssessmentType'][value='07']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=TransferInPatientDischarged_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=TransferInPatientDischarged_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=TransferInPatientDischarged_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=TransferInPatientDischarged_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=TransferInPatientDischarged_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=TransferInPatientDischarged_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=TransferInPatientDischarged_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#TransferInPatientDischarged_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                    }
                }
            }
        });
    }
    ,
    EditTransferDischarge: function(id, patientId, assessmentType) {
        // var patientId = Oasis.GetId();
        $("#editTransferdischarge").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {
            var patient = eval(result);
            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#TransferInPatientDischargedTitle").text("Edit Transfer to an Inpatient Facility - patient discharged from agency - " + firstName + " " + lastName);
            TransferForDischarge.SetTransferForDischargeId(id);
            $("input[name='TransferInPatientDischarged_Id']").val(id);
            $("input[name='TransferInPatientDischarged_Action']").val('Edit');
            $("input[name='TransferInPatientDischarged_PatientGuid']").val(patientId);
            $("#TransferInPatientDischarged_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#TransferInPatientDischarged_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#TransferInPatientDischarged_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=TransferInPatientDischarged_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#TransferInPatientDischarged_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $("#TransferInPatientDischarged_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#TransferInPatientDischarged_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#TransferInPatientDischarged_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#TransferInPatientDischarged_M0040FirstName").val(firstName);
            $("#TransferInPatientDischarged_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#TransferInPatientDischarged_M0040LastName").val(lastName);
            $("#TransferInPatientDischarged_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#TransferInPatientDischarged_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=TransferInPatientDischarged_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#TransferInPatientDischarged_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $("#TransferInPatientDischarged_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=TransferInPatientDischarged_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#TransferInPatientDischarged_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $("#TransferInPatientDischarged_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#TransferInPatientDischarged_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $("#TransferInPatientDischarged_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }
            $("#TransferInPatientDischarged_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=TransferInPatientDischarged_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#TransferInPatientDischarged_M0090AssessmentCompletedDate").val(result["M0090AssessmentCompletedDate"] != null && result["M0090AssessmentCompletedDate"] != undefined ? result["M0090AssessmentCompletedDate"].Answer : "");
            $("input[name='TransferInPatientDischarged_M0100AssessmentType'][value='07']").attr('checked', true);

            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#TransferInPatientDischarged_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#TransferInPatientDischarged_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }
            $("#TransferInPatientDischarged_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=TransferInPatientDischarged_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=TransferInPatientDischarged_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#TransferInPatientDischarged_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=TransferInPatientDischarged_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=TransferInPatientDischarged_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);

            $('input[name=TransferInPatientDischarged_M1040InfluenzaVaccine][value=' + (result["M1040InfluenzaVaccine"] != null && result["M1040InfluenzaVaccine"] != undefined ? result["M1040InfluenzaVaccine"].Answer : "") + ']').attr('checked', true);


            if (result["M1040InfluenzaVaccine"] != null && (result["M1040InfluenzaVaccine"].Answer == "01" || result["M1040InfluenzaVaccine"].Answer == "NA")) {
                $("#transfer_M1045").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=TransferInPatientDischarged_M1045InfluenzaVaccineNotReceivedReason][value=' + (result["M1045InfluenzaVaccineNotReceivedReason"] != null && result["M1045InfluenzaVaccineNotReceivedReason"] != undefined ? result["M1045InfluenzaVaccineNotReceivedReason"].Answer : "") + ']').attr('checked', true);
                $("#transfer_M1045").unblock();
            }

            $('input[name=TransferInPatientDischarged_M1050PneumococcalVaccine][value=' + (result["M1050PneumococcalVaccine"] != null && result["M1050PneumococcalVaccine"] != undefined ? result["M1050PneumococcalVaccine"].Answer : "") + ']').attr('checked', true);
            if (result["M1050PneumococcalVaccine"] != null && result["M1050PneumococcalVaccine"].Answer == 1) {
                $("#transfer_M1055").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else if ($("input[name=TransferInPatientDischarged_M1040InfluenzaVaccine]").val() == 0) {
                $('input[name=TransferInPatientDischarged_M1055PPVNotReceivedReason][value=' + (result["M1055PPVNotReceivedReason"] != null && result["M1055PPVNotReceivedReason"] != undefined ? result["M1055PPVNotReceivedReason"].Answer : "") + ']').attr('checked', true);
                $("#transfer_M1055").unblock();

            }
            var heartFailureSymptons = "";
            if (result["M1500HeartFailureSymptons"] != null && result["M1500HeartFailureSymptons"] != undefined) {
                heartFailureSymptons = result["M1500HeartFailureSymptons"].Answer
            }
            $('input[name=TransferInPatientDischarged_M1500HeartFailureSymptons][value=' + (heartFailureSymptons != "" ? heartFailureSymptons : "") + ']').attr('checked', true);

            if (heartFailureSymptons == "00" || heartFailureSymptons == "02" || heartFailureSymptons == "NA") {
                $("#transfer_M1510").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {
                $("#transfer_M1510").unblock();
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupNoAction][value=' + (result["M1510HeartFailureTransferInPatientDischargedNoAction"] != null && result["M1510HeartFailureTransferInPatientDischargedNoAction"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedNoAction"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupPhysicianCon][value=' + (result["M1510HeartFailureTransferInPatientDischargedPhysicianCon"] != null && result["M1510HeartFailureTransferInPatientDischargedPhysicianCon"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedPhysicianCon"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupAdvisedEmg][value=' + (result["M1510HeartFailureTransferInPatientDischargedAdvisedEmg"] != null && result["M1510HeartFailureTransferInPatientDischargedAdvisedEmg"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedAdvisedEmg"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupParameters][value=' + (result["M1510HeartFailureTransferInPatientDischargedParameters"] != null && result["M1510HeartFailureTransferInPatientDischargedParameters"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedParameters"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupInterventions][value=' + (result["M1510HeartFailureTransferInPatientDischargedInterventions"] != null && result["M1510HeartFailureTransferInPatientDischargedInterventions"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedInterventions"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupChange][value=' + (result["M1510HeartFailureTransferInPatientDischargedChange"] != null && result["M1510HeartFailureTransferInPatientDischargedChange"] != undefined ? result["M1510HeartFailureTransferInPatientDischargedChange"].Answer : "") + ']').attr('checked', true);

            }

            $('input[name=TransferInPatientDischarged_M2004MedicationIntervention][value=' + (result["M2004MedicationIntervention"] != null && result["M2004MedicationIntervention"] != undefined ? result["M2004MedicationIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention][value=' + (result["M2015PatientOrCaregiverDrugEducationIntervention"] != null && result["M2015PatientOrCaregiverDrugEducationIntervention"] != undefined ? result["M2015PatientOrCaregiverDrugEducationIntervention"].Answer : "") + ']').attr('checked', true);

            var emergentCare = "";
            if (result["M2300EmergentCare"] != null && result["M2300EmergentCare"] != undefined) {
                emergentCare = result["M2300EmergentCare"].Answer
            }
            $('input[name=TransferInPatientDischarged_M2300EmergentCare][value=' + (emergentCare != "" ? emergentCare : "") + ']').attr('checked', true);

            if (emergentCare == "00" || emergentCare == "UK") {
                $("#transfer_M2310").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {
                $("#transfer_M2310").unblock();
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareMed][value=' + (result["M2310ReasonForEmergentCareMed"] != null && result["M2310ReasonForEmergentCareMed"] != undefined ? result["M2310ReasonForEmergentCareMed"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareFall][value=' + (result["M2310ReasonForEmergentCareFall"] != null && result["M2310ReasonForEmergentCareFall"] != undefined ? result["M2310ReasonForEmergentCareFall"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareResInf][value=' + (result["M2310ReasonForEmergentCareResInf"] != null && result["M2310ReasonForEmergentCareResInf"] != undefined ? result["M2310ReasonForEmergentCareResInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareOtherResInf][value=' + (result["M2310ReasonForEmergentCareOtherResInf"] != null && result["M2310ReasonForEmergentCareOtherResInf"] != undefined ? result["M2310ReasonForEmergentCareOtherResInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareHeartFail][value=' + (result["M2310ReasonForEmergentCareHeartFail"] != null && result["M2310ReasonForEmergentCareHeartFail"] != undefined ? result["M2310ReasonForEmergentCareHeartFail"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareCardiac][value=' + (result["M2310ReasonForEmergentCareCardiac"] != null && result["M2310ReasonForEmergentCareCardiac"] != undefined ? result["M2310ReasonForEmergentCareCardiac"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareMyocardial][value=' + (result["M2310ReasonForEmergentCareMyocardial"] != null && result["M2310ReasonForEmergentCareMyocardial"] != undefined ? result["M2310ReasonForEmergentCareMyocardial"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareHeartDisease][value=' + (result["M2310ReasonForEmergentCareHeartDisease"] != null && result["M2310ReasonForEmergentCareHeartDisease"] != undefined ? result["M2310ReasonForEmergentCareHeartDisease"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareStroke][value=' + (result["M2310ReasonForEmergentCareStroke"] != null && result["M2310ReasonForEmergentCareStroke"] != undefined ? result["M2310ReasonForEmergentCareStroke"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareHypo][value=' + (result["M2310ReasonForEmergentCareHypo"] != null && result["M2310ReasonForEmergentCareHypo"] != undefined ? result["M2310ReasonForEmergentCareHypo"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareGI][value=' + (result["M2310ReasonForEmergentCareGI"] != null && result["M2310ReasonForEmergentCareGI"] != undefined ? result["M2310ReasonForEmergentCareGI"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareDehMal][value=' + (result["M2310ReasonForEmergentCareDehMal"] != null && result["M2310ReasonForEmergentCareDehMal"] != undefined ? result["M2310ReasonForEmergentCareDehMal"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareUrinaryInf][value=' + (result["M2310ReasonForEmergentCareUrinaryInf"] != null && result["M2310ReasonForEmergentCareUrinaryInf"] != undefined ? result["M2310ReasonForEmergentCareUrinaryInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareIV][value=' + (result["M2310ReasonForEmergentCareIV"] != null && result["M2310ReasonForEmergentCareIV"] != undefined ? result["M2310ReasonForEmergentCareIV"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareWoundInf][value=' + (result["M2310ReasonForEmergentCareWoundInf"] != null && result["M2310ReasonForEmergentCareWoundInf"] != undefined ? result["M2310ReasonForEmergentCareWoundInf"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareUncontrolledPain][value=' + (result["M2310ReasonForEmergentCareUncontrolledPain"] != null && result["M2310ReasonForEmergentCareUncontrolledPain"] != undefined ? result["M2310ReasonForEmergentCareUncontrolledPain"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareMental][value=' + (result["M2310ReasonForEmergentCareMental"] != null && result["M2310ReasonForEmergentCareMental"] != undefined ? result["M2310ReasonForEmergentCareMental"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareDVT][value=' + (result["M2310ReasonForEmergentCareDVT"] != null && result["M2310ReasonForEmergentCareDVT"] != undefined ? result["M2310ReasonForEmergentCareDVT"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareOther][value=' + (result["M2310ReasonForEmergentCareOther"] != null && result["M2310ReasonForEmergentCareOther"] != undefined ? result["M2310ReasonForEmergentCareOther"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2310ReasonForEmergentCareUK][value=' + (result["M2310ReasonForEmergentCareUK"] != null && result["M2310ReasonForEmergentCareUK"] != undefined ? result["M2310ReasonForEmergentCareUK"].Answer : "") + ']').attr('checked', true);

            }

            $('input[name=TransferInPatientDischarged_M2400DiabeticFootCare][value=' + (result["M2400DiabeticFootCare"] != null && result["M2400DiabeticFootCare"] != undefined ? result["M2400DiabeticFootCare"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2400FallsPreventionInterventions][value=' + (result["M2400FallsPreventionInterventions"] != null && result["M2400FallsPreventionInterventions"] != undefined ? result["M2400FallsPreventionInterventions"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2400DepressionIntervention][value=' + (result["M2400DepressionIntervention"] != null && result["M2400DepressionIntervention"] != undefined ? result["M2400DepressionIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2400PainIntervention][value=' + (result["M2400PainIntervention"] != null && result["M2400PainIntervention"] != undefined ? result["M2400PainIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2400PressureUlcerIntervention][value=' + (result["M2400PressureUlcerIntervention"] != null && result["M2400PressureUlcerIntervention"] != undefined ? result["M2400PressureUlcerIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=TransferInPatientDischarged_M2400PressureUlcerTreatment][value=' + (result["M2400PressureUlcerTreatment"] != null && result["M2400PressureUlcerTreatment"] != undefined ? result["M2400PressureUlcerTreatment"].Answer : "") + ']').attr('checked', true);

            var typeOfInpatientFacility = "";
            if (result["M2410TypeOfInpatientFacility"] != null && result["M2410TypeOfInpatientFacility"] != undefined) {
                typeOfInpatientFacility = result["M2410TypeOfInpatientFacility"].Answer
            }
            $('input[name=TransferInPatientDischarged_M2410TypeOfInpatientFacility][value=' + (typeOfInpatientFacility != "" ? typeOfInpatientFacility : "") + ']').attr('checked', true);


            if (typeOfInpatientFacility == "01") {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("#transfer_M2430").unblock();
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationMed][value=' + (result["M2430ReasonForHospitalizationMed"] != null && result["M2430ReasonForHospitalizationMed"] != undefined ? result["M2430ReasonForHospitalizationMed"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationFall][value=' + (result["M2430ReasonForHospitalizationFall"] != null && result["M2430ReasonForHospitalizationFall"] != undefined ? result["M2430ReasonForHospitalizationFall"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationInfection][value=' + (result["M2430ReasonForHospitalizationInfection"] != null && result["M2430ReasonForHospitalizationInfection"] != undefined ? result["M2430ReasonForHospitalizationInfection"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationOtherRP][value=' + (result["M2430ReasonForHospitalizationOtherRP"] != null && result["M2430ReasonForHospitalizationOtherRP"] != undefined ? result["M2430ReasonForHospitalizationOtherRP"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationHeartFail][value=' + (result["M2430ReasonForHospitalizationHeartFail"] != null && result["M2430ReasonForHospitalizationHeartFail"] != undefined ? result["M2430ReasonForHospitalizationHeartFail"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationCardiac][value=' + (result["M2430ReasonForHospitalizationCardiac"] != null && result["M2430ReasonForHospitalizationCardiac"] != undefined ? result["M2430ReasonForHospitalizationCardiac"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationMyocardial][value=' + (result["M2430ReasonForHospitalizationMyocardial"] != null && result["M2430ReasonForHospitalizationMyocardial"] != undefined ? result["M2430ReasonForHospitalizationMyocardial"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationHeartDisease][value=' + (result["M2430ReasonForHospitalizationHeartDisease"] != null && result["M2430ReasonForHospitalizationHeartDisease"] != undefined ? result["M2430ReasonForHospitalizationHeartDisease"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationStroke][value=' + (result["M2430ReasonForHospitalizationStroke"] != null && result["M2430ReasonForHospitalizationStroke"] != undefined ? result["M2430ReasonForHospitalizationStroke"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationHypo][value=' + (result["M2430ReasonForHospitalizationHypo"] != null && result["M2430ReasonForHospitalizationHypo"] != undefined ? result["M2430ReasonForHospitalizationHypo"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationGI][value=' + (result["M2430ReasonForHospitalizationGI"] != null && result["M2430ReasonForHospitalizationGI"] != undefined ? result["M2430ReasonForHospitalizationGI"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationDehMal][value=' + (result["M2430ReasonForHospitalizationDehMal"] != null && result["M2430ReasonForHospitalizationDehMal"] != undefined ? result["M2430ReasonForHospitalizationDehMal"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationUrinaryInf][value=' + (result["M2430ReasonForHospitalizationUrinaryInf"] != null && result["M2430ReasonForHospitalizationUrinaryInf"] != undefined ? result["M2430ReasonForHospitalizationUrinaryInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationIV][value=' + (result["M2430ReasonForHospitalizationIV"] != null && result["M2430ReasonForHospitalizationIV"] != undefined ? result["M2430ReasonForHospitalizationIV"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationWoundInf][value=' + (result["M2430ReasonForHospitalizationWoundInf"] != null && result["M2430ReasonForHospitalizationWoundInf"] != undefined ? result["M2430ReasonForHospitalizationWoundInf"].Answer : "") + ']').attr('checked', true);

                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationUncontrolledPain][value=' + (result["M2430ReasonForHospitalizationUncontrolledPain"] != null && result["M2430ReasonForHospitalizationUncontrolledPain"] != undefined ? result["M2430ReasonForHospitalizationUncontrolledPain"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationMental][value=' + (result["M2430ReasonForHospitalizationMental"] != null && result["M2430ReasonForHospitalizationMental"] != undefined ? result["M2430ReasonForHospitalizationMental"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationDVT][value=' + (result["M2430ReasonForHospitalizationDVT"] != null && result["M2430ReasonForHospitalizationDVT"] != undefined ? result["M2430ReasonForHospitalizationDVT"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationScheduled][value=' + (result["M2430ReasonForHospitalizationScheduled"] != null && result["M2430ReasonForHospitalizationScheduled"] != undefined ? result["M2430ReasonForHospitalizationScheduled"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationOther][value=' + (result["M2430ReasonForHospitalizationOther"] != null && result["M2430ReasonForHospitalizationOther"] != undefined ? result["M2430ReasonForHospitalizationOther"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2430ReasonForHospitalizationUK][value=' + (result["M2430ReasonForHospitalizationUK"] != null && result["M2430ReasonForHospitalizationUK"] != undefined ? result["M2430ReasonForHospitalizationUK"].Answer : "") + ']').attr('checked', true);


            }
            else if (typeOfInpatientFacility == "02" || typeOfInpatientFacility == "04") {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else if (typeOfInpatientFacility == "03") {
                $("#transfer_M2440").unblock();
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2440").unblock();
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedTherapy][value=' + (result["M2440ReasonPatientAdmittedTherapy"] != null && result["M2440ReasonPatientAdmittedTherapy"] != undefined ? result["M2440ReasonPatientAdmittedTherapy"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedRespite][value=' + (result["M2440ReasonPatientAdmittedRespite"] != null && result["M2440ReasonPatientAdmittedRespite"] != undefined ? result["M2440ReasonPatientAdmittedRespite"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedHospice][value=' + (result["M2440ReasonPatientAdmittedHospice"] != null && result["M2440ReasonPatientAdmittedHospice"] != undefined ? result["M2440ReasonPatientAdmittedHospice"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedPermanent][value=' + (result["M2440ReasonPatientAdmittedPermanent"] != null && result["M2440ReasonPatientAdmittedPermanent"] != undefined ? result["M2440ReasonPatientAdmittedPermanent"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedUnsafe][value=' + (result["M2440ReasonPatientAdmittedUnsafe"] != null && result["M2440ReasonPatientAdmittedUnsafe"] != undefined ? result["M2440ReasonPatientAdmittedUnsafe"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedOther][value=' + (result["M2440ReasonPatientAdmittedOther"] != null && result["M2440ReasonPatientAdmittedOther"] != undefined ? result["M2440ReasonPatientAdmittedOther"].Answer : "") + ']').attr('checked', true);
                $('input[name=TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown][value=' + (result["M2440ReasonPatientAdmittedUnknown"] != null && result["M2440ReasonPatientAdmittedUnknown"] != undefined ? result["M2440ReasonPatientAdmittedUnknown"].Answer : "") + ']').attr('checked', true);

            }
            $("#TransferInPatientDischarged_M0903LastHomeVisitDate").val(result["M0903LastHomeVisitDate"] != null && result["M0903LastHomeVisitDate"] != undefined ? result["M0903LastHomeVisitDate"].Answer : "");
            $("#TransferInPatientDischarged_M0906DischargeDate").val(result["M0906DischargeDate"] != null && result["M0906DischargeDate"] != undefined ? result["M0906DischargeDate"].Answer : "");
        };
    }

}