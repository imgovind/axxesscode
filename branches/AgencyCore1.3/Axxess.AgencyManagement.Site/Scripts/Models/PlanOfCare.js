﻿var PlanOfCare = {
    Edit: function(planofCareId) {
        $("#edit485Form").clearForm();
        var data = 'Id=' + planofCareId
        $.ajax({
            url: '/Oasis/Get485',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                loadPlanOfCareEdit(result);
            }
        });
        loadPlanOfCareEdit = function(result) {
            var planofCare = eval(result);
            $("#txtEdit_PlanOfCare_Medications").val(planofCare.Medications !== null ? planofCare.Medications : "");
            $("#txtEdit_PlanOfCare_DMESupplies").val(planofCare.DME !== null ? planofCare.DME : "");

            if (planofCare.DMEComments !== null) {
                $("#txtEdit_PlanOfCare_DMESupplies").val($("#txtEdit_PlanOfCare_DMESupplies").val() + " " + planofCare.DMEComments);
            }

            if (planofCare.Supplies !== null) {
                $("#txtEdit_PlanOfCare_DMESupplies").val($("#txtEdit_PlanOfCare_DMESupplies").val() + " " + planofCare.Supplies);
            }

            if (planofCare.SuppliesComments !== null) {
                $("#txtEdit_PlanOfCare_DMESupplies").val($("#txtEdit_PlanOfCare_DMESupplies").val() + " " + planofCare.SuppliesComments);
            }

            $("#txtEdit_PlanOfCare_SafetyMeasures").val(planofCare.SafetyMeasures !== null ? planofCare.SafetyMeasures : "");
            $("#txtEdit_PlanOfCare_NutritionalRequirements").val(planofCare.NutritionalRequirements !== null ? planofCare.NutritionalRequirements : "");
            $("#txtEdit_PlanOfCare_Allergies").val(planofCare.Allergies !== null ? planofCare.Allergies : "");
            $("#txtEdit_PlanOfCare_Interventions").val(planofCare.Interventions !== null ? planofCare.Interventions : "");
            $("#txtEdit_PlanOfCare_Goals").val(planofCare.Goals !== null ? planofCare.Goals : "");

            if (planofCare.FunctionalLimitations !== null && planofCare.FunctionalLimitations != undefined) {
                var functionalLimitationsArray = (planofCare.FunctionalLimitations).split(',');
                var i = 0;
                for (i = 0; i < functionalLimitationsArray.length; i++) {
                    $('input[name=txtEdit_PlanOfCare_FunctionLimitations][value=' + functionalLimitationsArray[i] + ']').attr('checked', true);
                    if (functionalLimitationsArray[i] == "B") {
                        $("#txtEdit_PlanOfCare_FunctionLimitationsOther").val((planofCare.FunctionalLimitationsOther !== null ? planofCare.FunctionalLimitationsOther : ""));
                    }
                }
            }

            if (planofCare.ActivitiesPermitted !== null && planofCare.ActivitiesPermitted != undefined) {
                var activitiesPermittedArray = (planofCare.ActivitiesPermitted).split(',');
                var j = 0;
                for (j = 0; j < activitiesPermittedArray.length; j++) {
                    $('input[name=txtEdit_PlanOfCare_ActivitiesPermitted][value=' + activitiesPermittedArray[j] + ']').attr('checked', true);
                    if (activitiesPermittedArray[j] == "D") {
                        $("#txtEdit_PlanOfCare_ActivitiesPermittedOther").val((planofCare.ActivitiesPermittedOther !== null ? planofCare.ActivitiesPermittedOther : ""));
                    }
                }
            }

            if (planofCare.MentalStatus !== null && planofCare.MentalStatus != undefined) {
                var mentalStatusArray = (planofCare.MentalStatus).split(',');
                var k = 0;
                for (k = 0; k < mentalStatusArray.length; k++) {
                    $('input[name=txtEdit_PlanOfCare_MentalStatus][value=' + mentalStatusArray[k] + ']').attr('checked', true);
                    if (mentalStatusArray[k] == "8") {
                        $("#txtEdit_PlanOfCare_MentalStatusOther").val((planofCare.MentalStatusOther !== null ? planofCare.MentalStatusOther : ""));
                    }
                }
            }

            if (planofCare.Prognosis !== null && planofCare.Prognosis != undefined) {
                $('input[name=New_StartOfCare_485Prognosis][value=' + planofCare.Prognosis + ']').attr('checked', true);
            }
        }
    }
}