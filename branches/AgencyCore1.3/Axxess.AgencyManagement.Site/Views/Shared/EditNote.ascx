﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editNote" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="EditNoteTitle" class="float_left">Customer Notes </span><span class="float_right">
                <a href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <% using (Html.BeginForm("Note", "Patient", FormMethod.Post, new { @id = "patientNoteForm" }))%>
            <%  { %>
            <%=Html.Hidden("patientId", "", new { @id = "txt_PatientNote_PatientID" })%>
            <div class="editNoteLeft">
                <div class="editNoteLeftTop">
                    <fieldset id="customerInfo">
                        <legend>Note for
                            <label id="editNotePatientName">
                            </label>
                            :</legend>
                    </fieldset>
                </div>
                <div class="editNoteLeftBottom">
                    <div class="row">
                        <textarea name="patientNote" cols=" " rows=" " id="notes" style="width: 100%; height: 100%;
                            min-height: 200px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="editNoteRight">
                <div class="editNoteButtonCont">
                    <div class="notes row">
                        <div class="row">
                            <input id="editnote_Ok" type="submit" value="Ok" />
                        </div>
                        <div class="row">
                            <input type="button" value="Cancel" onclick="Patient.Close($(this));" />
                        </div>
                        <div class="row">
                            <input type="button" value="Help" />
                        </div>
                        <div class="row">
                            <input type="button" value="Date Stamp" onclick="Patient.NoteTimeStamp();" />
                        </div>
                        <div class="row">
                            <input type="button" value="New To Do" />
                        </div>
                        <div class="row">
                            <input type="button" value="Print" />
                        </div>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</div>
