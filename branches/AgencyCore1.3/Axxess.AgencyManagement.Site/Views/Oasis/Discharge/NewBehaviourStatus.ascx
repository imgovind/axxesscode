﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyBehaviourialForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1700) Cognitive Functioning: Patient's current (day of assessment) level of alertness,
                orientation, comprehension, concentration, and immediate memory for simple commands.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1700CognitiveFunctioning", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "00", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Alert/oriented, able to focus and shift attention, comprehends and recalls
                task directions independently.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "01", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar
                conditions.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "02", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Requires assistance and some direction in specific situations (e.g., on all tasks
                involving shifting of attention), or consistently requires low stimulus environment
                due to distractibility.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "03", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Requires considerable assistance in routine situations. Is not alert and oriented
                or is unable to shift attention and recall directions more than half the time.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "04", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Totally dependent due to disturbances such as constant disorientation, coma, persistent
                vegetative state, or delirium.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1710) When Confused (Reported or Observed Within the Last 14 Days):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1710WhenConfused", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "00", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Never<br />
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "01", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - In new or complex situations only<br />
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "02", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - On awakening or at night only<br />
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "03", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - During the day and evening, but not constantly<br />
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "04", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Constantly<br />
            <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "NA", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient nonresponsive
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1720) When Anxious (Reported or Observed Within the Last 14 Days):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1720WhenAnxious", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "00", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - None of the time<br />
            <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "01", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Less often than daily<br />
            <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "02", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Daily, but not constantly<br />
            <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "03", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - All of the time<br />
            <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "NA", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient nonresponsive
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at
                least once a week (Reported or Observed): (Mark all that apply.)
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;1
                - Memory deficit: failure to recognize familiar persons/places, inability to recall
                events of past 24 hours, significant memory loss so that supervision is required<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;2
                - Impaired decision-making: failure to perform usual ADLs or IADLs, inability to
                appropriately stop activities, jeopardizes safety through actions<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;3
                - Verbal disruption: yelling, threatening, excessive profanity, sexual references,
                etc.<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;4
                - Physical aggression: aggressive or combative to self and others (e.g., hits self,
                throws objects, punches, dangerous maneuvers with wheelchair or other objects)<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;5
                - Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;6
                - Delusional, hallucinatory, or paranoid behavior<br />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"
                    value="" type="hidden" />
                <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;7
                - None of the above behaviors demonstrated
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical,
                verbal, or other disruptive/dangerous symptoms that are injurious to self or others
                or jeopardize personal safety.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Never<br />
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Less than once a month<br />
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Once a month<br />
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Several times each month<br />
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Several times a week<br />
            <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
            - At least daily
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1750) Is this patient receiving Psychiatric Nursing Services at home provided
                by a qualified psychiatric nurse?<br />
                &nbsp;
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1750PsychiatricNursingServicing", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1750PsychiatricNursingServicing", "0", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("DischargeFromAgency_M1750PsychiatricNursingServicing", "1", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
