﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareRiskAssessmentForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1032) Risk for Hospitalization: Which of the following signs or symptoms characterize
                this patient as at risk for hospitalization? (Mark all that apply.)
            </div>
        </div>
        <div class="insideCol margin">
            <input name="StartOfCare_M1032HospitalizationRiskRecentDecline" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskRecentDecline" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskRecentDecline") && data["M1032HospitalizationRiskRecentDecline"].Answer == "1" ){ %>checked="checked"<% }%>'" />1
            - Recent decline in mental, emotional, or behavioral status<br />
            <input name="StartOfCare_M1032HospitalizationRiskMultipleHosp" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskMultipleHosp" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskMultipleHosp") && data["M1032HospitalizationRiskMultipleHosp"].Answer == "1" ){ %>checked="checked"<% }%>'" />2
            - Multiple hospitalizations (2 or more) in the past 12 months<br />
            <input name="StartOfCare_M1032HospitalizationRiskHistoryOfFall" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskHistoryOfFall" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskHistoryOfFall") && data["M1032HospitalizationRiskHistoryOfFall"].Answer == "1" ){ %>checked="checked"<% }%>'" />3
            - History of falls (2 or more falls - or any fall with an injury - in the past year)<br />
            <input name="StartOfCare_M1032HospitalizationRiskMedications" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskMedications" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskMedications") && data["M1032HospitalizationRiskMedications"].Answer == "1" ){ %>checked="checked"<% }%>'" />4
            - Taking five or more medications<br />
        </div>
        <div class="insideCol margin">
            <input name="StartOfCare_M1032HospitalizationRiskFrailty" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskFrailty" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskFrailty") && data["M1032HospitalizationRiskFrailty"].Answer == "1" ){ %>checked="checked"<% }%>'" />5
            - Frailty indicators, e.g., weight loss, self-reported exhaustion<br />
            <input name="StartOfCare_M1032HospitalizationRiskOther" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskOther" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskOther") && data["M1032HospitalizationRiskOther"].Answer == "1" ){ %>checked="checked"<% }%>'" />6
            - Other<br />
            <input name="StartOfCare_M1032HospitalizationRiskNone" value=" " type="hidden" />
            <input name="StartOfCare_M1032HospitalizationRiskNone" value="1" type="checkbox" '<% if( data.ContainsKey("M1032HospitalizationRiskNone") && data["M1032HospitalizationRiskNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />7
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1034) Overall Status: Which description best fits the patient’s overall status?
                (Check one)
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("StartOfCare_M1034OverallStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("StartOfCare_M1034OverallStatus", "00", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            0 - The patient is stable with no heightened risk(s) for serious complications and
            death (beyond those typical of the patient’s age).<br />
            <%=Html.RadioButton("StartOfCare_M1034OverallStatus", "01", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "01" ? true : false, new { @id = "" })%>
            1 - The patient is temporarily facing high health risk(s) but is likely to return
            to being stable without heightened risk(s) for serious complications and death (beyond
            those typical of the patient’s age).<br />
            <%=Html.RadioButton("StartOfCare_M1034OverallStatus", "02", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "02" ? true : false, new { @id = "" })%>
            2 - The patient is likely to remain in fragile health and have ongoing high risk(s)
            of serious complications and death.<br />
            <%=Html.RadioButton("StartOfCare_M1034OverallStatus", "03", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "03" ? true : false, new { @id = "" })%>
            3 - The patient has serious progressive conditions that could lead to death within
            a year.<br />
            <%=Html.RadioButton("StartOfCare_M1034OverallStatus", "UK", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "UK" ? true : false, new { @id = "" })%>
            UK - The patient’s situation is unknown or unclear.
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1036) Risk Factors, either present or past, likely to affect current health status
                and/or outcome: (Mark all that apply.)
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <input name="StartOfCare_M1036RiskFactorsSmoking" value=" " type="hidden" />
                <input name="StartOfCare_M1036RiskFactorsSmoking" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsSmoking") && data["M1036RiskFactorsSmoking"].Answer == "1" ){ %>checked="checked"<% }%>'" />1
                - Smoking<br />
                <input name="StartOfCare_M1036RiskFactorsObesity" value=" " type="hidden" />
                <input name="StartOfCare_M1036RiskFactorsObesity" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsObesity") && data["M1036RiskFactorsObesity"].Answer == "1" ){ %>checked="checked"<% }%>'" />2
                - Obesity<br />
                <input name="StartOfCare_M1036RiskFactorsAlcoholism" value=" " type="hidden" />
                <input name="StartOfCare_M1036RiskFactorsAlcoholism" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsAlcoholism") && data["M1036RiskFactorsAlcoholism"].Answer == "1" ){ %>checked="checked"<% }%>'" />3
                - Alcohol dependency<br />
            </div>
        </div>
        <div class="insideCol">
            <input name="StartOfCare_M1036RiskFactorsDrugs" value=" " type="hidden" />
            <input name="StartOfCare_M1036RiskFactorsDrugs" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsDrugs") && data["M1036RiskFactorsDrugs"].Answer == "1" ){ %>checked="checked"<% }%>'" />4
            - Drug dependency<br />
            <input name="StartOfCare_M1036RiskFactorsNone" value=" " type="hidden" />
            <input name="StartOfCare_M1036RiskFactorsNone" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsNone") && data["M1036RiskFactorsNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />5
            - None of the above<br />
            <input name="StartOfCare_M1036RiskFactorsUnknown" value=" " type="hidden" />
            <input name="StartOfCare_M1036RiskFactorsUnknown" value="1" type="checkbox" '<% if( data.ContainsKey("M1036RiskFactorsUnknown") && data["M1036RiskFactorsUnknown"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            UK - Unknown
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="5">
                Most Recent Immunizations
            </th>
        </tr>
        <tr>
            <td>
                Pneumonia
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485Pnemonia", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485Pnemonia", "Yes", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Pnemonia", "No", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Pnemonia", "UK", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485PnemoniaDate", data.ContainsKey("485PnemoniaDate") ? data["485PnemoniaDate"].Answer : "", new { @id = "StartOfCare_485PnemoniaDate" })%>
            </td>
        </tr>
        <tr>
            <td>
                Flu
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485Flu", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485Flu", "Yes", data.ContainsKey("485Flu") && data["485Flu"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Flu", "No", data.ContainsKey("485Flu") && data["485Flu"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Flu", "UK", data.ContainsKey("485Flu") && data["485Flu"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485FluDate", data.ContainsKey("485FluDate") ? data["485FluDate"].Answer : "", new { @id = "StartOfCare_485FluDate" })%>
            </td>
        </tr>
        <tr>
            <td>
                Tetanus
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485Tetanus", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485Tetanus", "Yes", data.ContainsKey("485Tetanus") && data["485Tetanus"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Tetanus", "No", data.ContainsKey("485Tetanus") && data["485Tetanus"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485Tetanus", "UK", data.ContainsKey("485Tetanus") && data["485Tetanus"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485TetanusDate", data.ContainsKey("485TetanusDate") ? data["485TetanusDate"].Answer : "", new { @id = "StartOfCare_485TetanusDate" })%>
            </td>
        </tr>
        <tr>
            <td>
                TB
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485TB", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485TB", "Yes", data.ContainsKey("485TB") && data["485TB"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485TB", "No", data.ContainsKey("485TB") && data["485TB"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485TB", "UK", data.ContainsKey("485TB") && data["485TB"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485TBDate", data.ContainsKey("485TBDate") ? data["485TBDate"].Answer : "", new { @id = "StartOfCare_485TBDate" })%>
            </td>
        </tr>
        <tr>
            <td>
                TB Exposure
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485TBExposure", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485TBExposure", "Yes", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485TBExposure", "No", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485TBExposure", "UK", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485TBExposureDate", data.ContainsKey("485TBExposureDate") ? data["485TBExposureDate"].Answer : "", new { @id = "StartOfCare_485TBExposureDate" })%>
            </td>
        </tr>
        <tr>
            <td>
                Hepatitis B
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485HepatitisB", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485HepatitisB", "Yes", data.ContainsKey("485HepatitisB") && data["485HepatitisB"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485HepatitisB", "No", data.ContainsKey("485HepatitisB") && data["485HepatitisB"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485HepatitisB", "UK", data.ContainsKey("485HepatitisB") && data["485HepatitisB"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485HepatitisBDate", data.ContainsKey("485HepatitisBDate") ? data["485HepatitisBDate"].Answer : "", new { @id = "StartOfCare_485HepatitisBDate" })%>
            </td>
        </tr>
        <tr>
            <th colspan="5">
                Additional Immunizations
            </th>
        </tr>
        <tr>
            <td>
                <%=Html.TextBox("StartOfCare_485AdditionalImmunization1Name", data.ContainsKey("485AdditionalImmunization1Name") ? data["485AdditionalImmunization1Name"].Answer : "", new { @id = "StartOfCare_485AdditionalImmunization1Name" })%>
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdditionalImmunization1", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization1", "Yes", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization1", "No", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization1", "UK", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485AdditionalImmunization1Date", data.ContainsKey("485AdditionalImmunization1Date") ? data["485AdditionalImmunization1Date"].Answer : "", new { @id = "StartOfCare_485AdditionalImmunization1Date" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%=Html.TextBox("StartOfCare_485AdditionalImmunization2Name", data.ContainsKey("485AdditionalImmunization2Name") ? data["485AdditionalImmunization2Name"].Answer : "", new { @id = "StartOfCare_485AdditionalImmunization2Name" })%>
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdditionalImmunization2", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization2", "Yes", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization2", "No", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
            <td>
                <%=Html.RadioButton("StartOfCare_485AdditionalImmunization2", "UK", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "UK" ? true : false, new { @id = "" })%>
                Unknown
            </td>
            <td>
                Date:
                <%=Html.TextBox("StartOfCare_485AdditionalImmunization2Date", data.ContainsKey("485AdditionalImmunization2Date") ? data["485AdditionalImmunization2Date"].Answer : "", new { @id = "StartOfCare_485AdditionalImmunization2Date" })%>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                Comments:<br />
                <%=Html.TextArea("StartOfCare_485ImmunizationComments", data.ContainsKey("485ImmunizationComments") ? data["485ImmunizationComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485ImmunizationComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Health Screening
            </th>
        </tr>
        <tr>
            <td style="width: 40%;">
                Last Cholesterol Level:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastCholesterolLevel", data.ContainsKey("485LastCholesterolLevel") ? data["485LastCholesterolLevel"].Answer : "", new { @id = "StartOfCare_485LastCholesterolLevel",@size="20", @maxlength="20" })%>
            </td>
        </tr>
        <tr>
            <td>
                Last Mammogram:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastMammogram", data.ContainsKey("485LastMammogram") ? data["485LastMammogram"].Answer : "", new { @id = "StartOfCare_485LastMammogram", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td>
                Does patient perform monthly self breast exams?
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485PerformMonthlySelfBreastExams", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485PerformMonthlySelfBreastExams", "Yes", data.ContainsKey("485PerformMonthlySelfBreastExams") && data["485PerformMonthlySelfBreastExams"].Answer == "Yes" ? true : false, new { @id = "" })%>
                Yes
                <%=Html.RadioButton("StartOfCare_485PerformMonthlySelfBreastExams", "No", data.ContainsKey("485PerformMonthlySelfBreastExams") && data["485PerformMonthlySelfBreastExams"].Answer == "No" ? true : false, new { @id = "" })%>
                No
            </td>
        </tr>
        <tr>
            <td>
                Last Pap Smear:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastPapSmear", data.ContainsKey("485LastPapSmear") ? data["485LastPapSmear"].Answer : "", new { @id = "StartOfCare_485LastPapSmear", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td>
                Last PSA:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastPSA", data.ContainsKey("485LastPSA") ? data["485LastPSA"].Answer : "", new { @id = "StartOfCare_485LastPSA", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td>
                Last Prostate Exam:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastProstateExam", data.ContainsKey("485LastProstateExam") ? data["485LastProstateExam"].Answer : "", new { @id = "StartOfCare_485LastProstateExam", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td>
                Last Colonoscopy:
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485LastColonoscopy", data.ContainsKey("485LastColonoscopy") ? data["485LastColonoscopy"].Answer : "", new { @id = "StartOfCare_485LastColonoscopy", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="StartOfCare_485RiskAssessmentIntervention" value=" " />
                <input name="StartOfCare_485RiskAssessmentIntervention" value="1" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assist patient to obtain ERS button
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485RiskAssessmentIntervention" value="2" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "2" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to develop individualized emergency plan with patient
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485RiskAssessmentIntervention" value="3" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "3" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on importance of receiving influenza and pneumococcal vaccines
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485RiskAssessmentIntervention" value="4" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "4" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer influenza vaccination as follows:
                <br />
                <%=Html.TextBox("StartOfCare_485AdministerVaccinationDetails", data.ContainsKey("485AdministerVaccinationDetails") ? data["485AdministerVaccinationDetails"].Answer : "", new { @id = "StartOfCare_485AdministerVaccinationDetails", @size = "60", @maxlength = "75" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485RiskAssessmentIntervention" value="5" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "5" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer pneumococcal vaccination as follows:
                <br />
                <%=Html.TextBox("StartOfCare_485AdministerPneumococcalVaccineDetails", data.ContainsKey("485AdministerPneumococcalVaccineDetails") ? data["485AdministerPneumococcalVaccineDetails"].Answer : "", new { @id = "StartOfCare_485AdministerPneumococcalVaccineDetails", @size = "60", @maxlength = "75" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders:
                <%var riskInterventionTemplates = new SelectList(new[] { new SelectListItem { Text = "Sample Template", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485RiskInterventionTemplates") && data["485RiskInterventionTemplates"].Answer != "" ? data["485RiskInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485RiskInterventionTemplates", riskInterventionTemplates)%>
                <br />
                <%=Html.TextArea("StartOfCare_485RiskInterventionComments", data.ContainsKey("485RiskInterventionComments") ? data["485RiskInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485RiskInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="StartOfCare_485RiskAssessmentGoals" value=" " />
                <input name="StartOfCare_485RiskAssessmentGoals" value="1" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer == "1" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The patient will have no hospitalizations during the certification period
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485RiskAssessmentGoals" value="2" type="checkbox" '<% if( data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer == "2" ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeEmergencyPlanPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer != "" ? data["485VerbalizeEmergencyPlanPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("StartOfCare_485VerbalizeEmergencyPlanPerson", verbalizeEmergencyPlanPerson)%>
                will verbalize understanding of individualized emergency plan by:
                <input type="text" size="20" maxlength="12" name="StartOfCare_485VerbalizeEmergencyPlanDate"
                    id="StartOfCare_485VerbalizeEmergencyPlanDate" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals:
                <%var riskGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "Sample Template", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485RiskGoalTemplates") && data["485RiskGoalTemplates"].Answer != "" ? data["485RiskGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485RiskGoalTemplates", riskGoalTemplates)%>
                <br />
                <%=Html.TextArea("StartOfCare_485RiskGoalComments", data.ContainsKey("485RiskGoalComments") ? data["485RiskGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485RiskGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
