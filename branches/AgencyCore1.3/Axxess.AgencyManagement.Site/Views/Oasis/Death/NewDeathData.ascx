﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDeathAtHomeDeathForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgencyDeath_Id",Model.Id)%>
<%= Html.Hidden("DischargeFromAgencyDeath_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgencyDeath_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgencyDeath")%>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0903) Date of Last (Most Recent) Home Visit:
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("DischargeFromAgencyDeath_M0903LastHomeVisitDate", data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0903LastHomeVisitDate" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                or death (at home) of the patient.
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("DischargeFromAgencyDeath_M0906DischargeDate", data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0906DischargeDate" })%>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save" class="SaveContinue" onclick="DeathAtHome.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="DeathAtHome.FormSubmit($(this));" /></li>
    </ul>
</div>
<%  } %>