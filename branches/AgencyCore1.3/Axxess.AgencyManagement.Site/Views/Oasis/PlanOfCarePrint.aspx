﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Axxess.OasisC.ViewData.PlanOfCareViewData>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>485 - Plan of Care Print View</title>
    <%= Html.Telerik().StyleSheetRegistrar()
                      .DefaultGroup(group => group
                          .Add("485.css")
                          .Combined(true)
                          .Compress(true))
    %>
</head>
<body>
    <span class="small">Department of Health and Human Services Health Care Financing Administration
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form
        Approved OHB No. 0938-0357 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Order Number# 15516982 </span>
    <table width="763" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000"
        height="940">
        <tr>
            <td colspan="7" align="center">
                Home Health Certification and Plan of Care
            </td>
        </tr>
        <tr>
            <td valign="top" style="border-right: 1px solid #000000">
                1. Patient's HI Claim No.<br />
                <span class="OASIS485times">
                    <%= Model.Patient.HicClaimNumber %></span>
            </td>
            <td valign="top" style="border-right: 1px solid #000000">
                2. Start of Care Date<br />
                <span class="OASIS485times">
                    <%= Model.Patient.StartOfCareDate %></span>
            </td>
            <td colspan="3" valign="top" style="border-right: 1px solid #000000">
                3. Certification Period<br />
                From:&nbsp;&nbsp; <span class="OASIS485times">
                    <%= Model.Patient.CertificationPeriodFrom %></span> To:&nbsp;&nbsp; <span class="OASIS485times">
                        <%= Model.Patient.CertificationPeriodTo %></span>
            </td>
            <td valign="top" style="border-right: 1px solid #000000">
                4. Medical Record No.<br />
                <span class="OASIS485times">
                    <%= Model.Patient.MedicalRecordNumber %></span>
            </td>
            <td valign="top">
                5. Provider No.<br />
                <span class="OASIS485times">
                    <%= Model.Agency.ProviderNumber %></span>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top" style="border-right: 1px solid #000000">
                6. Patient's Name and Address<br />
                <span class="OASIS485times">
                    <%= Model.Patient.DisplayName %>
                    <br />
                    <%= Model.Patient.AddressFirstLine %>
                    <br />
                    <%= Model.Patient.AddressSecondLine %>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <%= Model.Patient.TelephoneNumber %>
                </span>
            </td>
            <td colspan="4" valign="top">
                7. Provider's Name, Address and Telephone Number
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="OASIS485times" colspan="2" style="border: none;">
                            <%= Model.Agency.DisplayName %>
                        </td>
                    </tr>
                    <tr>
                        <td class="OASIS485times" colspan="2" style="border: none;">
                            <%= Model.Agency.AddressFirstLine %>
                        </td>
                    </tr>
                    <tr>
                        <td class="OASIS485times" style="border: none;" colspan="2">
                            <%= Model.Agency.AddressSecondLine %>
                        </td>
                    </tr>
                    <tr>
                        <td class="OASIS485times" style="border: none;">
                            <%= Model.Agency.TelephoneNumber %>
                        </td>
                        <td align="right" class="OASIS485times" style="border: none;" nowrap="nowrap">
                            <%= Model.Agency.FaxNumber %>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <table cellpadding="0" cellspacing="0" bordercolor="#000000" width="100%">
                    <tr>
                        <td style="border-top: 1px; border-right: 1px; border-bottom: 1px  solid;" colspan="2">
                            8. Date of Birth: <span class="OASIS485times">
                                <%= Model.Patient.DateOfBirth %></span>
                        </td>
                        <td style="border-top: 1px; border-right: 1px; border-left: 1px  solid; border-bottom: 1px  solid;"
                            colspan="2">
                            9. Sex
                            <% if (Model.Patient.IsMale) %>
                            <% { %>
                            <img src="/Images/checkbox-checked.gif" alt="checked" border="0" height="13" width="13" />
                            <% } %>
                            <% else %>
                            <% { %>
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            <% } %>
                            M
                            <% if (!Model.Patient.IsMale) %>
                            <% { %>
                            <img src="/Images/checkbox-checked.gif" alt="checked" border="0" height="13" width="13" />
                            <% } %>
                            <% else %>
                            <% { %>
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            <% } %>
                            F
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" bordercolor="#ffffff" width="100%" border="1">
                    <tr valign="top">
                        <td width="90" style="border-right: 1px solid #000; border-bottom: 1px solid #000;">
                            11. ICD-9-CM<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.PrincipalDiagnosis.Code %></span>
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;" nowrap="nowrap">
                            Principal Diagnosis<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.PrincipalDiagnosis.Text %></span>
                        </td>
                        <td style="border-bottom: 1px solid #000;">
                            Date<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.PrincipalDiagnosis.Date %></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid #000; border-bottom: 1px solid #000;">
                            12. ICD-9-CM<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.SurgicalProcedure.Code %></span>
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                            Surgical Procedure<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.SurgicalProcedure.Text %></span>
                        </td>
                        <td style="border-bottom: 1px solid #000;">
                            Date<br />
                            <span class="OASIS485times">
                                <%= Model.Questions.SurgicalProcedure.Date %></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-left: 0px #000; border-bottom: 0px #000; border-right: 1px solid #000;"
                            valign="top" nowrap="nowrap">
                            13. ICD-9-CM<br />
                            <span class="OASIS485times"></span>
                        </td>
                        <td style="border-bottom: 0px; border-left: 0px; border-right: 1px solid #000;" valign="top"
                            nowrap="true">
                            Other Pertinent Diagnosis<br />
                            <span class="OASIS485times"></span>
                        </td>
                        <td style="border-right: 0px; border-bottom: 0px; border-left: 1px solid;" valign="top"
                            nowrap="nowrap">
                            Date<br />
                            <span class="OASIS485times"></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="4" valign="top" style="border-left: 1px solid;" width="350">
                10. Medications: Dose/Freq./Route (N)ew (C)hanged<br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="350" valign="top">
                14. DME and Supplies<br />
                <span class="OASIS485times"></span>
            </td>
            <td colspan="5" width="350" valign="top" style="border-left: 1px solid;">
                15. Safety measures<br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top" width="350">
                16. Nutritional Requirements<br />
                <span class="OASIS485times"></span>
            </td>
            <td colspan="5" valign="top" style="border-left: 1px solid; width: 325">
                17. Allergies<br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                18.A. Functional Limitations<br />
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="border: none; vertical-align: top;" width="33%">
                            1.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Amputation
                        </td>
                        <td style="border: none;" width="33%">
                            2.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Paralysis
                        </td>
                        <td style="border: none;" width="33%">
                            3.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Legally Blind
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            4.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Bowel/Bladder
                        </td>
                        <td style="border: none;" width="33%">
                            5.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Endurance
                        </td>
                        <td style="border: none;" width="33%">
                            6.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Dyspnea
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            7.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Contracture
                        </td>
                        <td style="border: none;" width="33%">
                            8.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Ambulation
                        </td>
                        <td style="border: none;" width="33%">
                            9.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Hearing
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            A.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Speech
                        </td>
                        <td style="border: none;" width="33%">
                            B.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Other
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
            <td colspan="4" valign="top" style="border-left: 1px solid;">
                18.B. Activities Permitted<br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="border: none;" width="33%">
                            1.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Complete bed rest
                        </td>
                        <td style="border: none;" width="33%">
                            2.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Up as tolerated
                        </td>
                        <td style="border: none;" width="33%">
                            3.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Exercise prescribed
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            4.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Independent
                        </td>
                        <td style="border: none;" width="33%">
                            5.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Cane
                        </td>
                        <td style="border: none;" width="33%">
                            6.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Walker
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            7.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Bed rest with BRP
                        </td>
                        <td style="border: none;" width="33%">
                            8.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Transfer bed-chair
                        </td>
                        <td style="border: none;" width="33%">
                            9.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Partial weight bearing
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            A.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Crutches
                        </td>
                        <td style="border: none;" width="33%">
                            B.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Wheelchair
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;" width="33%">
                            C.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Other (specify):
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="border: none;">
                            19. Mental Status
                        </td>
                        <td style="border: none;">
                            1.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Oriented
                        </td>
                        <td style="border: none;">
                            2.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Comatose
                        </td>
                        <td style="border: none;">
                            3.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Forgetful
                        </td>
                        <td style="border: none;">
                            4.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Agitated
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border: none;">
                            5.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Depressed
                        </td>
                        <td style="border: none;">
                            6.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Disoriented
                        </td>
                        <td style="border: none;">
                            7.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Lethargic
                        </td>
                        <td style="border: none;">
                            8.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Other
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border: none;">
                            9.
                            <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                            Additional Orders
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="100%" width="100%">
                20. Prognosis&nbsp;&nbsp;
                <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                Guarded&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                Poor&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                Fair&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                Good&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="/Images/checkbox-empty.gif" alt="checked" border="0" height="13" width="13" />
                Excellent&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="8" valign="top">
                21. Orders for Discipline and Treatments (Specify Amount/ Frequency/ Duration)<br />
                <table width="100%">
                    <tr>
                        <td style="border: none;" width="100%" valign="top" class="OASIS485times">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8" valign="top">
                22. Goals/ Rehabiliation Potential/ Discharge Plans<br />
                <table width="100%">
                    <tr>
                        <td style="border: none;" width="100%" valign="top" class="OASIS485times">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="40">
            <td colspan="3" valign="top">
                23. Nurse Signature and Date of Verbal SOC Where Applicable
                <br />
                <span class="Signature">
                    <br />
                    </span>
            </td>
            <td colspan="4" valign="top" style="border-left: 1px solid;">
                25. Date HHA Received Signed POT<br />
            </td>
        </tr>
        <tr height="60">
            <td colspan="3" valign="top">
                24. Physician's Name and Address<br />
                <span class="OASIS485times">
                    </span>
            </td>
            <td colspan="4" width="350" valign="top" style="border-left: 1px solid;">
                26. I <u>Certify</u>/ Recertify that this patient is confined to his/her home and
                needs intermittent skilled nursing care, physical therapy and/or speech therapy
                or continue to need occupational therapy. The patient is under my care, and I have
                authorized the services on this plan of care and will periodically review the plan.
            </td>
        </tr>
        <tr height="50">
            <td colspan="3" valign="top">
                27. Attending Physician's Signature and Date Signed<br />
                <table>
                    <tr height="50">
                        <td colspan="1" valign="top">
                            <span class="Signature"></span>
                        </td>
                        <td colspan="1" valign="top">
                            <span class="Signature"></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="4" width="350" valign="top" style="border-left: 1px solid;">
                28. Anyone who misrepresents, falsifies, or conceals essential information required
                for payment of Federal funds may be subject to fine, imprisonment, or civil penalty
                under applicable Federal laws.
            </td>
        </tr>
    </table>
    <span class="small">Form HCFA-485 (U4) (2-94)</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page
    1 of 2<div style="page-break-before: always; line-height: 0px">
        &nbsp;</div>
    <span class="small">Department of Health and Human Services Health Care Financing Administration
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form
        Approved OHB No. 0938-0357 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Order Number# 15516982 </span>
    <table width="763" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
        <tr>
            <td colspan="7" align="center" height="10">
                <strong>Addendum to Plan of Care</strong>
            </td>
        </tr>
        <tr>
            <td valign="top" style="border-right: 1px solid #000000">
                1. Patient's HI Claim No.<br />
                <span class="OASIS485times"></span>
            </td>
            <td valign="top" style="border-right: 1px solid #000000">
                2. Start of Care Date<br />
                <span class="OASIS485times"></span>
            </td>
            <td colspan="3" valign="top" style="border-right: 1px solid #000000">
                3. Certification Period<br />
                From:&nbsp;&nbsp; <span class="OASIS485times"></span> To:&nbsp;&nbsp;
                <span class="OASIS485times"></span>
            </td>
            <td valign="top" style="border-right: 1px solid #000000">
                4. Medical Record No.<br />
                <span class="OASIS485times"></span>
            </td>
            <td valign="top">
                5. Provider No.<br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
        <tr height="30">
            <td colspan="4" valign="top" style="border-right: 1px solid #000000">
                6. Patient's Name<br />
                <span class="OASIS485times">
                    <br />
                    <br />
                </span>
            </td>
            <td colspan="3" valign="top">
                7. Provider's Name
                <br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
    </table>
    <table width="763" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000"
        height="840">
        <tr>
            <td colspan="8" valign="top" style="border-top: 0px;">
                10. Medications
                <table width="100%">
                    <tr>
                        <td style="border: none;" style="border-top: 0px;" valign="top">
                            <span class="OASIS485times">
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                12. Surgical Procedures
                <table width="80%">
                    <tr>
                        <td style="border: none;">
                        </td>
                        <td style="border: none;">
                        </td>
                        <td style="border: none;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                13. Other Pertinent Diagnosis
                <table width="80%">
                    <tr>
                        <td style="border: none;">
                            <span class="OASIS485times">
                                <br />
                            </span><span class="OASIS485times"><br />
                            </span><span class="OASIS485times"><br />
                            </span>
                        </td>
                        <td style="border: none;">
                            <span class="OASIS485times"><br />
                            </span><span class="OASIS485times"><br />
                            </span><span class="OASIS485times"><br />
                            </span>
                        </td>
                        <td style="border: none;">
                            <span class="OASIS485times">
                                <br />
                            </span><span class="OASIS485times">
                                <br />
                            </span><span class="OASIS485times">
                                <br />
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                14. DME
                <table width="750">
                    <tr>
                        <td style="border: none;">
                            <span class="OASIS485times"></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                15. Safety Measures
                <table width="750">
                    <tr>
                        <td style="border: none;">
                            <span class="OASIS485times"></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                16. Nutrition
                <table width="750">
                    <tr>
                        <td style="border: none;">
                            <span class="OASIS485times"></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8" valign="top">
                18.A. Functional Limitations<br />
                <span class="OASIS485times"></span>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                21. Orders
                <table width="750">
                    <tr>
                        <td style="border: none;" width="750">
                            <span class="OASIS485times"><br />
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                22. Goals
                <table width="750">
                    <tr>
                        <td style="border: none;" width="750">
                            <span class="OASIS485times"><br />
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="50">
            <td colspan="1" valign="top">
                9. Signature of Physician<br />
                <span class="Signature"></span>
            </td>
            <td colspan="1" valign="top">
                10. Date<br />
                <span class="Signature"></span>
            </td>
        </tr>
        <tr height="50">
            <td colspan="1" valign="top">
                11. Optional Name/ Signature of Nurse/ Therapist<br />
                <br />
                <span class="Signature"></span>
                <br />
                <br />
            </td>
            <td colspan="1" valign="top">
                12. Date<br />
                <br />
                <span class="Signature"></span>
            </td>
        </tr>
    </table>
    <span class="small">Form HCFA-487 (C4) (4-87)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page
    2 of 2
</body>
</html>
