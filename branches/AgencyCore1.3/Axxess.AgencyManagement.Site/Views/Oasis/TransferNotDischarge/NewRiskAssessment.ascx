﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedRiskAssessmentForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your
                agency for this year’s influenza season (October 1 through March 31) during this
                episode of care?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M1040InfluenzaVaccine", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "00", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "01", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes [ Go to M1050 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "NA", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge)
            is outside this influenza season. [ Go to M1050 ]<br />
        </div>
    </div>
    <div class="insideCol" id="discharge_M1045">
        <div class="insiderow title">
            <div class="margin">
                (M1045) Reason Influenza Vaccine not received: If the patient did not receive the
                influenza vaccine from your agency during this episode of care, state reason:
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "01", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Received from another health care provider (e.g., physician)<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "02", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Received from your agency previously during this year’s flu season<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "03", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Offered and declined<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "04", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Assessed and determined to have medical contraindication(s)<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "05", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
            - Not indicated; patient does not meet age/condition guidelines for influenza vaccine<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "06", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
            - Inability to obtain vaccine due to declared shortage<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "07", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "07" ? true : false, new { @id = "" })%>&nbsp;7
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide
                vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M1050PneumococcalVaccine", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1050PneumococcalVaccine", "0", data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1050PneumococcalVaccine", "1", data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes [ Go to M1500 at TRN; Go to M1230 at DC ]
        </div>
    </div>
    <div class="insideCol" id="discharge_M1055">
        <div class="insiderow title">
            <div class="margin">
                (M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide
                vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge),
                state reason:
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "01", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient has received PPV in the past<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "02", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Offered and declined<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "03", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Assessed and determined to have medical contraindication(s)<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "04", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Not indicated; patient does not meet age/condition guidelines for PPV<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "05", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
