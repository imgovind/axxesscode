﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CliamViewData>>" %>
<% if(Model!=null && Model.Count>0) {%>
<table class="claim">
    <thead>
        <tr>
            
            <th>
                Patient Date
            </th>
            <th>
                Patient Name
            </th>
            <th>
                Medicare No
            </th>
            <th>
                Episode 
            </th>
            <th>
                Cliam Date
            </th>
            <th>
               Status
            </th>
            <th>
               Payment
            </th>
        </tr>
    </thead>
    <tbody>
      
        <% int j = 1;%>
        <% foreach (var claim in Model)
           {%>
        <% if (j % 2 == 0)
           {%>
        <tr class="even">
            <%}
           else
           { %>
            <tr class="odd">
                <%} %>
                <td>
                   <%=Html.TextBox("TempName",claim.PaymentDate!=null?claim.PaymentDate.ToShortDateString():"")%>
                </td>
                <td>
                   <%=string.Format("{0}. {1}" ,claim.LastName.Substring(0,1),claim.FirstName)%>
                </td>
                <td>
                  <%=claim.MedicareNumber %>
                </td>
                <td>
                  <%=claim.Type %>
                </td>
                <td>
                </td>
                <td>
                 <%var status = new SelectList(new[]
               { 
                   new SelectListItem { Value = "13", Text = "Claim Created" },
                   new SelectListItem { Value = "14", Text = "Claim Submitted" },
                   new SelectListItem { Value = "15", Text = "Claim Rejected"} ,
                   new SelectListItem { Value = "16", Text = "Payment Pending" },
                   new SelectListItem { Value = "17", Text = "Claim Accepted/Processing" },
                   new SelectListItem { Value = "18", Text = "Claim With Errors"} ,
                   new SelectListItem { Value = "19", Text = "Paid Claim" },
                   new SelectListItem { Value = "20", Text = "Cancelled Claim" }
                   
               }
                   , "Value", "Text", claim.Status);%>
                <%= Html.DropDownList("claimDropDown", status)%>
                  
                </td>
                <td>
                   
                  
                </td>               
            </tr>
            <% j++;
           } %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9" style="height: 30px; background: #AA9494;">
                <span>
                    <input value="Update the Claims" type="button" onclick="Billing.loadGenerate('#Billing_CenterContent');" />
                 
            </td>
        </tr>
    </tfoot>
</table>
<%} %>

