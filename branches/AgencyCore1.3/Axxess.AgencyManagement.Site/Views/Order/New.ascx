﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("Order", "Patient", FormMethod.Post, new { @id = "newOrderForm" }))%>
<% 
    {
        var mod = Model.Id;
%>
<%=Html.Hidden("PatientId", Model.Id, new { @id = "txtNew_Order_PatientID" })%>
<div id="newOrderValidaton" class="marginBreak " style="display: none">
</div>
<div class="row">
    <div class="contentDivider">
        <div class="row">
            <label for="FirstName">
                &nbsp;&nbsp;&nbsp; Patient Name:&nbsp;&nbsp;&nbsp;</label>
            <span name="OrderPatient" id="txtNew_Order_Patient">
                <%=Model.DisplayName%>
                </span>
        </div>
        <div class="row ">
            <label for="phone">
                &nbsp;&nbsp;&nbsp;&nbsp;Physician:&nbsp;&nbsp;&nbsp;</label>
            <select style="width: 150px;" class="Physicians input_wrapper required selectDropDown"
                tabindex="17" name="PhysicianId" id="txtNew_Order_Physician">
                <option value="0" selected>** Select Physician **</option>
            </select>
        </div>
    </div>
    <div class="contentDivider">
        <div class="row">
            <label for="FirstName">
                &nbsp;&nbsp;&nbsp; Order Date:&nbsp;&nbsp;&nbsp;</label>
            <%= Html.Telerik().DatePicker()
                                        .Name("OrderDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new {@id = "txtNew_Order_Date"})
                                                                         
            %>
        </div>
        <div class="row">
            <label for="FirstName">
                &nbsp;&nbsp;&nbsp; Assign to Clinician :&nbsp;&nbsp;&nbsp;</label>
            <select style="width: 150px;" class="Employees input_wrapper required selectDropDown"
                tabindex="17" name="EmployeeId" id="txtNew_Order_Assign">
                <option value="0" selected>** Select Employee **</option>
            </select>
        </div>
    </div>
    <div class="row">
        <label for="FirstName" class="row">
            &nbsp;&nbsp;&nbsp; Order Summary (Optional)&nbsp;&nbsp;&nbsp;</label>
        <div class="marginBreakNotes">
            <%=Html.TextBox("OrderSummary", "", new { @id = "txtNew_Order_Summary", @style = "width:100%;" })%>
        </div>
    </div>
</div>
<div class="row">
    <label for="FirstName">
        &nbsp;&nbsp;&nbsp; Order Text&nbsp;&nbsp;&nbsp;</label>
    <div class="marginBreakNotes">
        <%=Html.TextArea("OrderText", new { @id = "txtNew_Order_text", @class = "notesTextArea required" })%>
    </div>
</div>
<div class="row">
    <div class="marginBreakNotes">
        <div style="float: left; padding-top: 10px;">
            <input name="OrderProcessType" value="1" id="new_order_checkbox" type="checkbox"
                style="float: left;" />
            Check To Send Electronically
        </div>
        <div class="buttons buttonfix" style="float: left; padding-left: 10px;">
            <ul>
                <li>
                    <input id="new_order_save" name="" type="submit" value="Save" /></li>
                <li>
                    <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                <li>
                    <input id="new_order_sendElectronically" name="" type="submit" value="Send Electronically" /></li>
            </ul>
        </div>
    </div>
</div>
<%} %>
