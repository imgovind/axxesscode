﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Axxess Technology - AgencyCore&trade; Desktop
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="patient_window" class="abs window ui-widget-content">
        <div class="abs window_inner ">
            <div class="window_top ">
                <span class="float_left">Patient Center </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="PatientCenterResult" class="abs window_content">
            </div>
            <div class="abs window_bottom">
                Patient Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <%--<%Html.RenderPartial("~/Views/Patient/PatientLanding.ascx", Model);%>--%>
    <%--<%Html.RenderPartial("~/Views/Schedule/ScheduleLanding.ascx", Model);%>--%>
    <%--<%Html.RenderPartial("~/Views/Oasis/Validation.ascx", Model);%>--%>
    <%Html.RenderPartial("PatientInfo", Model);%>
    <%Html.RenderPartial("~/Views/windows/BillingCenter.ascx", Model);%>
    <%Html.RenderPartial("~/Views/patient/list.ascx", Model);%>
    <%Html.RenderPartial("~/Views/windows/MasterCalendar.ascx", Model);%>
    <%Html.RenderPartial("~/Views/windows/MessageInbox.ascx", Model);%>
    <div id="schedule_window" class="abs window ui-widget-content">
        <div class="abs window_inner ">
            <div class="window_top ">
                <span class="float_left">Scheduling Center</span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="scheduleLandingResult" class="abs window_content ">
            </div>
            <div class="abs window_bottom">
                Schedule Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="newpatient" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span id="StartOfCareTitle" class="float_left">New Patient</span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="newPatientResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
                New Patient
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="editPatient" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Edit Patient </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="editPatientResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
                Edit Patient
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="soc" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Start of Care</span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"> </a>
                    <a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="socResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                OASIS Start of Care Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="recertWindow" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Recertefication</span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"> </a>
                    <a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="recertWindowResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                OASIS Start of Care Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="newPhysicianContact" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">New Physician Contact </span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="newPhysicianResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
                New Agency Physician
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="editRecertification" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span id="RecertificationTitle" class="float_left">Edit Recertification</span><span
                    class="float_right"><a href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);"
                        class="window_resize"> </a><a href="javascript:void(0);" class="window_close">
                    </a></span>
            </div>
            <div id="recertificationResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="roc" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left" id="ResumptionofCareTitle">Resumption of Care</span><span
                    class="float_right"><a href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);"
                        class="window_resize"> </a><a href="javascript:void(0);" class="window_close">
                    </a></span>
            </div>
            <div id="resumptionOfCareResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                OASIS Resumption of Care Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="followUp" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span id="NewFollowUpTitle" class="float_left">Follow-Up</span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="followUpResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                Patient Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="death" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span id="NewDischargeFromAgencyDeathTitle" class="float_left">Death at home</span>
                <span class="float_right"><a href="javascript:void(0);" class="window_min"></a><a
                    href="javascript:void(0);" class="window_resize"></a><a href="javascript:void(0);"
                        class="window_close"></a></span>
            </div>
            <div id="deathResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                Patient Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="validation" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Oasis Validation Result </span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="validationResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="discharge" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span id="NewDischargeFromAgencyTitle" class="float_left">New Discharge from Agency
                </span><span class="float_right"><a href="javascript:void(0);" class="window_min"></a>
                    <a href="javascript:void(0);" class="window_resize"></a><a href="javascript:void(0);"
                        class="window_close"></a></span>
            </div>
            <div id="dischargeResult" class="abs window_content general_form oasisAssWindowContent">
            </div>
            <div class="abs window_bottom">
                Patient Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="orderNote" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">New Order </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_close"></a></span>
            </div>
            <div id="orderNoteResult" class="abs window_content general_form">
            </div>
        </div>
    </div>
    <div id="communicationNote" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">New Communication Note </span><span class="float_right"><a
                    href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="communicationNoteResult" class="abs window_content general_form">
            </div>
        </div>
    </div>
    <div id="newEmergencyContact" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">New Emergency Contact </span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="newEmergencyContactResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="editEmergencyContact" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Edit Emergency Contact </span><span class="float_right"><a
                    href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                    </a><a href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="editEmergencyContactResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="existingreferral" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Existing Referrals</span> <span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="existingreferralResult" class="abs window_content general_form">
            </div>
        </div>
        <div class="abs window_bottom">
            List of Existing Referrals
        </div>
    </div>
    <div id="editReferral" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Edit Referal</span> <span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="editReferralResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="admitpatient" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">New Patient </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="admitpatientResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="rap" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Rap </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="rapResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="final" class="abs window">
        <div class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Final </span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="finalResult" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="Billing_window" class="abs window">
        <div class="abs window_inner ">
            <div class="window_top ">
                <span class="float_left">Billing History</span><span class="float_right"><a href="javascript:void(0);"
                    class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                        href="javascript:void(0);" class="window_close"></a></span>
            </div>
            <div id="billingWindowResult" class="abs window_content">
            </div>
            <div class="abs window_bottom">
                Billing Landing Screen
            </div>
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
    <div id="cliamSummary" class="abs window">
        <div style="background-color: Transparent;" class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Claim Summary </span><span class="float_right"><a class="window_min"
                    href="javascript:void(0);"></a><a class="window_resize" href="javascript:void(0);">
                    </a><a class="window_close" href="javascript:void(0);"></a></span>
            </div>
            <div id="cliamSummaryResult" style="display: block;" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
                Cliam Summary
            </div>
            <span class="abs ui-resizable-handle ui-resizable-se"></span>
        </div>
    </div>
    <div id="pendingClaims" class="abs window">
        <div style="background-color: Transparent;" class="abs window_inner">
            <div class="window_top">
                <span class="float_left">Pending Claims </span><span class="float_right"><a class="window_min"
                    href="javascript:void(0);"></a><a class="window_resize" href="javascript:void(0);">
                    </a><a class="window_close" href="javascript:void(0);"></a></span>
            </div>
            <div id="pendingClaimsResult" style="display: block;" class="abs window_content general_form">
            </div>
            <div class="abs window_bottom">
                Cliam Summary
            </div>
            <span class="abs ui-resizable-handle ui-resizable-se"></span>
        </div>
    </div>
    <% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Schedule.js"))
       .OnDocumentReady(() =>
        {%>
    <%}); 
    %>
</asp:Content>
