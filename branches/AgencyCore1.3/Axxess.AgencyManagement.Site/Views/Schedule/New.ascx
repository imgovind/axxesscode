﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="newschedule" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">New Schedule</span><span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <div>
                <div class="fix">
                    <div class="row">
                        <div class="row divide">
                            <div class="fixedRow">
                                <label>
                                    Patient:</label></div>
                            <div class="inputs">
                                <span class="input_wrapper blank">
                                    <select name="list">
                                        <option value="0">All</option>
                                        <option value="1">Medicare (traditional)</option>
                                        <option value="2">Medicare (HMO/managed care)</option>
                                        <option value="3">Medicaid (traditional)</option>
                                        <option value="4">Medicaid (HMO/managed care) </option>
                                        <option value="5">Workers' compensation</option>
                                        <option value="6">Title programs </option>
                                        <option value="7">Other government</option>
                                        <option value="8">Private</option>
                                        <option value="9">Private HMO/managed care</option>
                                        <option value="10">Self Pay</option>
                                        <option value="11">Unknown</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                        <div class="row divide">
                            <div class="fixedRow">
                                <label>
                                    Employee:</label></div>
                            <div class="inputs">
                                <span class="input_wrapper blank">
                                    <select name="list">
                                        <option value="0">All</option>
                                        <option value="1">Medicare (traditional)</option>
                                        <option value="2">Medicare (HMO/managed care)</option>
                                        <option value="3">Medicaid (traditional)</option>
                                        <option value="4">Medicaid (HMO/managed care) </option>
                                        <option value="5">Workers' compensation</option>
                                        <option value="6">Title programs </option>
                                        <option value="7">Other government</option>
                                        <option value="8">Private</option>
                                        <option value="9">Private HMO/managed care</option>
                                        <option value="10">Self Pay</option>
                                        <option value="11">Unknown</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="discipline">
                            <li>
                                <input type="radio" name='discipline' />
                                Skilled Nurse </li>
                            <li>
                                <input type="radio" name='discipline' />
                                Home Health Aide</li>
                            <li>
                                <input type="radio" name='discipline' />
                                Physical Therapist</li>
                            <li>
                                <input type="radio" name='discipline' />
                                Speech Therapist</li>
                            <li>
                                <input type="radio" name='discipline' />
                                Occup. Therapist</li>
                            <li>
                                <input type="radio" name='discipline' />
                                Med Soc Worker</li>
                        </ul>
                    </div>
                    <%--<div class="row">
                <table style="width: 100%;">
                    <tr>
                        <td align="left" id="newScheduleCal1" valign="top" style="padding: 10px;">
                            loading calendar one...
                        </td>
                    </tr>
                    <tr>
                        <td align="left" id="newScheduleCal2" valign="top" style="padding: 10px;">
                            <ul class="visitType">
                                <li><span style="background-color: red;"></span>SN </li>
                                <li><span style="background-color: blue;"></span>HHA </li>
                                <li><span style="background-color: green;"></span>PT </li>
                                <li><span style="background-color: #8B008B;"></span>MSW </li>
                                <li><span style="background-color: #FFA07A;"></span>ST </li>
                                <li><span style="background-color: #9ACD32"></span>OT </li>
                                <li><span style="background-color: #FFFF00"></span>Multiple </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>--%>
                    <div class="row">
                      <%--  <div id="createCalendar1" class="clanderWidth">
                        </div>
                        <div id="createCalendar2" class="clanderWidth">
                        </div>
                        <div id="createCalendar3" class="clanderWidth">
                        </div>--%>
                    </div>
                </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Add</span></span>
                            <input id="Submit5" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button12" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset5"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="abs window_bottom">
    </div>
</div>
<span class="abs ui-resizable-handle ui-resizable-se"></span></div> 