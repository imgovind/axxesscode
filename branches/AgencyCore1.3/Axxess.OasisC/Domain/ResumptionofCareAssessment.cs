﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class ResumptionofCareAssessment : Assessment
    {
        public ResumptionofCareAssessment()
        {
            this.Type = AssessmentType.ResumptionOfCare;
        }
    }
}
