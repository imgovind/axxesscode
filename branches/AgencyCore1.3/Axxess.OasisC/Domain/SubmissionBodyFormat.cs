﻿namespace Axxess.OasisC.Domain
{
    using System;

    [Serializable]
    public class SubmissionBodyFormat
    {
        public short Id { get; set; }
        public string Item { get; set; }
        public double Length { get; set; }
        public double Start { get; set; }
        public double End { get; set; }
        public double RFA01 { get; set; }
        public double RFA02 { get; set; }
        public double RFA03 { get; set; }
        public double RFA04 { get; set; }
        public double RFA05 { get; set; }
        public double RFA06 { get; set; }
        public double RFA07 { get; set; }
        public double RFA08 { get; set; }
        public double RFA09 { get; set; }
        public double RFA10 { get; set; }
        public string RFA_REQ { get; set; }
        public string RFA_BLANK { get; set; }
        public string ElementName { get; set; }
        public string DataType { get; set; }
        public string PadType { get; set; }
        public string DefaultValue { get; set; }
    }
}
