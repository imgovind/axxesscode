﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class StartOfCareAssessment : Assessment
    {
        public StartOfCareAssessment()
        {
            this.Type = AssessmentType.StartOfCare;
        }
    }
}
