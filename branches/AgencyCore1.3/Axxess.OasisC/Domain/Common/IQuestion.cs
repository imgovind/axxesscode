﻿namespace Axxess.OasisC.Domain
{
    using Axxess.OasisC.Enums;

    public interface IQuestion
    {
        string Name { get; set; }
        string Answer { get; set; }
        QuestionType Type { get; set; }
    }
}
