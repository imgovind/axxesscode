﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class PlanofCareXml
    {
        #region Members

        private static XElement planofCareXml;

        public static XElement PlanofCareXmlInstance
        {
            get
            {
                if (planofCareXml == null)
                {
                    LoadPlanofCareXml();
                }
                return planofCareXml;
            }
        }

        private static void LoadPlanofCareXml()
        {
            string xmlLocation = HttpContext.Current.Server.MapPath("~/App_Data/xml/OasisC-Plan-of-care.xml");
            planofCareXml = XElement.Load(xmlLocation);
        }

        #endregion

        #region Static Methods

        private static List<Item> GetItemNodes(string node)
        {
            List<Item> items = null;
            string cacheKey = string.Format("PoC:{0}", node);
            if (!Cacher.TryGet(cacheKey, out items))
            {
                items = new List<Item>();
                var elements = PlanofCareXmlInstance.Elements(XName.Get(node)).Elements(XName.Get("Item"));
                if (elements != null && elements.Count() > 0)
                {
                    elements.ForEach(e =>
                    {
                        items.Add(new Item { Id = e.Attribute(XName.Get("id")).Value, Value = e.Attribute(XName.Get("value")).Value });
                    });

                    Cacher.Set(cacheKey, items);
                }
            }

            return items;
        }

        private static List<Sentence> GetSentenceNodes(string node)
        {
            List<Sentence> sentences = null;
            string cacheKey = string.Format("PoC:{0}", node);

            try
            {
                if (!Cacher.TryGet(cacheKey, out sentences))
                {
                    sentences = new List<Sentence>();

                    var elements = PlanofCareXmlInstance.Elements(XName.Get(node)).Elements(XName.Get("Sentence"));
                    if (elements.Any())
                    {
                        elements.ForEach(e =>
                        {
                            var sentence = new Sentence { Id = e.Attribute(XName.Get("id")).Value, Format = e.Attribute(XName.Get("format")).Value };
                            var fields = e.Elements(XName.Get("Field"));
                            if (fields.Any())
                            {
                                fields.ForEach(f =>
                                {
                                    var field = new Field { Name = f.Attribute(XName.Get("name")).Value };

                                    var options = f.Elements(XName.Get("Option"));
                                    if (options.Any())
                                    {
                                        options.ForEach(o =>
                                        {
                                            field.Options.Add(new Option { Text = o.Attribute(XName.Get("text")).Value, Value = o.Attribute(XName.Get("value")).Value });
                                        });
                                    }

                                    sentence.Fields.Add(field);
                                });
                            }

                            var items = e.Elements(XName.Get("Item"));
                            if (items.Any())
                            {
                                items.ForEach(i =>
                                {
                                    sentence.Items.Add(new Item
                                    {
                                        Id = i.Attribute(XName.Get("id")).Value,
                                        Value = i.Attribute(XName.Get("value")).Value,
                                        Name = i.Attribute(XName.Get("name")).Value
                                    });
                                });
                            }

                            sentences.Add(sentence);
                        });

                        Cacher.Set(cacheKey, sentences);
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Log Exception
            }

            return sentences;
        }

        private static string BuildSentences(string answers, List<Sentence> sentenceList, IDictionary<string, Question> questions)
        {
            var sentenceBuilder = new StringBuilder();

            try
            {
                string[] answerArray = answers.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                answerArray.ForEach(a =>
                {
                    var sentence = sentenceList.Find(s =>
                    {
                        if (s.Id.IsEqual(a))
                        {
                            return true;
                        }
                        return false;
                    });

                    if (sentence != null)
                    {
                        var parameters = new List<string>();
                        sentence.Fields.ForEach(f =>
                        {
                            if (f.Options.Count > 0)
                            {
                                foreach (Option option in f.Options)
                                {
                                    if (questions[f.Name] != null && questions[f.Name].Answer == option.Value)
                                    {
                                        parameters.Add(option.Text);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                parameters.Add(questions[f.Name] != null ? questions[f.Name].Answer : string.Empty);
                            }
                        });

                        sentence.Items.ForEach(i =>
                        {
                            if (questions[i.Name] != null && questions[i.Name].Answer.Contains(i.Id))
                            {
                                parameters.Add(i.Value);
                            }
                            else
                            {
                                parameters.Add(string.Empty);
                            }
                        });

                        if (parameters.Count > 0)
                        {
                            sentenceBuilder.AppendFormat(sentence.Format, parameters.ToArray());
                        }
                        else
                        {
                            sentenceBuilder.Append(sentence.Format);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                //TODO: Log ex;
            }

            return sentenceBuilder.ToString();
        }

        public static string LookupText(string node, string answer)
        {
            var stringBuilder = new StringBuilder();
            if (node.IsNotNullOrEmpty() && answer.IsNotNullOrEmpty())
            {
                var items = GetItemNodes(node);
                string[] answerArray = answer.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                int index = 0;
                answerArray.ForEach(a =>
                {
                    Item item = items.Find(i =>
                    {
                        if (i.Id.IsEqual(a))
                        {
                            return true;
                        }
                        return false;
                    });

                    if (item != null)
                    {
                        if (index == answerArray.Length - 1)
                        {
                            stringBuilder.Append(item.Value);
                        }
                        else
                        {
                            stringBuilder.AppendFormat("{0}, ", item.Value);
                        }
                    }
                    index++;
                });
            }
            return stringBuilder.ToString();
        }

        public static string ExtractText(string type, IDictionary<string, Question> questions)
        {
            string sentences = string.Empty;

            if (type.IsEqual("NutritionalRequirements"))
            {
                var nutritionRequirements = questions["485NutritionalReqs"].Answer;
                if (nutritionRequirements.IsNotNullOrEmpty())
                {
                    sentences += BuildSentences(nutritionRequirements, GetSentenceNodes(type), questions);
                }
            }
            else
            {
                var items = questions.Where(q => q.Value.Type == QuestionType.PlanofCare && (q.Key.EndsWith(type) || q.Key.EndsWith(string.Format("{0}s", type))));

                items.ForEach(q =>
                {
                    var answers = questions[q.Key].Answer;
                    if (answers.IsNotNullOrEmpty())
                    {
                        sentences += BuildSentences(answers, GetSentenceNodes(q.Key.Replace("485", "")), questions);
                    }
                });
            }

            return sentences;
        }

        #endregion

    }
}
