﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;

    public static class Databases
    {
        public static void InsertAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Add<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Add<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Add<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Add<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Add<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Add<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Add<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Add<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static void UpdateAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Update<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Update<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Update<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Update<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Update<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Update<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Update<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Update<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static Assessment FindAny(this SimpleRepository repository, string assessmentType, Guid assessmentId ,Guid agencyId)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    assessment = repository.Single<StartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessment = repository.Single<ResumptionofCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessment = repository.Single<FollowUpAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessment = repository.Single<RecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "TransferInPatientDischarged":
                case "OASISCTransferDischarge":
                case "OASISCTransferOT":
                case "OASISCTransferPT":
                    assessment = repository.Single<TransferDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessment = repository.Single<DeathAtHomeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
                    break;
                default:
                    break;
            }
            return assessment;
        }
        public static Assessment FindAny(this SimpleRepository repository,Guid agencyId ,Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    assessment = repository.Single<StartOfCareAssessment>(a => a.AgencyId== agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessment = repository.Single<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessment = repository.Single<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessment = repository.Single<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "TransferInPatientDischarged":
                case "OASISCTransferDischarge":
                case "OASISCTransferOT":
                case "OASISCTransferPT":
                    assessment = repository.Single<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessment = repository.Single<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                default:
                    break;
            }
            return assessment;
        }

       //public static Assessment FindAny(this SimpleRepository repository,Guid agencyId ,Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType)
    //{
    //    Assessment assessment = null;
    //    switch (assessmentType)
    //    {
    //        case "StartOfCare":
    //        case "OASISCStartofCare":
    //        case "OASISCStartofCarePT":
    //            assessment = repository.Single<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "ResumptionOfCare":
    //        case "OASISCResumptionofCare":
    //        case "OASISCResumptionofCareOT":
    //        case "OASISCResumptionofCarePT":
    //            assessment = repository.Single<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "FollowUp":
    //        case "OASISCFollowUp":
    //        case "OASISCFollowUpOT":
    //        case "OASISCFollowUpPT":
    //            assessment = repository.Single<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "Recertification":
    //        case "OASISCRecertification":
    //        case "OASISCRecertificationOT":
    //        case "OASISCRecertificationPT":
    //            assessment = repository.Single<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "TransferInPatientNotDischarged":
    //            assessment = repository.Single<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "TransferInPatientDischarged":
    //        case "OASISCTransferDischarge":
    //        case "OASISCTransferOT":
    //        case "OASISCTransferPT":
    //            assessment = repository.Single<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "DischargeFromAgencyDeath":
    //        case "OASISCDeath":
    //        case "OASISCDeathOT":
    //        case "OASISCDeathPT":
    //            assessment = repository.Single<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        case "DischargeFromAgency":
    //        case "OASISCDischarge":
    //        case "OASISCDischargeOT":
    //        case "OASISCDischargePT":
    //            assessment = repository.Single<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
    //            break;
    //        default:
    //            break;
    //    }
    //    return assessment;
    //}

       public static IList<Assessment> FindAnyByStatus(this SimpleRepository repository,Guid agencyId ,string assessmentType, int status)
        {
            IList<Assessment> assessments = new List<Assessment>();
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    assessments = repository.Find<StartOfCareAssessment>(a =>a.AgencyId==agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessments = repository.Find<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessments = repository.Find<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "TransferInPatientNotDischarged":
                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "TransferInPatientDischarged":
                case "OASISCTransferDischarge":
                case "OASISCTransferOT":
                case "OASISCTransferPT":
                    assessments = repository.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Status == status).Cast<Assessment>().ToList();
                    break;
                default:
                    break;
            }
            return assessments;
        }
    }
}
