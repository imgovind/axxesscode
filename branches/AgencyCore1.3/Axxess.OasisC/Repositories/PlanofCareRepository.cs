﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;

    using Domain;

    using SubSonic.Repository;

    public class PlanofCareRepository : IPlanofCareRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PlanofCareRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IPlanofCareRepository Member

        public bool Add(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                planofCare.Created = DateTime.Now;
                planofCare.Modified = DateTime.Now;
                database.Add<PlanofCare>(planofCare);
                return true;
            }
            return false;
        }

        public bool Update(PlanofCare planofCare)
        {
            bool result = false;

            if (planofCare != null)
            {
                planofCare.Modified = DateTime.Now;
                database.Update<PlanofCare>(planofCare);
                result = true;
            }
            return result;
        }

        public PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        }

        public PlanofCare Get(Guid agencyId, Guid id)
        {
            return database.Single<PlanofCare>(p => p.Id == id && p.AgencyId == agencyId);
        }

        public List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            if (status > 0)
            {
                return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.Status == status).ToList();
            }
            return new List<PlanofCare>();
        }

        #endregion

    }
}
