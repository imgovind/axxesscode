﻿
namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IAssessmentRepository
    {
        bool Add(Assessment oasisAssessment);
        bool Update(Assessment oasisAssessment);
        Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId);
        Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId);
        Assessment GetWithNoList(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType);
        List<Assessment> GetAllByStatus(Guid agencyId, int status);
    }
}
