﻿using System;

using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Domain;

namespace Axxess.ConsoleApp.Tests
{
    public static class FluentAdoTest
    {
        private static void Run()
        {
            string sql = @"select * from users where id = @userid";

            var user = new FluentCommand<User>(sql)
                .SetConnection(DatabaseConnection.GetConnection("server=10.0.3.31;user=appdata;database=agencymanagement;password=@gencyC0re;"))
                .AddGuid("userid", new Guid("13AA1C55-CE6E-4C75-AA30-6387ADD2E85B"))
                .SetMap(reader => new User
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName")
                })
                .AsSingle();


            if (user != null)
            {
                Console.WriteLine("User Nationality: {0}", user.DisplayName);
            }
        }
    }
}
