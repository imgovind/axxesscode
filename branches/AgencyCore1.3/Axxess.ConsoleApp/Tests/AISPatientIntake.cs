﻿using System;
using System.IO;
using System.Data;

using Excel;

namespace Axxess.ConsoleApp.Tests
{
    public static class AISPatientIntake
    {
        private static string input = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "KAN.xlsx");
        private static string output = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("KAN_{0}.txt", DateTime.Now.Ticks.ToString()));

        const string agencyId = "b95cb587-587f-45e9-95a8-2698fdd4ec0d"; // Agency Id
        const string agencyLocationId = "E6B50988-57F2-49B2-A97D-3580F52697CD"; // Location Id
        const string state = "TX";  // TX
        const string patientStatusId = "1";  // Pending
        const string claimStatusId = "300"; //
        const string ethnicity = "5;"; // White
        const string maritalStatus = "Married"; // Married
        const string created = "CURDATE()"; // Current Date
        const string modified = "CURDATE()"; // Current Date
        const string episodeDetails = "<EpisodeDetail />"; // No Episode Details
        const string episodeSchedule = "<ArrayOfScheduleEvent />"; // No Scheduled Events
        const string medication = "<ArrayOfMedication />"; // No Medication
        const string diagnosis = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>"; // Diagnosis

        const string PATIENT_INSERT = "INSERT INTO `patients_copy`(`Id`,`AgencyId`,`AgencyLocationId`,`PatientIdNumber`,`MedicareNumber`," +
            "`FirstName`,`LastName`,`MiddleInitial`,`DOB`,`Gender`,`Ethnicities`,`MaritalStatus`,`AddressLine1`,`AddressLine2`," +
            "`AddressCity`,`AddressStateCode`,`AddressZipCode`,`StartofCareDate`,`Status`,`Created`,`Modified`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', {18}, {19}, {20});";

        const string MEDPROFILE_INSERT = "INSERT INTO `medicationprofiles_copy`(`Id`,`PatientId`,`AgencyId`,`Medication`,`Created`,`Modified`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', {4}, {5});";

        const string EPISODE_INSERT = "INSERT INTO `patientepisodes_copy`(`Id`, `AgencyId`, `PatientId`, `Details`, `StartDate`, `EndDate`," +
            "`Schedule`, `Created`, `Modified`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8});";

        const string RAP_INSERT = "INSERT INTO `raps_copy`(`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`PatientIdNumber`,`EpisodeStartDate`, " +
            "`EpisodeEndDate`,`IsOasisComplete`,`IsFirstBillableVisit`,`FirstBillableVisitDate`,`Modified`,`Created`,`FirstName`,`LastName`," +
            "`DOB`,`Gender`,`AddressLine1`,`AddressLine2`,`AddressCity`,`AddressStateCode`,`AddressZipCode`,`StartofCareDate`,`DiagonasisCode`," +
            "`HippsCode`,`ClaimKey`,`PrimaryInsuranceId`,`Status`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8}, '{9}', {10}, {11}, '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', {25}, {26});";

        const string FINAL_INSERT = "INSERT INTO `finals_copy`(`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`PatientIdNumber`,`EpisodeStartDate`," +
            "`EpisodeEndDate`,`FirstBillableVisitDate`,`MedicareNumber`,`FirstName`,`LastName`,`DOB`,`Gender`," +
            "`AddressLine1`,`AddressLine2`,`AddressCity`,`AddressStateCode`,`AddressZipCode`,`StartofCareDate`," +
            "`DiagonasisCode`,`HippsCode`,`ClaimKey`,`Modified`,`Created`,`PrimaryInsuranceId`,`Status`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', " +
            "'{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', {22}, {23}, {24}, {25});";

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    var patientId = Guid.NewGuid();
                                    var patientNumber = row.GetValue(5);
                                    var gender = row.GetValue(3) == "M" ? "Male" : "Female";
                                    var medicareNumber = row.GetValue(4);
                                    var firstName = row.GetValue(0); 
                                    var lastName = row.GetValue(1);
                                    var middleInitial = row.GetValue(2);
                                    var birthDate = row.GetValue(8).ToMySqlDate(0);
                                    var addressLine1 = row.GetValue(9);
                                    var addressLine2 = row.GetValue(10);
                                    var addressCity = row.GetValue(11);
                                    var zipCode = row.GetValue(13).Substring(0, 5);
                                    var startofCareDate = row.GetValue(6).ToMySqlDate(0);

                                    textWriter.WriteLine(PATIENT_INSERT, patientId, agencyId, agencyLocationId, patientNumber,  medicareNumber, 
                                        firstName, lastName, middleInitial, birthDate, gender, ethnicity, maritalStatus, addressLine1,
                                        addressLine2, addressCity, state, zipCode, startofCareDate, patientStatusId, created, modified);

                                    textWriter.WriteLine(MEDPROFILE_INSERT, Guid.NewGuid(), patientId, agencyId, medication, created, modified);

                                    if (row.GetValue(16) != string.Empty)
                                    {
                                        var episodeId = Guid.NewGuid();
                                        var episodeStart = row.GetValue(16).ToMySqlDate(0);
                                        var episodeEnd = row.GetValue(16).ToMySqlDate(59);
                                        var firstBillableDate = row.GetValue(15).ToMySqlDate(0);
                                        var hippsCode = row.GetValue(19);
                                        var claimKey = row.GetValue(20);

                                        textWriter.WriteLine(EPISODE_INSERT, episodeId, agencyId, patientId, episodeDetails, episodeStart, 
                                            episodeEnd, episodeSchedule, created, modified);

                                        textWriter.WriteLine(RAP_INSERT, Guid.NewGuid(), agencyId, patientId, episodeId, patientNumber, episodeStart,
                                            episodeEnd, 0, 0, firstBillableDate, modified, created, firstName, lastName, birthDate, gender,
                                            addressLine1, addressLine2, addressCity, state, zipCode, startofCareDate, diagnosis, hippsCode, claimKey, 
                                            0, claimStatusId);

                                        textWriter.WriteLine(FINAL_INSERT, Guid.NewGuid(), agencyId, patientId, episodeId, patientNumber, episodeStart,
                                            episodeEnd, firstBillableDate, medicareNumber, firstName, lastName, birthDate, gender,
                                            addressLine1, addressLine2, addressCity, state, zipCode, startofCareDate, diagnosis, hippsCode, claimKey,
                                            modified, created, 0, claimStatusId);

                                    }
                                    textWriter.Write(textWriter.NewLine);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
