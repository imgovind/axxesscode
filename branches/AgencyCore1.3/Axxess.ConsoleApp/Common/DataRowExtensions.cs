﻿using System;
using System.Data;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp
{
    public static class DataRowExtensions
    {
        public static string GetValue(this DataRow dataRow, int columnIndex)
        {
            if (dataRow != null && columnIndex < dataRow.ItemArray.Length)
            {
                if (dataRow[columnIndex] != null && dataRow[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRow[columnIndex].ToString().Trim();
                }
            }
            return string.Empty;
        }

        public static string ToMySqlDate(this string text, int addDays)
        {
            if (text.IsNotNullOrEmpty())
            {
                var date = text.ToDateTime();
                if (addDays > 0)
                {
                    return date.AddDays(addDays).ToString("yyyy-M-d");
                }
                return date.ToString("yyyy-M-d");
            }
            return string.Empty;
        }
        public static bool IsCBSAValid(this DataRow dataRow, params string[] inputs)
        {
            bool result=true;
            if (dataRow!=null && inputs.Length > 0)
            {
                foreach(var input in inputs){
                    if(input.IsNullOrEmpty()){
                        return false;
                    }
                }
            }
            return result;
        }
    }  
}
