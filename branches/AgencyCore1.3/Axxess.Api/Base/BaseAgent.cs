﻿namespace Axxess.Api
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;

    using Axxess.Api.Contracts;

    public abstract class BaseAgent<T> : IDisposable where T : class, IService 
    {
        private bool disposed;
        public delegate void ServiceProxyDelegate<T>(T proxy);

        private T service;
        public T Service
        {
            get
            {
                if (service == null)
                {
                    service = GetServiceInstance(this.ToString());
                }
                return service;
            }
        }

        private static ChannelFactory<T> channelFactory;
        public static T GetServiceInstance(string endPointName)
        {
            if (channelFactory == null)
            {
                channelFactory = new ChannelFactory<T>(endPointName);
                channelFactory.Faulted += (sender, e) =>
                {
                    channelFactory.Abort();
                };

            }
            return channelFactory.CreateChannel();
        }

        public static void Call(ServiceProxyDelegate<T> proxyDelegate, string endPointName)
        {
            T service = GetServiceInstance(endPointName);

            try
            {
                proxyDelegate(service);
            }
            catch (TimeoutException timeoutException)
            {
                Windows.EventLog.WriteEntry(timeoutException.ToString(), EventLogEntryType.Error);
            }
            catch (FaultException<DefaultFault> faultException)
            {
                Windows.EventLog.WriteEntry(faultException.ToString(), EventLogEntryType.Error);
            }
            catch (CommunicationException communicationException)
            {
                Windows.EventLog.WriteEntry(communicationException.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                var channel = (ICommunicationObject)service;
                if (channel.State == CommunicationState.Faulted)
                {
                    channel.Abort();
                }
                else
                {
                    try
                    {
                        channel.Close();
                    }
                    catch
                    {
                        channel.Abort();
                    }
                }
            }
        }

        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (channelFactory.State == CommunicationState.Faulted)
                {
                    channelFactory.Abort();
                }
                else
                {
                    try
                    {
                        channelFactory.Close();
                    }
                    catch
                    {
                        channelFactory.Abort();
                    }
                }
                channelFactory = null;
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
