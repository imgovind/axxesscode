<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.IndexViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="teaser_view">
    <%= (Model.IncludeDefaultStyles) ? ForumViewHelper.GetDefaultStyles() : ""%>
    <% if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
    <%= Model.Message.IsNotNullOrEmpty() ? "<div class='openforum_message'>" + Model.Message + "</div>" : ""%>
    <div class="openforum_maincontent"><% int i = 0;
    foreach(var item in Model.Posts ?? new Post[0]) { i++; %>
        <div class="message">
            <div class="replies"><span class="num block"><%= item.ReplyCount.ToString() %></span>Replies</div>
            <div class="views"><span class="num block"><%= item.ViewCount %></span>Views</div>
            <%= string.Format("<a href=\"/Forum/view/{0}?title={1}\" class=\"title\" id=\"title{2}\"></a>",item.Id,ForumViewHelper.ToUrlFriendlyTitle(item.Title),i.ToString()) %>
            <%= string.Format("<script type=\"text/javascript\"> prevent_overflow(\"{0}\",document.getElementById(\'{1}\')); </script>", Regex.Replace(Regex.Replace(item.Title, "<[^>]*>", ""), "[^a-zA-Z0-9-_.,:;! ]", ""), "title" + i.ToString())%>
            <div class="creation">created on <%= item.CreatedDate.ToString("MM/dd/yyyy hh:mm tt") %> by <%= Html.Encode(item.CreatedBy) %></div>
            <div class="last_activity">last activity <%= item.LastPostDate.ToString("MM/dd/yyyy hh:mm tt") %> by <%= Html.Encode(item.LastPostBy) %></div>
            <%= string.Format("<div class=\"content\" id=\"content{0}\"></div>", i.ToString())%>
            <%= string.Format("<script type=\"text/javascript\"> prevent_overflow(\"{0}\",document.getElementById(\'{1}\')); </script>", Regex.Replace(Regex.Replace(item.Body, "<[^>]*>", ""), "[^a-zA-Z0-9-_.,:;! ]", ""), "content" + i.ToString())%>
        </div><%
    } %>
    </div>
    <ul class="post_controls"><%
    if ((Model.Posts ?? new Post[0]).Count() > 0) { %>
        <li><span class="block">Page <%= Model.CurrentPage + 1 %> of <%= Model.TotalPages %></span></li><%
    }
    if (Model.CurrentPage > 0) {
        Html.BeginForm(); %>
        <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Previous Page</a><input type="hidden" name="searchQuery" value="<%= Model.SearchQuery %>" /><input type="hidden" name="page" value="<%= Model.CurrentPage - 1 %>" /></li><%
        Html.EndForm();
    }
    if (Model.CurrentPage < Model.TotalPages - 1) {
        Html.BeginForm(); %>
        <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Next Page</a><input type="hidden" name="searchQuery" value="<%= Model.SearchQuery %>" /><input type="hidden" name="page" value="<%= Model.CurrentPage + 1 %>" /><%
        Html.EndForm();
    } %>
    </ul>
</div>

</asp:Content>
