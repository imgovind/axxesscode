﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons">
    <ul>
        <li><a class="groupLink" name="" type="" onclick="User.LoadUserSchedule('PatientName');">Group By Patient</a></li>
        <li><a class="groupLink" name="" type="" onclick="User.LoadUserSchedule('VisitDate');">Group By Date</a></li>
        <li><a class="groupLink" name="" type="" onclick="User.LoadUserSchedule('TaskName');">Group ByTask</a></li>
    </ul>
</div>
<div id="myScheduledTasksContentId"><% Html.RenderPartial("~/Views/User/ScheduleGrid.ascx"); %></div>