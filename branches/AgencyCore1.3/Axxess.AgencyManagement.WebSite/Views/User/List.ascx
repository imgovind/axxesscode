﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Users | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listusers');</script>")%>
<% using (Html.BeginForm("Users", "Export", FormMethod.Post)) { %>
<% var action = string.Empty;
   if (Current.HasRight(Permissions.ManageUsers))
   {
       action = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUser('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Deactivate('<#=Id#>');\">Deactivate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('<#=Id#>');\">Delete</a>";
   }    
 %>
<div class="wrapper">
    <%= Html.Telerik().Grid<User>().Name("List_User").ToolBar(commnds => commnds.Custom()).Columns(columns =>{
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(false);
    columns.Bound(u => u.TitleType).Title("Title").Sortable(true).Width(180);
    columns.Bound(u => u.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(200).Sortable(false);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(110);
    columns.Bound(u => u.EmploymentType).Sortable(false).Width(120);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(80);
    columns.Bound(u => u.Id).Width(180).Sortable(false).ClientTemplate(action).Title("Action");
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "User")).Pageable(paging => paging.PageSize(15)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
    $("#List_User .t-grid-toolbar").html("");
<% if (Current.HasRight(Permissions.ManageUsers)) { %>
    $("#List_User .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewUser(); return false;%22%3ENew User%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    $("#List_User .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
