﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<% using (Html.BeginForm("Update", "User", FormMethod.Post, new { @id = "editUserForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_User_Id" }) %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>User Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_User_LastName">Last Name:</label><div class="float_right"><%=Html.TextBox("LastName", Model.LastName, new { @id = "Edit_User_LastName", @maxlength = "30", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_FirstName">First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_User_FirstName", @maxlength = "30", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_Suffix">Suffix:</label><div class="float_right"><%= Html.TextBox("Profile.Suffix", Model.Suffix, new { @id = "Edit_User_Suffix", @maxlength = "30", @class = "" })%></div></div>
            <div class="row"><label for="Edit_User_TitleType">Title:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", Model.TitleType, new { @id = "Edit_User_TitleType", @class = "TitleType valid" })%></div></div>
            <div id="Edit_User_TitleOther_Div" class="row"><label for="Edit_User_OtherTitleType">Other Title (specify):</label><div class="float_right"><%=Html.TextBox("TitleTypeOther", Model.TitleTypeOther, new { @id = "Edit_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row"></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_User_AddressLine1">Address Line 1:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "Edit_User_AddressLine1", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressLine2">Address Line 2:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "Edit_User_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressCity">City:</label><div class="float_right"><%=Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_User_AddressCity", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressStateCode">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_User_AddressStateCode", @class = "AddressStateCode valid" })%><%=Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="Edit_User_HomePhoneArray1">Home Phone:</label><div class="float_right"><%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_User_MobilePhoneArray1">Mobile Phone:</label><div class="float_right"><%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <table class="form">
            <tbody>
                <tr class="firstrow">
                    <td><input id="Edit_User_Role_1" type="checkbox" class="radio required" name="AgencyRoleList" value="1" />&nbsp;<label for="Edit_User_Role_1">Administrator</label></td>
                    <td><input id="Edit_User_Role_2" type="checkbox" class="radio required" name="AgencyRoleList" value="2" />&nbsp;<label for="Edit_User_Role_2">Director of Nursing</label></td>
                    <td><input id="Edit_User_Role_3" type="checkbox" class="radio required" name="AgencyRoleList" value="3" />&nbsp;<label for="Edit_User_Role_3">Case Manager</label></td>
                    <td><input id="Edit_User_Role_4" type="checkbox" class="radio required" name="AgencyRoleList" value="4" />&nbsp;<label for="Edit_User_Role_4">Nursing</label></td>
                    <td><input id="Edit_User_Role_5" type="checkbox" class="radio required" name="AgencyRoleList" value="5" />&nbsp;<label for="Edit_User_Role_5">Clerk (non-clinical)</label></td>
                </tr><tr>
                    <td><input id="Edit_User_Role_6" type="checkbox" class="radio required" name="AgencyRoleList" value="6" />&nbsp;<label for="Edit_User_Role_6">Physical Therapist</label></td>
                    <td><input id="Edit_User_Role_7" type="checkbox" class="radio required" name="AgencyRoleList" value="7" />&nbsp;<label for="Edit_User_Role_7">Occupational Therapist</label></td>
                    <td><input id="Edit_User_Role_8" type="checkbox" class="radio required" name="AgencyRoleList" value="8" />&nbsp;<label for="Edit_User_Role_8">Speech Therapist</label></td>
                    <td><input id="Edit_User_Role_9" type="checkbox" class="radio required" name="AgencyRoleList" value="9" />&nbsp;<label for="Edit_User_Role_9">Medicare Social Worker</label></td>
                    <td><input id="Edit_User_Role_10" type="checkbox" class="radio required" name="AgencyRoleList" value="10" />&nbsp;<label for="Edit_User_Role_10">Home Health Aide</label></td>
                </tr><tr>
                    <td><input id="Edit_User_Role_11" type="checkbox" class="radio required" name="AgencyRoleList" value="11" />&nbsp;<label for="Edit_User_Role_11">Scheduler</label></td>
                    <td><input id="Edit_User_Role_12" type="checkbox" class="radio required" name="AgencyRoleList" value="12" />&nbsp;<label for="Edit_User_Role_12">Biller</label></td>
                    <td><input id="Edit_User_Role_13" type="checkbox" class="radio required" name="AgencyRoleList" value="13" />&nbsp;<label for="Edit_User_Role_13">Quality Assurance</label></td>
                    <td><input id="Edit_User_Role_14" type="checkbox" class="radio required" name="AgencyRoleList" value="14" />&nbsp;<label for="Edit_User_Role_14">Physician</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a id="Edit_User_SaveButton" href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edituser');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
<% Html.Telerik().TabStrip().Name("editUserTabStrip").Items(tabstrip => { 
       tabstrip.Add().Text("Licenses").HtmlAttributes(new { id = "editUserLicenses_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
            <% Html.RenderPartial("Licenses", Model.Id); %>
        <% });%>
        <% tabstrip.Add().Text("Permissions").HtmlAttributes(new { id = "editPermissions_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
        <% using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "editUserPermissionsForm" })) { %>
            <%= Html.Hidden("UserId", Model.Id, new { @id = "Edit_UserPermission_UserId" }) %>
                <div class="main">
                    <fieldset>
                        <legend></legend>
                        <div class="wide_column"><input id="Edit_User_AllPermissions" type="checkbox" class="radio" value="" />&nbsp;<label for="Edit_User_AllPermissions">Select all permissions</label></div>
                        <div class="column">
                             <%= Html.PermissionList("General", Model.PermissionsArray) %>
                             <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
                             <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
                         </div><div class="column">
                             <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
                             <%= Html.PermissionList("QA", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Case Management", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
                         </div>
                     </fieldset>
                     <div class="buttons"><ul>
                        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update Permissions</a></li>
                    </ul></div>
                 </div>
             <% } %>
        <% });%>
<% }).SelectedIndex(0).Render();%>
    


