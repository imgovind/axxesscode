﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('",
        Current.User.DisplayName,
        "&rsquo;s Schedule and Tasks','listuserschedule');</script>")%>
<%= Html.Telerik().Grid<UserVisit>().Name("List_User_Schedule").Columns(columns => {
        columns.Bound(v => v.PatientName);
        columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width(250);
        columns.Bound(v => v.VisitDate).Title("Date").Width(100);
        columns.Bound(v => v.StatusName).Width(200).Title("Status");
        columns.Bound(v => v.VisitNotes).Title("&nbsp;").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=VisitNotes#>\"></a>");
        columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
        columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Missed Visit Form</a>").Title(" ").Width(150);
    })
    .Groupable(settings => settings.Groups(groups =>
    {
        var data = ViewData["UserScheduleGroupName"].ToString();
        if (data == "PatientName")
        {
            groups.Add(s => s.PatientName);
        }
        else if (data == "VisitDate")
        {
            groups.Add(s => s.VisitDate);
        }
        else if (data == "TaskName")
        {
            groups.Add(s => s.TaskName);
        }
        else
        {
            groups.Add(s => s.VisitDate);
        }
    }))
    .ClientEvents(c => c.OnRowDataBound("Schedule.tooltip"))
    .DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleList", "User"))
    .Scrollable().Sortable() %>
<script type="text/javascript">
    $(".t-group-indicator").hide();
    $(".t-grouping-header").remove();
    $(".t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
</script>