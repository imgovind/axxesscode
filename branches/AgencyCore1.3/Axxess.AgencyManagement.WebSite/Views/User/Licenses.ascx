﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="main">
    <% using (Html.BeginForm("AddLicense", "User", FormMethod.Post, new { @id = "editUserLicenseForm" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "Edit_UserLicense_Id" }) %>
        <fieldset>
            <legend></legend>
            <table class="form layout_auto"><tbody>
                <tr>
                    <td>License Type</td>
                    <td>Other License Type</td>
                    <td>Initiation Date</td>
                    <td>Expiration Date</td>
                    <td colspan="2">Attachment</td>
                </tr>
                <tr>
                    <td><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "Edit_UserLicense_Type", @class = "valid" })%></td>
                    <td><%= Html.TextBox("OtherLicenseType", "", new { @id="Edit_UserLicense_TypeOther", @class="valid", @maxlength="25" }) %></td>
                    <td><%= Html.Telerik().DatePicker().Name("InitiationDate").Value(DateTime.Now).HtmlAttributes(new { @id = "Edit_UserLicense_InitDate", @class = "text required date" })%></td>
                    <td><%= Html.Telerik().DatePicker().Name("ExpirationDate").Value(DateTime.Now).HtmlAttributes(new { @id = "Edit_UserLicense_ExpDate", @class = "text required date" })%></td>
                    <td><input type="file" name="Attachment" id="Edit_UserLicense_Attachment" /></td>
                    <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add License</a></li></ul></div></td>
                </tr>
            </tbody></table>
        </fieldset>
    <% } %>
    <div class="currentlicenses">
        <%= Html.Telerik().Grid<License>().Name("List_User_Licenses").DataKeys(keys =>  { keys.Add(M => M.Id); }).Columns(columns => {
            columns.Bound(l => l.LicenseType).ReadOnly().Sortable(false);
            columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Initiation Date").Sortable(false);
            columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
            columns.Bound(l => l.AssetUrl).Title("Attachment").ReadOnly().Sortable(false);
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(135).Title("Action");
            }).DataBinding(dataBinding => dataBinding.Ajax()
                .Select("LicenseList", "User", new { userId = Model })
                .Update("UpdateLicense", "User", new { userId = Model })
                .Delete("DeleteLicense", "User", new { userId = Model }))
            .Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true))
        %>
    </div>
</div>
