﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Message>" %>
<div class="messageContent" id="messageContent">
    <div class="messageContentPanel" id="messageContentPanel">
        <div class="messageHeaderContainer">
            <div class="messageHeaderRow">
                <label for="messageSender">
                    From:</label><span id="messageSender"><%= Model.FromName %></span></div>
            <div class="messageHeaderRow">
                <label for="messageRecipients">
                    To:</label><span id="messageRecipients"><%= Model.RecipientNames %></span></div>
            <div class="messageHeaderRow">
                <label for="messageDate">
                    Sent:</label><span id="messageDate"><%= Model.MessageDate %></span></div>
            <div class="messageHeaderRow">
                <label for="messageSubject">
                    Subject:</label><span id="messageSubject"><%= Model.Subject %></span></div>
            <div class="messageHeaderRow">
                <label for="messageSubject">
                    Regarding:</label><span id="messageRegarding"><%= Model.PatientName %></span></div>
            <div class="messageHeaderRow messageHeaderButtons">
                <div class="buttons float_left"><ul>
                    <li><a href="javascript:void(0);" id="MessageReplyButton">Reply</a></li>
                    <li><a href="javascript:void(0);" id="MessageForwardButton">Forward</a></li>
                    <li><a href="javascript:void(0);" id="MessageDeleteButton">Delete</a></li>
                </ul></div>
            </div>
        </div>
        <div class="messageBodyContainer"><div id="messageBody"><%= Model.Body %></div></div>
    </div>
</div>
<input type="hidden" id="messageId" value='<%= Model.Id %>' />
