﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Axxess.Core.Infrastructure" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('",
        Axxess.AgencyManagement.App.Current.User.DisplayName,
        "&rsquo;s Dashboard','homepage');</script>")%>
<ul id="widget_col1" class="widgets">
    <li class="widget" id="intro_widget">
        <div class="widget-head"><h5>Hello,&nbsp;<%= Current.User.DisplayName %>!</h5></div>
        <div class="widget-content">
            <div class="align_center"><span class="bigtext">Welcome to</span><br /><img src="/images/agencycore_logo.png" alt="AgencyCore Logo" /></div>
            <div>AgencyCore&trade; is a secure Enterpise Agency Management software designed from the ground up to provide powerful online capabilities that enables real-time collaboration for you and your team.</div>
        </div>
    </li><li class="widget" id="news_widget">
        <div class="widget-head"><h5>News/Updates</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/NewsFeed.ascx"); %>    
        </div>
    </li><li class="widget" id="recertsdue_widget">
        <div class="widget-head"><h5>Past Due Recerts</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/RecertsPastDue.ascx"); %>
        </div>
    </li>
</ul><ul id="widget_col2" class="widgets">
    <li class="widget" id="local_widget">
        <div class="widget-head"><h5>Local</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/Local.ascx"); %>
        </div>
    </li><li class="widget" id="schedule_widget">
        <div class="widget-head"><h5>My Scheduled Tasks</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/Schedule.ascx"); %>
        </div>
    </li><li class="widget" id="upcomingrecert_widget">
        <div class="widget-head"><h5>Upcoming Recerts</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/RecertsUpcoming.ascx"); %>
        </div>
    </li>
</ul><ul id="widget_col3" class="widgets">
    <% if (Current.HasRight(Permissions.Messaging)) { %><li class="widget" id="messages_widget">
        <div class="widget-head"><h5>Messages</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/Messages.ascx"); %>
        </div>
    </li><% } %><li class="widget" id="birthday_widget">
        <div class="widget-head"><h5>Patient Birthdays</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/Birthday.ascx"); %>
        </div>
    </li><li class="widget" id="Li1">
        <div class="widget-head"><h5>Outstanding Claims</h5></div>
        <div class="widget-content">
            <% Html.RenderPartial("~/Views/Widget/Claims.ascx"); %>
        </div>
    </li>
</ul>