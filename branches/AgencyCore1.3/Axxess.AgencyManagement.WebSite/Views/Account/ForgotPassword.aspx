﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PasswordReset>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reset Password - Axxess Home Health Management System</title>
    <link type="text/css" href="/Content/account.css" rel="Stylesheet" />
    <link href="/images/icons/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="forgotPassword-wrapper">
        <div id="forgotpassword-window">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Axxess&trade; Forgot Password</span></div>
            <div class="box">
                <div id="messages"></div>
                <% using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "forgotPasswordForm", @class = "forgotPassword" })) %>
                <% { %>
                <div class="row">
                    <%= Html.LabelFor(m => m.UserName) %>
                    <%= Html.TextBoxFor(m => m.UserName, new { @class = "required" })%>
                </div>
                <div class="row">
                    <%= Html.LabelFor(m => m.captchaValid) %>
                    <span>Enter both words below, separated by a space. </span>
                    <%= Html.GenerateCaptcha() %>
                </div>
                <div class="row tr">
                    <input type="submit" value="Send" class="button" style="width: 90px!important;" />
                    <br />
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/validate.min.js")
             .Add("/Plugins/form.min.js")
             .Add("/Plugins/blockui.min.js")
             .Add("/Modules/Utility.js")
             .Add("/Modules/Account.min.js")
             .Compress(true))
        .OnDocumentReady(() =>
        { 
    %>
    ResetPassword.Init();
    <% 
        }).Render(); %>
</body>
</html>
