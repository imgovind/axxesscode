﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PasswordChange>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Change Password - Axxess Home Health Management System</title>
    <link type="text/css" href="/Content/account.css" rel="Stylesheet" />
    <link href="/images/icons/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="changepassword-wrapper">
        <div id="changepassword-window">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Axxess&trade; Password Change</span></div>
            <div class="box">
                <div id="messages"></div>
                <% using (Html.BeginForm("ChangePassword", "Account", FormMethod.Post, new { @id = "changePasswordForm", @class = "changePassword" })) %>
                <% { %>
                <img src="images/gui/axxess_logo.jpeg" alt="Axxess Logo" id="logo" />
                <div class="row">
                    <%= Html.LabelFor(m => m.CurrentPassword) %>
                    <%= Html.PasswordFor(m => m.CurrentPassword, new { @class = "required" }) %>
                </div>
                <div class="row">
                    <%= Html.LabelFor(m => m.NewPassword) %>
                    <%= Html.PasswordFor(m => m.NewPassword, new { @class = "required" }) %>
                    <div id="iSM">
                        <ul class="weak">
                            <li id="iWeak">WEAK</li>
                            <li id="iMedium">MEDIUM</li>
                            <li id="iStrong"">STRONG</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <%= Html.LabelFor(m => m.NewPasswordConfirm) %>
                    <%= Html.PasswordFor(m => m.NewPasswordConfirm, new { @class = "required" }) %>
                </div>
                <div class="bottom-right">
                    <input type="submit" value="Change" class="button" style="width: 90px!important;" />
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/validate.min.js")
             .Add("/Plugins/form.min.js")
             .Add("/Plugins/blockui.min.js")
             .Add("/Plugins/passwordstrength.min.js")
             .Add("/Modules/Utility.js")
             .Add("/Modules/Account.min.js")
             .Compress(true))
        .OnDocumentReady(() =>
        { 
    %>
    ChangePassword.Init();
    <% 
        }).Render(); %>
</body>
</html>