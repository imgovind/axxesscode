﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientContactInfoViewData>" %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Address Listing</legend>
        <div class="column">
            <div class="row"><label for="Report_Patient_AL_BranchCode" class="float_left">Branch:</label><div class="float_right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = "Report_Patient_AL_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientAddressListing();">Generate Report</a></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div class="ReportGrid">
        <%= Html.Telerik().Grid<AddressBookEntry>().Name("Report_Patient_AL_Grid").Columns(columns =>
           {
               columns.Bound(e => e.Name).Width(140);
               columns.Bound(e => e.AddressFirstRow).Title("Address");
               columns.Bound(e => e.AddressCity).Title("City").Width(90);
               columns.Bound(e => e.AddressStateCode).Title("State").Width(50);
               columns.Bound(e => e.AddressZipCode).Title("Zip Code").Width(60);
               columns.Bound(e => e.PhoneHome).Title("Home Phone").Width(110);
               columns.Bound(e => e.PhoneMobile).Title("Mobile Phone").Width(110);
               columns.Bound(e => e.EmailAddress).Width(110);
           })
           .DataBinding(dataBinding => dataBinding.Ajax().Select("PatientAddressList", "Report", new { AddressBranchCode = Guid.Empty }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$(".t-grid-content").css({ 'height': 'auto' });</script>
