﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleEmployeeListing"; %>
<h3>
    Patient By Responsible Employee Listing</h3>
<fieldset>
    <div class="row">
        <label for="AddressStateCode">
            Patient Name:</label><%= Html.LookupSelectList(SelectListTypes.Patients, pagename + "_Name", "", new { @id = pagename + "_Name", @class = "Name report_input valid", @tabindex = "1" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            State:</label><%= Html.LookupSelectList(SelectListTypes.States, pagename + "_AddressStateCode", "", new { @id = pagename + "_AddressStateCode", @class = "AddressStateCode report_input valid", @tabindex = "2" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            Branch:</label><%= Html.LookupSelectList(SelectListTypes.Branches, pagename + "_AddressBranchCode", "", new { @id = pagename + "_AddressBranchCode", @class = "AddressBranchCode report_input valid", @tabindex = "3" })%>
    </div>
    <input id="report_step" type="hidden" name="step" class="report_input" value="2" />
    <input id="report_action" type="hidden" name="action" value="<%= pagename %>" />
    <p>
        <input type="button" value="Generate Report" onclick="Report.populateReport($(this));" />
    </p>
</fieldset>
<div id="<%= pagename %>Result" class="ReportGrid">
    <h3>
    <% =Html.Telerik().Grid<PatientRoster>()
                                 .Name(pagename + "Grid")        
         .Columns(columns =>
     {
     columns.Bound(p => p.ResponsibleEmployee);
     columns.Bound(p => p.PatientLastName);
     columns.Bound(p => p.PatientFirstName);
     columns.Bound(p => p.PatientSoC).Title("SOC Date");
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename+"Result", "Report", new { Name = Guid.Empty, AddressStateCode = " ", AddressBranchCode = Guid.Empty, ReportStep = "" }))
   .Sortable()
   
   .Selectable()
   .Scrollable()
   .Footer(false)
  
    %>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>