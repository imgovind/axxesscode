﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Statistical Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Statistical/ReferralSource" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Admission By Referral Source</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/ContactList" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Admission By Diagnosis Code</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/Birthdays" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Discharge by Discharge Type</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/ServiceLength" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patient Statistics By Age, Race, Sex</span></a></li> 
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Statistical/License" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patients By Insurance Company</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/PatientVisits" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patient Visit History</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/EmployeeVisits" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Employee Visit History</span></a></li> 
                </ul>
                <div class="clear">&nbsp;</div>
            </div>
        </div>
    </li>
</ul>