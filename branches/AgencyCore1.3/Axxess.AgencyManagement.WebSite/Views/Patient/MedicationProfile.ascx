﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Medication Profile | ",
            (Model.Patient.LastName + ", " + Model.Patient.FirstName).ToTitleCase(),
        "','medicationprofile');</script>")%>
<% using (Html.BeginForm("SaveMedicationProfile", "Patient", FormMethod.Post, new { @id = "newMedicationProfileForm" })) { %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<% = Html.Hidden("Id", Model.MedicationProfile.Id, new  {@id="medicationProfileHistroyId" })%>
<% = Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<div class="visitContainer">
    <table class="nursing">
        <tr>
            <th>
                <div class="third">
                    <span class="float_left strong">Patient Name:</span>
                    <span class="float_right normal"><%= Html.TextBox("Name", Model.Patient.FirstName+" "+ Model.Patient.LastName, new { @readonly = "readonly" } ) %></span>
                </div><div class="third">
                    <span class="float_left strong">Patient ID:</span>
                    <span class="float_right normal"><%= Html.TextBox("ID", Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber : string.Empty, new { @readonly = "readonly" } ) %></span>
                </div><div class="third">
                    <span class="float_left strong">Episode/Period:</span>
                    <span class="float_right normal"><%= Html.TextBox("Eps", string.Format(" {0} – {1}", Model != null && Model.StartDate != null ? Model.StartDate.ToShortDateString() : string.Empty, Model != null && Model.StartDate != null ? Model.EndDate.ToShortDateString() : string.Empty), new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Primary Diagnosis:</span>
                    <span class="float_right normal"><%= Html.TextBox("PriDia", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Secondary Diagnosis:</span>
                    <span class="float_right normal"><%= Html.TextBox("SecDia", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Tertiary Diagnosis:</span>
                    <span class="float_right normal"><%= Html.TextBox("TerDia", data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : string.Empty, new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Allergies:</span>
                    <span class="float_right normal"><%= Html.TextBox("Allergies", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : string.Empty, new { @readonly = "readonly" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Pharmacy Name:</span>
                    <span class="float_right normal"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "MedicationProfile_PharmacyName" })%></span>
                </div><div class="third">
                    <span class="float_left strong">Pharmacy Phone:</span>
                    <span class="float_right normal"><%= Html.TextBox("PharmacyPhone", Model.PharmacyPhone, new { @id = "MedicationProfile_PharmacyPhone" })%></span>
                </div>
            </th>
        </tr>
    </table>
    <div class="buttons" style="text-align:right;">
        <ul>
            <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadMedicationProfileSnapshot('<%= Model.Patient.Id%>');">Sign Medication Profile</a></li>
            <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="acore.openprintview('/Patient/MedicationProfilePrint/<%=Model.Patient.Id %>');">Print Medication Profile</a></li>
        </ul>
    </div>
    <table class="nursing">
        <tr>
            <th>
                <div class="newRow one medication align_left">
                    <%= Html.Telerik().Grid<Medication>()
                    .Name("MedicatonProfileGridActive")
                    .DataKeys(keys => {
                        keys.Add(M => M.Id);
                    })
                    .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "MedicatonProfileGridInsertButton", style = "margin-left:0" }))
                    .DataBinding(dataBinding => {
                        dataBinding.Ajax()
                            .Select("Medication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "Active" })
                            .Insert("InsertMedication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "Active" })
                            .Update("UpdateMedication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "Active" })
                            .Delete("DeleteMedication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "Active" });
                    })
                    .Columns(columns => {
                        columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
                        columns.Bound(M => M.StartDate).Format("{0:d}").Width(90);
                        columns.Bound(M => M.MedicationDosage).Title("Medication & Dosage");
                        columns.Bound(M => M.Route).Title("Route");
                        columns.Bound(M => M.Frequency).Title("Frequency");
                        columns.Bound(M => M.MedicationType).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
                        columns.Bound(M => M.Classification);
                        columns.Bound(M => M.DCDate).Format("{0:d}").Title("Discharge").Width(100); ;
                        columns.Command(commands => {
                            commands.Edit();
                            commands.Delete();
                        }).Width(140).Title("Commands");
                    }).ClientEvents(e => e.OnEdit("Patient.onMedicationProfileEdit").OnRowDataBound("Patient.onMedicationProfileRowDataBound").OnDataBound("Patient.OnDataBound"))
                    .Editable(editing => editing.Mode(GridEditMode.InLine)).Scrollable().Sortable().Footer(false) 
                    %>
                </div>
                <div class="newRow one medication align_left">
                    <%= Html.Telerik().Grid<Medication>()
                    .Name("MedicatonProfileGridDC")
                    .DataKeys(keys => {
                        keys.Add(M => M.Id);
                    })
                    .DataBinding(dataBinding => {
                        dataBinding.Ajax()
                            .Select("Medication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "DC" })
                            .Update("UpdateMedication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "DC" })
                            .Delete("DeleteMedication", "Patient", new { MedId = Model.MedicationProfile.Id, medicationCategory = "DC" });
                    })
                    .Columns(columns => {
                        columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
                        columns.Bound(M => M.StartDate).Format("{0:d}").Width(90);
                        columns.Bound(M => M.MedicationDosage).Title("Medication & Dosage");
                        columns.Bound(M => M.Route).Title("Route");
                        columns.Bound(M => M.Frequency).Title("Frequency");
                        columns.Bound(M => M.MedicationType).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
                        columns.Bound(M => M.Classification);
                        columns.Bound(M => M.DCDate).Format("{0:d}").Title("Date").Width(100); 
                        columns.Command(commands => {
                            commands.Edit();
                            commands.Delete();
                        }).Width(150).Title("Commands");
                    }).ClientEvents(e => e.OnEdit("Patient.onMedicationProfileEdit").OnRowDataBound("Patient.onMedicationProfileRowDataBound"))
                    .Editable(editing => editing.Mode(GridEditMode.InLine)).Scrollable().Sortable().Footer(false) 
                    %>
                </div>
            </th>
        </tr>
    </table>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('medicationprofile');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<div id="dischargeMedicationProfileDialog" class="hidden dischargeMedication">
    <form id="dischargeMedicationProfileForm" action="/Patient/UpdateMedicationForDischarge" method="post">
        <fieldset>
            <legend>Discharge Medication</legend>
            <div class="wide_column">
                <div class="row">
                    <label for="">Enter the discharge date:</label>
                    <div class="float_right"><%= Html.Telerik().DatePicker().Name("DischargeDate").Value(DateTime.Today).HtmlAttributes(new { @id = "MedicationProfile_DischargeDate", @class = "text date" })%></div>
                </div>
            </div>
        </fieldset>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
                <li><a href="javascript:void(0);" onclick="U.closeDialog('');">Cancel</a></li>
            </ul>
        </div>
    </form>
</div>
<script type="text/javascript">
    $("#MedicatonProfileGridInsertButton").html("<span class=\"t-add t-icon\"></span>Add New Medication");
</script>