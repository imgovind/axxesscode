﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%= Html.Telerik().Grid<Medication>()
        .Name("OasisMedicatonGrid")
        .DataKeys(keys =>  {
            keys.Add(M => M.Id);
        })
        .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "OasisMedicatonGridInsertButton", style = "margin-left:0" }))
        .DataBinding(dataBinding => {
            dataBinding.Ajax()
                .Select("Medication", "Patient", new { MedId = Model, medicationCategory = "Active" })
                .Insert("InsertMedication", "Patient", new { MedId = Model, medicationCategory = "Active" })
                .Update("UpdateMedication", "Patient", new { MedId = Model, medicationCategory = "Active" })
                .Delete("DeleteMedication", "Patient", new { MedId = Model, medicationCategory = "Active" });
        })
        .Columns(columns => {
            columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
            columns.Bound(M => M.StartDate).Format("{0:d}").Width(100);
            columns.Bound(M => M.MedicationDosage).Title("Name & Dose").Width(110);
            columns.Bound(M => M.Route).Title("Route").Width(110);
            columns.Bound(M => M.Frequency).Title("Frequency").Width(110);
            columns.Bound(M => M.MedicationType).Title("Type").Width(100).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
            columns.Bound(M => M.Classification).Width(120);
            columns.Bound(M => M.DCDate).Format("{0:d}").Title("D/C (Date)").Width(80);
            columns.Command(commands => {
                commands.Edit();
                commands.Delete();
            }).Width(135).Title("Commands");
        }).ClientEvents(e => e.OnEdit("Patient.onMedicationProfileEdit").OnRowDataBound("Patient.onMedicationProfileRowDataBound")).Editable(editing => editing.Mode(GridEditMode.InLine))
        .Pageable().Scrollable().Sortable()        
%>
<div id="dischargeMedicationDialog" class="hidden dischargeMedication">
    <form id="dischargeMedicationForm" action="/Oasis/UpdateMedicationForDischarge" method="post">
    <fieldset>
        <legend>Discharge Medication</legend>
        <br />
        <div class="wide_column">
            <div class="row">
                <label for="">
                    Enter the discharge date:</label><div class="float_right">
                        <%= Html.Telerik().DatePicker().Name("DischargeDate").Value(DateTime.Today).HtmlAttributes(new { @id = "Medication_DischargeDate", @class = "text date" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();">Cancel</a></li>
    </ul></div>
    </form>
</div>

<script type="text/javascript">
    $("#OasisMedicatonGridInsertButton").html("<span class=\"t-add t-icon\"></span>Add New Medication");
    $(".t-last").width(135);
</script>