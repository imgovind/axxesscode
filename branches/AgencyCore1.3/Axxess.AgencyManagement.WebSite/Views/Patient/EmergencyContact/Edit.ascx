﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEmergencyContact>" %>
<% using (Html.BeginForm("EditEmergencyContact", "Patient", FormMethod.Post, new { @id = "editEmergencyContactForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model.PatientId, new { @id = "txtEdit_EmergencyContactPatientID" })%>
<%=Html.Hidden("Id", Model.Id, new { @id ="txtEdit_EmergencyContactID" })%>
  <fieldset>
        <legend></legend>
         <div class="column">
                 <div class="row">
                        <label for="FirstName" class="float_left">  First Name:</label><div class="float_right"> <%=Html.TextBox("FirstName", Model.FirstName, new { @id = "txtEdit_EmergencyContact_FirstName", @class = "text input_wrapper", @maxlength = "100", @tabindex = "1" })%></div>    
                  </div>
                 <div class="row">
                        <label for="LastName" class="float_left">Last Name:</label><div class="float_right">  <%=Html.TextBox("LastName", Model.LastName, new { @id = "txtEdit_EmergencyContact_LastName", @class = "text input_wrapper", @maxlength = "100", @tabindex = "2" })%></div>
                  </div>
                   <div class="row">
                        <label for="Relationship" class="float_left">Relationship:</label><div  class="float_right"><%=Html.TextBox("Relationship", Model.Relationship, new { @id = "txtEdit_EmergencyContact_Relationship", @class = "text input_wrapper", @tabindex = "3" })%></div>
                    </div>
                
                  </div>
                   <div class="column">
                    <div class="row">
                        <label for="PhoneHome" class="float_left">Primary Phone:</label>
                          <div  class="float_right">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 3 ? Model.PrimaryPhone.Substring(0, 3) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "4" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 6 ? Model.PrimaryPhone.Substring(3, 3) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray2", @class = "autotext required digits phone_short",  @maxlength = "3", @size = "3", @tabindex = "5" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length > 6 ? Model.PrimaryPhone.Substring(6) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray3", @class = "autotext required digits phone_long",@maxlength = "4", @size = "5", @tabindex = "6" })%>
                            </span>
                        </div>
                    </div>
                 <div class="row" style="vertical-align: middle;">
                        <label for="Email" class="float_left">Email:</label><div  class="float_right"><%=Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "txtEdit_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "7" })%></div>
                    </div>
                
                 <div class="row">
                        <label for="SetPrimary" class="float_left">Set Primary:</label><div  class="float_left"><%=Html.CheckBox("IsPrimary",Model.IsPrimary ,new { @id = "txtEdit_EmergencyContact_SetPrimary" })%></div>
                    </div>
                  </div>
  </fieldset>
  <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editemergencycontact');">Cancel</a></li>
        </ul>
    </div>
<%} %>
