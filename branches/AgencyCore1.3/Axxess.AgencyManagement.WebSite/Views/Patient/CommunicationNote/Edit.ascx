﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>

<% using (Html.BeginForm("Update", "CommunicationNote", FormMethod.Post, new { @id = "editCommunicationNoteForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="column">
            <div class="row"><label for="Edit_CommunicationNote_PatientName" class="float_left">Patient Name:</label><div class="float_right"><span class="bigtext"><%= Model.DisplayName %></span></div></div>
            <div class="row"><label for="Edit_CommunicationNote_Date" class="float_left">Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("Created").Value(Model.Created).HtmlAttributes(new { @id = "Edit_CommunicationNote_Date", @class = "text required date" })%></div></div>
        </div>
        <table class="forms"><tbody>
            <tr><td><label for="Edit_CommunicationNote_Text">Communication Text</label><br /><textarea id="Edit_CommunicationNote_Text" name="Text" cols="5" rows="12" style="height: 120px;"><%= Model.Text %></textarea></td></tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editcommnote');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
<%} %>
