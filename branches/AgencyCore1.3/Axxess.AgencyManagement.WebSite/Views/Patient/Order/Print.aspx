﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PhysicianOrder>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Physician Order<%= Model != null && Model.Patient != null ? (" | " + (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body></body><%
Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("/Modules/printview.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader = printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            '<%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Agency != null && Model.Agency.MainLocation != null ? (Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : "") + (Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : "") + "%3Cbr /%3E" + (Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") + (Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : "") + (Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : "") : ""%>' +
            "%3C/td%3E%3Cth colspan=%222%22 class=%22h1%22%3EPhysician Order%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToTitleCase() : "") + ", " + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToTitleCase() : "") + " " + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() : "") : ""%>' +
            "%3C/span%3E%3Cbr /%3E" +
            '<%= Model != null && Model.Patient != null && Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Patient != null && Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Patient != null && Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase() + ", " : ""%><%= Model != null && Model.Patient != null && Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode.ToTitleCase() + " &nbsp;" : ""%><%= Model != null && Model.Patient != null && Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode + "%3Cbr /%3E" : ""%><%= Model != null && Model.Patient != null && Model.Patient.PhoneHome.IsNotNullOrEmpty() ? Model.Patient.PhoneHome.ToPhone() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Patient != null && Model.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + Model.Patient.MedicareNumber : ""%>' +
            "%3C/td%3E%3Ctd colspan=%223%22%3E%3Cspan%3EPhysician Name: " +
            '<%= Model != null && Model.Physician != null ? (Model.Physician.LastName.IsNotNullOrEmpty() ? Model.Physician.LastName.ToTitleCase() + ", " : "") + (Model.Physician.FirstName.IsNotNullOrEmpty() ? Model.Physician.FirstName.ToTitleCase() : "") : "" %>' +
            "%3C/span%3E%3Cbr /%3E" +
            '<%= Model != null && Model.Physician != null && Model.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Physician.AddressLine1.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Physician != null && Model.Physician.AddressLine2.IsNotNullOrEmpty() ? Model.Physician.AddressLine2.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Physician != null && Model.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Physician.AddressCity.ToTitleCase() + ", " : ""%><%= Model != null && Model.Physician != null && Model.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Physician.AddressStateCode.ToTitleCase() + " &nbsp;" : ""%><%= Model != null && Model.Physician != null && Model.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Physician.AddressZipCode + "%3Cbr /%3E" : ""%><%= Model != null && Model.Physician != null && Model.Physician.PhoneWork.IsNotNullOrEmpty() ? Model.Physician.PhoneWork.ToPhone() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Physician != null && Model.Physician.FaxNumber.IsNotNullOrEmpty() ? Model.Physician.FaxNumber.ToPhone() + "%3Cbr /%3E" : ""%><%= Model != null && Model.Physician != null && Model.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + Model.Physician.NPI : ""%>' +
            "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
        printview.addsection(
            printview.col(3,
                printview.span("%3Cstrong%3EOrder Date:%3C/strong%3E <%= Model != null && Model.OrderDate != null ? Model.OrderDate.ToShortDateString() : "<span class='blank'></span>"%>") +
                printview.span("%3Cstrong%3EOrder #:%3C/strong%3E <%= Model != null && Model.OrderNumber != null ? Model.OrderNumber.ToString() : "<span class='blank'></span>"%>") +
                printview.span("%3Cstrong%3EDate Received:%3C/strong%3E <%= Model != null && Model.ReceivedDate != null ? Model.ReceivedDate.ToShortDateString() : "<span class='blank'></span>"%>")));
        printview.addsection(printview.span("<%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : "" %>",0,2),"Allergies");
        printview.addsection(
            printview.col(2,
                printview.span("Episode:",1) +
                printview.span("<%= Model != null ? Model.EpisodeStartDate + " &ndash; " + Model.EpisodeEndDate : "" %>",0,1) +
                printview.span("Diagnosis:",1) +
                printview.col(2,
                    printview.span("<%= Model != null ? Model.PrimaryDiagnosisText : "" %>",0,1) +
                    printview.span("<%= Model != null ? Model.PrimaryDiagnosisCode : "" %>",0,1) +
                    printview.span("<%= Model != null ? Model.SecondaryDiagnosisText : "" %>",0,1) +
                    printview.span("<%= Model != null ? Model.SecondaryDiagnosisCode : "" %>",0,1))),
            "Episode");
        printview.addsection(printview.col(1,printview.span("<%= Model != null ? Model.Summary : "" %>",0,10)),"Summary");
        printview.addsection(printview.col(1,printview.span("<%= Model != null ? Model.Text : "" %>",0,10)),"Orders");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature",1) +
                printview.span("Date") +
                printview.span("<%= Model != null ? Model.SignatureText : "" %>",0,1) +
                printview.span("<%= Model != null ? Model.SignatureDate.ToShortDateString() : "" %>",0,1)));
        printview.addsection(
            printview.col(2,
                printview.span("Physician Signature",1) +
                printview.span("Date") +
                printview.span("<%= Model != null ? Model.PhysicianSignatureText : "" %>",0,1) +
                printview.span("<%= Model != null ? Model.ReceivedDate.ToShortDateString() : "" %>",0,1)));
            <%
     }).Render(); %>
</html>