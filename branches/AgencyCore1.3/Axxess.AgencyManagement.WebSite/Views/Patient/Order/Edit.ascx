﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianOrder>" %>
<% using (Html.BeginForm("Update", "Order", FormMethod.Post, new { @id = "editOrderForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Order_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Order_PatientId" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Order_EpisodeId" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Order_UserId" })%>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row"><label for="Edit_Order_PatientName" class="float_left">Patient Name:</label><div class="float_right"><span class="bigtext"><%= Model.DisplayName %></span></div></div>
            <div class="row"><label for="Edit_Order_PhysicianDropDown" class="float_left">Physician:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "PhysicianId",  Model.PhysicianId.ToString(), new { @id = "Edit_Order_PhysicianDropDown" })%></div></div>
            <div class="row"><label for="Edit_Order_Date" class="float_left">Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("OrderDate").Value(Model.OrderDate).HtmlAttributes(new { @id = "Edit_Order_Date", @class = "text required date" })%></div></div>
        </div>
        <div class="clear"></div>
        <table class="forms"><tbody>
            <tr><td><label for="Edit_Order_Summary">Summary (Optional)</label><br /><%= Html.TextBox("Summary", Model.Summary, new { @id = "Edit_Order_Summary", @class = "", @maxlength = "100", @style = "width: 600px;" })%></td></tr>
            <tr><td><label for="Edit_Order_Text">Order Description</label><br /><textarea id="Edit_Order_Text" name="Text" cols="5" rows="12" style="height: 120px;"><%= Model.Text %></textarea></td></tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
                <div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Order_ClinicianSignature", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignatureDate" class="bigtext float_left">Signature Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("SignatureDate").Value(DateTime.Today).MaxDate(DateTime.Today).HtmlAttributes(new { @id = "Edit_Order_ClinicianSignatureDate", @class = "date required" })%></div>
            </div>
        </div>
    </fieldset><%= Html.Hidden("Status", Model.Status, new { @id = "Edit_Order_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$('#Edit_Order_Status').val('110');$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="$('#Edit_Order_Status').val('125');$(this).closest('form').submit();">Complete</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editorder');">Cancel</a></li>
    </ul></div>
</div>
<%} %>