﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="winmenu">
    <ul>
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%=Model.Id %>');" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');">New Order</a></li><% } %>
        <li><a href="javascript:void(0);" onclick="Patient.LoadNewCommunicationNote('<%=Model.Id %>');" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');">New Communication Note</a></li>
    </ul>
</div>
<div class="twothirds">
    <div class="float_left"><% if (!Model.PhotoId.IsEmpty()) { %><img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" /><% } else { %><img src="/Images/blank_user.jpeg" alt="User Photo" /><% } %><div class="clear"></div><% if (Current.HasRight(Permissions.ManagePatients)) { %><div class="align_center">[ <a href="javascript:void(0);" onclick="UserInterface.ShowNewPhotoModal('<%= Model.Id%>');">Change Photo</a> ] </div><% } %></div>
    <div class="column">
        <div class="shortrow"><span class="bigtext"><%=Model.DisplayName%></span></div>
        <div class="shortrow"><label class="float_left">Birthday:</label><span><%= Model.DOB.ToString("m") %></span></div>
        <% if (Model.PhoneHome.IsNotNullOrEmpty()) { %><div class="shortrow"><label class="float_left">Primary Phone:</label><span><%= Model.PhoneHome.ToPhone() %></span></div><% } %>
        <div class="shortrow"><label class="float_left">Start of Care Date:</label><span><%= Model.StartOfCareDateFormatted %></span></div>
        <% if (Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0) { %>
        <% var physician = Model.PhysicianContacts.SingleOrDefault(p => p.Primary); %>
        <div class="shortrow"><label class="float_left">Physician Name:</label><span><%= physician!=null? physician.DisplayName.Trim():"" %></span></div>
        <% } %>
        <div class="shortrow"><% if (Current.HasRight(Permissions.ManagePatients)) { %>[ <a href="javascript:void(0);" onclick="UserInterface.ShowEditPatient('<%= Model.Id%>'); ">Edit</a> ]&nbsp; <% } %>[ <a href="javascript:void(0);" onclick="Patient.InfoPopup($(this));">More</a> ]</div>
    </div>
</div>
<div class="onethird encapsulation">
    <h4>Quick Reports</h4>
    <ul>
<% if (Current.HasRight(Permissions.ViewPatientProfile)) { %>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="acore.openprintview('/Patient/Profile/<%=Model.Id %>')">Patient Profile</a></li>
<% } %>    
<% if (Current.HasRight(Permissions.ViewMedicationProfile)) { %>
       
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadMedicationProfile('<%= Model.Id%>');">Medication Profile</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadMedicationProfileSnapshotHistory('<%= Model.Id%>');">Medication Snapshot History</a></li>
 <% } %>
    </ul>
</div>
<fieldset class="notelegend">
    <ul>
        <li><img src="/Images/Icons/note.png">Visit Comments</li>
        <li><img src="/Images/Icons/note_blue.png">Episode Comments</li>
        <li><img src="/Images/Icons/note_red.png">Missed/Returned</li>
    </ul>
</fieldset>
<div class="clear"></div>
<div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowScheduleCenter('<%= Model.Id %>');">Schedule Activity</a></li></ul></div>
