﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper">
    <%= Html.Telerik().Grid<MedicationProfileHistory>().Name("List_MedicationProfileSnapshotHistory_Grid").Columns(columns => {
        columns.Bound(m => m.UserName).Title("Signed By").Sortable(false);
        columns.Bound(m => m.SignedDateFormatted).Title("Signed Date").Sortable(false);
        columns.Bound(m => m.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Patient/MedicationProfileSnapshotPrint/<#=Id#>');\">Print View</a>").Sortable(false).Title("Action");
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("MedicationSnapshotHistory", "Patient", new { PatientId = Model.Id })).Pageable(paging => paging.PageSize(15)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $(".t-grid-content").css({ 'height': 'auto' });
</script>

