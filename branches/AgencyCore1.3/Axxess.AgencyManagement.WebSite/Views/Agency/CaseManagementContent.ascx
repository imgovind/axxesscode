﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= Html.Telerik().Grid<ScheduleEvent>().Name("caseManagementGrid").Columns(columns => {
            columns.Bound(s => s.PatientName);
            columns.Bound(s => s.EventDate).Width(100);
            columns.Bound(s => s.DisciplineTaskName).ClientTemplate("<#=PrintUrl#>").Title("Task").Width(250);
            columns.Bound(s => s.StatusName).Width(200);
            columns.Bound(s => s.UserName).Width(150);
        })
        .Groupable(settings => settings.Groups(groups => {
            var data = ViewData["GroupName"].ToString();
            if (data == "PatientName")
            {
                groups.Add(s => s.PatientName);
            }
            else if (data == "EventDate")
            {
                groups.Add(s => s.EventDate);
            }
            else if (data == "DisciplineTaskName")
            {
                groups.Add(s => s.DisciplineTaskName);
            }
            else if (data == "UserName")
            {
                groups.Add(s => s.UserName);
            }
            else
            {
                groups.Add(s => s.EventDate);
            }
        }))
        .DataBinding(dataBinding => dataBinding.Ajax().Select("CaseManagementGrid", "Agency"))
        .Scrollable().Sortable()
%>
<script type="text/javascript">
    $(".t-group-indicator").hide();
    $(".t-grouping-header").remove();
    $(".t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
</script>