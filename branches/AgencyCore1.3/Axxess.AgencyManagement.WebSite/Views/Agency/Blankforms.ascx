﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div class="form_wrapper">
    <fieldset>
        <legend>OASIS Forms</legend>
        <div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/StartofCare/View/Blank')">OASIS-C Start of Care</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/ResumptionOfCare/View/Blank')">OASIS-C Resumption of Care</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/Recertification/View/Blank')">OASIS-C Recertification</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/Followup/View/Blank')">OASIS-C Follow-Up</a></div>
        </div>
        <div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/TransferNotDischarge/View/Blank')">OASIS-C Transfer Not Discharged</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/TransferDischarge/View/Blank')">OASIS-C Transfer and Discharge</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/Death/View/Blank')">OASIS-C Death at Home</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/Discharge/View/Blank')">OASIS-C Discharge from Agency</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHACarePlan/View/Blank')">HHA Care Plan</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/TransferSummary/View/Blank')">Transfer Summary</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHAVisitNote/View/Blank')">HHA Visit Note</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/SixtyDaySummary/View/Blank')">60 Day Summary</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/MissedVisit/View/Blank')">Missed Visit Form</a></div>
        </div><div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/LVNSupervisoryVisit/View/Blank')">LVN Supervisory Visit</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHAideSupervisoryVisit/View/Blank')">HHA Supervisory Visit</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/DischargeSummary/View/Blank')">Discharge Summary</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/SNVisit/View/Blank')">Skilled Nurse Visit</a></div>
        </div>
    </fieldset>
</div>