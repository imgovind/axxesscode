﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<%  using (Html.BeginForm("Update", "Physician", FormMethod.Post, new { @id = "editPhysicianForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Physician_Id" })%>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Physician | ",
            Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase() : "",
            "','editphysician');</script>")%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Physician_FirstName" class="float_left">First Name:</label><div class="float_right"><%=Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Physician_FirstName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%></div></div>
            <div class="row"><label for="Edit_Physician_MiddleIntial" class="float_left">MI:</label><div class="float_right"><%= Html.TextBox("MiddleName", Model.MiddleName, new { @id = "Edit_Physician_MiddleIntial", @class = "text input_wrapper mi" })%></div></div>
            <div class="row"><label for="Edit_Physician_LastName" class="float_left">Last Name:</label><div class="float_right"><%=Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Physician_LastName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "49" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="Edit_Physician_Credentials" class="float_left">Credentials:</label><div class="float_right"><%=Html.TextBox("Credentials", Model.Credentials, new { @id = "Edit_Physician_Credentials", @class = "text input_wrapper required", @maxlength = "40", @tabindex = "49" })%></div></div>
            <div class="row"><label for="Edit_Physician_NpiNumber" class="float_left">NPI No:</label><div class="float_right">  <%=Html.TextBox("NPI", Model.NPI, new { @id = "Edit_Physician_NpiNumber", @class = "text input_wrapper digits", @maxlength = "10", @tabindex = "57" })%></div></div>
            <div class="row"><div id="Edit_Physician_PecosCheck" class="hidden"></div></div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Physician_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"> <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Physician_AddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"> <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Physician_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressCity" class="float_left">City:</label><div class="float_right"> <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Physician_AddressCity", @class = "text input_wrapper required", @tabindex = "60" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressStateCode" class="float_left"> State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Physician_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Physician_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Physician_Phone1" class="float_left">Primary Phone:</label><div class="float_right"><%=Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Physician_Phone1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Physician_Phone2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Physician_Phone3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Physician_Fax1" class="float_left">Fax Number:</label><div class="float_right"><%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Physician_Fax1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Physician_Fax2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Physician_Fax3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Physician_Email" class="float_left">Email:</label><div class="float_right">  <%=Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Physician_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "56" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editphysician');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
