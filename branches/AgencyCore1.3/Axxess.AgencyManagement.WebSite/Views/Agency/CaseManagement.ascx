﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons">
    <ul>
        <li><a class="groupLink" onmouseover="$('#caseman_patient').css({'background':'#5d8cc9'});" onmouseout="$('#caseman_patient').css({'background':'#fff'});" onclick="Agency.loadCaseManagement('PatientName');">Group By Patient</a></li>
        <li><a class="groupLink" onmouseover="$('#caseman_date').css({'background':'#5d8cc9'});" onmouseout="$('#caseman_date').css({'background':'#fff'});" onclick="Agency.loadCaseManagement('EventDate');">Group By Date</a></li>
        <li><a class="groupLink" onmouseover="$('#caseman_task').css({'background':'#5d8cc9'});" onmouseout="$('#caseman_task').css({'background':'#fff'});" onclick="Agency.loadCaseManagement('DisciplineTaskName');">Group By Task</a></li>
        <li><a class="groupLink" onmouseover="$('#caseman_clinician').css({'background':'#5d8cc9'});" onmouseout="$('#caseman_clinician').css({'background':'#fff'});" onclick="Agency.loadCaseManagement('UserName');">Group By Clinician</a></li>
    </ul>
</div>
<div id="caseManagementContentId"> <% Html.RenderPartial("~/Views/Agency/CaseManagementContent.ascx", Model); %></div>