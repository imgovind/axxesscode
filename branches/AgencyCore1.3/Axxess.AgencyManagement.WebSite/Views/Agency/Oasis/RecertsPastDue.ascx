﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Past Due Recerts | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listpastduerecerts');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<RecertEvent>().Name("List_PastDueRecerts").Columns(columns => {
    columns.Bound(r => r.PatientName).Sortable(false);
    columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(false).Width(120);
    columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
    columns.Bound(r => r.TargetDate).Title("Due Date").Width(120);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsPastDue", "Agency")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
