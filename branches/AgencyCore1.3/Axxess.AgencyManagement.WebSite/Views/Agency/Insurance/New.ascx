﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Insurance/Payor | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newinsurance');</script>")%>
<% using (Html.BeginForm("Add", "Insurance", FormMethod.Post, new { @id = "newInsuranceForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_Name" class="float_left">Insurance/Payor Name:</label><div class="float_right"><%=Html.TextBox("Name", "", new { @id = "New_Insurance_Name", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_PayorType" class="float_left">Payor Type:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.PaymentSource, "PayorType", "", new { @id = "New_Insurance_PayorType", @class = "requireddropdown valid" })%></div></div>
            <div class="row"><label for="New_Insurance_InvoiceType" class="float_left">Invoice Type:</label><div class="float_right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "New_Insurance_InvoiceType" })%></div></div>
           
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_ProviderCode" class="float_left">Provider Code:</label><div class="float_right"><%=Html.TextBox("ProviderCode", "", new { @id = "New_Insurance_ProviderCode", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_ProviderNumber" class="float_left">Provider Number:</label><div class="float_right"><%=Html.TextBox("ProviderNumber", "", new { @id = "New_Insurance_ProviderNumber", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_HealthPlanId" class="float_left">Health Plan Id:</label><div class="float_right"><%=Html.TextBox("HealthPlanId", "", new { @id = "New_Insurance_HealthPlanId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_OtherProviderId" class="float_left">Other Provider Id:</label><div class="float_right"><%=Html.TextBox("OtherProviderId", "", new { @id = "New_Insurance_OtherProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_Ub04Locator81cca" class="float_left">UB04 Locator 81CCa:</label><div class="float_right"><%=Html.TextBox("Ub04Locator81cca", "", new { @id = "New_Insurance_Ub04Locator81cca", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Rates</legend>
           <div class="wide_column">
            <table class="fixed insurance">
            <col width=140 /><col width=60 /><col /> <col width=20 /><col width=150 /><col width=60 /><col />
            <tbody>
                    <tr><th colspan="7" class="align_center borderBottom">Charge Rates</th></tr>
                    
                    <tr><th class="borderBottom align_left">Disciplines</th><th class="borderBottom align_center" >Bill Rate</th><th class="borderBottom borderRight align_center">Bill Type </th><th class="borderBottom"></th><th class="borderBottom align_left">Disciplines</th><th class="borderBottom align_center">Bill Rate</th><th class="borderBottom align_center">Bill Type</th></tr>
                    <tr>
                        <td><label class="float_left">Skilled Nurse</label><%=Html.Hidden("RateDiscipline", "SkilledNurse")%>  </td><td class="align_center"><%= Html.TextBox("SkilledNurse_Charge", "", new { @id = "New_Insurance_SkilledNurseCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeSN = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("SkilledNurse_ChargeType", chargeTypeSN, new { @id = "New_Insurance_ChargeTypeSN" })%></td><td></td>
                        <td ><label class="float_left">Home Health Aide</label><%=Html.Hidden("RateDiscipline", "HomeHealthAide")%></td><td class="align_center" ><%= Html.TextBox("HomeHealthAide_Charge", "", new { @id = "New_Insurance_HomeHealthAideCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeHHA = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("HomeHealthAide_ChargeType", chargeTypeHHA, new { @id = "New_Insurance_ChargeTypeHHA" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Physical Therapy</label><%=Html.Hidden("RateDiscipline", "PhysicalTherapy")%></td><td class="align_center"> <%= Html.TextBox("PhysicalTherapy_Charge", "", new { @id = "New_Insurance_PhysicalTherapyCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypePT = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("PhysicalTherapy_ChargeType", chargeTypePT, new { @id = "New_Insurance_ChargeTypePT" })%></td><td></td>
                        <td><label class="float_left">Attendant</label><%=Html.Hidden("RateDiscipline", "Attendant")%></td><td class="align_center"> <%= Html.TextBox("Attendant_Charge", "", new { @id = "New_Insurance_AttendantCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeAttendant = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("Attendant_ChargeType", chargeTypeAttendant, new { @id = "New_Insurance_ChargeTypeAttendant" })%></td>
                     </tr>
                    <tr>  
                        <td><label class="float_left">Occupational Therapy</label><%=Html.Hidden("RateDiscipline", "OccupationalTherapy")%></td><td class="align_center"> <%= Html.TextBox("OccupationalTherapy_Charge", "", new { @id = "New_Insurance_OccupationalTherapyCharge ", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeOT = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("OccupationalTherapy_ChargeType", chargeTypeOT, new { @id = "New_Insurance_ChargeTypeOT" })%></td><td></td>
                        <td><label class="float_left">Companion Care</label><%=Html.Hidden("RateDiscipline", "CompanionCare")%></td><td class="align_center" > <%= Html.TextBox("CompanionCare_Charge", "", new { @id = "New_Insurance_CompanionCareCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeCompanionCare = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("CompanionCare_ChargeType", chargeTypeCompanionCare, new { @id = "New_Insurance_ChargeTypeCompanionCare" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Speech Therapy</label><%=Html.Hidden("RateDiscipline", "SpeechTherapy")%></td><td class="align_center"> <%= Html.TextBox("SpeechTherapy_Charge", "", new { @id = "New_Insurance_SpeechTherapyCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeST = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("SpeechTherapy_ChargeType", chargeTypeST, new { @id = "New_Insurance_ChargeTypeST" })%></td><td></td>
                        <td><label class="float_left">Homemaker Services </label><%=Html.Hidden("RateDiscipline", "HomemakerServices")%></td><td class="align_center"> <%= Html.TextBox("HomemakerServices_Charge", "", new { @id = "New_Insurance_HomemakerServicesCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeHomemakerServices = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("HomemakerServices_ChargeType", chargeTypeHomemakerServices, new { @id = "New_Insurance_ChargeTypeHomemakerServices" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Medical Social Worker</label><%=Html.Hidden("RateDiscipline", "MedicareSocialWorker")%></td><td class="align_center"> <%= Html.TextBox("MedicareSocialWorker_Charge", "", new { @id = "New_Insurance_MedicareSocialWorkerCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeMSW = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("MedicareSocialWorker_ChargeType", chargeTypeMSW, new { @id = "New_Insurance_ChargeTypeMSW" })%></td><td></td>
                        <td><label class="float_left">Private Duty Sitter</label><%=Html.Hidden("RateDiscipline", "PrivateDutySitter")%></td><td class="align_center"> <%= Html.TextBox("PrivateDutySitter_Charge", "", new { @id = "New_Insurance_PrivateDutySitterCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypePrivateDutySitter = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("PrivateDutySitter_ChargeType", chargeTypePrivateDutySitter, new { @id = "New_Insurance_ChargeTypePrivateDutySitter" })%></td>
                    </tr>
                    </tbody></table></div>
    </fieldset>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", "", new { @id = "New_Insurance_AddressLine1", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", "", new { @id = "New_Insurance_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressCity" class="float_left">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", "", new { @id = "New_Insurance_AddressCity", @class = "text input_wrapper", @tabindex = "60" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI</legend>
        <div class="column">
            <div class="row">
                <label for="New_Insurance_ClearingHouse" class="float_left">
                    Clearing House:</label>
                <div class="float_right">
                    <% var clearingHouse = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "", Value = "0" },
                       new SelectListItem { Text = "Palmetto GBA", Value = "1" },
                       new SelectListItem { Text = "Availity", Value = "2" },
                       new SelectListItem { Text = "NGS", Value = "3" },
                       new SelectListItem { Text = "ZirMed", Value = "4" },
                       new SelectListItem { Text = "NHIC", Value = "5" },
                       new SelectListItem { Text = "Cahaba GBA", Value = "6" },
                       new SelectListItem { Text = "TMHP", Value = "7" },
                       new SelectListItem { Text = "Office Ally", Value = "8" }                         
                    }, "Value", "Text");%>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "New_Insurance_ClearingHouse" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_Insurance_InterchangeReceiverId" class="float_left">Interchange Receiver Id:</label>
                <div class="float_right">
                    <%var interchangeReceiverId = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "1" },
                       new SelectListItem { Text = "Duns (Dun & Bradstreet) (01)", Value = "2" },
                       new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "3" },
                       new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "4" },
                       new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "5" },
                       new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "6" },
                       new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "7" },
                       new SelectListItem { Text = "National Association of Insurance Commisioners Company Code (NAIC)(33)", Value = "8" } ,                        
                       new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "9" }
                    }, "Value", "Text");%>
                    <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "New_Insurance_InterchangeReceiverId" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_PayorId" class="float_left">Payor Id:</label><div class="float_right"><%=Html.TextBox("PayorId", "", new { @id = "New_Insurance_PayorId", @class = "text input_wrapper", @tabindex = "58" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Contact Person</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_ContactPersonFirstName" class="float_left">First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @tabindex = "58" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactPersonLastName" class="float_left">Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", "", new { @id = "New_Insurance_ContactPersonLastName", @class = "text input_wrapper", tabindex = "59" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactEmailAddress" class="float_left">Email:</label><div class="float_right"><%=Html.TextBox("ContactEmailAddress", "", new { @id = "New_Insurance_ContactEmailAddress", @class = "text input_wrapper", @tabindex = "60" })%></div></div>
            <div class="row"><label for="New_Insurance_PhoneNumberArray1" class="float_left">Phone:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short"name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray1" maxlength="3" />&nbsp;-&nbsp;<input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray2" maxlength="3" />&nbsp;-&nbsp;<input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Insurance_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray1" maxlength="3" />&nbsp;-&nbsp;<input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray2" maxlength="3" />&nbsp;-&nbsp;<input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Insurance_FaxNumberArray3" maxlength="4" /></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_CurrentBalance" class="float_left">Current Balance:</label><div class="float_right"><%=Html.TextBox("CurrentBalance", "", new { @id = "New_Insurance_CurrentBalance", @class = "text input_wrapper", @tabindex = "58" })%></div></div>
            <div class="row"><label for="New_Insurance_WorkWeekStartDay" class="float_left">Work Week Begins:</label>
                <div class="float_right">
                    <% var workWeekStartDay = new SelectList(new[]
                        { 
                           new SelectListItem { Text = "Sunday", Value = "1" },
                           new SelectListItem { Text = "Monday", Value = "2" }               
                           
                        }, "Value", "Text");%>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "New_Insurance_WorkWeekStartDay" })%>
                </div>
            </div><br />
            <div class="row"><label for="New_Insurance_VisitAuthReq" class="float_left">Visit Authorization Required:</label><div class="float_right"><%= Html.RadioButton("IsVisitAuthorizationRequired", "1", new {  @class = "radio" })%><label class="inlineradio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired", "0", new { @id = "New_Insurance_VisitAuthReq", @class = "radio" })%><label class="inlineradio">No</label></div></div><br />
            <div class="row"><label for="New_Insurance_DefaultFiscalIntermediary" class="float_left">Default Fiscal Intermediary:</label><div class="float_right"><%= Html.RadioButton("DefaultFiscalIntermediary", "1", new { @id = "New_Insurance_DefaultFiscalIntermediary", @class = "radio" })%><label class="inlineradio">Yes</label><%= Html.RadioButton("DefaultFiscalIntermediary", "0", new { @class = "radio" })%><label class="inlineradio">No</label></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newinsurance');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
