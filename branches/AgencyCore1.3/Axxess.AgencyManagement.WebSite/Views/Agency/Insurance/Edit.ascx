﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<%  using (Html.BeginForm("Update", "Insurance", FormMethod.Post, new { @id = "editInsuranceForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Insurance_Id" })%>
<% var data = Model != null ? Model.ToChargeRateDictionary() : new Dictionary<string, ChargeRate>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Insurance | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','editinsurance');</script>")%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_Name" class="float_left">Insurance/Payor Name:</label><div class="float_right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Insurance_Name", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_PayorType" class="float_left">Payor Type:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.PaymentSource, "PayorType", Convert.ToString(Model.PayorType), new { @id = "Edit_Insurance_PayorType", @class = "requireddropdown valid" })%></div></div>
            <div class="row"><label for="Edit_Insurance_InvoiceType" class="float_left">Invoice Type:</label><div class="float_right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", Convert.ToString(Model.InvoiceType)); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "Edit_Insurance_InvoiceType" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ParentInsurance" class="float_left">Parent Insurance:</label><div class="float_right"><% var parentInsurance = new SelectList(new[] { new SelectListItem { Text = "-- Select --", Value = "0" }, new SelectListItem { Text = "Advantra Freedom", Value = "1" }, new SelectListItem { Text = "Aetna Health Inc.", Value = "2" }, new SelectListItem { Text = "Care Improvement Plus of Texas Insurance", Value = "3" }, new SelectListItem { Text = "First Health Life and Health Insurance", Value = "4" }, new SelectListItem { Text = "Palmetto GBA", Value = "5" }, new SelectListItem { Text = "Well Care Health Insurance of Arizona", Value = "6" }, new SelectListItem { Text = "Well Care of Texas Inc.", Value = "7" } }, "Value", "Text", Convert.ToString(Model.ParentInsurance)); %><%= Html.DropDownList("ParentInsurance", parentInsurance, new { @id = "Edit_Insurance_ParentInsurance" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_ProviderCode" class="float_left">Provider Code:</label><div class="float_right"><%=Html.TextBox("ProviderCode", Model.ProviderCode, new { @id = "Edit_Insurance_ProviderCode", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ProviderNumber" class="float_left">Provider Number:</label><div class="float_right"><%=Html.TextBox("ProviderNumber", Model.ProviderNumber, new { @id = "Edit_Insurance_ProviderNumber", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_HealthPlanId" class="float_left">Health Plan Id:</label><div class="float_right"><%=Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @id = "Edit_Insurance_HealthPlanId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_OtherProviderId" class="float_left">Other Provider Id:</label><div class="float_right"><%=Html.TextBox("OtherProviderId", Model.OtherProviderId, new { @id = "Edit_Insurance_OtherProviderId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Insurance_Ub04Locator81cca" class="float_left">UB04 Locator 81CCa:</label><div class="float_right"><%=Html.TextBox("Ub04Locator81cca", Model.Ub04Locator81cca, new { @id = "Edit_Insurance_Ub04Locator81cca", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
        </div>
    </fieldset>
     <fieldset>
        <legend>Rates</legend>
           <div class="wide_column">
            <table class="fixed insurance">
            <col width=140 /><col width=60 /><col /> <col width=20 /><col width=150 /><col width=60 /><col />
            <tbody>
                    <tr><th colspan="7" class="align_center borderBottom">Charge Rates</th></tr>
                    
                    <tr><th class="borderBottom align_left">Disciplines</th><th class="borderBottom align_center" >Bill Rate</th><th class="borderBottom borderRight align_center">Bill Type </th><th class="borderBottom"></th><th class="borderBottom align_left">Disciplines</th><th class="borderBottom align_center">Bill Rate</th><th class="borderBottom align_center">Bill Type</th></tr>
                    <tr>
                        <td><label class="float_left">Skilled Nurse</label><%=Html.Hidden("RateDiscipline", "SkilledNurse")%>  </td><td class="align_center"><%= Html.TextBox("SkilledNurse_Charge", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].Charge : string.Empty, new { @id = "Edit_Insurance_SkilledNurseCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeSN = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].ChargeType : "1"); %><%= Html.DropDownList("SkilledNurse_ChargeType", chargeTypeSN, new { @id = "Edit_Insurance_ChargeTypeSN" })%></td><td></td>
                        <td ><label class="float_left">Home Health Aide</label><%=Html.Hidden("RateDiscipline", "HomeHealthAide")%></td><td class="align_center" ><%= Html.TextBox("HomeHealthAide_Charge", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].Charge : string.Empty, new { @id = "Edit_Insurance_HomeHealthAideCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeHHA = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].ChargeType : "1"); %><%= Html.DropDownList("HomeHealthAide_ChargeType", chargeTypeHHA, new { @id = "Edit_Insurance_ChargeTypeHHA" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Physical Therapy</label><%=Html.Hidden("RateDiscipline", "PhysicalTherapy")%></td><td class="align_center"> <%= Html.TextBox("PhysicalTherapy_Charge", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypePT = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].ChargeType : "1"); %><%= Html.DropDownList("PhysicalTherapy_ChargeType", chargeTypePT, new { @id = "Edit_Insurance_ChargeTypePT" })%></td><td></td>
                        <td><label class="float_left">Attendant</label><%=Html.Hidden("RateDiscipline", "Attendant")%></td><td class="align_center"> <%= Html.TextBox("Attendant_Charge", data.ContainsKey("Attendant") ? data["Attendant"].Charge : string.Empty, new { @id = "Edit_Insurance_AttendantCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeAttendant = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("Attendant") ? data["Attendant"].ChargeType : "1"); %><%= Html.DropDownList("Attendant_ChargeType", chargeTypeAttendant, new { @id = "Edit_Insurance_ChargeTypeAttendant" })%></td>
                     </tr>
                    <tr>  
                        <td><label class="float_left">Occupational Therapy</label><%=Html.Hidden("RateDiscipline", "OccupationalTherapy")%></td><td class="align_center"> <%= Html.TextBox("OccupationalTherapy_Charge", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeOT = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].ChargeType : "1"); %><%= Html.DropDownList("OccupationalTherapy_ChargeType", chargeTypeOT, new { @id = "Edit_Insurance_ChargeTypeOT" })%></td><td></td>
                        <td><label class="float_left">Companion Care</label><%=Html.Hidden("RateDiscipline", "CompanionCare")%></td><td class="align_center" > <%= Html.TextBox("CompanionCare_Charge", data.ContainsKey("CompanionCare") ? data["CompanionCare"].Charge : string.Empty, new { @id = "Edit_Insurance_CompanionCareCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeCompanionCare = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("CompanionCare") ? data["CompanionCare"].ChargeType : "1"); %><%= Html.DropDownList("CompanionCare_ChargeType", chargeTypeCompanionCare, new { @id = "Edit_Insurance_ChargeTypeCompanionCare" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Speech Therapy</label><%=Html.Hidden("RateDiscipline", "SpeechTherapy")%></td><td class="align_center"> <%= Html.TextBox("SpeechTherapy_Charge", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_SpeechTherapyCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeST = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].ChargeType : "1"); %><%= Html.DropDownList("SpeechTherapy_ChargeType", chargeTypeST, new { @id = "Edit_Insurance_ChargeTypeST" })%></td><td></td>
                        <td><label class="float_left">Homemaker Services </label><%=Html.Hidden("RateDiscipline", "HomemakerServices")%></td><td class="align_center"> <%= Html.TextBox("HomemakerServices_Charge", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].Charge : string.Empty, new { @id = "Edit_Insurance_HomemakerServicesCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypeHomemakerServices = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].ChargeType : "1"); %><%= Html.DropDownList("HomemakerServices_ChargeType", chargeTypeHomemakerServices, new { @id = "Edit_Insurance_ChargeTypeHomemakerServices" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Medical Social Worker</label><%=Html.Hidden("RateDiscipline", "MedicareSocialWorker")%></td><td class="align_center"> <%= Html.TextBox("MedicareSocialWorker_Charge", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].Charge : string.Empty, new { @id = "Edit_Insurance_MedicareSocialWorkerCharge", @class = "rates currency" })%></td><td class="borderRight align_center"><% var chargeTypeMSW = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].ChargeType : "1"); %><%= Html.DropDownList("MedicareSocialWorker_ChargeType", chargeTypeMSW, new { @id = "Edit_Insurance_ChargeTypeMSW" })%></td><td></td>
                        <td><label class="float_left">Private Duty Sitter</label><%=Html.Hidden("RateDiscipline", "PrivateDutySitter")%></td><td class="align_center"> <%= Html.TextBox("PrivateDutySitter_Charge", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].Charge : string.Empty, new { @id = "Edit_Insurance_PrivateDutySitterCharge", @class = "rates currency" })%></td><td class="align_center"><% var chargeTypePrivateDutySitter = new SelectList(new[] { new SelectListItem { Text = "Per Visit", Value = "1" }, new SelectListItem { Text = "Hourly", Value = "2" }, new SelectListItem { Text = "15 Minute Increment (Unit)", Value = "3" } }, "Value", "Text", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].ChargeType : "1"); %><%= Html.DropDownList("PrivateDutySitter_ChargeType", chargeTypePrivateDutySitter, new { @id = "Edit_Insurance_ChargeTypePrivateDutySitter" })%></td>
                    </tr>
                    </tbody></table></div>
    </fieldset>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Insurance_AddressLine1", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Insurance_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressCity" class="float_left">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Insurance_AddressCity", @class = "text input_wrapper", @tabindex = "60" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Insurance_ClearingHouse" class="float_left">
                    Clearing House:</label>
                <div class="float_right">
                    <% var clearingHouse = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "", Value = "0" },
                       new SelectListItem { Text = "Palmetto GBA", Value = "1" },
                       new SelectListItem { Text = "Availity", Value = "2" },
                       new SelectListItem { Text = "NGS", Value = "3" },
                       new SelectListItem { Text = "ZirMed", Value = "4" },
                       new SelectListItem { Text = "NHIC", Value = "5" },
                       new SelectListItem { Text = "Cahaba GBA", Value = "6" },
                       new SelectListItem { Text = "TMHP", Value = "7" },
                       new SelectListItem { Text = "Office Ally", Value = "8" }                         
                    }, "Value", "Text", Convert.ToString(Model.ClearingHouse));%>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "Edit_Insurance_ClearingHouse" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_InterchangeReceiverId" class="float_left">
                    Interchange Receiver Id:</label>
                <div class="float_right">
                    <%var interchangeReceiverId = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "1" },
                       new SelectListItem { Text = "Duns (Dun & Bradstreet) (01)", Value = "2" },
                       new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "3" },
                       new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "4" },
                       new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "5" },
                       new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "6" },
                       new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "7" },
                       new SelectListItem { Text = "National Association of Insurance Commisioners Company Code (NAIC)(33)", Value = "8" } ,                        
                       new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "9" }
                    }, "Value", "Text", Convert.ToString(Model.InterchangeReceiverId));%>
                    <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "Edit_Insurance_InterchangeReceiverId" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_PayorId" class="float_left">Payor Id:</label><div class="float_right"><%=Html.TextBox("PayorId", Convert.ToString(Model.PayorId), new { @id = "Edit_Insurance_PayorId", @class = "text input_wrapper required" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Contact Person</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_ContactPersonFirstName" class="float_left">First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Insurance_ContactPersonFirstName", @class = "text input_wrapper"})%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactPersonLastName" class="float_left">Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Insurance_ContactPersonLastName", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactEmailAddress" class="float_left">Email:</label><div class="float_right"><%=Html.TextBox("ContactEmailAddress", Model.ContactEmailAddress, new { @id = "Edit_Insurance_ContactEmailAddress", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Insurance_PhoneNumberArray1" class="float_left">Phone:</label><div class="float_right"><%=Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_PhoneNumberArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Insurance_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_FaxNumberArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_CurrentBalance" class="float_left">Current Balance:</label><div class="float_right"><%=Html.TextBox("CurrentBalance", Model.CurrentBalance, new { @id = "Edit_Insurance_CurrentBalance", @class = "text input_wrapper", @tabindex = "58" })%></div></div>
            <div class="row"><label for="Edit_Insurance_WorkWeekStartDay" class="float_left">Work Week Begins:</label><div class="float_right">
                    <%var workWeekStartDay = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "Sunday", Value = "1" },
                       new SelectListItem { Text = "Monday", Value = "2" }               
                       
                    }, "Value", "Text", Convert.ToString(Model.WorkWeekStartDay));%>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "Edit_Insurance_WorkWeekStartDay" })%>
                </div>
            </div>
            <br />
            <div class="row"><label for="Edit_Insurance_IsAuthReq" class="float_left">Visit Authorization Required:</label><div class="float_right"><%= Html.RadioButton("IsVisitAuthorizationRequired", true, Model.IsVisitAuthorizationRequired, new { @id = "Edit_Insurance_IsAuthReq", @class = "required radio" })%><label class="inlineradio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired",false ,!Model.IsVisitAuthorizationRequired, new { @class = "required radio" })%><label class="inlineradio">No</label></div></div>
            <br />
            <div class="row"><label for="DefaultFiscalIntermediary" class="float_left">Default Fiscal Intermediary:</label><div class="float_right"><%= Html.RadioButton("DefaultFiscalIntermediary", true, Model.DefaultFiscalIntermediary, new { @id = "Edit_Insurance_DefaultFiscalInter", @class = "required radio" })%><label class="inlineradio">Yes</label><%= Html.RadioButton("DefaultFiscalIntermediary",false ,!Model.DefaultFiscalIntermediary, new { @class = "required radio" })%><label class="inlineradio">No</label></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editinsurance');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
