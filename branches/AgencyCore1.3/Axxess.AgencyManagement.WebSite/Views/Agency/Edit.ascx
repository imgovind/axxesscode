﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%  using (Html.BeginForm("Update", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Agency | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','editagency');</script>")%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Agency_Id" }) %>
<div class="form_wrapper">
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Agency_CompanyName">Company Name:</label><div class="float_right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Agency_CompanyName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_TaxId">Tax Id:</label><div class="float_right"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_TaxIdType">Tax Id Type:</label><div class="float_right"><%var taxIdTypes = new SelectList(new[] { new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" }, new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" }, new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" } }, "Value", "Text", Model.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "Edit_Agency_TaxIdType", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_Payor">Payor:</label><div class="float_right"><%var payors = new SelectList(new[] { new SelectListItem { Text = "** Select Payor **", Value = "0" }, new SelectListItem { Text = "Palmetto GBA", Value = "1" }, new SelectListItem { Text = "National Government Services (formerly known as United Government Services)", Value = "2" }, new SelectListItem { Text = "Blue Cross Blue Shield of Alabama (AKA Chaba GBA)", Value = "3" }, new SelectListItem { Text = "Anthem Health Plans of Maine", Value = "4" } }, "Value", "Text", Model.Payor); %><%= Html.DropDownList("Payor", payors, new { @id = "Edit_Agency_Payor", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_SubmitterId">Submitter Id:</label><div class="float_right"><%=Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "Edit_Agency_SubmitterId", @maxlength = "15", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float_right"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = "Edit_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPhone1">Contact Person Phone:</label><div class="float_right"><%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : "", new { @id = "Edit_Agency_ContactPhone1", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : "", new { @id = "Edit_Agency_ContactPhone2", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : "", new { @id = "Edit_Agency_ContactPhone3", @class = "input_wrappermultible autotext required numeric phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Agency_NationalProviderNo">National Provider Number:</label><div class="float_right"><%=Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float_right"><%=Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_SubmitterName">Submitter Name:</label><div class="float_right"><%=Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "Edit_Agency_SubmitterName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editagency');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
