﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Final>>" %>
<ul>
    <li class="align_center"><h3>Final</h3></li>
    <li>
        <span class="rapcheck"></span>
        <span class="rapname">Patient Name</span>
        <span class="rapid">Patient ID</span>
        <span class="rapeps">Episode Date</span>
        <span class="finalicon">Rap</span>
        <span class="finalicon">Visit</span>
        <span class="finalicon">Order</span>
        <span class="finalicon">Verified</span>
    </li>
</ul><ol><%
if (Model != null) {
    var finals = Model.Where(c =>c.EpisodeEndDate < DateTime.Now);
    int i = 1;
    foreach (var final in finals) { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") + (final.IsRapGenerated ? "ready" : "notready"))%>
            <span class="rapcheck"><%= i %>. <%= (final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified && final.AreOrdersComplete && final.IsRapGenerated) ? "<input name='FinalSelected' class='radio' type='checkbox' value='" + final.Id + "' id='FinalSelected" + final.Id + "' />" : ""%></span>
        <a href='javascript:void(0);' onclick="<%= (final.IsRapGenerated) ? "UserInterface.ShowFinal('" + final.EpisodeId + "','" + final.PatientId + "');\" title=\"Final Ready" : "U.growl('Error: Rap Not Generated', 'error');\" title=\"Not Complete" %>">
            <span class="rapname"><%= final.DisplayName%></span>
            <span class="rapid"><%= final.PatientIdNumber%></span>
            <span class="rapeps"><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("MM/dd/yyy") : ""%><%= final.EpisodeEndDate != null ? "&ndash;" + final.EpisodeEndDate.ToString("MM/dd/yyy") : ""%></span>
            <span class="finalicon"><img src='<%= final.IsRapGenerated ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
            <span class="finalicon"><img src='<%= final.IsVisitVerified ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
            <span class="finalicon"><img src='<%= final.AreOrdersComplete ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
            <span class="finalicon"><img src='<%= final.IsVisitVerified ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
        </a>
    </li><%
        i++;
    }
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentFinal ol li:first").addClass("first");
    $("#Billing_CenterContentFinal ol li:last").addClass("last");
</script>