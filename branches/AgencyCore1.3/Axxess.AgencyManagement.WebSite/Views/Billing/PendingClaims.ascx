﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimViewData>>" %>
<% if(Model!=null && Model.Count>0) {%>
<table class="claim">
    <thead>
        <tr>
            <th>
                Payment Date
            </th>
            <th>
                Patient Name
            </th>
            <th>
                Medicare No
            </th>
            <th>
                Episode 
            </th>
            <th>
                Cliam Date
            </th>
            <th>
               Status
            </th>
            <th>
               Payment
            </th>
        </tr>
    </thead>
    <tbody>
      
        <% int j = 1;%>
        <% foreach (var claim in Model)
           {%>
        <% if (j % 2 == 0)
           {%>
        <tr class="even">
            <%}
           else
           { %>
            <tr class="odd">
                <%} %>
                <td class="align_center"><%= Html.Telerik().DatePicker().Name("PaymentDate").Value(claim.PaymentDate != null && claim.PaymentDate.IsValid()?claim.PaymentDate: DateTime.Now).HtmlAttributes(new { @class = "text date" })%></td>
                <td><%=string.Format("{0}. {1}" ,claim.LastName.Substring(0,1),claim.FirstName)%></td>
                <td><%=claim.MedicareNumber %></td>
                <td class="align_center"> <%=claim.EpisodeStartDate!=null && claim.EpisodeStartDate.IsValid()? claim.EpisodeStartDate.ToString("MM/dd/yyyy"):string.Empty%>-<%=claim.EpisodeEndDate!=null && claim.EpisodeEndDate.IsValid()? claim.EpisodeEndDate.ToString("MM/dd/yyyy"):string.Empty%></td>
                <td></td>
                <td class="align_center">
                 <%var status = new SelectList(new[]
               { 
                   new SelectListItem { Value = "300", Text = "Claim Created" },
                   new SelectListItem { Value = "305", Text = "Claim Submitted" },
                   new SelectListItem { Value = "310", Text = "Claim Rejected"} ,
                   new SelectListItem { Value = "315", Text = "Payment Pending" },
                   new SelectListItem { Value = "320", Text = "Claim Accepted/Processing" },
                   new SelectListItem { Value = "325", Text = "Claim With Errors"} ,
                   new SelectListItem { Value = "330", Text = "Paid Claim" },
                   new SelectListItem { Value = "335", Text = "Cancelled Claim" }
                   
               }
                   , "Value", "Text", claim.Status);%>
                <%= Html.DropDownList("claimDropDown", status)%>
                </td>
                <td>
                  
                </td>               
            </tr>
            <% j++;
           } %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9" style="height: 30px; background: #AA9494;">
                <div class="buttons"><ul>
                    <li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#Billing_CenterContent');">Update the Claims</a></li>
                </ul></div>
            </td>
        </tr>
    </tfoot>
</table>
<%} %>

