﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Billing Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','billingcenter');</script>")%>
<div id="Billing_CenterContent" class="main wrapper">
    <div class="billing">
        <div id="Billing_CenterContentRap"><% Html.RenderPartial("RapGrid", Model.Raps); %></div>
        <div id="Billing_CenterContentFinal"><% Html.RenderPartial("FinalGrid", Model.Finals); %></div>
    </div>
    <% if (Current.HasRight(Permissions.GenerateClaimFiles)) { %>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#Billing_CenterContent');">Generate Selected</a></li>
        <li><a href="javascript:void(0);" onclick="Billing.GenerateAllCompleted('#Billing_CenterContent');">Generate All Completed</a></li>
    </ul></div>
    <% } %>
</div>
