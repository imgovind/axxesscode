﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Rap>>" %>
<ul>
    <li class="align_center"><h3>Rap</h3></li>
    <li>
        <span class="rapcheck"></span>
        <span class="rapname">Patient Name</span>
        <span class="rapid">Patient ID</span>
        <span class="rapeps">Episode Date</span>
        <span class="rapicon">Oasis</span>
        <span class="rapicon">Billable Visit</span>
        <span class="rapicon">Verified</span>
    </li>
</ul><ol><%
if (Model != null) {
    int i = 1;
    foreach (var rap in Model) { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") + (rap.IsOasisComplete && rap.IsFirstBillableVisit ? "ready" : "notready")) %>
            <span class="rapcheck"><%= i %>. <%=  rap.IsOasisComplete &&  rap.IsFirstBillableVisit && rap.IsVerified ? "<input name='RapSelected' class='radio' type='checkbox' value='" + rap.Id + "' id='RapSelected" + rap.Id + "' />" : "" %></span>
        <a href='javascript:void(0);' onclick="<%= (rap.IsOasisComplete && rap.IsFirstBillableVisit) ? "UserInterface.ShowRap('" + rap.EpisodeId + "','" + rap.PatientId + "');\" title=\"Rap Ready" : "U.growl('Error: Not Completed', 'error');\" title=\"Not Complete" %>">
            <span class="rapname"><%= rap.DisplayName%></span>
            <span class="rapid"><%= rap.PatientIdNumber%></span>
            <span class="rapeps"><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("MM/dd/yyy") : ""%><%= rap.EpisodeEndDate != null ? "&ndash;" + rap.EpisodeEndDate.ToString("MM/dd/yyy") : ""%></span>
            <span class="rapicon"><img src='<%= rap.IsOasisComplete ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
            <span class="rapicon"><img src='<%= rap.IsFirstBillableVisit ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
            <span class="rapicon"><img src='<%=  rap.IsOasisComplete &&  rap.IsFirstBillableVisit && rap.IsVerified ? "/Images/icons/success.png' alt='Complete" : "/Images/icons/error.png' alt='Not Complete" %>' width="16" height="16" /></span>
        </a>
    </li><%
        i++;
    }
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentRap ol li:first").addClass("first");
    $("#Billing_CenterContentRap ol li:last").addClass("last");
</script>
