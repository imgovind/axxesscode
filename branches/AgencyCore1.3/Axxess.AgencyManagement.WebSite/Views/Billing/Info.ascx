﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("InfoVerify", "Billing", FormMethod.Post, new { @id = "billingInfo" })) { %>
<div class="req_legend"><span class="req_red">*</span> = Required Field</div>
<fieldset>
    <div class="column"><%= Html.Hidden("Id",Model.Id) %>
        <div class="row">
            <label for="FirstName" class="float_left">Patient First Name:</label>
            <div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "text input_wrapper {validate:{required:true}}", @maxlength = "20" })%></div>
        </div><div class="row">
            <label for="LastName" class="float_left">Patient Last Name:</label>
            <div class="float_right"><%= Html.TextBox("LastName", Model.LastName, new { @class = "text input_wrapper required", @maxlength = "20" })%></div>
        </div><div class="row">
            <label for="MedicareNumber" class="float_left">Medicare #:</label>
            <div class="float_right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @class = "text input_wrapper required", @maxlength = "11" })%></div>
        </div><div class="row">
            <label for="PatientIdNumber" class="float_left">Patient Record #:</label>
            <div class="float_right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text input_wrapper required", @maxlength = "11" })%></div>
        </div><div class="row">
            <label class="float_left">Gender:</label>
            <div class="float_right"><%= Html.RadioButton("Gender", "Female",  Model.Gender == "Female" ? true : false, new { @id = "GenderF", @class = "radio" })%><label for="GenderF" class="inlineradio">Female</label><%=Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "GenderM", @class = "radio" })%><label for="GenderM" class="inlineradio">Male</label></div>
        </div><div class="row">
            <label for="DOB" class="float_left">Date of Birth:</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("DOB").Value(Model.DOB).HtmlAttributes(new { @class = "text  date required" }) %></div>
        </div><div class="row">
            <label for="EpisodeStartDate" class="float_left">Episode Start Date:</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("EpisodeStartDate").Value(Model.EpisodeStartDate).HtmlAttributes(new { @class = "text required date required" }) %></div>
        </div><div class="row">
            <label for="StartOfCareDate" class="float_left">Admission Date:</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("StartOfCareDate").Value(Model.StartofCareDate).HtmlAttributes(new {  @class = "text required date required" }) %></div>
        </div><div class="row">
                <label for="AdmissionSource" class="float_left">Admission Source:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource .IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource)> 0) ? Model.AdmissionSource.ToString() : "", new {  @class = "AdmissionSource" })%></div>
            </div><div class="row">
            <label for="AddressLine1" class="float_left">Address Line 1:</label>
            <div class="float_right"><%= Html.TextBox("AddressLine1",Model.AddressLine1 , new { @class = "text required" }) %></div>
        </div><div class="row">
            <label for="AddressLine2" class="float_left">Address Line 2:</label>
            <div class="float_right"><%= Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text" }) %></div>
        </div><div class="row">
            <label for="AddressCity" class="float_left">City:</label>
            <div class="float_right"><%= Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required" }) %></div>
        </div><div class="row">
            <label for="AddressStateCode" class="float_left">State, Zip Code:</label>
            <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @class = "input_wrapper AddressStateCode" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip zip", @maxlength = "5" }) %></div>
        </div><div class="row">
                <label for="Type" class="float_left">Bill Type:</label><%  var billType = new SelectList(new[] {new SelectListItem { Text = "Initial Final", Value = "0" },new SelectListItem { Text = "Final Cancellation", Value = "1" }}, "Value", "Text",Model.Type);%>
                <div class="float_right"><%= Html.DropDownList("Type", billType)%></div>
            </div>
    </div><div class="column">
        <div class="row">
            <label for="HippsCode" class="float_left">HIPPS Code:</label>
            <div class="float_right"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @class = "text required", @maxlength = "5", @readonly = "readonly" }) %></div>
        </div><div class="row">
            <label for="ClaimKey" class="float_left">Oasis Matching Key:</label>
            <div class="float_right"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "text required", @maxlength = "18", @readonly = "readonly" }) %></div>
        </div><div class="row">
            <label for="FirstBillableVisitDate" class="float_left">First Billable Visit Date:</label>
            <div class="float_right">
                <% if (Model.FirstBillableVisitDate != null && Model.FirstBillableVisitDate > DateTime.MaxValue) { %>
                    <%= Html.Telerik().DatePicker().Name("FirstBillableVisitDate").Value(Model.FirstBillableVisitDate).HtmlAttributes(new { @class = "text required date" }) %>
                <% } else { %>
                    <%= Html.Telerik().DatePicker().Name("FirstBillableVisitDate").HtmlAttributes(new { @class = "text required date" }) %>
                <% } %>
            </div>
        </div><div class="row">
            <label for="PhysicianLastName" class="float_left">Physician Last Name:</label>
            <div class="float_right"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20" }) %></div>
        </div><div class="row">
                <label for="PhysicianFirstName" class="float_left">Physician First Name:</label>
                <div class="float_right"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "text required", @maxlength = "20" })%></div>
            </div><div class="row">
            <label for="PhysicianNPI" class="float_left">Physician NPI #:</label>
            <div class="float_right"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "10" }) %></div>
        </div><div class="row">
            <div><% var diganosis = XElement.Parse(Model.DiagonasisCode); %>Diagonasis Codes:</div>
            <div class="margin">
                <label for="Primary" class="float_left">Primary</label>
                <div class="float_right"><%= Html.TextBox("Primary", diganosis!=null && diganosis.Element("code1")!=null?diganosis.Element("code1").Value:"" ) %></div><div class="clear"></div>
                <label for="Second" class="float_left">Second</label>
                <div class="float_right"><%= Html.TextBox("Second", diganosis != null && diganosis.Element("code2") != null ? diganosis.Element("code2").Value : "")%></div><div class="clear"></div>
                <label for="Third" class="float_left">Third</label>
                <div class="float_right"><%= Html.TextBox("Third", diganosis != null && diganosis.Element("code3") != null ? diganosis.Element("code3").Value : "")%></div><div class="clear"></div>
                <label for="Fourth" class="float_left">Fourth</label>
                <div class="float_right"><%= Html.TextBox("Fourth", diganosis != null && diganosis.Element("code4") != null ? diganosis.Element("code4").Value : "")%></div><div class="clear"></div>
                <label for="Fifth" class="float_left">Fifth</label>
                <div class="float_right"><%= Html.TextBox("Fifth", diganosis != null && diganosis.Element("code5") != null ? diganosis.Element("code5").Value : "")%></div><div class="clear"></div>
                <label for="Sixth" class="float_left">Sixth</label>
                <div class="float_right"><%= Html.TextBox("Sixth", diganosis != null && diganosis.Element("code6") != null ? diganosis.Element("code6").Value : "")%></div><div class="clear"></div>
            </div>
        </div><div class="row">
            <label for="RapRemark">Remark:</label><%= Html.TextArea("FinalRemark",Model.Remark ,new {  }) %>
        </div>
    </div>        
</fieldset>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="Billing.Navigate(1,'#billingInfo');">Verify and Next</a></li>
</ul></div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>