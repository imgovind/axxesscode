﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateBilling" }))%>
<% { %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0)){%>
 <div class="buttons float_left">
    <ul class="">
        <li class="align_left"> <a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate Claim and Save</a></li>
    </ul>
</div>
<%} else {%>
<div class="row"><b>Select the claim you want to generate.</b></div>
<%} %>
<% if (Model != null){ %>
<% if (Model.Raps != null && Model.Raps.Count > 0){%>
<div class="row">
    <table class="claim" width="100%">
        <thead>
            <tr>
                <th>
                    Patient Name
                </th>
                <th>
                    Medicare No
                </th>
                <th>
                    Episode
                </th>
                <th>
                    Claim Amount
                </th>
            </tr>
        </thead>
        <tbody>
            <%
                int j = 1;%>
            <% foreach (var rap in Model.Raps)
               {%>
            <% if (j % 2 == 0)
               {%>
            <tr class="even">
                <%}
               else
               { %>
                <tr class="odd">
                    <%} %>
                    <td><%=Html.Hidden("rapSelected", rap.Id) %><%=j %>.&nbsp;&nbsp;&nbsp;<%=rap.DisplayName + "(" + rap.PatientIdNumber + ")"%></td>
                    <td><%=rap.MedicareNumber%></td>
                    <td><% if (rap.EpisodeStartDate != null) {%><%=rap.EpisodeStartDate.IsValid()? rap.EpisodeStartDate.ToString("MM/dd/yyyy"):string.Empty%><%} %> <% if (rap.EpisodeEndDate != null){%>-<%=rap.EpisodeEndDate .IsValid()?rap.EpisodeEndDate.ToString("MM/dd/yyyy"):string.Empty%><%} %></td>
                    <td></td>
                </tr>
                <% j++;
               } %>
        </tbody>
        <tfoot><tr></tr></tfoot>
    </table>
</div>
<%} %><% if (Model.Finals != null && Model.Finals.Count > 0){%>
<div class="row">
    <table class="claim">
        <thead>
            <tr>
                <th>
                    Patient Name
                </th>
                <th>
                    Medicare No
                </th>
                <th>
                    Episode Date
                </th>
                <th>
                    Claim Amount
                </th>
            </tr>
        </thead>
        <tbody>
            <% int j = 1;%>
            <% foreach (var final in Model.Finals)
               {%>
            <% if (j % 2 == 0)
               {%>
            <tr class="even">
                <%}
               else
               { %>
                <tr class="odd">
                    <%} %>
                    <td>
                        <%=Html.Hidden("finalSelected", final.Id) %>
                        <%=j %>
                        .&nbsp;&nbsp;&nbsp;
                        <%=final.DisplayName + "(" + final.PatientIdNumber + ")"%>
                    </td>
                    <td>
                        <%=final.MedicareNumber%>
                    </td>
                    <td>
                        <% if (final.EpisodeStartDate != null)
                           {%>
                        <%=final.EpisodeStartDate.IsValid()? final.EpisodeStartDate.ToString("MM/dd/yyyy"): string.Empty%>
                        <%} %>
                        <% if (final.EpisodeEndDate != null)
                           {%>
                        -<%=final.EpisodeEndDate.IsValid() ? final.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty%>
                        <%} %>
                    </td>
                    <td>
                    </td>
                </tr>
                <% j++;
               } %>
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
</div>
<% }%><div class="clear"></div><% } %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0)){%>
<% = Html.Hidden("StatusType","Submit") %>
<div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Billing.UpdateStatus('#generateBilling');">Mark Claim(s) As Submitted</a></li></ul></div>
<%}%><%}%>
