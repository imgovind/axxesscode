﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisitForm" })) { %>
<%= Html.Hidden("Id",Model.Id) %>
<%= Html.Hidden("episodeId",Model.EpisodeId) %>
<%= Html.Hidden("patientId",Model.PatientId) %><%
    var notVerifiedVisits = Model.Visits.ToObject<List<ScheduleEvent>>().Where(s=>s.IsDeprecated==false && s.EventDate.ToDateTime()>=Model.EpisodeStartDate && s.EventDate.ToDateTime()<=Model.EpisodeEndDate && s.DisciplineTask>0);
    List<ScheduleEvent>[] a = new List<ScheduleEvent>[] { notVerifiedVisits.Where(v => v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "430" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "230" || v.Status == "235") && !v.IsMissedVisit).ToList(), notVerifiedVisits.Where(v => (!v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit) || (!v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit)).ToList(), notVerifiedVisits.Where(v => v.IsMissedVisit).ToList() };
    String[] b = new String[] {"Billable Visits","Non-Billable Visits","Missed Visit"};
    String[] c = new String[] {"PT", "Nursing", "ST", "OT", "MSW","HHA"};
    String[] d = new String[] {"Physical Therapy (0421)", "Skilled Nursing (0551)", "Speech Therapy (0440)", "Occupational Therapy (0431)", "Social Worker (0561)","HHA (0571)"};
    String[] e = new String[] {"G0151", "G0154", "G0153", "G0152", "G0155","G0156"};
    %>
<div class="wrapper main">
    <div class="billing">
        <h3 class="align_center">Episode: <%= Model.EpisodeStartDate.ToShortDateString()%> &ndash; <%= Model.EpisodeEndDate.ToShortDateString()%></h3><%
    for (int g = 0; g < 3; g++) {
        if (a[g] != null && a[g].ToList().Count > 0) { %>
        <ul>
            <li class="align_center"><h3><%= b[g] %></h3></li>
            <li>
                <span class="rapcheck"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Date</span>
                <span class="visithcpcs">HCPCS</span>
                <span class="visitstatus">Status</span>
                <span class="visitunits">Units</span>
                <span class="visitcharge">Charge</span>
            </li>
        </ul><%
            for (int h = 0; h < 6; h++) {
                if (a[g] != null) {
                    var visits = a[g].Where(f => f.Discipline == c[h]).ToList();
                    if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="discpiline-title"><span><%= d[h] %></span></li><% 
                        int i = 1;
                        foreach (var visit in visits) { %> <% var discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;  %>
            <li class="<%= i % 2 != 0 ? "odd notready" : "even notready" %>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');">
                <label for="<%= "Visit" + visit.EventId.ToString() %>">
                    <span class="rapcheck"><%= i%>. <input class="radio" name="Visit" type="checkbox" id='<%= "Visit" + visit.EventId.ToString() %>' value='<%= g == 0 ? visit.EventId.ToString() + "' checked='checked" : visit.EventId.ToString() %>' /></span>
                    <span class="visittype"><%= visit.DisciplineTaskName%></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty()? visit.EventDate.ToDateTime().ToString("MM/dd/yyy"):"" %></span>
                    <span class="visithcpcs"><%= e[h] %></span>
                    <span class="visitstatus"><%= visit.StatusName %></span>
                    <span class="visitunits"><% = Model.BillInformations!=null && Model.BillInformations.ContainsKey(discipline)?Model.BillInformations[discipline].Unit.ToString():""  %> </span>
                    <span class="visitcharge"><% = Model.BillInformations!=null && Model.BillInformations.ContainsKey(discipline)?Model.BillInformations[discipline].Amount.ToString():""  %></span>
                </label>
            </li><%
                            i++;
                        }
                    }
                }
                } %>
        </ol><%
        }
    } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(0);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.Navigate(2,'#billingVisitForm');">Verify and Next</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $("#FinalTabStrip-2 ol").each(function() { $("li:last", $(this)).addClass("last"); });
</script>
