﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
    <fieldset><%= Html.Hidden("Id", Model.Id)%>
        <div class="column">
            <div class="row">
                <div class="float_left">Patient First Name:</div>
                <div class="float_right light"><%= Model.FirstName%></div>
            </div><div class="row">
                <div class="float_left">Patient Last Name:</div>
                <div class="float_right light"><%= Model.LastName%></div>
            </div><div class="row">
                <div class="float_left">Medicare #:</div>
                <div class="float_right light"><%= Model.MedicareNumber%></div>
            </div><div class="row">
                <div class="float_left">Patient Record #:</div>
                <div class="float_right light"><%= Model.PatientIdNumber%></div>
            </div><div class="row">
                <div class="float_left">Gender:</div>
                <div class="float_right light"><%= Model.Gender%></div>
            </div><div class="row">
                <div class="float_left">Date of Birth:</div>
                <div class="float_right light"><%= Model.DOB != null ? Model.DOB.ToShortDateString() : ""%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float_left">Episode Start Date:</div>
                <div class="float_right light"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Admission Date:</div>
                <div class="float_right light"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Address Line 1:</div>
                <div class="float_right light"><%= Model.AddressLine1%></div>
            </div><div class="row">
                <div class="float_left">Address Line 2:</div>
                <div class="float_right light"><%= Model.AddressLine2%></div>
            </div><div class="row">
                <div class="float_left">City:</div>
                <div class="float_right light"><%= Model.AddressCity%></div>
            </div><div class="row">
                <div class="float_left">State, Zip Code:</div>
                <div class="float_right light"><%= Model.AddressStateCode + Model.AddressZipCode%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column"><% var total = Model.SupplyTotal; %>
            <div class="row">
                <div class="float_left">HIPPS Code:</div>
                <div class="float_right light"><%= Model.HippsCode%></div>
            </div><div class="row">
                <div class="float_left">Oasis Matching Key:</div>
                <div class="float_right light"><%= Model.ClaimKey%></div>
            </div><div class="row">
                <div class="float_left">Date Of First Billable Visit:</div>
                <div class="float_right light"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Physician Last Name, F.I.:</div>
                <div class="float_right light"><%= Model.PhysicianLastName + Model.PhysicianFirstName != null && Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1) : ""%></div>
            </div><div class="row">
                <div class="float_left">Physician NPI #:</div>
                <div class="float_right light"><%= Model.PhysicianNPI%></div>
            </div><div class="row">
                <div>Remark:</div>
                <div class="margin light"><p><%= Model.Remark%></p></div>
            </div>
        </div><div class="column">
            <div class="row"><% var diganosis = XElement.Parse(Model.DiagonasisCode); var val = (diganosis != null && diganosis.Element("code1") != null ? diganosis.Element("code1").Value : ""); %>
                <div>Diagonasis Codes:</div>
                <div class="margin">
                    <div class="float_left">Primary</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Second</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Third</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Fourth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Fifth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Sixth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="billing">
        <ul>
            <li>
                <span class="rapcheck"></span>
                <span class="sumdesc">Description</span>
                <span class="visittype">HCPCS/HIIPS Code</span>
                <span class="visitdate">Service Date</span>
                <span class="rapicon">Service Unit</span>
                <span class="rapicon">Total Charges</span>
            </li>
        </ul><ol>
            <li class="first even">
                    <span class="rapcheck">0023</span>
                    <span class="sumdesc">Home Health Services</span>
                    <span class="visittype"><%= Model.HippsCode %></span>
                    <span class="visitdate"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToString("MM/dd/yyy") : ""%></span>
                    <span class="rapicon"></span>
                    <span class="rapicon"></span>
            </li><li class="odd">
                    <span class="rapcheck">0272</span>
                    <span class="sumdesc">Service Supplies</span>
                    <span class="visittype"></span>
                    <span class="visitdate"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToString("MM/dd/yyy") : "" %></span>
                    <span class="rapicon"></span>
                    <span class="rapicon"><%= Math.Round( Model.SupplyTotal + 0.00, 2) %></span>
            </li><%
    var visits = Model != null && Model.VerifiedVisits.IsNotNullOrEmpty() ? Model.VerifiedVisits.ToObject<List<ScheduleEvent>>().OrderBy(s => s.EventDate).ToList() : new List<ScheduleEvent>();
    var i = 0;
    foreach (var visit in visits) { %><% var discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline; total += Model.BillInformations != null && Model.BillInformations.ContainsKey(discipline) && Model.BillInformations[discipline].Amount.IsNotNullOrEmpty() ? double.Parse(Model.BillInformations[discipline].Amount) : 0.00; %>
            <li class='<%= i % 2 != 0 ? "odd" : "even" %>'>
                    <span class="rapcheck"><%
        switch (visit.Discipline) {
            case "PT": Response.Write("0421"); break;
            case "OT": Response.Write("0431"); break;
            case "ST": Response.Write("0440"); break;
            case "Nursing": Response.Write("0551"); break;
            case "MSW": Response.Write("0561"); break;
            case "HHA": Response.Write("0571"); break;
        } %>
                    </span>
                    <span class="sumdesc">Visit</span>
                    <span class="visittype"><%
        switch (visit.Discipline) {
            case "PT": Response.Write("G0151"); break;
            case "OT": Response.Write("G0152"); break;
            case "ST": Response.Write("G0153"); break;
            case "Nursing": Response.Write("G0154"); break;
            case "MSW": Response.Write("G0155"); break;
            case "HHA": Response.Write("G0156"); break;
        } %>
                    </span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate()? visit.EventDate.ToDateTime().ToString("MM/dd/yyyy"):"" %></span>
                    <span class="rapicon"><% = Model.BillInformations!=null && Model.BillInformations.ContainsKey(discipline)?Model.BillInformations[discipline].Unit.ToString():""  %> </span>
                    <span class="rapicon"><% = Model.BillInformations!=null && Model.BillInformations.ContainsKey(discipline)?Model.BillInformations[discipline].Amount.ToString():"" + "$" %></span>
            </li><%
        i++;
    } %>
        </ol>
        <ul class="total"><li class="align_right"><label for="Total">Total:</label> <label> <%= Math.Round(total,2)%></label></li></ul>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(2);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.FinalComplete('<%=Model.Id%>');">Complete</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript"> $("#FinalTabStrip-4 ol li:last").addClass("last"); </script>