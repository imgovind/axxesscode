﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Death at Home | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','deathathome');</script>")
%>
<div id="deathTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_death" tooltip="M0010 &ndash; M0100">Clinical Record Items</a></li>
        <li><a href="#deathAdd_death" tooltip="M0903 &ndash; M0906">Death</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_death" class="general"><% Html.RenderPartial("~/Views/Oasis/Death/Demographics.ascx", Model); %></div>
    <div id="deathAdd_death" class="general loading"></div>
</div>