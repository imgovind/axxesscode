﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<div class="wrapper main">
    <fieldset class="medication">
        <legend>New Medications (Locator #10)</legend>
        <% var medicationProfile = (Model != null && Model.MedicationProfile.IsNotNullOrEmpty()) ? Model.MedicationProfile.ToObject<MedicationProfile>() : new MedicationProfile(); %>
        <% Html.RenderPartial("/Views/Patient/MedicationProfileGrid.ascx", medicationProfile.Id); %>
    </fieldset>
     <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationMedicationForm" })) { %>
    <%= Html.Hidden("Recertification_Id", Model.Id)%>
    <%= Html.Hidden("Recertification_Action", "Edit")%>
    <%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", "Recertification")%> 
    <%= Html.Hidden("categoryType", "Medications")%> 
    <fieldset>
        <legend>Medication Administration Record</legend>
        <div class="column">
            <div class="row">
                <label for="Recertification_GenericMedRecTime" class="float_left">Time:</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecTime", data.ContainsKey("GenericMedRecTime") ? data["GenericMedRecTime"].Answer : "", new { @id = "Recertification_GenericMedRecTime", @class = "vitals", @maxlength = "10" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecMedication" class="float_left">Medication</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecMedication", data.ContainsKey("GenericMedRecMedication") ? data["GenericMedRecMedication"].Answer : "", new { @id = "Recertification_GenericMedRecMedication", @maxlength="30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecDose" class="float_left">Dose</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecDose", data.ContainsKey("GenericMedRecDose") ? data["GenericMedRecDose"].Answer : "", new { @id = "Recertification_GenericMedRecDose", @maxlength = "30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecRoute" class="float_left">Route</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecRoute", data.ContainsKey("GenericMedRecRoute") ? data["GenericMedRecRoute"].Answer : "", new { @id = "Recertification_GenericMedRecRoute", @maxlength = "30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecFrequency" class="float_left">Frequency</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecFrequency", data.ContainsKey("GenericMedRecFrequency") ? data["GenericMedRecFrequency"].Answer : "", new { @id = "Recertification_GenericMedRecFrequency", @maxlength = "30" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="Recertification_GenericMedRecPRN" class="float_left">PRN Reason</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecPRN", data.ContainsKey("GenericMedRecPRN") ? data["GenericMedRecPRN"].Answer : "", new { @id = "Recertification_GenericMedRecPRN", @maxlength = "30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecLocation" class="float_left">Location</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecLocation", data.ContainsKey("GenericMedRecLocation") ? data["GenericMedRecLocation"].Answer : "", new { @id = "Recertification_GenericMedRecLocation", @maxlength = "30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecResponse" class="float_left">Patient Response</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_GenericMedRecResponse", data.ContainsKey("GenericMedRecResponse") ? data["GenericMedRecResponse"].Answer : "", new { @id = "Recertification_GenericMedRecResponse", @maxlength = "30" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericMedRecComments" class="strong">Comment</label>
                <div>
                    <%=Html.TextArea("Recertification_GenericMedRecComments", data.ContainsKey("GenericMedRecComments") ? data["GenericMedRecComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericMedRecComments" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M2030</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2030');">(M2030)</a> Management of Injectable Medications: Patient&rsquo;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.</label>
                <%=Html.Hidden("Recertification_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "Recertification_M2030ManagementOfInjectableMedications0", @class = "radio float_left" })%>
                        <label for="Recertification_M2030ManagementOfInjectableMedications0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "Recertification_M2030ManagementOfInjectableMedications1", @class = "radio float_left" })%>
                        <label for="Recertification_M2030ManagementOfInjectableMedications1">
                            <span class="float_left">1 &ndash;</span>
                            <span class="normal margin">Able to take injectable medication(s) at the correct times if:
                                <ul>
                                    <li><span class="float_left">(a)</span><span class="radio">individual syringes are prepared in advance by another person; OR</span></li>
                                    <li><span class="float_left">(b)</span><span class="radio">another person develops a drug diary or chart.</span></li>
                                </ul>
                            </span>
                        </label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "Recertification_M2030ManagementOfInjectableMedications2", @class = "radio float_left" })%>
                        <label for="Recertification_M2030ManagementOfInjectableMedications2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "Recertification_M2030ManagementOfInjectableMedications3", @class = "radio float_left" })%>
                        <label for="Recertification_M2030ManagementOfInjectableMedications3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to take injectable medication unless administered by another person.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2030');">?</div>
                        </div>
                        <%=Html.RadioButton("Recertification_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "Recertification_M2030ManagementOfInjectableMedications4", @class = "radio float_left" })%>
                        <label for="Recertification_M2030ManagementOfInjectableMedications4"><span class="float_left">NA &ndash;</span><span class="normal margin">No injectable medications prescribed.</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <%string[] medicationInterventions = data.ContainsKey("485MedicationInterventions") && data["485MedicationInterventions"].Answer != "" ? data["485MedicationInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485MedicationInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions1' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='1' {0} />", medicationInterventions!=null && medicationInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485MedicationInterventions1" class="radio">SN to assess patient filling medication box to determine if patient is preparing correctly.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions2' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='2' {0} />", medicationInterventions!=null && medicationInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485MedicationInterventions2" class="radio">SN to assess caregiver filling medication box to determine if caregiver is preparing correctly.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions3' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='3' {0} />", medicationInterventions!=null && medicationInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions3">SN to determine if the</label>
                    <%  var determineFrequencEachMedPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485DetermineFrequencEachMedPerson") && data["485DetermineFrequencEachMedPerson"].Answer != "" ? data["485DetermineFrequencEachMedPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485DetermineFrequencEachMedPerson", determineFrequencEachMedPerson)%>
                    <label for="Recertification_485MedicationInterventions3">is able to identify the correct dose, route, and frequency of each medication.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions4' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='4' {0} />", medicationInterventions!=null && medicationInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions4">SN to assess if the</label>
                    <%  var assessIndicationEachMedPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485AssessIndicationEachMedPerson") && data["485AssessIndicationEachMedPerson"].Answer != "" ? data["485AssessIndicationEachMedPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485AssessIndicationEachMedPerson", assessIndicationEachMedPerson)%>
                    <label for="Recertification_485MedicationInterventions4">can verbalize an understanding of the indication for each medication.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions5' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='5' {0} />", medicationInterventions!=null && medicationInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485MedicationInterventions5" class="radio">SN to establish reminders to alert patient to take medications at correct times.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions6' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='6' {0} />", medicationInterventions!=null && medicationInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions6">SN to assess the</label>
                    <%  var assessAdminInjectMedsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485AssessAdminInjectMedsPerson") && data["485AssessAdminInjectMedsPerson"].Answer != "" ? data["485AssessAdminInjectMedsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485AssessAdminInjectMedsPerson", assessAdminInjectMedsPerson)%>
                    <label for="Recertification_485MedicationInterventions6">administering injectable medications to determine if proper technique is utilized.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions7' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='7' {0} />", medicationInterventions!=null && medicationInterventions.Contains("7") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions7">SN to instruct the</label>
                    <select name="Recertification_485InstructHighRiskMedsPerson" id="Recertification_485InstructHighRiskMedsPerson">
                        <option value="Patient/Caregiver">Patient/Caregiver</option>
                        <option value="Patient">Patient</option>
                        <option value="Caregiver">Caregiver</option>
                    </select>
                    <label for="Recertification_485MedicationInterventions7">on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets, sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle relaxants.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions8' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='8' {0} />", medicationInterventions!=null && medicationInterventions.Contains("8") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions8">SN to administer IV</label>
                    <%=Html.TextBox("Recertification_485AdministerIVType", data.ContainsKey("485AdministerIVType") ? data["485AdministerIVType"].Answer : "", new { @id = "Recertification_485AdministerIVType", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions8">at rate of</label>
                    <%=Html.TextBox("Recertification_485AdministerIVRate", data.ContainsKey("485AdministerIVRate") ? data["485AdministerIVRate"].Answer : "", new { @id = "Recertification_485AdministerIVRate", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions8">via</label>
                    <%=Html.TextBox("Recertification_485AdministerIVVia", data.ContainsKey("485AdministerIVVia") ? data["485AdministerIVVia"].Answer : "", new { @id = "Recertification_485AdministerIVVia", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions8">every</label>
                    <%=Html.TextBox("Recertification_485AdministerIVEvery", data.ContainsKey("485AdministerIVEvery") ? data["485AdministerIVEvery"].Answer : "", new { @id = "Recertification_485AdministerIVEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions9' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='9' {0} />", medicationInterventions!=null && medicationInterventions.Contains("9") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions9">SN to instruct the</label>
                    <%  var instructAdministerIVPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructAdministerIVPerson") && data["485InstructAdministerIVPerson"].Answer != "" ? data["485InstructAdministerIVPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructAdministerIVPerson", instructAdministerIVPerson)%>
                    <label for="Recertification_485MedicationInterventions9">to administer IV at rate of</label>
                    <%=Html.TextBox("Recertification_485InstructAdministerIVRate", data.ContainsKey("485InstructAdministerIVRate") ? data["485InstructAdministerIVRate"].Answer : "", new { @id = "Recertification_485InstructAdministerIVRate", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions9">via</label>
                    <%=Html.TextBox("Recertification_485InstructAdministerIVVia", data.ContainsKey("485InstructAdministerIVVia") ? data["485InstructAdministerIVVia"].Answer : "", new { @id = "Recertification_485InstructAdministerIVVia", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions9">every</label>
                    <%=Html.TextBox("Recertification_485InstructAdministerIVEvery", data.ContainsKey("485InstructAdministerIVEvery") ? data["485InstructAdministerIVEvery"].Answer : "", new { @id = "Recertification_485InstructAdministerIVEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions10' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='10' {0} />", medicationInterventions!=null && medicationInterventions.Contains("10") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions10">SN to flush peripheral IV with</label>
                    <%=Html.TextBox("Recertification_485FlushPeripheralIVWith", data.ContainsKey("485FlushPeripheralIVWith") ? data["485FlushPeripheralIVWith"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVWith", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions10">cc of</label>
                    <%=Html.TextBox("Recertification_485FlushPeripheralIVOf", data.ContainsKey("485FlushPeripheralIVOf") ? data["485FlushPeripheralIVOf"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVOf", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions10">every</label>
                    <%=Html.TextBox("Recertification_485FlushPeripheralIVEvery", data.ContainsKey("485FlushPeripheralIVEvery") ? data["485FlushPeripheralIVEvery"].Answer : "", new { @id = "Recertification_485FlushPeripheralIVEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions11' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='11' {0} />", medicationInterventions!=null && medicationInterventions.Contains("11") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions11">SN to instruct the</label>
                    <%  var instructFlushPerpheralIVPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructFlushPerpheralIVPerson") && data["485InstructFlushPerpheralIVPerson"].Answer != "" ? data["485InstructFlushPerpheralIVPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructFlushPerpheralIVPerson", instructFlushPerpheralIVPerson)%>
                    <label for="Recertification_485MedicationInterventions11">to flush peripheral IV with</label>
                    <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVWith", data.ContainsKey("485InstructFlushPerpheralIVWith") ? data["485InstructFlushPerpheralIVWith"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVWith", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions11">cc of</label>
                    <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVOf", data.ContainsKey("485InstructFlushPerpheralIVOf") ? data["485InstructFlushPerpheralIVOf"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVOf", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions11">every</label>
                    <%=Html.TextBox("Recertification_485InstructFlushPerpheralIVEvery", data.ContainsKey("485InstructFlushPerpheralIVEvery") ? data["485InstructFlushPerpheralIVEvery"].Answer : "", new { @id = "Recertification_485InstructFlushPerpheralIVEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions12' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='12' {0} />", medicationInterventions!=null && medicationInterventions.Contains("12") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions12">SN to flush central line with</label>
                    <%=Html.TextBox("Recertification_485FlushCentralLineWith", data.ContainsKey("485FlushCentralLineWith") ? data["485FlushCentralLineWith"].Answer : "", new { @id = "Recertification_485FlushCentralLineWith", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions12">cc of</label>
                    <%=Html.TextBox("Recertification_485FlushCentralLineOf", data.ContainsKey("485FlushCentralLineOf") ? data["485FlushCentralLineOf"].Answer : "", new { @id = "Recertification_485FlushCentralLineOf", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions12">every</label>
                    <%=Html.TextBox("Recertification_485FlushCentralLineEvery", data.ContainsKey("485FlushCentralLineEvery") ? data["485FlushCentralLineEvery"].Answer : "", new { @id = "Recertification_485FlushCentralLineEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions13' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='13' {0} />", medicationInterventions!=null && medicationInterventions.Contains("13") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions13">SN to instruct</label>
                    <%  var instructFlushCentralLinePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructFlushCentralLinePerson") && data["485InstructFlushCentralLinePerson"].Answer != "" ? data["485InstructFlushCentralLinePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructFlushCentralLinePerson", instructFlushCentralLinePerson)%>
                    <label for="Recertification_485MedicationInterventions13">to flush central line with</label>
                    <%=Html.TextBox("Recertification_485InstructFlushCentralLineWith", data.ContainsKey("485InstructFlushCentralLineWith") ? data["485InstructFlushCentralLineWith"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineWith", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions13">cc of</label>
                    <%=Html.TextBox("Recertification_485InstructFlushCentralLineOf", data.ContainsKey("485InstructFlushCentralLineOf") ? data["485InstructFlushCentralLineOf"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineOf", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions13">every</label>
                    <%=Html.TextBox("Recertification_485InstructFlushCentralLineEvery", data.ContainsKey("485InstructFlushCentralLineEvery") ? data["485InstructFlushCentralLineEvery"].Answer : "", new { @id = "Recertification_485InstructFlushCentralLineEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions14' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='14' {0} />", medicationInterventions!=null && medicationInterventions.Contains("14") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions14">SN to access</label>
                    <%=Html.TextBox("Recertification_485AccessPortType", data.ContainsKey("485AccessPortType") ? data["485AccessPortType"].Answer : "", new { @id = "Recertification_485AccessPortType", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions14">port every</label>
                    <%=Html.TextBox("Recertification_485AccessPortTypeEvery", data.ContainsKey("485AccessPortTypeEvery") ? data["485AccessPortTypeEvery"].Answer : "", new { @id = "Recertification_485AccessPortTypeEvery", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions14">and flush with</label>
                    <%=Html.TextBox("Recertification_485AccessPortTypeWith", data.ContainsKey("485AccessPortTypeWith") ? data["485AccessPortTypeWith"].Answer : "", new { @id = "Recertification_485AccessPortTypeWith", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions14">cc of</label>
                    <%=Html.TextBox("Recertification_485AccessPortTypeOf", data.ContainsKey("485AccessPortTypeOf") ? data["485AccessPortTypeOf"].Answer : "", new { @id = "Recertification_485AccessPortTypeOf", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485MedicationInterventions14">every</label>
                    <%=Html.TextBox("Recertification_485AccessPortTypeFrequency", data.ContainsKey("485AccessPortTypeFrequency") ? data["485AccessPortTypeFrequency"].Answer : "", new { @id = "Recertification_485AccessPortTypeFrequency", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationInterventions15' class='radio float_left' type='checkbox' name='Recertification_485MedicationInterventions' value='15' {0} />", medicationInterventions!=null && medicationInterventions.Contains("15") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationInterventions15">SN to change IV tubing every</label>
                    <%=Html.TextBox("Recertification_485ChangeIVTubingEvery", data.ContainsKey("485ChangeIVTubingEvery") ? data["485ChangeIVTubingEvery"].Answer : "", new { @id = "Recertification_485ChangeIVTubingEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <label for="Recertification_485MedicationInterventionTemplates">Additional Orders:</label>
                <%  var medicationInterventionTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485MedicationInterventionTemplates") && data["485MedicationInterventionTemplates"].Answer != "" ? data["485MedicationInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485MedicationInterventionTemplates", medicationInterventionTemplates)%>
                <%=Html.TextArea("Recertification_485MedicationInterventionComments", data.ContainsKey("485MedicationInterventionComments") ? data["485MedicationInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485MedicationInterventionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <%string[] medicationGoals = data.ContainsKey("485MedicationGoals") && data["485MedicationGoals"].Answer != "" ? data["485MedicationGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485MedicationGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485MedicationGoals1' class='radio float_left' type='checkbox' name='Recertification_485MedicationGoals' value='1' {0} />", medicationGoals!=null && medicationGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485MedicationGoals1" class="radio">Patient will remain free of adverse medication reactions during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationGoals2' class='radio float_left' type='checkbox' name='Recertification_485MedicationGoals' value='2' {0} />", medicationGoals!=null && medicationGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationGoals2">The</label>
                    <%  var verbalizeMedRegimenUnderstandingPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485VerbalizeMedRegimenUnderstandingPerson") && data["485VerbalizeMedRegimenUnderstandingPerson"].Answer != "" ? data["485VerbalizeMedRegimenUnderstandingPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485VerbalizeMedRegimenUnderstandingPerson", verbalizeMedRegimenUnderstandingPerson)%>
                    <label for="Recertification_485MedicationGoals2">will verbalize understanding of medication regimen, dose, route, frequency, indications, and side effects by:</label>
                    <%=Html.TextBox("Recertification_485VerbalizeMedRegimenUnderstandingDate", data.ContainsKey("485VerbalizeMedRegimenUnderstandingDate") ? data["485VerbalizeMedRegimenUnderstandingDate"].Answer : "", new { @id = "Recertification_485VerbalizeMedRegimenUnderstandingDate", @class = "zip", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485MedicationGoals3' class='radio float_left' type='checkbox' name='Recertification_485MedicationGoals' value='3' {0} />", medicationGoals!=null && medicationGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485MedicationGoals3">The</label>
                    <%  var demonstratePeripheralIVLineFlushPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485DemonstratePeripheralIVLineFlushPerson") && data["485DemonstratePeripheralIVLineFlushPerson"].Answer != "" ? data["485DemonstratePeripheralIVLineFlushPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485DemonstratePeripheralIVLineFlushPerson", demonstratePeripheralIVLineFlushPerson)%>
                    <label for="Recertification_485MedicationGoals3">will demonstrate understanding of flushing peripheral IV line.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485MedicationGoalTemplates">Additional Goals:</label>
                <%  var medicationGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485MedicationGoalTemplates") && data["485MedicationGoalTemplates"].Answer != "" ? data["485MedicationGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485MedicationGoalTemplates", medicationGoalTemplates)%>
                <%=Html.TextArea("Recertification_485MedicationGoalComments", data.ContainsKey("485MedicationGoalComments") ? data["485MedicationGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485MedicationGoalComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
    $("#OasisMedicatonGrid .t-grid-toolbar").prepend(unescape("%3Cem class=%22abs%22 style=%22left:50%;margin-left:-50px;%22%3E* LS = Long Standing%3C/em%3E%3Cdi" +
        "v class=%22buttons abs_right%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22acore.openprintview('/Patient/MedicationProfilePrint/" +
        "<%= Model.PatientId %>');%22%3EView Medication Profile%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
</script>