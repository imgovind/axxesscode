﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationSensoryStatusForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id) %>
<%= Html.Hidden("Recertification_Action", "Edit") %>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification") %>
<%= Html.Hidden("categoryType", "Sensory")%>
<div class="wrapper main">
    <fieldset>
        <legend>Eyes</legend>
        <% string[] eyes = data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer != "" ? data["GenericEyes"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericEyes" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes1' name='Recertification_GenericEyes' value='1' {0} />", eyes!=null && eyes.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes2' name='Recertification_GenericEyes' value='2' {0} />", eyes!=null && eyes.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes2" class="radio">Glasses</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes3' name='Recertification_GenericEyes' value='3' {0} />", eyes!=null && eyes.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes3" class="radio">Contacts Left</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes4' name='Recertification_GenericEyes' value='4' {0} />", eyes!=null && eyes.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes4" class="radio">Contacts Right</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes5' name='Recertification_GenericEyes' value='5' {0} />", eyes!=null && eyes.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes5" class="radio">Blurred Vision</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes6' name='Recertification_GenericEyes' value='6' {0} />", eyes!=null && eyes.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes6" class="radio">Glaucoma</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes7' name='Recertification_GenericEyes' value='7' {0} />", eyes!=null && eyes.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes7" class="radio">Cataracts</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes8' name='Recertification_GenericEyes' value='8' {0} />", eyes!=null && eyes.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes8" class="radio">Macular Degeneration</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEyes13' name='Recertification_GenericEyes' value='13' {0} />", eyes!=null && eyes.Contains("13") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEyes13" class="radio more small">Other</label>
                            </td><td>
                                <div id="Recertification_GenericEyes13More"><label for="Recertification_GenericEyesOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericEyesOtherDetails", data.ContainsKey("GenericEyesOtherDetails") ? data["GenericEyesOtherDetails"].Answer : "", new { @id = "Recertification_GenericEyesOtherDetails", @maxlength = "20" })%></div>
                            </td><td colspan="2">
                                <label for="Recertification_GenericEyesLastEyeExamDate" class="radio more small">Date of Last Eye Exam</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_GenericEyesLastEyeExamDate").Value(data.ContainsKey("GenericEyesLastEyeExamDate") && data["GenericEyesLastEyeExamDate"].Answer.IsNotNullOrEmpty() ? data["GenericEyesLastEyeExamDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_GenericEyesLastEyeExamDate", @class = "date" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Ears</legend>
        <% string[] ears = data.ContainsKey("GenericEars") && data["GenericEars"].Answer != "" ? data["GenericEars"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericEars" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars1' name='Recertification_GenericEars' value='1' {0} />", ears!=null && ears.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars3' name='Recertification_GenericEars' value='3' {0} />", ears!=null && ears.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars3" class="radio">Deaf</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars4' name='Recertification_GenericEars' value='4' {0} />", ears!=null && ears.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars4" class="radio">Drainage</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars5' name='Recertification_GenericEars' value='5' {0} />", ears!=null && ears.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars5" class="radio">Pain</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars2' name='Recertification_GenericEars' value='2' {0} />", ears!=null && ears.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars2" class="radio more small">Hearing Impaired</label>
                            </td><td>
                                <div id="Recertification_GenericEars2More">
                                    <%= Html.Hidden("Recertification_GenericEarsHearingImpairedPosition")%>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingImpairedPosition", "0", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "0" ? true : false, new { @id = "Recertification_GenericEarsHearingImpairedPosition0", @class = "radio" }) %>
                                    <label for="Recertification_GenericEarsHearingImpairedPosition0" class="inlineradio">Bilateral</label>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingImpairedPosition", "1", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "1" ? true : false, new { @id = "Recertification_GenericEarsHearingImpairedPosition1", @class = "radio" }) %>
                                    <label for="Recertification_GenericEarsHearingImpairedPosition1" class="inlineradio">Left</label>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingImpairedPosition", "2", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "2" ? true : false, new { @id = "Recertification_GenericEarsHearingImpairedPosition2", @class = "radio" })%>
                                    <label for="Recertification_GenericEarsHearingImpairedPosition2" class="inlineradio">Right</label>
                                </div>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars6' name='Recertification_GenericEars' value='6' {0} />", ears!=null && ears.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericEars6" class="radio more small">Hearing Aids</label>
                            </td><td>
                                <div id="Recertification_GenericEars6More">
                                    <%= Html.Hidden("Recertification_GenericEarsHearingAidsPosition") %>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingAidsPosition", "0", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "0" ? true : false, new { @id = "Recertification_GenericEarsHearingAidsPosition0", @class = "radio" })%>
                                    <label for="Recertification_GenericEarsHearingAidsPosition0" class="inlineradio">Bilateral</label>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingAidsPosition", "1", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "1" ? true : false, new { @id = "Recertification_GenericEarsHearingAidsPosition1", @class = "radio" })%>
                                    <label for="Recertification_GenericEarsHearingAidsPosition1" class="inlineradio">Left</label>
                                    <%= Html.RadioButton("Recertification_GenericEarsHearingAidsPosition", "2", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "2" ? true : false, new { @id = "Recertification_GenericEarsHearingAidsPosition2", @class = "radio" }) %>
                                    <label for="Recertification_GenericEarsHearingAidsPosition2" class="inlineradio">Right</label>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericEars7' name='Recertification_GenericEars' value='7' {0} />", ears != null && ears.Contains("7") ? "checked='checked'" : "")%>
                                <label for="Recertification_GenericEars7" class="radio more small">Other</label>
                            </td><td colspan="3">
                                <div id="Recertification_GenericEars7More"><label for="Recertification_GenericEarsOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericEarsOtherDetails", data.ContainsKey("GenericEarsOtherDetails") ? data["GenericEarsOtherDetails"].Answer : "", new { @id = "Recertification_GenericEarsOtherDetails", @maxlength = "20" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Nose</legend>
        <% string[] nose = data.ContainsKey("GenericNose") && data["GenericNose"].Answer != "" ? data["GenericNose"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericNose" value=" " />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericNose1' name='Recertification_GenericNose' value='1' {0} />", nose!=null && nose.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericNose1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericNose2' name='Recertification_GenericNose' value='2' {0} />", nose!=null && nose.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericNose2" class="radio">Congestion</label>
                            </td><td colspan="2">
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericNose3' name='Recertification_GenericNose' value='3' {0} />", nose!=null && nose.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericNose3" class="radio">Loss of Smell</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericNose4' name='Recertification_GenericNose' value='4' {0} />", nose!=null && nose.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericNose4" class="radio more small fixed">Nose Bleeds</label>
                            </td><td>
                                <div id="Recertification_GenericNose4More"><label for="Recertification_GenericNoseBleedsFrequency"><em>How often?</em></label><%= Html.TextBox("Recertification_GenericNoseBleedsFrequency", data.ContainsKey("GenericNoseBleedsFrequency") ? data["GenericNoseBleedsFrequency"].Answer : "", new { @class = "oe", @id = "Recertification_GenericNoseBleedsFrequency", @maxlength = "10" })%></div>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericNose5' name='Recertification_GenericNose' value='5' {0} />", nose!=null && nose.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_GenericNose5" class="radio more small fixed">Other</label>
                            </td><td>
                                <div id="Recertification_GenericNose5More"><label for="Recertification_GenericNoseOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericNoseOtherDetails", data.ContainsKey("GenericNoseOtherDetails") ? data["GenericNoseOtherDetails"].Answer : "", new { @id = "Recertification_GenericNoseOtherDetails", @maxlength = "20" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Mouth</legend>
        <% string[] mouth = data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer != "" ? data["GenericMouth"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericMouth" value="" />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericMouth1' name='Recertification_GenericMouth' value='1' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericMouth1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericMouth2' name='Recertification_GenericMouth' value='2' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericMouth2" class="radio">Dentures</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericMouth3' name='Recertification_GenericMouth' value='3' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericMouth3" class="radio">Difficulty chewing</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericMouth4' name='Recertification_GenericMouth' value='4' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericMouth4" class="radio">Dysphagia</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericMouth5' name='Recertification_GenericMouth' value='5' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericMouth5" class="radio">Other</label>
                    <div id="Recertification_GenericMouth5More" class="float_right"><label for="Recertification_GenericMouthOther"><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericMouthOther", data.ContainsKey("GenericMouthOther") ? data["GenericMouthOther"].Answer : "", new { @id = "Recertification_GenericMouthOther", @maxlength = "20" })%></div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Throat</legend>
        <% string[] throat = data.ContainsKey("GenericThroat") && data["GenericThroat"].Answer != "" ? data["GenericThroat"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericThroat1' name='Recertification_GenericThroat' value='1' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericThroat1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericThroat2' name='Recertification_GenericThroat' value='2' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericThroat2" class="radio">Sore throat</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericThroat3' name='Recertification_GenericThroat' value='3' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericThroat3" class="radio">Hoarseness</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='Recertification_GenericThroat4' name='Recertification_GenericThroat' value='4' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericThroat4" class="radio">Other</label>
                    <div id="Recertification_GenericThroat4More" class="float_right"><label for="Recertification_GenericThroatOther"><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericThroatOther", data.ContainsKey("GenericThroatOther") ? data["GenericThroatOther"].Answer : "", new { @id = "Recertification_GenericThroatOther", @maxlength = "20" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1200');">(M1200)</a> Vision (with corrective lenses if the patient usually wears them)</label>
                <div class="margin">
                    <%= Html.Hidden("Recertification_M1200Vision") %>
                    <div>
                        <%= Html.RadioButton("Recertification_M1200Vision", "00", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? true : false, new { @id = "Recertification_M1200Vision00", @class = "radio float_left" }) %>
                        <label for="Recertification_M1200Vision00"><span class="float_left">0 &ndash;</span><span class="normal margin">Normal vision: sees adequately in most situations; can see medication labels, newsprint.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%= Html.RadioButton("Recertification_M1200Vision", "01", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? true : false, new { @id = "Recertification_M1200Vision01", @class = "radio float_left" })%>
                        <label for="Recertification_M1200Vision01"><span class="float_left">1 &ndash;</span><span class="normal margin">Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1200');">?</div>
                        </div>
                        <%= Html.RadioButton("Recertification_M1200Vision", "02", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? true : false, new { @id = "Recertification_M1200Vision02", @class = "radio float_left" })%>
                        <label for="Recertification_M1200Vision02"><span class="float_left">2 &ndash;</span><span class="normal margin">Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Interventions</legend>
        <% string[] sensoryStatusIntervention = data.ContainsKey("485SensoryStatusIntervention") && data["485SensoryStatusIntervention"].Answer != "" ? data["485SensoryStatusIntervention"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485SensoryStatusIntervention" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='Recertification_485SensoryStatusIntervention1' name='Recertification_485SensoryStatusIntervention' value='1' type='checkbox' {0} />", sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485SensoryStatusIntervention1" class="radio">ST to evaluate.</label>
            </div><div class="row">
                <label for="Recertification_485SensoryStatusOrderTemplates" class="strong">Additional Orders:</label>
                <%  var sensoryStatusOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485SensoryStatusOrderTemplates") && data["485SensoryStatusOrderTemplates"].Answer != "" ? data["485SensoryStatusOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485SensoryStatusOrderTemplates", sensoryStatusOrderTemplates) %>
                <%= Html.TextArea("Recertification_485SensoryStatusInterventionComments", data.ContainsKey("485SensoryStatusInterventionComments") ? data["485SensoryStatusInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485SensoryStatusInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Goals</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Recertification_485SensoryStatusGoalTemplates" class="strong">Additional Goals:</label>
                <%  var sensoryStatusGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485SensoryStatusGoalTemplates") && data["485SensoryStatusGoalTemplates"].Answer != "" ? data["485SensoryStatusGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485SensoryStatusGoalTemplates", sensoryStatusGoalTemplates) %>
                <%= Html.TextArea("Recertification_485SensoryStatusGoalComments", data.ContainsKey("485SensoryStatusGoalComments") ? data["485SensoryStatusGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485SensoryStatusGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#Recertification_GenericEyes13"), $("#Recertification_GenericEyes13More"));
    Oasis.showIfChecked($("#Recertification_GenericEars2"), $("#Recertification_GenericEars2More"));
    Oasis.showIfChecked($("#Recertification_GenericEars6"), $("#Recertification_GenericEars6More"));
    Oasis.showIfChecked($("#Recertification_GenericEars7"), $("#Recertification_GenericEars7More"));
    Oasis.showIfChecked($("#Recertification_GenericNose4"), $("#Recertification_GenericNose4More"));
    Oasis.showIfChecked($("#Recertification_GenericNose5"), $("#Recertification_GenericNose5More"));
    Oasis.showIfChecked($("#Recertification_GenericMouth5"), $("#Recertification_GenericMouth5More"));
    Oasis.showIfChecked($("#Recertification_GenericThroat4"), $("#Recertification_GenericThroat4More"));
</script>