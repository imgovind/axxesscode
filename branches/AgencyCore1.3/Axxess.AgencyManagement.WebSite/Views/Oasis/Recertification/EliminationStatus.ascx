﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationEliminationForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id) %>
<%= Html.Hidden("Recertification_Action", "Edit") %>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId) %>
<%= Html.Hidden("assessment", "Recertification") %>
<%= Html.Hidden("categoryType", "Elimination") %>
<div class="wrapper main">
    <fieldset class="half float_left">
        <legend>GU</legend>
        <input type="hidden" name="Recertification_GenericGU" value=" " />
        <%string[] genericGU = data.ContainsKey("GenericGU") && data["GenericGU"].Answer != "" ? data["GenericGU"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='Recertification_GenericGU1' class='radio float_left' name='Recertification_GenericGU' value='1' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU2' class='radio float_left' name='Recertification_GenericGU' value='2' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU2" class="radio">Incontinence</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU3' class='radio float_left' name='Recertification_GenericGU' value='3' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU3" class="radio">Bladder Distention</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU4' class='radio float_left' name='Recertification_GenericGU' value='4' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU4" class="radio">Discharge</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU5' class='radio float_left' name='Recertification_GenericGU' value='5' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU5" class="radio">Frequency</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU6' class='radio float_left' name='Recertification_GenericGU' value='6' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU6" class="radio">Dysuria</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU7' class='radio float_left' name='Recertification_GenericGU' value='7' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU7" class="radio">Retention</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU8' class='radio float_left' name='Recertification_GenericGU' value='8' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU8" class="radio">Urgency</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericGU9' class='radio float_left' name='Recertification_GenericGU' value='9' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericGU9" class="radio">Oliguria</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericGU10' class='radio float_left' name='Recertification_GenericGU' value='10' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("10") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericGU10" class="radio">Catheter/Device:</label>
                </div><div id="Recertification_GenericGU10More" class="rel float_right">
                    <div class="float_right">
                        <%  var genericGUCatheterList = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "N/A", Value = "1" },
                                new SelectListItem { Text = "Foley Catheter ", Value = "2" },
                                new SelectListItem { Text = "Condom Catheter", Value = "3" },
                                new SelectListItem { Text = "Suprapubic Catheter", Value = "4" },
                                new SelectListItem { Text = "Urostomy", Value = "5" },
                                new SelectListItem { Text = "Other", Value = "6" }
                            }, "Value", "Text", data.ContainsKey("GenericGUCatheterList") && data["GenericGUCatheterList"].Answer != "" ? data["GenericGUCatheterList"].Answer : "0"); %>
                        <%= Html.DropDownList("Recertification_GenericGUCatheterList", genericGUCatheterList) %>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right"><label for="Recertification_GenericGUCatheterLastChanged">Last Changed</label><%=Html.TextBox("Recertification_GenericGUCatheterLastChanged", data.ContainsKey("GenericGUCatheterLastChanged") ? data["GenericGUCatheterLastChanged"].Answer : "", new { @id = "Recertification_GenericGUCatheterLastChanged", @class = "st", @maxlength = "10" })%></div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%=Html.TextBox("Recertification_GenericGUCatheterFrequency", data.ContainsKey("GenericGUCatheterFrequency") ? data["GenericGUCatheterFrequency"].Answer : "", new { @id = "Recertification_GenericGUCatheterFrequency", @class = "vitals", @maxlength = "5" })%>
                        <label for="Recertification_GenericGUCatheterFrequency">Fr</label>
                        <%=Html.TextBox("Recertification_GenericGUCatheterAmount", data.ContainsKey("GenericGUCatheterAmount") ? data["GenericGUCatheterAmount"].Answer : "", new { @id = "Recertification_GenericGUCatheterAmount", @class = "vitals", @maxlength = "5" })%>
                        <label for="Recertification_GenericGUCatheterAmount">cc</label>
                    </div>
                </div>
            </div><div class="row">
                <div>
                    <%= string.Format("<input id='Recertification_GenericGU11' class='radio float_left' name='Recertification_GenericGU' value='11' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("11") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericGU11" class="radio">Urine:</label>
                </div><div id="Recertification_GenericGU11More" class="rel float_right">
                    <div class="float_left">
                        <% string[] genericGUUrine = data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer != "" ? data["GenericGUUrine"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericGUUrine" value=" " />
                        <%= string.Format("<input id='Recertification_GenericGUUrine1' class='radio float_left' name='Recertification_GenericGUUrine' value='1' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericGUUrine1" class="fixed inlineradio">Cloudy</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericGUUrine2' class='radio float_left' name='Recertification_GenericGUUrine' value='2' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericGUUrine2" class="fixed inlineradio">Odorous</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericGUUrine3' class='radio float_left' name='Recertification_GenericGUUrine' value='3' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericGUUrine3" class="fixed inlineradio">Sediment</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericGUUrine4' class='radio float_left' name='Recertification_GenericGUUrine' value='4' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericGUUrine4" class="fixed inlineradio">Hematuria</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericGUUrine5' class='radio float_left' name='Recertification_GenericGUUrine' value='5' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericGUUrine5" class="fixed inlineradio">Other</label>
                    </div><div class="float_left">
                        <%=Html.TextBox("Recertification_GenericGUOtherText", data.ContainsKey("GenericGUOtherText") ? data["GenericGUOtherText"].Answer : "", new { @id = "Recertification_GenericGUOtherText", @class = "st", @maxlength = "20" })%>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericGU12' class='radio float_left' name='Recertification_GenericGU' value='12' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("12") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericGU12" class="radio">External Genitalia:</label>
                </div><div id="Recertification_GenericGU12More" class="rel float_right">
                    <div class="float_right"><%=Html.Hidden("Recertification_GenericGUNormal")%>
                        <%=Html.RadioButton("Recertification_GenericGUNormal", "1", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "1" ? true : false, new { @id = "Recertification_GenericGUNormal1", @class = "radio" })%>
                        <label for="Recertification_GenericGUNormal1" class="inlineradio">Normal</label>
                        <%=Html.RadioButton("Recertification_GenericGUNormal", "0", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "0" ? true : false, new { @id = "Recertification_GenericGUNormal0", @class = "radio" })%>
                        <label for="Recertification_GenericGUNormal0" class="inlineradio">Abnormal</label>
                    </div>
                    <div class="clear"></div>
                    <%=Html.Hidden("Recertification_GenericGUClinicalAssessment")%>
                    <div class="float_right">
                        <label>As per:</label>
                        <div class="margin">
                        <%=Html.RadioButton("Recertification_GenericGUClinicalAssessment", "1", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "1" ? true : false, new { @id = "Recertification_GenericGUClinicalAssessment1", @class = "radio" })%>
                        <label for="Recertification_GenericGUClinicalAssessment1" class="inlineradio">Clinician Assessment</label><div class="clear"></div>
                        <%=Html.RadioButton("Recertification_GenericGUClinicalAssessment", "0", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "0" ? true : false, new { @id = "Recertification_GenericGUClinicalAssessment0", @class = "radio" })%>
                        <label for="Recertification_GenericGUClinicalAssessment0" class="inlineradio">Pt/CG Report</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Dialysis</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Is patient on dialysis?</label>
                <%=Html.Hidden("Recertification_GenericPatientOnDialysis")%>
                <div class="float_right">
                    <%=Html.RadioButton("Recertification_GenericPatientOnDialysis", "1", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "1" ? true : false, new { @id = "Recertification_GenericPatientOnDialysis1", @class = "radio" })%>
                    <label for="Recertification_GenericPatientOnDialysis1" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("Recertification_GenericPatientOnDialysis", "0", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "0" ? true : false, new { @id = "Recertification_GenericPatientOnDialysis0", @class = "radio" })%>
                    <label for="Recertification_GenericPatientOnDialysis0" class="inlineradio">No</label>
                    <% string[] genericDialysis = data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer != "" ? data["GenericDialysis"].Answer.Split(',') : null; %>
                    <input type="hidden" name="Recertification_GenericDialysis" value=" " />
                </div>
            </div>
        </div><div class="column">
            <div class="DialysisSpecifics row">
                <div class="row">
                    <label class="float_left">Dialysis Type</label>
                </div><div class="row">
                    <%= string.Format("<input id='Recertification_GenericDialysis1' class='radio float_left' name='Recertification_GenericDialysis' value='1' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDialysis1" class="radio">Peritoneal Dialysis</label>
                </div><div class="row">
                    <%= string.Format("<input id='Recertification_GenericDialysis2' class='radio float_left' name='Recertification_GenericDialysis' value='2' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDialysis2" class="radio">CCPD (Continuous Cyclic Peritoneal Dialysis)</label>
                </div><div class="row">
                    <%= string.Format("<input id='Recertification_GenericDialysis3' class='radio float_left' name='Recertification_GenericDialysis' value='3' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDialysis3" class="radio">IPD (Intermittent Peritoneal Dialysis)</label>
                </div><div class="row">
                    <%= string.Format("<input id='Recertification_GenericDialysis4' class='radio float_left' name='Recertification_GenericDialysis' value='4' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDialysis4" class="radio">CAPD (Continuous Ambulatory Peritoneal Dialysis)</label>
                </div><div class="row">
                    <div>
                        <%= string.Format("<input id='Recertification_GenericDialysis5' class='radio float_left' name='Recertification_GenericDialysis' value='5' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDialysis5" class="radio">Hemodialysis</label>
                    </div><div id="Recertification_GenericDialysis5More" class="float_right rel margin">
                        <% string[] genericDialysisHemodialysis = data.ContainsKey("GenericDialysisHemodialysis") && data["GenericDialysisHemodialysis"].Answer != "" ? data["GenericDialysisHemodialysis"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericDialysisHemodialysis" value=" " />
                        <div class="float_left">
                            <%= string.Format("<input id='Recertification_GenericDialysisHemodialysis1' class='radio float_left' name='Recertification_GenericDialysisHemodialysis' value='1' type='checkbox' {0} />", genericDialysisHemodialysis != null && genericDialysisHemodialysis.Contains("1") ? "checked='checked'" : "")%>
                            <label for="Recertification_GenericDialysisHemodialysis1" class="radio">AV Graft/ Fistula Site:</label>
                        </div><div class="float_right">
                            <%=Html.TextBox("Recertification_GenericDialysisHemodialysisGriftAV", data.ContainsKey("GenericDialysisHemodialysisGriftAV") ? data["GenericDialysisHemodialysisGriftAV"].Answer : "", new { @id = "Recertification_GenericDialysisHemodialysisGriftAV", @maxlength = "25" })%>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='Recertification_GenericDialysisHemodialysis2' class='radio float_left' name='Recertification_GenericDialysisHemodialysis' value='2' type='checkbox' {0} />", genericDialysisHemodialysis != null && genericDialysisHemodialysis.Contains("2") ? "checked='checked'" : "")%>
                            <label for="Recertification_GenericDialysisHemodialysis2" class="radio">Central Venous Catheter Access Site:</label>
                        </div><div class="float_right">
                            <%=Html.TextBox("Recertification_GenericDialysisHemodialysisCentralVenousAV", data.ContainsKey("GenericDialysisHemodialysisCentralVenousAV") ? data["GenericDialysisHemodialysisCentralVenousAV"].Answer : "", new { @id = "Recertification_GenericDialysisHemodialysisCentralVenousAV", @maxlength = "25" })%>
                        </div>
                    </div>
                </div><div class="row">
                    <%= string.Format("<input id='Recertification_GenericDialysis6' class='radio float_left' name='Recertification_GenericDialysis' value='6' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("6") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDialysis6" class="radio">Catheter site free from signs and symptoms of infection</label>
                </div><div class="row">
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericDialysis7' class='radio float_left' name='Recertification_GenericDialysis' value='7' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("7") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDialysis7" class="radio">Other:</label>
                    </div><div class="float_right">
                        <%=Html.TextBox("Recertification_GenericDialysisOtherDesc", data.ContainsKey("GenericDialysisOtherDesc") ? data["GenericDialysisOtherDesc"].Answer : "", new { @id = "Recertification_GenericDialysisOtherDesc", @maxlength = "25" })%>
                    </div>
                </div><div class="row">
                    <label for="Recertification_GenericDialysisCenter" class="strong">Dialysis Center:</label>
                    <%=Html.TextArea("Recertification_GenericDialysisCenter", data.ContainsKey("GenericDialysisCenter") ? data["GenericDialysisCenter"].Answer : "", 3,100,new { @id = "Recertification_GenericDialysisCenter", @maxlength = "25" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>OASIS M1610</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1610');">(M1610)</a> Urinary Incontinence or Urinary Catheter Presence:<%=Html.Hidden("Recertification_M1610UrinaryIncontinence", " ", new { @id = "" })%>
                </div><div class="margin">
                    <div>
                        <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "Recertification_M1610UrinaryIncontinence0", @class = "radio float_left" })%>
                        <label for="Recertification_M1610UrinaryIncontinence0"><span class="float_left">0 &ndash;</span><span class="normal margin">No incontinence or catheter (includes anuria or ostomy for urinary drainage)</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "Recertification_M1610UrinaryIncontinence1", @class = "radio float_left" })%>
                        <label for="Recertification_M1610UrinaryIncontinence1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient is incontinent</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1610');">?</div>
                        </div>
                        <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "Recertification_M1610UrinaryIncontinence2", @class = "radio float_left" })%>
                        <label for="Recertification_M1610UrinaryIncontinence2"><span class="float_left">2 &ndash;</span><span class="normal margin">Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic)</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1620/M1630</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1620');">(M1620)</a> Bowel Incontinence Frequency:
                    <%=Html.Hidden("Recertification_M1620BowelIncontinenceFrequency")%>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency0", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Very rarely or never has bowel incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency1", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency2", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">One to three times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency3", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Four to six times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency4", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">On a daily basis</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequency5", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">More often than once daily</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequencyNA", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequencyNA"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient has ostomy for bowel elimination</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1620');">?</div>
                    </div>
                    <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "UK", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? true : false, new { @id = "Recertification_M1620BowelIncontinenceFrequencyUK", @class = "radio float_left" })%>
                    <label for="Recertification_M1620BowelIncontinenceFrequencyUK"><span class="float_left">UK &ndash;</span><span class="normal margin">Unknown</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1630');">(M1630)</a> Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?
                    <%=Html.Hidden("Recertification_M1630OstomyBowelElimination")%>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "00", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? true : false, new { @id = "Recertification_M1630OstomyBowelElimination0", @class = "radio float_left" })%>
                    <label for="Recertification_M1630OstomyBowelElimination0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient does not have an ostomy for bowel elimination.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "01", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? true : false, new { @id = "Recertification_M1630OstomyBowelElimination1", @class = "radio float_left" })%>
                    <label for="Recertification_M1630OstomyBowelElimination1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1630');">?</div>
                    </div>
                    <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "02", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? true : false, new { @id = "Recertification_M1630OstomyBowelElimination2", @class = "radio float_left" })%>
                    <label for="Recertification_M1630OstomyBowelElimination2"><span class="float_left">2 &ndash;</span><span class="normal margin">The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>GI</legend>
        <% string[] genericDigestive = data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer != "" ? data["GenericDigestive"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericDigestive" name=" " />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='Recertification_GenericDigestive1' class='radio float_left' name='Recertification_GenericDigestive' value='1' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericDigestive1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestive2' class='radio float_left' name='Recertification_GenericDigestive' value='2' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestive2" class="radio">Bowel Sounds:</label>
                </div><div class="float_right">
                    <%  var bowelSounds = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Present/WNL", Value = "1" },
                            new SelectListItem { Text = "Hyperactive", Value = "2" },
                            new SelectListItem { Text = "Hypoactive", Value = "3" },
                            new SelectListItem { Text = "Absent", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericDigestiveBowelSoundsType") ? data["GenericDigestiveBowelSoundsType"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = "Recertification_GenericDigestiveBowelSoundsType" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestive3' class='radio float_left' name='Recertification_GenericDigestive' value='3' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestive3" class="radio">Abdominal Palpation:</label>
                </div><div class="float_right">
                    <%  var abdominalPalpation = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Soft/WNL", Value = "1" },
                            new SelectListItem { Text = "Firm", Value = "2" },
                            new SelectListItem { Text = "Tender", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericAbdominalPalpation") ? data["GenericAbdominalPalpation"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_GenericAbdominalPalpation", abdominalPalpation, new { @id = "Recertification_GenericAbdominalPalpation" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericDigestive4' class='radio float_left' name='Recertification_GenericDigestive' value='4' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericDigestive4" class="radio">Bowel Incontinence</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericDigestive5' class='radio float_left' name='Recertification_GenericDigestive' value='5' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericDigestive5" class="radio">Nausea</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericDigestive6' class='radio float_left' name='Recertification_GenericDigestive' value='6' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericDigestive6" class="radio">Vomiting</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericDigestive7' class='radio float_left' name='Recertification_GenericDigestive' value='7' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericDigestive7" class="radio">GERD</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestive8' class='radio float_left' name='Recertification_GenericDigestive' value='8' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("8") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestive8" class="radio">Abd Girth:</label>
                </div><div class="float_right">
                    <%= Html.TextBox("Recertification_GenericDigestiveAbdGirthLength", data.ContainsKey("GenericDigestiveAbdGirthLength") ? data["GenericDigestiveAbdGirthLength"].Answer : "", new { @id = "Recertification_GenericDigestiveAbdGirthLength", @class = "vitals", @maxlength = "5" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericGUDigestiveComments">Comments:</label>
                <%= Html.TextArea("Recertification_GenericGUDigestiveComments", data.ContainsKey("GenericGUDigestiveComments") ? data["GenericGUDigestiveComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericGUDigestiveComments" })%>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">Elimination</div>
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestive11' class='radio float_left' name='Recertification_GenericDigestive' value='11' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("11") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestive11" class="radio">Last BM:</label>
                </div><div id="Recertification_GenericDigestive11More" class="float_right">
                    <label for="Recertification_GenericDigestiveLastBMDate">Date:</label>
                    <%= Html.Telerik().DatePicker().Name("Recertification_GenericDigestiveLastBMDate").Value(data.ContainsKey("GenericDigestiveLastBMDate") ? data["GenericDigestiveLastBMDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_GenericDigestiveLastBMDate", @class = "date" })%>
                </div>
                <div class="clear"></div>
                <% string[] genericDigestiveLastBM = data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer != "" ? data["GenericDigestiveLastBM"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericDigestiveLastBM" value=" " />
                <div>
                    <%= string.Format("<input id='Recertification_GenericDigestiveLastBM1' class='radio float_left' name='Recertification_GenericDigestiveLastBM' value='1' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestiveLastBM1" class="radio">WNL</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericDigestiveLastBM2' class='radio float_left' name='Recertification_GenericDigestiveLastBM' value='2' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestiveLastBM2" class="radio">Abnormal Stool:</label>
                    <% string[] genericDigestiveLastBMAbnormalStool = data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer != "" ? data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',') : null; %>
                    <input type="hidden" name="Recertification_GenericDigestiveLastBMAbnormalStool" value=" " />
                </div><div id="Recertification_GenericDigestiveLastBM2More" class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericDigestiveLastBMAbnormalStool1' class='radio float_left' name='Recertification_GenericDigestiveLastBMAbnormalStool' value='1' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDigestiveLastBMAbnormalStool1" class="fixed inlineradio">Gray</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericDigestiveLastBMAbnormalStool2' class='radio float_left' name='Recertification_GenericDigestiveLastBMAbnormalStool' value='2' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDigestiveLastBMAbnormalStool2" class="fixed inlineradio">Tarry</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericDigestiveLastBMAbnormalStool3' class='radio float_left' name='Recertification_GenericDigestiveLastBMAbnormalStool' value='3' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDigestiveLastBMAbnormalStool3" class="fixed inlineradio">Fresh Blood</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericDigestiveLastBMAbnormalStool4' class='radio float_left' name='Recertification_GenericDigestiveLastBMAbnormalStool' value='4' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericDigestiveLastBMAbnormalStool4" class="fixed inlineradio">Black</label>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestiveLastBM3' class='radio float_left' name='Recertification_GenericDigestiveLastBM' value='3' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestiveLastBM3" class="radio">Constipation:</label>
                    <%=Html.Hidden("Recertification_GenericDigestiveLastBMConstipationType")%>
                </div><div id="Recertification_GenericDigestiveLastBM3More" class="float_right">
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Chronic", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Chronic" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMConstipationTypeChronic", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMConstipationTypeChronic" class="inlineradio">Chronic</label>
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Acute", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Acute" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMConstipationTypeAcute", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMConstipationTypeAcute" class="inlineradio">Acute</label>
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Occasional", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Occasional" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMConstipationTypeOccasional", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMConstipationTypeOccasional" class="inlineradio">Occasional</label>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestiveLastBM4' class='radio float_left' name='Recertification_GenericDigestiveLastBM' value='4' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericDigestiveLastBM4" class="radio">Diarrhea:</label>
                    <%=Html.Hidden("Recertification_GenericDigestiveLastBMDiarrheaType")%>
                </div><div id="Recertification_GenericDigestiveLastBM4More" class="float_right">
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMDiarrheaType", "Chronic", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Chronic" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMDiarrheaTypeChronic", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMDiarrheaTypeChronic" class="inlineradio">Chronic</label>
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMDiarrheaType", "Acute", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Acute" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMDiarrheaTypeAcute", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMDiarrheaTypeAcute" class="inlineradio">Acute</label>
                    <%=Html.RadioButton("Recertification_GenericDigestiveLastBMDiarrheaType", "Occasional", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Occasional" ? true : false, new { @id = "Recertification_GenericDigestiveLastBMDiarrheaTypeOccasional", @class = "radio" })%>
                    <label for="Recertification_GenericDigestiveLastBMDiarrheaTypeOccasional" class="inlineradio">Occasional</label>
                </div>
                <div class="clear"></div>
            </div><div class="row">
                <div class="strong">Ostomy:</div>
                <% string[] genericDigestiveOstomy = data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer != "" ? data["GenericDigestiveOstomy"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericDigestiveOstomy" value=" " />
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestiveOstomy1' class='radio float_left' name='Recertification_GenericDigestiveOstomy' value='1' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericDigestiveOstomy1" class="">Ostomy Type:</label>
                </div><div class="float_right">
                    <%  var ostomy = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Ileostomy ", Value = "2" },
                            new SelectListItem { Text = "Colostomy", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericDigestiveOstomyType") ? data["GenericDigestiveOstomyType"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_GenericDigestiveOstomyType", ostomy, new { @id = "Recertification_GenericDigestiveOstomyType" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestiveOstomy2' class='radio float_left' name='Recertification_GenericDigestiveOstomy' value='2' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericDigestiveOstomy2" class="radio">Stoma Appearance:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("Recertification_GenericDigestiveStomaAppearance", data.ContainsKey("GenericDigestiveStomaAppearance") ? data["GenericDigestiveStomaAppearance"].Answer : "", new { @id = "Recertification_GenericDigestiveStomaAppearance", @class = "st", @maxlength = "15" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericDigestiveOstomy3' class='radio float_left' name='Recertification_GenericDigestiveOstomy' value='3' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("3") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericDigestiveOstomy3" class="radio">Surrounding Skin:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("Recertification_GenericDigestiveSurSkinType", data.ContainsKey("GenericDigestiveSurSkinType") ? data["GenericDigestiveSurSkinType"].Answer : "", new { @id = "Recertification_GenericDigestiveSurSkinType", @class = "st", @maxlength = "15" })%>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] eliminationInterventions = data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer != "" ? data["485EliminationInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485EliminationInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions1' class='radio float_left' name='Recertification_485EliminationInterventions' value='1' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("1") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions1" class="radio">SN to instruct on establishing bowel regimen.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions2' class='radio float_left' name='Recertification_485EliminationInterventions' value='2' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions2" class="radio">SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions3' class='radio float_left' name='Recertification_485EliminationInterventions' value='3' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions3" class="radio">SN to instruct on care of stoma, surrounding skin and use of skin barrier.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions4' class='radio float_left' name='Recertification_485EliminationInterventions' value='4' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("4") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions4" class="radio">SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions5' class='radio float_left' name='Recertification_485EliminationInterventions' value='5' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("5") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions5" class="radio">SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions6' class='radio float_left' name='Recertification_485EliminationInterventions' value='6' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("6") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485EliminationInterventions6">SN to change catheter every month and PRN using a</label>
                    <%=Html.TextBox("Recertification_485EliminationFoleyCatheterType", data.ContainsKey("485EliminationFoleyCatheterType") ? data["485EliminationFoleyCatheterType"].Answer : "", new { @id = "Recertification_485EliminationFoleyCatheterType" })%>
                    <label for="Recertification_485EliminationInterventions6">F catheter.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions7' class='radio float_left' name='Recertification_485EliminationInterventions' value='7' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("7") ? "checked='checked'" : "")%>
                <label for="Recertification_485EliminationInterventions7" class="radio">SN to instruct on intermittent catheterizations.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485EliminationInterventions8' class='radio float_left' name='Recertification_485EliminationInterventions' value='8' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("8") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485EliminationInterventions8">SN to perform intermittent catheterization every</label>
                    <%=Html.TextBox("Recertification_485EliminationCatheterizationNumber", data.ContainsKey("485EliminationCatheterizationNumber") ? data["485EliminationCatheterizationNumber"].Answer : "", new { @id = "Recertification_485EliminationCatheterizationNumber" })%>
                    <label for="Recertification_485EliminationInterventions8">&amp; prn using sterile technique.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485EliminationOrderTemplates" class="strong">Additional Orders:</label>
                <%  var endocrineOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EliminationOrderTemplates") && data["485EliminationOrderTemplates"].Answer != "" ? data["485EliminationOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485EliminationOrderTemplates", endocrineOrderTemplates)%>
                <%= Html.TextArea("Recertification_485EliminationInterventionComments", data.ContainsKey("485EliminationInterventionComments") ? data["485EliminationInterventionComments"].Answer : "", 2, 70, new { @id = "Recertification_485EliminationInterventionComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#Recertification_GenericGU10"), $("#Recertification_GenericGU10More"));
    Oasis.showIfChecked($("#Recertification_GenericGU11"), $("#Recertification_GenericGU11More"));
    Oasis.showIfChecked($("#Recertification_GenericGUUrine5"), $("#Recertification_GenericGUOtherText"));
    Oasis.showIfChecked($("#Recertification_GenericGU12"), $("#Recertification_GenericGU12More"));
    Oasis.showIfRadioEquals("Recertification_GenericPatientOnDialysis", "1", $(".DialysisSpecifics"));
    Oasis.showIfChecked($("#Recertification_GenericDialysis5"), $("#Recertification_GenericDialysis5More"));
    Oasis.showIfChecked($("#Recertification_GenericDialysisHemodialysis1"), $("#Recertification_GenericDialysisHemodialysisGriftAV"));
    Oasis.showIfChecked($("#Recertification_GenericDialysisHemodialysis2"), $("#Recertification_GenericDialysisHemodialysisCentralVenousAV"));
    Oasis.showIfChecked($("#Recertification_GenericDialysis7"), $("#Recertification_GenericDialysisOtherDesc"));
    Oasis.showIfChecked($("#Recertification_GenericDigestive2"), $("#Recertification_GenericDigestiveBowelSoundsType"));
    Oasis.showIfChecked($("#Recertification_GenericDigestive3"), $("#Recertification_GenericAbdominalPalpation"));
    Oasis.showIfChecked($("#Recertification_GenericDigestive8"), $("#Recertification_GenericDigestiveAbdGirthLength"));
    Oasis.showIfChecked($("#Recertification_GenericDigestive11"), $("#Recertification_GenericDigestive11More"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveLastBM2"), $("#Recertification_GenericDigestiveLastBM2More"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveLastBM3"), $("#Recertification_GenericDigestiveLastBM3More"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveLastBM4"), $("#Recertification_GenericDigestiveLastBM4More"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveOstomy1"), $("#Recertification_GenericDigestiveOstomyType"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveOstomy2"), $("#Recertification_GenericDigestiveStomaAppearance"));
    Oasis.showIfChecked($("#Recertification_GenericDigestiveOstomy3"), $("#Recertification_GenericDigestiveSurSkinType"));
    Oasis.interventions($(".interventions"));
</script>