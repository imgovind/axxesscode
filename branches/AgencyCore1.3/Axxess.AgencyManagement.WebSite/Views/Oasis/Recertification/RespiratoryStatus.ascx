﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationRespiratoryForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id) %>
<%= Html.Hidden("Recertification_Action", "Edit") %>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification") %>
<%= Html.Hidden("categoryType", "Respiratory")%> 
<div class="wrapper main">
    <fieldset>
        <legend>Respiratory</legend>
        <% string[] respiratoryCondition = data.ContainsKey("GenericRespiratoryCondition") && data["GenericRespiratoryCondition"].Answer != "" ? data["GenericRespiratoryCondition"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericRespiratoryCondition" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition1' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='1' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition2' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='2' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("2") ? "checked='checked'" : "" ) %><label for="Recertification_GenericRespiratoryCondition2" class="radio">Lung Sounds:</label></td><% string[] respiratorySounds = data.ContainsKey("GenericRespiratorySounds") && data["GenericRespiratorySounds"].Answer != "" ? data["GenericRespiratorySounds"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericRespiratorySounds" value=" " />
                <div id="Recertification_GenericRespiratoryCondition2More" class="rel">
                    <table class="form">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds1' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='1' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("1") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds1" class="fixed inlineradio">CTA</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsCTAText", data.ContainsKey("GenericLungSoundsCTAText") ? data["GenericLungSoundsCTAText"].Answer : "", new { @id = "Recertification_GenericLungSoundsCTAText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds2' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='2' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("2") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds2" class="fixed inlineradio">Rales</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsRalesText", data.ContainsKey("GenericLungSoundsRalesText") ? data["GenericLungSoundsRalesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsRalesText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds3' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='3' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("3") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds3" class="fixed inlineradio">Rhonchi</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsRhonchiText", data.ContainsKey("GenericLungSoundsRhonchiText") ? data["GenericLungSoundsRhonchiText"].Answer : "", new { @id = "Recertification_GenericLungSoundsRhonchiText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds4' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='4' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("4") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds4" class="fixed inlineradio">Wheezes</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsWheezesText", data.ContainsKey("GenericLungSoundsWheezesText") ? data["GenericLungSoundsWheezesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsWheezesText", @class = "st", @maxlength = "20" }) %>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds5' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='5' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("5") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds5" class="fixed inlineradio">Crackles</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsCracklesText", data.ContainsKey("GenericLungSoundsCracklesText") ? data["GenericLungSoundsCracklesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsCracklesText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds6' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='6' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("6") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds6" class="fixed inlineradio">Diminished</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsDiminishedText", data.ContainsKey("GenericLungSoundsDiminishedText") ? data["GenericLungSoundsDiminishedText"].Answer : "", new { @id = "Recertification_GenericLungSoundsDiminishedText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds7' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='7' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("7") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds7" class="fixed inlineradio">Absent</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsAbsentText", data.ContainsKey("GenericLungSoundsAbsentText") ? data["GenericLungSoundsAbsentText"].Answer : "", new { @id = "Recertification_GenericLungSoundsAbsentText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='Recertification_GenericRespiratorySounds8' class='radio float_left' name='Recertification_GenericRespiratorySounds' value='8' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("8") ? "checked='checked'" : "" ) %>
                                    <label for="Recertification_GenericRespiratorySounds8" class="fixed inlineradio">Stridor</label>
                                    <%= Html.TextBox("Recertification_GenericLungSoundsStridorText", data.ContainsKey("GenericLungSoundsStridorText") ? data["GenericLungSoundsStridorText"].Answer : "", new { @id = "Recertification_GenericLungSoundsStridorText", @class = "st", @maxlength = "20" }) %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition3' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='3' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition3" class="fixed inlineradio">Cough:</label>
                <div id="Recertification_GenericRespiratoryCondition3More" class="rel">
                    <%  var coughList = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Productive", Value = "1" },
                            new SelectListItem { Text = "Nonproductive", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericCoughList") && data["GenericCoughList"].Answer != "" ? data["GenericCoughList"].Answer : "0"); %>
                    <%= Html.DropDownList("Recertification_GenericCoughList", coughList) %>
                    <label for="Recertification_GenericCoughDescribe">Describe</label>
                    <%= Html.TextBox("Recertification_GenericCoughDescribe", data.ContainsKey("GenericCoughDescribe") ? data["GenericCoughDescribe"].Answer : "", new { @id = "Recertification_GenericCoughDescribe", @class = "st", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition4' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='4' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition4" class="fixed inlineradio">O<sub>2</sub></label>
                <div id="Recertification_GenericRespiratoryCondition4More" class="rel">
                    <div id="Recertification_GenericRespiratoryCondition4More" class="rel">
                        <label for="Recertification_Generic02AtText">At:</label>
                        <%= Html.TextBox("Recertification_Generic02AtText", data.ContainsKey("Generic02AtText") ? data["Generic02AtText"].Answer : "", new { @id = "Recertification_Generic02AtText", @class = "st", @maxlength = "20" }) %>
                        <label for="Recertification_GenericLPMVia">LPM via:</label>
                        <%  var via = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Nasal Cannula", Value = "1" },
                                new SelectListItem { Text = "Mask", Value = "2" },
                                new SelectListItem { Text = "Trach", Value = "3" },
                                new SelectListItem { Text = "Other", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericLPMVia") ? data["GenericLPMVia"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericLPMVia", via, new { @id = "Recertification_GenericLPMVia" })%>
                    </div>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition5' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='5' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition5" class="fixed inlineradio">O<sub>2</sub> Sat:</label>
                <div id="Recertification_GenericRespiratoryCondition5More" class="rel">
                    <%= Html.TextBox("Recertification_Generic02SatText", data.ContainsKey("Generic02SatText") ? data["Generic02SatText"].Answer : "", new { @id = "Recertification_Generic02SatText", @class = "st", @maxlength = "20" }) %>
                    <label>On</label>
                    <%  var satList = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Room Air", Value = "Room Air" },
                            new SelectListItem { Text = "02", Value = "02" }
                        }, "Value", "Text", data.ContainsKey("Generic02SatList") && data["Generic02SatList"].Answer != "" ? data["Generic02SatList"].Answer : "0"); %>
                    <%= Html.DropDownList("Recertification_Generic02SatList", satList) %>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition6' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='6' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition6" class="fixed inlineradio">Nebulizer:</label>
                <%= Html.TextBox("Recertification_GenericNebulizerText", data.ContainsKey("GenericNebulizerText") ? data["GenericNebulizerText"].Answer : "", new { @id = "Recertification_GenericNebulizerText", @class = "st", @maxlength = "20" }) %>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericRespiratoryCondition7' class='radio float_left' name='Recertification_GenericRespiratoryCondition' value='7' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericRespiratoryCondition7" class="fixed inlineradio">Tracheostomy</label>
            </div><div class="row">
                <label for="Recertification_GenericRespiratoryComments" class="strong">Comments:</label>
                <%= Html.TextArea("Recertification_GenericRespiratoryComments", data.ContainsKey("GenericRespiratoryComments") ? data["GenericRespiratoryComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericRespiratoryComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1400');">(M1400)</a> When is the patient dyspneic or noticeably Short of Breath?</label>
                <%= Html.Hidden("Recertification_M1400PatientDyspneic") %>
                <div>
                    <%= Html.RadioButton("Recertification_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "Recertification_M1400PatientDyspneic0", @class = "radio float_left" })%>
                    <label for="Recertification_M1400PatientDyspneic0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient is not short of breath</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "Recertification_M1400PatientDyspneic1", @class = "radio float_left" })%>
                    <label for="Recertification_M1400PatientDyspneic1"><span class="float_left">1 &ndash;</span><span class="normal margin">When walking more than 20 feet, climbing stairs</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "Recertification_M1400PatientDyspneic2", @class = "radio float_left" })%>
                    <label for="Recertification_M1400PatientDyspneic2"><span class="float_left">2 &ndash;</span><span class="normal margin">With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "Recertification_M1400PatientDyspneic3", @class = "radio float_left" })%>
                    <label for="Recertification_M1400PatientDyspneic3"><span class="float_left">3 &ndash;</span><span class="normal margin">With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "Recertification_M1400PatientDyspneic4", @class = "radio float_left" })%>
                    <label for="Recertification_M1400PatientDyspneic4"><span class="float_left">4 &ndash;</span><span class="normal margin">At rest (during day or night)</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1410');">(M1410)</a> Respiratory Treatments utilized at home: (Mark all that apply)</label>
                <div>
                    <input name="Recertification_M1410HomeRespiratoryTreatmentsOxygen" value=" " type="hidden" />
                    <%= string.Format("<input id='Recertification_M1410HomeRespiratoryTreatmentsOxygen' class='radio float_left M1410' name='Recertification_M1410HomeRespiratoryTreatmentsOxygen' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="Recertification_M1410HomeRespiratoryTreatmentsOxygen"><span class="float_left">1 &ndash;</span><span class="normal margin">Oxygen (intermittent or continuous)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="Recertification_M1410HomeRespiratoryTreatmentsVentilator" value=" " type="hidden" />
                    <%= string.Format("<input id='Recertification_M1410HomeRespiratoryTreatmentsVentilator' class='radio float_left M1410' name='Recertification_M1410HomeRespiratoryTreatmentsVentilator' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="Recertification_M1410HomeRespiratoryTreatmentsVentilator"><span class="float_left">2 &ndash;</span><span class="normal margin">Ventilator (continually or at night)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="Recertification_M1410HomeRespiratoryTreatmentsContinuous" value=" " type="hidden" />
                    <%= string.Format("<input id='Recertification_M1410HomeRespiratoryTreatmentsContinuous' class='radio float_left M1410' name='Recertification_M1410HomeRespiratoryTreatmentsContinuous' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="Recertification_M1410HomeRespiratoryTreatmentsContinuous"><span class="float_left">3 &ndash;</span><span class="normal margin">Continuous / Bi-level positive airway pressure</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <input name="Recertification_M1410HomeRespiratoryTreatmentsNone" value=" " type="hidden" />
                    <%= string.Format("<input id='Recertification_M1410HomeRespiratoryTreatmentsNone' class='radio float_left M1410' name='Recertification_M1410HomeRespiratoryTreatmentsNone' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="Recertification_M1410HomeRespiratoryTreatmentsNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] respiratoryInterventions = data.ContainsKey("485RespiratoryInterventions") && data["485RespiratoryInterventions"].Answer != "" ? data["485RespiratoryInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485RespiratoryInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions1' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='1' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions1">SN to instruct the</label>
                    <%  var instructNebulizerUsePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructNebulizerUsePerson") && data["485InstructNebulizerUsePerson"].Answer != "" ? data["485InstructNebulizerUsePerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485InstructNebulizerUsePerson", instructNebulizerUsePerson) %>
                    <label for="Recertification_485RespiratoryInterventions1">on proper use of nebulizer/inhaler, and assess return demonstration.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions2' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='2' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions2">SN to assess O<sub>2</sub> saturation on room air (freq)</label>
                    <%= Html.TextBox("Recertification_485AssessOxySaturationFrequency", data.ContainsKey("485AssessOxySaturationFrequency") ? data["485AssessOxySaturationFrequency"].Answer : "", new { @id = "Recertification_485AssessOxySaturationFrequency", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions3' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='3' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions3">SN to assess O<sub>2</sub> saturation on O<sub>2</sub> @</label>
                    <%=Html.TextBox("Recertification_485AssessOxySatOnOxyAt", data.ContainsKey("485AssessOxySatOnOxyAt") ? data["485AssessOxySatOnOxyAt"].Answer : "", new { @id = "Recertification_485AssessOxySatOnOxyAt", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485RespiratoryInterventions3">LPM/</label>
                    <%=Html.TextBox("Recertification_485AssessOxySatLPM", data.ContainsKey("485AssessOxySatLPM") ? data["485AssessOxySatLPM"].Answer : "", new { @id = "Recertification_485AssessOxySatLPM", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485RespiratoryInterventions3">(freq)</label>
                    <%=Html.TextBox("Recertification_485AssessOxySatOnOxyFrequency", data.ContainsKey("485AssessOxySatOnOxyFrequency") ? data["485AssessOxySatOnOxyFrequency"].Answer : "", new { @id = "Recertification_485AssessOxySatOnOxyFrequency", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions4' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='4' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions4">SN to instruct the</label>
                    <%  var instructSobFactorsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructSobFactorsPerson") && data["485InstructSobFactorsPerson"].Answer != "" ? data["485InstructSobFactorsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructSobFactorsPerson", instructSobFactorsPerson)%>
                    <label for="Recertification_485RespiratoryInterventions4">on factors that contribute to SOB.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions5' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='5' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions5">SN to instruct the</label>
                    <%  var instructAvoidSmokingPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructAvoidSmokingPerson") && data["485InstructAvoidSmokingPerson"].Answer != "" ? data["485InstructAvoidSmokingPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructAvoidSmokingPerson", instructAvoidSmokingPerson)%>
                    <label for="Recertification_485RespiratoryInterventions5">to avoid smoking or allowing people to smoke in patient&rsquo;s home. Instruct patient to avoid irritants/allergens known to increase SOB.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions6' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='6' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryInterventions6" class="radio">SN to instruct patient on energy conserving measures including frequent rest periods, small frequent meals, avoiding large meals/overeating, controlling stress.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions7' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='7' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryInterventions7" class="radio">SN to instruct caregiver on proper suctioning technique.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions8' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='8' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("8") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions8">SN to instruct the </label>
                    <%  var recognizePulmonaryDysfunctionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485RecognizePulmonaryDysfunctionPerson") && data["485RecognizePulmonaryDysfunctionPerson"].Answer != "" ? data["485RecognizePulmonaryDysfunctionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485RecognizePulmonaryDysfunctionPerson", recognizePulmonaryDysfunctionPerson)%>
                    <label for="Recertification_485RespiratoryInterventions8">on methods to recognize pulmonary dysfunction and relieve complications.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions9' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='9' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("9") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryInterventions9">Report to physician O<sub>2</sub> saturation less than</label>
                    <%=Html.TextBox("Recertification_485OxySaturationLessThanPercent", data.ContainsKey("485OxySaturationLessThanPercent") ? data["485OxySaturationLessThanPercent"].Answer : "", new { @id = "Recertification_485OxySaturationLessThanPercent", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485RespiratoryInterventions9">%.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions10' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='10' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("10") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryInterventions10" class="radio">SN to assess/instruct on signs &amp; symptoms of pulmonary complications.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions11' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='11' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("11") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryInterventions11" class="radio">SN to instruct on all aspects of trach care and equipment. SN to instruct PT/CG on emergency procedures for complications and dislodgment of tube.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryInterventions12' class='radio float_left' name='Recertification_485RespiratoryInterventions' value='12' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("12") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryInterventions12" class="radio">SN to perform trach care using sterile technique (i.e. suctioning, dressing change).</label>
            </div><div class="row">
                <label for="Recertification_485RespiratoryOrderTemplates" class="strong">Additional Orders:</label>
                <%  var respiratoryOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RespiratoryOrderTemplates") && data["485RespiratoryOrderTemplates"].Answer != "" ? data["485RespiratoryOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485RespiratoryOrderTemplates", respiratoryOrderTemplates) %>
                <%= Html.TextArea("Recertification_485RespiratoryInterventionComments", data.ContainsKey("485RespiratoryInterventionComments") ? data["485RespiratoryInterventionComments"].Answer : "", 2, 70, new { @id = "Recertification_485RespiratoryInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] respiratoryGoals = data.ContainsKey("485RespiratoryGoals") && data["485RespiratoryGoals"].Answer != "" ? data["485RespiratoryGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485RespiratoryGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals1' class='radio float_left' name='Recertification_485RespiratoryGoals' value='1' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryGoals1" class="radio">Respiratory status will improve with reduced shortness of breath and improved lung sounds by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals2' class='radio float_left' name='Recertification_485RespiratoryGoals' value='2' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryGoals2" class="radio">PT/CG will verbalize and demonstrate correct use and care of oxygen and equipment by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals3' class='radio float_left' name='Recertification_485RespiratoryGoals' value='3' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RespiratoryGoals3" class="radio">Patient will be free from signs and symptoms of respiratory distress during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals4' class='radio float_left' name='Recertification_485RespiratoryGoals' value='4' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryGoals4">Patient and caregiver will verbalize an understanding of factors that contribute to shortness of breath by:</label>
                    <%=Html.TextBox("Recertification_485VerbalizeFactorsSobDate", data.ContainsKey("485VerbalizeFactorsSobDate") ? data["485VerbalizeFactorsSobDate"].Answer : "", new { @id = "Recertification_485VerbalizeFactorsSobDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals5' class='radio float_left' name='Recertification_485RespiratoryGoals' value='5' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryGoals5">Patient will verbalize an understanding of energy conserving measures by:</label>
                    <%=Html.TextBox("Recertification_485VerbalizeEnergyConserveDate", data.ContainsKey("485VerbalizeEnergyConserveDate") ? data["485VerbalizeEnergyConserveDate"].Answer : "", new { @id = "Recertification_485VerbalizeEnergyConserveDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals6' class='radio float_left' name='Recertification_485RespiratoryGoals' value='6' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryGoals6">The</label>
                    <%  var verbalizeSafeOxyManagementPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeSafeOxyManagementPerson") && data["485VerbalizeSafeOxyManagementPerson"].Answer != "" ? data["485VerbalizeSafeOxyManagementPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485VerbalizeSafeOxyManagementPerson", verbalizeSafeOxyManagementPerson)%>
                    <label for="Recertification_485RespiratoryGoals6">will verbalize and demonstrate safe management of oxygen by:</label>
                    <%=Html.TextBox("Recertification_485VerbalizeSafeOxyManagementPersonDate", data.ContainsKey("485VerbalizeSafeOxyManagementPersonDate") ? data["485VerbalizeSafeOxyManagementPersonDate"].Answer : "", new { @id = "Recertification_485VerbalizeSafeOxyManagementPersonDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RespiratoryGoals7' class='radio float_left' name='Recertification_485RespiratoryGoals' value='7' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("7") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RespiratoryGoals7">Patient will return demonstrate proper use of nebulizer treatment by </label>
                    <%=Html.TextBox("Recertification_485DemonstrateNebulizerUseDate", data.ContainsKey("485DemonstrateNebulizerUseDate") ? data["485DemonstrateNebulizerUseDate"].Answer : "", new { @id = "Recertification_485DemonstrateNebulizerUseDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <label for="Recertification_485RespiratoryGoalTemplates" class="strong">Additional Goals:</label>
                <%  var respiratoryGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RespiratoryGoalTemplates") && data["485RespiratoryGoalTemplates"].Answer != "" ? data["485RespiratoryGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485RespiratoryGoalTemplates", respiratoryGoalTemplates) %>
                <%= Html.TextArea("Recertification_485RespiratoryGoalComments", data.ContainsKey("485RespiratoryGoalComments") ? data["485RespiratoryGoalComments"].Answer : "", 2, 70, new { @id = "Recertification_485RespiratoryGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#Recertification_GenericRespiratoryCondition2"), $("#Recertification_GenericRespiratoryCondition2More"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratoryCondition3"), $("#Recertification_GenericRespiratoryCondition3More"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds1"), $("#Recertification_GenericLungSoundsCTAText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds2"), $("#Recertification_GenericLungSoundsRalesText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds3"), $("#Recertification_GenericLungSoundsRhonchiText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds4"), $("#Recertification_GenericLungSoundsWheezesText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds5"), $("#Recertification_GenericLungSoundsCracklesText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds6"), $("#Recertification_GenericLungSoundsDiminishedText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds7"), $("#Recertification_GenericLungSoundsAbsentText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratorySounds8"), $("#Recertification_GenericLungSoundsStridorText"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratoryCondition4"), $("#Recertification_GenericRespiratoryCondition4More"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratoryCondition5"), $("#Recertification_GenericRespiratoryCondition5More"));
    Oasis.showIfChecked($("#Recertification_GenericRespiratoryCondition6"), $("#Recertification_GenericNebulizerText"));
    Oasis.noneOfTheAbove($("#Recertification_M1410HomeRespiratoryTreatmentsNone"), $("#window_recertification .M1410"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>