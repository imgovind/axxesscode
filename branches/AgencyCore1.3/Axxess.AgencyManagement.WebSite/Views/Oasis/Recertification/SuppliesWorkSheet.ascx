﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="wrapper main">
    <div class="medication medDiv">
        <div class="align_center strong">Add New Supplies</div>
        <% var actionCommand1 = string.Format("<div class=\"buttons\"><ul><li><a href=\"javascript:void(0);\" onclick=\"Oasis.AddSupply($(this),'{0}','{1}','{2}','{3}','<#=Id#>');\">Add</a></li></ul></div>", Model.EpisodeId, Model.PatientId, Model.Id, "Recertification"); %>
        <% var input = string.Format("<input type=\"text\" class=\"quantity fixedinputWidth\"/>", "", string.Empty); %>
        <% var date = string.Format("<input type=\"text\" class=\"date\"/>", "", string.Empty); %>
        <%= Html.Telerik().Grid<Supply>().Name("Recertification_SupplyFilterGrid").ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "", style = "margin-left:0;" })).Columns(columns => {
                columns.Bound(s => s.Description);
                columns.Bound(s => s.Code).Title("Code").Width(55);
                columns.Bound(e => e.Quantity).ClientTemplate(input).Title("Quantity").Width(85);
                columns.Bound(e => e.Date).ClientTemplate(date).Title("Date").Width(85);
                columns.Bound(e => e.Id).ClientTemplate(actionCommand1).Title("Action").Width(100);
            }).DataBinding(dataBinding => { dataBinding.Ajax().Select("SuppliesGrid", "LookUp", new { q = string.Empty, limit = 0, type = string.Empty }); }).Footer(false)
        %>
    </div>
    <div class="medication medDiv">
        <div class="align_center strong">Current Supplies Used</div>
        <%= Html.Telerik().Grid<Supply>().Name("Recertification_SupplyGrid").DataKeys(keys => { keys.Add(M => M.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).DataBinding(dataBinding =>
{
                dataBinding.Ajax()
                    .Select("Supply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = "Recertification" })
                    .Update("EditSupply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = "Recertification" })
                    .Delete("DeleteSupply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = "Recertification" });
            }).Columns(columns => {
                columns.Bound(s => s.Description).ReadOnly();
                columns.Bound(s => s.Code).Title("Code").Width(55).ReadOnly();
                columns.Bound(s => s.Quantity).Title("Quantity").Width(85);
                columns.Bound(e => e.Date).Title("Date").Format("{0:MM/dd/yyyy}").ReadOnly().Width(85);
                columns.Command(commands => { commands.Edit(); commands.Delete(); }).Width(180).Title("Action");
            }).Editable(editing => editing.Mode(GridEditMode.InLine)).Sortable().Footer(false)
        %>
    </div>
    <% var con = string.Format("<label class='float_left' >Enter the Supply Name: </label> <div class='float_left'><input id='{0}_GenericSupplyDescription' onfocus=Oasis.SupplyDescription('{0}'); /> </div>  <label class='float_left'>  or  Enter the Supply Code: </label> <div class='float_left'> <input id='{0}_GenericSupplyCode' onfocus=Oasis.SupplyCode('{0}'); /> </div>", "Recertification"); %>
    <%= string.Format("<script type='text/javascript'>  $(\"#{0}_SupplyFilterGrid .t-grid-toolbar\").html(\"{1}\");  $(\"#{0}_SupplyGrid .t-grid-toolbar\").empty(); $(\"#{0}_SupplyGrid .t-grid-toolbar\").remove(); $(\" .t-grid\").css({2});</script>", "Recertification", con, "{'min-height': '5px'}")%>
    <script type="text/javascript">
        $("#suppliesworksheet_recertification .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '40px !important', 'bottom': '10' });
        $("#suppliesworksheet_recertification .t-grid").css({ 'min-height': '5px' });
    </script>
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationSuppliesWorksheetForm" })) {
            var data = Model.ToDictionary(); %>
    <%= Html.Hidden("Recertification_Id", Model.Id)%>
    <%= Html.Hidden("Recertification_Action", "Edit")%>
    <%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", "Recertification")%>
    <%= Html.Hidden("categoryType", "SuppliesWorksheet")%>
    <fieldset>
        <legend>Supplies</legend>
        <%string[] supplies = data.ContainsKey("485Supplies") && data["485Supplies"].Answer != "" ? data["485Supplies"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485Supplies" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies1' class='radio float_left' name='Recertification_485Supplies' value='1' type='checkbox' {0} />", supplies!=null && supplies.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies1" class="radio">ABDs</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies2' class='radio float_left' name='Recertification_485Supplies' value='2' type='checkbox' {0} />", supplies!=null && supplies.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies2" class="radio">Ace Wrap</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies3' class='radio float_left' name='Recertification_485Supplies' value='3' type='checkbox' {0} />", supplies!=null && supplies.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies3" class="radio">Alcohol Pads</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies4' class='radio float_left' name='Recertification_485Supplies' value='4' type='checkbox' {0} />", supplies!=null && supplies.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies4" class="radio">Chux/Underpads</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies5' class='radio float_left' name='Recertification_485Supplies' value='5' type='checkbox' {0} />", supplies!=null && supplies.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies5" class="radio">Diabetic Supplies</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies6' class='radio float_left' name='Recertification_485Supplies' value='6' type='checkbox' {0} />", supplies!=null && supplies.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies6" class="radio">Drainage Bag</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies7' class='radio float_left' name='Recertification_485Supplies' value='7' type='checkbox' {0} />", supplies!=null && supplies.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies7" class="radio">Dressing Supplies</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies8' class='radio float_left' name='Recertification_485Supplies' value='8' type='checkbox' {0} />", supplies!=null && supplies.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies8" class="radio">Duoderm</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies9' class='radio float_left' name='Recertification_485Supplies' value='9' type='checkbox' {0} />", supplies!=null && supplies.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies9" class="radio">Exam Gloves</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies10' class='radio float_left' name='Recertification_485Supplies' value='10' type='checkbox' {0} />", supplies!=null && supplies.Contains("10") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies10" class="radio">Foley Catheter</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies11' class='radio float_left' name='Recertification_485Supplies' value='11' type='checkbox' {0} />", supplies!=null && supplies.Contains("11") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies11" class="radio">Gauze Pads</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies12' class='radio float_left' name='Recertification_485Supplies' value='12' type='checkbox' {0} />", supplies!=null && supplies.Contains("12") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies12" class="radio">Insertion Kit</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies13' class='radio float_left' name='Recertification_485Supplies' value='13' type='checkbox' {0} />", supplies!=null && supplies.Contains("13") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies13" class="radio">Irrigation Set</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies14' class='radio float_left' name='Recertification_485Supplies' value='14' type='checkbox' {0} />", supplies!=null && supplies.Contains("14") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies14" class="radio">Irrigation Solution</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies15' class='radio float_left' name='Recertification_485Supplies' value='15' type='checkbox' {0} />", supplies!=null && supplies.Contains("15") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies15" class="radio">Kerlix Rolls</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies16' class='radio float_left' name='Recertification_485Supplies' value='16' type='checkbox' {0} />", supplies!=null && supplies.Contains("16") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies16" class="radio">Leg Bag</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies17' class='radio float_left' name='Recertification_485Supplies' value='17' type='checkbox' {0} />", supplies!=null && supplies.Contains("17") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies17" class="radio">Needles</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies18' class='radio float_left' name='Recertification_485Supplies' value='18' type='checkbox' {0} />", supplies!=null && supplies.Contains("18") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies18" class="radio">NG Tube</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies19' class='radio float_left' name='Recertification_485Supplies' value='19' type='checkbox' {0} />", supplies!=null && supplies.Contains("19") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies19" class="radio">Probe Covers</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies20' class='radio float_left' name='Recertification_485Supplies' value='20' type='checkbox' {0} />", supplies!=null && supplies.Contains("20") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies20" class="radio">Sharps Container</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485Supplies21' class='radio float_left' name='Recertification_485Supplies' value='21' type='checkbox' {0} />", supplies!=null && supplies.Contains("21") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies21" class="radio">Sterile Gloves</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485Supplies22' class='radio float_left' name='Recertification_485Supplies' value='22' type='checkbox' {0} />", supplies!=null && supplies.Contains("22") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies22" class="radio">Syringe</label>
                            </td><td colspan="2">
                                <%= string.Format("<input id='Recertification_485Supplies23' class='radio float_left' name='Recertification_485Supplies' value='23' type='checkbox' {0} />", supplies!=null && supplies.Contains("23") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485Supplies23" class="radio">Tape</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="Recertification_485SuppliesComment">Other:</label>
                <%=Html.TextArea("Recertification_485SuppliesComment", data.ContainsKey("485SuppliesComment") ? data["485SuppliesComment"].Answer : "", 5, 70, new { @id = "Recertification_485SuppliesComment" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>DME (Locator #14)</legend>
        <%string[] dME = data.ContainsKey("485DME") && data["485DME"].Answer != "" ? data["485DME"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485DME" value="" />
        <div class="column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485DME1' class='radio float_left' name='Recertification_485DME' value='1' type='checkbox' {0} />", dME!=null && dME.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME1" class="radio">Bedside Commode</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME2' class='radio float_left' name='Recertification_485DME' value='2' type='checkbox' {0} />", dME!=null && dME.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME2" class="radio">Cane</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME3' class='radio float_left' name='Recertification_485DME' value='3' type='checkbox' {0} />", dME!=null && dME.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME3" class="radio">Elevated Toilet Seat</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485DME4' class='radio float_left' name='Recertification_485DME' value='4' type='checkbox' {0} />", dME!=null && dME.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME4" class="radio">Grab Bars</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME5' class='radio float_left' name='Recertification_485DME' value='5' type='checkbox' {0} />", dME!=null && dME.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME5" class="radio">Hospital Bed</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME6' class='radio float_left' name='Recertification_485DME' value='6' type='checkbox' {0} />", dME!=null && dME.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME6" class="radio">Nebulizer</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='Recertification_485DME7' class='radio float_left' name='Recertification_485DME' value='7' type='checkbox' {0} />", dME!=null && dME.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME7" class="radio">Oxygen</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME8' class='radio float_left' name='Recertification_485DME' value='8' type='checkbox' {0} />", dME!=null && dME.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME8" class="radio">Tub/Shower Bench</label>
                            </td><td>
                                <%= string.Format("<input id='Recertification_485DME9' class='radio float_left' name='Recertification_485DME' value='9' type='checkbox' {0} />", dME!=null && dME.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME9" class="radio">Walker</label>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <%= string.Format("<input id='Recertification_485DME10' class='radio float_left' name='Recertification_485DME' value='10' type='checkbox' {0} />", dME!=null && dME.Contains("10") ? "checked='checked'" : "" ) %>
                                <label for="Recertification_485DME10" class="radio">Wheelchair</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="Recertification_485DMEComments">Other:</label>
                <%=Html.TextArea("Recertification_485DMEComments", data.ContainsKey("485DMEComments") ? data["485DMEComments"].Answer : "", 5, 70, new { @id = "Recertification_485DMEComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>DME Provider</legend>
        <div class="column">
            <div class="row">
                <label for="Recertification_GenericDMEProviderName" class="float_left">Name:</label>
                <div class="float_right"><%=Html.TextBox("Recertification_GenericDMEProviderName", data.ContainsKey("GenericDMEProviderName") ? data["GenericDMEProviderName"].Answer : "", new { @id = "Recertification_GenericDMEProviderName", @maxlength = "40" })%></div>
            </div><div class="row">
                <label for="Recertification_GenericDMEProviderPhone" class="float_left">Phone Number:</label>
                <div class="float_right"><%=Html.TextBox("Recertification_GenericDMEProviderPhone", data.ContainsKey("GenericDMEProviderPhone") ? data["GenericDMEProviderPhone"].Answer : "", new { @id = "Recertification_GenericDMEProviderPhone", @class = "st", @maxlength = "12" })%></div>
            </div><div class="row">
                <label for="Recertification_GenericDMESuppliesProvided" class="strong">DME/Supplies Provided:</label>
                <div><%=Html.TextArea("Recertification_GenericDMESuppliesProvided", data.ContainsKey("GenericDMESuppliesProvided") ? data["GenericDMESuppliesProvided"].Answer : "", 5, 70, new { @id = "Recertification_GenericDMESuppliesProvided" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
    <% } %>
</div>