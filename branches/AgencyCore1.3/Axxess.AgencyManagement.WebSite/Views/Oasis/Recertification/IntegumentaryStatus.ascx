﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationIntegumentaryForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification")%>  
<%= Html.Hidden("categoryType", "Integumentary")%> 
<div class="wrapper main"> 
    <fieldset id="RecertificationBradenScale">
        <legend>Braden Scale</legend>
        <div class="column">
            <div class="row">
                <div>
                    <h4>Sensory Perception</h4>
                    Ability to respond meaningfully to pressure-related discomfort
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleSensory1' name='Recertification_485BradenScaleSensory' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleSensory1"><span class="float_left">1.</span><span class="margin normal">Completely Limited. Unresponsive (does not moan, flinch, or grasp) to painful stimuli, due to diminished level of consciousness or sedation OR limited ability to feel pain over most of body.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleSensory2' name='Recertification_485BradenScaleSensory' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleSensory2"><span class="float_left">2.</span><span class="margin normal">Very Limited. Responds only to painful stimuli. Cannot communicate discomfort except by moaning or restlessness OR has a sensory impairment which limits the ability to feel pain or discomfort over 1/2 of body.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleSensory3' name='Recertification_485BradenScaleSensory' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleSensory3"><span class="float_left">3.</span><span class="margin normal">Slightly Limited. Responds to verbal commands, but cannot always communicate discomfort or the need to be turned OR has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleSensory4' name='Recertification_485BradenScaleSensory' value='4' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer == "4" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleSensory4"><span class="float_left">4.</span><span class="margin normal">No Impairment. Responds to verbal commands. Has no sensory deficit which would limit ability to feel or voice pain or discomfort.</span></label>
                </div>
            </div><div class="row">
                <div>
                    <h4>Moisture</h4>
                    Degree to which skin is exposed to moisture
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMoisture1' name='Recertification_485BradenScaleMoisture' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMoisture1"><span class="float_left">1.</span><span class="margin normal">Constantly Moist. Skin is kept moist almost constantly by perspiration, urine, etc. Dampness is detected every time patient is moved or turned.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMoisture2' name='Recertification_485BradenScaleMoisture' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMoisture2"><span class="float_left">2.</span><span class="margin normal">Often Moist. Skin is often, but not always moist. Linen must be changed as often as 3 times in 24 hours.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMoisture3' name='Recertification_485BradenScaleMoisture' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMoisture3"><span class="float_left">3.</span><span class="margin normal">Occasionally Moist. Skin is occasionally moist, requiring an extra linen change approximately once a day.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMoisture4' name='Recertification_485BradenScaleMoisture' value='4' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer == "4" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMoisture4"><span class="float_left">4.</span><span class="margin normal">Rarely Moist. Skin is usually dry; Linen only requires changing at routine intervals.</span></label>
                </div>
            </div><div class="row">
                <div>
                    <h4>Activity</h4>
                    Degree of physical activity
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleActivity1' name='Recertification_485BradenScaleActivity' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleActivity1"><span class="float_left">1.</span><span class="margin normal">Bedfast. Confined to bed.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleActivity2' name='Recertification_485BradenScaleActivity' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleActivity2"><span class="float_left">2.</span><span class="margin normal">Chairfast. Ability to walk severely limited or non-existent. Cannot bear own weight and/or must be assisted into chair or wheelchair.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleActivity3' name='Recertification_485BradenScaleActivity' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleActivity3"><span class="float_left">3.</span><span class="margin normal">Walks Occasionally. Walks occasionally during day, but for very short distances, with or without assistance. Spends majority of day in bed or chair.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleActivity4' name='Recertification_485BradenScaleActivity' value='4' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer == "4" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleActivity4"><span class="float_left">4.</span><span class="margin normal">Walks Frequently. Walks outside bedroom twice a day and inside room at least once every two hours during waking hours.</span></label>
                </div>
            </div><div class="row"> 
                <div>
                    <h4>Mobility</h4>
                    Ability to change and control body position
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMobility1' name='Recertification_485BradenScaleMobility' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMobility1"><span class="float_left">1.</span><span class="margin normal">Completely Immobile. Does not make even slight changes in body or extremity position without assistance.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMobility2' name='Recertification_485BradenScaleMobility' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMobility2"><span class="float_left">2.</span><span class="margin normal">Very Limited. Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMobility3' name='Recertification_485BradenScaleMobility' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMobility3"><span class="float_left">3.</span><span class="margin normal">Slightly Limited. Makes frequent though slight changes in body or extremity position independently.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleMobility4' name='Recertification_485BradenScaleMobility' value='4' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer == "4" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleMobility4"><span class="float_left">4.</span><span class="margin normal">No Limitation. Makes major and frequent changes in position without assistance.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div>
                    <h4>Nutrition</h4>
                    Usual food intake pattern
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleNutrition1' name='Recertification_485BradenScaleNutrition' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleNutrition1"><span class="float_left">1.</span><span class="margin normal">Very Poor. Never eats a complete meal. Rarely eats more than 1/3 of any food offered. Eats 2 servings or less of protein (meat or dairy products) per day. Takes fluids poorly. Does not take a liquid dietary supplement OR is NPO and/or maintained on clear liquids or IVs for more than 5 days.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleNutrition2' name='Recertification_485BradenScaleNutrition' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleNutrition2"><span class="float_left">2.</span><span class="margin normal">Probably Inadequate. Rarely eats a complete meal and generally eats only about 1/2 of any food offered. Protein intake includes only 3 servings of meat or dairy products per day. Occasionally will take a dietary supplement OR receives less than optimum amount of liquid diet or tube feeding.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleNutrition3' name='Recertification_485BradenScaleNutrition' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleNutrition3"><span class="float_left">3.</span><span class="margin normal">Adequate. Eats over half of most meals. Eats a total of 4 servings of protein (meat, dairy products) per day. Occasionally will refuse a meal, but will usually take a supplement when offered OR is on a tube feeding or TPN regimen which probably meets most of nutritional needs.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleNutrition4' name='Recertification_485BradenScaleNutrition' value='4' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer == "4" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleNutrition4"><span class="float_left">4.</span><span class="margin normal">Excellent. Eats most of every meal. Never refuses a meal. Usually eats a total of 4 or more servings of meat and dairy products. Occasionally eats between meals. Does not require supplementation.</span></label>
                </div>
            </div><div class="row">
                <div>
                    <h4>Friction &amp; Shear</h4>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleFriction1' name='Recertification_485BradenScaleFriction' value='1' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleFriction") && data["485BradenScaleFriction"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleFriction1"><span class="float_left">1.</span><span class="margin normal">Problem. Requires moderate to maximum assistance in moving. Complete lifting without sliding against sheets is impossible. Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance. Spasticity, contractures or agitation leads to almost constant friction.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleFriction2' name='Recertification_485BradenScaleFriction' value='2' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleFriction") && data["485BradenScaleFriction"].Answer == "2" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleFriction2"><span class="float_left">2.</span><span class="margin normal">Potential Problem. Moves feebly or requires minimum assistance. During a move skin probably slides to some extent against sheets, chair, restraints or other devices. Maintains relatively good position in chair or bed most of the time but occasionally slides down.</span></label>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='radio' id='485BradenScaleFriction3' name='Recertification_485BradenScaleFriction' value='3' onclick=\"javascript:Oasis.BradenScaleScore('Recertification');\" {0} />", data.ContainsKey("485BradenScaleFriction") && data["485BradenScaleFriction"].Answer == "3" ? "checked='checked'" : "" ) %>
                    <label for="485BradenScaleFriction3"><span class="float_left">3.</span><span class="margin normal">No Apparent Problem. Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move. Maintains good position in bed or chair.</span></label>
                </div>
            </div><div class="row">
                <label for="Recertification_485BradenScaleTotal" class="float_left">Total</label>
                <div class="float_right">
                    <%= Html.TextBox("Recertification_485BradenScaleTotal", data.ContainsKey("485BradenScaleTotal") ? data["485BradenScaleTotal"].Answer : "", new { @id = "Recertification_485BradenScaleTotal", @class = "mi", @readonly="readonly"}) %>
                    <em id="Recertification_485ResultBox"></em>
                </div>
                <div class="clear"></div>
                <h4 class="float_right">
                    Braden Scale Scoring<br />
                    Risk of developing pressure ulcers:
                </h4><ul class="float_right">
                    <li id="Recertification_485BradenScale19">19 or above: Not at Risk;</li>
                    <li id="Recertification_485BradenScale15-18">15-18: At risk;</li>
                    <li id="Recertification_485BradenScale13-14">13-14: Moderate risk;</li>
                    <li id="Recertification_485BradenScale10-12">10-12: High risk;</li>
                    <li id="Recertification_485BradenScale9">9 or below: Very high risk</li>
                </ul>
                <div class="clear">
                    <br />
                    <em>Copyright. Barbara Braden and Nancy Bergstrom, 1988. Reprinted with permission. All Rights Reserved.</em>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Integumentary Status</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Skin Turgor</label>
                <%= Html.Hidden("Recertification_GenericSkinTurgor") %>
                <div class="float_right">
                    <%= Html.RadioButton("Recertification_GenericSkinTurgor", "Good", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Good" ? true : false, new { @id = "Recertification_GenericSkinTurgorGood", @class = "radio" })%>
                    <label for="Recertification_GenericSkinTurgorGood" class="inlineradio">Good</label>
                    <%= Html.RadioButton("Recertification_GenericSkinTurgor", "Fair", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Fair" ? true : false, new { @id = "Recertification_GenericSkinTurgorFair", @class = "radio" })%>
                    <label for="Recertification_GenericSkinTurgorFair" class="inlineradio">Fair</label>
                    <%= Html.RadioButton("Recertification_GenericSkinTurgor", "Poor", data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Poor" ? true : false, new { @id = "Recertification_GenericSkinTurgorPoor", @class = "radio" })%>
                    <label for="Recertification_GenericSkinTurgorPoor" class="inlineradio">Poor</label>
                </div>
            </div><div class="row">
                <div class="strong">Skin Color</div>
                <% string[] skinColor = data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer != "" ? data["GenericSkinColor"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericSkinColor" value="" />
                <div class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinColor1' name='Recertification_GenericSkinColor' value='1' type='checkbox' {0} />", skinColor!=null && skinColor.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericSkinColor1" class="fixed inlineradio">Pink/WNL</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinColor2' name='Recertification_GenericSkinColor' value='2' type='checkbox' {0} />", skinColor!=null && skinColor.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericSkinColor2" class="fixed inlineradio">Pallor</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinColor3' name='Recertification_GenericSkinColor' value='3' type='checkbox' {0} />", skinColor!=null && skinColor.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericSkinColor3" class="fixed inlineradio">Jaundice</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinColor4' name='Recertification_GenericSkinColor' value='4' type='checkbox' {0} />", skinColor!=null && skinColor.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericSkinColor4" class="fixed inlineradio">Cyanotic</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinColor5' name='Recertification_GenericSkinColor' value='5' type='checkbox' {0} />", skinColor!=null && skinColor.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericSkinColor5" class="fixed inlineradio">Other</label>
                        <%= Html.TextBox("Recertification_GenericSkinColorOther", data.ContainsKey("GenericSkinColorOther") ? data["GenericSkinColorOther"].Answer : "", new { @id = "Recertification_GenericSkinColorOther", @class = "st", @maxlength = "20" })%>
                    </div>
                </div>
            </div><div class="row">
                <label class="float_left">Skin Temp.</label>
                <div class="float_right"><% var skinTemp = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Warm", Value = "1" }, new SelectListItem { Text = "Cool", Value = "2" }, new SelectListItem { Text = "Clammy", Value = "3" }, new SelectListItem { Text = "Other", Value = "4" } }, "Value", "Text", data.ContainsKey("GenericSkinTemp") && data["GenericSkinTemp"].Answer != "" ? data["GenericSkinTemp"].Answer : "0");%><%= Html.DropDownList("Recertification_GenericSkinTemp", skinTemp, new { @id = "Recertification_GenericSkinTemp", @tabindex = "" })%></div>
            </div><div class="row">
                <div class="strong">Condition</div>
                <% string[] skinCondition = data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer != "" ? data["GenericSkinCondition"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericSkinCondition" value=" " />
                <div class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition1' name='Recertification_GenericSkinCondition' value='1' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("1") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition1" class="fixed inlineradio">Dry</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition2' name='Recertification_GenericSkinCondition' value='2' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("2") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition2" class="fixed inlineradio">Wound</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition3' name='Recertification_GenericSkinCondition' value='3' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("3") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition3" class="fixed inlineradio">Ulcer</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition4' name='Recertification_GenericSkinCondition' value='4' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("4") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition4" class="fixed inlineradio">Incision</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition5' name='Recertification_GenericSkinCondition' value='5' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("5") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition5" class="fixed inlineradio">Rash</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='Recertification_GenericSkinCondition6' name='Recertification_GenericSkinCondition' value='6' type='checkbox' {0} />", skinCondition != null && skinCondition.Contains("6") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericSkinCondition6" class="fixed inlineradio">Other</label>
                        <%= Html.TextBox("Recertification_GenericSkinConditionOther", data.ContainsKey("GenericSkinConditionOther") ? data["GenericSkinConditionOther"].Answer : "", new { @id = "Recertification_GenericSkinConditionOther", @class = "st", @maxlength = "20" })%>
                    </div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="float_left">Instructed on measures to control infections?</label>
                <%= Html.Hidden("Recertification_GenericInstructedControlInfections") %>
                <div class="float_right">
                    <%= Html.RadioButton("Recertification_GenericInstructedControlInfections", "1", data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "1" ? true : false, new { @id = "Recertification_GenericInstructedControlInfectionsYes", @class = "radio" })%>
                    <label for="Recertification_GenericInstructedControlInfectionsYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("Recertification_GenericInstructedControlInfections", "0", data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "0" ? true : false, new { @id = "Recertification_GenericInstructedControlInfectionsNo", @class = "radio" })%>
                    <label for="Recertification_GenericInstructedControlInfectionsNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Nail Condition</label>
                <%= Html.Hidden("Recertification_GenericNails") %>
                <div class="float_right">
                    <%= Html.RadioButton("Recertification_GenericNails", "Good", data.ContainsKey("GenericNails") && data["GenericNails"].Answer == "Good" ? true : false, new { @id = "Recertification_GenericNailsGood", @class = "radio" }) %>
                    <label for="Recertification_GenericNailsGood" class="inlineradio">Good</label>
                    <div class="clear"></div>
                    <%= Html.RadioButton("Recertification_GenericNails", "Problem", data.ContainsKey("GenericNails") && data["GenericNails"].Answer == "Problem" ? true : false, new { @id = "Recertification_GenericNailsProblem", @class = "radio" })%>
                    <label for="Recertification_GenericNailsProblem" class="inlineradio">Problems</label>
                    <div id="Recertification_GenericNailsProblemMore" class="float_right"><label for=""><em>(Specify)</em></label><%= Html.TextBox("Recertification_GenericNailsProblemOther", data.ContainsKey("GenericNailsProblemOther") ? data["GenericNailsProblemOther"].Answer : "", new { @id = "Recertification_GenericNailsProblemOther", @maxlength = "20" })%></div>
                </div>
            </div><div class="row">
                <label class="float_left">Is patient using pressure-relieving device(s)?</label>
                <%= Html.Hidden("Recertification_GenericPressureRelievingDevice") %>
                <div class="float_right">
                    <%= Html.RadioButton("Recertification_GenericPressureRelievingDevice", "1", data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "1" ? true : false, new { @id = "Recertification_GenericPressureRelievingDeviceYes", @class = "radio" }) %>
                    <label for="Recertification_GenericPressureRelievingDeviceYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("Recertification_GenericPressureRelievingDevice", "0", data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "0" ? true : false, new { @id = "Recertification_GenericPressureRelievingDeviceNo", @class = "radio" }) %>
                    <label for="Recertification_GenericPressureRelievingDeviceNo" class="inlineradio">No</label>
                </div>
                <div class="clear"></div>
                <div id="Recertification_GenericPressureRelievingDeviceMore" class="float_right">
                    <label for="Recertification_GenericPressureRelievingDeviceType">Type</label>
                    <%  var pressureRelievingDeviceType = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Low Air Mattress", Value = "1" },
                            new SelectListItem { Text = "Gel Cushion", Value = "2" },
                            new SelectListItem { Text = "Egg Crate", Value = "3" },
                            new SelectListItem { Text = "Other (Specify)", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericPressureRelievingDeviceType") && data["GenericPressureRelievingDeviceType"].Answer != "" ? data["GenericPressureRelievingDeviceType"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_GenericPressureRelievingDeviceType", pressureRelievingDeviceType, new { @id = "Recertification_GenericPressureRelievingDeviceType", @class = "loc" })%>
                    <%= Html.TextBox("Recertification_GenericPressureRelievingDeviceTypeOther", data.ContainsKey("GenericPressureRelievingDeviceTypeOther") ? data["GenericPressureRelievingDeviceTypeOther"].Answer : "", new { @id = "Recertification_GenericPressureRelievingDeviceTypeOther", @class = "loc", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericIntegumentaryStatusComments" class="strong">Comments</label>
                <%= Html.TextArea("Recertification_GenericIntegumentaryStatusComments", data.ContainsKey("GenericIntegumentaryStatusComments") ? data["GenericIntegumentaryStatusComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericIntegumentaryStatusComments" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>IV Access</legend>
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Does patient have IV access?</label>
                <%=Html.Hidden("Recertification_GenericIVAccess")%>
                <div class="float_left">
                    <%=Html.RadioButton("Recertification_GenericIVAccess", "1", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "1" ? true : false, new { @id = "Recertification_GenericIVAccess1", @class = "radio" })%>
                    <label for="Recertification_GenericIVAccess1" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("Recertification_GenericIVAccess", "0", data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "0" ? true : false, new { @id = "Recertification_GenericIVAccess0", @class = "radio" })%>
                    <label for="Recertification_GenericIVAccess0" class="inlineradio">No</label>
                </div>
            </div><div class="IVAccess row">
                <div class="column">
                    <div class="row">
                        <label for="Recertification_GenericIVAccessDate" class="float_left">Date of Insertion:</label>
                        <div class="float_right">
                            <%= Html.Telerik().DatePicker().Name("Recertification_GenericIVAccessDate").Value(data.ContainsKey("GenericIVAccessDate") && data["GenericIVAccessDate"].Answer.IsNotNullOrEmpty() ? data["GenericIVAccessDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_GenericIVAccessDate", @class = "date" })%>
                        </div>
                    </div><div class="row">
                        <label for="Recertification_GenericIVAccessType" class="float_left">IV Access Type:</label>
                        <div class="float_right">
                            <%  var IVIVAccess = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Saline Lock", Value = "1" },
                                    new SelectListItem { Text = "PICC Line", Value = "2" },
                                    new SelectListItem { Text = "Central Line", Value = "3" },
                                    new SelectListItem { Text = "Port-A-Cath", Value = "4" },
                                    new SelectListItem { Text = "Med-A-Port", Value = "5" },
                                    new SelectListItem { Text = "Other", Value = "6" }
                                }, "Value", "Text", data.ContainsKey("GenericIVAccessType") ? data["GenericIVAccessType"].Answer : "0");%>
                            <%= Html.DropDownList("Recertification_GenericIVAccessType", IVIVAccess, new { @id = "Recertification_GenericIVAccessType" })%>
                        </div>
                    </div><div class="row">
                        <label for="Recertification_GenericIVLocation" class="float_left">IV Location:</label>
                        <div class="float_right"><%= Html.TextBox("Recertification_GenericIVLocation", data.ContainsKey("GenericIVLocation") ? data["GenericIVLocation"].Answer : "", new { @id = "Recertification_GenericIVLocation", @maxlength = "25" })%></div>
                    </div><div class="row">
                        <label for="GenericIVConditionOfIVSite" class="float_left">Condition of IV Site:</label>
                        <div class="float_right">
                            <%  var IVConditionOfIVSite = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "WNL", Value = "1" },
                                    new SelectListItem { Text = "Phlebitis", Value = "2" },
                                    new SelectListItem { Text = "Redness", Value = "3" },
                                    new SelectListItem { Text = "Swelling", Value = "4" },
                                    new SelectListItem { Text = "Pallor ", Value = "5" },
                                    new SelectListItem { Text = "Warmth ", Value = "6" },
                                    new SelectListItem { Text = "Bleeding ", Value = "7" }
                                }, "Value", "Text", data.ContainsKey("GenericIVConditionOfIVSite") ? data["GenericIVConditionOfIVSite"].Answer : "0");%>
                            <%= Html.DropDownList("Recertification_GenericIVConditionOfIVSite", IVConditionOfIVSite, new { @id = "Recertification_GenericIVConditionOfIVSite" })%>
                        </div>
                    </div>
                </div><div class="column">
                    <div class="row">
                        <label for="GenericIVSiteDressing" class="float_left">IV site Dressing Changed performed on this visit:</label>
                        <div class="float_right">
                            <%  var IVSiteDressing = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Yes", Value = "2" },
                                    new SelectListItem { Text = "No", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("GenericIVSiteDressing") ? data["GenericIVSiteDressing"].Answer : "0");%>
                            <%= Html.DropDownList("Recertification_GenericIVSiteDressing", IVSiteDressing, new { @id = "Recertification_GenericIVSiteDressing" })%>
                        </div>
                    </div><div class="row">
                        <label for="Recertification_GenericIVAccessDressingChange" class="float_left">Date of Last Dressing Change:</label>
                        <div class="float_right"><%= Html.TextBox("Recertification_GenericIVAccessDateOfLastDressingChange", data.ContainsKey("GenericIVAccessDateOfLastDressingChange") ? data["GenericIVAccessDateOfLastDressingChange"].Answer : "", new { @id = "Recertification_GenericIVAccessDateOfLastDressingChange", @class = "st", @maxlength = "11" })%></div>
                    </div><div class="row">
                        <label for="Recertification_GenericIVConditionOfDressing" class="float_left">Condition of Dressing:</label>
                        <div class="float_right">
                            <%  var IVConditionOfDressing = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Dry & Intact/WNL", Value = "1" },
                                    new SelectListItem { Text = "Bloody", Value = "2" },
                                    new SelectListItem { Text = "Soiled", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.ContainsKey("GenericIVConditionOfDressing") ? data["GenericIVConditionOfDressing"].Answer : "0");%>
                            <%= Html.DropDownList("Recertification_GenericIVConditionOfDressing", IVConditionOfDressing, new { @id = "Recertification_GenericIVConditionOfDressing" })%>
                        </div>
                    </div><div class="row">
                        <label class="float_left">Flush:</label>
                        <div class="float_right">
                            <%= Html.RadioButton("Recertification_GenericFlush", "1", data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "1" ? true : false, new { @id = "Recertification_GenericFlush1", @class = " radio" })%>
                            <label for="Recertification_GenericFlush1" class="inlineradio">Yes</label>
                            <%= Html.RadioButton("Recertification_GenericFlush", "0", data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "0" ? true : false, new { @id = "Recertification_GenericFlush2", @class = " radio" })%>
                            <label for="Recertification_GenericFlush2" class="inlineradio">No</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float_right" id="Recertification_GenericFlushMore">
                            <label for="Recertification_GenericIVAccessFlushed">flushed with</label>
                            <%= Html.TextBox("Recertification_GenericIVAccessFlushed", data.ContainsKey("GenericIVAccessFlushed") ? data["GenericIVAccessFlushed"].Answer : "", new { @class = "st numeric", @id = "Recertification_GenericIVAccessFlushed", @maxlength = "5" })%>
                            <label for="Recertification_GenericIVAccessFlushed">/ml of</label>
                            <%= Html.TextBox("Recertification_GenericIVSiteDressingUnit", data.ContainsKey("GenericIVSiteDressingUnit") ? data["GenericIVSiteDressingUnit"].Answer : "", new { @id = "Recertification_GenericIVSiteDressingUnit", @class = "IVSiteDressingUnit", @maxlength = "25" })%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] integumentaryIVInterventions = data.ContainsKey("485IntegumentaryIVInterventions") && data["485IntegumentaryIVInterventions"].Answer != "" ? data["485IntegumentaryIVInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485IntegumentaryIVInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVInterventions1' class='radio float_left' name='Recertification_485IntegumentaryIVInterventions' value='1' type='checkbox' {0} />", integumentaryIVInterventions != null && integumentaryIVInterventions.Contains("1") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryIVInterventions1">SN to instruct the</label>
                    <%  var instructInfectionSignsSymptomsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructInfectionSignsSymptomsPerson") && data["485InstructInfectionSignsSymptomsPerson"].Answer != "" ? data["485InstructInfectionSignsSymptomsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructInfectionSignsSymptomsPerson", instructInfectionSignsSymptomsPerson)%>
                    <label for="Recertification_485IntegumentaryIVInterventions1">on signs and symptoms of infection and infiltration.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVInterventions2' class='radio float_left' name='Recertification_485IntegumentaryIVInterventions' value='2' type='checkbox' {0} />", integumentaryIVInterventions != null && integumentaryIVInterventions.Contains("2") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryIVInterventions2">SN to change central line dressing every</label>
                    <%=Html.TextBox("Recertification_485ChangeCentralLineEvery", data.ContainsKey("485ChangeCentralLineEvery") ? data["485ChangeCentralLineEvery"].Answer : "", new { @id = "Recertification_485ChangeCentralLineEvery", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485IntegumentaryIVInterventions2">using sterile technique.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVInterventions3' class='radio float_left' name='Recertification_485IntegumentaryIVInterventions' value='3' type='checkbox' {0} />", integumentaryIVInterventions != null && integumentaryIVInterventions.Contains("3") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryIVInterventions3">SN to instruct the</label>
                    <%  var instructChangeCentralLinePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructChangeCentralLinePerson") && data["485InstructChangeCentralLinePerson"].Answer != "" ? data["485InstructChangeCentralLinePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructChangeCentralLinePerson", instructChangeCentralLinePerson)%>
                    <label for="Recertification_485IntegumentaryIVInterventions3">to change central line dressing every</label>
                    <%=Html.TextBox("Recertification_485InstructChangeCentralLineEvery", data.ContainsKey("485InstructChangeCentralLineEvery") ? data["485InstructChangeCentralLineEvery"].Answer : "", new { @id = "Recertification_485InstructChangeCentralLineEvery", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485IntegumentaryIVInterventions3">using sterile technique.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVInterventions4' class='radio float_left' name='Recertification_485IntegumentaryIVInterventions' value='4' type='checkbox' {0} />", integumentaryIVInterventions != null && integumentaryIVInterventions.Contains("4") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryIVInterventions4">SN to change</label>
                    <%=Html.TextBox("Recertification_485ChangePortDressingType", data.ContainsKey("485ChangePortDressingType") ? data["485ChangePortDressingType"].Answer : "", new { @id = "Recertification_485ChangePortDressingType", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485IntegumentaryIVInterventions4">port dressing using sterile technique every</label>
                    <%=Html.TextBox("Recertification_485ChangePortDressingEvery", data.ContainsKey("485ChangePortDressingEvery") ? data["485ChangePortDressingEvery"].Answer : "", new { @id = "Recertification_485ChangePortDressingEvery", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <label for="Recertification_485IntegumentaryIVOrderTemplates" class="strong">Additional Orders:</label>
                <%  var integumentaryIVOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = " ", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IntegumentaryIVOrderTemplates") && data["485IntegumentaryIVOrderTemplates"].Answer != "" ? data["485IntegumentaryIVOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485IntegumentaryIVOrderTemplates", integumentaryIVOrderTemplates)%>
                <%= Html.TextArea("Recertification_485IntegumentaryIVInterventionComments", data.ContainsKey("485IntegumentaryIVInterventionComments") ? data["485IntegumentaryIVInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485IntegumentaryIVInterventionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] integumentaryIVGoals = data.ContainsKey("485IntegumentaryIVGoals") && data["485IntegumentaryIVGoals"].Answer != "" ? data["485IntegumentaryIVGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485IntegumentaryIVGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVGoals1' class='radio float_left' type='checkbox' name='Recertification_485IntegumentaryIVGoals' value='1' {0} />", integumentaryIVGoals != null && integumentaryIVGoals.Contains("1") ? "checked='checked'" : "")%>
                <label for="Recertification_485IntegumentaryIVGoals1" class="radio">IV will remain patent and free from signs and symptoms of infection.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryIVGoals2' class='radio float_left' type='checkbox' name='Recertification_485IntegumentaryIVGoals' value='2' {0} />", integumentaryIVGoals != null && integumentaryIVGoals.Contains("2") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryIVGoals2">The</label>
                    <%  var instructChangeDress = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructChangeDress") && data["485InstructChangeDress"].Answer != "" ? data["485InstructChangeDress"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructChangeDress", instructChangeDress)%>
                    <label for="Recertification_485IntegumentaryIVGoals2">will demonstrate understanding of changing</label>
                    <%=Html.TextBox("Recertification_485InstructChangeDressSterile", data.ContainsKey("485InstructChangeDressSterile") ? data["485InstructChangeDressSterile"].Answer : "", new { @id = "Recertification_485InstructChangeDressSterile", @class = "st", @maxlength = "15" })%>
                    <label for="Recertification_485IntegumentaryIVGoals2">dressing using sterile technique.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485IntegumentaryIVGoalTemplates" class="strong">Additional Goals:</label>
                <%  var integumentaryIVGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IntegumentaryIVGoalTemplates") && data["485IntegumentaryIVGoalTemplates"].Answer != "" ? data["485IntegumentaryIVGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485IntegumentaryIVGoalTemplates", integumentaryIVGoalTemplates)%>
                <%= Html.TextArea("Recertification_485IntegumentaryIVGoalComments", data.ContainsKey("485IntegumentaryIVGoalComments") ? data["485IntegumentaryIVGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485IntegumentaryIVGoalComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1306</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1306');">(M1306)</a> Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?</label>
                <%= Html.Hidden("Recertification_M1306UnhealedPressureUlcers") %>
                <div class="margin">
                    <%= Html.RadioButton("Recertification_M1306UnhealedPressureUlcers", "0", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? true : false, new { @id = "Recertification_M1306UnhealedPressureUlcers0", @class = "radio float_left" })%>
                    <label for="Recertification_M1306UnhealedPressureUlcers0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</em></span></label>
                    <div class="clear"></div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1306');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1306UnhealedPressureUlcers", "1", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? true : false, new { @id = "Recertification_M1306UnhealedPressureUlcers1", @class = "radio float_left" })%>
                    <label for="Recertification_M1306UnhealedPressureUlcers1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis Recertification_M1306_00">
        <legend>OASIS M1308</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1308');">(M1308)</a> Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: (Enter &ldquo;0&rdquo; if none; excludes Stage I pressure ulcers)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td colspan="2"></td>
                            <td>
                                <h4>Column 1</h4>
                                <em>Complete at SOC/ROC/FU &amp; D/C</em>
                            </td><td>
                                <h4>Column 2</h4>
                                <em>Complete at FU &amp; D/C</em>
                            </td>
                        </tr><tr>
                            <td colspan="2">Stage description &ndash; unhealed pressure ulcers</td>
                            <td>Number Currently Present</td>
                            <td>Number of those listed in Column 1 that were present on admission (most recent SOC/ ROC)</td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">a.</span><span class="radio">Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">b.</span><span class="radio">Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">c.</span><span class="radio">Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.1</span><span class="radio">Unstageable: Known or likely but unstageable due to non-removable dressing or device</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.2</span><span class="radio">Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.3</span><span class="radio">Unstageable: Suspected deep tissue injury in evolution.</span>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn numeric", @maxlength = "5" })%>
                            </td><td>
                                <%=Html.TextBox("Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "", new { @id = "Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", @class = "sn numeric", @maxlength = "5" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1308');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1322/M1324</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1322');">(M1322)</a> Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.
                    <%= Html.Hidden("Recertification_M1322CurrentNumberStageIUlcer") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1322CurrentNumberStageIUlcer", "00", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? true : false, new { @id = "Recertification_M1322CurrentNumberStageIUlcer0", @class = "radio float_left" })%>
                    <label for="Recertification_M1322CurrentNumberStageIUlcer0"><span class="float_left">0 &ndash;</span><span class="normal margin">Zero</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1322CurrentNumberStageIUlcer", "01", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? true : false, new { @id = "Recertification_M1322CurrentNumberStageIUlcer1", @class = "radio float_left" })%>
                    <label for="Recertification_M1322CurrentNumberStageIUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">One</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1322CurrentNumberStageIUlcer", "02", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? true : false, new { @id = "Recertification_M1322CurrentNumberStageIUlcer2", @class = "radio float_left" })%>
                    <label for="Recertification_M1322CurrentNumberStageIUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Two</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1322CurrentNumberStageIUlcer", "03", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? true : false, new { @id = "Recertification_M1322CurrentNumberStageIUlcer3", @class = "radio float_left" })%>
                    <label for="Recertification_M1322CurrentNumberStageIUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Three</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1322');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1322CurrentNumberStageIUlcer", "04", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? true : false, new { @id = "Recertification_M1322CurrentNumberStageIUlcer4", @class = "radio float_left" })%>
                    <label for="Recertification_M1322CurrentNumberStageIUlcer4"><span class="float_left">4 &ndash;</span><span class="normal margin">Four or more</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1324');">(M1324)</a> Stage of Most Problematic Unhealed (Observable) Pressure Ulcer
                    <%= Html.Hidden("Recertification_M1324MostProblematicUnhealedStage") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1324MostProblematicUnhealedStage", "01", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? true : false, new { @id = "Recertification_M1324MostProblematicUnhealedStage1", @class = "radio float_left" })%>
                    <label for="Recertification_M1324MostProblematicUnhealedStage1"><span class="float_left">1 &ndash;</span><span class="normal margin">Stage I</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1324MostProblematicUnhealedStage", "02", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? true : false, new { @id = "Recertification_M1324MostProblematicUnhealedStage2", @class = "radio float_left" })%>
                    <label for="Recertification_M1324MostProblematicUnhealedStage2"><span class="float_left">2 &ndash;</span><span class="normal margin">Stage II</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1324MostProblematicUnhealedStage", "03", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? true : false, new { @id = "Recertification_M1324MostProblematicUnhealedStage3", @class = "radio float_left" })%>
                    <label for="Recertification_M1324MostProblematicUnhealedStage3"><span class="float_left">3 &ndash;</span><span class="normal margin">Stage III</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1324MostProblematicUnhealedStage", "04", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? true : false, new { @id = "Recertification_M1324MostProblematicUnhealedStage4", @class = "radio float_left" })%>
                    <label for="Recertification_M1324MostProblematicUnhealedStage4"><span class="float_left">4 &ndash;</span><span class="normal margin">Stage IV</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1324');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1324MostProblematicUnhealedStage", "NA", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? true : false, new { @id = "Recertification_M1324MostProblematicUnhealedStageNA", @class = "radio float_left" })%>
                    <label for="Recertification_M1324MostProblematicUnhealedStageNA"><span class="float_left">NA &ndash;</span><span class="normal margin">No observable pressure ulcer or unhealed pressure ulcer</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1330</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1330');">(M1330)</a> Does this patient have a Stasis Ulcer?
                    <%= Html.Hidden("Recertification_M1330StasisUlcer") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1330StasisUlcer", "00", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? true : false, new { @id = "Recertification_M1330StasisUlcer0", @class = "radio float_left" })%>
                    <label for="Recertification_M1330StasisUlcer0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1330StasisUlcer", "01", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? true : false, new { @id = "Recertification_M1330StasisUlcer1", @class = "radio float_left" })%>
                    <label for="Recertification_M1330StasisUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, patient has BOTH observable and unobservable stasis ulcers</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1330StasisUlcer", "02", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? true : false, new { @id = "Recertification_M1330StasisUlcer2", @class = "radio float_left" })%>
                    <label for="Recertification_M1330StasisUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, patient has observable stasis ulcers ONLY</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1330');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1330StasisUlcer", "03", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? true : false, new { @id = "Recertification_M1330StasisUlcer3", @class = "radio float_left" })%>
                    <label for="Recertification_M1330StasisUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing) <em>[Go to M1340]</em></span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="Recertification_M1332AndM1334" class="oasis">
        <legend>OASIS M1332/M1334</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1332');">(M1332)</a> Current Number of (Observable) Stasis Ulcer(s)
                    <%= Html.Hidden("Recertification_M1332CurrentNumberStasisUlcer") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1332CurrentNumberStasisUlcer", "01", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? true : false, new { @id = "Recertification_M1332CurrentNumberStasisUlcer1", @class = "radio float_left" })%>
                    <label for="Recertification_M1332CurrentNumberStasisUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">One</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1332CurrentNumberStasisUlcer", "02", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? true : false, new { @id = "Recertification_M1332CurrentNumberStasisUlcer2", @class = "radio float_left" })%>
                    <label for="Recertification_M1332CurrentNumberStasisUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Two</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1332CurrentNumberStasisUlcer", "03", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? true : false, new { @id = "Recertification_M1332CurrentNumberStasisUlcer3", @class = "radio float_left" })%>
                    <label for="Recertification_M1332CurrentNumberStasisUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Three</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1332');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1332CurrentNumberStasisUlcer", "04", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? true : false, new { @id = "Recertification_M1332CurrentNumberStasisUlcer4", @class = "radio float_left" })%>
                    <label for="Recertification_M1332CurrentNumberStasisUlcer4"><span class="float_left">4 &ndash;</span><span class="normal margin">Four or more</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1334');">(M1334)</a> Status of Most Problematic (Observable) Stasis Ulcer
                    <%= Html.Hidden("Recertification_M1334StasisUlcerStatus") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1334StasisUlcerStatus", "00", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? true : false, new { @id = "Recertification_M1334StasisUlcerStatus0", @class = "radio float_left" })%>
                    <label for="Recertification_M1334StasisUlcerStatus0"><span class="float_left">0 &ndash;</span><span class="normal margin">Newly epithelialized</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1334StasisUlcerStatus", "01", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? true : false, new { @id = "Recertification_M1334StasisUlcerStatus1", @class = "radio float_left" })%>
                    <label for="Recertification_M1334StasisUlcerStatus1"><span class="float_left">1 &ndash;</span><span class="normal margin">Fully granulating</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1334StasisUlcerStatus", "02", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? true : false, new { @id = "Recertification_M1334StasisUlcerStatus2", @class = "radio float_left" })%>
                    <label for="Recertification_M1334StasisUlcerStatus2"><span class="float_left">2 &ndash;</span><span class="normal margin">Early/partial granulation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1334');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1334StasisUlcerStatus", "03", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? true : false, new { @id = "Recertification_M1334StasisUlcerStatus3", @class = "radio float_left" })%>
                    <label for="Recertification_M1334StasisUlcerStatus3"><span class="float_left">3 &ndash;</span><span class="normal margin">Not healing</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1340</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1340');">(M1340)</a> Does this patient have a Surgical Wound?
                    <%= Html.Hidden("Recertification_M1340SurgicalWound") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1340SurgicalWound", "00", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? true : false, new { @id = "Recertification_M1340SurgicalWound0", @class = "radio float_left" })%>
                    <label for="Recertification_M1340SurgicalWound0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1340SurgicalWound", "01", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? true : false, new { @id = "Recertification_M1340SurgicalWound1", @class = "radio float_left" })%>
                    <label for="Recertification_M1340SurgicalWound1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, patient has at least one (observable) surgical wound</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1340');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1340SurgicalWound", "02", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? true : false, new { @id = "Recertification_M1340SurgicalWound2", @class = "radio float_left" })%>
                    <label for="Recertification_M1340SurgicalWound2"><span class="float_left">2 &ndash;</span><span class="normal margin">Surgical wound known but not observable due to non-removable dressing <em>[Go to M1350]</em></span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="Recertification_M1342" class="oasis">
        <legend>OASIS M1342</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1342');">(M1342)</a> Status of Most Problematic (Observable) Surgical Wound
                    <%= Html.Hidden("Recertification_M1342SurgicalWoundStatus") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1342SurgicalWoundStatus", "00", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? true : false, new { @id = "Recertification_M1342SurgicalWoundStatus0", @class = "radio float_left" })%>
                    <label for="Recertification_M1342SurgicalWoundStatus0"><span class="float_left">0 &ndash;</span><span class="normal margin">Newly epithelialized</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1342SurgicalWoundStatus", "01", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? true : false, new { @id = "Recertification_M1342SurgicalWoundStatus1", @class = "radio float_left" })%>
                    <label for="Recertification_M1342SurgicalWoundStatus1"><span class="float_left">1 &ndash;</span><span class="normal margin">Fully granulating</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1342SurgicalWoundStatus", "02", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? true : false, new { @id = "Recertification_M1342SurgicalWoundStatus2", @class = "radio float_left" })%>
                    <label for="Recertification_M1342SurgicalWoundStatus2"><span class="float_left">2 &ndash;</span><span class="normal margin">Early/partial granulation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1342');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1342SurgicalWoundStatus", "03", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? true : false, new { @id = "Recertification_M1342SurgicalWoundStatus3", @class = "radio float_left" })%>
                    <label for="Recertification_M1342SurgicalWoundStatus3"><span class="float_left">3 &ndash;</span><span class="normal margin">Not healing</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1350</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1350');">(M1350)</a> Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?
                    <%= Html.Hidden("Recertification_M1350SkinLesionOpenWound") %>
                </div><div>
                    <%= Html.RadioButton("Recertification_M1350SkinLesionOpenWound", "0", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? true : false, new { @id = "Recertification_M1350SkinLesionOpenWound0", @class = "radio float_left" })%>
                    <label for="Recertification_M1350SkinLesionOpenWound0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1350');">?</div>
                    </div>
                    <%= Html.RadioButton("Recertification_M1350SkinLesionOpenWound", "1", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? true : false, new { @id = "Recertification_M1350SkinLesionOpenWound1", @class = "radio float_left" })%>
                    <label for="Recertification_M1350SkinLesionOpenWound1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Wound Graph</legend>
        <div class="wide_column">
            <div class="row align_center">
                <img alt="Body Outline" src="/Images/body_outline.gif" />
                <table class="form bordergrid">
                    <tbody>
                        <tr>
                            <th></th>
                            <th class="textCenter bold">Wound One</th>
                            <th class="textCenter bold">Wound Two</th>
                            <th class="textCenter bold">Wound Three</th>
                            <th class="textCenter bold">Wound Four</th>
                            <th class="textCenter bold">Wound Five</th>
                        </tr><tr>
                            <td>
                                <label class="strong">Upload File:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td><%
            if (data.ContainsKey("GenericUploadFile" + i.ToString()) && data["GenericUploadFile" + i.ToString()].Answer != Guid.Empty.ToString()) { %>
                                <%= Html.Hidden("Recertification_GenericUploadFile" + i.ToString(), data["GenericUploadFile" + i.ToString()].Answer.ToGuid()) %>
                                <%= Html.Asset(data["GenericUploadFile" + i.ToString()].Answer.ToGuid()) %> |
                                <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.DeleteAsset($(this),'Recertification','GenericUploadFile{0}','{1}');\">Delete</a>&nbsp;", i.ToString(), data["GenericUploadFile" + i.ToString()].Answer)%><%
            } else { %>
                                <input type="file" name="Recertification_GenericUploadFile<%= i.ToString() %>" value="Upload" class="uploadWidth" size="1" /><%
            } %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Location:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%= Html.TextBox("Recertification_GenericLocation" + i.ToString(), data.ContainsKey("GenericLocation" + i.ToString()) ? data["GenericLocation" + i.ToString()].Answer : "", new { @id = "Recertification_GenericLocation" + i.ToString(), @class = "fill", @maxlength = "30" })%>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Onset Date:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%= Html.Telerik().DatePicker().Name("Recertification_GenericOnsetDate" + i.ToString()).Value(data.ContainsKey("GenericOnsetDate" + i.ToString()) ? data["GenericOnsetDate" + i.ToString()].Answer : "").HtmlAttributes(new { @id = "Recertification_GenericOnsetDate" + i.ToString(), @class = "date fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Wound Type:</label> 
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%= Html.TextBox("Recertification_GenericWoundType" + i.ToString(), data.ContainsKey("GenericWoundType" + i.ToString()) ? data["GenericWoundType" + i.ToString()].Answer : "", new { @id = "Recertification_GenericWoundType" + i.ToString(), @class = "WoundType fill", @maxlength = "30" })%>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Pressure Ulcer Stage:</label> 
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var pressureUlcerStage = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "I", Value = "I" },
                                        new SelectListItem { Text = "II", Value = "II" },
                                        new SelectListItem { Text = "III", Value = "III" },
                                        new SelectListItem { Text = "IV", Value = "IV" },
                                        new SelectListItem { Text = "Unstageable", Value = "Unstageable" }
                                    }, "Value", "Text", data.ContainsKey("GenericPressureUlcerStage" + i.ToString()) ? data["GenericPressureUlcerStage" + i.ToString()].Answer : "0"); %>
                                <%= Html.DropDownList("Recertification_GenericPressureUlcerStage" + i.ToString(), pressureUlcerStage, new { @id = "Recertification_GenericPressureUlcerStage" + i.ToString(), @class = "fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Measurements:(cm)</li>
                                    <li class="align_right woundPad">Length:</li>
                                    <li class="align_right woundPad">Width:</li>
                                    <li class="align_right woundPad">Depth:</li>
                                </ul>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&nbsp;</li>
                                    <li><%= Html.TextBox("Recertification_GenericMeasurementLength" + i.ToString(), data.ContainsKey("GenericMeasurementLength" + i.ToString()) ? data["GenericMeasurementLength" + i.ToString()].Answer : "", new { @id = "Recertification_GenericMeasurementLength" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                    <li><%= Html.TextBox("Recertification_GenericMeasurementWidth" + i.ToString(), data.ContainsKey("GenericMeasurementWidth" + i.ToString()) ? data["GenericMeasurementWidth" + i.ToString()].Answer : "", new { @id = "Recertification_GenericMeasurementWidth" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                    <li><%= Html.TextBox("Recertification_GenericMeasurementDepth" + i.ToString(), data.ContainsKey("GenericMeasurementDepth" + i.ToString()) ? data["GenericMeasurementDepth" + i.ToString()].Answer : "", new { @id = "Recertification_GenericMeasurementDepth" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                </ul>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Wound Bed:</li>
                                    <li class="align_right woundPad">Granulation %:</li>
                                    <li class="align_right woundPad">Slough %:</li>
                                    <li class="float_right woundPad">Eschar%:</li>
                                </ul>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&nbsp;</li>
                                    <li><%= Html.TextBox("Recertification_GenericWoundBedGranulation" + i.ToString(), data.ContainsKey("GenericWoundBedGranulation" + i.ToString()) ? data["GenericWoundBedGranulation" + i.ToString()].Answer : "", new { @id = "Recertification_GenericWoundBedGranulation" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                    <li><%= Html.TextBox("Recertification_GenericWoundBedSlough" + i.ToString(), data.ContainsKey("GenericWoundBedSlough" + i.ToString()) ? data["GenericWoundBedSlough" + i.ToString()].Answer : "", new { @id = "Recertification_GenericWoundBedSlough" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                    <li><%= Html.TextBox("Recertification_GenericWoundBedEschar" + i.ToString(), data.ContainsKey("GenericWoundBedEschar" + i.ToString()) ? data["GenericWoundBedEschar" + i.ToString()].Answer : "", new { @id = "Recertification_GenericWoundBedEschar" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5" })%></li>
                                </ul>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Surrounding Tissue:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var surroundingTissue = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Pink", Value = "Pink" },
                                        new SelectListItem { Text = "Dry", Value = "Dry" },
                                        new SelectListItem { Text = "Pale", Value = "Pale" },
                                        new SelectListItem { Text = "Moist", Value = "Moist" },
                                        new SelectListItem { Text = "Excoriated", Value = "Excoriated" },
                                        new SelectListItem { Text = "Calloused", Value = "Calloused" },
                                        new SelectListItem { Text = "Normal", Value = "Normal" }
                                    }, "Value", "Text", data.ContainsKey("GenericSurroundingTissue" + i.ToString()) ? data["GenericSurroundingTissue" + i.ToString()].Answer : "0");%>
                                <%= Html.DropDownList("Recertification_GenericSurroundingTissue" + i.ToString(), surroundingTissue, new { @id = "Recertification_GenericSurroundingTissue" + i.ToString(), @class = "fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Drainage:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var drainage = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Serious", Value = "Serious" },
                                        new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" },
                                        new SelectListItem { Text = "Purulent", Value = "Purulent" },
                                        new SelectListItem { Text = "None", Value = "None" }
                                    }, "Value", "Text", data.ContainsKey("GenericDrainage" + i.ToString()) ? data["GenericDrainage" + i.ToString()].Answer : "0");%>
                                <%= Html.DropDownList("Recertification_GenericDrainage" + i.ToString(), drainage, new { @id = "Recertification_GenericDrainage" + i.ToString(), @class = "fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Drainage Amount:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var drainageAmount = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Minimal", Value = "Minimal" },
                                        new SelectListItem { Text = "Moderate", Value = "Moderate" },
                                        new SelectListItem { Text = "Heavy", Value = "Heavy" },
                                        new SelectListItem { Text = "None", Value = "None" }
                                    }, "Value", "Text", data.ContainsKey("GenericDrainageAmount" + i.ToString()) ? data["GenericDrainageAmount" + i.ToString()].Answer : "0");%>
                                <%= Html.DropDownList("Recertification_GenericDrainageAmount" + i.ToString(), drainageAmount, new { @id = "Recertification_GenericDrainageAmount" + i.ToString(), @class = "fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <label class="strong">Odor:</label>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var odor = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Yes", Value = "Yes" },
                                        new SelectListItem { Text = "No", Value = "No" }
                                    }, "Value", "Text", data.ContainsKey("GenericOdor" + i.ToString()) ? data["GenericOdor" + i.ToString()].Answer : "0");%>
                                <%= Html.DropDownList("Recertification_GenericOdor" + i.ToString(), odor, new { @id = "Recertification_GenericOdor" + i.ToString(), @class = "fill" }) %>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Tunneling:</li>
                                    <li class="align_right woundPad">Length:</li>
                                    <li class="align_right woundPad" style="height:52px;">Time:</li>
                                </ul>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&nbsp;</li>
                                    <li>
                                        <%= Html.TextBox("Recertification_GenericTunnelingLength" + i.ToString(), data.ContainsKey("GenericTunnelingLength" + i.ToString()) ? data["GenericTunnelingLength" + i.ToString()].Answer : "", new { @id = "Recertification_GenericTunnelingLength" + i.ToString(), @class = "vitals woundPad numeric", @maxlength = "5" })%>
                                        <label>cm</label>
                                    </li><li>
                                        <%  var tunnelingTime = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.ContainsKey("GenericTunnelingTime" + i.ToString()) ? data["GenericTunnelingTime" + i.ToString()].Answer : "");%>
                                        <%= Html.DropDownList("Recertification_GenericTunnelingTime" + i.ToString(), tunnelingTime, new { @id = "Recertification_GenericTunnelingTime" + i.ToString(), @class = "vitals" })%>
                                        <label>o&rsquo;clock</label>
                                    </li>
                                </ul>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Undermining:</li>
                                    <li class="align_right woundPad">Length:</li>
                                    <li class="align_right woundPad" style="height:52px;">Time:</li>
                                </ul>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&nbsp;</li>
                                    <li>
                                        <%= Html.TextBox("Recertification_GenericUnderminingLength" + i.ToString(), data.ContainsKey("GenericUnderminingLength" + i.ToString()) ? data["GenericUnderminingLength" + i.ToString()].Answer : "", new { @id = "Recertification_GenericUnderminingLength" + i.ToString(), @class = "vitals woundPad numeric", @maxlength = "5" })%>
                                        <label>cm</label>
                                    </li><li style="height:52px;">
                                        <%  var underminingTimeFrom = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeFrom" + i.ToString()) ? data["GenericUnderminingTimeFrom" + i.ToString()].Answer : "");%>
                                        <%= Html.DropDownList("Recertification_GenericUnderminingTimeFrom" + i.ToString(), underminingTimeFrom, new { @id = "Recertification_GenericUnderminingTimeFrom" + i.ToString(), @class = "vitals" })%>
                                        <label>to</label>
                                        <%  var underminingTimeTo = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.ContainsKey("GenericUnderminingTimeTo" + i.ToString()) ? data["GenericUnderminingTimeTo" + i.ToString()].Answer : "");%>
                                        <%= Html.DropDownList("Recertification_GenericUnderminingTimeTo" + i.ToString(), underminingTimeTo, new { @id = "Recertification_GenericUnderminingTimeTo" + i.ToString(), @class = "vitals" })%>
                                        <label>o&rsquo;clock</label>
                                    </li>
                                </ul>
                            </td><%
        } %>
                        </tr><tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Device:</li>
                                    <li class="align_right woundPad">Type:</li>
                                    <li class="align_right woundPad">Setting:</li>
                                </ul>
                            </td><%
        for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&nbsp;</li>
                                    <li><%= Html.TextBox("Recertification_GenericDeviceType" + i.ToString(), data.ContainsKey("GenericDeviceType" + i.ToString()) ? data["GenericDeviceType" + i.ToString()].Answer : "", new { @id = "Recertification_GenericDeviceType" + i.ToString(), @class = "fill woundPad DeviceType", @maxlength = "30" })%></li>
                                    <li><%= Html.TextBox("Recertification_GenericDeviceSetting" + i.ToString(), data.ContainsKey("GenericDeviceSetting" + i.ToString()) ? data["GenericDeviceSetting" + i.ToString()].Answer : "", new { @id = "Recertification_GenericDeviceSetting" + i.ToString(), @class = "fill woundPad", @maxlength = "30" })%></li>
                                </ul>
                            </td><%
        } %>
                        </tr><tr>
                            <td colspan="6">
                                <label class="strong">Treatment Performed:</label>
                                <div><%= Html.TextArea("Recertification_GenericTreatmentPerformed", data.ContainsKey("GenericTreatmentPerformed") ? data["GenericTreatmentPerformed"].Answer : "", 8, 20, new { @class = "fill", @id = "Recertification_GenericTreatmentPerformed" })%></div>
                            </td>
                        </tr><tr>
                            <td colspan="6">
                                <label class="strong">Narrative:</label>
                                <div><%= Html.TextArea("Recertification_GenericNarrative", data.ContainsKey("GenericNarrative") ? data["GenericNarrative"].Answer : "", 8, 20, new { @class = "fill", @id = "Recertification_GenericNarrative" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] integumentaryInterventions = data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer != "" ? data["485IntegumentaryInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485IntegumentaryInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions1' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='1' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryInterventions1">SN to instruct</label>
                    <%  var instructTurningRepositionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructTurningRepositionPerson") && data["485InstructTurningRepositionPerson"].Answer != "" ? data["485InstructTurningRepositionPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485InstructTurningRepositionPerson", instructTurningRepositionPerson) %>
                    <label for="Recertification_485IntegumentaryInterventions1">on turning/repositioning every 2 hours.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions2' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='2' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryInterventions2">SN to instruct the</label>
                    <%  var instructReduceFrictionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructReduceFrictionPerson") && data["485InstructReduceFrictionPerson"].Answer != "" ? data["485InstructReduceFrictionPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485InstructReduceFrictionPerson", instructReduceFrictionPerson) %>
                    <label for="Recertification_485IntegumentaryInterventions2">on methods to reduce friction and shear.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions3' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='3' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryInterventions3">SN to instruct the</label>
                    <%  var instructPadBonyProminencesPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructPadBonyProminencesPerson") && data["485InstructPadBonyProminencesPerson"].Answer != "" ? data["485InstructPadBonyProminencesPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485InstructPadBonyProminencesPerson", instructPadBonyProminencesPerson) %>
                    <label for="Recertification_485IntegumentaryInterventions3">to pad all bony prominences.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions4' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='4' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryInterventions4">SN to perform/instruct on wound care as follows:</label>
                    <%= Html.TextArea("Recertification_485InstructWoundCarePersonFrequency", data.ContainsKey("485InstructWoundCarePersonFrequency") ? data["485InstructWoundCarePersonFrequency"].Answer : "", 5, 70, new { @id = "Recertification_485InstructWoundCarePersonFrequency" }) %>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions5' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='5' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryInterventions5" class="radio">SN to assess skin for breakdown every visit.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions6' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='6' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryInterventions6">SN to instruct the</label>
                    <%  var instructSignsWoundInfectionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructSignsWoundInfectionPerson") && data["485InstructSignsWoundInfectionPerson"].Answer != "" ? data["485InstructSignsWoundInfectionPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485InstructSignsWoundInfectionPerson", instructSignsWoundInfectionPerson) %>
                    <label for="Recertification_485IntegumentaryInterventions6">on signs/symptoms of wound infection to report to physician, to include increased temp &gt; 100.5, chills, increase in drainage, foul odor, redness, pain and any other significant changes.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions7' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='7' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryInterventions7" class="radio">May discontinue wound care when wound(s) have healed.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions8' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='8' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryInterventions8" class="radio">SN to assess wound for S&amp;S of infection, healing status, wound deterioration, and complications.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryInterventions9' class='radio float_left' name='Recertification_485IntegumentaryInterventions' value='9' type='checkbox' {0} />", integumentaryInterventions!=null && integumentaryInterventions.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryInterventions9" class="radio">SN to perform/instruct on incision/suture site care &amp; dressing.</label>
            </div><div class="row">
                <label for="Recertification_485IntegumentaryOrderTemplates" class="strong">Additional Orders:</label>
                <%  var integumentaryOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = " ", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IntegumentaryOrderTemplates") && data["485IntegumentaryOrderTemplates"].Answer != "" ? data["485IntegumentaryOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485IntegumentaryOrderTemplates", integumentaryOrderTemplates) %>
                <%= Html.TextArea("Recertification_485IntegumentaryInterventionComments", data.ContainsKey("485IntegumentaryInterventionComments") ? data["485IntegumentaryInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485IntegumentaryInterventionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] integumentaryGoals = data.ContainsKey("485IntegumentaryGoals") && data["485IntegumentaryGoals"].Answer != "" ? data["485IntegumentaryGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485IntegumentaryGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryGoals1' class='radio float_left' name='Recertification_485IntegumentaryGoals' value='1' type='checkbox' {0} />", integumentaryGoals!=null && integumentaryGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryGoals1" class="radio">Wound(s) will heal without complication by the end of the episode</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryGoals2' class='radio float_left' name='Recertification_485IntegumentaryGoals' value='2' type='checkbox' {0} />", integumentaryGoals!=null && integumentaryGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryGoals2" class="radio">Wound(s) will be free from signs and symptoms of infection during 60 day episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryGoals3' class='radio float_left' name='Recertification_485IntegumentaryGoals' value='3' type='checkbox' {0} />", integumentaryGoals!=null && integumentaryGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485IntegumentaryGoals3">Wound(s) will decrease in size by</label>
                    <%= Html.TextBox("Recertification_485WoundSizeDecreasePercent", data.ContainsKey("485WoundSizeDecreasePercent") ? data["485WoundSizeDecreasePercent"].Answer : "", new { @id = "Recertification_485WoundSizeDecreasePercent", @class = "vitals", @maxlength = "4" }) %>
                    <label for="Recertification_485IntegumentaryGoals3">% by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryGoals4' class='radio float_left' name='Recertification_485IntegumentaryGoals' value='4' type='checkbox' {0} />", integumentaryGoals!=null && integumentaryGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485IntegumentaryGoals4" class="radio">Patient skin integrity will remain intact during this episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485IntegumentaryGoals5' class='radio float_left' name='Recertification_485IntegumentaryGoals' value='5' type='checkbox' {0} />", integumentaryGoals != null && integumentaryGoals.Contains("5") ? "checked='checked'" : "")%>
                <span class="radio">
                    <%  var demonstrateSterileDressingTechniquePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485DemonstrateSterileDressingTechniquePerson") && data["485DemonstrateSterileDressingTechniquePerson"].Answer != "" ? data["485DemonstrateSterileDressingTechniquePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485DemonstrateSterileDressingTechniquePerson", demonstrateSterileDressingTechniquePerson)%>
                    <label for="Recertification_485IntegumentaryGoals5">will demonstrate understanding of changing</label>
                    <%=Html.TextBox("Recertification_485DemonstrateSterileDressingTechniqueType", data.ContainsKey("485DemonstrateSterileDressingTechniqueType") ? data["485DemonstrateSterileDressingTechniqueType"].Answer : "", new { @id = "Recertification_485DemonstrateSterileDressingTechniqueType", @class = "zip", @maxlength = "10" })%>
                    <label for="Recertification_485IntegumentaryGoals5">dressing using sterile technique.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485IntegumentaryGoalTemplates" class="strong">Additional Goals:</label>
                <%  var integumentaryGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IntegumentaryGoalTemplates") && data["485IntegumentaryGoalTemplates"].Answer != "" ? data["485IntegumentaryGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485IntegumentaryGoalTemplates", integumentaryGoalTemplates) %>
                <%= Html.TextArea("Recertification_485IntegumentaryGoalComments", data.ContainsKey("485IntegumentaryGoalComments") ? data["485IntegumentaryGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485IntegumentaryGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.BradenScaleScore("Recertification");
    Oasis.showIfChecked($("#Recertification_GenericSkinColor5"), $("#Recertification_GenericSkinColorOther"));
    Oasis.showIfChecked($("#Recertification_GenericSkinCondition6"), $("#Recertification_GenericSkinConditionOther"));
    Oasis.showIfRadioEquals("Recertification_GenericNails", "Problem", $("#Recertification_GenericNailsProblemMore"));
    Oasis.showIfRadioEquals("Recertification_GenericPressureRelievingDevice", "1", $("#Recertification_GenericPressureRelievingDeviceMore"));
    Oasis.showIfSelectEquals($("#Recertification_GenericPressureRelievingDeviceType"), "4", $("#Recertification_GenericPressureRelievingDeviceTypeOther"));
    Oasis.showIfRadioEquals("Recertification_GenericIVAccess", "1", $("#window_Recertification .IVAccess"));
    Oasis.showIfRadioEquals("Recertification_GenericFlush", "1", $("#Recertification_GenericFlushMore"));
    Oasis.hideIfRadioEquals("Recertification_M1300PressureUlcerAssessment", "00", $("#Recertification_M1302"));
    Oasis.hideIfRadioEquals("Recertification_M1306UnhealedPressureUlcers", "0", $(".Recertification_M1306_00"));
    Oasis.hideIfRadioEquals("Recertification_M1330StasisUlcer", "00|03", $("#Recertification_M1332AndM1334"));
    Oasis.hideIfRadioEquals("Recertification_M1340SurgicalWound", "00", $("#Recertification_M1342"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
    $("input.WoundType").each(function() {
        var data = ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"];
        $(this).bind("focus", function() { $(this).autocomplete(data); });
    });
    $("input.DeviceType").each(function() {
        var dataDevice = ["J.P.", "Wound Vac", "None"];
        $(this).bind("focus", function() { $(this).autocomplete(dataDevice); });
    });
    $("input.IVSiteDressingUnit").each(function() {
        var dataDevice = ["Normal Saline", "Heparin"];
        $(this).bind("focus", function() { $(this).autocomplete(dataDevice); });
    });
</script>