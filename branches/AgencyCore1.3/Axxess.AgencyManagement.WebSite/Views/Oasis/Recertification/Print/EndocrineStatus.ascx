﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericEndocrineWithinNormalLimits") && data["GenericEndocrineWithinNormalLimits"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.span("Is patient diabetic?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "0" ? "true" : "false"%>)))) +
        printview.col(5,
            printview.span("Diabetic Management:",true) +
            printview.checkbox("Diet",<%= data != null && data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Oral Hypoglycemic",<%= data != null && data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Exercise",<%= data != null && data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Insulin",<%= data != null && data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.col(2,
            printview.col(2,
                printview.span("Insulin dependent?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "0" ? "true" : "false"%>))) +
            printview.col(2,
                printview.span("How long?") +
                printview.span("<%= data != null && data.ContainsKey("GenericInsulinDependencyDuration") && data["GenericInsulinDependencyDuration"].Answer.IsNotNullOrEmpty() ? data["GenericInsulinDependencyDuration"].Answer : ""%>",false,1)) +
            printview.span("Insulin Administered by:",true) +
            printview.col(2,
                printview.col(2,
                    printview.checkbox("N/A",<%= data != null && data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Patient",<%= data != null && data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',').Contains("2") ? "true" : "false"%>)) +
                printview.col(2,
                    printview.checkbox("Caregiver",<%= data != null && data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
                    printview.checkbox("SN",<%= data != null && data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',').Contains("4") ? "true" : "false"%>))) +
            printview.span("Is patient independent with glucometer use?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Is caregiver independent with glucometer use?",true) +
            printview.col(3,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer.Split(',').Contains("0") ? "true" : "false"%>) +
                printview.checkbox("N/A, no caregiver",<%= data != null && data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer.Split(',').Contains("2") ? "true" : "false"%>))) +
        printview.span("Does patient have any of the following?",true) +
        printview.col(6,
            printview.checkbox("Polyuria",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Polydipsia",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Polyphagia",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Neuropathy",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Radiculopathy",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Thyroid problems",<%= data != null && data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer.Split(',').Contains("6") ? "true" : "false"%>)) +
        printview.col(2,
            printview.col(2,
                printview.span("<strong>Blood Sugar:</strong> <%= data != null && data.ContainsKey("GenericBloodSugarLevelText") && data["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() ? data["GenericBloodSugarLevelText"].Answer : "<span class='short blank'></span>"%> mg/dl") +
                printview.col(2,
                    printview.checkbox("Random",<%= data != null && data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Random" ? "true" : "false"%>) +
                    printview.checkbox("Fasting",<%= data != null && data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Fasting" ? "true" : "false"%>))) +
            printview.col(2,
                printview.span("Blood sugar checked by:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericBloodSugarCheckedBy") && data["GenericBloodSugarCheckedBy"].Answer.IsNotNullOrEmpty() ? (data["GenericBloodSugarCheckedBy"].Answer == "0" ? "" : "") + (data["GenericBloodSugarCheckedBy"].Answer == "1" ? "Patient" : "") + (data["GenericBloodSugarCheckedBy"].Answer == "2" ? "SN" : "") + (data["GenericBloodSugarCheckedBy"].Answer == "3" ? "Caregiver" : "") : ""%>",false,1))) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericEndocrineComments") && data["GenericEndocrineComments"].Answer.IsNotNullOrEmpty() ? data["GenericEndocrineComments"].Answer : ""%>",false,2),
        "Endocrine");
    printview.addsection(
        printview.checkbox("SN to assess <%= data != null && data.ContainsKey("485AbilityToManageDiabetic") && data["485AbilityToManageDiabetic"].Answer.IsNotNullOrEmpty() ? data["485AbilityToManageDiabetic"].Answer : "<span class='blank'></span>"%> ability to manage diabetic disease process.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to perform BG checks every visit &amp; PRN.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on insulin preparation, administration, site rotation and disposal of supplies.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to assess/instruct on diabetic management to include: nail, skin &amp; foot care, medication administration and proper diet.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to administer <%= data != null && data.ContainsKey("485AmountOfInsuline") && data["485AmountOfInsuline"].Answer.IsNotNullOrEmpty() ? data["485AmountOfInsuline"].Answer : "<span class='blank'></span>"%> insulin as follows: <%= data != null && data.ContainsKey("485AmountOfInsulineAsFollows") && data["485AmountOfInsulineAsFollows"].Answer.IsNotNullOrEmpty() ? data["485AmountOfInsulineAsFollows"].Answer : "<span class='blank'></span>"%>.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("SN to prefill syringes with <%= data != null && data.ContainsKey("485PrefillOfInsuline") && data["485PrefillOfInsuline"].Answer.IsNotNullOrEmpty() ? data["485PrefillOfInsuline"].Answer : "<span class='blank'></span>"%> insulin as follows: <%= data != null && data.ContainsKey("485PrefillOfInsulineAsFollows") && data["485PrefillOfInsulineAsFollows"].Answer.IsNotNullOrEmpty() ? data["485PrefillOfInsulineAsFollows"].Answer : "<span class='blank'></span>"%>.",<%= data != null && data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485EndocrineInterventionComments") && data["485EndocrineInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485EndocrineInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485GlucometerUseIndependencePerson") && data["485GlucometerUseIndependencePerson"].Answer.IsNotNullOrEmpty() ? data["485GlucometerUseIndependencePerson"].Answer : "<span class='blank'></span>"%> will be independent with glucometer use by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485IndependentInsulinAdministrationPerson") && data["485IndependentInsulinAdministrationPerson"].Answer.IsNotNullOrEmpty() ? data["485IndependentInsulinAdministrationPerson"].Answer : "<span class='blank'></span>"%> will be independent with insulin administration by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485VerbalizeProperFootCarePerson") && data["485VerbalizeProperFootCarePerson"].Answer.IsNotNullOrEmpty() ? data["485VerbalizeProperFootCarePerson"].Answer : "<span class='blank'></span>"%> will verbalize understanding of proper diabetic foot care by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485DiabetesManagement") && data["485DiabetesManagement"].Answer.IsNotNullOrEmpty() ? data["485DiabetesManagement"].Answer : "<span class='blank'></span>"%> will verbalize knowledge of diabetes management, S&amp;S of complications, hypo/hyperglycemia, foot care and management during illness or stress by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485GeneralDemonstration") && data["485GeneralDemonstration"].Answer.IsNotNullOrEmpty() ? data["485GeneralDemonstration"].Answer : "<span class='blank'></span>"%> will demonstrate correct insulin preparation, injection procedure, storage and disposal of supplies by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485GeneralVerbalization") && data["485GeneralVerbalization"].Answer.IsNotNullOrEmpty() ? data["485GeneralVerbalization"].Answer : "<span class='blank'></span>"%> will verbalize understanding of the importance of keeping blood glucose levels within parameters by the end of the episode.",<%= data != null && data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485EndocrineGoalComments") && data["485EndocrineGoalComments"].Answer.IsNotNullOrEmpty() ? data["485EndocrineGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>