﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("Incontinence",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Bladder Distention",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("Discharge",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Frequency",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("Dysuria",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Retention",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
                printview.checkbox("Urgency",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Oliguria",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("9") ? "true" : "false"%>,true) +
                printview.checkbox("Catheter/Device:",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("10") ? "true" : "false"%>,true)) +
            printview.col(3,
                printview.span("<%= data != null && data.ContainsKey("GenericGUCatheterList") && data["GenericGUCatheterList"].Answer.IsNotNullOrEmpty() ? (data["GenericGUCatheterList"].Answer == "0" ? "" : "") + (data["GenericGUCatheterList"].Answer == "1" ? "N/A" : "") + (data["GenericGUCatheterList"].Answer == "2" ? "Foley Catheter" : "") + (data["GenericGUCatheterList"].Answer == "3" ? "Condom Catheter" : "") + (data["GenericGUCatheterList"].Answer == "4" ? "Suprapubic Catheter" : "") + (data["GenericGUCatheterList"].Answer == "5" ? "Urostomy" : "") + (data["GenericGUCatheterList"].Answer == "6" ? "Other" : "") : ""%>",false,1) +
                printview.span("Last Changed <%= data != null && data.ContainsKey("GenericGUCatheterLastChanged") && data["GenericGUCatheterLastChanged"].Answer.IsNotNullOrEmpty() ? data["GenericGUCatheterLastChanged"].Answer : "<span class='blank'></span>"%>") +
                printview.span("<%= data != null && data.ContainsKey("GenericGUCatheterFrequency") && data["GenericGUCatheterFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericGUCatheterFrequency"].Answer + "Fr " : ""%><%= data != null && data.ContainsKey("GenericGUCatheterAmount") && data["GenericGUCatheterAmount"].Answer.IsNotNullOrEmpty() ? data["GenericGUCatheterAmount"].Answer + "cc" : ""%>",false,1))) +
        printview.col(4,
            printview.checkbox("Urine:",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("11") ? "true" : "false"%>,true) +
            printview.checkbox("Cloudy",<%= data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Odorous",<%= data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Sediment",<%= data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.span("") + printview.checkbox("Hematuria",<%= data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericGUOtherText") && data["GenericGUOtherText"].Answer.IsNotNullOrEmpty() ? data["GenericGUOtherText"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.span("") + printview.checkbox("External Genitalia:",<%= data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer.Split(',').Contains("12") ? "true" : "false"%>,true) +
            printview.span("") + printview.checkbox("Normal",<%= data != null && data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Abnormal",<%= data != null && data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "0" ? "true" : "false"%>) +
            printview.span("") + printview.span("As per:") +
            printview.checkbox("Clinician Assessment",<%= data != null && data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Pt/CG Report",<%= data != null && data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "0" ? "true" : "false"%>)),
        "GU");
    printview.addsection(
        printview.col(2,
            printview.span("Is patient on dialysis?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "0" ? "true" : "false"%>))) +<%
    if (data == null || data.ContainsKey("GenericPatientOnDialysis") == false || data["GenericPatientOnDialysis"].Answer != "0") { %>
        printview.col(2,
            printview.checkbox("Peritoneal Dialysis",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("CCPD (Continuous Cyclic Peritoneal Dialysis)",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("IPD (Intermittent Peritoneal Dialysis)",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("CAPD (Continuous Ambulatory Peritoneal Dialysis)",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Hemodialysis",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("AV Graft/ Fistula Site:",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericDialysisHemodialysisGriftAV") && data["GenericDialysisHemodialysisGriftAV"].Answer.IsNotNullOrEmpty() ? data["GenericDialysisHemodialysisGriftAV"].Answer : ""%>",false,1) +
                printview.checkbox("Central Venous Catheter Site:",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericDialysisHemodialysisCentralVenousAV") && data["GenericDialysisHemodialysisCentralVenousAV"].Answer.IsNotNullOrEmpty() ? data["GenericDialysisHemodialysisCentralVenousAV"].Answer : ""%>",false,1)) +
            printview.checkbox("Catheter site free from signs and symptoms of infection",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericDialysisOtherDesc") && data["GenericDialysisOtherDesc"].Answer.IsNotNullOrEmpty() ? data["GenericDialysisOtherDesc"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true)) +
        printview.span("Dialysis Center:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericDialysisCenter") && data["GenericDialysisCenter"].Answer.IsNotNullOrEmpty() ? data["GenericDialysisCenter"].Answer : ""%>",false,2) +<%
    } %>"","Dialysis");
    printview.addsection(
        printview.span("(M1610) Urinary Incontinence or Urinary Catheter Presence:",true) +
        printview.checkbox("0 &ndash; No incontinence or catheter (includes anuria or ostomy for urinary drainage)",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Patient is incontinent",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic)",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1620) Bowel Incontinence Frequency:",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; Very rarely or never has bowel incontinence",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less than once weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; One to three times weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Four to six times weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; On a daily basis",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; More often than once daily",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient has ostomy for bowel elimination",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?",true) +
        printview.checkbox("0 &ndash; Patient does not have an ostomy for bowel elimination.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? "true" : "false"%>));
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Bowel Sounds: <%= data != null && data.ContainsKey("GenericDigestiveBowelSoundsType") && data["GenericDigestiveBowelSoundsType"].Answer.IsNotNullOrEmpty() ? (data["GenericDigestiveBowelSoundsType"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericDigestiveBowelSoundsType"].Answer == "1" ? "Present/WNL" : "") + (data["GenericDigestiveBowelSoundsType"].Answer == "2" ? "Hyperactive" : "") + (data["GenericDigestiveBowelSoundsType"].Answer == "3" ? "Hypoactive" : "") + (data["GenericDigestiveBowelSoundsType"].Answer == "4" ? "Absent" : "") : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Abdominal Palpation: <%= data != null && data.ContainsKey("GenericAbdominalPalpation") && data["GenericAbdominalPalpation"].Answer.IsNotNullOrEmpty() ? (data["GenericAbdominalPalpation"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericAbdominalPalpation"].Answer == "1" ? "Soft/ WNL" : "") + (data["GenericAbdominalPalpation"].Answer == "2" ? "Firm" : "") + (data["GenericAbdominalPalpation"].Answer == "3" ? "Tender" : "") + (data["GenericAbdominalPalpation"].Answer == "4" ? "Other" : "") : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Bowel Incontinence",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Nausea",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.checkbox("Vomiting",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("GERD",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
            printview.checkbox("Abd Girth: <%= data != null && data.ContainsKey("GenericDigestiveAbdGirthLength") && data["GenericDigestiveAbdGirthLength"].Answer.IsNotNullOrEmpty() ? data["GenericDigestiveAbdGirthLength"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericGUDigestiveComments") && data["GenericGUDigestiveComments"].Answer.IsNotNullOrEmpty() ? data["GenericGUDigestiveComments"].Answer : ""%>",false,2) +
        printview.col(4,
            printview.span("Elimination",true) +
            printview.checkbox("Last BM:",<%= data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer.Split(',').Contains("11") ? "true" : "false"%>,true) +
            printview.span("Date: <%= data != null && data.ContainsKey("GenericDigestiveLastBMDate") && data["GenericDigestiveLastBMDate"].Answer.IsNotNullOrEmpty() ? data["GenericDigestiveLastBMDate"].Answer : "<span class='blank'></span>"%>") +
            printview.checkbox("WNL",<%= data != null && data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true)) +
        printview.col(5,
            printview.checkbox("Abnormal Stool:",<%= data != null && data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Gray",<%= data != null && data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Tarry",<%= data != null && data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Fresh Blood",<%= data != null && data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Black",<%= data != null && data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.col(4,
            printview.checkbox("Constipation:",<%= data != null && data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Chronic",<%= data != null && data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Chronic" ? "true" : "false"%>) +
            printview.checkbox("Acute",<%= data != null && data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Acute" ? "true" : "false"%>) +
            printview.checkbox("Occasional",<%= data != null && data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Occasional" ? "true" : "false"%>) +
            printview.checkbox("Diarrhea:",<%= data != null && data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Chronic",<%= data != null && data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Chronic" ? "true" : "false"%>) +
            printview.checkbox("Acute",<%= data != null && data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Acute" ? "true" : "false"%>) +
            printview.checkbox("Occasional",<%= data != null && data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Occasional" ? "true" : "false"%>)) +
        printview.col(4,
            printview.span("Ostomy",true) +
            printview.checkbox("Ostomy Type: <%= data != null && data.ContainsKey("GenericDigestiveOstomyType") && data["GenericDigestiveOstomyType"].Answer.IsNotNullOrEmpty() ? (data["GenericDigestiveOstomyType"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericDigestiveOstomyType"].Answer == "1" ? "N/A" : "") + (data["GenericDigestiveOstomyType"].Answer == "2" ? "Ileostomy" : "") + (data["GenericDigestiveOstomyType"].Answer == "3" ? "Colostomy" : "") + (data["GenericDigestiveOstomyType"].Answer == "4" ? "Other" : "") : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Stoma Appearance: <%= data != null && data.ContainsKey("GenericDigestiveStomaAppearance") && data["GenericDigestiveStomaAppearance"].Answer.IsNotNullOrEmpty() ? data["GenericDigestiveStomaAppearance"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Surrounding Skin: <%= data != null && data.ContainsKey("GenericDigestiveSurSkinType") && data["GenericDigestiveSurSkinType"].Answer.IsNotNullOrEmpty() ? data["GenericDigestiveSurSkinType"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true)),
        "GI");
    printview.addsection(
        printview.checkbox("SN to instruct on establishing bowel regimen.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on care of stoma, surrounding skin and use of skin barrier.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("SN to change catheter every month and PRN using a <%= data != null && data.ContainsKey("485EliminationFoleyCatheterType") && data["485EliminationFoleyCatheterType"].Answer.IsNotNullOrEmpty() ? data["485EliminationFoleyCatheterType"].Answer : "<span class='blank'></span>"%> F catheter.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct on intermittent catheterizations.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
        printview.checkbox("SN to perform intermittent catheterization every <%= data != null && data.ContainsKey("485EliminationCatheterizationNumber") && data["485EliminationCatheterizationNumber"].Answer.IsNotNullOrEmpty() ? data["485EliminationCatheterizationNumber"].Answer : "<span class='blank'></span>"%> &amp; prn using sterile technique.",<%= data != null && data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485EliminationInterventionComments") && data["485EliminationInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485EliminationInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
</script>