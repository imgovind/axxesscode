﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationOrdersDisciplineTreatmentForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<%= Html.Hidden("categoryType", "OrdersDisciplineTreatment")%>
<div class="wrapper main">
    <div id="Recertification_BlankMasterCalendar"></div>
    <fieldset>
        <legend>Orders for Discipline and Treatments</legend>
        <div class="wide_column">
            <div class="row">
                <div class="buttons align_center">
                    <ul>
                        <li><a id="Recertification_Show" href="javascript:void(0);" onclick="Oasis.LoadBlankMasterCalendar('Recertification','<%=Model.EpisodeId %>','<%=Model.PatientId %>');">Show Calendar</a></li>
                        <li><a id="Recertification_Hide" style="display:none;" href="javascript:void(0);" onclick="Oasis.HideBlankMasterCalendar('Recertification');"> Hide Calendar</a></li>
                    </ul>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="Recertification_485SNFrequency" class="float_left">SN Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485SNFrequency", data.ContainsKey("485SNFrequency") ? data["485SNFrequency"].Answer : "", new { @id = "Recertification_485SNFrequency", @maxlength = "70" })%></div>
            </div><div class="row">
                <label for="Recertification_485PTFrequency" class="float_left">PT Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485PTFrequency", data.ContainsKey("485PTFrequency") ? data["485PTFrequency"].Answer : "", new { @id = "Recertification_485PTFrequency", @maxlength = "70" })%></div>
            </div><div class="row">
                <label for="Recertification_485OTFrequency" class="float_left">OT Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485OTFrequency", data.ContainsKey("485OTFrequency") ? data["485OTFrequency"].Answer : "", new { @id = "Recertification_485OTFrequency", @maxlength = "70" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="Recertification_485STFrequency" class="float_left">ST Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485STFrequency", data.ContainsKey("485STFrequency") ? data["485STFrequency"].Answer : "", new { @id = "Recertification_485STFrequency", @maxlength = "70" })%></div>
            </div><div class="row">
                <label for="Recertification_485MSWFrequency" class="float_left">MSW Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485MSWFrequency", data.ContainsKey("485MSWFrequency") ? data["485MSWFrequency"].Answer : "", new { @id = "Recertification_485MSWFrequency", @maxlength = "70" })%></div>
            </div><div class="row">
                <label for="Recertification_485HHAFrequency" class="float_left">HHA Frequency</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485HHAFrequency", data.ContainsKey("485HHAFrequency") ? data["485HHAFrequency"].Answer : "", new { @id = "Recertification_485HHAFrequency", @maxlength = "70" })%></div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="Recertification_485OrdersDisciplineInterventionTemplates" class="strong">Additional Orders:</label>
                <%  var ordersDisciplineInterventionTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1"}
                    }, "Value", "Text", data.ContainsKey("485OrdersDisciplineInterventionTemplates") && data["485OrdersDisciplineInterventionTemplates"].Answer != "" ? data["485OrdersDisciplineInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485OrdersDisciplineInterventionTemplates", ordersDisciplineInterventionTemplates)%>
                <div><%=Html.TextArea("Recertification_485OrdersDisciplineInterventionComments", data.ContainsKey("485OrdersDisciplineInterventionComments") ? data["485OrdersDisciplineInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485OrdersDisciplineInterventionComments" })%></div>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Conclusions</legend>
        <%string[] conclusions = data.ContainsKey("485Conclusions") && data["485Conclusions"].Answer != "" ? data["485Conclusions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485Conclusions" value="" />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485Conclusions1' class='radio float_left' name='Recertification_485Conclusions' value='1' type='checkbox' {0} />", conclusions!=null && conclusions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485Conclusions1" class="radio">Skilled Intervention Needed</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485Conclusions2' class='radio float_left' name='Recertification_485Conclusions' value='2' type='checkbox' {0} />", conclusions!=null && conclusions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485Conclusions2" class="radio">Skilled Instruction Needed</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485Conclusions3' class='radio float_left' name='Recertification_485Conclusions' value='3' type='checkbox' {0} />", conclusions!=null && conclusions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485Conclusions3" class="radio">No Skilled Service Needed</label>
            </div><div class="row">
                <label for="Recertification_485ConclusionOther" class="float_left">Other:</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485ConclusionOther", data.ContainsKey("485ConclusionOther") ? data["485ConclusionOther"].Answer : "", new { @id = "Recertification_485ConclusionOther", @maxlength = "30" })%></div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Rehabilitation Potential</legend>
        <%string[] rehabilitationPotential = data.ContainsKey("485RehabilitationPotential") && data["485RehabilitationPotential"].Answer != "" ? data["485RehabilitationPotential"].Answer.Split(',') : null; %>
        <input name="Recertification_485RehabilitationPotential" value="" type="hidden" />
        <div class="column">
            <div class="row">
                <label class="float_left">Rehabilitation potential for stated goals:</label>
                <div class="float_right">
                    <%= string.Format("<input id='Recertification_485RehabilitationPotential1' class='radio' name='Recertification_485RehabilitationPotential' value='1' type='checkbox' {0} />", rehabilitationPotential!=null && rehabilitationPotential.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485RehabilitationPotential1" class="inlineradio">Good</label>
                    <%= string.Format("<input id='Recertification_485RehabilitationPotential2' class='radio' name='Recertification_485RehabilitationPotential' value='2' type='checkbox' {0} />", rehabilitationPotential!=null && rehabilitationPotential.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485RehabilitationPotential2" class="inlineradio">Fair</label>
                    <%= string.Format("<input id='Recertification_485RehabilitationPotential3' class='radio' name='Recertification_485RehabilitationPotential' value='3' type='checkbox' {0} />", rehabilitationPotential!=null && rehabilitationPotential.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485RehabilitationPotential3" class="inlineradio">Poor</label>
                </div>
            </div><div class="row">
                <label for="Recertification_485AchieveGoalsTemplates" class="float_left">Other rehabilitation potential:</label>
                <div class="float_right">
                    <%  var achieveGoalsTemplates = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "-----------", Value = "-2" },
                            new SelectListItem { Text = "Erase", Value = "-1"}
                        }, "Value", "Text", data.ContainsKey("485AchieveGoalsTemplates") && data["485AchieveGoalsTemplates"].Answer != "" ? data["485AchieveGoalsTemplates"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_485AchieveGoalsTemplates", achieveGoalsTemplates)%>
                </div>
                <div class="clear"></div>
                <%=Html.TextArea("Recertification_485AchieveGoalsComments", data.ContainsKey("485AchieveGoalsComments") ? data["485AchieveGoalsComments"].Answer : "", 5, 70, new { @id = "Recertification_485AchieveGoalsComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Discharge Plans</legend>
        <% string[] dischargePlans = data.ContainsKey("485DischargePlans") && data["485DischargePlans"].Answer != "" ? data["485DischargePlans"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485DischargePlans" value="" />
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Patient to be discharged to the care of:</label>
                <div class="float_right">
                    <%= string.Format("<input id='Recertification_485DischargePlans1' class='radio' name='Recertification_485DischargePlans' value='1' type='checkbox' {0} />", dischargePlans!=null && dischargePlans.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485DischargePlans1" class="inlineradio">Physician</label>
                    <%= string.Format("<input id='Recertification_485DischargePlans2' class='radio' name='Recertification_485DischargePlans' value='2' type='checkbox' {0} />", dischargePlans!=null && dischargePlans.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485DischargePlans2" class="inlineradio">Caregiver</label>
                    <%= string.Format("<input id='Recertification_485DischargePlans3' class='radio' name='Recertification_485DischargePlans' value='3' type='checkbox' {0} />", dischargePlans!=null && dischargePlans.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485DischargePlans3" class="inlineradio">Self care</label>
               </div>
            </div><div class="row">
                <label class="float_left">Discharge Plans</label>
                <div class="clear"></div>
                <div class="column">
                    <div class="row">
                        <% string[] dischargePlansReason = data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer != "" ? data["485DischargePlansReason"].Answer.Split(',') : null; %>
                        <%= string.Format("<input id='Recertification_485DischargePlansReason1' class='radio float_left' name='Recertification_485DischargePlansReason' value='1' type='checkbox' {0} />", dischargePlansReason != null && dischargePlansReason.Contains("1") ? "checked='checked'" : "")%>
                        <label for="Recertification_485DischargePlansReason1" class="radio">Discharge when caregiver willing and able to manage all aspects of patient's care.</label>
                    </div><div class="row">
                        <%= string.Format("<input id='Recertification_485DischargePlansReason2' class='radio float_left' name='Recertification_485DischargePlansReason' value='2' type='checkbox' {0} />", dischargePlansReason != null && dischargePlansReason.Contains("2") ? "checked='checked'" : "")%>
                        <label for="Recertification_485DischargePlansReason2" class="radio">Discharge when goals met.</label>
                    </div><div class="row">
                        <%= string.Format("<input id='Recertification_485DischargePlansReason3' class='radio float_left' name='Recertification_485DischargePlansReason' value='3' type='checkbox' {0} />", dischargePlansReason != null && dischargePlansReason.Contains("3") ? "checked='checked'" : "")%>
                        <label for="Recertification_485DischargePlansReason3" class="radio">Discharge when wound(s) healed.</label>
                    </div>
                </div>
            </div><div class="row">
                <label for="Recertification_485DischargePlanTemplates" class="strong">Additional discharge plans:</label>
                <%  var dischargePlanTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1"}
                    }, "Value", "Text", data.ContainsKey("485DischargePlanTemplates") && data["485DischargePlanTemplates"].Answer != "" ? data["485DischargePlanTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485DischargePlanTemplates", dischargePlanTemplates)%>
                <div><%=Html.TextArea("Recertification_485DischargePlanComments", data.ContainsKey("485DischargePlanComments") ? data["485DischargePlanComments"].Answer : "", 5, 70, new { @id = "Recertification_485DischargePlanComments" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Recertification_485SkilledInterventionTemplate" class="strong">Skilled Intervention/Teaching:</label>
                <%  var skilledInterventionTemplate = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1"}
                    }, "Value", "Text", data.ContainsKey("485SkilledInterventionTemplate") && data["485SkilledInterventionTemplate"].Answer != "" ? data["485SkilledInterventionTemplate"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485SkilledInterventionTemplate", skilledInterventionTemplate)%>
                <div><%=Html.TextArea("Recertification_485SkilledInterventionComments", data.ContainsKey("485SkilledInterventionComments") ? data["485SkilledInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485SkilledInterventionComments" })%></div>
                <input type="hidden" name="Recertification_485SIResponse" id="Recertification_485SIResponse" />
                <div class="column">
                    <div class="row">
                        <label class="float_left">Verbalized Understanding</label>
                        <div class="float_right">
                            <input type="hidden" name="Recertification_485SIVerbalizedUnderstandingPT" value=" " />
                            <%= string.Format("<input id='Recertification_485SIVerbalizedUnderstandingPT' class='radio' name='Recertification_485SIVerbalizedUnderstandingPT' value='1' type='checkbox' {0} />", data.ContainsKey("485SIVerbalizedUnderstandingPT") && data["485SIVerbalizedUnderstandingPT"].Answer == "1" ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485SIVerbalizedUnderstandingPT" class="fixed inlineradio small">PT</label>
                            <div id="Recertification_485SIVerbalizedUnderstandingPTMore" class="float_right"><input type="text" name="Recertification_485SIVerbalizedUnderstandingPTPercent" id="Recertification_485SIVerbalizedUnderstandingPTPercent" maxlength="4" value="<%= data.ContainsKey("485SIVerbalizedUnderstandingPTPercent") ? data["485SIVerbalizedUnderstandingPTPercent"].Answer : "" %>" class="vitals numeric" />%</div>
                        </div>
                        <div class="clear"></div>
                        <div class="float_right">
                            <input type="hidden" name="Recertification_485SIVerbalizedUnderstandingCG" value=" " />
                            <%= string.Format("<input id='Recertification_485SIVerbalizedUnderstandingCG' class='radio' name='Recertification_485SIVerbalizedUnderstandingCG' value='1' type='checkbox' {0} />", data.ContainsKey("485SIVerbalizedUnderstandingCG") && data["485SIVerbalizedUnderstandingCG"].Answer == "1" ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485SIVerbalizedUnderstandingCG" class="fixed inlineradio small">CG</label>
                            <div id="Recertification_485SIVerbalizedUnderstandingCGMore" class="float_right"><input type="text" name="Recertification_485SIVerbalizedUnderstandingCGPercent" id="Recertification_485SIVerbalizedUnderstandingCGPercent" maxlength="4" value="<%= data.ContainsKey("485SIVerbalizedUnderstandingCGPercent") ? data["485SIVerbalizedUnderstandingCGPercent"].Answer : "" %>" class="vitals numeric" />%</div>
                        </div>
                    </div>
                </div><div class="column">
                    <div class="row">
                        <label class="float_left">Return Demonstration</label>
                        <div class="float_right">
                            <input type="hidden" name="Recertification_485SIReturnDemonstrationPT" value=" " />
                            <%= string.Format("<input id='Recertification_485SIReturnDemonstrationPT' class='radio' name='Recertification_485SIReturnDemonstrationPT' value='1' type='checkbox' {0} />", data.ContainsKey("485SIReturnDemonstrationPT") && data["485SIReturnDemonstrationPT"].Answer == "1" ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485SIReturnDemonstrationPT" class="fixed inlineradio small">Pt</label>
                            <div id="Recertification_485SIReturnDemonstrationPTMore" class="float_right"><%=Html.TextBox("Recertification_485SIReturnDemonstrationPTPercent", data.ContainsKey("485SIReturnDemonstrationPTPercent") ? data["485SIReturnDemonstrationPTPercent"].Answer : "", new { @id = "Recertification_485SIReturnDemonstrationPTPercent", @class = "vitals numeric", @maxlength = "4" })%>%</div>
                        </div>
                        <div class="clear"></div>
                        <div class="float_right">
                            <input type="hidden" name="Recertification_485SIReturnDemonstrationCG" value=" " />
                            <%= string.Format("<input id='Recertification_485SIReturnDemonstrationCG' class='radio' name='Recertification_485SIReturnDemonstrationCG' value='1' type='checkbox' {0} />", data.ContainsKey("485SIReturnDemonstrationCG") && data["485SIReturnDemonstrationCG"].Answer == "1" ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485SIReturnDemonstrationCG" class="fixed inlineradio small">CG</label>
                            <div id="Recertification_485SIReturnDemonstrationCGMore" class="float_right"><%=Html.TextBox("Recertification_485SIReturnDemonstrationCGPercent", data.ContainsKey("485SIReturnDemonstrationCGPercent") ? data["485SIReturnDemonstrationCGPercent"].Answer : "", new { @id = "Recertification_485SIReturnDemonstrationCGPercent", @class = "vitals numeric", @maxlength = "4" })%>%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Cordination of Care</legend>
        <div class="column">
            <div class="row">
                <label for="Recertification_485ConferencedWith" class="float_left">Conferenced With:</label>
                <div class="float_right">
                    <%  var conferencedWith = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "MD", Value = "MD" },
                            new SelectListItem { Text = "SN", Value = "SN" },
                            new SelectListItem { Text = "PT", Value = "PT" },
                            new SelectListItem { Text = "OT", Value = "OT" },
                            new SelectListItem { Text = "ST", Value = "ST" },
                            new SelectListItem { Text = "MSW", Value = "MSW" },
                            new SelectListItem { Text = "HHA", Value = "HHA"}
                        }, "Value", "Text", data.ContainsKey("485ConferencedWith") && data["485ConferencedWith"].Answer != "" ? data["485ConferencedWith"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_485ConferencedWith", conferencedWith)%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="Recertification_485ConferencedWithName" class="float_left">Name:</label>
                <div class="float_right"><%=Html.TextBox("Recertification_485ConferencedWithName", data.ContainsKey("485ConferencedWithName") ? data["485ConferencedWithName"].Answer : "", new { @id = "Recertification_485ConferencedWithName", @maxlength = "30" })%></div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="Recertification_485SkilledInterventionRegarding" class="strong">Regarding:</label>
                <div><%=Html.TextArea("Recertification_485SkilledInterventionRegarding", data.ContainsKey("485SkilledInterventionRegarding") ? data["485SkilledInterventionRegarding"].Answer : "", 5, 70, new { @id = "Recertification_485SkilledInterventionRegarding" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Save &amp; Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul>
    </div>
</div>
<% } %>

<script type="text/javascript">
    Oasis.showIfChecked($("#Recertification_485SIVerbalizedUnderstandingPT"), $("#Recertification_485SIVerbalizedUnderstandingPTMore"));
    Oasis.showIfChecked($("#Recertification_485SIVerbalizedUnderstandingCG"), $("#Recertification_485SIVerbalizedUnderstandingCGMore"));
    Oasis.showIfChecked($("#Recertification_485SIReturnDemonstrationPT"), $("#Recertification_485SIReturnDemonstrationPTMore"));
    Oasis.showIfChecked($("#Recertification_485SIReturnDemonstrationCG"), $("#Recertification_485SIReturnDemonstrationCGMore"));
</script>