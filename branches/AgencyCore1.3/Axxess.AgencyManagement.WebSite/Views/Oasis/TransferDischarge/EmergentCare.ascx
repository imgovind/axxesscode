﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientDischargedPainForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientDischarged")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M2300</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2300');">(M2300)</a> Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?</div>
                <%=Html.Hidden("TransferInPatientDischarged_M2300EmergentCare", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2300EmergentCare", "00", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2300EmergentCare0", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2300EmergentCare0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2300EmergentCare", "01", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2300EmergentCare1", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2300EmergentCare1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, used hospital emergency department WITHOUT hospital admission</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2300EmergentCare", "02", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "02" ? true : false, new { @id = "TransferInPatientDischarged_M2300EmergentCare2", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2300EmergentCare2"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, used hospital emergency department WITH hospital admission</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2300');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2300EmergentCare", "UK", data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "UK" ? true : false, new { @id = "TransferInPatientDischarged_M2300EmergentCareUK", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2300EmergentCareUK"><span class="float_left">UK</span><span class="normal margin">Unknown</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientDischarged_M2310" class="oasis">
        <legend>OASIS M2310</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2310');">(M2310)</a> Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)? (Mark all that apply.)</div>
                <div class="margin">
                    <div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareMed" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareMed' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareMed' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareMed") && data["M2310ReasonForEmergentCareMed"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareMed"><span class="float_left">1 &ndash;</span><span class="normal margin">Improper medication administration, medication side effects, toxicity, anaphylaxis</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareFall" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareFall' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareFall' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareFall") && data["M2310ReasonForEmergentCareFall"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareFall"><span class="float_left">2 &ndash;</span><span class="normal margin">Injury caused by fall</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareResInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareResInf' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareResInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareResInf") && data["M2310ReasonForEmergentCareResInf"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareResInf"><span class="float_left">3 &ndash;</span><span class="normal margin">Respiratory infection (e.g., pneumonia, bronchitis)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareOtherResInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareOtherResInf' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareOtherResInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareOtherResInf") && data["M2310ReasonForEmergentCareOtherResInf"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareOtherResInf"><span class="float_left">4 &ndash;</span><span class="normal margin">Other respiratory problem</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareHeartFail" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareHeartFail' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareHeartFail' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareHeartFail") && data["M2310ReasonForEmergentCareHeartFail"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareHeartFail"><span class="float_left">5 &ndash;</span><span class="normal margin">Heart failure (e.g., fluid overload)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareCardiac" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareCardiac' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareCardiac' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareCardiac") && data["M2310ReasonForEmergentCareCardiac"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareCardiac"><span class="float_left">6 &ndash;</span><span class="normal margin">Cardiac dysrhythmia (irregular heartbeat)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareMyocardial" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareMyocardial' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareMyocardial' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareMyocardial") && data["M2310ReasonForEmergentCareMyocardial"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareMyocardial"><span class="float_left">7 &ndash;</span><span class="normal margin">Myocardial infarction or chest pain</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareHeartDisease" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareHeartDisease' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareHeartDisease' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareHeartDisease") && data["M2310ReasonForEmergentCareHeartDisease"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareHeartDisease"><span class="float_left">8 &ndash;</span><span class="normal margin">Other heart disease</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareStroke" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareStroke' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareStroke' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareStroke") && data["M2310ReasonForEmergentCareStroke"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareStroke"><span class="float_left">9 &ndash;</span><span class="normal margin">Stroke (CVA) or TIA</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareHypo" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareHypo' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareHypo' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareHypo") && data["M2310ReasonForEmergentCareHypo"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareHypo"><span class="float_left">10 &ndash;</span><span class="normal margin">Hypo/Hyperglycemia, diabetes out of control</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareGI" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareGI' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareGI' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareGI") && data["M2310ReasonForEmergentCareGI"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareGI"><span class="float_left">11 &ndash;</span><span class="normal margin">GI bleeding, obstruction, constipation, impaction</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareDehMal" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareDehMal' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareDehMal' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareDehMal") && data["M2310ReasonForEmergentCareDehMal"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareDehMal"><span class="float_left">12 &ndash;</span><span class="normal margin">Dehydration, malnutrition</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareUrinaryInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareUrinaryInf' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareUrinaryInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareUrinaryInf") && data["M2310ReasonForEmergentCareUrinaryInf"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareUrinaryInf"><span class="float_left">13 &ndash;</span><span class="normal margin">Urinary tract infection</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareIV" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareIV' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareIV' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareIV") && data["M2310ReasonForEmergentCareIV"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareIV"><span class="float_left">14 &ndash;</span><span class="normal margin">IV catheter-related infection or complication</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareWoundInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareWoundInf' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareWoundInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareWoundInf") && data["M2310ReasonForEmergentCareWoundInf"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareWoundInf"><span class="float_left">15 &ndash;</span><span class="normal margin">Wound infection or deterioration</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareUncontrolledPain" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareUncontrolledPain' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareUncontrolledPain' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain") && data["M2310ReasonForEmergentCareUncontrolledPain"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareUncontrolledPain"><span class="float_left">16 &ndash;</span><span class="normal margin">Uncontrolled pain</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareMental" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareMental' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareMental' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareMental") && data["M2310ReasonForEmergentCareMental"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareMental"><span class="float_left">17 &ndash;</span><span class="normal margin">Acute mental/behavioral health problem</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareDVT" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareDVT' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareDVT' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareDVT") && data["M2310ReasonForEmergentCareDVT"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareDVT"><span class="float_left">18 &ndash;</span><span class="normal margin">Deep vein thrombosis, pulmonary embolus</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareOther" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareOther' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareOther' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareOther") && data["M2310ReasonForEmergentCareOther"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareOther"><span class="float_left">19 &ndash;</span><span class="normal margin">Other than above reasons</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2310');">?</div>
                        </div>
                        <input name="TransferInPatientDischarged_M2310ReasonForEmergentCareUK" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2310ReasonForEmergentCareUK' class='radio float_left' name='TransferInPatientDischarged_M2310ReasonForEmergentCareUK' type='checkbox' value='1' {0} />", data.ContainsKey("M2310ReasonForEmergentCareUK") && data["M2310ReasonForEmergentCareUK"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientDischarged_M2310ReasonForEmergentCareUK"><span class="float_left">UK</span><span class="normal margin">Reason unknown</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"TransferForDischarge.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','TransferInPatientDischarged');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfRadioEquals("TransferInPatientDischarged_M2300EmergentCare", "00|UK", $("#TransferInPatientDischarged_M2310"));
    Oasis.noneOfTheAbove($("#TransferInPatientDischarged_M2310ReasonForEmergentCareUK"), $("#TransferInPatientDischarged_M2310 input"));
</script>