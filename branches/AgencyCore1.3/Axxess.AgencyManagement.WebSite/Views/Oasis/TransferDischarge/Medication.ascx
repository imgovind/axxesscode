﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientDischargedMedicationForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientDischarged")%>
<fieldset class="oasis">
    <div class="column">
        <div class="row">
            <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2004');">(M2004)</a> Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?<%=Html.Hidden("TransferInPatientDischarged_M2004MedicationIntervention", " ", new { @id = "" })%></div>
            <div><%=Html.RadioButton("TransferInPatientDischarged_M2004MedicationIntervention", "00", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2004MedicationIntervention0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2004MedicationIntervention0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label></div>
            <div><%=Html.RadioButton("TransferInPatientDischarged_M2004MedicationIntervention", "01", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2004MedicationIntervention1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2004MedicationIntervention1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label></div>
            <div><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M2004');">?</div></div><%=Html.RadioButton("TransferInPatientDischarged_M2004MedicationIntervention", "NA", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2004MedicationInterventionNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2004MedicationInterventionNA"><span class="float_left">NA</span><span class="normal margin">No clinically significant medication issues identified since the previous OASIS assessment</span></label></div>
        </div>
    </div><div class="column">
        <div class="row">
            <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2015');">(M2015)</a> Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?<%=Html.Hidden("TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention", " ", new { @id = "" })%></div>
            <div><%=Html.RadioButton("TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label></div>
            <div><%=Html.RadioButton("TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label></div>
            <div><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M2015');">?</div></div><%=Html.RadioButton("TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationInterventionNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2015PatientOrCaregiverDrugEducationInterventionNA"><span class="float_left">NA</span><span class="normal margin">Patient not taking any drugs</span></label></div>
        </div>
    </div>
</fieldset>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save &amp; Continue</a></li>
    <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save &amp; Exit</a></li>
</ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"TransferForDischarge.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','TransferInPatientDischarged');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
</ul></div>
<%  } %>