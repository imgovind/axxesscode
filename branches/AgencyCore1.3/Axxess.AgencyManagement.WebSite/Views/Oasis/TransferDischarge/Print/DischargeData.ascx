﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("(M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("Plan/ Intervention",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("No",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("Yes",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("NA &ndash; Not Applicable",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Patient is not diabetic or is bilateral amputee",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Falls prevention interventions") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Intervention(s) to monitor and mitigate pain") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment did not indicate pain since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Intervention(s) to prevent pressure ulcers") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Pressure ulcer treatment based on principles of moist wound healing") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Dressings that support the principles of moist wound healing not indicated for this patient&rsquo;s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/table%3E");
    printview.addsection(
        printview.span("(M2410) To which Inpatient Facility has the patient been admitted?",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Hospital",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Rehabilitation facility",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Nursing home",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Hospice",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M2430) Reason for Hospitalization: For what reason(s) did the patient require hospitalization? (Mark all that apply.)",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationMed") && data["M2430ReasonForHospitalizationMed"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Injury caused by fall",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationFall") && data["M2430ReasonForHospitalizationFall"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationInfection") && data["M2430ReasonForHospitalizationInfection"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Other respiratory problem",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationOtherRP") && data["M2430ReasonForHospitalizationOtherRP"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Heart failure (e.g., fluid overload)",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationHeartFail") && data["M2430ReasonForHospitalizationHeartFail"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Cardiac dysrhythmia (irregular heartbeat)",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationCardiac") && data["M2430ReasonForHospitalizationCardiac"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("7 &ndash; Myocardial infarction or chest pain",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationMyocardial") && data["M2430ReasonForHospitalizationMyocardial"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("8 &ndash; Other heart disease",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationHeartDisease") && data["M2430ReasonForHospitalizationHeartDisease"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("9 &ndash; Stroke (CVA) or TIA",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationStroke") && data["M2430ReasonForHospitalizationStroke"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("10 &ndash; Hypo/Hyperglycemia, diabetes out of control",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationHypo") && data["M2430ReasonForHospitalizationHypo"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("11 &ndash; GI bleeding, obstruction, constipation, impaction",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationGI") && data["M2430ReasonForHospitalizationGI"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("12 &ndash; Dehydration, malnutrition",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationDehMal") && data["M2430ReasonForHospitalizationDehMal"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("13 &ndash; Urinary tract infection",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationUrinaryInf") && data["M2430ReasonForHospitalizationUrinaryInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("14 &ndash; IV catheter-related infection or complication",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationIV") && data["M2430ReasonForHospitalizationIV"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("15 &ndash; Wound infection or deterioration",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationWoundInf") && data["M2430ReasonForHospitalizationWoundInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("16 &ndash; Uncontrolled pain",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain") && data["M2430ReasonForHospitalizationUncontrolledPain"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("17 &ndash; Acute mental/behavioral health problem",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationMental") && data["M2430ReasonForHospitalizationMental"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("18 &ndash; Deep vein thrombosis, pulmonary embolus",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationDVT") && data["M2430ReasonForHospitalizationDVT"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("19 &ndash; Scheduled treatment or procedure",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationScheduled") && data["M2430ReasonForHospitalizationScheduled"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("20 &ndash; Other than above reasons",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationOther") && data["M2430ReasonForHospitalizationOther"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Reason unknown",<%= data != null && data.ContainsKey("M2430ReasonForHospitalizationUK") && data["M2430ReasonForHospitalizationUK"].Answer == "1" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M2440) For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Therapy services",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedTherapy") && data["M2440ReasonPatientAdmittedTherapy"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Respite care",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedRespite") && data["M2440ReasonPatientAdmittedRespite"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Hospice care",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedHospice") && data["M2440ReasonPatientAdmittedHospice"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Permanent placement",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedPermanent") && data["M2440ReasonPatientAdmittedPermanent"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Unsafe for care at home",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedUnsafe") && data["M2440ReasonPatientAdmittedUnsafe"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Other",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedOther") && data["M2440ReasonPatientAdmittedOther"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M2440ReasonPatientAdmittedUnknown") && data["M2440ReasonPatientAdmittedUnknown"].Answer == "1" ? "true" : "false"%>)));
    printview.addsection(
        printview.col(2,
            printview.span("(M0903) Date of Last (Most Recent) Home Visit:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0903LastHomeVisitDate") && data["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty() ? data["M0903LastHomeVisitDate"].Answer : ""%>",false,1) +
            printview.span("(M0906) Discharge/Transfer/Death Date:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0906DischargeDate") && data["M0906DischargeDate"].Answer.IsNotNullOrEmpty() ? data["M0906DischargeDate"].Answer : ""%>",false,1)));
</script>