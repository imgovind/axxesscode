﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientDischargedOrdersForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientDischarged")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M2400</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2400');">(M2400)</a> Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?</div>
                <table>
                    <tr>
                        <th colspan="4">Plan/ Intervention</th>
                        <th>0 &ndash; No</th>
                        <th>1 &ndash; Yes</th>
                        <th colspan="4">NA &ndash; Not Applicable</th>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">a.</span><span class="radio">Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span><%=Html.Hidden("TransferInPatientDischarged_M2400DiabeticFootCare", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400DiabeticFootCare", "00", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400DiabeticFootCare0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DiabeticFootCare0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400DiabeticFootCare", "01", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400DiabeticFootCare1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DiabeticFootCare1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400DiabeticFootCare", "NA", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400DiabeticFootCareNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DiabeticFootCareNA"><span class="float_left">NA</span><span class="normal margin">Patient is not diabetic or is bilateral amputee</span></td>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">b.</span><span class="radio">Falls prevention interventions</span><%=Html.Hidden("TransferInPatientDischarged_M2400FallsPreventionInterventions", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400FallsPreventionInterventions", "00", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400FallsPreventionInterventions0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400FallsPreventionInterventions0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400FallsPreventionInterventions", "01", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400FallsPreventionInterventions1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400FallsPreventionInterventions1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400FallsPreventionInterventions", "NA", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400FallsPreventionInterventionsNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400FallsPreventionInterventionsNA"><span class="float_left">NA</span><span class="normal margin">Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment</span></td>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">c.</span><span class="radio">Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span><%=Html.Hidden("TransferInPatientDischarged_M2400DepressionIntervention", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400DepressionIntervention", "00", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400DepressionIntervention0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DepressionIntervention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400DepressionIntervention", "01", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400DepressionIntervention1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DepressionIntervention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400DepressionIntervention", "NA", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400DepressionInterventionNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400DepressionInterventionNA"><span class="float_left">NA</span><span class="normal margin">Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment</span></td>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">d.</span><span class="radio">Intervention(s) to monitor and mitigate pain</span><%=Html.Hidden("TransferInPatientDischarged_M2400PainIntervention", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PainIntervention", "00", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400PainIntervention0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PainIntervention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PainIntervention", "01", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400PainIntervention1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PainIntervention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400PainIntervention", "NA", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400PainInterventionNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PainInterventionNA"><span class="float_left">NA</span><span class="normal margin">Formal assessment did not indicate pain since the last OASIS assessment</span></td>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">e.</span><span class="radio">Intervention(s) to prevent pressure ulcers</span><%=Html.Hidden("TransferInPatientDischarged_M2400PressureUlcerIntervention", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerIntervention", "00", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerIntervention0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerIntervention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerIntervention", "01", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerIntervention1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerIntervention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerIntervention", "NA", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerInterventionNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerInterventionNA"><span class="float_left">NA</span><span class="normal margin">Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment</span></td>
                    </tr><tr>
                        <td colspan="4"><span class="float_left">f.</span><span class="radio">Pressure ulcer treatment based on principles of moist wound healing</span><%=Html.Hidden("TransferInPatientDischarged_M2400PressureUlcerTreatment", " ", new { @id = "" })%></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerTreatment", "00", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerTreatment0", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerTreatment0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label></td>
                        <td><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerTreatment", "01", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerTreatment1", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerTreatment1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label></td>
                        <td colspan="4"><%=Html.RadioButton("TransferInPatientDischarged_M2400PressureUlcerTreatment", "NA", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? true : false, new { @id = "TransferInPatientDischarged_M2400PressureUlcerTreatmentNA", @class = "radio float_left" })%><label for="TransferInPatientDischarged_M2400PressureUlcerTreatmentNA"><span class="float_left">NA</span><span class="normal margin">Dressings that support the principles of moist wound healing not indicated for this patient’s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing</span></td>
                    </tr>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2400');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M2410</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2410');">(M2410)</a> To which Inpatient Facility has the patient been admitted?</div>
                <%=Html.Hidden("TransferInPatientDischarged_M2410TypeOfInpatientFacility", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "01", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? true : false, new { @id = "TransferInPatientDischarged_M2410TypeOfInpatientFacility1", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2410TypeOfInpatientFacility1"><span class="float_left">1 &ndash;</span><span class="normal margin">Hospital</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "02", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? true : false, new { @id = "TransferInPatientDischarged_M2410TypeOfInpatientFacility2", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2410TypeOfInpatientFacility2"><span class="float_left">2 &ndash;</span><span class="normal margin">Rehabilitation facility</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "03", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? true : false, new { @id = "TransferInPatientDischarged_M2410TypeOfInpatientFacility3", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2410TypeOfInpatientFacility3"><span class="float_left">3 &ndash;</span><span class="normal margin">Nursing home</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2410');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "04", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? true : false, new { @id = "TransferInPatientDischarged_M2410TypeOfInpatientFacility4", @class = "radio float_left" })%>
                        <label for="TransferInPatientDischarged_M2410TypeOfInpatientFacility4"><span class="float_left">4 &ndash;</span><span class="normal margin">Hospice</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientDischarged_M2430" class="oasis">
        <legend>OASIS M2430</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2430');">(M2430)</a> Reason for Hospitalization: For what reason(s) did the patient require hospitalization? (Mark all that apply.)</div>
                <div class="margin">
                    <div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationMed" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationMed' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationMed' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationMed") && data["M2430ReasonForHospitalizationMed"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationMed"><span class="float_left">1 &ndash;</span><span class="margin normal">Improper medication administration, medication side effects, toxicity, anaphylaxis</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationFall" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationFall' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationFall' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationFall") && data["M2430ReasonForHospitalizationFall"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationFall"><span class="float_left">2 &ndash;</span><span class="margin normal">Injury caused by fall</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationInfection" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationInfection' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationInfection' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationInfection") && data["M2430ReasonForHospitalizationInfection"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationInfection"><span class="float_left">3 &ndash;</span><span class="margin normal">Respiratory infection (e.g., pneumonia, bronchitis)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationOtherRP" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationOtherRP' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationOtherRP' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationOtherRP") && data["M2430ReasonForHospitalizationOtherRP"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationOtherRP"><span class="float_left">4 &ndash;</span><span class="margin normal">Other respiratory problem</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationHeartFail" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationHeartFail' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationHeartFail' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationHeartFail") && data["M2430ReasonForHospitalizationHeartFail"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationHeartFail"><span class="float_left">5 &ndash;</span><span class="margin normal">Heart failure (e.g., fluid overload)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationCardiac" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationCardiac' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationCardiac' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationCardiac") && data["M2430ReasonForHospitalizationCardiac"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationCardiac"><span class="float_left">6 &ndash;</span><span class="margin normal">Cardiac dysrhythmia (irregular heartbeat)</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationMyocardial" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationMyocardial' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationMyocardial' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationMyocardial") && data["M2430ReasonForHospitalizationMyocardial"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationMyocardial"><span class="float_left">7 &ndash;</span><span class="margin normal">Myocardial infarction or chest pain</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationHeartDisease" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationHeartDisease' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationHeartDisease' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationHeartDisease") && data["M2430ReasonForHospitalizationHeartDisease"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationHeartDisease"><span class="float_left">8 &ndash;</span><span class="margin normal">Other heart disease</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationStroke" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationStroke' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationStroke' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationStroke") && data["M2430ReasonForHospitalizationStroke"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationStroke"><span class="float_left">9 &ndash;</span><span class="margin normal">Stroke (CVA) or TIA</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationHypo" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationHypo' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationHypo' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationHypo") && data["M2430ReasonForHospitalizationHypo"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationHypo"><span class="float_left">10 &ndash;</span><span class="margin normal">Hypo/Hyperglycemia, diabetes out of control</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationGI" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationGI' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationGI' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationGI") && data["M2430ReasonForHospitalizationGI"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationGI"><span class="float_left">11 &ndash;</span><span class="margin normal">GI bleeding, obstruction, constipation, impaction</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationDehMal" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationDehMal' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationDehMal' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationDehMal") && data["M2430ReasonForHospitalizationDehMal"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationDehMal"><span class="float_left">12 &ndash;</span><span class="margin normal">Dehydration, malnutrition</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationUrinaryInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationUrinaryInf' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationUrinaryInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationUrinaryInf") && data["M2430ReasonForHospitalizationUrinaryInf"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationUrinaryInf"><span class="float_left">13 &ndash;</span><span class="margin normal">Urinary tract infection</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationIV" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationIV' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationIV' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationIV") && data["M2430ReasonForHospitalizationIV"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationIV"><span class="float_left">14 &ndash;</span><span class="margin normal">IV catheter-related infection or complication</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationWoundInf" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationWoundInf' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationWoundInf' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationWoundInf") && data["M2430ReasonForHospitalizationWoundInf"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationWoundInf"><span class="float_left">15 &ndash;</span><span class="margin normal">Wound infection or deterioration</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationUncontrolledPain" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationUncontrolledPain' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationUncontrolledPain' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain") && data["M2430ReasonForHospitalizationUncontrolledPain"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationUncontrolledPain"><span class="float_left">16 &ndash;</span><span class="margin normal">Uncontrolled pain</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationMental" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationMental' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationMental' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationMental") && data["M2430ReasonForHospitalizationMental"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationMental"><span class="float_left">17 &ndash;</span><span class="margin normal">Acute mental/behavioral health problem</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationDVT" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationDVT' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationDVT' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationDVT") && data["M2430ReasonForHospitalizationDVT"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationDVT"><span class="float_left">18 &ndash;</span><span class="margin normal">Deep vein thrombosis, pulmonary embolus</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationScheduled" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationScheduled' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationScheduled' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationScheduled") && data["M2430ReasonForHospitalizationScheduled"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationScheduled"><span class="float_left">19 &ndash;</span><span class="margin normal">Scheduled treatment or procedure</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationOther" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationOther' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationOther' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationOther") && data["M2430ReasonForHospitalizationOther"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationOther"><span class="float_left">20 &ndash;</span><span class="margin normal">Other than above reasons</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2430');">?</div>
                        </div>
                        <input name="TransferInPatientDischarged_M2430ReasonForHospitalizationUK" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2430ReasonForHospitalizationUK' class='radio float_left' name='TransferInPatientDischarged_M2430ReasonForHospitalizationUK' type='checkbox' value='1' {0} />", data.ContainsKey("M2430ReasonForHospitalizationUK") && data["M2430ReasonForHospitalizationUK"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2430ReasonForHospitalizationUK"><span class="float_left">UK</span><span class="margin normal">Reason unknown</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientDischarged_M2440" class="oasis">
        <legend>OASIS M2440</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2440');">(M2440)</a> For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)</div>
                <div class="margin">
                    <div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedTherapy" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedTherapy' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedTherapy' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedTherapy") && data["M2440ReasonPatientAdmittedTherapy"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedTherapy"><span class="float_left">1 &ndash;</span><span class="margin normal">Therapy services</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedRespite" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedRespite' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedRespite' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedRespite") && data["M2440ReasonPatientAdmittedRespite"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedRespite"><span class="float_left">2 &ndash;</span><span class="margin normal">Respite care</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedHospice" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedHospice' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedHospice' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedHospice") && data["M2440ReasonPatientAdmittedHospice"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedHospice"><span class="float_left">3 &ndash;</span><span class="margin normal">Hospice care</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedPermanent" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedPermanent' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedPermanent' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedPermanent") && data["M2440ReasonPatientAdmittedPermanent"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedPermanent"><span class="float_left">4 &ndash;</span><span class="margin normal">Permanent placement</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedUnsafe" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedUnsafe' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedUnsafe' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedUnsafe") && data["M2440ReasonPatientAdmittedUnsafe"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedUnsafe"><span class="float_left">5 &ndash;</span><span class="margin normal">Unsafe for care at home</span></label>
                    </div><div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedOther" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedOther' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedOther' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedOther") && data["M2440ReasonPatientAdmittedOther"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedOther"><span class="float_left">6 &ndash;</span><span class="margin normal">Other</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2440');">?</div>
                        </div>
                        <input name="TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown' class='radio float_left' name='TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown' type='checkbox' value='1' {0} />", data.ContainsKey("M2440ReasonPatientAdmittedUnknown") && data["M2440ReasonPatientAdmittedUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                        <label for="TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown"><span class="float_left">UK</span><span class="margin normal">Unknown</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M0903 &amp; M0906</legend>
        <div class="column">
            <div class="row">
                <label for="TransferInPatientDischargedDeath_M0903LastHomeVisitDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0903');">(M0903)</a> Date of Last (Most Recent) Home Visit:</label>
                <div class="float_right">
                    <%=Html.TextBox("TransferInPatientDischargedDeath_M0903LastHomeVisitDate", data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "", new { @id = "TransferInPatientDischargedDeath_M0903LastHomeVisitDate" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0903');">?</div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="TransferInPatientDischargedDeath_M0906DischargeDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0906');">(M0906)</a> Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.</label>
                <div class="float_right">
                    <%=Html.TextBox("TransferInPatientDischargedDeath_M0906DischargeDate", data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : "", new { @id = "TransferInPatientDischargedDeath_M0906DischargeDate" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0906');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"TransferForDischarge.FormSubmit($(this),{0});\">Save &amp; Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','TransferInPatientDischarged');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
            <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfRadioEquals("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "02|03|04", $("#TransferInPatientDischarged_M2430"));
    Oasis.hideIfRadioEquals("TransferInPatientDischarged_M2410TypeOfInpatientFacility", "02|04", $("#TransferInPatientDischarged_M2440"));
    Oasis.noneOfTheAbove($("#TransferInPatientDischarged_M2430ReasonForHospitalizationUK"), $("#TransferInPatientDischarged_M2430 input"));
    Oasis.noneOfTheAbove($("#TransferInPatientDischarged_M2440ReasonPatientAdmittedUnknown"), $("#TransferInPatientDischarged_M2440 input"));
</script>