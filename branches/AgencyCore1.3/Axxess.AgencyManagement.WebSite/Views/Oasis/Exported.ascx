﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Of Exported Oasis | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','oasisExported');</script>")%>
<% using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = "generateExportedOasis", @style = "width:100%; height:100%;"})) { %>
<table style="width: 100%; height: 100%;">
    <tbody>
        <tr>
            <td  style="height:25px;">
                <div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="GenerateExportedSubmit($(this));">Generate Selected</a></li></ul></div>
            </td>
        </tr>
        <tr>
            <td>
                <% Html.Telerik().Grid<Assessment>()
        .Name("exportedOasisGrid")
        .Columns(columns =>
        {
            columns.Bound(o => o.Id)
                   .Template(o =>
            {
                %>
                <%= string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", o.Identifier)%>
                <%
                    }).ClientTemplate("<input name='OasisSelected' type='checkbox' value='<#= Identifier #>'/>")
                   .Title("Check")
                   .Width(50)
                   .HtmlAttributes(new { style = "text-align:center" }).Sortable(false);

            columns.Bound(o => o.PatientName);
            columns.Bound(o => o.TypeDescription);
            columns.Bound(o => o.Modified).Format("{0:MM/dd/yyyy}").Title("Assessment Date");
            columns.Bound(o => o.Insurance);
            columns.Bound(o => o.Insurance).Title("Action").Template(o =>
            {
                %>
                <%= string.Format("<a href=\"javascript:void(0)\" name=\"\"  onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');\" >Reopen</a>", o.Id, o.PatientId, o.EpisodeId, o.TypeName, "ReOpen")%>
                <%
                    }).ClientTemplate("<a href=\"javascript:void(0)\" name=\"\"  onclick=\"Oasis.Reopen('<#= Id#>','<#= PatientId#>','<#= EpisodeId#>','<#= TypeName#>','ReOpen');\" >Reopen</a>").Sortable(false);
        })
        .DataBinding(dataBinding => dataBinding.Ajax()
                .Select("Exported", "Oasis"))
                .Scrollable()
                .Sortable()
                .Render();
                %>
            </td>
        </tr>
      
    </tbody>
</table>
<%} %>

<div id="oasisReopenDialog" class="hidden">
    <fieldset> 
        <div class="wide_column">
            <div class="row">
                <label for="">
                   This OASIS assessment has been exported to the state already . <br /> You will have to re-export the assessment after key changes are made.
                  <br /> <br /> <b>Are you sure you want to reopen the assessment? </b></label><div class="float_right">
                       </div>
            </div>
            <div  class="row">
             <%= Html.TextArea("Oasis_OasisReopenReason", "", 5, 70, new { @id = "Oasis_OasisReopenReason" })%>
            </div>
        </div>
        <div class="buttons"><ul>
            <li><a href="javascript:void(0);" id="reopenOasisYes">Yes</a></li>
            <li><a href="javascript:void(0);" onclick="U.closeDialog();">No</a></li>
        </ul></div>
    </fieldset>
</div>


<script type="text/javascript">
    $("#window_oasisExported .t-group-indicator").hide();
    $("#window_oasisExported .t-grouping-header").remove();
    $("#window_oasisExported .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    $("#window_oasisExported .t-grid").css({ 'top': '30px', 'bottom': '0px' });
    function GenerateExportedSubmit(control) {
        var check = false;
        $("#generateExportedOasis input[name=OasisSelected]").each(function() { if ($(this).is(":checked")) { check = true; } })
        if (check) { $(control).closest('form').submit(); } else { $.jGrowl("Select an assessment to generate.", { theme: 'error', life: 5000 }); }
    }
</script>

