﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1700) Cognitive Functioning: Patient’s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.",true) +
        printview.checkbox("0 &ndash; Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1710) When Confused (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; Never",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; In new or complex situations only",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; On awakening or at night only",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; During the day and evening, but not constantly",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Constantly",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient nonresponsive",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1720) When Anxious (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; None of the time",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less often than daily",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Daily, but not constantly",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; All of the time",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient nonresponsive",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): (Mark all that apply.)",true) +
        printview.checkbox("1 &ndash; Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Delusional, hallucinatory, or paranoid behavior",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("7 &ndash; None of the above behaviors demonstrated",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; Never",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less than once a month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Once a month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Several times each month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Several times a week",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; At least daily",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1750) Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? "true" : "false"%>)));
</script>