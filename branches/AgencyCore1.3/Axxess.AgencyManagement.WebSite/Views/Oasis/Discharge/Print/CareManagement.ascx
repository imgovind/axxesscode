﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable%3E%3Ctr%3E%3Cth colspan=%227%22%3E" +
        printview.span("(M2100) Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed. (Check only one box in each row.)",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3E" +
        printview.span("Type of Assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("No assistance needed in this area",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) currently provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) need training/ supportive services to provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Caregiver(s) not likely to provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Unclear if Caregiver(s) will provide assistance",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Assistance needed, but no Caregiver(s) available",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Medication administration (e.g., oral, inhaled or injectable)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Medical procedures/ treatments (e.g., changing wound dressing)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Supervision and safety (e.g., due to cognitive impairment)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("g. Advocacy or facilitation of patient’s participation in appropriate medical care (includes transporta-tion to or from appointments)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "04" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "05" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/table%3E");
    printview.addsection(
        printview.span("(M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?",true) +
        printview.col(3,
            printview.checkbox("1 &ndash; At least daily",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Three or more times per week",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; One to two times per week",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Received, but less often than weekly",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; No assistance received",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "05" ? "true" : "false"%>)));
</script>