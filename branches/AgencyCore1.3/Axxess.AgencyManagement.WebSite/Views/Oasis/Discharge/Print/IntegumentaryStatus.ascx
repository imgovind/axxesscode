﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "01" ? "true" : "false"%>)));<%
    if (data == null || data.ContainsKey("M1306UnhealedPressureUlcers") == false || data["M1306UnhealedPressureUlcers"].Answer != "00") { %>
    printview.addsection(
        printview.span("(M1307) The Oldest Non-epithelialized Stage II Pressure Ulcer that is present at discharge",true) +
        printview.checkbox("1 &ndash; Was present at the most recent SOC/ROC assessment",<%= data != null && data.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && data["M1307NonEpithelializedStageTwoUlcerDate"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Developed since the most recent SOC/ROC assessment: record date pressure ulcer first identified: <%= data != null && data.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && data["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsNotNullOrEmpty() ? data["M1307NonEpithelializedStageTwoUlcerDate"].Answer : "<span class='blank'></span>" %>",<%= data != null && data.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && data["M1307NonEpithelializedStageTwoUlcerDate"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("NA &ndash; No non-epithelialized Stage II pressure ulcers are present at discharge",<%= data != null && data.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && data["M1307NonEpithelializedStageTwoUlcerDate"].Answer == "NA" ? "true" : "false"%>));
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%226%22%3E" +
        printview.span("(M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage:",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%224%22%3E" +
        printview.span("Stage Description&mdash; Unhealed Pressure Ulcer",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 1",true) +
        printview.span("Number Currently Present") +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 2",true) +
        printview.span("Number of those listed in Column 1 that were present on admission") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") && data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") && data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") && data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") && data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or device.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.3 Unstageable: Suspected deep tissue injury in evolution.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : ""%>") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");<%
    } %>
    printview.addsection(
        printview.span("(M1310) Pressure Ulcer Length: Longest length &ldquo;head-to-toe&rdquo;",true) +
        printview.span("<%= data != null && data.ContainsKey("M1310PressureUlcerLength") && data["M1310PressureUlcerLength"].Answer.IsNotNullOrEmpty() ? data["M1310PressureUlcerLength"].Answer : "<span class='blank'></span>" %>.<%= data != null && data.ContainsKey("M1310PressureUlcerLengthDecimal") && data["M1310PressureUlcerLengthDecimal"].Answer.IsNotNullOrEmpty() ? data["M1310PressureUlcerLengthDecimal"].Answer : "<span class='blank'></span>" %>cm",false));
    printview.addsection(
        printview.span("(M1312) Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular to the length",true) +
        printview.span("<%= data != null && data.ContainsKey("M1312PressureUlcerWidth") && data["M1312PressureUlcerWidth"].Answer.IsNotNullOrEmpty() ? data["M1312PressureUlcerWidth"].Answer : "<span class='blank'></span>" %>.<%= data != null && data.ContainsKey("M1312PressureUlcerWidthDecimal") && data["M1312PressureUlcerWidthDecimal"].Answer.IsNotNullOrEmpty() ? data["M1312PressureUlcerWidthDecimal"].Answer : "<span class='blank'></span>" %>cm",false));
    printview.addsection(
        printview.span("(M1314) Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface to the deepest area",true) +
        printview.span("<%= data != null && data.ContainsKey("M1314PressureUlcerDepth") && data["M1314PressureUlcerDepth"].Answer.IsNotNullOrEmpty() ? data["M1314PressureUlcerDepth"].Answer : "<span class='blank'></span>" %>.<%= data != null && data.ContainsKey("M1314PressureUlcerDepthDecimal") && data["M1314PressureUlcerDepthDecimal"].Answer.IsNotNullOrEmpty() ? data["M1314PressureUlcerDepthDecimal"].Answer : "<span class='blank'></span>" %>cm",false));
    printview.addsection(
        printview.span("(M1320) Status of Most Problematic (Observable) Pressure Ulcer",true) +
        printview.col(5,
            printview.checkbox("0 &ndash; Newly epithelialized",<%= data != null && data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Fully granulating",<%= data != null && data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Early/partial granulation",<%= data != null && data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Not healing",<%= data != null && data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; No observable pressure ulcer",<%= data != null && data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.",true) +
        printview.col(5,
            printview.checkbox("0 &ndash; Zero",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; One",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Two",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Three",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Four or More",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Stage I",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Stage II",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Stage III",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Stage IV",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? "true" : "false"%>)) +
        printview.checkbox("NA &ndash; No observable pressure ulcer or unhealed pressure ulcer",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1330) Does this patient have a Stasis Ulcer?",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes, patient has BOTH observable and unobservable stasis ulcers",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Yes, patient has observable stasis ulcers ONLY",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1332) Current Number of (Observable) Stasis Ulcer(s)",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; One",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Two",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Three",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Four or more",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1334) Status of Most Problematic (Observable) Stasis Ulcer",true) +
        printview.col(4,
            printview.checkbox("0 &ndash; Newly epithelialized",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Fully granulating",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Early/partial granulation",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Not healing",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1340) Does this patient have a Surgical Wound? ",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes, patient has at least one (observable) surgical wound",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Surgical wound known but not observable due to non-removable dressing",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? "true" : "false"%>));<%
    if (data == null || data.ContainsKey("M1340SurgicalWound") == false || data["M1340SurgicalWound"].Answer != "00") { %>
    printview.addsection(
        printview.span("(M1342) Status of Most Problematic (Observable) Surgical Wound ",true) +
        printview.col(4,
            printview.checkbox("0 &ndash; Newly epithelialized",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Fully granulating",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Early/partial granulation",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Not healing",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? "true" : "false"%>)));<%
    } %>
    printview.addsection(
        printview.span("(M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "01" ? "true" : "false"%>)));
</script>