﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1242) Frequency of Pain Interfering with patient&rsquo;s activity or movement",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; Patient has no pain",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Patient has pain that does not interfere with activity or movement",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Less often than daily",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Daily, but not constantly",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; All of the time",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? "true" : "false"%>)));
</script>