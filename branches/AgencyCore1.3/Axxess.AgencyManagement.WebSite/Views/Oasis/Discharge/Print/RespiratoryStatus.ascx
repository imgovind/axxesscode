﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1400) When is the patient dyspneic or noticeably Short of Breath?",true) +
        printview.checkbox("0 &ndash; Patient is not short of breath",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; When walking more than 20 feet, climbing stairs",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; At rest (during day or night)",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1410) Respiratory Treatments utilized at home:",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Oxygen (intermittent or continuous)",<%= data != null && data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Ventilator (continually or at night)",<%= data != null && data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Continuous/ Bi-level positive airway pressure",<%= data != null && data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; None of the above",<%= data != null && data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ? "true" : "false"%>)));
</script>