﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?",true) +
        printview.col(4,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Not assessed",<%= data != null && data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient does not have diagnosis of heart failure",<%= data != null && data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "NA" ? "true" : "false"%>)));
<% if (data == null || (data.ContainsKey("M1500HeartFailureSymptons") && (data["M1500HeartFailureSymptons"].Answer.IsNullOrEmpty() || data["M1500HeartFailureSymptons"].Answer == "01"))) { %>
    printview.addsection(
        printview.span("(M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)",true) +
        printview.checkbox("0 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupPhysicianCon") && data["M1510HeartFailureFollowupPhysicianCon"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupAdvisedEmg") && data["M1510HeartFailureFollowupAdvisedEmg"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupParameters") && data["M1510HeartFailureFollowupParameters"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupInterventions") && data["M1510HeartFailureFollowupInterventions"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; ",<%= data != null && data.ContainsKey("M1510HeartFailureFollowupChange") && data["M1510HeartFailureFollowupChange"].Answer == "1" ? "true" : "false"%>));
<% } %>
</script>