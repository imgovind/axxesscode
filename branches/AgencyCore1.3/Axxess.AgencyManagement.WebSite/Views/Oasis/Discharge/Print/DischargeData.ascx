﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("(M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("Plan/ Intervention",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("No",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("Yes",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("NA &ndash; Not Applicable",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Patient is not diabetic or is bilateral amputee",<%= data != null && data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Falls prevention interventions") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Intervention(s) to monitor and mitigate pain") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment did not indicate pain since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Intervention(s) to prevent pressure ulcers") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment",<%= data != null && data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Pressure ulcer treatment based on principles of moist wound healing") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &ndash; Dressings that support the principles of moist wound healing not indicated for this patient&rsquo;s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing",<%= data != null && data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/table%3E");
    printview.addsection(
        printview.span("(M2410) To which Inpatient Facility has the patient been admitted?",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Hospital",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Rehabilitation facility",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Nursing home",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Hospice",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; No inpatient facility admission",<%= data != null && data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M2420) Discharge Disposition: Where is the patient after discharge from your agency? (Choose only one answer.)",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Patient remained in the community (without formal assistive services)",<%= data != null && data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Patient remained in the community (with formal assistive services)",<%= data != null && data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Patient transferred to a non-institutional hospice",<%= data != null && data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Unknown because patient moved to a geographic location not served by this agency",<%= data != null && data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Other unknown",<%= data != null && data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "UK" ? "true" : "false"%>)));
    printview.addsection(
        printview.col(2,
            printview.span("(M0903) Date of Last (Most Recent) Home Visit:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0903LastHomeVisitDate") && data["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty() ? data["M0903LastHomeVisitDate"].Answer : ""%>",false,1) +
            printview.span("(M0906) Discharge/Transfer/Death Date:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0906DischargeDate") && data["M0906DischargeDate"].Answer.IsNotNullOrEmpty() ? data["M0906DischargeDate"].Answer : ""%>",false,1)));
</script>