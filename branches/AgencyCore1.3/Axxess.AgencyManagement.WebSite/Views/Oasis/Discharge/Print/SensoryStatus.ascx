﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1230) Speech and Oral (Verbal) Expression of Language (in patient&rsquo;s own language)",true) +
        printview.checkbox("0 &ndash; Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Patient nonresponsive or unable to speak.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? "true" : "false"%>));
</script>