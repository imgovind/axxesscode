﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyBehaviourialForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Behavioral")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1700');">(M1700)</a> Cognitive Functioning: Patient&rsquo;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.</label>
                <%=Html.Hidden("DischargeFromAgency_M1700CognitiveFunctioning")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "00", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1700CognitiveFunctioning0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1700CognitiveFunctioning0"><span class="float_left">0 &ndash;</span><span class="normal margin">Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "01", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1700CognitiveFunctioning1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1700CognitiveFunctioning1"><span class="float_left">1 &ndash;</span><span class="normal margin">Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "02", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1700CognitiveFunctioning2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1700CognitiveFunctioning2"><span class="float_left">2 &ndash;</span><span class="normal margin">Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "03", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1700CognitiveFunctioning3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1700CognitiveFunctioning3"><span class="float_left">3 &ndash;</span><span class="normal margin">Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1700');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1700CognitiveFunctioning", "04", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1700CognitiveFunctioning4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1700CognitiveFunctioning4"><span class="float_left">4 &ndash;</span><span class="normal margin">Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1710');">(M1710)</a> When Confused (Reported or Observed Within the Last 14 Days):</label>
                <%=Html.Hidden("DischargeFromAgency_M1710WhenConfused")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "00", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfused0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfused0"><span class="float_left">0 &ndash;</span><span class="normal margin">Never</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "01", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfused1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfused1"><span class="float_left">1 &ndash;</span><span class="normal margin">In new or complex situations only</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "02", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfused2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfused2"><span class="float_left">2 &ndash;</span><span class="normal margin">On awakening or at night only</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "03", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfused3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfused3"><span class="float_left">3 &ndash;</span><span class="normal margin">During the day and evening, but not constantly</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "04", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfused4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfused4"><span class="float_left">4 &ndash;</span><span class="normal margin">Constantly</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1710');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1710WhenConfused", "NA", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M1710WhenConfusedNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1710WhenConfusedNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient nonresponsive</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1720');">(M1720)</a> When Anxious (Reported or Observed Within the Last 14 Days):</label>
                <%=Html.Hidden("DischargeFromAgency_M1720WhenAnxious")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "00", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1720WhenAnxious0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1720WhenAnxious0"><span class="float_left">0 &ndash;</span><span class="normal margin">None of the time</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "01", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1720WhenAnxious1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1720WhenAnxious1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less often than daily</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "02", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1720WhenAnxious2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1720WhenAnxious2"><span class="float_left">2 &ndash;</span><span class="normal margin">Daily, but not constantly</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "03", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1720WhenAnxious3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1720WhenAnxious3"><span class="float_left">3 &ndash;</span><span class="normal margin">All of the time</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1720');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1720WhenAnxious", "NA", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M1720WhenAnxiousNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1720WhenAnxiousNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient nonresponsive</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1740');">(M1740)</a> Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): (Mark all that apply.)</label>
                <div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"><span class="float_left">1 &ndash;</span><span class="normal margin">Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required</span></label>
                </div><div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"><span class="float_left">2 &ndash;</span><span class="normal margin">Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions</span></label>
                </div><div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"><span class="float_left">3 &ndash;</span><span class="normal margin">Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.</span></label>
                </div><div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"><span class="float_left">4 &ndash;</span><span class="normal margin">Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)</span></label>
                </div><div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB"><span class="float_left">5 &ndash;</span><span class="normal margin">Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)</span></label>
                </div><div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"><span class="float_left">6 &ndash;</span><span class="normal margin">Delusional, hallucinatory, or paranoid behavior</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1740');">?</div>
                    </div>
                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone" value="" type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone' class='radio float_left M1740' name='DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"><span class="float_left">7 &ndash;</span><span class="normal margin">None of the above behaviors demonstrated</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1745');">(M1745)</a> Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.</label>
                <%=Html.Hidden("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency0", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Never</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency1", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once a month</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency2", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">Once a month</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency3", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Several times each month</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency4", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">Several times a week</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1745');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency5", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">At least daily</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1750');">(M1750)</a> Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?</label>
                <%=Html.Hidden("DischargeFromAgency_M1750PsychiatricNursingServicing")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1750PsychiatricNursingServicing", "0", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? true : false, new { @id = "M1750PsychiatricNursingServicing0", @class = "radio float_left" })%>
                    <label for="M1750PsychiatricNursingServicing0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1750');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1750PsychiatricNursingServicing", "1", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? true : false, new { @id = "M1750PsychiatricNursingServicing1", @class = "radio float_left" })%>
                    <label for="M1750PsychiatricNursingServicing1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Discharge.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','DischargeFromAgency');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.noneOfTheAbove($("#DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"), $("#behaviourialstatus_discharge .M1740"));
</script>