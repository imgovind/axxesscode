﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencySensoryStatusForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Sensory")%>
<div class="wrapper main">
<fieldset class="oasis">
    <legend>OASIS</legend>
    <div class="wide_column">
        <div class="row">
            <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1230');">(M1230)</a> Speech and Oral (Verbal) Expression of Language (in patient's own language)</label><%= Html.Hidden("DischargeFromAgency_M1230SpeechAndOral", " ", new { @id = "" }) %>
            <div class="margin">
                <div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "00", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral00", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral00"><span class="float_left">0 &ndash;</span><span class="normal margin">Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "01", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral01", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral01"><span class="float_left">1 &ndash;</span><span class="normal margin">Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "02", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral02", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral02"><span class="float_left">2 &ndash;</span><span class="normal margin">Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "03", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral03", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral03"><span class="float_left">3 &ndash;</span><span class="normal margin">Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases</span>.</label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "04", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral04", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral04"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).</span></label></div>
                <div><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M1230');">?</div></div><%= Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "05", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1230SpeechAndOral05", @class = "radio float_left" })%><label for="DischargeFromAgency_M1230SpeechAndOral05"><span class="float_left">5 &ndash;</span><span class="normal margin">Patient nonresponsive or unable to speak.</span></label></div>
            </div>
        </div>
    </div>
</fieldset>
<div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Discharge.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','DischargeFromAgency');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>