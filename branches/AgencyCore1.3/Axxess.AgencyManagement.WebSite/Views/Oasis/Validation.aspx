﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ValidationInfoViewData>" %><%
var dictonary = new Dictionary<string, string>() {
    { AssessmentType.StartOfCare.ToString(), "startofcare" },
    { AssessmentType.Recertification.ToString(), "recertification" },
    { AssessmentType.ResumptionOfCare.ToString(), "resumptionofcare" },
    { AssessmentType.FollowUp.ToString(), "followup" },
    { AssessmentType.DischargeFromAgency.ToString(), "discharge" },
    { AssessmentType.DischargeFromAgencyDeath.ToString(), "deathathome" },
    { AssessmentType.TransferInPatientDischarged.ToString(), "transferfordischarge" },
    { AssessmentType.TransferInPatientNotDischarged.ToString(), "transfernotdischarge" } }; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>OASIS-C Validation</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("validation.css").Combined(true).Compress(true)) %>
    <% Html.Telerik().ScriptRegistrar().Globalization(true).DefaultGroup(group => group.Add("jquery-1.4.2.min.js").Compress(true).Combined(true).CacheDurationInDays(5)).Render(); %>
</head>
<body>
    <div class="wrapper main">
        <div class="title">You have <strong><%=Model.ValidationError.Where(e => e.ErrorType == "WARNING").Count()%> warning<%= Model.ValidationError.Where(e => e.ErrorType == "WARNING").Count() != 1 ? "s" : "" %></strong> and <strong><%= Model.ValidationError.Where(e => e.ErrorType == "ERROR").Count() %> error<%= Model.ValidationError.Where(e => e.ErrorType == "ERROR").Count() != 1 ? "s" : "" %></strong>.</div><%
if (Model.ValidationError != null) {
    foreach (var data in Model.ValidationError) { %>
        <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"window.parent.Oasis.gotoQuestion('{0}','{1}'); window.parent.UserInterface.CloseOasisValidationModal();\"{2}>",data.ErrorDup.Substring(0,5),Model.AssessmentType.ToLower(),data.ErrorType == "FATAL" ? " class='red'" : "") %>
            <div>
                <%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<img src='/Images/icons/error.png'>" : "<img src='/Images/icons/warning.png'>"%>
                <span><%= data.ErrorDup %></span>
                <span class="description"><%= data.Description %></span>
            </div>
        </a><%
    }
}
if (Model.ValidationError != null && Model.ValidationError.Where(e => e.ErrorType == "ERROR").Count() == 0) { %>
        <div class="hipps">
            <span><label>HIPPS Code:</label><strong><%= Model.HIPPSCODE %></strong></span>
            <span><label>OASIS Claim Matching Key:</label><strong><%= Model.HIPPSKEY %></strong></span>
        </div>
        <script type="text/javascript"> $("#oasis_validation_controls ul", window.parent.document).html(unescape("%3Cli%3E<%= string.Format("%3Ca href=%22javascript:void(0);%22 onclick='Oasis.OasisStatusAction(%22{0}%22,%22{1}%22,%22{2}%22,%22{3}%22,%22Submit%22,%22{4}%22);'%3EFinish%3C/a%3E", Model.AssessmentId, Model.PatientId, Model.EpisodeId, Model.AssessmentType, dictonary[Model.AssessmentType])%>%3C/li%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.CloseOasisValidationModal();%22%3ECancel%3C/a%3E%3C/li%3E" )); </script><%
} %>
    </div>
</body>
</html>