﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("(M0020) Patient ID Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0020PatientIdNumber") && data["M0020PatientIdNumber"].Answer.IsNotNullOrEmpty() ? data["M0020PatientIdNumber"].Answer : ""%>",false,1) +
            printview.span("(M0040) Patient Name:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : ""%><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : ""%><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : ""%><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : ""%>",false,1) +
            printview.span("(M0050) State:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0050PatientState") && data["M0050PatientState"].Answer.IsNotNullOrEmpty() ? data["M0050PatientState"].Answer : ""%>",false,1) +
            printview.span("(M0060) Zip:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0060PatientZipCode") && data["M0060PatientZipCode"].Answer.IsNotNullOrEmpty() ? data["M0060PatientZipCode"].Answer : ""%>",false,1) +
            printview.span("(M0069) Gender:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "1" ? "Male" : "" %><%= data != null && data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "2" ? "Female" : "" %>",false,1) +
            printview.span("(M0064) Social Security Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "UK &ndash; Unknown" : (data != null && data.ContainsKey("M0064PatientSSN") && data["M0064PatientSSN"].Answer.IsNotNullOrEmpty() ? data["M0064PatientSSN"].Answer : "" )%>",false,1) +
            printview.span("(M0063) Medicare Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "NA &ndash; No Medicare" : (data != null && data.ContainsKey("M0063PatientMedicareNumber") && data["M0063PatientMedicareNumber"].Answer.IsNotNullOrEmpty() ? data["M0063PatientMedicareNumber"].Answer : "")%>",false,1) +
            printview.span("(M0065) Medicaid Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "NA &ndash; No Medicaid" : (data != null && data.ContainsKey("M0065PatientMedicaidNumber") && data["M0065PatientMedicaidNumber"].Answer.IsNotNullOrEmpty() ? data["M0065PatientMedicaidNumber"].Answer : "")%>",false,1) +
            printview.span("(M0066) Birth Date:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0066PatientDoB") && data["M0066PatientDoB"].Answer.IsNotNullOrEmpty() ? data["M0066PatientDoB"].Answer : ""%>",false,1) +
            printview.span("(M0140) Race/Ethnicity (Below):",true)),
        "Patient Information");
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("(M0030) Start of Care Date:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0030SocDate") && data["M0030SocDate"].Answer.IsNotNullOrEmpty() ? data["M0030SocDate"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Episode Start Date:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericEpisodeStartDate") && data["GenericEpisodeStartDate"].Answer.IsNotNullOrEmpty() ? data["GenericEpisodeStartDate"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("(M0032) Resumption of Care:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "NA &ndash; Not Applicable" : (data != null && data.ContainsKey("M0032ROCDate") && data["M0032ROCDate"].Answer.IsNotNullOrEmpty() ? data["M0032ROCDate"].Answer : "")%>",false,1)) +
            printview.col(2,
                printview.span("(M0090) Date Completed:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0090AssessmentCompleted") && data["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() ? data["M0090AssessmentCompleted"].Answer : ""%>",false,1)) +
            printview.span("<strong>(M0080) Discipline of Person Completing Assessment:</strong> <%= data != null && data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer.IsNotNullOrEmpty() ? (data["M0080DisciplinePerson"].Answer == "01" ? "RN" : "") + (data["M0080DisciplinePerson"].Answer == "02" ? "PT" : "") + (data["M0080DisciplinePerson"].Answer == "03" ? "SLP/ST" : "") + (data["M0080DisciplinePerson"].Answer == "04" ? "OT" : "") : "<span class='blank'></span>" %>") +
            printview.span("<strong>(M0010) CMS Certification Number:</strong> <%= data != null && data.ContainsKey("M0010CertificationNumber") && data["M0010CertificationNumber"].Answer.IsNotNullOrEmpty() ? data["M0010CertificationNumber"].Answer : "<span class='blank'></span>" %>") +
            printview.col(2,
                printview.span("(M0014) Branch State:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0014BranchState") && data["M0014BranchState"].Answer.IsNotNullOrEmpty() ? data["M0014BranchState"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("(M0016) Branch ID Number:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0016BranchId") && data["M0016BranchId"].Answer.IsNotNullOrEmpty() ? data["M0016BranchId"].Answer : ""%>",false,1)) +
            printview.span("(M0018) National Provider Identifier (NPI):",true) +
            printview.span("<%= data != null && data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1" ? "UK &ndash; Unknown" : (data != null && data.ContainsKey("M0018NationalProviderId") && data["M0018NationalProviderId"].Answer.IsNotNullOrEmpty() ? data["M0018NationalProviderId"].Answer : "<span class='blank'></span>")%>",false,1) +
            printview.span("(M0100) Reason for this Assessment:",true) +
            printview.span("8 &ndash; Death at Home.")),
        "Episode Information");
</script>