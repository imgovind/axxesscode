﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("(M0903) Date of Last (Most Recent) Home Visit:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0903LastHomeVisitDate") && data["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty() ? data["M0903LastHomeVisitDate"].Answer : ""%>",false,1) +
            printview.span("(M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0906DischargeDate") && data["M0906DischargeDate"].Answer.IsNotNullOrEmpty() ? data["M0906DischargeDate"].Answer : ""%>",false,1)),
        "Death Information");
</script>