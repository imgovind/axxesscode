﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %><%
var data = Model.ToDictionary();
var agency = Model.AgencyData != null ? Model.AgencyData.ToObject<Agency>() : new Agency(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : ""%>OASIS-C Death<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? " | " + data["M0040LastName"].Answer : ""%><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : ""%><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : ""%><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : ""%></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
    <% Html.Telerik().ScriptRegistrar().Globalization(true).DefaultGroup(group => group.Add("jquery-1.4.2.min.js").Add("/Modules/printview.js").Compress(true).Combined(true).CacheDurationInDays(5)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= agency.MainLocation != null && agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EOASIS-C%3Cbr /%3EDeath%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : ""%><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : ""%><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : ""%><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : ""%>' +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssessment Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.AssessmentDate != null ?  Model.AssessmentDate.ToShortDateString() : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : ""%>' +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= agency.MainLocation != null && agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= agency.MainLocation != null && agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EOASIS-C%3Cbr /%3EDeath%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : ""%><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : ""%><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : ""%><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : ""%>' +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
</script>
<% Html.RenderPartial("~/Views/Oasis/Death/Print/Demographics.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Death/Print/DeathData.ascx", Model); %>
</body>
</html>