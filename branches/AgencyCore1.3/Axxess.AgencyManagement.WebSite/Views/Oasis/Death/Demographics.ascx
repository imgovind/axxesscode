﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyDeathDemographicsForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgencyDeath_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgencyDeath_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgencyDeath_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgencyDeath_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgencyDeath")%>
<%= Html.Hidden("categoryType", "Demographics")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>Patient Information</legend>
        <div class="column">
            <div class="row">
                <label for="DischargeFromAgencyDeath_M0020PatientIdNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0020');">(M0020)</a> ID Number:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0020PatientIdNumber", data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0020PatientIdNumber", @maxlength = "15" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0020');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0040FirstName" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> First Name:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0040FirstName", data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0040FirstName"}) %>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0040MI" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> MI:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0040MI", data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0040MI", @class = "mi", @maxlength = "1" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0040LastName" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> Last Name:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0040LastName", data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0040LastName" }) %>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0040Suffix" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> Suffix:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0040Suffix", data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0040Suffix", @class = "mi", @maxlength = "4" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0050PatientState" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0050');">(M0050)</a> State, <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0060');">(M0060)</a> Zip:</label>
                <div class="float_right oasis">
                    <%= Html.LookupSelectList(SelectListTypes.States, "DischargeFromAgencyDeath_M0050PatientState", data.ContainsKey("M0050PatientState") ? data["M0050PatientState"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0050PatientState", @class = "AddressStateCode" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0050');">?</div>
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0060PatientZipCode", data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0060PatientZipCode", @class = "zip numeric", @maxlength = "5" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0060');">?</div>
                </div>
            </div><div class="row">
                <label class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0069');">(M0069)</a> Gender:</label><%= Html.Hidden("DischargeFromAgencyDeath_M0069Gender", " ", new { }) %>
                <div class="float_right oasis">
                    <%= Html.RadioButton("DischargeFromAgencyDeath_M0069Gender", "1", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "1" ? true : false, new { @id = "DischargeFromAgencyDeath_M0069GenderMale", @class = "radio" })%>
                    <label for="DischargeFromAgencyDeath_M0069GenderMale" class="inlineradio">Male</label>
                    <%= Html.RadioButton("DischargeFromAgencyDeath_M0069Gender", "2", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "2" ? true : false, new { @id = "DischargeFromAgencyDeath_M0069GenderFemale", @class = "radio" })%>
                    <label for="DischargeFromAgencyDeath_M0069GenderFemale" class="inlineradio">Female</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0069');">?</div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="DischargeFromAgencyDeath_M0064PatientSSN" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0064');">(M0064)</a> Social Security Number:</label>
                <div class="float_right">
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0064PatientSSN' name='DischargeFromAgencyDeath_M0064PatientSSN' type='text' value='{0}' maxlength='9' />", data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "" : data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : "") %>
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <input type="hidden" name="DischargeFromAgencyDeath_M0064PatientSSNUnknown" value="" />
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0064PatientSSNUnknown' name='DischargeFromAgencyDeath_M0064PatientSSNUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgencyDeath_M0064PatientSSNUnknown">UK &ndash; Unknown or Not Available</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0064');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0063PatientMedicareNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0063');">(M0063)</a> Medicare Number:</label>
                <div class="float_right">
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0063PatientMedicareNumber' name='DischargeFromAgencyDeath_M0063PatientMedicareNumber' type='text' value='{0}' maxlength='11' />", data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "" : data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : "") %>
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <input type="hidden" name="DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown" value="" />
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown' name='DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown" >NA &ndash; No Medicare</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0063');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0065PatientMedicaidNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0065');">(M0065)</a> Medicaid Number:</label>
                <div class="float_right">
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0065PatientMedicaidNumber' name='DischargeFromAgencyDeath_M0065PatientMedicaidNumber' type='text' value='{0}' maxlength='9' />", data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "" : data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : "") %>
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <input type="hidden" name="DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown" value="" />
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown' name='DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown">NA &ndash; No Medicaid</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0065');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0066PatientDoB" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0066');">(M0066)</a> Birth Date:</label>
                <div class="float_right oasis">
                    <%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0066PatientDoB").Value(data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer :"").HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0066PatientDoB", @class = "date" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0066');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Episode Information</legend>
        <div class="column">
            <div class="row">
                <label for="DischargeFromAgencyDeath_M0030SocDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0030');">(M0030)</a> Start of Care Date:</label>
                <div class="float_right oasis">
                    <%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0030SocDate").Value(data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "").HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0030SocDate", @class = "date" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0030');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_GenericEpisodeStartDate" class="float_left">Episode Start Date:</label>
                <div class="float_right">
                    <%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_GenericEpisodeStartDate").Value(data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "").HtmlAttributes(new { @id = "DischargeFromAgencyDeath_GenericEpisodeStartDate", @class = "date" })%>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0032ROCDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0032');">(M0032)</a> Resumption of Care Date:</label>
                <div class="float_right">
                    <%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0032ROCDate").Value(data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "").HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0032ROCDate", @class = "date" })%>
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0032ROCDateNotApplicable' name='DischargeFromAgencyDeath_M0032ROCDateNotApplicable' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgencyDeath_M0032ROCDateNotApplicable">NA &ndash; Not Applicable</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0032');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0090AssessmentCompleted" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0090');">(M0090)</a> Date Assessment Completed:</label>
                <div class="float_right oasis">
                    <%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0090AssessmentCompleted").Value(data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : "").HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0090AssessmentCompleted", @class = "date" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0090');">?</div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="DischargeFromAgencyDeath_M0080DisciplinePerson" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0080');">(M0080)</a> Discipline of Person Completing Assessment:</label>
                <div class="float_right oasis">
                    <%= Html.Hidden("DischargeFromAgencyDeath_M0080DisciplinePerson")%>
                    <select name="DischargeFromAgencyDeath_M0080DisciplinePerson" id="DischargeFromAgencyDeath_M0080DisciplinePerson">
                        <%= string.Format("<option value='01' {0}>1 &ndash; RN</option>", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "01" ? "selected='selected'" : "") %>
                        <%= string.Format("<option value='02' {0}>2 &ndash; PT</option>", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "02" ? "selected='selected'" : "") %>
                        <%= string.Format("<option value='03' {0}>3 &ndash; SLP/ST</option>", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "03" ? "selected='selected'" : "")%>
                        <%= string.Format("<option value='04' {0}>4 &ndash; OT</option>", data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "04" ? "selected='selected'" : "") %>
                    </select>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0080');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0010CertificationNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0010');">(M0010)</a> CMS Certification Number:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0010CertificationNumber", data.ContainsKey("M0010CertificationNumber") ? data["M0010CertificationNumber"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0010CertificationNumber" }) %>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0010');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0014BranchState" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0014');">(M0014)</a> Branch State:</label>
                <div class="float_right oasis">
                    <%= Html.LookupSelectList(SelectListTypes.States, "DischargeFromAgencyDeath_M0014BranchState", data.ContainsKey("M0014BranchState") ? data["M0014BranchState"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0014BranchState", @class = "AddressStateCode" })%>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0014');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0016BranchId" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0016');">(M0016)</a> Branch ID Number:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("DischargeFromAgencyDeath_M0016BranchId", data.ContainsKey("M0016BranchId") ? data["M0016BranchId"].Answer : "", new { @id = "DischargeFromAgencyDeath_M0016BranchId" }) %>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0016');">?</div>
                </div>
            </div><div class="row">
                <label for="DischargeFromAgencyDeath_M0018NationalProviderId" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0018');">(M0018)</a> National Provider Identifier (NPI):</label>
                <div class="float_right oasis">
                    <%= string.Format("<input id='DischargeFromAgencyDeath_M0018NationalProviderId' name='DischargeFromAgencyDeath_M0018NationalProviderId'  type='text' value='{0}' />", data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1" ? "" : data.ContainsKey("M0018NationalProviderId") ? data["M0018NationalProviderId"].Answer : "") %>
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <input type="hidden" name="DischargeFromAgencyDeath_M0018NationalProviderIdUnknown" value=" " />
                    <%= string.Format("<input type='checkbox' id='DischargeFromAgencyDeath_M0018NationalProviderIdUnknown' name='DischargeFromAgencyDeath_M0018NationalProviderIdUnknown' class='radio' value='1' {0} />", data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgencyDeath_M0018NationalProviderIdUnknown">UK &ndash; Unknown or Not Available</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0018');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="DischargeFromAgencyDeath_M0100AssessmentType" value="08" />
    <fieldset class="assessmentType oasis">
        <legend>Assessment Reason</legend>
        <div class="column">
            <div class="row strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0100');">(M0100)</a> Reason for this Assessment:</div>
            <div class="row">
                <div>Start/Resumption of Care:</div>
                <div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType1" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType1"><span class="float_left">1 &ndash;</span><span class="normal margin">Start of care&mdash;further visits planned</span></label>
                </div><div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType3" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType3"><span class="float_left">3 &ndash;</span><span class="normal margin">Resumption of care (after inpatient stay)</span></label>
                </div>
            </div><div class="row">
                <div>Follow-Up:</div>
                <div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType4" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType4"><span class="float_left">4 &ndash;</span><span class="normal margin">Recertification (follow-up) reassessment</span></label>
                </div><div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType5" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType5"><span class="float_left">5 &ndash;</span><span class="normal margin">Other follow-up</span></label>
                </div>
            </div><div class="row">
                <div>Transfer to an Inpatient Facility:</div>
                <div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType6" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType6"><span class="float_left">6 &ndash;</span><span class="normal margin">Transferred to an inpatient facility—patient not discharged from agency</span></label>
                </div><div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType7" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType7"><span class="float_left">7 &ndash;</span><span class="normal margin">Transferred to an inpatient facility—patient discharged from agency</span></label>
                </div>
            </div><div class="row">
                <div>Discharge from Agency &mdash; Not to an Inpatient Facility:</div>
                <div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType8" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" checked="checked" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType8"><span class="float_left">8 &ndash;</span><span class="normal margin">Death at home</span></label>
                </div><div>
                    <input id="DischargeFromAgencyDeath_M0100AssessmentType9" name="DischargeFromAgencyDeath_M0100AssessmentType" type="radio" class="radio float_left" />
                    <label for="DischargeFromAgencyDeath_M0100AssessmentType9"><span class="float_left">9 &ndash;</span><span class="normal margin">Discharge from agency</span></label>
                </div><div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M0100');">?</div>
                </div>
            </div>
        </div>
        <div class="shade"></div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="DeathAtHome.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="DeathAtHome.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"DeathAtHome.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','DischargeFromAgencyDeath');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfChecked($("#DischargeFromAgencyDeath_M0064PatientSSNUnknown"), $("#DischargeFromAgencyDeath_M0064PatientSSN"));
    Oasis.hideIfChecked($("#DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown"), $("#DischargeFromAgencyDeath_M0063PatientMedicareNumber"));
    Oasis.hideIfChecked($("#DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown"), $("#DischargeFromAgencyDeath_M0065PatientMedicaidNumber"));
    Oasis.hideIfChecked($("#DischargeFromAgencyDeath_M0032ROCDateNotApplicable"), $("#DischargeFromAgencyDeath_M0032ROCDate"));
    Oasis.hideIfChecked($("#DischargeFromAgencyDeath_M0018NationalProviderIdUnknown"), $("#DischargeFromAgencyDeath_M0018NationalProviderId"));
</script>