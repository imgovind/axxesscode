﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDeathAtHomeDeathForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgencyDeath_Id",Model.Id)%>
<%= Html.Hidden("DischargeFromAgencyDeath_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgencyDeath_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgencyDeath_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgencyDeath")%>
<fieldset class="oasis">
    <div class="column">
        <div class="row">
            <label for="" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0903');">(M0903)</a> Date of Last (Most Recent) Home Visit:</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0903LastHomeVisitDate").Value(data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Today).HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0903LastHomeVisitDate", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0903');">?</div></div>
        </div>
    </div><div class="column">
        <div class="row">
            <label for="" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0906');">(M0906)</a> Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("DischargeFromAgencyDeath_M0906DischargeDate").Value(data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Today).HtmlAttributes(new { @id = "DischargeFromAgencyDeath_M0906DischargeDate", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0906');">?</div></div>
        </div>
    </div>
</fieldset>
<div class="buttons"><ul>
    <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"DeathAtHome.FormSubmit($(this),{0});\">Save &amp; Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','DischargeFromAgencyDeath');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
    <li><a href="javascript:void(0);" onclick="DeathAtHome.FormSubmit($(this));">Save &amp; Exit</a></li>
</ul></div>
<% } %>