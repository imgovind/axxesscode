﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Resumption Of Care | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','resumptionofcare');</script>")
%>
<div id="rocTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_roc" tooltip="M0010 &ndash; M0150">Clinical Record Items</a></li>
        <li><a href="#patienthistory_roc" tooltip="M1000 &ndash; M1030">Patient History &amp; Diagnoses</a></li>
        <li><a href="#riskassessment_roc" tooltip="M1032 &ndash; M1034">Risk Assessment</a></li>
        <li><a href="#prognosis_roc">Prognosis</a></li>
        <li><a href="#supportiveassistance_roc" tooltip="M1100">Supportive Assistance</a></li>
        <li><a href="#sensorystatus_roc" tooltip="M1200 &ndash; M1230">Sensory Status</a></li>
        <li><a href="#pain_roc" tooltip="M1240 &ndash; M1242">Pain</a></li>
        <li><a href="#integumentarystatus_roc" tooltip="M1300 &ndash; M1350">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_roc" tooltip="M1400 &ndash; M1410">Respiratory Status</a></li>
        <li><a href="#endocrine_roc">Endocrine</a></li>
        <li><a href="#cardiacstatus_roc">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_roc" tooltip="M1600 &ndash; M1630">Elimination Status</a></li>
        <li><a href="#nutrition_roc">Nutrition</a></li>
        <li><a href="#behaviourialstatus_roc" tooltip="M1700 &ndash; M1750">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_roc" tooltip="M1800 &ndash; M1910">ADL/IADLs</a></li>
        <li><a href="#suppliesworksheet_roc">Supplies Worksheet</a></li>
        <li><a href="#medications_roc" tooltip="M2000 &ndash; M2040">Medications</a></li>
        <li><a href="#caremanagement_roc" tooltip="M2100 &ndash; M2110">Care Management</a></li>
        <li><a href="#therapyneed_roc" tooltip="M2200 &ndash; M2250">Therapy Need &amp; Plan Of Care</a></li>
        <li><a href="#ordersdisciplinetreatment_roc">Orders for Discipline and Treatment</a></li>
    </ul>
   <!-- Add validation back in later -->
    <div id="clinicalRecord_roc" class="general"><% Html.RenderPartial("~/Views/Oasis/ResumptionofCare/Demographics.ascx", Model); %></div>
    <div id="patienthistory_roc" class="general loading"></div>
    <div id="riskassessment_roc" class="general loading"></div>
    <div id="prognosis_roc" class="general loading"></div>
    <div id="supportiveassistance_roc" class="general loading"></div>
    <div id="sensorystatus_roc" class="general loading"></div>
    <div id="pain_roc" class="general loading"></div>
    <div id="integumentarystatus_roc" class="general loading"></div>
    <div id="respiratorystatus_roc" class="general loading"></div>
    <div id="endocrine_roc" class="general loading"></div>
    <div id="cardiacstatus_roc" class="general loading"></div>
    <div id="eliminationstatus_roc" class="general loading"></div>
    <div id="nutrition_roc" class="general loading"></div>
    <div id="behaviourialstatus_roc" class="general loading"></div>
    <div id="adl_roc" class="general loading"></div>
    <div id="suppliesworksheet_roc" class="general loading"></div>
    <div id="medications_roc" class="general loading"></div>
    <div id="caremanagement_roc" class="general loading"></div>
    <div id="therapyneed_roc" class="general loading"></div>
    <div id="ordersdisciplinetreatment_roc" class="general loading"></div>
</div>