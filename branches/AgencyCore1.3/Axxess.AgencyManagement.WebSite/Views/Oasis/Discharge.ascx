﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
        "<script type='text/javascript'>acore.renamewindow('Oasis-C Discharge from Agency | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','discharge');</script>")
%>
<div id="dischargeTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_discharge" tooltip="M0010 &ndash; M0150">Clinical Record Items</a></li>
        <li><a href="#riskassessment_discharge" tooltip="M1040 &ndash; M1050">Risk Assessment</a></li>
        <li><a href="#sensorystatus_discharge" tooltip="M1230">Sensory Status</a></li>
        <li><a href="#pain_discharge" tooltip="M1242">Pain</a></li>
        <li><a href="#integumentarystatus_discharge" tooltip="M1306 &ndash; M1350">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_discharge" tooltip="M1400 &ndash; M1410">Respiratory Status</a></li>
        <li><a href="#cardiacstatus_discharge" tooltip="M1500 &ndash; M1510">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_discharge" tooltip="M1600 &ndash; M1620">Elimination Status</a></li>
        <li><a href="#behaviourialstatus_discharge" tooltip="M1700 &ndash; M1750">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_discharge" tooltip="M1800 &ndash; M1890">ADL/IADLs</a></li>
        <li><a href="#medications_discharge" tooltip="M2004 &ndash; M2030">Medications</a></li>
        <li><a href="#caremanagement_discharge" tooltip="M2100 &ndash; M2110">Care Management</a></li>
        <li><a href="#emergentcare_discharge" tooltip="M2300 &ndash; M2310">Emergent Care</a></li>
        <li><a href="#dischargeAdd_discharge" tooltip="M0903 &ndash; M0906<br />M2400 &ndash; M2420">Discharge</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_discharge" class="general"><% Html.RenderPartial("~/Views/Oasis/Discharge/Demographics.ascx", Model); %></div>
    <div id="riskassessment_discharge" class="general loading"></div>
    <div id="sensorystatus_discharge" class="general loading"></div>
    <div id="pain_discharge" class="general loading"></div>
    <div id="integumentarystatus_discharge" class="general loading"></div>
    <div id="respiratorystatus_discharge" class="general loading"></div>
    <div id="cardiacstatus_discharge" class="general loading"></div>
    <div id="eliminationstatus_discharge" class="general loading"></div>
    <div id="behaviourialstatus_discharge" class="general loading"></div>
    <div id="adl_discharge" class="general loading"></div>
    <div id="medications_discharge" class="general loading"></div>
    <div id="caremanagement_discharge" class="general loading"></div>
    <div id="emergentcare_discharge" class="general loading"></div>
    <div id="dischargeAdd_discharge" class="general loading"></div>
</div>