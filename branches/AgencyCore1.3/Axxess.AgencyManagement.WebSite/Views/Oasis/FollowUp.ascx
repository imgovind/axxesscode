﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
        "<script type='text/javascript'>acore.renamewindow('Oasis-C Follow-up | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','followup');</script>")
%>
<div id="followupTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_followUp" tooltip="M0010 &ndash; M0150">Clinical Record Items</a></li>
        <li><a href="#patienthistory_followUp" tooltip="M1020 &ndash; M1030">Patient History &amp; Diagnoses</a></li>
        <li><a href="#sensorystatus_followUp" tooltip="M1200">Sensory Status</a></li>
        <li><a href="#pain_followUp" tooltip="M1242">Pain</a></li>
        <li><a href="#integumentarystatus_followUp" tooltip="M1306 &ndash; M1350">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_followUp" tooltip="M1400">Respiratory Status</a></li>
        <li><a href="#eliminationstatus_followUp" tooltip="M1610 &ndash; M1630">Elimination Status</a></li>
        <li><a href="#adl_followUp" tooltip="M1810 &ndash; M1860">ADL/IADLs</a></li>
        <li><a href="#medications_followUp" tooltip="M2030">Medications</a></li>
        <li><a href="#therapyneed_followUp" tooltip="M2200">Therapy Need &amp; Plan Of Care</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_followUp" class="general"><% Html.RenderPartial("~/Views/Oasis/Followup/Demographics.ascx", Model); %></div>
    <div id="patienthistory_followUp" class="general loading"></div>
    <div id="sensorystatus_followUp" class="general loading"></div>
    <div id="pain_followUp" class="general loading"></div>
    <div id="integumentarystatus_followUp" class="general loading"></div>
    <div id="respiratorystatus_followUp" class="general loading"></div>
    <div id="eliminationstatus_followUp" class="general loading"></div>
    <div id="adl_followUp" class="general loading"></div>
    <div id="medications_followUp" class="general loading"></div>
    <div id="therapyneed_followUp" class="general loading"></div>
</div>