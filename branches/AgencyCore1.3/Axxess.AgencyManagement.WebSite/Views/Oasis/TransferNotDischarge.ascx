﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Transfer, Not Discharge | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','transfernotdischarge');</script>")
%>
<div id="transfernotdischargeTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_nottransfer" tooltip="M0010 &ndash; M0150">Clinical Record Items</a></li>
        <li><a href="#riskassessment_nottransfer" tooltip="M1040 &ndash; M1055">Risk Assessment</a></li>
        <li><a href="#cardiacstatus_nottransfer" tooltip="M1500 &ndash; M1510">Cardiac Status</a></li>
        <li><a href="#medications_nottransfer" tooltip="M2004 &ndash; M2015">Medications</a></li>
        <li><a href="#emergentcare_nottransfer" tooltip="M2300 &ndash; M2310">Emergent Care</a></li>
        <li><a href="#dischargeAdd_nottransfer" tooltip="M0903 &ndash; M0906<br />M2400 &ndash; M2440">Transfer</a></li>
    </ul>
    <div id="clinicalRecord_nottransfer" class="general">
     <% Html.RenderPartial("~/Views/Oasis/TransferNotDischarge/Demographics.ascx", Model); %>
    </div>
    <div id="riskassessment_nottransfer" class="general loading">
    </div>
    <div id="cardiacstatus_nottransfer" class="general loading">
    </div>
    <div id="medications_nottransfer" class="general loading">
    </div>
    <div id="emergentcare_nottransfer" class="general loading">
    </div>
    <div id="dischargeAdd_nottransfer" class="general loading">
    </div>
</div>