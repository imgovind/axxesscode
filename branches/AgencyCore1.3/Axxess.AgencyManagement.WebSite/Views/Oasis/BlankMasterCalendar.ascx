﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeDateViewData>" %>
<%var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%var startDate = Model.StartDate; %>
<%var endDate = Model.EndDate; %>
<%var currentDate = Model.StartDate.AddDays(-startWeekDay);%>
<div style="width: 100%; height: 100%;">
    <table style="width: 100%; height: 100%;" border="1px" id="masterCalendarTable" class="masterCalendar oasisCalendar">
        <thead>
            <tr>
                <th> </th><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th> Fri</th><th>Sat </th>
            </tr>
        </thead>
        <tbody>
            <%for (int i = 1; i <= 10; i++)
              {%>
            <tr>
                <td style="width: 60px; height:30px">Week<%=i %></td>
                <%int addedDate = (i - 1) * 7; %>
                <%for (int j = 0; j <= 6; j++)
                  {%>
                <%var specificDate = currentDate.AddDays(j + addedDate);%>
                <%if (specificDate < startDate || specificDate > endDate)
                  { %>
                <td></td>
                <%}
                  else
                  {%>
                <%if (j == 6)
                  {%>
                <td class="lastTd">
                    <%}
                  else
                  { %>
                    <td>
                        <%} %>
                        <span style="font-size:11.px;"> <%=string.Format("{0:MM/dd}", specificDate)%></span>
                    </td>
                    <%}%><%} %>
            </tr>
            <%} %>
        </tbody>
    </table>
</div>