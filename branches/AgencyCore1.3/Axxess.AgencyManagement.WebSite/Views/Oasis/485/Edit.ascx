﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCare>" %>
<% using (Html.BeginForm("Save485", "Oasis", FormMethod.Post, new { @id = "edit485Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_485_Id" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_485_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_485_PatientId" })%>
<% var data = Model.ToDictionary(); %>
<fieldset>
    <legend>Medications (Locator #10): Dose/Frequency/Route (N)ew (C)hanged</legend>
    <div class="wide_column">
        <div class="buttons float_left"><ul>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowMedicationModal('<%= Model.EpisodeId %>','<%= Model.PatientId %>','<%= Model.Id %>');">Add/Edit Medications</a></li>
        </ul></div>
        <div class="row"><%=Html.TextArea("485Medications", data.ContainsKey("485Medications") && data["485Medications"].Answer.IsNotNullOrEmpty() ? data["485Medications"].Answer : "", new { @id = "485Medications", @style = "height: 180px; font-size: 16px;" })%></div>
    </div>
</fieldset>
<fieldset>
    <legend>DME and Supplies(Locator #14)</legend><%string[] dME = data.ContainsKey("485DME") && data["485DME"].Answer != "" ? data["485DME"].Answer.Split(',') : null; %><input type="hidden" name="485DME" value="" />
    <table class="form"><tbody>
        <tr>
            <td><%= string.Format("<input id='485DME1' class='radio float_left' name='485DME' value='1' type='checkbox' {0} />", dME != null && dME.Contains("1") ? "checked='checked'" : "") %>
                <label for="485DME1" class="radio">Bedside Commode</label></td>
            <td><%= string.Format("<input id='485DME2' class='radio float_left' name='485DME' value='2' type='checkbox' {0} />", dME != null && dME.Contains("2") ? "checked='checked'" : "") %>
                <label for="485DME2" class="radio">Cane</label></td>
            <td><%= string.Format("<input id='485DME3' class='radio float_left' name='485DME' value='3' type='checkbox' {0} />", dME != null && dME.Contains("3") ? "checked='checked'" : "") %>
                <label for="485DME3" class="radio">Elevated Toilet Seat</label></td>
        </tr><tr>
            <td><%= string.Format("<input id='485DME4' class='radio float_left' name='485DME' value='4' type='checkbox' {0} />", dME != null && dME.Contains("4") ? "checked='checked'" : "") %>
                <label for="485DME4" class="radio">Grab Bars</label></td>
            <td><%= string.Format("<input id='485DME5' class='radio float_left' name='485DME' value='5' type='checkbox' {0} />", dME != null && dME.Contains("5") ? "checked='checked'" : "") %>
                <label for="485DME5" class="radio">Hospital Bed</label></td>
            <td><%= string.Format("<input id='485DME6' class='radio float_left' name='485DME' value='6' type='checkbox' {0} />", dME != null && dME.Contains("6") ? "checked='checked'" : "") %>
                <label for="485DME6" class="radio">Nebulizer</label></td>
        </tr><tr>
            <td><%= string.Format("<input id='485DME7' class='radio float_left' name='485DME' value='7' type='checkbox' {0} />", dME != null && dME.Contains("7") ? "checked='checked'" : "") %>
                <label for="485DME7" class="radio">Oxygen</label></td>
            <td><%= string.Format("<input id='485DME8' class='radio float_left' name='485DME' value='8' type='checkbox' {0} />", dME != null && dME.Contains("8") ? "checked='checked'" : "") %>
                <label for="485DME8" class="radio">Tub/Shower Bench</label></td>
            <td><%= string.Format("<input id='485DME9' class='radio float_left' name='485DME' value='9' type='checkbox' {0} />", dME != null && dME.Contains("9") ? "checked='checked'" : "") %>
                <label for="485DME9" class="radio">Walker</label></td>
        </tr><tr>
            <td><%= string.Format("<input id='485DME10' class='radio float_left' name='485DME' value='10' type='checkbox' {0} />", dME != null && dME.Contains("10") ? "checked='checked'" : "") %>
                <label for="485DME10" class="radio">Wheelchair</label></td>
            <td colspan="2"><label for="485DMEComments">Other:</label><%=Html.TextBox("485DMEComments", data.ContainsKey("485DMEComments") ? data["485DMEComments"].Answer : "", new { @id = "485DMEComments", @maxlength = "20" })%></td>
        </tr>
    </tbody></table>
</fieldset>
<fieldset>
    <legend>Safety Measures (Locator #15)</legend><% string[] safetyMeasure = data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer != "" ? data["485SafetyMeasures"].Answer.Split(',') : null; %><input type="hidden" name="485SafetyMeasures" value="" />
    <table class="form"><tbody>
        <tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures1' name='485SafetyMeasures' value='1' {0} />", safetyMeasure != null && safetyMeasure.Contains("1") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures1" class="radio">Anticoagulant Precautions</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures2' name='485SafetyMeasures' value='2' {0} />", safetyMeasure != null && safetyMeasure.Contains("2") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures2" class="radio">Emergency Plan Developed</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures3' name='485SafetyMeasures' value='3' {0} />", safetyMeasure != null && safetyMeasure.Contains("3") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures3" class="radio">Fall Precautions</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures4' name='485SafetyMeasures' value='4' {0} />", safetyMeasure != null && safetyMeasure.Contains("4") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures4" class="radio">Keep Pathway Clear</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures5' name='485SafetyMeasures' value='5' {0} />", safetyMeasure != null && safetyMeasure.Contains("5") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures5" class="radio">Keep Side Rails Up</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures6' name='485SafetyMeasures' value='6' {0} />", safetyMeasure != null && safetyMeasure.Contains("6") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures6" class="radio">Neutropenic Precautions</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures7' name='485SafetyMeasures' value='7' {0} />", safetyMeasure != null && safetyMeasure.Contains("7") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures7" class="radio">O2 Precautions</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures8' name='485SafetyMeasures' value='8' {0} />", safetyMeasure != null && safetyMeasure.Contains("8") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures8" class="radio">Proper Position During Meals</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures9' name='485SafetyMeasures' value='9' {0} />", safetyMeasure != null && safetyMeasure.Contains("9") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures9" class="radio">Safety in ADLs</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures10' name='485SafetyMeasures' value='10' {0} />", safetyMeasure != null && safetyMeasure.Contains("10") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures10" class="radio">Seizure Precautions</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures11' name='485SafetyMeasures' value='11' {0} />", safetyMeasure != null && safetyMeasure.Contains("11") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures11" class="radio">Sharps Safety</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures12' name='485SafetyMeasures' value='12' {0} />", safetyMeasure != null && safetyMeasure.Contains("12") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures12" class="radio">Slow Position Change</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures13' name='485SafetyMeasures' value='13' {0} />", safetyMeasure != null && safetyMeasure.Contains("13") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures14' name='485SafetyMeasures' value='14' {0} />", safetyMeasure != null && safetyMeasure.Contains("14") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures15' name='485SafetyMeasures' value='15' {0} />", safetyMeasure != null && safetyMeasure.Contains("15") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures15" class="radio">Use of Assistive Devices</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures16' name='485SafetyMeasures' value='16' {0} />", safetyMeasure != null && safetyMeasure.Contains("16") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures16" class="radio">Instructed on safe utilities management</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures17' name='485SafetyMeasures' value='17' {0} />", safetyMeasure != null && safetyMeasure.Contains("17") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures17" class="radio">Instructed on mobility safety</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures18' name='485SafetyMeasures' value='18' {0} />", safetyMeasure != null && safetyMeasure.Contains("18") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures18" class="radio">Instructed on DME &amp; electrical safety</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures19' name='485SafetyMeasures' value='19' {0} />", safetyMeasure != null && safetyMeasure.Contains("19") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures19" class="radio">Instructed on sharps container</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures20' name='485SafetyMeasures' value='20' {0} />", safetyMeasure != null && safetyMeasure.Contains("20") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures20" class="radio">Instructed on medical gas</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures21' name='485SafetyMeasures' value='21' {0} />", safetyMeasure != null && safetyMeasure.Contains("21") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures21" class="radio">Instructed on disaster/ emergency plan</label></td>
        </tr><tr>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures22' name='485SafetyMeasures' value='22' {0} />", safetyMeasure != null && safetyMeasure.Contains("22") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures22" class="radio">Instructed on safety measures</label></td>
            <td><%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures23' name='485SafetyMeasures' value='23' {0} />", safetyMeasure != null && safetyMeasure.Contains("23") ? "checked='checked'" : "") %>
                <label for="485SafetyMeasures23" class="radio">Instructed on proper handling of biohazard waste</label></td>
            <td><label for="485SafetyMeasuresComments">Other (Specify):</label><%= Html.TextBox("485OtherSafetyMeasures", data.ContainsKey("485OtherSafetyMeasures") ? data["485OtherSafetyMeasures"].Answer : "", new { @id = "485OtherSafetyMeasures" })%></td>
        </tr>
    </tbody></table>
</fieldset>
<fieldset>
    <legend>Nutritional Req. (Locator #16)</legend>
    <div class="wide_column">
        <div class="row"><%=Html.TextArea("485NutritionalReqs", data.ContainsKey("485NutritionalReqs") && data["485NutritionalReqs"].Answer.IsNotNullOrEmpty() ? data["485NutritionalReqs"].Answer : "", new { @id = "485NutritionalReqs", @style = "height: 120px; font-size: 16px;" })%></div>
    </div>
</fieldset>
<fieldset>
    <legend>Allergies (Locator #17)</legend>
    <div class="column">
        <div class="row"><%= Html.RadioButton("485Allergies", "Yes", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "Yes" ? true : false, new { @id = "SOC_Allergies_Specify", @class = "radio" }) %><label for="SOC_Allergies_Specify">Allergic to:</label><%= Html.TextArea("485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "", new { @id = "", @style = "font-size: 16px;" })%></div>
    </div><div class="column">
        <div class="row"><%= Html.Hidden("485Allergies"," ", new { @id = "" }) %><%= Html.RadioButton("485Allergies", "No", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? true : false, new { @id = "SOC_Allergies_NKA", @class = "radio" }) %><label for="SOC_Allergies_NKA">NKA (Food/Drugs/Latex)</label></div>
    </div>
</fieldset>
<fieldset>
    <legend>Functional Limitations (locator #18.A)</legend><input name="485FunctionLimitations" value=" " type="hidden" />
    <% string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null; %>
    <table class="form"><tbody>
        <tr>
            <td><%= string.Format("<input id='485FunctionLimitations1' name='485FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="485FunctionLimitations1" class="fixed radio">Amputation</label></td>
            <td><%= string.Format("<input id='485FunctionLimitations5' name='485FunctionLimitations' value='5' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations5" class="fixed radio">Paralysis</label></td>
            <td><%= string.Format("<input id='485FunctionLimitations9' name='485FunctionLimitations' value='9' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations9" class="fixed radio">Legally Blind</label></td>
        </tr><tr> 
            <td><%= string.Format("<input id='485FunctionLimitations2' name='485FunctionLimitations' value='2' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations2" class="fixed radio">Bowel/Bladder Incontinence</label></td>
            <td><%= string.Format("<input id='485FunctionLimitations6' name='485FunctionLimitations' value='6' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations6" class="fixed radio">Endurance</label></td>
            <td><%= string.Format("<input id='485FunctionLimitationsA' name='485FunctionLimitations' value='A' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitationsA" class="fixed radio">Dyspnea</label></td>
        </tr><tr> 
            <td><%= string.Format("<input id='485FunctionLimitations3' name='485FunctionLimitations' value='3' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations3" class="fixed radio">Contracture</label></td>
            <td><%= string.Format("<input id='485FunctionLimitations7' name='485FunctionLimitations' value='7' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations7" class="fixed radio">Ambulation</label></td>
            <td><%= string.Format("<input id='485FunctionLimitations4' name='485FunctionLimitations' value='4' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations4" class="fixed radio">Hearing</label></td>
        </tr><tr> 
            <td><%= string.Format("<input id='485FunctionLimitations8' name='485FunctionLimitations' value='8' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitations8" class="fixed radio">Speech</label></td>
            <td colspan="2"><%= string.Format("<input id='485FunctionLimitationsB' name='485FunctionLimitations' value='B' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "")%>
                <label for="485FunctionLimitationsB" class="fixed radio">Other (Specify)</label>
            <%= Html.TextBox("485FunctionLimitationsOther", data.ContainsKey("485FunctionLimitationsOther") ? data["485FunctionLimitationsOther"].Answer : "", new { @id = "485FunctionLimitationsOther", @maxlength = "40" }) %>    
            </td>
        </tr>
    </tbody></table>
</fieldset>
<fieldset>
	<legend>Activities Permitted (locator #18.B)</legend><input type="hidden" name="485ActivitiesPermitted" value="" />
	<% string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
	<div class="column">
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted1' name='485ActivitiesPermitted' value='1' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("1") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted1" class="radio">Complete bed rest</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted3' name='485ActivitiesPermitted' value='3' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("3") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted3" class="radio">Up as tolerated</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted5' name='485ActivitiesPermitted' value='5' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("5") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted5" class="radio">Exercise prescribed</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted7' name='485ActivitiesPermitted' value='7' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("7") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted7" class="radio">Independent at home</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted9' name='485ActivitiesPermitted' value='9' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("9") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted9" class="radio">Cane</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermittedB' name='485ActivitiesPermitted' value='B' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("B") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermittedB" class="radio">Walker</label></div>
    </div><div class="column">
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted2' name='485ActivitiesPermitted' value='2' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("2") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted2" class="radio">Bed rest with BRP</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted4' name='485ActivitiesPermitted' value='4' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("4") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted4" class="radio">Transfer bed-chair</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted6' name='485ActivitiesPermitted' value='6' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("6") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted6" class="radio">Partial weight bearing</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermitted8' name='485ActivitiesPermitted' value='8' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("8") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermitted8" class="radio">Crutches</label></div>
        <div class="row"><%= string.Format("<input id='485ActivitiesPermittedA' name='485ActivitiesPermitted' value='A' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("A") ? "checked='checked'" : "" ) %>
            <label for="485ActivitiesPermittedA" class="radio">Wheelchair</label></div>
        <div class="row"><label for="485ActivitiesPermittedOther">Other (specify):</label><%= Html.TextBox("485ActivitiesPermittedOther", data.ContainsKey("485ActivitiesPermittedOther") ? data["485ActivitiesPermittedOther"].Answer : "", new { @id = "485ActivitiesPermittedOther", @maxlength = "40" }) %></div>
	</div>
</fieldset>
<fieldset>
    <legend>Mental Status (Locator #19)</legend><%string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %><input type="hidden" name="485MentalStatus" value=" " />
    <table class="form"><tbody>
        <tr>
            <td><%= string.Format("<input id='485MentalStatus1' class='radio float_left' name='485MentalStatus' value='1' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus1" class="radio">Oriented</label></td>
            <td><%= string.Format("<input id='485MentalStatus2' class='radio float_left' name='485MentalStatus' value='2' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus2" class="radio">Comatose</label></td>
            <td><%= string.Format("<input id='485MentalStatus3' class='radio float_left' name='485MentalStatus' value='3' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus3" class="radio">Forgetful</label></td>
            <td><%= string.Format("<input id='485MentalStatus4' class='radio float_left' name='485MentalStatus' value='4' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus4" class="radio">Agitated</label></td>
            <td><%= string.Format("<input id='485MentalStatus5' class='radio float_left' name='485MentalStatus' value='5' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus5" class="radio">Depressed</label></td>
        </tr><tr>
            <td><%= string.Format("<input id='485MentalStatus6' class='radio float_left' name='485MentalStatus' value='6' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus6" class="radio">Disoriented</label></td>
            <td><%= string.Format("<input id='485MentalStatus7' class='radio float_left' name='485MentalStatus' value='7' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus7" class="radio">Lethargic</label></td>
            <td colspan="3"><%= string.Format("<input id='485MentalStatus8' class='radio float_left' name='485MentalStatus' value='8' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="485MentalStatus8" class="radio more fixed">Other (specify):</label><%=Html.TextBox("485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "485MentalStatusOther", @class = "st", @maxlength = "20" })%></td>
        </tr>
    </tbody></table>
</fieldset>
<fieldset>
    <legend>Prognosis (Locator #20)</legend>
    <table class="form"><tbody>
        <tr>
            <td><%= Html.RadioButton("485Prognosis", "Guarded", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? true : false, new { @id = "485PrognosisGuarded", @class = "radio float_left" } )%><label for="485PrognosisGuarded" class="radio">Guarded</label></td>
            <td><%= Html.RadioButton("485Prognosis", "Poor", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? true : false, new { @id = "485PrognosisPoor", @class = "radio float_left" })%><label for="485PrognosisPoor" class="radio">Poor</label></td>
            <td><%= Html.RadioButton("485Prognosis", "Fair", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? true : false, new { @id = "485PrognosisFair", @class = "radio float_left" })%><label for="485PrognosisFair" class="radio">Fair</label></td>
            <td><%= Html.RadioButton("485Prognosis", "Good", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? true : false, new { @id = "485PrognosisGood", @class = "radio float_left" })%><label for="485PrognosisGood" class="radio">Good</label></td>
            <td><%= Html.RadioButton("485Prognosis", "Excellent", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? true : false, new { @id = "485PrognosisExcellent", @class = "radio float_left" })%><label for="485PrognosisExcellent" class="radio">Excellent</label></td>
        </tr>
    </tbody></table>
</fieldset>
<fieldset>
    <legend>Orders for Discipline and Treatments (Specify Amount/Frequency/Duration) (#21)</legend>
    <div class="wide_column">
        <div class="row"><%= Html.TextArea("485Interventions", data.ContainsKey("485Interventions") && data["485Interventions"].Answer.IsNotNullOrEmpty() ? data["485Interventions"].Answer : "", new { @id = "Edit_485_Interventions", @style = "height: 180px;font-size: 16px;" })%></div>
    </div>
</fieldset>
<fieldset>
    <legend>Goals/Rehabilitation Potential/Discharge Plans (#22)</legend>
    <div class="wide_column">
        <div class="row"><%= Html.TextArea("485Goals", data.ContainsKey("485Goals") && data["485Goals"].Answer.IsNotNullOrEmpty() ? data["485Goals"].Answer : "", new { @id = "Edit_485_Goals", @cols = "100", @style = "height: 180px;font-size: 16px;" })%></div>
    </div>
</fieldset>
<fieldset>
    <div class="column">
        <div class="row">
            <label for="Edit_485_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
            <div class="float_right"><%= Html.TextBox("SignatureText","", new { @id = "Edit_485_ClinicianSignature" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="Edit_485_ClinicianSignatureDate" class="bigtext float_left">Date:</label>
            <% var minDate = DateTime.Now.AddDays(-60);
               if (Model.EpisodeStart.IsNotNullOrEmpty())
               {
                   minDate = DateTime.Parse(Model.EpisodeStart).AddDays(-60);
               }
            %>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("SignatureDate").MinDate(minDate).MaxDate(DateTime.Today).Value(DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "Edit_485_ClinicianSignatureDate", @class = "date" })%></div>
        </div>
    </div>
</fieldset>
<%= Html.Hidden("Status", "", new { @id = "Edit_485_Status" })%>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('110');$(this).closest('form').submit();">Save</a></li>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('125');$(this).closest('form').submit();">Complete</a></li>
    <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editplanofcare');">Cancel</a></li>
</ul></div>
<% } %>
