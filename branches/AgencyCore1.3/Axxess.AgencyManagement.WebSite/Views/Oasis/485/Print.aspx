﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PlanofCare>" %><%
var data = Model.ToDictionary();
var medicationList = Model.MedicationProfileData != null ? Model.MedicationProfileData.ToObject<List<Medication>>() : new List<Medication>();
var patient = Model.PatientData != null ? Model.PatientData.ToObject<Patient>() : new Patient();
var agency = Model.AgencyData != null ? Model.AgencyData.ToObject<Agency>() : new Agency();
var location = agency != null && agency.MainLocation != null ? agency.MainLocation : new AgencyLocation();
var physician = Model.PhysicianData != null ? Model.PhysicianData.ToObject<AgencyPhysician>() : new AgencyPhysician();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%= agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : ""%>485 Plan of Care<%= patient != null ? (" | " + patient.LastName + ", " + patient.FirstName + " " + patient.MiddleInitial).ToTitleCase() : ""%></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
	<script type="text/javascript">
	    function overflow(text, delimiter, primary, overflow) {
	        text = text.split(delimiter);
	        if (typeof (primary) == "string") primary = document.getElementById(primary);
	        if (typeof (overflow) == "string") overflow = document.getElementById(overflow);
	        sub = delimiter.length;
	        while (text.length > 0 && primary.scrollHeight <= primary.clientHeight) {
	            primary.innerHTML += text[0] + delimiter;
	            if (primary.scrollHeight <= primary.clientHeight) text.shift();
	            else sub += text[0].length;
	        }
	        primary.innerHTML = primary.innerHTML.substr(0, (primary.innerHTML.length - sub));
	        sub = delimiter.length;
	        while (text.length > 0 && overflow.scrollHeight <= overflow.clientHeight) {
	            overflow.innerHTML += text[0] + delimiter;
	            if (overflow.scrollHeight <= overflow.clientHeight) text.shift();
	            else {
	                sub += text[0].length;
	                break;
	            }
	        }
	        overflow.innerHTML = overflow.innerHTML.substr(0, (overflow.innerHTML.length - sub));
	    }
    </script>
</head>
<body>
    <div class="page">
        <div class="small float_left">Department of Health and Human Services<br />Centers for Medicare &amp; Medicaid Services</div>
        <div class="small float_right">Form Approved<br />OMB No. 0938-0357</div>
        <span class="clear"></span>
        <h1>Home Health Certification and Plan of Care</h1>
        <div>
            <table><tbody>
                <tr>
                    <td>1. Patient&rsquo;s HI Claim No.<span id="loc1" class="mono"><%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "" %></span></td>
                    <td>2. Start of Care Date<span id="loc2" class="mono"><%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "" %></span></td>
                    <td>3. Certification Period<span id="loc3" class="mono">From: <%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart : "<span class='noline blank'></span>"%> To: <%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd : "<span class='noline blank'></span>"%></span></td>
                    <td>4. Medical Record No.<span id="loc4" class="mono"><%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "" %></span></td>
                    <td>5. Provider No.<span id="loc5" class="mono"><%= location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber : "" %></span></td>
                </tr>
            </tbody></table>
        </div><div>
            <table class="fixed"><tbody>
                <tr>
                    <td>6. Patient&rsquo;s Name and Address<span id="loc6" class="quad"><%= patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "" %><%= patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "" %><%= patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + "." : "" %><br /><%= patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : "" %><%= patient.AddressLine2.IsNotNullOrEmpty() ? " " + patient.AddressLine2.ToTitleCase() : ""%><%= patient.AddressCity.IsNotNullOrEmpty() ? "<br />" + patient.AddressCity.ToTitleCase() : ""%><%= patient.AddressStateCode.IsNotNullOrEmpty() ? ", " + patient.AddressStateCode : "" %><%= patient.AddressZipCode.IsNotNullOrEmpty() ? " &nbsp;" + patient.AddressZipCode : "" %></span></td>
                    <td>7. Provider&rsquo;s Name, Address and Telephone Number<span id="loc7" class="pent"><%= agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() : "" %><br /><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? " " + location.AddressLine2.ToTitleCase() : ""%><%= location.AddressCity.IsNotNullOrEmpty() ? "<br />" + location.AddressCity.ToTitleCase() : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? ", " + location.AddressStateCode : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? " &nbsp;" + location.AddressZipCode : ""%></span></td>
                </tr><tr>
                    <td class="nopad">
                        <table><tbody>
                            <tr>
                                <td>8. Date of Birth <%= patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : "<span class='noline blank'></span>" %></td>
                                <td>9. Sex <%= patient.Gender.IsNotNullOrEmpty() ? patient.Gender : "<span class='noline blank'></span>" %></td>
                            </tr>
                        </tbody></table>
                    </td>
                    <td rowspan="2">10. Medications: Dose/Freq./Route (N)ew (C)hanged<span id="loc10" class="deca"></span></td>
                </tr><tr>
                    <td class="nopad">
                        <table><tbody>
                            <tr>
                                <td>11. ICD-9-CM<span id="loc11a" class="mono"><%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : ""%></span></td>
                                <td>Principal Diagnosis<span id="loc11b" class="mono"><%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : ""%></span></td>
                                <td>Date<span id="loc11c" class="mono"><%= data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer : ""%></span></td>
                            </tr><tr>
                                <td>12. ICD-9-CM<span id="loc12a" class="mono"><%= data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer : ""%></span></td>
                                <td>Surgical Procedure<span id="loc12b" class="mono"><%= data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer : ""%></span></td>
                                <td>Date<span id="loc12c" class="mono"><%= data.ContainsKey("485SurgicalProcedureCode1Date") ? data["485SurgicalProcedureCode1Date"].Answer : ""%></span></td>
                            </tr><tr>
                                <td>13. ICD-9-CM<span id="loc13a" class="quad"><% for (int i = 1; i < 4; i++) { %><%= data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "<br />" : "") + data["M1022ICD9M" + i].Answer : ""%><% } %></span></td>
                                <td>Other Pertinent Diagnoses<span id="loc13b" class="quad"><% for (int i = 1; i < 4; i++) { %><%= data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "<br />" : "") + data["M1022PrimaryDiagnosis" + i].Answer : ""%><% } %></span></td>
                                <td>Date<span id="loc13c" class="quad"><% for (int i = 1; i < 4; i++) { %><%= data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() && data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? (i > 1 ? "<br />" : "") + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : ""%><% } %></span></td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr><tr>
                    <td>14. DME and Supplies<span id="loc14" class="trip"></span></td>
                    <td>15. Safety measures<span id="loc15" class="trip"></span></td>
                </tr><tr>
                    <td>16. Nutritional Requirements<span id="loc16" class="trip"></span></td>
                    <td>17. Allergies<span id="loc17" class="trip"><%= data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? "NKA (Food/Drugs/Latex)" : "Allergic to:" + (data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "") %></span></td>
                </tr><tr>
                    <td>18.A. Functional Limitations<br />
                        <table class="noborders small fixed"><tbody>
                            <tr><% string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null; %>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("1") ? "&#x2713;" : "" %></span> 1. Amputation</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("5") ? "&#x2713;" : "" %></span> 5. Paralysis</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("9") ? "&#x2713;" : "" %></span> 9. Legally Blind</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("2") ? "&#x2713;" : "" %></span> 2. Bowel/Bladder (Incontinance)</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("6") ? "&#x2713;" : "" %></span> 6. Endurance</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("A") ? "&#x2713;" : "" %></span> A. Dyspnea With<br />Minimal Exertion</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("3") ? "&#x2713;" : "" %></span> 3. Contracture</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("7") ? "&#x2713;" : "" %></span> 7. Ambulation</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("B") ? "&#x2713;" : "" %></span> B. Other (Specify)</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("4") ? "&#x2713;" : "" %></span> 4. Hearing</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("8") ? "&#x2713;" : "" %></span> 8. Speech</span></td>
                                <td></td>
                            </tr><tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                    <td>18.B. Activities Permitted<br />
                        <table class="noborders small fixed"><tbody>
                            <tr><% string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("1") ? "&#x2713;" : "" %></span> 1. Complete Bedrest</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("6") ? "&#x2713;" : "" %></span> 6. Partial Weight Bearing</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("A") ? "&#x2713;" : "" %></span> A. Wheelchair</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("2") ? "&#x2713;" : "" %></span> 2. Bedrest BRP</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("7") ? "&#x2713;" : "" %></span> 7. Independent At Home</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("B") ? "&#x2713;" : "" %></span> B. Walker</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("3") ? "&#x2713;" : "" %></span> 3. Up As Tolerated</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("8") ? "&#x2713;" : "" %></span> 8. Crutches</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("C") ? "&#x2713;" : "" %></span> C. No Restrictions</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("4") ? "&#x2713;" : "" %></span> 4. Transfer Bed/Chair</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("9") ? "&#x2713;" : "" %></span> 9. Cane</span></td>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("D") ? "&#x2713;" : "" %></span> D. Other (Specify)</span></td>
                            </tr><tr>
                                <td><span class="checklabel auto"><span class="checkbox"><%= activitiesPermitted != null && activitiesPermitted.Contains("5") ? "&#x2713;" : "" %></span> 5. Exercises Prescribed</span></td>
                                <td></td>
                                <td></td>
                            </tr><tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </div><div class="hexcol">
            <span>19. Mental Status</span><%string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("1") ? "&#x2713;" : "" %></span> 1. Oriented</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("3") ? "&#x2713;" : "" %></span> 3. Forgetful</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("5") ? "&#x2713;" : "" %></span> 5. Disoriented</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("7") ? "&#x2713;" : "" %></span> 7. Agitated</span>
            <span class="checklabel"></span><span></span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("2") ? "&#x2713;" : "" %></span> 2. Comatose</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("4") ? "&#x2713;" : "" %></span> 4. Depressed</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("6") ? "&#x2713;" : "" %></span> 6. Lethargic</span>
            <span class="checklabel small"><span class="checkbox"><%= mentalStatus!=null && mentalStatus.Contains("8") ? "&#x2713;" : "" %></span> 8. Other</span>
        </div><div class="hexcol">
            <span>20. Prognosis</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? "&#x2713;" : "" %></span> 1. Poor</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? "&#x2713;" : "" %></span> 2. Guarded</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? "&#x2713;" : "" %></span> 3. Fair</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? "&#x2713;" : "" %></span> 4. Good</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? "&#x2713;" : "" %></span> 5. Excellent</span>
        </div><div>
            <span>21. Orders for Discipline and Treatments (Specify Amount/Frequency/Duration)</span>
            <span id="loc21" class="deca"></span>
        </div><div>
            <span>22. Goals/Rehabilitation Potential/Discharge Plans</span>
            <span id="loc22" class="deca"></span>
        </div><div>
            <table><tbody>
                <tr>
                    <td>23. Nurse&rsquo;s Signature and Date of Verbal SOC Where Applicable:<span id="loc23" class="dual"></span></td>
                    <td>25. Date of HHA Received Signed POT:<span id="loc25" class="dual"></span></td>
                </tr>
            </tbody></table>
        </div><div>
            <table class="fixed"><tbody>
                <tr>
                    <td>24. Physician&rsquo;s Name and Address<span id="loc24" class="quad">
                    <%= physician.LastName.IsNotNullOrEmpty() ? physician.LastName : "" %>, <%= physician.FirstName.IsNotNullOrEmpty() ? physician.FirstName : ""%><%= physician.MiddleName.IsNotNullOrEmpty() ? " " + physician.MiddleName : "" %><br />
                    <%= physician.AddressFirstRow.IsNotNullOrEmpty() ? physician.AddressFirstRow + "<br />" : ""%>
                    <%= physician.AddressSecondRow.IsNotNullOrEmpty() ? physician.AddressSecondRow : ""%>
                    </span></td>
                    <td>26. I certify/recertify that this patient is confined to his/her home and needs intermittent skilled nursing care, physical therapy and/or speech therapy or continues to need occupational therapy. The patient is under my care, and I have authorized services on this plan of care and will periodically review the plan.<span id="loc26" class="dual"></span></td>
                </tr><tr>
                    <td>27. Attending Physician&rsquo;s Signature and Date Signed<span id="loc27" class="dual"></span></td>
                    <td>28. Anyone who misrepresents, falsifies, or conceals essential information required for payment of Federal funds may be subject to fine, imprisonment, or civil penalty under applicable Federal laws.<span id="loc28" class="dual"></span></td>
                </tr>
            </tbody></table>
        </div>
        <div class="small float_left">CMS-485 (C-3) (02-94) (Formerly HCFA-485) (Print Aligned)</div>
        <div class="small float_right">Page 1 of 2</div>
    </div><div class="page">
        <div class="small float_left">Department of Health and Human Services<br />Health Care Financing Administration</div>
        <div class="small float_right">Form Approved<br />OHB No. 0938-0357</div>
        <span class="clear"></span>
        <h1>Addendum to Plan of Care</h1> 
        <div>
            <table><tbody>
                <tr>
                    <td>1. Patient&rsquo;s HI Claim No.<span id="loc1-487" class="mono"><%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "" %></span></td>
                    <td>2. Start of Care Date<span id="loc2-487" class="mono"><%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "" %></span></td>
                    <td>3. Certification Period<span id="loc3-487" class="mono">From: <%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart : "<span class='noline blank'></span>"%> To: <%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd : "<span class='noline blank'></span>"%></span></td>
                    <td>4. Medical Record No.<span id="loc4-487" class="mono"><%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "" %></span></td>
                    <td>5. Provider No.<span id="loc5-487" class="mono"><%= agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "" %></span></td>
                </tr>
            </tbody></table>
        </div><div>
            <table class="fixed"><tbody>
                <tr>
                    <td>6. Patient&rsquo;s Name<span id="loc6-487" class="dual"><%= patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "" %><%= patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "" %><%= patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + "." : "" %></span></td>
                    <td>7. Provider&rsquo;s Name<span id="loc7-487" class="dual"><%= agency.Name.IsNotNullOrEmpty() ? agency.Name : "" %></span></td>
                </tr>
            </tbody></table>
        </div>
        <div><span>10. Medications</span><span id="loc10-487" class="trip"></span></div>
        <div><span>12. Surgical Procedures</span><span id="loc12-487" class="trip"></span></div>
        <div><span>13. Other Pertinent Diagnosis</span><span id="loc13-487" class="trip">
            <% for (int i = 4; i < 6; i++) { %>
                <%= data.ContainsKey("M1022ICD9M" + i) ? (i > 4 ? "<br />" : "") + data["M1022ICD9M" + i].Answer : ""%>
                <%= data.ContainsKey("M1022PrimaryDiagnosis" + i) ? data["M1022PrimaryDiagnosis" + i].Answer : ""%>
            <% } %></span></div>
        <div><span>14. DME</span><span id="loc14-487" class="quad"></span></div>
        <div><span>15. Safety Measures</span><span id="loc15-487" class="octo"></span></div>
        <div><span>16. Nutrition</span><span id="loc16-487" class="quad"></span></div>
        <div><span>18.A. Functional Limitations</span><span id="loc18a-487" class="quad"></span></div>
        <div><span>21. Orders</span><span id="loc21-487" class="mega487"></span></div>
        <div><span>22. Goals</span><span id="loc22-487" class="mega487"></span></div>
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="5">9. Signature of Physician<span id="loc9-487" class="dual"></span></td>
                    <td>10. Date<span id="loc10b-487" class="dual"></span></td>
                </tr><tr>
                    <td colspan="5">11. Optional Name/Signature of Nurse/Therapist<span id="loc11-487" class="dual"></span></td>
                    <td>12. Date<span id="loc12b-487" class="dual"></span></td>
                </tr>
            </tbody></table>
        </div>
        <div class="small float_left">Form HCFA-487 (C4)(4-87)</div>
        <div class="small float_right">Page 2 of 2</div>
    </div>
<%= string.Format("<script type='text/javascript'>\n\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", data.ContainsKey("485Medications") ? data["485Medications"].Answer.Replace("\n", ", ").Replace("\r", "").Replace("\"", "”") : "", ", ", "loc10", "loc10-487")%>
<%= string.Format("\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", data.ContainsKey("485DME") ? PlanofCareXml.LookupText("DME", data["485DME"].Answer) + (data.ContainsKey("485Supplies") ? PlanofCareXml.LookupText("Supplies", data["485Supplies"].Answer) : "") : "", ", ", "loc14", "loc14-487")%>
<%= string.Format("\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", data.ContainsKey("485SafetyMeasures") ? PlanofCareXml.LookupText("SafetyMeasures", data["485SafetyMeasures"].Answer) + (data.ContainsKey("485TriageRiskCode") && data["485TriageRiskCode"].Answer.IsNotNullOrEmpty() ? ", Triage/Risk Code: " + data["485TriageRiskCode"].Answer : "") + (data.ContainsKey("485DisasterCode") && data["485DisasterCode"].Answer.IsNotNullOrEmpty() ? ", Disaster Code: " + data["485DisasterCode"].Answer : "") + (data.ContainsKey("485SafetyMeasuresComments") ? ", Comments: " + data["485SafetyMeasuresComments"].Answer : "") : "", ", ", "loc15", "loc15-487")%>
<%= string.Format("\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", data.ContainsKey("485NutritionalReqs") ? data["485NutritionalReqs"].Answer.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\"", "”") : "", ". ", "loc16", "loc16-487")%>
<%= string.Format("\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", data.ContainsKey("485Interventions") ? data["485Interventions"].Answer.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\"", "”") : "", ". ", "loc21", "loc21-487")%>
<%= string.Format("\toverflow(\"{0}\",\"{1}\",\"{2}\",\"{3}\");\n</script>", data.ContainsKey("485Goals") ? data["485Goals"].Answer.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\"", "”") : "", ". ", "loc22", "loc22-487")%>
</body>
</html>