﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareEndocrineForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id) %>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit") %>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare") %>
<%= Html.Hidden("categoryType", "Endocrine")%> 
<div class="wrapper main">
    <fieldset>
        <legend>Endocrine</legend>
        <input type="hidden" name="ResumptionOfCare_GenericEndocrineWithinNormalLimits" />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericEndocrineWithinNormalLimits' class='radio float_left' name='ResumptionOfCare_GenericEndocrineWithinNormalLimits' value='1' type='checkbox' {0} />", data.ContainsKey("GenericEndocrineWithinNormalLimits") && data["GenericEndocrineWithinNormalLimits"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericEndocrineWithinNormalLimits">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <label class="float_left">Is patient diabetic?</label>
                <%= Html.Hidden("ResumptionOfCare_GenericPatientDiabetic") %>
                <div class="float_right">
                    <%= Html.RadioButton("ResumptionOfCare_GenericPatientDiabetic", "1", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericPatientDiabetic1", @class = "radio" })%><label for="ResumptionOfCare_GenericPatientDiabetic1" class="inlineradio">Yes</label><%= Html.RadioButton("ResumptionOfCare_GenericPatientDiabetic", "0", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericPatientDiabetic0", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericPatientDiabetic0" class="inlineradio">No</label>
                </div>
            </div>
            <div class="row diabetic">
                <div class="row">
                    <label class="float_left">Diabetic Management:</label>
                    <% string[] diabeticCareDiabeticManagement = data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer != "" ? data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericDiabeticCareDiabeticManagement" value="" />
                    <div class="float_right">
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareDiabeticManagement1' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareDiabeticManagement' value='1' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("1") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareDiabeticManagement1" class="fixed inlineradio">Diet</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareDiabeticManagement2' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareDiabeticManagement' value='2' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("2") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareDiabeticManagement2" class="fixed inlineradio">Oral Hypoglycemic</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareDiabeticManagement3' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareDiabeticManagement' value='3' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("3") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareDiabeticManagement3" class="fixed inlineradio">Exercise</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareDiabeticManagement4' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareDiabeticManagement' value='4' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("4") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareDiabeticManagement4" class="fixed inlineradio">Insulin</label>
                        </div>
                    </div>
                </div><div class="row">
                    <label class="float_left">Insulin dependent?</label>
                    <%= Html.Hidden("ResumptionOfCare_GenericInsulinDependent") %>
                    <div class="float_right">
                        <%= Html.RadioButton("ResumptionOfCare_GenericInsulinDependent", "1", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericInsulinDependent1", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericInsulinDependent1" class="inlineradio">Yes</label>
                        <%= Html.RadioButton("ResumptionOfCare_GenericInsulinDependent", "0", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericInsulinDependent0", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericInsulinDependent0" class="inlineradio">No</label>
                    </div>
                </div><div class="row">
                    <label class="float_left">How long?</label>
                    <div class="float_right">
                        <%= Html.TextBox("ResumptionOfCare_GenericInsulinDependencyDuration", data.ContainsKey("GenericInsulinDependencyDuration") ? data["GenericInsulinDependencyDuration"].Answer : "", new { @id = "ResumptionOfCare_GenericInsulinDependencyDuration", @class = "st", @maxlength = "10" }) %>
                    </div>
                </div><div class="row">
                    <div class="strong">Insulin Administered by:</div>
                    <% string[] diabeticCareInsulinAdministeredby = data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer != "" ? data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby" value="" />
                    <div class="float_right">
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby1' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby' value='1' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("1") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby1" class="fixed inlineradio">N/A</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby2' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby' value='2' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("2") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby2" class="fixed inlineradio">Patient</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby3' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby' value='3' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("3") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby3" class="fixed inlineradio">Caregiver</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby4' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby' value='4' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("4") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDiabeticCareInsulinAdministeredby4" class="fixed inlineradio">SN</label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div><div class="column">
           <div class="row">
                <label class="float_left">Is patient independent with glucometer use?</label>
                <%= Html.Hidden("ResumptionOfCare_GenericGlucometerUseIndependent") %>
                <div class="float_right">
                    <%= Html.RadioButton("ResumptionOfCare_GenericGlucometerUseIndependent", "1", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericGlucometerUseIndependent1", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericGlucometerUseIndependent1" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericGlucometerUseIndependent", "0", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericGlucometerUseIndependent0", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericGlucometerUseIndependent0" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Is caregiver independent with glucometer use?</label>
                <%= Html.Hidden("ResumptionOfCare_GenericCareGiverGlucometerUse") %>
                <div class="float_right">
                    <div class="float_left">
                        <%= Html.RadioButton("ResumptionOfCare_GenericCareGiverGlucometerUse", "1", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericCareGiverGlucometerUse1", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericCareGiverGlucometerUse1" class="inlineradio">Yes</label>
                    </div><div class="float_left">
                        <%= Html.RadioButton("ResumptionOfCare_GenericCareGiverGlucometerUse", "0", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericCareGiverGlucometerUse0", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericCareGiverGlucometerUse0" class="inlineradio">No</label>
                    </div><div class="float_left">
                        <%= Html.RadioButton("ResumptionOfCare_GenericCareGiverGlucometerUse", "2", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "2" ? true : false, new { @id = "ResumptionOfCare_GenericCareGiverGlucometerUse2", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericCareGiverGlucometerUse2" class="inlineradio">N/A, no caregiver</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="strong">Does patient have any of the following?</div>
                <% string[] patientEdocrineProblem = data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer != "" ? data["GenericPatientEdocrineProblem"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericPatientEdocrineProblem" value="" />
                <div class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem1' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='1' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("1")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem1" class="fixed inlineradio">Polyuria</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem2' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='2' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("2")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem2" class="fixed inlineradio">Polydipsia</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem3' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='3' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("3")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem3" class="fixed inlineradio">Polyphagia</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem4' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='4' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("4")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem4" class="fixed inlineradio">Neuropathy</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem5' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='5' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("5")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem5" class="fixed inlineradio">Radiculopathy</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericPatientEdocrineProblem6' class='radio float_left' type='checkbox' name='ResumptionOfCare_GenericPatientEdocrineProblem' value='6' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("6")? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericPatientEdocrineProblem6" class="fixed inlineradio">Thyroid problems</label>
                    </div>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericBloodSugarLevelText" class="float_left">Blood Sugar:</label>
                <div class="float_right">
                    <%= Html.TextBox("ResumptionOfCare_GenericBloodSugarLevelText", data.ContainsKey("GenericBloodSugarLevelText") ? data["GenericBloodSugarLevelText"].Answer : "", new { @id = "ResumptionOfCare_GenericBloodSugarLevelText", @class = "st numeric", @maxlength = "5" })%>
                    <%= Html.Hidden("ResumptionOfCare_GenericBloodSugarLevel") %>
                    <label>mg/dl</label>
                </div>
                <div class="clear"></div>
                <div class="float_right">
                    <%= Html.RadioButton("ResumptionOfCare_GenericBloodSugarLevel", "Random", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Random" ? true : false, new { @id = "ResumptionOfCare_GenericBloodSugarLevelRandom", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericBloodSugarLevelRandom" class="inlineradio">Random</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericBloodSugarLevel", "Fasting", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Fasting" ? true : false, new { @id = "ResumptionOfCare_GenericBloodSugarLevelFasting", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericBloodSugarLevelFasting" class="inlineradio">Fasting</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericBloodSugarCheckedBy" class="float_left">Blood sugar checked by:</label>
                <div class="float_right">
                    <%  var diabeticCarePerformedby = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Patient", Value = "1" },
                            new SelectListItem { Text = "SN", Value = "2" },
                            new SelectListItem { Text = "Caregiver", Value = "3" }
                        }, "Value", "Text", data.ContainsKey("GenericBloodSugarCheckedBy") ? data["GenericBloodSugarCheckedBy"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = "ResumptionOfCare_GenericBloodSugarCheckedBy" })%>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericEndocrineComments" class="strong">Comments:</label>
                <%= Html.TextArea("ResumptionOfCare_GenericEndocrineComments", data.ContainsKey("GenericEndocrineComments") ? data["GenericEndocrineComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericEndocrineComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] endocrineInterventions = data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer != "" ? data["485EndocrineInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485EndocrineInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions1' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='1' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("1") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="ResumptionOfCare_485EndocrineInterventions1">SN to assess</label>
                    <%  var abilityToManageDiabetic = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485AbilityToManageDiabetic") && data["485AbilityToManageDiabetic"].Answer != "" ? data["485AbilityToManageDiabetic"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485AbilityToManageDiabetic", abilityToManageDiabetic)%>
                    <label for="ResumptionOfCare_485EndocrineInterventions1">ability to manage diabetic disease process.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions2' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='2' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EndocrineInterventions2" class="radio">SN to perform BG checks every visit &amp; PRN.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions3' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='3' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EndocrineInterventions3" class="radio">SN to instruct on insulin preparation, administration, site rotation and disposal of supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions4' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='4' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("4") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EndocrineInterventions4" class="radio">SN to assess/instruct on diabetic management to include: nail, skin &amp; foot care, medication administration and proper diet.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions5' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='5' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("5") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="ResumptionOfCare_485EndocrineInterventions5">SN to administer</label>
                    <%= Html.TextBox("ResumptionOfCare_485AmountOfInsuline", data.ContainsKey("485AmountOfInsuline") ? data["485AmountOfInsuline"].Answer : "", new { @id = "ResumptionOfCare_485AmountOfInsuline", @maxlength = "30" })%>
                    <label for="ResumptionOfCare_485EndocrineInterventions5">insulin as follows:</label>
                    <%= Html.TextBox("ResumptionOfCare_485AmountOfInsulineAsFollows", data.ContainsKey("485AmountOfInsulineAsFollows") ? data["485AmountOfInsulineAsFollows"].Answer : "", new { @id = "ResumptionOfCare_485AmountOfInsulineAsFollows", @maxlength = "50" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineInterventions6' class='radio float_left' name='ResumptionOfCare_485EndocrineInterventions' value='6' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("6") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="ResumptionOfCare_485EndocrineInterventions6">SN to prefill syringes with</label>
                    <%= Html.TextBox("ResumptionOfCare_485PrefillOfInsuline", data.ContainsKey("485PrefillOfInsuline") ? data["485PrefillOfInsuline"].Answer : "", new { @id = "ResumptionOfCare_485PrefillOfInsuline", @maxlength = "30" })%>
                    <label for="ResumptionOfCare_485EndocrineInterventions6">insulin as follows:</label>
                    <%= Html.TextBox("ResumptionOfCare_485PrefillOfInsulineAsFollows", data.ContainsKey("485PrefillOfInsulineAsFollows") ? data["485PrefillOfInsulineAsFollows"].Answer : "", new { @id = "ResumptionOfCare_485PrefillOfInsulineAsFollows", @maxlength = "50" })%>.
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485EndocrineOrderTemplates" class="strong">Additional Orders:</label>
                <%  var endocrineOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EndocrineOrderTemplates") && data["485EndocrineOrderTemplates"].Answer != "" ? data["485EndocrineOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("ResumptionOfCare_485EndocrineOrderTemplates", endocrineOrderTemplates)%>
                <%= Html.TextArea("ResumptionOfCare_485EndocrineInterventionComments", data.ContainsKey("485EndocrineInterventionComments") ? data["485EndocrineInterventionComments"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_485EndocrineInterventionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] endocrineGoals = data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer != "" ? data["485EndocrineGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485EndocrineGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals1' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='1' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("1")? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485EndocrineGoals1" class="radio">Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals2' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='2' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("2")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var glucometerUseIndependencePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GlucometerUseIndependencePerson") && data["485GlucometerUseIndependencePerson"].Answer != "" ? data["485GlucometerUseIndependencePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485GlucometerUseIndependencePerson", glucometerUseIndependencePerson) %>
                    <label for="ResumptionOfCare_485EndocrineGoals2">will be independent with glucometer use by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals3' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='3' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("3")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var independentInsulinAdministrationPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485IndependentInsulinAdministrationPerson") && data["485IndependentInsulinAdministrationPerson"].Answer != "" ? data["485IndependentInsulinAdministrationPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485IndependentInsulinAdministrationPerson", independentInsulinAdministrationPerson) %>
                    <label for="ResumptionOfCare_485EndocrineGoals3">will be independent with insulin administration by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals4' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='4' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("4")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var verbalizeProperFootCarePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeProperFootCarePerson") && data["485VerbalizeProperFootCarePerson"].Answer != "" ? data["485VerbalizeProperFootCarePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485VerbalizeProperFootCarePerson", verbalizeProperFootCarePerson) %>
                    <label for="ResumptionOfCare_485EndocrineGoals4">will verbalize understanding of proper diabetic foot care by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals5' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='5' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("5")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var diabetesManagement = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485DiabetesManagement") && data["485DiabetesManagement"].Answer != "" ? data["485DiabetesManagement"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DiabetesManagement", diabetesManagement)%>
                    <label for="ResumptionOfCare_485EndocrineGoals5">will verbalize knowledge of diabetes management, S&amp;S of complications, hypo/hyperglycemia, foot care and management during illness or stress by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals6' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='6' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("6")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var generalDemonstration = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GeneralDemonstration") && data["485GeneralDemonstration"].Answer != "" ? data["485GeneralDemonstration"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485GeneralDemonstration", generalDemonstration)%>
                    <label for="ResumptionOfCare_485EndocrineGoals6">will demonstrate correct insulin preparation, injection procedure, storage and disposal of supplies by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EndocrineGoals7' class='radio float_left' name='ResumptionOfCare_485EndocrineGoals' value='7' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("7")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var generalVerbalization = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GeneralVerbalization") && data["485GeneralVerbalization"].Answer != "" ? data["485GeneralVerbalization"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485GeneralVerbalization", generalVerbalization)%>
                    <label for="ResumptionOfCare_485EndocrineGoals7">will verbalize understanding of the importance of keeping blood glucose levels within parameters by the end of the episode.</label>
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485EndocrineGoalTemplates" class="strong">Additional Goals:</label>
                <%  var endocrineGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EndocrineGoalTemplates") && data["485EndocrineGoalTemplates"].Answer != "" ? data["485EndocrineGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485EndocrineGoalTemplates", endocrineGoalTemplates) %>
                <%= Html.TextArea("ResumptionOfCare_485EndocrineGoalComments", data.ContainsKey("485EndocrineGoalComments") ? data["485EndocrineGoalComments"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_485EndocrineGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfRadioEquals("ResumptionOfCare_GenericPatientDiabetic", "1", $("#window_resumptionofcare .diabetic"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>