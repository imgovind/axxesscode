﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareNutritionForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<%= Html.Hidden("categoryType", "Nutrition")%>
<div class="wrapper main">
    <fieldset>
        <legend>Nutrition</legend>
        <input type="hidden" name="ResumptionOfCare_GenericNutrition" value=" " />
        <% string[] genericNutrition = data.ContainsKey("GenericNutrition") && data["GenericNutrition"].Answer != "" ? data["GenericNutrition"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition1' name='ResumptionOfCare_GenericNutrition' value='1' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNutrition1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition2' name='ResumptionOfCare_GenericNutrition' value='2' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNutrition2" class="radio">Dysphagia</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition3' name='ResumptionOfCare_GenericNutrition' value='3' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNutrition3" class="radio">Appetite</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition4' name='ResumptionOfCare_GenericNutrition' value='4' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNutrition4" class="radio">Weight</label>
                </div><div id="ResumptionOfCare_GenericNutrition4More" class="float_right">
                    <%=Html.Hidden("ResumptionOfCare_GenericNutritionWeightGainLoss", " ", new { @id = "" })%>
                    <%=Html.RadioButton("ResumptionOfCare_GenericNutritionWeightGainLoss", "Loss", data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Loss" ? true : false, new { @id = "GenericNutritionWeightLoss", @class = "radio" })%>
                    <label for="GenericNutritionWeightLoss" class="inlineradio">Loss</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericNutritionWeightGainLoss", "Gain", data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Gain" ? true : false, new { @id = "GenericNutritionWeightGain", @class = "radio" })%>
                    <label for="GenericNutritionWeightGain" class="inlineradio">Gain</label>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition5' name='ResumptionOfCare_GenericNutrition' value='5' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("5") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNutrition5" class="radio">Diet</label>
                </div><div id="ResumptionOfCare_GenericNutrition5More" class="float_right">
                    <%=Html.Hidden("ResumptionOfCare_GenericNutritionDietAdequate", " ", new { @id = "" })%>
                    <%=Html.RadioButton("ResumptionOfCare_GenericNutritionDietAdequate", "Adequate", data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Adequate" ? true : false, new { @id = "ResumptionOfCare_GenericNutritionDietAdequate", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericNutritionDietAdequate" class="inlineradio">Adequate</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericNutritionDietAdequate", "Inadequate", data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Inadequate" ? true : false, new { @id = "ResumptionOfCare_GenericNutritionDietInadequate", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericNutritionDietInadequate" class="inlineradio">Inadequate</label>
                    <% string[] genericNutritionDiet = data.ContainsKey("GenericNutritionDiet") && data["GenericNutritionDiet"].Answer != "" ? data["GenericNutritionDiet"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericNutritionDiet" />
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition6' name='ResumptionOfCare_GenericNutrition' value='6' type='checkbox' {0} />", genericNutrition!=null && genericNutrition.Contains("6") ? "checked='checked'" : "" ) %>
                    <label class="radio" for="ResumptionOfCare_GenericNutrition6">Enteral Feeding</label>
                </div>
                <input type="hidden" name="ResumptionOfCare_GenericNutritionEnteralFeeding" value=" " />
                <%string[] genericNutritionEnteralFeeding = data.ContainsKey("GenericNutritionEnteralFeeding") && data["GenericNutritionEnteralFeeding"].Answer != "" ? data["GenericNutritionEnteralFeeding"].Answer.Split(',') : null; %>
                <div id="ResumptionOfCare_GenericNutrition6More" class="margin float_right">
                    <div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionEnteralFeeding1' name='ResumptionOfCare_GenericNutritionEnteralFeeding' value='1' type='checkbox' {0} />", genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("1") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNutritionEnteralFeeding1" class="inlineradio fixed">NG</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionEnteralFeeding2' name='ResumptionOfCare_GenericNutritionEnteralFeeding' value='2' type='checkbox' {0} />", genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("2") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNutritionEnteralFeeding2" class="inlineradio fixed">PEG</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionEnteralFeeding3' name='ResumptionOfCare_GenericNutritionEnteralFeeding' value='3' type='checkbox' {0} />", genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("3") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNutritionEnteralFeeding3" class="inlineradio fixed">Dobhoff</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition7' name='ResumptionOfCare_GenericNutrition' value='7' type='checkbox' {0} />", genericNutrition != null && genericNutrition.Contains("7") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericNutrition7" class="radio">Tube Placement Checked</label>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutrition8' name='ResumptionOfCare_GenericNutrition' value='8' type='checkbox' {0} />", genericNutrition != null && genericNutrition.Contains("8") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericNutrition8" class="radio">Residual Checked</label>
                </div><div id="ResumptionOfCare_GenericNutrition8More" class="float_right">
                    <label for="ResumptionOfCare_GenericNutritionResidualCheckedAmount">Amount:</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericNutritionResidualCheckedAmount", data.ContainsKey("GenericNutritionResidualCheckedAmount") ? data["GenericNutritionResidualCheckedAmount"].Answer : "", new { @id = "ResumptionOfCare_GenericNutritionResidualCheckedAmount", @class = "vitals numeric", @maxlength = "5" })%>
                    <label for="ResumptionOfCare_GenericNutritionResidualCheckedAmount">ml</label>
                    <% string[] genericNutritionResidualProblem = data.ContainsKey("GenericNutritionResidualProblem") && data["GenericNutritionResidualProblem"].Answer != "" ? data["GenericNutritionResidualProblem"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericNutritionResidualProblem" value=" " />
                </div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="ResumptionOfCare_GenericNutritionComments" class="strong">Comments:</label>
                <div><%=Html.TextArea("ResumptionOfCare_GenericNutritionComments", data.ContainsKey("GenericNutritionComments") ? data["GenericNutritionComments"].Answer : "", 10, 50, new { @id = "ResumptionOfCare_GenericNutritionCommentsComments" })%></div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Nutritional Health Screen</legend>
        <%string[] genericNutritionalHealth = data.ContainsKey("GenericNutritionalHealth") && data["GenericNutritionalHealth"].Answer != "" ? data["GenericNutritionalHealth"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericNutritionalHealth" value=" " />
        <div class="column">
            <div class="row">
                <table class="form layout_auto">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth1' name='ResumptionOfCare_GenericNutritionalHealth' value='1' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth1" class="radio">Without reason, has lost more than 10 lbs, in the last 3 months</label>
                            </td><td>
                                <label>15</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth2' name='ResumptionOfCare_GenericNutritionalHealth' value='2' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth2" class="radio">Has an illness or condition that made pt change the type and/or amount of food eaten</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth3' name='ResumptionOfCare_GenericNutritionalHealth' value='3' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth3" class="radio">Has open decubitus, ulcer, burn or wound</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth4' name='ResumptionOfCare_GenericNutritionalHealth' value='4' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth4" class="radio">Eats fewer than 2 meals a day</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth5' name='ResumptionOfCare_GenericNutritionalHealth' value='5' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth5" class="radio">Has a tooth/mouth problem that makes it hard to eat</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth6' name='ResumptionOfCare_GenericNutritionalHealth' value='6' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth6" class="radio">Has 3 or more drinks of beer, liquor or wine almost every day</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth7' name='ResumptionOfCare_GenericNutritionalHealth' value='7' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth7" class="radio">Does not always have enough money to buy foods needed</label>
                            </td><td>
                                <label>10</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth8' name='ResumptionOfCare_GenericNutritionalHealth' value='8' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth8" class="radio">Eats few fruits or vegetables, or milk products</label>
                            </td><td>
                                <label>5</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth9' name='ResumptionOfCare_GenericNutritionalHealth' value='9' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth9" class="radio">Eats alone most of the time</label>
                            </td><td>
                                <label>5</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth10' name='ResumptionOfCare_GenericNutritionalHealth' value='10' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("10") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth10" class="radio">Takes 3 or more prescribed or OTC medications a day</label>
                            </td><td>
                                <label>5</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth11' name='ResumptionOfCare_GenericNutritionalHealth' value='11' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("11") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth11" class="radio">Is not always physically able to cook and/or feed self and has no caregiver to assist</label>
                            </td><td>
                                <label>5</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionalHealth12' name='ResumptionOfCare_GenericNutritionalHealth' value='12' type='checkbox' {0} />", genericNutritionalHealth!=null && genericNutritionalHealth.Contains("12") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNutritionalHealth12" class="radio">Frequently has diarrhea or constipation</label>
                            </td><td>
                                <label>5</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><div class="column">
            <div class="row">
                <% string[] genericGoodNutritionScore = data.ContainsKey("GenericGoodNutritionScore") && data["GenericGoodNutritionScore"].Answer != "" ? data["GenericGoodNutritionScore"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericGoodNutritionScore" value="" />
                <div class="float_left pad strong">Total:<br /><input type="text" class="mi" readonly="readonly" value="0" id="ResumptionOfCare_NutritionalHealthScreenTotal" /></div>
                <div>
                    <label id="ResumptionOfCare_GoodNutritionalStatus" class="radio">Good Nutritional Status (Score 0 &ndash; 25)</label>
                    <label id="ResumptionOfCare_ModerateNutritionalRisk" class="radio">Moderate Nutritional Risk (Score 25 &ndash; 55)</label>
                    <label id="ResumptionOfCare_HighNutritionalRisk" class="radio">High Nutritional Risk (Score 55 &ndash; 100)</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericNutritionalStatusComments" class="strong">Nutritional Status Comments:</label>
                <%=Html.TextArea("ResumptionOfCare_GenericNutritionalStatusComments", data.ContainsKey("GenericNutritionalStatusComments") ? data["GenericNutritionalStatusComments"].Answer : "",10, 50, new { @id = "ResumptionOfCare_GenericNutritionalStatusComments" })%>
            </div><div class="row">
                <% string[] genericNutritionDiffect = data.ContainsKey("GenericNutritionDiffect") && data["GenericNutritionDiffect"].Answer != "" ? data["GenericNutritionDiffect"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericNutritionDiffect" value=" " />
                <div>
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionDiffect1' name='ResumptionOfCare_GenericNutritionDiffect' value='1' type='checkbox' {0} />", genericNutritionDiffect!=null && genericNutritionDiffect.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNutritionDiffect1" class="radio">Non-compliant with prescribed diet</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_GenericNutritionDiffect2' name='ResumptionOfCare_GenericNutritionDiffect' value='2' type='checkbox' {0} />", genericNutritionDiffect!=null && genericNutritionDiffect.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNutritionDiffect2" class="radio">Over/under weight by 10%</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericMealsPreparedBy" class="strong">Meals prepared by:</label>
                <%=Html.TextArea("ResumptionOfCare_GenericMealsPreparedBy", data.ContainsKey("GenericMealsPreparedBy") ? data["GenericMealsPreparedBy"].Answer : "", 10, 50, new { @id = "ResumptionOfCare_GenericMealsPreparedBy" })%>
            </div>
        </div>
    </fieldset><fieldset class="loc485">
        <legend>Enter Physician&rsquo;s Orders or Diet Requirements (Locator #16)</legend>
        <% string[] nutritionalReqs = data.ContainsKey("485NutritionalReqs") && data["485NutritionalReqs"].Answer != "" ? data["485NutritionalReqs"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485NutritionalReqs" value=" " />
        <div class="column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs1' name='ResumptionOfCare_485NutritionalReqs' value='1' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs1" class="radio">Regular</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs2' name='ResumptionOfCare_485NutritionalReqs' value='2' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs2" class="radio">Mechanical Soft</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs3' name='ResumptionOfCare_485NutritionalReqs' value='3' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs3" class="radio">Heart Healthy</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs4' name='ResumptionOfCare_485NutritionalReqs' value='4' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs4" class="radio">Low Cholesterol</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs5' name='ResumptionOfCare_485NutritionalReqs' value='5' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs5" class="radio">Low Fat</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs6' name='ResumptionOfCare_485NutritionalReqs' value='6' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsSodiumAmount", data.ContainsKey("485NutritionalReqsSodiumAmount") ? data["485NutritionalReqsSodiumAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsSodiumAmount", @class = "vitals", @maxlength = "10" })%>
                    <label for="ResumptionOfCare_485NutritionalReqs6">Sodium</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs7' name='ResumptionOfCare_485NutritionalReqs' value='7' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs7" class="radio">No Added Salt</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs8' name='ResumptionOfCare_485NutritionalReqs' value='8' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("8") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsCalorieADADietAmount", data.ContainsKey("485NutritionalReqsCalorieADADietAmount") ? data["485NutritionalReqsCalorieADADietAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsCalorieADADietAmount", @class = "vitals", @maxlength = "10" })%>
                    <label for="ResumptionOfCare_485NutritionalReqs8">Calorie ADA Diet</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs9' name='ResumptionOfCare_485NutritionalReqs' value='9' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs9" class="radio">No Concentrated Sweets</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs10' name='ResumptionOfCare_485NutritionalReqs' value='10' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("10") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs10" class="radio">Coumadin Diet</label>
            </div>
        </div><div class="column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs11' name='ResumptionOfCare_485NutritionalReqs' value='11' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("11") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionalReqs11" class="radio">Renal Diet</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs12' name='ResumptionOfCare_485NutritionalReqs' value='12' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("12") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485NutritionalReqs12" class="radio">Other:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsPhyDietOtherName", data.ContainsKey("485NutritionalReqsPhyDietOtherName") ? data["485NutritionalReqsPhyDietOtherName"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsPhyDietOtherName", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs13' name='ResumptionOfCare_485NutritionalReqs' value='13' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("13") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485NutritionalReqs13" class="radio">Supplement:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsSupplementType", data.ContainsKey("485NutritionalReqsSupplementType") ? data["485NutritionalReqsSupplementType"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsSupplementType", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs14' name='ResumptionOfCare_485NutritionalReqs' value='14' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("14") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485NutritionalReqs14" class="radio">Fluid Restriction</label>
                </div><div id="ResumptionOfCare_485NutritionalReqs14More" class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsFluidResAmount", data.ContainsKey("485NutritionalReqsFluidResAmount") ? data["485NutritionalReqsFluidResAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsFluidResAmount", @class = "st numeric", @maxlength = "5" })%>
                    <label for="ResumptionOfCare_485NutritionalReqsFluidResAmount">ml/24 hours</label>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs15' name='ResumptionOfCare_485NutritionalReqs' value='15' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("15") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485NutritionalReqs15" class="radio">Enteral Nutrition</label>
                </div><div id="ResumptionOfCare_485NutritionalReqs15More" class="rel float_right">
                    <div class="float_right">
                        <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralDesc", data.ContainsKey("485NutritionalReqsEnteralDesc") ? data["485NutritionalReqsEnteralDesc"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralDesc", @class = "st", @maxlength = "15" })%>
                        <label for="ResumptionOfCare_485NutritionalReqsEnteralDesc" class="inlineradio">(formula)</label>
                        <label for="ResumptionOfCare_485NutritionalReqsEnteralAmount">Amount</label>
                        <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralAmount", data.ContainsKey("485NutritionalReqsEnteralAmount") ? data["485NutritionalReqsEnteralAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralAmount", @class = "vitals numeric", @maxlength = "5" })%>
                        <label for="ResumptionOfCare_485NutritionalReqsEnteralAmount">ml/day</label>
                        <% string[] nutritionalReqsEnteral = data.ContainsKey("485NutritionalReqsEnteral") && data["485NutritionalReqsEnteral"].Answer != "" ? data["485NutritionalReqsEnteral"].Answer.Split(',') : null; %>
                        <input name="ResumptionOfCare_485NutritionalReqsEnteral" value=" " type="hidden" />
                    </div>
                    <div class="clear"></div>
                    <div class="margin">
                        <div class="float_left">Per:</div>
                        <div class="float_right">
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralPer1' name='ResumptionOfCare_485NutritionalReqsEnteral' value='1' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("1") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralPer1" class="inlineradio">PEG</label>
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralPer2' name='ResumptionOfCare_485NutritionalReqsEnteral' value='2' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("2") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralPer2" class="inlineradio">NG</label>
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralPer3' name='ResumptionOfCare_485NutritionalReqsEnteral' value='3' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("3") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralPer3" class="inlineradio">Dobhoff</label>
                            <br />
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralPer4' name='ResumptionOfCare_485NutritionalReqsEnteral' value='4' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("4") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralPer4" class="inlineradio">Other</label>
                            <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsEnteralOtherName", data.ContainsKey("485NutritionalReqsEnteralOtherName") ? data["485NutritionalReqsEnteralOtherName"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsEnteralOtherName", @class = "mediumWidth", @maxlength = "20" })%>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">Via:</div>
                        <div class="float_right">
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralVia1' name='ResumptionOfCare_485NutritionalReqsEnteral' value='5' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("5") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralVia1" class="inlineradio">Pump</label>
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralVia2' name='ResumptionOfCare_485NutritionalReqsEnteral' value='6' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("6") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralVia2" class="inlineradio">Gravity</label>
                            <%= string.Format("<input class='radio' id='ResumptionOfCare_485NutritionalReqsEnteralVia3' name='ResumptionOfCare_485NutritionalReqsEnteral' value='7' type='checkbox' {0} />", nutritionalReqsEnteral!=null && nutritionalReqsEnteral.Contains("7") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485NutritionalReqsEnteralVia3" class="inlineradio">Bolus</label>
                        </div>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485NutritionalReqs16' name='ResumptionOfCare_485NutritionalReqs' value='16' type='checkbox' {0} />", nutritionalReqs!=null && nutritionalReqs.Contains("16") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485NutritionalReqs16" class="radio">TPN</label>
                </div><div id="ResumptionOfCare_485NutritionalReqs16More" class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsTPNAmount", data.ContainsKey("485NutritionalReqsTPNAmount") ? data["485NutritionalReqsTPNAmount"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsTPNAmount", @class = "st", @maxlength = "15" })%>
                    <label for="ResumptionOfCare_485NutritionalReqsTPNAmount">@ml/hr</label>
                    <label for="ResumptionOfCare_485NutritionalReqsTPNVia">via</label>
                    <%=Html.TextBox("ResumptionOfCare_485NutritionalReqsTPNVia", data.ContainsKey("485NutritionalReqsTPNVia") ? data["485NutritionalReqsTPNVia"].Answer : "", new { @id = "ResumptionOfCare_485NutritionalReqsTPNVia", @class = "st", @maxlength = "15" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] nutritionInterventions = data.ContainsKey("485NutritionInterventions") && data["485NutritionInterventions"].Answer != "" ? data["485NutritionInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485NutritionInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions1' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='1' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionInterventions1">SN to instruct</label>
                    <%  var instructOnDietPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructOnDietPerson") && data["485InstructOnDietPerson"].Answer != "" ? data["485InstructOnDietPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructOnDietPerson", instructOnDietPerson)%>
                    <label for="ResumptionOfCare_485NutritionInterventions1">on</label>
                    <%=Html.TextBox("ResumptionOfCare_485InstructOnDietDesc", data.ContainsKey("485InstructOnDietDesc") ? data["485InstructOnDietDesc"].Answer : "", new { @id = "ResumptionOfCare_485InstructOnDietDesc" })%>
                    <label for="ResumptionOfCare_485NutritionInterventions1">diet.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions2' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='2' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionInterventions2" class="radio">SN to assess patient for diet compliance.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions3' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='3' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485NutritionInterventions3" class="radio">SN to instruct on proper technique for tube feeding, aspiration precautions and care of feeding tube site.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions4' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='4' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionInterventions4">SN to instruct the</label>
                    <%  var instructEnteralNutritionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructEnteralNutritionPerson") && data["485InstructEnteralNutritionPerson"].Answer != "" ? data["485InstructEnteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructEnteralNutritionPerson", instructEnteralNutritionPerson)%>
                    <label for="ResumptionOfCare_485NutritionInterventions4">on enteral nutrition and the care/use of equipment.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions5' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='5' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionInterventions5">SN to instruct the</label>
                    <%  var instructCareOfTubePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructCareOfTubePerson") && data["485InstructCareOfTubePerson"].Answer != "" ? data["485InstructCareOfTubePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructCareOfTubePerson", instructCareOfTubePerson)%>
                    <label for="ResumptionOfCare_485NutritionInterventions5">on proper care of</label>
                    <%=Html.TextBox("ResumptionOfCare_485InstructCareOfTubeDesc", data.ContainsKey("485InstructCareOfTubeDesc") ? data["485InstructCareOfTubeDesc"].Answer : "", new { @id = "ResumptionOfCare_485InstructCareOfTubeDesc" })%>
                    <label for="ResumptionOfCare_485NutritionInterventions5">tube.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionInterventions6' class='radio float_left' name='ResumptionOfCare_485NutritionInterventions' value='6' type='checkbox' {0} />", nutritionInterventions!=null && nutritionInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionInterventions6">SN to instruct the</label>
                    <%  var instructFreeWaterPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructFreeWaterPerson") && data["485InstructFreeWaterPerson"].Answer != "" ? data["485InstructFreeWaterPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructFreeWaterPerson", instructFreeWaterPerson)%>
                    <label for="ResumptionOfCare_485NutritionInterventions6">to give</label>
                    <%=Html.TextBox("ResumptionOfCare_485InstructFreeWaterAmount", data.ContainsKey("485InstructFreeWaterAmount") ? data["485InstructFreeWaterAmount"].Answer : "", new { @id = "ResumptionOfCare_485InstructFreeWaterAmount", @class = "st" })%>
                    <label for="ResumptionOfCare_485NutritionInterventions6">cc of free water every</label>
                    <%=Html.TextBox("ResumptionOfCare_485InstructFreeWaterEvery", data.ContainsKey("485InstructFreeWaterEvery") ? data["485InstructFreeWaterEvery"].Answer : "", new { @id = "ResumptionOfCare_485InstructFreeWaterEvery", @class = "st" })%>.
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485NutritionOrderTemplates" class="strong">Additional Orders:</label>
                <%  var nutritionOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485NutritionOrderTemplates") && data["485NutritionOrderTemplates"].Answer != "" ? data["485NutritionOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485NutritionOrderTemplates", nutritionOrderTemplates)%>
                <%=Html.TextArea("ResumptionOfCare_485NutritionComments", data.ContainsKey("485NutritionComments") ? data["485NutritionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485NutritionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] nutritionGoals = data.ContainsKey("485NutritionGoals") && data["485NutritionGoals"].Answer != "" ? data["485NutritionGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485NutritionGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionGoals1' class='radio float_left' name='ResumptionOfCare_485NutritionGoals' value='1' type='checkbox' {0} />", nutritionGoals!=null && nutritionGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionGoals1">Patient will maintain</label>
                    <%=Html.TextBox("ResumptionOfCare_485MaintainDietComplianceType", data.ContainsKey("485MaintainDietComplianceType") ? data["485MaintainDietComplianceType"].Answer : "", new { @id = "ResumptionOfCare_485MaintainDietComplianceType" })%>
                    <label for="ResumptionOfCare_485NutritionGoals1">diet compliance during the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485NutritionGoals2' class='radio float_left' name='ResumptionOfCare_485NutritionGoals' value='2' type='checkbox' {0} />", nutritionGoals!=null && nutritionGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485NutritionGoals2">The</label>
                    <%  var demonstrateEnteralNutritionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485DemonstrateEnteralNutritionPerson") && data["485DemonstrateEnteralNutritionPerson"].Answer != "" ? data["485DemonstrateEnteralNutritionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485DemonstrateEnteralNutritionPerson", demonstrateEnteralNutritionPerson)%>
                    <label for="ResumptionOfCare_485NutritionGoals2">will demonstrate proper care/use of enteral nutrition equipment by</label>
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateEnteralNutritionDate", data.ContainsKey("485DemonstrateEnteralNutritionDate") ? data["485DemonstrateEnteralNutritionDate"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateEnteralNutritionDate", @class = "st" })%>.
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485NutritionGoalTemplates" class="strong">Additional Goals:</label>
                <%  var nutritionGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485NutritionGoalTemplates") && data["485NutritionGoalTemplates"].Answer != "" ? data["485NutritionGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485NutritionGoalTemplates", nutritionGoalTemplates)%>
                <%= Html.TextArea("ResumptionOfCare_485NutritionGoalComments", data.ContainsKey("485NutritionGoalComments") ? data["485NutritionGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485NutritionGoalComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.CalculateNutritionScore('ResumptionOfCare');
    $("input[name=ResumptionOfCare_GenericNutritionalHealth]").change(function() { Oasis.CalculateNutritionScore('ResumptionOfCare'); });
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNutrition4"), $("#ResumptionOfCare_GenericNutrition4More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNutrition5"), $("#ResumptionOfCare_GenericNutrition5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNutrition6"), $("#ResumptionOfCare_GenericNutrition6More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNutrition8"), $("#ResumptionOfCare_GenericNutrition8More"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs6"), $("#ResumptionOfCare_485NutritionalReqsSodiumAmount"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs8"), $("#ResumptionOfCare_485NutritionalReqsCalorieADADietAmount"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs12"), $("#ResumptionOfCare_485NutritionalReqsPhyDietOtherName"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs13"), $("#ResumptionOfCare_485NutritionalReqsSupplementType"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs14"), $("#ResumptionOfCare_485NutritionalReqs14More"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs15"), $("#ResumptionOfCare_485NutritionalReqs15More"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqsEnteralPer4"), $("#ResumptionOfCare_485NutritionalReqsEnteralOtherName"));
    Oasis.showIfChecked($("#ResumptionOfCare_485NutritionalReqs16"), $("#ResumptionOfCare_485NutritionalReqs16More"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>