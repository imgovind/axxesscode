﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCarePatientHistoryForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<%= Html.Hidden("categoryType", "PatientHistory")%>
<div class="wrapper main">
    <fieldset class="half float_left loc485">
        <legend>Allergies (Locator #17)</legend>
        <div class="column">
            <div class="row">
                <%= Html.Hidden("ResumptionOfCare_485Allergies") %>
                <%= Html.RadioButton("ResumptionOfCare_485Allergies", "No", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? true : false, new { @id = "ROC_Allergies_NKA", @class = "radio" })%>
                <label for="ROC_Allergies_NKA">NKA (Food/Drugs/Latex)</label>
            </div>
        </div><div class="column">
            <div class="row">
                <%= Html.RadioButton("ResumptionOfCare_485Allergies", "Yes", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "Yes" ? true : false, new { @id = "ROC_Allergies_Specify", @class = "radio" })%>
                <label for="ROC_Allergies_Specify">Allergic to:</label>
                <%= Html.TextArea("ResumptionOfCare_485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "", new { @id = "ResumptionOfCare_485AllergiesDescription" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Vital Sign Parameters:</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Temperature</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericTempGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericTempGreaterThan", data.ContainsKey("GenericTempGreaterThan") ? data["GenericTempGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericTempGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericTempLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericTempLessThan", data.ContainsKey("GenericTempLessThan") ? data["GenericTempLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericTempLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Pulse</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericPulseGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericPulseGreaterThan", data.ContainsKey("GenericPulseGreaterThan") ? data["GenericPulseGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericPulseLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericPulseLessThan", data.ContainsKey("GenericPulseLessThan") ? data["GenericPulseLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Respirations</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericRespirationGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericRespirationGreaterThan", data.ContainsKey("GenericRespirationGreaterThan") ? data["GenericRespirationGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericRespirationLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericRespirationLessThan", data.ContainsKey("GenericRespirationLessThan") ? data["GenericRespirationLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Systolic BP</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericSystolicBPGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericSystolicBPGreaterThan", data.ContainsKey("GenericSystolicBPGreaterThan") ? data["GenericSystolicBPGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericSystolicBPGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericSystolicBPLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericSystolicBPLessThan", data.ContainsKey("GenericSystolicBPLessThan") ? data["GenericSystolicBPLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRespirationGreaterThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Diastolic BP</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericDiastolicBPGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericDiastolicBPGreaterThan", data.ContainsKey("GenericDiastolicBPGreaterThan") ? data["GenericDiastolicBPGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericDiastolicBPGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericDiastolicBPLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericDiastolicBPLessThan", data.ContainsKey("GenericDiastolicBPLessThan") ? data["GenericDiastolicBPLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericDiastolicBPLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">O<sub>2</sub> Sat (percent)</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_Generic02SatLessThan">less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_Generic02SatLessThan", data.ContainsKey("Generic02SatLessThan") ? data["Generic02SatLessThan"].Answer : "", new { @id = "ResumptionOfCare_Generic02SatLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Fasting blood sugar</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericFastingBloodSugarGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericFastingBloodSugarGreaterThan", data.ContainsKey("GenericFastingBloodSugarGreaterThan") ? data["GenericFastingBloodSugarGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericFastingBloodSugarGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericFastingBloodSugarLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericFastingBloodSugarLessThan", data.ContainsKey("GenericFastingBloodSugarLessThan") ? data["GenericFastingBloodSugarLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericFastingBloodSugarLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Random blood sugar</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericRandomBloddSugarGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericRandomBloddSugarGreaterThan", data.ContainsKey("GenericRandomBloddSugarGreaterThan") ? data["GenericRandomBloddSugarGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericRandomBloddSugarGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericRandomBloodSugarLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericRandomBloodSugarLessThan", data.ContainsKey("GenericRandomBloodSugarLessThan") ? data["GenericRandomBloodSugarLessThan"].Answer : "", new { @id = "GenericRandomBloodSugarLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Weight (lbs/week)</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericWeightGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericWeightGreaterThan", data.ContainsKey("GenericWeightGreaterThan") ? data["GenericWeightGreaterThan"].Answer : "", new { @id = "ResumptionOfCare_GenericWeightGreaterThan", @class = "vitals" })%>
                    <label="ResumptionOfCare_GenericWeightLessThan">or less than (<)</label>
                    <%=Html.TextBox("ResumptionOfCare_GenericWeightLessThan", data.ContainsKey("GenericWeightLessThan") ? data["GenericWeightLessThan"].Answer : "", new { @id = "ResumptionOfCare_GenericWeightLessThan", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Vital Signs</legend>
        <div class="column">
            <div class="row">
                <label for="ResumptionOfCare_GenericPulseApical" class="float_left">Apical Pulse:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("ResumptionOfCare_GenericPulseApical", data.ContainsKey("GenericPulseApical") ? data["GenericPulseApical"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseApical", @class = "vitals" }) %>
                    <%= Html.Hidden("ResumptionOfCare_GenericPulseApicalRegular") %>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPulseApicalRegular", "1", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "1" ? true : false, new { @id = "GenericPulseApicalRegular", @class = "radio" })%>
                    <label for="GenericPulseApicalRegular">(Reg)</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPulseApicalRegular", "2", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "2" ? true : false, new { @id = "GenericPulseApicalIrregular", @class = "radio" })%>
                    <label for="GenericPulseApicalIrregular">(Irreg)</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericPulseRadial" class="float_left">Radial Pulse:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("ResumptionOfCare_GenericPulseRadial", data.ContainsKey("GenericPulseRadial") ? data["GenericPulseRadial"].Answer : "", new { @id = "ResumptionOfCare_GenericPulseRadial", @class = "vitals" }) %>
                    <%= Html.Hidden("ResumptionOfCare_GenericPulseRadialRegular") %>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPulseRadialRegular", "1", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "1" ? true : false, new { @id = "GenericPulseRadialRegular", @class = "radio" })%>
                    <label for="GenericPulseRadialRegular">(Reg)</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPulseRadialRegular", "2", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "2" ? true : false, new { @id = "GenericPulseRadialIrregular", @class = "radio" })%>
                    <label for="GenericPulseRadialIrregular">(Irreg)</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericHeight" class="float_left">Height:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("ResumptionOfCare_GenericHeight", data.ContainsKey("GenericHeight") ? data["GenericHeight"].Answer : "", new { @id = "ResumptionOfCare_GenericHeight", @class = "vitals" }) %>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericWeight" class="float_left">Weight:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("ResumptionOfCare_GenericWeight", data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer : "", new { @id = "ResumptionOfCare_GenericWeight", @class = "vitals" }) %>
                    <%= Html.Hidden("ResumptionOfCare_GenericWeightActualStated")%>
                    <%= Html.RadioButton("ResumptionOfCare_GenericWeightActualStated", "1", data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericWeightActual", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericWeightActual">Actual</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericWeightActualStated", "2", data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "2" ? true : false, new { @id = "ResumptionOfCare_GenericWeightStated", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericWeightStated">Stated</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericTemp" class="float_left">Temp:</label>
                <div class="float_right oasis"><%= Html.TextBox("ResumptionOfCare_GenericTemp", data.ContainsKey("GenericTemp") ? data["GenericTemp"].Answer : "", new { @id = "ResumptionOfCare_GenericTemp", @class = "vitals" }) %></div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericResp" class="float_left">Resp:</label>
                <div class="float_right oasis"><%= Html.TextBox("ResumptionOfCare_GenericResp", data.ContainsKey("GenericResp") ? data["GenericResp"].Answer : "", new { @id = "ResumptionOfCare_GenericResp", @class = "vitals" }) %></div>
            </div><div class="row">
                <label class="float_left">BP Lying</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericBPLeftLying">Left</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPLeftLying", data.ContainsKey("GenericBPLeftLying") ? data["GenericBPLeftLying"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftLying", @class = "vitals" })%>
                    <label for="ResumptionOfCare_GenericBPRightLying">Right</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPRightLying", data.ContainsKey("GenericBPRightLying") ? data["GenericBPRightLying"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightLying", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">BP Sitting</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericBPLeftSitting">Left</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPLeftSitting", data.ContainsKey("GenericBPLeftSitting") ? data["GenericBPLeftSitting"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftSitting", @class = "vitals" })%>
                    <label for="ResumptionOfCare_GenericBPRightSitting">Right</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPRightSitting", data.ContainsKey("GenericBPRightSitting") ? data["GenericBPRightSitting"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightSitting", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">BP Standing</label>
                <div class="float_right oasis">
                    <label for="ResumptionOfCare_GenericBPLeftStanding">Left</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPLeftStanding", data.ContainsKey("GenericBPLeftStanding") ? data["GenericBPLeftStanding"].Answer : "", new { @id = "ResumptionOfCare_GenericBPLeftStanding", @class = "vitals" })%>
                    <label for="ResumptionOfCare_GenericBPRightStanding">Right</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericBPRightStanding", data.ContainsKey("GenericBPRightStanding") ? data["GenericBPRightStanding"].Answer : "", new { @id = "ResumptionOfCare_GenericBPRightStanding", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Immunizations</legend>
        <div class="column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <label>Pneumonia</label>
                                <%= Html.Hidden("ResumptionOfCare_485Pnemonia") %>
                            </td><td>
                                <select name="ResumptionOfCare_485Pnemonia" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "" || !data.ContainsKey("485Pnemonia") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485PnemoniaDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485PnemoniaDate").Value(data.ContainsKey("485PnemoniaDate") && data["485PnemoniaDate"].Answer.IsNotNullOrEmpty() ? data["485PnemoniaDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485PnemoniaDate", @class = "date" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <label>Flu</label>
                                <%= Html.Hidden("ResumptionOfCare_485Flu")%>
                            </td><td>
                                <select name="ResumptionOfCare_485Flu" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "" || !data.ContainsKey("485Flu") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485FluDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485FluDate").Value(data.ContainsKey("485FluDate") && data["485FluDate"].Answer.IsNotNullOrEmpty() ? data["485FluDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485FluDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td>
                                <label>TB</label>
                                <%= Html.Hidden("ResumptionOfCare_485TB")%>
                            </td><td>
                                <select name="ResumptionOfCare_485TB" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485TB") && data["485TB"].Answer == "" || !data.ContainsKey("485TB") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485TBDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485TBDate").Value(data.ContainsKey("485FluDate") && data["485TBDate"].Answer.IsNotNullOrEmpty() ? data["485TBDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485TBDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td>
                                <label>TB Exposure</label>
                                <%= Html.Hidden("ResumptionOfCare_485TBExposure")%>
                            </td><td>
                                <select name="ResumptionOfCare_485TBExposure" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "" || !data.ContainsKey("485TBExposure") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485TBExposureDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485TBExposureDate").Value(data.ContainsKey("485TBExposureDate") && data["485TBExposureDate"].Answer.IsNotNullOrEmpty() ? data["485TBExposureDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485TBExposureDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <th colspan="3" class="align_center">Additional Immunizations</th>
                        </tr><tr>
                            <td><%= Html.TextBox("ResumptionOfCare_485AdditionalImmunization1Name", data.ContainsKey("485AdditionalImmunization1Name") ? data["485AdditionalImmunization1Name"].Answer : "", new { @id = "ResumptionOfCare_485AdditionalImmunization1Name", @class = "fill" }) %></td>
                            <td>
                                <select name="ResumptionOfCare_485AdditionalImmunization1" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "" || !data.ContainsKey("485AdditionalImmunization1") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485AdditionalImmunization1Date">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485AdditionalImmunization1Date").Value(data.ContainsKey("485AdditionalImmunization1Date") && data["485AdditionalImmunization1Date"].Answer.IsNotNullOrEmpty() ? data["485AdditionalImmunization1Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485AdditionalImmunization1Date", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td><%= Html.TextBox("ResumptionOfCare_485AdditionalImmunization2Name", data.ContainsKey("485AdditionalImmunization2Name") ? data["485AdditionalImmunization2Name"].Answer : "", new { @id = "ResumptionOfCare_485AdditionalImmunization2Name", @class = "fill" }) %></td>
                            <td>
                                <select name="ResumptionOfCare_485AdditionalImmunization1" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "" || !data.ContainsKey("485AdditionalImmunization2") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="ResumptionOfCare_485AdditionalImmunization2Date">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485AdditionalImmunization2Date").Value(data.ContainsKey("485AdditionalImmunization2Date") && data["485AdditionalImmunization2Date"].Answer.IsNotNullOrEmpty() ? data["485AdditionalImmunization2Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485AdditionalImmunization2Date", @class = "date" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><div class="row">
                <label for="ResumptionOfCare_485ImmunizationComments" class="strong">Comments</label>
                <div><%= Html.TextArea("ResumptionOfCare_485ImmunizationComments", data.ContainsKey("485ImmunizationComments") ? data["485ImmunizationComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485ImmunizationComments" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>Inpatient Discharges in the Past 14 Days</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1000');">(M1000)</a> From which of the following Inpatient Facilities was the patient dischargedduring the past 14 days? (Mark all that apply)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesLTC" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesLTC' name='ResumptionOfCare_M1000InpatientFacilitiesLTC' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesLTC") && data["M1000InpatientFacilitiesLTC"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesLTC"><span class="float_left">1 &ndash;</span><span class="normal margin">Long-term nursing facility (NF)</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesSNF" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesSNF' name='ResumptionOfCare_M1000InpatientFacilitiesSNF' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesSNF") && data["M1000InpatientFacilitiesSNF"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesSNF"><span class="float_left">2 &ndash;</span><span class="normal margin">Skilled nursing facility (SNF / TCU)</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesIPPS" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesIPPS' name='ResumptionOfCare_M1000InpatientFacilitiesIPPS' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesIPPS") && data["M1000InpatientFacilitiesIPPS"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesIPPS"><span class="float_left">3 &ndash;</span><span class="normal margin">Short-stay acute hospital (IPPS)</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesLTCH" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesLTCH' name='ResumptionOfCare_M1000InpatientFacilitiesLTCH' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesLTCH") && data["M1000InpatientFacilitiesLTCH"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesLTCH"><span class="float_left">4 &ndash;</span><span class="normal margin">Long-term care hospital (LTCH)</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesIRF" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesIRF' name='ResumptionOfCare_M1000InpatientFacilitiesIRF' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesIRF") && data["M1000InpatientFacilitiesIRF"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesIRF"><span class="float_left">5 &ndash;</span><span class="normal margin">Inpatient rehabilitation hospital or unit (IRF)</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesPhych" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesPhych' name='ResumptionOfCare_M1000InpatientFacilitiesPhych' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesPhych") && data["M1000InpatientFacilitiesPhych"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesPhych"><span class="float_left">6 &ndash;</span><span class="normal margin">Psychiatric hospital or unit</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesOTHR" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesOTHR' name='ResumptionOfCare_M1000InpatientFacilitiesOTHR' value='1' class='radio float_left M1000' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesOTHR") && data["M1000InpatientFacilitiesOTHR"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesOTHR"><span class="float_left">7 &ndash;</span><span class="normal margin">Other</span></label>
                            </td><td>
                                <div id="ResumptionOfCare_M1000InpatientFacilitiesOTHRMore"><label for="ResumptionOfCare_M1000InpatientFacilitiesOther"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_M1000InpatientFacilitiesOther", data.ContainsKey("M1000InpatientFacilitiesOther") ? data["M1000InpatientFacilitiesOther"].Answer : "", new { @id = "ResumptionOfCare_M1000InpatientFacilitiesOther" }) %></div>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1000InpatientFacilitiesNone" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1000InpatientFacilitiesNone' name='ResumptionOfCare_M1000InpatientFacilitiesNone' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1000InpatientFacilitiesOther") && data["M1000InpatientFacilitiesOther"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_M1000InpatientFacilitiesNone"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient was not discharged from an inpatient facility</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1000');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="ROC_M1005" class="oasis">
        <legend>Discharges Date</legend>
        <div class="wide_column">
            <div class="row">
                <label for="ResumptionOfCare_M1005InpatientDischargeDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1005');">(M1005)</a> Inpatient Discharge Date (most recent)</label>
                <div class="float_right oasis">
                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1005InpatientDischargeDate").Value(data.ContainsKey("M1005InpatientDischargeDate") ? data["M1005InpatientDischargeDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1005InpatientDischargeDate", @class = "date" })%>
                    <input type="hidden" name="ResumptionOfCare_M1005InpatientDischargeDateUnknown" value=" " />
                    <%= string.Format("<input id='ResumptionOfCare_M1005InpatientDischargeDateUnknown' name='ResumptionOfCare_M1005InpatientDischargeDateUnknown' class='radio' type='checkbox' value='1' {0} onclick=\"Oasis.blockText($(this),'#ResumptionOfCare_M1005InpatientDischargeDate-input');\"/>", data.ContainsKey("M1005InpatientDischargeDateUnknown") && data["M1005InpatientDischargeDateUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1005InpatientDischargeDateUnknown">UK &ndash; Unknown</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1000');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="ROC_M1010" class="oasis">
        <legend>Inpatient Diagnosis</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1010');">(M1010)</a> List each Inpatient Diagnosis and ICD-9-C M code at the level of highest specificity for only those conditions treated during an inpatient stay within the last 14 days <em>(no E-codes, or V-codes)</em></label>
                <table class="form">
                    <thead>
                        <tr>
                            <th>
                                <span class="alphali"></span>
                                Inpatient Facility Diagnosis (Locator #11)
                            </th><th>
                                ICD-9-C M Code
                            </th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis1", data.ContainsKey("M1010InpatientFacilityDiagnosis1") ? data["M1010InpatientFacilityDiagnosis1"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode1", data.ContainsKey("M1010InpatientFacilityDiagnosisCode1") ? data["M1010InpatientFacilityDiagnosisCode1"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis2", data.ContainsKey("M1010InpatientFacilityDiagnosis2") ? data["M1010InpatientFacilityDiagnosis2"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode2", data.ContainsKey("M1010InpatientFacilityDiagnosisCode2") ? data["M1010InpatientFacilityDiagnosisCode2"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis3", data.ContainsKey("M1010InpatientFacilityDiagnosis3") ? data["M1010InpatientFacilityDiagnosis3"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode3", data.ContainsKey("M1010InpatientFacilityDiagnosisCode3") ? data["M1010InpatientFacilityDiagnosisCode3"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis4", data.ContainsKey("M1010InpatientFacilityDiagnosis4") ? data["M1010InpatientFacilityDiagnosis4"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode4", data.ContainsKey("M1010InpatientFacilityDiagnosisCode4") ? data["M1010InpatientFacilityDiagnosisCode4"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis5", data.ContainsKey("M1010InpatientFacilityDiagnosis5") ? data["M1010InpatientFacilityDiagnosis5"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode5", data.ContainsKey("M1010InpatientFacilityDiagnosisCode5") ? data["M1010InpatientFacilityDiagnosisCode5"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosis6", data.ContainsKey("M1010InpatientFacilityDiagnosis6") ? data["M1010InpatientFacilityDiagnosis6"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1010InpatientFacilityDiagnosisCode6", data.ContainsKey("M1010InpatientFacilityDiagnosisCode6") ? data["M1010InpatientFacilityDiagnosisCode6"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1010');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="ROC_M1012" class="oasis">
        <legend>Inpatient Procedure</legend>
        <div class="wide_column">
            <div class="ROC_M1012 row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1012');">(M1012)</a> List each Inpatient Procedure and the associated ICD-9-C M procedure code relevant to the plan of care.</label>
                <table class="form">
                    <thead>
                        <tr>
                            <th>
                                <span class="alphali"></span>
                                Inpatient Procedure (locator #12)
                            </th><th>
                                Procedure Code
                            </th>
                        </tr>
                    </thead><tbody>
                        <tr class="M1012">
                            <td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure1", data.ContainsKey("M1012InpatientFacilityProcedure1") ? data["M1012InpatientFacilityProcedure1"].Answer : "", new { @class = "procedureDiagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode1", data.ContainsKey("M1012InpatientFacilityProcedureCode1") ? data["M1012InpatientFacilityProcedureCode1"].Answer : "", new { @class = "procedureICD" }) %>
                            </td>
                        </tr><tr class="M1012">
                            <td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure2", data.ContainsKey("M1012InpatientFacilityProcedure2") ? data["M1012InpatientFacilityProcedure2"].Answer : "", new { @class = "procedureDiagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode2", data.ContainsKey("M1012InpatientFacilityProcedureCode2") ? data["M1012InpatientFacilityProcedureCode2"].Answer : "", new { @class = "procedureICD" }) %>
                            </td>
                        </tr><tr class="M1012">
                            <td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure3", data.ContainsKey("M1012InpatientFacilityProcedure3") ? data["M1012InpatientFacilityProcedure3"].Answer : "", new { @class = "procedureDiagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode3", data.ContainsKey("M1012InpatientFacilityProcedureCode3") ? data["M1012InpatientFacilityProcedureCode3"].Answer : "", new { @class = "procedureICD" }) %>
                            </td>
                        </tr><tr class="M1012">
                            <td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedure4", data.ContainsKey("M1012InpatientFacilityProcedure4") ? data["M1012InpatientFacilityProcedure4"].Answer : "", new { @class = "procedureDiagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1012InpatientFacilityProcedureCode4", data.ContainsKey("M1012InpatientFacilityProcedureCode4") ? data["M1012InpatientFacilityProcedureCode4"].Answer : "", new { @class = "procedureICD" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1012');">?</div>
                </div>
            </div><div class="row">
                <input type="hidden" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable" value="" />
                <%= string.Format("<input id='ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable' name='ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") && data["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable">NA &ndash; Not applicable</label>
                <input type="hidden" name="ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown" value="" />
                <%= string.Format("<input id='ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown' name='ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && data["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown">UK &ndash; Unknown</label>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Diagnoses Requiring Medical or Treatment Regimen Change</legend>
        <div class="wide_column">
            <div class="row ROC_M1016">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1016');">(M1016)</a> Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days: List the patient's Medical Diagnoses and ICD-9-C M codes at the level of highest specificity for those conditions requiring changed medical or treatment regimen within the past 14 days (no surgical, E-codes, or V-codes)</label>
                <table class="form">
                    <thead>
                        <tr>
                            <th>
                                <span class="alphali"></span>
                                Changed Medical Regimen Diagnosis(locator #17)
                            </th><th>
                                ICD-9-C M Code
                            </th>
                        </tr>
                    </thead><tbody>
                        <tr class="M1016">
                            <td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis1", data.ContainsKey("M1016MedicalRegimenDiagnosis1") ? data["M1016MedicalRegimenDiagnosis1"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode1", data.ContainsKey("M1016MedicalRegimenDiagnosisCode1") ? data["M1016MedicalRegimenDiagnosisCode1"].Answer : "", new { @class = "icd" }) %>
                            </td>
                        </tr><tr class="M1016">
                            <td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis2", data.ContainsKey("M1016MedicalRegimenDiagnosis2") ? data["M1016MedicalRegimenDiagnosis2"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode2", data.ContainsKey("M1016MedicalRegimenDiagnosisCode2") ? data["M1016MedicalRegimenDiagnosisCode2"].Answer : "", new { @class = "icd" })%>
                            </td>
                        </tr><tr class="M1016">
                            <td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis3", data.ContainsKey("M1016MedicalRegimenDiagnosis3") ? data["M1016MedicalRegimenDiagnosis3"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode3", data.ContainsKey("M1016MedicalRegimenDiagnosisCode3") ? data["M1016MedicalRegimenDiagnosisCode3"].Answer : "", new { @class = "icd" })%>
                            </td>
                        </tr><tr class="M1016">
                            <td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis4", data.ContainsKey("M1016MedicalRegimenDiagnosis4") ? data["M1016MedicalRegimenDiagnosis4"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode4", data.ContainsKey("M1016MedicalRegimenDiagnosisCode4") ? data["M1016MedicalRegimenDiagnosisCode4"].Answer : "", new { @class = "icd" })%>
                            </td>
                        </tr><tr class="M1016">
                            <td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis5", data.ContainsKey("M1016MedicalRegimenDiagnosis5") ? data["M1016MedicalRegimenDiagnosis5"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode5", data.ContainsKey("M1016MedicalRegimenDiagnosisCode5") ? data["M1016MedicalRegimenDiagnosisCode5"].Answer : "", new { @class = "icd" })%>
                            </td>
                        </tr><tr class="M1016">
                            <td>
                                <span class="alphali">f.</span><%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosis6", data.ContainsKey("M1016MedicalRegimenDiagnosis6") ? data["M1016MedicalRegimenDiagnosis6"].Answer : "", new { @class = "diagnosis" }) %>
                            </td><td>
                                <%= Html.TextBox("ResumptionOfCare_M1016MedicalRegimenDiagnosisCode6", data.ContainsKey("M1016MedicalRegimenDiagnosisCode6") ? data["M1016MedicalRegimenDiagnosisCode6"].Answer : "", new { @class = "icd" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1012');">?</div>
                </div>
            </div><div class="row">
                <input type="hidden" name="ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable" value="" />
                <%= string.Format("<input id='ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable' name='ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && data["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable">NA &ndash; Not applicable (no medical or treatment regimen changes within the past 14 days)</label>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Conditions Prior to Medical or Treatment Regimen Change</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1018');">(M1018)</a> Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay Within Past 14 Days: If this patient experienced an inpatient facility discharge or change in medical or treatment regimen within the past 14 days, indicate any conditions which existed prior to the inpatient stay or change in medical or treatment regime. (Mark all that apply)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI") && data["M1018ConditionsPriorToMedicalRegimenUI"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI"><span class="float_left">1 &ndash;</span><span class="normal margin">Urinary incontinence</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH") && data["M1018ConditionsPriorToMedicalRegimenCATH"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH"><span class="float_left">2 &ndash;</span><span class="normal margin">Indwelling/suprapubic catheter</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain") && data["M1018ConditionsPriorToMedicalRegimenPain"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain"><span class="float_left">3 &ndash;</span><span class="normal margin">Intractable pain</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN") && data["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN"><span class="float_left">4 &ndash;</span><span class="normal margin">Impaired decision-making</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive") && data["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive"><span class="float_left">5 &ndash;</span><span class="normal margin">Disruptive or socially inappropriate behavior</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss") && data["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss"><span class="float_left">6 &ndash;</span><span class="normal margin">Memory loss to the extent that supervision required</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone") && data["M1018ConditionsPriorToMedicalRegimenNone"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone"><span class="float_left">7 &ndash;</span><span class="normal margin">None of the above</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA") && data["M1018ConditionsPriorToMedicalRegimenNA"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA"><span class="float_left">NA &ndash; </span><span class="normal margin">No inpatient facility discharge and no change in medical or treatment regimen in past 14 days</span></label>
                            </td><td>
                                <input type="hidden" name="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK" value="" />
                                <%= string.Format("<input id='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK' name='ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK' value='1' class='radio float_left M1018' type='checkbox' {0} />", data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK") && data["M1018ConditionsPriorToMedicalRegimenUK"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK"><span class="float_left">UK &ndash; </span><span class="normal margin">Unknown</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1018');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Diagnoses, Symptom Control, and Payment Diagnoses</legend>
        <div class="wide_column">
            <div class="row">
                <div class="instructions">
                    <p>List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is reported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign symptom control ratings for V- or E-codes.</p>
                    <strong>Code each row according to the following directions for each column:</strong>
                    <ul>
                        <li><span class="float_left">Column 1:</span><p>Enter the description of the diagnosis.</p></li>
                        <li><span class="float_left">Column 2:</span><p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                            <ul>
                                <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                                <li><span class="float_left">0.</span><p>Asymptomatic, no treatment needed at this time</p></li>
                                <li><span class="float_left">1.</span><p>Symptoms well controlled with current therapy</p></li>
                                <li><span class="float_left">2.</span><p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p></li>
                                <li><span class="float_left">3.</span><p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p></li>
                                <li><span class="float_left">4.</span><p>Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of diagnoses should reflect the seriousnessof each condition and support the disciplines and services provided.</p></li>
                            </ul>
                        </li>
                        <li><span class="float_left">Column 3:</span><p>(OPTIONAL) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.</p></li>
                        <li><span class="float_left">Column 4:</span><p>(OPTIONAL) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code, record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise, leave Column 4 blank in that row.</p></li>
                    </ul>
                </div>
                <table class="form bordergrid">
                    <thead>
                        <tr class="align_center">
                            <td colspan="2">
                                <div class="halfOfTd"><h4>Column 1</h4></div>
                                <div class="halfOfTd"><h4>Column 2</h4></div>
                            </td><td>
                                <h4>Column 3</h4>
                            </td><td>
                                <h4>Column 4</h4>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <label for="ResumptionOfCare_M1020PrimaryDiagnosis" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1020');">(M1020)</a> Primary Diagnosis</label>
                                </div><div class="halfOfTd">
                                    <div>ICD-9-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.</div>
                                    <div for="ResumptionOfCare_M1020ICD9M" class="strong">ICD-9-C M/ Symptom Control Rating</div>
                                    <em>(V-codes are allowed)</em>
                                </div>
                            </td><td>
                                <div>Complete if a V-code is assigned under certain circumstances to Column 2 in place of a case mix diagnosis.</div>
                                <div for="ResumptionOfCare_M1024ICD9MA3" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td><td>
                                <div>Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis that is a multiple coding situation (e.g., a manifestation code).</div>
                                <div for="ResumptionOfCare_M1024ICD9MA4" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1020PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1020PrimaryDiagnosis" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1020ICD9M", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1020ICD9M" }) %>
                                    <label for="ResumptionOfCare_M1020SymptomControlRating">Severity:</label>
                                    <%  var severity = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1020SymptomControlRating") ? data["M1020SymptomControlRating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1020SymptomControlRating", severity, new { @id = "ResumptionOfCare_M1020SymptomControlRating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis", OE, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1020PrimaryDiagnosisDate").Value(data.ContainsKey("M1020PrimaryDiagnosisDate") && data["M1020PrimaryDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosisDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1020PrimaryDiagnosisDate", @class = "diagnosisDate date" })%>
                                    <div class="float_right oasis">
                                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1020');">?</div>
                                    </div>
                                </div>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesA3", data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesA3" })%>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MA3", data.ContainsKey("M1024ICD9MA3") ? data["M1024ICD9MA3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MA3" })%>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesA4", data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesA4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MA4", data.ContainsKey("M1024ICD9MA4") ? data["M1024ICD9MA4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MA4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2"><label for="ResumptionOfCare_M1022PrimaryDiagnosis1" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1022');">(M1022)</a> Other Diagnoses</label></td>
                            <td></td>
                            <td></td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis1", data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis1" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%=Html.TextBox("ResumptionOfCare_M1022ICD9M1", data.ContainsKey("M1022ICD9M1") ? data["M1022ICD9M1"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1022ICD9M1" }) %>
                                    <label for="ResumptionOfCare_M1022OtherDiagnose1Rating">Severity:</label>
                                    <%  var severity1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose1Rating") ? data["M1022OtherDiagnose1Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1022OtherDiagnose1Rating", severity1, new { @id = "ResumptionOfCare_M1022OtherDiagnose1Rating", @class = "severity" }) %>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis1") ? data["485ExacerbationOrOnsetPrimaryDiagnosis1"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis1", OE1, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis1", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1022PrimaryDiagnosis1Date").Value(data.ContainsKey("M1022PrimaryDiagnosis1Date") && data["M1022PrimaryDiagnosis1Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1022PrimaryDiagnosis1Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesB3", data.ContainsKey("M1024PaymentDiagnosesB3") ? data["M1024PaymentDiagnosesB3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesB3" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MB3", data.ContainsKey("M1024ICD9MB3") ? data["M1024ICD9MB3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MB3" })%>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesB4", data.ContainsKey("M1024PaymentDiagnosesB4") ? data["M1024PaymentDiagnosesB4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesB4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MB4", data.ContainsKey("M1024ICD9MB4") ? data["M1024ICD9MB4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MB4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis2", data.ContainsKey("M1022PrimaryDiagnosis2") ? data["M1022PrimaryDiagnosis2"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis2" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%=Html.TextBox("ResumptionOfCare_M1022ICD9M2", data.ContainsKey("M1022ICD9M2") ? data["M1022ICD9M2"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1022ICD9M2" }) %>
                                    <label for="ResumptionOfCare_M1022OtherDiagnose2Rating">Severity:</label>
                                    <%  var severity2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose2Rating") ? data["M1022OtherDiagnose2Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1022OtherDiagnose2Rating", severity2, new { @id = "ResumptionOfCare_M1022OtherDiagnose2Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis2") ? data["485ExacerbationOrOnsetPrimaryDiagnosis2"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis2", OE2, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis2", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1022PrimaryDiagnosis2Date").Value(data.ContainsKey("M1022PrimaryDiagnosis2Date") && data["M1022PrimaryDiagnosis2Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis2Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1022PrimaryDiagnosis2Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesC3", data.ContainsKey("M1024PaymentDiagnosesC3") ? data["M1024PaymentDiagnosesC3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesC3" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MC3", data.ContainsKey("M1024ICD9MC3") ? data["M1024ICD9MC3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MC3" })%>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesC4", data.ContainsKey("M1024PaymentDiagnosesC4") ? data["M1024PaymentDiagnosesC4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesC4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MC4", data.ContainsKey("M1024ICD9MC4") ? data["M1024ICD9MC4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MC4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis3", data.ContainsKey("M1022PrimaryDiagnosis3") ? data["M1022PrimaryDiagnosis3"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis3" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%=Html.TextBox("ResumptionOfCare_M1022ICD9M3", data.ContainsKey("M1022ICD9M3") ? data["M1022ICD9M3"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1022ICD9M3" }) %>
                                    <label for="ResumptionOfCare_M1022OtherDiagnose3Rating">Severity:</label>
                                    <%  var severity3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose3Rating") ? data["M1022OtherDiagnose3Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1022OtherDiagnose3Rating", severity3, new { @id = "ResumptionOfCare_M1022OtherDiagnose3Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis3") ? data["485ExacerbationOrOnsetPrimaryDiagnosis3"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis3", OE3, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis3", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1022PrimaryDiagnosis3Date").Value(data.ContainsKey("M1022PrimaryDiagnosis3Date") && data["M1022PrimaryDiagnosis3Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis3Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1022PrimaryDiagnosis3Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesD3", data.ContainsKey("M1024PaymentDiagnosesD3") ? data["M1024PaymentDiagnosesD3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesD3" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MD3", data.ContainsKey("M1024ICD9MD3") ? data["M1024ICD9MD3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MD3" })%>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesD4", data.ContainsKey("M1024PaymentDiagnosesD4") ? data["M1024PaymentDiagnosesD4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesD4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MD4", data.ContainsKey("M1024ICD9MD4") ? data["M1024ICD9MD4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MD4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis4", data.ContainsKey("M1022PrimaryDiagnosis4") ? data["M1022PrimaryDiagnosis4"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis4" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%=Html.TextBox("ResumptionOfCare_M1022ICD9M4", data.ContainsKey("M1022ICD9M4") ? data["M1022ICD9M4"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1022ICD9M4" }) %>
                                    <label for="ResumptionOfCare_M1022OtherDiagnose4Rating">Severity:</label>
                                    <%  var severity4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose4Rating") ? data["M1022OtherDiagnose4Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1022OtherDiagnose4Rating", severity4, new { @id = "ResumptionOfCare_M1022OtherDiagnose4Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis4") ? data["485ExacerbationOrOnsetPrimaryDiagnosis4"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis4", OE3, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis4", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1022PrimaryDiagnosis4Date").Value(data.ContainsKey("M1022PrimaryDiagnosis4Date") && data["M1022PrimaryDiagnosis4Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis4Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1022PrimaryDiagnosis4Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesE3", data.ContainsKey("M1024PaymentDiagnosesE3") ? data["M1024PaymentDiagnosesE3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesE3" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9ME3", data.ContainsKey("M1024ICD9ME3") ? data["M1024ICD9ME3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9ME3" })%>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesE4", data.ContainsKey("M1024PaymentDiagnosesE4") ? data["M1024PaymentDiagnosesE4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesE4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9ME4", data.ContainsKey("M1024ICD9ME4") ? data["M1024ICD9ME4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9ME4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%= Html.TextBox("ResumptionOfCare_M1022PrimaryDiagnosis5", data.ContainsKey("M1022PrimaryDiagnosis5") ? data["M1022PrimaryDiagnosis5"].Answer : "", new { @class = "diagnosis", @id = "ResumptionOfCare_M1022PrimaryDiagnosis5" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%=Html.TextBox("ResumptionOfCare_M1022ICD9M5", data.ContainsKey("M1022ICD9M5") ? data["M1022ICD9M5"].Answer : "", new { @class = "icd", @id = "ResumptionOfCare_M1022ICD9M5" }) %>
                                    <label for="ResumptionOfCare_M1022OtherDiagnose5Rating">Severity:</label>
                                    <%  var severity5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose5Rating") ? data["M1022OtherDiagnose5Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("ResumptionOfCare_M1022OtherDiagnose5Rating", severity5, new { @id = "ResumptionOfCare_M1022OtherDiagnose5Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis5") ? data["485ExacerbationOrOnsetPrimaryDiagnosis5"].Answer : "0");%>
                                    <%= Html.DropDownList("ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis5", OE3, new { @id = "ResumptionOfCare_485ExacerbationOrOnsetPrimaryDiagnosis5", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_M1022PrimaryDiagnosis5Date").Value(data.ContainsKey("M1022PrimaryDiagnosis5Date") && data["M1022PrimaryDiagnosis5Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis5Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_M1022PrimaryDiagnosis5Date", @class = "diagnosisDate date" })%>
                                </div>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1022');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesF3", data.ContainsKey("M1024PaymentDiagnosesF3") ? data["M1024PaymentDiagnosesF3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesF3" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MF3", data.ContainsKey("M1024ICD9MF3") ? data["M1024ICD9MF3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MF3" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("ResumptionOfCare_M1024PaymentDiagnosesF4", data.ContainsKey("M1024PaymentDiagnosesF4") ? data["M1024PaymentDiagnosesF4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "ResumptionOfCare_M1024PaymentDiagnosesF4" }) %>
                                <%= Html.TextBox("ResumptionOfCare_M1024ICD9MF4", data.ContainsKey("M1024ICD9MF4") ? data["M1024ICD9MF4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "ResumptionOfCare_M1024ICD9MF4" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="loc485">
        <legend>Surgical Procedure (Locator #12)</legend>
        <div class="wide_column">
            <div class="row">
                <table class="form layout_auto align_center">
                    <thead>
                        <tr>
                            <th>Surgical Procedure</th>
                            <th>Code</th>
                            <th>Date</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td><%= Html.TextBox("ResumptionOfCare_485SurgicalProcedureDescription1", data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer : "", new { @class = "procedureDiagnosis", @id = "ResumptionOfCare_485SurgicalProcedureDescription1" })%></td>
                            <td><%= Html.TextBox("ResumptionOfCare_485SurgicalProcedureCode1", data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer : "", new { @class = "procedureICD pad", @id = "ResumptionOfCare_485SurgicalProcedureCode1" })%></td>
                            <td><%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485SurgicalProcedureCode1Date").Value(data.ContainsKey("485SurgicalProcedureCode1Date") && data["485SurgicalProcedureCode1Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485SurgicalProcedureCode1Date", @class = "date" })%></td>
                        </tr><tr>
                            <td><%= Html.TextBox("ResumptionOfCare_485SurgicalProcedureDescription2", data.ContainsKey("485SurgicalProcedureDescription2") ? data["485SurgicalProcedureDescription2"].Answer : "", new { @class = "procedureDiagnosis", @id = "ResumptionOfCare_485SurgicalProcedureDescription2" })%></td>
                            <td><%= Html.TextBox("ResumptionOfCare_485SurgicalProcedureCode2", data.ContainsKey("485SurgicalProcedureCode2") ? data["485SurgicalProcedureCode2"].Answer : "", new { @class = "procedureICD pad", @id = "ResumptionOfCare_485SurgicalProcedureCode2" })%></td>
                            <td><%= Html.Telerik().DatePicker().Name("ResumptionOfCare_485SurgicalProcedureCode2Date").Value(data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2Date"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_485SurgicalProcedureCode2Date", @class = "date" })%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Therapies the Patient Receives at Home</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1030');">(M1030)</a> Therapies the patient receives at home (Mark all that apply)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <input name="ResumptionOfCare_M1030HomeTherapiesInfusion" value="" type="hidden" />
                                <%= string.Format("<input id='ResumptionOfCare_M1030HomeTherapiesInfusion' name='ResumptionOfCare_M1030HomeTherapiesInfusion' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1030HomeTherapiesInfusion"><span class="float_left">1 &ndash;</span><span class="normal margin">Intravenous or infusion therapy (excludes TPN)</span></label>
                            </td><td>
                                <input name="ResumptionOfCare_M1030HomeTherapiesParNutrition" value="" type="hidden" />
                                <%= string.Format("<input id='ResumptionOfCare_M1030HomeTherapiesParNutrition' name='ResumptionOfCare_M1030HomeTherapiesParNutrition' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1030HomeTherapiesParNutrition"><span class="float_left">2 &ndash;</span><span class="normal margin">Parenteral nutrition (TPN or lipids)</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input name="ResumptionOfCare_M1030HomeTherapiesEntNutrition" value="" type="hidden" />
                                <%= string.Format("<input id='ResumptionOfCare_M1030HomeTherapiesEntNutrition' name='ResumptionOfCare_M1030HomeTherapiesEntNutrition' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1030HomeTherapiesEntNutrition"><span class="float_left">3 &ndash;</span><span class="normal margin">Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)</span></label>
                            </td><td>
                                <input name="ResumptionOfCare_M1030HomeTherapiesNone" value="" type="hidden" />
                                <%= string.Format("<input id='ResumptionOfCare_M1030HomeTherapiesNone' name='ResumptionOfCare_M1030HomeTherapiesNone' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_M1030HomeTherapiesNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1030');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfRadioEquals("ResumptionOfCare_485Allergies", "Yes", $("#ResumptionOfCare_485AllergiesDescription"));
    Oasis.showIfChecked($("#ResumptionOfCare_M1000InpatientFacilitiesOTHR"), $("#ResumptionOfCare_M1000InpatientFacilitiesOTHRMore"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1000InpatientFacilitiesNone"), $("#window_resumptionofcare .M1000"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1000InpatientFacilitiesNone"), $("#ROC_M1005"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1000InpatientFacilitiesNone"), $("#ROC_M1010"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1000InpatientFacilitiesNone"), $("#ROC_M1012"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1005InpatientDischargeDateUnknown"), $("#ResumptionOfCare_M1005InpatientDischargeDate"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown"), $("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#window_resumptionofcare .ROC_M1012 table"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown"), $("#window_resumptionofcare .ROC_M1012 table"));
    Oasis.hideIfChecked($("#ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable"), $("#window_resumptionofcare .ROC_M1016 table"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone"), $("#window_resumptionofcare .M1018"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA"), $("#window_resumptionofcare .M1018"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK"), $("#window_resumptionofcare .M1018"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1030HomeTherapiesNone"), $("#window_resumptionofcare .M1030"));
</script>