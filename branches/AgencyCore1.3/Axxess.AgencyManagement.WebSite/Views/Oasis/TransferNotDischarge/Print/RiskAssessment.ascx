﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&rsquo;s influenza season (October 1 through March 31) during this episode of care?",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("NA &ndash; Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season.",<%= data != null && data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "NA" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1045) Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:",true) +
        printview.checkbox("1 &ndash; Received from another health care provider (e.g., physician)",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Received from your agency previously during this year&rsquo;s flu season",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Offered and declined",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Assessed and determined to have medical contraindication(s)",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "04" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Not indicated; patient does not meet age/condition guidelines for influenza vaccine",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "05" ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Inability to obtain vaccine due to declared shortage",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "06" ? "true" : "false"%>) +
        printview.checkbox("7 &ndash; None of the above",<%= data != null && data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "07" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "1" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:",true) +
        printview.checkbox("1 &ndash; Patient has received PPV in the past",<%= data != null && data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Offered and declined",<%= data != null && data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Assessed and determined to have medical contraindication(s)",<%= data != null && data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Not indicated; patient does not meet age/condition guidelines for PPV",<%= data != null && data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "04" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; None of the above",<%= data != null && data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "05" ? "true" : "false"%>));
</script>