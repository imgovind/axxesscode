﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M2300) Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes, used hospital emergency department w/o hospital admission",<%= data != null && data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Yes, used hospital emergency department with hospital admission",<%= data != null && data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "UK" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)? (Mark all that apply.)",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareMed") && data["M2310ReasonForEmergentCareMed"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Injury caused by fall",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareFall") && data["M2310ReasonForEmergentCareFall"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareResInf") && data["M2310ReasonForEmergentCareResInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Other respiratory problem",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareOtherResInf") && data["M2310ReasonForEmergentCareOtherResInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Heart failure (e.g., fluid overload)",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareHeartFail") && data["M2310ReasonForEmergentCareHeartFail"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Cardiac dysrhythmia (irregular heartbeat)",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareCardiac") && data["M2310ReasonForEmergentCareCardiac"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("7 &ndash; Myocardial infarction or chest pain",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareMyocardial") && data["M2310ReasonForEmergentCareMyocardial"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("8 &ndash; Other heart disease",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareHeartDisease") && data["M2310ReasonForEmergentCareHeartDisease"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("9 &ndash; Stroke (CVA) or TIA",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareStroke") && data["M2310ReasonForEmergentCareStroke"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("10 &ndash; Hypo/Hyperglycemia, diabetes out of control",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareHypo") && data["M2310ReasonForEmergentCareHypo"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("11 &ndash; GI bleeding, obstruction, constipation, impaction",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareGI") && data["M2310ReasonForEmergentCareGI"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("12 &ndash; Dehydration, malnutrition",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareDehMal") && data["M2310ReasonForEmergentCareDehMal"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("13 &ndash; Urinary tract infection",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareUrinaryInf") && data["M2310ReasonForEmergentCareUrinaryInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("14 &ndash; IV catheter-related infection or complication",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareIV") && data["M2310ReasonForEmergentCareIV"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("15 &ndash; Wound infection or deterioration",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareWoundInf") && data["M2310ReasonForEmergentCareWoundInf"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("16 &ndash; Uncontrolled pain",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain") && data["M2310ReasonForEmergentCareUncontrolledPain"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("17 &ndash; Acute mental/behavioral health problem",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareMental") && data["M2310ReasonForEmergentCareMental"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("18 &ndash; Deep vein thrombosis, pulmonary embolus",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareDVT") && data["M2310ReasonForEmergentCareDVT"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("19 &ndash; Other than above reasons",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareOther") && data["M2310ReasonForEmergentCareOther"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Reason unknown",<%= data != null && data.ContainsKey("M2310ReasonForEmergentCareUK") && data["M2310ReasonForEmergentCareUK"].Answer == "1" ? "true" : "false"%>)));
</script>