﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M2004) Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer.Split(',').Contains("01") ? "true" : "false"%>)) +
        printview.checkbox("NA &ndash; No clinically significant medication issues identified since the previous OASIS assessment",<%= data != null && data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer.Split(',').Contains("NA") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.Split(',').Contains("01") ? "true" : "false"%>)) +
        printview.checkbox("NA &ndash; Patient not taking any drugs",<%= data != null && data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.Split(',').Contains("NA") ? "true" : "false"%>));
</script>