﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedRiskAssessmentForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientNotDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M1040</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1040');">(M1040)</a> Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&rsquo;s influenza season (October 1 through March 31) during this episode of care?</div>
                <%=Html.Hidden("TransferInPatientNotDischarged_M1040InfluenzaVaccine", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "00", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "00" ? true : false, new { @id = "TransferInPatientNotDischarged_M1040InfluenzaVaccine0", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1040InfluenzaVaccine0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "01", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "01" ? true : false, new { @id = "TransferInPatientNotDischarged_M1040InfluenzaVaccine1", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1040InfluenzaVaccine1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</em></span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1040');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "NA", data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "NA" ? true : false, new { @id = "TransferInPatientNotDischarged_M1040InfluenzaVaccineNA", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1040InfluenzaVaccineNA"><span class="float_left">NA</span><span class="normal margin">Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season.</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientNotDischarged_M1045" class="oasis">
        <legend>OASIS M1045</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1045');">(M1045)</a> Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:</div>
                <%=Html.Hidden("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "01", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "01" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason1", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason1"><span class="float_left">1 &ndash;</span><span class="normal margin">Received from another health care provider (e.g., physician)</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "02", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "02" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason2", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason2"><span class="float_left">2 &ndash;</span><span class="normal margin">Received from your agency previously during this year&rsquo;s flu season</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "03", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "03" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason3", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason3"><span class="float_left">3 &ndash;</span><span class="normal margin">Offered and declined</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "04", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "04" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason4", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason4"><span class="float_left">4 &ndash;</span><span class="normal margin">Assessed and determined to have medical contraindication(s)</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "05", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "05" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason5", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason5"><span class="float_left">5 &ndash;</span><span class="normal margin">Not indicated; patient does not meet age/condition guidelines for influenza vaccine</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "06", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "06" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason6", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason6"><span class="float_left">6 &ndash;</span><span class="normal margin">Inability to obtain vaccine due to declared shortage</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1045');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason", "07", data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "07" ? true : false, new { @id = "TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason7", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason7"><span class="float_left">7 &ndash;</span><span class="normal margin">None of the above</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientNotDischarged_M1050" class="oasis">
        <legend>OASIS M1050</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1050');">(M1050)</a> Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?</div>
                <%=Html.Hidden("TransferInPatientNotDischarged_M1050PneumococcalVaccine", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1050PneumococcalVaccine", "0", data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "0" ? true : false, new { @id = "TransferInPatientNotDischarged_M1050PneumococcalVaccine0", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1050PneumococcalVaccine0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1050');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1050PneumococcalVaccine", "1", data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "1" ? true : false, new { @id = "TransferInPatientNotDischarged_M1050PneumococcalVaccine1", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1050PneumococcalVaccine1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientNotDischarged_M1055" class="oasis">
        <legend>OASIS M1055</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1055');">(M1055)</a>  Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:</div>
                <%=Html.Hidden("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "01", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "01" ? true : false, new { @id = "TransferInPatientNotDischarged_M1055PPVNotReceivedReason1", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1055PPVNotReceivedReason1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient has received PPV in the past</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "02", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "02" ? true : false, new { @id = "TransferInPatientNotDischarged_M1055PPVNotReceivedReason2", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1055PPVNotReceivedReason2"><span class="float_left">2 &ndash;</span><span class="normal margin">Offered and declined</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "03", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "03" ? true : false, new { @id = "TransferInPatientNotDischarged_M1055PPVNotReceivedReason3", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1055PPVNotReceivedReason3"><span class="float_left">3 &ndash;</span><span class="normal margin">Assessed and determined to have medical contraindication(s)</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "04", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "04" ? true : false, new { @id = "TransferInPatientNotDischarged_M1055PPVNotReceivedReason4", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1055PPVNotReceivedReason4"><span class="float_left">4 &ndash;</span><span class="normal margin">Not indicated; patient does not meet age/condition guidelines for PPV</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1055');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1055PPVNotReceivedReason", "05", data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "05" ? true : false, new { @id = "TransferInPatientNotDischarged_M1055PPVNotReceivedReason5", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1055PPVNotReceivedReason5"><span class="float_left">5 &ndash;</span><span class="normal margin">None of the above</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="TransferNotDischarge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="TransferNotDischarge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"TransferNotDischarge.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','TransferInPatientNotDischarged');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfRadioEquals("TransferInPatientNotDischarged_M1040InfluenzaVaccine", "01|NA", $("#TransferInPatientNotDischarged_M1045"));
    Oasis.hideIfRadioEquals("TransferInPatientNotDischarged_M1050PneumococcalVaccine", "1", $("#TransferInPatientNotDischarged_M1055"));
</script>