﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Recertification | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','recertification');</script>")
%>
<div id="recertificationTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_recertification" tooltip="M0010 &ndash; M0150">Clinical Record Items</a></li>
        <li><a href="#patienthistory_recertification" tooltip="M1020 &ndash; M1030">Patient History &amp; Diagnoses</a></li>
        <li><a href="#prognosis_recertification">Prognosis</a></li>
        <li><a href="#supportiveassistance_recertification">Supportive Assistance</a></li>
        <li><a href="#sensorystatus_recertification" tooltip="M1200">Sensory Status</a></li>
        <li><a href="#pain_recertification" tooltip="M1242">Pain</a></li>
        <li><a href="#integumentarystatus_recertification" tooltip="M1306 &ndash; M1350">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_recertification" tooltip="M1400 &ndash; M1410">Respiratory Status</a></li>
        <li><a href="#endocrine_recertification">Endocrine</a></li>
        <li><a href="#cardiacstatus_recertification">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_recertification" tooltip="M1610 &ndash; M1630">Elimination Status</a></li>
        <li><a href="#nutrition_recertification">Nutrition</a></li>
        <li><a href="#behaviourialstatus_recertification">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_recertification" tooltip="M1810 &ndash; M1910">ADL/IADLs</a></li>
        <li><a href="#suppliesworksheet_recertification">Supplies Worksheet</a></li>
        <li><a href="#medications_recertification" tooltip="M2030">Medications</a></li>
        <li><a href="#therapyneed_recertification" tooltip="M2200">Therapy Need &amp; Plan Of Care</a></li>
        <li><a href="#ordersdisciplinetreatment_recertification">Orders for Discipline and Treatment</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_recertification" class="general"><% Html.RenderPartial("~/Views/Oasis/Recertification/Demographics.ascx", Model); %></div>
    <div id="patienthistory_recertification" class="general loading"></div>
    <div id="prognosis_recertification" class="general loading"></div>
    <div id="supportiveassistance_recertification" class="general loading"></div>
    <div id="sensorystatus_recertification" class="general loading"></div>
    <div id="pain_recertification" class="general loading"></div>
    <div id="integumentarystatus_recertification" class="general loading"></div>
    <div id="respiratorystatus_recertification" class="general loading"></div>
    <div id="endocrine_recertification" class="general loading"></div>
    <div id="cardiacstatus_recertification" class="general loading"></div>
    <div id="eliminationstatus_recertification" class="general loading"></div>
    <div id="nutrition_recertification" class="general loading"></div>
    <div id="behaviourialstatus_recertification" class="general loading"></div>
    <div id="adl_recertification" class="general loading"></div>
    <div id="suppliesworksheet_recertification" class="general loading"></div>
    <div id="medications_recertification" class="general loading"></div>
    <div id="therapyneed_recertification" class="general loading"></div>
    <div id="ordersdisciplinetreatment_recertification" class="general loading"></div>
</div>