﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Start Of Care | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','startofcare');</script>")
%>
<div id="socTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#demographics_soc" tooltip="M0010 &ndash; M0150">Demographics</a></li>
        <li><a href="#patienthistory_soc" tooltip="M1000 &ndash; M1030">Patient History &amp; Diagnoses</a></li>
        <li><a href="#riskassessment_soc" tooltip="M1032 &ndash; M1034">Risk Assessment</a></li>
        <li><a href="#prognosis_soc">Prognosis</a></li>
        <li><a href="#supportiveassistance_soc" tooltip="M1100">Supportive Assistance</a></li>
        <li><a href="#sensorystatus_soc" tooltip="M1200 &ndash; M1230">Sensory</a></li>
        <li><a href="#pain_soc" tooltip="M1240 &ndash; M1242">Pain</a></li>
        <li><a href="#integumentarystatus_soc" tooltip="M1300 &ndash; M1350">Integumentary</a></li>
        <li><a href="#respiratorystatus_soc" tooltip="M1400 &ndash; M1410">Respiratory</a></li>
        <li><a href="#endocrine_soc">Endocrine</a></li>
        <li><a href="#cardiacstatus_soc">Cardiac</a></li>
        <li><a href="#eliminationstatus_soc" tooltip="M1600 &ndash; M1630">Elimination</a></li>
        <li><a href="#nutrition_soc">Nutrition</a></li>
        <li><a href="#behaviourialstatus_soc" tooltip="M1700 &ndash; M1750">Neuro/Behavioral</a></li>
        <li><a href="#adl_soc" tooltip="M1800 &ndash; M1910">ADL/IADLs</a></li>
        <li><a href="#suppliesworksheet_soc">Supplies and DME</a></li>
        <li><a href="#medications_soc" tooltip="M2000 &ndash; M2040">Medications</a></li>
        <li><a href="#caremanagement_soc" tooltip="M2100 &ndash; M2110">Care Management</a></li>
        <li><a href="#therapyneed_soc" tooltip="M2200 &ndash; M2250">Therapy Need &amp; Plan Of Care</a></li>
        <li><a href="#ordersdisciplinetreatment_soc">Orders for Discipline and Treatment</a></li>
    </ul>
    <div id="demographics_soc" class="general"><% Html.RenderPartial("~/Views/Oasis/StartOfCare/Demographics.ascx", Model); %></div>
    <div id="patienthistory_soc" class="general loading"></div>
    <div id="riskassessment_soc" class="general loading"></div>
    <div id="prognosis_soc" class="general loading"></div>
    <div id="supportiveassistance_soc" class="general loading"></div>
    <div id="sensorystatus_soc" class="general loading"></div>
    <div id="pain_soc" class="general loading"></div>
    <div id="integumentarystatus_soc" class="general loading"></div>
    <div id="respiratorystatus_soc" class="general loading"></div>
    <div id="endocrine_soc" class="general loading"></div>
    <div id="cardiacstatus_soc" class="general loading"></div>
    <div id="eliminationstatus_soc" class="general loading"></div>
    <div id="nutrition_soc" class="general loading"></div>
    <div id="behaviourialstatus_soc" class="general loading"></div>
    <div id="adl_soc" class="general loading"></div>
    <div id="suppliesworksheet_soc" class="general loading"></div>
    <div id="medications_soc" class="general loading"></div>
    <div id="caremanagement_soc" class="general loading"></div>
    <div id="therapyneed_soc" class="general loading"></div>
    <div id="ordersdisciplinetreatment_soc" class="general loading"></div>
</div>