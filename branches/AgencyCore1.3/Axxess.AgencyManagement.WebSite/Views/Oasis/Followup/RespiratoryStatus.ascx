﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpRespiratoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "Respiratory")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1400');">(M1400)</a> When is the patient dyspneic or noticeably Short of Breath?</label>
                <%= Html.Hidden("FollowUp_M1400PatientDyspneic") %>
              <div class="margin">
                <div>
                    <%= Html.RadioButton("FollowUp_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "FollowUp_M1400PatientDyspneic0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1400PatientDyspneic0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient is not short of breath</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "FollowUp_M1400PatientDyspneic1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1400PatientDyspneic1"><span class="float_left">1 &ndash;</span><span class="normal margin">When walking more than 20 feet, climbing stairs</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "FollowUp_M1400PatientDyspneic2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1400PatientDyspneic2"><span class="float_left">2 &ndash;</span><span class="normal margin">With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "FollowUp_M1400PatientDyspneic3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1400PatientDyspneic3"><span class="float_left">3 &ndash;</span><span class="normal margin">With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "FollowUp_M1400PatientDyspneic4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1400PatientDyspneic4"><span class="float_left">4 &ndash;</span><span class="normal margin">At rest (during day or night)</span></label>
                </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"FollowUp.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','FollowUp');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>