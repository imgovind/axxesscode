﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpIntegumentaryForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("FollowUp_Id", Model.Id)%>
        <%= Html.Hidden("FollowUp_Action", "Edit")%>
        <%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "FollowUp")%>
        <%= Html.Hidden("categoryType", "Integumentary")%> 
<div class="wrapper main"> 
    <fieldset class="oasis">
        <legend>OASIS M1306</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1306');">(M1306)</a> Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?</label>
                <%= Html.Hidden("FollowUp_M1306UnhealedPressureUlcers") %>
                <div class="margin">
                    <%= Html.RadioButton("FollowUp_M1306UnhealedPressureUlcers", "0", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? true : false, new { @id = "FollowUp_M1306UnhealedPressureUlcers0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1306UnhealedPressureUlcers0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1306');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1306UnhealedPressureUlcers", "1", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? true : false, new { @id = "FollowUp_M1306UnhealedPressureUlcers1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1306UnhealedPressureUlcers1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="followup_M1308" class="oasis">
        <legend>OASIS M1308</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1308');">(M1308)</a> Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: (Enter &ldquo;0&rdquo; if none; excludes Stage I pressure ulcers)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td colspan="2"></td>
                            <td>
                                <h4>Column 1</h4>
                                <em>Complete at SOC/ROC/FU &amp; D/C</em>
                            </td><td>
                                <h4>Column 2</h4>
                                <em>Complete at FU &amp; D/C</em>
                            </td>
                        </tr><tr>
                            <td colspan="2">Stage description &ndash; unhealed pressure ulcers</td>
                            <td>Number Currently Present</td>
                            <td>Number of those listed in Column 1 that were present on admission (most recent SOC/ ROC)</td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">a.</span><span class="radio">Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageTwoUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageTwoUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">b.</span><span class="radio">Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageThreeUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageThreeUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">c.</span><span class="radio">Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageFourUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageIVUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.1</span><span class="radio">Unstageable: Known or likely but unstageable due to non-removable dressing or device</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.2</span><span class="radio">Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.3</span><span class="radio">Unstageable: Suspected deep tissue injury in evolution.</span>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn" })%>
                            </td><td>
                                <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", @class = "sn" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_left">Directions for M1310, M1312, and M1314: If the patient has one or more unhealed (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or IV pressure ulcer with the largest surface dimension (length x width) and record in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.</div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1308');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>OASIS M1322/M1324</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1322');">(M1322)</a> Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.
                    <%= Html.Hidden("FollowUp_M1322CurrentNumberStageIUlcer") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "00", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? true : false, new { @id = "FollowUp_M1322CurrentNumberStageIUlcer0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1322CurrentNumberStageIUlcer0"><span class="float_left">0 &ndash;</span><span class="normal margin">Zero</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "01", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? true : false, new { @id = "FollowUp_M1322CurrentNumberStageIUlcer1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1322CurrentNumberStageIUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">One</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "02", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? true : false, new { @id = "FollowUp_M1322CurrentNumberStageIUlcer2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1322CurrentNumberStageIUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Two</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "03", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? true : false, new { @id = "FollowUp_M1322CurrentNumberStageIUlcer3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1322CurrentNumberStageIUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Three</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1322');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "04", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? true : false, new { @id = "FollowUp_M1322CurrentNumberStageIUlcer4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1322CurrentNumberStageIUlcer4"><span class="float_left">4 &ndash;</span><span class="normal margin">Four or more</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1324');">(M1324)</a> Stage of Most Problematic Unhealed (Observable) Pressure Ulcer
                    <%= Html.Hidden("FollowUp_M1324MostProblematicUnhealedStage") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "01", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? true : false, new { @id = "FollowUp_M1324MostProblematicUnhealedStage1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1324MostProblematicUnhealedStage1"><span class="float_left">1 &ndash;</span><span class="normal margin">Stage I</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "02", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? true : false, new { @id = "FollowUp_M1324MostProblematicUnhealedStage2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1324MostProblematicUnhealedStage2"><span class="float_left">2 &ndash;</span><span class="normal margin">Stage II</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "03", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? true : false, new { @id = "FollowUp_M1324MostProblematicUnhealedStage3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1324MostProblematicUnhealedStage3"><span class="float_left">3 &ndash;</span><span class="normal margin">Stage III</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "04", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? true : false, new { @id = "FollowUp_M1324MostProblematicUnhealedStage4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1324MostProblematicUnhealedStage4"><span class="float_left">4 &ndash;</span><span class="normal margin">Stage IV</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1324');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "NA", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? true : false, new { @id = "FollowUp_M1324MostProblematicUnhealedStageNA", @class = "radio float_left" })%>
                    <label for="FollowUp_M1324MostProblematicUnhealedStageNA"><span class="float_left">NA &ndash;</span><span class="normal margin">No observable pressure ulcer or unhealed pressure ulcer</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>OASIS M1330</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1330');">(M1330)</a> Does this patient have a Stasis Ulcer?
                    <%= Html.Hidden("FollowUp_M1330StasisUlcer") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1330StasisUlcer", "00", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? true : false, new { @id = "FollowUp_M1330StasisUlcer0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1330StasisUlcer0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</em></span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1330StasisUlcer", "01", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? true : false, new { @id = "FollowUp_M1330StasisUlcer1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1330StasisUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, patient has BOTH observable and unobservable stasis ulcers</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1330StasisUlcer", "02", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? true : false, new { @id = "FollowUp_M1330StasisUlcer2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1330StasisUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, patient has observable stasis ulcers ONLY</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1330');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1330StasisUlcer", "03", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? true : false, new { @id = "FollowUp_M1330StasisUlcer3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1330StasisUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)</em></span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="followup_M1332AndM1334" class="oasis">
        <legend>OASIS M1332/M1334</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1332');">(M1332)</a> Current Number of (Observable) Stasis Ulcer(s)
                    <%= Html.Hidden("FollowUp_M1332CurrentNumberStasisUlcer") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "01", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">One</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "02", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Two</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "03", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Three</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1332');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "04", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer4"><span class="float_left">4 &ndash;</span><span class="normal margin">Four or more</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1334');">(M1334)</a> Status of Most Problematic (Observable) Stasis Ulcer
                    <%= Html.Hidden("FollowUp_M1334StasisUlcerStatus") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "00", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer0"><span class="float_left">0 &ndash;</span><span class="normal margin">Newly epithelialized</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "01", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer1"><span class="float_left">1 &ndash;</span><span class="normal margin">Fully granulating</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "02", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer2"><span class="float_left">2 &ndash;</span><span class="normal margin">Early/partial granulation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1334');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "03", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? true : false, new { @id = "FollowUp_M1332CurrentNumberStasisUlcer3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1332CurrentNumberStasisUlcer3"><span class="float_left">3 &ndash;</span><span class="normal margin">Not healing</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>OASIS M1340</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1340');">(M1340)</a> Does this patient have a Surgical Wound?
                    <%= Html.Hidden("FollowUp_M1340SurgicalWound") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1340SurgicalWound", "00", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? true : false, new { @id = "FollowUp_M1340SurgicalWound0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1340SurgicalWound0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1340SurgicalWound", "01", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? true : false, new { @id = "FollowUp_M1340SurgicalWound1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1340SurgicalWound1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, patient has at least one (observable) surgical wound</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1340');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1340SurgicalWound", "02", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? true : false, new { @id = "FollowUp_M1340SurgicalWound2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1340SurgicalWound2"><span class="float_left">2 &ndash;</span><span class="normal margin">Surgical wound known but not observable due to non-removable dressing</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="followup_M1342" class="oasis">
        <legend>OASIS M1342</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1342');">(M1342)</a> Status of Most Problematic (Observable) Surgical Wound
                    <%= Html.Hidden("FollowUp_M1342SurgicalWoundStatus") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "00", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? true : false, new { @id = "FollowUp_M1342SurgicalWoundStatus0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1342SurgicalWoundStatus0"><span class="float_left">0 &ndash;</span><span class="normal margin">Newly epithelialized</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "01", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? true : false, new { @id = "FollowUp_M1342SurgicalWoundStatus1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1342SurgicalWoundStatus1"><span class="float_left">1 &ndash;</span><span class="normal margin">Fully granulating</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "02", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? true : false, new { @id = "FollowUp_M1342SurgicalWoundStatus2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1342SurgicalWoundStatus2"><span class="float_left">2 &ndash;</span><span class="normal margin">Early/partial granulation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1342');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "03", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? true : false, new { @id = "FollowUp_M1342SurgicalWoundStatus3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1342SurgicalWoundStatus3"><span class="float_left">3 &ndash;</span><span class="normal margin">Not healing</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>OASIS M1350</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1350');">(M1350)</a> Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?
                    <%= Html.Hidden("FollowUp_M1350SkinLesionOpenWound") %>
                </div><div>
                    <%= Html.RadioButton("FollowUp_M1350SkinLesionOpenWound", "0", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? true : false, new { @id = "FollowUp_M1350SkinLesionOpenWound0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1350SkinLesionOpenWound0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1350');">?</div>
                    </div>
                    <%= Html.RadioButton("FollowUp_M1350SkinLesionOpenWound", "1", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? true : false, new { @id = "FollowUp_M1350SkinLesionOpenWound1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1350SkinLesionOpenWound1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"FollowUp.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','FollowUp');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfRadioEquals("FollowUp_M1306UnhealedPressureUlcers", "0", $("#followup_M1308"));
    Oasis.hideIfRadioEquals("FollowUp_M1330StasisUlcer", "00|03", $("#followup_M1332AndM1334"));
    Oasis.hideIfRadioEquals("FollowUp_M1340SurgicalWound", "00|02", $("#followup_M1342"));
</script>