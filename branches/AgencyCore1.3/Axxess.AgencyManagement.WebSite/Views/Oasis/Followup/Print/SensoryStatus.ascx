﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1200) Vision (with corrective lenses if the patient usually wears them)",true) +
        printview.checkbox("0 &ndash; Normal vision: sees adequately in most situations; can see medication labels, newsprint.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? "true" : "false"%>));
</script>