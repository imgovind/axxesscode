﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?",true) +
        printview.span("Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined). <%= data != null && data.ContainsKey("M2200NumberOfTherapyNeed") && data["M2200NumberOfTherapyNeed"].Answer.IsNotNullOrEmpty() ? data["M2200NumberOfTherapyNeed"].Answer : ""%>") +
        printview.checkbox("NA &ndash; Not Applicable: No case mix group defined by this assessment.",<%= data != null && data.ContainsKey("M2200NumberOfTherapyNeed") && data["M2200NumberOfTherapyNeed"].Answer == "1" ? "true" : "false"%>));
</script>