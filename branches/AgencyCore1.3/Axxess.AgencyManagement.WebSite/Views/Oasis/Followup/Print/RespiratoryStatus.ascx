﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1400) When is the patient dyspneic or noticeably Short of Breath?",true) +
        printview.checkbox("0 &ndash; Patient is not short of breath",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; When walking more than 20 feet, climbing stairs",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; At rest (during day or night)",<%= data != null && data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? "true" : "false"%>));
</script>