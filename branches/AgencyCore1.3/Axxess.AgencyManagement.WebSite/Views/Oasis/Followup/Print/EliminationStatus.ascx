﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1610) Urinary Incontinence or Urinary Catheter Presence:",true) +
        printview.checkbox("0 &ndash; No incontinence or catheter (includes anuria or ostomy for urinary drainage)",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Patient is incontinent",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic)",<%= data != null && data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1620) Bowel Incontinence Frequency:",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; Very rarely or never has bowel incontinence",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less than once weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; One to three times weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Four to six times weekly",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; On a daily basis",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; More often than once daily",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient has ostomy for bowel elimination",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?",true) +
        printview.checkbox("0 &ndash; Patient does not have an ostomy for bowel elimination.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.",<%= data != null && data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? "true" : "false"%>));
</script>