﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareTherapyNeedForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "TherapyNeed")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M2200</legend>
        <div class="wide_column">
            <div class="row">
                <label for="StartOfCare_M2200NumberOfTherapyNeed" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2200');">(M2200)</a> Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)? (Enter zero [&ldquo;000&rdquo;] if no therapy visits indicated.)</label>
                <div id="StartOfCare_M2200Right" class="float_right">
                    <label>Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).</label>
                    <%=Html.TextBox("StartOfCare_M2200NumberOfTherapyNeed", data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : "", new { @id = "StartOfCare_M2200NumberOfTherapyNeed", @class = "vitals numeric", @maxlength = "3" })%>
                </div>
                <div class="clear"></div>
                <div class="float_right oasis">
                    <input name="StartOfCare_M2200TherapyNeedNA" value="" type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M2200TherapyNeedNA' class='radio' name='StartOfCare_M2200TherapyNeedNA' value='1' type='checkbox' {0} onclick=\"Oasis.blockText($(this),'#StartOfCare_M2200NumberOfTherapyNeed');\"/>", data.ContainsKey("M2200TherapyNeedNA") && data["M2200TherapyNeedNA"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M2200TherapyNeedNA">NA &ndash; Not Applicable: No case mix group defined by this assessment.</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2200');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M2250</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2250');">(M2250)</a> Plan of Care Synopsis: (Check only one box in each row.) Does the physician-ordered plan of care include the following:</label>
                <table class="form bordergrid">
                    <thead>
                        <tr>
                            <th colspan="4">Plan/ Intervention</th>
                            <th class="fiexdwidthsamll align_center">0 &ndash; No</th>
                            <th class="fiexdwidthsamll align_center">1 &ndash; Yes</th>
                            <th colspan="4">NA &ndash; Not Applicable</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="4">
                                <span class="float_left">a.</span>
                                <span class="radio">Patient-specific parameters for notifying physician of changes in vital signs or other clinical findings</span>
                                <%=Html.Hidden("StartOfCare_M2250PatientParameters") %>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "00", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250PatientParameters0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PatientParameters0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "01", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250PatientParameters1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PatientParameters1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250PatientParameters", "NA", data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250PatientParametersNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PatientParametersNA"><span class="float_left">NA</span><span class="normal margin">Physician has chosen not to establish patient-specific parameters for this patient. Agency will use standardized clinical guidelines accessible for all care providers to reference</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">b.</span>
                                <span class="radio">Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span>
                                <%=Html.Hidden("StartOfCare_M2250DiabeticFoot") %>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "00", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250DiabeticFoot0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DiabeticFoot0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "01", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250DiabeticFoot1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DiabeticFoot1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250DiabeticFoot", "NA", data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250DiabeticFootNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DiabeticFootNA"><span class="float_left">NA</span><span class="normal margin">Patient is not diabetic or is bilateral amputee</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">c.</span>
                                <span class="radio">Falls prevention interventions</span>
                                <%=Html.Hidden("StartOfCare_M2250FallsPrevention") %>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "00", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250FallsPrevention0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250FallsPrevention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "01", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250FallsPrevention1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250FallsPrevention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250FallsPrevention", "NA", data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250FallsPreventionNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250FallsPreventionNA"><span class="float_left">NA</span><span class="normal margin">Patient is not assessed to be at risk for falls</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">d.</span>
                                <span class="radio">Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span>
                                <%=Html.Hidden("StartOfCare_M2250DepressionPrevention")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "00", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250DepressionPrevention0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DepressionPrevention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "01", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250DepressionPrevention1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DepressionPrevention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250DepressionPrevention", "NA", data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250DepressionPreventionNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250DepressionPreventionNA"><span class="float_left">NA</span><span class="normal margin">Patient has no diagnosis or symptoms of depression</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">e.</span>
                                <span class="radio">Intervention(s) to monitor and mitigate pain</span>
                                <%=Html.Hidden("StartOfCare_M2250MonitorMitigatePainIntervention") %>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "00", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250MonitorMitigatePainIntervention0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250MonitorMitigatePainIntervention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "01", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250MonitorMitigatePainIntervention1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250MonitorMitigatePainIntervention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250MonitorMitigatePainIntervention", "NA", data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250MonitorMitigatePainInterventionNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250MonitorMitigatePainInterventionNA"><span class="float_left">NA</span><span class="normal margin">No pain identified</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">f.</span>
                                <span class="radio">Intervention(s) to prevent pressure ulcers</span>
                                <%=Html.Hidden("StartOfCare_M2250PressureUlcerIntervention")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "00", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerIntervention0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerIntervention0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "01", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerIntervention1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerIntervention1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerIntervention", "NA", data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerInterventionNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerInterventionNA"><span class="float_left">NA</span><span class="normal margin">Patient is not assessed to be at risk for pressure ulcers</span></label>
                            </td>
                        </tr><tr>
                            <td colspan="4">
                                <span class="float_left">g.</span>
                                <span class="radio">Pressure ulcer treatment based on principles of moist wound healing OR order for treatment based on moist wound healing has been requested from physician</span>
                                <%=Html.Hidden("StartOfCare_M2250PressureUlcerTreatment") %>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "00", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerTreatment0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerTreatment0"><span class="float_left">0 &ndash;</span><span class="margin normal">No</span></label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "01", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerTreatment1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerTreatment1"><span class="float_left">1 &ndash;</span><span class="margin normal">Yes</span></label>
                            </td><td colspan="4">
                                <%=Html.RadioButton("StartOfCare_M2250PressureUlcerTreatment", "NA", data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M2250PressureUlcerTreatmentNA", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2250PressureUlcerTreatmentNA"><span class="float_left">NA</span><span class="normal margin">Patient has no pressure ulcers with need for moist wound healing</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2250');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Interventions</legend>
        <%string[] therapyInterventions = data.ContainsKey("485TherapyInterventions") && data["485TherapyInterventions"].Answer != "" ? data["485TherapyInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485TherapyInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485TherapyInterventions1' class='radio float_left' name='StartOfCare_485TherapyInterventions' value='1' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("1") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485TherapyInterventions1" class="radio">Physical therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485TherapyInterventions2' class='radio float_left' name='StartOfCare_485TherapyInterventions' value='2' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485TherapyInterventions2" class="radio">Occupational therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485TherapyInterventions3' class='radio float_left' name='StartOfCare_485TherapyInterventions' value='3' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485TherapyInterventions3" class="radio">Speech therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <label for="StartOfCare_485TherapyOrderTemplates">Additional Orders:</label>
                <%  var therapyOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485TherapyOrderTemplates") && data["485TherapyOrderTemplates"].Answer != "" ? data["485TherapyOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485TherapyOrderTemplates", therapyOrderTemplates)%>
                <%= Html.TextArea("StartOfCare_485TherapyComments", data.ContainsKey("485TherapyComments") ? data["485TherapyComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485TherapyComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div> 
<% } %>
<script type="text/javascript">
    Oasis.hideIfChecked($("#StartOfCare_M2200TherapyNeedNA"), $("#StartOfCare_M2200Right"));
</script>