﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%227%22%3E" +
        printview.span("(M2100) Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed.",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3EType of Assistance%3C/th%3E%3Cth%3ENo assistance in this area%3C/th%3E%3Cth%3ECaregiver(s) currently provide assistance%3C/th%3E%3Cth%3ECaregiver(s) need training/ supportive services to provide assistance%3C/th%3E%3Cth%3ECaregiver(s) not likely to provide assistance%3C/th%3E%3Cth%3EUnclear if Caregiver(s) will provide assistance%3C/th%3E%3Cth%3EAssistance needed, but no caregiver(s) available%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b.IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c.Medication administration (e.g., oral, inhaled or injectable)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d.Medical procedures/ treatments (e.g., changing wound dressing)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e.Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f.Supervision and safety (e.g., due to cognitive impairment)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("g.Advocacy or facilitation of patient’s participation in appropriate medical care (includes transporta-tion to or from appointments)") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("4",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("5",<%= data != null && data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        printview.span("(M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?",true) +
        printview.col(3,
            printview.checkbox("1 &ndash; At least daily",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Three or more times per week",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; One to two times per week",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Received, but less often than weekly",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; No assistance received",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer.Split(',').Contains("UK") ? "true" : "false"%>)));
</script>