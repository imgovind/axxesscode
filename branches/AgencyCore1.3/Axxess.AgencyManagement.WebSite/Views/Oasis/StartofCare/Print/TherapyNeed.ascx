﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?",true) +
        printview.span("Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined). <%= data != null && data.ContainsKey("M2200NumberOfTherapyNeed") && data["M2200NumberOfTherapyNeed"].Answer.IsNotNullOrEmpty() ? data["M2200NumberOfTherapyNeed"].Answer : ""%>") +
        printview.checkbox("NA &ndash; Not Applicable: No case mix group defined by this assessment.",<%= data != null && data.ContainsKey("M2200NumberOfTherapyNeed") && data["M2200NumberOfTherapyNeed"].Answer == "1" ? "true" : "false"%>));
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%224%22%3E" +
        printview.span("(M2250) Plan of Care Synopsis: (Check only one box in each row.) Does the physician-ordered plan of care include the following:",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3E" +
        printview.span("Plan/Intervention",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("&nbsp;No ",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("&nbsp;Yes ",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Not Applicable",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Patient-specific parameters for notifying physician of changes in vital signs or other clinical findings") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Physician has chosen not to establish patient-specific parameters for this patient. Agency will use standardized clinical guidelines accessible for all care providers to reference",<%= data != null && data.ContainsKey("M2250PatientParameters") && data["M2250PatientParameters"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not diabetic or is bilateral amputee",<%= data != null && data.ContainsKey("M2250DiabeticFoot") && data["M2250DiabeticFoot"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Falls prevention interventions") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not assessed to be at risk for falls",<%= data != null && data.ContainsKey("M2250FallsPrevention") && data["M2250FallsPrevention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient has no diagnosis or symptoms of depression",<%= data != null && data.ContainsKey("M2250DepressionPrevention") && data["M2250DepressionPrevention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Intervention(s) to monitor and mitigate pain") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("No pain identified",<%= data != null && data.ContainsKey("M2250MonitorMitigatePainIntervention") && data["M2250MonitorMitigatePainIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Intervention(s) to prevent pressure ulcers") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient is not assessed to be at risk for pressure ulcers",<%= data != null && data.ContainsKey("M2250PressureUlcerIntervention") && data["M2250PressureUlcerIntervention"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("g. Pressure ulcer treatment based on principles of moist wound healing OR order for treatment based on moist wound healing has been requested from physician") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("Patient has no pressure ulcers with need for moist wound healing",<%= data != null && data.ContainsKey("M2250PressureUlcerTreatment") && data["M2250PressureUlcerTreatment"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        printview.checkbox("Physical therapist to evaluate and submit plan of treatment.",<%= data != null && data.ContainsKey("485TherapyInterventions") && data["485TherapyInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Occupational therapist to evaluate and submit plan of treatment.",<%= data != null && data.ContainsKey("485TherapyInterventions") && data["485TherapyInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Speech therapist to evaluate and submit plan of treatment.",<%= data != null && data.ContainsKey("485TherapyInterventions") && data["485TherapyInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485TherapyComments") && data["485TherapyComments"].Answer.IsNotNullOrEmpty() ? data["485TherapyComments"].Answer : ""%>",false,2),
        "Interventions");
</script>