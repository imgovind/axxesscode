﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareAdlForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<%= Html.Hidden("categoryType", "Adl")%> 
<%string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
<div class="wrapper main">
    <fieldset class="loc485">
	    <legend>Activities Permitted (locator #18.B)</legend>
        <div class="column">
            <div class="row">
                <input type="hidden" name="StartOfCare_485ActivitiesPermitted" value="" />
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted1' name='StartOfCare_485ActivitiesPermitted' value='1' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("1")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted1" class="radio">Complete bed rest</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted3' name='StartOfCare_485ActivitiesPermitted' value='3' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("3")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted3" class="radio">Up as tolerated</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted5' name='StartOfCare_485ActivitiesPermitted' value='5' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("5")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted5" class="radio">Exercise prescribed</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted7' name='StartOfCare_485ActivitiesPermitted' value='7' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("7")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted7" class="radio">Independent at home</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted9' name='StartOfCare_485ActivitiesPermitted' value='9' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("9")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted9" class="radio">Cane</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermittedB' name='StartOfCare_485ActivitiesPermitted' value='B' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("B")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermittedB" class="radio">Walker</label>
            </div>
        </div><div class="column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted2' name='StartOfCare_485ActivitiesPermitted' value='2' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("2")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted2" class="radio">Bed rest with BRP</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted4' name='StartOfCare_485ActivitiesPermitted' value='4' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("4")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted4" class="radio">Transfer bed-chair</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted6' name='StartOfCare_485ActivitiesPermitted' value='6' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("6")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted6" class="radio">Partial weight bearing</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermitted8' name='StartOfCare_485ActivitiesPermitted' value='8' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("8")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermitted8" class="radio">Crutches</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485ActivitiesPermittedA' name='StartOfCare_485ActivitiesPermitted' value='A' class='radio float_left' type='checkbox' {0} />", activitiesPermitted!=null && activitiesPermitted.Contains("A")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485ActivitiesPermittedA" class="radio">Wheelchair</label>
            </div><div class="row">
                <label for="StartOfCare_485ActivitiesPermittedOther">Other <em>(specify)</em>:</label>
                <%= Html.TextBox("StartOfCare_485ActivitiesPermittedOther", data.ContainsKey("485ActivitiesPermittedOther") ? data["485ActivitiesPermittedOther"].Answer : "", new { @id = "StartOfCare_485ActivitiesPermittedOther" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Musculoskeletal</legend><%string[] genericMusculoskeletal = data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer != "" ? data["GenericMusculoskeletal"].Answer.Split(',') : null; %><input type="hidden" name="StartOfCare_GenericMusculoskeletal" value="" />
	    <div class="column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal1' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='1' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("1")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericMusculoskeletal1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal2' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='2' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("2")   ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericMusculoskeletal2" class="radio">Grip Strength:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal2More" class="rel float_right">
                    <div class="float_right">
                        <%  var handGrips = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Strong", Value = "1" },
                                new SelectListItem { Text = "Weak", Value = "2" },
                                new SelectListItem { Text = "Other", Value = "3" }
                            }, "Value", "Text", data.ContainsKey("GenericMusculoskeletalHandGripsPosition") ? data["GenericMusculoskeletalHandGripsPosition"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_GenericMusculoskeletalHandGrips", handGrips, new { @id = "StartOfCare_GenericMusculoskeletalHandGrips" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%= Html.RadioButton("StartOfCare_GenericMusculoskeletalHandGripsPosition", "0", new { @id = "StartOfCare_GenericMusculoskeletalHandGripsPosition0", @class = "radio" })%>
                        <label for="StartOfCare_GenericMusculoskeletalHandGripsPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("StartOfCare_GenericMusculoskeletalHandGripsPosition", "1", new { @id = "StartOfCare_GenericMusculoskeletalHandGripsPosition1", @class = "radio" })%>
                        <label for="StartOfCare_GenericMusculoskeletalHandGripsPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("StartOfCare_GenericMusculoskeletalHandGripsPosition", "2", new { @id = "StartOfCare_GenericMusculoskeletalHandGripsPosition2", @class = "radio" })%>
                        <label for="StartOfCare_GenericMusculoskeletalHandGripsPosition2" class="inlineradio">Right</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal3' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='3' type='checkbox' {0} />", genericMusculoskeletal != null && genericMusculoskeletal.Contains("3") ? "checked='checked'" : "")%>
                    <label for="StartOfCare_GenericMusculoskeletal3" class="radio">Impaired Motor Skill:</label>
                </div><div class="float_right">
                    <%  var impairedMotorSkills = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Fine", Value = "2" },
                            new SelectListItem { Text = "Gross", Value = "3" }
                        }, "Value", "Text", data.ContainsKey("GenericMusculoskeletalImpairedMotorSkills") ? data["GenericMusculoskeletalImpairedMotorSkills"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericMusculoskeletalImpairedMotorSkills", impairedMotorSkills, new { @id = "StartOfCare_GenericMusculoskeletalImpairedMotorSkills" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal4' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='4' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("4")   ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericMusculoskeletal4" class="radio">Limited ROM:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal4More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("StartOfCare_GenericLimitedROMLocation", data.ContainsKey("GenericLimitedROMLocation") ? data["GenericLimitedROMLocation"].Answer : "", new { @id = "StartOfCare_GenericLimitedROMLocation", @class = "loc", @maxlength = "24" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal5' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='5' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("5")   ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericMusculoskeletal5" class="radio">Mobility:</label>
                </div><div class="float_right">
                    <%  var mobility = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "WNL", Value = "1" },
                            new SelectListItem { Text = "Ambulatory", Value = "2" },
                            new SelectListItem { Text = "Ambulatory w/assistance", Value = "3" },
                            new SelectListItem { Text = "Chair fast", Value = "4" },
                            new SelectListItem { Text = "Bedfast", Value = "5" },
                            new SelectListItem { Text = "Non-ambulatory", Value = "6" }
                        }, "Value", "Text", data.ContainsKey("GenericMusculoskeletalMobility") ? data["GenericMusculoskeletalMobility"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericMusculoskeletalMobility", mobility, new { @id = "StartOfCare_GenericMusculoskeletalMobility" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal6' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='6' type='checkbox' {0} />", genericMusculoskeletal != null && genericMusculoskeletal.Contains("6") ? "checked='checked'" : "")%>
                <label for="StartOfCare_GenericMusculoskeletal6" class="radio">Type Assistive Device:</label>
                <div id="StartOfCare_GenericMusculoskeletal6More" class="float_right">
                    <% string[] genericAssistiveDevice = data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer != "" ? data["GenericAssistiveDevice"].Answer.Split(',') : null; %>
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericAssistiveDevice1' class='radio float_left' name='StartOfCare_GenericAssistiveDevice' value='1' type='checkbox' {0} />", genericAssistiveDevice != null && genericAssistiveDevice.Contains("1") ? "checked='checked'" : "")%>
                        <label for="StartOfCare_GenericAssistiveDevice1" class="fixed inlineradio">Cane</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericAssistiveDevice2' class='radio float_left' name='StartOfCare_GenericAssistiveDevice' value='2' type='checkbox' {0} />", genericAssistiveDevice != null && genericAssistiveDevice.Contains("2") ? "checked='checked'" : "")%>
                        <label for="StartOfCare_GenericAssistiveDevice2" class="fixed inlineradio">Crutches</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericAssistiveDevice3' class='radio float_left' name='StartOfCare_GenericAssistiveDevice' value='3' type='checkbox' {0} />", genericAssistiveDevice != null && genericAssistiveDevice.Contains("3") ? "checked='checked'" : "")%>
                        <label for="StartOfCare_GenericAssistiveDevice3" class="fixed inlineradio">Walker</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericAssistiveDevice4' class='radio float_left' name='StartOfCare_GenericAssistiveDevice' value='4' type='checkbox' {0} />", genericAssistiveDevice != null && genericAssistiveDevice.Contains("4") ? "checked='checked'" : "")%>
                        <label for="StartOfCare_GenericAssistiveDevice4" class="fixed inlineradio">Wheelchair</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericAssistiveDevice5' class='radio float_left' name='StartOfCare_GenericAssistiveDevice' value='5' type='checkbox' {0} />", genericAssistiveDevice != null && genericAssistiveDevice.Contains("5") ? "checked='checked'" : "")%>
                        <label for="StartOfCare_GenericAssistiveDevice5" class="fixed inlineradio">Other</label>
                    </div><div class="float_left">
                        <%=Html.TextBox("StartOfCare_GenericAssistiveDeviceOther", data.ContainsKey("GenericAssistiveDeviceOther") ? data["GenericAssistiveDeviceOther"].Answer : "", new { @id = "StartOfCare_GenericAssistiveDeviceOther", @class = "st" })%>
                    </div>
                </div>
            </div>
        </div><div class="column">
            <% string[] genericBoundType = data.ContainsKey("GenericBoundType") && data["GenericBoundType"].Answer != "" ? data["GenericBoundType"].Answer.Split(',') : null; %>
            <div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal7' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='7' type='checkbox' {0} />", genericMusculoskeletal != null && genericMusculoskeletal.Contains("3") ? "checked='checked'" : "")%>
                    <label for="StartOfCare_GenericMusculoskeletal7" class="radio">Contracture:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal7More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("StartOfCare_GenericContractureLocation", data.ContainsKey("GenericContractureLocation") ? data["GenericContractureLocation"].Answer : "", new { @id = "StartOfCare_GenericContractureLocation", @class = "loc", @maxlength = "24" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal8' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='8' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("8")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericMusculoskeletal8" class="radio">Weakness</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal9' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='9' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("9")   ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericMusculoskeletal9" class="radio">Joint Pain:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal9More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("StartOfCare_GenericJointPainLocation", data.ContainsKey("GenericJointPainLocation") ? data["GenericJointPainLocation"].Answer : "", new { @id = "StartOfCare_GenericJointPainLocation", @class = "loc", @maxlength = "24" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal10' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='10' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("10")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericMusculoskeletal10" class="radio">Poor Balance</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal11' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='11' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("11")   ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericMusculoskeletal11" class="radio">Joint Stiffness</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal12' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='12' type='checkbox' {0} />", genericMusculoskeletal!=null && genericMusculoskeletal.Contains("12")   ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericMusculoskeletal12" class="radio">Amputation:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal12More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("StartOfCare_GenericAmputationLocation", data.ContainsKey("GenericAmputationLocation") ? data["GenericAmputationLocation"].Answer : "", new { @id = "StartOfCare_GenericAmputationLocation", @class = "loc", @maxlength = "24" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericMusculoskeletal13' class='radio float_left' name='StartOfCare_GenericMusculoskeletal' value='13' type='checkbox' {0} />", genericMusculoskeletal != null && genericMusculoskeletal.Contains("13") ? "checked='checked'" : "")%>
                    <label for="StartOfCare_GenericMusculoskeletal13" class="radio">Weight Bearing Restrictions:</label>
                </div><div id="StartOfCare_GenericMusculoskeletal13More" class="rel float_right">
                    <div class="float_right">
                        <div class="float_left">
                            <%=Html.RadioButton("StartOfCare_GenericWeightBearingRestriction", "0", data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericWeightBearingRestriction0", @class = "radio float_left" })%>
                            <label for="StartOfCare_GenericWeightBearingRestriction0">Full</label>
                        </div><div class="float_left">
                            <%=Html.RadioButton("StartOfCare_GenericWeightBearingRestriction", "1", data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericWeightBearingRestriction1", @class = "radio float_left" })%>
                            <label for="StartOfCare_GenericWeightBearingRestriction">Partial</label>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <em>(location)</em>
                        <%= Html.TextBox("StartOfCare_GenericWeightBearingRestrictionLocation", data.ContainsKey("GenericWeightBearingRestrictionLocation") ? data["GenericWeightBearingRestrictionLocation"].Answer : "", new { @id = "StartOfCare_GenericWeightBearingRestrictionLocation", @class = "loc", @maxlength = "24" })%>
                    </div>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericMusculoskeletalComments" class="float_left">Comments:</label>
                <%=Html.TextArea("StartOfCare_GenericMusculoskeletalComments", data.ContainsKey("GenericMusculoskeletalComments") ? data["GenericMusculoskeletalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericMusculoskeletalComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1800');">(M1800)</a> Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).</label>
                <%=Html.Hidden("StartOfCare_M1800Grooming")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1800Grooming", "00", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1800Grooming0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1800Grooming0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to groom self unaided, with or without the use of assistive devices or adapted methods.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1800Grooming", "01", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1800Grooming1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1800Grooming1"><span class="float_left">1 &ndash;</span><span class="normal margin">Grooming utensils must be placed within reach before able to complete grooming activities.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1800Grooming", "02", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1800Grooming2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1800Grooming2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must assist the patient to groom self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1800');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1800Grooming", "03", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1800Grooming3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1800Grooming3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon someone else for grooming needs.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1810');">(M1810)</a> Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:</label>
                <%=Html.Hidden("StartOfCare_M1810CurrentAbilityToDressUpper")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1810CurrentAbilityToDressUpper0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1810CurrentAbilityToDressUpper0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1810CurrentAbilityToDressUpper1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1810CurrentAbilityToDressUpper1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1810CurrentAbilityToDressUpper2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1810CurrentAbilityToDressUpper2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on upper body clothing.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1810');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1810CurrentAbilityToDressUpper3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1810CurrentAbilityToDressUpper3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress the upper body.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1820');">(M1820)</a> Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:</label>
                <%=Html.Hidden("StartOfCare_M1820CurrentAbilityToDressLower")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1820CurrentAbilityToDressLower0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1820CurrentAbilityToDressLower0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to obtain, put on, and remove clothing and shoes without assistance.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1820CurrentAbilityToDressLower1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1820CurrentAbilityToDressLower1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1820CurrentAbilityToDressLower2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1820CurrentAbilityToDressLower2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1820CurrentAbilityToDressLower3", @class = "radio float_left" })%>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1820');">?</div>
                    </div>
                    <label for="StartOfCare_M1820CurrentAbilityToDressLower3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress lower body.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1830');">(M1830)</a> Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).</label>
                <%=Html.Hidden("StartOfCare_M1830CurrentAbilityToBatheEntireBody")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody2">
                        <span class="float_left">2 &ndash;</span>
                        <span class="normal margin">
                            Able to bathe in shower or tub with the intermittent assistance of another person:
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">for intermittent supervision or encouragement or reminders, OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">to get in and out of the shower or tub, OR</span></li>
                                <li><span class="float_left">(c)</span><span class="radio">for washing difficult to reach areas.</span></li>
                            </ul>
                        </span>
                    </label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody5", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody5"><span class="float_left">5 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1830');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "StartOfCare_M1830CurrentAbilityToBatheEntireBody6", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1830CurrentAbilityToBatheEntireBody6"><span class="float_left">6 &ndash;</span><span class="normal margin">Unable to participate effectively in bathing and is bathed totally by another person.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1840');">(M1840)</a> Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.</label>
                <%=Html.Hidden("StartOfCare_M1840ToiletTransferring")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1840ToiletTransferring0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1840ToiletTransferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get to and from the toilet and transfer independently with or without a device.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1840ToiletTransferring1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1840ToiletTransferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1840ToiletTransferring2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1840ToiletTransferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1840ToiletTransferring3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1840ToiletTransferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1840');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1840ToiletTransferring4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1840ToiletTransferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Is totally dependent in toileting.</span></label>
                    <div class="clear"></div>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1845');">(M1845)</a> Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.</label>
                <%=Html.Hidden("StartOfCare_M1845ToiletingHygiene")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1845ToiletingHygiene", "00", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1845ToiletingHygiene0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1845ToiletingHygiene0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to manage toileting hygiene and clothing management without assistance.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1845ToiletingHygiene", "01", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1845ToiletingHygiene1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1845ToiletingHygiene1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1845ToiletingHygiene", "02", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1845ToiletingHygiene2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1845ToiletingHygiene2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient to maintain toileting hygiene and/or adjust clothing.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1845');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1845ToiletingHygiene", "03", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1845ToiletingHygiene3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1845ToiletingHygiene3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to maintain toileting hygiene.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1850');">(M1850)</a> Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.</label>
                <%=Html.Hidden("StartOfCare_M1850Transferring")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1850Transferring0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently transfer.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1850Transferring1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to transfer with minimal human assistance or with use of an assistive device.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1850Transferring2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to bear weight and pivot during the transfer process but unable to transfer self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1850Transferring3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1850Transferring4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Bedfast, unable to transfer but is able to turn and position self in bed.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1850');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "StartOfCare_M1850Transferring5", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1850Transferring5"><span class="float_left">5 &ndash;</span><span class="normal margin">Bedfast, unable to transfer and is unable to turn and position self.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1860');">(M1860)</a> Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.</label>
                <%=Html.Hidden("StartOfCare_M1860AmbulationLocomotion")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion2"><span class="float_left">2 &ndash;</span><span class="normal margin">Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to walk only with the supervision or assistance of another person at all times.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion4"><span class="float_left">4 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate but is able to wheel self independently.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion5", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion5"><span class="float_left">5 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate and is unable to wheel self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1860');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "StartOfCare_M1860AmbulationLocomotion6", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1860AmbulationLocomotion6"><span class="float_left">6 &ndash;</span><span class="normal margin">Bedfast, unable to ambulate or be up in a chair.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1870');">(M1870)</a> Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.</label>
                <%=Html.Hidden("StartOfCare_M1870FeedingOrEating")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "00", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently feed self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "01", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating1">
                        <span class="float_left">1 &ndash;</span>
                        <span class="normal margin">
                            Able to feed self independently but requires:
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">meal set-up; OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">intermittent assistance or supervision from another person; OR</span></li>
                                <li><span class="float_left">(c)</span><span class="radio">a liquid, pureed or ground meat diet.</span></li>
                            </ul>
                        </span>
                    </label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "02", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to feed self and must be assisted or supervised throughout the meal/snack.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "03", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "04", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1870');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1870FeedingOrEating", "05", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "05" ? true : false, new { @id = "StartOfCare_M1870FeedingOrEating5", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1870FeedingOrEating5"><span class="float_left">5 &ndash;</span><span class="normal margin">Unable to take in nutrients orally or by tube feeding.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1880');">(M1880)</a> Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:</label>
                <%=Html.Hidden("StartOfCare_M1880AbilityToPrepareLightMeal")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1880AbilityToPrepareLightMeal", "00", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1880AbilityToPrepareLightMeal0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1880AbilityToPrepareLightMeal0">
                        <span class="float_left">0 &ndash;</span>
                        <span class="normal margin">
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</span></li>
                            </ul>
                        </span>
                    </label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1880AbilityToPrepareLightMeal", "01", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1880AbilityToPrepareLightMeal1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1880AbilityToPrepareLightMeal1"><span class="float_left">1 &ndash;</span><span class="normal margin"> Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1880');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1880AbilityToPrepareLightMeal", "02", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1880AbilityToPrepareLightMeal2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1880AbilityToPrepareLightMeal2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to prepare any light meals or reheat any delivered meals.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1890');">(M1890)</a> Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.</label>
                <%=Html.Hidden("StartOfCare_M1890AbilityToUseTelephone")%>
                <div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "00", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone0", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to dial numbers and answer calls appropriately and as desired.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "01", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone1", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "02", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone2", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "03", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone3", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to answer the telephone only some of the time or is able to carry on only a limited conversation.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "04", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone4", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to answer the telephone at all but can listen if assisted with equipment.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "05", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "05" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone5", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone5"><span class="float_left">5 &ndash;</span><span class="normal margin">Totally unable to use the telephone.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1890');">?</div>
                    </div>
                    <%=Html.RadioButton("StartOfCare_M1890AbilityToUseTelephone", "NA", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "NA" ? true : false, new { @id = "StartOfCare_M1890AbilityToUseTelephone6", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1890AbilityToUseTelephone6"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient does not have a telephone.</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] nursingInterventions = data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer != "" ? data["485NursingInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485NursingInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions1' class='radio float_left' name='StartOfCare_485NursingInterventions' value='1' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingInterventions1">Physical therapy to evaluate.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions2' class='radio float_left' name='StartOfCare_485NursingInterventions' value='2' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingInterventions2">Occupational therapy to evaluate.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions3' class='radio float_left' name='StartOfCare_485NursingInterventions' value='3' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingInterventions3">SN to assess/instruct on pain management, proper body mechanics and safety measures.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions4' class='radio float_left' name='StartOfCare_485NursingInterventions' value='4' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingInterventions4" class="radio">SN to assess for patient adherence to appropriate activity levels.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions5' class='radio float_left' name='StartOfCare_485NursingInterventions' value='5' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingInterventions5" class="radio">SN to assess patient&rsquo;s compliance with home exercise program.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingInterventions6' class='radio float_left' name='StartOfCare_485NursingInterventions' value='6' type='checkbox' {0} />", nursingInterventions!=null && nursingInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485NursingInterventions6">SN to instruct the</label>
                    <%  var instructRomExcercisePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructRomExcercisePerson") && data["485InstructRomExcercisePerson"].Answer != "" ? data["485InstructRomExcercisePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485InstructRomExcercisePerson", instructRomExcercisePerson)%>
                    <label for="StartOfCare_485NursingInterventions6">on proper ROM exercises and body alignment techniques.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485ADLOrderTemplates">Additional Orders:</label>
                <%  var aDLOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485ADLOrderTemplates") && data["485ADLOrderTemplates"].Answer != "" ? data["485ADLOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485ADLOrderTemplates", aDLOrderTemplates)%>
                <%=Html.TextArea("StartOfCare_485ADLComments", data.ContainsKey("485ADLComments") ? data["485ADLComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485ADLComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] nursingGoals = data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer != "" ? data["485NursingGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485NursingGoals" id="StartOfCare_485EstablishHomeExercisePT" />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingGoals1' class='radio float_left' name='StartOfCare_485NursingGoals' value='1' type='checkbox' {0} />", nursingGoals!=null && nursingGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingGoals1" class="radio">Patient will have increased mobility, self care, endurance, ROM and decreased pain by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingGoals2' class='radio float_left' name='StartOfCare_485NursingGoals' value='2' type='checkbox' {0} />", nursingGoals!=null && nursingGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingGoals2" class="radio">Patient will maintain optimal joint function, increased mobility and independence in ADL&rsquo;s by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingGoals3' class='radio float_left' name='StartOfCare_485NursingGoals' value='3' type='checkbox' {0} />", nursingGoals!=null && nursingGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485NursingGoals3" class="radio">Patient&rsquo;s strength, endurance and mobility will be improved.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485NursingGoals4' class='radio float_left' name='StartOfCare_485NursingGoals' value='4' type='checkbox' {0} />", nursingGoals!=null && nursingGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485NursingGoals4">The</label>
                    <%  var demonstrateROMExcercisePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485DemonstrateROMExcercisePerson") && data["485DemonstrateROMExcercisePerson"].Answer != "" ? data["485DemonstrateROMExcercisePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485DemonstrateROMExcercisePerson", demonstrateROMExcercisePerson)%>
                    <label for="StartOfCare_485NursingGoals4">will demonstrate proper ROM exercise and body alignment techniques.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485ADLGoalTemplates">Additional Goals:</label>
                <%  var aDLGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485ADLGoalTemplates") && data["485ADLGoalTemplates"].Answer != "" ? data["485ADLGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485ADLGoalTemplates", aDLGoalTemplates)%>
                <%=Html.TextArea("StartOfCare_485ADLGoalComments", data.ContainsKey("485ADLGoalComments") ? data["485ADLGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485ADLGoalComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1900');">(M1900)</a> Prior Functioning ADL/IADL: Indicate the patient&rsquo;s usual ability with everyday activities prior to this current illness, exacerbation, or injury. Check only one box in each row.</div>
                <table class="form bordergrid">
                    <thead>
                        <tr>
                            <th colspan="2">Functional Area</th>
                            <th>Independent</th>
                            <th>Needed Some Help</th>
                            <th>Dependent</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="2">
                                a. Self-Care (e.g., grooming, dressing, and bathing)
                                <%=Html.Hidden("StartOfCare_M1900SelfCareFunctioning")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900SelfCareFunctioning", "00", data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1900SelfCareFunctioning0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900SelfCareFunctioning0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900SelfCareFunctioning", "01", data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1900SelfCareFunctioning1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900SelfCareFunctioning1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900SelfCareFunctioning", "02", data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1900SelfCareFunctioning2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900SelfCareFunctioning2" class="radio">2</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                b. Ambulation
                                <%=Html.Hidden("StartOfCare_M1900Ambulation")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Ambulation", "00", data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1900Ambulation0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Ambulation0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Ambulation", "01", data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1900Ambulation1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Ambulation1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Ambulation", "02", data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1900Ambulation2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Ambulation2" class="radio">2</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                c. Transfer
                                <%=Html.Hidden("StartOfCare_M1900Transfer")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Transfer", "00", data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1900Transfer0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Transfer0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Transfer", "01", data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1900Transfer1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Transfer1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900Transfer", "02", data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1900Transfer2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900Transfer2" class="radio">2</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                d. Household tasks (e.g., light meal preparation, laundry, shopping)
                                <%=Html.Hidden("StartOfCare_M1900HouseHoldTasks")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900HouseHoldTasks", "00", data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1900HouseHoldTasks0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900HouseHoldTasks0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900HouseHoldTasks", "01", data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1900HouseHoldTasks1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900HouseHoldTasks1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M1900HouseHoldTasks", "02", data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1900HouseHoldTasks2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M1900HouseHoldTasks2" class="radio">2</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1900');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Fall Assessment</legend>
        <div class="wide_column soc_assessmentcolumn">
            <div class="row">
                <em>One point is assessed for each <strong>yes</strong> selection</em>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericAge65Plus", "1", data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericAge65PlusYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericAge65PlusYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericAge65Plus", "0", data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericAge65PlusNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericAge65PlusNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Age 65+</h5>
                    <%=Html.Hidden("StartOfCare_GenericAge65Plus")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericHypotensionDiagnosis", "1", data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericHypotensionDiagnosisYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericHypotensionDiagnosisYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericHypotensionDiagnosis", "0", data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericHypotensionDiagnosisNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericHypotensionDiagnosisNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Diagnosis (3 or more co-existing)</h5>
                    <label class="smallFont">Assess for hypotension.</label>
                    <%=Html.Hidden("StartOfCare_GenericHypotensionDiagnosis")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericPriorFalls", "1", data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericPriorFallsYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericPriorFallsYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericPriorFalls", "0", data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericPriorFallsNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericPriorFallsNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Prior history of falls within 3 months</h5>
                    <label class="smallFont">Fall definition: "An unintentional change in position resulting in coming to rest on the ground or at a lower level."</label>
                    <%=Html.Hidden("StartOfCare_GenericPriorFalls")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericFallIncontinence", "1", data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericFallIncontinenceYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericFallIncontinenceYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericFallIncontinence", "0", data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericFallIncontinenceNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericFallIncontinenceNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Incontinence</h5>
                    <label class="smallFont">Inability to make it to the bathroom or commode in timely manner. Includes frequency, urgency, and/or nocturia.</label>
                    <%=Html.Hidden("StartOfCare_GenericFallIncontinence")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericVisualImpairment", "1", data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericVisualImpairmentYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericVisualImpairmentYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericVisualImpairment", "0", data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericVisualImpairmentNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericVisualImpairmentNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Visual impairment</h5>
                    <label class="smallFont">Includes macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription.</label>
                    <%=Html.Hidden("StartOfCare_GenericVisualImpairment")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericImpairedFunctionalMobility", "1", data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericImpairedFunctionalMobilityYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericImpairedFunctionalMobilityYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericImpairedFunctionalMobility", "0", data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericImpairedFunctionalMobilityNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericImpairedFunctionalMobilityNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Impaired functional mobility</h5>
                    <label class="smallFont">May include patients who need help with IADL&rsquo;s or ADL&rsquo;s or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices.</label>
                    <%=Html.Hidden("StartOfCare_GenericImpairedFunctionalMobility")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericEnvHazards", "1", data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericEnvHazardsYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericEnvHazardsYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericEnvHazards", "0", data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericEnvHazardsNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericEnvHazardsNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Environmental hazards</h5>
                    <label class="smallFont">May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.</label>
                    <%=Html.Hidden("StartOfCare_GenericEnvHazards")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericPolyPharmacy", "1", data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericPolyPharmacyYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericPolyPharmacyYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericPolyPharmacy", "0", data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericPolyPharmacyNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericPolyPharmacyNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Poly Pharmacy (4 or more prescriptions)</h5>
                    <label class="smallFont">Drugs highly associated with fall risk include but are not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</label>
                    <%=Html.Hidden("StartOfCare_GenericPolyPharmacy")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericPainAffectingFunction", "1", data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericPainAffectingFunctionYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericPainAffectingFunctionYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericPainAffectingFunction", "0", data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericPainAffectingFunctionNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericPainAffectingFunctionNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Pain affecting level of function</h5>
                    <label class="smallFont">Pain often affects an individual&rsquo;s desire or ability to move or pain can be a factor in depression or compliance with safety recommendations.</label>
                    <%=Html.Hidden("StartOfCare_GenericPainAffectingFunction")%>
                </div>
            </div><div class="row assessmentrow">
                <div class="float_right">
                    <%=Html.RadioButton("StartOfCare_GenericCognitiveImpairment", "1", data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCognitiveImpairmentYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericCognitiveImpairmentYes" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("StartOfCare_GenericCognitiveImpairment", "0", data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCognitiveImpairmentNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericCognitiveImpairmentNo" class="inlineradio">No</label>
                </div><div class="float_left">
                    <h5>Cognitive impairment</h5>
                    <label class="smallFont">Could include patients with dementia, Alzheimer&rsquo;s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patient&rsquo;s ability to adhere to the plan of care.</label>
                    <%=Html.Hidden("StartOfCare_GenericCognitiveImpairment")%>
                </div>
            </div><div class="row">
                <div class="float_right">
                    <h5 class="fixed">Total:</h5> <%=Html.TextBox("StartOfCare_485FallAssessmentScore", data.ContainsKey("485FallAssessmentScore") ? data["485FallAssessmentScore"].Answer : "", new { @id = "StartOfCare_485FallAssessmentScore",@class="sn", @size = "10", @maxlength = "10" })%>
                    <em>A score of 4 or more is considered at risk for falling</em>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1910');">(M1910)</a> Has this patient had a multi-factor Fall Risk Assessment (such as falls history, use of multiple medications, mental impairment, toileting frequency, general mobility/transferring impairment, environmental hazards)?</div>
                <%=Html.Hidden("StartOfCare_M1910FallRiskAssessment")%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("StartOfCare_M1910FallRiskAssessment", "00", data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1910FallRiskAssessment0", @class = "radio float_left" })%>
                        <label for="StartOfCare_M1910FallRiskAssessment0"><span class="float_left">0 &ndash;</span><span class="normal margin">No multi-factor falls risk assessment conducted.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M1910FallRiskAssessment", "01", data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1910FallRiskAssessment1", @class = "radio float_left" })%>
                        <label for="StartOfCare_M1910FallRiskAssessment1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, and it does not indicate a risk for falls.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M1910FallRiskAssessment", "02", data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1910FallRiskAssessment2", @class = "radio float_left" })%>
                        <label for="StartOfCare_M1910FallRiskAssessment2"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, and it indicates a risk for falls.</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] instructInterventions = data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer != "" ? data["485InstructInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485InstructInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions1' class='radio float_left' name='StartOfCare_485InstructInterventions' value='1' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructInterventions1" class="radio">SN to instruct patient to wear proper footwear when ambulating.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions2' class='radio float_left' name='StartOfCare_485InstructInterventions' value='2' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructInterventions2" class="radio">SN to instruct patient to use prescribed assistive device when ambulating.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions3' class='radio float_left' name='StartOfCare_485InstructInterventions' value='3' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructInterventions3" class="radio">SN to instruct patient to change positions slowly.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions4' class='radio float_left' name='StartOfCare_485InstructInterventions' value='4' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485InstructInterventions4">SN to instruct the</label>
                    <%  var instructRemoveClutterPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructRemoveClutterPerson") && data["485InstructRemoveClutterPerson"].Answer != "" ? data["485InstructRemoveClutterPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485InstructRemoveClutterPerson", instructRemoveClutterPerson)%>
                    <label for="StartOfCare_485InstructInterventions4">to remove clutter from patient&rsquo;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions5' class='radio float_left' name='StartOfCare_485InstructInterventions' value='5' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485InstructInterventions5">SN to instruct the</label>
                    <%  var instructContactForFallPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructContactForFallPerson") && data["485InstructContactForFallPerson"].Answer != "" ? data["485InstructContactForFallPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485InstructContactForFallPerson", instructContactForFallPerson)%>
                    <label for="StartOfCare_485InstructInterventions5">to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructInterventions6' class='radio float_left' name='StartOfCare_485InstructInterventions' value='6' type='checkbox' {0} />", instructInterventions!=null && instructInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructInterventions6" class="radio">HHA to assist with ADL&rsquo;s &amp; IADL&rsquo;s per HHA care plan.</label>
            </div><div class="row">
                <label for="StartOfCare_485IADLOrderTemplates">Additional Orders:</label>
                <%  var iADLOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IADLOrderTemplates") && data["485IADLOrderTemplates"].Answer != "" ? data["485IADLOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485IADLOrderTemplates", iADLOrderTemplates)%>
                <%=Html.TextArea("StartOfCare_485IADLComments", data.ContainsKey("485IADLComments") ? data["485IADLComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485IADLComments" })%>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Goals</legend>
        <% string[] instructGoals = data.ContainsKey("485InstructGoals") && data["485InstructGoals"].Answer != "" ? data["485InstructGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485InstructGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructGoals1' class='radio float_left' name='StartOfCare_485InstructGoals' value='1' type='checkbox' {0} />", instructGoals!=null && instructGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructGoals1" class="radio">The patient will be free from falls during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485InstructGoals2' class='radio float_left' name='StartOfCare_485InstructGoals' value='2' type='checkbox' {0} />", instructGoals!=null && instructGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485InstructGoals2" class="radio">The patient will be free from injury during the episode.</label>
            </div><div class="row">
                <label for="StartOfCare_485IADLGoalTemplates">Additional Goals:</label>
                <%  var iADLGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485IADLGoalTemplates") && data["485IADLGoalTemplates"].Answer != "" ? data["485IADLGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485IADLGoalTemplates", iADLGoalTemplates)%>
                <%=Html.TextArea("StartOfCare_485IADLGoalComments", data.ContainsKey("485IADLGoalComments") ? data["485IADLGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485IADLGoalComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal2"), $("#StartOfCare_GenericMusculoskeletal2More"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal3"), $("#StartOfCare_GenericMusculoskeletalImpairedMotorSkills"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal4"), $("#StartOfCare_GenericMusculoskeletal4More"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal5"), $("#StartOfCare_GenericMusculoskeletalMobility"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal6"), $("#StartOfCare_GenericMusculoskeletal6More"));
    Oasis.showIfChecked($("#StartOfCare_GenericAssistiveDevice5"), $("#StartOfCare_GenericAssistiveDeviceOther"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal7"), $("#StartOfCare_GenericMusculoskeletal7More"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal9"), $("#StartOfCare_GenericMusculoskeletal9More"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal12"), $("#StartOfCare_GenericMusculoskeletal12More"));
    Oasis.showIfChecked($("#StartOfCare_GenericMusculoskeletal13"), $("#StartOfCare_GenericMusculoskeletal13More"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
    var total = 0;
    $("div.soc_assessmentcolumn div.assessmentrow div.float_right").each(function() { var radioInput = $(this).find("input[type=radio]:checked");  if (radioInput != undefined && radioInput.val()!=undefined) {total = total + parseInt(radioInput.val());  }});
    $("#StartOfCare_485FallAssessmentScore").val(total);
    $("div.soc_assessmentcolumn div.assessmentrow div.float_right input[type=radio]").each(function() { $(this).bind('click', function() {var totalonclick = 0; $("div.soc_assessmentcolumn div.assessmentrow div.float_right").each(function() { var radioInput = $(this).find("input[type=radio]:checked"); if (radioInput != undefined && radioInput.val() != undefined) { totalonclick = totalonclick + parseInt(radioInput.val()); } }); $("#StartOfCare_485FallAssessmentScore").val(totalonclick);});});
</script>