﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareEndocrineForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "Endocrine")%> 
<div class="wrapper main">
    <fieldset>
        <legend>Endocrine</legend>
        <input type="hidden" name="StartOfCare_GenericEndocrineWithinNormalLimits" />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_GenericEndocrineWithinNormalLimits' class='radio float_left' name='StartOfCare_GenericEndocrineWithinNormalLimits' value='1' type='checkbox' {0} />", data.ContainsKey("GenericEndocrineWithinNormalLimits") && data["GenericEndocrineWithinNormalLimits"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericEndocrineWithinNormalLimits">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <label class="float_left">Is patient diabetic?</label>
                <%= Html.Hidden("StartOfCare_GenericPatientDiabetic") %>
                <div class="float_right">
                    <%= Html.RadioButton("StartOfCare_GenericPatientDiabetic", "1", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericPatientDiabetic1", @class = "radio" })%><label for="StartOfCare_GenericPatientDiabetic1" class="inlineradio">Yes</label><%= Html.RadioButton("StartOfCare_GenericPatientDiabetic", "0", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericPatientDiabetic0", @class = "radio" })%>
                    <label for="StartOfCare_GenericPatientDiabetic0" class="inlineradio">No</label>
                </div>
            </div>
            <div class="row diabetic">
                <div class="row">
                    <label class="float_left">Diabetic Management:</label>
                    <% string[] diabeticCareDiabeticManagement = data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer != "" ? data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',') : null; %>
                    <input type="hidden" name="StartOfCare_GenericDiabeticCareDiabeticManagement" value="" />
                    <div class="float_right">
                        <div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareDiabeticManagement1' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareDiabeticManagement' value='1' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("1") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareDiabeticManagement1" class="fixed inlineradio">Diet</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareDiabeticManagement2' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareDiabeticManagement' value='2' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("2") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareDiabeticManagement2" class="fixed inlineradio">Oral Hypoglycemic</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareDiabeticManagement3' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareDiabeticManagement' value='3' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("3") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareDiabeticManagement3" class="fixed inlineradio">Exercise</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareDiabeticManagement4' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareDiabeticManagement' value='4' {0} />", diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("4") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareDiabeticManagement4" class="fixed inlineradio">Insulin</label>
                        </div>
                    </div>
                </div><div class="row">
                    <label class="float_left">Insulin dependent?</label>
                    <%= Html.Hidden("StartOfCare_GenericInsulinDependent") %>
                    <div class="float_right">
                        <%= Html.RadioButton("StartOfCare_GenericInsulinDependent", "1", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericInsulinDependent1", @class = "radio" })%>
                        <label for="StartOfCare_GenericInsulinDependent1" class="inlineradio">Yes</label>
                        <%= Html.RadioButton("StartOfCare_GenericInsulinDependent", "0", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericInsulinDependent0", @class = "radio" })%>
                        <label for="StartOfCare_GenericInsulinDependent0" class="inlineradio">No</label>
                    </div>
                </div><div class="row">
                    <label class="float_left">How long?</label>
                    <div class="float_right">
                        <%= Html.TextBox("StartOfCare_GenericInsulinDependencyDuration", data.ContainsKey("GenericInsulinDependencyDuration") ? data["GenericInsulinDependencyDuration"].Answer : "", new { @id = "StartOfCare_GenericInsulinDependencyDuration", @class = "st", @maxlength = "10" }) %>
                    </div>
                </div><div class="row">
                    <div class="strong">Insulin Administered by:</div>
                    <% string[] diabeticCareInsulinAdministeredby = data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer != "" ? data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',') : null; %>
                    <input type="hidden" name="StartOfCare_GenericDiabeticCareInsulinAdministeredby" value="" />
                    <div class="float_right">
                        <div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareInsulinAdministeredby1' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareInsulinAdministeredby' value='1' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("1") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareInsulinAdministeredby1" class="fixed inlineradio">N/A</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareInsulinAdministeredby2' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareInsulinAdministeredby' value='2' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("2") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareInsulinAdministeredby2" class="fixed inlineradio">Patient</label>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareInsulinAdministeredby3' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareInsulinAdministeredby' value='3' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("3") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareInsulinAdministeredby3" class="fixed inlineradio">Caregiver</label>
                        </div><div class="float_left">
                            <%= string.Format("<input id='StartOfCare_GenericDiabeticCareInsulinAdministeredby4' class='radio float_left' type='checkbox' name='StartOfCare_GenericDiabeticCareInsulinAdministeredby' value='4' {0} />", diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("4") ? "checked='checked'" : "")%>
                            <label for="StartOfCare_GenericDiabeticCareInsulinAdministeredby4" class="fixed inlineradio">SN</label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div><div class="column">
           <div class="row">
                <label class="float_left">Is patient independent with glucometer use?</label>
                <%= Html.Hidden("StartOfCare_GenericGlucometerUseIndependent") %>
                <div class="float_right">
                    <%= Html.RadioButton("StartOfCare_GenericGlucometerUseIndependent", "1", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericGlucometerUseIndependent1", @class = "radio" })%>
                    <label for="StartOfCare_GenericGlucometerUseIndependent1" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("StartOfCare_GenericGlucometerUseIndependent", "0", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericGlucometerUseIndependent0", @class = "radio" })%>
                    <label for="StartOfCare_GenericGlucometerUseIndependent0" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Is caregiver independent with glucometer use?</label>
                <%= Html.Hidden("StartOfCare_GenericCareGiverGlucometerUse") %>
                <div class="float_right">
                    <div class="float_left">
                        <%= Html.RadioButton("StartOfCare_GenericCareGiverGlucometerUse", "1", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCareGiverGlucometerUse1", @class = "radio" })%>
                        <label for="StartOfCare_GenericCareGiverGlucometerUse1" class="inlineradio">Yes</label>
                    </div><div class="float_left">
                        <%= Html.RadioButton("StartOfCare_GenericCareGiverGlucometerUse", "0", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCareGiverGlucometerUse0", @class = "radio" })%>
                        <label for="StartOfCare_GenericCareGiverGlucometerUse0" class="inlineradio">No</label>
                    </div><div class="float_left">
                        <%= Html.RadioButton("StartOfCare_GenericCareGiverGlucometerUse", "2", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "2" ? true : false, new { @id = "StartOfCare_GenericCareGiverGlucometerUse2", @class = "radio" })%>
                        <label for="StartOfCare_GenericCareGiverGlucometerUse2" class="inlineradio">N/A, no caregiver</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="strong">Does patient have any of the following?</div>
                <% string[] patientEdocrineProblem = data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer != "" ? data["GenericPatientEdocrineProblem"].Answer.Split(',') : null; %>
                <input type="hidden" name="StartOfCare_GenericPatientEdocrineProblem" value="" />
                <div class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem1' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='1' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("1")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem1" class="fixed inlineradio">Polyuria</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem2' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='2' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("2")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem2" class="fixed inlineradio">Polydipsia</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem3' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='3' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("3")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem3" class="fixed inlineradio">Polyphagia</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem4' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='4' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("4")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem4" class="fixed inlineradio">Neuropathy</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem5' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='5' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("5")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem5" class="fixed inlineradio">Radiculopathy</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='StartOfCare_GenericPatientEdocrineProblem6' class='radio float_left' type='checkbox' name='StartOfCare_GenericPatientEdocrineProblem' value='6' {0} />", patientEdocrineProblem!=null && patientEdocrineProblem.Contains("6")? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericPatientEdocrineProblem6" class="fixed inlineradio">Thyroid problems</label>
                    </div>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericBloodSugarLevelText" class="float_left">Blood Sugar:</label>
                <div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericBloodSugarLevelText", data.ContainsKey("GenericBloodSugarLevelText") ? data["GenericBloodSugarLevelText"].Answer : "", new { @id = "StartOfCare_GenericBloodSugarLevelText", @class = "st numeric", @maxlength = "5" })%>
                    <%= Html.Hidden("StartOfCare_GenericBloodSugarLevel") %>
                    <label>mg/dl</label>
                </div>
                <div class="clear"></div>
                <div class="float_right">
                    <%= Html.RadioButton("StartOfCare_GenericBloodSugarLevel", "Random", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Random" ? true : false, new { @id = "StartOfCare_GenericBloodSugarLevelRandom", @class = "radio" })%>
                    <label for="StartOfCare_GenericBloodSugarLevelRandom" class="inlineradio">Random</label>
                    <%= Html.RadioButton("StartOfCare_GenericBloodSugarLevel", "Fasting", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Fasting" ? true : false, new { @id = "StartOfCare_GenericBloodSugarLevelFasting", @class = "radio" })%>
                    <label for="StartOfCare_GenericBloodSugarLevelFasting" class="inlineradio">Fasting</label>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericBloodSugarCheckedBy" class="float_left">Blood sugar checked by:</label>
                <div class="float_right">
                    <%  var diabeticCarePerformedby = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Patient", Value = "1" },
                            new SelectListItem { Text = "SN", Value = "2" },
                            new SelectListItem { Text = "Caregiver", Value = "3" }
                        }, "Value", "Text", data.ContainsKey("GenericBloodSugarCheckedBy") ? data["GenericBloodSugarCheckedBy"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = "StartOfCare_GenericBloodSugarCheckedBy" })%>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericEndocrineComments" class="strong">Comments:</label>
                <%= Html.TextArea("StartOfCare_GenericEndocrineComments", data.ContainsKey("GenericEndocrineComments") ? data["GenericEndocrineComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericEndocrineComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] endocrineInterventions = data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer != "" ? data["485EndocrineInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485EndocrineInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions1' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='1' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("1") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="StartOfCare_485EndocrineInterventions1">SN to assess</label>
                    <%  var abilityToManageDiabetic = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485AbilityToManageDiabetic") && data["485AbilityToManageDiabetic"].Answer != "" ? data["485AbilityToManageDiabetic"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485AbilityToManageDiabetic", abilityToManageDiabetic)%>
                    <label for="StartOfCare_485EndocrineInterventions1">ability to manage diabetic disease process.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions2' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='2' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485EndocrineInterventions2" class="radio">SN to perform BG checks every visit &amp; PRN.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions3' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='3' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485EndocrineInterventions3" class="radio">SN to instruct on insulin preparation, administration, site rotation and disposal of supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions4' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='4' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("4") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485EndocrineInterventions4" class="radio">SN to assess/instruct on diabetic management to include: nail, skin &amp; foot care, medication administration and proper diet.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions5' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='5' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("5") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="StartOfCare_485EndocrineInterventions5">SN to administer</label>
                    <%= Html.TextBox("StartOfCare_485AmountOfInsuline", data.ContainsKey("485AmountOfInsuline") ? data["485AmountOfInsuline"].Answer : "", new { @id = "StartOfCare_485AmountOfInsuline", @maxlength = "30" })%>
                    <label for="StartOfCare_485EndocrineInterventions5">insulin as follows:</label>
                    <%= Html.TextBox("StartOfCare_485AmountOfInsulineAsFollows", data.ContainsKey("485AmountOfInsulineAsFollows") ? data["485AmountOfInsulineAsFollows"].Answer : "", new { @id = "StartOfCare_485AmountOfInsulineAsFollows", @maxlength = "50" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineInterventions6' class='radio float_left' name='StartOfCare_485EndocrineInterventions' value='6' type='checkbox' {0} />", endocrineInterventions != null && endocrineInterventions.Contains("6") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="StartOfCare_485EndocrineInterventions6">SN to prefill syringes with</label>
                    <%= Html.TextBox("StartOfCare_485PrefillOfInsuline", data.ContainsKey("485PrefillOfInsuline") ? data["485PrefillOfInsuline"].Answer : "", new { @id = "StartOfCare_485PrefillOfInsuline", @maxlength = "30" })%>
                    <label for="StartOfCare_485EndocrineInterventions6">insulin as follows:</label>
                    <%= Html.TextBox("StartOfCare_485PrefillOfInsulineAsFollows", data.ContainsKey("485PrefillOfInsulineAsFollows") ? data["485PrefillOfInsulineAsFollows"].Answer : "", new { @id = "StartOfCare_485PrefillOfInsulineAsFollows", @maxlength = "50" })%>.
                </span>
            </div><div class="row">
                <label for="StartOfCare_485EndocrineOrderTemplates" class="strong">Additional Orders:</label>
                <%  var endocrineOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EndocrineOrderTemplates") && data["485EndocrineOrderTemplates"].Answer != "" ? data["485EndocrineOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("StartOfCare_485EndocrineOrderTemplates", endocrineOrderTemplates)%>
                <%= Html.TextArea("StartOfCare_485EndocrineInterventionComments", data.ContainsKey("485EndocrineInterventionComments") ? data["485EndocrineInterventionComments"].Answer : "", 2, 70, new { @id = "StartOfCare_485EndocrineInterventionComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] endocrineGoals = data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer != "" ? data["485EndocrineGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485EndocrineGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals1' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='1' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("1")? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485EndocrineGoals1" class="radio">Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals2' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='2' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("2")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var glucometerUseIndependencePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GlucometerUseIndependencePerson") && data["485GlucometerUseIndependencePerson"].Answer != "" ? data["485GlucometerUseIndependencePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485GlucometerUseIndependencePerson", glucometerUseIndependencePerson) %>
                    <label for="StartOfCare_485EndocrineGoals2">will be independent with glucometer use by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals3' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='3' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("3")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var independentInsulinAdministrationPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485IndependentInsulinAdministrationPerson") && data["485IndependentInsulinAdministrationPerson"].Answer != "" ? data["485IndependentInsulinAdministrationPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485IndependentInsulinAdministrationPerson", independentInsulinAdministrationPerson) %>
                    <label for="StartOfCare_485EndocrineGoals3">will be independent with insulin administration by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals4' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='4' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("4")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var verbalizeProperFootCarePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeProperFootCarePerson") && data["485VerbalizeProperFootCarePerson"].Answer != "" ? data["485VerbalizeProperFootCarePerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485VerbalizeProperFootCarePerson", verbalizeProperFootCarePerson) %>
                    <label for="StartOfCare_485EndocrineGoals4">will verbalize understanding of proper diabetic foot care by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals5' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='5' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("5")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var diabetesManagement = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485DiabetesManagement") && data["485DiabetesManagement"].Answer != "" ? data["485DiabetesManagement"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485DiabetesManagement", diabetesManagement)%>
                    <label for="StartOfCare_485EndocrineGoals5">will verbalize knowledge of diabetes management, S&amp;S of complications, hypo/hyperglycemia, foot care and management during illness or stress by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals6' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='6' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("6")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var generalDemonstration = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GeneralDemonstration") && data["485GeneralDemonstration"].Answer != "" ? data["485GeneralDemonstration"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485GeneralDemonstration", generalDemonstration)%>
                    <label for="StartOfCare_485EndocrineGoals6">will demonstrate correct insulin preparation, injection procedure, storage and disposal of supplies by the end of the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485EndocrineGoals7' class='radio float_left' name='StartOfCare_485EndocrineGoals' value='7' type='checkbox' {0} />", endocrineGoals!=null && endocrineGoals.Contains("7")? "checked='checked'" : "" ) %>
                <span class="radio">
                    <%  var generalVerbalization = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485GeneralVerbalization") && data["485GeneralVerbalization"].Answer != "" ? data["485GeneralVerbalization"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485GeneralVerbalization", generalVerbalization)%>
                    <label for="StartOfCare_485EndocrineGoals7">will verbalize understanding of the importance of keeping blood glucose levels within parameters by the end of the episode.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485EndocrineGoalTemplates" class="strong">Additional Goals:</label>
                <%  var endocrineGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EndocrineGoalTemplates") && data["485EndocrineGoalTemplates"].Answer != "" ? data["485EndocrineGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485EndocrineGoalTemplates", endocrineGoalTemplates) %>
                <%= Html.TextArea("StartOfCare_485EndocrineGoalComments", data.ContainsKey("485EndocrineGoalComments") ? data["485EndocrineGoalComments"].Answer : "", 2, 70, new { @id = "StartOfCare_485EndocrineGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfRadioEquals("StartOfCare_GenericPatientDiabetic", "1", $("#window_startofcare .diabetic"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>