﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareRiskAssessmentForm" })) { %>
<% var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "RiskAssessment")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1032');">(M1032)</a> Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization? (Mark all that apply.)</label>
                <div>
                    <input name="StartOfCare_M1032HospitalizationRiskRecentDecline" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskRecentDecline' name='StartOfCare_M1032HospitalizationRiskRecentDecline' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskRecentDecline") && data["M1032HospitalizationRiskRecentDecline"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_M1032HospitalizationRiskRecentDecline"><span class="float_left">1 &ndash;</span><span class="normal margin">Recent decline in mental, emotional, or behavioral status</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1032HospitalizationRiskMultipleHosp" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskMultipleHosp' name='StartOfCare_M1032HospitalizationRiskMultipleHosp' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskMultipleHosp") && data["M1032HospitalizationRiskMultipleHosp"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskMultipleHosp"><span class="float_left">2 &ndash;</span><span class="normal margin">Multiple hospitalizations (2 or more) in the past 12 months</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1032HospitalizationRiskHistoryOfFall" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskHistoryOfFall' name='StartOfCare_M1032HospitalizationRiskHistoryOfFall' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskHistoryOfFall") && data["M1032HospitalizationRiskHistoryOfFall"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskHistoryOfFall"><span class="float_left">3 &ndash;</span><span class="normal margin">History of falls (2 or more falls &mdash; or any fall with an injury &mdash; in the past year)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1032HospitalizationRiskMedications" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskMedications' name='StartOfCare_M1032HospitalizationRiskMedications' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskMedications") && data["M1032HospitalizationRiskMedications"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskMedications"><span class="float_left">4 &ndash;</span><span class="normal margin">Taking five or more medications</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1032HospitalizationRiskFrailty" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskFrailty' name='StartOfCare_M1032HospitalizationRiskFrailty' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskFrailty") && data["M1032HospitalizationRiskFrailty"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskFrailty"><span class="float_left">5 &ndash;</span><span class="normal margin">Frailty indicators, e.g., weight loss, self-reported exhaustion</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1032HospitalizationRiskOther" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskOther' name='StartOfCare_M1032HospitalizationRiskOther' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskOther") && data["M1032HospitalizationRiskOther"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskOther"><span class="float_left">6 &ndash;</span><span class="normal margin">Other</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1032');">?</div>
                    </div>
                    <input name="StartOfCare_M1032HospitalizationRiskNone" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1032HospitalizationRiskNone' name='StartOfCare_M1032HospitalizationRiskNone' value='1' class='radio float_left M1032' type='checkbox' {0} />", data.ContainsKey("M1032HospitalizationRiskNone") && data["M1032HospitalizationRiskNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1032HospitalizationRiskNone"><span class="float_left">7 &ndash;</span><span class="normal margin">None of the above</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1036');">(M1036)</a> Risk Factors, either present or past, likely to affect current health status and/or outcome: (Mark all that apply.)</label>
                <div>
                    <input name="StartOfCare_M1036RiskFactorsSmoking" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsSmoking' name='StartOfCare_M1036RiskFactorsSmoking' value='1' class='radio float_left M1036' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsSmoking") && data["M1036RiskFactorsSmoking"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1036RiskFactorsSmoking"><span class="float_left">1 &ndash;</span><span class="normal margin">Smoking</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1036RiskFactorsObesity" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsObesity' name='StartOfCare_M1036RiskFactorsObesity' value='1' class='radio float_left M1036' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsObesity") && data["M1036RiskFactorsObesity"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1036RiskFactorsObesity"><span class="float_left">2 &ndash;</span><span class="normal margin">Obesity</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1036RiskFactorsAlcoholism" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsAlcoholism' name='StartOfCare_M1036RiskFactorsAlcoholism' value='1' class='radio float_left M1036' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsAlcoholism") && data["M1036RiskFactorsAlcoholism"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1036RiskFactorsAlcoholism"><span class="float_left">3 &ndash;</span><span class="normal margin">Alcohol dependency</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1036RiskFactorsDrugs" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsDrugs' name='StartOfCare_M1036RiskFactorsDrugs' value='1' class='radio float_left M1036' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsDrugs") && data["M1036RiskFactorsDrugs"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1036RiskFactorsDrugs"><span class="float_left">4 &ndash;</span><span class="normal margin">Drug dependency</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="StartOfCare_M1036RiskFactorsNone" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsNone' name='StartOfCare_M1036RiskFactorsNone' value='1' class='radio float_left M1036' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsNone") && data["M1036RiskFactorsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="StartOfCare_M1036RiskFactorsNone"><span class="float_left">5 &ndash;</span><span class="normal margin">None of the above</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1036');">?</div>
                    </div>
                    <input name="StartOfCare_M1036RiskFactorsUnknown" value=" " type="hidden" />
                    <%= string.Format("<input id='StartOfCare_M1036RiskFactorsUnknown' name='StartOfCare_M1036RiskFactorsUnknown' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1036RiskFactorsUnknown") && data["M1036RiskFactorsUnknown"].Answer == "1" ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_M1036RiskFactorsUnknown"><span class="float_left">UK &ndash;</span><span class="normal margin">Unknown</span></label>
                    <div class="clear"></div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1034');">(M1034)</a> Overall Status: Which description best fits the patient&rsquo;s overall status? (Check one)</label>
                <%= Html.Hidden("StartOfCare_M1034OverallStatus") %>
                <div>
                    <%= Html.RadioButton("StartOfCare_M1034OverallStatus", "00", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "00" ? true : false, new { @id = "M1034OverallStatus00", @class = "radio float_left" }) %>
                    <label for="M1034OverallStatus00"><span class="float_left">0 &ndash;</span><span class="normal margin">The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient&rsquo;s age).</span></label>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1034OverallStatus", "01", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "01" ? true : false, new { @id = "M1034OverallStatus01", @class = "radio float_left" }) %>
                    <label for="M1034OverallStatus01"><span class="float_left">1 &ndash;</span><span class="normal margin">The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications and death (beyond those typical of the patient&rsquo;s age).</span></label>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1034OverallStatus", "02", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "02" ? true : false, new { @id = "M1034OverallStatus02", @class = "radio float_left" }) %>
                    <label for="M1034OverallStatus02"><span class="float_left">2 &ndash;</span><span class="normal margin">The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death.</span></label>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1034OverallStatus", "03", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "03" ? true : false, new { @id = "M1034OverallStatus03", @class = "radio float_left" }) %>
                    <label for="M1034OverallStatus03"><span class="float_left">3 &ndash;</span><span class="normal margin">The patient has serious progressive conditions that could lead to death within a year.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1034');">?</div>
                    </div>
                    <%= Html.RadioButton("StartOfCare_M1034OverallStatus", "UK", data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "UK" ? true : false, new { @id = "M1034OverallStatusUK", @class = "radio float_left" }) %>
                    <label for="M1034OverallStatusUK"><span class="float_left">UK &ndash;</span><span class="normal margin">The patient&rsquo;s situation is unknown or unclear.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Interventions</legend>
        <input type="hidden" name="StartOfCare_485RiskAssessmentIntervention" value=" " />
        <div class="wide_column">
            <div class="row"> <%string[] riskAssessmentIntervention = data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer != "" ? data["485RiskAssessmentIntervention"].Answer.Split(',') : null; %>
                <%= string.Format("<input id='StartOfCare_485RiskAssessmentIntervention1' name='StartOfCare_485RiskAssessmentIntervention' value='1' class='radio float_left' type='checkbox' {0} />", riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("1") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485RiskAssessmentIntervention1" class="radio">SN to assist patient to obtain ERS button.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485RiskAssessmentIntervention2' name='StartOfCare_485RiskAssessmentIntervention' value='2' class='radio float_left' type='checkbox' {0} />", riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("2") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485RiskAssessmentIntervention2" class="radio">SN to develop individualized emergency plan with patient.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485RiskAssessmentIntervention3' name='StartOfCare_485RiskAssessmentIntervention' value='3' class='radio float_left' type='checkbox' {0} />", riskAssessmentIntervention != null && riskAssessmentIntervention.Contains("3") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485RiskAssessmentIntervention3" class="radio">SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.</label>
            </div><div class="row">
                <label for="StartOfCare_485RiskInterventionTemplates" class="float_left">Additional Orders</label>
                <%  var riskInterventionTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "Sample Template", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RiskInterventionTemplates") && data["485RiskInterventionTemplates"].Answer != "" ? data["485RiskInterventionTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("StartOfCare_485RiskInterventionTemplates", riskInterventionTemplates) %>
                <%= Html.TextArea("StartOfCare_485RiskInterventionComments", data.ContainsKey("485RiskInterventionComments") ? data["485RiskInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485RiskInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <input type="hidden" name="StartOfCare_485RiskAssessmentGoals" value=" " />
        <div class="wide_column">
            <div class="row"> <%string[] riskAssessmentGoals = data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer != "" ? data["485RiskAssessmentGoals"].Answer.Split(',') : null; %>
                <%= string.Format("<input id='StartOfCare_485RiskAssessmentGoals1' name='StartOfCare_485RiskAssessmentGoals' value='1' class='radio float_left' type='checkbox' {0} />", riskAssessmentGoals != null && riskAssessmentGoals.Contains("1") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485RiskAssessmentGoals1" class="radio">The patient will have no hospitalizations during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485RiskAssessmentGoals2' name='StartOfCare_485RiskAssessmentGoals' value='2' class='radio float_left' type='checkbox' {0} />", riskAssessmentGoals != null && riskAssessmentGoals.Contains("2") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="StartOfCare_485RiskAssessmentGoals2">The</label>
                    <%  var verbalizeEmergencyPlanPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer != "" ? data["485VerbalizeEmergencyPlanPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("StartOfCare_485VerbalizeEmergencyPlanPerson", verbalizeEmergencyPlanPerson) %>
                    <label for="StartOfCare_485RiskAssessmentGoals2">will verbalize understanding of individualized emergency plan by the end of the episode.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485RiskGoalTemplates" class="float_left">Additional Goals</label>
                <%  var riskGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "Sample Template", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RiskGoalTemplates") && data["485RiskGoalTemplates"].Answer != "" ? data["485RiskGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("StartOfCare_485RiskGoalTemplates", riskGoalTemplates) %>
                <%= Html.TextArea("StartOfCare_485RiskGoalComments", data.ContainsKey("485RiskGoalComments") ? data["485RiskGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485RiskGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);"  onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.noneOfTheAbove($("#StartOfCare_M1032HospitalizationRiskNone"), $("#window_startofcare .M1032"));
    Oasis.noneOfTheAbove($("#StartOfCare_M1036RiskFactorsUnknown"), $("#window_startofcare .M1036"));
    Oasis.goals($(".goals"));
</script>