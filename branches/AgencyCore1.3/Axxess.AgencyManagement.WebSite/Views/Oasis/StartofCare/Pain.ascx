﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCarePainForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "Pain")%>
<div class="wrapper main">
    <fieldset>
        <legend>Pain Scale</legend>
        <div class="wide_column align_center">
            <div class="row">
                <img src="/Images/painscales3.jpg" /><br />
                <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&rsquo;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="StartOfCare_GenericPainOnSetDate" class="float_left">Onset Date</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("StartOfCare_GenericPainOnSetDate").Value(data.ContainsKey("GenericPainOnSetDate") && data["GenericPainOnSetDate"].Answer.IsNotNullOrEmpty() ? data["GenericPainOnSetDate"].Answer : "").HtmlAttributes(new { @id = "StartOfCare_GenericPainOnSetDate", @class = "date" }) %></div>
            </div><div class="row">
                <label for="StartOfCare_GenericIntensityOfPain" class="float_left">Pain Intensity:</label>
                <div class="float_right">
                    <%  var painIntensity = new SelectList(new[] {
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5 = Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer != "" ? data["GenericIntensityOfPain"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericIntensityOfPain", painIntensity, new { @id = "StartOfCare_GenericIntensityOfPain" }) %>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericLocationOfPain" class="float_left">Primary Site</label>
                <div class="float_right"><%= Html.TextBox("StartOfCare_GenericLocationOfPain", data.ContainsKey("GenericLocationOfPain") ? data["GenericLocationOfPain"].Answer : "", new { @id = "StartOfCare_GenericLocationOfPain", @maxlength = "80" }) %></div>
            </div><div class="row">
                <label for="StartOfCare_GenericWhatMakesPainBetter" class="strong">What makes pain better</label>
                <div><%= Html.TextArea("StartOfCare_GenericWhatMakesPainBetter", data.ContainsKey("GenericWhatMakesPainBetter") ? data["GenericWhatMakesPainBetter"].Answer : "", 2, 70, new { @id = "StartOfCare_GenericWhatMakesPainBetter" }) %></div>
            </div><div class="row">
                <label for="StartOfCare_GenericPatientPainGoal" class="strong">Patient&rsquo;s pain goal</label>
                <div><%= Html.TextArea("StartOfCare_GenericPatientPainGoal", data.ContainsKey("GenericPatientPainGoal") ? data["GenericPatientPainGoal"].Answer : "", 2, 70, new { @id = "StartOfCare_GenericPatientPainGoal" }) %></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="StartOfCare_GenericDurationOfPain" class="float_left">Duration</label>
                <div class="float_right">
                    <%  var duration = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Continuous", Value = "1" },
                            new SelectListItem { Text = "Intermittent", Value = "2" }
                        }, "Value", "Text", data.ContainsKey("GenericDurationOfPain") ? data["GenericDurationOfPain"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericDurationOfPain", duration, new { @id = "StartOfCare_GenericDurationOfPain" })%>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericQualityOfPain" class="float_left">Description</label>
                <div class="float_right">
                    <%  var painDescription = new SelectList(new[] { 
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Aching", Value = "1" },
                            new SelectListItem { Text = "Throbbing", Value = "2" },
                            new SelectListItem { Text = "Burning", Value = "3" },
                            new SelectListItem { Text = "Sharp", Value = "4" },
                            new SelectListItem { Text = "Tender", Value = "5" },
                            new SelectListItem { Text = "Other", Value = "6" }
                        } , "Value", "Text", data.ContainsKey("GenericQualityOfPain") ? data["GenericQualityOfPain"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericQualityOfPain", painDescription, new { @id = "StartOfCare_GenericQualityOfPain" })%>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericMedicationEffectiveness" class="strong">Current Pain Management Effectiveness:</label>
                <div class="float_right">
                    <%  var currentPainManagementEffectiveness = new SelectList(new[] {
                            new SelectListItem { Text = "N/A", Value = "0" },
                            new SelectListItem { Text = "Effective", Value = "1" },
                            new SelectListItem { Text = "Not Effective", Value = "2" }
                        }, "Value", "Text", data.ContainsKey("GenericMedicationEffectiveness") ? data["GenericMedicationEffectiveness"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericMedicationEffectiveness", currentPainManagementEffectiveness, new { @id = "StartOfCare_GenericMedicationEffectiveness" })%>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericWhatMakesPainWorse" class="strong">What makes pain worse</label>
                <div><%= Html.TextArea("StartOfCare_GenericWhatMakesPainWorse", data.ContainsKey("GenericWhatMakesPainWorse") ? data["GenericWhatMakesPainWorse"].Answer : "", 2, 70, new { @id = "StartOfCare_GenericWhatMakesPainWorse" }) %></div>
            </div><div class="row">
                <label for="StartOfCare_GenericPatientPainComment" class="strong">Comments:</label>
                <div><%= Html.TextArea("StartOfCare_GenericPatientPainComment", data.ContainsKey("GenericPatientPainComment") ? data["GenericPatientPainComment"].Answer : "", 2, 70, new { @id = "StartOfCare_GenericPatientPainComment" })%></div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1240');">(M1240)</a> Has this patient had a formal Pain Assessment using a standardized pain assessment tool (appropriate to the patient&rsquo;s ability to communicate the severity of pain)?</label>
                <%= Html.Hidden("StartOfCare_M1240FormalPainAssessment") %>
                <div>
                    <%= Html.RadioButton("StartOfCare_M1240FormalPainAssessment", "00", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1240FormalPainAssessment00", @class = "radio float_left" }) %>
                    <label for="StartOfCare_M1240FormalPainAssessment00"><span class="float_left">0 &ndash;</span><span class="normal margin">No standardized assessment conducted</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1240FormalPainAssessment", "01", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1240FormalPainAssessment01", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1240FormalPainAssessment01"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, and it does not indicate severe pain</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1240');">?</div>
                    </div>
                    <%= Html.RadioButton("StartOfCare_M1240FormalPainAssessment", "02", data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1240FormalPainAssessment02", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1240FormalPainAssessment02"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, and it indicates severe pain</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1242');">(M1242)</a> Frequency of Pain Interfering with patient&rsquo;s activity or movement</label>
                <%= Html.Hidden("StartOfCare_M1242PainInterferingFrequency") %>
                <div>
                    <%= Html.RadioButton("StartOfCare_M1242PainInterferingFrequency", "00", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? true : false, new { @id = "StartOfCare_M1242PainInterferingFrequency00", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1242PainInterferingFrequency00"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient has no pain</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1242PainInterferingFrequency", "01", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? true : false, new { @id = "StartOfCare_M1242PainInterferingFrequency01", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1242PainInterferingFrequency01"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient has pain that does not interfere with activity or movement</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1242PainInterferingFrequency", "02", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? true : false, new { @id = "StartOfCare_M1242PainInterferingFrequency02", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1242PainInterferingFrequency02"><span class="float_left">2 &ndash;</span><span class="normal margin">Less often than daily</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("StartOfCare_M1242PainInterferingFrequency", "03", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? true : false, new { @id = "StartOfCare_M1242PainInterferingFrequency03", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1242PainInterferingFrequency03"><span class="float_left">3 &ndash;</span><span class="normal margin">Daily, but not constantly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1242');">?</div>
                    </div>
                    <%= Html.RadioButton("StartOfCare_M1242PainInterferingFrequency", "04", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? true : false, new { @id = "StartOfCare_M1242PainInterferingFrequency04", @class = "radio float_left" })%>
                    <label for="StartOfCare_M1242PainInterferingFrequency04"><span class="float_left">4 &ndash;</span><span class="normal margin">All of the time</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] painInterventions = data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer != "" ? data["485PainInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485PainInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainInterventions1' name='StartOfCare_485PainInterventions' value='1' type='checkbox' {0} />",  painInterventions!=null && painInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainInterventions1" class="radio">SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainInterventions2' name='StartOfCare_485PainInterventions' value='2' type='checkbox' {0} />",  painInterventions!=null && painInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainInterventions2" class="radio">SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainInterventions3' name='StartOfCare_485PainInterventions' value='3' type='checkbox' {0} />",  painInterventions!=null && painInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainInterventions3" class="radio">SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainInterventions4' name='StartOfCare_485PainInterventions' value='4' type='checkbox' {0} />",  painInterventions!=null && painInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485PainInterventions4">SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than </label>
                    <%= Html.TextBox("StartOfCare_485PainTooGreatLevel", data.ContainsKey("485PainTooGreatLevel") ? data["485PainTooGreatLevel"].Answer : "", new { @id = "StartOfCare_485PainTooGreatLevel", @class = "zip", @maxlength = "10" }) %>
                    <label for="StartOfCare_485PainInterventions5">, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&rsquo;s normal activities.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485PainInterventionComments" class="strong">Additional Orders</label>
                <%  var painOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485PainOrderTemplates") && data["485PainOrderTemplates"].Answer != "" ? data["485PainOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("StartOfCare_485PainOrderTemplates", painOrderTemplates) %>
                <%= Html.TextArea("StartOfCare_485PainInterventionComments", data.ContainsKey("485PainInterventionComments") ? data["485PainInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485PainInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Goals</legend>
        <% string[] painGoals = data.ContainsKey("485PainGoals") && data["485PainGoals"].Answer != "" ? data["485PainGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485PainGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainGoals1' name='StartOfCare_485PainGoals' value='1' type='checkbox' {0} />",  painGoals!=null && painGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainGoals1" class="radio">Patient will verbalize understanding of proper use of pain medication by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainGoals2' name='StartOfCare_485PainGoals' value='2' type='checkbox' {0} />",  painGoals!=null && painGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainGoals2" class="radio">PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input class='radio float_left' id='StartOfCare_485PainGoals3' name='StartOfCare_485PainGoals' value='3' type='checkbox' {0} />",  painGoals!=null && painGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485PainGoals3" class="radio">Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.</label>
            </div><div class="row">
                <label for="StartOfCare_485PainGoalComments" class="strong">Additional Goals</label>
                <%  var painGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485PainGoalTemplates") && data["485PainGoalTemplates"].Answer != "" ? data["485PainGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("StartOfCare_485PainGoalTemplates", painGoalTemplates) %>
                <%= Html.TextArea("StartOfCare_485PainGoalComments", data.ContainsKey("485PainGoalComments") ? data["485PainGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485PainGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
</script>