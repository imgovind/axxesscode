﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareCardiacForm" })) {
       var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "Cardiac")%> 
<div class="wrapper main">
    <fieldset>
        <legend>Cardiovascular</legend>
        <% string[] genericCardioVascular = data.ContainsKey("GenericCardioVascular") && data["GenericCardioVascular"].Answer != "" ? data["GenericCardioVascular"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_GenericCardioVascular" value=" " />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_GenericCardioVascular1' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='1' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_GenericCardioVascular1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <div class="float_left">
                   <%= string.Format("<input id='StartOfCare_GenericCardioVascular2' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='2' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("2") ? "checked='checked'" : "" ) %>
                   <label for="StartOfCare_GenericCardioVascular2" class="radio">Heart Rhythm:</label>
                </div><div class="float_right">
                    <%  var heartRhythm = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Regular/WNL", Value = "1" },
                            new SelectListItem { Text = "Tachycardia", Value = "2" },
                            new SelectListItem { Text = "Bradycardia", Value = "3" },
                            new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
                            new SelectListItem { Text = "Other", Value = "5" }
                        }, "Value", "Text", data.ContainsKey("GenericHeartSoundsType") ? data["GenericHeartSoundsType"].Answer : "0");%>
                    <%= Html.DropDownList("StartOfCare_GenericHeartSoundsType", heartRhythm, new { @id = "StartOfCare_GenericHeartSoundsType" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular3' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='3' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular3" class="radio">Cap Refill:</label>
                    <%= Html.Hidden("StartOfCare_GenericCapRefillLessThan3") %>
                </div><div id="StartOfCare_GenericCardioVascular3More" class="float_right">
                    <%= Html.RadioButton("StartOfCare_GenericCapRefillLessThan3", "1", data.ContainsKey("GenericCapRefillLessThan3") && data["GenericCapRefillLessThan3"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCapRefillLessThan31", @class = "radio" }) %>
                    <label for="StartOfCare_GenericCapRefillLessThan31" class="inlineradio">&lt;3 sec</label>
                    <%= Html.RadioButton("StartOfCare_GenericCapRefillLessThan3", "0", data.ContainsKey("GenericCapRefillLessThan3") && data["GenericCapRefillLessThan3"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCapRefillLessThan30", @class = "radio" })%>
                    <label for="StartOfCare_GenericCapRefillLessThan30" class="inlineradio">&gt;3 sec</label>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular4' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='4' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular4" class="radio">Pulses:</label>
                </div><div id="StartOfCare_GenericCardioVascular4More" class="rel float_right">
                    <div class="float_right">
                        <label for="StartOfCare_GenericCardiovascularRadial">Radial:</label>
                        <%  var radialPulse = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "1+ Weak", Value = "1" },
                                new SelectListItem { Text = "2+ Normal", Value = "2" },
                                new SelectListItem { Text = "3+ Strong", Value = "3" },
                                new SelectListItem { Text = "4+ Bounding", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericCardiovascularRadial") ? data["GenericCardiovascularRadial"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_GenericCardiovascularRadial", radialPulse, new { @id = "StartOfCare_GenericCardiovascularRadial" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularRadialPosition", "0", data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCardiovascularRadialPosition0", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularRadialPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularRadialPosition", "1", data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCardiovascularRadialPosition1", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularRadialPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularRadialPosition", "2", data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "2" ? true : false, new { @id = "StartOfCare_GenericCardiovascularRadialPosition2", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularRadialPosition2" class="inlineradio">Right</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <label for="StartOfCare_GenericCardiovascularPedal">Pedal:</label>
                        <%  var pedalPulse = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "1+ Weak", Value = "1" },
                                new SelectListItem { Text = "2+ Normal", Value = "2" },
                                new SelectListItem { Text = "3+ Strong", Value = "3" },
                                new SelectListItem { Text = "4+ Bounding", Value = "4" }
                            } , "Value", "Text", data.ContainsKey("GenericCardiovascularPedal") ? data["GenericCardiovascularPedal"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_GenericCardiovascularPedal", pedalPulse, new { @id = "StartOfCare_GenericCardiovascularPedal" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularPedalPosition", "0", data.ContainsKey("GenericCardiovascularPedalPosition") && data["GenericCardiovascularRadialPosition"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCardiovascularPedalPosition0", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularPedalPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularPedalPosition", "1", data.ContainsKey("GenericCardiovascularPedalPosition") && data["GenericCardiovascularRadialPosition"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCardiovascularPedalPosition1", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularPedalPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("StartOfCare_GenericCardiovascularPedalPosition", "2", data.ContainsKey("GenericCardiovascularPedalPosition") && data["GenericCardiovascularRadialPosition"].Answer == "2" ? true : false, new { @id = "StartOfCare_GenericCardiovascularPedalPosition2", @class = "radio" })%>
                        <label for="StartOfCare_GenericCardiovascularPedalPosition2" class="inlineradio">Right</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular5' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='5' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("5") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular5" class="radio">Edema:</label>
                </div><div id="StartOfCare_GenericCardioVascular5More" class="rel margin float_right">
                    <% string[] genericPittingEdemaType = data.ContainsKey("GenericPittingEdemaType") && data["GenericPittingEdemaType"].Answer != "" ? data["GenericPittingEdemaType"].Answer.Split(',') : null; %>
                    <input type="hidden" name="StartOfCare_GenericPittingEdemaType" value="" />
                    <label for="StartOfCare_GenericEdemaLocation">Location:</label>
                    <div class="float_right">
                        <%= Html.TextBox("StartOfCare_GenericEdemaLocation", data.ContainsKey("GenericEdemaLocation") ? data["GenericEdemaLocation"].Answer : "", new { @id = "StartOfCare_GenericEdemaLocation", @maxlength = "50" })%>
                    </div>
                    <div class="clear"></div>
                    <%= string.Format("<input id='StartOfCare_GenericPittingEdemaType1' class='radio float_left' name='StartOfCare_GenericPittingEdemaType' value='1' type='checkbox' {0} />", genericPittingEdemaType != null && genericPittingEdemaType.Contains("1") ? "checked='checked'" : "")%>
                    <label for="StartOfCare_GenericPittingEdemaType1">Pitting</label>
                    <div class="float_right">
                        <%  var pitting = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "1+", Value = "1" },
                                new SelectListItem { Text = "2+", Value = "2" },
                                new SelectListItem { Text = "3+", Value = "3" },
                                new SelectListItem { Text = "4+", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericEdemaPitting") ? data["GenericEdemaPitting"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_GenericEdemaPitting", pitting, new { @id = "StartOfCare_GenericEdemaPitting" })%>
                    </div>
                    <div class="clear"></div>
                    <%= string.Format("<input id='StartOfCare_GenericPittingEdemaType2' class='radio float_left' name='StartOfCare_GenericPittingEdemaType' value='2' type='checkbox' {0} />", genericPittingEdemaType != null && genericPittingEdemaType.Contains("2") ? "checked='checked'" : "")%>
                    <label for="StartOfCare_GenericPittingEdemaType2">Nonpitting</label>
                    <div class="float_right">
                        <%  var nonPitting = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "N/A", Value = "1" },
                                new SelectListItem { Text = "Mild", Value = "2" },
                                new SelectListItem { Text = "Moderate", Value = "3" },
                                new SelectListItem { Text = "Severe", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericEdemaNonPitting") ? data["GenericEdemaNonPitting"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_GenericEdemaNonPitting", nonPitting, new { @id = "StartOfCare_GenericEdemaNonPitting" })%>
                    </div>
                    <div class="clear"></div>
                    <div id="StartOfCare_GenericPittingEdemaType2More" class="float_right">
                        <%= Html.RadioButton("StartOfCare_GenericEdemaNonPittingPosition", "0", data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericEdemaNonPittingPosition0", @class = "radio" })%>
                        <label for="StartOfCare_GenericEdemaNonPittingPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("StartOfCare_GenericEdemaNonPittingPosition", "1", data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericEdemaNonPittingPosition1", @class = "radio" })%>
                        <label for="StartOfCare_GenericEdemaNonPittingPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("StartOfCare_GenericEdemaNonPittingPosition", "2", data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "2" ? true : false, new { @id = "StartOfCare_GenericEdemaNonPittingPosition2", @class = "radio" })%>
                        <label for="StartOfCare_GenericEdemaNonPittingPosition2" class="inlineradio">Right</label>
                    </div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular6' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='6' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("6") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular6" class="radio">Dizziness:</label>
                </div><div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericDizzinessDesc", data.ContainsKey("GenericDizzinessDesc") ? data["GenericDizzinessDesc"].Answer : "", new { @id = "StartOfCare_GenericDizzinessDesc", @class = "st", @maxlength = "20" }) %>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular7' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='7' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("7") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular7" class="radio">Chest Pain:</label>
                </div><div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericChestPainDesc", data.ContainsKey("GenericChestPainDesc") ? data["GenericChestPainDesc"].Answer : "", new { @id = "StartOfCare_GenericChestPainDesc", @class = "st", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='StartOfCare_GenericCardioVascular8' class='radio float_left' name='StartOfCare_GenericCardioVascular' value='8' type='checkbox' {0} />", genericCardioVascular!=null && genericCardioVascular.Contains("8") ? "checked='checked'" : "" ) %>
                    <label for="StartOfCare_GenericCardioVascular8" class="radio">Neck Vein Distention:</label>
                </div><div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericNeckVeinDistentionDesc", data.ContainsKey("GenericNeckVeinDistentionDesc") ? data["GenericNeckVeinDistentionDesc"].Answer : "", new { @id = "StartOfCare_GenericNeckVeinDistentionDesc", @class = "st", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericPacemakerDate" class="float_left">Pacemaker:</label>
                <div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericPacemakerDate", data.ContainsKey("GenericPacemakerDate") ? data["GenericPacemakerDate"].Answer : "", new { @id = "StartOfCare_GenericPacemakerDate", @class = "st", @maxlength = "10" })%><em>(Insertion date)</em>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericPacemakerDate" class="float_left">AICD:</label>
                <div class="float_right">
                    <%= Html.TextBox("StartOfCare_GenericAICDDate", data.ContainsKey("GenericAICDDate") ? data["GenericAICDDate"].Answer : "", new { @id = "StartOfCare_GenericAICDDate", @class = "st", @maxlength = "10" })%><em>(Insertion date)</em>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericAICDDate" class="strong">Comments:</label>
                <%= Html.TextArea("StartOfCare_GenericCardiovascularComments", data.ContainsKey("GenericCardiovascularComments") ? data["GenericCardiovascularComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericCardiovascularComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] cardiacStatusInterventions = data.ContainsKey("485CardiacStatusInterventions") && data["485CardiacStatusInterventions"].Answer != "" ? data["485CardiacStatusInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485CardiacStatusInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions1' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='1' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions1" class="radio">SN to instruct on daily/weekly weights and recordings.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions2' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='2' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions2" class="radio">SN to perform weekly weights.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions3' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='3' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusInterventions3">SN to instruct on application of</label>
                    <%  var applicationWrap = new SelectList(new[] {
                            new SelectListItem { Text = "Ted Hose", Value = "Ted Hose" },
                            new SelectListItem { Text = "Ace Wraps", Value = "Ace Wraps" }
                        }, "Value", "Text", data.ContainsKey("485ApplicationWrap") && data["485ApplicationWrap"].Answer != "" ? data["485ApplicationWrap"].Answer : "Ted Hose");%>
                    <%= Html.DropDownList("StartOfCare_485ApplicationWrap", applicationWrap)%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions4' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='4' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusInterventions4">SN to instruct patient on daily weight self-monitoring program, and to report weight</label>
                    <%= Html.Hidden("StartOfCare_485WeightSelfMonitorGain") %>
                    <%= Html.RadioButton("StartOfCare_485WeightSelfMonitorGain", "1", data.ContainsKey("485WeightSelfMonitorGain") && data["485WeightSelfMonitorGain"].Answer == "1" ? true : false, new { @id = "StartOfCare_485CardiacStatusInterventionsGain", @class = "radio" })%>
                    <label for="StartOfCare_485CardiacStatusInterventionsGain">gain or</label>
                    <%= Html.RadioButton("StartOfCare_485WeightSelfMonitorGain", "0", data.ContainsKey("485WeightSelfMonitorGain") && data["485WeightSelfMonitorGain"].Answer == "0" ? true : false, new { @id = "StartOfCare_485CardiacStatusInterventionsLoss", @class = "radio" })%>
                    <label for="StartOfCare_485CardiacStatusInterventionsLoss">loss of</label>
                    <%= Html.TextBox("StartOfCare_485WeightSelfMonitorGainDay", data.ContainsKey("485WeightSelfMonitorGainDay") ? data["485WeightSelfMonitorGainDay"].Answer : "", new { @id = "StartOfCare_485WeightSelfMonitorGainDay", @class = "vitals", @maxlength = "3" })%>
                    <label for="StartOfCare_485CardiacStatusInterventions4">lbs/ day,</label>
                    <%= Html.TextBox("StartOfCare_485WeightSelfMonitorGainWeek", data.ContainsKey("485WeightSelfMonitorGainWeek") ? data["485WeightSelfMonitorGainWeek"].Answer : "", new { @id = "StartOfCare_485WeightSelfMonitorGainWeek", @class = "vitals", @maxlength = "3" })%>
                    <label for="StartOfCare_485CardiacStatusInterventions4">lbs/week.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions5' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='5' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions5" class="radio">SN to assess patient&rsquo;s weight log every visit.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions6' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='6' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusInterventions6">SN to instruct the</label>
                    <%  var instructRecognizeCardiacDysfunctionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructRecognizeCardiacDysfunctionPerson") && data["485InstructRecognizeCardiacDysfunctionPerson"].Answer != "" ? data["485InstructRecognizeCardiacDysfunctionPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("StartOfCare_485InstructRecognizeCardiacDysfunctionPerson", instructRecognizeCardiacDysfunctionPerson) %>
                    <label for="StartOfCare_485CardiacStatusInterventions6">on measures to recognize cardiac dysfunction and relieve complications.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions7' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='7' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions7" class="radio">SN to instruct patient on measures to detect and alleviate edema.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions8' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='8' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions8" class="radio">SN to instruct patient when (s)he starts feeling chest pain, tightness, or squeezing in the chest to take nitroglycerin. Patient may take nitroglycerin one time every 5 minutes. If no relief after 3 doses, call 911.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions9' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='9' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusInterventions9" class="radio">SN to instruct the patient the following symptoms could be signs of a heart attack: chest discomfort, discomfort in one or both arms, back, neck, jaw, stomach, shortness of breath, cold sweat, nausea, or dizziness. Instruct patient on signs and symptoms that necessitate calling 911.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusInterventions10' class='radio float_left' name='StartOfCare_485CardiacStatusInterventions' value='10' type='checkbox' {0} />", cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("10") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusInterventions10">No blood pressure or venipuncture in</label>
                    <%= Html.TextBox("StartOfCare_485NoBloodPressureArm", data.ContainsKey("485NoBloodPressureArm") ? data["485NoBloodPressureArm"].Answer : "", new { @id = "StartOfCare_485NoBloodPressureArm", @class = "zip", @maxlength = "10" }) %>
                    <label for="StartOfCare_485CardiacStatusInterventions10">arm.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485CardiacOrderTemplates" class="strong">Additional Orders:</label>
                <%  var cardiacOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485CardiacOrderTemplates") && data["485CardiacOrderTemplates"].Answer != "" ? data["485CardiacOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485CardiacOrderTemplates", cardiacOrderTemplates) %>
                <%= Html.TextArea("StartOfCare_485CardiacInterventionComments", data.ContainsKey("485CardiacInterventionComments") ? data["485CardiacInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485CardiacInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <%string[] cardiacStatusGoals = data.ContainsKey("485CardiacStatusGoals") && data["485CardiacStatusGoals"].Answer != "" ? data["485CardiacStatusGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485CardiacStatusGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusGoals1' class='radio float_left' name='StartOfCare_485CardiacStatusGoals' value='1' type='checkbox' {0} />", cardiacStatusGoals!=null && cardiacStatusGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusGoals1">Patient weight will be maintained between</label>
                    <%= Html.TextBox("StartOfCare_485WeightMaintainedMin", data.ContainsKey("485WeightMaintainedMin") ? data["485WeightMaintainedMin"].Answer : "", new { @id = "StartOfCare_485WeightMaintainedMin", @class = "zip", @maxlength = "10" })%>
                    <label for="StartOfCare_485CardiacStatusGoals1">lbs and</label>
                    <%= Html.TextBox("StartOfCare_485WeightMaintainedMax", data.ContainsKey("485WeightMaintainedMax") ? data["485WeightMaintainedMax"].Answer : "", new { @id = "StartOfCare_485WeightMaintainedMax", @class = "zip", @maxlength = "10" })%>
                    <label for="StartOfCare_485CardiacStatusGoals1">lbs during the episode.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusGoals2' class='radio float_left' name='StartOfCare_485CardiacStatusGoals' value='2' type='checkbox' {0} />", cardiacStatusGoals!=null && cardiacStatusGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="StartOfCare_485CardiacStatusGoals2" class="radio">Patient will remain free from chest pain, or chest pain will be relieved with nitroglycerin, during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusGoals3' class='radio float_left' name='StartOfCare_485CardiacStatusGoals' value='3' type='checkbox' {0} />", cardiacStatusGoals!=null && cardiacStatusGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusGoals3">The</label>
                    <%  var verbalizeCardiacSymptomsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeCardiacSymptomsPerson") && data["485VerbalizeCardiacSymptomsPerson"].Answer != "" ? data["485VerbalizeCardiacSymptomsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485VerbalizeCardiacSymptomsPerson", verbalizeCardiacSymptomsPerson) %>
                    <label for="StartOfCare_485CardiacStatusGoals3">will verbalize understanding of symptoms of cardiac complications and when to call 911 by:</label>
                    <%= Html.TextBox("StartOfCare_485VerbalizeCardiacSymptomsDate", data.ContainsKey("485VerbalizeCardiacSymptomsDate") ? data["485VerbalizeCardiacSymptomsDate"].Answer : "", new { @id = "StartOfCare_485VerbalizeCardiacSymptomsDate", @class = "zip", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='StartOfCare_485CardiacStatusGoals4' class='radio float_left' name='StartOfCare_485CardiacStatusGoals' value='4' type='checkbox' {0} />", cardiacStatusGoals!=null && cardiacStatusGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="StartOfCare_485CardiacStatusGoals4">The</label>
                    <%  var verbalizeEdemaRelieverPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeEdemaRelieverPerson") && data["485VerbalizeEdemaRelieverPerson"].Answer != "" ? data["485VerbalizeEdemaRelieverPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("StartOfCare_485VerbalizeEdemaRelieverPerson", verbalizeEdemaRelieverPerson) %>
                    <label for="StartOfCare_485CardiacStatusGoals4">will verbalize and demonstrate edema-relieving measures by the episode.</label>
                </span>
            </div><div class="row">
                <label for="StartOfCare_485CardiacGoalTemplates" class="strong">Additional Goals:</label>
                <%  var cardiacGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485CardiacGoalTemplates") && data["485CardiacGoalTemplates"].Answer != "" ? data["485CardiacGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485CardiacGoalTemplates", cardiacGoalTemplates) %>
                <%= Html.TextArea("StartOfCare_485CardiacGoalComments", data.ContainsKey("485CardiacGoalComments") ? data["485CardiacGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485CardiacGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular2"), $("#StartOfCare_GenericHeartSoundsType"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular3"), $("#StartOfCare_GenericCardioVascular3More"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular4"), $("#StartOfCare_GenericCardioVascular4More"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular5"), $("#StartOfCare_GenericCardioVascular5More"));
    Oasis.showIfChecked($("#StartOfCare_GenericPittingEdemaType1"), $("#StartOfCare_GenericEdemaPitting"));
    Oasis.showIfChecked($("#StartOfCare_GenericPittingEdemaType2"), $("#StartOfCare_GenericEdemaNonPitting"));
    Oasis.showIfChecked($("#StartOfCare_GenericPittingEdemaType2"), $("#StartOfCare_GenericPittingEdemaType2More"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular6"), $("#StartOfCare_GenericDizzinessDesc"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular7"), $("#StartOfCare_GenericChestPainDesc"));
    Oasis.showIfChecked($("#StartOfCare_GenericCardioVascular8"), $("#StartOfCare_GenericNeckVeinDistentionDesc"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>