﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VisitSummary>>" %>

<% if (Model != null && Model.Count > 0) { %>
<table class="claim">
    <thead class="align_center">
        <tr><th colspan="3">Summary [<a href="javascript:void(0);" onclick="Payroll.LoadDetails();">View Details</a>]</th></tr>
        <tr><th class="align_left">&nbsp;User</th><th class="align_left">&nbsp;Count</th><th class="align_left">&nbsp;Action</th></tr>
    </thead>
    <tbody><%
            int i = 1;
            foreach (var summary in Model) { %>
            <tr class='<%= i % 2 != 0 ? "odd" : "even" %>'>
                <td>&nbsp;<%= summary.UserName %></td>
                <td>&nbsp;<%= summary.VisitCount.ToString() %></td>
                <% if (summary.VisitCount == 0) { %>
                    <td></td>
                <% } else { %>
                    <td>&nbsp;<a href="javascript:void(0);" onclick="Payroll.LoadDetail('<%= summary.UserId.ToString() %>');">Detail</a></td>
                <% } %>
            </tr>
            <% i++; %>
            <% } %>
    </tbody>
</table>
<% } else { %>
<table class="claim">
    <tr><td class="errormessage">Your search yielded 0 results.</td></tr>
</table>
 <% } %>       