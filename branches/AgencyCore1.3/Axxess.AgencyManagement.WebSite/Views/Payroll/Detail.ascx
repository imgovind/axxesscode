﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollDetail>" %>
<%= Model.Name.IsNotNullOrEmpty() ? "<strong>" + Model.Name + "</strong>" : "" %>
[<a href="javascript:void(0);" onclick="$('#payrollSearchResultDetails').hide();$('#payrollSearchResult').show();">Back to Search Results</a>]<br />
<table class="claim">
    <thead class="align_center">
        <tr><th>Patient Name</th><th>Task</th><th>Visit Rate</th><th>Surcharge</th><th>Status</th></tr>
    </thead><tbody><%
if (Model.Visits != null && Model.Visits.Count > 0) {
    int i = 1;
    foreach (var visit in Model.Visits) { %>
        <tr><td colspan="5"><%= visit.VisitDate %></td></tr>
        <tr class="odd">
            <td class="align_center"><%= visit.PatientName %></td>
            <td class="align_center"><%= visit.TaskName %></td>
            <td class="align_center"><%= visit.VisitRate %></td>
            <td class="align_center"><%= visit.Surcharge %></td>
            <td class="align_center"><%= visit.StatusName %></td>
        </tr><%
        i++;
    } 
} else { %>
        <tr><td colspan="5">No Results found</td></tr><%
} %>
    </tbody>
</table>