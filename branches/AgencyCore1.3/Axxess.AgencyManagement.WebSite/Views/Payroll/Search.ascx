﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Payroll Summary | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','payrollsummary');</script>")%>
<% using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
<div class="main wrapper">
    <fieldset>
        <legend>Payroll Summary</legend>
        <table class="form"><tbody>
            <tr>
                <td><span>From</span>&nbsp;<%= Html.Telerik().DatePicker().Format("MM/dd/yyyy").Name("startDate").Value(Model).HtmlAttributes(new { @class = "required date" }) %></td>
                <td><span>To</span>&nbsp;<%= Html.Telerik().DatePicker().Format("MM/dd/yyyy").Name("endDate").Value(DateTime.Today).MaxDate(DateTime.Today).HtmlAttributes(new { @class = "required date" })%></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody></table>
        <div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate</a></li></ul></div>
        <div class="clear">&nbsp;</div>
    </fieldset>
    <div id="payrollSearchResult"></div>
    <div id="payrollSearchResultDetails"></div>
</div>
<% } %>