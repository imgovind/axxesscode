﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "SixtyDaySummaryForm" })) {
        var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('60 Day Summary/Case Conference | ",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "",
        "','sixtyDaySummary');</script>") %>
<%= Html.Hidden("SixtyDaySummary_PatientId", Model.PatientId)%>
<%= Html.Hidden("SixtyDaySummary_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("SixtyDaySummary_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "SixtyDaySummary")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="2">60 Day Summary/Case Conference</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="SixtyDaySummary_PatientName" class="float_left">Patient Name:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "SixtyDaySummary_PatientName", Model.Patient.Id.ToString(), new { @id = "SixtyDaySummary_PatientName", @disabled = "disabled" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_MR" class="float_left">MR#:</label>
                        <div class="float_right"><%= Html.TextBox("SixtyDaySummary_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "SixtyDaySummary_MR", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_VisitDate" class="float_left">Visit Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("SixtyDaySummary_VisitDate").Value(Model.EventDate.FormatWith("MM/dd/yyy")).MaxDate(Model.EndDate).MinDate(Model.StartDate).HtmlAttributes(new { @id = "SixtyDaySummary_VisitdDate", @class = "date required" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_EpsPeriod" class="float_left">Episode/Period:</label>
                        <div class="float_right"><%= Html.TextBox("SixtyDaySummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "SixtyDaySummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_VisitDate" class="float_left">Physician:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "SixtyDaySummary_Physician", Model.PhysicianId.ToString(), new { @id = "SixtyDaySummary_Physician" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_PrimaryDiagnosis" class="float_left">Primary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("SixtyDaySummary_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "SixtyDaySummary_PrimaryDiagnosis" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_PrimaryDiagnosis1" class="float_left">Secondary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("SixtyDaySummary_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "SixtyDaySummary_PrimaryDiagnosis1" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_PrimaryDiagnosis2" class="float_left">Tertiary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("SixtyDaySummary_PrimaryDiagnosis2", data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : string.Empty, new { @id = "SixtyDaySummary_PrimaryDiagnosis2" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_DNR" class="float_left">DNR:</label>
                        <div class="float_right"><%= Html.RadioButton("SixtyDaySummary_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "SixtyDaySummary_DNR1", @class = "radio" })%><label for="SixtyDaySummary_DNR1" class="inlineradio">Yes</label><%=Html.RadioButton("SixtyDaySummary_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "SixtyDaySummary_DNR2", @class = "radio" })%><label for="SixtyDaySummary_DNR2" class="inlineradio">No</label></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_NotificationDate" class="float_left">If Yes:</label>
                        <div class="float_right"><% var patientReceived = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "5 day", Value = "1" }, new SelectListItem { Text = "2 day", Value = "2" }, new SelectListItem { Text = "Other", Value = "3" } }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0");%><%= Html.DropDownList("SixtyDaySummary_NotificationDate", patientReceived, new { @id = "SixtyDaySummary_NotificationDate" })%><%= Html.TextBox("SixtyDaySummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "SixtyDaySummary_NotificationDateOther", @style = "display:none;" })%></div>
                    </div><div class="third">
                        <label for="SixtyDaySummary_HomeboundStatus" class="float_left">Homebound Status:</label>
                        <div class="float_right"><% var homeboundStatus = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "N/A", Value = "N/A" }, new SelectListItem { Text = "Exhibits considerable & taxing effort to leave home", Value = "Exhibits considerable & taxing effort to leave home" }, new SelectListItem { Text = "Requires the assistance of another to get up and moving safely", Value = "Requires the assistance of another to get up and moving safely" }, new SelectListItem { Text = "Severe Dyspnea", Value = "Severe Dyspnea" }, new SelectListItem { Text = "Unable to safely leave home unassisted", Value = "Unable to safely leave home unassisted" }, new SelectListItem { Text = "Unsafe to leave home due to cognitive or psychiatric impairments", Value = "Unsafe to leave home due to cognitive or psychiatric impairments" }, new SelectListItem { Text = "Unable to leave home due to medical restriction(s)", Value = "Unable to leave home due to medical restriction(s)" }, new SelectListItem { Text = "Other", Value = "Other" } }, "Value", "Text", data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : "0");%><%= Html.DropDownList("TransferSummary_HomeboundStatus", homeboundStatus, new { @id = "SixtyDaySummary_HomeboundStatus" })%><%= Html.TextBox("SixtyDaySummary_HomeboundStatusOther", data.ContainsKey("HomeboundStatusOther") ? data["HomeboundStatusOther"].Answer : string.Empty, new { @id = "SixtyDaySummary_HomeboundStatusOther", @style = "display:none;" })%></div>
                    </div>
                </td>
            </tr>
            <tr><th>Functional Limitations</th><th>Patient Condition</th></tr>
            <tr>
                <td>
                    <table class="fixed align_left">
                        <tbody><% string[] functionLimitations = data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null; %><input name="SixtyDaySummary_FunctionLimitations" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations1' name='SixtyDaySummary_FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations1" class="radio">Amputation</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations9' name='SixtyDaySummary_FunctionLimitations' value='9' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations9" class="radio">Legally Blind</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations6' name='SixtyDaySummary_FunctionLimitations' value='6' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations6" class="radio">Endurance</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations3' name='SixtyDaySummary_FunctionLimitations' value='3' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations3" class="radio">Contracture</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations4' name='SixtyDaySummary_FunctionLimitations' value='4' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations4" class="radio">Hearing</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations5' name='SixtyDaySummary_FunctionLimitations' value='5' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations5" class="radio">Paralysis</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations2' name='SixtyDaySummary_FunctionLimitations' value='2' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitationsA' name='SixtyDaySummary_FunctionLimitations' value='A' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitationsA" class="radio">Dyspnea</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations7' name='SixtyDaySummary_FunctionLimitations' value='7' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations7" class="radio">Ambulation</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_FunctionLimitations8' name='SixtyDaySummary_FunctionLimitations' value='8' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitations8" class="radio">Speech</label></td>
                                <td colspan="2"><%= string.Format("<input id='SixtyDaySummary_FunctionLimitationsB' name='SixtyDaySummary_FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_FunctionLimitationsB" class="radio">Other (Specify) </label><%= Html.TextBox("SixtyDaySummary_FunctionLimitationsOther", data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : string.Empty, new { @id = "SixtyDaySummary_FunctionLimitationsOther" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <table class="fixed align_left">
                        <tbody>
                            <tr><% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="SixtyDaySummary_PatientCondition" value=" " type="hidden" />
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionStable' name='SixtyDaySummary_PatientCondition' value='1' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_PatientConditionStable" class="radio">Stable</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionImproved' name='SixtyDaySummary_PatientCondition' value='2' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionImproved" class="radio">Improved</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionUnchanged' name='SixtyDaySummary_PatientCondition' value='3' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_PatientConditionUnstable' name='SixtyDaySummary_PatientCondition' value='4' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionUnstable" class="radio">Unstable</label></td>
                                <td colspan="2"><%= string.Format("<input id='SixtyDaySummary_PatientConditionDeclined' name='SixtyDaySummary_PatientCondition' value='5' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_PatientConditionDeclined" class="radio">Declined</label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th>Service(s) Provided</th><th>Vital Sign Ranges</th></tr>
            <tr>
                <td>
                    <table class="fixed align_left">
                        <tbody><% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="SixtyDaySummary_ServiceProvided" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedSN' name='SixtyDaySummary_ServiceProvided' value='1' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "" )%><label for="SixtyDaySummary_ServiceProvidedSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedPT' name='SixtyDaySummary_ServiceProvided' value='2' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedPT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOT' name='SixtyDaySummary_ServiceProvided' value='3' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedOT" class="radio">OT</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedST' name='SixtyDaySummary_ServiceProvided' value='4' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedMSW' name='SixtyDaySummary_ServiceProvided' value='5' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedHHA' name='SixtyDaySummary_ServiceProvided' value='6' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedHHA" class="radio">HHA</label></td>
                            </tr><tr>
                                <td colspan="3"><%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOther' name='SixtyDaySummary_ServiceProvided' value='7' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%><label for="SixtyDaySummary_ServiceProvidedOther" class="radio">Other</label><%= Html.TextBox("SixtyDaySummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = "SixtyDaySummary_ServiceProvidedOtherValue" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <table class="fixed">
                        <tbody>
                            <tr><th></th><th>BP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BG</th></tr>
                            <tr>
                                <th>Lowest</th>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMin", @class = "fill" })%></td>
                            </tr><tr>
                                <th>Highest</th>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("SixtyDaySummary_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMax", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th>Summary of Care Provided</th><th>Patient&rsquo;s Current Condition</th></tr>
            <tr>
                <td><%= Html.TextArea("SixtyDaySummary_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_SummaryOfCareProvided" })%></td>
                <td><%= Html.TextArea("SixtyDaySummary_PatientCurrentCondition", data.ContainsKey("PatientCurrentCondition") ? data["PatientCurrentCondition"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_PatientCurrentCondition" })%></td>
            </tr>
            <tr><th>Goals</th><th>Recomended Services</th></tr>
            <tr>
                <td><%= Html.TextArea("SixtyDaySummary_Goals", data.ContainsKey("Goals") ? data["Goals"].Answer : string.Empty, new { @class = "fill", @id = "SixtyDaySummary_Goals" })%></td>
                <td>
                    <table class="fixed align_left">
                        <tbody><% string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %><input name="SixtyDaySummary_RecommendedService" value="" type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceSN' name='SixtyDaySummary_RecommendedService' value='1' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("1") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServicePT' name='SixtyDaySummary_RecommendedService' value='2' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("2") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServicePT" class="radio">PT</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOT' name='SixtyDaySummary_RecommendedService' value='3' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("3") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceOT" class="radio">OT</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceST' name='SixtyDaySummary_RecommendedService' value='4' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("4") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceST" class="radio">ST</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceMSW' name='SixtyDaySummary_RecommendedService' value='5' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("5") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceHHA' name='SixtyDaySummary_RecommendedService' value='6' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("6") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceHHA" class="radio">HHA</label></td>
                            </tr><tr>
                                <td colspan="3"><%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOther' name='SixtyDaySummary_RecommendedService' value='7' class='radio float_left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("7") ? "checked='checked'" : "")%><label for="SixtyDaySummary_RecommendedServiceOther" class="radio">Other</label><%= Html.TextBox("SixtyDaySummary_RecommendedServiceOtherValue", data.ContainsKey("RecommendedServiceOtherValue") ? data["RecommendedServiceOtherValue"].Answer:string.Empty, new { @id = "SixtyDaySummary_RecommendedServiceOtherValue" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="SixtyDaySummary_ClinicianSignature" class="float_left">Clinician:</label>
                        <div class="float_right"><%= Html.Password("SixtyDaySummary_Clinician", "", new { @id = "SixtyDaySummary_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="SixtyDaySummary_ClinicianSignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("SixtyDaySummary_SignatureDate").Value(data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Now).MinDate(Model.StartDate).HtmlAttributes(new { @id = "SixtyDaySummary_SignatureDate", @class = "date" })%></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="SixtyDaySummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove();$('#SixtyDaySummary_Button').val($(this).html()).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryAdd(); $('#SixtyDaySummary_Button').val($(this).html()).closest('form').submit();">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="SixtyDaySummaryRemove(); UserInterface.CloseWindow('sixtyDaySummary');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#SixtyDaySummary_MR").attr('readonly', true);
    $("#SixtyDaySummary_EpsPeriod").attr('readonly', true);
    function SixtyDaySummaryAdd() {
        $("#SixtyDaySummary_Clinician").removeClass('required').addClass('required');
        $("#SixtyDaySummary_SignatureDate").removeClass('required').addClass('required');
    }
    function SixtyDaySummaryRemove() {
        $("#SixtyDaySummary_Clinician").removeClass('required');
        $("#SixtyDaySummary_SignatureDate").removeClass('required');
    }
</script>
