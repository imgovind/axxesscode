﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Schedule Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','schedulecenter');</script>")%>
<% string[] stabs = new string[] { "Nursing", "HHA" }; %>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top">
            <div class="buttons heading"><ul><li><a href="javascript:void(0);" onclick="javascript:acore.open('newpatient');" title="Add New Patient">Add New Patient</a></li></ul></div>
            <div class="row">Select Patient</div>
            <div class="row"><label>View:</label><div><select name="list" class="scheduleStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><select name="list" class="schedulePaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs </option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Schedule_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("Patients", Model.Patients); %></div>
    </div>
    <div id="scheduleMainResult" class="layout_main">
    <% if (Model.Patients != null && Model.Patients.Count > 0) { %>
        <% if (Model.Episode != null) { %>
            <div class="top">
                <div id="scheduleTop"><% Html.RenderPartial("Calendar", Model.Episode); %></div>
                <div id="schedule_collapsed"><a href="javascript:void(0);" onclick="Schedule.ShowScheduler()" class="show_scheduler">Show Scheduler</a></div>
            <% if (Current.HasRight(Permissions.EditEpisode)) { %>
                <% Html.Telerik().TabStrip().Name("ScheduleTabStrip").ClientEvents(events => events.OnSelect("Schedule.OnSelect")).Items(tabstrip =>
                   { %>
                <% for (int sindex = 0; sindex < stabs.Length; sindex++)
                   { %>
                    <% string stitle = stabs[sindex]; %>
                    <% tabstrip.Add().Text(stitle).HtmlAttributes(new { id = stitle + "_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() =>
                       { %>
                        <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post))
                           { %>
                <%= Html.Hidden("patientId")%>
                <div class="tabcontents">
                    <table id="<%= stitle %>ScheduleTable" data="<%= stitle %>" class="scheduleTables purgable">
                        <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th><th>Action</th></tr></thead>
                        <tbody></tbody>
                    </table>
                    <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" class="scheduleValue" />
                    <div class="buttons"><ul>
                        <li><%= String.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ScheduleInputFix($(this),'Patient','#{0}ScheduleTable');\">Save</a>", stitle)%></li>
                        <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                    </ul></div>
                </div><% }
                       });
                   }
                   tabstrip.Add().Text("Orders/Care Plans").HtmlAttributes(new { id = "Orders_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() =>
                   {
                       using (Html.BeginForm("Add", "Schedule", FormMethod.Post))
                       { %>
                <%= Html.Hidden("patientId")%>
                <div class="tabcontents">
                    <table id="OrdersScheduleTable" data="Orders" class="scheduleTables purgable">
                        <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th><th>Action</th></tr></thead>
                        <tbody></tbody>
                    </table>
                    <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" />
                    <div class="buttons float_right"><ul>
                        <li><a href="javascript:void(0);" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OrdersScheduleTable');">Save</a></li>
                        <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                    </ul></div>
                </div><%
            }
                   });
                   tabstrip.Add().Text("Daily/Outlier").ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() =>
                   {
                       using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post, new { @id = " ", }))
                       { %>
                <%= Html.Hidden("patientId", "", new { @id = "" })%>
                <div class="tabcontents">
                    <table id="multipleScheduleTable" data="Multiple" class="scheduleTables">
                        <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th></tr></thead>
                        <tbody><tr>
                            <% var htmlAttributes = new Dictionary<string, string>();
                               htmlAttributes.Add("id", "multipleDisciplineTask");
                               htmlAttributes.Add("class", "MultipleDisciplineTask requireddropdown");
                            %>
                            <td><%= Html.MultipleDisciplineTasks("DisciplineTask", "", htmlAttributes)%></td>
                            <td><%= Html.Users("userId", "", new { @class = "suppliesCode Users requireddropdown" })%></td>
                            <td class="daterange"><%= Html.Telerik().DatePicker().Name("StartDate").Value(Model.Episode.StartDate).MinDate(Model.Episode.StartDate).MaxDate(Model.Episode.EndDate).HtmlAttributes(new { @id = "outlierStartDate" }) %><span>to</span><%= Html.Telerik().DatePicker().Name("EndDate").Value(Model.Episode.StartDate).MinDate(Model.Episode.StartDate).MaxDate(Model.Episode.EndDate).HtmlAttributes(new { @id = "outlierEndDate" })%></td>
                        </tr></tbody>
                    </table>
                    <input type="hidden" name="episodeId" value="" class="scheduleValue" /><input type="hidden" name="Discipline" value="" /><input type="hidden" name="IsBillable" value="" />
                    <div class="buttons float_right"><ul>
                        <li><a href="javascript:void(0);" onclick="Schedule.FormSubmitMultiple($(this));">Save</a></li>
                        <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                    </ul></div>
                </div><%
            }
                   });
                   }).SelectedIndex(0).Render();
               } %>
            </div>
            <div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Activities", new ScheduleActivityArgument { EpisodeId = Model.Episode.Id, PatientId = Model.Episode.PatientId, Discpline = "all" }); %></div>
        <%} else{ %>
            <script type="text/javascript">
                $('#scheduleMainResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Episodes found for this patient.%3C/h1%3E" +
                "%3Cdiv class=%22buttons heading%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewEpisodeModal(Schedule.GetId());%22" +
                " title=%22Add New Episode%22%3EAdd New Episode%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            </script>
        <% } %>
    <%} else {%>
        <div class="ajaxerror">No Patients Found</div>
    <% } %>
    </div>
</div>