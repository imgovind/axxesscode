﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<fieldset>
    <div class="wide_column">
        <div class="row"><label for="Missed_Visit_Info_Name" class="float_left">Patient Name:</label><div class="float_right"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_TypeofVisit" class="float_left">Type of Visit:</label><div class="float_right"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_DateofVisit" class="float_left">Date of Visit:</label><div class="float_right"><%= Model.Date.ToShortDateString() %></div></div>
        <div class="row"><label for="Missed_Visit_Info_UserName" class="float_left">Assigned To:</label><div class="float_right"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_OrderGenerated" class="float_left">Order Generated:</label><div class="float_right"><%= (Model.IsOrderGenerated) ? "Yes" : "No"%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianOfficeNotified" class="float_left">Physician Office Notified:</label><div class="float_right"><%= (Model.IsPhysicianOfficeNotified) ? "Yes" : "No" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_Reason" class="float_left">Reason:</label><div class="float_right"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_Comments" class="float_left">Comments:</label><div class="float_right"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div></div>
    </div>
</fieldset>