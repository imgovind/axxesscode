﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<% if(Model!=null){ %>
<%  var scheduleEvents = Model.Schedule.IsNotNullOrEmpty() ? Model.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.ToDateTime() >= Model.StartDate && s.EventDate.ToDateTime() <= Model.EndDate && s.DisciplineTask>0).OrderBy(o => o.EventDate.ToZeroFilled().ToDateTime()).ToList() : new List<ScheduleEvent>();
    DateTime[] startdate = new DateTime[3];
    DateTime[] enddate = new DateTime[3];
    DateTime[] currentdate = new DateTime[3];
    startdate[0] = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year);
    enddate[0] = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year);
    currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek);
    startdate[1] = enddate[0].AddDays(1);
    enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year);
    currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek);
    startdate[2] = enddate[1].AddDays(1);
    enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year);
    currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
<%=Html.Hidden("SchedulePatientID", Model.PatientId, new { @id="SchedulePatientID"})%>
<%=Html.Hidden("ScheduleEpisodeID", Model.Id, new { @id = "ScheduleEpisodeID" })%>
<div class="trical">
    <% if (Model.HasPrevious) { %><span class="abs_left"><a onclick="Schedule.NavigateEpisode('<%= Model.PreviousEpisode.Id%>','<%=Model.PatientId %>');"><span class="largefont">&laquo;</span> Previous Episode</a></span><% } %>
    <% if (Current.HasRight(Permissions.EditEpisode)) { %><span class="edit_eps"><a href="javascript:void(0);" onclick="UserInterface.ShowEditEpisodeModal('<%= Model.Id %>','<%= Model.PatientId %>');"><span class="largefont">&nbsp;</span>Edit Episode Information</a></span><% } %>
    <% if (Model.HasNext) { %><span class="abs_right"><a onclick="Schedule.NavigateEpisode('<%= Model.NextEpisode.Id%>','<%=Model.PatientId %>');">Next Episode <span class="largefont">&raquo;</span></a></span><% } %>
    <div class="clear"></div>
    <span class="strong"><%= Model.DisplayName %></span> | <%= Model.StartDate.ToShortDateString() %> &ndash; <%= Model.EndDate.ToShortDateString() %>
    <div class="clear"></div>
    <% for (int c = 0; c < 3; c++) { %>
        <div class="cal"><table><thead><tr><td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c]) %></td></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><%
        for (int i = 0; i <= DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); i++) { %>
        <tr><%
            string tooltip = "";
            int addedDate = (i) * 7;
            for (int j = 0; j <= 6; j++) {
                var specificDate = currentdate[c].AddDays(j + addedDate);
                if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate > Model.EndDate.AddDays(1)) { %>
                    <td class="inactive ui-droppable"></td><%
                } else {
                    var events = scheduleEvents.FindAll(e => e.EventDate.ToZeroFilled() == specificDate.ToShortDateString().ToZeroFilled());
                    var count = events.Count;
                    if (count > 1)
                    {
                        var allevents = "<br />";
                        events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.DisciplineTaskName, e.UserName); });
                        %>
                        <td class="multi ui-droppable" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                        tooltip = specificDate.ToShortDateString() + allevents;
                    } else if (count == 1) {
                        var evnt = events.First();
                        var missed = (evnt.IsMissedVisit) ? "missed" : "";
                        var status = evnt.Status != null ? int.Parse(evnt.Status) : 1000; %>
                        <td class="status<%= status %> scheduled <%= missed %> ui-droppable" date="<%= specificDate %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                        tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.DisciplineTaskName, evnt.UserName);
                    } else { %>
                        <td class="ui-droppable" date='<%= specificDate %>' onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');"><%
                        tooltip = "";
                    } %>
                <%= string.Format("<div class=\"datelabel\" onclick=\"Schedule.Add('{0}');\"{1}><a>{2}</a></div></td>",specificDate.ToShortDateString(),tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "",specificDate.Day) %><%
                }
            } %></tr><%
        } %></tbody></table></div><%
    } %>
</div><div class="clear"></div>
<div>
<div style="width:180px; margin-bottom:-60px;" class="abs buttons heading"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewEpisodeModal('<%= Model.PatientId %>');" title="Add New Episode">Add New Episode</a></li></ul></div>
<fieldset class="callegend">
    <ul>
        <li><div class="scheduled">&nbsp;</div>Scheduled</li>
        <li><div class="completed">&nbsp;</div>Completed</li>
        <li><div class="missed">&nbsp;</div>Missed</li>
        <li><div class="multi">&nbsp;</div>Multiple</li>
    </ul>
</fieldset>
</div>
<script type="text/javascript">
    $(".datelabel").each(function() {
        if ($(this).attr("tooltip") != undefined) {
            $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: "calday",
                bodyHandler: function() { return $(this).attr("tooltip"); }
            });
        }
    });
    $("#scheduleBottomPanel").show();
</script>
<%} else{ %>
<script type="text/javascript">    
    $('#scheduleMainResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Episodes found for this patient.%3C/h1%3E" +
    "%3Cdiv class=%22buttons heading%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewEpisodeModal(Schedule.GetId());%22" +
    " title=%22Add New Episode%22%3EAdd New Episode%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
</script>
<%} %>