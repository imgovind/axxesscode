﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeViewData>" %>
<% using (Html.BeginForm("AddEpisode", "Schedule", FormMethod.Post, new { @id = "newEpisodeForm" })){%>
<%= Html.Hidden("PatientId", Model.PatientId) %>
<div class="wrapper main">
<span class="bigtext align_center">New Episode : <%=Model.DisplayName %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <span class="bold"><%= Model.DisplayName %></span>
            </div>
        </div>
          <div class="column">
            <div class="row">
               <label for="Edit_Episode_TargetDate" class="float_left">Start Of Care Date:</label> <div class="float-right bold"><%= Model.StartOfCareDateFormated %></div>
            </div>
        </div>
    </fieldset><label class="bold" >Tip:</label> <em> Last Episode end date is :- <%=Model.EndDateFormatted %></em>
    <fieldset>
        <legend>Details</legend>
        <table class="form"><tbody>
            <tr>
                <td><label for="New_Episode_TargetDate" class="float_left">Episode Start Date:</label><br />
                <%= Html.Telerik().DatePicker().Name("StartDate").Value(Model.End.AddDays(1)).ClientEvents(events => events.OnChange("Schedule.newEpisodeStartDateOnChange")).HtmlAttributes(new { @id = "New_Episode_StartDate", @class = "text required date" })%>
                </td>
                <td><label for="New_Episode_EndDate" class="float_left">Episode End Date:</label><br />
                <%= Html.Telerik().DatePicker().Name("EndDate").Value(Model.End.AddDays(60)).HtmlAttributes(new { @id = "New_Episode_EndDate", @class = "text required date" }) %>
                </td>
                <td>
                <label for="New_Episode_CaseManager" class="float_left">Case Manager:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.EpisodeDetail.CaseManager, new { @id = "New_Episode_CaseManager", @class = "Users required valid" })%>
               
                </td>
            </tr>
                   <tr>
                <td><label for="New_Episode_PrimaryPhysician" class="float_left">Primary Physician:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Physicians, "Detail.PrimaryPhysician", Model.EpisodeDetail.PrimaryPhysician, new { @id = "New_Episode_PrimaryPhysician", @class = "Physicians" })%>
                </td>
                <td><label for="New_Episode_PrimaryInsurance" class="float_left">Primary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.PrimaryInsurance", Model.EpisodeDetail.PrimaryInsurance, new { @id = "New_Episode_PrimaryInsurance", @class = "Insurance" })%>
                </td>
                <td><label for="New_Episode_SecondaryInsurance" class="float_left">Secondary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.SecondaryInsurance", Model.EpisodeDetail.SecondaryInsurance, new { @id = "New_Episode_SecondaryInsurance", @class = "Insurance" })%>
                </td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row">
                <textarea id="New_Episode_Comments" name="Detail.Comments" rows="10"  cols=""><%= Model.EpisodeDetail.Comments%></textarea>
            </div>
        </div>
    </fieldset>
<div class="clear"></div>
<div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="U.closeDialog();">Cancel</a></li>
        </ul>
  </div>
  </div>
  <%} %>