﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<% using (Html.BeginForm("AddMissedVisit", "Schedule", FormMethod.Post, new { @id = "newMissedVisitForm" }))   { %>
<%= Html.Hidden("Id", Model.EventId, new { @id = "New_MissedVisit_EventId" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "New_MissedVisit_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "New_MissedVisit_PatientId" })%>
<div class="wrapper main">
    <div><span id="New_MissedVisit_Patient" class="bigtext"><%=Model.PatientName %></span></div>
    <fieldset>
        <legend>Missed Visit Details</legend>
        <div class="column">
            <div class="row"><label for="New_MissedVisit_TaskName" class="float_left">Type of Visit:</label><div class="float_right"><span id="New_MissedVisit_TaskName"><%=Model.DisciplineTaskName %></span></div></div>
            <div class="row"><label for="New_MissedVisit_Date" class="float_left">Date of Visit:</label><div class="float_right"><span id="New_MissedVisit_Date"><%=Model.EventDate %></span></div></div>
            <div class="row"><label for="New_MissedVisit_Order_Yes" class="float_left">Order Generated:</label><div class="float_right"><input id="New_MissedVisit_Order_Yes" type="radio" class="radio" name="IsOrderGenerated" value="true" />Yes<input id="New_MissedVisit_Order_No" type="radio" class="radio" name="IsOrderGenerated" value="false" />No</div></div>
            <div class="row"><label for="New_MissedVisit_PhysicianNotified_Yes" class="float_left">Physician Office Notified:</label><div class="float_right"><input id="New_MissedVisit_PhysicianNotified_Yes" type="radio" checked="checked" class="radio" name="IsPhysicianOfficeNotified" value="true" />Yes<input id="New_MissedVisit_PhysicianNotified_No" type="radio" class="radio" name="IsPhysicianOfficeNotified" value="false" />No</div></div>
            <div class="row"><label for="New_MissedVisit_Reason" class="float_left">Reason:</label><div class="float_right">
                <select id="New_MissedVisit_Reason" name="Reason" class="selectDropDown">
                    <option value="">** Select **</option>
                    <option value="Cancellation of Care">Cancellation of Care</option>
                    <option value="Doctor - Clinic Appointment">Doctor - Clinic Appointment</option>
                    <option value="Family - Caregiver Able to Assist Patient">Family - Caregiver Able to Assist Patient</option>
                    <option value="No Answer to Locked Door">No Answer to Locked Door</option>
                    <option value="No Answer to Phone Call">No Answer to Phone Call</option>
                    <option value="Patient - Family Uncooperative">Patient - Family Uncooperative</option>
                    <option value="Patient Hospitalized">Patient Hospitalized</option>
                    <option value="Patient Unable to Answer Door">Patient Unable to Answer Door</option>
                    <option value="Therapy on Hold">Therapy on Hold</option>
                    <option value="Other">Other (Specify)</option>
                </select>
            </div></div>
        </div>
        <table class="form"><tbody>           
            <tr class="linesep vert">
               <td ><label for="Comment">Comments:</label>
                 <div ><%= Html.TextArea("Comments", "", new { @id = "New_MissedVisit_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Submit</a></li>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('newmissedvisit');">Close</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>

