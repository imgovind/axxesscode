﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Discharge Summary<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page largerfont"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "" %>
                        <%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%> <%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%><br />
                        <%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>
                    </td><th class="h1">Discharge Summary</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="big dual">Patient Name: <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></span><br />
                        <span class="quadcol">
                            <span><strong>Completion Date:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("CompletedDate") && data["CompletedDate"].Answer.IsNotNullOrEmpty() ? data["CompletedDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : ""%></span>
                            <span><strong>Discharge Date:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() ? data["DischargeDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : ""%></span>
                            <span><strong>Episode Period:</strong></span>
                            <span class="trip"><%= data != null ? string.Format(" {0} &ndash; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %></span>
                            <span><strong>MR#</strong></span>
                            <span class="trip"><%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %></span>
                            <span><strong>Physician:</strong></span>
                            <span class="trip"><%= Model.PhysicianDisplayName.ToTitleCase()  %></span>
                            <span><strong>Patient Notified of Discharge:</strong></span>
                            <span class="trip"><%= data != null ? (data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? "Yes" + (data.ContainsKey("NotificationDate") ? " " + data["NotificationDate"].Answer : "") + (data.ContainsKey("NotificationDateOther") ? " " + data["NotificationDateOther"].Answer : "") : "No") : ""%></span>
                            <span><strong>Reason for D/C:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : ""%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Patient Condition and Outcomes</h3>
        <div class="quadcol"><%string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("1") ? "&#x2713;" : ""%></span>Stable</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("2") ? "&#x2713;" : ""%></span>Improved</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("3") ? "&#x2713;" : ""%></span>Unchanged</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("4") ? "&#x2713;" : ""%></span>Unstable</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("5") ? "&#x2713;" : ""%></span>Declined</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("6") ? "&#x2713;" : ""%></span>Goals Met</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("7") ? "&#x2713;" : ""%></span>Goals Not Met</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("8") ? "&#x2713;" : ""%></span>Goals Partially Met</span>
        </div>
        <h3>Service(s) Provided</h3>
        <div class="quadcol"><%string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("1") ? "&#x2713;" : ""%></span>SN</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("2") ? "&#x2713;" : ""%></span>PT</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("3") ? "&#x2713;" : ""%></span>OT</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("4") ? "&#x2713;" : ""%></span>ST</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("5") ? "&#x2713;" : ""%></span>MSW</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("6") ? "&#x2713;" : ""%></span>HHA</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("7") ? "&#x2713;" : ""%></span>Other</span>
            <span class="trip"><%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : ""%></span>
        </div>
        <h3>Care Summary: (Care Given, Progress, Regress Including Therapies)</h3>
        <div><span class="deca"><%= data != null && data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : ""%></span></div>
        <h3>Condition of discharge (Include VS, BS, Functional and Overall Status)</h3>
        <div>
            <span class="deca"><%= data != null && data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer : ""%></span>
        </div><div>
            <span><strong>Discharge Disposition: Where is the patient after discharge from your agency?</strong></span><span></span>
            <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? "&#x2713;" : ""%></span>1 - Patient remained in the community (without formal assistive services)</span>
            <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? "&#x2713;" : ""%></span>3 - Patient transferred to a non-institutional hospice)</span>
            <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? "&#x2713;" : ""%></span>2 - Patient remained in the community (with formal assistive services)</span>
            <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? "&#x2713;" : ""%></span>4 - Unknown because patient moved to a geographic location not served by this agency</span>
            <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? "&#x2713;" : ""%></span>UK - Other unknown </span>
        </div><div class="bicol">
            <span><%string[] dischargeInstructionsGivenTo = data != null && data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null; %>
                <strong>Discharge Instructions given to:</strong><br />
                <span class="checklabel"><span class="checkbox"><%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "&#x2713;" : ""%></span>Patient</span>
                <span class="checklabel"><span class="checkbox"><%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "&#x2713;" : ""%></span>Caregiver</span>
                <span class="checklabel"><span class="checkbox"><%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "&#x2713;" : ""%></span>N/A</span>
                <span class="checklabel"><span class="checkbox"><%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "&#x2713;" : ""%></span>Other <span class="auto trip"><%= data != null && data.ContainsKey("DischargeInstructionsGivenToOther") ? ": " + data["DischargeInstructionsGivenToOther"].Answer : ""%></span></span>
            </span><span>
                <strong>Verbalized Understanding:</strong><br />
                <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? "&#x2713;" : ""%></span>Yes</span>
                <span class="checklabel"><span class="checkbox"><%= data != null && data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? "&#x2713;" : ""%></span>No</span>
            </span>
        </div><div class="bicol"><%string[] differentTasks = data != null && data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= differentTasks != null && differentTasks.Contains("1") ? "&#x2713;" : ""%></span>All services notified and discontinued</span>
            <span class="checklabel"><span class="checkbox"><%= differentTasks != null && differentTasks.Contains("2") ? "&#x2713;" : ""%></span>Order and summary completed</span>
            <span class="checklabel"><span class="checkbox"><%= differentTasks != null && differentTasks.Contains("3") ? "&#x2713;" : ""%></span>Information provided to patient for continuing needs</span>
            <span class="checklabel"><span class="checkbox"><%= differentTasks != null && differentTasks.Contains("4") ? "&#x2713;" : ""%></span>Physician notified</span>
        </div><div class="bicol">
            <span class="quad"><strong>Clinician Signature:</strong><%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : "" %></span>
            <span class="quad"><strong>Date:</strong><%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() ? Model.SignatureDate : "" %></span>
        </div>
    </div>
</body>
</html>

