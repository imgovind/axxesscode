﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<% using (Html.BeginForm("UpdateEpisode", "Schedule", FormMethod.Post, new { @id = "editEpisodeForm" })){ %>
   <%= Html.Hidden("Id", Model.Id) %>
   <%= Html.Hidden("PatientId", Model.PatientId) %>
<div class="wrapper main">
<span class="bigtext align_center">Edit Episode : <%=Model.DisplayName %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <span class="bigtext"><%= Model.DisplayName %></span>
            </div>
        </div>
         <div class="column">
            <div class="row">
               <label for="Edit_Episode_TargetDate" class="float_left">Start Of Care Date: </label><div class="float-right bold"><%= Model.StartOfCareDateFormatted %></div>
            </div>
        </div>
    </fieldset>
    <label class="bold">Tip: </label> <% if (Model.PreviousEpisode != null) {%>
        <em>Previous episode end date is : <%=Model.PreviousEpisode.EndDate.ToShortDateString()%>  </em> 
        <%} if (Model.NextEpisode != null) {%>
        <em>Next episode start date is: <%=Model.NextEpisode.StartDate.ToShortDateString()%></em>
        <%} %>
    <fieldset>
        <legend>Details</legend>
        <table class="form"><tbody>
            <tr>
                <td><label for="Edit_Episode_TargetDate" class="float_left">Episode Start Date:</label><br />
                <%= Html.Telerik().DatePicker().Name("StartDate").Value(Model.StartDate).ClientEvents(events => events.OnChange("Schedule.newEpisodeStartDateOnChange")).HtmlAttributes(new { @id = "Edit_Episode_StartDate", @class = "text required date" })%>
                </td>
                <td><label for="Edit_Episode_VisitDate" class="float_left">Episode End Date:</label><br />
                <%= Html.Telerik().DatePicker().Name("EndDate").Value(Model.EndDate).HtmlAttributes(new { @id = "Edit_Episode_EndDate", @class = "text required date" }) %>
                </td>
                <td><label for="Edit_Episode_IsActive" class="float_left">Inactivate Episode:</label><br />
                <%= Html.CheckBox("IsActive",false, new { @id = "Edit_Episode_IsActive", @class = "radio" })%>
                </td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_CaseManager" class="float_left">Case Manager:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.Detail.CaseManager, new { @id = "Edit_Episode_CaseManager", @class = "Users required valid" })%>
                </td>
                <td> <label for="Edit_Episode_PrimaryInsurance" class="float_left">Primary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.PrimaryInsurance", Model.Detail.PrimaryInsurance, new { @id = "Edit_Episode_PrimaryInsurance", @class = "Insurance" })%>
             </td>
                 <td> <label for="Edit_Episode_SecondaryInsurance" class="float_left">Secondary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.SecondaryInsurance", Model.Detail.SecondaryInsurance, new { @id = "Edit_Episode_SecondaryInsurance", @class = "Insurance" })%>
               </td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_PrimaryPhysician" class="float_left">Primary Physician:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Physicians, "Detail.PrimaryPhysician", Model.Detail.PrimaryPhysician, new { @id = "Edit_Episode_PrimaryPhysician", @class = "Physicians" })%>
                </td>
                <td>    </td>
                <td>  </td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row">
                <textarea id="Edit_Episode_Comments" name="Detail.Comments" cols="" rows="10"><%= Model.Detail.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();">Cancel</a></li>
    </ul></div>
</div>
<% } %>
