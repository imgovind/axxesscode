﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>LVN Supervisory Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page largerfont"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "" %>
                        <%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%> <%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%><br />
                        <%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>
                    </td><th class="h1">LVN Supervisory Visit</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="big dual">Patient Name: <%= Model != null && Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : ""%></span><br />
                        <span class="quadcol">
                            <span><strong>Visit Date:</strong></span>
                            <span class="trip"><%= Model != null && data.ContainsKey("VisitDate") && data["VisitDate"].Answer.IsNotNullOrEmpty() ? data["VisitDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : ""%></span>
                            <span><strong>Associated Milage:</strong></span>
                            <span class="trip"><%= Model != null && data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : ""%></span>
                            <span><strong>Time In:</strong></span>
                            <span class="trip"><%= Model != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : ""%></span>
                            <span><strong>Time Out:</strong></span>
                            <span class="trip"><%= Model != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : ""%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div><div>
            <ol>
                <li>
                    <label>Arrives for assigned visits as scheduled</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? "No" : "" %></label></span>
                </li><li>
                    <label>Follows client&rsquo;s ordered plan of care</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? "No" : ""%></label></span>
                </li><li>
                    <label>Demonstrates positive and helpful attitude towards the client and others</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? "No" : ""%></label></span>
                </li><li>
                    <label>Informs Nurse Supervisor/Case Manager of client needs and changes in condition</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? "No" : ""%></label></span>
                </li><li>
                    <label>Implements Universal Precautions per agency policy</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? "No" : ""%></label></span>
                </li><li>
                    <label>Any changes made to client plan of care at this time</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? "No" : ""%></label></span>
                </li><li>
                    <label>Patient/CG satisfied with care and services provided by LVN/LPN</label>
                    <span class="dual"><label class="float_right"><%= Model != null && data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? "Yes" : ""%><%= data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? "No" : ""%></label></span>
                </li><li><label>Additional Comments/Findings</label></li>
            </ol>
            <span class="deca"><%= Model != null && data.ContainsKey("AdditionalComments") && data["AdditionalComments"].Answer.IsNotNullOrEmpty() ? data["AdditionalComments"].Answer : ""%></span>
        </div><div class="bicol">
            <span class="quad"><strong>Signature:</strong><%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : "" %></span>
            <span class="quad"><strong>Date:</strong><%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() ? Model.SignatureDate : ""%></span>
        </div>
    </div>
</body>
</html>
