﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>MR#</th>
				<th>Due Date</th>
			</tr>
        </thead>
	    <tbody id="recertUpcomingWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="upcomingRecertsMore" class="widget-more"><a onclick="UserInterface.ShowUpcomingRecerts();" href="javascript:void(0);">More &raquo;</a></div>

<% if (Current.HasRight(Permissions.ViewScheduledTasks)) { %>
<script type="text/javascript">
    U.postUrl("/Agency/RecertsUpcomingWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
        if (data != undefined && data.length > 0) {
            for (var i = 0; i < data.length && i < 5; i++) $('#recertUpcomingWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDate + "</td></tr>");
        } else {
            $('#recertUpcomingWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Upcoming Recertifications found.</h1></td></tr>");
            $('#upcomingRecertsMore').hide();
        }
    });
</script>
<% } %>