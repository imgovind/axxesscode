﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
                <th>Birth Day</th>
                <th>Age</th>
                <th>Name</th>
                <th>Home Phone</th>
            </tr>
        </thead>
        <tbody id="birthdayWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="birthdayWidgetMore" class="widget-more"><a onclick="UserInterface.ShowPatientBirthdays();" href="javascript:void(0);">More &raquo;</a></div>
<% if (Current.HasRight(Permissions.AccessReports)) { %>
<script type="text/javascript">
    U.postUrl("/Report/PatientBirthdayWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
        if (data.Data != undefined) {
            for (var i = 0; i < data.Data.length && i < 5; i++) $('#birthdayWidgetContent').append("<tr><td>" + data.Data[i].BirthDay + "</td><td>" + data.Data[i].Age + "</td><td>" + data.Data[i].Name + "</td><td>" + data.Data[i].PhoneHomeFormatted + "</td></tr>");
        } else {
            $('#birthdayWidgetContent').append("<tr><td colspan='5' class='align_center'>No Messages found.</td></tr>");
            $('#birthdayWidgetMore').hide();
        }
    });
</script>
<% } %>