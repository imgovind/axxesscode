﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="newsfeed_widget">
<% try
   {
        using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(Axxess.Core.Infrastructure.AppSettings.NewsFeedUrl)) {
            if (xmlReader.ReadState != System.Xml.ReadState.Error) {
                System.ServiceModel.Syndication.SyndicationFeed syndicationFeed = System.ServiceModel.Syndication.SyndicationFeed.Load(xmlReader); %>
                <ul><% int i = 0;
                    foreach (var item in syndicationFeed.Items) {
                        if (i < 2) { %> 
                            <li class="post">
                                <a href="<%= item.Links[0].Uri.AbsoluteUri %>" target="_blank">
                                    <span class="title" ><%= item.Title.Text.ToTitleCase() %></span>&nbsp;-&nbsp;
                                    <span class="date"><%= item.PublishDate.ToString("d") %></span>
                                    <br />
                                    <span class="summary"><%= item.Summary.Text %></span>
                                </a>
                            </li><%
                        }
                        i++;
                    } %>
                </ul><%
            }
        }
   }
   catch (Exception ex) { %>
        <div class="align_center"><span>No news or updates found.</span></div>   
<% } %>
</div>
<div class="widget-more"><a target="_blank" href="http://acore.axxessconsult.com/blog">More &raquo;</a></div>
    
