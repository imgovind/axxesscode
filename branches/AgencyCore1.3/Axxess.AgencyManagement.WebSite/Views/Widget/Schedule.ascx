﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th style="width: 27%;">Patient Name</th>
				<th style="width: 35%;">Task</th>
				<th style="width: 25%;">Status</th>
				<th style="width: 18%;">Date</th>
			</tr>
        </thead>
	    <tbody id="scheduleWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="userScheduleWidgetMore" class="widget-more"><a onclick="UserInterface.ShowUserTasks();" href="javascript:void(0);">More &raquo;</a></div>
<% if (Current.HasRight(Permissions.ViewLists)) { %>
<script type="text/javascript">
    U.postUrl("/User/ScheduleWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
        if (data != undefined) {
            for (var i = 0; i < data.length && i < 5; i++) $('#scheduleWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].TaskName + "</td><td>" + data[i].StatusName + "</td><td>" + data[i].VisitDate + "</td></tr>");
        }
        else {
            $('#scheduleWidgetContent').append("<tr><td colspan='5' class='align_center'><h1>No Scheduled Tasks found.</h1></td></tr>");
            $('#userScheduleWidgetMore').hide();
        }
    });
</script>
<% } %>