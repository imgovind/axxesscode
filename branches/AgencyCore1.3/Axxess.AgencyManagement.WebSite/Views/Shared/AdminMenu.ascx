﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
acore.init();
acore.addmenu("Agency", "agency", "mainmenu", "admin.png");
acore.addwindow("newagency", "New Agency", "Agency/New", function() { Agency.InitNew(); }, "agency");
acore.addwindow("impersonate", "Impersonate User", "Agency/Impersonate", function() { Admin.Init(); }, "agency");
acore.addwindow("listagencies", "View Agencies", "Agency/List", function() { }, "agency");

acore.addwindow("editagency", "Edit Agency", null, null, false);
    
$('ul#mainmenu').superfish();

</script>

