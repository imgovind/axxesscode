﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<%  using (Html.BeginForm("Update", "Referral", FormMethod.Post, new { @id = "editReferralForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Referral | ",
        Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase() : "",
        "','editreferral');</script>")%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Referral_Id" }) %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row"><label for="Edit_Referral_Physician" class="float_left">Physician:</label><div class="float_right"><%= Html.Physicians("ReferrerPhysician",Model.ReferrerPhysician.ToString() , true, new { @id = "Edit_Referral_Physician", @class = "ReferrerPhysician" })%></div></div>
            <div class="row"><label for="Edit_Referral_AdmissionSource" class="float_left">Admission Source:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "Edit_Referral_AdmissionSource", @class = "AdmissionSource" })%></div></div>
            <div class="row"><label for="Edit_Referral_OtherReferralSource" class="float_left">Other Referral Source:</label><div class="float_right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Referral_Date" class="float_left">Referral Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("ReferralDate").Value(Model.ReferralDate).HtmlAttributes(new { @id = "Edit_Referral_Date", @class = "text date" })%></div></div>
            <div class="row"><label for="Edit_Referral_InternalReferral" class="float_left">Internal Referral:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Referral_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset><fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row"><label for="Edit_Referral_FirstName" class="float_left">First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Referral_FirstName", @maxlength = "30", @class = "required" }) %></div></div>
            <div class="row"><label for="Edit_Referral_LastName" class="float_left">Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Referral_LastName", @maxlength = "30", @class = "required" }) %></div></div>
            <div class="row"><label for="Edit_Referral_MedicareNo" class="float_left">Medicare No:</label><div class="float_right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Referral_MedicareNo", @maxlength = "11", @class = "text MedicareNo" })%></div></div>
            <div class="row"><label for="Edit_Referral_MedicaidNo" class="float_left">Medicaid No:</label><div class="float_right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Referral_MedicaidNo", @maxlength = "9", @class = "text MedicaidNo" })%></div></div>
            <div class="row"><label for="Edit_Referral_SSN" class="float_left">SSN:</label><div class="float_right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Referral_SSN", @maxlength = "9" }) %></div></div>
            <div class="row"><label for="Edit_Referral_DateOfBirth" class="float_left">Date of Birth:</label><div class="float_right"><%= Html.TextBox("DOB", Model.DOB.ToShortDateString(), new { @id = "Edit_Referral_DateOfBirth", @class = "required date" }) %></div></div>
            <div class="row"><label class="float_left">Gender:</label><div class="float_right"><%= Html.RadioButton("Gender", "Female", new { @id = "Edit_Referral_Gender_F", @class = "radio required" }) %><label for="Edit_Referral_Gender_F" class="inlineradio">Female</label><%= Html.RadioButton("Gender", "Male", new { @id = "Edit_Referral_Gender_M", @class = "radio required" })%><label for="Edit_Referral_Gender_M" class="inlineradio">Male</label></div></div>
        </div><div class="column">   <div class="row"><label for="Edit_Referral_HomePhone1" class="float_left">Home Phone:</label><div class="float_right"><span class="input_wrappermultible"><%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Referral_HomePhone1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "19" })%> </span>- <span class="input_wrappermultible"><%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Referral_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "20" })%> </span>- <span class="input_wrappermultible"> <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Referral_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5", @tabindex = "21" })%> </span></div></div>
            <div class="row"><label for="Edit_Referral_Email" class="float_left">Email Address:</label><div class="float_right"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Referral_Email", @class = "text email input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Referral_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Referral_AddressLine1", @maxlength = "20", @class = "text required input_wrapper" }) %></div></div>
            <div class="row"><label for="Edit_Referral_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Referral_AddressLine2", @maxlength = "20", @class = "text input_wrapper" }) %></div></div>
            <div class="row"><label for="Edit_Referral_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Referral_AddressCity", @maxlength = "20", @class = "text required input_wrapper" }) %></div></div>
            <div class="row"><label for="Edit_Referral_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Referral_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="Edit_Referral_Assign" class="float_left">Assign to Clinician:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Edit_Referral_Assign", @class = "Users required valid" })%></div></div>
        </div>
    </fieldset><fieldset>
        <legend>Services Required</legend>
        <table class="form"><tbody>
            <%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
            <tr>
                <td><%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%>
                    <label for="ServicesRequiredCollection0" class="radio">SNV</label></td>
                <td><%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ServicesRequiredCollection1" class="radio">HHA</label></td>
                <td><%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                    <label for="ServicesRequiredCollection2" class="radio">PT</label></td>
                <td><%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                    <label for="ServicesRequiredCollection3" class="radio">OT</label></td>
                <td><%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                    <label for="ServicesRequiredCollection4" class="radio">SP</label></td>
                <td><%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                    <label for="ServicesRequiredCollection5" class="radio">MSW</label></td>
            </tr>
        </tbody></table>
    </fieldset><fieldset>
        <legend>DME Needed</legend>
        <table class="form">
        <%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="DMECollection" />
         <tbody>
            <tr class="firstrow">
                <td><%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                    <label for="DMECollection0" class="radio">Bedside Commode</label></td>
                <td><%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                    <label for="DMECollection1" class="radio">Cane</label></td>
                <td><%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                    <label for="DMECollection2" class="radio">Elevated Toilet Seat</label></td>
                <td><%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                    <label for="DMECollection3" class="radio">Grab Bars</label></td>
                <td><%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                    <label for="DMECollection4" class="radio">Hospital Bed</label></td>
            </tr><tr>
                <td><%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                    <label for="DMECollection5" class="radio">Nebulizer</label></td>
                <td><%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                    <label for="DMECollection6" class="radio">Oxygen</label></td>
                <td><%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                    <label for="DMECollection7" class="radio">Tub/Shower Bench</label></td>
                <td><%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                    <label for="DMECollection8" class="radio">Walker</label></td>
                <td><%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                    <label for="DMECollection9" class="radio">Wheelchair</label></td>
            </tr><tr>
                <td colspan="5"><%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                    <label for="DMECollection10" class="radio">Other</label><%=Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Referral_DMEOther", @class = "text", @style = "display:none;" })%></td>
            </tr>
        </tbody>
        </table>
    </fieldset><fieldset>
        <legend>Physician Information</legend>
        <% var physcian=Model.Physicians.IsNotNullOrEmpty()?Model.Physicians.ToObject<List<Physician>>().SingleOrDefault(p=>p.IsPrimary):null; %>
        <div class="column">
            <div class="row"><label for="Edit_Referral_PhysicianDropDown" class="float_left">Primary Physician:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "AgencyPhysicians", physcian!=null? physcian.Id.ToString() : Guid.Empty.ToString() , new { @id = "Edit_Referral_PhysicianDropDown", @class = "Physicians" })%></div></div>
            <div class="row"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysician();">New Physician</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <textarea id="Edit_Referral_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editreferral');">Cancel</a></li>
    </ul></div>
</div>
<%= string.Format("<script type='text/javascript'> if({0}==1) {{ $(\"#Edit_Referral_DMEOther\").show();}} else {{ $(\"#Edit_Referral_DMEOther\").hide(); }}</script>", DME != null && DME.Contains("9") ? 1 : 0) %>
<% } %>
<!--[if !IE]>end forms<![endif]-->
<script type="text/javascript">
    $("#DMECollection10").click(function() {
    var otherDme = $('#DMECollection10:checked').is(':checked');
            if (!otherDme) {
                $("#Edit_Referral_DMEOther").hide();
            }
            else {
                $("#Edit_Referral_DMEOther").show();
            }
        });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>