﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Referral", FormMethod.Post, new { @id = "newReferralForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Referral | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newreferral');</script>")%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row"><label for="New_Referral_Physician" class="float_left">Physician:</label><div class="float_right"><%= Html.Physicians("ReferrerPhysician", "", true, new { @id = "New_Referral_Physician", @class = "ReferrerPhysician" })%></div></div>
            <div class="row"><label for="New_Referral_AdmissionSource" class="float_left">Admission Source:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", "", new { @id = "New_Referral_AdmissionSource", @class = "AdmissionSource" })%></div></div>
            <div class="row"><label for="New_Referral_OtherReferralSource" class="float_left">Other Referral Source:</label><div class="float_right"><%= Html.TextBox("OtherReferralSource", "", new { @id = "New_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Referral_Date" class="float_left">Referral Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("ReferralDate").Value(DateTime.Today).HtmlAttributes(new { @id = "New_Referral_Date", @class = "text date" })%></div></div>
            <div class="row"><label for="New_Referral_InternalReferral" class="float_left">Internal Referral:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", "", new { @id = "New_Referral_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row"><label for="New_Referral_FirstName" class="float_left">First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", "", new { @id = "New_Referral_FirstName", @maxlength = "30", @class = "required" }) %></div></div>
            <div class="row"><label for="New_Referral_LastName" class="float_left">Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", "", new { @id = "New_Referral_LastName", @maxlength = "30", @class = "required" }) %></div></div>
            <div class="row"><label for="New_Referral_MedicareNo" class="float_left">Medicare No:</label><div class="float_right"><%= Html.TextBox("MedicareNumber", " ", new { @id = "New_Referral_MedicareNo", @maxlength = "11", @class = "text MedicareNo" })%></div></div>
            <div class="row"><label for="New_Referral_MedicaidNo" class="float_left">Medicaid No:</label><div class="float_right"><%= Html.TextBox("MedicaidNumber", " ", new { @id = "New_Referral_MedicaidNo", @maxlength = "9", @class = "text MedicaidNo" })%></div></div>
            <div class="row"><label for="New_Referral_SSN" class="float_left">SSN:</label><div class="float_right"><%= Html.TextBox("SSN", "", new { @id = "New_Referral_SSN", @maxlength = "9" }) %></div></div>
            <div class="row"><label for="New_Referral_DateOfBirth" class="float_left">Date of Birth:</label><div class="float_right"><%= Html.TextBox("DOB", "", new { @id = "New_Referral_DateOfBirth", @class = "required date" }) %></div></div>
            <div class="row"><label class="float_left">Gender:</label><div class="float_right"><%= Html.RadioButton("Gender", "Female", new { @id = "New_Referral_Gender_F", @class = "radio required" }) %><label for="New_Referral_Gender_F" class="inlineradio">Female</label><%= Html.RadioButton("Gender", "Male", new { @id = "New_Referral_Gender_M", @class = "radio required" })%><label for="New_Referral_Gender_M" class="inlineradio">Male</label></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Referral_HomePhone1" class="float_left">Home Phone:</label><div class="float_right"><input type="text" class="autotext numeric required phone_short" name="PhoneHomeArray" id="New_Referral_HomePhone1" maxlength="3" /> - <input type="text" class="autotext numeric required phone_short" name="PhoneHomeArray" id="New_Referral_HomePhone2" maxlength="3" /> - <input type="text" class="autotext numeric required phone_long" name="PhoneHomeArray" id="New_Referral_HomePhone3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Referral_Email" class="float_left">Email Address:</label><div class="float_right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Referral_Email", @class = "text email input_wrapper" })%></div></div>
            <div class="row"><label for="New_Referral_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Referral_AddressLine1", @maxlength = "20", @class = "text required input_wrapper" }) %></div></div>
            <div class="row"><label for="New_Referral_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Referral_AddressLine2", @maxlength = "20", @class = "text input_wrapper" }) %></div></div>
            <div class="row"><label for="New_Referral_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Referral_AddressCity", @maxlength = "20", @class = "text required input_wrapper" }) %></div></div>
            <div class="row"><label for="New_Referral_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Referral_AddressStateCode", @class = "AddressStateCode requireddropdown valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="New_Referral_Assign" class="float_left">Assign to Clinician:</label><div class="float_right"><%= Html.Clinicians("UserId", "", new { @id = "New_Referral_Assign", @class = "Users required valid" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <table class="form"><tbody>
            <tr>
                <td><input type="checkbox" value="0" class="radio float_left" id="ServicesRequiredCollection0" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection0" class="radio">SNV</label></td>
                <td><input type="checkbox" value="1" class="radio float_left" id="ServicesRequiredCollection1" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection1" class="radio">HHA</label></td>
                <td><input type="checkbox" value="2" class="radio float_left" id="ServicesRequiredCollection2" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection2" class="radio">PT</label></td>
                <td><input type="checkbox" value="3" class="radio float_left" id="ServicesRequiredCollection3" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection3" class="radio">OT</label></td>
                <td><input type="checkbox" value="4" class="radio float_left" id="ServicesRequiredCollection4" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection4" class="radio">SP</label></td>
                <td><input type="checkbox" value="5" class="radio float_left" id="ServicesRequiredCollection5" name="ServicesRequiredCollection" /><label for="ServicesRequiredCollection5" class="radio">MSW</label></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <table class="form"><tbody>
            <tr>
                <td><input type="checkbox" value="0" class="radio float_left" id="DMECollection0" name="DMECollection" /><label for="DMECollection0" class="radio">Bedside Commode</label></td>
                <td><input type="checkbox" value="1" class="radio float_left" id="DMECollection1" name="DMECollection" /><label for="DMECollection1" class="radio">Cane</label></td>
                <td><input type="checkbox" value="2" class="radio float_left" id="DMECollection2" name="DMECollection" /><label for="DMECollection2" class="radio">Elevated Toilet Seat</label></td>
                <td><input type="checkbox" value="3" class="radio float_left" id="DMECollection3" name="DMECollection" /><label for="DMECollection3" class="radio">Grab Bars</label></td>
                <td><input type="checkbox" value="4" class="radio float_left" id="DMECollection4" name="DMECollection" /><label for="DMECollection4" class="radio">Hospital Bed</label></td>
            </tr><tr>
                <td><input type="checkbox" value="5" class="radio float_left" id="DMECollection5" name="DMECollection" /><label for="DMECollection5" class="radio">Nebulizer</label></td>
                <td><input type="checkbox" value="6" class="radio float_left" id="DMECollection6" name="DMECollection" /><label for="DMECollection6" class="radio">Oxygen</label></td>
                <td><input type="checkbox" value="7" class="radio float_left" id="DMECollection7" name="DMECollection" /><label for="DMECollection7" class="radio">Tub/Shower Bench</label></td>
                <td><input type="checkbox" value="8" class="radio float_left" id="DMECollection8" name="DMECollection" /><label for="DMECollection8" class="radio">Walker</label></td>
                <td><input type="checkbox" value="9" class="radio float_left" id="DMECollection9" name="DMECollection" /><label for="DMECollection9" class="radio">Wheelchair</label></td>
            </tr><tr>
                <td colspan="5"><input type="checkbox" value="10" id="New_Referral_DMEOther" class="radio" name="DMECollection" /><label for="New_Referral_DMEOther" class="radio more">Other</label><%= Html.TextBox("OtherDME", "", new { @id = "New_Referral_OtherDME", @class = "text" ,@style="display:none;"}) %></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_PhysicianDropDown1" class="float_left">Primary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Referral_PhysicianDropDown1", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.addPhysician(this)">add new</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown2" class="float_left">Additional Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Referral_PhysicianDropDown2", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown3" class="float_left">Additional Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Referral_PhysicianDropDown3", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown4" class="float_left">Additional Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Referral_PhysicianDropDown4", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown5" class="float_left">Additional Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Referral_PhysicianDropDown5", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row"><textarea id="New_Referral_Comments" name="Comments" cols="5" rows="6"></textarea></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newreferral');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<% } %>
