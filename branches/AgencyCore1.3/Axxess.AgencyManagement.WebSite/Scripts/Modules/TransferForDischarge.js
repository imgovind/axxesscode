﻿var TransferForDischarge = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("TransferForDischarge", "transferfordischarge", control.closest("form"), control, action); },
    loadTransferForDischarge: function(id, patientId, assessmentType) { acore.open("transferfordischarge", 'Oasis/TransferForDischarge', function() { Oasis.InitTabs("TransferForDischarge", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}