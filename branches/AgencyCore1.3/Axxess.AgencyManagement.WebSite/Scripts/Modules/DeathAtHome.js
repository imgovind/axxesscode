﻿var DeathAtHome = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("death", "deathathome", control.closest("form"), control, action); },
    loadDeathAtHome: function(id, patientId, assessmentType) { acore.open("deathathome", 'Oasis/Death', function() { Oasis.InitTabs("Death", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}