﻿var FollowUp = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("followup", "followup", control.closest("form"), control, action); },
    loadFollowUp: function(id, patientId, assessmentType) { acore.open("followup", 'Oasis/FollowUp', function() { Oasis.InitTabs("FollowUp", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}