﻿var Recertification = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("recertificationcategoryTabs", "recertification", control.closest("form"), control, action); },
    loadRecertification: function(id, patientId, assessmentType) { acore.open("recertification", 'Oasis/Recertification', function() { Oasis.InitTabs("Recertification", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}