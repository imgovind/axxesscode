﻿var SOC = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("socTabs", "startofcare", control.closest("form"), control, action); },
    loadSoc: function(id, patientId, assessmentType) { acore.open("startofcare", 'Oasis/Soc', function() { Oasis.InitTabs("Soc", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}