﻿var Payroll = {
    InitSearch: function() {
        $("#searchPayrollForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'html',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        $("#payrollSearchResultDetails").hide();
                        $("#payrollSearchResult").show();
                        $('#payrollSearchResult').html(result);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadDetail: function(userId) {
        var startDate = new Date($("#startDate").data("tDatePicker").value());
        var endDate = new Date($("#endDate").data("tDatePicker").value());
        startDate = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        endDate = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();

        $('#payrollSearchResultDetails').load('Payroll/Detail', { userId: userId, startDate: startDate, endDate: endDate }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#payrollSearchResultDetails').html('<p>There was an error making the AJAX request</p>');
            } else if (textStatus == "success") {
                $("#payrollSearchResult").hide();
            }
            $("#payrollSearchResultDetails").show();
        });
    },
    LoadDetails: function() {
        var startDate = new Date($("#startDate").data("tDatePicker").value());
        var endDate = new Date($("#endDate").data("tDatePicker").value());
        startDate = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        endDate = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();

        $('#payrollSearchResultDetails').load('Payroll/Details', { startDate: startDate, endDate: endDate }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#payrollSearchResultDetails').html('<p>There was an error making the AJAX request</p>');
            } else if (textStatus == "success") {
                $("#payrollSearchResult").hide();
            }
            $("#payrollSearchResultDetails").show();
        });

    }
}
