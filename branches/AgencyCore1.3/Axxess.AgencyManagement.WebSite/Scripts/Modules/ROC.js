﻿var ROC = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("rocTabs", "resumptionofcare", control.closest("form"), control, action); },
    loadRoc: function(id, patientId, assessmentType) { acore.open("resumptionofcare", 'Oasis/Roc', function() { Oasis.InitTabs("Roc", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}