﻿var Patient = {
    _patientId: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    GetId: function() {
        return Patient._patientId;
    },
    SetId: function(patientId) {
        Patient._patientId = patientId;
    },
    SetPatientRowIndex: function(patientRowIndex) {
        Patient._patientRowIndex = patientRowIndex;
    },
    GetEpisodeId: function() {
        return Patient._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        Patient._EpisodeId = EpisodeId;
    },
    InitNew: function() {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: true });

        var $triage = $('input.Triage');
        $triage.click(function() {
            $triage.removeAttr('checked');
            $(this).attr('checked', true);
        });

        $('#New_Patient_HomePhone1').autotab({ target: 'New_Patient_HomePhone2', format: 'numeric' });
        $('#New_Patient_HomePhone2').autotab({ target: 'New_Patient_HomePhone3', format: 'numeric', previous: 'New_Patient_HomePhone1' });
        $('#New_Patient_HomePhone3').autotab({ target: 'New_Patient_MobilePhone1', format: 'numeric', previous: 'New_Patient_HomePhone2' });

        $('#New_Patient_MobilePhone1').autotab({ target: 'New_Patient_MobilePhone2', format: 'numeric' });
        $('#New_Patient_MobilePhone2').autotab({ target: 'New_Patient_MobilePhone3', format: 'numeric', previous: 'New_Patient_MobilePhone1' });
        $('#New_Patient_MobilePhone3').autotab({ target: 'New_Patient_Email', format: 'numeric', previous: 'New_Patient_MobilePhone2' });

        $('#New_Patient_PharmacyPhone1').autotab({ target: 'New_Patient_PharmacyPhone2', format: 'numeric' });
        $('#New_Patient_PharmacyPhone2').autotab({ target: 'New_Patient_PharmacyPhone3', format: 'numeric', previous: 'New_Patient_PharmacyPhone1' });
        $('#New_Patient_PharmacyPhone3').autotab({ target: 'New_Patient_PrimaryInsurance', format: 'numeric', previous: 'New_Patient_PharmacyPhone2' });

        $('#New_Patient_EmergencyContactPhonePrimary1').autotab({ target: 'New_Patient_EmergencyContactPhonePrimary2', format: 'numeric' });
        $('#New_Patient_EmergencyContactPhonePrimary2').autotab({ target: 'New_Patient_EmergencyContactPhonePrimary3', format: 'numeric', previous: 'New_Patient_EmergencyContactPhonePrimary1' });
        $('#New_Patient_EmergencyContactPhonePrimary3').autotab({ target: 'New_Patient_EmergencyContactRelationship', format: 'numeric', previous: 'New_Patient_EmergencyContactPhonePrimary2' });

        $("#New_Patient_PaymentSource").click(function() {
            var q = $('#New_Patient_PaymentSource:checked').is(':checked');
            if (!q) {
                $("#New_Patient_OtherPaymentSource").hide();
            }
            else {
                $("#New_Patient_OtherPaymentSource").show();
            }
        });

        $("#New_Patient_DMEOther").click(function() {
            var otherDme = $('#New_Patient_DMEOther:checked').is(':checked');
            if (!otherDme) {
                $("#New_Patient_OtherDME").hide();
            }
            else {
                $("#New_Patient_OtherDME").show();
            }
        });

        $("#newPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient added successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            Schedule.Rebind();
                            Referral.RebindList();
                            if ($('#New_Patient_Status').val() == "1") {
                                UserInterface.ShowPatientPrompt();
                            }
                            UserInterface.CloseWindow('newpatient');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: true });

        $('#Edit_Patient_HomePhone1').autotab({ target: 'Edit_Patient_HomePhone2', format: 'numeric' });
        $('#Edit_Patient_HomePhone2').autotab({ target: 'Edit_Patient_HomePhone3', format: 'numeric', previous: 'Edit_Patient_HomePhone1' });
        $('#Edit_Patient_HomePhone3').autotab({ target: 'Edit_Patient_MobilePhone1', format: 'numeric', previous: 'Edit_Patient_HomePhone2' });

        $('#Edit_Patient_MobilePhone1').autotab({ target: 'Edit_Patient_MobilePhone2', format: 'numeric' });
        $('#Edit_Patient_MobilePhone2').autotab({ target: 'Edit_Patient_MobilePhone3', format: 'numeric', previous: 'Edit_Patient_MobilePhone1' });
        $('#Edit_Patient_MobilePhone3').autotab({ target: 'Edit_Patient_Email', format: 'numeric', previous: 'Edit_Patient_MobilePhone2' });

        $('#Edit_Patient_PharmacyPhone1').autotab({ target: 'Edit_Patient_PharmacyPhone2', format: 'numeric' });
        $('#Edit_Patient_PharmacyPhone2').autotab({ target: 'Edit_Patient_PharmacyPhone3', format: 'numeric', previous: 'Edit_Patient_PharmacyPhone1' });
        $('#Edit_Patient_PharmacyPhone3').autotab({ target: 'Edit_Patient_PrimaryInsurance', format: 'numeric', previous: 'Edit_Patient_PharmacyPhone2' });

        $("#editPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient updated successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow('editpatient');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitCenter: function() {
        $("select.patientStatusDropDown").change(function() {
            $('#patientMainResult').html('<p>No patients found matching your search criteria!</p>');
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });
        });

        $("select.patientPaymentDropDown").change(function() {
            $('#patientMainResult').html('<p>No patients found matching your search criteria!</p>');
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });
        });

        $('#txtSearch_Patient_Selection').keyup(function() {
            $('#patientMainResult').html('<p>No patients found matching your search criteria!</p>');
            var patientGrid = $('#PatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.patientStatusDropDown").val(), paymentSourceId: $("select.patientPaymentDropDown").val(), name: $("#txtSearch_Patient_Selection").val() });
        });

        $("#patientActivityDropDown").change(function() {
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });
    },
    InitNewPhoto: function() {
        $("#changePatientPhotoForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Patient photo updated successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            U.closeDialog();
                        }
                        else {
                            alert(result.responseText);
                        }
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Patient photo updated successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            U.closeDialog();
                        }
                        else {
                            alert(result.responseText);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    activityRowDataBound: function(e) {
        Schedule.tooltip();

        var dataItem = e.dataItem;
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            Patient.SetId(patientId);
            Patient.loadInfoAndActivity(patientId);
        }
    },
    loadInfoAndActivity: function(id) {
        $('#patientMainResult').load('Patient/Data', { patientId: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#patientMainResult').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $('#window_patientcenter .layout').layout({ west__paneSelector: '.layout_left' });
                $('#window_patientcenter .t-grid-content').css({ height: 'auto' });
            }
        });
    },
    loadEditPatient: function(id) {
        acore.open("editpatient", 'Patient/EditPatientContent', function() {
            Patient.InitEdit();
            Lookup.loadAdmissionSources();
            Lookup.loadRaces();
            Lookup.loadUsers();
            Lookup.loadReferralSources();
        }, { patientId: id });
    },
    InitNewOrder: function() {
        $("#newOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('neworder');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewCommunicationNote: function() {
        $("#newCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("New Communication Note saved successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newcommnote');
                            Patient.Rebind();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditOrder: function() {
        $("#editOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editorder');
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Rebind: function() {

        var patientSelectionGrid = $('#PatientSelectionGrid').data('tGrid');
        if (patientSelectionGrid != null) {
            patientSelectionGrid.rebind();
        }
        var patientList = $('#List_Patient_Grid').data('tGrid');
        if (patientList != null) {
            patientList.rebind();
        }

        var patientPendingList = $('#List_PatientPending_Grid').data('tGrid');
        if (patientPendingList != null) {
            patientPendingList.rebind();
        }

    },
    LoadNewOrder: function(patientId) {
        acore.open("neworder", 'Order/New', function() {
            Patient.InitNewOrder();
        });
    },
    LoadNewCommunicationNote: function(patientId) {
        acore.open("newcommnote", 'CommunicationNote/New', function() {
            Patient.InitNewCommunicationNote();
        }, { patientId: patientId });
    },
    InfoPopup: function(e) {
        var $dialog = $("#patientInfo");
        $dialog.dialog({
            width: 500,
            position: [$(e).offset().left, $(e).offset().top],
            modal: false,
            resizable: false,
            close: function() {
                $dialog.dialog("destroy");
                $dialog.hide();
            }
        }).show();
        $('#ui-dialog-title-patientInfo').html('Patient Info');
    },
    addPhysician: function(el) { $(el).closest('.column').find('.row.hidden').first().removeClass('hidden'); },
    removePhysician: function(el) { $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000'); },
    InitMedicationProfile: function() {
        $("#newMedicationProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('medicationprofile');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitMedicationProfileSnapshot: function() {
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    loadMedicationProfileSnapshot: function(id) {
        acore.open("medicationprofilesnapshot", 'Patient/MedicationProfileSnapShot', function() { Patient.InitMedicationProfileSnapshot(); }, { PatientId: id });
    },
    loadMedicationProfile: function(id) {
        acore.open("medicationprofile", 'Patient/MedicationProfile', function() { Patient.InitMedicationProfile(); }, { PatientId: id });
    },
    loadMedicationProfileSnapshotHistory: function(id) {
        acore.open("medicationprofilesnapshothistory", 'Patient/MedicationProfileSnapShotHistory', function() { Patient.InitMedicationProfileSnapshot(); }, { PatientId: id });
    },
    onMedicationProfileEdit: function(e) {

        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 7) {
                    if (e.dataItem['MedicationCategory'] != 'DC') {
                        $(this).html('');
                    }
                }
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 6) {
                    if (e.dataItem['Classification'] == null || e.dataItem['Classification'] == '') {
                        $(this).find("input[name=MedicationClassification]").val('');
                    }
                }
            });
        }
    },
    onPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
        }
    },
    Discharge: function(assessmentId, PatientId, Id) {
        U.showDialog("#dischargeMedicationProfileDialog", function() {
            $("#dischargeMedicationProfileForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        data: { AssessmentId: assessmentId, PatientId: PatientId, AssessmentType: assessmentType, Id: Id },
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                U.closeDialog();
                                var patientGrid = $('#MedicatonProfileGrid').data('tGrid');
                                patientGrid.rebind({ AssessmentId: assessmentId, PatientId: PatientId });
                                $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        });
    },
    onMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt ').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        }
        else {
            e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" href="javascript:void(0);" onclick="' + dataItem.DischargeUrl + '">D/C</a>';
        }
    },
    OnDataBound: function(e) {
        var id = $("#medicationProfileHistroyId").val();
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ MedId: id, medicationCategry: "DC" });
    },
    DischargeMed: function(MedId, Id) {
        U.showDialog("#dischargeMedicationProfileDialog", function() {
            $("#dischargeMedicationProfileForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        data: { MedId: MedId, Id: Id },
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                U.closeDialog();
                                var medicatonProfileGridActive = $('#MedicatonProfileGridActive').data('tGrid');
                                medicatonProfileGridActive.rebind({ MedId: MedId, medicationCategry: "Active" });

                                var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
                                medicatonProfileGridDC.rebind({ MedId: MedId, medicationCategry: "DC" });

                                $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        });
    },
    DateRange: function(id) {

        var data = 'dateRangeId=' + id + "&patientId=" + $("#PatientCenter_PatientId").val();
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                $("#dateRangeText").empty();
                $("#dateRangeText").append(reportObject.StartDateFormatted + "-" + reportObject.EndDateFormatted);
            }
        });
    },
    CustomDateRange: function() {
        var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        if (PatientActivityGrid != null) {
            PatientActivityGrid.rebind({ patientId: $("#PatientCenter_PatientId").val(), discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val(), rangeStartDate: $("#patientActivityFromDate-input").val(), rangeEndDate: $("#patientActivityToDate-input").val() });
        }
    },
    DeleteSchedule: function(episodeId, patientId, eventId, employeeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId;
            U.postUrl("/Schedule/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.CustomDateRange();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                }
            });
        }
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?")) {
            var input = "patientId=" + patientId;
            U.postUrl("/Patient/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    //Patient.CustomDateRange();
                    Patient.Rebind();
                    Schedule.Rebind();
                    $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    InitAdmit: function(patientId) {
        $("#Admit_Patient_EpisodeStartDate").data("tDatePicker").hidePopup();
        $("#Admit_Patient_PatientReferralDate").data("tDatePicker").hidePopup();
        $("#Admit_Patient_StartOfCareDate").data("tDatePicker").hidePopup();

        var e = $("#Admit_Patient_StartOfCareDate").data("tDatePicker").value();
        $("#Admit_Patient_EpisodeStartDate").data("tDatePicker").value(e);
        $("#Admit_Patient_EpisodeStartDate").data("tDatePicker").minDate.value = e;

        $("#newAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient admission successful.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('admitpatient');
                            U.closeDialog();
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.ShowPatientPrompt();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").data("tDatePicker").hidePopup();
        $("#newNonAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient non-admission successful.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('nonadmitpatient');
                            U.closeDialog();
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    loadEditEmergencyContact: function(patientId, id) {
        acore.open("editemergencycontact", 'Patient/EditEmergencyContactContent', function() {
            $("#editEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                $.jGrowl("Edit emergency contact is successful.", { theme: 'success', life: 5000 });
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: patientId });
                                }
                                UserInterface.CloseWindow('editemergencycontact');
                            } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    loadNewEmergencyContact: function(id) {
        acore.open("newemergencycontact", 'Patient/NewEmergencyContactContent', function() {
            $("#newEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                $.jGrowl("New emergency contact added successful.", { theme: 'success', life: 5000 });
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: id });
                                }
                                UserInterface.CloseWindow('newemergencycontact');
                            } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        }, { patientId: id });
    }
    ,
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeleteEmergencyContact",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                        if (emergencyContact != null) {
                            emergencyContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            }
            );
        }
    },
    AddPatientPhysicain: function(id, patientId) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/AddPatientPhysicain",
            data: "Id=" + id + "&patientId=" + patientId,
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var physicianContact = $('#Edit_patient_PhysicianContact_Grid').data('tGrid');
                    $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                    if (physicianContact != null) {
                        physicianContact.rebind({ PatientId: patientId });
                    }
                }
                else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            }

        }
            );
    },
    DeletePhysicianContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeletePhysicianContact",
                data: "id=" + id + "&patientID=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var physicianContact = $('#Edit_patient_PhysicianContact_Grid').data('tGrid');
                        if (physicianContact != null) {
                            physicianContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            });
        }
    },

    medicationClassficationtips: function(control) {
        $(control).autocomplete('/LookUp/MedicationClassfication', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Id + '-' + row.Name;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $(control).val(row.Name);
            }
        });
    }
    ,
    medicationDosagetips: function(control) {
        $(control).autocomplete('/LookUp/MedicationDosage', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.ProperName + ' ' + row.Dose;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $(control).val(row.ProperName + ' ' + row.Dose);
            }
        });
    },
    medicationRoutetips: function(control) {
        $(control).autocomplete('/LookUp/MedicationRoute', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Id + '-' + row.ShortName + '  ' + row.LongName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $(control).val(row.LongName + '(' + row.ShortName + ')');
            }
        });
    },
    OnSocChange: function(e) {
        var date = e.date;
        $("#New_Patient_EpisodeStartDate").data("tDatePicker").value(date);
        $("#New_Patient_EpisodeStartDate").data("tDatePicker").minDate.value = date;
    }
}
