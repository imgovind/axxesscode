﻿var Home = {
    Init: function() {
        if ($("#firstVisit").attr("id") != undefined) {
            U.showDialog("#firstVisit", function() {
                U.InitTemplate("first", "Visit", function() { U.closeDialog(); window.location.replace("/"); });
            });
        }
        if ($("#Change_Signature_Trigger").val() == "true") UserInterface.ShowChangeSignatureModal();
    },
    LoadWidgets: function() {
        U.postUrl("/Billing/Unprocessed", "", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#claimsWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].Type + "</td><td>" + data[i].EpisodeRange + "</td></tr>");
            } else {
                $('#claimsWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Bills found.</h1></td></tr>");
                $('#claimsWidgetMore').hide();
            }
        });
    }
}