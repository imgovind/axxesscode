﻿var ChangePassword = {
    Init: function() {
        $('input[name="NewPassword"]').passwordStrength({ targetDiv: '#iSM', classes: Array('weak', 'medium', 'strong') });
        $("#changePasswordForm").validate({
            rules: {
                NewPassword: { required: true, minlength: 8 },
                ConfirmPassword: { required: true, minlength: 8 }
            },
            messages: {
                OldPassword: "",
                NewPassword: { required: "", minlength: "8 characters minimum" },
                ConfirmPassword: { required: "", minlength: "8 characters minimum" }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) window.location.replace(resultObject.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span>' + resultObject.errorMessage + '</span>');
                            U.unBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Logon = {
    Init: function() {
        $("#loginForm").validate({
            messages: { UserName: "", Password: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) window.location.replace(resultObject.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span>' + resultObject.errorMessage + '</span>');
                            U.unBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var ResetPassword = {
    Init: function() {
        $("#forgotPasswordForm").validate({
            messages: { UserName: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span>' + resultObject.errorMessage + '</span>');
                            $("#formContainer").hide();
                        } else $("#messages").empty().removeClass().addClass("notification error").append('<span>' + resultObject.errorMessage + '</span>');
                        U.unBlock();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}