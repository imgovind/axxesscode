﻿var U =
{
    ajaxError: "%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E",
    tooltip: function(o, c) { o.each(function() { if ($(this).attr("tooltip") != undefined) { $(this).tooltip({ track: true, showURL: false, top: 10, left: 10, extraClass: c, bodyHandler: function() { return $(this).attr("tooltip"); } }); } }); },
    rebindTGrid: function(g, a) { if (g.data('tGrid') != null) g.data('tGrid').rebind(a); },
    growl: function(m, t) { $.jGrowl($.trim(m), { theme: t, life: 5000 }); },
    HandlerHelperTemplate: function(t, w, form, control, action) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
                form.find('.form-omitted :input').val("").end().find('.form-omitted:input').val("");
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    if (control.html() == "Save &amp; Exit") {
                        UserInterface.CloseWindow(w);
                        Schedule.Rebind();
                        User.RebindScheduleList();
                    }
                    else if (control.html() == "Save &amp; Continue") Oasis.NextTab("#" + t + "Tabs.tabs");
                    else if (control.html() == "Save &amp; Check for Errors") action();
                    else if (control.html() == "Check for Errors") action();
                } else U.growl(result.responseText, "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    if (control.html() == "Save &amp; Exit") {
                        UserInterface.CloseWindow(w);
                        Schedule.Rebind();
                        User.RebindScheduleList();
                    }
                    else if (control.html() == "Save &amp; Continue") Oasis.NextTab("#" + t + "Tabs.tabs");
                    else if (control.html() == "Save &amp; Check for Errors") action();
                    else if (control.html() == "Check for Errors") action();
                } else U.growl(result.responseText, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    DeleteTemplate: function(t, i, f, b) {
        if (b || confirm("Are you sure you want to delete this " + t.toLowerCase() + "?")) {
            U.postUrl("/" + t + "/Delete", { Id: i }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    f();
                    U.growl(t + " successfully deleted.", "success");
                } else U.growl(resultObject.errorMessage, "error");
            });
        }
    },
    InitTemplate: function(s, t, f) {
        $("#" + s + t + "Form").validate({
            submitHandler: function(form) {
                if (t == "Message") $("textarea[name=Body]").val(CKEDITOR.instances['New_Message_Body'].getData());
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            f();
                            U.growl(((s == "new" ? (t == "Message" ? "Your message has been sent." : "New" + t.toLowerCase() + " successfully added.") : "") +
                                (s == "edit" ? t + " successfully updated." : "") + (s == "first" ? "Welcome to Agency Core!" : "")), "success");
                            UserInterface.CloseWindow(s + t.toLowerCase());
                        } else U.growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    showDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    closeDialog: function() {
        $.unblockUI();
    },
    getUrl: function(url, input, onSuccess) {
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    postUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: 'Loading...',
            css: {
                color: '#fff',
                opacity: .6,
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                top: ($(window).height() - 40) / 2 + 'px',
                left: ($(window).width() - 100) / 2 + 'px'
            }
        });
    },
    unBlock: function(selector) {
        $.unblockUI();
    },
    toTitleCase: function(text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    }
};


jQuery.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }

