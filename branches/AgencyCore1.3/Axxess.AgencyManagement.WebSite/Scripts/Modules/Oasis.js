﻿var Oasis = {
    DeleteAsset: function(control, assessmentType, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#" + assessmentType + "_EpisodeId").val() + "&patientId=" + $("#" + assessmentType + "_PatientGuid").val() + "&eventId=" + $("#" + assessmentType + "_Id").val() + "&assessmentType=" + assessmentType + "&name=" + name + "&assetId=" + assetId;
            U.postUrl("/Oasis/DeleteWoundCareAsset", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"" + assessmentType + "_" + name + "\" value=\"Upload\" size=\"1\" class = \"float_left uploadWidth\" />");
                }
            });
        }
    },
    InitTabs: function(t, id, patientId, assessmentType) {
        U.tooltip($(".verttab a"), "calday"); 
        $("#" + t.toLowerCase() + "Tabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#" + t.toLowerCase() + "Tabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $("#" + t.toLowerCase() + "Tabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) { Oasis.loadParts(t, event, ui); });
    },
    loadParts: function(t, event, ui) {
        $($(ui.tab).attr('href')).empty().addClass("loading").load('Oasis/' + t + 'Category', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') $($(ui.tab).attr('href')).html(unescape(ui.ajaxError));
            else if (textStatus == "success") $($(ui.tab).attr('href')).removeClass("loading");
        });
    },
    Init: function() {
        $(".numeric").numeric();
        $(".tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $(".tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $.ajaxSetup({ type: "POST" });
        $(".diagnosis").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.ShortDescription).parent().parent().find('.icd').val((row.FormatCode)); });
        $(".icd").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "icd" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.FormatCode).parent().parent().find('.diagnosis').val(row.ShortDescription); });
        $(".procedureICD").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "PROCEDUREICD" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.FormatCode).parent().parent().find('.procedureDiagnosis').val(row.ShortDescription); });
        $(".procedureDiagnosis").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "PROCEDURE" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.ShortDescription).parent().parent().find('.procedureICD').val((row.FormatCode)); });
        $(".ICDM1024").autocomplete('/LookUp/DiagnosisCodeNoVE', {
            max: 50,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "icd" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.FormatCode).parent().find('.diagnosisM1024').val(row.ShortDescription); });
        $(".diagnosisM1024").autocomplete('/LookUp/DiagnosisCodeNoVE', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.ShortDescription).parent().find('.ICDM1024').val((row.FormatCode)); });
    },
    stripDecimal: function(data) {
        var decimalPos = data.indexOf('.');
        var newData = '';
        for (i = 0; i < data.length; i++) if (i != decimalPos && data.charAt(i) != ' ') newData = newData + data.charAt(i);
        return newData;
    },
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/GetPhysicianContact",
            data: "PhysicianContactId=" + id,
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text((resultObject.FirstName !== null ? resultObject.FirstName : "") + " " + (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (resultObject.PhoneWork.substring(0, 3) + "-" + resultObject.PhoneWork.substring(3, 6) + "-" + resultObject.PhoneWork.substring(6)) : "");
            }
        });
    },
    Close: function(control) { control.closest('div.window').hide(); },
    BlockAssessmentType: function() { $("div.assessmentType").block({ message: '', overlayCSS: { "backgroundColor": "transparent", "cursor": "default"} }); },
    NextTab: function(id) { $(id).tabs().tabs('select', $(id).tabs().data("selected.tabs") + 1); },
    Refresh: function(id) { $(id).tabs().tabs('select', 0).find(id + " .general").each(function() { $(this).scrollTop(0) }); },
    SaveClose: function(control) {
        var formId = control.closest("form").attr("id");
        $("#" + formId).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) Oasis.Close(control);
                        else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(cont, id) {
        var row = cont.parents('tr:first');
        if (confirm("Are you sure you want to delete this Assessment?")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Oasis/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) $(row).remove();
                }
            });
        }
    },
    addTableRow: function(jQtable) {
        $(jQtable).each(function() {
            var $table = $(this);
            var tds = '<tr><td colspan="2"><input class="fill" value="" type="text" onfocus="Oasis.SupplyDescription($(this));" /></td><td colspan="2"><input cla' +
                'ss="fill" value="" type="text" onfocus="Oasis.SupplyCode($(this));" class="suppliesCode"/></td><td colspan="2"><input class="fill" value="" type' +
                '="text" class="supplyQuantity"/></td><td class="align_center"><img src="/Images/forms/grey_left.png" /><input class="grey_button" type="button" ' +
                'onclick="Oasis.DeleteRow($(this));" value="Delete" /><img src="/Images/forms/grey_right.png" /></td></tr>';
            if ($('tbody', this).length > 0) $('tbody', this).append(tds);
            else $(this).append(tds);
        });
    },
    addToSupplyTable: function(data, table) {
        Oasis.ClearRows(table);
        var tableRow = "";
        for (var i = 0; i < data.length; i++) tableRow += '<tr><td class="icdtext"><input value="' + data[i].suppliesDescription + '" type="text" onfocus="Oasis.' +
            'SupplyDescriptionOld($(this));" class="suppliesDescription"/></td><td><input value="' + data[i].suppliesCode + '" type="text" onfocus="Oasis.SupplyC' +
            'odeOld($(this));" class="suppliesCode"/></td><td><input value="' + data[i].supplyQuantity + '" type="text" class="supplyQuantity"/></td><td><input t' +
            'ype="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /></td></tr>';
        if ($('tbody', table).length > 0) $('tbody', table).append(tableRow);
        else $(table).append(tableRow);
    },
    SupplyCodeOld: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.Description; },
            width: 400,
            extraParams: { "type": "Code" }
        }).result(function(event, row, formatted) { if (row) $(this).val(Oasis.stripDecimal(row.Code)).parent().parent().find('.suppliesDescription').val(row.Description); });
    },
    SupplyDescriptionOld: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.Description; },
            width: 500,
            extraParams: { "type": "Description" }
        }).result(function(event, row, formatted) { if (row) $(this).val(row.Description).parent().parent().find('.suppliesCode').val(Oasis.stripDecimal(row.Code)); });
    },
    supplyInputFix: function(assessmentType, tableName) {
        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 0;
        var jsonData = '{ "Supply": [';
        $(tableTr).each(function() {
            jsonData += '{"suppliesDescription":"' + $(this).find('.suppliesDescription').val() + '","suppliesCode":"' + $(this).find('.suppliesCode').val() + '"' +
                ',"supplyQuantity":"' + $(this).find('.supplyQuantity').val() + '"}';
            if (len > ++i) jsonData += ',';
        });
        jsonData += '] }';
        $("#" + assessmentType + "_GenericSupply").val(jsonData);
    },
    ClearRows: function(table) { $('tbody tr', table).each(function() { $(this).remove(); }); },
    DeleteRow: function(control) {
        if ($(control).closest('tbody').find('tr').length == 1) {
            $('.scheduleTables.purgable').each(function() { $(this).find('tbody').empty(); })
            Schedule.ShowScheduler();
        } else $(control).closest('tr').remove();
        Schedule.positionBottom();
    },
    BradenScaleScore: function(assessmentType) {
        var sensory = $("input[name=" + assessmentType + "_485BradenScaleSensory]:checked");
        var moisture = $("input[name=" + assessmentType + "_485BradenScaleMoisture]:checked");
        var activity = $("input[name=" + assessmentType + "_485BradenScaleActivity]:checked");
        var mobility = $("input[name=" + assessmentType + "_485BradenScaleMobility]:checked");
        var nutrition = $("input[name=" + assessmentType + "_485BradenScaleNutrition]:checked");
        var friction = $("input[name=" + assessmentType + "_485BradenScaleFriction]:checked");
        $("#" + assessmentType + "_485BradenScaleTotal").val((sensory.length ? parseInt(sensory.val()) : 0) + (moisture.length ? parseInt(moisture.val()) : 0) + (activity.length ? parseInt(activity.val()) : 0) + (mobility.length ? parseInt(mobility.val()) : 0) + (nutrition.length ? parseInt(nutrition.val()) : 0) + (friction.length ? parseInt(friction.val()) : 0));
        Oasis.displayRisk(assessmentType);
    },
    displayRisk: function(assessmentType) {
        var total = parseInt($("#" + assessmentType + "_485BradenScaleTotal").val());
        var resultText = '';
        if (total == 0) resultText = '';
        else if (total >= 19) resultText = 'Patients with this value are generally considered not at risk';
        else if (total >= 15 && total <= 18) resultText = 'At risk to develop pressure ulcers';
        else if (total >= 13 && total <= 14) resultText = 'Moderate risk for developing pressure ulcers';
        else if (total >= 10 && total <= 12) resultText = 'High risk for developing pressure ulcers';
        else if (total <= 9) resultText = 'Very high risk for developing pressure ulcers';
        $("#" + assessmentType + "_485ResultBox").html(resultText);
    },
    BradenScaleOnchange: function(assessmentType, tableId) { $('input:radio', $(tableId)).change(function() { Oasis.BradenScaleScore(assessmentType); }) },
    CalculateNutritionScore: function(assessmentType) {
        var score = 0;
        $("input[name=" + assessmentType + "_GenericNutritionalHealth][type=checkbox]:checked").each(function() {
            score += parseInt($(this).parents('td').next().find('label').html());
        });
        if (score <= 25) {
            $("#" + assessmentType + "_GoodNutritionalStatus").addClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 55) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").addClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 100) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").addClass("strong");
        }
        $("#" + assessmentType + "_NutritionalHealthScreenTotal").val(score);
    },
    ToolTip: function(mooCode) {
        U.postUrl("/Oasis/Guide", 'mooCode=' + mooCode, function(data) {
            $('.tooltipbox').each(function() { $(this).remove() });
            $('#desktop').append(unescape("%3Cdiv class=%22tooltipbox%22%3E%3Cdiv class=%22closer%22 onclick=%22$(this).parent().remove();%22%3EX%3C/div%3E%3Ch3%" +
                "3EItem Intent%3C/h3%3E%3Cdiv class=%22content intent%22%3E" + data.ItemIntent + "%3C/div%3E%3Ch3%3EResponse%3C/h3%3E%3Cdiv class=%22content resp" +
                "onse%22%3E" + data.Response + "%3C/div%3E%3Ch3%3EData Sources%3C/h3%3E%3Cdiv class=%22content sources%22%3E" + data.DataSources + "%3C/div%3E%3C" +
                "/div%3E"));
        });
    },
    OasisStatusAction: function(id, patientId, episodeId, assessmentType, actionType, pageName) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        acore.closeprintview();
                        U.closeDialog();
                        $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        Agency.RebindCaseManagement();
                        UserInterface.CloseOasisValidationModal();
                        UserInterface.CloseWindow(pageName);
                    } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    acore.closeprintview();
                    U.closeDialog();
                    $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                    UserInterface.CloseOasisValidationModal();
                    UserInterface.CloseWindow(pageName);
                } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    MarkAsExpotred: function(control) {
        var check = false;
        $(control).find("input[name=OasisSelected]").each(function() { if ($(this).is(":checked")) { check = true; } })
        if (check) {
            var fields = $(":input", $(control)).serializeArray();
            U.postUrl('Oasis/MarkExported', fields, function(data) {
                if (data.isSuccessful) {
                    $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
            });
        } else $.jGrowl("Select an assessment to mark as generated.", { theme: 'error', life: 5000 });
    },
    Reopen: function(id, patientId, episodeId, assessmentType, actionType) {
        U.showDialog("#oasisReopenDialog", function() {
            $('#reopenOasisYes').bind('click', function() {
                var reason = $("#Oasis_OasisReopenReason").val();
                U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        Agency.RebindCaseManagement();
                        U.closeDialog();
                    } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
                });
            });
        });
    },
    RebindToExport: function() { if ($('#generateOasisGrid').data('tGrid') != null) $('#generateOasisGrid').data('tGrid').rebind(); },
    RebindExported: function() { if ($('#exportedOasisGrid').data('tGrid') != null) $('#exportedOasisGrid').data('tGrid').rebind(); },
    AddSupply: function(control, episodeId, patientId, eventId, assessmentType, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&assessmentType=" + assessmentType + "&supplyId=" + supplyId +
            "&quantity=" + quantity + "&date=" + date;
        U.postUrl("/Oasis/AddSupply", input, function(result) {
            var gridfilter = $("#" + assessmentType + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + assessmentType + "_GenericSupplyDescription").val('');
                $("#" + assessmentType + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + assessmentType + "_SupplyGrid").data('tGrid');
            if (grid != null) grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId, assessmentType: assessmentType });
        });
    },
    SupplyDescription: function(type) {
        $("#" + type + "_GenericSupplyDescription").keyup(function(event) {
            var message = $("#" + type + "_GenericSupplyDescription").val().length;
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyDescription").val(), limit: 20, type: "Description" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    SupplyCode: function(type) {
        $("#" + type + "_GenericSupplyCode").keyup(function(event) {
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyCode").val(), limit: 20, type: "Code" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    blockText: function(control, id) {
        if ($(control).is(':checked')) $(id).attr("disabled", "disabled").val("");
        else $(id).attr("disabled", "");
    },
    unBlockText: function(id) { $(id).attr("disabled", ""); },
    LoadBlankMasterCalendar: function(id, episodeId, patientId) {
        $("#" + id + "_BlankMasterCalendar").load('Oasis/BlankMasterCalendar', { episodeId: episodeId, patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#" + id + "_BlankMasterCalendar").html('<p>There was an error making the AJAX request</p>');
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            } else if (textStatus == "success") {
                $("#" + id + "_BlankMasterCalendar").removeClass("loading");
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            }
        });
    },
    HideBlankMasterCalendar: function(id) {
        $("#" + id + "_BlankMasterCalendar").empty();
        $("#" + id + "_Show").show();
        $("#" + id + "_Hide").hide();
    },
    showIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    hideIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    showIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted'); else field.hide().addClass('form-omitted'); });
    },
    hideIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted'); else field.show().removeClass('form-omitted'); });
    },
    showIfSelectEquals: function(select, value, field) {
        if (select.val() == value) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        select.bind('change', function() { field.toggleClass('form-omitted').toggle(); });
    },
    noneOfTheAbove: function(checkbox, group) {
        if (checkbox.is(":checked")) group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); });
        group.bind('change', function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked") && checkbox.is(":checked")) checkbox.click(); });
        checkbox.bind('change', function() { group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); }); });
    },
    goals: function(fieldset) {
        fieldset.find("input[type=checkbox]").each(function() {
            if ($(this).parent().find("span.radio").length) {
                if ($(this).is(":not(:checked)")) $(this).parent().find("span.radio :input").attr("disabled", true);
                $(this).change(function() {
                    if ($(this).is(":checked")) $(this).parent().find("span.radio :input").attr("disabled", false);
                    else $(this).parent().find("span.radio :input").attr("disabled", true);
                });
            }
        });
    },
    interventions: function(fieldset) { Oasis.goals(fieldset); },
    gotoQuestion: function(question, window) {
        var tab = 1;
        q = parseInt(question.substr(1, 4));
        if (window == 'startofcare' || window == 'resumptionofcare') {
            if (q >= 1000 && q <= 1030) tab = 2;
            if (q >= 1032 && q <= 1034) tab = 3;
            if (q == 1100) tab = 5;
            if (q >= 1200 && q <= 1230) tab = 6;
            if (q >= 1240 && q <= 1242) tab = 7;
            if (q >= 1300 && q <= 1350) tab = 8;
            if (q >= 1400 && q <= 1410) tab = 9;
            if (q >= 1600 && q <= 1630) tab = 12;
            if (q >= 1700 && q <= 1750) tab = 14;
            if (q >= 1800 && q <= 1910) tab = 15;
            if (q >= 2000 && q <= 2040) tab = 17;
            if (q >= 2100 && q <= 2110) tab = 18;
            if (q >= 2200 && q <= 2250) tab = 19;
        } else if (window == 'recertification') {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 5;
            if (q == 1242) tab = 6;
            if (q >= 1306 && q <= 1350) tab = 7;
            if (q >= 1400 && q <= 1410) tab = 8;
            if (q >= 1610 && q <= 1630) tab = 11;
            if (q >= 1810 && q <= 1910) tab = 14;
            if (q == 2030) tab = 16;
            if (q >= 2200) tab = 17;
        } else if (window == 'followup') {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 3;
            if (q == 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q == 1400) tab = 6;
            if (q >= 1610 && q <= 1630) tab = 7;
            if (q >= 1810 && q <= 1860) tab = 8;
            if (q == 2030) tab = 9;
            if (q == 2200) tab = 10;
        } else if (window == 'transfernotdischarge' || window == 'transferfordischarge') {
            if (q >= 1040 && q <= 1055) tab = 2;
            if (q >= 1500 && q <= 1510) tab = 3;
            if (q >= 2004 && q <= 2015) tab = 4;
            if (q >= 2300 && q <= 2310) tab = 5;
            if ((q >= 903 && q <= 906) || (q >= 2400 && q <= 2440)) tab = 6;
        } else if (windows == 'deathathome') {
            if (q >= 903 && q <= 906) tab = 2;
        } else if (window == 'discharge') {
            if (q >= 1040 && q <= 1050) tab = 2;
            if (q == 1230) tab = 3;
            if (q >= 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q >= 1400 && q <= 1410) tab = 6;
            if (q >= 1500 && q <= 1510) tab = 7;
            if (q >= 1600 && q <= 1620) tab = 8;
            if (q >= 1700 && q <= 1750) tab = 9;
            if (q >= 1800 && q <= 1890) tab = 10;
            if (q >= 2004 && q <= 2030) tab = 11;
            if (q >= 2100 && q <= 2110) tab = 12;
            if (q >= 2300 && q <= 2310) tab = 13;
            if ((q >= 903 && q <= 906) || (q >= 2400 && q <= 2420)) tab = 14;
        }
        if (acore.windows[window].isopen) {
            acore.winfocus(window);
            $('.verttab li:nth-child(' + tab + ') a', acore.getwin(window)).click();
            setTimeout(function() { $('.general:not(:hidden)', acore.getwin(window)).scrollTop(parseInt($(".green:contains('" + question + "')").closest('fieldset').position().top)); }, 1000);
        }
    }
}