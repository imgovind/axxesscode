﻿var Schedule = {
    _scheduleWindow: "",
    _patientId: "",
    _EpisodeId: "",
    _patientName: "",
    _EpisodeStartDate: "",
    _EpisodeEndDate: "",
    _tableId: "NursingScheduleTable",
    _Discipline: "all",
    _DisciplineIndex: 0,
    GetTableId: function() { return Schedule._tableId; },
    SetTableId: function(tableId) { Schedule._tableId = tableId; },
    GetId: function() { return Schedule._patientId; },
    SetId: function(patientId) { Schedule._patientId = patientId; },
    SetName: function(patientName) { Schedule._patientName = patientName; },
    SetPatientRowIndex: function(patientRowIndex) { Schedule._patientRowIndex = patientRowIndex; },
    GetEpisodeId: function() { return Schedule._EpisodeId; },
    SetEpisodeId: function(episodeId) { Schedule._EpisodeId = episodeId; },
    SetStartDate: function(episodeStartDate) { Schedule._EpisodeStartDate = episodeStartDate; },
    SetEndDate: function(episodeEndDate) { Schedule._EpisodeEndDate = episodeEndDate; },
    SetDiscipline: function(discipline) { Schedule._Discipline = discipline; },
    SetDisciplineIndex: function(disciplineIndex) { Schedule._DisciplineIndex = disciplineIndex; },
    InitCenter: function(patient) {
        Lookup.loadMultipleDisciplines("#multipleScheduleTable");
        $('#window_schedulecenter .layout').layout();
        $('#window_schedulecenter .t-grid-content').css({ height: 'auto' });
        Schedule.positionBottom();
        $("select.scheduleStatusDropDown").change(function() {
            $('#scheduleMainResult').html('<div class="ajaxerror">No patients found matching your search criteria!</div>');
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });
        $("select.schedulePaymentDropDown").change(function() {
            $('#scheduleMainResult').html('<div class="ajaxerror">No patients found matching your search criteria!</div>');
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });
        $('#txtSearch_Schedule_Selection').keyup(function() {
            $('#scheduleMainResult').html('<div class="ajaxerror">No patients found matching your search criteria!</div>');
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });
        if (patient != undefined) Schedule._patientId = patient;
    },
    Add: function(date) {
        if ($(".scheduleTables.purgable tbody tr").length > 0) Schedule.CurrentTableAdd(date);
        else Schedule.AllTableAdd(date);
    },
    CurrentTableAdd: function(currentday) {
        if ($('#ScheduleTabStrip').is(':hidden')) Schedule.ShowScheduler();
        if ($('tbody  tr', $("#" + Schedule.GetTableId())).length < 4) Schedule.addTableRow("#" + Schedule.GetTableId(), $.format.date(currentday, 'MM/dd/yyyy'), Schedule._Discipline);
    },
    AllTableAdd: function(date) {
        if (date == undefined) date = "";
        Schedule.addTableRow("#NursingScheduleTable", date, "Nursing");
        //Schedule.addTableRow("#PTScheduleTable", date, "PT");
        //Schedule.addTableRow("#OTScheduleTable", date, "OT");
        //Schedule.addTableRow("#STScheduleTable", date, "ST");
        Schedule.addTableRow("#HHAScheduleTable", date, "HHA");
        //Schedule.addTableRow("#MSWScheduleTable", date, "MSW");
        Schedule.addTableRow("#OrdersScheduleTable", date, "Orders");
        //Schedule.addTableRow("#ClaimScheduleTable", date, "Claim");
        //$("#multipleScheduleTable .Users").append(Lookup._Users);
        Schedule.ShowScheduler();
    },
    RebindActivity: function() {
        var grid = $('#ScheduleActivityGrid').data('tGrid');
        if (grid != null) grid.rebind({ episodeId: Schedule._EpisodeId, patientId: Schedule._patientId, discipline: Schedule._Discipline });
    },
    RebindCalendar: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Schedule/GetEpisode",
            data: "patientId=" + Schedule._patientId + "&episodeId=" + Schedule._EpisodeId + "&discipline=" + Schedule._Discipline,
            success: function(result) {
                var data = eval(result);
                $("#EpisodeStartDate").text((data.StartDateFormatted !== null ? data.StartDateFormatted : "________"));
                $("#EpisodeEndDate").text((data.EndDateFormatted !== null && data.EndDateFormatted !== undefined ? data.EndDateFormatted : "________"));
                if (data.HasNext) {
                    if (data.NextEpisode != null) {
                        $("#nextEpisode").val(data.NextEpisode.Id);
                        $("#nextEpisode").show();
                    } else $("#nextEpisode").hide();
                } else $("#nextEpisode").hide();
                if (data.HasPrevious) {
                    if (data.PreviousEpisode != null) {
                        $("#previousEpisode").val(data.PreviousEpisode.Id);
                        $("#previousEpisode").show();
                    } else $("#previousEpisode").hide();
                } else $("#previousEpisode").hide();
            }
        });
    },
    MissedVisitPopup: function(e, missedVisitId) {
        $('#missedVisitInfo').load('Schedule/MissedVisitInfo', { id: missedVisitId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#missedVisitInfo').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error " +
                    "loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/di" +
                    "v%3E"));
            }
            else if (textStatus == "success") {
                var $dialog = $("#missedVisitInfo");
                $dialog.dialog({
                    width: 500,
                    position: [$(e).offset().left, $(e).offset().top],
                    modal: false,
                    resizable: false,
                    close: function() {
                        $dialog.dialog("destroy");
                        $dialog.hide();
                    }
                }).show();
                $('#ui-dialog-title-missedVisitInfo').html('Missed Visit Info');
            }
        });
    },
    ShowAll: function(patientId) {
        Schedule.SetDiscipline('all');
        if ($("#scheduleTop").html() != null) {
            Schedule.loadCalendar(patientId, Schedule._Discipline);

        } else {
            Schedule.loadCalendarAndActivities(patientId);
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            Schedule.SetId(patientId);
            if ($("#scheduleTop").html() != null) {
                Schedule.loadCalendar(patientId, Schedule._Discipline);

            } else {
                Schedule.loadCalendarAndActivities(patientId);
            }
            Lookup.loadMultipleDisciplines("#multipleScheduleTable");
        }
    },
    loadCalendarAndActivities: function(patientId) {
        $('#scheduleMainResult').load('Schedule/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleMainResult').html("No Patient Episode information found!");
            }
            else if (textStatus == "success") {
                $('#window_schedulecenter .layout').layout({ west__paneSelector: '.layout_left' });
                $('#window_schedulecenter .t-grid-content').css({ height: 'auto' });
            }
        });
    },
    NoPatientBind: function(id) {
        Schedule.loadCalendar(id, Schedule._Discipline);
        Schedule.loadFirstActivity(id);
    },
    addTableRow: function(table, currentday, disp) {
        if ($(table + " tbody tr").length && $(table + " tbody tr:first input.currentDate").val() == "") $(table + " tbody tr:first input.currentDate").val(currentday);
        else {
            var row = "%3Ctr%3E%3Ctd%3E%3Cselect class=%22DisciplineTask requireddropdown%22%3E%3Coption value=%220%22%3E-- Select Discipline --%3C/option%3E%3C/select%3E%3C/td%3" +
                "E%3Ctd%3E%3Cselect class=%22supplies Code Users requireddropdown%22%3E%3Coption value=%2200000000-0000-0000-0000-000000000000%22%3ESelect Employee%3C/option%3E%3" +
                "C/select%3E%3C/td%3E%3Ctd%3E%3Cinput onclick=%22javascript:void(0);%22 type=%22text%22 class=%22currentDate%22 value=%22" + currentday + "%22 re" +
                "adonly=%22readonly%22 /%3E%3C/td%3E%3Ctd%3E%3Ca href=%22javascript:void(0);%22 class=%22action%22 onclick=%22Oasis.DeleteRow($(this));%22%3EDele" +
                "te%3C/a%3E%3C/td%3E%3C/tr%3E";
            $(table).find('tbody').append(unescape(row));
            Lookup.loadDiscipline(table, disp);
            Lookup.loadUsers(table);
            Schedule.positionBottom();
        }
    },
    ScheduleInputFix: function(control, Type, tableName) {
        var submit = true;
        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 1;
        var jsonData = '[';
        $(tableTr).each(function() {
            if ($(this).find("select.DisciplineTask").val() != "0" && $(this).find("select.Users").val() != "00000000-0000-0000-0000-000000000000" && $(this).find("input.currentDate").val() != "") {
                if (len + 1 > i) jsonData += '{"DisciplineTask":"' + $(this).find('.DisciplineTask').val() + '","UserId":"' + $(this).find('.Users').val() + '","EventDate":"' + $(this).find('.currentDate').val() + '","Discipline":"' + $(this).find('.DisciplineTask').find(":selected").attr("data") + '","IsBillable":"' + $(this).find('.DisciplineTask').find(":selected").attr("isbillable") + ' "}';
                if (len > i) jsonData += ',';
                i++
            } else {
                $.jGrowl("Unable to schedule this task. Make sure all required fields are entered in row " + ($(this).parent().children().index($(this)) + 1) + ". ", { theme: 'error', life: 5000 });
                submit = false;
            }
        });
        jsonData += ']';
        control.closest('form').find('input[name= ' + Type + '_Schedule][type=hidden]').val(jsonData.toString());
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        if (submit) Schedule.FormSubmit(control);
    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val()
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    control.closest("table").find('tbody').empty();
                    Schedule.CurrentTableAdd('');
                    Schedule.CloseNewEvent(control);
                    $.jGrowl("Task(s) successfully added to the patient's episode.", { theme: 'success', life: 5000 });
                } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandlerHelperMultiple: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var tabstrip = $("#ScheduleTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[Schedule._DisciplineIndex];
                    tabstrip.select(item);
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Schedule.CloseNewEvent(control);
                    $.jGrowl("Task(s) successfully added to the patient's episode.", { theme: 'success', life: 5000 });
                } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelper(form, control);
    },
    FormSubmitMultiple: function(control) {
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        control.closest('form').find('input[name=Discipline][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("data"));
        control.closest('form').find('input[name=IsBillable][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("IsBillable"));

        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelperMultiple(form, control);
    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            },
            error: function() { $.jGrowl("Error: Unable to Assign Task", { theme: 'error', life: 5000 }) }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    SubmitReassign: function(control, patientId, episodeId) {
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    },
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    },
    OnSelect: function(e) {
        var content = $(e.contentElement);
        var tableControl = $('table', content);
        if ($(tableControl).attr('id') == "multipleScheduleTable") return true;
        Schedule.SetDisciplineIndex($(e.item).index());
        Schedule.SetTableId($(tableControl).attr('id'));
        Schedule.SetDiscipline($(tableControl).attr('data'));
        if (Schedule._Discipline == "Multiple") return;
        else {
            var patientId = $("#SchedulePatientID").val();
            var episodeId = $("#ScheduleEpisodeID").val()
            Schedule.loadCalendarNavigation(episodeId, patientId);
            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
            var name = "#" + Schedule.GetTableId();
            var $table = $(name);
        }
    },
    Delete: function(patientId, episodeId, eventId, employeeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId;
            U.postUrl("/Schedule/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Patient.CustomDateRange();
                }
            });
        }
    },
    ReAssign: function(control, episodeId, patientId, id, oldEmployeeId) {
        control.hide();
        control.parent().append(unescape("%3Cform action=%22/Schedule/ReAssign%22 method=%22post%22%3E%3Cinput name=%22episodeId%22 type=%22hidden%22 value=%22" +
            episodeId + "%22 /%3E%3Cinput name=%22oldUserId%22 type=%22hidden%22 value=%22" + oldEmployeeId + "%22 /%3E%3Cinput name=%22patientId%22 type=%22hidd" +
            "en%22 value=%22" + patientId + "%22 /%3E%3Cinput name=%22eventId%22 type=%22hidden%22 value=%22" + id + "%22 /%3E%3Cselect class=%22Users%22 name=%2" +
            "2userId%22 style=%22width:130px;%22 class=%22Users%22%3E%3C/select%3E%3Cinput type=%22button%22 value=%22Save%22 onclick=%22Schedule.SubmitReassign(" +
            "$(this),'" + patientId + "','" + episodeId + "');%22/%3E %3Cinput type=%22button%22 value=%22Cancel%22 onclick=%22Schedule.CancelReassign($(this));%" +
            "22 /%3E%3C/form%3E"));
        Lookup.loadUsers("#ScheduleActivityGrid");
    },
    ReOpen: function(episodeId, patientId, eventId) {
        var input = "patientId=" + patientId + "&eventId=" + eventId + "&episodeId=" + episodeId;
        U.postUrl("/Schedule/Reopen", input, function(result) {
            if (result.isSuccessful) {
                if ($("#scheduleTop").html() != null) {
                    Schedule.loadCalendar(patientId, Schedule._Discipline);

                } else {
                    Schedule.loadCalendarAndActivities(patientId);
                }
                Lookup.loadMultipleDisciplines("#multipleScheduleTable");
                Patient.Rebind();
            }
        });
    },
    CancelReassign: function(control) {
        var reassignLink = control.parent().parent().find('a.reassign').show();
        control.parent().remove();
    },
    CloseNewEvent: function(control) {
        $('.scheduleTables.purgable').each(function() { $(this).find('tbody').empty(); })
        Schedule.ShowScheduler();
    },
    NavigateEpisode: function(episodeId, patientId) {
        Schedule.SetEpisodeId(episodeId);
        $("#nursingTab").click();
        Schedule.loadCalendarNavigation(episodeId, patientId);
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
        Schedule.positionBottom();
    },
    loadCalendarNavigation: function(EpisodeId, PatientId) {
        $('#scheduleTop').load('Schedule/CalendarNav', { patientId: PatientId, episodeId: EpisodeId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleTop').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error loading t" +
                    "his window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E"));
            }
            Schedule.positionBottom();
        });
    },
    loadMasterCalendar: function(_patientId, _EpisodeId) {
        $('#masterCalendarResult').load('Schedule/PartialGrid', { patientId: _patientId, episodeId: _EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html('<p>There was an error making the request</p>');
            }
            else if (textStatus == "success") {
                JQD.open_window('#masterCalendar');
                $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                    $(this).css({
                        position: 'relative',
                        left: -100,
                        top: 0
                    });
                });
            }
        });
    },
    loadMasterCalendarNavigation: function(EpisodeId, PatientId) {
        $('#masterCalendarResult').load('Schedule/PartialGrid', { patientId: PatientId, episodeId: EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error " +
                    "loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/di" +
                    "v%3E"));
            }
            else if (textStatus == "success") {
                JQD.open_window('#masterCalendar');
            }
        });
    },
    loadCalendar: function(patientId, discipline) {
        $('#scheduleTop').load('Schedule/Calendar', { patientId: patientId, discipline: discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleMainResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error loading t" +
                    "his window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E"));
            }
            else if (textStatus == "success") {
                Schedule.loadActivity(patientId);
            }
        });
    },
    loadActivity: function(patientId) {
        $('#scheduleBottomPanel').load('Schedule/ActivityFirstTime', { patientId: patientId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error l" +
                    "oading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div" +
                    "%3E"));
            }
            else if (textStatus == "success") {
            }
        });
    },
    loadFirstActivity: function(PatientId) {
        $('#scheduleBottomPanel').load('Schedule/ActivityFirstTime', { patientId: PatientId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error l" +
                    "oading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div" +
                    "%3E"));
            }
            else if (textStatus == "success") {

            }
        });
    },
    InitEpisode: function() {
        $("#Edit_Episode_StartDate").data("tDatePicker").hidePopup();
        $("#Edit_Episode_EndDate").data("tDatePicker").hidePopup();
        $("#editEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            U.closeDialog();
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewEpisode: function() {
        $("#New_Episode_StartDate").data("tDatePicker").hidePopup();
        $("#New_Episode_EndDate").data("tDatePicker").hidePopup();
        $("#newEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            U.closeDialog();
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetEpisode: function(episodeId, patientId) {
        acore.open("editepisode", 'Schedule/EditEpisode', function() {
            Schedule.InitEpisode();
        }, { episodeId: episodeId, patientId: patientId });
    },
    InitTaskDetails: function() {
        $("#scheduleDetailsForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Task details updated successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('scheduledetails');
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Task details updated successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('scheduledetails');
                            Patient.Rebind();
                            Schedule.Rebind();
                        }
                        else {
                            $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetTaskDetails: function(episodeId, patientId, eventId) {
        acore.open("scheduledetails", 'Schedule/GetDetails', function() {
            Schedule.InitTaskDetails();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    EventMouseOver: function(control) {
        var currentControl = $('.events', $(control));
        currentControl.show();
    },
    EventMouseOut: function(control) {
        $('.events', $(control)).hide();
    },
    loadMasterSchedulingCenter: function() {
        $('#scheduleLandingResult').load('Schedule/Center', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleLandingResult').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error" +
                    " loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/d" +
                    "iv%3E"));
                JQD.open_window('#schedule_window');
            }
            else if (textStatus == "success") {
                JQD.open_window('#schedule_window');
                Schedule.Init();
                $("#ScheduleTabStrip").tTabStrip();
            }
        });
    },
    positionBottom: function() {
        $('#window_schedulecenter .layout_main .bottom').css({ top: (parseInt($('#window_schedulecenter .layout_main .top').attr('scrollHeight')).toString() + "px") });
    },
    loadHHASVisit: function(episodeId, patientId, eventId) {
        acore.open("hhasVisit", 'Schedule/HHASVisit', function() {
            Schedule.hhaInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadLVNSVisit: function(episodeId, patientId, eventId) {

        acore.open("lvnsVisit", 'Schedule/LVNSVisit', function() {
            Schedule.lvnInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadDischargeSummary: function(episodeId, patientId, eventId) {
        acore.open("dischargeSummary", 'Schedule/DischargeSummary', function() {
            Schedule.dischargeSummaryInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadHHAVisitNote: function(episodeId, patientId, eventId) {
        acore.open("hhAideVisit", 'Schedule/HHAVisitNote', function() {
            Schedule.hhaVisitNoteInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadHHACarePlan: function(episodeId, patientId, eventId) {
        acore.open("hhaCarePlan", 'Schedule/HHACarePlan', function() {
            Schedule.hhaCarePlanInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSixtyDaySummary: function(episodeId, patientId, eventId) {
        acore.open("sixtyDaySummary", 'Schedule/SixtyDaySummary', function() {
            Schedule.sixtyDaySummaryInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadTransferSummary: function(episodeId, patientId, eventId) {
        acore.open("transferSummary", 'Schedule/TransferSummary', function() {
            Schedule.transferSummaryInit();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadSnVisit: function(episodeId, patientId, eventId) {
        acore.open("snVisit", 'Schedule/SNVisit', function() { Schedule.snVisitInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadWoundCare: function(episodeId, patientId, eventId) {
        acore.open("woundcare", 'Schedule/WoundCare', function() { Schedule.WoundCareInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    loadNoteSupplyWorkSheet: function(episodeId, patientId, eventId) {
        acore.open("notessupplyworksheet", 'Schedule/SupplyWorksheet', function() { }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    hhaInit: function() {
        $("#HHASVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('hhasVisit');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    lvnInit: function() {
        $("#LVNSVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('lvnsVisit');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    dischargeSummaryInit: function() {
        $("#dischargeSummaryForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('dischargeSummary');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#DischargeSummary_NotificationDate").change(function() {
            if ($("#DischargeSummary_NotificationDate").val() == "3") {
                $("#DischargeSummary_NotificationDateOther").show();
            }
            else {
                $("#DischargeSummary_NotificationDateOther").hide();
            }
        });
        if ($("#DischargeSummary_NotificationDate").val() == "3") {
            $("#DischargeSummary_NotificationDateOther").show();
        }
        else {
            $("#DischargeSummary_NotificationDateOther").hide();
        }
        $("#DischargeSummary_ReasonForDC").change(function() {
            if ($("#DischargeSummary_ReasonForDC").val() == "9") {
                $("#DischargeSummary_ReasonForDCOther").show();
            }
            else {
                $("#DischargeSummary_ReasonForDCOther").hide();
            }
        });
        if ($("#DischargeSummary_ReasonForDC").val() == "9") {
            $("#DischargeSummary_ReasonForDCOther").show();
        }
        else {
            $("#DischargeSummary_ReasonForDCOther").hide();
        }
    },
    hhaVisitNoteInit: function() {
        $("#HHAideVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('hhAideVisit');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $("#HHAVisitNote_HomeboundStatus").change(function() {
            if ($("#HHAideVisit_HomeboundStatus").val() == "8") {
                $("#HHAideVisit_HomeboundStatusOther").show();
            }
            else {
                $("#HHAideVisit_HomeboundStatusOther").hide();
            }
        });
        if ($("#HHAideVisit_HomeboundStatus").val() == "8") {
            $("#HHAideVisit_HomeboundStatusOther").show();
        }
        else {
            $("#HHAideVisit_HomeboundStatusOther").hide();
        }
    },
    hhaCarePlanInit: function() {
        $("#HHACarePlanForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                            if (scheduleActivityGrid != null) {
                                scheduleActivityGrid.rebind({ episodeId: $("#HHACarePlan_EpisodeId").val(), patientId: $("#HHACarePlan_PatientId").val(), discipline: Schedule._Discipline });
                            }
                            UserInterface.CloseWindow('hhaCarePlan');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    sixtyDaySummaryInit: function() {
        $("#SixtyDaySummaryForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('sixtyDaySummary');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $("#SixtyDaySummary_HomeboundStatus").change(function() {
            if ($("#SixtyDaySummary_HomeboundStatus").val() == "8") {
                $("#SixtyDaySummary_HomeboundStatusOther").show();
            }
            else {
                $("#SixtyDaySummary_HomeboundStatusOther").hide();
            }
        });
        if ($("#SixtyDaySummary_HomeboundStatus").val() == "8") {
            $("#SixtyDaySummary_HomeboundStatusOther").show();
        }
        else {
            $("#SixtyDaySummary_HomeboundStatusOther").hide();
        }
    },
    transferSummaryInit: function() {
        $("#TransferSummaryForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('transferSummary');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $("#TransferSummary_HomeboundStatus").change(function() {
            if ($("#TransferSummary_HomeboundStatus").val() == "8") {
                $("#TransferSummary_HomeboundStatusOther").show();
            }
            else {
                $("#TransferSummary_HomeboundStatusOther").hide();
            }
        });
        if ($("#TransferSummary_HomeboundStatus").val() == "8") {
            $("#TransferSummary_HomeboundStatusOther").show();
        }
        else {
            $("#TransferSummary_HomeboundStatusOther").hide();
        }
    },
    ProcessNote: function(button, episodeId, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl("/Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        acore.closeprintview();
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                        User.RebindScheduleList();
                    } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl("/Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    acore.closeprintview();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    User.RebindScheduleList();
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    snVisitInit: function() {
        $("#SNVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('snVisit');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    tooltip: function() {
        $("a.tooltip").each(function() {
            if ($(this).hasClass("blue_note")) var c = "blue_note";
            if ($(this).hasClass("red_note")) var c = "red_note";
            if ($(this).attr("tooltip")) {
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    activityRowDataBound: function(e) {
        Schedule.tooltip();

        var dataItem = e.dataItem;
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
        }
    },
    WoundCareInit: function() {
        $("#WoundCareForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Wound care saved successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('woundcare');
                            Schedule.Rebind();
                            User.RebindScheduleList();
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Wound care saved successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('woundcare');
                            User.RebindScheduleList();
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        $("input.WoundType").each(function() {
            var data = ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"];
            $(this).bind("focus", function() {
                $(this).autocomplete(data);
            });
        });
        $("input.DeviceType").each(function() {
            var dataDevice = ["J.P.", "Wound Vac", "None"];
            $(this).bind("focus", function() {
                $(this).autocomplete(dataDevice);
            });
        });
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#WoundCare_EpisodeId").val() + "&patientId=" + $("#WoundCare_PatientId").val() + "&eventId=" + $("#WoundCare_EventId").val() + "&name=" + name + "&assetId=" + assetId;
            U.postUrl("/Schedule/DeleteWoundCareAsset", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"float_left uploadWidth\" />");
                }
            });
        }
    },
    Rebind: function() {
        var grid = $('#SchedulePatientSelectionGrid').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    AddSupply: function(control, episodeId, patientId, eventId, type, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&supplyId=" + supplyId + "&quantity=" + quantity + "&date=" + date;
        U.postUrl("/Schedule/AddNoteSupply", input, function(result) {
            var gridfilter = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + type + "_GenericSupplyDescription").val('');
                $("#" + type + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + type + "_SupplyGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId });
            }
        });
    },
    ShowScheduler: function() {
        if ($('#ScheduleTabStrip').is(':visible')) {
            $('#schedule_collapsed').find(".show_scheduler").html("Show Scheduler");
            $('#ScheduleTabStrip').hide();
            Schedule.showAll();
        } else {
            Schedule.SetDiscipline('Nursing');
            Schedule.RebindCalendar();
            Schedule.RebindActivity();
            if ($(".scheduleTables.purgable tbody tr").length == 0) Schedule.AllTableAdd();
            else {
                $('#ScheduleTabStrip').show();
                $('#schedule_collapsed').find(".show_scheduler").html("Hide Scheduler");
            }
        }
        Schedule.positionBottom();
    },
    newEpisodeStartDateOnChange: function(e) {
        var dateEndControl = $("#New_Episode_EndDate").data("tDatePicker");
        var dateStartControl = $("#New_Episode_StartDate").data("tDatePicker");
        if (dateEndControl != null || dateEndControl != undefined) {
            var depart = $.telerik.formatString('{0:MM/dd/yyyy}', dateStartControl.value());
            var departDate = $.datepicker.parseDate('mm/dd/yy', depart);
            departDate.setDate(departDate.getDate() + 59);
            dateEndControl.minValue = dateStartControl.value();
            dateEndControl.maxValue = departDate;
            dateEndControl.value(departDate)
        }
    },
    SuppliesLoad: function() {
        $("#SkilledNurseVisit_SupplyGrid .t-grid-toolbar").html(unescape("%3Cdiv class=%22align_center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3ECurrent Supplies%3C/div%3E%3C/div%3E"));
    },
    showAll: function() {
        Schedule.SetDiscipline('all');
        Schedule.RebindCalendar();
        Schedule.RebindActivity();
    }
}

