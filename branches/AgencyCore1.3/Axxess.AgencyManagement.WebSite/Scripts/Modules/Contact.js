﻿var Contact = {
    Delete: function(Id) { U.DeleteTemplate("Contact", Id, function() { Contact.RebindList(); }) },
    InitEdit: function() { U.InitTemplate("edit", "Contact", function() { Contact.RebindList(); }) },
    InitNew: function() {
        $("#New_Contact_Other_Div").hide();
        $("#New_Contact_Type").change(function() {
            if ($('#New_Contact_Type').val() == "Other") $("#New_Contact_Other_Div").show();
            else $("#New_Contact_Other_Div").hide();
        });
        U.InitTemplate("new", "Contact", function() { Contact.RebindList(); });
    },
    RebindList: function() { U.rebindTGrid($('#List_Contact')); }
}