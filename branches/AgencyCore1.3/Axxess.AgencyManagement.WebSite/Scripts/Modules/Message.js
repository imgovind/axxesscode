﻿var Message = {
    _tokenlist: null,
    defaultMessage: "",
    AddRemoveRecipient: function(input) {
        if ($('#' + input).attr('checked'))
            $.data($('#' + input).get(0), "tokenbox", { "token": Message._tokenlist.insertToken($('#' + input).attr('value'), $('#' + input).attr('title')) });
        else Message._tokenlist.removeToken($.data($('#' + input).get(0), "tokenbox").token);
        Message.positionBottom();
    },
    Cancel: function() { UserInterface.CloseWindow('newmessage'); },
    Delete: function(Id, BypassConfirm) { U.DeleteTemplate("Message", Id, function() { Message.Rebind(); }, BypassConfirm); },
    Init: function(messageId) {
        if (messageId != undefined) Message.defaultMessage = messageId;
        $("#MessageReplyButton").click(function() {
            acore.open("newmessage", 'Message/New', function() { Message.InitNew('reply', $("#messageId").val()); });
        });
        $("#MessageForwardButton").click(function() {
            acore.open("newmessage", 'Message/New', function() { Message.InitNew('forward', $("#messageId").val()); });
        });
        $("#MessageDeleteButton").click(function() { Message.Delete($("#messageId").val()); });
        $("#inboxType").change(function() {
            Message.Rebind();
            $("#List_Messages .t-grid-header tbody tr:has(th):first a.t-link").text($("#inboxType").find(":selected").text());
        });
    },
    initCKE: function() {
        CKEDITOR.replace('New_Message_Body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    },
    InitNew: function(type, id) {
        $('#New_Message_CheckallRecipients').change(function() {
            $('input[name="Recipients"]').each(function() {
                if ($('#New_Message_CheckallRecipients').attr('checked') != $(this).attr('checked')) {
                    $(this).attr('checked', $('#New_Message_CheckallRecipients').attr('checked'));
                    Message.AddRemoveRecipient($(this).attr('id'));
                }
            });
        });
        U.InitTemplate('new', 'Message', function() { });
        Message._tokenlist = $.fn.tokenInput("#New_Message_Recipents", "/Message/Recipients", {
            classes: { tokenList: "token-input-list-facebook", token: "token-input-token-facebook", tokenDelete: "token-input-delete-token-facebook",
                selectedToken: "token-input-selected-token-facebook", highlightedToken: "token-input-highlighted-token-facebook",
                dropdown: "token-input-dropdown-facebook", dropdownItem: "token-input-dropdown-item-facebook", dropdownItem2: "token-input-dropdown-item2-facebook",
                selectedDropdownItem: "token-input-selected-dropdown-item-facebook", inputToken: "token-input-input-token-facebook" }
        });
        Message.initCKE();
        if (type == "reply" || type == "forward") {
            U.postUrl("/Message/Get", "id=" + id, function(message) {
                if (type == "reply") {
                    if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
                    if ($('input[name="Recipients"][value=' + message.FromId + ']').length)
                        Message.AddRemoveRecipient($('input[name="Recipients"][value=' + message.FromId + ']').attr('checked', true).attr('id'));
                } else if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
                $("#New_Message_Subject").val(message.Subject);
                $("#New_Message_PatientId").val(message.PatientId);
                CKEDITOR.instances['New_Message_Body'].setData(message.ReplyForwardBody);
            });
        }
    },
    initWidget: function() {
        U.postUrl("/Message/List", "inboxType=inbox&page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Total > 0) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#messagesWidgetContent').append(unescape("%3Cdiv" + (data.Data[i].MarkAsRead ? "" : " clas" +
                    "s=%22strong%22") + "%3E%3Cinput type=%22checkbox%22 class=%22radio float_left%22 value=%22" + data.Data[i].Id + "%22 /%3E%3Ca href=%22javasc" +
                    "ript:void(0);%22 onclick=%22$(this).parent().removeClass('strong'); if($('#window_messageinbox').length == 0) { acore.open('messageinbox', '" +
                    "Message/Inbox', function() { Message.Init('" + data.Data[i].Id + "'); }); } else { acore.winfocus('messageinbox'); $('#" + data.Data[i].Id    +
                    "').closest('tr').click(); }%22 onmouseover=%22$(this).addClass('t-state-hover');%22 onmouseout=%22$(this).removeClass('t-state-hover');%22 c" +
                    "lass=%22message%22%3E" + data.Data[i].FromName + " &ndash; " + data.Data[i].Subject + "%3C/a%3E%3C/div%3E"));
            } else $('#messagesWidgetContent').append(unescape("%3Ch1 class=%22align_center%22%3ENo Messages found.%3C/h1%3E"))
                .parent().find('.widget-more').remove();
        });
        $(".mailcontrols select").bind("change", function() {
            if ($(this).find("option:first").is(":not(:selected)")) {
                if ($("#messagesWidgetContent input:checked").length) {
                    if ($(this).find(":selected").html() == "Archive") { U.growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Delete") {
                        if (confirm("Are you sure you want to delete all selected messages?")) {
                            $("#messagesWidgetContent input:checked").each(function() {
                                Message.Delete($(this).val(), true);
                                $(this).parent().remove();
                            });
                        }
                    }
                    if ($(this).find(":selected").html() == "Mark as Read") { U.growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Mark as Unread") { U.growl("This feature coming soon."); }
                } else {
                    $.jGrowl("Error: No messages selected to " + $(this).find(":selected").html(), { theme: 'error', life: 5000 });
                    $(this).find("option:first").attr("selected", true);
                }
            }
        });
    },
    OnDataBound: function() {
        if (Message.defaultMessage != "" && $("#" + Message.defaultMessage).length) $("#" + Message.defaultMessage).closest("tr").click();
        else $("#List_Messages .t-grid-content tbody tr:has(td):first").click();
        Message.defaultMessage = "";
    },
    OnRowSelected: function(e) {
        if (e.row.cells[1] != undefined) {
            var messageId = e.row.cells[1].innerHTML;
            U.postUrl("/Message/Get", "id=" + messageId, function(message) {
                $("#messageBody").html(message.Body);
                $("#messageSender").html(message.FromName);
                $("#messageSubject").html(message.Subject);
                $("#messageDate").html(message.MessageDate);
                $("#messageId").val(message.Id);
            });
            $("#" + messageId).removeClass('false').addClass('true');
        }
    },
    positionBottom: function() { $('#newMessageBodyDiv').css({ top: (parseInt($('.token-input-list-facebook').attr('clientHeight') + 100).toString() + "px") }); },
    Rebind: function() { U.rebindTGrid($('#List_Messages'), { inboxType: $("#inboxType").val() }); }
}