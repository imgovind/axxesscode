﻿var Billing = {
    tab: undefined,
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    var tabstrip = $("#FinalTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[index];
                    tabstrip.select(item);
                    var $link = $(item).find('.t-link');
                    var contentUrl = $link.data('ContentUrl');
                    if (contentUrl != null) Billing.LoadContent(contentUrl, $("#FinalTabStrip").find("div.t-content.t-state-active"));
                } else U.growl(result.errorMessage, 'error');
            },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    finalTabStripOnSelect: function(e) { Billing.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement); },
    InitCenter: function() { },
    InitRap: function() {
        $("#rapVerification").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('rap');
                            Billing.ReLoadUpProcessedRap();
                            Billing.ReLoadUpProcessedFinal();
                        }
                        else {
                            $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RapVerify: function() {

    },
    ReLoadUpProcessedRap: function() {
        $("#Billing_CenterContentRap").addClass("loading");
        $('#Billing_CenterContentRap').load('Billing/RapGrid', null, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#Billing_CenterContentRap').html('<p>There was an error loading the content.</p>');
            }
            else if (textStatus == "success") {
                $("#Billing_CenterContentRap").removeClass("loading");
            }
        });
    },
    ReLoadUpProcessedFinal: function() {
        $("#Billing_CenterContentFinal").addClass("loading");
        $('#Billing_CenterContentFinal').load('Billing/FinalGrid', null, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#Billing_CenterContentFinal').html('<p>There was an error loading the content.</p>');
            }
            else if (textStatus == "success") {
                $("#Billing_CenterContentFinal").removeClass("loading");
            }
        });
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl) {
            $(contentElement).html("&nbsp;").addClass("loading");
            $.get(contentUrl, function(data) { $(contentElement).removeClass("loading").html(data); });
        }
    },
    loadGenerate: function(control) { acore.open("claimSummary", 'Billing/ClaimSummary', function() { }, $(":input", $(control)).serializeArray()); },
    Navigate: function(index, id) {
        var form = $(id);
        form.validate();
        Billing.BillingHandler(index, form);
    },
    NavigateBack: function(index) { var tabstrip = $("#FinalTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); },
    NoPatientBind: function(id) { Billing.RebindActivity(id); },
    OnPatientRowSelected: function(e) { Billing.RebindActivity(e.row.cells[2].innerHTML); },
    RebindActivity: function(id) { $('#BillingHistoryActivityGrid').data('tGrid').rebind({ patientId: id }); },
    UpdateStatus: function(control) { U.postUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal(); Billing.ReLoadUpProcessedRap(); UserInterface.CloseWindow('claimSummary'); $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, null); },
    FinalComplete: function(id) { U.postUrl('Billing/FinalComplete', { id: id }, function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal(); Billing.ReLoadUpProcessedRap(); UserInterface.CloseWindow('final'); $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, null); },
    GenerateAllCompleted: function(control) {
    $("input[name=RapSelected]", $(control)).each(function() { $(this).attr("checked", true) });
    $("input[name=FinalSelected]", $(control)).each(function() { $(this).attr("checked", true) });
     Billing.loadGenerate(control);
    }
}