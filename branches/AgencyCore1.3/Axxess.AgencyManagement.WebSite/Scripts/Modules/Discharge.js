﻿var Discharge = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("discharge", "discharge", control.closest("form"), control, action); },
    loadDischarge: function(id, patientId, assessmentType) { acore.open("discharge", 'Oasis/Discharge', function() { Oasis.InitTabs("Discharge", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}