﻿var Report = {
    Init: function() {
        $("#reportingTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#reportingTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');

        $("#reportingTabs").find('a').each(function() {
            var tab = $(this);
            tab.bind("click", function(event) {
                $(tab.attr('href')).empty();
                $(tab.attr('href')).addClass("loading");
                $(tab.attr('href')).load(tab.attr('title'), function(responseText, textStatus, XMLHttpRequest) {
                    if (textStatus == 'error') {
                        $(tab.attr('href')).html('<p>There was an error making the AJAX request</p>');
                    }
                    else if (textStatus == "success") {
                        $(tab.attr('href')).removeClass("loading");
                    }
                });
            });
        });
    },
    Show: function(tabIndex, container, url) {
        var $tabs = $("#reportingTabs.tabs").tabs();
        $tabs.tabs('select', tabIndex);
        $(container).empty();
        $(container).load(url, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $(container).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $(container).removeClass("loading");
            }
        });
    },
    RebindPatientRoster: function() {
        var grid = $('#ReportPatientRosterGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ StatusId: $("#Report_Patient_Roster_Status").val(), AddressBranchCode: $("select.AddressBranchCode").val() });
        }
    },
    RebindPatientEmergencyListing: function() {
        var grid = $('#Report_Patient_EC_Grid').data('tGrid');
        if (grid != null) {
            grid.rebind({ StatusId: $("#Report_Patient_EC_Status").val(), AddressBranchCode: $("select.AddressBranchCode").val() });
        }
    },
    RebindPatientBirthdayListing: function() {
        var grid = $('#Report_Patient_BL_Grid').data('tGrid');
        if (grid != null) {
            grid.rebind({ AddressBranchCode: $("select.AddressBranchCode").val() });
        }
    },
    RebindPatientAddressListing: function() {
        var grid = $('#Report_Patient_AL_Grid').data('tGrid');
        if (grid != null) {
            grid.rebind({ AddressBranchCode: $("select.AddressBranchCode").val() });
        }
    },
    RebindPatientPhysician: function() {
        var grid = $('#Report_Patient_Physician_Grid').data('tGrid');
        if (grid != null) {
            grid.rebind({ AgencyPhysicianId: $("select.Physicians").val() });
        }
    },
    RebindClinicalOrders: function() {
        var grid = $('#Report_Patient_Orders_Grid').data('tGrid');
        if (grid != null) {
            grid.rebind({ StatusId: $("#Report_Patient_Orders_Status").val() });
        }
    }
}


