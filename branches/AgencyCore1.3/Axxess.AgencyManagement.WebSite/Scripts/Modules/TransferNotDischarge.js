﻿var TransferNotDischarge = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("TransferNotDischarge", "transfernotdischarge", control.closest("form"), control, action); },
    loadTransferNotDischarge: function(id, patientId, assessmentType) { acore.open("transfernotdischarge", 'Oasis/TransferNotDischarge', function() { Oasis.InitTabs("TransferNotDischarge", id, patientId, assessmentType) }, { Id: id, PatientId: patientId, AssessmentType: assessmentType }); }
}