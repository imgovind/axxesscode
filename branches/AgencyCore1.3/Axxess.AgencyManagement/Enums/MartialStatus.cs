﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum MartialStatus
    {
        [Description("Married")]
        Married ,
        [Description("Divorce")]
        Divorce ,
        [Description("Widowed")]
        Widowed ,
        [Description("Single")]
        Single ,
        [Description("Unknown")]
        Unknown 
    }
}
