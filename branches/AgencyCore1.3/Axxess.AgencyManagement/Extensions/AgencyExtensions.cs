﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class AgencyExtensions
    {
        public static bool HasTrialPeriodExpired(this Agency agency)
        {
            var expirationDate = agency.Created.Add(TimeSpan.FromDays(agency.TrialPeriod));
            if (!agency.IsAgreementSigned && DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }
    }
}
