﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class ScheduleEventExtensions
    {
        public static bool IsAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("oasis") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsStartofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("startofcare") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsRecertificationAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("recertification") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsResumptionofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("resumptionofcare") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsOpen(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
                if (status != ScheduleStatus.NoteCompleted || status != ScheduleStatus.OasisCompletedExportReady || status != ScheduleStatus.OasisExported || status != ScheduleStatus.OrderReturnedWPhysicianSignature || status != ScheduleStatus.ClaimAccepted)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOrderAndStatus(this ScheduleEvent scheduleEvent, int statusId)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty() && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), scheduleEvent.Discipline);
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
                if (discipline == Disciplines.Orders && statusId == (int)status)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompleteRecertAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);

                if (disciplineTask.ToLowerInvariant().Contains("recert") && (status == ScheduleStatus.OasisCompletedPendingReview || status == ScheduleStatus.OasisCompletedExportReady)) 
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ContainsRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        {
            var result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                    var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                    if (disciplineTask.ToLowerInvariant().Contains("recert"))
                    {
                        result = true;
                        return;
                    }
                });
            }
            return result;
        }

        public static ScheduleEvent GetRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        {
            var recert = new ScheduleEvent();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                    var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                    if (disciplineTask.ToLowerInvariant().Contains("recert"))
                    {
                        recert = scheduleEvent;
                        return;
                    }
                });
            }
            return recert;
        }

        public static bool IsCompleted(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(scheduleEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status) : ScheduleStatus.NoStatus;

                if (status == ScheduleStatus.OrderReturnedWPhysicianSignature
                    || status == ScheduleStatus.OrderSentToPhysician
                    || status == ScheduleStatus.NoteCompleted
                    || status == ScheduleStatus.NoteMissedVisit
                    || status == ScheduleStatus.NoteSubmittedWithSignature
                    || status == ScheduleStatus.OasisCompletedExportReady
                    || status == ScheduleStatus.OasisCompletedPendingReview
                    || status == ScheduleStatus.OasisExported)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompletelyFinished(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(scheduleEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status) : ScheduleStatus.NoStatus;

                if (status == ScheduleStatus.NoteCompleted
                    || status == ScheduleStatus.OasisCompletedExportReady
                    || status == ScheduleStatus.OasisExported)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompleted(this UserEvent userEvent)
        {
            if (userEvent != null && userEvent.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = userEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(userEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), userEvent.Status) : ScheduleStatus.NoStatus;

                if (status == ScheduleStatus.OrderReturnedWPhysicianSignature
                    || status == ScheduleStatus.OrderSentToPhysician
                    || status == ScheduleStatus.NoteCompleted
                    || status == ScheduleStatus.NoteMissedVisit
                    || status == ScheduleStatus.NoteSubmittedWithSignature
                    || status == ScheduleStatus.OasisCompletedExportReady
                    || status == ScheduleStatus.OasisCompletedPendingReview
                    || status == ScheduleStatus.OasisExported)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
