﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;


    using SubSonic.Repository;

    using Domain;
    using Axxess.AgencyManagement.Enums;

    public class ReferralRepository : IReferralRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ReferralRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IReferralRepository Members

        public bool Add(Referral referral)
        {
            var result = false;
            if (referral != null)
            {
                referral.Id = Guid.NewGuid();
                if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count >= 2)
                {
                    referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                }
                if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                {
                    referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                {
                    referral.DME = referral.DMECollection.ToArray().AddColons();
                }
                if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                {
                    var physicianList = new List<Physician>();

                    int i = 0;
                    foreach (var guid in referral.AgencyPhysicians)
                    {
                        if (!guid.IsEmpty())
                        {
                            physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                            i++;
                        }
                    }
                    referral.Physicians = physicianList.ToXml();
                }
                referral.Status = (byte)ReferralStatus.Pending;
                referral.Created = DateTime.Now;
                referral.Modified = DateTime.Now;
                database.Add<Referral>(referral);
                result = true;
            }

            return result;
        }

        public IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status)
        {
            return database.Find<Referral>(r => r.AgencyId == agencyId && r.Status == (int)status && r.IsDeprecated == false);
        }

        public Referral Get(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id && r.IsDeprecated == false);
        }

        public void SetStatus(Guid agencyId, Guid id, ReferralStatus status)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var referral = Get(agencyId, id);

            if (referral != null)
            {
                referral.Status = (int)status;
                database.Update<Referral>(referral);
            }
        }

        public bool Edit(Referral referral)
        {
            bool result = false;
            if (referral != null)
            {
                if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                {
                    referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                {
                    referral.DME = referral.DMECollection.ToArray().AddColons();
                }
                if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count > 0)
                {
                    referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                }

                var editReferral = Get(referral.AgencyId, referral.Id);
                if (editReferral != null)
                {
                    editReferral.AdmissionSource = referral.AdmissionSource;
                    editReferral.ReferrerPhysician = referral.ReferrerPhysician;
                    editReferral.ReferralDate = referral.ReferralDate;
                    editReferral.OtherReferralSource = referral.OtherReferralSource;
                    editReferral.InternalReferral = referral.InternalReferral;
                    editReferral.FirstName = referral.FirstName;
                    editReferral.LastName = referral.LastName;
                    editReferral.MedicaidNumber = referral.MedicaidNumber;
                    editReferral.MedicareNumber = referral.MedicareNumber;
                    editReferral.SSN = referral.SSN;
                    editReferral.DOB = referral.DOB;
                    editReferral.Gender = referral.Gender;
                    editReferral.PhoneHome = referral.PhoneHome;
                    editReferral.EmailAddress = referral.EmailAddress;
                    editReferral.AddressLine1 = referral.AddressLine1;
                    editReferral.AddressLine2 = referral.AddressLine2;
                    editReferral.AddressCity = referral.AddressCity;
                    editReferral.AddressStateCode = referral.AddressStateCode;
                    editReferral.AddressZipCode = referral.AddressZipCode;
                    editReferral.ServicesRequired = referral.ServicesRequired;
                    editReferral.DME = referral.DME;
                    editReferral.OtherDME = referral.OtherDME;
                    editReferral.UserId = referral.UserId;
                    editReferral.Comments = referral.Comments;
                    if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                    {
                        var physicianList = new List<Physician>();

                        int i = 0;
                        foreach (var guid in referral.AgencyPhysicians)
                        {
                            if (!guid.IsEmpty())
                            {
                                physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                                i++;
                            }
                        }
                        editReferral.Physicians = physicianList.ToXml();
                    }
                    editReferral.Modified = DateTime.Now;
                    database.Update<Referral>(editReferral);
                    result = true;
                }
            }

            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            var referral = database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
            if (referral != null)
            {
                referral.IsDeprecated = true;
                referral.Modified = DateTime.Now;
                database.Update<Referral>(referral);
                return true;
            }
            return false;
        }

        #endregion
    }
}
