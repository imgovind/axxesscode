﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain.Billing;


    public class BillingRepository : IBillingRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public BillingRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IBillingRepository Members

        public bool AddClaim(Claim rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.RapModified = DateTime.Now;
                rap.FinalModified = DateTime.Now;
                rap.AgencyId = rap.AgencyId;
                database.Add<Claim>(rap);
                result = true;
            }
            return result;
        }

        public bool AddRap(Rap rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.Modified = DateTime.Now;
                rap.AgencyId = rap.AgencyId;
                database.Add<Rap>(rap);
                result = true;
            }
            return result;
        }

        public Claim GetClaim(Guid agencyId, int claimId)
        {
            Check.Argument.IsNotNegativeOrZero(claimId, "claimId");
            return database.Single<Claim>(c => (c.AgencyId == agencyId && c.Id == claimId));
        }

        public Rap GetRap(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == claimId));
        }

        public Final GetFinal(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<Final>(f => (f.AgencyId == agencyId && f.Id == claimId));
        }

        public Claim GetClaim(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Claim>(c => (c.AgencyId == agencyId && c.PatientId == patientId && c.EpisodeId == episodeId));
        }

        public Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool UpdateClaimStatus(Claim claim)
        {
            Check.Argument.IsNotNull(claim, "claim");
            var currentClaim = database.Single<Claim>(r => (r.AgencyId == claim.AgencyId && r.PatientId == claim.PatientId && r.EpisodeId == claim.EpisodeId));

            if (currentClaim != null)
            {
                try
                {
                    currentClaim.PatientIdNumber = claim.PatientIdNumber;
                    currentClaim.EpisodeStartDate = claim.EpisodeStartDate;
                    currentClaim.EpisodeEndDate = claim.EpisodeEndDate;
                    currentClaim.IsOasisComplete = claim.IsOasisComplete;
                    currentClaim.IsFirstBillableVisit = claim.IsFirstBillableVisit;
                    currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDate;
                    currentClaim.IsRapGenerated = claim.IsRapGenerated;
                    currentClaim.IsRapVerified = claim.IsRapVerified;

                    currentClaim.RapRemark = claim.RapRemark;
                    currentClaim.FinalRemark = claim.FinalRemark;
                    currentClaim.MedicareNumber = claim.MedicareNumber;
                    currentClaim.FirstName = claim.FirstName;
                    currentClaim.LastName = claim.LastName;
                    currentClaim.DOB = claim.DOB;
                    currentClaim.Gender = claim.Gender;
                    currentClaim.AddressLine1 = claim.AddressLine1;
                    currentClaim.AddressLine2 = claim.AddressLine2;
                    currentClaim.AddressCity = claim.AddressCity;
                    currentClaim.AddressStateCode = claim.AddressStateCode;
                    currentClaim.AddressZipCode = claim.AddressZipCode;
                    currentClaim.StartofCareDate = claim.StartofCareDate;
                    currentClaim.PhysicianNPI = claim.PhysicianNPI;
                    currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                    currentClaim.PhysicianLastName = claim.PhysicianLastName;
                    currentClaim.DiagonasisCode = claim.DiagonasisCode;
                    currentClaim.HippsCode = claim.HippsCode;
                    currentClaim.ClaimKey = claim.ClaimKey;
                    currentClaim.AreOrdersComplete = claim.AreOrdersComplete;
                    currentClaim.AreVisitsComplete = claim.AreVisitsComplete;
                    currentClaim.IsFinalGenerated = claim.IsFinalGenerated;

                    currentClaim.Created = claim.Created;
                    currentClaim.VerifiedVisits = claim.VerifiedVisits;
                    currentClaim.PrimaryInsuranceId = claim.PrimaryInsuranceId;
                    currentClaim.IsSupplyVerified = claim.IsSupplyVerified;
                    currentClaim.IsFinalInfoVerified = claim.IsFinalInfoVerified;
                    currentClaim.IsVisitVerified = claim.IsVisitVerified;
                    currentClaim.AgencyId = claim.AgencyId;
                    database.Update<Claim>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool AddFinal(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            bool result = false;
            try
            {
                final.Created = DateTime.Now;
                final.Modified = DateTime.Now;
                final.AgencyId = final.AgencyId;
                database.Add<Final>(final);
                result = true;
            }
            catch (Exception e)
            {
                return false;
            }
            return result;
        }

        public Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Final>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool UpdateFinalStatus(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentFinal = database.Single<Final>(r => (r.AgencyId == final.AgencyId && r.PatientId == final.PatientId && r.EpisodeId == final.EpisodeId));

            if (currentFinal != null)
            {
                try
                {
                    currentFinal.PatientIdNumber = final.PatientIdNumber;
                    currentFinal.EpisodeStartDate = final.EpisodeStartDate;
                    currentFinal.EpisodeEndDate = final.EpisodeEndDate;
                    currentFinal.IsOasisComplete = final.IsOasisComplete;
                    currentFinal.IsFirstBillableVisit = final.IsFirstBillableVisit;
                    currentFinal.FirstBillableVisitDate = final.FirstBillableVisitDate;
                    currentFinal.Remark = final.Remark;
                    currentFinal.MedicareNumber = final.MedicareNumber;
                    currentFinal.FirstName = final.FirstName;
                    currentFinal.LastName = final.LastName;
                    currentFinal.DOB = final.DOB;
                    currentFinal.Gender = final.Gender;
                    currentFinal.AddressLine1 = final.AddressLine1;
                    currentFinal.AddressLine2 = final.AddressLine2;
                    currentFinal.AddressCity = final.AddressCity;
                    currentFinal.AddressStateCode = final.AddressStateCode;
                    currentFinal.AddressZipCode = final.AddressZipCode;
                    currentFinal.StartofCareDate = final.StartofCareDate;
                    currentFinal.PhysicianNPI = final.PhysicianNPI;
                    currentFinal.PhysicianFirstName = final.PhysicianFirstName;
                    currentFinal.PhysicianLastName = final.PhysicianLastName;
                    currentFinal.DiagonasisCode = final.DiagonasisCode;
                    currentFinal.HippsCode = final.HippsCode;
                    currentFinal.ClaimKey = final.ClaimKey;
                    currentFinal.AreOrdersComplete = final.AreOrdersComplete;
                    currentFinal.AreVisitsComplete = final.AreVisitsComplete;
                    currentFinal.Created = final.Created;
                    currentFinal.VerifiedVisits = final.VerifiedVisits;
                    currentFinal.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    currentFinal.Supply = final.Supply;
                    currentFinal.SupplyTotal = final.SupplyTotal;
                    currentFinal.IsSupplyVerified = final.IsSupplyVerified;
                    currentFinal.IsFinalInfoVerified = final.IsFinalInfoVerified;
                    currentFinal.IsVisitVerified = final.IsVisitVerified;
                    currentFinal.AgencyId = final.AgencyId;
                    currentFinal.IsRapGenerated = final.IsRapGenerated;
                    currentFinal.Status = final.Status;
                    currentFinal.IsGenerated = final.IsGenerated;
                    database.Update<Final>(currentFinal);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRapStatus(Rap rap)
        {

            Check.Argument.IsNotNull(rap, "rap");
            var currentRap = database.Single<Rap>(r => (r.AgencyId == rap.AgencyId && r.PatientId == rap.PatientId && r.EpisodeId == rap.EpisodeId));
            if (currentRap != null)
            {
                try
                {
                    currentRap.PatientIdNumber = rap.PatientIdNumber;
                    currentRap.EpisodeStartDate = rap.EpisodeStartDate;
                    currentRap.EpisodeEndDate = rap.EpisodeEndDate;
                    currentRap.IsOasisComplete = rap.IsOasisComplete;
                    currentRap.IsFirstBillableVisit = rap.IsFirstBillableVisit;
                    currentRap.FirstBillableVisitDate = rap.FirstBillableVisitDate;
                    currentRap.IsGenerated = rap.IsGenerated;
                    currentRap.IsVerified = rap.IsVerified;
                    currentRap.Modified = rap.Modified;
                    currentRap.Remark = rap.Remark;
                    currentRap.MedicareNumber = rap.MedicareNumber;
                    currentRap.FirstName = rap.FirstName;
                    currentRap.LastName = rap.LastName;
                    currentRap.DOB = rap.DOB;
                    currentRap.Gender = rap.Gender;
                    currentRap.AddressLine1 = rap.AddressLine1;
                    currentRap.AddressLine2 = rap.AddressLine2;
                    currentRap.AddressCity = rap.AddressCity;
                    currentRap.AddressStateCode = rap.AddressStateCode;
                    currentRap.AddressZipCode = rap.AddressZipCode;
                    currentRap.StartofCareDate = rap.StartofCareDate;
                    currentRap.PhysicianNPI = rap.PhysicianNPI;
                    currentRap.PhysicianFirstName = rap.PhysicianFirstName;
                    currentRap.PhysicianLastName = rap.PhysicianLastName;
                    currentRap.DiagonasisCode = rap.DiagonasisCode;
                    currentRap.HippsCode = rap.HippsCode;
                    currentRap.ClaimKey = rap.ClaimKey;
                    currentRap.AreOrdersComplete = rap.AreOrdersComplete;
                    currentRap.Created = rap.Created;
                    currentRap.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    currentRap.Status = rap.Status;
                    currentRap.AgencyId = rap.AgencyId;
                    database.Update<Rap>(currentRap);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public Bill AllUnProcessedBill(Guid agencyId)
        {
            var raps = database.Find<Rap>(r => r.IsGenerated == false && r.AgencyId == agencyId);
            var finals = database.Find<Final>(f => f.IsGenerated == false && f.AgencyId == agencyId);
            return new Bill { Raps = raps, Finals = finals };
        }

        public bool VerifyRap(Rap rap, Guid agencyId)
        {
            Check.Argument.IsNotNull(rap, "rap");
            var currentClaim = database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == rap.Id));
            if (currentClaim != null)
            {
                try
                {
                    currentClaim.FirstName = rap.FirstName;
                    currentClaim.LastName = rap.LastName;
                    currentClaim.MedicareNumber = rap.MedicareNumber;
                    currentClaim.FirstBillableVisitDate = rap.FirstBillableVisitDate;
                    currentClaim.PatientIdNumber = rap.PatientIdNumber;
                    currentClaim.Gender = rap.Gender;
                    currentClaim.DOB = rap.DOB;
                    currentClaim.AddressCity = rap.AddressLine1;
                    currentClaim.AddressLine2 = rap.AddressLine2;
                    currentClaim.AddressCity = rap.AddressCity;
                    currentClaim.AddressStateCode = rap.AddressStateCode;
                    currentClaim.AddressZipCode = rap.AddressZipCode;
                    currentClaim.PhysicianLastName = rap.PhysicianLastName;
                    currentClaim.PhysicianNPI = rap.PhysicianNPI;
                    currentClaim.PhysicianFirstName = rap.PhysicianFirstName;
                    currentClaim.DiagonasisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>",rap.Primary,rap.Second,rap.Third,rap.Fourth,rap.Fifth,rap.Sixth);
                    currentClaim.HippsCode = rap.HippsCode;
                    currentClaim.ClaimKey = rap.ClaimKey;
                    currentClaim.Remark = rap.Remark;
                    currentClaim.IsVerified = true;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Rap>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;


        }

        public bool VerifyInfo(Final final, Guid agencyId)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentClaim = database.Single<Final>(r => (r.AgencyId == agencyId && r.Id == final.Id));
            if (currentClaim != null)
            {
                try
                {
                    currentClaim.FirstName = final.FirstName;
                    currentClaim.LastName = final.LastName;
                    currentClaim.MedicareNumber = final.MedicareNumber;
                    currentClaim.PatientIdNumber = final.PatientIdNumber;
                    currentClaim.Gender = final.Gender;
                    currentClaim.DOB = final.DOB;
                    currentClaim.EpisodeStartDate = final.EpisodeStartDate;
                    currentClaim.StartofCareDate = final.StartofCareDate;
                    currentClaim.AddressLine1 = final.AddressLine1;
                    currentClaim.AddressLine2 = final.AddressLine2;
                    currentClaim.AddressCity = final.AddressCity;
                    currentClaim.AddressStateCode = final.AddressStateCode;
                    currentClaim.AddressZipCode = final.AddressZipCode;
                    currentClaim.HippsCode = final.HippsCode;
                    currentClaim.ClaimKey = final.ClaimKey;
                    currentClaim.FirstBillableVisitDate = final.FirstBillableVisitDate;
                    currentClaim.PhysicianLastName = final.PhysicianLastName;
                    currentClaim.PhysicianFirstName = final.PhysicianFirstName;
                    currentClaim.PhysicianNPI = final.PhysicianNPI;
                    currentClaim.DiagonasisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", final.Primary, final.Second, final.Third, final.Fourth, final.Fifth, final.Sixth);
                    currentClaim.Remark = final.Remark;
                    currentClaim.IsFinalInfoVerified = true;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Final>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool VisitVerify(int Id, Guid episodeId, Guid patientId, List<Guid> Visit, Guid agencyId)
        {
            return true;
        }

        public List<Rap> Raps(List<Guid> rapToGenerate, Guid agencyId)
        {
            List<Rap> rapClaim = new List<Rap>();
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(rap =>
                {
                    Rap claim = null;
                    if (rap != Guid.Empty)
                    {
                        claim = GetRap(agencyId, rap);
                    }
                    if (claim != null)
                    {
                        rapClaim.Add(claim);
                    }
                });
            }
            return rapClaim;
        }

        public List<Final> Finals(List<Guid> finalToGenerate, Guid agencyId)
        {
            List<Final> finlaClaim = new List<Final>();
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(final =>
                {
                    Final claim = null;
                    if (final != Guid.Empty)
                    {
                        claim = GetFinal(agencyId, final);
                    }
                    if (claim != null)
                    {
                        finlaClaim.Add(claim);
                    }
                });
            }
            return finlaClaim;
        }

        public IList<Rap> GetRaps(Guid agencyId)
        {
            return database.Find<Rap>(r => r.AgencyId == agencyId);
        }
     
        public IList<Final> GetFinals(Guid agencyId)
        {
            return database.Find<Final>(r => r.AgencyId == agencyId);
        }

        public IList<Rap> GetRapProcessed(Guid patientId, Guid agencyId)
        {
            return database.Find<Rap>(r => r.PatientId == patientId && r.AgencyId == agencyId);
        }

        public IList<Final> GetFinalProcessed(Guid patientId, Guid agencyId)
        {
            return database.Find<Final>(r => r.PatientId == patientId && r.AgencyId == agencyId);
        }

        public IList<Rap> GetUnProcessedRaps(Guid agencyId)
        {
            return database.Find<Rap>(r => r.AgencyId == agencyId && r.Status == (int)ScheduleStatus.ClaimCreated);
        }

        public IList<Final> GetUnProcessedFinals(Guid agencyId)
        {
            return database.Find<Final>(r => r.AgencyId == agencyId && r.Status == (int)ScheduleStatus.ClaimCreated);
        }

        public bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var rap = database.Single<Rap>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == episodeId);
            try
            {
                if (rap != null)
                {
                    database.Delete<Rap>(rap);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var final = database.Single<Final>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == episodeId);
            try
            {
                if (final != null)
                {
                    database.Delete<Rap>(final.Id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public long AddClaimData(ClaimData claimData)
        {
            long claimId = -1;
            if (claimData != null)
            {
                claimData.Created = DateTime.Now;
                claimData.Modified = DateTime.Now;
                try
                {
                    database.Add<ClaimData>(claimData);
                    claimId = claimData.Id;
                }
                catch (Exception ex)
                {
                    return claimId;
                }
            }
            return claimId;
        }

        public bool UpdateClaimData(ClaimData claimData)
        {
            bool result = false;
            if (claimData != null)
            {
                try
                {
                    var claimDataToEdit = database.Single<ClaimData>(c => c.AgencyId == claimData.AgencyId && c.Id == claimData.Id);
                    if (claimDataToEdit != null)
                    {
                        claimDataToEdit.Data = claimData.Data;
                        claimDataToEdit.BillIdentifers = claimData.BillIdentifers;
                        claimDataToEdit.Modified = DateTime.Now;
                        database.Update<ClaimData>(claimDataToEdit);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteClaimData(Guid agencyId, long claimId)
        {
            bool result = false;
            try
            {
                var claimDataToDelete = database.Single<ClaimData>(c => c.AgencyId == agencyId && c.Id == claimId);
                if (claimDataToDelete != null)
                {
                    database.Delete<ClaimData>(claimDataToDelete.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        #endregion
    }
}
