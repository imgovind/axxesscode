﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Domain;

    public interface IMessageRepository
    {
        bool Delete(Guid id);
        bool Add(Message message);
        Message GetMessage(Guid id, bool markAsRead);
        int MessageCount(Guid userId);

        PagedResult<Message> GetReadMessages();
        PagedResult<Message> GetSentMessages(Guid userId);
        PagedResult<Message> GetUserMessages(Guid userId);
        PagedResult<Message> GetMessagesByDateCreated(DateTime startDate, DateTime endDate);
    }
}
