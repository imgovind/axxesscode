﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IPhysicianRepository
    {
        bool Add(AgencyPhysician physician);
        bool Edit(AgencyPhysician physician);
        bool Update(AgencyPhysician physician);
        bool Delete(Guid agencyId, Guid id);
        bool Link(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkAll(Guid patientId);
        bool Unlink(Guid patientId, Guid physicianId);
        AgencyPhysician Get(Guid physicianId, Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId);
        bool DoesPhysicianExist(Guid patientId, Guid physicianId);
        IList<AgencyPhysician> GetPatientPhysicians(Guid patientId);
        IList<Patient> GetPhysicanPatients(Guid physicianId);
        bool SetPrimary(Guid patientId, Guid physicianId);
        AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId);

    }
}
