﻿namespace Axxess.AgencyManagement.Domain
{

    using System;
    using System.Xml;
    using System.Xml.Serialization;

    [XmlRoot()]   
    public class NotesQuestion
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Answer { get; set; }
        [XmlAttribute]
        public string Type { get; set; }

    }
}
