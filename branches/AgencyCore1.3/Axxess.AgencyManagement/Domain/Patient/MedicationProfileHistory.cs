﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class MedicationProfileHistory
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Medication { get; set; }
        public bool IsSigned { get; set; }
        public DateTime SignedDate { get; set; }
        public bool IsOasis { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public Guid  UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string SignedDateFormatted { get { return this.SignedDate.ToShortDateString(); } }

        [SubSonicIgnore]
        public string Signature { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

    }
}
