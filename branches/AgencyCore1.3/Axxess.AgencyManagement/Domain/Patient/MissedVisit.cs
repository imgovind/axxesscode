﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;

    public class MissedVisit
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime Date { get; set; }
        public string Reason { get; set; }
        public string Comments { get; set; }
        public bool IsOrderGenerated { get; set; }
        public bool IsPhysicianOfficeNotified { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string VisitType { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public Agency Agency { get; set; }

        [SubSonicIgnore]
        public Patient Patient { get; set; }

        [SubSonicIgnore]
        public string EventDate { get; set; }

        [SubSonicIgnore]
        public string DisciplineTaskName { get; set; }
        
        public override string ToString()
        {
            var comments = new StringBuilder();
            if (this.Reason.IsNotNullOrEmpty())
            {
                comments.AppendFormat("Missed Visit Reason: {0}. ", this.Reason);
                comments.Append("<br />");
            }

            if (this.Comments.IsNotNullOrEmpty())
            {
                comments.AppendFormat("Comments: {0}", this.Comments);
                comments.Append("<br />");
            }

            return comments.ToString();
        }
    }
}
