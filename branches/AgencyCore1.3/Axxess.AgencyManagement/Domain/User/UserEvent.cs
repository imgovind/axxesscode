﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;

    [XmlRoot()]
    [DataContract]
    public class UserEvent
    {
        public UserEvent()
        {
            this.EventDate = string.Empty;
        }

        [XmlElement]
        [DataMember]
        public Guid EventId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid EpisodeId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid PatientId { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }    
   
        [XmlElement]
        [DataMember]
        public string EventDate { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeOut { get; set; }

        [XmlElement]
        [DataMember]
        public string ReturnReason { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status != null)
                {
                    if ((this.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString()) && DateTime.Parse(this.EventDate) < DateTime.Today)
                    {
                        return ScheduleStatus.CommonNotStarted.GetDescription();
                    }
                    else
                    {
                        return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), int.Parse(this.Status)));
                    }
                }
                return "";
            }
        }


    }
}
