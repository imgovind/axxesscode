﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;

    using SubSonic.SqlGeneration.Schema;

    public class License
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AssetId { get; set; }
        public DateTime Created { get; set; }
        public string LicenseType { get; set; }
        public DateTime InitiationDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        public string OtherLicenseType { get; set; }

        [XmlIgnore]
        public string AssetUrl { get; set; }

        [XmlIgnore]
        public Guid UserId { get; set; }

        [XmlIgnore]
        public string InitiationDateFormatted { get { return this.InitiationDate.ToShortDateString(); } }

        [XmlIgnore]
        public string ExpirationDateFormatted { get { return this.ExpirationDate.ToShortDateString(); } }

        #endregion
    }
}
