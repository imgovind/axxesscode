﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain.Billing
{
    public class BillInfo
    {
        public string Discipline { get; set; }
        public string Amount { get; set; }
        public string CodeOne { get; set; }
        public string CodeTwo { get; set; }
        public int Unit { get; set; }
    }
}
