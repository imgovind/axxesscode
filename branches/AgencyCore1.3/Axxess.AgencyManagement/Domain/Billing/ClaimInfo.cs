﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Axxess.AgencyManagement.Domain.Billing
{
    [XmlRoot()]
   public class ClaimInfo
    {
        [XmlElement]
       public Guid ClaimId {get; set;}
        [XmlElement]
       public Guid PatientId { get; set; }
        [XmlElement]
       public Guid EpisodeId { get; set; }
        [XmlElement]
       public string ClaimType { get; set; }
    }
}
