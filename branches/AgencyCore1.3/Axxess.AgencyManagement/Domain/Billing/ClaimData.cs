﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain.Billing
{
    public class ClaimData
    {
        public long Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Data { get; set; }
        public string BillIdentifers { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
