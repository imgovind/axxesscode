﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class TypeOfBill
    {
        public string Type { get; set; }
        public string PatientName { get; set; }
        public string EpisodeRange { get; set; }
    }
}
