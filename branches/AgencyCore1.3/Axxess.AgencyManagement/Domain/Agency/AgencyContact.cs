﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class AgencyContact : EntityBase, IAddress
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CompanyName { get; set; }
        public string ContactType { get; set; }
        public string PhonePrimary { get; set; }
        public string PhoneAlternate { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region IAddress members

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string EmailAddress { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string PhonePrimaryFormatted { get { return this.PhonePrimary.ToPhone(); } }
        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1} {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        [SubSonicIgnore]
        public string ContactTypeOther { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "First name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Address is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "City is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "State is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Zipcode is required."));

            if (this.PhonePrimary.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhonePrimaryArray.Count == 0, "Primary Phone is required."));
            }
        }

        #endregion

    }

}
