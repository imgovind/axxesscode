@ECHO OFF

GOTO %1

:all
ECHO Compressing All JavaScript Files...

:acore
ECHO Compressing AgencyCore Files...
set FOLDER_LIST=(Agency Billing Home Message Notes Oasis Patient Payroll Plugins\Custom Print Referral Report Schedule System User)
FOR %%i in %FOLDER_LIST% DO FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\%%i\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\%%i\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\%%i\min\%%f
IF "%1"=="acore" GOTO end

:support
ECHO Compressing Axxess Support Files...
FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Modules\min\%%f
REM FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\min\%%f
IF "%1"=="support" GOTO end

:md
ECHO Compressing Axxess MD Files...
FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.Physician.WebSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.Physician.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.Physician.WebSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.Physician.WebSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.Physician.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.Physician.WebSite\Scripts\Plugins\Custom\min\%%f

:end
del ~compile.tmp
ECHO Complete
pause