﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;

    public class UserSessionEngine
    {
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        public static UserActiveSession GetSession(Guid agencyId, Guid userId, AgencyServices acessibleServices, string sessionId)
        {
            UserActiveSession activeSession = null;
            if (sessionId.IsNotNullOrEmpty())
            {
                var key = Key(sessionId);
                activeSession = Cacher.Get<UserActiveSession>(key);
                if (activeSession!=null)
                {
                    if (activeSession.AgencyId == agencyId && activeSession.UserId == userId)
                    {
                        return activeSession;
                    }
                    else
                    {
                        return AddUser( agencyId, userId,acessibleServices, key);
                    }
                }
                else
                {
                    return AddUser( agencyId,  userId,acessibleServices, key);
                }
            }
            return activeSession;
        }


        public static IDictionary<int, Dictionary<int, List<int>>> ReportPermissions(Guid agencyId, Guid userId, AgencyServices acessibleServices, string sessionId)
        {
            var activeSession = GetSession(agencyId, userId, acessibleServices, sessionId);
            if (activeSession != null)
            {
                return activeSession.ReportPermissions;
            }
            else
            {
                return new Dictionary<int, Dictionary<int, List<int>>>();
            }
        }

        public static List<Guid> LocationIds(Guid agencyId, Guid userId, AgencyServices acessibleServices, AgencyServices service, string sessionId)
        {
            var activeSession = GetSession(agencyId, userId, acessibleServices, sessionId);
            if (activeSession != null)
            {
                return activeSession.Locations.Where(l => !l.LocationId.IsEmpty() && ((l.Services & service) == service)).Select(l=>l.LocationId).ToList();
            }
            else
            {
                return new List<Guid>();
            }
        }

        public static List<UserLocation> Locations(Guid agencyId, Guid userId, AgencyServices acessibleServices,string sessionId)
        {
            var activeSession = GetSession(agencyId, userId, acessibleServices, sessionId);
            if (activeSession != null)
            {
                return activeSession.Locations.Where(l => !l.LocationId.IsEmpty()).ToList();
            }
            else
            {
                return new List<UserLocation>();
            }
        }


        //public static void AddOrUpdate(string userName, UserProfile userProfile)
        //{
        //    if (!agencyId.IsEmpty() && userProfile != null && !userProfile.Id.IsEmpty())
        //    {
        //        var key = Key(agencyId, userProfile.Id);
        //        var userXml = Cacher.Get<string>(key);
        //        if (userXml.IsNotNullOrEmpty())
        //        {
        //            var user = userXml.ToObject<User>();
        //            if (user != null)
        //            {
        //                user.Profile = userProfile;
        //                Cacher.Set<string>(key, user.ToXml());
        //            }
        //            else
        //            {
        //                AddUser(agencyId, userProfile.Id, key);
        //            }
        //        }
        //        else
        //        {
        //            AddUser(agencyId, userProfile.Id, key);
        //        }
        //    }
        //}

        private static UserActiveSession AddUser(Guid agencyId, Guid userId, AgencyServices acessibleServices, string key)
        {
            var activeSession = new UserActiveSession { AgencyId = agencyId, UserId = userId };
            var user = dataProvider.UserRepository.GetUserOnly(agencyId, userId);
            if (user != null)
            {
                activeSession.ReportPermissions = UserSessionHelper.GetPermission(user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>(), acessibleServices); 
                activeSession.Locations = dataProvider.UserRepository.UserLocations(agencyId, userId,(int)acessibleServices);
                Cacher.Set<UserActiveSession>(key, activeSession);
            }
            return activeSession;
        }

        public static UserActiveSession AddUser(User user, AgencyServices acessibleServices, string sessionId)
        {
            var activeSession = new UserActiveSession { AgencyId = user.AgencyId, UserId = user.Id };
            if (user != null)
            {
                activeSession.ReportPermissions = UserSessionHelper.GetPermission(user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>(), acessibleServices); 
                activeSession.Locations = dataProvider.UserRepository.UserLocations(user.AgencyId, user.Id,(int)acessibleServices);
                var key = Key(sessionId);
                Cacher.Set<UserActiveSession>(key, activeSession);
            }
            return activeSession;
        }

        private static string Key(string sessionId)
        {
            return string.Format("{0}_{1}", (int)CacheType.UserSession, sessionId);
        }
    }
}
