﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;

    public static class UserSessionHelper
    {

        public static Dictionary<int, Dictionary<int, List<int>>> GetPermission(IDictionary<int, Dictionary<int, List<int>>> permissions, AgencyServices acessibleServices, bool IsReportCenterVisbile)
        {
            var flags = GetValidServices();

            var formattedPermissions = GetPermissionHelper(permissions, acessibleServices, flags);
            //var reportActions = reportActionString.FromJson<Dictionary<int, List<int>>>();
            //if (reportActions.IsNotNullOrEmpty())
            //{
            //    var actionData = SetActionPermission(reportActions, flags, (int)acessibleServices, (int)AgencyServicesBoundary.All);
            //    if (actionData.IsNotNullOrEmpty())
            //    {
            if (IsReportCenterVisbile && !formattedPermissions.ContainsKey((int)ParentPermission.Report))
            {
                var services = flags.Where(f => ((int)acessibleServices & f) == f).ToList();
                formattedPermissions[(int)ParentPermission.Report] = new Dictionary<int, List<int>>() { { (int)PermissionActions.ViewList, services } };
            }
            //    }
            //}
            return formattedPermissions;
        }

        public static Dictionary<int, Dictionary<int, List<int>>> GetPermission(IDictionary<int, Dictionary<int, List<int>>> permissions, AgencyServices acessibleServices)
        {
            var flags = GetValidServices();
            return GetPermissionHelper(permissions, acessibleServices, flags);
        }

        private static Dictionary<int, Dictionary<int, List<int>>> GetPermissionHelper(IDictionary<int, Dictionary<int, List<int>>> permissions, AgencyServices acessibleServices, List<int> currentActiveServices)
        {
            var formattedPermissions = new Dictionary<int, Dictionary<int, List<int>>>();
            var flags = currentActiveServices ?? GetValidServices();
            //var allowedService = 0;
            if (permissions.IsNotNullOrEmpty())
            {
                var acessibleServicesInt = (int)acessibleServices;
                var allService = (int)AgencyServicesBoundary.All;
                permissions.ForEach((key, value) =>
                {
                    if (value.IsNotNullOrEmpty())
                    {
                        var actionData = SetActionPermission(value, flags, acessibleServicesInt, allService);
                        if (actionData.IsNotNullOrEmpty())
                        {
                            formattedPermissions.Add(key, actionData);
                        }
                    }

                });

            }
            return formattedPermissions;
        }

        private static Dictionary<int, List<int>> SetActionPermission(Dictionary<int, List<int>> value, List<int> flags, int acessibleServicesInt, int allService)
        {
            var actionData = new Dictionary<int, List<int>>();
            value.ForEach((iKey, iValue) =>
            {
                if (iValue.IsNotNullOrEmpty())
                {
                    var service = new List<int>();
                    if (iValue.Count > 1)
                    {
                        iValue.ForEach(v =>
                        {
                            if ((acessibleServicesInt & v) == v)
                            {
                                service.Add(v);
                                //allowedService = allowedService | v;
                            }
                        });
                    }
                    else
                    {
                        var val = iValue.FirstOrDefault();
                        if (val == allService)
                        {
                            service = flags;
                        }
                        else if (flags.Contains(val))
                        {
                            service.Add(val);
                        }
                    }

                    if (service.Count > 0)
                    {
                        actionData.Add(iKey, service);
                    }
                }
            });
            return actionData;
        }

        private static List<int> GetValidServices()
        {
            var flags = new List<int>();
            AgencyServices[] values = (AgencyServices[])Enum.GetValues(typeof(AgencyServices));
            Array.ForEach<AgencyServices>(values, delegate(AgencyServices v)
            {
                var vInt = (int)v;
                if (vInt > 0)
                {
                    flags.Add(vInt);
                }

            });
            return flags;
        }


    }
}
