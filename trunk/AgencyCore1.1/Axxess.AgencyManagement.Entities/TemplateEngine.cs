﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Repositories;

    public static class TemplateEngine
    {
        #region Private Members

        private static readonly IAgencyManagementDataProvider DataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        public static AgencyTemplate GetTemplate(Guid templateId, Guid agencyId)
        {
            AgencyTemplate template = null;
            if (!templateId.IsEmpty() && !agencyId.IsEmpty())
            {
                List<AgencyTemplate> templates = GetTemplates(agencyId);
                template = templates.FirstOrDefault(t => t.Id == templateId) ?? AddTemplate(agencyId, templateId, templates);
            }
            return template;
        }

        public static List<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            List<AgencyTemplate> templates = null;
            if (!agencyId.IsEmpty())
            {
                var key = Key(agencyId);
                if (!Cacher.TryGet(key, out templates))
                {
                    templates = DataProvider.AgencyRepository.GetTemplates(agencyId).ToList();
                    Cacher.Set(key, templates);
                }
            }
            return templates;
        }

        public static void AddOrUpdate(Guid agencyId, AgencyTemplate template)
        {
            if (!agencyId.IsEmpty() && template != null && !template.Id.IsEmpty())
            {
                List<AgencyTemplate> templates = GetTemplates(agencyId);
                int index = templates.FindIndex(d => d.Id == template.Id);
                if (index >= 0)
                {
                    templates.RemoveAt(index);
                    templates.Insert(index, template);
                }
                else
                {
                    templates.Add(template);
                }
                var key = Key(agencyId);
                Cacher.Set(key, templates);
            }
        }

        public static void Remove(Guid agencyId, AgencyTemplate template)
        {
            if (!agencyId.IsEmpty() && template != null && !template.Id.IsEmpty())
            {
                List<AgencyTemplate> templates = GetTemplates(agencyId);
                templates.Remove(template);
                var key = Key(agencyId);
                Cacher.Set(key, templates);
            }
        }


        #endregion

        #region Private Methods

        private static AgencyTemplate AddTemplate(Guid agencyId, Guid templateId, List<AgencyTemplate> templates)
        {
            var template = DataProvider.AgencyRepository.GetTemplate(templateId, agencyId);
            if (template != null)
            {
                var key = Key(agencyId);
                templates.Add(template);
                Cacher.Set(key, templates);
            }
            return template;
        }

        private static string Key(Guid agencyId)
        {
            return string.Format("Templates_{0}", agencyId);
        }
        #endregion

    }
}
