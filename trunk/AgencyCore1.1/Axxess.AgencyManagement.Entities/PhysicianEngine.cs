﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Repositories;
    
    

    public static class PhysicianEngine
    {
        #region Private Members

        //private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        public static string GetName(Guid physicianId, Guid agencyId)
        {
            var name = string.Empty;
            if (!physicianId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, physicianId);
                var physicianXml = string.Empty;
                if (!Cacher.TryGet<string>(key, out physicianXml))
                {
                    var physician = dataProvider.PhysicianRepository.Get(physicianId, agencyId);
                    if (physician != null)
                    {
                        var xml = physician.ToXml();
                        Cacher.Set(key, physician.ToXml());
                        name = physician.DisplayName;
                    }
                }
                else
                {
                    var physician = physicianXml.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        name = physician.DisplayName;
                    }
                }
            }
            return name;
        }

        public static AgencyPhysician Get(Guid physicianId, Guid agencyId)
        {
            AgencyPhysician physician = null;
            if (!physicianId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, physicianId);
                var physicianXml = Cacher.Get<string>(key);
                if (physicianXml.IsNotNullOrEmpty())
                {
                    physician = physicianXml.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        return physician;
                    }
                    else
                    {
                        return AddPhysician(agencyId, physicianId, key);
                    }
                }
                else
                {
                    return AddPhysician(agencyId, physicianId, key);
                }
            }
            return physician;
        }

        public static AgencyPhysician GetOnlyFromCatch(Guid physicianId, Guid agencyId)
        {
            AgencyPhysician physician = null;
            if (!physicianId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, physicianId);
                var physicianXml = Cacher.Get<string>(key);
                if (physicianXml.IsNotNullOrEmpty())
                {
                    physician = physicianXml.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        return physician;
                    }

                }
            }
            return physician;
        }
       
        //public static IList<AgencyPhysician> AsList(Guid agencyId)
        //{
        //    IList<AgencyPhysician> physicianList = new List<AgencyPhysician>();

        //    //var physicianInfoList = cacheAgent.GetPhysicians(agencyId);
        //    //if (physicianInfoList == null || physicianInfoList.Count == 0)
        //    //{
        //        physicianList = dataProvider.PhysicianRepository.GetAgencyPhysicians(agencyId);
        //    //}
        //    //else
        //    //{
        //    //    physicianInfoList.ForEach(p =>
        //    //    {
        //    //        if (p.IsNotNullOrEmpty())
        //    //        {
        //    //            physicianList.Add(p.ToObject<AgencyPhysician>());
        //    //        }
        //    //    });
        //    //}

        //    return physicianList;
        //}

        public static List<AgencyPhysician> GetAgencyPhysicians(Guid agencyId, List<Guid> physicianIds)
        {
            var physicians = new List<AgencyPhysician>();
            var physicianIdsNotInCache = new List<Guid>();
            if (!agencyId.IsEmpty() && physicianIds != null && physicianIds.Count > 0)
            {
                var keys = physicianIds.Select(id => Key(agencyId, id));
                var results = Cacher.Get<string>(keys);
                if (results != null && results.Count > 0)
                {
                    results.ForEach((key, value) =>
                    {
                        if (key.IsNotNullOrEmpty() && value != null)
                        {
                            string userXml = value.ToString();
                            if (userXml.IsNotNullOrEmpty())
                            {
                                physicians.Add(userXml.ToObject<AgencyPhysician>());
                            }
                        }

                    });
                }
                physicianIdsNotInCache = physicianIds.Where(id => !physicians.Exists(u => u.Id == id)).ToList();
                if (physicianIdsNotInCache != null && physicianIdsNotInCache.Count > 0)
                {
                    var physiciansNotInCache = dataProvider.PhysicianRepository.GetPhysiciansByIdsLean(agencyId, physicianIdsNotInCache);
                    if (physicianIdsNotInCache != null && physicianIdsNotInCache.Count > 0)
                    {
                        physiciansNotInCache.ForEach(u =>
                        {
                            var key = Key(agencyId, u.Id);
                            Cacher.Set<string>(key, u.ToXml());
                            physicians.Add(u);

                        });
                    }
                }
            }
            return physicians;
        }

        public static void AddOrUpdate(Guid agencyId, AgencyPhysician physician)
        {
            if (!agencyId.IsEmpty() && physician != null && !physician.Id.IsEmpty())
            {
                var physicianXml = physician.ToXml();
                if (physicianXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId, physician.Id);
                    Cacher.Set<string>(key, physicianXml);
                }
            }
        }


        #endregion

        #region Private Methods

        private static AgencyPhysician AddPhysician(Guid agencyId, Guid physicianId, string key)
        {
            var physician = dataProvider.PhysicianRepository.Get(physicianId, agencyId);
            if (physician != null)
            {
                Cacher.Set<string>(key, physician.ToXml());
            }
            return physician;
        }

        private static string Key(Guid agencyId, Guid physicianId)
        {
            return string.Format("{0}_{1}_{2}", agencyId, (int)CacheType.Physician, physicianId);
        }

        #endregion
    }
}
