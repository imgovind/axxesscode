﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class DashboardMessage : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Title), "Title is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Content is required. <br />"));
        }

        #endregion

    }
}
