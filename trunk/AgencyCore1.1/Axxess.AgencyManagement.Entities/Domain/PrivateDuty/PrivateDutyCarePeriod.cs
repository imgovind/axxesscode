﻿namespace Axxess.AgencyManagement.Entities
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    [SubSonicTableNameOverride("patientepisodes")]
    public class PrivateDutyCarePeriod : CarePeriod
    {
        [SubSonicIgnore]
        public override bool HasNext { get { return this.NextPeriod != null; } }
        [SubSonicIgnore]
        public PrivateDutyCarePeriod NextPeriod { get; set; }
        [SubSonicIgnore]
        public override bool HasPrevious { get { return this.PreviousPeriod != null; } }
        [SubSonicIgnore]
        public PrivateDutyCarePeriod PreviousPeriod { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
