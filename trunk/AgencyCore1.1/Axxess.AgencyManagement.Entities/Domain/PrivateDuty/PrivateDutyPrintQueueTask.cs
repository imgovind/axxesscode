﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using System.ComponentModel.DataAnnotations;

    public class PrivateDutyPrintQueueTask
    {
        public Guid Id { get; set; }
        public string TaskName { get; set; }
        public string Patient { get; set; }
        public string Status { get; set; }
        [UIHint("EmptyDate")]
        public DateTime EventDate { get; set; }
        public string Date
        {
            get
            {
                return this.EventDate.ToZeroFilled();
            }
        }
        public string Employee { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string PrintUrl { get; set; }
    }
}
