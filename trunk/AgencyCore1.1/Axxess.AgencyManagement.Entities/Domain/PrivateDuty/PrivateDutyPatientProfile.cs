﻿//namespace Axxess.AgencyManagement.Entities
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Xml.Serialization;
//    using System.Web.Script.Serialization;

//    using SubSonic.SqlGeneration.Schema;

//    using Axxess.Core;
//    using Axxess.Core.Extension;
//    using Axxess.Core.Infrastructure;

//    using Enums;
//    using System.Runtime.Serialization;
//    [DataContract]
//    [SubSonicTableNameOverride("patientprofiles")]
//    public class PrivateDutyPatientProfile : Profile
//    {
//        #region Members

//        public Guid AuditorId { get; set; }
//        public DateTime LastEligibilityCheck { get; set; }

//        #endregion

//        #region Domain

//        [XmlIgnore]
//        [SubSonicIgnore]
//        public string PrimaryInsuranceName { get; set; }
//        [XmlIgnore]
//        [SubSonicIgnore]
//        public string SecondaryInsuranceName { get; set; }
//        [XmlIgnore]
//        [SubSonicIgnore]
//        public string TertiaryInsuranceName { get; set; }



//        #endregion

//        #region Validation Rules

//        protected override void AddValidationRules()
//        {
//            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
//            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
//            AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));
//            AddValidationRule(new Validation(() => this.CaseManagerId.IsEmpty(), "Assign to Clinician/Case Manager is required."));
//            AddValidationRule(new Validation(() => this.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
//            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.StartofCareDate.ToString()), "Patient Start of care date is required."));
//            if (this.ReferralDate.IsValid())
//            {
//                AddValidationRule(new Validation(() => this.StartofCareDate < this.ReferralDate, "Referral date must be less than or equal to the start of care."));
//            }
//        }

//        #endregion

//        #region Encode the multi array string field to one field

//        public void Encode()
//        {
//            if (this.MedicareNumber.IsNotNullOrEmpty())
//            {
//                this.MedicareNumber = this.MedicareNumber.Trim();
//            }
//            if (this.MedicaidNumber.IsNotNullOrEmpty())
//            {
//                this.MedicaidNumber = this.MedicaidNumber.Trim();
//            }

//        }

//        #endregion



//    }
//}
