﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Enums;
    using Extensions;

    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using System.Globalization;


    [XmlRoot()]
    [DataContract]
    [Serializable]
    [SubSonicTableNameOverride("scheduletasks")]
    public class PrivateDutyScheduleTask : ITask
    {
        public PrivateDutyScheduleTask()
        {
            this.UserId = Guid.Empty;
            this.Id = Guid.Empty;
            this.Assets = new List<Guid>();
        }

        [XmlElement("EventStartTime")]
        [SubSonicIgnore]
        public string EventStartTimeXml { get { return EventStartTime.ToDateAndTime(); } set { EventStartTime = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlElement("EventEndTime")]
        [SubSonicIgnore]
        public string EventEndTimeXml { get { return EventEndTime.ToDateAndTime(); } set { EventEndTime = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlElement("VisitStartTime")]
        [SubSonicIgnore]
        public string VisitStartTimeXml { get { return VisitStartTime.ToDateAndTime(); } set { VisitStartTime = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlElement("VisitEndTime")]
        [SubSonicIgnore]
        public string VisitEndTimeXml { get { return VisitEndTime.ToDateAndTime(); } set { VisitEndTime = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlIgnore]
        public DateTime EventStartTime { get; set; }

        [XmlIgnore]
        public DateTime EventEndTime { get; set; }

        [XmlIgnore]
        public DateTime VisitStartTime { get; set; }

        [XmlIgnore]
        public DateTime VisitEndTime { get; set; }

        public bool IsHidden { get; set; }
        public bool IsAllDay { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public override string ReturnReason { get; set; }


        [XmlIgnore]
        [SubSonicIgnore]
        public override int MinSpent
        {
            get
            {
                if (this.VisitStartTime.IsValid() && this.VisitEndTime.IsValid())
                {
                    if (this.VisitEndTime >= this.VisitStartTime.Date)
                    {
                        return (int)this.VisitEndTime.Subtract(this.VisitStartTime).TotalMinutes;
                    }
                    else
                    {
                        return (int)this.VisitStartTime.Subtract(this.VisitEndTime).TotalMinutes;
                    }
                }
                return 0;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override DateTime EventDate
        {
            get
            {
                return this.EventStartTime.Date;
            }
            set
            {
                this.EventStartTime = new DateTime(value.Year, value.Month, value.Day, EventStartTime.Hour, EventStartTime.Minute, EventStartTime.Second);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override DateTime VisitDate
        {
            get
            {
                return this.VisitStartTime.Date;
            }
            set
            {
                this.VisitStartTime = new DateTime(value.Year, value.Month, value.Day, VisitStartTime.Hour, VisitStartTime.Minute, VisitStartTime.Second);
            }

        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override string TimeIn
        {
            get
            {
                return this.VisitStartTime.ToString("hh:mm tt");
            }
            set
            {
                this.VisitStartTime = DateTime.ParseExact(this.VisitStartTime.ToZeroFilled() + " " + value, "MM/dd/yyyy hh:mm tt", CultureInfo.CurrentCulture);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override string TimeOut
        {
            get
            {
                return this.VisitEndTime.ToString("hh:mm tt");
            }
            set
            {
                this.VisitEndTime = DateTime.ParseExact(this.VisitEndTime.ToZeroFilled() + " " + value, "MM/dd/yyyy hh:mm tt", CultureInfo.CurrentCulture);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override string EventDateTimeRange
        {
            get
            {
                if (!IsAllDay)
                {
                    return string.Format("{0}-{1}", this.EventStartTime.ToString("hh:mm tt"), this.EventEndTime.ToString("hh:mm tt"));
                }
                return "All Day";
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override string VisitDateTimeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.VisitStartTime.ToString("hh:mm tt"), this.VisitEndTime.ToString("hh:mm tt"));
            }
        }

        [SubSonicIgnore]
        public override AgencyServices Service { get { return AgencyServices.PrivateDuty; } }

        protected override void AddValidationRules()
        {
           
            if (!this.IsAllDay)
            {
                Rules.Add(new Validation(() => this.EventStartTime.Date > this.EventEndTime.Date, "The end date must be after the start date."));
                Rules.Add(new Validation(() => this.EventStartTime.Date == DateTime.MinValue && this.EventStartTime.TimeOfDay == TimeSpan.MinValue, "The start date and time must be set."));
                Rules.Add(new Validation(() => this.EventEndTime.Date == DateTime.MinValue && this.EventEndTime.TimeOfDay == TimeSpan.MinValue, "The end date and time must be set."));
            }
            else
            {
                Rules.Add(new Validation(() => this.EventStartTime.Date == DateTime.MinValue, "The start date must be set."));
                Rules.Add(new Validation(() => this.EventEndTime.Date == DateTime.MinValue, "The end date must be set."));
            
            }
        }
       
      
    }
}
