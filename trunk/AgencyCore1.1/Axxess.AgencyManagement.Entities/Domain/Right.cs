﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class Right
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
    }
}
