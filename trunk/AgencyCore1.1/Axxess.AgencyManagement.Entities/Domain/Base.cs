﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using Axxess.Core;

    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public abstract class AgencyBase : EntityBase
    {
        public Guid AgencyId { get; set; }
    }
    [Serializable]
    [DataContract]
    public abstract class PatientBase : AgencyBase
    {
        public Guid PatientId { get; set; }
    }
    [Serializable]
    [DataContract]
    public abstract class EpisodeBase : PatientBase
    {
        public Guid EpisodeId { get; set; }
    }
    [Serializable]
    [DataContract]
    public abstract class SignableNoteBase : PatientBase
    {
        public int Status { get; set; }
        public DateTime SignatureDate { get; set; }
        public string SignatureText { get; set; }
    }
}
