﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class EmergencyContactInfo
    {
        public Guid Id { get; set; }
        public string PatientName { get; set; }
        public int Triage { get; set; }
        public string PatientIdNumber { get; set; }
        public string ContactName { get; set; }
        public string ContactRelation { get; set; }
        public string ContactPhoneHome { get; set; }
        public string ContactEmailAddress { get; set; }
    }
}
