﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    using Enums;

    public class ReferralData
    {
        public Guid Id { get; set; }
        public int StatusId { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime ReferralDate { get; set; }
        public int AdmissionSourceId { get; set; }
        public string AdmissionSource { get; set; }
        public string PatientIdNumber { get; set; }
        public Guid CreatedById { get; set; }
        public bool IsPatientExist { get; set; }
        /// <summary>
        /// the status of the exisiting service status for the patient
        /// </summary>
        public int ExistingPatientStatus { get; set; }
        public string PatientTypeName { get { return this.IsPatientExist ? "Patient" : "Referral"; } }

        public string Status
        {
            get
            {
                return this.StatusId.ToEnum<ReferralStatus>(ReferralStatus.Pending).GetDescription();
            }
        }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName).ToTitleCase();
            }
        }

        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanAdmit { get; set; }
        public bool IsUserCanNonAdmit { get; set; }
        public bool IsUserCanPrint { get; set; }

    }
}
