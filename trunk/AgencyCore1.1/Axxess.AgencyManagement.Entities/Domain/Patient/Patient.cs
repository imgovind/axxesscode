﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    [Serializable]
    [DataContract]
    public class Patient : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        [ScriptIgnore]
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }
        [ScriptIgnore]
        public string SSN { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public float Height { get; set; }
        public int HeightMetric { get; set; }
        public float Weight { get; set; }
        public int WeightMetric { get; set; }
        public string Ethnicities { get; set; }
        public string MaritalStatus { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string EmailAddress { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string DME { get; set; }
        public string OtherDME { get; set; }
        public bool IsDNR { get; set; }
        public int Triage { get; set; }
        public string EvacuationZone { get; set; }
        public Guid PhotoId { get; set; }
        public string ServicesRequired { get; set; }
        public Guid ReferrerPhysician { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Comments { get; set; }
        // //TODO: delete
        //[SubSonicIgnore]
        //public bool IsDeprecated { get; set; }
        public bool IsHospitalized { get; set; }
        public Guid HospitalizationId { get; set; }
        public int PrivateDutyStatus { get; set; }
        public int HomeHealthStatus { get; set; }
        public AgencyServices Services { get; set; }
        public string VisitRates { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        [SubSonicIgnore]
        public Profile Profile { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsOnlyOneService { get; set; }
        /// <summary>
        /// Used to filter service for specific purpose 
        /// </summary>
        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyServices CurrentServices { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyServices RequestedService{ get; set; }
        /// <summary>
        /// Used to to count service for admit, delete or any action
        /// </summary>
        [XmlIgnore]
        [SubSonicIgnore]
        public int TotalServiceToProcess { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public FormAction FormAction { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string HHStatusName
        {
            get
            {
                return Enum.IsDefined(typeof(PatientStatus), this.HomeHealthStatus) ? ((PatientStatus)this.HomeHealthStatus).GetDescription() : string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PDStatusName
        {
            get
            {
                return Enum.IsDefined(typeof(PatientStatus), this.PrivateDutyStatus) ? ((PatientStatus)this.PrivateDutyStatus).GetDescription() : string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ServiceProvided { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public Guid ReferralId { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string Branch { get; set; }
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string StatusName
        //{
        //    get
        //    {
        //        return EnumExtensions.GetDescription((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.Status));
        //    }
        //}
        [XmlIgnore]
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldRequestElligibility { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DOBFormatted { get { return this.DOB.ToString("MM/dd/yyyy"); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> EthnicRaces { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<Guid> AgencyPhysicians { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToString("MM/dd/yyyy"); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> DMECollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public PatientEmergencyContact EmergencyContact { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<PatientEmergencyContact> EmergencyContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<AgencyPhysician> PhysicianContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneHomeArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneMobileArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PharmacyPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }

        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanDelete { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanAdd { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanPrint { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanExport { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => !this.DOB.ToString().IsValidDate(), "Date of birth is not a valid date."));
            AddValidationRule(new Validation(() => this.DOB <= DateTime.MinValue, "Patient date of birth is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient gender has to be selected."));
            if (this.EmailAddress.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.EmailAddress.IsEmail(), "Patient e-mail is not in a valid format."));
            }
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            if (this.SSN.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.SSN.IsSSN(), "Patient SSN is not in valid format."));
            }
            if (Ethnicities.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.EthnicRaces.Count == 0, "Patient Race/Ethnicity is required."));
            }
            if (this.PhoneHome.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneHomeArray.Count == 0, "Patient phone is required."));
            }
            AddValidationRule(new Validation(() => this.Triage <= 0, "Emergency Triage is required."));
            //AddValidationRule(new Validation(() => this.EmergencyContact == null, "Emergency contact details are required."));
        }

        #endregion

        #region Encode the multi array string field to one field

        public void Encode()
        {
            if (this.EmergencyContact != null)
            {
                if (this.EmergencyContact.PhonePrimaryArray != null && this.EmergencyContact.PhonePrimaryArray.Count >= 2)
                {
                    this.EmergencyContact.PrimaryPhone = this.EmergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (this.EmergencyContact.PhoneAlternateArray != null && this.EmergencyContact.PhoneAlternateArray.Count >= 2)
                {
                    this.EmergencyContact.AlternatePhone = this.EmergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
            }
            if (this.SSN.IsNotNullOrEmpty())
            {
                this.SSN = this.SSN.Trim();
            }
            if (this.PhoneHomeArray.IsNotNullOrEmpty())
            {
                this.PhoneHome = this.PhoneHomeArray.ToArray().PhoneEncode();
            }
            if ( this.PhoneMobileArray.IsNotNullOrEmpty())
            {
                this.PhoneMobile = this.PhoneMobileArray.ToArray().PhoneEncode();
            }
            if (this.EthnicRaces.IsNotNullOrEmpty())
            {
                this.Ethnicities = this.EthnicRaces.ToArray().AddColons();
            }

            if (this.ServicesRequiredCollection.IsNotNullOrEmpty())
            {
                this.ServicesRequired = this.ServicesRequiredCollection.ToArray().AddColons();
            }
            if (this.PharmacyPhoneArray.IsNotNullOrEmpty())
            {
                this.PharmacyPhone = this.PharmacyPhoneArray.ToArray().PhoneEncode();
            }
            if (this.DMECollection.IsNotNullOrEmpty())
            {
                this.DME = this.DMECollection.ToArray().AddColons();
            }
        }

        #endregion

        public Patient(){}
        public Patient(OldPatient patient)
        {
            this.Id = patient.Id;
            this.PatientIdNumber = patient.PatientIdNumber;
            this.FirstName = patient.FirstName;
            this.LastName = patient.LastName;
            this.MiddleInitial = patient.MiddleInitial;
            this.SSN = patient.SSN;
            this.MedicareNumber = patient.MedicareNumber;
            this.MedicaidNumber = patient.MedicaidNumber;
            this.DOB = patient.DOB;
            this.Gender = patient.Gender;
            this.Height = patient.Height;
            this.HeightMetric = patient.HeightMetric;
            this.Weight = patient.Weight;
            this.WeightMetric = patient.WeightMetric;
            this.Ethnicities = patient.Ethnicities;
            this.MaritalStatus = patient.MaritalStatus;
            this.AddressLine1 = patient.AddressLine1;
            this.AddressLine2 = patient.AddressLine2;
            this.AddressCity = patient.AddressCity;
            this.AddressStateCode = patient.AddressStateCode;
            this.AddressZipCode = patient.AddressZipCode;
            this.PhoneHome = patient.PhoneHome;
            this.PhoneMobile = patient.PhoneMobile;
            this.EmailAddress = patient.EmailAddress;
            this.PharmacyName = patient.PharmacyName;
            this.PharmacyPhone = patient.PharmacyPhone;
            this.DME = patient.DME;
            this.OtherDME = patient.OtherDME;
            this.IsDNR = patient.IsDNR;
            this.Triage = patient.Triage;
            this.EvacuationZone = patient.EvacuationZone;
            this.PhotoId = patient.PhotoId;
            this.ServicesRequired = patient.ServicesRequired;
            this.ReferrerPhysician = patient.ReferrerPhysician;
            this.Created = patient.Created;
            this.Modified = patient.Modified;
            this.Comments = patient.Comments;
            this.IsHospitalized = patient.IsHospitalized;
            this.HospitalizationId = patient.HospitalizationId;
            this.PrivateDutyStatus = 0;
            this.HomeHealthStatus = patient.Status;
            this.Services = AgencyServices.HomeHealth;
        }
    }
}
