﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using System.Web.Script.Serialization;

    [Serializable]
    public class PatientVisitNote : IVisitNote
    {
        #region Domain

       

        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime StartDate { get; set; }
        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime EndDate { get; set; }


        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime EventDate { get; set; }

        [SubSonicIgnore]
        [ScriptIgnore]
        public DateTime VisitDate { get; set; }

        #endregion

        protected override void AddValidationRules()
        {
        }

    }
}
