﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    public class ReturnComment : AgencyBase
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid EventId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }

        public ReturnComment() {}

        public ReturnComment(Guid agencyId, Guid episodeId, Guid userId, Guid eventId, string comment)
        {
            AgencyId = agencyId;
            EpisodeId = episodeId;
            UserId = userId;
            EventId = eventId;
            Comments = comment;
        }

        [SubSonicIgnore]
        public User User { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
