﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public class PatientInsurance
    {
        public int Id { get; set; }
        public int InsuranceId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid AdmissionId { get; set; }
        public string HealthPlanId { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public int RelationId { get; set; }
        public int Type { get; set; }
        public int ParentInsuranceId { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsOpenEndOfAdmission { get; set; }
        public bool IsOpenStartOfAdmission { get; set; }
        public bool IsApplicableRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime ActualStartDate { get; set; }
        public DateTime ActualEndDate { get; set; }

        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        [SubSonicIgnore]
        public string Name { get; set; }
        [SubSonicIgnore]
        public string TypeName {
            get
            {
                if (this.Type > 0 && Enum.IsDefined(typeof(InsuranceType),this.Type))
                {
                    return ((InsuranceType) this.Type).GetDescription();
                }
                return "";
            }
        }
        [SubSonicIgnore]
        public string RelationName { get; set; }
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        [SubSonicIgnore]
        public bool IsStartDateSet { get; set; }
        [SubSonicIgnore]
        public bool IsEndDateSet { get; set; }
    }
}
