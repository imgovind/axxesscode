﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.Entities
{
    public class TaskLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientIdNumber { get; set; }
        public string PatientName { get; set; }
        public string DisciplineTaskName { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime VisitDate { get; set; }
        public string EventDateTimeRange { get; set; }
        public string VisitDateTimeRange { get; set; }
        public string StatusName { get; set; }
        public string UserName { get; set; }
        public string Type { get; set; }
    }

}
