﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Enums;

    public class MedicareEligibility :EpisodeBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Result { get; set; }
        public int Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string DisplayName { get; set; }

        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [SubSonicIgnore]
        public string AssignedTo { get; set; }

        [SubSonicIgnore]
        public string TaskName { get; set; }

        [SubSonicIgnore]
        public string EpisodeRange { get; set; }

        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                ScheduleStatus status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                return status.GetDescription();
            }
        }

        #endregion

        protected override void AddValidationRules()
        {
        }
    }
}
