﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;
    using System.ComponentModel.DataAnnotations;
    using Axxess.AgencyManagement.Entities.Enums;
    using System.Web.Script.Serialization;

    public class Authorization : PatientBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AssetId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string Number1 { get; set; }
        public string Number2 { get; set; }
        public string Number3 { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime StartDate { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime EndDate { get; set; }
        public int InsuranceId { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }

        public string SNVisit { get; set; }
        public string SNVisitCountType { get; set; }
        public string PTVisit { get; set; }
        public string PTVisitCountType { get; set; }
        public string OTVisit { get; set; }
        public string OTVisitCountType { get; set; }
        public string STVisit { get; set; }
        public string STVisitCountType { get; set; }
        public string MSWVisit { get; set; }
        public string MSWVisitCountType { get; set; }
        public string HHAVisit { get; set; }
        public string HHAVisitCountType { get; set; }
        public string DieticianVisit { get; set; }
        public string DieticianVisitCountType { get; set; }
        public string RNVisit { get; set; }
        public string RNVisitCountType { get; set; }
        public string LVNVisit { get; set; }
        public string LVNVisitCountType { get; set; }

        public bool IsDeprecated { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public AgencyServices Service { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string Branch { get; set; }
        [SubSonicIgnore]
        public string InsuranceName { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        [ScriptIgnore]
        public AgencyServices PatientServices { get; set; }
        
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => this.InsuranceId <= 0, "Insurance is required."));
            AddValidationRule(new Validation(() => this.StartDate == DateTime.MinValue, "Start date must be valid."));
            AddValidationRule(new Validation(() => this.EndDate == DateTime.MinValue, "End date must be valid."));
            AddValidationRule(new Validation(() => this.EndDate.Date < this.StartDate.Date, "End date is must be greater than start date."));
            AddValidationRule(new Validation(() => this.Number1.IsNullOrEmpty(), "Authorization number 1 is required."));
        }

        #endregion

    }
}
