﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [Serializable]
    public class PatientEmergencyContact : PatientBase
    {
        #region Members

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Relationship { get; set; }
        public bool IsPrimary { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        [SubSonicIgnore]
        public string PrimaryPhoneFormatted { get { return this.PrimaryPhone.ToPhone(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Emergency contact first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Emergency contact last name is required."));

            if (this.PrimaryPhone.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhonePrimaryArray.Count == 0, "Emergency contact primary phone is required."));
            }
        }

        #endregion

        public void Encode()
        {
            if (this.PhonePrimaryArray.Count >= 2)
            {
                this.PrimaryPhone = this.PhonePrimaryArray.ToArray().PhoneEncode();
            }
            if (this.PhoneAlternateArray.Count >= 2)
            {
                this.AlternatePhone = this.PhoneAlternateArray.ToArray().PhoneEncode();
            }
        }
    }

    public class PatientEmergencyContactGridRow
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string Relationship { get; set; }
        public string Email { get; set; }

        public PatientEmergencyContactGridRow(PatientEmergencyContact contact)
        {
            this.Id = contact.Id;
            this.FirstName = contact.FirstName;
            this.LastName = contact.LastName;
            this.Email = contact.EmailAddress.IsNotNullOrEmpty() ? contact.EmailAddress.ToLower() : string.Empty;
            this.PrimaryPhone = contact.PrimaryPhoneFormatted;
            this.Relationship = contact.Relationship.ToTitleCase();
        }
    }
}
