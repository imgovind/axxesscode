﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Xml.Serialization;

    public class Allergy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsDeprecated { get; set; }

        [XmlIgnore]
        public Guid ProfileId { get; set; }
        

    }
}
