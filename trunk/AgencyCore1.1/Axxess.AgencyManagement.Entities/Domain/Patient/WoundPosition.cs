﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using System.Web.UI.HtmlControls;

    public class WoundPosition
    {
        public int Number { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public WoundPosition()
        {
            
        }

        public WoundPosition(int number, int x, int y)
        {
            this.Number = number;
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is WoundPosition)
            {
                return (obj as WoundPosition).Number == this.Number;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Number.GetHashCode();
        }

    }
}
