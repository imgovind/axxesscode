﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class EpisodeLean : EpisodeDate
    {
        public Guid PatientId { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsActive { get; set; }
    }
}
