﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class EpisodePayor
    {
        public int Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public int PayorId { get; set; }
        public int PayorType { get; set; }
        public int ParentPayorId { get; set; }
    }
}
