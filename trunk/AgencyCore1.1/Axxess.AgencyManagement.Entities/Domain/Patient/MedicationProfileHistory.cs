﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using SubSonic.SqlGeneration.Schema;

    public class MedicationProfileHistory : PatientBase
    {
        public Guid Id { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid AssociatedAssessment { get; set; }
        public string Medication { get; set; }
        public string Allergies { get; set; }
        public string SignatureText { get; set; }
        [DataType(DataType.Date)]
        public DateTime SignedDate { get; set; }
        public bool IsDeprecated { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public Guid  UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string PhysicianData { get; set; }
        public AgencyServices Service { get; set; }

        [SubSonicIgnore]
        public List<string> PharmacyPhoneArray { get; set; }

        [SubSonicIgnore]
        public string Area 
        { 
            get
            {
                return Service.ToArea();
            }
        }

        [SubSonicIgnore]
        public string SignedDateFormatted { get { return this.SignedDate.ToShortDateString(); } }

        [SubSonicIgnore]
        public string Signature { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public Guid ProfileId { get; set; }

        protected override void AddValidationRules()
        {
        }

    }
}
