﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Enums;
    using System.ComponentModel.DataAnnotations;

    public class PatientAdmission 
    {
        public Guid Id { get; set; }
        public string PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string PhoneHome { get; set; }
        public string InsuranceId { get; set; }
        public Guid InternalReferral { get; set; }
        public string InternalReferralName { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime StartOfCareDate { get; set; }
        public DateTime DischargedDate { get; set; }
        public int Status { get; set; }
        public int Admit { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianName { get; set; }
        public string StartOfCareDateFormatted 
        { 
            get 
            { 
                return (this.Status == (int)PatientStatus.Pending || this.Status == (int)PatientStatus.NonAdmission) ? this.StatusName : this.StartOfCareDate != DateTime.MinValue ? this.StartOfCareDate.ToZeroFilled() : ""; 
            } 
        }
        public string DischargedDateFormatted 
        { 
            get 
            {
                if (this.Status == (int)PatientStatus.Pending || this.Status == (int)PatientStatus.NonAdmission)
                {
                    return string.Empty;
                }
                else if (this.DischargedDate > this.StartOfCareDate && this.DischargedDate != DateTime.MinValue)
                {
                    return this.DischargedDate.ToZeroFilled();
                }
                else
                {
                    return string.Empty;
                }
            } 
        }
        public string DisplayName
        {
            get
            {
                return string.Format("{0}, {1} {2}", this.LastName.ToUpperCase(), this.FirstName.ToUpperCase(), this.MiddleInitial.ToInitial());
            }
        }
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.Status));
            }
        }
        public string InsuranceName { get; set; }
    }
}
