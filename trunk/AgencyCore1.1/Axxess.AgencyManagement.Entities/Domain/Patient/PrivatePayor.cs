﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using MongoDB.Bson.Serialization.Attributes;

    [Serializable]
    [DataContract]
    public class PrivatePayor : PatientBase
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool IsCurrentPayor { get; set; }
        public string Relationship { get; set; }
        public string OtherRelationship { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }


        [BsonIgnore]
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return PhoneHome.ToPhone(); } }
        [BsonIgnore]
        [SubSonicIgnore]
        public string PhoneMobileFormatted { get { return PhoneMobile.ToPhone(); } }
        [BsonIgnore]
        [SubSonicIgnore]
        public string FaxNumberFormatted { get { return FaxNumber.ToPhone(); } }
        [BsonIgnore]
        [SubSonicIgnore]
        public List<string> HomePhoneArray { get; set; }
        [BsonIgnore]
        [SubSonicIgnore]
        public List<string> MobilePhoneArray { get; set; }
        [BsonIgnore]
        [SubSonicIgnore]
        public List<string> FaxPhoneArray { get; set; }

        [SubSonicIgnore]
        [XmlIgnore]
        [BsonIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        [XmlIgnore]
        [BsonIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.IsNotNullOrEmpty() ? this.AddressCity.Trim() : string.Empty, this.AddressStateCode.IsNotNullOrEmpty() ? this.AddressStateCode.Trim() : string.Empty, this.AddressZipCode.IsNotNullOrEmpty() ? this.AddressZipCode.Trim() : string.Empty);
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        [BsonIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MI.IsNotNullOrEmpty() ? " " + this.MI.ToUpperCase() + "." : string.Empty));
            }
        }

        protected override void AddValidationRules()
        {

            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "First Name is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Last Name is required.  <br />"));

            if (this.EmailAddress.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.EmailAddress.IsEmail(), "E-mail is not in a valid  format.  <br />"));
            }

            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Address line 1 is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Address City is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Address State is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Address Zip Code is required."));

            if (this.AddressZipCode.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.AddressZipCode.IsZipCode(), "Zip Code is not in a valid  format.  <br />"));
            }
            AddValidationRule(new Validation(() => !(this.Relationship.IsNotNullOrEmpty() && !this.Relationship.IsEqual("0")), "Relationship is required.  <br />"));

        }

        public void Encode()
        {
            if (this.HomePhoneArray.IsNotNullOrEmpty())
            {
                this.PhoneHome = this.HomePhoneArray.ToArray().PhoneEncode();
            }
            if (this.MobilePhoneArray.IsNotNullOrEmpty())
            {
                this.PhoneMobile = this.MobilePhoneArray.ToArray().PhoneEncode();
            }
            if (this.FaxPhoneArray.IsNotNullOrEmpty())
            {
                this.FaxNumber = this.FaxPhoneArray.ToArray().PhoneEncode();
            }
        }
    }
}
