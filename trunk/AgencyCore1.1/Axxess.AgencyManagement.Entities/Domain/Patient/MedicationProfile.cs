﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;

    public class MedicationProfile : PatientBase
    {
        public Guid Id { get; set; }
        [SubSonicIgnore]
        public string Allergies { get; set; }
        public string Medication { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanAdd { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanSign { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanDelete { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanReactivate { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanDiscontinue { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanViewList { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.Medication.IsNotNullOrEmpty())
            {
                var medications = this.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDate.ToShortDateString().ToZeroFilled()).ToList();
                medications.ForEach(m =>
                {
                    if (m.MedicationCategory.IsEqual("Active"))
                    {
                        sb.AppendFormat("{0} {1} {2} {3}", m.MedicationDosage, m.Frequency, m.Route, m.MedicationType != null ? m.MedicationType.Value : "");
                        sb.AppendLine();
                    }
                });
            }
            return sb.ToString();
        }

        protected override void AddValidationRules()
        {
        }
    }
}
