﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Enums;
    using Extensions;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    using System.Web.Script.Serialization;
    using System.Globalization;
    using System.ComponentModel.DataAnnotations;

    [XmlRoot()]
    [DataContract]
    [Serializable]
    [SubSonicTableNameOverride("scheduletasks")]
    public class ScheduleEvent : ITask
    {
        public ScheduleEvent()
        {
            this.UserId = Guid.Empty;
            this.Id = Guid.Empty;
            //this.EventDate = string.Empty;
            this.Assets = new List<Guid>();
        }

        [XmlElement("EventDate")]
        [SubSonicIgnore]
        public string EventDateXml { get { return EventDate.ToString("MM/dd/yyyy"); } set { EventDate = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlElement("VisitDate")]
        [SubSonicIgnore]
        public string VisitDateXml { get { return VisitDate.ToString("MM/dd/yyyy"); } set { VisitDate = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlIgnore]
        [DataMember]
        [UIHint("HiddenMinDate")]
        public override DateTime EventDate { get; set; }

        [XmlIgnore]
        [DataMember]
        [UIHint("HiddenMinDate")]
        public override DateTime VisitDate { get; set; }

        [XmlElement]
        [DataMember]
        public override string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public override string TimeOut { get; set; }

        [XmlElement]
        [DataMember]
        [SubSonicIgnore]
        public string MissedVisitFormReturnReason { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsPastDue
        {
            get
            {
                return (this.EventDate.IsValid() && this.EventDate.Date < DateTime.Now.Date && (this.Status == (int)ScheduleStatus.NoteNotYetDue || this.Status == (int)ScheduleStatus.OasisNotYetDue || this.Status == (int)ScheduleStatus.OrderNotYetDue));
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public override int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn))
                {
                    if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                    {
                        var outi = 12 * 60 - timeIn.Hour* 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO;
                    }
                    else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO + 12 * 60;
                    }
                    else
                    {
                        if (timeOut >= timeIn)
                        {
                            return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                        }
                    }
                }
                return 0;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid NewEpisodeId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsEpisodeReassiged { get; set; }

        

        [XmlIgnore]
        [SubSonicIgnore]
        public string Note { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string VisitType { get; set; }
       
        [XmlIgnore]
        [SubSonicIgnore]
        public Guid NextEpisodeId { get; set; }

        //[XmlIgnore]
        //[SubSonicIgnore]
        //public Guid AdmissionId { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string FirstName { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string LastName { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string MiddleInitial { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string AddressLine1 { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string AddressLine2 { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string AddressCity { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string AddressStateCode { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string AddressZipCode { get; set; }
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string PhoneHome { get; set; }
        
        
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public int PrimaryInsurance { get; set; }
        
      

        [XmlElement]
        [DataMember]
        public override string ReturnReason { get; set; }

        [SubSonicIgnore]
        public override AgencyServices Service { get { return AgencyServices.HomeHealth; } }

        protected override void AddValidationRules()
        {
            
        }

        [SubSonicIgnore]
        public override string EventDateTimeRange { get { return string.Empty; } }

        [SubSonicIgnore]
        public override string VisitDateTimeRange { get { return string.Empty; } }

      
    }
}
