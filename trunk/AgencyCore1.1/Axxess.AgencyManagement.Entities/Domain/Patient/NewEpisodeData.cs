﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class NewEpisodeData
    {

        public Guid PatientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DisplayName { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public string CaseManager { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string PrimaryPhysician { get; set; }
        public bool IsLinkedToDischarge { get; set; }
        public Guid AgencyLocationId { get; set; }
        public bool IsFromMainMenu { get; set; }
        public AgencyServices Service { get; set; }

        public string EndDateFormatted { get { return this.EndDate.ToShortDateString(); } }

        public string StartDateFormatted { get { return this.StartDate.ToShortDateString(); } }

        public List<SelectListItem> AdmissionDates { get; set; }
        public Guid AdmissionId { get; set; }

        public NewEpisodeData()
        {
            this.AdmissionDates = new List<SelectListItem>();
        }
    }
}
