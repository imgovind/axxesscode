﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot()]
    public class Physician
    {
        [XmlElement]
        public Guid Id { get; set; }
        [XmlElement]
        public bool IsPrimary { get; set; }
    }
}
