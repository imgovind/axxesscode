﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    using Enums;

    public class PatientData : IAddress
    {
        public Guid Id { get; set; }
        public int StatusId { get; set; }
        public string Phone { get; set; }
        public string PhoneMobile { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string InsuranceId { get; set; }
        public string InsuranceNumber { get; set; }
        public string InsuranceName { get; set; }

        public string AddressCity { get; set; }
        public string EmailAddress { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressStateCode { get; set; }

        public string PolicyNumber
        {
            get
            {
                return this.MedicareNumber.IsNotNullOrEmpty() ? this.MedicareNumber : this.InsuranceNumber;
            }
        }

        public string Status
        {
            get
            {
                return this.StatusId.ToEnum(PatientStatus.Active).GetDescription();
            }
        }

        public string Address
        {
            get
            {
                return new StringBuilder()
                    .Append(this.AddressLine1.IsNotNullOrEmpty() ? this.AddressLine1.ToTitleCase() + " " : string.Empty)
                    .Append(this.AddressLine2.IsNotNullOrEmpty() ? this.AddressLine2.ToTitleCase() + " " : string.Empty)
                    .Append(this.AddressCity.IsNotNullOrEmpty() ? this.AddressCity.ToTitleCase() + " " : string.Empty)
                    .Append(this.AddressStateCode.IsNotNullOrEmpty() ? this.AddressStateCode.ToUpper() + " " : string.Empty)
                    .Append(this.AddressZipCode.IsNotNullOrEmpty() ? this.AddressZipCode : string.Empty)
                    .ToString();
            }
        }
        public string AddressFullFormatted
        {
            get
            {
                if (this.Address.IsNotNullOrEmpty())
                {
                    return string.Format("{0}\n{1}, {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
                }
                return string.Empty;
            }
        }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName, " ", this.MiddleInitial.IsNotNullOrEmpty() ? this.MiddleInitial + "." : string.Empty).ToTitleCase();
            }
        }

        public bool IsDNR { get; set; }
        public string ServicePrefix { get; set; }

        //These properties are never used
        //public DateTime StartofCareDate { get; set; }
        //public string SSN { get; set; }
        //public string PhysicianFirstName { get; set; }
        //public string PhysicianLastName { get; set; }
        //public string PhysicianNPI { get; set; }
        //public Guid UserId { get; set; }
        //public Guid PhysicianId { get; set; }
        //public Guid CaseManagerId { get; set; }

    }
}
