﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AdmissionEpisode
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public DateTime DischargedDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsCurrent { get; set; }
        public bool HasDetails { get; set; }

    }
}
