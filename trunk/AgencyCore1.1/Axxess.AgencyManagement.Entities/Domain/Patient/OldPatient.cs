﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;

    [XmlRoot("Patient")]
    public class OldPatient : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        [ScriptIgnore]
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }
        [ScriptIgnore]
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public float Height { get; set; }
        public int HeightMetric { get; set; }
        public float Weight { get; set; }
        public int WeightMetric { get; set; }
        public string Ethnicities { get; set; }
        public string MaritalStatus { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string EmailAddress { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string PrimaryHealthPlanId { get; set; }
        public string SecondaryHealthPlanId { get; set; }
        public string TertiaryHealthPlanId { get; set; }

        public string PrimaryGroupName { get; set; }
        public string SecondaryGroupName { get; set; }
        public string TertiaryGroupName { get; set; }

        public string PrimaryGroupId { get; set; }
        public string SecondaryGroupId { get; set; }
        public string TertiaryGroupId { get; set; }

        public string PrimaryRelationship { get; set; }
        public string SecondaryRelationship { get; set; }
        public string TertiaryRelationship { get; set; }

        public string DME { get; set; }
        public string OtherDME { get; set; }
        public string Payer { get; set; }
        public string ServicesRequired { get; set; }
        public string PaymentSource { get; set; }
        public string OtherPaymentSource { get; set; }
        public string AdmissionSource { get; set; }
        public Guid ReferrerPhysician { get; set; }
        public DateTime ReferralDate { get; set; }
        public string OtherReferralSource { get; set; }
        public Guid InternalReferral { get; set; }
        public Guid CaseManagerId { get; set; }
        public Guid AuditorId { get; set; }
        public int Triage { get; set; }
        public string EvacuationZone { get; set; }
        public Guid PhotoId { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }
        public string Comments { get; set; }
        
        public bool IsDeprecated { get; set; }
        public bool IsHospitalized { get; set; }
        public Guid HospitalizationId { get; set; }
        public DateTime DischargeDate { get; set; }
        public int DischargeReasonId { get; set; }
        public string DischargeReason { get; set; }
        public DateTime LastEligibilityCheck { get; set; }
        public Guid AdmissionId { get; set; }
        public bool IsDNR { get; set; }
        #endregion

        #region Domain
        [XmlIgnore]
        [SubSonicIgnore]
        public Guid ReferralId { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string Branch { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.Status));
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldCreateEpisode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsFaceToFaceEncounterCreated { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldRequestElligibility { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DOBFormatted { get { return this.DOB.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string NonAdmissionDateFormatted { get { return this.NonAdmissionDate.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> EthnicRaces { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<Guid> AgencyPhysicians { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1} {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string ReferralDateFormatted { get { return this.ReferralDate.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PaymentSources { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> DMECollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public PatientEmergencyContact EmergencyContact { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<PatientEmergencyContact> EmergencyContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<AgencyPhysician> PhysicianContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneHomeArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneMobileArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PharmacyPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string SecondaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string TertiaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CaseManagerName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsDischarged { get { return this.Status == (int)PatientStatus.Discharged; } }
        //[XmlIgnore]
        //[SubSonicIgnore]
        //public string DNR { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => this.DOB == DateTime.MinValue, "Patient date of birth is required."));
            AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate == DateTime.MinValue, "Episode start date is required."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate < this.StartofCareDate, "Episode start date is must be greater or equal to the start of care date."));
            AddValidationRule(new Validation(() => !this.DOB.ToString().IsValidDate(), "Date of birth is not a valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient gender has to be selected."));
            AddValidationRule(new Validation(() => (this.EmailAddress == null ? !string.IsNullOrEmpty(this.EmailAddress) : !this.EmailAddress.IsEmail()), "Patient e-mail is not in a valid format."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            if (this.SSN.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.SSN.IsSSN(), "Patient SSN is not in valid format."));
            }
            AddValidationRule(new Validation(() => this.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.StartofCareDate.ToString()), "Patient Start of care date is required."));

            if (Ethnicities.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.EthnicRaces.Count == 0, "Patient Race/Ethnicity is required."));
            }

            if (PaymentSource.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PaymentSources.Count == 0, "Patient Payment Source is required."));
            }

            if (this.PhoneHome.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneHomeArray.Count == 0, "Patient phone is required."));
            }
            if (this.ReferralDate.IsValid())
            {
                AddValidationRule(new Validation(() => this.StartofCareDate < this.ReferralDate, "Referral date must be less than or equal to the start of care."));
            }
            AddValidationRule(new Validation(() => this.EmergencyContact == null, "Emergency contact details are required."));

        }

        #endregion

        #region Encode the multi array string field to one field

        public void Encode()
        {
            if (this.MedicareNumber.IsNotNullOrEmpty())
            {
               this.MedicareNumber = this.MedicareNumber.Trim();
            }
            if (this.MedicaidNumber.IsNotNullOrEmpty())
            {
                this.MedicaidNumber = this.MedicaidNumber.Trim();
            }
            if (this.SSN.IsNotNullOrEmpty())
            {
                this.SSN = this.SSN.Trim();
            }
            if (this.PhoneHomeArray.Count > 0)
            {
                this.PhoneHome = this.PhoneHomeArray.ToArray().PhoneEncode();
            }
            if (this.PhoneMobileArray.Count > 0)
            {
                this.PhoneMobile = this.PhoneMobileArray.ToArray().PhoneEncode();
            }
            if (this.EthnicRaces.Count > 0)
            {
                this.Ethnicities = this.EthnicRaces.ToArray().AddColons();
            }
            if (this.PaymentSources.Count > 0)
            {
                this.PaymentSource = this.PaymentSources.ToArray().AddColons();
            }
            if (this.ServicesRequiredCollection.Count > 0)
            {
                this.ServicesRequired = this.ServicesRequiredCollection.ToArray().AddColons();
            }
            if (this.PharmacyPhoneArray.Count > 0)
            {
                this.PharmacyPhone = this.PharmacyPhoneArray.ToArray().PhoneEncode();
            }
            if (this.DMECollection.Count > 0)
            {
                this.DME = this.DMECollection.ToArray().AddColons();
            }
        }

        #endregion

    }
}
