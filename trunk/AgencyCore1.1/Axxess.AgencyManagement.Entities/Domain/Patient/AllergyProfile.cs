﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
   
    public class AllergyProfile :PatientBase
    {
        public Guid Id { get; set; }
        public string Allergies { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string Type { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanDelete { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanRestore { get; set; }

        public override string ToString()
        {
            if (this.Allergies.IsNotNullOrEmpty())
            {
                var allergyList = this.Allergies.ToObject<List<Allergy>>().Where(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList();
                string allergies = allergyList.Select(a => a.Name).ToArray().Join(", ");
                if (allergies.IsNotNullOrEmpty())
                {
                    return allergies;
                }
            }
            return "NKA (Food/Drugs/Latex/Environment)";
        }

        protected override void AddValidationRules()
        {
        }
    }
}
