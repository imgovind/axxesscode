﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    public class OrderNumber
    {
        public long Id { get; set; }
        public DateTime Created { get; set; }
    }
}
