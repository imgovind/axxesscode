﻿namespace Axxess.AgencyManagement.Entities
{
    public class PatientSchedule : Patient
    {
        public PatientEpisode Episode { get; set; }
    }
}
