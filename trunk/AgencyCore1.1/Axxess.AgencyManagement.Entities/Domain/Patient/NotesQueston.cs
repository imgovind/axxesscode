﻿namespace Axxess.AgencyManagement.Entities
{

    using System;
    using System.Xml;
    using System.Xml.Serialization;

    [XmlRoot()]
    public class NotesQuestion : QuestionBase
    {
        [XmlAttribute]
        public string Type { get; set; }


        public override QuestionBase Create(string name, string answer)
        {
            return new NotesQuestion() { Name = name, Answer = answer };
        }
    }
}
