﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using System.ComponentModel.DataAnnotations;

    public class MissedVisit : SignableNoteBase
    {
        public Guid Id { get; set; }
        public Guid EpisodeId { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime Date { get; set; }
        public string Reason { get; set; }
        public string Comments { get; set; }
        public bool IsOrderGenerated { get; set; }
        public bool IsPhysicianOfficeNotified { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string Signature { get; set; }

        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }

        [SubSonicIgnore]
        public string VisitType { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public LocationPrintProfile LocationProfile { get; set; }

        [SubSonicIgnore]
        public PatientProfileLean PatientProfile { get; set; }

        [SubSonicIgnore]
        public string EventDate { get; set; }

        [SubSonicIgnore]
        public DateTime EndDate { get; set; }

        [SubSonicIgnore]
        public DateTime StartDate { get; set; }

        [SubSonicIgnore]
        public string DisciplineTaskName { get; set; }
        
        public override string ToString()
        {
            var comments = new StringBuilder();
            if (this.Reason.IsNotNullOrEmpty())
            {
                comments.AppendFormat("Missed Visit Reason: {0}. ", this.Reason);
                comments.Append("<br />");
            }

            if (this.Comments.IsNotNullOrEmpty())
            {
                comments.AppendFormat("Comments: {0}", this.Comments);
                comments.Append("<br />");
            }

            return comments.ToString().Clean();
        }

        protected override void AddValidationRules()
        {
        }
    }
}
