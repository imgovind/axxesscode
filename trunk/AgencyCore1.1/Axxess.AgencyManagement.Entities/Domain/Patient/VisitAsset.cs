﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class VisitAsset
    {
        public Guid Id { get; set; }
        public Guid AssetId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EventId { get; set; }
    }
}
