﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class HospitalizationLog : PatientBase
    {
        public Guid Id { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid UserId { get; set; }
        public string Data { get; set; }
        public int SourceId { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime ReturnDate { get; set; }
        public DateTime LastHomeVisitDate { get; set; }
        public DateTime HospitalizationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string Source { get { return SourceId.ToEnum<TransferSourceTypes>(TransferSourceTypes.User).GetDescription(); } }
        [SubSonicIgnore]
        public string UserName { get; set; }
       
        [SubSonicIgnore]
        public LocationPrintProfile Location { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public string EpisodeRange { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanPrint { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanDelete { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
