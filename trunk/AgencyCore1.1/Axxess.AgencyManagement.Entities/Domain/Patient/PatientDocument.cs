﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public class PatientDocument : PatientBase
    {
        public Guid Id { get; set; }
        public Guid AssetId { get; set; }
        public Guid UploadTypeId { get; set; }
        public bool IsDeprecated { get; set; }
        public string Filename { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string ModifiedDateFormatted { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string TypeName { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.UploadTypeId.IsEmpty(), "Type is required.<br/>"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Filename), "Document name is required.<br/>"));
        }
    }
}
