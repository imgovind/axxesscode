﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    public class QuestionData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Type { get; set; }
        public List<NotesQuestion> Question { get; set; }
        public List<NotesQuestion> WoundNoteQuestion { get; set; }

        public QuestionData(PatientVisitNote visitNote)
        {
            this.Id = visitNote.Id;
            this.AgencyId = visitNote.AgencyId;
            this.PatientId = visitNote.PatientId;
            this.EpisodeId = visitNote.EpisodeId;
            this.Type = visitNote.NoteType;
            this.Question = visitNote.Questions;
            this.WoundNoteQuestion = visitNote.WoundQuestions;
        }
    }
}
