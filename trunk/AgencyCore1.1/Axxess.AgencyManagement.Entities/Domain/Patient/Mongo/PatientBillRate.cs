﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB.Bson;

   public class PatientBillRate
    {

       public ObjectId Id { get; set; }
       public Guid AgencyId { get; set; }
       public Guid PatientId { get; set; }
       public List<ChargeRate> VisitRates { get; set; }
       public DateTime Created { get; set; }
       public DateTime Modified { get; set; }
       public bool IsDeprecated { get; set; }

    }
}
