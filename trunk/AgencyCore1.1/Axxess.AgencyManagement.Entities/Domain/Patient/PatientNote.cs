﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class PatientNote
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
