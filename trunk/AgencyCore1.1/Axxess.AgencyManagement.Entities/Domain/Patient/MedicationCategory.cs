﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class MedicationCategory
    {
     
        public string Text { get; set; }
      
        public string Value { get; set; }
    }
}
