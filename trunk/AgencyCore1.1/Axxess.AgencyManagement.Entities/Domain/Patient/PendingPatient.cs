﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class PendingPatient : EntityBase
    {
        #region Members
        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime StartofCareDate { get; set; }
        public int PrimaryInsurance { get; set; }
        public int SecondaryInsurance { get; set; }
        public int TertiaryInsurance { get; set; }
        public string Payer { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }
        public int ReasonId { get; set; }
        public string Reason { get; set; }
        public bool IsAdmit { get; set; }
        public DateTime NonAdmitDate { get; set; }
        public DateTime ReferralDate { get; set; }
        public Guid CaseManagerId { get; set; }
        public DateTime DateOfDischarge { get; set; }
        public Guid AdmissionId { get; set; }
        public DateTime Created { get; set; }

        /// <summary>
        /// user permission flags
        /// </summary>
        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanAdmit { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanNonAdmit { get; set; }


        #endregion

        #region Domain

        [SubSonicIgnore]
        public string ServicePrefix { get; set; }

        [SubSonicIgnore]
        public bool IsFaceToFaceEncounterCreated { get; set; }

        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public NonAdmitTypes Type { get; set; }
        [SubSonicIgnore]
        public string Branch { get; set; }
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public Guid PrimaryPhysician { get; set; }

        [SubSonicIgnore]
        public int PrivateDutyStatus { get; set; }
        [SubSonicIgnore]
        public int HomeHealthStatus { get; set; }
        [SubSonicIgnore]
        public AgencyServices Services { get; set; }
        [SubSonicIgnore]
        public AgencyServices CurrentService { get; set; }

        [SubSonicIgnore]
        public bool IsOnlyOneService { get; set; }

        [SubSonicIgnore]
        public string Service { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            if (this.IsAdmit)
            {
                AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));
                AddValidationRule(new Validation(() => this.EpisodeStartDate == DateTime.MinValue, "Episode start date is required."));
                AddValidationRule(new Validation(() => this.EpisodeStartDate < this.StartofCareDate, "Episode start date is must be greater or equal to the start of care date."));
                AddValidationRule(new Validation(() => this.PrimaryInsurance <= 0, "Insurance is required."));
            }
            else
            {
                AddValidationRule(new Validation(() => this.NonAdmitDate == DateTime.MinValue, "Non-Admission date is required."));
                AddValidationRule(new Validation(() => this.Reason.IsNullOrEmpty(), "Reason for Non-Admission required."));
            }
        }

        #endregion

    }
}
