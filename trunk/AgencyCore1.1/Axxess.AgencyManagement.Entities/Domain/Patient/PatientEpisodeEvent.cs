﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Axxess.AgencyManagement.Entities
{
    public class PatientEpisodeEvent
    {
        public Guid EventId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public int Task { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public string TaskName { get; set; }
        public string Type { get; set; }
        public string Group { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime EventDate { get; set; }
        public string Time { get; set; }
        public string MRN { get; set; }
        public string PatientName { get; set; }
        public string EpisodeRange { get; set; }
        public string RedNote { get; set; }
        public string BlueNote { get; set; }
        public string YellowNote { get; set; }
        public bool IsPOCNeeded { get; set; }
        public bool IsComplete { get; set; }
    }
}
