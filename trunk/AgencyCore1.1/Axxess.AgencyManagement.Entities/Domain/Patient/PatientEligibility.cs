﻿namespace Axxess.AgencyManagement.Entities
{
    using System;


    public class PatientEligibility
    {
        public RequestResult Request_Result { get; set; }
        public RequestValidation Request_Validation { get; set; }
        public FunctionalAcknowledgment Functional_Acknowledgment { get; set; }
        public MedicarePart Medicare_Part_A { get; set; }
        public MedicarePart Medicare_Part_B { get; set; }
        public Subscriber Subscriber { get; set; }
        public GenericResult Dependent { get; set; }
        public HealthBenefitPlanCoverage Health_Benefit_Plan_Coverage { get; set; }
        public Episode Episode { get; set; }
        public OtherAgencyData Other_Agency_Data { get; set; }
    }
    public class RequestResult
    {
        public string success { get; set; }
        public string response { get; set; }
    }
    public class RequestValidation
    {
        public string success { get; set; }
        public string yes_no_response_code { get; set; }
        public string reject_reason_code { get; set; }
        public string follow_up_action_code { get; set; }
    }
    public class GenericResult
    {
        public string success { get; set; }
    }
    public class FunctionalAcknowledgment
    {
        public string success { get; set; }
        public string functional_identifier_code { get; set; }
        public string transaction_set_identifier_code { get; set; }
        public string segment_id_code { get; set; }
        public string segment_syntax_error_code { get; set; }
        public string position_in_segment { get; set; }
        public string data_element_syntax_error_code { get; set; }
        public string copy_of_bad_data_element { get; set; }
        public string functional_group_acknowledge_code { get; set; }
    }
    public class MedicarePart
    {
        public string success { get; set; }
        public string date { get; set; }
        public string eligibility_or_benefit_information { get; set; }
        public string insurance_type_code { get; set; }
    }
    public class Subscriber
    {
        public string success { get; set; }
        public string date { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string identification_code { get; set; }
        public string gender { get; set; }
    }
    public class HealthBenefitPlanCoverage
    {
        public string success { get; set; }
        public string payer { get; set; }
        public string name { get; set; }
        public string insurance_type { get; set; }
        public string reference_id_qualifier { get; set; }
        public string reference_id { get; set; }
        public string date { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
    }
    public class Episode
    {
        public string success { get; set; }
        public string payer { get; set; }
        public string name { get; set; }
        public string reference_id_qualifier { get; set; }
        public string reference_id { get; set; }
        public string period_date_range { get; set; }
        public string period_start { get; set; }
        public string period_end { get; set; }
    }
    public class OtherAgencyData {
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
    }
}
