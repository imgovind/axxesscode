﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    using SubSonic.SqlGeneration.Schema;

    [Serializable]
    public class PatientAdmissionDate : PatientBase
    {
        public Guid Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartOfCareDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime DischargedDate { get; set; }
        public string PatientData { get; set; }
        public string ProfileData { get; set; }
        public int Status { get; set; }
        public int DischargeReasonId { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        /// <summary>
        /// Comma Separated Insuraces
        /// </summary>
        public string InsuranceIds { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
