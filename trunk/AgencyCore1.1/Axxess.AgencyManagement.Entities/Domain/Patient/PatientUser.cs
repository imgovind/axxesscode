﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PatientUser 
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId{ get; set; }
    }
}
