﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    public class PatientEpisodeTherapyException
    {
        public int Status { get; set; }
        public DateTime EndDate { get; set; }
        public string Schedule { get; set; }
        public DateTime StartDate { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string EventDate { get; set; }
        public int ScheduledTherapy { get; set; }
        public int CompletedTherapy { get; set; }
        public int EpisodeDay { get; set; }
        public string ThirteenVisit { get; set; }
        public string NineteenVisit { get; set; }

        public string PTEval { get; set; }
        public string STEval { get; set; }
        public string OTEval { get; set; }

        public string EpisodeRange { get { return string.Format("{0:MM/dd/yyyy}-{1:MM/dd/yyyy}", this.StartDate, this.EndDate); } }

    }
}
