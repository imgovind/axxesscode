﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    public class PatientProfileLean
    {
        public Guid AgencyId { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string MedicaidNumber { get; set; }
        public string Gender { get; set; }
        public string MedicareNumber { get; set; }
        public string PrimaryHealthPlanId { get; set; }

        public DateTime DOB { get; set; }
        public DateTime StartofCareDate { get; set; }
        public Guid AgencyLocationId { get; set; }

        public Address Address { get; set; }
        public string Phone { get; set; }

        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        

        public string DOBFormatted { get { return this.DOB.ToShortDateString().ToZeroFilled(); } }
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString().ToZeroFilled(); } }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        public bool IsDNR { get; set; }
    }

    public class Address
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode
        {
            get;
            set;
        }

        #region Domain

        public string FirstRow
        {
            get
            {
                if (this.Line1.IsNotNullOrEmpty() && this.Line2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.Line1.Trim(), this.Line2.Trim());
                }
                if (this.Line1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.Line2))
                {
                    return this.Line1.Trim();
                }
                return string.Empty;
            }

        }

        public string SecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.City.TrimWithNullable(), this.StateCode.TrimWithNullable(), this.ZipCode.TrimWithNullable());
            }
        }

        #endregion
    }
}
