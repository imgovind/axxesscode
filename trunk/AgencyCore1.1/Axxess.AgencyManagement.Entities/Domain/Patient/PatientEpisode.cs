﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public class PatientEpisode : CarePeriod
    {
       
        public string Schedule { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public bool IsRecertCompleted { get; set; }

        [SubSonicIgnore]
        public Guid AgencyLocationId { get; set; }
        [SubSonicIgnore]
        public override bool HasNext { get { return this.NextEpisode != null; } }
        [SubSonicIgnore]
        public PatientEpisode NextEpisode { get; set; }
        [SubSonicIgnore]
        public override bool HasPrevious { get { return this.PreviousEpisode != null; } }
        [SubSonicIgnore]
        public PatientEpisode PreviousEpisode { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
