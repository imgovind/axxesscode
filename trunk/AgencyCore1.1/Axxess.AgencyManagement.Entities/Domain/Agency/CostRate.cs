﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Xml.Serialization;

    [XmlRoot()]
    public class CostRate
    {
        [XmlElement]
        public string RateDiscipline { get; set; }
        [XmlElement]
        public string PerVisit { get; set; }
        [XmlElement]
        public string PerHour { get; set; }
        [XmlElement]
        public string PerUnit { get; set; }
    }
}
