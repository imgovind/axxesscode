﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [XmlRoot("License")]
    public class PhysicainLicense : EntityBase
    {
        #region Members
        public Guid Id { get; set; }
        public string LicenseNumber { get; set; }
        public string State { get; set; }
        public DateTime InitiationDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        public Guid PhysicianId { get; set; }

        [XmlIgnore]
        public string InitiationDateFormatted { get { return this.InitiationDate.ToString("MM/dd/yyyy"); } }

        [XmlIgnore]
        public string ExpirationDateFormatted { get { return this.ExpirationDate.ToString("MM/dd/yyyy"); } }

        [XmlIgnore]
        public string UserDispalyName { get; set; }

        #endregion

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LicenseNumber), "License Number is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.State), "State is required. <br />"));
            AddValidationRule(new Validation(() => !InitiationDate.IsValid(), "Issue Date is required and must be a valid date. <br />"));
            AddValidationRule(new Validation(() => !ExpirationDate.IsValid(), "Expiration Date is required and must be a valid date. <br />"));
            AddValidationRule(new Validation(() => InitiationDate > ExpirationDate, "Issue Date must be less than the Expiration Date. <br />"));
        }
    }
}
