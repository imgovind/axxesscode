﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class MessageState
    {
        public Guid Id { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeprecated { get; set; }
        public Guid FolderId { get; set; }
    }
}
