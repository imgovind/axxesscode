﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class Report : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AssetId { get; set; }
        public string Type { get; set; }
        public string Format { get; set; }
        public string Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Completed { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Type), "Report Type is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Status), "Report Status is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Format), "Report Format is required. <br />"));
        }

        #endregion

        public static Report CreateExcelReport(Guid agencyId, Guid userId, string type)
        {
            return new Report(){
                Id = Guid.NewGuid(),
                AgencyId = agencyId,
                UserId = userId,
                Type = type,
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
        }
    }
}
