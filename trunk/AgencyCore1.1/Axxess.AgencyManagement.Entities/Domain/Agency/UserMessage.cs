﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
using Axxess.AgencyManagement.Entities.Enums;

    public class UserMessage
    {
        public Guid Id { get; set; }
        public Guid MessageId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public int MessageType { get; set; }
        public Guid FolderId { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeprecated { get; set; }
        public Guid ThreadId { get; set; }

        public UserMessage() 
        {
            this.Id = Guid.NewGuid();
            this.FolderId = Guid.Empty;
        }
        public UserMessage(Guid agencyId, Guid userId, Guid messageId, MessageType messageType)
            : this()
        {
            this.AgencyId = agencyId;
            this.UserId = userId;
            this.MessageId = messageId;
            this.MessageType = (int)messageType;
        }
    }
}
