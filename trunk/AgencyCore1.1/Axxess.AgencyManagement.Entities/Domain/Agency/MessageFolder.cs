﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class MessageFolder : AgencyBase   
    {
        public Guid     Id          { get; set; }
        public Guid     ParentId    { get; set; }
        public Guid     OwnerId     { get; set; }
        public string   Name        { get; set; }
        public DateTime Created     { get; set; }


        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Folder name is required. <br />"));
        }
    }
}
