﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;

    public class MedicareEligibilitySummary :AgencyBase
    {
        #region Members
        public Guid Id { get; set; }
        public string Report { get; set; }
        public DateTime Created { get; set; }
        public bool IsDeprecated { get; set; }
        #endregion

        #region Domain
        public MedicareEligibilitySummaryData Data { get; set; }
        public string Text { get { return "Medicare Eligibility Summary"; } }
        public string PrintUrl {
            get
            {
                return "<a href=\"javascript:void(0);\"" +
                    " onclick=\"Acore.OpenPrintView({ " +
                        "Url: 'MedicareEligibility/Print/" + this.Id + "', " +
                        "PdfUrl: 'Agency/MedicareEligibilityPdf', " +
                        "PdfData: { 'Id': '" + this.Id + "'} })\"><span class=\"img icon print\"></span></a>";
            }
        }
        public string CreatedFormatted { get { return Created.ToZeroFilled(); } }
        public string AgencyName { get; set; }
        #endregion

        protected override void AddValidationRules()
        {
        }
    }
}
