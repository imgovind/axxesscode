﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core;

    public class AgencyMedicareInsurance : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        public int MedicareId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Name { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {

        }

        #endregion

    }
}
