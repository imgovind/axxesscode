﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public class AgencyAdjustmentCode : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string CreatedDateString { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public string ModifiedDateString { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public bool IsUserCanViewLog { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Code), "Code is required. <br />"));
            //AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required. <br />"));
        }

        #endregion
    }
}
