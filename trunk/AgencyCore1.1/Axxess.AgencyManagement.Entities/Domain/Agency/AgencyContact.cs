﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class AgencyContact : AgencyBase, IAddress
    {

        #region Members

        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CompanyName { get; set; }
        public string ContactType { get; set; }
        public string ContactTypeOther { get; set; }
        public string PhonePrimary { get; set; }
        public string PhoneExtension { get; set; }
        public string PhoneAlternate { get; set; }
        public string FaxNumber { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region IAddress members

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string EmailAddress { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string PhonePrimaryFormatted { get { return this.PhonePrimary.ToPhone(); } }
        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }
        [SubSonicIgnore]
        public string FaxNumberFormatted { get { return this.FaxNumber.ToPhone(); } }
        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", this.AddressLine1.TrimWithNullable(), this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.TrimWithNullable(), this.AddressLine2.TrimWithNullable());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.TrimWithNullable();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
        [SubSonicIgnore]
        public string PhonePrimaryExtended 
        {
            get
            {
                return this.PhoneExtension.IsNotNullOrEmpty() ? string.Concat(this.PhonePrimaryFormatted, " x", this.PhoneExtension) : this.PhonePrimaryFormatted;
            }
        }

        [SubSonicIgnore]
        public bool IsUserCanViewLog { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "First name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Last name is required."));

            if (this.PhonePrimary.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhonePrimaryArray.Count == 0, "Primary Phone is required."));
            }
        }

        #endregion

    }

    public class AgencyContactGridRow
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }

        public AgencyContactGridRow(AgencyContact contact)
        {
            this.Id = contact.Id;
            this.Name = contact.DisplayName;
            this.Company = contact.CompanyName;
            this.Type = contact.ContactType;
            this.Phone = contact.PhonePrimaryExtended;
            this.Fax = contact.FaxNumberFormatted;
            this.Email = contact.EmailAddress.IsNotNullOrEmpty() ? contact.EmailAddress.ToLower() : string.Empty;
            this.Comments = contact.Comments;
        }
    }

}
