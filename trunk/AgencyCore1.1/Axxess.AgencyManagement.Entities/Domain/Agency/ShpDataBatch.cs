﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    public class ShpDataBatch : PatientBase
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime Created { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .AppendFormat("Id: {0} ", Id)
                .AppendFormat("AgencyId: {0} ", AgencyId)
                .AppendFormat("PatientId: {0} ", PatientId)
                .AppendFormat("Status: {0} ", Status)
                .AppendFormat("Created: {0} ", Created.ToZeroFilled())
                .AppendFormat("Modified: {0}", LastModified.ToZeroFilled())
                .ToString();
        }

        protected override void AddValidationRules()
        {
        }
    }
}
