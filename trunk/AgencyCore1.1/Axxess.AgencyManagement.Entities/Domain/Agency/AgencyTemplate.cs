﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    [Serializable]
    public class AgencyTemplate : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain

        [IgnoreDataMember]
        [SubSonicIgnore]
        public string CreatedDateString { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        [IgnoreDataMember]
        public string ModifiedDateString { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public bool IsUserCanViewLog { get; set; }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is AgencyTemplate))
            {
                return false;
            }
            return (obj as AgencyTemplate).Id == this.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Title), "Title is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required. <br />"));
        }

        #endregion

    }
}
