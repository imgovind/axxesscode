﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Xml.Serialization;
    using Axxess.AgencyManagement.Entities.Enums;

    public class Incident : SignableNoteBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid EpisodeId { get; set; }
        public string IndividualInvolved { get; set; }
        public string IndividualInvolvedOther { get; set; }
        public string IncidentType { get; set; }
        public string Description { get; set; }
        public string ActionTaken { get; set; }
        public string Orders { get; set; }
        public string MDNotified { get; set; }
        public string FamilyNotified { get; set; }
        public string NewOrdersCreated { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        public string FollowUp { get; set; }
        #endregion

        #region Domain
        [SubSonicIgnore]
        public List<string> IndividualInvolvedArray { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string IncidentDateFormatted { get { return IncidentDate.ToZeroFilled(); } }

        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }

        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }

        [SubSonicIgnore]
        public LocationPrintProfile LocationProfile { get; set; }

        [SubSonicIgnore]
        public PatientProfileLean PatientProfile { get; set; }


        [SubSonicIgnore]
        public bool IsUserCanPrint { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanEdit { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanDelete { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanSeeSticky { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanAddPhysicain { get; set; }


        [SubSonicIgnore]
        public string StatusComment { get; set; }

        [SubSonicIgnore]
        public string StatusName { get; set; }

        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required. "));
            AddValidationRule(new Validation(() => this.EpisodeId.IsEmpty(), "Episode is required. "));
            AddValidationRule(new Validation(() => this.IncidentType.IsNullOrEmpty(), "Incident type is required. "));
            AddValidationRule(new Validation(() => !IncidentDate.IsValid(), "Incident Date is not valid."));
        }

        #endregion
    }
}
