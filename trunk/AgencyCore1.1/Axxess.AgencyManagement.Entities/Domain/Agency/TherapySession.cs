﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public sealed class TherapySession
    {
        public Guid LoginId { get; set; }
        public string DisplayName { get; set; }
        public Guid AgencyTherapyId { get; set; }
    }
}
