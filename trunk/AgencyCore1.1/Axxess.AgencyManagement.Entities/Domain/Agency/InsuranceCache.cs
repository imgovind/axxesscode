﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;


    public class InsuranceCache
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PayorType { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
