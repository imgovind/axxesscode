﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using Axxess.Core.Extension;
    public class AgencyInfo
    {
        public string Name { get; set; }
    }
}
