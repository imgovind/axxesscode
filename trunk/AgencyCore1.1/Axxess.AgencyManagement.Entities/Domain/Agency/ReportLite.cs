﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class ReportLite
    {
        #region Members

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AssetId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public string Status { get; set; }
        public string Created { get; set; }
        public string Completed { get; set; }

        #endregion
        
    }
}
