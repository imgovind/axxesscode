﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class CustomerNote : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid LoginId { get; set; }
        public string Comments { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string RepName { get; set; }
        [SubSonicIgnore]
        public string Preview
        {
            get
            {
                if (this.Comments.IsNotNullOrEmpty())
                {
                    return this.Comments.Length >= 75 ? string.Format("{0}...", this.Comments.Substring(0, 74)) : this.Comments;
                }
                return string.Empty;
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Comments), "Note Comments is required."));
        }

        #endregion
    }
}
