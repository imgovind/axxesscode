﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Xml.Serialization;

    [XmlRoot()]
    public class VisitRate
    {
        [XmlElement]
        public string RateDiscipline { get; set; }
        [XmlElement]
        public string Charge { get; set; }
        [XmlElement]
        public string ChargeType { get; set; }
    }
}
