﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class AgencySupply : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Code { get; set; }
        public double UnitCost { get; set; }
        public string RevenueCode { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public bool IsUserCanViewLog { get; set; }


        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Description), "Description is required. <br />"));
        }

        #endregion

    }
}
