﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using Axxess.Core.Enums;

    public class ReportPermissionRow
    {
        public string Description { get; set; }
        public string ToolTip { get; set; }
        public int Id { get; set; }
        public AgencyServices AvailableForServices { get; set; }
        public AgencyServices ViewChecked { get; set; }
        public AgencyServices ExportChecked { get; set; }
    }
}
