﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Web.Script.Serialization;
    using Enums;
    using Axxess.Core.Enums;

    public class UserVisitWidget
    {
        public Guid EventId { get; set; }
        public Guid PatientId { get; set; }
        public string TaskName { get; set; }
        public string Type { get; set; }
        public string EventDate { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsMissedVisit { get; set; }

        public string PatientName { get; set; }

        [ScriptIgnore]
        public int Status { get; set; }
        [ScriptIgnore]
        public Guid EpisodeId { get; set; }
        [ScriptIgnore]
        public int DisciplineTask { get; set; }

        public bool IsUserCanEdit { get; set; }
        public AgencyServices Service { get; set; }
    }
}
