﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class UserLicense
    {
        public Guid UserId { get; set; }
        public List<License> Licenses { get; set; }

    }
}
