﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;
    using Axxess.Core.Extension;

    public class SelectedPatient
    {
        public Guid PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid PatientUserId { get; set; }
        public Guid AgencyTeamId { get; set; }
        public bool Selected { get; set; }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
    }
}
