﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using Axxess.Core.Enums;

    public class PermissionRow
    {
        public string Description { get; set; }
        public string ToolTip { get; set; }
        public int ChildId { get; set; }
        public int Checked { get; set; }
    }
}
