﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;

    using Axxess.Core.Extension;
    using Enums;

    [XmlRoot()]
    [DataContract]
    public class UserEvent
    {
        public UserEvent()
        {
            this.EventDate = string.Empty;
        }

        [XmlElement]
        [DataMember]
        public Guid EventId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid EpisodeId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid PatientId { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }    
   
        [XmlElement]
        [DataMember]
        public string EventDate { get; set; }

        [XmlElement]
        [DataMember]
        public string VisitDate { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeOut { get; set; }

        [XmlElement]
        [DataMember]
        public string ReturnReason { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), this.DisciplineTask)).GetDescription(); } else { return string.Empty; };
            }
        }

        [XmlIgnore]
        public string StatusName
        {
            get
            {
                int check;
                if (int.TryParse(this.Status, out check))
                {
                    if ((this.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString()) && this.EventDate.IsValidDate() && this.EventDate.ToDateTime().Date < DateTime.Now.Date)
                    {
                        return ScheduleStatus.CommonNotStarted.GetDescription();
                    }
                    else
                    {
                        return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), check));
                    }
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        public string EpisodeDetails { get; set; }

        [XmlIgnore]
        public string EpisodeSchedule { get; set; }

        [XmlIgnore]
        public DateTime EpisodeStartDate { get; set; }

        [XmlIgnore]
        public DateTime EpisodeEndDate { get; set; }
     }
}
