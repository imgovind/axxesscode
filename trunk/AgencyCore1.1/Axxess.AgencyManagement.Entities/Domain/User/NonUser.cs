﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Infrastructure;

    public class NonUser : AgencyBase
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return LastName + ", " + FirstName;
            }
        }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "First Name is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Last Name is required.  <br />"));
        }
    }
}
