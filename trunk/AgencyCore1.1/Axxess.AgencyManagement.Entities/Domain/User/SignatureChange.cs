﻿namespace Axxess.AgencyManagement.Entities
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System;

    [Serializable]
    public class SignatureChange
    {
        public string CurrentSignature { get; set; }
        public string NewSignature { get; set; }
        public string NewSignatureConfirm { get; set; }
    }
}
