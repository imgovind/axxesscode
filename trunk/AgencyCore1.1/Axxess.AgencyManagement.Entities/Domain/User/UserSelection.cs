﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class UserSelection
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Credential { get; set; }
        public string LoginCreated { get; set; }
        public string DisplayName { get; set; }
        public bool IsLoginActive { get; set; }

        public UserSelection()
        {
            
        }

        public UserSelection(User user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            DisplayName = user.DisplayName;
        }
    }
}
