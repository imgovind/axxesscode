﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class UserGroup
    {
        public int Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid LocationId { get; set; }
        public string Name { get; set; }
        public AgencyServices Services { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
