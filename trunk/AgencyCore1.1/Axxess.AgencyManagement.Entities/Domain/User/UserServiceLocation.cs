﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;


   public class UserServiceLocation
    {
       public int Service { get; set; }
       public bool IsAllLocation { get; set; }
       public List<Guid> Locations { get; set; }
    }

   public class UserLocationService
   {
       public Guid Location { get; set; }
       public List<int> Services { get; set; }
      // public bool IsAllLocation { get; set; }
   }
}
