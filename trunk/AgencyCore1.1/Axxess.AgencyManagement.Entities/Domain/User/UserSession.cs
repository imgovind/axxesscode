﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Axxess.Core.Enums;

    [Serializable]
    public sealed class UserSession
    {
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string SessionId { get; set; }
        //public string Address { get; set; }
        public string FullName { get; set; }
        public string AgencyName { get; set; }
        public string DisplayName { get; set; }
        public string AgencyRoles { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsAgencyFrozen { get; set; }
        //public int MaxAgencyUserCount { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public DateTime AccountExpirationDate { get; set; }
        public string ImpersonatorName { get; set; }
        public bool OasisVendorExist { get; set; }
        public AgencyServices AcessibleServices { get; set; }
        public AgencyServices PreferredService { get; set; }
        public AgencyServices Services { get; set; }
        public string DBServerIp { get; set; }
        public string Payor { get; set; }
        public int LoginDay { get; set; }
        public string EarliestLoginTime { get; set; }
        public IDictionary<int, Dictionary<int,  List<int>>> Permissions { get; set; }
       // public IDictionary<int, Dictionary<int, List<int>>> ReportPermissions { get; set; }
    }


    [Serializable]
    public sealed class UserActiveSession
    {
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public List<UserLocation> Locations { get; set; }
        public IDictionary<int, Dictionary<int, List<int>>> ReportPermissions { get; set; }
      
    }


}
