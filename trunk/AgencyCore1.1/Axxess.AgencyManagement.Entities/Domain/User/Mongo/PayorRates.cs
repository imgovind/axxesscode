﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    public class PayorRates
    {
        /// <summary>
        /// insurance id
        /// </summary>
        public int PayorId { get; set; }
        public List<UserRate> Rates { get; set; }
    }
}
