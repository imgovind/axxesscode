﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;

    public class UserPayorRates
    {
        public ObjectId Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        public int PayorId { get; set; }
        public List<UserRate> PayorRates { get; set; }
        [BsonIgnore]
        public string InsuranceName { get; set; }
    }
}
