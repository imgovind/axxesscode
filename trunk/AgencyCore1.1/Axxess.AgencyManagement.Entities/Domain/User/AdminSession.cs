﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public sealed class AdminSession
    {
        public Guid LoginId { get; set; }
        public string DisplayName { get; set; }
        public bool IsAxxessAdmin { get; set; }
        public bool IsAxxessSupport { get; set; }
    }
}
