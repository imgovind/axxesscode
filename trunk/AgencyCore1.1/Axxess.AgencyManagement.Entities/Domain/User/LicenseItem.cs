﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;
    using Axxess.Core;
    [Serializable]
    [DataContract]
    public class License : AgencyBase
    {
       
        #region Members

        [XmlIgnore]
        public new Guid AgencyId { get; set; }
        public Guid Id { get; set; }
        public Guid AssetId { get; set; }

        [XmlIgnore]
        public Guid UserId { get; set; }
        [XmlIgnore]
        public string LastName { get; set; }
        [XmlIgnore]
        public string FirstName { get; set; }

        public string LicenseNumber { get; set; }
        public string LicenseType { get; set; }
        [UIHint("HiddenMinDate")]
        [XmlElement("InitiationDate")]
        public DateTime IssueDate { get; set; }
        [UIHint("HiddenMinDate")]
        [XmlElement("ExpirationDate")]
        public DateTime ExpireDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        [XmlIgnore]
        public bool IsNonUser { get; set; }
        

        #endregion

        #region Domain

        [SubSonicIgnore]
        [XmlIgnore]
        public string OtherLicenseType { get; set; }
        [SubSonicIgnore]
        [XmlIgnore]
        public string CustomId { get; set; }
        [SubSonicIgnore]
        [XmlIgnore]
        public string AssetUrl
        {
            get
            {
                if (!this.AssetId.IsEmpty())
                {
                    return string.Format("<a class=\"link\" href=\"/Asset/{0}\">{1}</a>&#160;", this.AssetId.ToString(), this.LicenseType);
                }
                return this.LicenseType;
            }
        }

        
        private string displayName;
        [SubSonicIgnore]
        [XmlIgnore]
        public string DisplayName
        {
            get
            {
                if (displayName.IsNullOrEmpty() && LastName.IsNotNullOrEmpty() && FirstName.IsNotNullOrEmpty())
                {
                    return LastName + ", " + FirstName;
                }
                return this.displayName;
            }
            set
            {
                this.displayName = value;
            }
        }
        [SubSonicIgnore]
        [XmlIgnore]
        public string CustomDisplayName
        {
            get
            {
                return DisplayName + (CustomId.IsNotNullOrEmpty() ? " - " + CustomId : "");
            }
        }


        [SubSonicIgnore]
        [XmlIgnore]
        public string IsUser
        {
            get
            {
                return this.IsNonUser ? "No" : "Yes";
            }
        }

        #endregion

        protected override void AddValidationRules()
        {
        }
    }
}

