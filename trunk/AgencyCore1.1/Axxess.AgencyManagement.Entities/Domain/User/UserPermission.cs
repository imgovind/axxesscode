﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public abstract class UserPermission
    {
        public Guid UserId { get; set; }
        //public List<string> Permissions { get; set; }

        /// <summary>
        /// This one used to capture all the user services
        /// </summary>
        public AgencyServices AvailableServices { get; set; }
       
    }

    public class GeneralPermission: UserPermission
    {
        public bool IsAllReportsSelected { get; set; }
        public List<IGrouping<ParentPermissionCategory, IGrouping<ParentPermissionGroupKey, PermissionRow>>> Permissions { get; set; }
        public List<IGrouping<ReportPermissionGroupKey, ReportPermissionRow>> ReportPermissions { get; set; }
        public List<UserLocation> ServiceLocations { get; set; }
    }

    public class ReportPermission : UserPermission
    {
        public Dictionary<int, Dictionary<int, List<int>>> Permissions { get; set; }
    }
}
