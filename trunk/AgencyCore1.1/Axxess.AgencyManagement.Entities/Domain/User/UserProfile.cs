﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class UserProfile
    {
        #region Members        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public DateTime EmploymentStartDate { get; set; }
        public DateTime TerminationDate { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string Ethnicity { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneFax { get; set; }
        public string EmailPersonal { get; set; }
        public bool HasMilitaryService { get; set; }
        public string DriversLicenseNumber { get; set; }
        public string DriversLicenseStateCode { get; set; }
        public DateTime DriversLicenseExpiration { get; set; }
        public Guid PhotoAssetId { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                return string.Format("{0} {1} {2} {3} {4}", this.AddressLine1.IsNotNullOrEmpty() ? this.AddressLine1.Trim() : string.Empty, this.AddressLine2.IsNotNullOrEmpty() ? this.AddressLine2.Trim() : string.Empty, this.AddressCity.IsNotNullOrEmpty() ? this.AddressCity.Trim() : string.Empty, this.AddressStateCode.IsNotNullOrEmpty() ? this.AddressStateCode.Trim() : string.Empty, this.AddressZipCode.IsNotNullOrEmpty() ? this.AddressZipCode.Trim() : string.Empty);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Address
        {
            get
            {
                return string.Format("{0} {1} ", this.AddressLine1.IsNotNullOrEmpty() ? this.AddressLine1.Trim() : string.Empty, this.AddressLine2.IsNotNullOrEmpty() ? this.AddressLine2.Trim() : string.Empty);
            }
        }

        [SubSonicIgnore]
        public List<string> HomePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> MobilePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> FaxPhoneArray { get; set; }

        #endregion

        public void Encode()
        {
            if (this.HomePhoneArray != null && this.HomePhoneArray.Count > 0)
            {
                this.PhoneHome = this.HomePhoneArray.ToArray().PhoneEncode();
            }

            if (this.MobilePhoneArray != null && this.MobilePhoneArray.Count > 0)
            {
                this.PhoneMobile = this.MobilePhoneArray.ToArray().PhoneEncode();
            }

            if (this.FaxPhoneArray != null && this.FaxPhoneArray.Count > 0)
            {
                this.PhoneFax = this.FaxPhoneArray.ToArray().PhoneEncode();
            }
        }
    }
}
