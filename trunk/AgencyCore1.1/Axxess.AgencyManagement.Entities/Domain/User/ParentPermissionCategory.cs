﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    public class ParentPermissionCategory
    {
        public string Name { get; set; }
        public bool IsSubCategory { get; set; }
        public bool IsAllSelected { get; set; }

        public ParentPermissionCategory(string name, bool isSubCategory)
        {
            Name = name;
            IsSubCategory = isSubCategory;
        }

        public override bool Equals(object obj)
        {
            if (obj is ParentPermissionCategory)
            {
                return (obj as ParentPermissionCategory).Name == this.Name;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
