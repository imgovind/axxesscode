﻿namespace Axxess.AgencyManagement.Entities
{
    using Axxess.Core.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class ReportPermissionGroupKey
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public AgencyServices ServicesInCategory { get; set; }
        public AgencyServices AllViewSelected { get; set; }
        public AgencyServices AllExportSelected { get; set; }
        public AgencyServices ServicesSelected
        {
            get
            {
                return AllViewSelected & AllExportSelected;
            }
        }
        public bool IsAllSelected
        {
            get
            {
                return ServicesSelected == ServicesInCategory;
            }
        }


        public ReportPermissionGroupKey(int id, string name)
        {
            Id = id;
            Name = name;
            ServicesInCategory = AgencyServices.None;
        }

        public override bool Equals(object obj)
        {
            if (obj is ReportPermissionGroupKey)
            {
                return (obj as ReportPermissionGroupKey).Id == this.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
