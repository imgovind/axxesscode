﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum UserType
    {
        LoginUser = 1,
        NonLoginUser = 2
    }
}
