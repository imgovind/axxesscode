﻿namespace Axxess.AgencyManagement.Entities
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System;

    [Serializable]
    public class PasswordChange
    {
        [Required]
        [DisplayName("E-mail")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Current Password")]
        public string CurrentPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string NewPasswordConfirm { get; set; }
    }
}
