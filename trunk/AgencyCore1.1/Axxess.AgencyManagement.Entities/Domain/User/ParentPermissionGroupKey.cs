﻿namespace Axxess.AgencyManagement.Entities
{
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class ParentPermissionGroupKey
    {
        private string name;
        public string Name
        {
            get
            {
                if (name.IsNullOrEmpty())
                {
                    name = ((ParentPermission)Id).GetDescription();
                }
                return name;
            }
        }

        public int Id { get; set; }
        public int AllSelected { get; set; }
        public List<int> AvailableForServices { get; set; }
        public bool IsAllSelected { get; set; }
        public bool HasServiceScope { get; set; }

        public ParentPermissionGroupKey(int id, bool hasServiceScope, AgencyServices availableForServices)
        {
            Id = id;
            HasServiceScope = hasServiceScope;
            this.AvailableForServices = hasServiceScope ? availableForServices.ToIntList() : new List<int>(){ (int)AgencyServicesBoundary.All };
        }

        public override bool Equals(object obj)
        {
            if (obj is ParentPermissionGroupKey)
            {
                return (obj as ParentPermissionGroupKey).Id == this.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
