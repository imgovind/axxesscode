﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using System.Runtime.Serialization;
    using Axxess.Core.Enums;

    [Serializable]
    [DataContract]
    public class UserLocation : AgencyBase
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        /// <summary>
        /// This value may br different a calculated value of this value, location service and user accessbile service
        /// </summary>

        public AgencyServices Services { get; set; }

        ///// <summary>
        ///// used to keep the current location services
        ///// </summary>
        //[SubSonicIgnore]
        //public int LocationServices { get; set; }
        [SubSonicIgnore]
        public string LocationName { get; set; }
        [SubSonicIgnore]
        public bool IsMainOffice { get; set; }
        [SubSonicIgnore]
        public bool IsLocationStandAlone { get; set; }
        [SubSonicIgnore]
        public string Payor { get; set; }


        protected override void AddValidationRules()
        {
            throw new NotImplementedException();
        }
    }
}
