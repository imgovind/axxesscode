﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    public class UserAndNonUserLite
    {
        #region Public Properties

        public string Credentials { get; set; }

        public string CredentialsOther { get; set; }

        public string DisplayName
        {
            get
            {
                var nameBuilder = new StringBuilder();

                if (Credentials.IsEqual("None"))
                {
                    if (FirstName.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0} ", FirstName.ToTitleCase());
                    }
                    if (LastName.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0} ", LastName.ToTitleCase());
                    }
                    if (Suffix.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0}", Suffix);
                    }

                    return nameBuilder.ToString().TrimEnd();
                }

                if (CredentialsOther.IsNotNullOrEmpty())
                {
                    if (FirstName.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0} ", FirstName.ToTitleCase());
                    }
                    if (LastName.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0} ", LastName.ToTitleCase());
                    }
                    if (Suffix.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0} ", Suffix);
                    }
                    if (CredentialsOther.IsNotNullOrEmpty())
                    {
                        nameBuilder.AppendFormat("{0}", CredentialsOther);
                    }
                    return nameBuilder.ToString().TrimEnd();
                }

                if (FirstName.IsNotNullOrEmpty())
                {
                    nameBuilder.AppendFormat("{0} ", FirstName.ToTitleCase());
                }
                if (LastName.IsNotNullOrEmpty())
                {
                    nameBuilder.AppendFormat("{0} ", LastName.ToTitleCase());
                }
                if (Suffix.IsNotNullOrEmpty())
                {
                    nameBuilder.AppendFormat("{0} ", Suffix);
                }
                if (Credentials.IsNotNullOrEmpty())
                {
                    nameBuilder.AppendFormat("{0}", Credentials);
                }
                return nameBuilder.ToString().TrimEnd();
            }
        }

        public string FirstName { get; set; }

        public Guid Id { get; set; }

        public bool IsNonUser { get; set; }

        public string LastName { get; set; }

        public string Suffix { get; set; }

        #endregion
    }
}