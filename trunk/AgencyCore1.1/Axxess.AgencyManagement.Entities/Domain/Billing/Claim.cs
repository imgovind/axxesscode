﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    [DataContract]
    public class Claim
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public int PrimaryInsuranceId { get; set; }

        public double ProspectivePay { get; set; }
        public string HippsCode { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime ClaimDate { get; set; }
        public string Schedule { get; set; }
        public int Status { get; set; }
        public Guid CaseManagerId { get; set; }
        public string CaseManager { get; set; }
        public Guid ClinicianId { get; set; }
        public string Clinician { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }
        [SubSonicIgnore]
        public string DateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }


        [DataMember]
        public double ClaimAmount { get; set; }
        [DataMember]
        [UIHint("HiddenMinDate")]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public double PaymentAmount { get; set; }



    }
}
