﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    public class RapBill : Claim
    {
        public bool IsFirstBillableVisit { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsVerified { get; set; }
    }
}
