﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Enums;
   

    public class ManagedClaimPayment : PatientBase
    {
        public Guid Id { get; set; }
        public Guid ClaimId { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }
        [SubSonicIgnore]
        public string PaymentDateFormatted { 
            get {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToZeroFilled() : "";
            } 
        }
        public int Payor { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        [SubSonicIgnore]
        public string PaymentAmount { get; set; }
        [SubSonicIgnore]
        public string PayorName { get; set; }
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public DateTime EpisodeEndDate { get; set; }
        [SubSonicIgnore]
        public string ClaimDateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => !PaymentDate.IsValid(), "Payment date must be a valid date."));
            AddValidationRule(new Validation(() => this.Payor == 0, "Payor is required."));
        }
    }
}
