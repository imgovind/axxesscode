﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    public class ClaimHistoryLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime PaymentDate { get; set; }
        public double PaymentAmount { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public double ClaimAmount { get; set; }
        public int InvoiceType { get; set; }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus),this.Status)? ((BillingStatus)this.Status).GetDescription():string.Empty;

            }
        }
        public string TypeName
        {
            get
            {
                return ((ClaimTypeSubCategory)this.Type).ToString();
            }
        }
        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool HasDetails { get; set; }
    }
}
