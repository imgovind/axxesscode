﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Enums;



    public class ManagedClaimAdjustment :PatientBase
    {
        #region Members
        public Guid Id { get; set; }
        public Guid ClaimId { get; set; }
        public double Adjustment { get; set; }
        public Guid TypeId { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        #endregion

        #region Domain

        [SubSonicIgnore]
        public string Type { get; set; }
        [SubSonicIgnore]
        public string Description { get; set; }

        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        #endregion

        protected override void AddValidationRules()
        {
        }
    }
}
