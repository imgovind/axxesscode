﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot()]
    public class ClaimInfo
    {
        [XmlElement]
        public Guid ClaimId { get; set; }
        [XmlElement]
        public Guid PatientId { get; set; }
        [XmlElement]
        public Guid EpisodeId { get; set; }
        [XmlElement]
        public string ClaimType { get; set; }
        [XmlElement]
        public string ClaimStatus { get; set; }


    }
}
