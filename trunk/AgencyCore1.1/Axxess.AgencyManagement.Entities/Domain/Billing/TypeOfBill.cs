﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    public class TypeOfBill
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string SortData { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public bool IsDischarged { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string EpisodeRange { get; set; }
        public string PatientDisplayName { get { return string.Format("{0}, {1} {2}", this.LastName, this.FirstName, this.MiddleInitial); } }
    }
    //public class ClaimBill
    //{
    //    public Guid Id { get; set; }
    //    public int Status { get; set; }
    //    public Guid PatientId { get; set; }
    //    public Guid EpisodeId { get; set; }
    //    public string LastName { get; set; }
    //    public bool IsVerified { get; set; }
    //    public string PatientDisplayName { get { return string.Format("{0}, {1} {2}", this.LastName, this.FirstName, this.MiddleInitial); } }
    //    public string MiddleInitial { get; set; }
    //    public string FirstName { get; set; }
    //    public bool IsRapGenerated { get; set; }
    //    public bool IsOasisComplete { get; set; }
    //    public string PatientIdNumber { get; set; }
    //    public bool AreOrdersComplete { get; set; }
    //    public bool AreVisitsComplete { get; set; }
    //    public DateTime EpisodeEndDate { get; set; }
    //    public DateTime EpisodeStartDate { get; set; }
    //    public bool IsFirstBillableVisit { get; set; }
    //    public double ProspectivePay { get; set; }
    //    public string HippsCode { get; set; }
    //    public DateTime ClaimDate { get; set; }
    //    public string Schedule { get; set; }
    //    public bool IsVisitVerified { get; set; }
    //    public bool IsSupplyVerified { get; set; }
    //    public bool IsFinalInfoVerified { get; set; }
    //    public Guid CaseManagerId { get; set; }
    //    public string CaseManager { get; set; }
    //    public Guid ClinicianId { get; set; }
    //    public string Clinician { get; set; }
    //    public string EpisodeRange { get { return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy")); } }
    //}
}
