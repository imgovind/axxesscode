﻿namespace Axxess.AgencyManagement.Entities
{
    public class Taxonomy
    {
        public int Id { get; set; }
        public string TaxoId { get; set; }
        public string TaxoClass { get; set; }
        public string TaxoSubclass { get; set; }
    }
}
