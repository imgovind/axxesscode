﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using Enums;

    public class ClaimData : AgencyBase
    {
        public long Id { get; set; }
        public string Data { get; set; }
        public string BillIdentifers { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ClaimType {get; set;}
        public string Response { get; set; }
        public bool IsDeprecated { get; set; }

        protected override void AddValidationRules()
        {
        }
    }
}
