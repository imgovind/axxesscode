﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Enums;


    public class ManagedClaim : BaseClaim
    {
        public string InsuranceIdNumber { get; set; }
        public string Type { get; set; }
        [SubSonicIgnore]
        public string MiddleInitial { get; set; }

        public  bool AreVisitsComplete { get; set; }
        public  bool IsRapGenerated { get; set; }
        public  bool AreOrdersComplete { get; set; }
        public  bool IsOasisComplete { get; set; }
        public string VerifiedVisits { get; set; }
        public string Supply { get; set; }
        public double SupplyTotal { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }

        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public string SupplyCode { get; set; }
       
       
        public bool IsHomeHealthServiceIncluded { get; set; }
        public int InvoiceNumber { get; set; }

        [SubSonicIgnore]
        public bool HasMultipleEpisodes { get; set; }
        [SubSonicIgnore]
        public List<ITask> Visits { get; set; }
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }


        [SubSonicIgnore]
        public override ClaimTypeSubCategory ClaimSubCategory { get { return ClaimTypeSubCategory.ManagedCare; } }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient First Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient Last Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PatientIdNumber), "Patient Medical Record Number is required."));

            if (this.Service == AgencyServices.PrivateDuty && this.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
            }
            else
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.InsuranceIdNumber), "Patient Insurance/Medicare Number is required."));
            }
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient Gender is required."));
            AddValidationRule(new Validation(() => !this.DOB.IsValid() || (this.DOB.Date <= DateTime.MinValue.Date), "Patient DOB is not valid date."));
            if (this.DOB.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.DOB.Date < DateTime.Now.Date), "Patient DOB cannot be a future date."));
            }
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date <= DateTime.MinValue.Date, "Claim Start Date is not valid."));
            AddValidationRule(new Validation(() => this.EpisodeEndDate.Date <= DateTime.MinValue.Date, "Claim End Date is not valid."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date > this.EpisodeEndDate.Date, "Claim Start Date must be less than the Claim End Date."));

            if (this.StartofCareDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.EpisodeStartDate.Date >= this.StartofCareDate.Date), "Episode Start Date has to be greater than or equal to the Patient Admission date."));
                AddValidationRule(new Validation(() => !(this.StartofCareDate.Date <= DateTime.Now.Date), "Admission Date has to be less than or equal to Today's Date."));
                if (this.DOB.IsValid())
                {
                    AddValidationRule(new Validation(() => !(this.StartofCareDate.Date > this.DOB.Date), "Admission Date has to be greater than the Patient's DOB."));
                }
            }

            AddValidationRule(new Validation(() => (this.FirstBillableVisitDate.Date <= DateTime.MinValue.Date), "First Billable Visit date is not valid date."));
            if (this.FirstBillableVisitDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.StartofCareDate.Date), "First Billable Visit date has to be greater than or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.EpisodeStartDate.Date), "First Billable Visit date has to be greater than or equal to Episode Start Date."));
            }
            if (UB4PatientStatusFactory.Discharge().Contains(this.UB4PatientStatus))
            {
                AddValidationRule(new Validation(() => (this.DischargeDate.Date < DateTime.MinValue.Date), "Discharge Date to be valid date."));
                AddValidationRule(new Validation(() => (this.DischargeDate.Date <= this.EpisodeStartDate.Date), "Discharge Date must be greater than the Episode Start Date."));
            }
            AddValidationRule(new Validation(() => !this.StartofCareDate.IsValid() || (this.StartofCareDate.Date <= DateTime.MinValue.Date), "Admission Date is not valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianLastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianFirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianNPI), "Physician NPI is required."));
        }

        #endregion
    }
}
