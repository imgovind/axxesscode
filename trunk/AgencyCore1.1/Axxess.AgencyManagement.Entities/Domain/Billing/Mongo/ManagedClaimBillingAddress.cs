﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class ManagedClaimBillingAddress
    {
        /// <summary>
        /// claim id
        /// </summary>
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public PrivatePayor Payor { get; set; }
        public List<ChargeRate> Rates { get; set; }

    }
}
