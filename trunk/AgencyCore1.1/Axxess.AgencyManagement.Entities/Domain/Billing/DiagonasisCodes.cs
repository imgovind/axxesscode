﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using System.Xml;
    using System.Xml.Serialization;

    [XmlRoot("DiagonasisCodes")]
    public class DiagnosisCodes
    {
        [XmlElement("code1")]
        public string Primary { get; set; }
        [XmlElement("code2")]
        public string Second { get; set; }
        [XmlElement("code3")]
        public string Third { get; set; }
        [XmlElement("code4")]
        public string Fourth { get; set; }
        [XmlElement("code5")]
        public string Fifth { get; set; }
        [XmlElement("code6")]
        public string Sixth { get; set; }

    }
}
