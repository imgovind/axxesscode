﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using Enums;
    using Axxess.Core.Enums;


    public class Bill
    {
        public Bill()
        {
            this.Claims = new List<Claim>();
        }
        public IList<Claim> Claims { get; set; }
        public bool IsElectronicSubmssion { get; set; }
        public Guid BranchId { get; set; }
        public int Insurance { get; set; }
        public bool IsMedicareHMO { get; set; }
        public ClaimTypeSubCategory ClaimType { get; set; }

        public string BranchName { get; set; }
        public string InsuranceName { get; set; }

        public Agency Agency { get; set; }
        public AgencyServices Service { get; set; }

        public bool IsUserCanExport { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanGenerate { get; set; }
        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanView { get; set; }
        public bool IsUserCanAdd { get; set; }
    }
}
