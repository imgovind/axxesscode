﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities.Enums;
    public class AllClaimData
    {
        public List<Claim> Claims { get; set; }
        public List<AgencyLocation> Locations { get; set; }
        public List<InsuranceCache> Insurances { get; set; }
        public bool IsAllLocation { get; set; }
        public bool IsAllInsurance { get; set; }
        public ClaimTypeSubCategory ClaimType { get; set; }
    }
}
