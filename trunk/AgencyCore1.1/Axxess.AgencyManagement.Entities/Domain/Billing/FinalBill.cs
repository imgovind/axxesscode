﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class FinalBill : Claim
    {
        public bool IsRapGenerated { get; set; }

        public bool AreOrdersComplete { get; set; }
        public bool AreVisitsComplete { get; set; }

        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }

    }
}
