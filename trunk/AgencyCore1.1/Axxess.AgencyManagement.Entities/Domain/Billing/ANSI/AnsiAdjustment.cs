﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiAdjustment
    {
        public string Group { get; set; }
        public string Reason { get; set; }
        public string Amount { get; set; }
        public string Quantity { get; set; }
    }
}
