﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiClaim
    {
        public bool claim_is_secondary_claim { get; set; }
        public Guid claim_id { get; set; }
        public string claim_type { get; set; }
        public string claim_physician_upin { get; set; }
        public string claim_physician_last_name { get; set; }
        public string claim_physician_first_name { get; set; }
        public string claim_first_visit_date { get; set; }
        public string claim_episode_start_date { get; set; }
        public string claim_episode_end_date { get; set; }
        public string claim_hipps_code { get; set; }
        public string claim_oasis_key { get; set; }
        public string hmo_plan_id { get; set; }
        public string claim_group_name { get; set; }
        public string claim_group_Id { get; set; }
        public string claim_hmo_auth_key { get; set; }
        public string claim_hmo_auth_key2 { get; set; }
        public string claim_hmo_auth_key3 { get; set; }
        public string claim_diagnosis_code1 { get; set; }
        public string claim_diagnosis_code2 { get; set; }
        public string claim_diagnosis_code3 { get; set; }
        public string claim_diagnosis_code4 { get; set; }
        public string claim_diagnosis_code5 { get; set; }
        public string claim_diagnosis_code6 { get; set; }
        public string claim_condition_code18 { get; set; }
        public string claim_condition_code19 { get; set; }
        public string claim_condition_code20 { get; set; }
        public string claim_condition_code21 { get; set; }
        public string claim_condition_code22 { get; set; }
        public string claim_condition_code23 { get; set; }
        public string claim_condition_code24 { get; set; }
        public string claim_condition_code25 { get; set; }
        public string claim_condition_code26 { get; set; }
        public string claim_condition_code27 { get; set; }
        public string claim_condition_code28 { get; set; }
        public string claim_admission_source_code { get; set; }
        public string claim_patient_status_code { get; set; }
        public string claim_dob { get; set; }
        public List<object> claim_ub04locator81 { get; set; }
        public List<object> claim_ub04locator39 { get; set; }
        public List<object> claim_ub04locator31 { get; set; }
        public List<object> claim_ub04locator32 { get; set; }
        public List<object> claim_ub04locator33 { get; set; }
        public List<object> claim_ub04locator34 { get; set; }
        public List<object> claim_hcfalocator33 { get; set; }
        public bool claim_supply_isBillable { get; set; }
        public double claim_supply_value { get; set; }
        public List<AnsiSupply> claim_supplies { get; set; }
        public double claim_total_charge_amount { get; set; }
        public List<AnsiVisit> claim_visits { get; set; }

        public string claim_remit_date { get; set; }
        public string claim_remit_id { get; set; }
        public double claim_total_adjustment_amount { get; set; }

    }
}
