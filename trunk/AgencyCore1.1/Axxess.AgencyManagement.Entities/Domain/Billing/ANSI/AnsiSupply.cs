﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiSupply
    {
        public string date { get; set; }
        public string revenue { get; set; }
        public string hcpcs { get; set; }
        public double amount { get; set; }
        public int units { get; set; }
        public string modifier { get; set; }
    }
}
