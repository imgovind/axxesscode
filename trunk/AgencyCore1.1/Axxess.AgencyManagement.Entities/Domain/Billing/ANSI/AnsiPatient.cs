﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiPatient
    {
        public string patient_gender { get; set; }
        public string patient_medicare_num { get; set; }
        public string patient_record_num { get; set; }
        public string patient_dob { get; set; }
        public string patient_doa { get; set; }
        public string patient_dod { get; set; }
        public string patient_address { get; set; }
        public string patient_address2 { get; set; }
        public string patient_city { get; set; }
        public string patient_state { get; set; }
        public string patient_zip { get; set; }
        public string patient_cbsa { get; set; }
        public string patient_last_name { get; set; }
        public string patient_first_name { get; set; }
        public string patient_middle_initial { get; set; }
        public List<AnsiClaim> claims_arr { get; set; }
    }
}
