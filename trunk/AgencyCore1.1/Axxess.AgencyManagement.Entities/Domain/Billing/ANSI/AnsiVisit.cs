﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiVisit
    {
        public DateTime date { get; set; }
        public string type { get; set; }
        public string revenue { get; set; }
        public string hcpcs { get; set; }
        public int units { get; set; }
        public double amount { get; set; }
        public string modifier1 { get; set; }
        public string modifier2 { get; set; }
        public string modifier3 { get; set; }
        public string modifier4 { get; set; }
        public List<AnsiAdjustment> adjustments { get; set; }
    }
}
