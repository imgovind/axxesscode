﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class AnsiAgency
    {
        public string format { get; set; }
        public string submit_type { get; set; }
        public string user_login_name { get; set; }
        public string hmo_payer_id { get; set; }
        public string hmo_payer_name { get; set; }
        public string hmo_submitter_id { get; set; }
        public string hmo_provider_id { get; set; }
        public string hmo_other_provider_id { get; set; }
        public string hmo_provider_subscriber_id { get; set; }
        // hmo_additional_codes = additionaCodes,
        public string payer_id { get; set; }
        public string payer_name { get; set; }
        public string submitter_id { get; set; }
        public bool insurance_is_axxess_biller { get; set; }
        public string clearing_house_id { get; set; }
        public string provider_claim_type { get; set; }
        public string interchange_receiver_id { get; set; }
        public string clearing_house { get; set; }
        public string claim_billtype { get; set; }
        public string submitter_name { get; set; }
        public string submitter_phone { get; set; }
        public string submitter_fax { get; set; }
        public string user_agency_name { get; set; }
        public string user_tax_id { get; set; }
        public string user_national_provider_id { get; set; }
        public string user_address_1 { get; set; }
        public string user_address_2 { get; set; }
        public string user_city { get; set; }
        public string user_state { get; set; }
        public string user_zip { get; set; }
        public string user_phone { get; set; }
        public string user_fax { get; set; }
        public string user_CBSA_code { get; set; }
        public long ansi_837_id { get; set; }
        public List<AnsiPatient> patients_arr { get; set; }
    }
}
