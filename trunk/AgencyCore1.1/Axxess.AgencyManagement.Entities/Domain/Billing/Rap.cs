﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;
  

    public class Rap : BaseClaim
    {
        public bool AreOrdersComplete { get; set; }
        public bool IsOasisComplete { get; set; }

        public bool IsVerified { get; set; }
        public string MedicareNumber { get; set; }
        public int Type { get; set; }
        public string Remittance { get; set; }

        [SubSonicIgnore]
        public override string Ub04Locator31 { get; set; }
        [SubSonicIgnore]
        public override string Ub04Locator32 { get; set; }
        [SubSonicIgnore]
        public override string Ub04Locator33 { get; set; }
        [SubSonicIgnore]
        public override string Ub04Locator34 { get; set; }

        [SubSonicIgnore]
        public override int PrivatePayorId { get; set; }
        [SubSonicIgnore]
        public override string HCFALocators { get; set; }
        [SubSonicIgnore]
        public override int PayorType { get; set; }
        [SubSonicIgnore]
        public override bool IsBillingAddressDifferent { get; set; }
      
        [SubSonicIgnore]
        public string NotVerifiedVisits { get; set; }
        [SubSonicIgnore]
        public string FirstBillableVisitDateFormat { get; set; }
        [SubSonicIgnore]
        public List<RapSnapShot> SnapShots { get; set; }
       
        [SubSonicIgnore]
        public LocationPrintProfile LocationProfile { get; set; }
        [SubSonicIgnore]
        public string AdmissionSourceDisplay { get; set; }

        [SubSonicIgnore]
        public override ClaimTypeSubCategory ClaimSubCategory { get { return ClaimTypeSubCategory.RAP; } }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.MedicareNumber), "Patient medicare number is required."));
            AddValidationRule(new Validation(() => (this.PrimaryInsuranceId <= 0), "Patient insurance  is required."));
            if (this.IsMedicareHMO)
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.HealthPlanId), "Patient insurance health plan Id is required."));
                //AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AuthorizationNumber), "Patient health plan Id authorization number is required."));
            }
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PatientIdNumber), "Patient record number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient gender has to be selected."));
            AddValidationRule(new Validation(() => !this.DOB.IsValid() || (this.DOB.Date <= DateTime.MinValue.Date), "DOB is not valid date."));
            if (this.DOB.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.DOB.Date <DateTime.Now.Date), "DOB has to be less than todays date."));
            }
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AdmissionSource), "Admission source is required."));
            AddValidationRule(new Validation(() => this.AdmissionSource.IsNotNullOrEmpty() && !this.AdmissionSource.IsInteger(), "Admission source is required."));
            if (this.AdmissionSource.IsNotNullOrEmpty() && this.AdmissionSource.IsInteger())
            {
                AddValidationRule(new Validation(() => this.AdmissionSource.ToInteger()<=0, "Admission source is required."));
            }
            AddValidationRule(new Validation(() => !this.StartofCareDate.IsValid()|| (this.StartofCareDate.Date <= DateTime.MinValue.Date), "Admission Date is not valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.HippsCode), "HIPPS Code is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ClaimKey), "OASIS  matching key is required."));
            AddValidationRule(new Validation(() => !this.EpisodeStartDate.IsValid() || (this.EpisodeStartDate.Date <= DateTime.MinValue.Date), "Episode start date is not valid date."));
            if (this.StartofCareDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.EpisodeStartDate.Date >= this.StartofCareDate.Date), "Episode start date has to be greater than or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.StartofCareDate.Date <= DateTime.Now), "Admission date has to be less than or equal to todays date."));
                if (this.DOB.IsValid())
                {
                    AddValidationRule(new Validation(() => !(this.StartofCareDate.Date > this.DOB.Date), "Admission date has to be greater than DOB."));
                }
            }
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstBillableVisitDateFormat), "First billable visit date is required."));
            if (this.FirstBillableVisitDateFormat.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.FirstBillableVisitDateFormat.IsValidDate()|| (this.FirstBillableVisitDateFormat.ToDateTime().Date <= DateTime.MinValue.Date), "First billable visit date is not valid date."));
                if (this.FirstBillableVisitDateFormat.IsValidDate())
                {
                    AddValidationRule(new Validation(() => !(this.FirstBillableVisitDateFormat.ToDateTime().Date >= this.StartofCareDate.Date), "First billable visit date has to be greater or equal to Admission date."));
                    AddValidationRule(new Validation(() => !(this.FirstBillableVisitDateFormat.ToDateTime().Date >= this.EpisodeStartDate.Date), "First billable visit date has to be greater or equal to episode start date."));
                }
            }

            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianLastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianFirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianNPI), "Physician NPI is required."));
            if (this.PhysicianNPI.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhysicianNPI.Length!=10, "Physician NPI is not the right lenght(10)."));
                if (this.PhysicianNPI.Length == 10)
                {
                    AddValidationRule(new Validation(() => !this.PhysicianNPI.IsDigitsOnly(), "Physician NPI is not the right format (only digits allowed)."));
                }
            }
        }

        #endregion
    }
}
