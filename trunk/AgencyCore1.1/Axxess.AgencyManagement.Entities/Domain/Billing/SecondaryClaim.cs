﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Enums;

    public class SecondaryClaim : BaseClaim
    {
       

        public Guid PrimaryClaimId { get; set; }
        public string InsuranceIdNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

       
        public string MedicareNumber { get; set; }
        public double ClaimAmount { get; set; }
        public int SecondaryInsuranceId { get; set; }
        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public bool IsRemittanceVerified { get; set; }
        public int Type { get; set; }
        public string SupplyCode { get; set; }
        public string Remittance { get; set; }
        public int InvoiceType { get; set; }
        public string Adjustments { get; set; }
        public string RemitId { get; set; }
        public DateTime RemitDate { get; set; }
        public double TotalAdjustmentAmount { get; set; }
        public bool IsHomeHealthServiceIncluded { get; set; }

        [SubSonicIgnore]
        public override int PrimaryInsuranceId { get; set; }



        [SubSonicIgnore]
        public override int PrivatePayorId { get; set; }
        [SubSonicIgnore]
        public override int PayorType { get; set; }
        [SubSonicIgnore]
        public override bool IsBillingAddressDifferent { get; set; }

        public string VerifiedVisits { get; set; }
        public string Supply { get; set; }
        public double SupplyTotal { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
     
        //[SubSonicIgnore]
        //public bool IsPatientDischarged { get; set; }
        //[SubSonicIgnore]
        //public string Visits { get; set; }
        //[SubSonicIgnore]
        //public string FirstBillableVisitDateFormat { get; set; }
        //[SubSonicIgnore]
        //public List<FinalSnapShot> SnapShots { get; set; }
        //[SubSonicIgnore]
        //public Guid BranchId { get; set; }

        [SubSonicIgnore]
        public string Range
        {
            get
            {
                return string.Concat(this.StartDate.ToString("MM/dd/yyyy"), " - ", this.EndDate.ToString("MM/dd/yyyy"));
            }
        }
        [SubSonicIgnore]
        public string ClaimDateFormatted 
        {
            get
            {
                return this.ClaimDate != DateTime.MinValue ? this.ClaimDate.ToZeroFilled() : string.Empty;
            }
        }
        [SubSonicIgnore]
        public string PaymentDateFormatted 
        {
            get
            {
                return this.PaymentDate != DateTime.MinValue ? this.PaymentDate.ToZeroFilled() : string.Empty;
            }
        }
        [SubSonicIgnore]
        public Remittance RemittanceData { get; set; }
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        [SubSonicIgnore]
        public override ClaimTypeSubCategory ClaimSubCategory { get { return ClaimTypeSubCategory.Secondary; } }

        #region Validation Rules
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient First Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient Last Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PatientIdNumber), "Patient Medical Record Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.InsuranceIdNumber), "Patient Medicare Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient Gender is required."));
            AddValidationRule(new Validation(() => !this.DOB.IsValid() || (this.DOB.Date <= DateTime.MinValue.Date), "Patient DOB is not valid date."));
            if (this.DOB.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.DOB.Date < DateTime.Now.Date), "Patient DOB cannot be a future date."));
            }
            AddValidationRule(new Validation(() => this.StartDate.Date <= DateTime.MinValue.Date, "Claim Start Date is not valid."));
            AddValidationRule(new Validation(() => this.EndDate.Date <= DateTime.MinValue.Date, "Claim End Date is not valid."));
            AddValidationRule(new Validation(() => this.StartDate.Date > this.EndDate.Date, "Claim Start Date must be less than the Claim End Date."));

            if (this.StartofCareDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.StartDate.Date >= this.StartofCareDate.Date), "Claim Start Date has to be greater than or equal to the Patient Admission date."));
                AddValidationRule(new Validation(() => !(this.StartofCareDate.Date <= DateTime.Now.Date), "Admission Date has to be less than or equal to Today's Date."));
                if (this.DOB.IsValid())
                {
                    AddValidationRule(new Validation(() => !(this.StartofCareDate.Date > this.DOB.Date), "Admission Date has to be greater than the Patient's DOB."));
                }
            }

            AddValidationRule(new Validation(() => (this.FirstBillableVisitDate.Date <= DateTime.MinValue.Date), "First Billable Visit date is not valid date."));
            if (this.FirstBillableVisitDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.StartofCareDate.Date), "First Billable Visit date has to be greater than or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.StartDate.Date), "First Billable Visit date has to be greater than or equal to Start Date."));
            }
            if (UB4PatientStatusFactory.Discharge().Contains(this.UB4PatientStatus))
            {
                AddValidationRule(new Validation(() => (this.DischargeDate.Date < DateTime.MinValue.Date), "Discharge Date to be valid date."));
                AddValidationRule(new Validation(() => (this.DischargeDate.Date <= this.EpisodeStartDate.Date), "Discharge Date must be greater than the Episode Start Date."));
            }
            AddValidationRule(new Validation(() => !this.StartofCareDate.IsValid() || (this.StartofCareDate.Date <= DateTime.MinValue.Date), "Admission Date is not valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianLastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianFirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianNPI), "Physician NPI is required."));
        }
        #endregion
    }
}