﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class ConditionCodes
    {
        public string ConditionCode18 { get; set; }
        public string ConditionCode19 { get; set; }
        public string ConditionCode20 { get; set; }
        public string ConditionCode21 { get; set; }
        public string ConditionCode22 { get; set; }
        public string ConditionCode23 { get; set; }
        public string ConditionCode24 { get; set; }
        public string ConditionCode25 { get; set; }
        public string ConditionCode26 { get; set; }
        public string ConditionCode27 { get; set; }
        public string ConditionCode28 { get; set; }
    }
}
