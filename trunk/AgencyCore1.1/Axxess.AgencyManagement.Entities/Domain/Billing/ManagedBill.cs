﻿
namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Enums;

   public class ManagedBill : Claim
    {
        public bool IsInfoVerified { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsSupplyVerified { get; set; }

        public string VerifiedVisits { get; set; }

        public bool IsGenerated { get; set; }

        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(ManagedClaimStatus), this.Status) ? ((ManagedClaimStatus)this.Status).GetDescription() : string.Empty;

            }
        }
    }
}
