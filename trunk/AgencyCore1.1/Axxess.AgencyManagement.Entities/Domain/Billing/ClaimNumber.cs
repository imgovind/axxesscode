﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class ClaimNumber
    {
        public int Id { get; set; }
        public Guid EntityId { get; set; }

        public ClaimNumber(){}
        public ClaimNumber(Guid entityId)
        {
            this.EntityId = entityId;
        }
    }
}
