﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;

    public abstract class BaseClaim : EpisodeBase
    {
        public Guid Id { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }

        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }

        public bool IsGenerated { get; set; }
        public DateTime ClaimDate { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public DateTime StartofCareDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public string AdmissionSource { get; set; }
        public int PatientStatus { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }
        public int Status { get; set; }
        public string Insurance { get; set; }

        public virtual int PrimaryInsuranceId { get; set; }
       
        /// <summary>
        /// Used to identify self payor from other insurance
        /// </summary>
        public virtual int PayorType { get; set; }
        public virtual bool IsBillingAddressDifferent { get; set; }

        /// <summary>
        /// Used to identify self payor from other insurance
        /// </summary>

        public virtual int PrivatePayorId { get; set; }

        //public virtual bool AreVisitsComplete { get; set; }
        //public virtual bool IsRapGenerated { get; set; }
        //public virtual bool AreOrdersComplete { get; set; }
        //public virtual bool IsOasisComplete { get; set; }
        //public string VerifiedVisits { get; set; }
        //public string Supply { get; set; }
        //public double SupplyTotal { get; set; }
        //public bool IsVisitVerified { get; set; }
        //public bool IsSupplyVerified { get; set; }

        // public string MedicareNumber { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }

        public string DiagnosisCode { get; set; }
        public string ConditionCodes { get; set; }

        public double ProspectivePay { get; set; }
        public string UB4PatientStatus { get; set; }

        public string HealthPlanId { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string Relationship { get; set; }
       
        public Guid Authorization { get; set; }
        public string AuthorizationNumber { get; set; }
        public string AuthorizationNumber2 { get; set; }
        public string AuthorizationNumber3 { get; set; }

        public string Ub04Locator81cca { get; set; }
        public string Ub04Locator39 { get; set; }
      
        public virtual string Ub04Locator31 { get; set; }
        public virtual string Ub04Locator32 { get; set; }
        public virtual string Ub04Locator33 { get; set; }
        public virtual string Ub04Locator34 { get; set; }
        public virtual string HCFALocators { get; set; }

        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
     
        //public bool IsVerified { get; set; }
        public string AssessmentType { get; set; }
      
        public string CBSA { get; set; }
        //public string Remittance { get; set; }
       
        public string Comment { get; set; }
        public string Remark { get; set; }
       
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }


        [SubSonicIgnore]
        public virtual PrivatePayor PrivatePayor { get; set; }

        [SubSonicIgnore]
        public string EpisodeRange
        {
            get
            {
                return string.Concat(this.EpisodeStartDate.ToString("MM/dd/yyyy"), " - ", this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }

        [SubSonicIgnore]
        public DiagnosisCodes DiagnosisCodesObject { get; set; }

        [SubSonicIgnore]
        public ConditionCodes ConditionCodesObject { get; set; }


        //[SubSonicIgnore]
        //public string FirstBillableVisitDateFormat { get; set; }

        //[SubSonicIgnore]
        //public List<RapSnapShot> SnapShots { get; set; }

        //[SubSonicIgnore]
        //public bool IsMedicareHMO { get; set; }

        //[SubSonicIgnore]
        //public Guid BranchId { get; set; }

        //[SubSonicIgnore]
        //public Agency Agency { get; set; }

        //[SubSonicIgnore]
        //public string AdmissionSourceDisplay { get; set; }

        [SubSonicIgnore]
        public int InvoiceType { get; set; }
        [SubSonicIgnore]
        public List<SelectListItem> Authorizations { get; set; }
        [SubSonicIgnore]
        public Guid AgencyLocationId { get; set; }
        //Used for prospective payment calculation
        [SubSonicIgnore]
        public string LocationZipCode { get; set; }
        [SubSonicIgnore]
        public AgencyInsurance AgencyInsurance { get; set; }
        //[SubSonicIgnore]
        //public int UnitType { get; set; }
       
        [SubSonicIgnore]
        public bool IsMedicareHMO { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator31 { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator32 { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator33 { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator34 { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator39 { get; set; }
        [SubSonicIgnore]
        public List<Locator> Locator81cca { get; set; }
        [SubSonicIgnore]
        public List<Locator> LocatorHCFA { get; set; }

        [SubSonicIgnore]
        public List<ChargeRate> VisitRates { get; set; }
        [SubSonicIgnore]
        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillVisitDatas { get; set; }
        [SubSonicIgnore]
        public List<BillSchedule> BillVisitSummaryDatas { get; set; }

        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }

        [SubSonicIgnore]
        public abstract ClaimTypeSubCategory ClaimSubCategory { get; }

    }
}
