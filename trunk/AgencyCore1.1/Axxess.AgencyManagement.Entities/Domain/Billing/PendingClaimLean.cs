﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using Axxess.Core.Extension;
   using System.ComponentModel.DataAnnotations;

    using Enums;

    [KnownType(typeof(PendingClaimLean))]
    public class PendingClaimLean
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public Guid PatientId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string AssessmentType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string HippsCode { get; set; }
        [UIHint("Status")]
        public int Status { get; set; }
        [UIHint("Date")]
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
        public string ClaimDate { get; set; }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public bool HasDetails { get; set; }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus), this.Status) ? ((BillingStatus)this.Status).GetDescription() : string.Empty;
            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName, " ", this.MiddleInitial);
            }
        }

    }
}
