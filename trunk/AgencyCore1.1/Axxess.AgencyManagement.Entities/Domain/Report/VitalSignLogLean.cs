﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;

    public class VitalSignLogLean
    {
        public Guid Id { get; set; }
        public Guid EntityId { get; set; }
        public Guid PatientId { get; set; }
        public DateTime Date { get; set; }

        public int? Pulse { get; set; }
        public int? Resp { get; set; }
        public double? Temp { get; set; }
        public int TempUnit { get; set; }
        public string BloodPressure { get; set; }

        public VitalSignLogLean(VitalSignLog log)
        {
            Id = log.Id;
            EntityId = log.EntityId;
            PatientId = log.PatientId;
            Date = log.VisitDate;
            Temp = log.Temp;
            TempUnit = log.TempUnit;
            Resp = log.Resp;
            Pulse = log.PulseToDisplay();
            BloodPressure = log.MostRecentBloodPressure();
        }
    }
}
