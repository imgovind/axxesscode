﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Axxess.Core.Extension;

    using SubSonic.SqlGeneration.Schema;

    public class VitalSignLog : PatientBase
    {
        public Guid Id { get; set; }

        public Guid EntityId { get; set; }

        public DateTime VisitDate { get; set; }

        public double? Temp { get; set; }

        public int TempUnit { get; set; }

        public int? TempLocation { get; set; }

        public int? Resp { get; set; }

        public int RespStatus { get; set; }

        public double? Weight { get; set; }

        public int WeightUnit { get; set; }

        public double? Height { get; set; }

        public int HeightUnit { get; set; }

        public int? BloodGlucose { get; set; }

        public int? BloodGlucoseType { get; set; }

        public double? OxygenSaturation { get; set; }

        public int? OxygenSaturationType { get; set; }

        public int? Pain { get; set; }

        public int? Pulse { get; set; }

        public int? PulseLocation { get; set; }

        public int? PulseStatus { get; set; }

        public int? Pulse2 { get; set; }

        public int? PulseLocation2 { get; set; }

        public int? PulseStatus2 { get; set; }

        public string BloodPressure { get; set; }

        public int? BloodPressureSide { get; set; }

        public int? BloodPressurePosition { get; set; }

        public string BloodPressure2 { get; set; }

        public int? BloodPressureSide2 { get; set; }

        public int? BloodPressurePosition2 { get; set; }

        public string BloodPressure3 { get; set; }

        public int? BloodPressureSide3 { get; set; }

        public int? BloodPressurePosition3 { get; set; }

        public string BloodPressure4 { get; set; }

        public int? BloodPressureSide4 { get; set; }

        public int? BloodPressurePosition4 { get; set; }

        public string BloodPressure5 { get; set; }

        public int? BloodPressureSide5 { get; set; }

        public int? BloodPressurePosition5 { get; set; }

        public string BloodPressure6 { get; set; }

        public int? BloodPressureSide6 { get; set; }

        public int? BloodPressurePosition6 { get; set; }

        public string Comments { get; set; }

        public bool IsDeprecated { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        #region Domain Members

        [SubSonicIgnore]
        public bool IsNew { get; set; }

        [SubSonicIgnore]
        public bool VitalSignsChanged { get; set; }

        private static readonly PropertyInfo[] Properties;

        #endregion

        static VitalSignLog()
        {
            Properties = typeof(VitalSignLog).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
        }

        public VitalSignLog()
        {
           
        }

        public VitalSignLog(bool isNew)
        {
            this.IsNew = isNew;
        }

        protected override void AddValidationRules()
        {
            Rules.Add(new Axxess.Core.Infrastructure.Validation(() => this.Temp > 0 && this.TempUnit == 0, "The temperature's unit type must be selected."));
            Rules.Add(new Axxess.Core.Infrastructure.Validation(() => this.Weight > 0 && this.WeightUnit == 0, "The weight's unit must be selected."));
        }

        public Dictionary<string, NotesQuestion> ToQuestionDictionary()
        {
            var questions = new Dictionary<string, NotesQuestion>();
            foreach (var propertyInfo in Properties)
            {
                string name = propertyInfo.Name;
                if (name == "Id" || name == "EntityId" || name == "VisitDate" || name == "Created" || name == "Modified" || name == "IsDeprecated" || name == "IsNew" || name == "VitalSignsChanged")
                {
                     continue;
                }
                MethodInfo getMethod = propertyInfo.GetGetMethod();
                object value = null;
                if (getMethod != null)
                {
                    value = getMethod.Invoke(this, null);
                }
                questions.Add(name, new NotesQuestion() { Answer = value.ToSafeString(), Name = "VitalSign" + name });
            }
            return questions;
        }
    } 
}
