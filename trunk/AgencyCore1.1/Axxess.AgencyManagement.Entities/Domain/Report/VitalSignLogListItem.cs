﻿using System;
using System.Collections.Generic;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Entities.Domain;

namespace Axxess.AgencyManagement.Entities
{

    public class VitalSignLogListItem
    {
        public DateTime Date { get; set; }
        public string EmployeeName { get; set; }
        public string Task { get; set; }
        public string MinBP { get; set; }
        public string MaxBP { get; set; }
        public List<BloodPressure> BloodPressures { get; set; }
        public List<Pulse> RadialPulses { get; set; }
        public List<Pulse> ApicalPulses { get; set; } 

        public int? Pulse { get; set; }
        public double TempNum { get; set; }
        public string Temp { get; set; }
        public int? Resp { get; set; }
        public int? BS { get; set; }
        public double? Weight { get; set; }
        public string WeightUnit { get; set; }
        public int? Pain { get; set; }
        public double? OxygenSaturation { get; set; }

        public bool IsTimeDisplayed { get; set; }

        public VitalSignLogListItem()
        {
            BloodPressures = new List<BloodPressure>();
            RadialPulses = new List<Pulse>();
            ApicalPulses = new List<Pulse>();
        }

        /// <summary>
        /// Constructor for setting the old vital sign to a new one
        /// </summary>
        /// <param name="vitalSign"></param>
        public VitalSignLogListItem(VitalSign vitalSign)    
        {
            BS = Math.Abs(vitalSign.BSMax) > 0 ? (int?)vitalSign.BSMax : null;
            Pulse = vitalSign.ApicalPulse != 0 ? vitalSign.ApicalPulse : (vitalSign.RadialPulse != 0 ? (int?)vitalSign.RadialPulse : null);
            TempNum = vitalSign.Temp;
            Temp = Math.Abs(vitalSign.Temp) > 0 ?  vitalSign.Temp + " °" + (vitalSign.Temp > 80 ? "F" : "C") : null;
            Resp = vitalSign.Resp > 0 ? (int?)vitalSign.Resp : null;
            Weight = Math.Abs(vitalSign.Weight) > 0 ? (double?)vitalSign.Weight : null;
            OxygenSaturation = vitalSign.OxygenSaturation.IsNotNullOrEmpty() ? (double?)vitalSign.OxygenSaturation.ToDoubleAfterNonDigitStrip() : null;
            Date = vitalSign.VisitDate;
            EmployeeName = vitalSign.UserDisplayName;
            Task = vitalSign.DisciplineTask;
            Pain = vitalSign.PainLevel != -1 ? (int?)vitalSign.PainLevel : null;
        }

        public void AddPulse(int? pulse, int? location, int? status)
        {
            if (pulse.HasValue)
            {
                var loc = location.HasValue ? location.Value.ToEnum(PulseLocation.Apical) : PulseLocation.Apical;
                var pulseObj = new Pulse() { Value = pulse.Value, Location = loc, Status = status };
                if (loc == PulseLocation.Apical)
                {
                    ApicalPulses.Add(pulseObj);
                }
                else
                {
                    RadialPulses.Add(pulseObj);
                }
            }
        }

        public void AddBloodPressure(string bloodpressure, int? position, int? side)
        {
            if (bloodpressure.IsNotNullOrEmpty())
            {
                var split = bloodpressure.Split('/');
                var bp = new BloodPressure(split[0].ToInteger(), split[1].ToInteger()) { Position = position.HasValue ? position.Value : 0, Side = side.HasValue ? side.Value : 0 };
                BloodPressures.Add(bp);
            }
        }
    }
}
