﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
using System.ComponentModel.DataAnnotations;

    public class PatientSocCertPeriod
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string SocCertPeriod { get; set; }
        public string SocEndDate { get; set; }
        public string PatientPatientID { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMiddleInitial { get; set; }
        public string PhysicianName { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime PatientSoC { get; set; }
        public string respEmp { get; set; }

        public Guid PhysicianId { get; set; }
        public string PatientData { get; set; }


        public string PatientDisplayName { get { return this.PatientFirstName + " " + this.PatientLastName; } }

    }

}
