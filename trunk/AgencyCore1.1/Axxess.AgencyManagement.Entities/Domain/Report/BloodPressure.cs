using System.Xml.Serialization;
using System;
namespace Axxess.AgencyManagement.Entities
{
    using Axxess.Core.Extension;

    public class BloodPressure
    {
        public Guid AssessmentId { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string Original { get; set; }
        [XmlIgnore]
        public int Systolic { get; set; }
        [XmlIgnore]
        public int Diastolic { get; set; }

        public int Side { get; set; }
        public int Position { get; set; }
        
        /// <summary>
        /// The difference between systolic and diastolic blood pressure
        /// </summary>
        [XmlIgnore]
        public int PulsePressure
        {
            get
            {
                return this.Systolic - this.Diastolic;
            }
        }

        public override string ToString()
        {
            return this.Systolic + "/" + this.Diastolic;
        }

        public string ToFullString()
        {
            var side = Side.ToEnum(BloodPressureSide.None);
            var position = Position.ToEnum(BloodPressurePosition.None);
            return (position != BloodPressurePosition.None ? position.ToString() : string.Empty) +  
                (side != BloodPressureSide.None ? side.ToString() : string.Empty) + ": " + 
                this.Systolic + "/" + this.Diastolic;
        }

        public BloodPressure()
        {
            
        }

        public BloodPressure(int sys, int dias)
        {
            this.Systolic = sys;
            this.Diastolic = dias;
        }
    }
}