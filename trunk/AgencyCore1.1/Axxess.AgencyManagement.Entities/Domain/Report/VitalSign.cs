﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class VitalSign
    {
        public double BSMax { get; set; }
        public double BSMin { get; set; }
        public double Temp { get; set; }
        public double Resp { get; set; }
        public double Weight { get; set; }
        public int PainLevel { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime VisitDate { get; set; }
        public int ApicalPulse { get; set; }
        public int RadialPulse { get; set; }

        public string DisciplineTask { get; set; }
        public string UserDisplayName { get; set; }
        public string PatientDisplayName { get; set; }

        public string BPLyingLeft { get; set; }
        public string BPLyingRight { get; set; }
        public string BPSittingLeft { get; set; }
        public string BPSittingRight { get; set; }
        public string BPStandingLeft { get; set; }
        public string BPStandingRight { get; set; }

        public string BPLying { get; set; }
        public string BPSitting { get; set; }
        public string BPStanding { get; set; }

        public string OxygenSaturation { get; set; }

        //public int BPLyingLeftSystolic { get; set; }
        //public int BPLyingRightSystolic { get; set; }
        //public int BPLyingLeftDiastolic { get; set; }
        //public int BPLyingRightDiastolic { get; set; }

        //public int BPSittingLeftSystolic { get; set; }
        //public int BPSittingRightSystolic { get; set; }
        //public int BPSittingLeftDiastolic { get; set; }
        //public int BPSittingRightDiastolic { get; set; }

        //public int BPStandingLeftSystolic { get; set; }
        //public int BPStandingRightSystolic { get; set; }
        //public int BPStandingLeftDiastolic { get; set; }
        //public int BPStandingRightDiastolic { get; set; }

        public int BPSystolic { get; set; }
        public int BPDiastolic { get; set; }

        public int SysMax { get; set; }
        public int SysMin { get; set; }
        public int DiaMax { get; set; }
        public int DiaMin { get; set; }
    }
}
