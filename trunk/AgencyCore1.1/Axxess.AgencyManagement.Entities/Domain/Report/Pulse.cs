﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities.Domain
{
    public class Pulse
    {
        public int Value { get; set; }
        public PulseLocation Location { get; set; }
        public int? Status { get; set; }
    }
}
