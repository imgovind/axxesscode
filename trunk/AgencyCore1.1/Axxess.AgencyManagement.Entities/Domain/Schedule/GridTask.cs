﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class GridTask : CalendarTask
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        
        public bool IsOASISProfileExist { get; set; }
        public bool IsAttachmentExist { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanReassign { get; set; }
        public bool IsUserCanReopen { get; set; }
        public bool IsUserCanEditDetail { get; set; }
        public bool IsUserCanRestore { get; set; }
        public bool IsUserCanEdit { get; set; }

        public string EpisodeNotes { get; set; }
        public string ReturnReason { get; set; }
        public string Comments { get; set; }
        public string StatusComment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsComplete { get; set; }
        public bool IsOrphaned { get; set; }
        public string Type { get; set; }
        public string Group { get; set; }
        public bool IsInQA { get; set; }
    }
}
