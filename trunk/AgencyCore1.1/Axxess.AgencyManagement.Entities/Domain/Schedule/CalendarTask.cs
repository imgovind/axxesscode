﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

   public class CalendarTask
    {

       public string UserName { get; set; }
       public string PatientName { get; set; }
       public string Discipline { get; set; }
       public int DisciplineTask { get; set; }
       public bool IsMissedVisit { get; set; }
       public bool IsAllDay { get; set; }
       public DateTime DateIn { get; set; }
       public DateTime DateOut { get; set; }
       public string Range { get; set; }
       public string TimeIn { get; set; }
       public string TimeOut { get; set; }
       public string StatusName { get; set; }
       public string TaskName { get; set; }
       public AgencyServices Service { get; set; }

    }
}
