﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class PayorTaskInput
    {
       /// <summary>
       /// Payor id
       /// </summary>
       public int Id { get; set; }
       public bool IsAll { get; set; }
       public List<int> Tasks { get; set; }
    }
}
