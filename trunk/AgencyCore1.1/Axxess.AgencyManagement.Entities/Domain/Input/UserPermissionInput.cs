﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public class UserPermissionInput
    {
        public Guid UserId { get; set; }
        public List<int> Services { get; set; }
        public IDictionary<int, Dictionary<int, List<int>>> Permissions { get; set; }
        public IDictionary<int, Dictionary<int, List<int>>> ReportPermissions { get; set; }
        public List<UserLocationService> ServiceLocations { get; set; }
        public List<int> AllLocationServices { get; set; }
        // public List<UserPermissionInput> Permissions { get; set; }

    }

    //public class UserPermissionInput
    //{
    //    public int ParentPermission { get; set; }
    //    public bool IsNoChild { get; set; }
    //    public List<int> ChildPermission { get; set; }
    //}


}
