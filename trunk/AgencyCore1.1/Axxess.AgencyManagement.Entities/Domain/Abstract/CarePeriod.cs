﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using System.Web.Mvc;
    using Axxess.Core.Extension;

    public abstract class CarePeriod : PatientBase
    {
        public CarePeriod()
        {
            
        }

        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid CaseManagerId { get; set; }
        public int PrimaryInsurance { get; set; }
        public int SecondaryInsurance { get; set; }
        public string Comments { get; set; }
        public List<Guid> Assets { get; set; }
        //TODO: Eventually remove Details after data has been transfered
        public string Details { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsLinkedToDischarge { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public Guid AdmissionId { get; set; }

        [SubSonicIgnore]
        public string StartDateFormatted { get { return this.StartDate.ToZeroFilled(); } }
        [SubSonicIgnore]
        public string EndDateFormatted { get { return this.EndDate.ToZeroFilled(); } }
        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get; set; }

        [SubSonicIgnore]
        public virtual bool HasNext { get { return false; } }

        [SubSonicIgnore]
        public virtual bool HasPrevious { get { return false; } }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        //[SubSonicIgnore]
        //public EpisodeDetail Detail { get; set; }
        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }
        [SubSonicIgnore]
        public List<SelectListItem> AdmissionDates { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanViewLog { get; set; }

    }
}
