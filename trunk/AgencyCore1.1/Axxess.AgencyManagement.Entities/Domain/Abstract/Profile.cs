﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using System.Runtime.Serialization;
    [Serializable]
    [DataContract]
    [SubSonicTableNameOverride("patientprofiles")]
    public class Profile : AgencyBase
    {
        #region Members

        public Guid Id { get; set; }
        [ScriptIgnore]
        public Guid UserId { get; set; }
        public Guid AgencyLocationId { get; set; }

        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }

        public DateTime StartofCareDate { get; set; }

        public string InsuranceIds { get; set; }

        public int PrimaryInsurance { get; set; }
        public int SecondaryInsurance { get; set; }
        public int TertiaryInsurance { get; set; }

        public string PrimaryHealthPlanId { get; set; }
        public string SecondaryHealthPlanId { get; set; }
        public string TertiaryHealthPlanId { get; set; }

        public string PrimaryGroupName { get; set; }
        public string SecondaryGroupName { get; set; }
        public string TertiaryGroupName { get; set; }

        public string PrimaryGroupId { get; set; }
        public string SecondaryGroupId { get; set; }
        public string TertiaryGroupId { get; set; }

        public string PrimaryRelationship { get; set; }
        public string SecondaryRelationship { get; set; }
        public string TertiaryRelationship { get; set; }

        public string PaymentSource { get; set; }
        public string OtherPaymentSource { get; set; }

        public string Payer { get; set; }

        public Guid CaseManagerId { get; set; }
        public string AdmissionSource { get; set; }
        public DateTime ReferralDate { get; set; }
        public string OtherReferralSource { get; set; }
        public Guid InternalReferral { get; set; }

        public Guid AuditorId { get; set; }
        public int Status { get; set; }
        /// <summary>
        /// Keeps the old status of the patient when change to Deprecated
        /// </summary>
        public int OldStatus { get; set; }

        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }
        public string Comments { get; set; }
        public DateTime LastEligibilityCheck { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime DischargeDate { get; set; }
        public int DischargeReasonId { get; set; }
        public string DischargeReason { get; set; }
        public Guid AdmissionId { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldCreateEpisode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsUnloadButtonVisible { get; set; }
        /// <summary>
        /// Only Used for HomeHealth
        /// </summary>
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsFaceToFaceEncounterCreated { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime EpisodeEndDate { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.Status));
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string NonAdmissionDateFormatted { get { return this.NonAdmissionDate.ToShortDateString().ToZeroFilled(); } }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString().ToZeroFilled(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string ReferralDateFormatted { get { return this.ReferralDate.ToString("MM/dd/yyyy"); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PaymentSources { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string SecondaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string TertiaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CaseManagerName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsDischarged { get { return this.Status == (int)PatientStatus.Discharged; } }

        [XmlIgnore]
        [SubSonicIgnore]
        public int ServiceType { get; set; }

        [SubSonicIgnore]
        public string MedicareNumber { get; set; }
        [SubSonicIgnore]
        public string MedicaidNumber { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }


      
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));

            AddValidationRule(new Validation(() => this.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.StartofCareDate.ToString()), "Patient Start of care date is required."));



            //if (PaymentSource.IsNullOrEmpty())
            //{
            //    AddValidationRule(new Validation(() => this.PaymentSources.Count == 0, "Patient Payment Source is required."));
            //}


            //if (this.ReferralDate.IsValid())
            //{
            //    AddValidationRule(new Validation(() => this.StartofCareDate < this.ReferralDate, "Referral date must be less than or equal to the start of care."));
            //}
        }

        #endregion

        #region Encode the multi array string field to one field

        public void Encode()
        {
            //if (this.MedicareNumber.IsNotNullOrEmpty())
            //{
            //    this.MedicareNumber = this.MedicareNumber.Trim();
            //}
            //if (this.MedicaidNumber.IsNotNullOrEmpty())
            //{
            //    this.MedicaidNumber = this.MedicaidNumber.Trim();
            //}
            if (this.PaymentSources.Count > 0)
            {
                this.PaymentSource = this.PaymentSources.ToArray().AddColons();
            }

        }

        #endregion

        public Profile() { }
        public Profile(OldPatient patient)
        {
            this.AgencyId = patient.AgencyId;
            this.FirstName = patient.FirstName;
            this.LastName = patient.LastName;
            this.MiddleInitial = patient.MiddleInitial;
            this.PatientIdNumber = patient.PatientIdNumber;
            this.AgencyLocationId = patient.AgencyLocationId; 
            this.StartofCareDate = patient.StartofCareDate;
            this.PrimaryInsurance = patient.PrimaryInsurance.ToIntegerAfterNonDigitStrip();
            this.SecondaryInsurance = patient.SecondaryInsurance.ToIntegerAfterNonDigitStrip();
            this.TertiaryInsurance = patient.TertiaryInsurance.ToIntegerAfterNonDigitStrip();
            this.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
            this.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
            this.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
            this.PrimaryGroupName = patient.PrimaryGroupName;
            this.SecondaryGroupName = patient.SecondaryGroupName;
            this.TertiaryGroupName = patient.TertiaryGroupName;
            this.PrimaryGroupId = patient.PrimaryGroupId;
            this.SecondaryGroupId = patient.SecondaryGroupId;
            this.TertiaryGroupId = patient.TertiaryGroupId;
            this.PrimaryRelationship = patient.PrimaryRelationship;
            this.SecondaryRelationship = patient.SecondaryRelationship;
            this.TertiaryRelationship = patient.TertiaryRelationship;
            this.PaymentSource = patient.PaymentSource;
            this.OtherPaymentSource = patient.OtherPaymentSource;
            this.Payer = patient.Payer;
            this.CaseManagerId = patient.CaseManagerId;
            this.AdmissionSource = patient.AdmissionSource;
            this.ReferralDate = patient.ReferralDate;
            this.OtherReferralSource = patient.OtherReferralSource;
            this.InternalReferral = patient.InternalReferral;
            this.AuditorId = patient.AuditorId;
            this.Status = patient.Status;
            this.NonAdmissionDate = patient.NonAdmissionDate;
            this.NonAdmissionReason = patient.NonAdmissionReason;
            this.LastEligibilityCheck = patient.LastEligibilityCheck;
            this.DischargeDate = patient.DischargeDate;
            this.DischargeReasonId = patient.DischargeReasonId;
            this.DischargeReason = patient.DischargeReason;
            this.AdmissionId = patient.AdmissionId;
            this.Created = patient.Created;
            this.Modified = patient.Modified;
            this.Comments = patient.Comments;
        }

    }
}
