﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.AgencyManagement.Entities.Enums;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Script.Serialization;

    [DataContract]
    [Serializable]
    public abstract class ITask : EpisodeBase
    {
        [XmlElement("EventId")]
        [DataMember]
        [SubSonicPrimaryKey]
        public Guid Id { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public int Status { get; set; }

        [XmlElement]
        [DataMember]
        [UIHint("ToolTip")]
        public string Comments { get; set; }

        [XmlElement]
        [DataMember]
        public int BeforeMissedVisitStatus { get; set; }

        [XmlElement]
        [DataMember]
        public string Surcharge { get; set; }

        [XmlElement]
        [DataMember]
        public string AssociatedMileage { get; set; }

        [XmlElement]
        [DataMember]
        public bool SendAsOrder { get; set; }

        public List<Guid> Assets { get; set; }

        [XmlElement]
        [DataMember]
        public int Version { get; set; }

        [XmlElement]
        [DataMember]
        public bool InPrintQueue { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsVisitPaid { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsBillable { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsOrderForNextEpisode { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlElement]
        [DataMember]
        public virtual string ReturnReason { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid PhysicianId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string UserName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string DisciplineTaskName
        {
            get
            {
                return DisciplineTaskFactory.ConvertDisciplineTaskToString(this.DisciplineTask);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime StartDate { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime EndDate { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Url { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string ActionUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string OasisProfileUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string AttachmentUrl { get; set; }

        [XmlIgnore]
        public string Asset { get; set; }

        [XmlElement]
        [DataMember]
        public int PayorId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string MissedVisitComments { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [UIHint("BlueToolTip")]
        public string EpisodeNotes { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [UIHint("RedToolTip")]
        public string StatusComment
        {
            get
            {
                var result = string.Empty;
                if (this.IsMissedVisit && this.MissedVisitComments.IsNotNullOrEmpty())
                    result += this.MissedVisitComments.Clean() + "\r\n";
                if (this.ReturnReason.IsNotNullOrEmpty() && !ScheduleStatusFactory.AfterQAStatus().Contains(this.Status))
                    result += this.ReturnReason;
                return result;
            }
        }

        [SubSonicIgnore]
        public bool IsComplete
        {
            get
            {
                return ScheduleStatusFactory.OnAndAfterQAStatus(true).Contains(this.Status);
            }
        }

        [XmlIgnore]
        [DataMember]
        [UIHint("HiddenMinDate")]
        public virtual DateTime EventDate { get; set; }

        [XmlIgnore]
        [DataMember]
        [UIHint("HiddenMinDate")]
        public virtual DateTime VisitDate { get; set; }

        [XmlIgnore]
        public virtual string TimeIn { get; set; }

        [XmlIgnore]
        public virtual string TimeOut { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public virtual int MinSpent { get { return 0; } }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    ScheduleStatus status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)this.Status : ScheduleStatus.NoStatus;
                    if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.EventDate.IsValid() && this.EventDate.Date <= DateTime.Now.Date)
                    {
                        return ScheduleStatus.CommonNotStarted.GetDescription();
                    }
                    return status.GetDescription();

                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsOrphaned
        {
            get
            {
                var result = false;
                if (this.EventDate.IsValid())
                {
                    if ((this.EventDate.Date < this.StartDate.Date || this.EventDate.Date > this.EndDate.Date))
                    {
                        if (this.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter)
                        {
                            result = true;
                        }
                    }
                }
                return result;
            }
        }

        [SubSonicIgnore]
        public abstract string EventDateTimeRange { get; }
        [SubSonicIgnore]
        public abstract string VisitDateTimeRange { get; }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid LocationId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool HasReturnComments { get; set; }

       


        [XmlIgnore]
        [SubSonicIgnore]
        public bool CanAddSupplies { get; set; }

        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public Guid PatientUserId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsUserDeleted { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsUserCanSeeSticky { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsTaskChangeable { get; set; }


        [SubSonicIgnore]
        public abstract AgencyServices Service { get; }
    }
}










