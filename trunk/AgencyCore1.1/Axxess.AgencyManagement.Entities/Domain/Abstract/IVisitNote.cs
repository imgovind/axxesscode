﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    [Serializable]
    public abstract class IVisitNote : EpisodeBase
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PhysicianId { get; set; }
        public string NoteType { get; set; }
        public int Status { get; set; }
        public bool IsBillable { get; set; }
        public bool IsWoundCare { get; set; }
        public bool IsSupplyExist { get; set; }
        public string Supply { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime SentDate { get; set; }
        public long OrderNumber { get; set; }
        public string PhysicianData { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int Version { get; set; }
        public Guid SelectedEpisodeId { get; set; }

        //#region Domain

        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }
        [SubSonicIgnore]
        public List<NotesQuestion> WoundQuestions { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string PhysicianName { get; set; }
        [SubSonicIgnore]
        public DateTime OrderDate { get; set; }
        [SubSonicIgnore]
        public AgencyServices Service { get; set; }

        //#endregion

    }
}
