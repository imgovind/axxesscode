﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Collections.Generic;

    public class Field
    {
        public Field()
        {
            this.Options = new List<Option>();
        }

        public string Name { get; set; }
        public IList<Option> Options { get; set; }
    }
}
