﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AssessmentQuestionData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Type { get; set; }
        public List<Question> Question { get; set; }

        public AssessmentQuestionData(Assessment assessment)
        {
            this.AgencyId = assessment.AgencyId;
            this.PatientId = assessment.PatientId;
            this.EpisodeId = assessment.EpisodeId;
            this.Id = assessment.Id;
            this.Type = assessment.Type;
            this.Question = assessment.Questions;
        }

        public AssessmentQuestionData(PlanofCare planOfCare)
        {
            this.AgencyId = planOfCare.AgencyId;
            this.PatientId = planOfCare.PatientId;
            this.EpisodeId = planOfCare.EpisodeId;
            this.Id = planOfCare.Id;
            this.Type = planOfCare.DisciplineTask.ToString();
            this.Question = planOfCare.Questions;
        }
    }
}
