﻿namespace Axxess.AgencyManagement.Entities
{
    public class Option
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
