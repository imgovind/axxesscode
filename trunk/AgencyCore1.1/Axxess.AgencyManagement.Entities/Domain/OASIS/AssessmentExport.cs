﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Web.Script.Serialization;
    using Axxess.Core.Extension;
    using Enums;
    using Axxess.Core.Enums;
   

    public class AssessmentExport
    {
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        [ScriptIgnore()]
        public int InsuranceId { get; set; }
        public string Insurance { get; set; }
        public string PatientName { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public string AssessmentName { get { return Enum.IsDefined(typeof(AssessmentType), this.AssessmentType) ? ((AssessmentType)Enum.Parse(typeof(AssessmentType), this.AssessmentType)).GetDescription() : string.Empty; } }
        public DateTime AssessmentDate { get; set; }
        public DateTime ExportedDate { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime VisitDate { get; set; }
        public bool IsValidated { get; set; }
        public bool IsSubmissionEmpty { get; set; }
        public string TaskName { get; set; }

        [ScriptIgnore()]
        public string PaymentSources { get; set; }

        [ScriptIgnore()]
        public string EpisodeData { get; set; }
        [ScriptIgnore()]
        public DateTime EpisodeStartDate { get; set; }
        [ScriptIgnore()]
        public DateTime EpisodeEndDate { get; set; }

        [ScriptIgnore()]
        public int CorrectionNumber { get; set; }

        public bool IsUserCanReopen { get; set; }
        public bool IsUserCanGenerate { get; set; }

        public AgencyServices Service { get; set; }

        
        public int Index { get; set; }

        public string EpisodeRange { get { return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy")); } }
    }
}
