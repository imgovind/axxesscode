﻿//namespace Axxess.AgencyManagement.Entities
//{
//    using System;
//    using System.Collections.Generic;

//    using SubSonic.SqlGeneration.Schema;
//    using Axxess.Core.Extension;


//    public class PlanofCareStandAlone : IPlanOfCare
//    {
//        public PlanofCareStandAlone()
//        {
//            this.Questions = new List<Question>();
//        }

//        #region Domain

//        [SubSonicIgnore]
//        public string AgencyData { get; set; }

//        [SubSonicIgnore]
//        public string PatientData { get; set; }

//        [SubSonicIgnore]
//        public string MedicationProfileData { get; set; }

//        [SubSonicIgnore]
//        public string PatientName { get; set; }

//        [SubSonicIgnore]
//        public string PhysicianName { get; set; }

//        [SubSonicIgnore]
//        public string OrderDateFormatted { get; set; }
//        [SubSonicIgnore]
//        public string ReceivedDateFormatted { get { return this.ReceivedDate.IsValid() && this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
//        [SubSonicIgnore]
//        public string SentDateFormatted { get { return this.SentDate.IsValid() && this.SentDate > DateTime.MinValue ? this.SentDate.ToString("MM/dd/yyyy") : string.Empty; } }

//        #endregion

//        #region Validation Rules

//        protected override void AddValidationRules()
//        { }

//        #endregion
//    }
//}
