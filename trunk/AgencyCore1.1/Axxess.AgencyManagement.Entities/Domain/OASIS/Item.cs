﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    [Serializable]
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
