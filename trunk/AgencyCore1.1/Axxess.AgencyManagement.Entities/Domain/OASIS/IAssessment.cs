﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using SubSonic.SqlGeneration.Schema;

    public interface IAssessment
    {
        Guid AgencyId { get; set; }
        Guid Id { get; set; }
        Guid PatientId { get; set; }
        Guid EpisodeId { get; set; }
        Guid UserId { get; set; }
        int Status { get; set; }
        string ClaimKey { get; set; }
        string HippsCode { get; set; }
        string HippsVersion { get; set; }
        string SubmissionFormat { get; set; }
        string CancellationFormat { get; set; }
        int VersionNumber { get; set; }
        string Supply { get; set; }
        string SignatureText { get; set; }
        string TimeIn { get; set; }
        string TimeOut { get; set; }
        bool IsDeprecated { get; set; }
        bool IsValidated { get; set; }
        DateTime AssessmentDate { get; set; }
        DateTime SignatureDate { get; set; }
        DateTime ExportedDate { get; set; }
        int Version { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }

        string Type { get; set; }
        string TaskName { get; set; }
        string Discipline { get; set; }
        string TypeName { get; }
        string MedicationProfile { get; set; }
        string AllergyProfile { get; set; }
        string PatientName { get; set; }
        string Insurance { get; set; }
        bool IsNew { get; set; }
        string TypeDescription { get; }
        DateTime VisitDate { get; set; }
        bool IsLastTab { get; set; }
        DateTime ScheduleDate { get; set; }
        string ValidationError { get; set; }
        List<Question> Questions { get; set; }

        [SubSonicIgnore]
        bool ShowOasisVendorButton { get; set; }
    }
}
