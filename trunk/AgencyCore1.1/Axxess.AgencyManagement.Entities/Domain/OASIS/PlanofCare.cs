﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class PlanofCare : EpisodeBase
    {
        public PlanofCare()
        {
            this.Questions = new List<Question>();
        }
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public string Data { get; set; }
        public int Status { get; set; }
        public string PhysicianData { get; set; }
        public long OrderNumber { get; set; }
        public int DisciplineTask { get; set; }
        public DateTime Created { get; set; }
        public DateTime SentDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime Modified { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsStandAlone { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsNonOasis { get; set; }

        #region Domain

        [SubSonicIgnore]
        public LocationPrintProfile LocationProfile { get; set; }

        [SubSonicIgnore]
        public PatientProfileLean PatientProfile { get; set; }

        [SubSonicIgnore]
        public string MedicationProfileData { get; set; }

        [SubSonicIgnore]
        public string Allergies { get; set; }

        [SubSonicIgnore]
        public List<Question> Questions { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string EpisodeStart { get; set; }

        [SubSonicIgnore]
        public string EpisodeEnd { get; set; }

        [SubSonicIgnore]
        public string OrderDateFormatted { get; set; }
        [SubSonicIgnore]
        public string ReceivedDateFormatted { get { return this.ReceivedDate.IsValid() && this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string SentDateFormatted { get { return this.SentDate.IsValid() && this.SentDate > DateTime.MinValue ? this.SentDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public List<String> PdfPages { get; set; }

        [SubSonicIgnore]
        public string StatusComment { get; set; }

        [SubSonicIgnore]
        public bool IsLinkedToAssessment{ get; set; }

        [SubSonicIgnore]
        public DateTime SOC { get; set; }

        public override string ToString()
        {
            return string.Format("PlanofCare: AgencyId: {0}  EpisodeId: {1}  PatientId: {2}  Assessment Type: {3}", 
                this.AgencyId, this.EpisodeId, this.PatientId, this.AssessmentType);
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        { }

        #endregion
    }
}
