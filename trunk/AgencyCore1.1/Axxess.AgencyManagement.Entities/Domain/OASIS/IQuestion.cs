﻿namespace Axxess.AgencyManagement.Entities
{
    using Enums;

    public interface IQuestion
    {
        string Name { get; set; }
        string Answer { get; set; }
        QuestionType Type { get; set; }
    }
}
