﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Xml.Serialization;

    using Enums;
    [XmlRoot()]
    public abstract class QuestionBase
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Answer { get; set; }

        public abstract QuestionBase Create(string name, string answer);
    }
}
