﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Enums;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    // using Axxess.Membership.Logging;

    public static class PlanofCareXml
    {
        #region Members

        private static XElement planofCareXml;

        public static XElement PlanofCareXmlInstance
        {
            get
            {
                if (planofCareXml == null)
                {
                    LoadPlanofCareXml();
                }
                return planofCareXml;
            }
        }

        private static void LoadPlanofCareXml()
        {
            string xmlLocation = HttpContext.Current.Server.MapPath("~/App_Data/xml/OasisC-Plan-of-care.xml");
            planofCareXml = XElement.Load(xmlLocation);
        }

        #endregion

        #region Static Methods

        private static List<Item> GetItemNodes(string node)
        {
            List<Item> items = new List<Item>();
            var elements = PlanofCareXmlInstance.Elements(XName.Get(node)).Elements(XName.Get("Item"));
            if (elements != null && elements.Count() > 0)
            {
                elements.ForEach(e =>
                {
                    items.Add(new Item { Id = e.Attribute(XName.Get("id")).Value, Value = e.Attribute(XName.Get("value")).Value });
                });
            }

            return items;
        }

        private static List<Sentence> GetSentenceNodes(string node)
        {
            List<Sentence> sentences = new List<Sentence>();
            try
            {
                var elements = PlanofCareXmlInstance.Elements(XName.Get(node)).Elements(XName.Get("Sentence"));
                if (elements.Any())
                {
                    elements.ForEach(e =>
                    {
                        if (e.Attribute(XName.Get("name")) != null && e.Attribute(XName.Get("name")).Value.IsNotNullOrEmpty())
                        {
                            var sentence = new Sentence { Name = e.Attribute(XName.Get("name")).Value };
                            sentences.Add(sentence);
                        }
                        else
                        {
                            var sentence = new Sentence { Id = e.Attribute(XName.Get("id")).Value, Format = e.Attribute(XName.Get("format")).Value };
                            var fields = e.Elements(XName.Get("Field"));
                            if (fields.Any())
                            {
                                fields.ForEach(f =>
                                {
                                    var field = new Field { Name = f.Attribute(XName.Get("name")).Value };

                                    var options = f.Elements(XName.Get("Option"));
                                    if (options.Any())
                                    {
                                        options.ForEach(o =>
                                        {
                                            field.Options.Add(new Option { Text = o.Attribute(XName.Get("text")).Value, Value = o.Attribute(XName.Get("value")).Value });
                                        });
                                    }
                                    sentence.Fields.Add(field);
                                });
                            }

                            var items = e.Elements(XName.Get("Item"));
                            if (items.Any())
                            {
                                items.ForEach(i =>
                                {
                                    sentence.Items.Add(new Item
                                    {
                                        Id = i.Attribute(XName.Get("id")).Value,
                                        Value = i.Attribute(XName.Get("value")).Value,
                                        Name = i.Attribute(XName.Get("name")).Value
                                    });
                                });
                            }

                            sentences.Add(sentence);
                        }
                    });
                }

            }
            catch (Exception)
            {
                //Logger.Exception(ex);
            }

            return sentences;
        }

        private static string BuildSentences(string answers, List<Sentence> sentenceList, IDictionary<string, Question> questions)
        {
            var sentenceBuilder = new StringBuilder();

            try
            {
                if (answers.IsNotNullOrEmpty())
                {
                    string[] answerArray = answers.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    answerArray.ForEach(a =>
                    {
                        var sentence = sentenceList.Find(s =>
                        {
                            if (s.Id.IsEqual(a))
                            {
                                return true;
                            }
                            return false;
                        });

                        if (sentence != null)
                        {
                            var parameters = new List<string>();
                            sentence.Fields.ForEach(f =>
                            {
                                var question = questions.Get(f.Name);
                                if (f.Options.Count > 0)
                                {
                                    foreach (Option option in f.Options)
                                    {
                                        if (question != null && question.Answer == option.Value)
                                        {
                                            parameters.Add(option.Text);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    parameters.Add(question != null ? question.Answer : string.Empty);
                                }
                            });

                            sentence.Items.ForEach(i =>
                            {
                                var question = questions.Get(i.Name);
                                if (question != null && question.Answer.Contains(i.Id))
                                {
                                    parameters.Add(i.Value);
                                }
                                else
                                {
                                    parameters.Add(string.Empty);
                                }
                            });

                            if (parameters.Count > 0)
                            {
                                sentenceBuilder.AppendFormat(sentence.Format, parameters.ToArray());
                            }
                            else
                            {
                                sentenceBuilder.Append(sentence.Format);
                            }
                        }
                    });
                }

                var commentSentence = sentenceList.Find(s =>
                {
                    if (s.Name.IsNotNullOrEmpty())
                    {
                        return true;
                    }
                    return false;
                });

                if (commentSentence != null)
                {
                    var i = questions.SingleOrDefault(q => q.Value.Type == QuestionType.PlanofCare && q.Key.IsEqual(commentSentence.Name));
                    if (i.Value != null && i.Value.Answer.IsNotNullOrEmpty())
                    {
                        if (i.Value.Answer.EndsWith("."))
                        {
                            sentenceBuilder.AppendFormat(" {0} ", i.Value.Answer);
                        }
                        else if (i.Value.Answer.EndsWith(". "))
                        {
                            sentenceBuilder.AppendFormat("{0}", i.Value.Answer);
                        }
                        else
                        {
                            sentenceBuilder.AppendFormat("{0}. ", i.Value.Answer);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Logger.Exception(ex);
            }

            return sentenceBuilder.ToString();
        }

        public static string LookupText(string node, string answer)
        {
            var stringBuilder = new StringBuilder();
            if (node.IsNotNullOrEmpty() && answer.IsNotNullOrEmpty())
            {
                var items = GetItemNodes(node);
                string[] answerArray = answer.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                int index = 0;
                answerArray.ForEach(a =>
                {
                    Item item = items.Find(i =>
                    {
                        if (i.Id.IsEqual(a))
                        {
                            return true;
                        }
                        return false;
                    });

                    if (item != null)
                    {
                        if (index == answerArray.Length - 1)
                        {
                            stringBuilder.Append(item.Value);
                        }
                        else
                        {
                            stringBuilder.AppendFormat("{0}, ", item.Value);
                        }
                    }
                    index++;
                });
            }
            return stringBuilder.ToString();
        }

        public static string ExtractText(string type, IDictionary<string, Question> questions)
        {
            string sentences = string.Empty;

            if (type.IsEqual("NutritionalRequirements"))
            {
                var nutritionRequirements = questions["485NutritionalReqs"].Answer;
                if (nutritionRequirements.IsNotNullOrEmpty())
                {
                    sentences += BuildSentences(nutritionRequirements, GetSentenceNodes(type), questions);
                }
            }
            else
            {
                var items = questions.Where(q => q.Value.Type == QuestionType.PlanofCare && ((q.Key.EndsWith(type) || q.Key.EndsWith(string.Format("{0}s", type))) ||
                    (type == "Goal" && (q.Key.EndsWith("GoalComments") || q.Key.EndsWith(string.Format("GoalsComments", type)))))).ToList();
                items.ForEach(q =>
                {
                    if (q.Value != null)
                    {
                        var key = q.Key.Replace("485", "");
                        if(type == "Goal") {
                            key = key.Replace("Comments", "");
                        }
                        sentences += BuildSentences(q.Value.Answer, GetSentenceNodes(key), questions);
                    }
                });
            }

            return sentences;
        }

        public static string ExtractVitalSigns(IDictionary<string, Question> questions)
        {
            var baseVitalSignFormat = "{0} greater than (>) {1} or less than (<) {2}. ";
            var vitalArray = new string[] { "Temp", "Pulse", "Respiration", "SystolicBP", "DiastolicBP", "FastingBloodSugar" };
            var vitalTitleArray = new string[] { "Temperature", "Pulse", "Respirations", "Systolic BP", "Diastolic BP", "Fasting blood sugar" };
            string vitalSignParameters = string.Empty;
            for (int i = 0; i < vitalArray.Length; i++)
            {
                var greaterThan = questions.Get("Generic" + vitalArray[i] + "GreaterThan");
                var lessThan = questions.Get("Generic" + vitalArray[i] + "LessThan");
                if (greaterThan != null && greaterThan.Answer.IsNotNullOrEmpty() &&
                    lessThan != null && lessThan.Answer.IsNotNullOrEmpty())
                {
                    vitalSignParameters += string.Format(baseVitalSignFormat, vitalTitleArray[i], greaterThan.Answer, lessThan.Answer);
                }
            }
            //Due to blood being mis-spelled on the view the key will also have to be mis-spelled.
            var randomBloodGreaterThan = questions.Get("GenericRandomBloddSugarGreaterThan");
            var randomBloodLessThan = questions.Get("GenericRandomBloodSugarLessThan");
            if (randomBloodGreaterThan != null && randomBloodGreaterThan.Answer.IsNotNullOrEmpty() &&
                   randomBloodLessThan != null && randomBloodLessThan.Answer.IsNotNullOrEmpty())
            {
                vitalSignParameters += string.Format(baseVitalSignFormat, "Random blood sugar", randomBloodGreaterThan.Answer, randomBloodLessThan.Answer);
            }
            var oxygenSat = questions.Get("Generic02SatLessThan");
            if (oxygenSat != null && oxygenSat.Answer.IsNotNullOrEmpty())
            {
                vitalSignParameters += string.Format("O2 Sat (percent) less than (<) {0}. ", oxygenSat.Answer);
            }
            var weight = questions.Get("GenericWeightGreaterThan");
            if (weight != null && weight.Answer.IsNotNullOrEmpty())
            {
                vitalSignParameters += string.Format("Weight Gain/Loss (lbs/7 days) Greater than {0}. ", weight.Answer);
            }
            return vitalSignParameters;
        }

        #endregion

    }
}
