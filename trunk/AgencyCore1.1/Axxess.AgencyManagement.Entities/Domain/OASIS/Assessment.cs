﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Enums;
    
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Enums;

    [Serializable]
    public class Assessment : EpisodeBase, IAssessment
    {
        #region Constructor

        public Assessment()
        {
            this.ValidationError = string.Empty;
            this.Questions = new List<Question>();
        }

        #endregion

        #region IAssessment Members
        
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        //TODO: Remove this property once all the assessments have been moved into one table
        public string OasisData { get; set; }
        public string ClaimKey { get; set; }
        public string HippsCode { get; set; }
        public string HippsVersion { get; set; }
        public string SubmissionFormat { get; set; }
        public string CancellationFormat { get; set; }
        public int VersionNumber { get; set; }
        public string Supply { get; set; }
        public string SignatureText { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsValidated { get; set; }
      
        public DateTime AssessmentDate { get; set; }
        public DateTime SignatureDate { get; set; }
        public DateTime ExportedDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }


        //[SubSonicIgnore]
        public string Type { get; set; }

        [SubSonicIgnore]
        public int Version { get; set; }
        [SubSonicIgnore]
        public bool ShowOasisVendorButton { get; set; }
        [SubSonicIgnore]
        public DateTime VisitDate { get; set; }
        [SubSonicIgnore]
        public string MedicationProfile { get; set; }
        [SubSonicIgnore]
        public string AllergyProfile { get; set; }
        [SubSonicIgnore]
        public string AgencyData { get; set; }
        [SubSonicIgnore]
        public string PatientData { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public string Insurance { get; set; }
        [SubSonicIgnore]
        public bool IsNew { get; set; }
        [SubSonicIgnore]
        public bool IsLastTab { get; set; }
        [SubSonicIgnore]
        public string StatusComment { get; set; }
        [SubSonicIgnore]
        public string TypeName { get { return (this.Type); } }
        [SubSonicIgnore]
        public string TypeDescription { get { return Enum.IsDefined(typeof(AssessmentType), this.Type) ? ((AssessmentType)Enum.Parse(typeof(AssessmentType), this.Type)).GetDescription() : string.Empty; } }
        [SubSonicIgnore]
        public string DiagnosisDataJson { get; set; }
        [SubSonicIgnore]
        public string ValidationError { get; set; }
        [SubSonicIgnore]
        public List<Question> Questions { get; set; }
        [SubSonicIgnore]
        public string AssessmentTypeNum
        {
            get
            {
                if (Enum.IsDefined(typeof(AssessmentType), this.Type))
                {
                    switch ((AssessmentType)Enum.Parse(typeof(AssessmentType), this.Type))
                    {
                        case AssessmentType.StartOfCare: return "01";
                        case AssessmentType.ResumptionOfCare: return "03";
                        case AssessmentType.Recertification: return "04";
                        case AssessmentType.FollowUp: return "05";
                        case AssessmentType.Transfer: return "06";
                        case AssessmentType.TransferDischarge: return "07";
                        case AssessmentType.Death: return "08";
                        case AssessmentType.Discharge: return "09";
                        case AssessmentType.NonOASISStartOfCare: return "11";
                        case AssessmentType.NonOASISRecertification: return "14";
                        case AssessmentType.NonOASISDischarge: return "19";
                        default: return "00";
                    }
                } 
                return "00";
            }
        }
        [SubSonicIgnore]
        public string Discipline  { get; set; }
        [SubSonicIgnore]
        public string TaskName { get; set; }
        [SubSonicIgnore]
        public DateTime ScheduleDate { get; set; }
        [SubSonicIgnore]
        public VitalSignLog VitalSignLog { get; set; }
        //[SubSonicIgnore]
        //public List<Supply> Supplies { get; set; }

        [SubSonicIgnore]
        public AgencyServices Service { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanApprove { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanReturn { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanLoadPrevious { get; set; }
        [SubSonicIgnore]
        public bool IsUserCanSeeSticky { get; set; }
        [SubSonicIgnore]
        public string WoundCareJson { get; set; }

        #endregion

        protected override void AddValidationRules()
        {
        }

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0}: AgencyId: {1}  EpisodeId: {2}  PatientId: {3}",
                this.TypeName, this.AgencyId, this.EpisodeId, this.PatientId);
        }

        #endregion
    }
}
