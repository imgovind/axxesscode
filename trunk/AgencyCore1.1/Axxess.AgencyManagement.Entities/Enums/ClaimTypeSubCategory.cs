﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum ClaimTypeSubCategory
    {
        [Description("RAP")]
        RAP = 1,
        [Description("Final")]
        Final = 2,
        [Description("Managed Care")]
        ManagedCare = 3,
        [Description("Secondary")]
        Secondary = 4
    }
}
