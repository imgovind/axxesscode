﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum BillUnitType
    {
        [Description("Per Visit")]
        PerVisit = 1,
        [Description("Hourly")]
        Hourly = 2,
        [Description("Per 15 Min")]
        Per15Min = 3
    }
}
