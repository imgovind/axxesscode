﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum ContactTypes : byte
    {
        [Description("Bank")]
        Bank,
        [Description("CPA")]
        Cpa,
        [Description("Referral Source")]
        ReferralSource,
        [Description("Book Keeper")]
        BookKeeper,
        [Description("Attorney")]
        Attorney,
        [Description("IT/Tech Support")]
        ITTechSupport,
        [Description("Restaurant")]
        Restaurant,
        [Description("Janitorial Service")]
        JanitorialService,
        [Description("Moving Company")]
        MovingCompany,
        [Description("Lab")]
        Lab,
        [Description("Therapy")]
        Therapy,
        [Description("Social Worker")]
        SocialWorker,
        [Description("Other")]
        Other
    }
}
