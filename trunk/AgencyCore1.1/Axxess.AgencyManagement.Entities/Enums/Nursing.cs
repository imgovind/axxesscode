﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum Nursing
    {
        [Description("Oasis-C Start Of Care")]
        soc,
        [Description("Oasis-C Resumption Of Care")]
        Roc,
        [Description("Oasis-C Discharge From Agency")]
        Discharge,
        [Description("Oasis-C Recertification")]
        Recertification,
        [Description("Oasis-C Other Follow-up")]
        FollowUp,
        [Description("Oasis-C Death at Home")]
        Death,
        [Description("Oasis-C Transfer to inpatient facility - Not Discharged")]
        Transfer,
        [Description("Oasis-C Transfer to inpatient facility - Discharged")]
        TransferDischarge,
        [Description("Discharge Summary")]
        DischargeSummary,
        [Description("Skilled Nurse Visit")]
        SkilledNurseVisit,
        [Description("60 Day Summary")]
        SixtyDaySummary,
        [Description("LVN Supervisory Visit")]
        LVNSupervisoryVisit,
    }
}
