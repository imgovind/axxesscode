﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

   public enum BillDiscipline
    {
        [CustomDescription("Skilled Nursing", "SNV")]
        Nursing,
        [CustomDescription("Physical Therapy", "PT")]
        PT,
        [CustomDescription("Occupational Therapy", "OT")]
        OT,
        [CustomDescription("Speech Therapy", "ST")]
        ST,
        [CustomDescription("HHA", "HHA")]
        HHA,
        [CustomDescription("Social Worker", "MSW")]
        MSW
    }
}
