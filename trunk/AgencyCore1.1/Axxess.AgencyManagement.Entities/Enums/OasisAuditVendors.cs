﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum OasisAuditVendors : byte
    {
        [Description("No OASIS Audit Vendor selected")]
        None = 0,
        [Description("Home Health Gold Edit Report")]
        HHG = 1,
        [Description("PPS Plus")]
        PPSPlus = 2,
        [Description("SHP")]
        SHP = 2
    }
}
