﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum PatientStatus
    {
        [Description("Active")]
        Active = 1,
        [Description("Discharge")]
        Discharged = 2,
        [Description("Pending")]
        Pending = 3,
        [Description("Non-Admit")]
        NonAdmission = 4,
        [Description("Deprecated")]
        Deprecated = 5,
        [Description("Referral")]
        Referral = 6,
    }
}
