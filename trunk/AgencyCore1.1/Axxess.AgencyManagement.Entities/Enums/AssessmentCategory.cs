﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum AssessmentCategory
    {
        [Description("Demographics")]
        Demographics = 0,
        [Description("Patient History & Diagnoses")]
        PatientHistory = 1,
        [Description("Risk Assessment")]
        RiskAssessment = 2,
        [Description("Prognosis")]
        Prognosis = 3,
        [Description("Supportive Assistance")]
        SupportiveAssistance = 4,
        [Description("Sensory Status")]
        Sensory = 5,
        [Description("Pain")]
        Pain = 6,
        [Description("Integumentary Status")]
        Integumentary = 7,
        [Description("Respiratory Status")]
        Respiratory = 8,
        [Description("Endocrine")]
        Endocrine = 9,
        [Description("Cardiac Status")]
        Cardiac = 10,
        [Description("Elimination Status")]
        Elimination = 11,
        [Description("Nutrition")]
        Nutrition = 12,
        [Description("Neuro/Behaviourial Status")]
        NeuroBehavioral = 13,
        [Description("ADL/IADLst")]
        AdlIadl = 14,
        [Description("Supplies Worksheet")]
        SuppliesDme = 15,
        [Description("Supplies Used This Visit")]
        Supplies = 16,
        [Description("Medications")]
        Medications = 17,
        [Description("Care Management")]
        CareManagement = 18,
        [Description("Therapy Need & Plan Of Care")]
        TherapyNeed = 19,
        [Description("Orders for Discipline and Treatment")]
        OrdersDisciplineTreatment = 20,
        [Description("Discharge Data")]
        Discharge = 21,
        [Description("Death Data")]
        Death = 22,
        [Description("Discharge Data")]
        TransferDischarge = 22,
        [Description("Discharge Data")]
        Transfer = 23,
        [Description("Emergent Care")]
        EmergentCare = 24,
        [Description("Discharge/Transfer Data")]
        TransferDischargeDeath
    }
}