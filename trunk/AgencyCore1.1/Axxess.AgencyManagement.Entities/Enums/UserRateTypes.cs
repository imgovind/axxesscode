﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum UserRateTypes
    {
        [Description("None Specified")]
        None = 0,
        [Description("Per Visit")]
        PerVisit = 1,
        [Description("Per Hour")]
        Hourly = 2
    }
}
