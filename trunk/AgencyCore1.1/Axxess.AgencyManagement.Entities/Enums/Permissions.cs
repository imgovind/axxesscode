﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using Axxess.Core.Infrastructure;
    using System.ComponentModel;

    [Flags]
    public enum Permissions : ulong
    {
        None = 0,
        [Permission("Add/Edit/Delete Users", "Manage Users", "Administration", "This permission allows a user to add, edit or delete user account.")]
        ManageUsers = 1,
        [Permission("Delete Any Task", "Delete scheduled tasks", "Administration", "This permission allows a user the ability to delete any task or documents from a patient’s chart or schedule center.")]
        DeleteTasks = 1 << 1,
        [Permission("Manage Payroll", "Run Payroll Reports", "Administration", "This permission allows a user access to the payroll center and in order to perform payroll functions.")]
        ManagePayroll = 1 << 2,
        [Permission("Access Billing Center", "Access Billing Center", "Billing", "This permission allows user access to the billing center where you can create and manage insurance claims and invoices for Medicare, private insurances or private pay.")]
        AccessBillingCenter = 1 << 3,
        [Permission("Receive Eligibility Report", "Receive Eligibility Report", "Billing", "This permission allows a user to view the Medicare eligibility reports for all of the patients admitted within an agency.")]
        ReceiveEligibilityReport = 1 << 4,
        [Permission("Manage Claims", "Manage Claims (RAP & Final)", "Billing", "This permission allows a user the ability to manage claims by updating the status of billed claims as they are processed, accepted and paid.  A user will also be able to change the status of a claim for resubmission.")]
        ManageClaims = 1 << 5,
        [Permission("Generate Claim files", "Generate Claim files", "Billing", "This permission allows a user create electronic claims files for submission to insurance payers.")]
        GenerateClaimFiles = 1 << 6,
        [Permission("View HHRG Calculations", "View HHRG Calculations", "Billing", "This permission allows a user the ability to see the HHRG calculations for a Medicare claim.  This permission also allows a user to view the payment details of an OASIS assessment from the patient’s charts or schedule center.")]
        ViewHHRGCalculations = 1 << 7,
        [Permission("Access Quality Assurance (QA) Center", "Access Quality Assurance (QA) Center", "QA", "This permission allows a user access to the QA center, to perform quality assurance tasks like reviewing submitted documents. This permission is typically granted to case managers, QA nurses or nursing directors.")]
        AccessCaseManagement = 1 << 8,
        [Permission("Re-open Previously Approved Documents", "Re-open Previously Approved Documents", "QA", "This permission allows a user to reopen previously completed or approved tasks/documents.")]
        ReOpenPreviousDocuments = 1 << 9,
        [Permission("Schedule PRN", "Schedule PRN", "QA", "This permission allows a user to schedule PRN visits, without giving them access to the entire scheduling center.")]
        SchedulePRN = 1 << 10,
        [Permission("Add/Edit/Delete Patients", "Manage Patients", "Clerical", "This permission allows a user to add new patients, edit or delete patient information.")]
        ManagePatients = 1 << 11,
        [Permission("Add/Edit/Delete Referrals", "Manage Referral", "Clerical", "This permission allows a user to add new referrals, edit or delete the referral information.")]
        ManageReferrals = 1 << 12,
        [Permission("View Existing Referrals", "View Existing Referrals", "Clerical", "This permission allows a user to view existing patient referrals.")]
        ViewExisitingReferrals = 1 << 13,
        [Permission("Add/Edit/Delete Hospital Information", "Manage Hospital Information", "Clerical", "This permission allows a user to add new hospitals and edit or delete listed hospitals.")]
        ManageHospital = 1 << 14,
        [Permission("Add/Edit/Delete Insurance Information", "Manage Insurance Information", "Clerical", "This permission allows a user to add insurances and edit or delete insurance information.")]
        ManageInsurance = 1 << 15,
        [Permission("Add/Edit/Delete Contact Information", "Manage Contact Information", "Clerical", "This permission will allow a user to add new contacts and edit or delete contacts and contact information (i.e. a CPA, Therapy group, bank, social worker, moving company, restaurant, etc.)")]
        ManageContact = 1 << 16,
        [Permission("View Lists", "View lists of  patients, referrals, contacts, users, etc", "Clerical", "This permission will allow a user the ability to view list of patients, referrals, physicians, hospitals, etc.")]
        ViewLists = 1 << 17,
        [Permission("Access Patient Charts", "Access Patient Charts", "None", "")]
        AccessPatientCharts = 1 << 18,
        [Permission("Access Print Queue", "Access Print Queue", "Clerical", "This permission allows a user access to the print queue where completed documents are listed to make for easy printing of the documents.")]
        PrintClinicalDocuments = 1 << 19,
        [Permission("Export List to Excel", "Export List to Excel", "Clerical", "This permission allows a user to export reports to Excel for printing or editing.")]
        ExportListToExcel = 1 << 20,
        [Permission("Edit Task Details", "Edit Task Details", "Clerical", "This permission allows a user the ability to edit task details, such as times and dates of schedules task, also allows a user to add attachments to task, view attachments add comments(sticky notes) to documents/tasks.")]
        EditTaskDetails = 1 << 21,
        [Permission("View Completed Documents/Tasks", "View Completed Documents/Tasks", "Clerical", "This permission allows a user the ability to view tasks/documents that have been completed and approved through the Quality Assurance (QA) center.")]
        ViewCompletedDocumentsTasks = 1 << 22,
        [Permission("Create/Edit Physician Orders", "Create/Edit Physician Orders", "Clinical", "This permission will allow a user to the ability create and edit physician orders. Clinicians (e.g. nurses, therapists) generally need this permission.")]
        ManagePhysicianOrders = 1 << 23,
        [Permission("Order Management Center", "Order Management Center", "Clerical", "This permission allows a user access to and the ability to manage and track physician orders and care plans (i.e. mark orders as sent, received and complete/returned with physician signature.)")]
        AccessOrderManagementCenter = 1 << 24,
        [Permission("Manage Medication Profile/History/Snapshot", "Manage Medication Profile/History/Snapshot", "Clinical", "This permission allows a user access to create, edit and delete patient medication information.")]
        ViewMedicationProfile = 1 << 25,
        [Permission("Dashboard", "Dashboard", "General", "")]
        Dashboard = 1 << 26,
        [Permission("Messaging", "Messaging", "General", "")]
        Messaging = 1 << 27,
        [Permission("Access Personal Profile", "Access Personal Profile", "General", "")]
        AccessPersonalProfile = 1 << 28,
        [Permission("View Scheduled Tasks", "View Scheduled Tasks", "General", "")]
        ViewScheduledTasks = 1 << 29,
        [Permission("View Patient Profile", "View Patient Profile", "General", "")]
        ViewPatientProfile = 1 << 30,
        [Permission("View Exported OASIS", "View Exported OASIS", "OASIS", "This permission allows a user to see OASIS assessments that have been exported and also to cancel OASIS assessments that are already submitted.")]
        ViewExportedOasis = (ulong)1 << 31,
        [Permission("Create OASIS Export file", "Create OASIS Export file", "OASIS", "This permission allows a user to create the OASIS export data file that is submitted to CMS or the designated OASIS office.")]
        CreateOasisSubmitFile = (ulong)1 << 32,
        [Permission("Approve Completed Documents", "Approve Completed Documents", "QA", "This permission allows a user the ability to review, edit and approve documents that have been submitted by users. This permission is typically granted to case managers, QA nurses or nursing directors.")]
        ApproveCompletedDocuments = (ulong)1 << 33,
        [Permission("Bypass QA Center", "Bypass Quality Assurance Center", "Clinical", "This permission will allow your clinical staff to submit documents that bypass the quality assurance review process. This permission can be reserved for Directors of Nursing or Case Managers. Documents submitted by persons with this permission will be marked as “completed” instead of “submitted pending QA review”.")]
        BypassCaseManagement = (ulong)1 << 34,
        [Permission("Reopen Documents", "Reopen Documents", "QA", "This permission allows a user to reopen completed tasks/documents.")]
        ReopenDocuments = (ulong)1 << 35,
        [Permission("Access Reports Center", "Access Reports Center", "Reporting", "This permission will allows a user to access to the report center, where they will be able to run a number of reports, such as patient roster, CAHPS reports, aged account receivable, survey census, unduplicated census report.  Reports can be exported to excel for additional options.")]
        AccessReports = (ulong)1 << 36,
        [Permission("Access Schedule Center", "Access Schedule Center", "None", "")]
        AccessScheduleCenter = (ulong)1 << 37,
        [Permission("Schedule Visits/Activites", "Schedule Visits/Activites", "Schedule Management", "This permission allows a user to schedule visits and other activities. Examples of tasks/activities that can be scheduled include: nursing, therapy, social work or home health aide visits. OASIS assessments, care summaries, etc are among other documents that can be scheduled.")]
        ScheduleVisits = (ulong)1 << 38,
        [Permission("Edit Episode Information", "Edit Episode Information", "Schedule Management", "This permission allows a user to edit the episode information i.e. change episode dates or adjust the length of an episode.")]
        EditEpisode = (ulong)1 << 39,
        [Permission("Add/Edit/Delete Physician Information", "Manage Physician Information", "Clerical", "This permission allows a user to add, edit or delete physicians and physician information.")]
        ManagePhysicians = (ulong)1 << 40,
        [Permission("Create Incident/Accident & Infection Log", "Create Incident/Accident & Infection Log", "Clerical", "This permission allows a user to create incidents, accident and infection logs.")]
        ManageIncidentAccidentInfectionReport = (ulong)1 << 41,
        [Permission("View Previous Notes", "View Previous Notes", "Clinical", "This permission allows a user the ability to view and copy previous documentation. This permission helps users compare documentation and facilitate faster documentation and helps improve compliance.")]
        ViewPreviousNotes = (ulong)1 << 42,
        [Permission("Access Patient Reports", "Access Patient Reports", "Reporting", "This permission grants a user access to be able to view and generate reports on patients. Reports can be exported to excel for additional options.")]
        AccessPatientReports = (ulong)1 << 43,
        [Permission("Access Clinical Reports", "Access Clinical Reports", "Reporting", "This permission grants a user access to be able to view and generate clinical reports such as missed visit reports, orders, assessments etc. Reports can be exported to excel for additional options.")]
        AccessClinicalReports = (ulong)1 << 44,
        [Permission("Access Schedule Reports", "Access Schedule Reports", "Reporting", "This permission grants a user access to be able to view and generate scheduling reports. Reports can be exported to excel for additional options.")]
        AccessScheduleReports = (ulong)1 << 45,
        [Permission("Access Billing/Financial Reports", "Access Billing/Financial Reports", "Reporting", "This permission grants a user access to be able to view and generate billing and financial reports. Reports can be exported to excel for additional options.")]
        AccessBillingReports = (ulong)1 << 46,
        [Permission("Access Employee Reports", "Access Employee Reports", "Reporting", "This permission grants a user access to view and generate employee reports.")]
        AccessEmployeeReports = (ulong)1 << 47,
        [Permission("Access Statistical Reports", "Access Statistical Reports", "Reporting", "This permission grants a user access to view and generate statistical reports.")]
        AccessStatisticalReports = (ulong)1 << 48,
        [Permission("Manage Company Information", "Manage Company Information", "Administration", "This permission grants a user access to modify company information such as address, phone and fax.")]
        ManageAgencyInformation = (ulong)1 << 49,
        [Permission("State Surveyor/Auditor", "State Surveyor/Auditor", "State Survey/Audit", "This permission grants access to a state surveyor/auditor to review patient records. After granting this access, you must go edit individual patient charts to grant the surveyor/auditor access to that particular patient record.")]
        StateSurveyorAuditorAccess = (ulong)1 << 50
    }

    //public enum PermissionCategory
    //{
    //    Administration = 1,
    //    Billing = 2,
    //    QA = 3,
    //    Clerical = 4,
    //    Clinical = 5,
    //    General = 6,
    //    OASIS = 7,
    //    Reporting = 8,
    //    Audit = 9

    //}

    public enum ParentPermission
    {
        [Description("User")]
        User = 1,
        [Description("Payroll")]
        Payroll = 2,
        [Description("Agency")]
        Agency = 3,
        [Description("RAP/Final")]
        MedicareClaim = 5,
        [Description("Managed Care")]
        ManagedCareClaim = 6,
        [Description("Quality Assurance (QA)")]
        QA = 8,
        [Description("Patient")]
        Patient = 11,
        [Description("Referral")]
        Referral = 12,
        [Description("Hospital")]
        Hospital = 14,
        [Description("Insurance & Authorization")]
        Insurance = 15,
        [Description("Contact")]
        Contact = 16,
        [Description("Plan Of Care/Physician Orders/Face-to-face Encounter")]
        Orders = 23,
        [Description("Order Management")]
        OrderManagement = 24,
        [Description("Medication Profile")]
        MedicationProfile = 25,
        [Description("Allergy Profile")]
        AllergyProfile = 26,

        [Description("OASIS")]
        OASIS = 31,
        [Description("Episode")]
        Episode = 39,
        [Description("Physician")]
        Physician = 40,
        [Description("Incident/Accident")]
        ManageIncidentAccidentReport = 41,
        [Description("Infection")]
        ManageInfectionReport = 42,
        
        [Description("Communication Note")]
        CommunicationNote = 51,
        [Description("Hospitalization Log")]
        HospitalizationLog = 52,
        [Description("Supply")]
        Supply = 53,
        [Description("Template")]
        Template = 54,
        [Description("Remittance")]
        Remittance = 55,
        [Description("Patient Document Type")]
        UploadType = 56,
        [Description("Adjustment Code")]
        AdjustmentCode = 57,
        [Description("Schedule")]
        Schedule = 100,

        //[Description("MedicareClaim")]
        //MedicareClaim = 27,
        //[Description("ManagedCareClaim")]
        //ManagedCareClaim = 24,
        
        [Description("Audit")]
        Audit = 50,
        [Description("Report")]
        Report = 150,

        [Description("General")]
        General = 500,
    }

    public enum PermissionActions
    {
        /// <summary>
        /// Common Permission Actions
        /// </summary>
        Add = 1,
        Edit = 2,
        Delete = 3,
        Restore = 4,
        Print = 5,
        Export = 6,
        [Description("View List")]
        ViewList = 7,
        Reactivate = 8,
        Upload = 9,
        [Description("View Details")]
        ViewDetail = 10,
        [Description("Change Status")]
        ChangeStatus = 11,
        [Description("View Log")]
        ViewLog = 12,
        Deactivate = 13,

        /// <summary>
        /// used for OASIS/visit
        /// </summary>
        ViewPastDue = 50,
        ViewUpComing = 51,

        /// <summary>
        /// Schedule/Document Permisssion Actions
        /// </summary>
        Approve = 100,
        Reopen = 101,
        [Description("Add PRN")]
        AddPRN = 102,
        [Description("Edit Approved")]
        EditApproved = 103,
        Reassign = 104,
        Return = 105,
        [Description("By Pass QA")]
        ByPassQA = 106,
        ViewStickyNotes=107,
        [Description("View Previous Notes")]
        LoadPrevious = 108,


        /// <summary>
        /// Patient/Referral Permisssion Actions
        /// </summary>
        Admit = 200,
        NonAdmit = 201,
        [Description("View Profile")]
        ViewProfile = 202,
        ViewEligibility = 203,

        Sign = 300,
        /// <summary>
        /// OASIS Permissions
        /// </summary>
        /// 
        ViewToExport = 400,
        GenerateOASISExport = 401,
        ViewExported = 402,
        ViewOASISProfile = 403,


        GenerateElectronicClaim = 500,
        ViewOutstandingClaim = 501,
        ViewPendingClaim = 502,
        PostRemittance = 503,
        ViewClaimHistory = 504,


        /// <summary>
        /// Patient/schedule/billing chart view Permisssion
        /// </summary>
        [Description("View Chart")]
        ViewChart = 600,

        /// <summary>
        /// Order Management Permissions
        /// </summary>
        /// 
        ViewOrderToBeSent = 700,
        ViewOrderSent = 701,
        ViewOrderHistory = 702,
        SendToPhysician=703,
        ReceiveFromPhysician = 704
        //ViewOASISProfile = 703,
    }

    public enum ReportPermissions
    {

        [Description("Patient Roster")]
        PatientRoster = 1,
        [Description("CAHPS Report")]
        CAHPSReport = 2,
        [Description("Emergency Contact Listing")]
        EmergencyContactListing = 3,
        [Description("Emergency Preparedness Plan List")]
        EmergencyPreparednessPlanList = 4,
        [Description("Patient Birthday Listing")]
        PatientBirthdayListing = 5,
        [Description("Patient Address Listing")]
        PatientAddressListing = 6,
        [Description("Patient By Physician Listing")]
        PatientByPhysicianListing = 7,
        [Description("Patient Start Of Care Certification Period Listing")]
        PatientStartOfCareCertificationPeriodListing = 8,
        [Description("Patient By Responsible Employee Listing")]
        PatientByResponsibleEmployeeListing = 9,
        [Description("Patient By Responsible Case Manager Listing")]
        PatientByResponsibleCaseManagerListing = 10,
        [Description("Expiring Authorizations")]
        ExpiringAuthorizations = 11,
        [Description("Survey Census")]
        SurveyCensus = 12,
        [Description("Patient Vital Sign Report")]
        PatientVitalSignReport = 13,
        [Description("60 Day Summary By Patient")]
        SixityDaySummaryByPatient = 14,
        [Description("Discharge Patients")]
        DischargePatients = 15,
        [Description("Referral Log Report")]
        ReferralLogReport = 16,
        [Description("Open OASIS")]
        OpenOASIS = 17,
        [Description("Missed Visit Report")]
        MissedVisitReport = 18,
        [Description("Orders To Be Sent")]
        OrdersToBeSent = 19,
        [Description("Orders Pending Signature")]
        OrdersPendingSignature = 20,
        [Description("Physician Order History")]
        PhysicianOrderHistory = 21,
        [Description("Plan of Care History")]
        PlanofCareHistory = 22,
        [Description("Therapy Management")]
        TherapyManagement = 23,
        [Description("13th And 19th Therapy Visit Exception")]
        ThirteenthAnd19thTherapyVisitException = 24,
        [Description("13th Therapy Re-evaluation Exception Reports")]
        ThirteenthTherapyReevaluationExceptionReports = 25,
        [Description("19th Therapy Re-evaluation Exception Reports")]
        NineteenthTherapyReevaluationExceptionReports = 26,
        [Description("Outstanding Claims")]
        OutstandingClaims = 27,
        [Description("Actual Submitted Claims")]
        ActualSubmittedClaims = 28,
        [Description("Expected Submitted Claims")]
        ExpectedSubmittedClaims = 29,
        [Description("Claims History By Status")]
        ClaimsHistoryByStatus = 30,
        [Description("Accounts Receivable Report")]
        AccountsReceivableReport = 31,
        [Description("Aged Accounts Receivable Report")]
        AgedAccountsReceivableReport = 32,
        [Description("PPS RAP Claims Needed")]
        PPSRAPClaimsNeeded = 33,
        [Description("PPS Final Claims Needed")]
        PPSFinalClaimsNeeded = 34,
        [Description("Potential Claim Auto Cancel")]
        PotentialClaimAutoCancel = 35,
        [Description("Billing Batch Report")]
        BillingBatchReport = 36,
        [Description("Billed and Unbilled Revenue Report")]
        BilledandUnbilledRevenueReport = 37,
        [Description("Earned and Unearned Revenue Report")]
        EarnedandUnearnedRevenueReport = 38,
        [Description("Earned Revenue (1/60 Method) Report")]
        EarnedRevenueReport = 39,
        [Description("Revenue Report")]
        RevenueReport = 40,
        [Description("HHRG Report")]
        HHRGReport = 41,
        [Description("Unbilled Managed Care Claims")]
        UnbilledManagedCareClaims = 42,
        [Description("Managed Care Claims Accounts Receivable")]
        ManagedCareClaimsAccountsReceivable = 43,
        [Description("Managed Care Claims History By Status")]
        ManagedCareClaimsHistoryByStatus = 44,
        [Description("Unbilled Visits for Managed Claims")]
        UnbilledVisitsforManagedClaims = 45,
        [Description("Patient Weekly Schedule")]
        PatientWeeklySchedule = 46,
        [Description("Patient Monthly Schedule")]
        PatientMonthlySchedule = 47,
        [Description("Employee Weekly Schedule")]
        EmployeeWeeklySchedule = 48,
        [Description("Employee Daily Work Schedule")]
        EmployeeDailyWorkSchedule = 49,
        [Description("Employee Monthly Work Schedule")]
        EmployeeMonthlyWorkSchedule = 50,
        [Description("Past Due Visits")]
        PastDueVisits = 51,
        [Description("Visits By Status")]
        VisitsByStatus = 52,
        [Description("Visits By Type")]
        VisitsByType = 53,
        [Description("Past Due Visits By Discipline")]
        PastDueVisitsByDiscipline = 54,
        [Description("Case Manager Task List")]
        CaseManagerTaskList = 55,
        [Description("Schedule Deviation")]
        ScheduleDeviation = 56,
        [Description("Past Due Recertification")]
        PastDueRecertification = 57,
        [Description("Upcoming Recertification")]
        UpcomingRecertification = 58,
        [Description("Patient Visit History")]
        PatientVisitHistory = 59,
        [Description("Employee Visit History")]
        EmployeeVisitHistory = 60,
        [Description("Census By Primary Insurance")]
        CensusByPrimaryInsurance = 61,
        [Description("Monthly Admission Report")]
        MonthlyAdmissionReport = 62,
        [Description("Annual Admission Report")]
        AnnualAdmissionReport = 63,
        [Description("Unduplicated Census Report by Start Of Care Date")]
        UnduplicatedCensusReportbyStartOfCareDate = 64,
        [Description("Unduplicated Census Report By Date Range")]
        UnduplicatedCensusReportByDateRange = 65,
        [Description("Medicare Cost Report")]
        MedicareCostReport = 66,
        [Description("Patient Admissions By Internal Referral Source")]
        PatientAdmissionsByInternalReferralSource = 67,
        [Description("PPS Episode Information Report")]
        PPSEpisodeInformationReport = 68,
        [Description("PPS Visit Information Report")]
        PPSVisitInformationReport = 69,
        [Description("PPS Payment Information Report")]
        PPSPaymentInformationReport = 70,
        [Description("PPS Charge Information Report")]
        PPSChargeInformationReport = 71,
        [Description("Patients And Visits By Age Report")]
        PatientsAndVisitsByAgeReport = 72,
        [Description("Discharges By Reason Report")]
        DischargesByReasonReport = 73,
        [Description("Primary Payment Source Report")]
        PrimaryPaymentSourceReport = 74,
        [Description("Type of Staff Report")]
        TypeofStaffReport = 75,
        [Description("Admissions By Referral Source Report")]
        AdmissionsByReferralSourceReport = 76,
        [Description("Principal Diagnosis Report")]
        PrincipalDiagnosisReport = 77,
        [Description("Employee Roster")]
        EmployeeRoster = 78,
        [Description("Employee License Listing")]
        EmployeeLicenseListing = 79,
        [Description("All Employee License Listing")]
        AllEmployeeLicenseListing = 80,
        [Description("Expiring Licenses")]
        ExpiringLicenses = 81,
        [Description("Employee Visit By Date Range")]
        EmployeeVisitByDateRange = 82,
        [Description("Employee Birthday Listing")]
        EmployeeBirthdayListing = 83,
        [Description("Employee Permissions Report")]
        EmployeePermissionsReport = 84,
        [Description("Payroll Detail Summary")]
        PayrollDetailSummary = 85,
        [Description("Patient List")]
        PatientList = 86,
        [Description("Employee List")]
        EmployeeList = 87
    }
   

}
