﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum AssessmentType
    {
        [Description("OASIS-C Start Of Care")]
        StartOfCare = 1,
        [Description("OASIS-C Resumption Of Care")]
        ResumptionOfCare = 3,
        [Description("OASIS-C Recertification")]
        Recertification = 4,
        [Description("OASIS-C Other Follow-up")]
        FollowUp = 5,
        [Description("OASIS-C Transfer to inpatient facility - Not Discharged")]
        Transfer = 6,
        [Description("OASIS-C Transfer to inpatient facility - Discharged")]
        TransferDischarge = 7,
        [Description("OASIS-C Death at Home")]
        Death = 8,
        [Description("OASIS-C Discharge From Agency")]
        Discharge = 9,
        [Description("OASIS C 485 (Plan of Care)")]
        PlanOfCare485 = 10,
        [Description("Non-OASIS Start Of Care")]
        NonOASISStartOfCare = 11,
        [Description("Non-OASIS Recertification")]
        NonOASISRecertification = 14,
        [Description("Non-OASIS Discharge")]
        NonOASISDischarge = 19,



        None = 0
    }
}
