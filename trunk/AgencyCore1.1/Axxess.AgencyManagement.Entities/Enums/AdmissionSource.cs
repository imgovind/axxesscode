﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum AdmissionSource
    {
        [Description("Non-Health Care Facility Point of Origin")]
        Source_1 = 1,
        [Description("Clinic or Physician's Office")]
        Source_2 = 2,
        [Description("Transfer from Hospital")]
        Source_4 = 3,
        [Description("Transfer from SNF")]
        Source_5 = 4,
        [Description("Transfer from another Health Care Facility")]
        Source_6 = 5,
        [Description("Court/Law Enforcement")]
        Source_8 = 6,
        [Description("Information Not Available")]
        Source_9 = 7,
        [Description("Transfer from Critical Access Hospital")]
        Source_A = 8
    }
}
