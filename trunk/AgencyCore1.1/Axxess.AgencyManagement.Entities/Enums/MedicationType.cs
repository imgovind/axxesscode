﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum MedicationTypeEnum
    {
        [Description("New")]
        N,
        [Description("Changed")]
        C,
        [Description("Unchanged")]
        U
    }
}
