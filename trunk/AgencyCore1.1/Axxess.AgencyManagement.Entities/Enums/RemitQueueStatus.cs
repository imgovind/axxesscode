﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum RemitQueueStatus
    {
        queued,
        processing,
        completed,
        unsucessful
    }
}
