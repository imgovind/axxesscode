﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum PayerTypes
    {
        [Description("None; no charge for current services")]
        None = 1,
        [Description("Medicare (HMO/managed care)")]
        MedicareHMO = 2,
        [Description("Medicare (traditional fee-for-service)")]
        MedicareTraditional = 3,
        [Description("Medicaid (HMO/managed care)")]
        MedicaidHMO = 4,
        [Description("Medicaid (traditional fee-for-service)")]
        MedicaidTraditional = 5,
        [Description("Title programs (e.g. Title III, V, or XX)")]
        TitlePrograms = 6,
        [Description("Other government(e.g. CHAMPUS, VA, etc.)")]
        OtherGovernment = 7,
        [Description("Private insurance")]
        PrivateInsurance = 8,
        [Description("Private HMO/managed care")]
        PrivateHMO = 9,
        [Description("Self-pay")]
        Selfpay = 10,
        [Description("Other(specify)")]
        Other = 11,
        [Description("Unknown")]
        Unknown = 12,
        [Description("Contract")]
        Contract = 13,
        [Description("Workers compensation")]
        WorkersCompensation = 14
    }
}
