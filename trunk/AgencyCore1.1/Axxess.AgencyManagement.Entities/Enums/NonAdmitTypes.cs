﻿using Axxess.Core.Infrastructure;
namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum UserStatus 
    {
        [CustomDescription("Active", "Active")]
        Active = 1,
        [CustomDescription("Inactive", "Inactive")]
        Inactive = 2
    }
}
