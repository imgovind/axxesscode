﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [Flags]
    public enum DocumentType : ulong
    {
        None=0,
        MissedVisit = 1,
        Notes = 1 << 1,
        OASIS = 1 << 2,
        PlanOfCare = 1 << 3,
        PhysicianOrder = 1 << 4,
        CommunicationNote = 1 << 5,
        FaceToFaceEncounter = 1 << 6,
        Infection = 1 << 7,
        IncidentAccident = 1 << 8,
        MedicareEligibility = 1 << 9
    }
}
