﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum MedicationCategoryEnum
    {
        Active,
        DC,       
        Deleted
    }
}
