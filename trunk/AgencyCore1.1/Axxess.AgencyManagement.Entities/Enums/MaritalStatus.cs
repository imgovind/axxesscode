﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum MaritalStatus
    {
        [Description("Married")]
        Married,
        [Description("Divorce")]
        Divorce,
        [Description("Widowed")]
        Widowed,
        [Description("Single")]
        Single,
        [Description("Separated")]
        Separated,
        [Description("Unknown")]
        Unknown
    }
}
