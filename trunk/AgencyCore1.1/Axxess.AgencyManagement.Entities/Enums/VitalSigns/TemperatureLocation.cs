﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public enum TemperatureLocation
    {
        None = 0,
        Axillary = 1,
        Oral = 2,
        Tympanic = 3,
        Temporal = 4
    }
}
