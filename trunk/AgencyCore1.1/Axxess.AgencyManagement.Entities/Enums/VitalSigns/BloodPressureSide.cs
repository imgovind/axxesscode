﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities
{
    public enum BloodPressureSide
    {
        [Description("N/A")]
        None = 0,
        Left = 1,
        Right = 2
    }
}
