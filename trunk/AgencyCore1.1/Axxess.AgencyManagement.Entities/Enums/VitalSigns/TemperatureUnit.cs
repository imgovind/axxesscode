﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum TemperatureUnit
    {
        [Description("°F")]
        F = 1,
        [Description("°C")]
        C = 2
    }
}
