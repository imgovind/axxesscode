﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public enum WeightUnit
    {
        lbs = 1,
        kgs = 2
    }
}
