﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities
{
    public enum BloodPressurePosition
    {
        [Description("N/A")]
        None = 0,
        Lying = 1,
        Sitting = 2,
        Standing = 3
    }
}
