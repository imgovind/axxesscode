﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities
{
    public enum OxygenSaturationType
    {
        [Description("N/A")]
        None = 0,
        [Description("On O₂")]
        Oxygen = 1,
        [Description("On Room Air")]
        RoomAir = 2,
    }
}
