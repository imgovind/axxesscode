﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities
{
    public enum PulseLocation
    {
        Radial = 1,
        Apical = 2
    }
}
