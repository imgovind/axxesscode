﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum HeightUnit
    {
        inch = 1,
        cm = 2
    }
}
