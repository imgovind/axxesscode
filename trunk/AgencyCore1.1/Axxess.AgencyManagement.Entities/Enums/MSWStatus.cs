﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum MSWStatus
    {
        Evaluation,
        Discharge,
        Visit
    }
}
