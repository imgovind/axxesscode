﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum FormAction
    {
         [Description("Add")]
        Add,
        [Description("Delete")]
        Delete,
        [Description("Edit")]
        Edit,
        [Description("Restore")]
        Restore,
    }
}
