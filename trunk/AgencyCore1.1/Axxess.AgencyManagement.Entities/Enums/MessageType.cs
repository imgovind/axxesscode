﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum MessageType
    { 
        User = 0,
        System = 1,
        Sent = 2,
        Custom = 3
    }
}
