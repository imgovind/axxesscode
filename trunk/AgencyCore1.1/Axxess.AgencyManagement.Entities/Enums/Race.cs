﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum Race
    {
        [Description("American Indian or Alaska Native")]
        RaceAmericanIndian = 0,
        [Description("Asian")]
        RaceAsian = 1,
        [Description("Black or African-American")]
        RaceBlack = 2,
        [Description("Hispanic or Latino")]
        RaceHispanic = 3,
        [Description("Native Hawaiian or Pacific Islander")]
        RaceHawaiian = 4,
        [Description("White")]
        RaceWhite = 5,
        [Description("Unknown")]
        RaceUnknown = 6
    }
}
