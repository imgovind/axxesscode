﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using Axxess.Core.Infrastructure;

    public enum ReferralSource
    {
        [CustomDescription("Non-Health Care Facility Point of Origin", "")]
        NonHealthCareFacilityPointOfOrigin = 1,
        [CustomDescription("Clinic or Physician's Office", "")]
        ClinicOrPhysiciansOffice = 2,
        [CustomDescription("Transfer from Hospital", "")]
        TransferFromHospital = 3,
        [CustomDescription("Transfer from SNF", "")]
        TransferFromSNF = 4,
        [CustomDescription("Transfer from another Health Care Facility", "")]
        TransferFromAnotherHealthCareFacility = 5,
        [CustomDescription("Court/Law Enforcement", "")]
        CourtLawEnforcement = 6,
        [CustomDescription("Information Not Available", "")]
        InformationNotAvailable = 7,
        [CustomDescription("Transfer from Critical Access Hospital", "")]
        TransferFromCriticalAccessHospital = 8
    }
}
