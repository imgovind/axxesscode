﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum InsuranceType
    {
        [Description("Primary Insurance")]
        Primary = 1,
        [Description("Secondary Insurance")]
        Secondary = 2,
        [Description("Tertiary Insurance")]
        Tertiary = 4
    }
}
