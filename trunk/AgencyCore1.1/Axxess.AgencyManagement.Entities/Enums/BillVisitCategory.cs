﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum BillVisitCategory
    {
        [CustomDescription("Billable Visits", "Billable")]
        Billable,
        [CustomDescription("Non-Billable Visits", "NonBillable")]
        NonBillable,
        [CustomDescription("Missed Visit", "MissedVisit")]
        MissedVisit
    }
}
