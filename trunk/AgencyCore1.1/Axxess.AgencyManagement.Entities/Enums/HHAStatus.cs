﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum HHAStatus
    {
        [Description("Visit")]
        Visit,
        [Description("Supervisory Visit")]
        SupervisoryVisit
    }
}
