﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum TableToMove
    {
        [CustomDescription("Schedule Event", "scheduleevents")]
        ScheduleEvent = 1000,
        [CustomDescription("Start Of Care", "startofcareassessments")]
        StartOfCareAssessment = 1001,
        [CustomDescription("Resumption Of Care", "resumptionofcareassessments")]
        ResumptionOfCareAssessment = 1002,
        [CustomDescription("Recertification", "recertificationassessments")]
        RecertificationAssessment = 1003,
        [CustomDescription("Follow Up", "followupassessments")]
        FollowUpAssessment = 1004,
        [CustomDescription("Transfer Discharge", "transferdischargeassessments")]
        TransferDischargeAssessment = 1005,
        [CustomDescription("Transfer Not Discharged", "transfernotdischargedassessments")]
        TransferAssessment = 1006,
        [CustomDescription("Death At Home", "deathathomeassessments")]
        DeathAssessment = 1007,
        [CustomDescription("Discharge From Agency", "dischargefromagencyassessments")]
        DischargeAssessment = 1008,
        [CustomDescription(" Non Oasis Discharge", "nonoasisdischargeassessments")]
        NonOASISDischargeAssessment = 1009,
        [CustomDescription("Non Oasis Recertification", "nonoasisrecertificationassessments")]
        NonOASISRecertificationAssessment = 1010,
        [CustomDescription("Non Oasis Start Of Care", "nonoasisstartofcareassessments")]
        NonOASISStartOfCareAssessment = 1011
    }
}
