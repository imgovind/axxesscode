﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum StaticBalance 
    {
        [Description("N = Maintain static sitting/standing with maximum challenges from all directions")]
        N = 1,
        [Description("G = Maintain static sitting/standing with moderate challenges from all directions.")]
        G = 2,
        [Description("F+ = Maintain static sitting/standing balance with minimal challenges from all directions.")]
        FPlus = 3,
        [Description("F = Able to sit/stand unsupported without balance loss or UE support.")]
        F = 4,
        [Description("F- = Maintain static sitting/standing balance with CG assist.")]
        FMinus = 5,
        [Description("P+ = Maintain static sitting/standing balance with minimal assist.")]
        PPlus = 6,
        [Description("P = Maintain static sitting/standing balance with moderate assist.")]
        P = 7,
        [Description("P- = Maintain static sitting/standing balance with maximum assist.")]
        PMinus = 8,
        [Description("U = Unable to attempt.")]
        U = 9
    }
}
