﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum AuthorizationStatusTypes : byte
    {
        [Description("Active")]
        Active,
        [Description("Pending")]
        Pending,
        [Description("Obtained")]
        Obtained,
        [Description("Closed")]
        Closed,
        [Description("Denied")]
        Denied
    }
}
