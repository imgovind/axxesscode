﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum CptCodes
    {
        [Description("HHA Initial Certification")]
        G0180 = 1,
        [Description("HHA Re-Certification")]
        G0179 = 2,
        [Description("Home Health Care Supervision")]
        G0181 = 3
    }
}
