﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities.Enums
{
    public enum VitalPosition
    {
        Bilateral = 0,
        Left = 1,
        Right = 2
    }
}
