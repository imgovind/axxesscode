﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum Sensory
    {
        [Description("Intact")]
        Intact = 1,
        [Description("Minimally Impaired")]
        MinimallyImpaired = 2,
        [Description("Moderately Impaired")]
        ModeratelyImpaired = 3,
        [Description("Severely Impaired")]
        SeverelyImpaired = 4,
        [Description("Unable to Test")]
        UnableToTest = 5
    }
}
