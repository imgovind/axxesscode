﻿namespace Axxess.AgencyManagement.Entities.Enums
{

    using System.ComponentModel;

    public enum BillType
    {
        [Description("Home Health - PPS RAP")]
        HHPPSRAP = 322,
        [Description("Home Health - PPS Final")]
        HHPPSFinal = 329,
        [Description("Home Health - Admit thru Discharge")]
        HHAdmitThruDischarge = 331,
        [Description("Home Health - 1st Claim")]
        HHFirstClaim = 332,
        [Description("Home Health - Continuing Claim")]
        HHContinuingClaim = 333,
        [Description("Home Health - Last Claim")]
        HHLastClaim = 334,
        [Description("OPT - Admit thru Discharge")]
        OPTAdmitThruDischarge = 341,
        [Description("OPT - 1st Claim")]
        OPTFirstClaim = 342,
        [Description("OPT - Continuing Claim")]
        OPTContinuingClaim = 343,
        [Description("OPT - Last Claim")]
        OPTLastClaim = 344,
        [Description("Hospice - Admit thru Discharge")]
        HospiceAdmitThruDischarge = 811,
        [Description("Hospice - 1st Claim")]
        HospiceFirstClaim = 812,
        [Description("Hospice - Continuing Claim")]
        HospiceContinuingClaim = 813,
        [Description("Hospice - Last Claim")]
        HospiceLastClaim = 814

    }
}
