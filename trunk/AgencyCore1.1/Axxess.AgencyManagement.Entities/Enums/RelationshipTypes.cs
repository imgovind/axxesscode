﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum RelationshipTypes
    {
        [Description("Spouse")]
        R_01 = 1,
        [Description("Self")]
        R_18 = 2,
        [Description("Child")]
        R_19 = 3,
        [Description("Employee")]
        R_20 = 4,
        [Description("Unknown")]
        R_21 = 5,
        [Description("Organ donor")]
        R_39 = 6,
        [Description("Cadaver donor")]
        R_40 = 7,
        [Description("Life partner")]
        R_53 = 8,
        [Description("Other")]
        R_G8 = 9
    }
}
