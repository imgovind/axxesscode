﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using Axxess.Core.Infrastructure;

    public enum PatientDateType
    {
        [CustomDescription("Start Of Care Date", "SOC")]
        StartOfCareDate = 1,
        [CustomDescription("Discharge Date", "DCD")]
        DischargeDate = 2
    }
}
