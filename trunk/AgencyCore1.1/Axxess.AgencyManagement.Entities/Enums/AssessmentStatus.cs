﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;

    public enum AssessmentStatus
    {
        [Description("Scheduled")]
        Scheduled,
        [Description("Not Started")]
        NotStarted,
        [Description("Past Due")]
        PastDue,
        [Description("Completed (ready)")]
        Completed,
        [Description("Open")]
        Open,
        [Description("Submitted (exported)")]
        Submitted,
        [Description("Reactivated (ready)")]
        Reactivated
    }
}
