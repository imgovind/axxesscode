﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Used to identify patient self pay(patient or any individual payor for the patient) from the other type of payor 
    /// </summary>
    public enum PayorTypeMainCategory
    {
        PrivatePayor = 10,
        NonPrivatePayor = 11
    }
}
