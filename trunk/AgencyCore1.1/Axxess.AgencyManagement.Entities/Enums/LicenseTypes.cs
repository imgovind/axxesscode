﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum LicenseTypes : byte
    {
        [Description("CNA License")]
        CNA,
        [Description("COTA License")]
        COTA,
        [Description("LSLP License")]
        LSLP,
        [Description("LVN License")]
        LVN,
        [Description("MSW License")]
        MSW,
        [Description("OT License")]
        OT,
        [Description("PT License")]
        PT,
        [Description("PTA License")]
        PTA,
        [Description("RN License")]
        RN,
        [Description("STA License")]
        STA,
        [Description("Auto Insurance")]
        Auto,
        [Description("Blood Bourne Pathogen Training")]
        BloodBourne,
        [Description("CEU Credit")]
        CEU,
        [Description("CPR")]
        CPR,
        [Description("Driver License")]
        DriverLicense,
        [Description("HIPAA Compliance Training")]
        HIPAA,
        [Description("Performance Evaluation")]
        PerformanceEvaluation,
        [Description("TB")]
        TB,
        [Description("Competency Evaluation")]
        CompetencyEvaluation,
        [Description("SkillsChecklist (SNV / HHA)")]
        SkillsConsent,
        [Description("Criminal History Consent")]
        CriminalHistoryConsent,
        [Description("OIG Verification")]
        OIG,
        [Description("Other")]
        Other
    }
}
