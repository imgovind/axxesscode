﻿namespace Axxess.AgencyManagement.Entities.Enums
{
    using System.ComponentModel;
    public enum TitleTypes : byte
    {
        [Description("CEO")]
        CEO,
        [Description("Administrator")]
        Administrator,
        [Description("Alternate Administrator")]
        AlternateAdministrator,
        [Description("Director of Nursing")]
        DON,
        [Description("Alternate Director of Nursing")]
        AlternateDON,
        [Description("CFO")]
        CFO,
        [Description("Community Liaison Officer/Marketer")]
        CLO,
        [Description("Registered Nurse")]
        RegisteredNurse,
        [Description("Case Manager")]
        CaseManager,
        [Description("LVN/LPN")]
        LVN,
        [Description("Certified Nurse Aide")]
        CNA,
        [Description("Intake Coordinator")]
        IntakeCoordinator,
        [Description("Physical Therapist")]
        PT,
        [Description("Physical Therapist Assistant")]
        PTAssistant,
        [Description("Occupational Therapist")]
        OT,
        [Description("Occupational Therapist Assistant")]
        OTAssistant,
        [Description("Speech Therapist")]
        ST,
        [Description("Speech Therapist Assistant")]
        STAssistant,
        [Description("QA Nurse")]
        QANurse,
        [Description("Office Manager")]
        OfficeManager,
        [Description("Administrative Assistant")]
        AdminAssistant,
        [Description("Home Health Aide/Nurse Aide")]
        HHA,
        [Description("Scheduler")]
        Scheduler,
        [Description("Disaster Coordinator")]
        DisasterCoordinator,
        [Description("Other")]
        Other,
        [Description("Medical Social Worker")]
        MSW
    }
}
