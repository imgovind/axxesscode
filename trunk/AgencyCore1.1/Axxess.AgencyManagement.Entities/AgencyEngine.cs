﻿namespace Axxess.AgencyManagement.Entities
{
    using System;

    using Axxess.Core.Infrastructure;

    using Repositories;
    

    public static class AgencyEngine
    {
        #region Private Members

        //private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        //public static Agency Get(Guid agencyId)
        //{
        //    Agency agency = null;
        //    //var agencyInfo = cacheAgent.GetAgencyXml(agencyId);
        //    //if (agencyInfo.IsNotNullOrEmpty())
        //    //{
        //    //    agency = agencyInfo.ToObject<Agency>();
        //    //}
        //    //else
        //    //{
        //        agency = dataProvider.AgencyRepository.GetWithBranches(agencyId);
        //    //}
        //    return agency;
        //}

        public static void Refresh(Guid agencyId)
        {
            //cacheAgent.RefreshAgency(agencyId);
        }

        #endregion

    }
}
