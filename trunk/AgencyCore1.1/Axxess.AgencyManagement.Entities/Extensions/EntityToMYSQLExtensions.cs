﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    

    public static class EntityToMysqlExtensions
    {
        public static string InserAgencyLocation(this AgencyLocation agencyLocation)
        {
            var query = string.Empty;
            if (agencyLocation != null)
            {
                query = string.Format(@"INSERT INTO 
                                        agencylocations ({0}) 
                                                  values('{1}');",
                    "Id", agencyLocation.Id,
                    "AgencyId", agencyLocation.AgencyId,
                    "Name", agencyLocation.Name,
                    "CustomId", agencyLocation.CustomId,
                    "CBSA", agencyLocation.CBSA,
                    "MedicaidProviderNumber", agencyLocation.MedicaidProviderNumber,
                    "AddressLine1", agencyLocation.AddressLine1,
                    "AddressLine2", agencyLocation.AddressLine2,
                    "AddressCity", agencyLocation.AddressCity,
                    "AddressStateCode", agencyLocation.AddressStateCode,
                    "AddressZipCode", agencyLocation.AddressZipCode,
                    "AddressZipCodeFour", agencyLocation.AddressZipCodeFour,
                    "PhoneWork", agencyLocation.PhoneWork,
                    "FaxNumber", agencyLocation.FaxNumber,
                    "Comments", agencyLocation.Comments,
                    "IsMainOffice", agencyLocation.IsMainOffice,
                    "Created", agencyLocation.Created,
                    "Modified", agencyLocation.Modified,
                    "Cost", agencyLocation.Cost,
                    "BillData", agencyLocation.BillData,
                    "IsDeprecated", agencyLocation.IsDeprecated,
                    "IsSubmitterInfoTheSame", agencyLocation.IsSubmitterInfoTheSame,
                    "SubmitterId", agencyLocation.SubmitterId,
                    "SubmitterName", agencyLocation.SubmitterName,
                    "SubmitterPhone", agencyLocation.SubmitterPhone,
                    agencyLocation.SubmitterFax,
                    agencyLocation.Payor,
                    agencyLocation.BranchId,
                    agencyLocation.BranchIdOther,
                    agencyLocation.Ub04Locator81cca,
                    agencyLocation.TaxId,
                    agencyLocation.TaxIdType,
                    agencyLocation.NationalProviderNumber,
                    agencyLocation.MedicaidProviderNumber,
                    agencyLocation.HomeHealthAgencyId,
                    agencyLocation.ContactPersonFirstName,
                    agencyLocation.ContactPersonLastName,
                    agencyLocation.ContactPersonEmail,
                    agencyLocation.ContactPersonPhone,
                    agencyLocation.CahpsVendor,
                    agencyLocation.CahpsVendorId,
                    agencyLocation.CahpsSurveyDesignator,
                    agencyLocation.IsAxxessTheBiller,
                    agencyLocation.OasisAuditVendor,
                    agencyLocation.IsLocationStandAlone);
            }
            return query;
        }
    }
}
