﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using System;

    public static class AgencyServicesExtensions
    {
        public static bool Has(this AgencyServices service, AgencyServices value)
        {
            try
            {
                return (service == 0 && value == 0) || ((service & value) == value);
            }
            catch
            {
                return false;
            }
        }

        public static bool HasValidValue(this AgencyServices service)
        {
            try
            {
                return ((int)service != 0 );
            }
            catch
            {
                return false;
            }
        }


        public static string ToArea(this AgencyServices service)
        {
            return service != AgencyServices.HomeHealth && service != AgencyServices.None && service.IsAlone() ? service.ToString() : string.Empty;
        }

        public static bool IsAlone(this AgencyServices service)
        {
            try
            {
                return service == 0 || ((int)service).IsPowerOfTwo();
            }
            catch
            {
                return false;
            }
        }

        public static AgencyServices GetTheFirstIfThePreferredNotIncluded(this AgencyServices services, AgencyServices prefrredService)
        {
            var outService = AgencyServices.None;
            if (services != AgencyServices.None)
            {
                if ((services & prefrredService) == prefrredService)
                {
                    outService = prefrredService;
                }
                else
                {
                    if (services.IsAlone())
                    {
                        outService = services;
                    }
                    else
                    {
                        outService = services.GetFirst();
                    }
                }
            }
            return outService;
        }


        public static AgencyServices GetFirst(this AgencyServices services)
        {
            var outService = AgencyServices.None;
            if (services != AgencyServices.None)
            {
                var values = services.ToString().Split(',');
                if (values.IsNotNullOrEmpty())
                {
                    outService = values.Select(x => (AgencyServices)Enum.Parse(typeof(AgencyServices), x.Trim())).FirstOrDefault();
                }
            }
            return outService;
        }

        public static List<AgencyServices> ToList(this AgencyServices services)
        {
            if (!services.IsAlone())
            {
                var values = services.ToString().Split(',');
                if (values.IsNotNullOrEmpty())
                {
                    return values.Select(x => (AgencyServices)Enum.Parse(typeof(AgencyServices), x.Trim())).ToList();
                }
            }
            return new List<AgencyServices>() { services };
        }

        public static List<int> ToIntList(this AgencyServices services)
        {
            if (!services.IsAlone())
            {
                var values = services.ToString().Split(',');
                if (values.IsNotNullOrEmpty())
                {
                    return values.Select(x => (int)Enum.Parse(typeof(AgencyServices), x)).ToList();
                }
            }
            return new List<int>() { (int)services };
        }

    }
}
