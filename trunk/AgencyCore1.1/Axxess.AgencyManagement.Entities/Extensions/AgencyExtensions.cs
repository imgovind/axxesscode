﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    
    using Axxess.Core.Extension;

    public static class AgencyExtensions
    {
        public static bool HasTrialPeriodExpired(this Agency agency)
        {
            var expirationDate = agency.Created.Add(TimeSpan.FromDays(agency.TrialPeriod));
            if (!agency.IsAgreementSigned && DateTime.Now > expirationDate)
            {
                return true;
            }
            return false;
        }

        public static AgencyLocation GetMainOffice(this Agency agency)
        {
            if (agency.Branches != null && agency.Branches.Count > 0)
            {
                return agency.Branches.Where(b => b.IsMainOffice == true).SingleOrDefault();
            }
            return null;
        }

        public static AgencyLocation GetBranch(this Agency agency, Guid locationId)
        {
            if (agency != null && agency.Branches != null && agency.Branches.Count > 0)
            {
                return agency.Branches.Where(b => b.Id == locationId).SingleOrDefault();
            }
            return null;
        }
    }
}
