﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;

    public static class QuestionDictionaryExtensions
    {
        public static void AddIfNotEmpty<T>(this IDictionary<string, T> to, IDictionary<string, T> from, string key) where T : QuestionBase, new()
        {
            var value = from.Get(key);
            if (value != null && value.Answer.IsNotNullOrEmpty())
            {
                to.Add(key, value);
            }
        }

        public static void AddIfNotEmpty<T>(this IList<string> to, IDictionary<string, T> from, string key, string prefix)
            where T : QuestionBase, new()
        {
            var value = from.AnswerOrEmptyString(key);
            if (value.IsNotNullOrEmpty())
            {
                to.Add(prefix + value);
            }
        }

        public static string AnswerOrEmptyString<T>(this IDictionary<string, T> questions, string key)
            where T : QuestionBase, new()
        {
            string answer = string.Empty;
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    answer = question.Answer;
                }
            }
            return answer;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        {
            string answer = string.Empty;
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    answer = question.Answer.Unclean();
                    if (key.IsEqual("ICD9M") || key.IsEqual("ICD9M1"))
                    {
                        answer = answer.TrimEndPeriod();
                    }
                }
            }
            return answer;
        }

        public static Guid AnswerOrEmptyGuid<T>(this IDictionary<string, T> questions, string key)
            where T : QuestionBase, new()
        {
            if (questions != null && questions.Count > 0)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    return question.Answer.ToGuid();
                }
            }
            return Guid.Empty;
        }


        public static string AnswerValidDateOrEmptyString<T>(this IDictionary<string, T> questions, string key)
            where T : QuestionBase, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() && answer.IsValidDateAndNotMin() ? answer : string.Empty;
        }

        public static DateTime AnswerOrMinDate<T>(this IDictionary<string, T> questions, string key)
             where T : QuestionBase, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.ToDateTimeOrMin() : DateTime.MinValue;
        }

        public static Double AnswerDoubleOrZero<T>(this IDictionary<string, T> questions, string key)
             where T : QuestionBase, new()
        {
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    return question.Answer.ToDoubleAfterNonDigitStrip();
                }
            }
            return 0.0;
        }

        public static string AnswerForDropDown<T>(this IDictionary<string, T> questions, string key, Dictionary<string, string> possibleAnswers)
             where T : QuestionBase, new()
        {
            string answer = string.Empty;
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    answer = question.Answer.Unclean();
                    foreach (KeyValuePair<string, string> pa in possibleAnswers)
                    {
                        if (answer.Equals(pa.Key))
                        {
                            answer = pa.Value;
                            break;
                        }
                    }
                }
            }
            return answer;
        }

        public static string AnswerOrDefault<T>(this IDictionary<string, T> questions, string key, string defaultValue)
             where T : QuestionBase, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer : defaultValue;
        }

        public static string[] AnswerArray<T>(this IDictionary<string, T> questions, string key, char separator)
             where T : QuestionBase, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.Split(separator) : new string[] { };
        }

        public static string[] AnswerArray<T>(this IDictionary<string, T> questions, string key)
             where T : QuestionBase, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.Split(',') : new string[] { };
        }

        public static string ToFrequencyString(this IDictionary<string, Question> assessmentQuestions)
        {
            var frequencyData = string.Empty;
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                var frequencyList = new List<string>();
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485SNFrequency", "SN Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485PTFrequency", "PT Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485OTFrequency", "OT Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485STFrequency", "ST Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485MSWFrequency", "MSW Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485HHAFrequency", "HHA Frequency:");
                if (frequencyList.Count > 0)
                {
                    frequencyData = frequencyList.ToArray().Join(", ") + ".";
                }
            }
            return frequencyData;
        }

        public static string ToClaimDiagonasisCodesXml(this IDictionary<string, Question> assessmentQuestions)
        {
            string diagnosis = string.Empty;
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                var diagnosisCodes = new DiagnosisCodes();
                diagnosisCodes.Primary = assessmentQuestions.AnswerOrDefault("M1020ICD9M", null);
                diagnosisCodes.Second = assessmentQuestions.AnswerOrDefault("M1020ICD9M1", null);
                diagnosisCodes.Third = assessmentQuestions.AnswerOrDefault("M1020ICD9M2", null);
                diagnosisCodes.Fourth = assessmentQuestions.AnswerOrDefault("M1020ICD9M3", null);
                diagnosisCodes.Fifth = assessmentQuestions.AnswerOrDefault("M1020ICD9M4", null);
                diagnosisCodes.Sixth = assessmentQuestions.AnswerOrDefault("M1020ICD9M5", null);
                diagnosis = diagnosisCodes.ToXml();
            }
            return diagnosis;
        }

        public static IDictionary<string, Question> ToDiagnosis(this IDictionary<string, Question> assessmentQuestions)
        {
            var diagnosis = new Dictionary<string, Question>();
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1020PrimaryDiagnosis");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1020ICD9M");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis1");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M1");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis2");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M2");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis3");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M3");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis4");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M4");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis5");
                    diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M5");
            }
            return diagnosis;
        }

        public static IDictionary<string, Question> ToAllergies(this IDictionary<string, Question> assessmentQuestions)
        {
            var allergies = new Dictionary<string, Question>();
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                allergies.AddIfNotEmpty(assessmentQuestions, "485Allergies");
                allergies.AddIfNotEmpty(assessmentQuestions, "485AllergiesDescription");
            }
            return allergies;
        }

        public static Question GetQuestion(this IDictionary<string, Question> assessmentQuestions, string questionName)
        {
            return assessmentQuestions.Get(questionName) ?? new Question();
        }

        public static IDictionary<string, Question> RemovePrimaryDiagnosis(this IDictionary<string, Question> questions)
        {
            if (questions == null || questions.Count == 0)
            {
                return new Dictionary<string, Question>();
            }
            questions.RemoveAll<string, Question>(i => i.Code == "M1020" || i.Code == "M1022");
            return questions;
        }

        //public static List<Question> Get485FromAssessment(Guid patientId, List<Question> questions)
        //{
        //    var planofCareQuestions = new List<Question>();
        //    //if (questions != null && questions.Count > 0)
        //    //{
        //    //    questions.ForEach(q =>
        //    //    {
        //    //        if (q.Type == QuestionType.Generic)
        //    //        {
        //    //        }
        //    //        else if (q.Type == QuestionType.PlanofCare)
        //    //        {
        //    //        }
        //    //        else if (q.Type == QuestionType.Moo)
        //    //        {
        //    //        }
        //    //    });
        //    //}

        //    if (questions.IsNotNullOrEmpty())
        //    {
        //        var assessmentQuestions = questions.ToOASISDictionary();
        //        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
        //        {
        //            // 10. Medications
        //            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
        //            if (medicationProfile != null)
        //            {
        //                planofCareQuestions.Add(Question.Create("485Medications", medicationProfile.ToString()));
        //            }

        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("M0102PhysicianOrderedDate"));

        //            // 11. Principal Diagnosis
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020PrimaryDiagnosis"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020ICD9M"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ExacerbationOrOnsetPrimaryDiagnosis"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020PrimaryDiagnosisDate"));

        //            // 12. Surgical Procedure
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureDescription1"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode1"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode1Date"));

        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureDescription2"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode2"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode2Date"));

        //            // 13. Other Pertinent Diagnosis
        //            for (int count = 1; count <= 14; count++)
        //            {
        //                planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022PrimaryDiagnosis{0}", count)));
        //                planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022ICD9M{0}", count)));
        //                planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("485ExacerbationOrOnsetPrimaryDiagnosis{0}", count)));
        //                planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022PrimaryDiagnosis{0}Date", count)));
        //            }

        //            // 14. DME and Supplies
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485DME"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485DMEComments"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Supplies"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SuppliesComment"));

        //            // 15. Safety Measures
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SafetyMeasures"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485OtherSafetyMeasures"));

        //            // 16. Nutritional Requirements
        //            if (assessmentQuestions.ContainsKey("485NutritionalReqs") && assessmentQuestions["485NutritionalReqs"] != null)
        //            {
        //                planofCareQuestions.Add(Question.Create("485NutritionalReqs", PlanofCareXml.ExtractText("NutritionalRequirements", assessmentQuestions)));
        //            }

        //            // 17. Allergies
        //            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Allergies"));
        //            if (assessmentQuestions.ContainsKey("485AllergiesDescription") && assessmentQuestions["485AllergiesDescription"] != null)
        //            {
        //                planofCareQuestions.Add(assessmentQuestions.GetQuestion("485AllergiesDescription"));
        //            }
        //            else
        //            {
        //                planofCareQuestions.Add(Question.Create("485AllergiesDescription", allergyProfile != null ? allergyProfile.ToString() : string.Empty));
        //            }

        //            // 18.A. Functional Limitations
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485FunctionLimitations"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485FunctionLimitationsOther"));

        //            // 18.B. Activities Permitted
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ActivitiesPermitted"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ActivitiesPermittedOther"));


        //            // 19. Mental Status
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485MentalStatus"));
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485MentalStatusOther"));

        //            // 20. Prognosis
        //            planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Prognosis"));

        //            // 21. Interventions
        //            var interventions = string.Empty;
        //            if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"] != null && assessmentQuestions["485SNFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("SN Frequency: {0}. ", assessmentQuestions["485SNFrequency"].Answer);
        //            }
        //            if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"] != null && assessmentQuestions["485PTFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("PT Frequency: {0}. ", assessmentQuestions["485PTFrequency"].Answer);
        //            }
        //            if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"] != null && assessmentQuestions["485OTFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("OT Frequency: {0}. ", assessmentQuestions["485OTFrequency"].Answer);
        //            }
        //            if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"] != null && assessmentQuestions["485STFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("ST Frequency: {0}. ", assessmentQuestions["485STFrequency"].Answer);
        //            }
        //            if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"] != null && assessmentQuestions["485MSWFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("MSW Frequency: {0}. ", assessmentQuestions["485MSWFrequency"].Answer);
        //            }
        //            if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"] != null && assessmentQuestions["485HHAFrequency"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += string.Format("HHA Frequency: {0}. ", assessmentQuestions["485HHAFrequency"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("485OrdersDisciplineInterventionComments") && assessmentQuestions["485OrdersDisciplineInterventionComments"] != null && assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer.IsNotNullOrEmpty())
        //            {
        //                interventions += assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer;
        //            }

        //            var baseVitalSignFormat = "{0} greater than (>) {1} or less than (<) {2}. ";

        //            var vitalSignParameters = string.Empty;
        //            if (assessmentQuestions.ContainsKey("GenericTempGreaterThan")
        //               && assessmentQuestions["GenericTempGreaterThan"] != null
        //               && assessmentQuestions["GenericTempGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericTempLessThan")
        //               && assessmentQuestions["GenericTempLessThan"] != null
        //               && assessmentQuestions["GenericTempLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Temperature", assessmentQuestions["GenericTempGreaterThan"].Answer, assessmentQuestions["GenericTempLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericPulseGreaterThan")
        //               && assessmentQuestions["GenericPulseGreaterThan"] != null
        //               && assessmentQuestions["GenericPulseGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericPulseLessThan")
        //               && assessmentQuestions["GenericPulseLessThan"] != null
        //               && assessmentQuestions["GenericPulseLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Pulse", assessmentQuestions["GenericPulseGreaterThan"].Answer, assessmentQuestions["GenericPulseLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericRespirationGreaterThan")
        //               && assessmentQuestions["GenericRespirationGreaterThan"] != null
        //               && assessmentQuestions["GenericRespirationGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericRespirationLessThan")
        //               && assessmentQuestions["GenericRespirationLessThan"] != null
        //               && assessmentQuestions["GenericRespirationLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Respirations", assessmentQuestions["GenericRespirationGreaterThan"].Answer, assessmentQuestions["GenericRespirationLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericSystolicBPGreaterThan")
        //               && assessmentQuestions["GenericSystolicBPGreaterThan"] != null
        //               && assessmentQuestions["GenericSystolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericSystolicBPLessThan")
        //               && assessmentQuestions["GenericSystolicBPLessThan"] != null
        //               && assessmentQuestions["GenericSystolicBPLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Systolic BP", assessmentQuestions["GenericSystolicBPGreaterThan"].Answer, assessmentQuestions["GenericSystolicBPLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericDiastolicBPGreaterThan")
        //               && assessmentQuestions["GenericDiastolicBPGreaterThan"] != null
        //               && assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericDiastolicBPLessThan")
        //               && assessmentQuestions["GenericDiastolicBPLessThan"] != null
        //               && assessmentQuestions["GenericDiastolicBPLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Diastolic BP", assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer, assessmentQuestions["GenericDiastolicBPLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("Generic02SatLessThan")
        //               && assessmentQuestions["Generic02SatLessThan"] != null
        //               && assessmentQuestions["Generic02SatLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format("O2 Sat (percent) less than (<) {0}. ", assessmentQuestions["Generic02SatLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericFastingBloodSugarGreaterThan")
        //               && assessmentQuestions["GenericFastingBloodSugarGreaterThan"] != null
        //               && assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericFastingBloodSugarLessThan")
        //               && assessmentQuestions["GenericFastingBloodSugarLessThan"] != null
        //               && assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Fasting blood sugar", assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer, assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericRandomBloddSugarGreaterThan")
        //               && assessmentQuestions["GenericRandomBloddSugarGreaterThan"] != null
        //               && assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer.IsNotNullOrEmpty()
        //               && assessmentQuestions.ContainsKey("GenericRandomBloodSugarLessThan")
        //               && assessmentQuestions["GenericRandomBloodSugarLessThan"] != null
        //               && assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format(baseVitalSignFormat, "Random blood sugar", assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer, assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer);
        //            }

        //            if (assessmentQuestions.ContainsKey("GenericWeightGreaterThan")
        //               && assessmentQuestions["GenericWeightGreaterThan"] != null
        //               && assessmentQuestions["GenericWeightGreaterThan"].Answer.IsNotNullOrEmpty())
        //            {
        //                vitalSignParameters += string.Format("Weight Gain/Loss (lbs/7 days) Greater than {0}. ", assessmentQuestions["GenericWeightGreaterThan"].Answer);
        //            }

        //            if (vitalSignParameters.IsNotNullOrEmpty())
        //            {
        //                interventions += "SN to notify MD of: " + vitalSignParameters;
        //            }

        //            interventions += PlanofCareXml.ExtractText("Intervention", assessmentQuestions);
        //            var interventionQuestion = Question.Create("485Interventions", interventions);
        //            planofCareQuestions.Add(interventionQuestion);

        //            var goals = string.Empty;
        //            goals += PlanofCareXml.ExtractText("Goal", assessmentQuestions);
        //            if (assessmentQuestions.ContainsKey("485SensoryStatusGoalComments") && assessmentQuestions["485SensoryStatusGoalComments"] != null)
        //            {
        //                goals += assessmentQuestions["485SensoryStatusGoalComments"].Answer;
        //            }
        //            if (assessmentQuestions.ContainsKey("485RehabilitationPotential") && assessmentQuestions["485RehabilitationPotential"] != null)
        //            {
        //                if (assessmentQuestions["485RehabilitationPotential"].Answer == "1")
        //                {
        //                    goals += "Rehab Potential: Good for stated goals.";
        //                }
        //                if (assessmentQuestions["485RehabilitationPotential"].Answer == "2")
        //                {
        //                    goals += "Rehab Potential: Fair for stated goals.";
        //                }
        //                if (assessmentQuestions["485RehabilitationPotential"].Answer == "3")
        //                {
        //                    goals += "Rehab Potential: Poor for stated goals.";
        //                }
        //            }

        //            if (assessmentQuestions.ContainsKey("485AchieveGoalsComments") && assessmentQuestions["485AchieveGoalsComments"] != null && assessmentQuestions["485AchieveGoalsComments"].Answer.IsNotNullOrEmpty())
        //            {
        //                goals += assessmentQuestions["485AchieveGoalsComments"].Answer;
        //            }

        //            if (assessmentQuestions.ContainsKey("485DischargePlans") && assessmentQuestions["485DischargePlans"] != null)
        //            {
        //                if (assessmentQuestions["485DischargePlans"].Answer == "1")
        //                {
        //                    goals += "Discharge Plan: Patient to be discharged to the care of Physician. ";
        //                }
        //                if (assessmentQuestions["485DischargePlans"].Answer == "2")
        //                {
        //                    goals += "Discharge Plan: Patient to be discharged to the care of Caregiver. ";
        //                }
        //                if (assessmentQuestions["485DischargePlans"].Answer == "3")
        //                {
        //                    goals += "Discharge Plan: Patient to be discharged to Self care. ";
        //                }
        //            }

        //            if (assessmentQuestions.ContainsKey("485DischargePlansReason") && assessmentQuestions["485DischargePlansReason"] != null && assessmentQuestions["485DischargePlansReason"].Answer.IsNotNullOrEmpty())
        //            {
        //                var reasons = assessmentQuestions["485DischargePlansReason"].Answer.Split(',');
        //                foreach (string reason in reasons)
        //                {
        //                    if (reason.IsEqual("1"))
        //                    {
        //                        goals += "Discharge when caregiver willing and able to manage all aspects of patient's care. ";
        //                    }
        //                    if (reason.IsEqual("2"))
        //                    {
        //                        goals += "Discharge when goals met. ";
        //                    }
        //                    if (reason.IsEqual("3"))
        //                    {
        //                        goals += "Discharge when wound(s) healed. ";
        //                    }
        //                }
        //            }

        //            if (assessmentQuestions.ContainsKey("485DischargePlanComments") && assessmentQuestions["485DischargePlanComments"] != null && assessmentQuestions["485DischargePlanComments"].Answer.IsNotNullOrEmpty())
        //            {
        //                goals += assessmentQuestions["485DischargePlanComments"].Answer;
        //            }

        //            var goalQuestion = Question.Create("485Goals", goals);
        //            planofCareQuestions.Add(goalQuestion);
        //        }
        //    }
        //    return planofCareQuestions;
        //}

    }
}
