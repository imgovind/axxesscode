﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;

    using Enums;

    using Axxess.Core;

    public static class PermissionExtensions
    {
        public static bool Has(this Permissions permission, Permissions value)
        {
            try
            {
                return (((ulong)(Permissions)permission & (ulong)(Permissions)value) == (ulong)(Permissions)value);
            }
            catch
            {
                return false;
            }
        }
    }
}
