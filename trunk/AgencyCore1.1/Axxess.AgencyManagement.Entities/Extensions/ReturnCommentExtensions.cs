﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using Axxess.Core.Extension;
    
    public static class ReturnCommentExtensions
    {

        [DebuggerStepThrough]
        public static string ToCommentText(this List<ReturnComment> target, string reasonFromTheTask, Guid currentUserId)
        {
            var commentString = string.Empty;
            try
            {
                if (reasonFromTheTask.IsNotNullOrEmpty())
                {
                    commentString += reasonFromTheTask;
                }
                if (target != null && target.Count > 0)
                {
                    foreach (var comment in target)
                    {
                        if (comment.IsDeprecated) continue;
                        if (commentString.IsNotNullOrEmpty())
                        {
                            commentString += "<hr/>";
                        }
                        if (comment.UserId == currentUserId)
                        {
                            commentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                        }
                        commentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", comment.User != null ? comment.User.DisplayName : string.Empty, comment.Modified.ToString("g"), comment.Comments.Clean());
                    }
                }
                return commentString;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
