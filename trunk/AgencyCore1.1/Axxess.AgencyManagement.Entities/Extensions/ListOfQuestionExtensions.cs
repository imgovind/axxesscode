﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Entities.Extensions
{
    public static class ListOfQuestionExtensions
    {
        public static IDictionary<string, Question> ToPOCDictionary(this List<Question> questions)
        {
            IDictionary<string, Question> questionDictionary = new Dictionary<string, Question>();
            if (questions != null && questions.Count > 0)
            {
                questions.ForEach(question =>
                {
                    string key = string.Empty;
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                    }
                    if (!questionDictionary.ContainsKey(key))
                    {
                        questionDictionary.Add(key, question);
                    }
                });
            }
            return questionDictionary;
        }

        public static IDictionary<string, Question> ToOASISDictionary(this List<Question> questions)
        {
            IDictionary<string, Question> questionDictionary = new Dictionary<string, Question>();
            var key = string.Empty;
            if (questions != null && questions.Count > 0)
            {
                questions.ForEach(question =>
                {
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.Generic)
                    {
                        key = string.Format("Generic{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                });
            }
            return questionDictionary;
        }
    }
}
