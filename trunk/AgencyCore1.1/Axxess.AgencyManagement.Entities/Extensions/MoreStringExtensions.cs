﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Diagnostics;

    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.Core.Extension;

    public static class MoreStringExtensions
    {
        [DebuggerStepThrough]
        public static string StringIntToEnumDescription<T>(string target)
        {
            var description = string.Empty;
            try
            {
                if (target.IsNotNullOrEmpty() && target.IsInteger())
                {
                    if (typeof(T).IsEnum && Enum.IsDefined(typeof(T), (object)target.ToInteger()))
                    {
                        description = EnumExtensions.GetDescription((Enum)(object)((T)(object)target.ToInteger()));
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return description;
        }

        public static string StringIntToEnumDescriptionFactory(this string target, string type)
        {
            var description = string.Empty;
            if (type.IsEqual("TherapyAssistance"))
            {
                description = StringIntToEnumDescription<TherapyAssistance>(target);
            }
            else if (type.IsEqual("TherapyAssistiveDevices"))
            {
                description = StringIntToEnumDescription<TherapyAssistiveDevices>(target);
            }
            else if (type.IsEqual("StaticBalance"))
            {
                description = StringIntToEnumDescription<StaticBalance>(target);
            }
            else if (type.IsEqual("DynamicBalance"))
            {
                description = StringIntToEnumDescription<DynamicBalance>(target);
            }
            else if (type.IsEqual("WeightBearingStatus"))
            {
                description = StringIntToEnumDescription<WeightBearingStatus>(target);
            }
            else if (type.IsEqual("DischargeReason"))
            {
                description = StringIntToEnumDescription<DischargeReason>(target);
            }
            else if (type.IsEqual("Sensory"))
            {
                description = StringIntToEnumDescription<Sensory>(target);
            }
            return description;
        }

    }
}
