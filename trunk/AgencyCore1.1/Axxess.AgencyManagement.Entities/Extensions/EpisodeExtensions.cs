﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    public static class EpisodeExtensions
    {
        public static Dictionary<string, TextNumberPair> ParseFrequency(this string target)
        {
            Dictionary<string, TextNumberPair> freqs = new Dictionary<string, TextNumberPair>();
            if (target.IsNotNullOrEmpty())
            {
                Regex reg = new Regex("([\\w]* [\\w]*):[ ]*([1-7](w|d)[1-9])([,|;]?[ ]*[1-7](w|d)[1-9])*");
                MatchCollection matchCollection = reg.Matches(target);
                foreach (Match match in matchCollection)
                {
                    string freq = "";
                    string value = "";
                    int total = 0;
                    if (match.Value.IsNotNullOrEmpty())
                    {
                        for (int i = 0; i < match.Groups.Count; i++)
                        {
                            if (match.Groups[i].Value.IsNotNullOrEmpty() && match.Groups[i].Value.Length > 1)
                            {
                                if (i == 1)
                                {
                                    freq = match.Groups[i].Value.Replace(" Frequency", "");
                                }
                                else if (i > 1)
                                {
                                    value += match.Groups[i].Value;
                                    //var numbers = value.Split('w', 'd');
                                    //if (numbers.Length == 2 && numbers[0].IsInteger() && numbers[1].IsInteger())
                                    //{
                                    //    total += numbers[0].ToInteger() * numbers[1].ToInteger();
                                    //}
                                }
                            }
                        }
                        freqs.Add(freq, new TextNumberPair(value, total));
                    }
                }
            }
            if (!freqs.ContainsKey("SN")) freqs.Add("SN", new TextNumberPair());
            if (!freqs.ContainsKey("HHA")) freqs.Add("HHA", new TextNumberPair());
            if (!freqs.ContainsKey("PT")) freqs.Add("PT", new TextNumberPair());
            if (!freqs.ContainsKey("OT")) freqs.Add("OT", new TextNumberPair());
            if (!freqs.ContainsKey("ST")) freqs.Add("ST", new TextNumberPair());
            if (!freqs.ContainsKey("MSW")) freqs.Add("MSW", new TextNumberPair());
            return freqs;
        }
    }
}
