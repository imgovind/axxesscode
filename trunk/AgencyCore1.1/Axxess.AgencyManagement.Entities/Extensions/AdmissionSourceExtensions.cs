﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using Axxess.Core.Extension;
    using Enums;

    public static class AdmissionSourceExtensions
    {
        [DebuggerStepThrough]
        public static string GetSplitValue(this int source)
        {
            if (Enum.IsDefined(typeof(AdmissionSource), source))
            {
                try
                {
                    var convertedValue = ((AdmissionSource)Enum.ToObject(typeof(AdmissionSource), source)).ToString();
                    if (convertedValue.IsNotNullOrEmpty() && convertedValue.Length > 0)
                    {
                        var arrayValues = convertedValue.Split('_');
                        if (arrayValues != null && arrayValues.Length >= 2)
                        {
                            return arrayValues[1];
                        }
                    }
                }
                catch (Exception)
                {
                    return "9";
                }
            }
            return "9";
        }

        [DebuggerStepThrough]
        public static string GetAdmissionDescription(this string traget)
        {
            int source;
            if (int.TryParse(traget, out source) && Enum.IsDefined(typeof(AdmissionSource), source))
            {
                try
                {
                    return ((AdmissionSource)Enum.ToObject(typeof(AdmissionSource), source)).GetDescription();
                   
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
            return string.Empty;
        }
    }
}
