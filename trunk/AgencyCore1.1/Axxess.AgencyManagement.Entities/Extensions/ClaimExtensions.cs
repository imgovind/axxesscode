﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    public static class ClaimExtensions
    {

        public static IDictionary<string, Locator> ToUb04Locator81Dictionary(this string ub04Locator81cca)
        {
            var questions = new Dictionary<string, Locator>();
            if (ub04Locator81cca.IsNotNullOrEmpty())
            {
                var locatorQuestions = ub04Locator81cca.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        if (n.LocatorId.IsNotNullOrEmpty())
                        {
                            questions.Add(n.LocatorId, n);
                        }
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToLocatorDictionary(this string locator)
        {
            var questions = new Dictionary<string, Locator>();
            if (locator.IsNotNullOrEmpty())
            {
                var locatorQuestions = locator.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        if (n.LocatorId.IsNotNullOrEmpty())
                        {
                            questions.Add(n.LocatorId, n);
                        }

                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this Final final)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (final.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = final.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (final.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = final.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (final.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = final.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (final.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = final.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    questions.Add(n.LocatorId, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this ManagedClaim managedClaim)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (managedClaim.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = managedClaim.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (managedClaim.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = managedClaim.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (managedClaim.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = managedClaim.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (managedClaim.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = managedClaim.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    if (n.LocatorId.IsNotNullOrEmpty())
                    {
                        questions.Add(n.LocatorId, n);
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToOccurrenceLocatorDictionary(this SecondaryClaim secondaryClaim)
        {
            var questions = new Dictionary<string, Locator>();
            var locatorQuestions = new List<Locator>();
            if (secondaryClaim.Ub04Locator31.IsNotNullOrEmpty())
            {
                var locator31 = secondaryClaim.Ub04Locator31.ToObject<List<Locator>>();
                if (locator31 != null)
                {
                    locatorQuestions.AddRange(locator31);
                }
            }
            if (secondaryClaim.Ub04Locator32.IsNotNullOrEmpty())
            {
                var locator32 = secondaryClaim.Ub04Locator32.ToObject<List<Locator>>();
                if (locator32 != null)
                {
                    locatorQuestions.AddRange(locator32);
                }
            }
            if (secondaryClaim.Ub04Locator33.IsNotNullOrEmpty())
            {
                var locator33 = secondaryClaim.Ub04Locator33.ToObject<List<Locator>>();
                if (locator33 != null)
                {
                    locatorQuestions.AddRange(locator33);
                }
            }
            if (secondaryClaim.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locator34 = secondaryClaim.Ub04Locator34.ToObject<List<Locator>>();
                if (locator34 != null)
                {
                    locatorQuestions.AddRange(locator34);
                }
            }
            if (locatorQuestions != null && locatorQuestions.Count > 0)
            {
                locatorQuestions.ForEach(n =>
                {
                    if (n.LocatorId.IsNotNullOrEmpty())
                    {
                        questions.Add(n.LocatorId, n);
                    }

                });
            }
            return questions;
        }


        public static Dictionary<int, ChargeRate> ToInsuranceBillDataDictionary(this  AgencyInsurance agencyInsurance)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (agencyInsurance != null && agencyInsurance.BillData.IsNotNullOrEmpty())
            {
                var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                if (rates != null && rates.Count > 0)
                {
                    rates.ForEach(n =>
                    {
                        if (!chargeRates.ContainsKey(n.Id))
                        {
                            chargeRates.Add(n.Id, n);
                        }
                    });
                }
            }
            return chargeRates;
        }

        //public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  Final final)
        //{
        //    var chargeRates = new Dictionary<int, ChargeRate>();
        //    if (final != null && final.Insurance.IsNotNullOrEmpty())
        //    {
        //        var insurance = final.Insurance.ToObject<AgencyInsurance>();
        //        if (insurance != null)
        //        {
        //            chargeRates = insurance.ToInsuranceBillDataDictionary();
        //        }
        //    }
        //    return chargeRates;
        //}

        //public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  ManagedClaim managedClaim)
        //{
        //    var chargeRates = new Dictionary<int, ChargeRate>();
        //    if (managedClaim != null && managedClaim.Insurance.IsNotNullOrEmpty())
        //    {
        //        var insurance = managedClaim.Insurance.ToObject<AgencyInsurance>();
        //        if (insurance != null)
        //        {
        //            chargeRates = insurance.ToInsuranceBillDataDictionary();
        //        }
        //    }
        //    return chargeRates;
        //}
    }
}
