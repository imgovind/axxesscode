﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Entities.Extensions
{
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    public static class VitalSignLogExtensions
    {
        public static void MinMaxBloodPressures(this VitalSignLog log, out string maxBP, out string minBP)
        {
            var bloodPressures = new List<BloodPressure>();
            SplitBloodPressure(log.BloodPressure, bloodPressures);
            SplitBloodPressure(log.BloodPressure2, bloodPressures);
            SplitBloodPressure(log.BloodPressure3, bloodPressures);
            SplitBloodPressure(log.BloodPressure4, bloodPressures);
            SplitBloodPressure(log.BloodPressure5, bloodPressures);
            SplitBloodPressure(log.BloodPressure6, bloodPressures);
            BloodPressure maxBloodPressure = null;
            BloodPressure minBloodPressure = null;
            foreach (var bloodPressure in bloodPressures)
            {
                if (maxBloodPressure == null)
                {
                    maxBloodPressure = bloodPressure;
                }
                else
                {
                    if (bloodPressure.PulsePressure >= maxBloodPressure.PulsePressure && bloodPressure.Diastolic >= maxBloodPressure.Diastolic)
                    {
                        maxBloodPressure = bloodPressure;
                    }
                }

                if (minBloodPressure == null)
                {
                    minBloodPressure = bloodPressure;
                }
                else
                {
                    if (bloodPressure.PulsePressure >= minBloodPressure.PulsePressure && bloodPressure.Diastolic >= minBloodPressure.Diastolic)
                    {
                        minBloodPressure = bloodPressure;
                    }
                }
            }
            maxBP = maxBloodPressure != null ? maxBloodPressure.ToString() : string.Empty;
            minBP = minBloodPressure != null ? minBloodPressure.ToString() : string.Empty;
        }

        public static string MostRecentBloodPressure(this VitalSignLog log)
        {
            string bloodPressure;
            if (log.BloodPressure6.IsNotNullOrEmpty())
            {
                bloodPressure = log.BloodPressure6;
            }
            else if (log.BloodPressure5.IsNotNullOrEmpty())
            {
                bloodPressure = log.BloodPressure5;
            }
            else if (log.BloodPressure4.IsNotNullOrEmpty())
            {
                bloodPressure = log.BloodPressure4;
            }
            else if (log.BloodPressure3.IsNotNullOrEmpty())
            {
                bloodPressure = log.BloodPressure3;
            }
            else if (log.BloodPressure2.IsNotNullOrEmpty())
            {
                bloodPressure = log.BloodPressure2;
            }
            else
            {
                bloodPressure = log.BloodPressure;
            }
            return bloodPressure;
        }

        /// <summary>
        /// Selects the pulse to display
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static int? PulseToDisplay(this VitalSignLog log)
        {
            int? pulse = null;
            if (log.Pulse.HasValue)
            {
                if (log.Pulse2.HasValue)
                {
                    //Will display the pulse that is Apical or Pulse if neither are.
                    if (log.PulseLocation.HasValue && log.PulseLocation.Value == (int)PulseLocation.Apical)
                    {
                        pulse = log.Pulse;
                    }
                    else if (log.PulseLocation2.HasValue && log.PulseLocation2.Value == (int)PulseLocation.Apical)
                    {
                        pulse = log.Pulse2;
                    }
                    else
                    {
                        pulse = log.Pulse;
                    }
                }
                else
                {
                    pulse = log.Pulse;
                }
            }
            else if (log.Pulse2.HasValue)
            {
                pulse = log.Pulse2;
            }
            return pulse;
        }

        static void SplitBloodPressure(string bp, List<BloodPressure> bloodPressures)
        {
            if (bp.IsNotNullOrEmpty())
            {
                var bps = bp.Split('/');
                if (bps.Length == 2)
                {
                     bloodPressures.Add(new BloodPressure(bps[0].ToInteger(), bps[1].ToInteger()));
                }
            }
        }
    }
}
