﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public static class PrivateDutyScheduleTaskExtensions
    {

        public static bool IsNote(this ITask task)
        {
            return DisciplineTaskFactory.AllPatientVisitNotes().Contains(task.DisciplineTask);
        }

        public static bool IsReport(this ITask task)
        {
            if (task != null)
            {
                Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), task.Discipline);
                if (discipline == Disciplines.ReportsAndNotes)
                {
                    return true;
                }
            }
            return false;
        }

        public static string[] Discipline<T>(this List<T> task) where T : ITask
        {
            var disciplines = new List<string>();
            if (task.IsNotNullOrEmpty())
            {
                disciplines = task.Select(s => s.Discipline == "Nursing" ? "SN" : s.Discipline).Distinct().ToList() ?? new List<string>();
            }
            return disciplines.ToArray();
        }

        public static bool IsPlanOfCare(this ITask task)
        {
            return DisciplineTaskFactory.POC().Contains(task.DisciplineTask);
        }

        public static string TypeOfEvent(this ITask task)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), task.DisciplineTask))
            {
                var type = ((DisciplineTasks)task.DisciplineTask);
                string group = type.GetCustomGroup();
                if (DisciplineTaskFactory.AllAssessments(true).Contains(task.DisciplineTask))
                {
                    return "OASIS";
                }
                else if (DisciplineTaskFactory.AllPatientVisitNotes().Contains(task.DisciplineTask))
                {
                    return "Notes";
                }
                switch (type)
                {
                    case DisciplineTasks.PhysicianOrder:
                        return "PhysicianOrder";
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.HCFA485StandAlone:
                        return "PlanOfCare";
                    case DisciplineTasks.FaceToFaceEncounter:
                        return "FaceToFaceEncounter";
                    case DisciplineTasks.IncidentAccidentReport:
                        return "Incident";
                    case DisciplineTasks.InfectionReport:
                        return "Infection";
                    case DisciplineTasks.CommunicationNote:
                        return "CommunicationNote";
                }
            }
            return string.Empty;
        }

        public static string GIdentify(this ITask task)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), task.DisciplineTask))
            {
                var type = (DisciplineTasks)task.DisciplineTask;
                switch (type)
                {
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCResumptionOfCare:
                    case DisciplineTasks.OASISCStartOfCare:
                    case DisciplineTasks.NonOASISStartOfCare:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.LVNSupervisoryVisit:
                    case DisciplineTasks.DieticianVisit:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                    case DisciplineTasks.SNDiabeticDailyVisit:
                    case DisciplineTasks.SNPediatricVisit:
                    case DisciplineTasks.SNPsychAssessment:
                        return "SN";

                    case DisciplineTasks.SNVTeachingTraining:
                        return "SNT";

                    case DisciplineTasks.SNVManagementAndEvaluation:
                        return "SNM";

                    case DisciplineTasks.SNVObservationAndAssessment:
                        return "SNO";

                    case DisciplineTasks.OASISCStartOfCarePT:
                    case DisciplineTasks.OASISCResumptionOfCarePT:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCTransferDischargePT:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTSupervisoryVisit:
                        return "PT";

                    case DisciplineTasks.PTAVisit:
                        return "PTA";

                    case DisciplineTasks.PTMaintenance:
                        return "PTM";

                    case DisciplineTasks.OASISCStartOfCareOT:
                    case DisciplineTasks.OASISCResumptionOfCareOT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                        return "OT";

                    case DisciplineTasks.OTMaintenance:
                        return "OTM";

                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                        return "ST";

                    case DisciplineTasks.STMaintenance:
                        return "STM";

                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                        return "MSW";

                    case DisciplineTasks.HHAideSupervisoryVisit:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.HomeMakerNote:
                        return "HHA";

                    case DisciplineTasks.COTAVisit:
                        return "OTA";

                    case DisciplineTasks.PhysicianOrder:
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.FaceToFaceEncounter:
                    case DisciplineTasks.IncidentAccidentReport:
                    case DisciplineTasks.InfectionReport:
                    case DisciplineTasks.CommunicationNote:
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        public static bool IsCompleted(this ITask scheduleEvent)
        {
            return ScheduleStatusFactory.OnAndAfterQAStatus(true).Contains(scheduleEvent.Status);
        }

        public static bool IsCompletelyFinished(this ITask scheduleEvent)
        {
            return ScheduleStatusFactory.OnAndAfterQAStatus(false).Contains(scheduleEvent.Status);
        }

        public static AssessmentType GetOasisType(this ITask scheduleTask)
        {
            AssessmentType oasisType = AssessmentType.None;
            if (scheduleTask != null)
            {
                if (DisciplineTaskFactory.SOCDisciplineTasks(false).Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.StartOfCare;
                }
                else if (DisciplineTaskFactory.ROCDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.ResumptionOfCare;
                }
                else if (DisciplineTaskFactory.RecertDisciplineTasks(false).Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.Recertification;
                }
                else if (DisciplineTaskFactory.DischargeOASISDisciplineTasks(false).Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.Discharge;
                }
                else if (DisciplineTaskFactory.DeathOASISDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.Death;
                }
                else if (DisciplineTaskFactory.FollowUpDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.FollowUp;
                }
                else if (DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.Transfer;
                }
                else if (DisciplineTaskFactory.TransferDischargeOASISDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.TransferDischarge;
                }
                else if (DisciplineTaskFactory.NONOASISSOCDisciplineTasks(true).Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.NonOASISStartOfCare;
                }
                else if (DisciplineTaskFactory.NONOASISRecertDisciplineTasks(true).Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.NonOASISRecertification;
                }
                else if (DisciplineTaskFactory.NONOASISDischargeDisciplineTasks().Contains(scheduleTask.DisciplineTask))
                {
                    oasisType = AssessmentType.NonOASISDischarge;
                }
            }
            return oasisType;
        }
    }



}
