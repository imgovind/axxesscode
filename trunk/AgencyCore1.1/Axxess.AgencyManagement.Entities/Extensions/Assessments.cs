﻿namespace Axxess.AgencyManagement.Entities.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Enums;
    

    public static class Assessments
    {

        public static IDictionary<string, Question> ToDictionary(this Assessment assessment)
        {
            return assessment != null ? GetAssessmentQuestionsFromList(assessment.Questions) : new Dictionary<string, Question>();
        }

        public static IDictionary<string, Question> ToDictionary(this AssessmentQuestionData questionData)
        {
            return questionData != null ? GetAssessmentQuestionsFromList(questionData.Question) : new Dictionary<string, Question>();
        }

        private static IDictionary<string, Question> GetAssessmentQuestionsFromList(List<Question> questions)
        {
            IDictionary<string, Question> questionDictionary = new Dictionary<string, Question>();
            if (questions != null)
            {
                var key = string.Empty;
                questions.ForEach(question =>
                {
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.Generic)
                    {
                        key = string.Format("Generic{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                        if (!questionDictionary.ContainsKey(key))
                        {
                            questionDictionary.Add(key, question);
                        }
                    }

                });
            }
            return questionDictionary;
        }

        public static IDictionary<string, Question> ToDictionary(this PlanofCare planofCare)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (planofCare != null && planofCare.Questions != null && planofCare.Questions.Count > 0)
            {
                planofCare.Questions.ForEach(question =>
                {
                    string key = string.Empty;
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                    }
                    if (!questions.ContainsKey(key))
                    {
                        questions.Add(key, question);
                    }
                });
            }
            return questions;
        }

        //public static string ToFrequencyString(this IDictionary<string, Question> questions)
        //{
        //    var frequencyData = string.Empty;
        //    if (questions != null)
        //    {
        //        frequencyData = questions.ToFrequencyString();
        //    }
        //    return frequencyData;
        //}


        //public static IDictionary<string, Question> ToDictionary(this PlanofCareStandAlone planofCare)
        //{
        //    IDictionary<string, Question> questions = new Dictionary<string, Question>();
        //    if (planofCare.Questions != null)
        //    {
        //        planofCare.Questions.ForEach(question =>
        //        {
        //            string key = string.Empty;
        //            if (question.Type == QuestionType.Moo)
        //            {
        //                key = string.Format("{0}{1}", question.Code, question.Name);
        //            }
        //            else if (question.Type == QuestionType.PlanofCare)
        //            {
        //                key = string.Format("485{0}", question.Name);
        //            }
        //            else
        //            {
        //                key = string.Format("{0}", question.Name);
        //            }
        //            if (!questions.ContainsKey(key))
        //            {
        //                questions.Add(key, question);
        //            }
        //        });
        //    }
        //    return questions;
        //}

        //public static PlanofCare ToPlanofCare(this PlanofCareStandAlone planofCareStandAlone)
        //{
        //    var planofCare = new PlanofCare();

        //    planofCare.Id = planofCareStandAlone.Id;
        //    planofCare.AgencyId = planofCareStandAlone.AgencyId;
        //    planofCare.EpisodeId = planofCareStandAlone.EpisodeId;
        //    planofCare.PatientId = planofCareStandAlone.PatientId;
        //    planofCare.PhysicianId = planofCareStandAlone.PhysicianId;
        //    planofCare.UserId = planofCareStandAlone.UserId;
        //    planofCare.OrderNumber = planofCareStandAlone.OrderNumber;
        //    planofCare.Data = planofCareStandAlone.Data;
        //    planofCare.Status = planofCareStandAlone.Status;
        //    planofCare.SignatureText = planofCareStandAlone.SignatureText;
        //    planofCare.SignatureDate = planofCareStandAlone.SignatureDate;
        //    planofCare.PhysicianSignatureText = planofCareStandAlone.PhysicianSignatureText;
        //    planofCare.PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate;
        //    planofCare.ReceivedDate = planofCareStandAlone.ReceivedDate;
        //    planofCare.SentDate = planofCareStandAlone.SentDate;
        //    planofCare.Created = planofCareStandAlone.Created;
        //    planofCare.Modified = planofCareStandAlone.Modified;
        //    planofCare.IsDeprecated = planofCareStandAlone.IsDeprecated;

        //    return planofCare;
        //}

        //public static IDictionary<string, Question> Diagnosis(this Assessment assessment)
        //{
        //    IDictionary<string, Question> diagnosis = new Dictionary<string, Question>();
        //    if (assessment != null)
        //    {
        //        var questions = assessment.ToDictionary();
        //        diagnosis = questions.ToDiagnosis();
        //    }
        //    return diagnosis;
        //}

        //public static IDictionary<string, Question> Allergies(Assessment assessment)
        //{
        //    IDictionary<string, Question> allergies = new Dictionary<string, Question>();
        //    if (assessment != null)
        //    {
        //        var questions = assessment.ToDictionary();
        //        allergies = questions.ToAllergies();
        //    }
        //    return allergies;
        //}
      
        public static List<Question> GetHospitalizationData(this IDictionary<string, Question> questions)
        {
            var transferLog = new List<Question>();
            if (questions.IsNotNullOrEmpty())
            {
                var names = new string[] { "M2410TypeOfInpatientFacility", "M2430ReasonForHospitalizationMed", "M2430ReasonForHospitalizationFall"
                    , "M2430ReasonForHospitalizationInfection", "M2430ReasonForHospitalizationOtherRP", "M2430ReasonForHospitalizationHeartFail"
                    , "M2430ReasonForHospitalizationCardiac", "M2430ReasonForHospitalizationMyocardial", "M2430ReasonForHospitalizationHeartDisease"
                    , "M2430ReasonForHospitalizationStroke", "M2430ReasonForHospitalizationHypo", "M2430ReasonForHospitalizationGI"
                    , "M2430ReasonForHospitalizationDehMal", "M2430ReasonForHospitalizationUrinaryInf", "M2430ReasonForHospitalizationIV"
                    , "M2430ReasonForHospitalizationWoundInf", "M2430ReasonForHospitalizationUncontrolledPain", "M2430ReasonForHospitalizationMental"
                    , "M2430ReasonForHospitalizationDVT", "M2430ReasonForHospitalizationScheduled", "M2430ReasonForHospitalizationOther"
                    , "M2440ReasonForHospitalizationUK", "M2440ReasonPatientAdmittedTherapy", "M2440ReasonPatientAdmittedRespite"
                    , "M2440ReasonPatientAdmittedHospice", "M2440ReasonPatientAdmittedPermanent", "M2440ReasonPatientAdmittedUnsafe"
                    , "M2440ReasonPatientAdmittedOther", "M2440ReasonPatientAdmittedUnknown", "GenericDischargeNarrative", "M2440ReasonPatientAdmittedTherapy", "M2440ReasonPatientAdmittedRespite"
                    , "M2440ReasonPatientAdmittedHospice", "M2440ReasonPatientAdmittedPermanent", "M2440ReasonPatientAdmittedUnsafe"
                    , "M2440ReasonPatientAdmittedOther", "M2440ReasonPatientAdmittedUnknown"};

                names.ForEach(name =>
                {
                    var question = questions.Get(name);
                    if (question != null)
                    {
                        transferLog.Add(question);
                    }
                });
            }
            return transferLog;
        }
    }
}
