﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public interface IReferralRepository
    {
      
        bool Add(Referral referral);
        //bool Delete(Guid agencyId, Guid id, Referral referralForDelete);
        bool UpdateModal(Referral referral);
        bool NonAdmitReferral(PendingPatient pending);
        Referral Get(Guid agencyId, Guid id);
        List<Referral> GetAll(Guid agencyId, ReferralStatus status, AgencyServices service);
        List<Referral> GetAllByCreatedUser(Guid agencyId, Guid userId, ReferralStatus status, AgencyServices service);
        List<ReferralData> All(Guid agencyId, ReferralStatus referralStatus, AgencyServices service);
        List<ReferralData> AllByUser(Guid agencyId, Guid userId, ReferralStatus status, AgencyServices service);
        List<NonAdmit> GetNonAdmitReferral(Guid agencyId, ReferralStatus status, AgencyServices service, bool isUserCanAdmit);

       
        bool AddEmergencyContact(ReferralEmergencyContact emergencyContactInfo);
        bool EditEmergencyContact(Guid agencyId, ReferralEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid agencyId, Guid Id, Guid referralId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid referralId, Guid emergencyContactId);
        ReferralEmergencyContact GetFirstEmergencyContactByReferral(Guid agencyId, Guid referralId);
        ReferralEmergencyContact GetEmergencyContact(Guid referralId, Guid emergencyContactId);
        IList<ReferralEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid referralId);
    }
}
