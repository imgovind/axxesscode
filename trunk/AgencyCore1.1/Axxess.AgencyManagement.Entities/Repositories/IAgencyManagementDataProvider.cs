﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;


    public interface IAgencyManagementDataProvider : IRepository
    {
        IReferralRepository ReferralRepository
        {
            get;
        }
        IPatientRepository PatientRepository
        {
            get;
        }
        IPhysicianRepository PhysicianRepository
        {
            get;
        }
        IUserRepository UserRepository
        {
            get;
        }
        IAgencyRepository AgencyRepository
        {
            get;
        }
        IAssetRepository AssetRepository
        {
            get;
        }
        //IBillingRepository BillingRepository
        //{
        //    get;
        //}

        IMessageRepository MessageRepository
        {
            get;
        }

        //IScheduleRepository ScheduleRepository
        //{
        //    get;
        //}

        //INoteRepository NoteRepository
        //{
        //    get;
        //}
        
        //IPlanofCareRepository PlanofCareRepository
        //{
        //    get;
        //}

        //IAssessmentRepository OasisAssessmentRepository
        //{
        //    get;
        //}

       
    }
}
