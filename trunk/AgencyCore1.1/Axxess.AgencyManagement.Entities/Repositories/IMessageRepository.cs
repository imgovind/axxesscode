﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums; 

    public interface IMessageRepository
    {
        IList<Message> GetSentMessages(Guid userId, Guid agencyId, int pageNumber);
        IList<Message> GetMessages(Guid userId, Guid agencyId, Guid folderId, int pageNumber, bool isDeprecated);
        bool Delete(Guid messageId, Guid userId, Guid agencyId);
        bool Read(Guid messageId, Guid userId, Guid agencyId);

        Message GetUserMessage(Guid messageId, Guid userId, Guid agencyId);
        Message GetSentMessage(Guid messageId, Guid userId, Guid agencyId);
        Message GetSystemMessage(Guid messageId, Guid userId, Guid agencyId);

        bool AddUserMessage(UserMessage userMessage);
        bool AddMessageDetail(MessageDetail messageDetail);
        bool AddUserMessage(List<UserMessage> userMessages);

        List<SystemMessage> GetSystemMessages();
        bool AddSystemMessage(SystemMessage message);

        int GetUserMessageCount(Guid userId, Guid agencyId, bool isDeprecated);
        int GetSentMessageCount(Guid userId, Guid agencyId);

        DashboardMessage GetCurrentDashboardMessage();
        bool AddDashboardMessage(DashboardMessage message);

        bool AddMessageFolder(Guid ownerId, Guid agencyId, string name);
        IList<MessageFolder> FolderList(Guid userId, Guid agencyId);
        bool DeleteFolder(Guid folderId);
        bool MoveMessageToFolder(Guid userId, Guid messageId, Guid folderId, string messageType);
        bool MoveSystemMessageToFolder(Guid userId, Guid messageId, Guid folderId, string messageType);

        bool DeleteMany(List<Guid> messageList, Guid userId, Guid agencyId);
    }
}
