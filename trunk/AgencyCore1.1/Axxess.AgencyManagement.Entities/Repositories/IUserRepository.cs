﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    public interface IUserRepository
    {
        #region Public Methods and Operators

        bool AddModel(User user);
        bool AddUserProfile(UserProfile userprofile);
        bool RemoveProfile(Guid agencyId, Guid userId);

        bool AddLicense(License licenseItem);

        bool AddNonUser(NonUser nonUser);

        IEnumerable<User> All();

        bool Delete(Guid agencyId, Guid userId);

        bool Remove(Guid agencyId, Guid userId);

        bool DeleteLicense(Guid licenseId, Guid userId, Guid agencyId);

        bool DeleteNonUser(Guid id, Guid agencyId);

        //User Get(Guid id, Guid agencyId);

        int GetActiveUserCount(Guid agencyId);

        IList<AgencyLite> GetAgencies(Guid loginId);

        //IList<User> GetAgencyUsers(Guid agencyId);

        IList<User> GetAgencyUsers(Guid agencyId, string query);

        IList<User> GetAgencyUsersOnly(Guid agencyId);

        //IList<User> GetAll();

        List<User> GetAllUsers(Guid agencyId, Guid branchId, int statusId);

        //Dictionary<string, User> GetAllUsers(Guid agencyId);

        IList<User> GetAuditors(Guid agencyId);

        //User GetByLoginId(Guid loginId);

        IList<User> GetCaseManagerUsers(Guid agencyId);

        IList<User> GetClinicalUsers(Guid agencyId);

        IList<User> GetEmployeeRoster(Guid agencyId, Guid branchId, int status);

        IList<User> GetHHAUsers(Guid agencyId);

        IList<User> GetLVNUsers(Guid agencyId);

        License GetLicense(Guid licenseId, Guid userId, Guid agencyId);

        IList<License> GetLicenses(Guid agencyId, Guid userId);

        IList<License> GetLicenses(Guid agencyId);

        IList<License> GetLicensesByBranchAndStatus(Guid agencyId, Guid branchId, int statusId);

        NonUser GetNonUser(Guid agencyId, Guid id);

        IList<NonUser> GetNonUsers(Guid agencyId);

        IList<User> GetOTUsers(Guid agencyId);

        IList<User> GetPTUsers(Guid agencyId);

        List<User> GetRatedUserByBranch(Guid agencyId, Guid branchId, Guid userIdToExclude);

        List<SelectedUser> GetUserAccessList(Guid agencyId, Guid patientId);

        IList<UserAndNonUserLite> GetUsersAndNonUsers(Guid agencyId);

        User GetUserOnly(Guid agencyId, Guid id);

        UserProfile GetUserProfileOnly(Guid agencyId, Guid id);

        //int GetUserPatientCount(Guid agencyId, Guid userId, byte statusId);

        // IList<LicenseItem> GetSoftwareUserLicenses(Guid agencyId);
        // LicenseItem GetUserLicenseItem(Guid licenseId, Guid userId, Guid agencyId);
        List<User> GetUsersByIds(Guid agencyId, List<Guid> userIds);

        IList<User> GetUsersByLoginId(Guid loginId);

        IList<User> GetUsersByLoginId(Guid agencyId, Guid loginId);

        IList<User> GetUsersByStatus(Guid agencyId, Guid branchId, int status);

        IList<User> GetUsersByStatusAndDOB(Guid agencyId, Guid branchId, int status, int month);

        IList<User> GetUsersByStatusAndPermissions(Guid agencyId, Guid branchId, int status);

        IList<User> GetUsersByStatusAndServicesLean(Guid agencyId, Guid branchId, int status, int service);

        IList<User> GetUsersByStatusLean(Guid agencyId, Guid branchId, int status, int service);

        IList<User> GetUsersOnly(Guid agencyId);

        IList<User> GetUsersOnly(Guid agencyId, int status);

        List<User> GetUsersWithCredentialsByIds(Guid agencyId, List<Guid> userIds);

        bool SetUserStatus(Guid agencyId, Guid userId, int status);


        bool UpdateLicense(License item);

        bool UpdateModel(User user, bool isCacheRefresh);

        bool UpdateUserProfile(UserProfile userProfile);

        bool UpdateNonUser(NonUser nonUser);

        bool UpdateProfile(UserProfile userProfile);

        bool AddMultipleUserLocations(List<UserLocation> userLocations);

        bool UpdateMultipleUserLocations(List<UserLocation> userLocations);

        bool RemoveMultipleUserLocations(Guid agencyId, Guid userId, List<Guid> locationIds);

        List<UserLocation> UserLocations(Guid agencyId, Guid userId, int acessibleServices);

        List<UserLocation> UserLocationsOnly(Guid agencyId, Guid userId);

        #endregion

        #region User Rates

        bool AddUserPayorRates(UserPayorRates data);

        bool AddUserPayorRates(List<UserPayorRates> datas);

        UserPayorRates GetUserPayorRate(Guid agencyId, Guid userId, int payorId);

        List<UserPayorRates> GetUserPayorRates(Guid agencyId, Guid userId, List<int> payorIds);

        List<UserRate> GetPayorUserRates(Guid agencyId, Guid userId, int payorId);

        bool UpdateUserPayorRates(Guid agencyId, Guid userId, int payorId, UserPayorRates data);

        List<UserPayorRates> GetUserPayorRates(Guid agencyId, Guid userId);

        /// <summary>
        /// pulls from mysql old users rates 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetUsersRates(Guid agencyId, Guid userId);

        #endregion
    }
}