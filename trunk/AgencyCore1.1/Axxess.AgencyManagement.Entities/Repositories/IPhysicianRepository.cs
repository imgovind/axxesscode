﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    public interface IPhysicianRepository
    {
        bool Add(AgencyPhysician physician);
        bool Update(AgencyPhysician physician);
        bool Delete(Guid agencyId, Guid id);
        AgencyPhysician Get(Guid physicianId, Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysiciansWithOutPecosVerification(Guid agencyId);
        IList<AgencyPhysicianGridRow> GetAgencyPhysiciansForGrid(Guid agencyId);
       
        bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode);
        IList<AgencyPhysician> GetPatientPhysicians( Guid agencyId,Guid patientId );
        AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId, Guid agencyId);
        IList<AgencyPhysician> GetByLoginId(Guid loginId);
        List<CarePlanOversight> GetCPO(Guid physicianLoginId);
        CarePlanOversight GetCPOById(Guid id);
        //IList<AgencyPhysician> GetAllPhysicians();
       
        AgencyPhysician GetPatientPrimaryPhysician(Guid agencyId, Guid patientId);
        AgencyPhysician GetPatientPrimaryOrFirstPhysician(Guid agencyId, Guid patientId);
        Guid GetPatientPrimaryOrFirstPhysicianId(Guid agencyId, Guid patientId);
        List<AgencyPhysician> GetPhysiciansByIdsLean(Guid agencyId, List<Guid> physicianIds);
        List<AgencyPhysician> GetPatientsPrimaryPhysician(Guid agencyId, List<Guid> patientIds);
    }
}
