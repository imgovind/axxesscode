﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    public interface IAssetRepository
    {
        bool Add(Asset asset);
        bool AddMany(List<Asset> assets);
        bool UpdateStatus(Guid agencyId, Guid id, bool isDeprecated);
        bool DeleteMany(Guid agencyId, List<Guid> assetIds);
        bool Remove(Guid id);
        Asset Get(Guid agencyId, Guid id);
        List<Asset> GetAssetsNameAndId(Guid agencyId, List<Guid> assetIds);

        bool DeletePatientDocument(Guid id, Guid patientId, Guid agencyId);
        bool AddPatientDocument(Guid assetId, PatientDocument patientDocument);
        bool UpdatePatientDocument(PatientDocument patientDocument);
        PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId);
        List<PatientDocument> GetPatientDocuments(Guid patientId);
    }
}
