﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
   

    public interface IPatientRepository
    {
        bool Add(Patient patient);
        bool AdmitPatient(PendingPatient patient, out Patient patientOut);
        bool AdmitReferral(PendingPatient pending, out Patient patientOut);
        bool DischargePatient(Guid agencyId, Guid patientId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        bool Update(Patient patient, out Patient patientOut);
        bool UpdateModal(Patient patient);
        bool Activate(Guid agencyId, Guid patientId);
        bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate);
        bool Edit(Patient patientFromDB, Patient patientFromClient, out Patient patientOut);
        bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId, out Patient patientOut);
        bool SetStatus(Guid agencyId, Guid patientId, PatientStatus status, AgencyServices service);
        bool RemovePatient(Guid agencyId, Guid id);
        bool DeprecatedPatient(Guid agencyId, Guid id);
        string GetPatientNameById(Guid patientId, Guid agencyId);
        Patient GetPatientNameAndServicesById( Guid agencyId,Guid patientId);
        Dictionary<Guid, string> GetPatientNamesByIds(Guid agencyId, List<Guid> patientIds);
        Patient GetPatientOnly(Guid id, Guid agencyId);
        IList<Patient> Find(Guid agencyId,int statusId, AgencyServices service);

        List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId, int status, AgencyServices service);
        Dictionary<string, string> GetPatientJsonByColumns(Guid agencyId, Guid id, params string[] columns);

        string LastPatientId(Guid agencyId);
        bool IsPatientIdExist(Guid agencyId, string patientIdNumber);
        bool IsMedicareExist(Guid agencyId, string medicareNumber);
        bool IsMedicaidExist(Guid agencyId, string medicaidNumber);
        bool IsSSNExist(Guid agencyId, string ssn);
        bool IsPatientIdExistForEdit(Guid agencyId, Guid patientId, string patientIdNumber);
        bool IsMedicareExistForEdit(Guid agencyId, Guid patientId, string medicareNumber);
        bool IsMedicaidExistForEdit(Guid agencyId, Guid patientId, string medicaidNumber);
        bool IsSSNExistForEdit(Guid agencyId, Guid patientId, string ssn);
        bool IsPatientExist(Guid agencyId, Guid patientId);
        long GetNextOrderNumber();


        bool Link(Guid patientId, Guid physicianId, bool isPrimary);
        bool Unlink(Guid patientId, Guid physicianId);
        bool UnlinkAll(Guid patientId);
        bool UpdateMultiplePatientPhysicians(List<PatientPhysician> patientPhysicians);
        bool DeletePhysicianContact(Guid Id, Guid patientId);
        List<PatientPhysician> GetPatientPhysicians(Guid patientId);
        bool DoesPhysicianExist(Guid patientId, Guid physicianId);

        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact);
        bool UpdateEmergencyContact(Guid agencyId, Guid patientId, PatientEmergencyContact emergencyContact);
        bool RemoveEmergencyContact(Guid agencyId, Guid patientId, Guid Id);
        bool RemoveEmergencyContacts(Guid agencyId, Guid patientId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactID);
        PatientEmergencyContact GetFirstEmergencyContactByPatient(Guid agencyId, Guid patientId);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid patientId);

        //bool AddAuthorization(Authorization authorization);
        //bool EditAuthorization(Authorization authorization);
        //bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id);
        //Authorization GetAuthorization(Guid agencyId, Guid Id);
        //Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id);
        //IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId, int service);
        //IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, int insuranceId, string status, int service, DateTime startDate, DateTime endDate);

        bool AddNewMedicationProfile(MedicationProfile medication);
        bool SaveMedicationProfile(MedicationProfile medicationProfile);
        bool UpdateMedication(MedicationProfile medicationProfile);
        //bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        //bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate);
        bool RemoveMedicationProfile(Guid agencyId, Guid patientId, Guid Id);
        //MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        //MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication);
        MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId);
        bool IsMedicationProfileExist(Guid AgencyId, Guid patientId);
        MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId);

        bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory);
        bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfile);
        MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId);
        MedicationProfileHistory GetSignedMedicationAssocatiedToAssessment(Guid patientId, Guid assessmentId);
        MedicationProfileHistory GetSignedMedicationProfileForPatientByEpisode(Guid agencyId, Guid patientId, Guid episodeId);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId);
        //IList<MedicationProfileHistory> GetAllMedicationProfileHistory();
      
        //bool AddCPO(CarePlanOversight cpo);
        //bool DeleteCPO(Guid id);
        //bool UpdateCPO(CarePlanOversight cpo);

        bool AddAllergyProfile(AllergyProfile allergyProfile);
        bool UpdateAllergyProfile(AllergyProfile allergyProfile);
        AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId);
        AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId);

        bool AddHospitalizationLog(HospitalizationLog transferLog);
        bool UpdateHospitalizationLog(HospitalizationLog transferLog);
        HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId);
        List<HospitalizationLog> GetHospitalizationLogs( Guid agencyId,Guid patientId);
        List<PatientHospitalizationData> GetHospitalizedPatients(Guid agencyId);

        bool AddPatientUser(PatientUser patientUser);
        bool RemovePatientUser(PatientUser patientUser);
        PatientUser GetPatientUser(Guid patientUserId);
        List<PatientSelection> GetPatientsWithUserAccess(Guid userId, Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name);

        List<PatientUser> GetPatientUsersByPatients(List<Guid> patientId);
        List<PatientUser> GetPatientUsersByUser(Guid userId);

        List<SelectedPatient> GetPatientAccessList(Guid agencyId, Guid userId);
        AgencyServices GetAccessibleServices(Guid agencyId, Guid patientId);

        List<Patient> FindPatientVeryLeanByNameAndService(Guid agencyId, string term, int service);

        Guid GetPhotoId(Guid agencyId, Guid patientId);
        bool SetPhotoId(Guid agencyId, Guid patientId, Guid assetId);

        Guid GetCurrentPatientHospitalizationLog(Guid patientId, Guid agencyId);
        bool ToggleHosipitalization(Guid patientId, Guid agencyId, Guid hospitalizationLogId, bool isHospitalized);

        bool MarkEndOfHospitalization(Guid id, Guid patientId, Guid agencyId, DateTime returnDate);
    }
}
