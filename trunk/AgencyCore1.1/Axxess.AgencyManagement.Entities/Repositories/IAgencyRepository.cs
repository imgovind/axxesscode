﻿namespace Axxess.AgencyManagement.Entities.Repositories
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Enums;
    

    public interface IAgencyRepository
    {
        bool ToggleDelete(Guid id);
        Agency GetAgencyOnly(Guid id);
        //Agency GetAgencyWithMainLocation(Guid id);
        IEnumerable<Agency> All();
        IList<Agency> AllAgencies();
        bool Add(Agency agency);
        bool Update(Agency agency);
        List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds);
        List<AgencyLocation> AgencyLocationsMoreInfo(Guid agencyId, List<Guid> locationIds);

        List<AgencyUser> GetUserNames();
        List<AgencyUser> GetUserNames(Guid agencyId);

        bool AddLocation(AgencyLocation agencyLocation);
        AgencyLocation GetMainLocation(Guid agencyId);
        Dictionary<string, string> GetLocation(Guid agencyId, Dictionary<string, string> where, string[] columns);
        Guid GetMainLocationId(Guid agencyId);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        List<AgencyLocation> GetBranchesForUserControl(Guid agencyId, int service);
        Guid GetBranchForSelectionId(Guid agencyId, int service);
        AgencyLocation GetBranchForSelection(Guid agencyId, int service);
        List<AgencyLocation> GetBranchesForUserControl(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        AgencyLocation FindLocationOrMain(Guid agencyId, Guid Id);
        bool EditLocation(AgencyLocation location);
        bool EditLocationModal(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);
        bool EditBranchLocator(AgencyLocation location);

        bool AddContact(AgencyContact contact);
        IList<AgencyContact> GetContacts(Guid agencyId);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);

        bool AddHospital(AgencyHospital hospital);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);
        
        bool AddInsurance(AgencyInsurance insurance);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        IList<InsuranceLean> GetLeanInsurances(Guid agencyId);
        AgencyInsurance GetFirstAgencyInsuranceByPayer(Guid agencyId, int[] payers);
        AgencyInsurance GetFirstInsurance(Guid agencyId, int[] excludedPayerTypes);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        bool EditInsurance(AgencyInsurance insurance);
        bool EditInsuranceModal(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);
        bool IsMedicareHMO(Guid agencyId, int Id);

        List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds);
        List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds);
        List<AgencyInsurance> GetAgencyInsuranceSelection(Guid agencyId, List<int> onlyIncludedPayorTypes, List<int> excludedPayorTypes);
        List<AgencyInsurance> GetAgencyInsuranceSelection(Guid agencyId, bool isMedicareHMOOnly, List<int> excludedPayorTypes);

        bool AddTemplate(AgencyTemplate template);
        IList<AgencyTemplate> GetTemplates(Guid agencyId);
        AgencyTemplate GetTemplate(Guid agencyId, Guid id);
        bool UpdateTemplate(AgencyTemplate template);
        bool DeleteTemplate(Guid agencyId, Guid id);
        void InsertTemplates(Guid agencyId);

        bool AddSupply(AgencySupply supply);
        IList<AgencySupply> GetSupplies(Guid agencyId);
        IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit);
        IList<AgencySupply> GetSuppliesByIds(Guid agencyId, List<Guid> supplyIds);
        AgencySupply GetSupply(Guid agencyId, Guid id);
        bool UpdateSupply(AgencySupply supply);
        bool DeleteSupply(Guid agencyId, Guid id);
        void InsertSupplies(Guid agencyId);
       

        bool AddReport(Report report);
        bool UpdateReport(Report report);
        Report GetReport(Guid agencyId, Guid reportId);
        int GetTotalNumberOfReports(Guid agencyId);
        IList<ReportLite> GetReports(Guid agencyId, int pageSize, int page);

        bool AddCustomerNote(CustomerNote note);
        bool UpdateCustomerNote(CustomerNote note);
        CustomerNote GetCustomerNote(Guid agencyId, Guid noteId);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId);

        List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate);
        MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId);
        AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor);
        bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare);
        bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare);
        LocationPrintProfile AgencyNameWithLocationName(Guid agencyId, Guid locationId);
        LocationPrintProfile AgencyNameWithLocationAddress(Guid agencyId, Guid locationId);
        LocationPrintProfile AgencyNameWithMainLocationAddress(Guid agencyId);
        LocationPrintProfile AgencyNameWithAddressAndMore(Guid agencyId, Guid locationId);
       

        bool AddAdjustmentCode(AgencyAdjustmentCode code);
        IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId);
        AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id);
        bool UpdateAdjustmentCode(AgencyAdjustmentCode code);
        bool DeleteAdjustmentCode(Guid agencyId, Guid id);

        bool AddUploadType(UploadType uploadType);
        IList<UploadType> GetUploadTypes(Guid agencyId);
        UploadType FindUploadType(Guid agencyId, Guid Id);
        bool UpdateUploadType(UploadType uploadType);
        bool DeleteUploadType(Guid agencyId, Guid id);

        AgencyServices GetServicesAccessibleForAgency(Guid agencyId);

        IList<AgencyTeam> GetTeams(Guid agencyId);
        AgencyTeam GetTeam(Guid agencyId, Guid id);
        bool AddTeam(AgencyTeam team);
        bool UpdateTeam(AgencyTeam team);
        bool DeleteTeam(Guid agencyid, Guid id);
        IList<SelectedUser> GetTeamUsers(Guid agencyId, Guid teamId, List<User> agencyUsers);
        IList<SelectedUser> GetUserNotInTeam(Guid agencyId, Guid agencyTeamId, List<User> agencyUsers);
        bool AddUsersToTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers);
        bool RemoveUsersFromTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers);
        List<SelectedPatient> GetPatientsInTeam(Guid agencyId, Guid teamId);
        List<SelectedPatient> GetPatientsNotInTeam(Guid agencyId, Guid teamId);
        bool AddPatientsToTeam(Guid agencyTeamId, List<Guid> patientIds);
        bool RemovePatientsFromTeam(Guid agencyTeamId, List<Guid> patientIds);
        List<SelectedPatient> GetTeamAccess(Guid agencyId, Guid currentUserId);
    }
}
