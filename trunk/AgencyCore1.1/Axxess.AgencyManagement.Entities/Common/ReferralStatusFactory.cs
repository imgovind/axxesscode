﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Entities
{
   public static class ReferralStatusFactory
    {

       public static List<int> All()
       {
           var list = new List<int>();
           list.Add((int)ReferralStatus.Admitted);
           list.Add((int)ReferralStatus.Pending);
           list.Add((int)ReferralStatus.NonAdmission);
           list.Add((int)ReferralStatus.Rejected);
           return list;
       }

       public static List<int> NonPending()
       {
           var list = new List<int>();
           list.Add((int)ReferralStatus.Admitted);
           list.Add((int)ReferralStatus.NonAdmission);
           list.Add((int)ReferralStatus.Rejected);
           return list;
       }

       public static List<int> PossibleToAdmit()
       {
           var list = new List<int>();
           list.Add((int)ReferralStatus.Pending);
           list.Add((int)ReferralStatus.NonAdmission);
           list.Add((int)ReferralStatus.Rejected);
           return list;
       }

       public static List<int> PossibleToNonAdmit()
       {
           var list = new List<int>();
           list.Add((int)ReferralStatus.Pending);
           list.Add((int)ReferralStatus.Rejected);
           return list;
       }
    }
}
