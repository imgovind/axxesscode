﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Enums;

    public static class UB4PatientStatusFactory
    {
        public static List<string> Discharge()
        {
            var list = new List<string>();
            list.Add(((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString());
            list.Add(((int)UB4PatientStatus.DischargeToHospiceHome).ToString());
            list.Add(((int)UB4PatientStatus.DischargeToHospiceMedicareFacility).ToString());
            list.Add(((int)UB4PatientStatus.DischargeToShortTermHospital).ToString());
            list.Add(((int)UB4PatientStatus.DischargeToSNF).ToString());
            return list;
        }
    }
}
