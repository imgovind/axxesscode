﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Enums;


    public static class MedicareIntermediaryFactory
    {

        public static List<int> Intermediaries()
        {
            var list = new List<int>();
            list.Add((int)MedicareIntermediary.MedicareAnthemHealthPlans);
            list.Add((int)MedicareIntermediary.MedicareCigna);
            list.Add((int)MedicareIntermediary.MedicareNGS);
            list.Add((int)MedicareIntermediary.MedicarePalmettoGBA);
            return list;
        }
    }
}
