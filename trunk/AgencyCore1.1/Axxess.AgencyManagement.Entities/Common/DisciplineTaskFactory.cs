﻿namespace Axxess.AgencyManagement.Entities
{
    #region

    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    #endregion

    public static class DisciplineTaskFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// Converts the integer value of a discipline task into its name
        /// </summary>
        /// <param name="disciplineTask">The Discipline Task to convert</param>
        /// <returns></returns>
        public static string ConvertDisciplineTaskToString(int disciplineTask)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
            {
                return ((DisciplineTasks)disciplineTask).GetDescription();
            }
            return string.Empty;
        }

        public static List<int> AllDischargingOASISDisciplineTasks(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(DischargeOASISDisciplineTasks(isNonOasisIncluded));
            list.AddRange(DeathOASISDisciplineTasks());
            list.AddRange(TransferDischargeOASISDisciplineTasks());
            return list;
        }

        public static List<int> AllNonOASIS(bool isSNOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(NONOASISDischargeDisciplineTasks());
            list.AddRange(NONOASISRecertDisciplineTasks(isSNOasisIncluded));
            list.AddRange(NONOASISSOCDisciplineTasks(isSNOasisIncluded));
            return list;
        }

        public static List<int> AllNoteOrders()
        {
            var list = new List<int>();
            list.AddRange(OnlyEvalOrders(true));
            list.AddRange(MSWDischargeAndNoteOrders());
            return list;
        }

        public static List<int> AllNoteOrdersButEvals()
        {
            var list = new List<int>();
            list.AddRange(OnlyEvalReassessments());
            list.AddRange(MSWDischargeAndNoteOrders());
            return list;
        }

        public static List<int> AllOrders()
        {
            var list = new List<int>();
            list.AddRange(AllPhysicianOrders());
            list.AddRange(AllNoteOrders());
            return list;
        }

        public static List<int> AllPhysicianOrders()
        {
            var list = new List<int> { (int)DisciplineTasks.FaceToFaceEncounter };
            list.AddRange(PhysicianOrdersWithOutFaceToFace());
            return list;
        }

        public static List<int> AllSkilledNurseDisciplineTasks(bool isAssessmentIncluded)
        {
            var list = new List<int> { 
                (int)DisciplineTasks.SNDiabeticDailyVisit, 
                (int)DisciplineTasks.SNVPsychNurse,
                (int)DisciplineTasks.SNPsychAssessment, 
                (int)DisciplineTasks.SNPediatricVisit, 
                (int)DisciplineTasks.SNPediatricAssessment 
                 };
            list.AddRange(SkilledNurseSharedFile());
            if (isAssessmentIncluded)
            {
                list.AddRange(SNAssessments());
            }
            return list;
        }

        public static List<int> AllTherapy(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.AddRange(PTNoteDisciplineTasks());
            list.AddRange(PTEvalDisciplineTasks());
            list.Add((int)DisciplineTasks.PTReassessment);

            list.AddRange(OTNoteDisciplineTasks());
            list.AddRange(OTEvalDisciplineTasks(isDischargeInclueded));
            list.Add((int)DisciplineTasks.OTReassessment);

            list.AddRange(STNoteDisciplineTasks());
            list.AddRange(STEvalDisciplineTasks(isDischargeInclueded));

            return list;
        }

        public static List<int> AllUAP()
        {
            var list = new List<int> { (int)DisciplineTasks.UAPInsulinPrepAdminVisit, (int)DisciplineTasks.UAPWoundCareVisit };
            return list;
        }

        public static List<string> CMSDischargingOASISByEnumName()
        {
            var list = CMSDischargingOASISHelper().Select(d => d.ToString()).ToList();
            list.Add("Discharge");
            return list;
        }

        public static List<int> CMSDischargingOASIS()
        {
            var list = CMSDischargingOASISHelper().Select(d => (int)d).ToList();
            return list;
        }

        private static List<DisciplineTasks> CMSDischargingOASISHelper()
        {
            var list = new List<DisciplineTasks>
                           {
                               DisciplineTasks.OASISCDischarge,
                               DisciplineTasks.OASISCDischargeOT,
                               DisciplineTasks.OASISCDischargePT,
                               DisciplineTasks.OASISCTransferDischarge,
                               DisciplineTasks.OASISCTransferDischargePT,
                               DisciplineTasks.OASISCDeath,
                               DisciplineTasks.OASISCDeathOT,
                               DisciplineTasks.OASISCDeathPT
                           };

            return list;
        }

        public static List<int> AllAssessments(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(isNonOasisIncluded));
            list.AddRange(RecertDisciplineTasks(isNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            list.AddRange(FollowUpDisciplineTasks());

            list.AddRange(DischargeOASISDisciplineTasks(isNonOasisIncluded));
            list.AddRange(TransferDischargeOASISDisciplineTasks());
            list.AddRange(TransferNotDischargeOASISDisciplineTasks());

            list.AddRange(DeathOASISDisciplineTasks());

            return list;
        }

        public static List<int> DeathOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCDeath);
            list.Add((int)DisciplineTasks.OASISCDeathOT);
            list.Add((int)DisciplineTasks.OASISCDeathPT);
            return list;
        }

        public static List<int> DietDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NutritionalAssessment);
            list.Add((int)DisciplineTasks.DieticianVisit);
            return list;
        }

        public static List<int> DischargeOASISDisciplineTasks(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCDischarge);
            list.Add((int)DisciplineTasks.OASISCDischargeOT);
            list.Add((int)DisciplineTasks.OASISCDischargePT);
            if (isNonOasisIncluded)
            {
                list.AddRange(NONOASISDischargeDisciplineTasks());
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isNonOasisIncluded"></param>
        /// <returns></returns>
        public static List<int> EpisodeAllAssessments(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(isNonOasisIncluded));
            list.AddRange(RecertDisciplineTasks(isNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        /// <summary>
        /// For all assessments
        /// </summary>
        /// <returns></returns>
        public static List<int> AllAssessments()
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(true));
            list.AddRange(RecertDisciplineTasks(true));
            list.AddRange(ROCDisciplineTasks());
            list.AddRange(DischargeOASISDisciplineTasks(true));
            list.AddRange(TransferDischargeOASISDisciplineTasks());
            list.AddRange(TransferNotDischargeOASISDisciplineTasks());

            list.AddRange(DeathOASISDisciplineTasks());
            return list;
        }


        public static List<int> FollowUpDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCFollowUp);
            list.Add((int)DisciplineTasks.OASISCFollowupPT);
            list.Add((int)DisciplineTasks.OASISCFollowupOT);
            return list;
        }

        public static List<int> LastFiveDayAssessments(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(RecertDisciplineTasks(isNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        public static List<int> MSWDischargeAndNoteOrders()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.MSWEvaluationAssessment);
            list.Add((int)DisciplineTasks.PTDischarge);
            list.Add((int)DisciplineTasks.STDischarge);
            list.Add((int)DisciplineTasks.OTDischarge);
            list.Add((int)DisciplineTasks.SixtyDaySummary);
            return list;
        }

        public static List<int> MSWProgressNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.MSWProgressNote);
            return list;
        }

        public static List<int> NONOASISDischargeDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISDischarge);
            return list;
        }

        public static List<int> NONOASISRecertDisciplineTasks(bool isSNOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISRecertification);
            if (isSNOasisIncluded)
            {
                list.Add((int)DisciplineTasks.SNAssessmentRecert);
            }
            return list;
        }

        public static List<int> NONOASISSOCDisciplineTasks(bool isSNOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.NonOASISStartOfCare);
            if (isSNOasisIncluded)
            {
                list.Add((int)DisciplineTasks.SNAssessment);
            }
            return list;
        }

        /// <summary>
        /// Notes which dosen't need a physical visit of the patient , no time in and time out
        /// </summary>
        /// <returns></returns>
        public static List<int> NoneVisitNotes()
        {
            var list = new List<int>();
            list.AddRange(SNNoneVisitsNote());
            list.Add((int)DisciplineTasks.PTDischargeSummary);
            list.Add((int)DisciplineTasks.OTDischargeSummary);
          
           
            return list;
        }

        public static List<int> NoteCarePlanFromDisciplineTask(int task)
        {
            var tasks = new List<int>();
            switch (task)
            {
                case (int)DisciplineTasks.HHAideCarePlan:
                    tasks.Add((int)DisciplineTasks.HHAideCarePlan);
                    break;
                case (int)DisciplineTasks.PTEvaluation:
                    tasks.Add((int)DisciplineTasks.PTEvaluation);
                    tasks.Add((int)DisciplineTasks.PTReEvaluation);
                    break;
                case (int)DisciplineTasks.STEvaluation:
                    tasks.Add((int)DisciplineTasks.STEvaluation);
                    tasks.Add((int)DisciplineTasks.STReEvaluation);
                    break;
                case (int)DisciplineTasks.OTEvaluation:
                    tasks.Add((int)DisciplineTasks.OTEvaluation);
                    tasks.Add((int)DisciplineTasks.OTReEvaluation);
                    break;
            }
            return tasks;
        }

        public static List<int> NoteDischargeSummaries()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.DischargeSummary);
            list.Add((int)DisciplineTasks.PTDischargeSummary);
            list.Add((int)DisciplineTasks.OTDischargeSummary);
            return list;
        }

        public static List<int> OTEvalDisciplineTasks(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OTEvaluation);
            list.Add((int)DisciplineTasks.OTReEvaluation);
            if (isDischargeInclueded)
            {
                list.Add((int)DisciplineTasks.OTDischarge);
            }
            list.Add((int)DisciplineTasks.OTMaintenance);
            return list;
        }

        public static List<int> OTNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OTVisit);
            list.Add((int)DisciplineTasks.COTAVisit);
            return list;
        }

        public static List<int> OnlyEvalOrders(bool includeReassessments)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTEvaluation);
            list.Add((int)DisciplineTasks.OTEvaluation);
            list.Add((int)DisciplineTasks.STEvaluation);
            list.Add((int)DisciplineTasks.PTReEvaluation);
            list.Add((int)DisciplineTasks.OTReEvaluation);
            list.Add((int)DisciplineTasks.STReEvaluation);
            if (includeReassessments)
            {
                list.AddRange(OnlyEvalReassessments());
            }
            return list;
        }

        public static List<int> OnlyEvalReassessments()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTReassessment);
            list.Add((int)DisciplineTasks.OTReassessment);
            return list;
        }

        public static List<int> OutOfEpisodeDisciplineTask()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.FaceToFaceEncounter);
            return list;
        }

        public static List<int> POC()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.HCFA485StandAlone);
            list.Add((int)DisciplineTasks.HCFA485);
            list.Add((int)DisciplineTasks.NonOasisHCFA485);
            return list;
        }

        public static List<int> PTDischargeDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTDischarge);
            return list;
        }

        public static List<int> PTEvalDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTEvaluation);
            list.Add((int)DisciplineTasks.PTReEvaluation);
            list.Add((int)DisciplineTasks.PTMaintenance);
            return list;
        }

        public static List<int> PTNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.PTAVisit);
            list.Add((int)DisciplineTasks.PTVisit);
            return list;
        }

        public static List<DisciplineTasks> PhysicianEvalNotes()
        {
            var list = new List<DisciplineTasks>();
            list.Add(DisciplineTasks.PTEvaluation);
            list.Add(DisciplineTasks.PTReEvaluation);
            list.Add(DisciplineTasks.OTEvaluation);
            list.Add(DisciplineTasks.OTReEvaluation);
            list.Add(DisciplineTasks.STEvaluation);
            list.Add(DisciplineTasks.STReEvaluation);
            return list;
        }

        public static List<int> PhysicianOrdersWithOutFaceToFace()
        {
            var list = new List<int>();
            list.AddRange(POC());
            list.Add((int)DisciplineTasks.PhysicianOrder);
            return list;
        }

        public static List<int> ROCDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCResumptionOfCare);
            list.Add((int)DisciplineTasks.OASISCResumptionOfCareOT);
            list.Add((int)DisciplineTasks.OASISCResumptionOfCarePT);
            return list;
        }

        public static List<int> RecertDisciplineTasks(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCRecertification);
            list.Add((int)DisciplineTasks.OASISCRecertificationPT);
            list.Add((int)DisciplineTasks.OASISCRecertificationOT);
            if (isNonOasisIncluded)
            {
                list.AddRange(NONOASISRecertDisciplineTasks(true));
            }
            return list;
        }

        public static List<int> SNAssessments()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.SNAssessment);
            list.Add((int)DisciplineTasks.SNAssessmentRecert);
            return list;
        }

        public static List<int> SOCAndROCDisciplineTasks(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.AddRange(SOCDisciplineTasks(isNonOasisIncluded));
            list.AddRange(ROCDisciplineTasks());
            return list;
        }

        public static List<int> SOCDisciplineTasks(bool isNonOasisIncluded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCStartOfCare);
            list.Add((int)DisciplineTasks.OASISCStartOfCarePT);
            list.Add((int)DisciplineTasks.OASISCStartOfCareOT);
            if (isNonOasisIncluded)
            {
                list.AddRange(NONOASISSOCDisciplineTasks(true));
            }
            return list;
        }

      
        public static List<int> STEvalDisciplineTasks(bool isDischargeInclueded)
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.STEvaluation);
            list.Add((int)DisciplineTasks.STReEvaluation);
            if (isDischargeInclueded)
            {
                list.Add((int)DisciplineTasks.STDischarge);
            }
            list.Add((int)DisciplineTasks.STMaintenance);
            return list;
        }

        public static List<int> STNoteDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.STVisit);
            return list;
        }

       
        public static List<int> SkilledNurseSharedFile()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.SkilledNurseVisit);
            list.Add((int)DisciplineTasks.SNInsulinAM);
            list.Add((int)DisciplineTasks.SNInsulinPM);
            list.Add((int)DisciplineTasks.SNInsulinHS);
            list.Add((int)DisciplineTasks.SNInsulinNoon);
            list.Add((int)DisciplineTasks.FoleyCathChange);
            list.Add((int)DisciplineTasks.SNB12INJ);
            list.Add((int)DisciplineTasks.SNBMP);
            list.Add((int)DisciplineTasks.SNCBC);
            list.Add((int)DisciplineTasks.SNHaldolInj);
            list.Add((int)DisciplineTasks.PICCMidlinePlacement);
            list.Add((int)DisciplineTasks.PRNFoleyChange);
            list.Add((int)DisciplineTasks.PRNSNV);
            list.Add((int)DisciplineTasks.PRNVPforCMP);
            list.Add((int)DisciplineTasks.PTWithINR);
            list.Add((int)DisciplineTasks.PTWithINRPRNSNV);
            list.Add((int)DisciplineTasks.SkilledNurseHomeInfusionSD);
            list.Add((int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional);
            list.Add((int)DisciplineTasks.SNDC);
            list.Add((int)DisciplineTasks.SNEvaluation);
            list.Add((int)DisciplineTasks.SNFoleyLabs);
            list.Add((int)DisciplineTasks.SNFoleyChange);
            list.Add((int)DisciplineTasks.SNInjection);
            list.Add((int)DisciplineTasks.SNInjectionLabs);
            list.Add((int)DisciplineTasks.SNLabsSN);
            list.Add((int)DisciplineTasks.SNVwithAideSupervision);
            list.Add((int)DisciplineTasks.SNVDCPlanning);
            list.Add((int)DisciplineTasks.SNVTeachingTraining);
            list.Add((int)DisciplineTasks.SNVManagementAndEvaluation);
            list.Add((int)DisciplineTasks.SNVObservationAndAssessment);

            return list;
        }

        public static List<int> TransferDischargeOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCTransferDischarge);
            return list;
        }

        public static List<int> ResumptionOASISDisciplineTasks()
        {
            var list = new List<int> { (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISCResumptionOfCarePT };
            return list;
        }

        public static List<int> TransferNotDischargeOASISDisciplineTasks()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.OASISCTransfer);
            list.Add((int)DisciplineTasks.OASISCTransferOT);
            list.Add((int)DisciplineTasks.OASISCTransferPT);
            return list;
        }

        public static List<int> VitalSignsDisciplineTasks(bool includeOasis)
        {
            var list = new List<int>();
            list.AddRange(AllSkilledNurseDisciplineTasks(false));
            if (includeOasis)
            {
                list.AddRange(EpisodeAllAssessments(true));
            }
            list.AddRange(AllUAP());
            list.AddRange(AllTherapy(false));
            list.Add((int)DisciplineTasks.HHAideVisit);
            list.Add((int)DisciplineTasks.PASVisit);
            return list;
        }

        /// <summary>
        /// Generates a list of discipline tasks that can have multiple vital signs associated to it
        /// </summary>
        /// <returns></returns>
        public static List<int> DisciplineTasksWithMultiVitalSigns()
        {
            var list = new List<int>(PTNoteDisciplineTasks());
            return list;
        }

        public static List<int> AllPTNote()
        {

            var list = new List<int> 
              { 
                (int)DisciplineTasks.PTEvaluation ,
                (int)DisciplineTasks.PTVisit ,
                (int)DisciplineTasks.PTDischarge ,
                (int)DisciplineTasks.PTAVisit,
                (int)DisciplineTasks.PTReEvaluation ,
                (int)DisciplineTasks.PTMaintenance,
                (int)DisciplineTasks.PTDischargeSummary ,
                (int)DisciplineTasks.PTSupervisoryVisit,
                (int)DisciplineTasks.PTReassessment };
            return list;
        }

         public static List<int> AllOTNote(){

              var list = new List<int> 
              { 
                (int)DisciplineTasks.OTEvaluation,
                (int)DisciplineTasks.OTReEvaluation,
                (int)DisciplineTasks.OTVisit,
                (int)DisciplineTasks.OTReassessment,
                (int)DisciplineTasks.OTSupervisoryVisit ,
                (int)DisciplineTasks.OTDischargeSummary,
                (int)DisciplineTasks.OTDischarge ,
                (int)DisciplineTasks.OTMaintenance ,
                (int)DisciplineTasks.COTAVisit 
              };
            return list;
         }

         public static List<int> AllSTNote()
         {

              var list = new List<int> 
              { 
                    (int)DisciplineTasks.STVisit,
                    (int)DisciplineTasks.STEvaluation,
                    (int)DisciplineTasks.STDischarge,
                    (int)DisciplineTasks.STMaintenance,
                    (int)DisciplineTasks.STReEvaluation ,
                    (int)DisciplineTasks.STDischargeSummary,
                    (int)DisciplineTasks.STReassessment
              };
            return list;
          }

          public static List<int> AllMSWNote()
          {

              var list = new List<int> 
              {
                (int)DisciplineTasks.MSWEvaluationAssessment,
                (int)DisciplineTasks.MSWVisit ,
                (int)DisciplineTasks.MSWDischarge ,
                (int)DisciplineTasks.MSWAssessment ,
                (int)DisciplineTasks.MSWProgressNote, 
                (int)DisciplineTasks.DriverOrTransportationNote
              };
              return list;
          }

         public static List<int> AllHHANote()
         {

             var list = new List<int> 
              {
                    (int)DisciplineTasks.HHAideVisit,
                    (int)DisciplineTasks.PASVisit,
                    (int)DisciplineTasks.PASTravel,
                    (int)DisciplineTasks.PASCarePlan,
                    (int)DisciplineTasks.UAPWoundCareVisit,
                    (int)DisciplineTasks.UAPInsulinPrepAdminVisit,
                    (int)DisciplineTasks.HomeMakerNote
              };
             return list;
         }

         public static List<int> SNNoneVisitsNote()
         {
             var list = new List<int>();
             list.Add((int)DisciplineTasks.HHAideCarePlan);
             list.Add((int)DisciplineTasks.DischargeSummary);
             list.Add((int)DisciplineTasks.SixtyDaySummary);
             list.Add((int)DisciplineTasks.TransferSummary);
             list.Add((int)DisciplineTasks.CoordinationOfCare);
             return list;
         }

         public static List<int> AllNursingVisitNotes()
         {
             var list = new List<int>();
             list.AddRange(AllSkilledNurseDisciplineTasks(true));
             list.AddRange(SNNoneVisitsNote());
             list.Add((int)DisciplineTasks.LVNSupervisoryVisit);
             list.Add((int)DisciplineTasks.HHAideSupervisoryVisit);
             list.Add((int)DisciplineTasks.InitialSummaryOfCare);
             list.Add((int)DisciplineTasks.Labs);
             return list;
         }

        public static List<int> AllPatientVisitNotes()
        {
            var list = new List<int>();
            list.AddRange(AllNursingVisitNotes());
            list.AddRange(AllPTNote());
            list.AddRange(AllSTNote());
            list.AddRange(AllOTNote());
            list.AddRange(AllMSWNote());
            list.AddRange(AllHHANote());
            list.AddRange(DietDisciplineTasks());
            return list;
        }

        /// <summary>
        /// Documents that are hidden in the schedule center
        /// </summary>
        /// <returns></returns>
        public static List<int> HiddenDocuments()
        {
            var list = new List<int>();
            list.Add((int)DisciplineTasks.IncidentAccidentReport);
            list.Add((int)DisciplineTasks.InfectionReport);
            return list;
        }

        #endregion
    }
}