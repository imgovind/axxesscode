﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Enums;

    public static class ManagedClaimStatusFactory
    {
        public static List<int> UnProcessed()
        {
            var list = new List<int>();
            list.Add((int)ManagedClaimStatus.ClaimCreated);
            list.Add((int)ManagedClaimStatus.ClaimReOpen);
            return list;
        }

        public static List<int> Processed()
        {
            var list = new List<int>();
            list.AddRange(PaidAndSubmitted());
            list.Add((int)ManagedClaimStatus.ClaimPaymentPending);
            list.Add((int)ManagedClaimStatus.ClaimAccepted);
            return list;
        }

        public static List<int> PaidAndSubmitted()
        {
            var list = new List<int>();
            list.Add((int)ManagedClaimStatus.ClaimSubmitted);
            list.Add((int)ManagedClaimStatus.ClaimPaidClaim);
            return list;
        }

        public static List<int> Pending()
        {
            var list = new List<int>();
            list.Add((int)ManagedClaimStatus.ClaimSubmitted);
            list.Add((int)ManagedClaimStatus.ClaimPaymentPending);
            list.Add((int)ManagedClaimStatus.ClaimAccepted);
            return list;
        }

        public static List<int> UnProcessedAndReopened()
        {
            var list = new List<int>();
            list.AddRange(UnProcessed());
            list.Add((int)ManagedClaimStatus.ClaimRejected);
            list.Add((int)ManagedClaimStatus.ClaimCancelledClaim);
            return list;
        }

        public static List<int> UnProcessedWithErrors()
        {
            var list = new List<int>();
            list.AddRange(UnProcessed());
            list.Add((int)ManagedClaimStatus.ClaimRejected);
            list.Add((int)ManagedClaimStatus.ClaimWithErrors);
            return list;
        }

        //public static List<int> PPSStatus()
        //{
        //    var list = new List<int>();
        //    list.Add((int)ManagedClaimStatus.ClaimCreated);
        //    list.Add((int)ManagedClaimStatus.ClaimReOpen);
        //    list.Add((int)ManagedClaimStatus.ClaimRejected);
        //    list.Add((int)ManagedClaimStatus.ClaimCancelledClaim);
        //    return list;
        //}
    }
}
