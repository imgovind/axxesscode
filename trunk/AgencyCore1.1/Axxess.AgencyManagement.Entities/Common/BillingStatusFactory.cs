﻿namespace Axxess.AgencyManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Enums;

    public static class BillingStatusFactory
    {
        public static List<int> UnProcessed()
        {
            var list = new List<int>();
            list.Add((int)BillingStatus.ClaimCreated);
            list.Add((int)BillingStatus.ClaimReOpen);
            return list;
        }

        public static List<int> Processed()
        {
            var list = new List<int>();
            list.AddRange(PaidAndSubmitted());
            list.Add((int)BillingStatus.ClaimPaymentPending);
            list.Add((int)BillingStatus.ClaimAccepted);
            list.Add((int)BillingStatus.ProcessedAsPrimary);
            list.Add((int)BillingStatus.ProcessedAsSecondary);
            list.Add((int)BillingStatus.ProcessedAsTertiary);
            list.Add((int)BillingStatus.Pended);
            list.Add((int)BillingStatus.ReceivedNotInProcess);
            list.Add((int)BillingStatus.ReversalOfPreviousPayment);
            list.Add((int)BillingStatus.NotOurClaimFTAP);
            list.Add((int)BillingStatus.PredeterminationPricingOnly);
            list.Add((int)BillingStatus.Reviewed);
            return list;
        }

        public static List<int> PaidAndSubmitted()
        {
            var list = new List<int>();
            list.Add((int)BillingStatus.ClaimSubmitted);
            list.Add((int)BillingStatus.ClaimPaidClaim);
            return list;
        }

        public static List<int> Pending()
        {
            var list = new List<int>();
            list.Add((int)BillingStatus.ClaimSubmitted);
            list.Add((int)BillingStatus.ClaimPaymentPending);
            list.Add((int)BillingStatus.ClaimAccepted);
            return list;
        }

        public static List<int> UnProcessedAndReopened()
        {
            var list = new List<int>();
            list.AddRange(UnProcessed());
            list.Add((int)BillingStatus.ClaimRejected);
            list.Add((int)BillingStatus.ClaimCancelledClaim);
            return list;
        }

        public static List<int> UnProcessedWithErrors()
        {
            var list = new List<int>();
            list.AddRange(UnProcessed());
            list.Add((int)BillingStatus.ClaimRejected);
            list.Add((int)BillingStatus.ClaimWithErrors);
            return list;
        }

        public static List<int> PPSStatus()
        {
            var list = new List<int>();
            list.Add((int)BillingStatus.ClaimCreated);
            list.Add((int)BillingStatus.ClaimReOpen);
            list.Add((int)BillingStatus.ClaimRejected);
            list.Add((int)BillingStatus.ClaimCancelledClaim);
            return list;
        }
    }
}
