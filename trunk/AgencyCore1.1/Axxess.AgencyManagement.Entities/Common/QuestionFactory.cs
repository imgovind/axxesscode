﻿namespace Axxess.AgencyManagement.Entities.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Generates questions
    /// </summary>
    public class QuestionFactory<T> where T : QuestionBase, new()
    {
        public static T Create(string name, string answer)
        {
            return new T().Create(name, answer) as T;
        }
    }
}
