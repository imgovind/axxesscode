﻿namespace Axxess.ConsoleApp
{
    using System;

    public class PatientMedProfileScript
    {
        private PatientData patientData;
        private const string MEDPROFILE_INSERT = "INSERT INTO `medicationprofiles`(`Id`,`PatientId`,`AgencyId`,`Medication`,`Created`,`Modified`) " +
            "VALUES ('{0}', '{1}', '{2}', '{3}', {4}, {5});";

        public PatientMedProfileScript(PatientData patientData)
        {
            this.patientData = patientData;
        }

        public override string ToString()
        {
            return string.Format(MEDPROFILE_INSERT, patientData.MedProfileId, patientData.PatientId, patientData.AgencyId, patientData.Medication, patientData.Created, patientData.Modified);
        }
    }
}
