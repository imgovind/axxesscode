﻿namespace Axxess.ConsoleApp
{
    using System;

    public class DrugListingData
    {
        #region Members

        public string FullName { get; set; }
        public string Unit { get; set; }
        public string RxOrOTC { get; set; }
        public string Strength { get; set; }
        public string LabelCode { get; set; }
        public string TradeName { get; set; }
        public string ProductCode { get; set; }
        public string SequenceNumber { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return FullName;
        }
        #endregion
    }
}
