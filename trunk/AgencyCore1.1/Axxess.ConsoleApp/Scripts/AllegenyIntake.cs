﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class AllegenyIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\eminent.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Eminent_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var patientData = new PatientData();
                                    patientData.AgencyId = "7dd5ba4e-08dd-4c32-9709-264b280451d3";
                                    patientData.AgencyLocationId = "2d5a9f3b-66bd-4b19-bd8d-7cf130ba64fd";

                                    patientData.PatientNumber = dataRow.GetValue(6);
                                    patientData.FirstName = dataRow.GetValue(4);
                                    patientData.MiddleInitial = dataRow.GetValue(5);
                                    patientData.LastName = dataRow.GetValue(3);
                                    
                                    patientData.MedicaidNumber = "";
                                    patientData.AddressLine1 = dataRow.GetValue(7);
                                    patientData.AddressLine2 = dataRow.GetValue(8);
                                    patientData.AddressCity = dataRow.GetValue(9);
                                    patientData.AddressState = dataRow.GetValue(10);
                                    patientData.AddressZipCode = dataRow.GetValue(11);
                                    patientData.Gender = dataRow.GetValue(21);
                                    patientData.Phone = dataRow.GetValue(18).IsNotNullOrEmpty() ? dataRow.GetValue(18).ToPhoneDB() : "";
                                    patientData.SSN = dataRow.GetValue(23).Replace("-", "");

                                    if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("m"))
                                    {
                                        patientData.MaritalStatus = "Married";
                                    }
                                    if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("s"))
                                    {
                                        patientData.MaritalStatus = "Single";
                                    }
                                    if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("w"))
                                    {
                                        patientData.MaritalStatus = "Widowed";
                                    }
                                    if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("d"))
                                    {
                                        patientData.MaritalStatus = "Divorce";
                                    }
                                    if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("u"))
                                    {
                                        patientData.MaritalStatus = "Unknown";
                                    }

                                    if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("active"))
                                    {
                                        patientData.PatientStatusId = "1";
                                    }
                                    else if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(20).ToLower().Contains("terminated/discharged"))
                                    {
                                        patientData.PatientStatusId = "2";
                                    }
                                    else 
                                    {
                                        patientData.PatientStatusId = "3";
                                    }

                                    if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                    {
                                        patientData.BirthDate = DateTime.Parse(dataRow.GetValue(19)).ToString("yyyy-M-d");
                                    }
                                    if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                    {
                                        patientData.StartofCareDate = DateTime.Parse(dataRow.GetValue(25)).ToString("yyyy-M-d");
                                    }

                                    if (dataRow.GetValue(12).IsNotNullOrEmpty() && dataRow.GetValue(12).ToLowerInvariant().Contains("medicare"))
                                    {
                                        patientData.PrimaryInsurance = "1";
                                        patientData.MedicareNumber = dataRow.GetValue(13);
                                    }

                                    if (dataRow.GetValue(12).IsNotNullOrEmpty() && !dataRow.GetValue(12).ToLowerInvariant().Contains("medicare"))
                                    {
                                        patientData.Comments += string.Format("Health Insurance: {0}. Health Insurance Number: {1}. ", dataRow.GetValue(12), dataRow.GetValue(13));
                                    }
                                    // Escort
                                    if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("{0}. ", dataRow.GetValue(2));
                                    }
                                    // Triage/Priority
                                    if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Priority: {0}. ", dataRow.GetValue(14));
                                    }
                                    // Services Required
                                    if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Services Required: {0}. ", dataRow.GetValue(17));
                                    }
                                    // Case Worker
                                    if (dataRow.GetValue(24).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Case Worker: {0}. ", dataRow.GetValue(24));
                                    }
                                    // Doctor
                                    if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(26));
                                    }
                                    // Physician number
                                    if (dataRow.GetValue(27).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician Number: {0}. ", dataRow.GetValue(27).ToPhoneDB());
                                    }
                                    // DX                                  
                                    if (dataRow.GetValue(30).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(30));
                                    }
                                    if (dataRow.GetValue(34).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Second Diagnosis: {0}. ", dataRow.GetValue(34));
                                    }
                                    if (dataRow.GetValue(33).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Third Diagnosis: {0}. ", dataRow.GetValue(33));
                                    }
                                    if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Fourth Diagnosis: {0}. ", dataRow.GetValue(32));
                                    }
                                    if (dataRow.GetValue(31).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Fifth Diagnosis: {0}. ", dataRow.GetValue(31));
                                    }
                                    // Resp Employee
                                    if (dataRow.GetValue(35).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Employee Responsible: {0}. ", dataRow.GetValue(35));
                                    }

                                    // Cert Period
                                    if (dataRow.GetValue(36).IsNotNullOrEmpty() && dataRow.GetValue(37).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Cert Period: {0} - {1}. ", DateTime.Parse(dataRow.GetValue(36)).ToShortDateString(), DateTime.Parse(dataRow.GetValue(37)).ToShortDateString());
                                    }

                                    textWriter.WriteLine(new PatientScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());

                                    textWriter.Write(textWriter.NewLine);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
