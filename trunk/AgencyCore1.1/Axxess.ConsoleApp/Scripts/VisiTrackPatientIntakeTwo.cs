﻿using System;
using System.IO;
using System.Linq;
using System.Data;

using HtmlAgilityPack;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class VisiTrackPatientIntakeTwo
    {
        private static string input = Path.Combine(App.Root, "Files\\royal.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\royal_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.Load(input);

                int counter = 1;
                PatientData patient = null;
                htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                {
                    if (htmlNode.Name.IsEqual("html"))
                    {
                        htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                        {
                            if (bodyNode.Name.IsEqual("body"))
                            {
                                bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                {
                                    if (tableNode.IsDataRow())
                                    {
                                        int columnCounter = 0;
                                        patient = new PatientData();
                                        patient.AgencyId = "933d2e7b-6680-4b65-982d-462d012dcbed";
                                        patient.AgencyLocationId = "64eda0ec-57c2-45be-b621-f81365e31a94";
                                        patient.PatientStatusId = "1";
                                        tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                        {
                                            if (tRowNode.Name.IsEqual("tr"))
                                            {
                                                tRowNode.ChildNodes.ToList().ForEach(node =>
                                                {
                                                    if (node.Name.IsEqual("td"))
                                                    {
                                                        Console.WriteLine("{0}: {1}", columnCounter, node.InnerText);

                                                        if (columnCounter == 1)
                                                        {
                                                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                                                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (firstName.Length > 1)
                                                            {
                                                                patient.FirstName = firstName[0].Trim();
                                                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                                                            }
                                                            else
                                                            {
                                                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                                                            }
                                                        }
                                                        else if (columnCounter == 2)
                                                        {
                                                            patient.PatientNumber = node.InnerText.Trim();
                                                        }
                                                        else if (columnCounter == 3)
                                                        {
                                                            patient.Gender = node.InnerText.IsEqual("F") ? "Female" : "Male";
                                                        }
                                                        else if (columnCounter == 4)
                                                        {
                                                            patient.BirthDate = node.InnerText.ToMySqlDate();
                                                        }
                                                        else if (columnCounter == 5)
                                                        {
                                                            patient.StartofCareDate = node.InnerText.ToMySqlDate();
                                                        }
                                                        else if (columnCounter == 6)
                                                        {
                                                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                                                            {
                                                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                                                {
                                                                    patient.PrimaryInsurance = "1";
                                                                }
                                                                else
                                                                {
                                                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Trim());
                                                                }
                                                            }
                                                        }
                                                        else if (columnCounter == 7)
                                                        {
                                                            if (patient.PrimaryInsurance.IsEqual("1"))
                                                            {
                                                                patient.MedicareNumber = node.InnerText.Trim();
                                                            }
                                                            else if (patient.Comments.ToLower().Contains("medicaid"))
                                                            {
                                                                patient.MedicaidNumber = node.InnerText.Trim();
                                                            }
                                                            else
                                                            {
                                                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Trim());
                                                            }
                                                        }
                                                        else if (columnCounter == 8)
                                                        {
                                                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                                                            {
                                                                patient.Comments += string.Format("ICD: {0}. ", node.InnerText.Trim());
                                                            }
                                                        }
                                                        else if (columnCounter == 9)
                                                        {
                                                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                                                            {
                                                                patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Trim());
                                                            }
                                                        }
                                                        columnCounter++;
                                                    }
                                                });
                                            }
                                        });
                                        textWriter.WriteLine(new PatientScript(patient).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patient).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patient).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                        counter++;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }
}
