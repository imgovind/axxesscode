﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using System.Threading;

namespace Axxess.ConsoleApp.Tests
{
    public static class DrugUpdate
    {
        private static string listings = "listings.txt";
        private static string packages = "packages.txt";
        private static string listingoutput = string.Format("Drugs_{0}.txt", DateTime.Now.Ticks.ToString());
        private static string packageoutput = string.Format("DrugPackage_{0}.txt", DateTime.Now.Ticks.ToString());
        public static void Run()
        {
            try
            {
                //LoadDrugListing();
                LoadDrugPackages();
                CombineDrugAndPackages();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void CombineDrugAndPackages()
        {
            int counter = 1;
            List<DrugListingData> drugs = new List<DrugListingData>();

            string selectdrugs = @"select * from druglistings";
            using (var cmd = new FluentCommand<DrugListingData>(selectdrugs))
            {
                cmd.SetConnection("AxxessLookupConnectionString");
                drugs = cmd.AsList();
            }
            drugs.ForEach(d =>
            {
                var package = GetPackage(d.SequenceNumber);
                if (package != null)
                {
                    var ndc = d.LabelCode.Substring(1, 5) + d.ProductCode.Replace("*", "0") + package.Code.Replace("*", "0");
                    string insertdrug = string.Format("INSERT INTO `drugs`(`Name`,`Code`) VALUES ('{0}', '{1}');", d.FullName, ndc);
                    using (var cmd = new FluentCommand<int>(insertdrug))
                    {
                        cmd.SetConnection("AxxessLookupConnectionString");
                        if (cmd.AsNonQuery() > 0)
                        {
                            Console.WriteLine("Inserted Row {0}: {1}", counter.ToString(), d.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), d.ToString());
                        }
                    }
                    Console.WriteLine();
                    counter++;
                    GC.Collect();
                }
            });
        }

        private static DrugPackageData GetPackage(string sequenceNumber)
        {
            DrugPackageData package = new DrugPackageData();
            string selectpackage = @"select * from drugpackages where sequencenumber = @sequencenumber";
            using (var cmd = new FluentCommand<DrugPackageData>(selectpackage))
            {
                cmd.SetConnection("AxxessLookupConnectionString");
                package = cmd.AddString("sequencenumber", sequenceNumber)
                    .SetMap(reader => new DrugPackageData {
                        Code = reader.GetString("Code"),
                        Size = reader.GetString("Size"),
                        Type = reader.GetString("Type"),
                        SequenceNumber = reader.GetString("SequenceNumber")
                    }).AsSingle();
            }
            return package;
        }

        private static void LoadDrugListing()
        {
            int counter = 1;
            using (TextWriter listWriter = new StreamWriter(listingoutput, true))
            {
                using (FileStream listStream = new FileStream(listings, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader listReader = new StreamReader(listStream))
                    {
                        while (listReader.Peek() > -1)
                        {
                            var line = listReader.ReadLine();

                            if (line.IsNotNullOrEmpty() && line.Length >= 144)
                            {
                                var drug = new DrugListingData();
                                drug.SequenceNumber = line.Substring(0, 7).Trim();
                                drug.LabelCode = line.Substring(8, 6);
                                drug.ProductCode = line.Substring(15, 4);
                                drug.Strength = line.Substring(20, 10).Trim().Replace("'", "");
                                drug.Unit = line.Substring(31, 10).Trim().Replace("'", "");
                                drug.RxOrOTC = line.Substring(42, 1);
                                drug.TradeName = line.Substring(44, 100).Trim().Replace("'", "");

                                if (drug.TradeName.IsNotNullOrEmpty())
                                {
                                    drug.FullName = string.Format("{0} {1} {2}", drug.TradeName, drug.Strength ?? string.Empty, drug.Unit ?? string.Empty);
                                }
                                else
                                {
                                    drug.FullName = string.Empty;
                                }

                                Console.WriteLine("Inserting Row {0}: {1}", counter.ToString(), drug.ToString());
                                var sql = new DrugListingScript(drug).ToString();
                                using (var cmd = new FluentCommand<int>(sql))
                                {
                                    cmd.SetConnection("AxxessLookupConnectionString");
                                    if (cmd.AsNonQuery() > 0)
                                    {
                                        Console.WriteLine("Inserted Row {0}: {1}", counter.ToString(), drug.ToString());
                                    }
                                    else
                                    {
                                        Console.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), drug.ToString());
                                        listWriter.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), drug.ToString());
                                    }
                                }
                                Console.WriteLine();
                                counter++;
                                GC.Collect();
                            }
                        }
                    }
                }
            }
            
        }

        private static void LoadDrugPackages()
        {
            int counter = 1;
            using (TextWriter packageWriter = new StreamWriter(packageoutput, false))
            {
                using (FileStream packageStream = new FileStream(packages, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader packageReader = new StreamReader(packageStream))
                    {
                        while (packageReader.Peek() >= 0)
                        {
                            var line = packageReader.ReadLine();

                            if (line.IsNotNullOrEmpty() && line.Length >= 62)
                            {
                                var package = new DrugPackageData();
                                package.SequenceNumber = line.Substring(0, 7).Trim();
                                package.Code = line.Substring(8, 2);
                                package.Size = line.Substring(11, 25).Trim().Replace("'", "");
                                package.Type = line.Substring(37, 25).Trim().Replace("'", "");

                                Console.WriteLine("Inserting Row {0}: {1}", counter.ToString(), package.ToString());
                                var sql = new DrugPackageScript(package).ToString();
                                using (var cmd = new FluentCommand<int>(sql))
                                {
                                    cmd.SetConnection("AxxessLookupConnectionString");
                                    if (cmd.AsNonQuery() > 0)
                                    {
                                        Console.WriteLine("Inserted Row {0}: {1}", counter.ToString(), package.ToString());
                                    }
                                    else
                                    {
                                        Console.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), package.ToString());
                                        packageWriter.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), package.ToString());
                                    }
                                }
                                Console.WriteLine();
                                counter++;
                                GC.Collect();
                            }
                        }
                    }
                }
            }
        }
    }
}
