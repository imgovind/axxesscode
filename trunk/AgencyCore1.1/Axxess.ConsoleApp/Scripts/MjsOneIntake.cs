﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class MjsOneIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\cottage.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Cottage_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "82df7e15-d09a-47ca-beee-c91930fc5761";
                                patientData.AgencyLocationId = "e299c0ce-fae1-4037-bf1c-d3457e60b9ef";
                                patientData.LastName = record.GetValue(0).ToTitleCase();
                                patientData.FirstName = record.GetValue(1).ToTitleCase();
                                patientData.MiddleInitial = record.GetValue(2);
                                patientData.PatientNumber = record.GetValue(3);
                                patientData.AddressLine1 = record.GetValue(4).ToTitleCase();
                                patientData.AddressLine2 = "";
                                patientData.AddressCity = record.GetValue(5).ToTitleCase();
                                patientData.AddressState = record.GetValue(6);
                                patientData.AddressZipCode = record.GetValue(7);
                                patientData.Phone = record.GetValue(8).ToPhoneDB();
                                patientData.BirthDate = record.GetValue(10).ToMySqlDate(0);
                                patientData.Gender = record.GetValue(11) == "M" ? "Male" : "Female";
                                patientData.MedicareNumber = record.GetValue(13);
                                patientData.StartofCareDate = record.GetValue(14).ToMySqlDate(0);
                                patientData.PatientStatusId = record.GetValue(17).IsEqual("active") ? "1" : "2";
                                
                                patientData.Comments += string.Format("P CODE: {0}. ", record.GetValue(20));
                                patientData.Comments += string.Format("PAYER: {0}. ", record.GetValue(21));
                                patientData.Comments += string.Format("PROVIDER: {0}. ", record.GetValue(24));
                                patientData.Comments += string.Format("DX: {0}. ", record.GetValue(25));
                                patientData.Comments += string.Format("DX DESC: {0}. ", record.GetValue(26));
                                patientData.Comments += string.Format("PHY FIRST: {0}.", record.GetValue(27));
                                patientData.Comments += string.Format("PHY LAST: {0}.", record.GetValue(28));
                                patientData.Comments += string.Format("TRIAGE CODE: {0}.", record.GetValue(29));
                                patientData.Comments += string.Format("EVACUATION CODE: {0}.", record.GetValue(30));

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
