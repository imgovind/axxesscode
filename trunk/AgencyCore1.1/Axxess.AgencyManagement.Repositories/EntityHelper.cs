﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.DataProviders;
    using SubSonic.Extensions;
    using SubSonic.Repository;
    using SubSonic.SqlGeneration.Schema;

    internal class EntityHelper
    {
        public static bool ToggleUserEntityDeprecation<T>(Guid id, Guid userId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND UserId = @userid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated.ToInteger())
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool TogglePatientEntityDeprecation<T>(Guid id, Guid patientId, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated.ToInteger())
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ToggleEntityDeprecation<T>(Guid id, Guid agencyId, bool isDeprecated, string connectionString)
        {
            try
            {
                var name = GetTableName<T>();
                string script = @"UPDATE " + name + " SET IsDeprecated = @deprecated, Modified = @modified WHERE AgencyId = @agencyid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", id)
                        .AddInt("deprecated", isDeprecated.ToInteger())
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemovePatientEntity<T>(Guid agencyId, Guid patientId, Guid id, string connectionString)
        {
            var name = GetTableName<T>();
            var script = "DELETE FROM " + name + " WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
            using (var cmd = new FluentCommand<int>(script))
            {
                return cmd.SetConnection(connectionString)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", id).AsNonQuery() > 0;
            }
        }

        public static bool RemoveEntity<T>(Guid agencyId, Guid id, string connectionString)
        {
            var name = GetTableName<T>();
            var script = "DELETE FROM " + name + " WHERE AgencyId = @agencyid AND Id = @id";
            using (var cmd = new FluentCommand<int>(script))
            {
                return cmd.SetConnection(connectionString)
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", id).AsNonQuery() > 0;
            }
        }



        public static bool RemoveMultiplePatientEntities<T>(Guid agencyId, Guid patientId, List<Guid> eventIds, string connectionString)
        {
            var name = GetTableName<T>();
            var script = string.Format("DELETE FROM " + name + " WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id IN ( {0} ) ", eventIds.ToCommaSeperatedList());
            using (var cmd = new FluentCommand<int>(script))
            {
                 return cmd.SetConnection(connectionString)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AsNonQuery() > 0;
            }
        }

        /// <summary>
        /// Selects data that does not have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="id">Entity's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectEntityJsonByColumns<T>(Guid agencyId, Guid id, string connectionStringName, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>()
                .Where("AgencyId").IsEqualTo(agencyId).And("Id").IsEqualTo(id);
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Selects data that does have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="patientId">Patient's Id</param>
        /// <param name="id">Entity's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectPatientEntityJsonByColumns<T>(Guid agencyId, Guid patientId, Guid id, string connectionStringName, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>()
                .Where("AgencyId").IsEqualTo(agencyId).And("PatientId").IsEqualTo(patientId).And("Id").IsEqualTo(id);
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Selects data that does have a patient id from the database based off the columns passed to it.
        /// </summary>
        /// <typeparam name="T">The entity object type that represents what is stored in the database.</typeparam>
        /// <param name="agencyId">Agency's Id</param>
        /// <param name="connectionStringName">Connection Name</param>
        /// <param name="columns">Columns to be pulled form the database</param>
        /// <returns>A Dictionary, whose key value pairs are the columns name and value. Can be passed as Json or as the model to a view.</returns>
        public static Dictionary<string, string> SelectWhereByColumns<T>(Guid agencyId, string connectionStringName, Dictionary<string, string> where, params string[] columns)
        {
            var values = new Dictionary<string, string>();
            var query = new SubSonic.Query.Select(ProviderFactory.GetProvider(connectionStringName), columns).From<T>().Where("AgencyId").IsEqualTo(agencyId);
            query = @where.Aggregate(query, (current, w) => current.And(w.Key).IsEqualTo(w.Value));
            using (var reader = query.ExecuteReader())
            {
                while (reader != null && reader.Read())
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        values.Add(columns[i], reader.GetString(i));
                    }
                }
            }
            return values;
        }


        private static string GetTableName<T>()
        {
            var subSonicTableNameOverrideAttribute = (SubSonicTableNameOverrideAttribute)Attribute.GetCustomAttribute(typeof(T), typeof(SubSonicTableNameOverrideAttribute));
            if (subSonicTableNameOverrideAttribute == null)
            {
                var name = typeof(T).Name.ToLower();
                return name.MakePlural();
            }
            return subSonicTableNameOverrideAttribute.TableName;
        }
    }
}
