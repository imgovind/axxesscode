﻿namespace Axxess.AgencyManagement.Repositories.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB.Driver;
    using MongoDB.Driver.Builders;
    using MongoDB.Bson;

    using Axxess.Core.Extension;

    public static class MongoDatabaseExtensions
    {

        public static bool AddData<T>(this MongoDatabase db, string tableName, T data)
        {
            var collection = db.GetCollection<T>(tableName);
            try
            {
                return collection.Insert(data).Ok;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool AddDatas<T>(this MongoDatabase db, string tableName, List<T> data)
        {
            var collection = db.GetCollection<T>(tableName);
            try
            {
                var results = collection.InsertBatch(data).ToList();
                var failures = results.Where(e => !e.Ok).ToList();
                if (failures.IsNotNullOrEmpty())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static T GetData<T>(this MongoDatabase db, Guid agencyId, BsonValue Id, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("_id", Id));
            return collection.FindOneAs<T>(query);
        }

        public static T GetData<T>(this MongoDatabase db, Guid agencyId, Guid patientId, BsonValue Id, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("_id", Id));
            return collection.FindOneAs<T>(query);
        }

        public static T GetData<T>(this MongoDatabase db, Guid agencyId, Guid patientId, Guid episodeId, BsonValue Id, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("EpisodeId", episodeId), Query.EQ("_id", Id));
            return collection.FindOneAs<T>(query);
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.EQ("AgencyId", agencyId);
            return collection.FindAs<T>(query).ToList();
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, Guid patientId, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId));
            return collection.FindAs<T>(query).ToList();
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, Guid patientId, Guid episodeId, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("EpisodeId", episodeId));
            return collection.FindAs<T>(query).ToList();
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, List<Guid> Ids, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.In("_id", new BsonArray(Ids)));
            return collection.FindAs<T>(query).ToList();
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, Guid patientId, List<Guid> Ids, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.In("_id", new BsonArray(Ids)));
            return collection.FindAs<T>(query).ToList();
        }

        public static List<T> GetManyDatas<T>(this MongoDatabase db, Guid agencyId, Guid patientId, Guid episodeId, List<Guid> Ids, string tableName)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("EpisodeId", episodeId), Query.In("_id", new BsonArray(Ids)));
            return collection.FindAs<T>(query).ToList();
        }

        public static bool UpdateData<T>(this MongoDatabase db, Guid agencyId, BsonValue Id, string tableName, T data)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("_id", Id));
            return collection.Update(query, Update.Replace<T>(data), UpdateFlags.Upsert).Ok;
        }

        public static bool UpdateData<T>(this MongoDatabase db, Guid agencyId, Guid patientId, BsonValue Id, string tableName, T data)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("_id", Id));
            return collection.Update(query, Update.Replace<T>(data), UpdateFlags.Upsert).Ok;
        }

        public static bool UpdateData<T>(this MongoDatabase db, Guid agencyId, Guid patientId, Guid episodeId, BsonValue Id, string tableName, T data)
        {
            var collection = db.GetCollection<T>(tableName);
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId), Query.EQ("EpisodeId", episodeId), Query.EQ("_id", Id));
            return collection.Update(query, Update.Replace<T>(data), UpdateFlags.Upsert).Ok;
        }

    }
}
