﻿namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.Repository;

    public class HHMultiDocument : MultiDocumentAbstract
    {
        public HHMultiDocument(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            this.connectionStringName = connectionStringName;
        }
    }
}
