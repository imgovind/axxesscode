﻿namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.Repository;

    public class HHVitalSignRepository : VitalSignAbstract
    {
        public HHVitalSignRepository(SimpleRepository database, string connectionStringName)
            : base(database, connectionStringName)
        {
        }
    }
}
