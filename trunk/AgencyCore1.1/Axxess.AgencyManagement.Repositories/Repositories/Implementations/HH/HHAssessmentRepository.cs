﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Repositories
{
    public class HHAssessmentRepository : AssessmentAbstract
    {
        public HHAssessmentRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            this.connectionStringName = connectionStringName;
        }
    }
}
