﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    public class FaceToFaceEncounterRepository
    {
        private readonly SimpleRepository database;
        private readonly string connectionStringName;
        public FaceToFaceEncounterRepository(SimpleRepository database, string connectionStringName)
        {
            this.database = database;
            this.connectionStringName = connectionStringName;
        }

        #region FaceToFaceEncounter

        public bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            try
            {
                if (faceToFaceEncounter != null)
                {
                    database.Add<FaceToFaceEncounter>(faceToFaceEncounter);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool AddMultipleFaceToFaceEncounter(List<FaceToFaceEncounter> faceToFaceEncounters)
        {
            try
            {
                if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                {
                    database.AddMany<FaceToFaceEncounter>(faceToFaceEncounters);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }


        public bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateSent)
        {
            Check.Argument.IsNotEmpty(orderId, "orderId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            bool result = false;
            try
            {
                var order = database.Single<FaceToFaceEncounter>(o => o.Id == orderId && o.AgencyId == agencyId && o.IsDeprecated == false);
                if (order != null)
                {
                    order.Status = status;
                    order.SentDate = dateSent;
                    database.Update<FaceToFaceEncounter>(order);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                if (faceToFaceEncounter != null)
                {
                    try
                    {
                        faceToFaceEncounter.Modified = DateTime.Now;
                        database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                        result = true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id)
        {
          
            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM facetofaceencounters ff WHERE ff.AgencyId = @agencyid AND ff.PatientId = @patientid AND ff.Id = @id ");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id).AsNonQuery();

                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

   

        public bool DeleteFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated)
        {
            var result = false;
            try
            {
                var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.IsDeprecated = IsDeprecated;
                    faceToFaceEncounter.Modified = DateTime.Now;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (faceToFaceEncounter != null)
            {
                try
                {
                    faceToFaceEncounter.UserId = employeeId;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.Id == Id);
        }

        public List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds)
        {
            var script = string.Format(@"SELECT 
                    ff.Id as Id,
                    ff.EpisodeId as EpisodeId,
                    ff.OrderNumber as OrderNumber,
                    ff.PhysicianId as PhysicianId ,
                    pr.Id as PatientId ,
                    pr.FirstName as FirstName,
                    pr.LastName as LastName,
                    pr.MiddleInitial as MiddleInitial, 
                    ff.Status as Status , 
                    ff.ReceivedDate as ReceivedDate,
                    ff.RequestDate as RequestDate , 
                    ff.SentDate as SentDate , 
                    ff.SignatureDate as SignatureDate
                        FROM 
                            facetofaceencounters ff INNER JOIN patientprofiles pr ON ff.PatientId = pr.Id 
                                WHERE 
                                    ff.AgencyId = @agencyid AND 
                                    pr.IsDeprecated = 0 AND 
                                    pr.Status IN (1,2) AND
                                    ff.IsDeprecated = 0 AND
                                    ff.Id IN ( {0} ) AND 
                                    ff.Status = @status ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                    ff.Id as Id, 
                    ff.EpisodeId as EpisodeId,
                    ff.OrderNumber as OrderNumber,
                    ff.PhysicianId as PhysicianId ,
                    ff.PatientId as PatientId ,
                    ff.Status as Status ,
                    ff.ReceivedDate as ReceivedDate,
                    ff.RequestDate as RequestDate 
                        FROM 
                            facetofaceencounters  ff
                                WHERE
                                    ff.AgencyId = @agencyid AND
                                    ff.PatientId = @patientId AND
                                    ff.IsDeprecated = 0 AND
                                    ff.Id IN ( {0} ) ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    PhysicianId = reader.GetGuid("PhysicianId")
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT
                ff.Id as Id, 
                ff.EpisodeId as EpisodeId,
                ff.OrderNumber as OrderNumber,
                ff.PhysicianId as PhysicianId ,
                ff.PatientId as PatientId , 
                pr.FirstName as FirstName, 
                pr.LastName as LastName, 
                ff.Status as Status ,
                ff.ReceivedDate as ReceivedDate,
                ff.RequestDate as RequestDate,
                ff.SentDate as SentDate 
                    FROM
                        facetofaceencounters ff INNER JOIN patientprofiles pr ON ff.PatientId = pr.Id  
                            WHERE
                                ff.AgencyId = @agencyid AND
                                pr.IsDeprecated = 0 AND 
                                pr.Status IN (1,2) AND
                                ff.IsDeprecated = 0 AND
                                ff.Id IN ( {0} ) AND 
                                ( ff.Status = 130 || ff.Status = 145 )", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianId = reader.GetGuid("PhysicianId")
                }).AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetAllFaceToFaceEncounters()
        {
            return database.All<FaceToFaceEncounter>().ToList();
        }

        #endregion

        #region Md.Axxessweb.com

        public List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();
            var list = new List<FaceToFaceEncounter>();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `facetofaceencounters` ff ")
                .Where(string.Format("(ff.Status = {0} || ff.Status = {1})"
                , (int)ScheduleStatus.OrderSentToPhysicianElectronically, status))
                .And("ff.IsDeprecated = 0")
                .And(string.Format("ff.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            using (var cmd = new FluentCommand<FaceToFaceEncounter>(query.Build()))
            {
                list = cmd.SetConnection(connectionStringName).AsList();
            }

            return list;
        }
        #endregion
    }
}
