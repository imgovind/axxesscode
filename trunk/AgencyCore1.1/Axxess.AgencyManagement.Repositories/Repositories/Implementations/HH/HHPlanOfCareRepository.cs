﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using SubSonic.Repository;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class HHPlanOfCareRepository : PlanOfCareAbstract
    {
        #region IPlanOfCareRepo Members

        public HHPlanOfCareRepository(SimpleRepository database, string connectionStringName)
        {
            base.connectionStringName = connectionStringName;
            base.database = database;
            base.Service = AgencyServices.HomeHealth;
        }
       
        #endregion
    }
}
