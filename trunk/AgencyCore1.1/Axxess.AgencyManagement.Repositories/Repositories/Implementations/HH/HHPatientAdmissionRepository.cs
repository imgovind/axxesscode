﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;

   public class HHPatientAdmissionRepository : PatientAdmissionAbstract
    {
       public HHPatientAdmissionRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }
    }
}
