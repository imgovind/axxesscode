﻿
namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;

   public class HHNoteRepository : VisitNoteAbstract
    {

       public HHNoteRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.HomeHealth;
        }

       #region PatientVisitNote
       
       public List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks)
       {
           var additionalScript = string.Empty;

           if (disciplineTasks != null && disciplineTasks.Length > 0)
           {
               additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
           }

           if (scheduleStatus != null && scheduleStatus.Length > 0)
           {
               additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
           }

           var script = string.Format(@"SELECT 
                        pv.Id as Id ,
                        pv.PatientId as PatientId ,
                        pv.EpisodeId as EpisodeId ,
                        pv.UserId as UserId ,
                        pv.PhysicianId as PhysicianId , 
                        pv.NoteType as NoteType ,
                        pv.Status as Status ,
                        pe.StartDate as StartDate,
                        pe.StartDate as EndDate,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate 
                            FROM 
                                patientvisitnotes pv
                                    INNER JOIN scheduletasks st  ON pv.Id = st.Id 
                                    INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                        WHERE 
                                            pe.AgencyId = @agencyid AND 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND 
                                            st.IsMissedVisit = 0  AND 
                                            st.IsDeprecated = 0 AND
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {0} 
                                             ORDER BY  DATE(st.EventDate) DESC ", additionalScript);

           var list = new List<PatientVisitNote>();
           using (var cmd = new FluentCommand<PatientVisitNote>(script))
           {
               list = cmd.SetConnection(connectionStringName)
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .SetMap(reader => new PatientVisitNote
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   EpisodeId = reader.GetGuid("EpisodeId"),
                   UserId = reader.GetGuid("UserId"),
                   PhysicianId = reader.GetGuid("PhysicianId"),
                   NoteType = reader.GetStringNullable("NoteType"),
                   Status = reader.GetInt("Status"),
                   StartDate = reader.GetDateTime("StartDate"),
                   EndDate = reader.GetDateTime("EndDate"),
                   EventDate = reader.GetDateTime("EventDate"),
                   VisitDate = reader.GetDateTime("VisitDate"),
               })
               .AsList();
           }
           return list;

           //return database.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.NoteType == task.ToString() && p.PatientId == patientId && p.Status == status && p.IsDeprecated == false).OrderByDescending(p => p.Created).ToList();
       }

       //public List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds, DateTime startDate, DateTime endDate)
       //{
       //    var script = string.Format(@"SELECT patientvisitnotes.Id as Id, patientvisitnotes.PhysicianId as PhysicianId, patientvisitnotes.EpisodeId as EpisodeId, patientvisitnotes.OrderNumber as OrderNumber, " +
       //       "patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, patientvisitnotes.NoteType as NoteType, " +
       //       "patientvisitnotes.Status as Status, patientvisitnotes.Note as Note, patientvisitnotes.ReceivedDate as ReceivedDate, patientvisitnotes.SentDate as SentDate, agencyphysicians.Credentials as PhysicianCredentials , patientvisitnotes.PhysicianSignatureDate as PhysicianSignatureDate " +
       //       "FROM patientvisitnotes INNER JOIN patients ON patientvisitnotes.PatientId = patients.Id INNER JOIN agencyphysicians ON patientvisitnotes.PhysicianId = agencyphysicians.Id " +
       //       "WHERE patientvisitnotes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2) " +
       //       "AND patientvisitnotes.IsDeprecated = 0 AND patientvisitnotes.Id IN ({0}) ", evalIds);

       //    var list = new List<PatientVisitNote>();
       //    using (var cmd = new FluentCommand<PatientVisitNote>(script))
       //    {
        //        list = cmd.SetConnection(connectionStringName)
       //        .AddGuid("agencyid", agencyId)
       //        .AddDateTime("startDate", startDate)
       //        .AddDateTime("endDate", endDate)
       //        .SetMap(reader => new PatientVisitNote
       //        {
       //            Id = reader.GetGuid("Id"),
       //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
       //            PatientId = reader.GetGuid("PatientId"),
       //            EpisodeId = reader.GetGuid("EpisodeId"),
       //            Status = reader.GetInt("Status"),
       //            OrderDate = GetNoteVisitDate(reader.GetStringNullable("Note")),
       //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
       //            NoteType = reader.GetStringNullable("NoteType"),
       //            SentDate = reader.GetDateTime("SentDate"),
       //            DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
       //            PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
       //            PhysicianId = reader.GetGuid("PhysicianId"),
       //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
       //        })
       //        .AsList();

       //    }
       //    var query = (from p in list
       //                 where p.OrderDate >= startDate && p.OrderDate <= endDate
       //                 select p).ToList();
       //    return query;
       //}

       //public List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate)
       //{
       //    var list = GetEvalOrders(agencyId, evalIds, startDate, endDate);
       //    return list.Where(p => status.Contains(p.Status)).ToList();
       //}

       #endregion

      
    }
}
