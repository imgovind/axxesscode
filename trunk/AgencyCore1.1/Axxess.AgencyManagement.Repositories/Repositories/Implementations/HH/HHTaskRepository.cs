﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Infrastructure.Data;

    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Enums;


    public class HHTaskRepository : TaskScheduleAbstract<ScheduleEvent>
    {
        public HHTaskRepository(SimpleRepository database, string connectionStringName)
            : base(database, connectionStringName)
        {
            base.Service = AgencyServices.HomeHealth;
        }

        #region Get Schedule

        public ScheduleEvent GetLastScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue  
                            FROM
                                scheduletasks st
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 AND
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventDate) between DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}  
                                            ORDER BY DATE(st.EventDate) DESC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = startDate,
                    EndDate = endDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetFirstScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.IsVisitPaid as IsVisitPaid  
                            FROM 
                                scheduletasks st  
                                    WHERE 
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND 
                                        st.DisciplineTask > 0 AND 
                                        st.IsMissedVisit = 0  AND 
                                        DATE(st.EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0} 
                                            ORDER BY DATE(st.EventDate) ASC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    StartDate = episodeStartDate,
                    EndDate = episodeEndDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFactory.AllStatusButOrdersAndReports().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid  AND
                                        st.EpisodeId = @episodeid AND 
                                        st.IsDeprecated = 0 AND 
                                        st.IsBillable = 1 AND  
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                        st.Status IN ( {0} ) 
                                            ORDER BY 
                                                 DATE(st.VisitDate) ASC LIMIT 1", status.Join(","));

            var schedule = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                schedule = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsSingle();
            }
            return schedule;
        }

        #endregion

        #region Get Schedules

     
        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EpisodeId == episodeId && e.IsDeprecated == false && e.DisciplineTask > 0)
                .Where(e => e.VisitDate.Date >= startDate.Date && e.VisitDate.Date <= endDate.Date).OrderBy(s => s.VisitDate.Date)
                .ToList();
        }

        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, int payorId, DateTime startDate, DateTime endDate)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EpisodeId == episodeId && e.PayorId==payorId && e.IsDeprecated == false && e.DisciplineTask > 0)
                .Where(e => e.VisitDate.Date >= startDate.Date && e.VisitDate.Date <= endDate.Date).OrderBy(s => s.VisitDate.Date)
                .ToList();
        }

        public List<ScheduleEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, int payor)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };


            var script1 = string.Format(@"SELECT 
                    '{5}' as Id ,
                    pe.PatientId as PatientId ,
                    pe.Id as EpisodeId ,
                    '{5}' as UserId ,
                    {7} as DisciplineTask , 
                    '{6}' as VisitDate ,
                    '{6}' as EventDate ,
                    {8} as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN
                                    patientprofiles pr ON pe.PatientId = pr.Id  
                                        WHERE 
                                               pr.AgencyId = @agencyid  AND
                                               pr.Status = 1 {0} {1} AND 
                                               pr.IsDeprecated = 0 AND 
                                               pe.IsActive = 1 AND 
                                               pe.IsDischarged = 0 AND 
                                               DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                               DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                               Not Exists ( SELECT * from scheduletasks st WHERE st.PatientId = pr.Id AND st.EpisodeId = pe.Id AND DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND st.DisciplineTask > 0 AND st.IsDeprecated = 0 AND  st.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4}) )",
                                                                                   insurance,
                                                                                   !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   Guid.Empty.ToString(),
                                                                                   DateTime.MinValue.ToString("yyyy-MM-dd"),
                                                                                   (int)DisciplineTasks.OASISCRecertification,
                                                                                   0);
            var script2 = string.Format(@"SELECT 
                    st.Id as Id ,
                    pr.Id as PatientId ,
                    pr.Id as EpisodeId ,
                    st.UserId as UserId ,
                    st.DisciplineTask as DisciplineTask , 
                    st.EventDate as EventDate ,
                    st.VisitDate as VisitDate ,
                    st.Status as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id  
                                LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id 
                                    WHERE 
                                           pr.AgencyId = @agencyid  AND
                                           pr.Status = 1 {0} {1} AND 
                                           pr.IsDeprecated = 0 AND 
                                           pe.IsActive = 1 AND 
                                           pe.IsDischarged = 0 AND 
                                           DATE(pe.EndDate) < DATE(curdate()) AND 
                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                           st.DisciplineTask > 0 AND
                                           st.IsDeprecated = 0 AND 
                                           st.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} OR {4} )",
                                                           insurance,
                                                           !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate", DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status", 0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, int payor)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor>0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };


            var script1 = string.Format(@"SELECT 
                    '{5}' as Id ,
                    pe.PatientId as PatientId ,
                    pe.Id as EpisodeId ,
                    '{5}' as UserId ,
                    {7} as DisciplineTask , 
                    '{6}' as EventDate ,
                    '{6}' as VisitDate ,
                    {8} as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id  
                                    WHERE 
                                           pr.AgencyId = @agencyid  AND
                                           pr.Status = 1 {0} {1} AND 
                                           pr.IsDeprecated = 0 AND 
                                           pe.IsActive = 1 AND 
                                           pe.IsDischarged = 0 AND 
                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           Not Exists ( SELECT * from scheduletasks st WHERE st.PatientId = pr.Id AND st.EpisodeId = pe.Id AND DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND st.DisciplineTask > 0 AND st.IsDeprecated = 0 AND  st.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4} ) )",
                                                                                     insurance,
                                                                                     !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     Guid.Empty.ToString(),
                                                                                     DateTime.MinValue.ToString("yyyy-MM-dd"),
                                                                                     (int)DisciplineTasks.OASISCRecertification,
                                                                                     0);
            var script2 = string.Format(@"SELECT 
                    st.Id as Id ,
                    st.PatientId as PatientId ,
                    st.EpisodeId as EpisodeId ,
                    st.UserId as UserId ,
                    st.DisciplineTask as DisciplineTask , 
                    st.EventDate as EventDate ,
                    st.VisitDate as VisitDate ,
                    st.Status as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id  
                                LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id 
                                    WHERE 
                                           pr.AgencyId = @agencyid  AND
                                           pr.Status = 1 {0} {1} AND 
                                           pr.IsDeprecated = 0 AND 
                                           pe.IsActive = 1 AND 
                                           pe.IsDischarged = 0 AND 
                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                           st.DisciplineTask > 0 AND
                                           st.IsDeprecated = 0 AND 
                                           st.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} OR {4} ) ",
                                                            insurance,
                                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate", DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status", 0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }

            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (status != null && status.Length > 0)
            {
                additionalScript += string.Format(" AND st.Status IN ( {0} )", status.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles  pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            pr.AgencyId = @agencyid  AND 
                                            pr.Status IN (1,2) {0} AND
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} ", 
                                                                                                                            branchScript,
                                                                                                                             additionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, Guid patientId, DateTime fromDate, DateTime toDate,int statusOptional)
        {
            var disciplines = DisciplineFactory.TherapyDisciplines().Select(d => d.ToString()).ToArray();// new string[] { Disciplines.PT.ToString(), Disciplines.OT.ToString(), Disciplines.ST.ToString() };
            var patientOrStatusScript = string.Empty;
            if (patientId.IsEmpty())
            {
                if (statusOptional > 0)
                {
                    patientOrStatusScript = string.Format(" AND pr.Status = {0} AND pr.IsDeprecated = 0 ", statusOptional);
                }
                else
                {
                    patientOrStatusScript = " AND pr.Status IN (1,2) AND pr.IsDeprecated = 0 ";
                }
            }
            else
            {
                patientOrStatusScript = " AND pr.Id = @patientId ";

            }
            var script = string.Format(@"SELECT 
                            st.Id as Id ,
                            st.PatientId as PatientId ,
                            st.EpisodeId as EpisodeId ,
                            st.UserId as UserId ,
                            st.DisciplineTask as DisciplineTask , 
                            st.EventDate as EventDate ,
                            st.VisitDate as VisitDate ,
                            st.Status as Status ,
                            st.Discipline as Discipline ,
                            pe.EndDate as EndDate,
                            pe.StartDate as StartDate,
                            pr.FirstName as FirstName, 
                            pr.LastName as LastName, 
                            pr.PatientIdNumber as PatientIdNumber 
                                 FROM scheduletasks st  
                                            INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                            INNER JOIN patientprofiles pr ON st.PatientId = pr.Id    
                                                  WHERE 
                                                        st.AgencyId = @agencyid {0} {1} AND
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0 AND 
                                                        DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        st.IsDeprecated = 0  AND
                                                        st.IsBillable = 1 AND
                                                        st.IsMissedVisit = 0 AND 
                                                        st.Discipline IN ( {2} )",

                                                                                 !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty,
                                                                                 patientOrStatusScript,
                                                                                 disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", fromDate)
                .AddDateTime("enddate", toDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<ScheduleEvent> GetScheduleTasksByStatusDisciplineAndRange(Guid agencyId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks)
        {
            var scheduleList = new List<ScheduleEvent>();
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                var statusScript = string.Empty;
                if (patientStatus <= 0)
                {
                    statusScript = " AND  pr.Status IN (1,2)";
                }
                else
                {
                    statusScript = " AND pr.Status = @statusid ";
                }
                string branchScript = string.Empty;
                if (!branchId.IsEmpty())
                {
                    branchScript = " AND pr.AgencyLocationId = @branchId ";
                }
                var additionalScript = string.Empty;
                if (disciplineTasks != null && disciplineTasks.Length > 0)
                {
                    additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
                }
                if (scheduleStatus != null && scheduleStatus.Length > 0)
                {
                    additionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }

                var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode ,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName, 
                        pr.PatientIdNumber as PatientIdNumber,
                        pr.MiddleInitial as MiddleInitial 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE
                                            st.AgencyId = @agencyid {0} {1} AND 
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {2} ", statusScript, branchScript, additionalScript);


                using (var cmd = new FluentCommand<ScheduleEvent>(script))
                {
                    scheduleList = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusid", patientStatus)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                    })
                    .AsList();
                }
            }
            return scheduleList;
        }

        public override List<ScheduleEvent> GetScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND
                                            pr.Status IN (1,2) AND
                                            st.IsDeprecated = 0  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            st.DisciplineTask IN ( {0} ) AND
                                            st.Status IN ( {1} ) {2}",
                                                                                
                                                                                 disciplineTasks.Select(d => d.ToString()).ToArray().Join(","),
                                                                                 status.Select(s => s.ToString()).ToArray().Join(","),
                                                                                 !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<ScheduleEvent> GetPatientScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid  AND
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            st.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }


        //        public List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        //        {
        //            var disciplineTasks = DisciplineTaskFactory.AllOrders().Select(s => s.ToString()).ToArray();// new string[] { ((int)DisciplineTasks.FaceToFaceEncounter).ToString(), ((int)DisciplineTasks.HCFA485).ToString(), ((int)DisciplineTasks.NonOasisHCFA485).ToString(), ((int)DisciplineTasks.PhysicianOrder).ToString(), ((int)DisciplineTasks.HCFA485StandAlone).ToString(), ((int)DisciplineTasks.PTEvaluation).ToString(), ((int)DisciplineTasks.OTEvaluation).ToString(), ((int)DisciplineTasks.STEvaluation).ToString(), ((int)DisciplineTasks.PTReEvaluation).ToString(), ((int)DisciplineTasks.OTReEvaluation).ToString(), ((int)DisciplineTasks.STReEvaluation).ToString() };
        //            var status = ScheduleStatusFactory.OrdersPendingPhysicianSignature().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString(), ((int)ScheduleStatus.OrderSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString() };

        //            var script = string.Format(@"SELECT 
        //                        scheduleevents.Id as Id ,
        //                        scheduleevents.PatientId as PatientId ,
        //                        scheduleevents.EpisodeId as EpisodeId ,
        //                        scheduleevents.UserId as UserId ,
        //                        scheduleevents.DisciplineTask as DisciplineTask , 
        //                        scheduleevents.EventDate as EventDate ,
        //                        scheduleevents.VisitDate as VisitDate ,
        //                        scheduleevents.Status as Status ,
        //                        scheduleevents.Discipline as Discipline ,
        //                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
        //                            FROM 
        //                                scheduleevents 
        //                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
        //                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
        //                                        WHERE
        //                                            scheduleevents.AgencyId = @agencyid  AND
        //                                            patients.IsDeprecated = 0 AND 
        //                                            patients.Status IN (1,2)  AND
        //                                            scheduleevents.IsDeprecated = 0  AND 
        //                                            patientepisodes.IsActive = 1  AND
        //                                            patientepisodes.IsDischarged = 0  AND
        //                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
        //                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
        //                                            scheduleevents.DisciplineTask IN ( {0} ) AND
        //                                            scheduleevents.Status IN ( {1} ) {2}", disciplineTasks.Join(","), status.Join(","), !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

        //            var scheduleList = new List<ScheduleEvent>();
        //            using (var cmd = new FluentCommand<ScheduleEvent>(script))
        //            {
        //                scheduleList = cmd.SetConnection(connectionStringName)
        //                .AddGuid("agencyid", agencyId)
        //                .AddGuid("branchId", branchId)
        //                .AddDateTime("startdate", startDate)
        //                .AddDateTime("enddate", endDate)
        //                .SetMap(reader => new ScheduleEvent
        //                {
        //                    Id = reader.GetGuid("Id"),
        //                    PatientId = reader.GetGuid("PatientId"),
        //                    EpisodeId = reader.GetGuid("EpisodeId"),
        //                    UserId = reader.GetGuid("UserId"),
        //                    DisciplineTask = reader.GetInt("DisciplineTask"),
        //                    EventDate = reader.GetDateTime("EventDate"),
        //                    VisitDate = reader.GetDateTime("VisitDate"),
        //                    Status = reader.GetInt("Status", 0),
        //                    Discipline = reader.GetStringNullable("Discipline"),
        //                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
        //                })
        //                .AsList();
        //            }
        //            return scheduleList;
        //        }

        public List<ScheduleEvent> GetEpisodesForSurveyCensesAndPatientRoster(Guid agencyId, string patientIds)
        {
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments(true).ToArray();
            var list = new List<ScheduleEvent>();
            var script = string.Format(@"SELECT 
                                            pe.PatientId as PatientId,
                                            pe.Id as EpisodeId ,
                                            pe.StartDate as StartDate, 
                                            pe.EndDate as EndDate, 
                                            st.Id as Id ,
                                            st.UserId as UserId ,
                                            st.DisciplineTask as DisciplineTask , 
                                            st.EventDate as EventDate ,
                                            st.VisitDate as VisitDate ,
                                            st.Status as Status ,
                                            st.Discipline as Discipline
                                                    FROM  
                                                        patientepisodes pe
                                                           LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id  AND st.DisciplineTask IN ( {1} )
                                                                WHERE 
                                                                     pe.AgencyId = @agencyid AND 
                                                                     pe.PatientId IN ( {0} ) AND
                                                                     pe.IsActive = 1 AND 
                                                                     pe.IsDischarged = 0 AND 
                                                                     st.DisciplineTask > 0 AND
                                                                     st.IsDeprecated = 0 AND 
                                                                     st.IsMissedVisit = 0 AND
                                                                     DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                                     DATE(pe.StartDate) < Date(@startdate)", patientIds, disciplineTasksOASISList.Select(d => d.ToString()).ToArray().Join(","));

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", DateTime.Now)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline")

                }).AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnlyLeanNew(Guid agencyId, Guid episodeId, Guid patientId, int[] scheduleStatus, string discipline, int[] disciplineTasks, bool isReportAndNotesIncluded)
        {
            var additionalScript = string.Empty;

            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (discipline.IsEqual("all"))
            {
                additionalScript += string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                additionalScript += " AND (STRCMP(st.Discipline ,'PT') = 0 OR STRCMP(st.Discipline, 'OT') = 0 OR STRCMP(st.Discipline , 'ST') = 0) ";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                additionalScript += " AND (STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP(st.Discipline, 'ReportsAndNotes') = 0) ";
            }
            else
            {
                additionalScript += " AND st.Discipline = @discipline ";
            }
            //if (discipline.IsNotNullOrEmpty() && discipline.IsEqual("all"))
            //{
            //    additionalScript += string.Format(" AND Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            //}
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate  
                            FROM 
                                scheduletasks  st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid {0} AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 
                                            ORDER BY DATE(st.EventDate) DESC ", additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid[] episodeIds, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var episodeScript = string.Empty;
            if (episodeIds != null && episodeIds.Length > 0)
            {
                if (episodeIds.Length == 1)
                {
                    episodeScript += string.Format(" AND st.EpisodeId = '{0}' ", episodeIds.FirstOrDefault());
                }
                else
                {
                    episodeScript += string.Format(" AND st.EpisodeId IN ( {0} )", episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
            }
            var dateRange = IsDateRange ? " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {
                disciplineScript = string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'PT') = 0 OR STRCMP(st.Discipline, 'OT') = 0 OR STRCMP(st.Discipline , 'ST') = 0 )";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP(st.Discipline, 'ReportsAndNotes') = 0 ) ";
            }
            else if (discipline.IsEqual("Dietation"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'Dietician') = 0 ";
            }
            else
            {
                disciplineScript = " AND st.Discipline = @discipline ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                        st.Asset as Asset ,
                        st.Version as Version ,
                        st.UserName as UserName 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid {2} AND 
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0 {0} {1} 
                                            ORDER BY DATE(st.EventDate) DESC ", disciplineScript, dateRange, episodeScript);
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddString("discipline", discipline)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetDeletedTasks(Guid agencyId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                        st.Version  as Version 
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND 
                                            st.IsDeprecated = 1 AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 
                                                ORDER BY DATE(st.EventDate) DESC");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate  
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND 
                                            st.PatientId = @patientid  AND
                                            st.Id != @eventid  AND
                                            st.IsMissedVisit = 0 AND
                                            st.IsDeprecated = 0  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            st.DisciplineTask IN ({0}) AND
                                            st.Status IN ({1}) 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.Id)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate  
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id  
                                        WHERE 
                                            st.AgencyId = @agencyid  AND 
                                            st.PatientId = @patientid  AND
                                            st.Id != @eventid  AND
                                            st.IsMissedVisit = 0 AND 
                                            st.IsDeprecated = 0 AND 
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  AND
                                            st.DisciplineTask IN ({0}) AND
                                            st.Status IN ({1}) AND
                                            st.Discipline IN ({2}) 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.Id)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty;
            if (patientStatus <= 0)
            {
                status = " AND  pr.Status IN (1,2)";
            }
            else
            {
                status = " AND pr.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND pr.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND st.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.BeforeMissedVisitStatus as BeforeMissedVisitStatus , 
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        pe.Comments as EpisodeNotes ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName 
                            FROM 
                               scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid {2} AND
                                            st.IsDeprecated = 0  {3} AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {0}  {1}",
                                                                                                                                                               schedulleStatusScript,
                                                                                                                                                               branch,
                                                                                                                                                               status,
                                                                                                                                                               missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    BeforeMissedVisitStatus = reader.GetInt("BeforeMissedVisitStatus"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }

       
         
        public List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName  
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND 
                                            pr.IsDeprecated = 0  AND 
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) <> DATE(st.VisitDate) AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} 
                                            ORDER BY pr.LastName ASC , pr.FirstName ASC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }

//        public override List<ScheduleEvent> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
//        {
//            var additonalScript = string.Empty;

//            if (!branchId.IsEmpty())
//            {
//                additonalScript += " AND pr.AgencyLocationId = @branchId";
//            }
//            var script = string.Format(@"SELECT 
//                        st.Id as Id ,
//                        st.PatientId as PatientId ,
//                        st.EpisodeId as EpisodeId ,
//                        st.UserId as UserId ,
//                        st.DisciplineTask as DisciplineTask , 
//                        st.EventDate as EventDate ,
//                        st.VisitDate as VisitDate ,
//                        st.Status as Status ,
//                        pr.FirstName as FirstName, 
//                        pr.LastName as LastName
//                                FROM 
//                                    scheduletasks st
//                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
//                                        INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  
//                                                WHERE
//                                                        st.AgencyId = @agencyid {0} AND 
//                                                        st.IsMissedVisit = 0 AND
//                                                        st.IsDeprecated = 0 AND
//                                                        st.InPrintQueue = 1 AND
//                                                        pr.IsDeprecated = 0  AND 
//                                                        pr.Status IN (1,2) AND 
//                                                        st.IsDeprecated = 0  AND 
//                                                        pe.IsActive = 1 AND 
//                                                        pe.IsDischarged = 0  AND 
//                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
//                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) 
//                                                        ORDER BY st.EventDate DESC ", additonalScript);

//            var scheduleList = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                scheduleList = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("branchId", branchId)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .SetMap(reader => new ScheduleEvent
//                {
//                    Id = reader.GetGuid("Id"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    EventDate = reader.GetDateTime("EventDate"),
//                    VisitDate = reader.GetDateTime("VisitDate"),
//                    Status = reader.GetInt("Status", 0),
//                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
//                })
//                .AsList();
//            }
//            return scheduleList;

//        }

        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByPatient(Guid agencyId, Guid patientId, int limit, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            
            var script = string.Format(@"SELECT 
                    st.Id as Id ,
                    st.VisitDate ,
                    st.DisciplineTask as DisciplineTask  
                FROM scheduletasks st
                INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id  AND st.EpisodeId = st.EpisodeId
                WHERE 
                    pe.AgencyId = @agencyid AND 
                    st.AgencyId = @agencyid AND 
                    st.PatientId = @patientid AND 
                    st.IsMissedVisit = 0 AND 
                    st.IsDeprecated = 0 AND 
                    pe.IsActive = 1 AND 
                    pe.IsDischarged = 0 {0} ORDER BY st.EventDate DESC LIMIT 0,{1}", additionalScript, limit);
           
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list.GetRange(0, limit < list.Count ? limit : list.Count);
        }

        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByEpisode(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, int[] notesDisciplineTasks)
        {
            var additionalScript = string.Empty;

            if (notesDisciplineTasks != null && notesDisciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", notesDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.VisitDate ,
                        st.DisciplineTask as DisciplineTask  
                            FROM scheduletasks st
                                WHERE 
                                      st.AgencyId = @agencyid AND 
                                      st.PatientId = @patientid AND 
                                      st.EpisodeId = @episodeid AND 
                                      st.IsMissedVisit = 0 AND 
                                      st.IsDeprecated = 0 AND 
                                      DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript);
            
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleEventsVeryLeanOptionalPar(Guid agencyId, Guid branchIdOptional, Guid patientIdOptional, Guid episodeIdOptional, int[] scheduleStatus, int[] disciplineTasks, int patientStatus, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var branchAdditionalScript = string.Empty;
            if (!branchIdOptional.IsEmpty())
            {
                branchAdditionalScript = " AND pr.AgencyLocationId = @branchId ";
            }
            var patientAdditionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                patientAdditionalScript += " AND st.PatientId = @patientId ";
            }
            else
            {
                patientAdditionalScript = string.Format(" AND pr.IsDeprecated = 0 {0} ", patientStatus <= 0 ? " AND pr.Status IN (1,2) " : " AND pr.Status = " + patientStatus);
            }
            var episodeAdditionalScript = string.Empty;
            if (!episodeIdOptional.IsEmpty())
            {
                episodeAdditionalScript = " AND st.EpisodeId = @episodeid ";
            }
            else
            {
                episodeAdditionalScript = " AND pe.IsActive = 1  AND pe.IsDischarged = 0  ";
            }
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (IsDateRange)
            {
                scheduleAdditionalScript += " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE
                                            st.AgencyId = @agencyId {0} {1} {2} AND 
                                            st.IsDeprecated = 0 AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {3} ", branchAdditionalScript, patientAdditionalScript, episodeAdditionalScript, scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchIdOptional)
                .AddGuid("patientId", patientIdOptional)
                .AddGuid("episodeid", episodeIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetScheduleEventsVeryLeanNonOptionalPar(Guid agencyId, Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (IsDateRange)
            {
                scheduleAdditionalScript += " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.IsBillable as IsBillable,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyId  AND 
                                            st.PatientId = @patientId  AND
                                            st.EpisodeId = @episodeid  AND 
                                            st.IsDeprecated = 0  {0} ", scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    Id = reader.GetGuid("Id"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

      
     
     
        public List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded)
        {
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {

            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'PT') = 0 OR STRCMP( st.Discipline, 'OT') = 0 OR STRCMP( st.Discipline , 'ST') = 0 ) ";
            }
            else
            {
                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
                {
                    disciplineScript = " AND ( STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP( st.Discipline, 'ReportsAndNotes') = 0) ";
                }
                else
                {
                    disciplineScript = " AND st.Discipline = @discipline ";
                }
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                        st.Asset as Asset ,
                        st.Version  as Version ,
                        st.UserName  as UserName 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.EpisodeId = @episodeid AND 
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0 {0}  
                                            ORDER BY DATE(st.EventDate) DESC  ", disciplineScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate)
        {
            var dateRange = IsDateRange ? " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                        st.Asset as Asset ,
                        st.Version  as Version ,
                        st.UserName  as UserName 
                            FROM
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND 
                                            st.EpisodeId = @episodeid AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 {0} 
                                                ORDER BY DATE(st.EventDate) DESC ", dateRange);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleByUserId(Guid agencyId, Guid userId, DateTime start, DateTime end, bool isMissedVist, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.PatientId as PatientId ,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.EventDate as EventDate ,
                                    st.VisitDate as VisitDate ,
                                    st.Status as Status ,
                                    st.IsMissedVisit as IsMissedVisit , 
                                    st.ReturnReason as ReturnReason ,
                                    st.Comments as Comments ,
                                    pe.StartDate as StartDate ,
                                    pe.EndDate as EndDate ,
                                    pr.FirstName as FirstName , 
                                    pr.LastName as LastName ,
                                    pe.Comments as EpisodeNotes 
                                        FROM 
                                            scheduletasks st
                                                INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                                INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.UserId = @userid AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.IsMissedVisit = {0} AND
                                                        st.DisciplineTask > 0 AND 
                                                        pe.IsActive = 1  AND 
                                                        pe.IsDischarged = 0 AND 
                                                        pr.IsDeprecated = 0 AND
                                                        pr.Status IN (1,2) AND
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} 
                                                            ORDER BY DATE(st.EventDate) DESC ", isMissedVist, additionalFilter);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", start)
                .AddDateTime("enddate", end)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("EndDate"),
                    Status = reader.GetInt("Status", 0),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public override IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                        pr.Id as PatientId , 
                        pe.Id as EpisodeId,
                        pe.IsDischarged as IsDischarged,
                        st.Id as Id,
                        st.DisciplineTask as DisciplineTask , 
                        st.IsMissedVisit as IsMissedVisit,
                        st.Status as Status , 
                        st.EventDate as EventDate ,
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId = @userid AND
                                            st.IsDeprecated = 0 AND 
                                            st.IsMissedVisit = {0} AND 
                                            st.DisciplineTask > 0 AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND
                                            pr.IsDeprecated = 0 AND
                                            pr.Status IN (1,2) AND 
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {2} 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT {1}", isMissedVisit, limit, additionalFilter);

            var list = new List<UserVisitWidget>();
            using (var cmd = new FluentCommand<UserVisitWidget>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", from)
                .AddDateTime("enddate", to)
                .SetMap(reader => new UserVisitWidget
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventId = reader.GetGuid("Id"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                    EventDate = reader.GetDateTime("EventDate").ToString("MM/dd/yyyy")
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetScheduleByUserIdLean(Guid agencyId, Guid patientIdOptional, Guid userId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            var leftJoinScript = string.Empty;
            var patientScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                patientScript = " AND st.PatientId = @patientid ";
            }
            else
            {
                leftJoinScript = " INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  ";
                patientScript = " AND pr.Status IN (1,2) AND pr.IsDeprecated = 0 ";
            }
            if (IsDateRange)
            {
                additionalScript += " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.PatientId as PatientId ,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.EventDate as EventDate ,
                                    st.VisitDate as VisitDate ,
                                    st.Status as Status ,
                                    pe.StartDate as StartDate ,
                                    pe.EndDate as EndDate 
                                        FROM 
                                            scheduletasks st
                                                INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                                {0}
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.UserId = @userid AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0 AND 
                                                        pe.IsActive = 1  AND 
                                                        pe.IsDischarged = 0  
                                                        {1} AND
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate)  {2} "
                , leftJoinScript, patientScript, additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                 .AddGuid("patientid", patientIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = userId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("EndDate"),
                    Status = reader.GetInt("Status", 0),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                })
                .AsList();
            }
            return list;
        }

        public override List<ScheduleEvent> GetScheduleEventsVeryLeanByIds(Guid agencyId, Guid patientId, List<Guid> ids, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (ids != null && ids.Count > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.Id IN ( {0} ) ", ids.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND st.IsMissedVisit = 0 ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.IsBillable as IsBillable,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyId  AND 
                                            st.PatientId = @patientId  AND
                                            st.IsDeprecated = 0  {0} ", scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new ScheduleEvent
                {
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientId = patientId,
                    Id = reader.GetGuid("Id"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetCurrentAndPreviousOrders(Guid agencyId, PatientEpisode episode, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate  
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.PatientId = pe.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND
                                            st.IsMissedVisit = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND 
                                            (( pe.Id = @episodeid AND st.IsOrderForNextEpisode = 0 ) OR (pe.Id != @episodeid AND  DATE(pe.EndDate) = DATE(@previousenddate) AND st.IsOrderForNextEpisode = 1 )) AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( {0} ))
                                                ORDER BY DATE(st.EventDate) DESC  ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", episode.PatientId)
                .AddGuid("episodeid", episode.Id)
                .AddDateTime("previousenddate", episode.StartDate.AddDays(-1))
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;

        }

        public override List<ScheduleEvent> GetUsersScheduleTasksBetweenDatesByStatus(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, params int[] scheduleStatus)
        {
            return GetScheduleByUserIdLean(agencyId, patientId, userId, scheduleStatus, new string[] { }, new int[] { }, true, startDate, endDate, false);
        }

        public override List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        CONCAT('Reason: ', m.Reason, '\r\n', 'Comments: ', m.Comments) as MissedVisitComments,
                        st.Comments as Comments,
                        pr.PatientIdNumber as PatientIdNumber,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName,
                        pr.UserId as PatientUserId
                                FROM 
                                    scheduletasks st
                                        INNER JOIN missedvisits m ON st.Id = m.Id
                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  
                                                WHERE
                                                        st.AgencyId = @agencyid {0} AND 
                                                        st.IsMissedVisit = 1 AND
                                                        st.IsDeprecated = 0 AND
                                                        pr.IsDeprecated = 0  AND 
                                                        pr.Status IN (1,2) AND 
                                                        st.IsDeprecated = 0  AND 
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0  AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY st.EventDate DESC ", additonalScript);

            List<ScheduleEvent> scheduleList;
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientUserId = reader.GetGuidIncludeEmpty("PatientUserId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    MissedVisitComments = reader.GetStringNullable("MissedVisitComments"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsMissedVisit = true,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }

        #endregion

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set  st.IsBillable = 1 WHERE  st.AgencyId = @agencyid AND  st.PatientId = @patientid AND  st.EpisodeId = @episodeid AND  st.Id IN ( {0})", scheduleEvents.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid", episodeId)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool ToggleScheduledEventsDelete(Guid agencyId, Guid patientId, Guid episodeId, List<Guid> eventIds, bool isDeprecated)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.IsDeprecated = {0} WHERE  st.AgencyId = @agencyid AND  st.PatientId = @patientid AND  st.EpisodeId = @episodeid AND  st.Id IN ( {1})", isDeprecated ? 1 : 0, eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid", episodeId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }
     
        public List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!userId.IsEmpty())
            {
                additionalScript += " AND st.UserId = @userid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND st.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND st.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsVisitPaid as IsVisitPaid ,
                        pe.PrimaryInsurance as PrimaryInsurance,
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName ,
                        pr.PatientIdNumber as PatientIdNumber 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            pe.IsActive = 1  AND 
                                            pe.IsDischarged = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {0} 
                                                ORDER BY DATE(st.EventDate) DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .AddGuid("userid", userId)
                .SetMap(reader => new UserVisit
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    PrimaryInsurance = reader.GetInt("PrimaryInsurance"),                    
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    VisitRate = "$0.00"
                })
                .AsList();
            }
            return list;
        }

        public List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND st.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND st.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                         st.UserId as UserId ,
                         COUNT(*) as VisitCount  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId != '{1}' AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND
                                            pr.IsDeprecated = 0 AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {0} 
                                                GROUP BY st.UserId", additionalScript, Guid.Empty.ToString());

            var list = new List<VisitSummary>();
            using (var cmd = new FluentCommand<VisitSummary>(script))
            {
                list = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new VisitSummary
                {
                    UserId = reader.GetGuid("UserId"),
                    VisitCount = reader.GetInt("VisitCount")
                })
                .AsList();
            }
            return list;
        }

        /// <summary>
        /// Gets the Schedule Tasks that belong to orders limited by their disipline and status
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="branchId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="disciplineTasks"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public override List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND
                                            pr.Status IN (1,2) AND
                                            st.IsDeprecated = 0  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( {0} )) AND
                                            st.Status IN ( {1} ) {2}",

                                                                                 disciplineTasks.Select(d => d.ToString()).ToArray().Join(","),
                                                                                 status.Select(s => s.ToString()).ToArray().Join(","),
                                                                                 !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        /// <summary>
        /// Gets the Schedule Tasks that belong to orders and a specific patient, and limited by their disipline and status
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="disciplineTasks"></param>
        /// <returns></returns>
        public override List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid  AND
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( {0} ))", 
                                                disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetRAPClaimSchedules(Guid agencyId, List<Guid> ids)
        {
            var completedStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().ToArray();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
            var socDisciplineTasks = DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCStartofCare, (int)DisciplineTasks.OASISCStartofCarePT, (int)DisciplineTasks.OASISCStartofCareOT };
            var script1 = string.Format(@"SELECT 
                                        st.Id as Id ,
                                        st.PatientId as PatientId ,
                                        st.EpisodeId as EpisodeId ,
                                        st.EventDate as EventDate ,
                                        st.VisitDate as VisitDate ,
                                        st.Status as Status ,
                                        st.DisciplineTask as DisciplineTask , 
                                        st.Discipline as Discipline ,
                                        st.IsBillable as IsBillable ,
                                        st.IsMissedVisit as IsMissedVisit , 
                                        st.TimeIn as TimeIn , 
                                        st.TimeOut as TimeOut ,
                                        pe.StartDate as StartDate ,
                                        pe.EndDate as EndDate ,
                                        pe.Id as NextEpisodeId ,
                                       'FirstBillableVisitOrSOC' as VisitType 
                                            FROM 
                                                 scheduletasks st
                                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id  
                                                        WHERE 
                                                            pe.AgencyId = @agencyid AND 
                                                            pe.Id IN ( {0} ) AND 
                                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                                            st.IsMissedVisit = 0  AND
                                                            st.IsDeprecated = 0 AND
                                                            st.DisciplineTask > 0  AND
                                                            (( st.IsBillable = 1 AND  st.Status IN ( {1} ) )  OR st.DisciplineTask IN ( {2} )) ",
                                                                                                                                                                                    ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "),
                                                                                                                                                                                    completedStatus.Select(s => s.ToString()).ToArray().Join(", "),
                                                                                                                                                                                    socDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));

            var rocOrRecertDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT }; 
            var script2 = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut , 
                        secondaryepisode.StartDate as StartDate ,
                        secondaryepisode.EndDate as EndDate ,
                        primaryepisode.Id as NextEpisodeId ,
                        'RecertOrROCOASIS' as VisitType 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes as primaryepisode ON st.PatientId = primaryepisode.PatientId  
                                    INNER JOIN patientepisodes as secondaryepisode ON st.PatientId = secondaryepisode.PatientId 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            primaryepisode.Id IN ( {0} )  AND
                                            DATE(secondaryepisode.EndDate) = DATE(DATE_SUB(primaryepisode.StartDate, INTERVAL 1 DAY)) AND
                                            secondaryepisode.IsActive = 1  AND 
                                            secondaryepisode.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between DATE(DATE_SUB(secondaryepisode.EndDate, INTERVAL 5 DAY)) and DATE(secondaryepisode.EndDate) AND
                                            st.IsMissedVisit = 0  AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND 
                                            st.DisciplineTask IN ( {1} ) ",
                                                                                      ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "),
                                                                                      rocOrRecertDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));

            var script = string.Format(" ({0}) UNION ({1}) ", script1, script2);

            var list = new List<ScheduleEvent>();

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    VisitType = reader.GetStringNullable("VisitType"),
                    NextEpisodeId = reader.GetStringNullable("VisitType").IsEqual("RecertOrROCOASIS") ? reader.GetGuid("NextEpisodeId") : Guid.Empty
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetFinalClaimSchedules(Guid agencyId, List<Guid> ids)
        {
            var visitDisciplines = DisciplineFactory.NonBillableDisciplines().Select(d => d.ToString()).ToArray();
            var visitStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().ToArray();
            var orderDisciplineTasks = DisciplineTaskFactory.PhysicianOrdersWithOutFaceToFace().ToArray();
            var orderStatus = new string[] { ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() };

            var script1 = string.Format(@"SELECT 
                                        st.Id as Id ,
                                        st.PatientId as PatientId ,
                                        st.EpisodeId as EpisodeId ,
                                        st.EventDate as EventDate ,
                                        st.VisitDate as VisitDate ,
                                        st.Status as Status ,
                                        st.DisciplineTask as DisciplineTask , 
                                        st.Discipline as Discipline ,
                                        st.IsBillable as IsBillable ,
                                        st.IsMissedVisit as IsMissedVisit , 
                                        st.IsOrderForNextEpisode as IsOrderForNextEpisode ,
                                        pe.StartDate as StartDate ,
                                        pe.EndDate as EndDate ,
                                        pe.Id as NextEpisodeId ,
                                       'VisitsOrOrders' as VisitType 
                                        FROM 
                                            scheduletasks st
                                                INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                                    WHERE
                                                        pe.AgencyId = @agencyid AND 
                                                        pe.Id IN ( {0} ) AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        st.IsMissedVisit = 0  AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0  AND 
                                                        (( st.IsBillable = 1 AND  st.Status NOT IN ( {1} )  AND st.Discipline NOT IN ( {2} ))  OR ( st.DisciplineTask IN ( {3} ) AND st.IsOrderForNextEpisode = 0 AND  st.Status NOT IN ( {4} ) )) ", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), visitStatus.Select(s => s.ToString()).ToArray().Join(", "), visitDisciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","), orderDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "), orderStatus.Join(", "));

            var script2 = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        secondaryepisode.StartDate as StartDate ,
                        secondaryepisode.EndDate as EndDate ,
                        primaryepisode.Id as NextEpisodeId ,
                       'PreviousOrders' as VisitType 
                            FROM
                                 scheduletasks st
                                    INNER JOIN patientepisodes as primaryepisode ON st.PatientId = primaryepisode.PatientId 
                                    INNER JOIN patientepisodes as secondaryepisode ON st.PatientId = secondaryepisode.PatientId  
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            primaryepisode.Id IN ( {0} )  AND 
                                            DATE(secondaryepisode.EndDate) = DATE(DATE_SUB(primaryepisode.StartDate, INTERVAL 1 DAY)) AND 
                                            secondaryepisode.IsActive = 1  AND
                                            secondaryepisode.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between DATE(secondaryepisode.StartDate) and DATE(secondaryepisode.EndDate) AND
                                            st.IsMissedVisit = 0  AND
                                            st.IsDeprecated = 0  AND
                                            st.IsOrderForNextEpisode = 1 AND
                                            st.DisciplineTask > 0  AND
                                            st.DisciplineTask IN ( {1} ) AND
                                            st.Status NOT IN ( {2} ) ", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), orderDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "), orderStatus.Join(", "));

            var script = string.Format(" ({0}) UNION ({1}) ", script1, script2);

            var list = new List<ScheduleEvent>();

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new ScheduleEvent
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     DisciplineTask = reader.GetInt("DisciplineTask"),
                     EventDate = reader.GetDateTime("EventDate"),
                     VisitDate = reader.GetDateTime("VisitDate"),
                     Status = reader.GetInt("Status", 0),
                     Discipline = reader.GetStringNullable("Discipline"),
                     IsBillable = reader.GetBoolean("IsBillable"),
                     IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                     IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                     EndDate = reader.GetDateTime("EndDate"),
                     StartDate = reader.GetDateTime("StartDate"),
                     VisitType = reader.GetStringNullable("VisitType"),
                     NextEpisodeId = reader.GetStringNullable("VisitType").IsEqual("PreviousOrders") ? reader.GetGuid("NextEpisodeId") : Guid.Empty
                 })
                 .AsList();
            }
            return list;
        }

        public override string GetReturnReason(Guid agencyId, Guid patientId,  Guid eventId, Guid currentUserId)
        {
            var task = this.GetScheduleTask(agencyId, patientId, eventId);
            var newComments = GetReturnCommentsWithUserInfo(agencyId,  eventId);
            return newComments.ToCommentText(task != null ? task.ReturnReason : string.Empty, currentUserId);
        }

        protected override string GetWhereAppSpecificSql(SqlSelectType sql)
        {
            string select = string.Empty;
            switch (sql)
            {
                case SqlSelectType.EventDate:
                    select = "st.EventDate";
                    break;
                case SqlSelectType.VisitDate:
                    select = "st.VisitDate";
                    break;
            }
            return select;
        }


        protected override string GetSelectAppSpecificSql(SqlSelectType sql)
        {
            string select = string.Empty;
            switch (sql)
            {
                case SqlSelectType.All:
                    select = "st.TimeIn as TimeIn , st.TimeOut as TimeOut , st.EventDate as EventDate , st.VisitDate as VisitDate , ";
                    break;
                case SqlSelectType.EventDate:
                    select = "st.EventDate";
                    break;
                case SqlSelectType.VisitDate:
                    select = "st.VisitDate";
                    break;
            }
            return select;
        }

        protected override void GetSelectAppSpecificSql(ScheduleEvent task, DataReader reader)
        {
            task.TimeIn = reader.GetStringNullable("TimeIn");
            task.TimeOut = reader.GetStringNullable("TimeOut");
            task.EventDate = reader.GetDateTime("EventDate");
            task.VisitDate = reader.GetDateTime("VisitDate");
        }

        protected override void GetSelectAppSpecificSql(UserVisit task, DataReader reader)
        {
            task.VisitDate = reader.GetDateTime("VisitDate");
            task.ScheduleDate = reader.GetDateTime("EventDate");
        }

    }
}
