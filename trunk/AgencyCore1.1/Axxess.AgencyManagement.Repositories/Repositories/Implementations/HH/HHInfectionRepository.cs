﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;

    public class HHInfectionRepository : InfectionAbstract
    {
        public HHInfectionRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            this.connectionStringName = connectionStringName;
        }
    }
}
