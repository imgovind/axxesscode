﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;

    public class HHBillingRepository : BillingAbstract
    {
        public HHBillingRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }


        public bool AddRap(Rap rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.Modified = DateTime.Now;
                database.Add<Rap>(rap);
                result = true;
            }
            return result;
        }

        public bool AddRap(Patient patient,Profile profile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            var result = false;
            try
            {
                var rap = CreateRap(patient,profile, episode, insuranceId, physician);
                if (this.AddRap(rap))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddRap(Patient patientWithProfile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            var result = false;
            try
            {
                var rap = CreateRap(patientWithProfile, patientWithProfile.Profile, episode, insuranceId, physician);
                if (this.AddRap(rap))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public Rap GetRapOnly(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == claimId));
        }

        public Rap GetRapOnly(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool AddFinal(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            bool result = false;
            try
            {
                final.Created = DateTime.Now;
                final.Modified = DateTime.Now;
                database.Add<Final>(final);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddFinal(Patient patient,Profile profile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            var result = false;
            try
            {
                var final = CreateFinal(patient,profile, episode, insuranceId, physician);
                if (this.AddFinal(final))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddFinal(Patient patientWithProfile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            var result = false;
            try
            {
                var final = CreateFinal(patientWithProfile,patientWithProfile.Profile, episode, insuranceId, physician);
                if (this.AddFinal(final))
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

//        public Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId)
//        {
//            Final final = null;
//            var script = string.Format(@"
//                SELECT
//	                fc.`Id`						as `Id`,                    fc.`AgencyId`				as `AgencyId`,
//	                fc.`PatientId`				as `PatientId`,             fc.`EpisodeId`				as `EpisodeId`,
//	                fc.`PatientIdNumber`		as `PatientIdNumber`,       pe.`StartDate` 	            as `EpisodeStartDate`,
//	                pe.`EndDate`		        as `EpisodeEndDate`,        fc.`IsOasisComplete`		as `IsOasisComplete`,
//	                fc.`IsFirstBillableVisit`	as `IsFirstBillableVisit`,  fc.`FirstBillableVisitDate` as `FirstBillableVisitDate`,
//	                fc.`Remark`					as `Remark`,                fc.`MedicareNumber`			as `MedicareNumber`,
//	                fc.`FirstName`				as `FirstName`,             fc.`LastName`				as `LastName`,
//	                fc.`DOB`					as `DOB`,                   fc.`Gender`					as `Gender`,
//	                fc.`PaymentDate`			as `PaymentDate`,           fc.`AddressLine1`			as `AddressLine1`,
//	                fc.`AddressLine2`			as `AddressLine2`,          fc.`AddressCity`			as `AddressCity`,
//	                fc.`AddressStateCode`		as `AddressStateCode`,      fc.`AddressZipCode`			as `AddressZipCode`,
//	                fc.`StartofCareDate`		as `StartofCareDate`,       fc.`PhysicianNPI`			as `PhysicianNPI`,
//	                fc.`PhysicianFirstName`		as `PhysicianFirstName`,    fc.`PhysicianLastName`		as `PhysicianLastName`,
//	                fc.`DiagnosisCode`			as `DiagnosisCode`,         fc.`HippsCode`				as `HippsCode`,
//	                fc.`ClaimKey`				as `ClaimKey`,              fc.`AreOrdersComplete`		as `AreOrdersComplete`,
//	                fc.`AreVisitsComplete`		as `AreVisitsComplete`,     fc.`IsGenerated`			as `IsGenerated`,
//	                fc.`Modified`				as `Modified`,              fc.`Created`				as `Created`,
//	                fc.`VerifiedVisits`			as `VerifiedVisits`,        fc.`Supply`					as `Supply`,
//	                fc.`SupplyTotal`			as `SupplyTotal`,           fc.`Payment`				as `Payment`,
//	                fc.`PrimaryInsuranceId`		as `PrimaryInsuranceId`,    fc.`IsSupplyVerified`		as `IsSupplyVerified`,
//	                fc.`IsFinalInfoVerified`	as `IsFinalInfoVerified`,   fc.`IsVisitVerified`		as `IsVisitVerified`,
//	                fc.`IsRapGenerated`			as `IsRapGenerated`,        fc.`Status`					as `Status`,
//	                fc.`Type`					as `Type`,                  fc.`AdmissionSource`		as `AdmissionSource`,
//	                fc.`PatientStatus`			as `PatientStatus`,         fc.`ClaimDate`				as `ClaimDate`,
//	                fc.`ProspectivePay`			as `ProspectivePay`,        fc.`AssessmentType`			as `AssessmentType`,
//	                fc.`Comment`				as `Comment`,               fc.`DischargeDate`			as `DischargeDate`,
//	                fc.`HealthPlanId`			as `HealthPlanId`,          fc.`GroupName`	            as `GroupName`,
//                    fc.`GroupId`			    as `GroupId`,               fc.`AuthorizationNumber`	as `AuthorizationNumber`,
//                    fc.`AuthorizationNumber2`   as `AuthorizationNumber2`,  fc.`AuthorizationNumber3`	as `AuthorizationNumber3`,
//                    fc.`Ub04Locator81cca`	    as `Ub04Locator81cca`,       
//	                fc.`ConditionCodes`			as `ConditionCodes`,        fc.`UB4PatientStatus`	    as `UB4PatientStatus`
//	            FROM finals fc INNER JOIN patientepisodes pe ON  fc.`EpisodeId` = pe.Id
//	            WHERE fc.`AgencyId` = @agencyId AND fc.`PatientId` = @patientId AND fc.`EpisodeId` = @episodeId;");
//            using (var cmd = new FluentCommand<Final>(script))
//                final = cmd.SetConnection(connectionStringName)
//                    .AddGuid("agencyid", agencyId)
//                    .AddGuid("patientId", patientId)
//                    .AddGuid("episodeId", episodeId)
//                    .SetMap(r => new Final
//                    {
//                        Id = r.GetGuid("Id"),
//                        AgencyId = r.GetGuid("AgencyId"),
//                        PatientId = r.GetGuid("PatientId"),
//                        EpisodeId = r.GetGuid("EpisodeId"),
//                        PatientIdNumber = r.GetStringNullable("PatientIdNumber"),
//                        EpisodeStartDate = r.GetDateTime("EpisodeStartDate"),
//                        EpisodeEndDate = r.GetDateTime("EpisodeEndDate"),
//                        IsOasisComplete = r.GetBoolean("IsOasisComplete"),
//                        IsFirstBillableVisit = r.GetBoolean("IsFirstBillableVisit"),
//                        FirstBillableVisitDate = r.GetDateTime("FirstBillableVisitDate"),
//                        Remark = r.GetStringNullable("Remark"),
//                        MedicareNumber = r.GetStringNullable("MedicareNumber"),
//                        FirstName = r.GetStringNullable("FirstName"),
//                        LastName = r.GetStringNullable("LastName"),
//                        DOB = r.GetDateTime("DOB"),
//                        Gender = r.GetStringNullable("Gender"),
//                        PaymentDate = r.GetDateTime("PaymentDate"),
//                        AddressLine1 = r.GetStringNullable("AddressLine1"),
//                        AddressLine2 = r.GetStringNullable("AddressLine2"),
//                        AddressCity = r.GetStringNullable("AddressCity"),
//                        AddressStateCode = r.GetStringNullable("AddressStateCode"),
//                        AddressZipCode = r.GetStringNullable("AddressZipCode"),
//                        StartofCareDate = r.GetDateTime("StartofCareDate"),
//                        PhysicianNPI = r.GetStringNullable("PhysicianNPI"),
//                        PhysicianFirstName = r.GetStringNullable("PhysicianFirstName"),
//                        PhysicianLastName = r.GetStringNullable("PhysicianLastName"),
//                        DiagnosisCode = r.GetStringNullable("DiagnosisCode"),
//                        HippsCode = r.GetStringNullable("HippsCode"),
//                        ClaimKey = r.GetStringNullable("ClaimKey"),
//                        AreOrdersComplete = r.GetBoolean("AreOrdersComplete"),
//                        AreVisitsComplete = r.GetBoolean("AreVisitsComplete"),
//                        IsGenerated = r.GetBoolean("IsGenerated"),
//                        Modified = r.GetDateTime("Modified"),
//                        Created = r.GetDateTime("Created"),
//                        VerifiedVisits = r.GetStringNullable("VerifiedVisits"),
//                        Supply = r.GetStringNullable("Supply"),
//                        SupplyTotal = (double)r.GetDecimalNullable("SupplyTotal"),
//                        Payment = (double)r.GetDecimalNullable("Payment"),
//                        PrimaryInsuranceId = (int)r.GetIntNullable("PrimaryInsuranceId"),
//                        IsSupplyVerified = r.GetBoolean("IsSupplyVerified"),
//                        IsFinalInfoVerified = r.GetBoolean("IsFinalInfoVerified"),
//                        IsVisitVerified = r.GetBoolean("IsVisitVerified"),
//                        IsRapGenerated = r.GetBoolean("IsRapGenerated"),
//                        Status = (int)r.GetIntNullable("Status"),
//                        Type = (int)r.GetIntNullable("Type"),
//                        AdmissionSource = r.GetStringNullable("AdmissionSource"),
//                        PatientStatus = (int)r.GetIntNullable("PatientStatus"),
//                        ClaimDate = r.GetDateTime("ClaimDate"),
//                        ProspectivePay = (double)r.GetDecimalNullable("ProspectivePay"),
//                        AssessmentType = r.GetStringNullable("AssessmentType"),
//                        Comment = r.GetStringNullable("Comment"),
//                        DischargeDate = r.GetDateTime("DischargeDate"),
//                        HealthPlanId = r.GetStringNullable("HealthPlanId"),
//                        AuthorizationNumber = r.GetStringNullable("AuthorizationNumber"),
//                        ConditionCodes = r.GetStringNullable("ConditionCodes"),
//                        UB4PatientStatus = r.GetStringNullable("UB4PatientStatus"),
//                        Ub04Locator81cca = r.GetStringNullable("Ub04Locator81cca"),
//                        GroupName = r.GetStringNullable("GroupName"),
//                        GroupId = r.GetStringNullable("GroupId"),
//                        AuthorizationNumber2 = r.GetStringNullable("AuthorizationNumber2"),
//                        AuthorizationNumber3 = r.GetStringNullable("AuthorizationNumber3")
//                    }).AsSingle();
//            return final;
//        }

//        public Final GetFinal(Guid agencyId, Guid claimId)
//        {
//            Final final = null;
//            var script = string.Format(@"
//                SELECT
//	                fc.`Id`						as `Id`,                    fc.`AgencyId`				as `AgencyId`,
//	                fc.`PatientId`				as `PatientId`,             fc.`EpisodeId`				as `EpisodeId`,
//	                fc.`PatientIdNumber`		as `PatientIdNumber`,       pe.`StartDate`          	as `EpisodeStartDate`,
//	                pe.`EndDate`		        as `EpisodeEndDate`,        fc.`IsOasisComplete`		as `IsOasisComplete`,
//	                fc.`IsFirstBillableVisit`	as `IsFirstBillableVisit`,  fc.`FirstBillableVisitDate` as `FirstBillableVisitDate`,
//	                fc.`Remark`					as `Remark`,                fc.`MedicareNumber`			as `MedicareNumber`,
//	                fc.`FirstName`				as `FirstName`,             fc.`LastName`				as `LastName`,
//	                fc.`DOB`					as `DOB`,                   fc.`Gender`					as `Gender`,
//	                fc.`PaymentDate`			as `PaymentDate`,           fc.`AddressLine1`			as `AddressLine1`,
//	                fc.`AddressLine2`			as `AddressLine2`,          fc.`AddressCity`			as `AddressCity`,
//	                fc.`AddressStateCode`		as `AddressStateCode`,      fc.`AddressZipCode`			as `AddressZipCode`,
//	                fc.`StartofCareDate`		as `StartofCareDate`,       fc.`PhysicianNPI`			as `PhysicianNPI`,
//	                fc.`PhysicianFirstName`		as `PhysicianFirstName`,    fc.`PhysicianLastName`		as `PhysicianLastName`,
//	                fc.`DiagnosisCode`			as `DiagnosisCode`,         fc.`HippsCode`				as `HippsCode`,
//	                fc.`ClaimKey`				as `ClaimKey`,              fc.`AreOrdersComplete`		as `AreOrdersComplete`,
//	                fc.`AreVisitsComplete`		as `AreVisitsComplete`,     fc.`IsGenerated`			as `IsGenerated`,
//	                fc.`Modified`				as `Modified`,              fc.`Created`				as `Created`,
//	                fc.`VerifiedVisits`			as `VerifiedVisits`,        fc.`Supply`					as `Supply`,
//	                fc.`SupplyTotal`			as `SupplyTotal`,           fc.`Payment`				as `Payment`,
//	                fc.`PrimaryInsuranceId`		as `PrimaryInsuranceId`,    fc.`IsSupplyVerified`		as `IsSupplyVerified`,
//	                fc.`IsFinalInfoVerified`	as `IsFinalInfoVerified`,   fc.`IsVisitVerified`		as `IsVisitVerified`,
//	                fc.`IsRapGenerated`			as `IsRapGenerated`,        fc.`Status`					as `Status`,
//	                fc.`Type`					as `Type`,                  fc.`AdmissionSource`		as `AdmissionSource`,
//	                fc.`PatientStatus`			as `PatientStatus`,         fc.`ClaimDate`				as `ClaimDate`,
//	                fc.`ProspectivePay`			as `ProspectivePay`,        fc.`AssessmentType`			as `AssessmentType`,
//	                fc.`Comment`				as `Comment`,               fc.`DischargeDate`			as `DischargeDate`,
//	                fc.`HealthPlanId`			as `HealthPlanId`,          fc.`AuthorizationNumber`	as `AuthorizationNumber`,
//	                fc.`ConditionCodes`			as `ConditionCodes`,        fc.`UB4PatientStatus`       as `UB4PatientStatus`  ,
//                    fc.`Ub04Locator81cca`       as `Ub04Locator81cca`,      fc.`Insurance`              as `Insurance`  
//	            FROM finals fc INNER JOIN patientepisodes pe ON fc.`EpisodeId` = pe.Id
//	            WHERE fc.`AgencyId` = @agencyId AND fc.`Id` = @claimId;");
//            using (var cmd = new FluentCommand<Final>(script))
//                final = cmd.SetConnection(connectionStringName)
//                    .AddGuid("agencyid", agencyId)
//                    .AddGuid("claimId", claimId)
//                    .SetMap(r => new Final
//                    {
//                        Id = r.GetGuid("Id"),
//                        AgencyId = r.GetGuid("AgencyId"),
//                        PatientId = r.GetGuid("PatientId"),
//                        EpisodeId = r.GetGuid("EpisodeId"),
//                        PatientIdNumber = r.GetStringNullable("PatientIdNumber"),
//                        EpisodeStartDate = r.GetDateTime("EpisodeStartDate"),
//                        EpisodeEndDate = r.GetDateTime("EpisodeEndDate"),
//                        IsOasisComplete = r.GetBoolean("IsOasisComplete"),
//                        IsFirstBillableVisit = r.GetBoolean("IsFirstBillableVisit"),
//                        FirstBillableVisitDate = r.GetDateTime("FirstBillableVisitDate"),
//                        Remark = r.GetStringNullable("Remark"),
//                        MedicareNumber = r.GetStringNullable("MedicareNumber"),
//                        FirstName = r.GetStringNullable("FirstName"),
//                        LastName = r.GetStringNullable("LastName"),
//                        DOB = r.GetDateTime("DOB"),
//                        Gender = r.GetStringNullable("Gender"),
//                        PaymentDate = r.GetDateTime("PaymentDate"),
//                        AddressLine1 = r.GetStringNullable("AddressLine1"),
//                        AddressLine2 = r.GetStringNullable("AddressLine2"),
//                        AddressCity = r.GetStringNullable("AddressCity"),
//                        AddressStateCode = r.GetStringNullable("AddressStateCode"),
//                        AddressZipCode = r.GetStringNullable("AddressZipCode"),
//                        StartofCareDate = r.GetDateTime("StartofCareDate"),
//                        PhysicianNPI = r.GetStringNullable("PhysicianNPI"),
//                        PhysicianFirstName = r.GetStringNullable("PhysicianFirstName"),
//                        PhysicianLastName = r.GetStringNullable("PhysicianLastName"),
//                        DiagnosisCode = r.GetStringNullable("DiagnosisCode"),
//                        HippsCode = r.GetStringNullable("HippsCode"),
//                        ClaimKey = r.GetStringNullable("ClaimKey"),
//                        AreOrdersComplete = r.GetBoolean("AreOrdersComplete"),
//                        AreVisitsComplete = r.GetBoolean("AreVisitsComplete"),
//                        IsGenerated = r.GetBoolean("IsGenerated"),
//                        Modified = r.GetDateTime("Modified"),
//                        Created = r.GetDateTime("Created"),
//                        VerifiedVisits = r.GetStringNullable("VerifiedVisits"),
//                        Supply = r.GetStringNullable("Supply"),
//                        SupplyTotal = (double)r.GetDecimalNullable("SupplyTotal"),
//                        Payment = (double)r.GetDecimalNullable("Payment"),
//                        PrimaryInsuranceId = (int)r.GetIntNullable("PrimaryInsuranceId"),
//                        IsSupplyVerified = r.GetBoolean("IsSupplyVerified"),
//                        IsFinalInfoVerified = r.GetBoolean("IsFinalInfoVerified"),
//                        IsVisitVerified = r.GetBoolean("IsVisitVerified"),
//                        IsRapGenerated = r.GetBoolean("IsRapGenerated"),
//                        Status = (int)r.GetIntNullable("Status"),
//                        Type = (int)r.GetIntNullable("Type"),
//                        AdmissionSource = r.GetStringNullable("AdmissionSource"),
//                        PatientStatus = (int)r.GetIntNullable("PatientStatus"),
//                        ClaimDate = r.GetDateTime("ClaimDate"),
//                        ProspectivePay = (double)r.GetDecimalNullable("ProspectivePay"),
//                        AssessmentType = r.GetStringNullable("AssessmentType"),
//                        Comment = r.GetStringNullable("Comment"),
//                        DischargeDate = r.GetDateTime("DischargeDate"),
//                        HealthPlanId = r.GetStringNullable("HealthPlanId"),
//                        AuthorizationNumber = r.GetStringNullable("AuthorizationNumber"),
//                        ConditionCodes = r.GetStringNullable("ConditionCodes"),
//                        UB4PatientStatus = r.GetStringNullable("UB4PatientStatus"),
//                        Ub04Locator81cca = r.GetStringNullable("Ub04Locator81cca"),
//                        Insurance = r.GetStringNullable("Insurance")
//                    }).AsSingle();
//            return final;
//        }

        public Final GetFinalOnly(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Final>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public Final GetFinalOnly(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<Final>(r => (r.AgencyId == agencyId && r.Id == claimId));
        }

        public Final GetFinalSnapShotInfo(Guid agencyId, Guid patientId, Guid claimId)
        {
            Final final = null;
            var script = string.Format(@"
                SELECT
	                fc.`Id`						as `Id`,                    fc.`AgencyId`				as `AgencyId`,
	                fc.`PatientId`				as `PatientId`,             fc.`EpisodeId`				as `EpisodeId`,
	                fc.`PatientIdNumber`		as `PatientIdNumber`,       fc.`MedicareNumber`			as `MedicareNumber`,
	                fc.`FirstName`				as `FirstName`,             fc.`LastName`				as `LastName`,
	                fc.`PaymentDate`			as `PaymentDate`,           fc.`HippsCode`				as `HippsCode`,
	                fc.`ClaimKey`				as `ClaimKey`,              fc.`Modified`				as `Modified`,              
                    fc.`Created`				as `Created`,               fc.`SupplyTotal`			as `SupplyTotal`,           
                    fc.`Payment`				as `Payment`,               fc.`PrimaryInsuranceId`		as `PrimaryInsuranceId`,    
	                fc.`Status`					as `Status`,                fc.`Type`					as `Type`,
	                fc.`PatientStatus`			as `PatientStatus`,         fc.`ClaimDate`				as `ClaimDate`,
	                fc.`ProspectivePay`			as `ProspectivePay`,        fc.`AssessmentType`			as `AssessmentType`,
	                fc.`DischargeDate`			as `DischargeDate`,         fc.`AdmissionSource`		as `AdmissionSource`,
                    fc.`EpisodeStartDate`       as `EpisodeStartDate`,      fc.`EpisodeEndDate`         as `EpisodeEndDate`
	            FROM finals fc 
	            WHERE fc.`AgencyId` = @agencyId AND fc.`PatientId` = @patientId AND fc.`Id` = @claimId;");
            using (var cmd = new FluentCommand<Final>(script))
                final = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("claimId", claimId)
                    .SetMap(r => new Final
                    {
                        Id = r.GetGuid("Id"),
                        AgencyId = r.GetGuid("AgencyId"),
                        PatientId = r.GetGuid("PatientId"),
                        EpisodeId = r.GetGuid("EpisodeId"),
                        PatientIdNumber = r.GetStringNullable("PatientIdNumber"),
                        MedicareNumber = r.GetStringNullable("MedicareNumber"),
                        FirstName = r.GetStringNullable("FirstName"),
                        LastName = r.GetStringNullable("LastName"),
                        PaymentDate = r.GetDateTime("PaymentDate"),
                        HippsCode = r.GetStringNullable("HippsCode"),
                        ClaimKey = r.GetStringNullable("ClaimKey"),
                        Modified = r.GetDateTime("Modified"),
                        Created = r.GetDateTime("Created"),
                        SupplyTotal = (double)(r.GetDecimalNullable("SupplyTotal") ?? 0),
                        Payment = (double)(r.GetDecimalNullable("Payment") ?? 0),
                        PrimaryInsuranceId = (int)r.GetIntNullable("PrimaryInsuranceId"),
                        Status = (int)r.GetIntNullable("Status"),
                        Type = (int)r.GetIntNullable("Type"),
                        AdmissionSource = r.GetStringNullable("AdmissionSource"),
                        PatientStatus = (int)r.GetIntNullable("PatientStatus"),
                        ClaimDate = r.GetDateTime("ClaimDate"),
                        ProspectivePay = (double)(r.GetDecimalNullable("ProspectivePay") ?? 0),
                        AssessmentType = r.GetStringNullable("AssessmentType"),
                        EpisodeStartDate = r.GetDateTime("EpisodeStartDate"),
                        EpisodeEndDate = r.GetDateTime("EpisodeEndDate")
                    }).AsSingle();
            return final;
        }

        public Final GetFinalSupplies(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(claimId, "claimId");
            Final final = null;
            string script = string.Format(@"SELECT 
                                                fc.Id, 
                                                fc.PatientId, 
                                                fc.EpisodeId, 
                                                fc.EpisodeStartDate, 
                                                fc.EpisodeEndDate,
                                                fc.Supply
                                                    FROM 
                                                        finals fc
                                                            WHERE
                                                            fc.AgencyId = @agencyid AND
                                                            fc.Id = @claimid");

            using (var cmd = new FluentCommand<Final>(script))
            {
                final = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("claimid", claimId)
                 .SetMap(reader => new Final
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                     EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                     Supply = reader.GetStringNullable("Supply")
                 })
                 .AsSingle();
            }
            return final;
        }

        public Final GetFinalSupplies(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Final final = null;
            string script = string.Format(@"SELECT 
                                                fc.Id, 
                                                fc.PatientId, 
                                                fc.EpisodeId, 
                                                fc.EpisodeStartDate, 
                                                fc.EpisodeEndDate,
                                                fc.Supply,
                                                fc.IsSupplyNotBillable
                                                    FROM 
                                                        finals fc
                                                            WHERE
                                                            fc.AgencyId = @agencyid AND
                                                            fc.PatientId = @patientid AND
                                                            fc.EpisodeId = @episodeid");

            using (var cmd = new FluentCommand<Final>(script))
            {
                final = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                 .AddGuid("episodeid", episodeId)
                 .SetMap(reader => new Final
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                     EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                     Supply = reader.GetStringNullable("Supply"),
                     IsSupplyNotBillable = reader.GetBoolean("IsSupplyNotBillable")
                 })
                 .AsSingle();
            }
            return final;
        }

        public bool UpdateFinalModel(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            if (final != null)
            {
                try
                {
                    final.Modified = DateTime.Now;
                    database.Update<Final>(final);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRapModel(Rap rap)
        {
            Check.Argument.IsNotNull(rap, "rap");
            if (rap != null)
            {
                try
                {
                    rap.Modified = DateTime.Now;
                    database.Update<Rap>(rap);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateFinalForDischarge( Guid agencyId,Guid id, DateTime dischargeDate)
        {
            var final = GetFinalOnly(agencyId, id);
            if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status) && dischargeDate.IsBetween(final.EpisodeStartDate, final.EpisodeEndDate))
            {
                try
                {
                    final.EpisodeEndDate = dischargeDate;
                    if (database.Update<Final>(final) > 0)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRapForDischarge(Guid agencyId, Guid id, DateTime dischargeDate)
        {
            var rap = GetRapOnly(agencyId, id);
            if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status) )
            {
                try
                {
                    rap.EpisodeEndDate = dischargeDate;
                    if (database.Update<Rap>(rap) > 0)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        //public bool UpdateFinalStatus(Final final)
        //{
        //    Check.Argument.IsNotNull(final, "final");
        //    var currentFinal = database.Single<Final>(r => (r.AgencyId == final.AgencyId && r.PatientId == final.PatientId && r.EpisodeId == final.EpisodeId));

        //    if (currentFinal != null && final != null)
        //    {
        //        try
        //        {
        //            currentFinal.PatientIdNumber = final.PatientIdNumber;
        //            currentFinal.EpisodeStartDate = final.EpisodeStartDate;
        //            currentFinal.EpisodeEndDate = final.EpisodeEndDate;
        //            currentFinal.IsOasisComplete = final.IsOasisComplete;
        //            currentFinal.IsFirstBillableVisit = final.IsFirstBillableVisit;
        //            currentFinal.FirstBillableVisitDate = final.FirstBillableVisitDate;
        //            currentFinal.Remark = final.Remark;
        //            currentFinal.MedicareNumber = final.MedicareNumber;
        //            currentFinal.FirstName = final.FirstName;
        //            currentFinal.LastName = final.LastName;
        //            currentFinal.DOB = final.DOB;
        //            currentFinal.Gender = final.Gender;
        //            currentFinal.AddressLine1 = final.AddressLine1;
        //            currentFinal.AddressLine2 = final.AddressLine2;
        //            currentFinal.AddressCity = final.AddressCity;
        //            currentFinal.AddressStateCode = final.AddressStateCode;
        //            currentFinal.AddressZipCode = final.AddressZipCode;
        //            currentFinal.StartofCareDate = final.StartofCareDate;
        //            currentFinal.PhysicianNPI = final.PhysicianNPI;
        //            currentFinal.PhysicianFirstName = final.PhysicianFirstName;
        //            currentFinal.PhysicianLastName = final.PhysicianLastName;
        //            currentFinal.DiagnosisCode = final.DiagnosisCode;
        //            currentFinal.HippsCode = final.HippsCode;
        //            currentFinal.ClaimKey = final.ClaimKey;
        //            currentFinal.AreOrdersComplete = final.AreOrdersComplete;
        //            currentFinal.AreVisitsComplete = final.AreVisitsComplete;
        //            currentFinal.Created = final.Created;
        //            currentFinal.VerifiedVisits = final.VerifiedVisits;
        //            currentFinal.PrimaryInsuranceId = final.PrimaryInsuranceId;
        //            currentFinal.Supply = final.Supply;
        //            currentFinal.SupplyTotal = final.SupplyTotal;
        //            currentFinal.IsSupplyVerified = final.IsSupplyVerified;
        //            currentFinal.IsFinalInfoVerified = final.IsFinalInfoVerified;
        //            currentFinal.IsVisitVerified = final.IsVisitVerified;
        //            currentFinal.AgencyId = final.AgencyId;
        //            currentFinal.IsRapGenerated = final.IsRapGenerated;
        //            currentFinal.Status = final.Status;
        //            currentFinal.IsGenerated = final.IsGenerated;
        //            currentFinal.ClaimDate = final.ClaimDate;
        //            currentFinal.Comment = final.Comment;
        //            currentFinal.Payment = final.Payment;
        //            currentFinal.PaymentDate = final.PaymentDate;
        //            currentFinal.Insurance = final.Insurance;
        //            database.Update<Final>(currentFinal);
        //            return true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        //public bool UpdateRapStatus(Rap rap)
        //{
        //    Check.Argument.IsNotNull(rap, "rap");
        //    var currentRap = database.Single<Rap>(r => (r.AgencyId == rap.AgencyId && r.PatientId == rap.PatientId && r.EpisodeId == rap.EpisodeId));
        //    if (currentRap != null && rap != null)
        //    {
        //        try
        //        {
        //            currentRap.PatientIdNumber = rap.PatientIdNumber;
        //            currentRap.EpisodeStartDate = rap.EpisodeStartDate;
        //            currentRap.EpisodeEndDate = rap.EpisodeEndDate;
        //            currentRap.IsOasisComplete = rap.IsOasisComplete;
        //            currentRap.IsFirstBillableVisit = rap.IsFirstBillableVisit;
        //            currentRap.FirstBillableVisitDate = rap.FirstBillableVisitDate;
        //            currentRap.IsGenerated = rap.IsGenerated;
        //            currentRap.IsVerified = rap.IsVerified;
        //            currentRap.Modified = rap.Modified;
        //            currentRap.Remark = rap.Remark;
        //            currentRap.MedicareNumber = rap.MedicareNumber;
        //            currentRap.FirstName = rap.FirstName;
        //            currentRap.LastName = rap.LastName;
        //            currentRap.DOB = rap.DOB;
        //            currentRap.Gender = rap.Gender;
        //            currentRap.AddressLine1 = rap.AddressLine1;
        //            currentRap.AddressLine2 = rap.AddressLine2;
        //            currentRap.AddressCity = rap.AddressCity;
        //            currentRap.AddressStateCode = rap.AddressStateCode;
        //            currentRap.AddressZipCode = rap.AddressZipCode;
        //            currentRap.StartofCareDate = rap.StartofCareDate;
        //            currentRap.PhysicianNPI = rap.PhysicianNPI;
        //            currentRap.PhysicianFirstName = rap.PhysicianFirstName;
        //            currentRap.PhysicianLastName = rap.PhysicianLastName;
        //            currentRap.DiagnosisCode = rap.DiagnosisCode;
        //            currentRap.HippsCode = rap.HippsCode;
        //            currentRap.ClaimKey = rap.ClaimKey;
        //            currentRap.AreOrdersComplete = rap.AreOrdersComplete;
        //            currentRap.Created = rap.Created;
        //            currentRap.PrimaryInsuranceId = rap.PrimaryInsuranceId;
        //            currentRap.Status = rap.Status;
        //            currentRap.AgencyId = rap.AgencyId;
        //            currentRap.ClaimDate = rap.ClaimDate;
        //            currentRap.ProspectivePay = rap.ProspectivePay;
        //            currentRap.AssessmentType = rap.AssessmentType;
        //            currentRap.Comment = rap.Comment;
        //            currentRap.Payment = rap.Payment;
        //            currentRap.PaymentDate = rap.PaymentDate;
        //            currentRap.Insurance = rap.Insurance;
        //            database.Update<Rap>(currentRap);
        //            return true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        //public bool VerifyRap(Guid agencyId, Rap rap)
        //{
        //    Check.Argument.IsNotNull(rap, "rap");
        //    var currentClaim = database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == rap.Id));
        //    if (currentClaim != null && rap != null)
        //    {
        //        try
        //        {
        //            currentClaim.FirstName = rap.FirstName;
        //            currentClaim.LastName = rap.LastName;
        //            currentClaim.MedicareNumber = rap.MedicareNumber;
        //            if (rap.FirstBillableVisitDateFormat.IsNotNullOrEmpty() && rap.FirstBillableVisitDateFormat.IsValidDate())
        //            {
        //                currentClaim.FirstBillableVisitDate = rap.FirstBillableVisitDateFormat.ToDateTime();
        //            }
        //            currentClaim.EpisodeStartDate = rap.EpisodeStartDate;
        //            currentClaim.StartofCareDate = rap.StartofCareDate;
        //            currentClaim.PatientIdNumber = rap.PatientIdNumber;
        //            currentClaim.Gender = rap.Gender;
        //            currentClaim.DOB = rap.DOB;
        //            currentClaim.AddressCity = rap.AddressLine1;
        //            currentClaim.AddressLine2 = rap.AddressLine2;
        //            currentClaim.AddressCity = rap.AddressCity;
        //            currentClaim.AddressStateCode = rap.AddressStateCode;
        //            currentClaim.AddressZipCode = rap.AddressZipCode;
        //            currentClaim.PhysicianLastName = rap.PhysicianLastName;
        //            currentClaim.PhysicianNPI = rap.PhysicianNPI;
        //            currentClaim.PhysicianFirstName = rap.PhysicianFirstName;
        //            currentClaim.DiagnosisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", rap.Primary, rap.Second, rap.Third, rap.Fourth, rap.Fifth, rap.Sixth);
        //            currentClaim.ConditionCodes = string.Format("<ConditionCodes><ConditionCode18>{0}</ConditionCode18><ConditionCode19>{1}</ConditionCode19><ConditionCode20>{2}</ConditionCode20><ConditionCode21>{3}</ConditionCode21><ConditionCode22>{4}</ConditionCode22><ConditionCode23>{5}</ConditionCode23><ConditionCode24>{6}</ConditionCode24><ConditionCode25>{7}</ConditionCode25><ConditionCode26>{8}</ConditionCode26><ConditionCode27>{9}</ConditionCode27><ConditionCode28>{10}</ConditionCode28></ConditionCodes>", rap.ConditionCode18, rap.ConditionCode19, rap.ConditionCode20, rap.ConditionCode21, rap.ConditionCode22, rap.ConditionCode23, rap.ConditionCode24, rap.ConditionCode25, rap.ConditionCode26, rap.ConditionCode27, rap.ConditionCode28);
        //            currentClaim.HippsCode = rap.HippsCode;
        //            currentClaim.ClaimKey = rap.ClaimKey;
        //            currentClaim.Remark = rap.Remark;
        //            currentClaim.ProspectivePay = rap.ProspectivePay;
        //            currentClaim.AssessmentType = rap.AssessmentType;
        //            currentClaim.AdmissionSource = rap.AdmissionSource;
        //            currentClaim.PatientStatus = rap.PatientStatus;
        //            currentClaim.UB4PatientStatus = rap.UB4PatientStatus;
        //            if (rap.UB4PatientStatus != ((int)UB4PatientStatus.StillPatient).ToString())
        //            {
        //                currentClaim.DischargeDate = rap.DischargeDate;
        //            }
        //            currentClaim.PrimaryInsuranceId = rap.PrimaryInsuranceId;
        //            if (rap.PrimaryInsuranceId >= 1000)
        //            {
        //                currentClaim.HealthPlanId = rap.HealthPlanId;
        //                currentClaim.GroupName = rap.GroupName;
        //                currentClaim.GroupId = rap.GroupId;
        //                currentClaim.Authorization = rap.Authorization;
        //                currentClaim.AuthorizationNumber = rap.AuthorizationNumber;
        //                currentClaim.AuthorizationNumber2 = rap.AuthorizationNumber2;
        //                currentClaim.AuthorizationNumber3 = rap.AuthorizationNumber3;
        //            }
        //            currentClaim.Ub04Locator81cca = rap.Ub04Locator81cca;
        //            currentClaim.Ub04Locator39 = rap.Ub04Locator39;
        //            currentClaim.Insurance = rap.Insurance;
        //            currentClaim.IsVerified = true;
        //            currentClaim.Modified = DateTime.Now;
        //            database.Update<Rap>(currentClaim);
        //            return true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        //public bool VerifyInfo(Guid agencyId, Final final)
        //{
        //    Check.Argument.IsNotNull(final, "final");
         
        //    if (currentClaim != null && final != null)
        //    {
        //        try
        //        {
                   
        //            currentClaim.Modified = DateTime.Now;
        //            database.Update<Final>(currentClaim);
        //            return true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        public List<Claim> GetRapsByIds(Guid agencyId, List<Guid> rapIds)
        {
            var list = new List<Claim>();
            if (rapIds != null && rapIds.Count > 0)
            {
                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                            rc.Id as Id, 
                            rc.EpisodeId as EpisodeId,
                            rc.FirstName as FirstName, 
                            rc.LastName as LastName, 
                            rc.PatientId as PatientId , 
                            rc.PatientIdNumber as PatientIdNumber ,
                            rc.MedicareNumber as MedicareNumber , 
                            rc.EpisodeStartDate as EpisodeStartDate,
                            rc.EpisodeEndDate as EpisodeEndDate ,
                            rc.ProspectivePay as ProspectivePay ,
                            pe.StartDate as StartDate,
                            pe.EndDate as EndDate 
                                FROM 
                                    raps rc
                                        INNER JOIN patientepisodes pe ON  rc.EpisodeId = pe.Id   
                                                WHERE
                                                    rc.AgencyId = @agencyid AND 
                                                    pe.IsActive = 1 AND 
                                                    pe.IsDischarged = 0  AND
                                                    rc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Claim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Claim
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EndDate"),
                         EpisodeStartDate = reader.GetDateTime("StartDate"),
                         ProspectivePay = reader.GetDouble("ProspectivePay", 0)
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<Claim> GetFinalsByIds(Guid agencyId, List<Guid> finalIds)
        {
            var list = new List<Claim>();
            if (finalIds != null && finalIds.Count > 0)
            {
                var ids = finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                    pe.StartDate as StartDate,
                    pe.EndDate as EndDate , 
                    fc.Id as Id, 
                    fc.EpisodeId as EpisodeId,
                    fc.FirstName as FirstName , 
                    fc.LastName as LastName, 
                    fc.PatientId as PatientId ,
                    fc.PatientIdNumber as PatientIdNumber ,
                    fc.MedicareNumber as MedicareNumber ,
                    fc.EpisodeStartDate as EpisodeStartDate,
                    fc.EpisodeEndDate as EpisodeEndDate , 
                    fc.ProspectivePay as ProspectivePay 
                        FROM
                            finals fc
                                INNER JOIN patientepisodes pe ON  fc.EpisodeId = pe.Id 
                                    WHERE 
                                        fc.AgencyId = @agencyid  AND
                                        pe.IsActive = 1 AND
                                        pe.IsDischarged = 0  AND
                                        fc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Claim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Claim
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EndDate"),
                         EpisodeStartDate = reader.GetDateTime("StartDate"),
                         ProspectivePay = reader.GetDouble("ProspectivePay", 0)
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<Rap> GetRapsToGenerateByIds(Guid agencyId, List<Guid> rapIds)
        {
            var list = new List<Rap>();
            if (rapIds != null && rapIds.Count > 0)
            {
                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT
                    rc.Id as Id ,
                    rc.AgencyId as AgencyId , 
                    rc.PatientId as PatientId ,
                    rc.EpisodeId as EpisodeId ,
                    rc.PatientIdNumber as PatientIdNumber,
                    rc.EpisodeStartDate as EpisodeStartDate, 
                    rc.EpisodeEndDate as EpisodeEndDate ,
                    rc.IsOasisComplete as IsOasisComplete,
                    rc.IsFirstBillableVisit as IsFirstBillableVisit ,
                    rc.FirstBillableVisitDate as FirstBillableVisitDate ,
                    rc.IsGenerated as IsGenerated ,
                    rc.IsVerified as IsVerified , 
                    rc.Modified as Modified , 
                    rc.Created as Created , 
                    rc.Remark as Remark, 
                    rc.PaymentDate as PaymentDate ,
                    rc.MedicareNumber as MedicareNumber ,
                    rc.FirstName as FirstName ,
                    rc.LastName as LastName , 
                    rc.DOB as DOB , 
                    rc.Gender as Gender , 
                    rc.Ub04Locator39 as Ub04Locator39,
                    rc.AddressLine1 as AddressLine1 , 
                    rc.AddressLine2 as AddressLine2 , 
                    rc.AddressCity as AddressCity ,
                    rc.AddressStateCode as AddressStateCode , 
                    rc.AddressZipCode as AddressZipCode , 
                    rc.StartofCareDate as StartofCareDate ,
                    rc.PhysicianNPI as PhysicianNPI ,
                    rc.PhysicianFirstName as PhysicianFirstName , 
                    rc.PhysicianLastName as PhysicianLastName ,
                    rc.DiagnosisCode as DiagnosisCode , 
                    rc.HippsCode as HippsCode , 
                    rc.ClaimKey as ClaimKey ,
                    rc.AreOrdersComplete as AreOrdersComplete ,
                    rc.PrimaryInsuranceId as PrimaryInsuranceId , 
                    rc.Status as Status , 
                    rc.AdmissionSource as AdmissionSource , 
                    rc.PatientStatus as PatientStatus ,
                    rc.ClaimDate as ClaimDate ,  
                    rc.Payment as Payment , 
                    rc.Type as Type ,
                    rc.ProspectivePay as ProspectivePay ,
                    rc.AssessmentType as AssessmentType ,
                    rc.Comment as Comment , 
                    rc.DischargeDate as DischargeDate ,
                    rc.HealthPlanId as HealthPlanId ,
                    rc.GroupName as GroupName ,
                    rc.GroupId as GroupId ,
                    rc.AuthorizationNumber as AuthorizationNumber, 
                    rc.AuthorizationNumber2 as AuthorizationNumber2,
                    rc.AuthorizationNumber3 as AuthorizationNumber3 ,
                    rc.ConditionCodes as ConditionCodes , 
                    rc.UB4PatientStatus as UB4PatientStatus,
                    rc.Ub04Locator81cca as Ub04Locator81cca  , 
                    pr.AgencyLocationId as AgencyLocationId 
                        FROM
                            raps rc INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                WHERE
                                    rc.AgencyId = @agencyid  AND
                                    rc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Rap>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Rap
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         IsVerified = reader.GetBoolean("IsVerified"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         Remark = reader.GetStringNullable("Remark"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetInt("Type", 0),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         Payment = reader.GetDouble("Payment", 0),
                         ProspectivePay = reader.GetDouble("ProspectivePay", 0),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         AuthorizationNumber2 = reader.GetStringNullable("AuthorizationNumber2"),
                         AuthorizationNumber3 = reader.GetStringNullable("AuthorizationNumber3"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus"),
                         Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                         Ub04Locator39 = reader.GetStringNullable("Ub04Locator39"),
                         AgencyLocationId = reader.GetGuid("AgencyLocationId")
                     }).AsList();
                }
            }
            return list;
        }

        public List<Final> GetFinalsToGenerateByIds(Guid agencyId, List<Guid> finalIds)
        {
            var list = new List<Final>();
            if (finalIds != null && finalIds.Count > 0)
            {
                var ids = finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                    fc.Id as Id , 
                    fc.AgencyId as AgencyId , 
                    fc.PatientId as PatientId , 
                    fc.EpisodeId as EpisodeId ,
                    fc.PatientIdNumber as PatientIdNumber,
                    fc.EpisodeStartDate as EpisodeStartDate,
                    fc.EpisodeEndDate as EpisodeEndDate ,
                    fc.IsOasisComplete as IsOasisComplete,
                    fc.IsFirstBillableVisit as IsFirstBillableVisit ,
                    fc.FirstBillableVisitDate as FirstBillableVisitDate , 
                    fc.Remark as Remark , 
                    fc.MedicareNumber as MedicareNumber , 
                    fc.FirstName as FirstName , 
                    fc.LastName as LastName , 
                    fc.DOB as DOB ,
                    fc.Gender as Gender ,
                    fc.PaymentDate as PaymentDate , 
                    fc.Ub04Locator39 as Ub04Locator39, 
                    fc.AddressLine1 as AddressLine1 ,
                    fc.AddressLine2 as AddressLine2 ,
                    fc.AddressCity as AddressCity ,
                    fc.AddressStateCode as AddressStateCode ,
                    fc.AddressZipCode as AddressZipCode , 
                    fc.StartofCareDate as StartofCareDate , 
                    fc.PhysicianNPI as PhysicianNPI , 
                    fc.PhysicianFirstName as PhysicianFirstName ,
                    fc.PhysicianLastName as PhysicianLastName ,
                    fc.DiagnosisCode as DiagnosisCode , 
                    fc.HippsCode as HippsCode , 
                    fc.ClaimKey as ClaimKey ,
                    fc.AreOrdersComplete as AreOrdersComplete ,
                    fc.AreVisitsComplete as AreVisitsComplete ,
                    fc.IsGenerated as IsGenerated ,
                    fc.Modified as Modified , 
                    fc.Created as Created , 
                    fc.VerifiedVisits as VerifiedVisits ,
                    fc.Supply as Supply , 
                    fc.SupplyTotal as SupplyTotal ,
                    fc.Payment as Payment , 
                    fc.PrimaryInsuranceId as PrimaryInsuranceId ,
                    fc.IsSupplyVerified as IsSupplyVerified ,
                    fc.IsFinalInfoVerified as IsFinalInfoVerified ,
                    fc.IsVisitVerified as IsVisitVerified , 
                    fc.IsRapGenerated as IsRapGenerated , 
                    fc.Type as Type ,
                    fc.Status as Status ,
                    fc.AdmissionSource as AdmissionSource , 
                    fc.Ub04Locator31 as Ub04Locator31,
                    fc.Ub04Locator32 as Ub04Locator32, 
                    fc.Ub04Locator33 as Ub04Locator33, 
                    fc.Ub04Locator34 as Ub04Locator34, 
                    fc.PatientStatus as PatientStatus ,
                    fc.ClaimDate as ClaimDate , 
                    fc.ProspectivePay as ProspectivePay ,
                    fc.AssessmentType as AssessmentType , 
                    fc.Comment as Comment , 
                    fc.DischargeDate as DischargeDate ,
                    fc.HealthPlanId as HealthPlanId ,
                    fc.GroupName as GroupName ,
                    fc.GroupId as GroupId , 
                    fc.AuthorizationNumber as AuthorizationNumber ,
                    fc.AuthorizationNumber2 as AuthorizationNumber2 , 
                    fc.AuthorizationNumber3 as AuthorizationNumber3 , 
                    fc.ConditionCodes as ConditionCodes  ,
                    fc.UB4PatientStatus as UB4PatientStatus, 
                    fc.Ub04Locator81cca as Ub04Locator81cca   ,
                    pr.AgencyLocationId as AgencyLocationId ,
                    fc.IsSupplyNotBillable as IsSupplyNotBillable 
                            FROM
                                finals fc INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id   
                                    WHERE
                                        fc.AgencyId = @agencyid  AND 
                                        fc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Final>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Final
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         Remark = reader.GetStringNullable("Remark"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         VerifiedVisits = reader.GetStringNullable("VerifiedVisits"),
                         Supply = reader.GetStringNullable("Supply"),
                         SupplyTotal = reader.GetDouble("SupplyTotal", 0),
                         Payment = reader.GetDouble("Payment", 0),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsFinalInfoVerified = reader.GetBoolean("IsFinalInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetInt("Type", 1),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         ProspectivePay = reader.GetDouble("ProspectivePay", 0),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         AuthorizationNumber2 = reader.GetStringNullable("AuthorizationNumber2"),
                         AuthorizationNumber3 = reader.GetStringNullable("AuthorizationNumber3"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus"),
                         Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                         Ub04Locator39 = reader.GetStringNullable("Ub04Locator39"),
                         Ub04Locator31 = reader.GetStringNullable("Ub04Locator31"),
                         Ub04Locator32 = reader.GetStringNullable("Ub04Locator32"),
                         Ub04Locator33 = reader.GetStringNullable("Ub04Locator33"),
                         Ub04Locator34 = reader.GetStringNullable("Ub04Locator34"),
                         AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                         IsSupplyNotBillable = reader.GetBoolean("IsSupplyNotBillable")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<ClaimHistoryLean> GetRapsHistory(Guid agencyId, Guid patientId, int insuranceId)
        {
            var script = string.Format(@"SELECT
                                            pe.StartDate as StartDate,
                                            pe.EndDate as EndDate , 
                                            rc.Id as Id,
                                            rc.EpisodeId as EpisodeId ,
                                            rc.Status as Status,
                                            rc.ClaimDate as ClaimDate,
                                            rc.PaymentDate as PaymentDate,
                                            rc.Payment as Payment ,
                                            rc.EpisodeStartDate as EpisodeStartDate, 
                                            rc.EpisodeEndDate as EpisodeEndDate ,
                                            rc.ProspectivePay as ProspectivePay ,
                                            EXISTS(select id from rapsnapshots rs where rs.AgencyId = rc.AgencyId AND rs.PatientId = rc.PatientId AND rs.Id = rc.Id) as HasDetails
                                              FROM 
                                                raps rc
                                                    INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                                        WHERE
                                                            rc.AgencyId = @agencyid AND
                                                            rc.PatientId = @patientId AND
                                                            pe.IsActive = 1 AND
                                                            pe.IsDischarged = 0 ");

            return new FluentCommand<ClaimHistoryLean>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimHistoryLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = (int)ClaimTypeSubCategory.RAP,
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                    HasDetails = reader.GetBoolean("HasDetails")
                    
                })
                .AsList();
        }

        public List<ClaimHistoryLean> GetFinalsHistory(Guid agencyId, Guid patientId, int insuranceId)
        {
            var script = string.Format(@"SELECT 
                                        pe.StartDate as StartDate,
                                        pe.EndDate as EndDate , 
                                        fc.Id as Id,
                                        fc.EpisodeId as EpisodeId ,
                                        fc.Status as Status,
                                        fc.ClaimDate as ClaimDate,
                                        fc.PaymentDate as PaymentDate,
                                        fc.Payment as Payment ,
                                        fc.EpisodeStartDate as EpisodeStartDate,
                                        fc.EpisodeEndDate as EpisodeEndDate ,
                                        fc.ProspectivePay as ProspectivePay ,
                                        EXISTS(select id from finalsnapshots fs where fc.Id = fs.Id) as HasDetails
                                            FROM 
                                                finals fc
                                                    INNER JOIN patientepisodes pe ON  fc.EpisodeId = pe.Id 
                                                        WHERE
                                                            fc.AgencyId = @agencyid AND 
                                                            fc.PatientId = @patientId AND
                                                            pe.IsActive = 1 AND 
                                                            pe.IsDischarged = 0 ");

            return new FluentCommand<ClaimHistoryLean>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimHistoryLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = (int)ClaimTypeSubCategory.Final,
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                    HasDetails = reader.GetBoolean("HasDetails")
                })
                .AsList();
        }

        public IList<TypeOfBill> GetOutstandingRaps(Guid agencyId, bool isLimit, int limit)
        {
            var limitScript = isLimit && limit > 0 ? string.Format(" Limit {0} ", limit) : string.Empty;
            var status = BillingStatusFactory.PPSStatus().Select(d => d.ToString()).ToArray().Join(",");
            var script = string.Format(@"SELECT 
                            pr.Id,
                            pe.StartDate,
                            pe.EndDate,
                            pr.FirstName,
                            pr.LastName,
                            pr.Status,
                            pr.PatientIdNumber  
                                FROM 
                                    raps rc
                                        INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                        INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                            WHERE 
                                                rc.AgencyId = @agencyid  AND 
                                                pr.Status IN (1,2)  AND
                                                pr.IsDeprecated = 0 AND
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND
                                                rc.Status IN ({0}) 
                                                    ORDER BY pe.EndDate ASC {1}",status, limitScript);

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "RAP",
                    Id = reader.GetGuid("Id"),
                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")
                })
                .AsList();
        }

        public List<TypeOfBill> GetRapsByStatus(Guid agencyId, Guid branchId, int status)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }

            var script = string.Format(@"SELECT
                            pe.StartDate, 
                            pe.EndDate,
                            pr.FirstName,
                            pr.LastName ,
                            pr.MiddleInitial ,
                            pr.PatientIdNumber 
                                FROM 
                                    raps rc
                                        INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                        INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id
                                            WHERE
                                                rc.AgencyId = @agencyid  AND 
                                                pr.Status IN(1,2 ) AND
                                                pr.IsDeprecated = 0 AND
                                                pe.IsActive = 1 AND 
                                                pe.IsDischarged = 0 AND 
                                                rc.Status = @status {0} 
                                                    ORDER BY pe.EndDate ASC", branchScript);

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("status", status)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "RAP",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")
                })
                .AsList();
        }

        public List<RapBill> GetOutstandingRapClaims(Guid agencyId, Guid branchId, int insuranceId, string insurnaceFilter)
        {
            var list = new List<RapBill>();
            var rapStatus = BillingStatusFactory.UnProcessedAndReopened().Select(s => s.ToString()).ToArray().Join(",");
            var script = string.Format(@"SELECT
                    pe.Id as EpisodeId,
                    pe.StartDate, 
                    pe.EndDate,
                    pr.AgencyLocationId as AgencyLocationId ,
                    pr.Id as PatientId, 
                    pr.FirstName, 
                    pr.LastName, 
                    pr.PatientIdNumber,
                    pr.MiddleInitial,
                    pr.CaseManagerId,
                    pr.UserId as ClinicianId,
                    rc.Id,
                    rc.Status, 
                    rc.IsFirstBillableVisit, 
                    rc.IsOasisComplete,
                    rc.IsVerified ,
                    rc.HippsCode as HippsCode , 
                    rc.ProspectivePay as ProspectivePay ,
                    rc.PrimaryInsuranceId as  PrimaryInsuranceId 
                        FROM
                            raps rc 
                                INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                    WHERE 
                                        pr.AgencyId = @agencyid AND
                                        pr.Status IN (1,2)  {0} {1}  AND
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND 
                                        pe.IsDischarged = 0 AND
                                        rc.Status IN ( {2} ) AND
                                        pe.StartDate <=  CURDATE() ",
                                            branchId.IsEmpty() ? string.Empty : " AND pr.AgencyLocationId = @branchId ", insurnaceFilter, rapStatus);

            using (var cmd = new FluentCommand<RapBill>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insuranceId)
                 .SetMap(reader => new RapBill
                 {
                     Id = reader.GetGuid("Id"),
                     AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                     Status = reader.GetInt("Status"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     IsVerified = reader.GetBoolean("IsVerified"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                     CaseManagerId = reader.GetGuidIncludeEmpty("CaseManagerId"),
                     ClinicianId = reader.GetGuidIncludeEmpty("ClinicianId"),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                     HippsCode = reader.GetStringNullable("HippsCode"),
                     ProspectivePay = (double)reader.GetDecimal("ProspectivePay"),
                     PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                 })
                 .AsList().OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ToList();
            }
            return list;
        }

        public List<TypeOfBill> GetFinalsByStatus(Guid agencyId, Guid branchId, int status)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }

            var script = string.Format(@"SELECT 
                    pe.StartDate, 
                    pe.EndDate,
                    pr.FirstName,
                    pr.LastName ,
                    pr.MiddleInitial,
                    pr.PatientIdNumber  
                        FROM
                            finals fc
                                INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id
                                INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id 
                                    WHERE
                                        fc.AgencyId = @agencyid AND
                                        pr.Status IN (1,2)  AND 
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND 
                                        pe.IsDischarged = 0 AND
                                        fc.Status = @status {0}
                                            ORDER BY pe.EndDate ASC", branchScript);

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("status", status)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "Final",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                })
                .AsList();
        }

        public IList<TypeOfBill> GetOutstandingFinals(Guid agencyId, bool isLimit, int limit)
        {
            var limitScript = isLimit && limit > 0 ? string.Format(" Limit {0} ", limit) : string.Empty;
            var status = BillingStatusFactory.PPSStatus().Select(d => d.ToString()).ToArray().Join(",");
            var script = string.Format(@"SELECT
                            pe.StartDate,
                            pe.EndDate,
                            pr.FirstName,
                            pr.LastName,
                            pr.Id,
                            pr.Status,
                            pr.MiddleInitial,
                            pr.PatientIdNumber  
                                FROM
                                    finals fc
                                        INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id
                                        INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id 
                                            WHERE 
                                                fc.AgencyId = @agencyid AND 
                                                pr.Status IN (1,2) AND 
                                                pr.IsDeprecated = 0 AND 
                                                pe.IsActive = 1 AND 
                                                pe.IsDischarged = 0 AND
                                                pe.EndDate <= Curdate() AND 
                                                fc.Status IN ({0})
                                                ORDER BY pe.EndDate ASC {1}",status, limitScript);

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "Final",
                    Id = reader.GetGuid("Id"),
                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                })
                .AsList();
        }

        public List<FinalBill> GetOutstandingFinalClaims(Guid agencyId, Guid branchId, int insuranceId, string insurnaceFilter)
        {
            var insurance = string.Empty;
            var list = new List<FinalBill>();
            var finalStatus = BillingStatusFactory.UnProcessedAndReopened().Select(s => s.ToString()).ToArray().Join(",");
            var script = string.Format(@"SELECT
                                    pe.Id as EpisodeId,
                                    pe.StartDate,
                                    pe.EndDate, 
                                    pr.AgencyLocationId as AgencyLocationId ,
                                    pr.Id as PatientId,
                                    pr.FirstName,
                                    pr.LastName, 
                                    pr.MiddleInitial, 
                                    pr.PatientIdNumber,
                                    pr.CaseManagerId,
                                    pr.UserId as ClinicianId,
                                    fc.Id, 
                                    fc.AreVisitsComplete,
                                    fc.IsRapGenerated,
                                    fc.IsFinalInfoVerified,
                                    fc.IsVisitVerified, 
                                    fc.Status,
                                    fc.AreOrdersComplete,
                                    fc.IsSupplyVerified ,
                                    fc.IsInsuranceVerified,
                                    fc.HippsCode as HippsCode ,
                                    fc.ProspectivePay as  ProspectivePay ,
                                    fc.PrimaryInsuranceId as PrimaryInsuranceId  
                                        FROM
                                            finals fc
                                                INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id 
                                                INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id 
                                                    WHERE 
                                                        fc.AgencyId = @agencyid  AND
                                                        pr.Status IN (1,2) {0} {1} AND
                                                        pr.IsDeprecated = 0 AND
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0 AND
                                                        pe.EndDate <= Curdate() AND 
                                                        fc.Status IN ( {2} ) 
                                                        ORDER BY pr.LastName ASC , pr.FirstName ASC",
                                                         branchId.IsEmpty() ? string.Empty : " AND pr.AgencyLocationId = @branchId ",
                                                         insurnaceFilter,
                                                         finalStatus);

            using (var cmd = new FluentCommand<FinalBill>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                  .AddGuid("agencyid", agencyId)
                  .AddGuid("branchId", branchId)
                  .AddInt("insuranceId", insuranceId)
                  .SetMap(reader => new FinalBill
                  {
                      Id = reader.GetGuid("Id"),
                      AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                      Status = reader.GetInt("Status"),
                      PatientId = reader.GetGuid("PatientId"),
                      EpisodeId = reader.GetGuid("EpisodeId"),
                      EpisodeEndDate = reader.GetDateTime("EndDate"),
                      EpisodeStartDate = reader.GetDateTime("StartDate"),
                      IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                      LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                      FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                      MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                      CaseManagerId = reader.GetGuidIncludeEmpty("CaseManagerId"),
                      ClinicianId = reader.GetGuidIncludeEmpty("ClinicianId"),
                      AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                      AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                      PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                      IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                      IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                      IsInfoVerified = reader.GetBoolean("IsFinalInfoVerified"),
                      IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                      HippsCode = reader.GetStringNullable("HippsCode"),
                      ProspectivePay = (double)reader.GetDecimal("ProspectivePay"),
                      PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                  }).AsList();
            }
            return list;
        }

        public IList<Claim> GetPotentialCliamAutoCancels(Guid agencyId, Guid branchId)
        {
            var finalStatus = BillingStatusFactory.PPSStatus().Select(d => d.ToString()).ToArray().Join(",");
            var script = string.Format(@"SELECT
                            fc.Id as Id , 
                            fc.PatientId as PatientId,
                            fc.EpisodeId as EpisodeId, 
                            pe.StartDate as StartDate ,
                            pe.EndDate as EndDate,
                            pr.MiddleInitial as MiddleInitial,
                            fc.FirstName as FirstName,
                            fc.LastName as LastName, 
                            fc.PatientIdNumber as PatientIdNumber,
                            fc.Status as Status , 
                            fc.ClaimDate as ClaimDate ,
                            fc.HippsCode as HippsCode ,
                            fc.ProspectivePay as  ProspectivePay  
                                FROM 
                                    finals fc
                                        INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id 
                                        INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id
                                        INNER JOIN raps rc ON fc.PatientId = rc.PatientId AND fc.EpisodeId = rc.EpisodeId 
                                            WHERE 
                                                rc.AgencyId = @agencyid {0}  AND 
                                                pr.Status IN(1,2)  AND 
                                                pr.IsDeprecated = 0 AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND
                                                DATEDIFF( Curdate() ,rc.ClaimDate ) >= 76 AND 
                                                fc.Status IN ({1}) AND 
                                                rc.Status IN ( 305, 315 , 320, 330 )
                                                     ORDER BY fc.LastName ASC ,fc.FirstName ASC ", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty, finalStatus);

            return new FluentCommand<Claim>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new FinalBill
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Status = reader.GetInt("Status"),
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")

                }).AsList();
        }

        public IList<Claim> GetPPSRapClaims(Guid agencyId, Guid branchId)
        {
            var script = string.Format(@"SELECT
                            pe.Id as EpisodeId,
                            pe.StartDate as StartDate ,
                            pe.EndDate as EndDate , 
                            pr.MiddleInitial as MiddleInitial,
                            rc.FirstName as FirstName , 
                            rc.LastName as LastName , 
                            rc.PatientIdNumber as PatientIdNumber ,
                            rc.PatientId as PatientId,
                            rc.Id as Id, 
                            rc.Status as Status, 
                            rc.HippsCode as HippsCode , 
                            rc.ProspectivePay as ProspectivePay  
                                FROM 
                                    raps rc
                                        INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                        INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                            WHERE
                                                rc.AgencyId = @agencyid {0} AND
                                                pr.Status IN (1,2)  AND 
                                                pr.IsDeprecated = 0 AND 
                                                pe.IsActive = 1 AND 
                                                pe.IsDischarged = 0 AND 
                                                rc.Status IN ({1}) 
                                                     ORDER BY pe.EndDate ASC",
                                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty, BillingStatusFactory.PPSStatus().Select(d => d.ToString()).ToArray().Join(","));

            return new FluentCommand<Claim>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new RapBill
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay"),
                    Status = reader.GetInt("Status")
                })
                .AsList();
        }

        public IList<Claim> GetPPSFinalClaims(Guid agencyId, Guid branchId)
        {
            var script = string.Format(@"SELECT 
                    pe.Id as EpisodeId,
                    pe.StartDate,
                    pe.EndDate,
                    fc.FirstName,
                    pr.MiddleInitial, 
                    fc.LastName,
                    fc.PatientIdNumber, 
                    fc.PatientId as PatientId, 
                    fc.Id as Id, 
                    fc.Status as Status , 
                    fc.HippsCode as HippsCode ,
                    fc.ProspectivePay as  ProspectivePay  
                        FROM
                            finals fc
                                INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id 
                                INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id
                                    WHERE 
                                        fc.AgencyId = @agencyid {0} AND 
                                        pr.Status IN( 1,2)  AND 
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND 
                                        pe.IsDischarged = 0 AND
                                        pe.EndDate <= Curdate() AND
                                        fc.Status IN ({1}) 
                                        ORDER BY pe.EndDate ASC",
                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty, BillingStatusFactory.PPSStatus().Select(d => d.ToString()).ToArray().Join(","));

            return new FluentCommand<Claim>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new FinalBill
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")
                }).AsList();
        }

        public bool RemoveMedicareClaim(Guid agencyId, Guid patientId, Guid episodeId , int type)
        {
            var result = false;
            try
            {
                if (type == (int)ClaimTypeSubCategory.RAP || type == (int)ClaimTypeSubCategory.Final)
                {
                    var count = 0;
                    var script = string.Format("DELETE FROM {0} WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id ", type == (int)ClaimTypeSubCategory.RAP ? "raps" : "finals");
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("id", episodeId).AsNonQuery();

                    }
                    result = count > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool IsEpisodeHasClaim(Guid agencyId, Guid patientId, Guid episodeId, int type)
        {
            try
            {
                var script = string.Format(@"SELECT 
                    1
                    FROM 
                        {0} 
                            WHERE 
                                Episodeid = @episodeid AND 
                                Agencyid = @agencyid AND 
                                Patientid = @patientid limit 1",
                    type == (int)ClaimTypeSubCategory.RAP ? "raps" : "finals");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                      .AddGuid("agencyid", agencyId)
                      .AddGuid("patientid", patientId)
                      .AddGuid("episodeid", episodeId)
                      .AsScalar();
                }
                return count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int MarkRapsAsSubmitted(Guid agencyId, List<Rap> raps)
        {
            return this.MarkRapsAsSubmitted(agencyId, raps.Select(r => r.Id).ToList());
        }

        public int MarkRapsAsSubmitted(Guid agencyId, List<Guid> ids)
        {

            int result = 0;
            if (ids != null && ids.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE raps rc SET rc.Status = @status , rc.IsGenerated = 1, rc.ClaimDate = @claimdate  WHERE rc.AgencyId = @agencyid AND rc.Status != @status AND rc.Id IN ( {0})", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("status", (int)BillingStatus.ClaimSubmitted)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    return count;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public int MarkFinalsAsSubmitted(Guid agencyId, List<Final> finals)
        {
            return this.MarkFinalsAsSubmitted(agencyId, finals.Select(f => f.Id).ToList());
        }


        public int MarkFinalsAsSubmitted(Guid agencyId, List<Guid> ids)
        {
            int result = 0;
            if (ids != null && ids.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE finals fc SET fc.Status = @status , fc.IsGenerated = 1, fc.ClaimDate = @claimdate  WHERE fc.AgencyId = @agencyid AND rc.Status != @status  AND fc.Id IN ( {0})", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("status", (int)BillingStatus.ClaimSubmitted)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    return count;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public bool MarkRapsAsGenerated(Guid agencyId, List<Rap> raps)
        {

            bool result = false;
            if (raps != null && raps.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE raps rc SET rc.IsGenerated = 1, rc.ClaimDate = @claimdate  WHERE rc.AgencyId = @agencyid AND rc.Id IN ( {0})", raps.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool MarkFinalsAsGenerated(Guid agencyId, List<Final> finals)
        {
            bool result = false;
            if (finals != null && finals.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE finals fc SET fc.IsGenerated = 1, fc.ClaimDate = @claimdate  WHERE fc.AgencyId = @agencyid AND fc.Id IN ( {0})", finals.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool MarkFinalsAsRapGenerated(Guid agencyId, List<Guid> ids)
        {
            bool result = false;
            if (ids != null && ids.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE finals fc SET fc.IsGenerated = 1  WHERE fc.AgencyId = @agencyid AND fc.Id IN ( {0})", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool AddRapSnapShots(List<Rap> raps, long batchId)
        {
            bool result = false;
            if (raps != null && raps.Count > 0)
            {
                try
                {
                    var rapSnapShots = new List<RapSnapShot>();
                    raps.ForEach(rap =>
                    {
                        rapSnapShots.Add( new RapSnapShot
                        {
                            MainId = string.Format("{0}-{1}", rap.Id, batchId),
                            Id = rap.Id,
                            BatchId = batchId,
                            AgencyId = rap.AgencyId,
                            PatientId = rap.PatientId,
                            EpisodeId = rap.EpisodeId,
                            PatientIdNumber = rap.PatientIdNumber,
                            EpisodeStartDate = rap.EpisodeStartDate,
                            EpisodeEndDate = rap.EpisodeEndDate,
                            IsOasisComplete = true,
                            IsFirstBillableVisit = true,
                            FirstBillableVisitDate = rap.FirstBillableVisitDate,
                            IsGenerated = true,
                            IsVerified = true,
                            Remark = rap.Remark,
                            MedicareNumber = rap.MedicareNumber,
                            FirstName = rap.FirstName,
                            LastName = rap.LastName,
                            DOB = rap.DOB,
                            Gender = rap.Gender,
                            AddressLine1 = rap.AddressLine1,
                            AddressLine2 = rap.AddressLine2,
                            AddressCity = rap.AddressCity,
                            AddressStateCode = rap.AddressStateCode,
                            AddressZipCode = rap.AddressZipCode,
                            StartofCareDate = rap.StartofCareDate,
                            PhysicianNPI = rap.PhysicianNPI,
                            PhysicianFirstName = rap.PhysicianFirstName,
                            PhysicianLastName = rap.PhysicianLastName,
                            DiagnosisCode = rap.DiagnosisCode,
                            HippsCode = rap.HippsCode,
                            ClaimKey = rap.ClaimKey,
                            AreOrdersComplete = true,
                            PrimaryInsuranceId = rap.PrimaryInsuranceId,
                            Status = (int)BillingStatus.ClaimSubmitted,
                            Type = rap.Type,
                            AdmissionSource = rap.AdmissionSource,
                            PatientStatus = rap.PatientStatus,

                            ClaimDate = DateTime.Now,
                            Payment = 00.00,
                            ProspectivePay = rap.ProspectivePay,
                            AssessmentType = rap.AssessmentType,
                            Comment = rap.Comment,
                            Reason = string.Empty,
                            Modified = DateTime.Now,
                            Created = DateTime.Now
                        });
                       

                    });
                    if (rapSnapShots.IsNotNullOrEmpty())
                    {
                        database.AddMany<RapSnapShot>(rapSnapShots);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                   
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddFinaSnapShots(List<Final> finals, long batchId)
        {
            bool result = false;
            if (finals != null && finals.Count > 0)
            {
                try
                {
                    var finalSnapShots=new List<FinalSnapShot>();
                    finals.ForEach(final =>
                    {
                        finalSnapShots.Add( new FinalSnapShot
                        {
                            MainId = string.Format("{0}-{1}", final.Id, batchId),
                            Id = final.Id,
                            BatchId = batchId,
                            AgencyId = final.AgencyId,
                            PatientId = final.PatientId,
                            EpisodeId = final.EpisodeId,
                            PatientIdNumber = final.PatientIdNumber,
                            EpisodeStartDate = final.EpisodeStartDate,
                            EpisodeEndDate = final.EpisodeEndDate,
                            IsOasisComplete = true,
                            IsFirstBillableVisit = true,
                            FirstBillableVisitDate = final.FirstBillableVisitDate,
                            IsGenerated = true,
                            AreVisitsComplete = true,
                            VerifiedVisits = final.VerifiedVisits,
                            Supply = final.Supply,
                            SupplyTotal = final.SupplyTotal,
                            IsSupplyVerified = true,
                            IsFinalInfoVerified = true,
                            IsVisitVerified = true,
                            IsRapGenerated = true,
                            Remark = final.Remark,
                            MedicareNumber = final.MedicareNumber,
                            FirstName = final.FirstName,
                            LastName = final.LastName,
                            DOB = final.DOB,
                            Gender = final.Gender,
                            AddressLine1 = final.AddressLine1,
                            AddressLine2 = final.AddressLine2,
                            AddressCity = final.AddressCity,
                            AddressStateCode = final.AddressStateCode,
                            AddressZipCode = final.AddressZipCode,
                            StartofCareDate = final.StartofCareDate,
                            PhysicianNPI = final.PhysicianNPI,
                            PhysicianFirstName = final.PhysicianFirstName,
                            PhysicianLastName = final.PhysicianLastName,
                            DiagnosisCode = final.DiagnosisCode,
                            HippsCode = final.HippsCode,
                            ClaimKey = final.ClaimKey,
                            AreOrdersComplete = true,
                            PrimaryInsuranceId = final.PrimaryInsuranceId,
                            Status = (int)BillingStatus.ClaimSubmitted,
                            Type = final.Type,
                            AdmissionSource = final.AdmissionSource,
                            PatientStatus = final.PatientStatus,
                            ClaimDate = DateTime.Now,
                            Payment = 00.00,
                            ProspectivePay = final.ProspectivePay,
                            AssessmentType = final.AssessmentType,
                            Comment = final.Comment,
                            Reason = string.Empty,
                            Modified = DateTime.Now,
                            Created = DateTime.Now
                        });
                    });
                    if (finalSnapShots.IsNotNullOrEmpty())
                    {
                        database.AddMany<FinalSnapShot>(finalSnapShots);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool DeleteRapSnapShots(long batchId)
        {
            bool result = false;
            try
            {
                database.DeleteMany<RapSnapShot>(r => r.BatchId == batchId);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeleteFinaSnapShots(long batchId)
        {
            bool result = false;
            try
            {
                database.DeleteMany<FinalSnapShot>(r => r.BatchId == batchId);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<RapSnapShot> GetRapSnapShots(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).ToList();
        }

        public List<FinalSnapShot> GetFinalSnapShots(Guid agencyId, Guid Id)
        {
            return database.Find<FinalSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).ToList();
        }

        public List<ClaimLean> GetSnapShots(Guid agencyId, string medicareNumber, string firstName, string lastName, DateTime startDate, string type)
        {
            var table = type.ToUpperCase() == "RAP" ? "rapsnapshots" : "finalsnapshots";
            var script = string.Format(@"SELECT
                                    mdcs.Id as Id,
                                    mdcs.PatientId as PatientId,
                                    mdcs.EpisodeId as EpisodeId,
                                    mdcs.BatchId as BatchId , 
                                    mdcs.FirstName as FirstName, 
                                    mdcs.LastName as LastName, 
                                    mdcs.PatientIdNumber as PatientIdNumber, 
                                    mdcs.MedicareNumber as MedicareNumber,
                                    mdcs.EpisodeStartDate as StartDate,
                                    mdcs.EpisodeEndDate as EndDate ,
                                    mdcs.ClaimDate as ClaimDate 
                                        FROM 
                                            {0} as mdcs
                                                WHERE 
                                                    mdcs.AgencyId = @agencyid AND
                                                    STRCMP(TRIM(LOWER(mdcs.MedicareNumber)), LOWER(@medicareNumber))= 0 AND STRCMP(TRIM(LOWER(mdcs.FirstName)), LOWER(@firstName))=0 AND  STRCMP(TRIM(LOWER(mdcs.LastName)), LOWER(@lastName)) = 0 AND DATE(mdcs.EpisodeStartDate) = DATE( @startDate) ", table);

            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddString("medicareNumber", medicareNumber.ToLowerCase())
                 .AddString("firstName", firstName.ToLowerCase())
                 .AddString("lastName", lastName.ToLowerCase())
                 .AddDateTime("startDate", startDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     BatchId = reader.GetInt("BatchId"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimDate = reader.GetDateTimeWithMin("ClaimDate"),
                     Type = type
                 })
                 .AsList();
            }
            return list;
        }

        public RapSnapShot GetRapSnapShot(Guid agencyId, Guid Id, long batchId)
        {
            return database.Single<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id && r.BatchId == batchId);
        }

        public RapSnapShot GetLastRapSnapShot(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).OrderBy(r => r.BatchId).LastOrDefault();
        }

        public long GetLastRapSnapShotBatchId(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).Max(r => r.BatchId);
        }

        public FinalSnapShot GetFinalSnapShot(Guid agencyId, Guid Id, long batchId)
        {
            return database.Single<FinalSnapShot>(r => r.AgencyId == agencyId && r.Id == Id && r.BatchId == batchId);
        }

        public FinalSnapShot GetLastFinalSnapShot(Guid agencyId, Guid Id)
        {
            return database.Find<FinalSnapShot>(f => f.AgencyId == agencyId && f.Id == Id).OrderBy(f => f.BatchId).LastOrDefault();
        }

        public long GetLastFinalSnapShotBatchId(Guid agencyId, Guid Id)
        {
            return database.Find<FinalSnapShot>(f => f.AgencyId == agencyId && f.Id == Id).Max(f => f.BatchId);
        }

        public bool UpdateRapSnapShots(RapSnapShot rapSnapShot)
        {
            bool result = false;
            if (rapSnapShot != null)
            {
                try
                {
                    database.Update<RapSnapShot>(rapSnapShot);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateFinalSnapShots(FinalSnapShot finalSnapShot)
        {
            bool result = false;
            if (finalSnapShot != null)
            {
                try
                {
                    database.Update<FinalSnapShot>(finalSnapShot);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public IList<RemittanceLean> GetRemittances(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var script = string.Format(@"SELECT 
                    rem.Id as Id,
                    rem.RemitId as RemitId ,
                    rem.TotalClaims as TotalClaims ,
                    rem.ChargedAmount as ChargedAmount,
                    rem.CoveredAmount as CoveredAmount,
                    rem.PaymentAmount as PaymentAmount ,
                    rem.RemittanceDate as RemittanceDate,
                    rem.PaymentDate as PaymentDate
                        FROM 
                            remittances rem
                                WHERE 
                                    rem.AgencyId = @agencyid AND
                                    DATE(rem.RemittanceDate) between DATE(@startdate) and DATE(@enddate) AND 
                                    rem.IsDeprecated = 0 
                                        ORDER BY rem.RemittanceDate");
            var list = new List<RemittanceLean>();
            using (var cmd = new FluentCommand<RemittanceLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new RemittanceLean
                 {
                     Id = reader.GetGuid("Id"),
                     RemitId = reader.GetString("RemitId"),
                     TotalClaims = reader.GetInt("TotalClaims"),
                     ChargedAmount = (double)reader.GetDecimalNullable("ChargedAmount"),
                     CoveredAmount = (double)reader.GetDecimalNullable("CoveredAmount"),
                     PaymentAmount = (double)reader.GetDecimalNullable("PaymentAmount"),
                     RemittanceDate = reader.GetDateTimeNullable("RemittanceDate") != null ? reader.GetDateTime("RemittanceDate") : DateTime.MinValue,
                     PaymentDate = reader.GetDateTimeNullable("PaymentDate") != null ? reader.GetDateTime("PaymentDate") : DateTime.MinValue
                 })
                 .AsList();
            }
            return list;
        }

        public Remittance GetRemittance(Guid agencyId, Guid Id)
        {
            return database.Single<Remittance>(r => r.AgencyId == agencyId && r.Id == Id);
        }

        public List<ClaimLean> GetManySnapShots(List<PaymentInformation> claimInformations, Guid agencyId, string type)
        {
            if (type.Equals("RAP"))
            {
                var raps = claimInformations.Where(w => w.ClaimStatementPeriodStartDate.IsNotNullOrEmpty() && w.ClaimStatementPeriodEndDate.IsNotNullOrEmpty()
                    && w.ClaimStatementPeriodStartDate.Equals(w.ClaimStatementPeriodEndDate));
                if (raps.Count() > 0)
                {
                    var rapInfos = raps.Select(s => new { MedicareNumber = s.Patient.Id, Date = s.ClaimStatementPeriodStartDate });
                    var rapScript = string.Format(@"SELECT 
                    rcs.Id as Id,
                    rcs.PatientId as PatientId,
                    rcs.EpisodeId as EpisodeId,
                    rcs.BatchId as BatchId , 
                    rcs.FirstName as FirstName, 
                    rcs.LastName as LastName, 
                    rcs.PatientIdNumber as PatientIdNumber,
                    rcs.MedicareNumber as MedicareNumber,
                    rcs.EpisodeStartDate as StartDate, 
                    rcs.EpisodeEndDate as EndDate , 
                    rcs.ClaimDate as ClaimDate 
                        FROM
                            rapsnapshots rcs
                                WHERE rcs.agencyid = '{0}' AND ", agencyId);
                                StringBuilder rapBuilder = new StringBuilder(rapScript);
                                rapBuilder.Append("TRIM(LOWER(rcs.MedicareNumber)) IN (");
                                var patients = rapInfos.Select(s => s.MedicareNumber.ToLowerCase()).Distinct();
                                patients.ForEach(r =>
                                {
                                    rapBuilder.Append("'").Append(r.ToLower()).Append("', ");
                                });
                                rapBuilder.Remove(rapBuilder.Length - 2, 2).Append(")");
                                rapBuilder.Append(" AND rcs.EpisodeStartDate in (");
                                var dates = rapInfos.Select(s => s.Date).Distinct();
                                dates.ForEach(r =>
                                {
                                    rapBuilder.Append("'").Append(r.ToDateTimePHP().ToString("yyyy-MM-dd")).Append("', ");
                                });
                                rapBuilder.Remove(rapBuilder.Length - 2, 2).Append(")");
                                return GetSnapShotsUsingScript(rapBuilder.ToString(), "RAP");
                }
            }
            else
            {
                var finals = claimInformations.Where(w => w.ClaimStatementPeriodStartDate.IsNotNullOrEmpty() && w.ClaimStatementPeriodEndDate.IsNotNullOrEmpty()
                    && !w.ClaimStatementPeriodStartDate.Equals(w.ClaimStatementPeriodEndDate)).ToList();
                if (finals.Count > 0)
                {
                    var finalInfos = finals.Select(s => new { MedicareNumber = s.Patient.Id, Date = s.ClaimStatementPeriodStartDate });
                    var finalScript = string.Format(@"SELECT
                    fcs.Id as Id,
                    fcs.PatientId as PatientId, 
                    fcs.EpisodeId as EpisodeId, 
                    fcs.BatchId as BatchId , 
                    fcs.FirstName as FirstName,
                    fcs.LastName as LastName, 
                    fcs.PatientIdNumber as PatientIdNumber,
                    fcs.MedicareNumber as MedicareNumber,
                    fcs.EpisodeStartDate as StartDate, 
                    fcs.EpisodeEndDate as EndDate , 
                    fcs.ClaimDate as ClaimDate 
                    FROM
                        finalsnapshots fcs
                            WHERE fcs.agencyid = '{0}' AND ", agencyId);
                            StringBuilder finalBuilder = new StringBuilder(finalScript);
                            finalBuilder.Append("TRIM(LOWER(fcs.MedicareNumber)) IN (");
                            var patients = finalInfos.Select(s => s.MedicareNumber.ToLowerCase()).Distinct();
                            patients.ForEach(f =>
                            {
                                finalBuilder.Append("'").Append(f.ToLower()).Append("', ");
                            });
                            finalBuilder.Remove(finalBuilder.Length - 2, 2).Append(")");
                            finalBuilder.Append(" AND fcs.EpisodeStartDate IN (");
                            var dates = finalInfos.Select(s => s.Date).Distinct();
                            dates.ForEach(f =>
                            {
                                finalBuilder.Append("'").Append(f.ToDateTimePHP().ToString("yyyy-MM-dd")).Append("', ");
                            });
                            finalBuilder.Remove(finalBuilder.Length - 2, 2).Append(")");
                            return GetSnapShotsUsingScript(finalBuilder.ToString(), "FINAL");
                }
            }
            return new List<ClaimLean>();
        }

        public List<ClaimLean> GetSnapShotsUsingScript(string script, string type)
        {
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .SetMap(reader => new ClaimLean
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     BatchId = reader.GetInt("BatchId"),
                     MedicareNumber = reader.GetStringNullable("MedicareNumber").ToLowerCase(),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimDate = reader.GetDateTimeWithMin("ClaimDate"),
                     Type = type
                 })
                 .AsList();
            }
            return list;
        }

        public bool DeleteRemittance(Guid agencyId, Guid Id)
        {
            //var result = false;
            //var remittance = database.Single<Remittance>(r => r.AgencyId == agencyId && r.Id == Id);
            //if (remittance != null)
            //{
            //    remittance.IsDeprecated = true;
            //    database.Update<Remittance>(remittance);
            //    result = true;
            //}
            //return result;

            try
            {
                var script = string.Format(@"UPDATE remittances rm SET mc.IsDeprecated = 1  WHERE rm.AgencyId = @agencyid AND rm.Id = @id");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }
                return count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateRemittance(Remittance remittance)
        {
            var result = false;
            if (remittance != null)
            {
                database.Update<Remittance>(remittance);
                result = true;
            }
            return result;
        }

        public List<ClaimLean> GetFinalClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                    pe.Id as EpisodeId, 
                    pe.StartDate,
                    pe.EndDate, 
                    pr.FirstName, 
                    pr.LastName, 
                    pr.MiddleInitial,
                    pr.PatientIdNumber, 
                    fc.ProspectivePay,
                    fc.Payment,
                    fc.PaymentDate,
                    fc.ClaimDate, 
                    fc.Status 
                        FROM 
                            finals fc
                                INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id
                                INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id 
                                    WHERE 
                                        fc.AgencyId = @agencyid {0}  AND 
                                        pr.Status IN(1,2)  AND
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND
                                        pe.IsDischarged = 0 AND 
                                        DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND 
                                        fc.Status=@status
                                             ORDER BY pe.StartDate ASC", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("status", status)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "Final"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetFinalClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                            pe.Id as EpisodeId,
                            pe.StartDate,
                            pe.EndDate, 
                            pr.FirstName,
                            pr.LastName,
                            pr.MiddleInitial, 
                            pr.PatientIdNumber,
                            fc.AssessmentType,
                            fc.HippsCode, 
                            fc.ProspectivePay, 
                            fc.Payment,
                            fc.PaymentDate,
                            fc.ClaimDate,
                            fc.Status 
                                FROM 
                                    finals fc
                                        INNER JOIN patientprofiles pr ON fc.PatientId = pr.Id 
                                        INNER JOIN patientepisodes pe ON fc.EpisodeId = pe.Id
                                            WHERE 
                                                fc.AgencyId = @agencyid {0}  AND 
                                                pr.Status IN (1,2)  AND 
                                                pr.IsDeprecated = 0 AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND
                                                DATE(fc.ClaimDate) between DATE(@startdate) and DATE(@enddate)  AND 
                                                fc.Status IN ( 305 , 315 , 320 , 330 ) 
                                                    ORDER BY pe.StartDate ASC", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetInt("Status") == 330 ? reader.GetDateTime("PaymentDate") : DateTime.MinValue,
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     HippsCode = reader.GetStringNullable("HippsCode"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     Type = "Final"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetRapClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                pe.Id as EpisodeId, 
                pe.StartDate,
                pe.EndDate, 
                pr.FirstName, 
                pr.LastName, 
                pr.MiddleInitial, 
                pr.PatientIdNumber,
                rc.ProspectivePay, 
                rc.Payment, 
                rc.PaymentDate , 
                rc.ClaimDate,
                rc.Status 
                    FROM
                        raps rc
                            INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                            INNER JOIN patientepisodes pe ON rc.Id = pe.Id
                                WHERE
                                    rc.AgencyId = @agencyid {0}  AND 
                                    pr.Status IN(1,2) AND
                                    pr.IsDeprecated = 0 AND 
                                    pe.IsActive = 1 AND 
                                    pe.IsDischarged = 0 AND 
                                    DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) AND 
                                    rc.Status = @status 
                                        ORDER BY pe.StartDate ASC", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("status", status)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "RAP"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetRapClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                        pe.Id as EpisodeId,
                        pe.StartDate as StartDate,
                        pe.EndDate as EndDate, 
                        pr.FirstName as FirstName,
                        pr.LastName as LastName,
                        pr.MiddleInitial, 
                        pr.PatientIdNumber as PatientIdNumber,
                        rc.AssessmentType, 
                        rc.HippsCode, 
                        rc.ProspectivePay as ProspectivePay,
                        rc.Payment as Payment, 
                        rc.PaymentDate as PaymentDate ,
                        rc.ClaimDate as ClaimDate, 
                        rc.Status as Status 
                            FROM
                                raps rc
                                    INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                    INNER JOIN patientepisodes pe ON rc.Id = pe.Id 
                                        WHERE
                                            rc.AgencyId = @agencyid {0}  AND 
                                            pr.Status IN(1,2) AND
                                            pr.IsDeprecated = 0 AND
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE(rc.ClaimDate) between DATE(@startdate) and DATE(@enddate) AND
                                            rc.Status IN ( 305 , 315 , 320 , 330 ) 
                                                ORDER BY pe.StartDate ASC", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty);

            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetInt("Status") == 330 ? reader.GetDateTime("PaymentDate") : DateTime.MinValue,
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     HippsCode = reader.GetStringNullable("HippsCode"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     Type = "RAP"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetAccountsReceivableRaps(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate,string insuranceFilter)
        {
            var list = new List<ClaimLean>();
            var script = string.Format(@"SELECT 
                    pe.Id as EpisodeId,
                    pe.StartDate,
                    pe.EndDate,
                    pr.Id as PatientId,
                    pr.FirstName,
                    pr.LastName,
                    pr.MiddleInitial, 
                    pr.PatientIdNumber, 
                    pr.AgencyLocationId, 
                    rc.AssessmentType,
                    rc.HippsCode,
                    rc.AddressZipCode, 
                    rc.Id, 
                    rc.Status,
                    rc.ClaimDate, 
                    rc.Payment, 
                    rc.ProspectivePay 
                        FROM 
                            raps rc
                                INNER JOIN patientprofiles pr ON  rc.PatientId = pr.Id  
                                INNER JOIN patientepisodes pe ON  rc.EpisodeId = pe.Id  
                                    WHERE
                                        rc.AgencyId = @agencyid {0} {1} AND
                                        pr.Status IN (1,2) AND
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND
                                        rc.Status IN (305, 315, 320) AND
                                        (DATE(rc.ClaimDate) between DATE(@startdate) and DATE(@enddate))",
                                        !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty, insuranceFilter);


            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "RAP",
                     AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase(),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay")
                 }).AsList();
            }
            return list;
        }

        public List<ClaimLean> GetAccountsReceivableFinals(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate, string insuranceFilter)
        {
            var list = new List<ClaimLean>();
            var script = string.Format(@"SELECT
                    pe.StartDate,
                    pe.EndDate,
                    pr.FirstName,
                    pr.LastName, 
                    pr.MiddleInitial ,
                    pr.PatientIdNumber,
                    pr.AgencyLocationId, 
                    fc.AssessmentType, 
                    fc.HippsCode,
                    fc.AddressZipCode, 
                    fc.Id, 
                    fc.Status,
                    fc.ClaimDate,
                    fc.Payment,
                    fc.ProspectivePay 
                        FROM
                            finals fc
                                INNER JOIN patientprofiles pr ON  fc.PatientId = pr.Id 
                                INNER JOIN patientepisodes pe ON  fc.EpisodeId = pe.Id 
                                    WHERE
                                        fc.AgencyId = @agencyid {0} {1} AND
                                        pr.Status IN (1,2) AND
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND
                                        fc.Status IN (305, 315, 320)AND
                                        ( DATE(fc.ClaimDate) between DATE(@startdate) and DATE(@enddate) )",
                                        !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty, insuranceFilter);


            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "Final",
                     AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase(),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay")
                 }).AsList();
            }
            return list;
        }

        public List<ManagedBill> GetAccountsReceivableManagedClaims(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var insuranceScript = insurance > 0 ? " AND mc.PrimaryInsuranceId = @insuranceId " : string.Empty;
            var branchScript = !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty;
            var list = new List<ManagedBill>();
            var script = string.Format(@"SELECT 
                            pr.FirstName as FirstName,
                            pr.LastName as LastName,
                            pr.MiddleInitial as MiddleInitial,
                            pr.PatientIdNumber as PatientIdNumber,
                            mc.EpisodeStartDate as StartDate ,
                            mc.EpisodeEndDate as EndDate ,
                            mc.ClaimDate as ClaimDate,
                            mc.Status as Status,
                            mc.ProspectivePay as ProspectivePay
                                FROM 
                                    managedclaims mc
                                        INNER JOIN patientprofiles pr ON  mc.PatientId = pr.Id 
                                            WHERE
                                                mc.AgencyId = @agencyid {0} {1} AND 
                                                pr.Status IN (1,2) AND
                                                pr.IsDeprecated = 0 AND
                                                mc.Status IN (3005, 3015, 3020) AND
                                                ( DATE(mc.EpisodeEndDate) between DATE(@startdate) and DATE(@enddate) ) AND
                                                mc.EpisodeEndDate <= Curdate()", branchScript, insuranceScript);


            using (var cmd = new FluentCommand<ManagedBill>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ManagedBill
                 {
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Status = reader.GetInt("Status"),
                     ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay")
                 }).AsList();
            }
            return list;
        }

        //TODO: needs fix for address
        public List<PendingClaimLean> PendingClaimRaps(Guid agencyId, Guid branchId, int insurance)
        {
            var insuranceScript = "AND pr.PrimaryInsurance = @insuranceId ";
            var script = string.Format(@"SELECT 
                                pe.StartDate,
                                pe.EndDate,
                                pr.FirstName, 
                                pr.LastName, 
                                pr.MiddleInitial,
                                pr.PatientIdNumber,
                                pr.AgencyLocationId, 
                                rc.AssessmentType,
                                rc.MedicareNumber,
                                rc.HippsCode,
                                rc.AddressZipCode as RapZipCode, 
                                rc.Id as Id,
                                rc.PatientId as PatientId,
                                rc.Status as Status,
                                rc.PaymentDate, 
                                rc.Payment, 
                                rc.ProspectivePay as ProspectivePay ,
                                rc.EpisodeStartDate,
                                rc.EpisodeEndDate,
                                EXISTS(select id from rapsnapshots rs where rs.AgencyId = rc.AgencyId AND rs.PatientId = rc.PatientId AND rs.Id = rc.Id) as HasDetails
                                    FROM
                                        raps rc
                                            INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                            INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                                WHERE 
                                                    pr.AgencyId = @agencyid {0} {1} AND
                                                    pr.Status IN (1,2) AND
                                                    pr.IsDeprecated = 0 AND
                                                    pe.IsActive = 1 AND 
                                                    rc.Status IN (305, 315, 320)", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty, insuranceScript);
            var list = new List<PendingClaimLean>();
            using (var cmd = new FluentCommand<PendingClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                  .AddGuid("agencyid", agencyId)
                  .AddGuid("branchId", branchId)
                  .AddInt("insuranceId", insurance)
                  .SetMap(reader => new PendingClaimLean
                  {
                      Id = reader.GetGuid("Id"),
                      HasDetails = reader.GetBoolean("HasDetails"),
                      PatientId = reader.GetGuid("PatientId"),
                      PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                      FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                      LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                      MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                      MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                      Status = reader.GetInt("Status"),
                      EpisodeEndDate = reader.GetDateTime("EndDate").IsValid() ? reader.GetDateTime("EndDate") : reader.GetDateTime("EpisodeEndDate"),
                      EpisodeStartDate = reader.GetDateTime("StartDate").IsValid() ? reader.GetDateTime("StartDate") : reader.GetDateTime("EpisodeStartDate"),
                      PaymentAmount = (double)(reader.GetDecimalNullable("Payment") ?? 0),
                      PaymentDate = reader.GetDateTime("PaymentDate"),
                      Type = "RAP",
                      AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                      AssessmentType = reader.GetStringNullable("AssessmentType"),
                      HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     // AddressZipCode = reader.GetStringNullable("RapZipCode").IsNotNullOrEmpty() ? reader.GetStringNullable("RapZipCode") : reader.GetStringNullable("PatientZipCode").ToUpperCase(),
                      ClaimAmount = (double)(reader.GetDecimalNullable("ProspectivePay") ?? 0)
                  })
                  .AsList().OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ToList();
            }
            return list;
        }
        //TODO: needs fix for address
        public List<PendingClaimLean> PendingClaimFinals(Guid agencyId, Guid branchId, int insurance)
        {
            var insuranceScript = "AND pr.PrimaryInsurance = @insuranceId ";
            var script = string.Format(@"SELECT 
                        pe.StartDate, 
                        pe.EndDate, 
                        pr.FirstName, 
                        pr.LastName,
                        pr.MiddleInitial, 
                        pr.PatientIdNumber, 
                        pr.AgencyLocationId,
                        fc.AssessmentType, 
                        fc.MedicareNumber, 
                        fc.HippsCode, 
                        fc.AddressZipCode as FinalZipCode,
                        fc.Id as Id,
                        fc.PatientId as PatientId,
                        fc.Status as Status,
                        fc.PaymentDate, 
                        fc.Payment, 
                        fc.ProspectivePay as ProspectivePay ,
                        fc.EpisodeStartDate,
                        fc.EpisodeEndDate,
                        EXISTS(select id from finalsnapshots fs where fc.Id = fs.Id AND fc.PatientId = fs.PatientId AND fc.EpisodeId = fs.EpisodeId AND fs.AgencyId = @agencyid) as HasDetails
                            FROM 
                                finals fc
                                    INNER JOIN patientprofiles pr ON  fc.PatientId = pr.Id 
                                    INNER JOIN patientepisodes pe ON  fc.EpisodeId = pe.Id 
                                        WHERE 
                                            pr.AgencyId = @agencyid {0} {1} AND
                                            pr.Status IN(1,2) AND 
                                            pr.IsDeprecated = 0 AND
                                            pe.IsActive = 1 AND 
                                            fc.Status IN (305, 315, 320)", !branchId.IsEmpty() ? "AND pr.AgencyLocationId=@branchId" : string.Empty, insuranceScript);
            var list = new List<PendingClaimLean>();
            using (var cmd = new FluentCommand<PendingClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insurance)
                .SetMap(reader => new PendingClaimLean
                {
                    Id = reader.GetGuid("Id"),
                    HasDetails = reader.GetBoolean("HasDetails"),
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate").IsValid() ? reader.GetDateTime("EndDate") : reader.GetDateTime("EpisodeEndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate").IsValid() ? reader.GetDateTime("StartDate") : reader.GetDateTime("EpisodeStartDate"),
                    PaymentAmount = (double)(reader.GetDecimalNullable("Payment") ?? -1),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = "Final",
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                   // AddressZipCode = reader.GetStringNullable("FinalZipCode").IsNotNullOrEmpty() ? reader.GetStringNullable("FinalZipCode") : reader.GetStringNullable("PatientZipCode").ToUpperCase(),
                    ClaimAmount = (double)(reader.GetDecimalNullable("ProspectivePay") ?? -1)
                })
                .AsList().OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ToList();
            }
            return list;
        }

        public bool AddRemitQueue(RemitQueue remitQueue)
        {
            Check.Argument.IsNotNull(remitQueue, "remitQueue");
            bool result = false;
            try
            {
                remitQueue.Created = DateTime.Now;
                remitQueue.Modified = DateTime.Now;
                database.Add<RemitQueue>(remitQueue);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<ClaimDataLean> ClaimDatas(Guid agencyId, DateTime startDate, DateTime endDate, string claimType)
        {
            var list = new List<ClaimDataLean>();
            var script = string.Format("Select cd.Id , cd.ClaimType , cd.BillIdentifers , cd.Response , cd.Created , cd.Modified FROM claimdatas cd WHERE  cd.AgencyId=@agencyId AND cd.Created >= @startdate AND cd.Created <= @enddate {0} ", claimType.ToUpperCase() == "ALL" ? string.Empty : " AND cd.ClaimType = @claimtype AND cd.IsDeprecated = 0");
            using (var cmd = new FluentCommand<ClaimDataLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                  .AddGuid("agencyid", agencyId)
                  .AddDateTime("startdate", startDate)
                  .AddDateTime("enddate", endDate)
                  .AddString("claimtype", claimType.ToUpperCase())
                  .SetMap(reader => new ClaimDataLean
                  {
                      Id = reader.GetInt("Id"),
                      ClaimType = reader.GetStringNullable("ClaimType"),
                      Claims = reader.GetStringNullable("BillIdentifers").IsNotNullOrEmpty() ? reader.GetStringNullable("BillIdentifers").ToObject<List<ClaimInfo>>() : new List<ClaimInfo>(),
                      Created = reader.GetDateTime("Created"),
                      IsResponseExist = reader.GetStringNullable("Response").IsNotNullOrEmpty(),
                  })
                  .AsList();
            }
            return list;
        }
     
        public List<ClaimInfoDetail> GetMedicareClaimInfoDetails(Guid agencyId, List<Guid> claimIds, string type)
        {
            var list = new List<ClaimInfoDetail>();
            if (type.IsNotNullOrEmpty() && (type.ToUpperCase() == "RAP" || type.ToUpperCase() == "FINAL"))
            {
                if (claimIds != null && claimIds.Count > 0)
                {
                    var script = string.Empty;
                    var ids = claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                    var table = type.ToUpperCase() == "RAP" ? "raps" : "finals";
                    script = string.Format(@"SELECT 
                    pe.StartDate as EpisodeStartDate,
                    pe.EndDate as EpisodeEndDate, 
                    pr.FirstName as FirstName,
                    pr.LastName as LastName, 
                    pr.MiddleInitial as MiddleInitial,
                    pr.PatientIdNumber as PatientIdNumber,
                    mdc.HippsCode as HippsCode,
                    mdc.MedicareNumber as MedicareNumber,
                    mdc.ProspectivePay as ProspectivePay,
                    mdc.AddressZipCode as AddressZipCode 
                        FROM
                            {0} as mdc
                                INNER JOIN patientepisodes pe ON mdc.EpisodeId = pe.Id
                                INNER JOIN patientprofiles pr ON mdc.PatientId = pr.Id 
                                    WHERE
                                        mdc.AgencyId = @agencyid AND
                                        mdc.Id IN ({1}) ", table, ids);

                    using (var cmd = new FluentCommand<ClaimInfoDetail>(script))
                    {
                        list = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .SetMap(reader => new ClaimInfoDetail
                        {
                            BillType = type.ToUpperCase(),
                            PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                            FirstName = reader.GetStringNullable("FirstName"),
                            LastName = reader.GetStringNullable("LastName"),
                            MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                            StartDate = reader.GetDateTime("EpisodeStartDate"),
                            EndDate = reader.GetDateTime("EpisodeEndDate"),
                            MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                            HippsCode = reader.GetStringNullable("HippsCode"),
                            AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                            ProspectivePay = reader.GetDouble("ProspectivePay")
                        }).AsList();
                    }
                }
            }
            return list;
        }

        public List<ClaimEpisode> GetEpisodeNeedsClaim(Guid agencyId, Guid patientId, int type)
        {
            var list = new List<ClaimEpisode>();
            if ((type == (int)ClaimTypeSubCategory.RAP || type == (int)ClaimTypeSubCategory.Final))
            {
                var script = string.Empty;
                var table = type == (int)ClaimTypeSubCategory.RAP ? "raps" : "finals";
                script = string.Format(@"SELECT 
                pe.Id as Id,
                pe.StartDate as EpisodeStartDate,
                pe.EndDate as EpisodeEndDate  
                    FROM  
                        patientepisodes pe 
                            WHERE 
                                pe.AgencyId = @agencyid AND
                                pe.PatientId = @patientId AND
                                pe.IsActive = 1 AND 
                                pe.IsDischarged = 0 AND 
                                pe.Id NOT IN ( SELECT mdc.Id FROM {0} as mdc WHERE mdc.AgencyId = @agencyid AND mdc.PatientId = @patientId )", table);

                using (var cmd = new FluentCommand<ClaimEpisode>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                     .AddGuid("patientId", patientId)
                    .SetMap(reader => new ClaimEpisode
                    {
                        Id = reader.GetGuid("Id"),
                        StartDate = reader.GetDateTime("EpisodeStartDate"),
                        EndDate = reader.GetDateTime("EpisodeEndDate"),
                        Type = type
                    }).AsList();
                }
            }
            return list;
        }

        //TODO: needs fix for schedule
        public List<Revenue> GetRevenue(Guid agencyId, Guid branchId, int insurance, List<int> status, DateTime endDate, string insuranceFilter, bool includeVisitCount)
        {
            var list = new List<Revenue>();
            var branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }
            var statusScript = string.Empty;
            if (status != null && status.Count > 0)
            {
                string statusIds = string.Empty;
                status.ForEach(s => statusIds += string.Format("{0}, ", s));
                statusScript = string.Format(" AND rc.Status IN ({0}) ", statusIds.Trim().RemoveAt(statusIds.Length - 2));
            }
            string visitScript = string.Empty;
            if (includeVisitCount)
            {
                visitScript = string.Format(",(SELECT Count(*) FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.IsBillable = 1 AND st.IsMissedVisit = 0 AND st.IsDeprecated = 0) as BillableVisitCount," +
                    "(SELECT Count(*) FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.IsBillable = 1 AND st.IsMissedVisit = 0 AND st.IsDeprecated = 0 AND st.Status IN ({0}) AND st.EventDate <= DATE('{1}')) as CompletedVisitCount", ScheduleStatusFactory.OnAndAfterQAStatus(true).ToCommaSeperatedList(), endDate.ToMySqlDate());
            }
            else
            {
                visitScript = ",0 as BillableVisitCount, 0 as CompletedVisitCount";
            }

            var script = string.Format(
                @"SELECT
                pe.Id as EpisodeId,
                pe.StartDate,
                pe.EndDate,
                pr.Id as PatientId,
                pr.FirstName, 
                pr.LastName,
                pr.MiddleInitial,
                pr.PatientIdNumber,
                pr.AgencyLocationId, 
                rc.AssessmentType,
                rc.HippsCode,
                rc.AddressZipCode, 
                rc.Id, 
                rc.Status,
                rc.ClaimDate, 
                rc.Payment
                {0}
                    FROM 
                        raps rc
                            INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                            INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                WHERE 
                                    rc.AgencyId = @agencyid AND 
                                    pr.Status IN(1,2) AND
                                    pr.IsDeprecated = 0 AND 
                                    pe.IsActive = 1 AND 
                                    DATE(rc.ClaimDate) <= DATE(@enddate) {1} {2} {3} ", visitScript, insuranceFilter, branchScript, statusScript);

            using (var cmd = new FluentCommand<Revenue>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new Revenue
                 {
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     ClaimDate = reader.GetDateTime("ClaimDate").ToString("MM/dd/yyyy"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase(),
                     BillableVisitCount = reader.GetInt("BillableVisitCount"),
                     CompletedVisitCount = reader.GetInt("CompletedVisitCount")
                 }).AsList();
            }
            return list;
        }
        //TODO: needs fix for schedule
        public List<Revenue> GetRevenue(Guid agencyId, Guid branchId, int insurance, List<int> status, DateTime startDate, DateTime endDate, bool includeVisitCount)
        {
            var list = new List<Revenue>();
            var insuranceScript = string.Empty;
            if (insurance > 0)
            {
                if (insurance < 1000)
                {
                    insuranceScript = " AND pr.PrimaryInsurance < 1000 ";
                }
                else
                {
                    insuranceScript = " AND pr.PrimaryInsurance = @insuranceId ";
                }
            }
            var branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }
            var statusScript = string.Empty;
            //if (status != null && status.Count > 0)
            //{
            //    string statusIds = string.Empty;
            //    status.ForEach(s => statusIds += string.Format("{0}, ", s));
            //    statusScript = string.Format(" AND raps.Status IN ({0}) ", statusIds.Trim().RemoveAt(statusIds.Length - 2));
            //}

            string visitScript = string.Empty;
            if (includeVisitCount)
            {
                visitScript = string.Format(",(SELECT Count(*) FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.IsBillable = 1 AND st.IsMissedVisit = 0 AND st.IsDeprecated = 0) as BillableVisitCount," +
                    "(SELECT Count(*) FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.IsBillable = 1 AND st.IsMissedVisit = 0 AND st.IsDeprecated = 0 AND st.Status IN ({0}) AND st.EventDate <= DATE('{1}')) as CompletedVisitCount", ScheduleStatusFactory.OnAndAfterQAStatus(true).ToCommaSeperatedList(), endDate.ToMySqlDate());
            }
            else
            {
                visitScript = ",0 as BillableVisitCount, 0 as CompletedVisitCount";
            }

            var script = string.Format(
                @"SELECT 
                    pe.Id as EpisodeId,
                    pe.StartDate,
                    pr.Id as PatientId,
                    pe.EndDate, 
                    pr.FirstName, 
                    pr.LastName,
                    pr.MiddleInitial,
                    pr.PatientIdNumber,
                    pr.AgencyLocationId, 
                    rc.AssessmentType, 
                    rc.HippsCode,
                    rc.AddressZipCode,
                    rc.Id, 
                    rc.Status,
                    rc.ClaimDate,
                    rc.Payment 
                    {0}
                        FROM 
                            raps rc
                                INNER JOIN patientprofiles pr ON rc.PatientId = pr.Id 
                                INNER JOIN patientepisodes pe ON rc.EpisodeId = pe.Id 
                                    WHERE
                                        rc.AgencyId = @agencyid AND 
                                        pr.Status IN(1,2) AND
                                        pr.IsDeprecated = 0 AND
                                        pe.IsActive = 1 AND 
                                        ((DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate)) OR (DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate)) OR (DATE(@startdate) between DATE(pe.StartDate) and DATE(pe.EndDate)) OR (DATE(@enddate) between DATE(pe.StartDate) and DATE(pe.EndDate))) {1}{2}{3}", visitScript, insuranceScript, branchScript, statusScript);

            using (var cmd = new FluentCommand<Revenue>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startdate", startDate)
                 .AddDateTime("enddate", endDate)
                 .SetMap(reader => new Revenue
                 {
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     ClaimDate = reader.GetDateTime("ClaimDate").ToString("MM/dd/yyyy"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase(),
                     BillableVisitCount = reader.GetInt("BillableVisitCount"),
                     CompletedVisitCount = reader.GetInt("CompletedVisitCount")
                 }).AsList();
            }
            return list;
        }


        public List<Rap> GetRapsToGenerateByIdsLean(Guid agencyId, List<Guid> rapIds)
        {
            var list = new List<Rap>();
            if (rapIds != null && rapIds.Count > 0)
            {
                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = @"SELECT 
                        rc.Id as Id ,
                        rc.PatientId as PatientId,
                        rc.IsGenerated as IsGenerated , 
                        rc.IsVerified as IsVerified , 
                        rc.Status as Status 
                            FROM
                                raps rc
                                    WHERE 
                                        rc.AgencyId = @agencyid AND
                                        rc.Id IN ( @ids )  ";

                using (var cmd = new FluentCommand<Rap>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddString("ids", ids)
                     .SetMap(reader => new Rap
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         IsVerified = reader.GetBoolean("IsVerified"),
                         Status = reader.GetInt("Status"),

                     }).AsList();
                }
            }
            return list;
        }

        public List<Final> GetFinalsToGenerateByIdsLean(Guid agencyId, List<Guid> finalIds)
        {
            var list = new List<Final>();
            if (finalIds != null && finalIds.Count > 0)
            {
                var ids = finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = @"SELECT 
                        fc.Id as Id ,
                        fc.PatientId as PatientId,
                        fc.IsGenerated as IsGenerated , 
                        fc.IsFinalInfoVerified as IsInfoVerified , 
                        fc.IsInsuranceVerified as IsInsuranceVerified , 
                        fc.IsVisitVerified as IsVisitVerified , 
                        fc.IsSupplyVerified as IsSupplyVerified , 
                        fc.Status as Status 
                            FROM
                                finals fc
                                    WHERE 
                                        fc.AgencyId = @agencyid AND
                                        fc.Id IN ( @ids ) ";

                using (var cmd = new FluentCommand<Final>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddString("ids",ids)
                     .SetMap(reader => new Final
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         IsFinalInfoVerified = reader.GetBoolean("IsInfoVerified"),
                         IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         Status = reader.GetInt("Status"),

                     }).AsList();
                }
            }
            return list;
        }

//        public List<Rap> GetRapsByIdAndStatusLeanForAutoCancel(Guid agencyId,int []  status, List<Guid> rapIds,int dateDifference)
//        {
//            var list = new List<Rap>();
//            if (rapIds != null && rapIds.Count > 0)
//            {
//                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
//                var script = string.Format(@"SELECT 
//                        raps.Id as Id ,
//                        raps.ClaimDate as ClaimDate , 
//                        raps.Status as Status 
//                            FROM
//                                raps 
//                                    WHERE 
//                                        raps.AgencyId = @agencyid AND
//                                        raps.Id IN ( {0} ) AND
//                                        rap.Status IN ({1}) AND
//                                        DATEDIFF(@date,Date(ClaimDate))>= @datedifference", ids, status.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

//                using (var cmd = new FluentCommand<Rap>(script))
//                {
        //                    list = cmd.SetConnection(connectionStringName)
//                     .AddGuid("agencyid", agencyId)
//                     .AddDateTime("date",DateTime.Now)
//                     .AddInt("datedifference", dateDifference)
//                     .SetMap(reader => new Rap
//                     {
//                         Id = reader.GetGuid("Id"),
//                         Status = reader.GetInt("Status"),
//                         ClaimDate= reader.GetDateTime("ClaimDate")

//                     }).AsList();
//                }
//            }
//            return list;
//        }


        public bool AddSecondaryClaim(SecondaryClaim claim)
        {
            Check.Argument.IsNotNull(claim, "claim");
            bool result = false;
            try
            {
                claim.Created = DateTime.Now;
                claim.Modified = DateTime.Now;
                database.Add<SecondaryClaim>(claim);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<SecondaryClaimLean> GetSecondaryClaimLeansOfPrimaryClaim(Guid agencyId, Guid patientId, Guid primaryClaimId)
        {
            var list = new List<SecondaryClaimLean>();

            var script = string.Format(
               @"SELECT 
                    sc.Id, 
                    sc.PatientId,
                    sc.PrimaryClaimId,
                    sc.Payment, 
                    sc.PaymentDate,
                    sc.ClaimDate, 
                    sc.ClaimAmount,
                    sc.StartDate, 
                    sc.EndDate,
                    sc.Status,
                    sc.InvoiceType,
                    sc.IsVisitVerified,
                    sc.IsInsuranceVerified,
                    sc.IsSupplyVerified,
                    sc.IsInfoVerified,
                    sc.IsRemittanceVerified
                        FROM secondaryclaims sc
                           WHERE 
                                sc.IsDeprecated = 0 AND 
                                sc.AgencyId = @agencyId AND
                                sc.PatientId = @patientId AND
                                sc.PrimaryClaimId = @primaryClaimId");

            using (var cmd = new FluentCommand<SecondaryClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyId", agencyId)
                 .AddGuid("primaryClaimId", primaryClaimId)
                 .AddGuid("patientId", patientId)
                 .SetMap(reader => new SecondaryClaimLean
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     PrimaryClaimId = reader.GetGuid("PrimaryClaimId"),
                     PaymentAmount = reader.GetDouble("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     ClaimAmount = reader.GetDouble("ClaimAmount"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     StartDate = reader.GetDateTime("StartDate"),
                     EndDate = reader.GetDateTime("EndDate"),
                     Status = reader.GetInt("Status"),
                     InvoiceType = reader.GetInt("InvoiceType"),
                     IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                     IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                     IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                     IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                     IsRemittanceVerified = reader.GetBoolean("IsRemittanceVerified")
                 }).AsList();
            }
            return list;
        }

        public SecondaryClaimLean FisrtSecondaryClaimExistInTheRange(Guid agencyId, Guid patientId, Guid primaryClaimId, DateTime startDate, DateTime endDate)
        {
            SecondaryClaimLean claim = null;
            var script = string.Format(
               @"SELECT 
                    sc.StartDate, 
                    sc.EndDate
                        FROM secondaryclaims sc
                           WHERE 
                                sc.AgencyId = @agencyId AND
                                sc.PatientId = @patientId AND
                                sc.PrimaryClaimId = @primaryClaimId AND
                                sc.IsDeprecated = 0 AND 
                                (DATE(@startdate) between DATE(sc.StartDate) and DATE(sc.EndDate) OR DATE(@enddate) between DATE(sc.StartDate) and DATE(sc.EndDate)) ORDER BY sc.StartDate DESC limit 0,1;");

            using (var cmd = new FluentCommand<SecondaryClaimLean>(script))
            {
                claim = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyId", agencyId)
                 .AddGuid("primaryClaimId", primaryClaimId)
                 .AddGuid("patientId", patientId)
                 .AddDateTime("startdate", startDate)
                  .AddDateTime("enddate", endDate)
                  .SetMap(reader => new SecondaryClaimLean
                 {
                     StartDate = reader.GetDateTime("StartDate"),
                     EndDate = reader.GetDateTime("EndDate")
                 }).AsSingle();
            }
            return claim;
        }

        public SecondaryClaim GetSecondaryClaim(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<SecondaryClaim>(s => s.AgencyId == agencyId && s.Id == claimId);
        }

        public SecondaryClaim GetSecondaryClaim(Guid agencyId, Guid patientId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<SecondaryClaim>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == claimId);
        }

        public bool MarkSecondaryClaimsAsSubmitted(Guid agencyId, List<SecondaryClaim> secondaryClaims)
        {
            //secondaryClaims.ForEach(claim =>
            //{
            //    var claimToEdit = database.Single<SecondaryClaim>(m => m.AgencyId == agencyId && m.Id == claim.Id);
            //    if (claimToEdit != null)
            //    {
            //        claimToEdit.Status = (int)ManagedClaimStatus.ClaimSubmitted;
            //        claimToEdit.IsGenerated = true;
            //        claimToEdit.ClaimDate = DateTime.Now;
            //        database.Update<SecondaryClaim>(claimToEdit);
            //    }
            //});

            bool result = false;
            if (secondaryClaims.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE secondaryclaims sc SET sc.IsGenerated = 1,sc.Status = @status , sc.ClaimDate = @claimdate  WHERE sc.AgencyId = @agencyid AND sc.Id IN (@ids)");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AddInt("status", (int)ManagedClaimStatus.ClaimSubmitted)
                        .AddString("ids", secondaryClaims.Select(m => m.Id).ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }
                    return count > 0;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public bool MarkSecondaryClaimsAsGenerated(Guid agencyId, List<SecondaryClaim> secondaryClaims)
        {
            //secondaryClaims.ForEach(claim =>
            //{
            //    var claimToEdit = database.Single<SecondaryClaim>(m => m.AgencyId == agencyId && m.Id == claim.Id);
            //    if (claimToEdit != null)
            //    {
            //        claimToEdit.IsGenerated = true;
            //        claimToEdit.ClaimDate = DateTime.Now;
            //        database.Update<SecondaryClaim>(claimToEdit);
            //    }
            //});

            bool result = false;
            if (secondaryClaims.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE secondaryclaims sc SET sc.IsGenerated = 1,sc.ClaimDate = @claimdate  WHERE sc.AgencyId = @agencyid AND sc.Id IN (@ids)");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AddString("ids", secondaryClaims.Select(m => m.Id).ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }
                    return count > 0;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;

        }

        public bool DeleteSecondaryClaim(Guid agencyId, Guid patientId, Guid Id)
        {
            //var claim = database.Single<SecondaryClaim>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
            //try
            //{
            //    if (claim != null)
            //    {
            //        database.Delete<SecondaryClaim>(claim.Id);
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch (Exception)
            //{
            //    return false;
            //}


            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  secondaryclaims WHERE AgencyId = @agencyid AND  PatientId = @patientid AND Id = @id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateSecondaryClaimModel(SecondaryClaim claim)
        {
            if (claim != null)
            {
                try
                {
                    claim.Modified = DateTime.Now;
                    database.Update<SecondaryClaim>(claim);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

//        public SecondaryClaim GetSecondaryClaimInsurance(Guid AgencyId, Guid Id)
//        {
//            var claim = new SecondaryClaim();
//            var script = string.Format(
//               @"SELECT 
//                    sc.PatientId,
//                    sc.PrimaryClaimId,
//                    sc.Insurance, 
//                    sc.SecondaryInsuranceId
//                        FROM 
//                          secondaryclaims sc
//                           WHERE 
//                                sc.IsDeprecated = 0 AND 
//                                sc.AgencyId = @agencyId AND
//                                sc.Id = @id");

//            using (var cmd = new FluentCommand<SecondaryClaim>(script))
//            {
//                claim = cmd.SetConnection(connectionStringName)
//                 .AddGuid("agencyId", AgencyId)
//                 .AddGuid("id", Id)
//                 .SetMap(reader => new SecondaryClaim
//                 {
//                     Id = Id,
//                     PatientId = reader.GetGuid("PatientId"),
//                     PrimaryClaimId = reader.GetGuid("PrimaryClaimId"),
//                     Insurance = reader.GetStringNullable("Insurance"),
//                     SecondaryInsuranceId = reader.GetInt("SecondaryInsuranceId")
//                 }).AsSingle();
//            }
//            return claim;
//        }

        //public bool SecondaryClaimVerifyInsurance(SecondaryClaim claim, Guid agencyId)
        //{
        //    if (claim != null)
        //    {
        //        var currentClaim = database.Single<SecondaryClaim>(r => (r.AgencyId == agencyId && r.Id == claim.Id));
        //        if (currentClaim != null)
        //        {
        //            try
        //            {
        //                var insurance = currentClaim.Insurance.ToObject<AgencyInsurance>();
        //                if (insurance != null)
        //                {
        //                    currentClaim.InvoiceType = insurance.InvoiceType;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //                currentClaim.IsInsuranceVerified = true;
        //                currentClaim.Ub04Locator81cca = claim.Ub04Locator81cca;
        //                currentClaim.Ub04Locator39 = claim.Ub04Locator39;
        //                currentClaim.Ub04Locator31 = claim.Ub04Locator31;
        //                currentClaim.Ub04Locator32 = claim.Ub04Locator32;
        //                currentClaim.Ub04Locator33 = claim.Ub04Locator33;
        //                currentClaim.Ub04Locator34 = claim.Ub04Locator34;
        //                currentClaim.HCFALocators = claim.HCFALocators;
        //                currentClaim.Modified = DateTime.Now;
        //                database.Update<SecondaryClaim>(currentClaim);
        //                return true;
        //            }
        //            catch (Exception)
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public List<SecondaryClaim> GetSecondaryClaimsToGenerateByIds(Guid agencyId, List<Guid> secondaryClaimIds)
        {
            var list = new List<SecondaryClaim>();
            if (secondaryClaimIds != null && secondaryClaimIds.Count > 0)
            {
                var ids = secondaryClaimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT
                            sc.Id as Id ,
                            sc.AgencyId as AgencyId ,
                            sc.PatientId as PatientId ,
                            sc.EpisodeId as EpisodeId , 
                            sc.PatientIdNumber as PatientIdNumber, 
                            sc.EpisodeStartDate as EpisodeStartDate, 
                            sc.EpisodeEndDate as EpisodeEndDate ,
                            sc.IsFirstBillableVisit as IsFirstBillableVisit ,
                            sc.FirstBillableVisitDate as FirstBillableVisitDate ,
                            sc.Remark as Remark , 
                            sc.InsuranceIdNumber as InsuranceIdNumber , 
                            sc.FirstName as FirstName , 
                            sc.LastName as LastName ,
                            sc.DOB as DOB , 
                            sc.Gender as Gender ,
                            sc.PaymentDate as PaymentDate , 
                            sc.AddressLine1 as AddressLine1 ,
                            sc.AddressLine2 as AddressLine2 ,
                            sc.AddressCity as AddressCity ,
                            sc.AddressStateCode as AddressStateCode ,
                            sc.AddressZipCode as AddressZipCode , 
                            sc.StartofCareDate as StartofCareDate , 
                            sc.PhysicianNPI as PhysicianNPI ,
                            sc.PhysicianFirstName as PhysicianFirstName ,
                            sc.PhysicianLastName as PhysicianLastName ,
                            sc.DiagnosisCode as DiagnosisCode ,
                            sc.HippsCode as HippsCode ,
                            sc.ClaimKey as ClaimKey ,
                            sc.Modified as Modified ,
                            sc.Created as Created , 
                            sc.Relationship as Relationship, 
                            sc.VerifiedVisits as VerifiedVisits ,
                            sc.Supply as Supply , 
                            sc.SupplyTotal as SupplyTotal ,
                            sc.Payment as Payment ,  
                            sc.SecondaryInsuranceId as SecondaryInsuranceId ,
                            sc.IsSupplyVerified as IsSupplyVerified ,
                            sc.IsInfoVerified as IsInfoVerified , 
                            sc.IsVisitVerified as IsVisitVerified ,
                            sc.IsRemittanceVerified as IsRemittanceVerified ,
                            sc.Status as Status ,
                            sc.Type as Type , 
                            sc.DischargeDate as DischargeDate ,
                            sc.AdmissionSource as AdmissionSource , 
                            sc.PatientStatus as PatientStatus ,
                            sc.ClaimDate as ClaimDate , 
                            sc.ProspectivePay as ProspectivePay , 
                            sc.IsInsuranceVerified as IsInsuranceVerified,
                            sc.AssessmentType as AssessmentType , 
                            sc.Comment as Comment  , 
                            sc.HealthPlanId as HealthPlanId ,
                            sc.GroupName as GroupName , 
                            sc.GroupId as GroupId , 
                            sc.AuthorizationNumber as AuthorizationNumber ,
                            sc.AuthorizationNumber2 as AuthorizationNumber2, 
                            sc.AuthorizationNumber3 as AuthorizationNumber3 , 
                            sc.ConditionCodes as ConditionCodes , 
                            sc.UB4PatientStatus as UB4PatientStatus  ,
                            sc.Ub04Locator81cca as Ub04Locator81cca , 
                            sc.Ub04Locator39 as Ub04Locator39, 
                            sc.Ub04Locator31 as Ub04Locator31,
                            sc.Ub04Locator32 as Ub04Locator32, 
                            sc.Ub04Locator33 as Ub04Locator33, 
                            sc.Ub04Locator34 as Ub04Locator34, 
                            pr.MiddleInitial as MiddleInitial,
                            pr.AgencyLocationId as AgencyLocationId  
                                FROM 
                                    secondaryclaims sc
                                        INNER JOIN patientprofiles pr ON sc.PatientId = pr.Id 
                                            WHERE 
                                                sc.AgencyId = @agencyid   AND 
                                                pr.AgencyId = @agencyid AND
                                                sc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<SecondaryClaim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new SecondaryClaim
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         Remark = reader.GetStringNullable("Remark"),
                         InsuranceIdNumber = reader.GetStringNullable("InsuranceIdNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         //MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         VerifiedVisits = reader.GetStringNullable("VerifiedVisits"),
                         Supply = reader.GetStringNullable("Supply"),
                         SupplyTotal = (double)reader.GetDecimalNullable("SupplyTotal"),
                         Payment = (double)reader.GetDecimalNullable("Payment"),
                         SecondaryInsuranceId = reader.GetInt("SecondaryInsuranceId"),
                         IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRemittanceVerified = reader.GetBoolean("IsRemittanceVerified"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetInt("Type"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay"),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         Relationship = reader.GetStringNullable("Relationship"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         AuthorizationNumber2 = reader.GetStringNullable("AuthorizationNumber2"),
                         AuthorizationNumber3 = reader.GetStringNullable("AuthorizationNumber3"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus"),
                         Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                         Ub04Locator39 = reader.GetStringNullable("Ub04Locator39"),
                         Ub04Locator31 = reader.GetStringNullable("Ub04Locator31"),
                         Ub04Locator32 = reader.GetStringNullable("Ub04Locator32"),
                         Ub04Locator33 = reader.GetStringNullable("Ub04Locator33"),
                         Ub04Locator34 = reader.GetStringNullable("Ub04Locator34"),
                         AgencyLocationId = reader.GetGuid("AgencyLocationId")
                     })
                     .AsList();
                }
            }
            return list;
        }

        #region Private

        private Rap CreateRap(Patient patient, Profile profile, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
        {
            var rap = new Rap
            {
                Id = episode.Id,
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                EpisodeStartDate = episode.StartDate,
                EpisodeEndDate = episode.EndDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                MedicareNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = episode.StartOfCareDate,
                AreOrdersComplete = false,
                AdmissionSource = profile.AdmissionSource,
                PatientStatus = profile.Status,
                UB4PatientStatus = profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty),
                Status = (int)BillingStatus.ClaimCreated,
                HealthPlanId = profile.PrimaryHealthPlanId,
                Relationship = profile.PrimaryRelationship,
                DiagnosisCode = new DiagnosisCodes().ToXml(),
                ConditionCodes = new ConditionCodes().ToXml(),
                Created = DateTime.Now
            };
            if (insuranceId > 0)
            {
                rap.PrimaryInsuranceId = insuranceId;
            }
            else if (profile.PrimaryInsurance > 0)
            {
                rap.PrimaryInsuranceId = profile.PrimaryInsurance;
            }
            if (agencyPhysician != null)
            {
                rap.PhysicianNPI = agencyPhysician.NPI;
                rap.PhysicianFirstName = agencyPhysician.FirstName;
                rap.PhysicianLastName = agencyPhysician.LastName;
            }
            return rap;
        }

        private Final CreateFinal(Patient patient, Profile profile, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
        {
            var final = new Final
            {
                Id = episode.Id,
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                EpisodeStartDate = episode.StartDate,
                EpisodeEndDate = episode.EndDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                MedicareNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = episode.StartOfCareDate,
                AdmissionSource = profile.AdmissionSource,
                PatientStatus = profile.Status,
                UB4PatientStatus = profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty),
                AreOrdersComplete = false,
                AreVisitsComplete = false,
                Status = (int)BillingStatus.ClaimCreated,
                IsSupplyVerified = false,
                IsFinalInfoVerified = false,
                IsVisitVerified = false,
                IsRapGenerated = false,
                HealthPlanId = profile.PrimaryHealthPlanId,
                Relationship = profile.PrimaryRelationship,
                Created = DateTime.Now,
                IsSupplyNotBillable = false,
                DiagnosisCode = new DiagnosisCodes().ToXml(),
                ConditionCodes = new ConditionCodes().ToXml(),
                Modified = DateTime.Now
            };
            if (insuranceId > 0)
            {
                final.PrimaryInsuranceId = insuranceId;
            }
            else if (profile.PrimaryInsurance>0)
            {
                final.PrimaryInsuranceId = profile.PrimaryInsurance;
            }
            if (agencyPhysician != null)
            {
                final.PhysicianNPI = agencyPhysician.NPI;
                final.PhysicianFirstName = agencyPhysician.FirstName;
                final.PhysicianLastName = agencyPhysician.LastName;
            }
            return final;
        }

        #endregion

        protected override string GetManagedClaimsAppSpecificInsuranceOrPayorTypeFilter(int insuranceId, bool IsZeroInsuraceIdAll)
        {
            var insurance = " AND mc.PrimaryInsuranceId = @insuranceId ";
            if (IsZeroInsuraceIdAll && insuranceId == 0)
            {
                insurance = string.Empty;
            }
            return insurance;
        }

        protected override string GetManagedClaimsPerPatientAppSpecificInsuranceOrPayorTypeFilter(int insuranceId)
        {
            var insurance = string.Empty;
            if (insuranceId > 0)
            {
                insurance = "AND mc.PrimaryInsuranceId = @insuranceId ";
            }
            else if (insuranceId == -1)
            {
                insurance = "AND mc.PrimaryInsuranceId <= 0 ";
            }
            else
            {
            }
            return insurance;
        }
    }
}
