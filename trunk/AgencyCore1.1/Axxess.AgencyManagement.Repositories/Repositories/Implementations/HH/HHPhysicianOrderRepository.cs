﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public class HHPhysicianOrderRepository : PhysicianOrderAbstract
    {
        public HHPhysicianOrderRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.HomeHealth;
        }
    }
}
