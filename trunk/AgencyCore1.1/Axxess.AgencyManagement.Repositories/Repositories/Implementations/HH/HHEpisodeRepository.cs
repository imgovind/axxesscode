﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.Core.Enums;
using Axxess.Core;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class HHEpisodeRepository : EpisodeAbstract<PatientEpisode>
    {
         #region Constructor


        public HHEpisodeRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.HomeHealth;
        }

        #endregion

      

        public List<PatientSocCertPeriod> PatientSocCertPeriods(Guid agencyId, string patientIds, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientSocCertPeriod>();
            var script = string.Format(@"SELECT 
                                        pe.PatientId as PatientId ,
                                        pad.PatientData as PatientData, 
                                        pe.StartOfCareDate as StartOfCareDate ,
                                        pe.EndDate as EndDate,
                                        pe.StartDate as StartDate 
                                            FROM 
                                                patientepisodes pe
                                                    LEFT JOIN 
                                                        patientadmissiondates pad ON pe.PatientId = pad.PatientId AND pe.AdmissionId = pad.Id 
                                                            WHERE 
                                                                pe.AgencyId = @agencyid  AND
                                                                pe.PatientId IN ( {0} ) AND 
                                                                pe.IsActive = 1  AND 
                                                                pe.IsDischarged = 0 AND
                                                                DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate)", patientIds.ToString());

            using (var cmd = new FluentCommand<PatientSocCertPeriod>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientSocCertPeriod
                {
                    Id = reader.GetGuid("PatientId"),
                    PatientSoC = reader.GetDateTime("StartOfCareDate"),
                    SocCertPeriod = string.Format("{0} - {1}", reader.GetDateTime("StartDate").ToString("MM/dd/yyyy"), reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")),
                    PatientData = reader.GetStringNullable("PatientData"),
                }).AsList();
            }
            return list;
        }

        public bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete)
        {
            var result = false;
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null)
                {
                    episode.IsRecertCompleted = isRecertComplete;
                    episode.Modified = DateTime.Now;
                    database.Update<PatientEpisode>(episode);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<PatientSelection> GetUserPatientsForDropDown(Guid agencyId, Guid branchId, Guid userId, byte statusId)
        {
            var patients = new List<PatientSelection>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                patients = new List<PatientSelection>();
                if (!userId.IsEmpty() && statusId > 0)
                {
                    var branchScript = !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId " : string.Empty;
                    var script = string.Format(@"SELECT 
                                            pr.Id as PatientId,
                                            pr.FirstName as FirstName, 
                                            pr.LastName as LastName, 
                                                    FROM patientepisodes pe
                                                            INNER JOIN patientprofiles pr ON pe.PatientId = p.Id 
                                                                    WHERE
                                                                           pe.AgencyId = @agencyid AND 
                                                                           pr.IsDeprecated = 0 AND
                                                                           pr.Status = @statusid {0} {1} AND 
                                                                           pe.IsActive = 1 AND 
                                                                           pe.IsDischarged = 0 AND 
                                                                           EXISTS (SELECT * FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.UserId = @userid  AND st.IsDeprecated = 0 AND DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) ) ORDER BY pr.LastName ASC ;",
                       this.Service.ToString(),branchScript );


                    using (var cmd = new FluentCommand<PatientSelection>(script))
                    {
                        patients = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                         .AddGuid("branchid", branchId)
                        .AddGuid("userid", userId)
                        .AddInt("statusid", statusId)
                        .SetMap(reader => new PatientSelection
                        {
                            Id = reader.GetGuid("PatientId"),
                            FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        })
                        .AsList();
                    }
                }
                return patients;
            }
            return patients;
        }

      

        //public List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId)
        //{
        //    var patients = new List<PatientSelection>();
        //    if (!userId.IsEmpty() && statusId > 0)
        //    {
        //        if (!userId.IsEmpty() && statusId > 0)
        //        {
        //            patients = GetPatientEpisodeData(agencyId, branchId, userId, statusId, insuranceId);
        //        }
        //    }
        //    return patients;//.OrderBy(p => p.LastName).ToList();
        //}

        public List<PatientSelection> GetPatientEpisodeDataForList(Guid agencyId, List<Guid> branchIds, Guid userId, byte statusId)
        {
            var list = new List<PatientSelection>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                var branchScript = string.Empty;
                if (branchIds.Count == 1)
                {
                    branchScript=string.Format("pr.AgencyLocationId == '{0}' ", branchIds[0]);
                }
                else
                {
                    branchScript=string.Format("pr.AgencyLocationId IN ({0}) ", branchIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
                //var branchScript = !branchId.IsEmpty() ? "AND pr.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty;
                var script = string.Format(@"SELECT 
                                            pr.Id as PatientId,
                                            pr.FirstName as FirstName, 
                                            pr.LastName as LastName, 
                                            pr.MiddleInitial as MiddleInitial, 
                                            pr.Status as Status
                                                    FROM 
                                                          patientepisodes pe 
                                                            INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id 
                                                                    WHERE 
                                                                           pe.AgencyId = @agencyid AND 
                                                                           pr.IsDeprecated = 0 AND 
                                                                           pr.Status = @statusid {0} AND 
                                                                           pe.IsActive = 1 AND 
                                                                           pe.IsDischarged = 0 AND 
                                                                           EXISTS (SELECT * FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.UserId = @userid  AND st.IsDeprecated = 0 AND DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) ) ORDER BY pr.LastName ASC ;"
                    , branchScript);



                using (var cmd = new FluentCommand<PatientSelection>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("userid", userId)
                    .AddInt("statusid", statusId)
                    .SetMap(reader => new PatientSelection
                    {
                        Id = reader.GetGuid("PatientId"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MI = reader.GetStringNullable("MiddleInitial").ToUpperCase(),
                        IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                    })
                    .AsList();
                }
            }
            return list;
        }


       
//        public List<PatientSelection> GetPatientEpisodeData(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId)
//        {
//            var insurance = string.Empty;
//            var list = new List<PatientSelection>();
//            if (insuranceId < 0)
//            {
//                return list;
//            }
//            else if (insuranceId == 0)
//            {
//                if (!branchId.IsEmpty())
//                {
//                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
//                    if (location != null && location.IsLocationStandAlone)
//                    {
//                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
//                        {
//                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", location.Payor);
//                        }
//                        else
//                        {
//                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
//                        }
//                    }
//                    else
//                    {
//                        var agency = database.Single<Agency>(l => l.Id == agencyId);
//                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
//                        {
//                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance => 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", agency.Payor);
//                        }
//                        else
//                        {
//                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
//                        }
//                    }
//                }
//            }
//            else
//            {
//                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} ) ", insuranceId);
//            }


//            var script = string.Format(@"SELECT 
//                                            patients.Id as PatientId,
//                                            patients.FirstName as FirstName, 
//                                            patients.LastName as LastName, 
//                                            patients.MiddleInitial as MiddleInitial, 
//                                            patients.PatientIdNumber as PatientIdNumber,
//                                            patients.Status as Status, 
//                                            patients.DOB as DOB, 
//                                            patients.PhoneHome as PhoneHome
//                                                    FROM patientepisodes 
//                                                            INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                                                    WHERE patientepisodes.AgencyId = @agencyid AND 
//                                                                           patients.IsDeprecated = 0 AND 
//                                                                           patients.Status = @statusid {0} {1} AND 
//                                                                           patientepisodes.IsActive = 1 AND 
//                                                                           patientepisodes.IsDischarged = 0 AND 
//                                                                           EXISTS (SELECT * FROM scheduleevents WHERE scheduleevents.EpisodeId = patientepisodes.Id AND scheduleevents.UserId = @userid  AND scheduleevents.IsDeprecated = 0 AND DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) )  ORDER BY LastName ASC ;", insurance, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty);


//            using (var cmd = new FluentCommand<PatientSelection>(script))
//            {
//                list = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("userid", userId)
//                .AddInt("statusid", statusId)
//                .SetMap(reader => new PatientSelection
//                {
//                    Id = reader.GetGuid("PatientId"),
//                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                    MI = reader.GetStringNullable("MiddleInitial").ToUpperCase(),
//                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
//                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
//                    DOB = reader.GetDateTime("DOB"),
//                    PhoneNumber = reader.GetStringNullable("PhoneHome")
//                })
//                .AsList();
//            }
//            return list;
//        }
    }
}
