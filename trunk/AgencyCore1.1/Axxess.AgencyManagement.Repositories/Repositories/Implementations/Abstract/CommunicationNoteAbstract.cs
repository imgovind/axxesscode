﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using SubSonic.Repository;

   public abstract class CommunicationNoteAbstract
    {
        #region Constructor

        protected SimpleRepository database;
        protected string connectionStringName;

        #endregion

        #region Communication Note

        public bool AddCommunicationNote(CommunicationNote communicationNote)
        {
            try
            {
                if (communicationNote != null)
                {
                    communicationNote.Modified = DateTime.Now;
                    database.Add<CommunicationNote>(communicationNote);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool AddMultipleCommunicationNote(List<CommunicationNote> communicationNotes)
        {
            try
            {
                if (communicationNotes != null && communicationNotes.Count > 0)
                {
                    database.AddMany<CommunicationNote>(communicationNotes);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool EditCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            Check.Argument.IsNotEmpty(communicationNote.Id, "communicationNote.Id");

            bool result = false;
            try
            {
                var communicationNoteToEdit = database.Single<CommunicationNote>(c => c.AgencyId == communicationNote.AgencyId && c.Id == communicationNote.Id && c.PatientId == communicationNote.PatientId);
                if (communicationNoteToEdit != null)
                {
                    communicationNoteToEdit.Created = communicationNote.Created;
                    communicationNoteToEdit.Text = communicationNote.Text;
                    communicationNoteToEdit.EpisodeId = communicationNote.EpisodeId;
                    communicationNoteToEdit.PhysicianId = communicationNote.PhysicianId;
                    communicationNoteToEdit.SignatureText = communicationNote.SignatureText;
                    communicationNoteToEdit.SignatureDate = communicationNote.SignatureDate;
                    communicationNoteToEdit.Status = communicationNote.Status;
                    communicationNoteToEdit.PhysicianData = communicationNote.PhysicianData;
                    database.Update<CommunicationNote>(communicationNoteToEdit);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateCommunicationNoteModal(CommunicationNote communicationNote)
        {
            bool result = false;
            try
            {
                if (communicationNote != null)
                {
                    database.Update<CommunicationNote>(communicationNote);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeleteCommunicationNote(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            try
            {
                var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id);
                if (commNote != null)
                {
                    commNote.IsDeprecated = isDeprecated;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveModel<T>(Guid Id) where T : class, new()
        {
            var result = false;
            try
            {
                database.Delete<T>(Id);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (commNote != null)
            {
                try
                {
                    commNote.UserId = employeeId;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public CommunicationNote GetCommunicationNote(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId)
        {
            return database.Find<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.IsDeprecated == false).ToList();
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId, Guid episodeId)
        {
            return database.Find<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.EpisodeId == episodeId && c.IsDeprecated == false).ToList();
        }

        public List<CommunicationNote> GetCommunicationNoteByIds(Guid agencyId, string communicationNoteIds)
        {
            var script = string.Format(@"SELECT 
                                     cmn.Id as Id,
                                     cmn.EpisodeId as EpisodeId,
                                     cmn.Status as Status, 
                                     cmn.Created as Created,
                                     cmn.PatientId as PatientId
                                        FROM
                                            communicationnotes cmn
                                                WHERE
                                                     cmn.AgencyId = @agencyid AND 
                                                     cmn.IsDeprecated = 0 AND
                                                     cmn.Id IN ( {0} ) ", communicationNoteIds);

            var list = new List<CommunicationNote>();
            using (var cmd = new FluentCommand<CommunicationNote>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new CommunicationNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created")
                })
                .AsList();
            }
            return list;
        }

        public List<CommunicationNote> GetAllCommunicationNotes()
        {
            return database.All<CommunicationNote>().ToList();
        }

        #endregion

    }
}
