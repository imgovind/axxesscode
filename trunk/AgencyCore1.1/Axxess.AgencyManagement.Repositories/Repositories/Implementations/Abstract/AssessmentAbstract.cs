﻿
namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Common;

    public abstract class AssessmentAbstract 
    {
         #region Constructor

        protected SimpleRepository database;
        public string connectionStringName;
       
        #endregion

        #region IAssessmentRepository Members

        public bool InsertAny<T>(T data) where T : class, IAssessment, new()
        {
            var result = false;
            try
            {
                if (data != null)
                {
                    data.Created = DateTime.Now;
                    data.Modified = DateTime.Now;
                    database.Add<T>(data);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool InsertAnyMultiple<T>(List<T> datas) where T : class, IAssessment, new()
        {
            var result = false;
            try
            {
                if (datas != null && datas.Count > 0)
                {
                    database.AddMany<T>(datas);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateAny<T>(T data) where T : class, IAssessment, new()
        {
            var result = false;
            if (data != null)
            {
                
                    data.Modified = DateTime.Now;
                    result = database.Update<T>(data) >= 1;
                    //throw new Exception();
               
            }
            return result;
        }

        public bool RemoveModel<T>(Guid Id) where T : class, new()
        {
            var result = false;
            try
            {
                result = database.Delete<T>(Id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public  T FindAny<T>( Guid agencyId, Guid assessmentId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(e => e.AgencyId == agencyId && e.Id == assessmentId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type).ToString();
            }
            return assessment;
        }

        public T FindAny<T>(Guid agencyId, Guid patientId, Guid assessmentId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == assessmentId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type).ToString();
            }
            return assessment;
        }

        public T FindAny<T>( Guid agencyId, Guid assessmentId, Guid patientId, Guid episodeId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type).ToString();
            }
            return assessment;
        }

        public List<AssessmentExport> FindAnyByStatusLean(Guid agencyId, Guid branchId, string[] assessmentTypes, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
          
                var patientStatusQuery = string.Empty;
                if (patientStatus <= 0)
                {
                    patientStatusQuery = " AND pr.Status IN (1,2) ";
                }
                else
                {
                    patientStatusQuery = " AND pr.Status = @patientstatusid ";
                }

                var dateRange = string.Empty;
                if (status == (int)ScheduleStatus.OasisExported || status == (int)ScheduleStatus.OasisCompletedNotExported )
                {
                    dateRange = " AND ast.ExportedDate between @startdate and @enddate ";
                }
                else if (status == (int)ScheduleStatus.OasisCompletedExportReady)
                {
                    dateRange = " AND ast.AssessmentDate between @startdate and @enddate ";
                }
                var branchScript = !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty;

                var script = string.Format(@"SELECT 
                                pr.Id as PatientId, 
                                pr.FirstName as FirstName, 
                                pr.LastName as LastName, 
                                pr.PrimaryInsurance as InsuranceId , 
                                ast.Id  as Id , 
                                ast.VersionNumber as VersionNumber, 
                                ast.AssessmentDate as AssessmentDate , 
                                ast.Type as Type ,
                                ast.Modified as Modified , 
                                ast.ExportedDate as ExportedDate , 
                                ast.EpisodeId as EpisodeId, 
                                pe.EndDate as EndDate, 
                                pe.StartDate as StartDate ,
                                st.EventDate as EventDate ,
                                st.VisitDate as VisitDate 
                                        FROM assessments ast
                                                INNER JOIN patientprofiles pr ON ast.PatientId = pr.Id
                                                INNER JOIN patientepisodes pe ON  ast.EpisodeId = pe.Id 
                                                INNER JOIN scheduletasks st ON  ast.Id = st.Id
                                                        WHERE 
                                                              pr.AgencyId = @agencyid {0} {1} AND
                                                              pr.IsDeprecated = 0 AND
                                                              pe.IsDischarged = 0 AND
                                                              pe.IsActive = 1 AND 
                                                              st.IsMissedVisit = 0 AND
                                                              st.IsDeprecated = 0 AND
                                                              DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                              ast.Type IN ( {3} ) {2} AND 
                                                              ast.Status = @status ", branchScript, patientStatusQuery, dateRange, assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    assessments = cmd.SetConnection(connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("patientstatusid", patientStatus)
                       .AddDateTime("startDate", startDate)
                       .AddDateTime("endDate", endDate)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           EventDate = reader.GetDateTime("EventDate"),
                           VisitDate = reader.GetDateTime("VisitDate"),
                           AssessmentType = reader.GetStringNullable("Type"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            return assessments;
        }

        public List<AssessmentExport> FindAnyByStatusLean(Guid agencyId, Guid branchId, string[] assessmentTypes, int status, List<int> paymentSources)
        {
            var results = new List<AssessmentExport>();
            if (paymentSources != null && paymentSources.Count > 0)
            {
                var branchScript = !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty;
                var script = string.Format(
                    @"SELECT 
                        pr.Id as PatientId,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName, 
                        pr.PrimaryInsurance as InsuranceId, 
                        pr.PaymentSource as PaymentSources,
                        ast.Id  as Id,
                        ast.VersionNumber as VersionNumber,
                        ast.AssessmentDate as AssessmentDate, 
                        ast.Modified as Modified, 
                        ast.ExportedDate as ExportedDate, 
                        ast.EpisodeId as EpisodeId, 
                        ast.Type as Type,
                        (ast.SubmissionFormat Is NULL || ast.SubmissionFormat = '') as IsSubmissionEmpty, 
                        ast.IsValidated as IsValidated, 
                        pe.EndDate as EndDate, 
                        pe.StartDate as StartDate, 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate 
                                FROM assessments ast
                                    INNER JOIN patientprofiles pr ON ast.PatientId = pr.Id
                                    INNER JOIN patientepisodes pe ON  ast.EpisodeId = pe.Id 
                                    INNER JOIN scheduletasks st ON  ast.Id = st.Id
                                            WHERE
                                                  pr.AgencyId = @agencyid  {0} AND 
                                                  pr.Status IN (1,2) AND 
                                                  pr.PaymentSource REGEXP '{1}' AND 
                                                  pr.IsDeprecated = 0 AND 
                                                  pe.IsDischarged = 0 AND 
                                                  pe.IsActive = 1 AND 
                                                  st.IsMissedVisit = 0 AND
                                                  st.IsDeprecated = 0 AND
                                                  DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                  ast.Type IN ( {2} ) AND 
                                                  ast.Status = @status ", branchScript, paymentSources.Select(d => "[[:<:]]" + d + "[[:>:]]").ToArray().Join("|"), assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    results = cmd.SetConnection(connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           EventDate = reader.GetDateTime("EventDate"),
                           VisitDate = reader.GetDateTime("VisitDate"),
                           AssessmentType = reader.GetString("Type"),
                           IsValidated = reader.GetBoolean("IsValidated"),
                           IsSubmissionEmpty = reader.GetBoolean("IsSubmissionEmpty"),
                           PaymentSources = reader.GetStringNullable("PaymentSources"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            }
            return results;



        }

        public List<Assessment> GetAssessmentSupplies(Guid agencyId, Guid patientId, List<Guid> assessmentIds)
        {
            var script = string.Format(@"SELECT
                        ast.Id,
                        ast.Supply 
                            FROM 
                                assessments ast
                                        WHERE 
                                            ast.AgencyId = @agencyid AND
                                            ast.PatientId = @patientid AND
                                            ast.Id IN ({0}) ", assessmentIds.Select(d => string.Format("'{0}'", d)).ToArray().Join(","));

            var list = new List<Assessment>();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    Supply = reader.GetStringNullable("Supply")
                })
                .AsList();

            }
            return list;
        }

        public IList<string> FindAssessmentFields(string assessmentType)
        {
            var results = new List<string>();
            var formats = new List<object>();
            var script = string.Format(
                @"SELECT ElementName " +
                "FROM submissionbodyformats " +
                "WHERE {0} = 1 AND IsIgnorable = 0 AND ElementName != ''", assessmentType);

            using (var cmd = new FluentCommand<object>(script))
            {
                formats = cmd.SetConnection(connectionStringName)
                   .SetMap(reader =>
                       reader.GetStringNullable("ElementName")
                   ).AsList();
            }
            if (formats != null && formats.Count > 0)
            {
                results = formats.Where(s => s.ToString().IsNotNullOrEmpty()).Select(s => s.ToString().Remove(0, 5)).ToList<string>();
                if (assessmentType == "RFA01" || assessmentType == "RFA03")
                {
                    for (int count = 1; count < 5; count++)
                    {
                        results.Add("InpatientFacilityProcedure" + count);
                    }
                    for (int count = 1; count < 7; count++)
                    {
                        results.Add("InpatientFacilityDiagnosis" + count);
                        results.Add("MedicalRegimenDiagnosis" + count);
                    }
                }
                if (assessmentType == "RFA01" || assessmentType == "RFA03" || assessmentType == "RFA05"
                    || assessmentType == "RFA04")
                {
                    results.Add("PrimaryDiagnosis");
                    results.Add("PrimaryDiagnosisDate");
                    int character = 65;
                    for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
                    {
                        results.Add("PrimaryDiagnosis" + diagnosisCount);
                        results.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
                        results.Add("PaymentDiagnoses" + (char)character + "3");
                        results.Add("PaymentDiagnoses" + (char)character + "4");
                        character++;
                    }
                    character = 71;
                    for (int diagCount2 = 6; diagCount2 < 26; diagCount2++)
                    {
                        results.Add("ICD9M" + (char)character + "3");
                        results.Add("ICD9M" + (char)character + "4");
                        results.Add("ICD9M" + diagCount2);
                        results.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
                        results.Add("OtherDiagnose" + diagCount2 + "Rating");
                        character++;
                    }
                    results.Add("PaymentDiagnosesZ3");
                    results.Add("PaymentDiagnosesZ4");
                }
                results.Add("PressureUlcerLengthDecimal");
                results.Add("PressureUlcerWidthDecimal");
                results.Add("PressureUlcerDepthDecimal");
            }
            return results;
        }

        public bool Add(Assessment assessment) 
        {
            if (assessment != null)
            {
                return this.InsertAny(assessment);
            }
            return false;
        }

        public bool AddMultiple(List<Assessment> assessments)
        {
            if (assessments != null && assessments.Count > 0)
            {
                return this.InsertAnyMultiple(assessments);
            }
            return false;
        }

        /// <summary>
        /// WARNING: THIS METHOD WILL DELETE THE ASSESSMENTS COMPLETLY FROM THE DATABASE
        /// </summary>
        /// <param name="assessments"></param>
        /// <returns></returns>
        public bool RemoveMultiple(List<Assessment> assessments)
        {
            if (assessments != null && assessments.Count > 0)
            {
                return database.DeleteMany(assessments) > 0;
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                return this.UpdateAny(assessment);
               
            }
            return result;
        }

        public bool UpdateModal(Assessment assessment) 
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                return this.UpdateAny(assessment);
            }
            return result;
        }

        public Assessment Get( Guid agencyId,Guid assessmentId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, assessmentId);
            }
            return assessment;
        }

        public Assessment Get(Guid agencyId, Guid patientId, Guid assessmentId) 
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, patientId, assessmentId);
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, assessmentId);
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId, Guid patientId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !patientId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, patientId, assessmentId);
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid patientId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE assessments ast set ast.IsDeprecated = {0}, ast.Modified = @modified WHERE ast.AgencyId = @agencyid AND ast.PatientId = @patientid  AND ast.Id = @assessmentId ", isDeprecated ? 1 : 0);
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("assessmentId", assessmentId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid patientId, Guid assessmentId, Guid employeeId)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty()  && !patientId.IsEmpty()  && !employeeId.IsEmpty())
            {
                var assessment = this.FindAny<Assessment>(agencyId, patientId, assessmentId);
                if (assessment != null)
                {
                    try
                    {
                        assessment.UserId = employeeId;
                        assessment.Modified = DateTime.Now;
                        this.UpdateAny(assessment);
                        result = true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
                
            }
            return result;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty()  && status > 0)
            {
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !AssessmentTypeFactory.NonCMSAssessments().Contains((int)d)).Select(d => d.ToString()).ToArray();
                if (assessmentTypes.IsNotNullOrEmpty())
                {
                    assessments = this.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, patientStatus, startDate, endDate);
                }
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !AssessmentTypeFactory.NonCMSAssessments().Contains((int)d)).Select(d => d.ToString()).ToArray();
                if (assessmentTypes.IsNotNullOrEmpty())
                {
                    assessments = this.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, paymentSources);
                }
            }
            return assessments;
        }

        public List<string> AssessmentType05Generics()
        {
            return new List<string>() { "AllergiesDescription", "PulseApical", "PulseApicalRegular",
                "PulseApicalIrregular", "LastVisitDate", "GenericHeight", "PulseRadial", "PulseRadialReqular",
                "PulseRadialIrregular", "Weight", "WeightActualStated", "WeightActualStated", "Temp", "Resp",
                "BPLeftLying", "BPRightLying", "BPSittingLeft", "BPSittingRight", "BPStandingLeft", "BPStandingRight",
                "TempGreaterThan", "TempLessThan", "PulseGreaterThan", "PulseLessThan", "RespirationGreaterThan",
                "RespirationLessThan", "SystolicBPGreaterThan", "SystolicBPLessThan", "DiastolicBPGreaterThan",
                "DiastolicBPLessThan", "02SatLessThan", "FastingBloodSugarGreaterThan", "FastingBloodSugarLessThan",
                "RandomBloddSugarGreaterThan", "RandomBloodSugarLessThan", "WeightGreaterThan", "Pnemonia", "PnemoniaDate",
                "Flu", "FluDate", "TB", "TBDate", "TBExposure", "TBExposureDate", "AdditionalImmunization1Name", "AdditionalImmunization1",
                "AdditionalImmunization1Date", "AdditionalImmunization2Name", "AdditionalImmunization2", "AdditionalImmunization2Date",
                "ImmunizationComments", "Allergies", "SurgicalProcedureDescription1", "SurgicalProcedureCode1", "SurgicalProcedureCode1Date",
                "SurgicalProcedureDescription2", "SurgicalProcedureCode2", "SurgicalProcedureCode2Date"
            };
        }


        #endregion

    }
}
