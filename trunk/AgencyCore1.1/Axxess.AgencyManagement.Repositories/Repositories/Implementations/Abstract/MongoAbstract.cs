﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

using Axxess.Core.Extension;

using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Repositories.Extensions;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public abstract class MongoAbstract
    {
        protected MongoDatabase db;

        protected MongoAbstract(MongoDatabase db)
        {
            this.db = db;
        }

        #region Patient Visit Note

        public bool AddNoteQuestion(QuestionData data)
        {
            return db.AddData<QuestionData>("patientvisitnote", data);
        }

        public bool UpdateNoteQuestion(QuestionData data)
        {
            return db.UpdateData<QuestionData>(data.AgencyId, data.PatientId, data.Id, "patientvisitnote", data);
        }

        public bool AddManyNoteQuestions(List<QuestionData> datas)
        {
            return db.AddDatas<QuestionData>("patientvisitnote", datas);
        }

        public QuestionData GetNoteQuestion(Guid agencyId, Guid noteId)
        {
            return db.GetData<QuestionData>(agencyId, noteId, "patientvisitnote");
        }

        public List<QuestionData> GetManyNoteQuestions(Guid agencyId, List<Guid> noteIds)
        {
            return db.GetManyDatas<QuestionData>(agencyId, noteIds, "patientvisitnote");
        }

        #endregion

        #region OASIS

        public bool AddAssessmentQuestion(AssessmentQuestionData data)
        {
            return db.AddData<AssessmentQuestionData>("assessment", data);
        }

        public bool UpdateAssessmentQuestion(AssessmentQuestionData data)
        {
            return db.UpdateData<AssessmentQuestionData>(data.AgencyId, data.PatientId, data.Id, "assessment", data);
        }

        public bool AddManyAssessmentQuestions(List<AssessmentQuestionData> datas)
        {
            return db.AddDatas<AssessmentQuestionData>("assessment", datas);
        }

        public AssessmentQuestionData GetAssessmentQuestion(Guid agencyId, Guid id)
        {
            return db.GetData<AssessmentQuestionData>(agencyId, id, "assessment");
        }

        public List<AssessmentQuestionData> GetManyAssessmentQuestions(Guid agencyId, List<Guid> assessmentIds)
        {
            return db.GetManyDatas<AssessmentQuestionData>(agencyId, assessmentIds, "assessment");
        }

        #endregion

        #region Plan Of Care

        public bool AddPlanofcareQuestion(AssessmentQuestionData data)
        {
            return db.AddData<AssessmentQuestionData>("planofcare", data);
        }

        public bool UpdatePlanofcareQuestion(AssessmentQuestionData data)
        {
            return db.UpdateData<AssessmentQuestionData>(data.AgencyId, data.PatientId, data.Id, "planofcare", data);
        }

        public AssessmentQuestionData GetPlanofcareQuestion(Guid agencyId, Guid id)
        {
            return db.GetData<AssessmentQuestionData>(agencyId, id, "planofcare");
        }

        #endregion

        #region Managed claim insurance

        public bool AddClaimInsurance<C>(C data) where C : ClaimInsurance
        {
            return db.AddData<C>(typeof(C).Name.ToString().ToLowerCase(), data);
        }

        public bool UpdateClaimInsurance<C>(C data) where C : ClaimInsurance
        {
            return db.UpdateData<C>(data.AgencyId, data.PatientId, data.Id, typeof(C).Name.ToString().ToLowerCase(), data);
        }

        public C GetClaimInsurance<C>(Guid agencyId, Guid id) where C : ClaimInsurance
        {
            return db.GetData<C>(agencyId, id, typeof(C).Name.ToString().ToLowerCase());
        }

        public List<ChargeRate> GetClaimInsuranceRates<C>(Guid agencyId, Guid id) where C : ClaimInsurance
        {
            var rates = new List<ChargeRate>();
            var claimInsurance = this.GetClaimInsurance<C>(agencyId, id);
            if (claimInsurance != null)
            {
                if (claimInsurance.Insurance != null && claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
                {
                    rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                }
            }
            return rates;
        }

        public List<ChargeRate> GetClaimInsuranceRates(Guid agencyId, Guid id, ClaimTypeSubCategory type)
        {
            var rates = new List<ChargeRate>();

            switch (type)
            {
                case ClaimTypeSubCategory.ManagedCare:
                    rates = this.GetClaimInsuranceRates<ManagedClaimInsurance>(agencyId, id);
                    break;
                case ClaimTypeSubCategory.Secondary:
                    rates = this.GetClaimInsuranceRates<SecondaryClaimInsurance>(agencyId, id);
                    break;
                case ClaimTypeSubCategory.Final:
                    rates = this.GetClaimInsuranceRates<FinalClaimInsurance>(agencyId, id);
                    break;
                case ClaimTypeSubCategory.RAP:
                    rates = this.GetClaimInsuranceRates<RapClaimInsurance>(agencyId, id);
                    break;
            }

            return rates;
        }

        public List<C> GetManyClaimInsurances<C>(Guid agencyId, List<Guid> ids) where C : ClaimInsurance
        {
            return db.GetManyDatas<C>(agencyId, ids, typeof(C).Name.ToString().ToLowerCase());
        }

        #endregion

    }
}
