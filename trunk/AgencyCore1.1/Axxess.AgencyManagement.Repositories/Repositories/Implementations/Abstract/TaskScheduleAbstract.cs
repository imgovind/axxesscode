﻿
namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    using MySql.Data.MySqlClient;
    using SubSonic.DataProviders;
    using SubSonic.Query;

    public abstract class TaskScheduleAbstract<T> : ITaskSchedule<T> where T : ITask, new()
    {
        protected SimpleRepository database;
        public string connectionStringName;
        protected AgencyServices Service { get; set; }

        public TaskScheduleAbstract(SimpleRepository database, string connectionStringName)
        {
            this.database = database;
            this.connectionStringName = connectionStringName;
        }

        public bool AddScheduleTask(T scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Add<T>(scheduleEvent);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool AddMultipleScheduleTask(List<T> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    database.AddMany<T>(scheduleEvents);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleTask(T scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                result = database.Update<T>(scheduleEvent) > 0;
            }
            return result;
        }

        public bool UpdateScheduleTaskPartial(Guid agencyId, Guid patientId, Guid eventId, int status)
        {
            return UpdateScheduleTaskPartial(agencyId, patientId, eventId, status, null, (bool?)null);
        }

        public bool UpdateScheduleTaskPartial(Guid agencyId, Guid patientId, Guid eventId, int status, bool isPrintQueue)
        {
            return UpdateScheduleTaskPartial(agencyId, patientId, eventId, status, null, isPrintQueue);
        }

        public bool UpdateScheduleTaskPartial(Guid agencyId, Guid patientId, Guid eventId, int status, string asset)
        {
            try
            {
                var query = new SubSonic.Query.Update<T>(ProviderFactory.GetProvider(connectionStringName)).Where(w => w.AgencyId == agencyId && w.PatientId == patientId && w.Id == eventId);
                if (status != 0)
                {
                    query.Set(s => s.Status == status);
                }
                if (asset != null)
                {
                    query.Set(s => s.Asset == asset);
                }
                return query.Execute() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateScheduleTaskPartial(Guid agencyId, Guid patientId, Guid eventId, int status, string asset, bool? isPrintQueue)
        {
            bool result = false;
            try
            {
                var query = new SubSonic.Query.Update<T>(ProviderFactory.GetProvider(connectionStringName)).Where(w => w.AgencyId == agencyId && w.PatientId == patientId && w.Id == eventId);
                if (status != 0)
                {
                    query.Set(s => s.Status == status);
                }
                if (asset != null)
                {
                    query.Set(s => s.Asset == asset);
                }
                if (isPrintQueue.HasValue)
                {
                    query.Set(s => s.InPrintQueue == isPrintQueue.Value);
                }
                result = query.Execute() > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        /// <summary>
        /// Deletes the schedule task from the database.
        /// WARNING: This uses the SQL Delete command instead of setting IsDeprecated to true.
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public bool RemoveScheduleTaskFully(Guid agencyId, Guid patientId, Guid eventId)
        {
            return EntityHelper.RemovePatientEntity<T>(agencyId, patientId, eventId, connectionStringName);
        }

        /// <summary>
        /// Deletes the schedule task from the database.
        /// WARNING: This uses the SQL Delete command instead of setting IsDeprecated to true.
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public bool RemoveMultipleScheduleTaskFully(Guid agencyId, Guid patientId, List<Guid> eventIds)
        {
            return EntityHelper.RemoveMultiplePatientEntities<T>(agencyId, patientId, eventIds, connectionStringName);
        }

        public T GetScheduleTask(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<T>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == eventId);
        }

        public T GetUserScheduleTask(Guid agencyId, Guid userId, Guid eventId)
        {
            return database.Single<T>(a => a.AgencyId == agencyId && a.UserId == userId && a.Id == eventId);
        }

        public T GetScheduleTask(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            return database.Single<T>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EpisodeId == episodeId && a.Id == eventId);
        }

        public T GetScheduleTaskByUserId(Guid agencyId, Guid userId, Guid id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(id, "id");
            try
            {
                return database.Single<T>(v => v.AgencyId == agencyId && v.UserId == userId && v.Id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Dictionary<string, string> GetScheduleTaskByColumns(Guid agencyId, Guid patientId, Guid eventId, params string[] columns)
        {
            return EntityHelper.SelectPatientEntityJsonByColumns<T>(agencyId, patientId, eventId, connectionStringName, columns);
        }

        public IList<T> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid patientId, Guid employeeId)
        {
            return database.Find<T>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }

        public IList<T> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid employeeId)
        {
            return database.Find<T>(e => e.AgencyId == agencyId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }

        public abstract List<T> GetScheduleByUserIdLean(Guid agencyId, Guid patientIdOptional, Guid userId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded);

        //public bool UpdateScheduleEventsForReassign(Guid agencyId, Guid userId, string ids)
        public abstract List<T> GetScheduleEventsVeryLeanByIds(Guid agencyId, Guid patientId, List<Guid> ids, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);

        public abstract List<T> GetPatientScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);

        //public abstract List<T> GetCurrentAndPreviousOrders(Guid agencyId, PatientEpisode episode, int[] disciplineTasks);

        public abstract List<T> GetUsersScheduleTasksBetweenDatesByStatus(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, params int[] scheduleStatus);

        public abstract List<T> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        public bool UpdateScheduleTasksForReassign(Guid agencyId, Guid userId, string ids)
        {
            bool result = false;
            if (ids.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.UserId = @userid WHERE st.AgencyId = @agencyid AND st.Id IN ( {0})", ids);
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, List<T> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Empty;
                    var billableTasks = scheduleEvents.Where(t => t.IsBillable);
                    var nonbillableTasks = scheduleEvents.Where(t => !t.IsBillable);
                    if (billableTasks.IsNotNullOrEmpty())
                    {
                        script = string.Format(@"UPDATE scheduletasks st set st.IsBillable = 1 WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id IN ( {0}); ", billableTasks.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    }
                    if (nonbillableTasks.IsNotNullOrEmpty())
                    {
                        script += string.Format(@"UPDATE scheduletasks st set st.IsBillable = 0 WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id IN ( {0}); ", nonbillableTasks.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    }
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        #region Delete Tasks

        public bool ToggleScheduledTaskDelete(Guid agencyId, T scheduleTask, bool isDeprecated)
        {
            return ToggleScheduledTasksDelete(agencyId, scheduleTask.PatientId, new List<Guid>() { scheduleTask.Id }, isDeprecated);
        }

        public bool ToggleScheduledTasksDelete(Guid agencyId, Guid patientId, List<Guid> eventIds, bool isDeprecated)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.IsDeprecated = {0} WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid  AND st.Id IN ( {1})", isDeprecated ? 1 : 0, eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool ToggleScheduledEventsPaid(Guid agencyId, List<Guid> eventIds, bool isVisitPaid)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.IsVisitPaid = {0} WHERE st.AgencyId = @agencyid AND  st.Id IN ( {1})", isVisitPaid ? 1 : 0, eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        #endregion

        public abstract List<T> GetPreviousNotes(Guid agencyId, T scheduledEvent, int[] disciplineTasks, int[] status);

        public abstract List<T> GetPreviousNotes(Guid agencyId, T scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status);

        public List<T> GetScheduledEventsLeanWithId(Guid agencyId, Guid patientId, List<Guid> eventIds)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.Discipline as Discipline 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.Id IN ( {0} ) AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 ", eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)

                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientId = patientId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                })
                .AsList();
            }
            return list;
        }

        public abstract List<T> GetDeletedTasks(Guid agencyId, Guid patientId);

        public int MarkScheduleTasksAsPrinted(Guid agencyId, List<Guid> ids)
        {
            int result = 0;
            if (ids != null && ids.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.InPrintQueue = 0 WHERE st.AgencyId = @agencyid AND st.Id IN ( {0})", ids.Select(d => "'" + d.ToString() + "'").ToArray().Join(","));
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        result = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AsNonQuery();
                    }
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public abstract List<T> GetScheduleTasksByStatusDisciplineAndRange(Guid agencyId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks);

        public abstract List<T> GetScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status);

        public abstract List<T> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid[] episodeIds, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange);

        #region Return Comments

        public bool AddReturnComment(ReturnComment returnComment)
        {
            int id;
            return AddReturnComment(returnComment, out id);
        }

        public bool AddReturnComment(ReturnComment returnComment, out int id)
        {
            id = 0;
            bool result = false;
            try
            {

                if (returnComment != null)
                {
                    returnComment.Created = DateTime.Now;
                    returnComment.Modified = DateTime.Now;

                    var obj = database.Add<ReturnComment>(returnComment);
                    id = obj.ToString().ToInteger();
                    result = id > 0;
                }
            }
            catch (Exception e)
            {
                
            }
            return result;
        }

        public bool UpdateReturnComment(ReturnComment returnComment)
        {
            var result = false;
            try
            {
                if (returnComment != null)
                {
                    returnComment.Modified = DateTime.Now;
                    database.Update<ReturnComment>(returnComment);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveReturnComment(int id)
        {
            return database.Delete<ReturnComment>(id) > 0;
        }

        public bool DeleteReturnComments(Guid agencyId, int id)
        {
            var returnComment = database.Single<ReturnComment>(a => a.AgencyId == agencyId && a.Id == id);
            if (returnComment != null)
            {
                returnComment.IsDeprecated = true;
                returnComment.Modified = DateTime.Now;
                database.Update<ReturnComment>(returnComment);
                return true;
            }
            return false;
        }

        public ReturnComment GetReturnComment(Guid agencyId, int id)
        {
            return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.Id == id).FirstOrDefault();
        }

        public List<ReturnComment> GetReturnComments(Guid agencyId, Guid episodeId, Guid eventId)
        {
            return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.EpisodeId == episodeId && a.EventId == eventId && a.IsDeprecated == false).ToList();

        }

        public List<ReturnComment> GetReturnCommentsWithUserInfo(Guid agencyId,Guid eventId)
        {
            var script = string.Format(@"SELECT 
                        rc.Id as Id , 
                        rc.Comments as Comments ,
                        rc.Modified as Modified ,
                        rc.UserId as UserId , 
                        au.FirstName as FirstName , 
                        au.LastName as LastName , 
                        au.Suffix as Suffix ,
                        au.Credentials as Credentials ,
                        au.CredentialsOther as CredentialsOther ,
                        au.IsDeprecated as IsDeprecated 
                            FROM
                                returncomments as rc
                                    LEFT JOIN agencymanagement.users as au ON rc.UserId = au.Id 
                                        WHERE 
                                            rc.AgencyId = @agencyid AND 
                                            rc.EventId = @eventid AND 
                                            rc.IsDeprecated = 0");

            var list = new List<ReturnComment>();
            using (var cmd = new FluentCommand<ReturnComment>(script))
            {
                list = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", eventId)
                .SetMap(reader => new ReturnComment
                {
                    Id = reader.GetInt("Id"),
                    Comments = reader.GetStringNullable("Comments"),
                    Modified = reader.GetDateTime("Modified"),
                    UserId = reader.GetGuid("UserId"),
                    User = new User
                    {
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        Suffix = reader.GetStringNullable("Suffix"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated")
                    }
                })
                .AsList();
            }
            return list;
        }

        public abstract string GetReturnReason(Guid agencyId, Guid patientId,  Guid eventId, Guid currentUserid);

        public List<ReturnComment> GetReturnComments(Guid agencyId, Guid eventId)
        {
            return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.EventId == eventId && a.IsDeprecated == false).ToList();
        }

        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, Guid episodeId)
        {
            var list = new List<ReturnComment>();
            var script = string.Format(@"SELECT 
                                            rc.Id,
                                            rc.EpisodeId,
                                            rc.EventId,
                                            rc.UserId,
                                            rc.Comments,
                                            rc.Modified
                                                FROM 
                                                    returncomments rc 
                                                        WHERE
                                                            rc.AgencyId = @agencyid AND
                                                            rc.EpisodeId = @episodeId AND
                                                            rc.IsDeprecated = 0");
            using (var cmd = new FluentCommand<ReturnComment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("episodeId", episodeId)
                 .SetMap(reader => new ReturnComment
                 {
                     Id = reader.GetInt("Id"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     EventId = reader.GetGuid("EventId"),
                     UserId = reader.GetGuid("UserId"),
                     Comments = reader.GetStringNullable("Comments"),
                     Modified = reader.GetDateTime("Modified")
                 }).AsList();
            }
            return list;
        }

        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds)
        {
            var list = new List<ReturnComment>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var ids = episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            rc.Id,
                                            rc.EpisodeId,
                                            rc.EventId,
                                            rc.UserId,
                                            rc.Comments,
                                            rc.Modified
                                                FROM 
                                                    returncomments rc
                                                        WHERE
                                                            rc.AgencyId = @agencyid AND
                                                            rc.EpisodeId IN ( {0} )AND
                                                            rc.IsDeprecated = 0", ids);
                using (var cmd = new FluentCommand<ReturnComment>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ReturnComment
                     {
                         Id = reader.GetInt("Id"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         EventId = reader.GetGuid("EventId"),
                         UserId = reader.GetGuid("UserId"),
                         Comments = reader.GetStringNullable("Comments"),
                         Modified = reader.GetDateTime("Modified")
                     }).AsList();
                }
            }
            return list;
        }

        public bool HasReturnReasons(Guid agencyId, Guid episodeId, Guid eventId)
        {
            bool result;
            var script = @"SELECT EXISTS(SELECT *
                            FROM
                                returncomments rc
                                        WHERE 
                                            rc.AgencyId = @agencyid AND 
                                            rc.EpisodeId = @episodeid AND 
                                            rc.EventId = @eventid AND 
                                            rc.IsDeprecated = 0)";
            using (var cmd = new FluentCommand<int>(script))
            {
                result = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("episodeid", episodeId)
                .AddGuid("eventid", eventId)
                .AsNonQuery() > 0;
            }
            return result;
        }

        #endregion

        #region  Missed Visit

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Add<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Update<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid id)
        {
            return database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == id);
        }

        public MissedVisit GetMissedVisitForPrint(Guid agencyId, Guid patientId, Guid id)
        {
            MissedVisit visit = null;
            var script = string.Format(@"SELECT 
                            m.PatientId, 
                            m.Date, 
                            m.Status, 
                            Reason, 
                            m.Comments, 
                            IsOrderGenerated, 
                            IsPhysicianOfficeNotified, 
                            SignatureText, 
                            SignatureDate,
                            {0} as EventDate,
                            st.DisciplineTask,
                            pp.AgencyLocationId,
                            pp.FirstName,
                            pp.LastName,
                            pp.MiddleInitial,
                            pp.PatientIdNumber
                                FROM missedvisits m 
                                INNER JOIN scheduletasks st on m.Id = st.Id 
                                INNER JOIN patientprofiles pp on m.PatientId = pp.Id
                                    WHERE m.Id = @id AND m.PatientId = @patientid AND m.AgencyId = @agencyid", GetSelectAppSpecificSql(SqlSelectType.EventDate));
            using (var cmd = new FluentCommand<MissedVisit>(script))
            {
                visit = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", id)
                .SetMap(reader => new MissedVisit
                {
                    Id = id,
                    PatientId = reader.GetGuid("PatientId"),
                    AgencyId = agencyId,
                    Date = reader.GetDateTime("Date"),
                    Status = reader.GetInt("Status"),
                    Reason = reader.GetStringNullable("Reason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
                    IsPhysicianOfficeNotified = reader.GetBoolean("IsPhysicianOfficeNotified"),
                    SignatureText = reader.GetStringNullable("SignatureText"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                    EventDate = reader.GetDateTime("EventDate").ToZeroFilled(),
                    DisciplineTaskName = DisciplineTaskFactory.ConvertDisciplineTaskToString(reader.GetInt("DisciplineTask")),
                    PatientProfile = new PatientProfileLean()
                                     {
                                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                                         AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                                         FirstName = reader.GetStringNullable("FirstName"),
                                         LastName = reader.GetStringNullable("LastName"),
                                         MiddleInitial = reader.GetStringNullable("MiddleInitial")
                                     }
                }).AsSingle();
            }
            return visit;
        }

        //public List<MissedVisit> GetMissedVisitsOnly(Guid agencyId, DateTime startDate, DateTime endDate)
        //{
        //    var missedVisits = database.Find<MissedVisit>(m => m.AgencyId == agencyId && m.Date >= startDate && m.Date <= endDate).ToList();
        //    return missedVisits;
        //}

        public List<MissedVisit> GetMissedVisitsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status)
        {
            var list = new List<MissedVisit>();
            var script = @"SELECT * 
                             FROM missedvisits m 
                                 WHERE 			                                 
                                 m.Date between @startDate AND @endDate AND
                                 m.Status = @status";

            using (var cmd = new FluentCommand<MissedVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("status", status)
                .SetMap(reader => new MissedVisit
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    AgencyId = agencyId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Date = reader.GetDateTime("Date"),
                    Status = reader.GetInt("Status"),
                    Reason = reader.GetStringNullable("Reason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
                    IsPhysicianOfficeNotified = reader.GetBoolean("IsPhysicianOfficeNotified"),
                    SignatureText = reader.GetStringNullable("SignatureText"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                }).AsList();
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.ToCommaSeperatedList();
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status,
                                            m.Reason,
                                            m.Comments
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         Reason = reader.GetStringNullable("Reason"),
                         Comments = reader.GetStringNullable("Comments")
                     }).AsList();
                }
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, int status, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.ToCommaSeperatedList();
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.Status = @status AND
                                                            m.Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddInt("status", status)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status")
                     }).AsList();
                }
            }
            return list;
        }

        #endregion

        public List<T> GetScheduledEventsOnlyWithId(Guid agencyId, Guid patientId,int payorId, string eventIds, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask ,
                        {0}
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue ,
                        st.Version  as Version 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.PayorId = @payorId AND 
                                            st.Id IN ( {1} ) AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            DATE({2}) between DATE(@startdate) and DATE(@enddate) {3}
                                                ORDER BY DATE({2}) DESC ", GetSelectAppSpecificSql(SqlSelectType.All), eventIds, GetWhereAppSpecificSql(SqlSelectType.EventDate), additionalScript);

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddInt("payorId", payorId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap((reader) =>
                {
                    var task = new T
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Surcharge = reader.GetStringNullable("Surcharge"),
                        AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        Version = reader.GetInt("Version")
                    };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return list;
        }

        public List<T> GetScheduledEventsOnly(Guid agencyId, Guid patientId, int payorId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        {0}
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue ,
                        pe.StartDate as StartDate ,
                        pe.EndDate as EndDate ,
                        st.Version as Version 
                            FROM 
                                 scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.PayorId = @payorId AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            pe.IsActive = 1  AND 
                                            pe.IsDischarged = 0  AND
                                            DATE({1}) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) {2}
                                                ORDER BY DATE({1}) DESC ", 
                                                                         GetSelectAppSpecificSql(SqlSelectType.All), 
                                                                         GetWhereAppSpecificSql(SqlSelectType.EventDate), 
                                                                         additionalScript); //AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddInt("payorId", payorId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        Surcharge = reader.GetStringNullable("Surcharge"),
                        AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        Version = reader.GetInt("Version")
                    };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                }
                )
                .AsList();
            }
            return list;
        }

        public abstract List<T> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);

        public List<T> GetScheduleEventsVeryLeanByIds(Guid agencyId, List<Guid> ids)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId,
                        st.PatientId as PatientId,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.IsBillable as IsBillable,
                        st.Discipline as Discipline  
                            FROM 
                                 scheduletasks st 
                                        WHERE
                                            st.AgencyId = @agencyId  AND 
                                            st.IsDeprecated = 0  AND
                                            st.Id IN ( {0} )  ", ids.Select(d => string.Format("'{0}'", d)).ToArray().Join(","));

            var scheduleList = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Id = reader.GetGuid("Id"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<T> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        {0}
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 
                                            ORDER BY DATE({1}) DESC ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate));

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap((reader) =>
                {
                    var task = new T
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        EpisodeId = episodeId,
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue")
                    };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                }
                )
                .AsList();
            }
            return list;
        }

        public T GetScheduleTaskForNoteEdit(Guid agencyId, Guid patientId, Guid eventId)
        {
            var script = string.Format(@"SELECT 
                        st.EpisodeId ,
                        st.UserId,
                        st.DisciplineTask  , 
                        st.Status ,
                        {0}
                        st.Discipline ,
                        st.IsBillable ,
                        st.Version ,
                        st.Surcharge ,
                        st.AssociatedMileage ,
                        st.IsOrderForNextEpisode ,
                        (SELECT EXISTS(SELECT * FROM returncomments rc
                            WHERE 
                                rc.AgencyId = st.AgencyId AND
                                rc.EpisodeId = st.EpisodeId AND
                                rc.EventId = st.Id AND 
                                rc.IsDeprecated = 0)) as 'HasReturnComments' 
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.Id = @eventid AND
                                        st.IsDeprecated = 0", GetSelectAppSpecificSql(SqlSelectType.All));

            T task = null;
            using (var cmd = new FluentCommand<T>(script))
            {
                task = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("eventid", eventId)
                .SetMap((reader) =>
                {
                    var scheduleTask = new T
                    {
                        Id = eventId,
                        PatientId = patientId,
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        Version = reader.GetInt("Version"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        Surcharge = reader.GetStringNullable("Surcharge"),
                        AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                        HasReturnComments = reader.GetBoolean("HasReturnComments")
                    };
                    GetSelectAppSpecificSql(scheduleTask, reader);
                    return scheduleTask;
                }).AsSingle();
            }
            return task;
        }

        public List<T> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.Version,
                        pe.StartDate as StartDate ,
                        pe.EndDate as EndDate 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            pe.IsActive = 1  AND 
                                            pe.IsDischarged = 0  AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) {2}
                                                ORDER BY DATE({1}) DESC ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additionalScript);

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                   {
                       var task = new T
                           {
                               Id = reader.GetGuid("Id"),
                               UserId = reader.GetGuid("UserId"),
                               DisciplineTask = reader.GetInt("DisciplineTask"),
                               Status = reader.GetInt("Status", 0),
                               IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                               Version = reader.GetInt("Version"),
                               StartDate = reader.GetDateTime("StartDate"),
                               EndDate = reader.GetDateTime("EndDate"),
                           };
                       GetSelectAppSpecificSql(task, reader);
                       return task;
                   })
                .AsList();
            }
            return list;
        }

        public List<T> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid ";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId ";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id
                                        WHERE
                                            st.AgencyId = @agencyid  AND 
                                            st.IsDeprecated = 0 AND
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate)  {2} ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additonalScript);

            var scheduleList = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<T> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty;
            if (patientStatus <= 0)
            {
                status = " AND  pr.Status IN (1,2)";
            }
            else
            {
                status = " AND pr.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND pr.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND st.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName ,
                        pr.PatientIdNumber as PatientIdNumber
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  {4} AND
                                            st.IsDeprecated = 0  {5} AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE({1}) between  DATE(pe.StartDate) and DATE(pe.EndDate) {2}  {3}", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), schedulleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader =>
                {
                    var task = new T
                           {
                               UserId = reader.GetGuid("UserId"),
                               DisciplineTask = reader.GetInt("DisciplineTask"),
                               Status = reader.GetInt("Status", 0),
                               Discipline = reader.GetStringNullable("Discipline"),
                               IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                               PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                               PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                           };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<T> GetPatientScheduledEventsLeanWithUserIdAndCheckMissedVisitStatus(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var eventDateWhere = GetWhereAppSpecificSql(SqlSelectType.EventDate);
            var additionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                additionalScript = " AND pr.Id = @patientid ";
            }
            else
            {
                additionalScript = " AND pr.Status IN (1,2) ";
            }
            if (!userIdOptional.IsEmpty())
            {
                additionalScript += " AND  st.UserId = @userid ";
            }
            if (IsDateRange)
            {
                additionalScript += string.Format(" AND DATE({0}) between DATE(@startdate) and DATE(@enddate) ", eventDateWhere);
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                if (IsMissedVisitIncluded)
                {
                    additionalScript += string.Format("  AND st.BeforeMissedVisitStatus IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }
                else
                {
                    additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += string.Format("  ORDER BY DATE({0}) ASC  ", eventDateWhere);
            }
            else
            {
                limitScript += string.Format("  ORDER BY DATE({0}) DESC  ", eventDateWhere);
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.BeforeMissedVisitStatus as MissedVisitStatus ,
                        st.Discipline as Discipline,
                        pr.PatientIdNumber as PatientIdNumber, 
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                    INNER JOIN patientprofiles pr ON  pr.Id = st.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) {2} {3} ", GetSelectAppSpecificSql(SqlSelectType.All), eventDateWhere, additionalScript, limitScript);

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientIdOptional)
                .AddGuid("userid", userIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt(IsMissedVisitIncluded ? "MissedVisitStatus" : "Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()

                };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return list;
        }

        public List<T> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var eventDateWhere = GetWhereAppSpecificSql(SqlSelectType.EventDate);
            var additionalScript = string.Empty;
            if (IsDateRange)
            {
                additionalScript += string.Format(" AND DATE({0}) between DATE(@startdate) and DATE(@enddate) ", eventDateWhere);
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += string.Format("  ORDER BY DATE({0}) ASC  ", eventDateWhere);
            }
            else
            {
                limitScript += string.Format("  ORDER BY DATE({0}) DESC  ", eventDateWhere);
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid  AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) {2} {3} ", GetSelectAppSpecificSql(SqlSelectType.All), eventDateWhere, additionalScript, limitScript);

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline")

                };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return list;
        }

        public List<T> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var eventDateWhere = GetWhereAppSpecificSql(SqlSelectType.EventDate);
            var additionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                additionalScript = " AND pr.Id = @patientid ";
            }
            else
            {
                additionalScript = " AND pr.Status IN (1,2) ";
            }
            if (!userIdOptional.IsEmpty())
            {
                additionalScript += " AND  st.UserId = @userid ";
            }
            if (IsDateRange)
            {
                additionalScript += string.Format(" AND DATE({0}) between DATE(@startdate) and DATE(@enddate) ", eventDateWhere);
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += string.Format("  ORDER BY DATE({0}) ASC  ", eventDateWhere);
            }
            else
            {
                limitScript += string.Format("  ORDER BY DATE({0}) DESC  ", eventDateWhere);
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.Discipline as Discipline,
                        pr.PatientIdNumber as PatientIdNumber, 
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                    INNER JOIN patientprofiles pr ON  pr.Id = st.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) {2} {3} ", GetSelectAppSpecificSql(SqlSelectType.All), eventDateWhere, additionalScript, limitScript);

            var list = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientIdOptional)
                 .AddGuid("userid", userIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                          {
                              Id = reader.GetGuid("Id"),
                              PatientId = reader.GetGuid("PatientId"),
                              EpisodeId = reader.GetGuid("EpisodeId"),
                              UserId = reader.GetGuid("UserId"),
                              DisciplineTask = reader.GetInt("DisciplineTask"),
                              Status = reader.GetInt("Status", 0),
                              Discipline = reader.GetStringNullable("Discipline"),
                              PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                              PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()

                          };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return list;
        }


        public List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id
                                        WHERE
                                            st.AgencyId = @agencyid  AND 
                                            pr.IsDeprecated = 0 AND 
                                            st.IsDeprecated = 0 AND
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) {2} ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additonalScript);

            var scheduleList = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var visit = new UserVisit
                        {
                            UserId = reader.GetGuid("UserId"),
                            DisciplineTask = reader.GetInt("DisciplineTask"),
                            IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                            Status = reader.GetInt("Status", 0),
                            PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                        };
                    GetSelectAppSpecificSql(visit, reader);
                    return visit;
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId = @userid AND 
                                            pe.IsActive = 1  AND 
                                            pe.IsDischarged = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE({1}) between  DATE(pe.StartDate) and DATE(pe.EndDate) {2} 
                                                ORDER BY pr.LastName ASC , pr.FirstName DESC ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader =>
                {
                    var visit = new UserVisit
                    {
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Status = reader.GetInt("Status", 0),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                    };
                    GetSelectAppSpecificSql(visit, reader);
                    return visit;
                })
                .AsList();
            }
            return list;
        }

        public List<T> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2)";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplineTasks.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 1 AND 
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate) {2} ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additonalScript);

            var scheduleList = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                    {
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status", 0),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                    };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<T> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        {0}
                        st.Status as Status ,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName
                                FROM 
                                    scheduletasks st
                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  
                                                WHERE
                                                        st.AgencyId = @agencyid {2} AND 
                                                        st.IsMissedVisit = 0 AND
                                                        st.IsDeprecated = 0 AND
                                                        st.InPrintQueue = 1 AND
                                                        pr.IsDeprecated = 0  AND 
                                                        pr.Status IN (1,2) AND 
                                                        st.IsDeprecated = 0  AND 
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0  AND 
                                                        DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE({1}) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY {1} DESC ", GetSelectAppSpecificSql(SqlSelectType.All), GetWhereAppSpecificSql(SqlSelectType.EventDate), additonalScript);

            var scheduleList = new List<T>();
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                {
                    var task = new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                };
                    GetSelectAppSpecificSql(task, reader);
                    return task;
                })
                .AsList();
            }
            return scheduleList;
        }

        public T GetCarePlan(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline
                            FROM
                                scheduletasks st
                                    INNER JOIN patientvisitnotes n ON n.Id = st.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.DisciplineTask in ({0}) AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 0 AND
                                            (n.SelectedEpisodeId = @episodeId OR
                                            (st.EpisodeId = @episodeid AND
                                            DATE({1}) between DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                            DATE({1}) between DATE(@startdate) and DATE(@enddate)))
                                                ORDER BY DATE({1}) DESC LIMIT 1", disciplineTasks.ToCommaSeperatedList(), GetWhereAppSpecificSql(SqlSelectType.EventDate));


            T scheduleEvent;
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleEvent = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public T GetLastEventLatestLean(Guid agencyId, Guid episodeId, Guid patientId, DateTime eventDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline
                            FROM
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.DisciplineTask in ({0}) AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 0 AND
                                            st.EpisodeId = @episodeid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0  AND 
                                            DATE({1}) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE({1}) <= DATE(@eventDate) AND
                                                ORDER BY DATE({1}) DESC LIMIT 1", disciplineTasks.ToCommaSeperatedList(), GetWhereAppSpecificSql(SqlSelectType.EventDate));


            T scheduleEvent;
            using (var cmd = new FluentCommand<T>(script))
            {
                scheduleEvent = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("eventdate", eventDate)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public List<Guid> GetScheduleTaskAssets(Guid agencyId,Guid patientId, Guid id)
        {
            var assetIds = new List<Guid>();
            var data = GetScheduleTaskByColumns(agencyId, patientId, id, "Asset");
            if (data.IsNotNullOrEmpty())
            {
                var assetXml = data["Asset"];
                assetIds = (assetXml.IsNotNullOrEmpty() ? assetXml.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
            }
            return assetIds;
        }

        protected abstract string GetWhereAppSpecificSql(SqlSelectType sql);

        protected abstract string GetSelectAppSpecificSql(SqlSelectType sql);

        protected abstract void GetSelectAppSpecificSql(T task, DataReader reader);

        protected abstract void GetSelectAppSpecificSql(UserVisit task, DataReader reader);

        public abstract List<T> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status);

        public abstract List<T> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);

        public abstract IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter);
    }


    public interface ITaskSchedule<T> where T : ITask, new()
    {
        bool AddScheduleTask(T scheduleEvent);

        bool AddMultipleScheduleTask(List<T> scheduleEvents);

        bool UpdateScheduleTask(T scheduleEvent);

        bool RemoveScheduleTaskFully(Guid agencyId, Guid patientId, Guid eventId);

        bool RemoveMultipleScheduleTaskFully(Guid agencyId, Guid patientId, List<Guid> eventIds);

        T GetScheduleTask(Guid agencyId, Guid patientId, Guid eventId);

        T GetScheduleTaskByUserId(Guid agencyId, Guid userId, Guid id);

        IList<T> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid patientId, Guid employeeId);

        IList<T> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid employeeId);

        List<T> GetScheduleEventsVeryLeanByIds(Guid agencyId, Guid patientId, List<Guid> ids, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);

        List<T> GetPatientScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);

        //List<T> GetCurrentAndPreviousOrders(Guid agencyId, PatientEpisode episode, int[] disciplineTasks);

        List<T> GetUsersScheduleTasksBetweenDatesByStatus(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, params int[] scheduleStatus);

        List<T> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        bool UpdateScheduleTasksForReassign(Guid agencyId, Guid userId, string ids);

        bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, List<T> scheduleEvents);

        bool ToggleScheduledTasksDelete(Guid agencyId, Guid patientId, List<Guid> eventIds, bool isDeprecated);

        bool ToggleScheduledEventsPaid(Guid agencyId, List<Guid> eventIds, bool isVisitPaid);

        List<T> GetPreviousNotes(Guid agencyId, T scheduledEvent, int[] disciplineTasks, int[] status);

        List<T> GetPreviousNotes(Guid agencyId, T scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status);

        List<T> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        List<T> GetDeletedTasks(Guid agencyId, Guid patientId);

        List<T> GetScheduleTasksByStatusDisciplineAndRange(Guid agencyId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks);

        List<T> GetScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status);

        List<T> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);

        List<T> GetScheduleEventsVeryLeanByIds(Guid agencyId, List<Guid> ids);

        List<T> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId);
    }
}
