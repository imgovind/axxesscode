﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core;
using Axxess.Core.Extension;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.DataProviders;

    public abstract class BillingAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;

        #region IBillingRepository Members

        public bool AddManagedClaim(ManagedClaim managedClaim)
        {
            bool result = false;
            if (managedClaim != null)
            {
                try
                {
                    managedClaim.Created = DateTime.Now;
                    managedClaim.Modified = DateTime.Now;
                    database.Add<ManagedClaim>(managedClaim);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public int MarkManagedClaimsAsSubmitted(Guid agencyId, List<ManagedClaim> managedClaims)
        {
           
            int result = 0;
            if (managedClaims != null && managedClaims.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE managedclaims mc SET mc.Status = @status , mc.IsGenerated = 1, mc.ClaimDate = @claimdate  WHERE mc.AgencyId = @agencyid AND mc.Status != @status AND mc.Id IN ( {0})", managedClaims.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("status", (int)ManagedClaimStatus.ClaimSubmitted)
                        .AddDateTime("claimdate", DateTime.Now)
                        .AsNonQuery();
                    }
                    return count;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public bool MarkManagedClaimsAsGenerated(Guid agencyId, List<ManagedClaim> managedClaims)
        {
            //managedClaims.ForEach(claim =>
            //{
            //    var claimToEdit = database.Single<ManagedClaim>(m => m.AgencyId == agencyId && m.Id == claim.Id);
            //    if (claimToEdit != null)
            //    {
            //        claimToEdit.IsGenerated = true;
            //        claimToEdit.ClaimDate = DateTime.Now;
            //        database.Update<ManagedClaim>(claimToEdit);
            //    }
            //});


            bool result = false;
            if (managedClaims.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE managedclaims mc SET mc.IsDeprecated = 1 , mc.Modified = @modified  WHERE mc.AgencyId = @agencyid AND mc.Id IN (@ids)");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("modified", DateTime.Now)
                        .AddString("ids", managedClaims.Select(m => m.Id).ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }
                    return count > 0;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public IList<ManagedClaimLean> GetManagedClaimsPerPatient(Guid agencyId, Guid patientId, int insuranceId)
        {
            var insurance = GetManagedClaimsPerPatientAppSpecificInsuranceOrPayorTypeFilter(insuranceId);

            var script = string.Format(@"SELECT
                                        mc.Id as Id,
                                        mc.PatientId as PatientId,
                                        mc.FirstName as FirstName,
                                        mc.LastName as LastName, 
                                        mc.InsuranceIdNumber as InsuranceIdNumber, 
                                        mc.PaymentDate as PaymentDate, 
                                        mc.Payment as Payment,
                                        mc.Status as Status,
                                        mc.EpisodeStartDate as EpisodeStartDate, 
                                        mc.EpisodeEndDate as EpisodeEndDate,
                                        mc.ClaimDate as ClaimDate, 
                                        mc.ProspectivePay as ProspectivePay,
                                        mc.IsInfoVerified as IsInfoVerified, 
                                        mc.IsSupplyVerified as IsSupplyVerified,
                                        mc.IsVisitVerified as IsVisitVerified
                                            FROM
                                                managedclaims mc
                                                        WHERE 
                                                            mc.PatientId= @patientid AND
                                                            mc.AgencyId = @agencyid  {0}", insurance);
            var list = new List<ManagedClaimLean>();
            using (var cmd = new FluentCommand<ManagedClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ManagedClaimLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    IsuranceIdNumber = reader.GetStringNullable("InsuranceIdNumber"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    Status = reader.GetInt("Status"),
                    EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                    EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    ClaimDate = reader.GetDateTime("ClaimDate").ToString("MM/dd/yyyy"),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                    IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                    IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                    IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                })
                .AsList();
            }
            return list;
        }

        protected abstract string GetManagedClaimsPerPatientAppSpecificInsuranceOrPayorTypeFilter(int insuranceId);

        public List<Claim> GetManagedClaims(Guid agencyId, Guid branchId, int insuranceId, int status, bool IsZeroInsuraceIdAll)
        {
            var insurance = GetManagedClaimsAppSpecificInsuranceOrPayorTypeFilter(insuranceId, IsZeroInsuraceIdAll);

            var script = string.Format(@"SELECT 
                            mc.Id as Id,
                            mc.PatientId as PatientId,
                            pr.AgencyLocationId as AgencyLocationId ,
                            pr.FirstName as FirstName,
                            pr.LastName as LastName,
                            pr.CaseManagerId as CaseManagerId,
                            pr.UserId as ClinicianId,
                            pr.PatientIdNumber as PatientIdNumber ,
                            mc.EpisodeStartDate as EpisodeStartDate,
                            mc.EpisodeEndDate as EpisodeEndDate,
                            mc.IsInfoVerified as IsInfoVerified,
                            mc.IsSupplyVerified as IsSupplyVerified,
                            mc.IsVisitVerified as IsVisitVerified ,
                            mc.PrimaryInsuranceId as PrimaryInsuranceId 
                              FROM 
                                managedclaims mc
                                    INNER JOIN patientprofiles pr ON mc.PatientId = pr.Id 
                                         Where 
                                            pr.Status IN (1,2) AND 
                                            mc.AgencyId = @agencyid AND
                                            DATE(mc.EpisodeEndDate) <= DATE(Curdate()) AND
                                            pr.IsDeprecated = 0  AND 
                                            mc.Status = @status {0} {1} ", branchId.IsEmpty() ? string.Empty : "AND pr.AgencyLocationId = @branchId", insurance);
            var list = new List<Claim>();
            using (var cmd = new FluentCommand<Claim>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insuranceId)
                .AddInt("status", status)
                .SetMap(reader => new ManagedBill
                {
                    Id = reader.GetGuid("Id"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    CaseManagerId = reader.GetGuidIncludeEmpty("CaseManagerId"),
                    ClinicianId = reader.GetGuidIncludeEmpty("ClinicianId"),
                    EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                    EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                    IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                    IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                    PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                })
                .AsList();
            }
            return list;
        }

        protected abstract string GetManagedClaimsAppSpecificInsuranceOrPayorTypeFilter(int insuranceId, bool IsZeroInsuraceIdAll);
       
        public List<Claim> GetManagedClaimByIds(Guid agencyId, Guid branchId, int insuranceId, List<Guid> claimIds)
        {
            var list = new List<Claim>();
            if (claimIds != null && claimIds.Count > 0)
            {
                var ids = claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                mc.Id as Id, 
                                mc.PatientId as PatientId, 
                                pr.FirstName as FirstName, 
                                pr.LastName as LastName, 
                                pr.PatientIdNumber as PatientIdNumber , 
                                mc.EpisodeStartDate as EpisodeStartDate, 
                                mc.EpisodeEndDate as EpisodeEndDate
                                    FROM 
                                        managedclaims mc
                                            INNER JOIN patientprofiles pr ON mc.PatientId = pr.Id
                                                WHERE 
                                                    pr.Status IN (1,2) AND 
                                                    mc.PrimaryInsuranceId = @insuranceId AND 
                                                    mc.AgencyId = @agencyid AND
                                                    {0}   
                                                    mc.Id IN ( {1} ) ", branchId.IsEmpty() ? string.Empty : "pr.AgencyLocationId = @branchId AND", ids);
                using (var cmd = new FluentCommand<Claim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("insuranceId", insuranceId)
                    .SetMap(reader => new ManagedBill
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                        EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    })
                    .AsList();
                }
            }
            return list;
        }

        public List<ManagedClaim> GetManagedClaimsToGenerateByIds(Guid agencyId, List<Guid> managedClaimIds)
        {
            var list = new List<ManagedClaim>();
            if (managedClaimIds != null && managedClaimIds.Count > 0)
            {
                var ids = managedClaimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                mc.Id as Id ,
                                mc.PatientId as PatientId ,
                                mc.EpisodeId as EpisodeId , 
                                mc.PatientIdNumber as PatientIdNumber, 
                                mc.EpisodeStartDate as EpisodeStartDate, 
                                mc.EpisodeEndDate as EpisodeEndDate ,
                                mc.IsOasisComplete as IsOasisComplete, 
                                mc.IsFirstBillableVisit as IsFirstBillableVisit , 
                                mc.FirstBillableVisitDate as FirstBillableVisitDate ,  
                                mc.Remark as Remark, 
                                mc.InsuranceIdNumber as InsuranceIdNumber ,
                                mc.FirstName as FirstName , 
                                mc.LastName as LastName , 
                                mc.DOB as DOB , 
                                mc.Gender as Gender , 
                                mc.PaymentDate as PaymentDate ,
                                mc.AddressLine1 as AddressLine1 , 
                                mc.AddressLine2 as AddressLine2 , 
                                mc.AddressCity as AddressCity , 
                                mc.AddressStateCode as AddressStateCode , 
                                mc.AddressZipCode as AddressZipCode , 
                                mc.StartofCareDate as StartofCareDate , 
                                mc.PhysicianNPI as PhysicianNPI , 
                                mc.PhysicianFirstName as PhysicianFirstName , 
                                mc.PhysicianLastName as PhysicianLastName ,
                                mc.DiagnosisCode as DiagnosisCode , 
                                mc.HippsCode as HippsCode , 
                                mc.ClaimKey as ClaimKey , 
                                mc.AreOrdersComplete as AreOrdersComplete ,  
                                mc.AreVisitsComplete as AreVisitsComplete ,
                                mc.IsGenerated as IsGenerated , 
                                mc.Modified as Modified , 
                                mc.Created as Created , 
                                mc.Relationship as Relationship, 
                                mc.VerifiedVisits as VerifiedVisits , 
                                mc.Supply as Supply ,
                                mc.SupplyTotal as SupplyTotal , 
                                mc.Payment as Payment ,  
                                mc.PrimaryInsuranceId as PrimaryInsuranceId , 
                                mc.IsSupplyVerified as IsSupplyVerified , 
                                mc.IsInfoVerified as IsInfoVerified , 
                                mc.IsVisitVerified as IsVisitVerified , 
                                mc.IsRapGenerated as IsRapGenerated , 
                                mc.Status as Status ,
                                mc.Type as Type , 
                                mc.DischargeDate as DischargeDate ,
                                mc.AdmissionSource as AdmissionSource , 
                                mc.PatientStatus as PatientStatus ,
                                mc.ClaimDate as ClaimDate , 
                                mc.ProspectivePay as ProspectivePay ,
                                mc.IsInsuranceVerified as IsInsuranceVerified, 
                                mc.AssessmentType as AssessmentType ,
                                mc.Comment as Comment  , 
                                mc.HealthPlanId as HealthPlanId , 
                                mc.GroupName as GroupName ,
                                mc.GroupId as GroupId , 
                                mc.AuthorizationNumber as AuthorizationNumber , 
                                mc.AuthorizationNumber2 as AuthorizationNumber2,  
                                mc.AuthorizationNumber3 as AuthorizationNumber3 , 
                                mc.ConditionCodes as ConditionCodes , 
                                mc.UB4PatientStatus as UB4PatientStatus  , 
                                mc.Ub04Locator81cca as Ub04Locator81cca ,
                                mc.Ub04Locator39 as Ub04Locator39, 
                                mc.Ub04Locator31 as Ub04Locator31,
                                mc.Ub04Locator32 as Ub04Locator32, 
                                mc.Ub04Locator33 as Ub04Locator33, 
                                mc.Ub04Locator34 as Ub04Locator34, 
                                pr.MiddleInitial as MiddleInitial, 
                                pr.AgencyLocationId as AgencyLocationId  
                                    FROM 
                                        managedclaims mc
                                            INNER JOIN patientprofiles pr ON mc.PatientId = pr.Id
                                                WHERE 
                                                    mc.AgencyId = @agencyid AND 
                                                    pr.AgencyId = @agencyid AND 
                                                    mc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<ManagedClaim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ManagedClaim
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         Remark = reader.GetStringNullable("Remark"),
                         InsuranceIdNumber = reader.GetStringNullable("InsuranceIdNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         VerifiedVisits = reader.GetStringNullable("VerifiedVisits"),
                         Supply = reader.GetStringNullable("Supply"),
                         SupplyTotal = (double)reader.GetDecimalNullable("SupplyTotal"),
                         Payment = (double)reader.GetDecimalNullable("Payment"),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetStringNullable("Type"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay"),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         Relationship = reader.GetStringNullable("Relationship"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         AuthorizationNumber2 = reader.GetStringNullable("AuthorizationNumber2"),
                         AuthorizationNumber3 = reader.GetStringNullable("AuthorizationNumber3"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus"),
                         Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                         Ub04Locator39 = reader.GetStringNullable("Ub04Locator39"),
                         Ub04Locator31 = reader.GetStringNullable("Ub04Locator31"),
                         Ub04Locator32 = reader.GetStringNullable("Ub04Locator32"),
                         Ub04Locator33 = reader.GetStringNullable("Ub04Locator33"),
                         Ub04Locator34 = reader.GetStringNullable("Ub04Locator34"),
                         AgencyLocationId = reader.GetGuid("AgencyLocationId")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<ManagedClaim> GetManagedClaimsToGenerateByIdsLean(Guid agencyId, List<Guid> managedClaimIds)
        {
            var list = new List<ManagedClaim>();
            if (managedClaimIds != null && managedClaimIds.Count > 0)
            {
                var ids = managedClaimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            mc.Id as Id ,
                                            mc.PatientId as PatientId ,
                                            mc.IsGenerated as IsGenerated , 
                                            mc.IsSupplyVerified as IsSupplyVerified , 
                                            mc.IsInfoVerified as IsInfoVerified , 
                                            mc.IsVisitVerified as IsVisitVerified , 
                                            mc.IsRapGenerated as IsRapGenerated , 
                                            mc.IsInsuranceVerified as IsInsuranceVerified
                                                FROM 
                                                    managedclaims mc
                                                            WHERE 
                                                                mc.AgencyId = @agencyid AND 
                                                                mc.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<ManagedClaim>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ManagedClaim
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         IsInsuranceVerified = reader.GetBoolean("IsInsuranceVerified"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRapGenerated = reader.GetBoolean("IsRapGenerated")
                     })
                     .AsList();
                }
            }
            return list;
        }


        public ManagedClaim GetManagedClaimOnly(Guid agencyId, Guid Id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == Id));
        }

        public ManagedClaim GetManagedClaimOnly(Guid agencyId, Guid patientId, Guid Id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == Id && r.PatientId == patientId));
        }

//        public ManagedClaim GetManagedClaimInsurance(Guid agencyId, Guid Id)
//        {
//            var claim = new ManagedClaim();
//            var script = string.Format(
//               @"SELECT 
//                    mc.Id, 
//                    mc.PatientId,
//                    mc.Insurance, 
//                    mc.PrimaryInsuranceId
//                        FROM managedclaims mc
//                           WHERE 
//                                mc.AgencyId = @agencyId AND
//                                mc.Id = @id");

//            using (var cmd = new FluentCommand<ManagedClaim>(script))
//            {
//                claim = cmd.SetConnection(connectionStringName)
//                 .AddGuid("agencyId", agencyId)
//                 .AddGuid("id", Id)
//                 .SetMap(reader => new ManagedClaim
//                 {
//                     Id = reader.GetGuid("Id"),
//                     PatientId = reader.GetGuid("PatientId"),
//                     Insurance = reader.GetStringNullable("Insurance"),
//                     PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
//                 }).AsSingle();
//            }
//            return claim;
//        }

        public List<ClaimInfoDetail> GetManagedClaimInfoDetails(Guid agencyId, List<Guid> claimIds)
        {
            var list = new List<ClaimInfoDetail>();
            if (claimIds != null && claimIds.Count > 0)
            {
                var ids = claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                p.FirstName as FirstName, 
                                p.LastName as LastName, 
                                p.MiddleInitial as MiddleInitial, 
                                p.PatientIdNumber as PatientIdNumber,
                                mc.EpisodeStartDate as EpisodeStartDate, 
                                mc.EpisodeEndDate as EpisodeEndDate, 
                                mc.ProspectivePay as ProspectivePay
                                    FROM 
                                        managedclaims mc
                                            INNER JOIN agencymanagement.patients p ON mc.PatientId = p.Id 
                                                WHERE 
                                                    mc.AgencyId = @agencyid AND 
                                                    mc.Id IN ({0}) ", ids);
                using (var cmd = new FluentCommand<ClaimInfoDetail>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ClaimInfoDetail
                    {
                        BillType = "MANAGED",
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                        StartDate = reader.GetDateTime("EpisodeStartDate"),
                        EndDate = reader.GetDateTime("EpisodeEndDate"),
                        ProspectivePay = reader.GetDouble("ProspectivePay"),
                        MedicareNumber = string.Empty
                    })
                    .AsList();
                }
            }
            return list;
        }

        public bool UpdateManagedClaimModel(ManagedClaim managedClaim)
        {
            if (managedClaim != null)
            {
                try
                {
                    managedClaim.Modified = DateTime.Now;
                    return database.Update<ManagedClaim>(managedClaim) > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateManagedClaim(ManagedClaim managedClaim)
        {
            if (managedClaim != null)
            {
                var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));

                if (currentManagedClaim != null)
                {
                    try
                    {
                        currentManagedClaim.PatientIdNumber = managedClaim.PatientIdNumber;
                        currentManagedClaim.EpisodeStartDate = managedClaim.EpisodeStartDate;
                        currentManagedClaim.EpisodeEndDate = managedClaim.EpisodeEndDate;
                        currentManagedClaim.IsOasisComplete = managedClaim.IsOasisComplete;
                        currentManagedClaim.IsFirstBillableVisit = managedClaim.IsFirstBillableVisit;
                        currentManagedClaim.FirstBillableVisitDate = managedClaim.FirstBillableVisitDate;
                        currentManagedClaim.Remark = managedClaim.Remark;
                        currentManagedClaim.InsuranceIdNumber = managedClaim.InsuranceIdNumber;
                        currentManagedClaim.FirstName = managedClaim.FirstName;
                        currentManagedClaim.LastName = managedClaim.LastName;
                        currentManagedClaim.DOB = managedClaim.DOB;
                        currentManagedClaim.Gender = managedClaim.Gender;
                        currentManagedClaim.AddressLine1 = managedClaim.AddressLine1;
                        currentManagedClaim.AddressLine2 = managedClaim.AddressLine2;
                        currentManagedClaim.AddressCity = managedClaim.AddressCity;
                        currentManagedClaim.AddressStateCode = managedClaim.AddressStateCode;
                        currentManagedClaim.AddressZipCode = managedClaim.AddressZipCode;
                        currentManagedClaim.StartofCareDate = managedClaim.StartofCareDate;
                        currentManagedClaim.PhysicianNPI = managedClaim.PhysicianNPI;
                        currentManagedClaim.PhysicianFirstName = managedClaim.PhysicianFirstName;
                        currentManagedClaim.PhysicianLastName = managedClaim.PhysicianLastName;
                        currentManagedClaim.DiagnosisCode = managedClaim.DiagnosisCode;
                        currentManagedClaim.HippsCode = managedClaim.HippsCode;
                        currentManagedClaim.ClaimKey = managedClaim.ClaimKey;
                        currentManagedClaim.AreOrdersComplete = managedClaim.AreOrdersComplete;
                        currentManagedClaim.AreVisitsComplete = managedClaim.AreVisitsComplete;
                        currentManagedClaim.Created = managedClaim.Created;
                        currentManagedClaim.VerifiedVisits = managedClaim.VerifiedVisits;
                        currentManagedClaim.PrimaryInsuranceId = managedClaim.PrimaryInsuranceId;
                        currentManagedClaim.Supply = managedClaim.Supply;
                        currentManagedClaim.SupplyTotal = managedClaim.SupplyTotal;
                        currentManagedClaim.IsInsuranceVerified = managedClaim.IsInsuranceVerified;
                        currentManagedClaim.IsSupplyVerified = managedClaim.IsSupplyVerified;
                        currentManagedClaim.IsInfoVerified = managedClaim.IsInfoVerified;
                        currentManagedClaim.IsVisitVerified = managedClaim.IsVisitVerified;
                        currentManagedClaim.AgencyId = managedClaim.AgencyId;
                        currentManagedClaim.IsRapGenerated = managedClaim.IsRapGenerated;
                        currentManagedClaim.Status = managedClaim.Status;
                        currentManagedClaim.IsGenerated = managedClaim.IsGenerated;
                        currentManagedClaim.ClaimDate = managedClaim.ClaimDate;
                        currentManagedClaim.Comment = managedClaim.Comment;
                        currentManagedClaim.Payment = managedClaim.Payment;
                        currentManagedClaim.PaymentDate = managedClaim.PaymentDate;
                        currentManagedClaim.ProspectivePay = managedClaim.ProspectivePay;
                        currentManagedClaim.Modified = DateTime.Now;
                        if (database.Update<ManagedClaim>(currentManagedClaim) > 0)
                        {
                            return true;
                        }
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        //public bool UpdateManagedClaimForVisitVerify(ManagedClaim managedClaim)
        //{
        //    if (managedClaim != null)
        //    {
        //        var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));
        //        if (currentManagedClaim != null)
        //        {
        //            try
        //            {
        //                currentManagedClaim.VerifiedVisits = managedClaim.VerifiedVisits;
        //                currentManagedClaim.IsVisitVerified = managedClaim.IsVisitVerified;
        //                currentManagedClaim.Supply = managedClaim.Supply;
        //                currentManagedClaim.Insurance = managedClaim.Insurance;
        //                currentManagedClaim.Modified = managedClaim.Modified;
        //                database.Update<ManagedClaim>(currentManagedClaim);
        //                return true;
        //            }
        //            catch (Exception)
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public bool UpdateManagedClaimForSupplyVerify(ManagedClaim managedClaim)
        {
            if (managedClaim != null)
            {
                var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));

                if (currentManagedClaim != null)
                {
                    try
                    {
                        currentManagedClaim.Supply = managedClaim.Supply;
                        currentManagedClaim.SupplyTotal = managedClaim.SupplyTotal;
                        currentManagedClaim.SupplyCode = managedClaim.SupplyCode;
                        currentManagedClaim.IsSupplyVerified = managedClaim.IsSupplyVerified;
                        currentManagedClaim.Modified = managedClaim.Modified;
                        database.Update<ManagedClaim>(currentManagedClaim);
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public bool DeleteManagedClaim(Guid agencyId, Guid patientId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  managedclaims WHERE AgencyId = @agencyid AND  PatientId = @patientid AND Id = @id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<ClaimLean> GetManagedClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                            pr.FirstName as FirstName,
                            pr.LastName as LastName,
                            pr.MiddleInitial as MiddleInitial,
                            pr.PatientIdNumber as PatientIdNumber,
                            mc.Id as Id,
                            mc.EpisodeStartDate as StartDate,
                            mc.EpisodeEndDate as EndDate,
                            mc.ClaimDate as ClaimDate,
                            mc.Status as Status,
                            mc.ProspectivePay as ProspectivePay,
                            mc.Payment as Payment,
                            mc.PaymentDate as PaymentDate,
                            mc.PrimaryInsuranceId as PrimaryInsuranceId
                                FROM 
                                    managedclaims mc
                                        INNER JOIN patientprofiles pr ON mc.PatientId = pr.Id 
                                            WHERE 
                                                mc.AgencyId = @agencyid {0} AND
                                                pr.Status IN (1,2) AND
                                                pr.IsDeprecated = 0 AND
                                                mc.PrimaryInsuranceId != 0 AND
                                                DATE(mc.EpisodeEndDate) between DATE(@startdate) and DATE(@enddate) AND
                                                mc.Status = @status
                                                    ORDER BY  mc.EpisodeStartDate ASC", !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("status", status)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Id = reader.GetGuid("Id"),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Status = reader.GetInt("Status"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")

                 })
                 .AsList();
            }
            return list;
        }

        //public bool ManagedVerifyInsurance(ManagedClaim claim, Guid agencyId)
        //{
        //    if (claim != null)
        //    {
        //        var currentClaim = database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == claim.Id));
        //        if (currentClaim != null)
        //        {
        //            try
        //            {
        //                currentClaim.IsInsuranceVerified = true;
        //                currentClaim.Ub04Locator81cca = claim.Ub04Locator81cca;
        //                currentClaim.Ub04Locator39 = claim.Ub04Locator39;
        //                currentClaim.Ub04Locator31 = claim.Ub04Locator31;
        //                currentClaim.Ub04Locator32 = claim.Ub04Locator32;
        //                currentClaim.Ub04Locator33 = claim.Ub04Locator33;
        //                currentClaim.Ub04Locator34 = claim.Ub04Locator34;
        //                currentClaim.HCFALocators = claim.HCFALocators;
        //                currentClaim.Modified = DateTime.Now;
        //                return database.Update<ManagedClaim>(currentClaim) > 0;
        //            }
        //            catch (Exception)
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public bool AddManagedClaimPayment(ManagedClaimPayment payment)
        {
            Check.Argument.IsNotNull(payment, "payment");
            bool result = false;
            try
            {
                payment.Created = DateTime.Now;
                payment.Modified = DateTime.Now;
                database.Add<ManagedClaimPayment>(payment);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        


        public bool DeleteManagedClaimPayment(Guid id)
        {

            bool result = false;
            if (!id.IsEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE managedclaimpayments mcp SET mcp.IsDeprecated = 1 , mcp.Modified = @modified  WHERE mcp.Id = @id AND mcp.IsDeprecated = 0");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("id", id)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery();
                    }
                    return count > 0;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public bool UpdateManagedClaimPayment(ManagedClaimPayment payment)
        {
            bool result = false;
            if (payment != null)
            {
                try
                {
                    var paymentToEdit = database.Single<ManagedClaimPayment>(p => p.Id == payment.Id && p.IsDeprecated == false);
                    if (paymentToEdit != null)
                    {
                        paymentToEdit.Payment = payment.Payment;
                        paymentToEdit.PaymentDate = payment.PaymentDate;
                        paymentToEdit.Payor = payment.Payor;
                        paymentToEdit.Comments = payment.Comments;
                        paymentToEdit.Modified = DateTime.Now;
                        database.Update<ManagedClaimPayment>(paymentToEdit);
                        result = true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsByDateRange(Guid agencyId, Guid patientId, Guid claimId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ManagedClaimPayment>();
            if (patientId.IsEmpty() && claimId.IsEmpty())
            {
                return list;
            }
            string patientScript = string.Empty;
            if (!patientId.IsEmpty())
            {
                patientScript = " AND mcp.PatientId = @patientId";
            }
            string claimScript = string.Empty;
            if (!claimId.IsEmpty())
            {
                claimScript = " AND mcp.ClaimId = @claimId";
            }
            string dateScript = string.Empty;
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                claimScript = " AND mcp.ClaimId = @claimId";
            }

            var script = string.Format(
                   @"SELECT mcp.Id,
                            mcp.AgencyId, 
                            mcp.ClaimId, 
                            mcp.PatientId,
                            mcp.Payment,
                            mcp.PaymentDate,
                            mcp.Payor, 
                            mcp.Created,
                            mcp.Comments,
                            mc.EpisodeStartDate,
                            mc.EpisodeEndDate 
                                FROM 
                                    managedclaimpayments mcp
                                        JOIN managedclaims mc ON mcp.ClaimId = mc.Id 
                                            WHERE 
                                                mcp.IsDeprecated = 0 &&
                                                mcp.AgencyId = @agencyId {0}{1}", patientScript, claimScript);

            using (var cmd = new FluentCommand<ManagedClaimPayment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyId", agencyId)
                 .AddGuid("claimId", claimId)
                 .AddGuid("patientId", patientId)
                 .SetMap(reader => new ManagedClaimPayment
                 {
                     Id = reader.GetGuid("Id"),
                     ClaimId = reader.GetGuid("ClaimId"),
                     PatientId = reader.GetGuid("PatientId"),
                     Payment = reader.GetDouble("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                     EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                     Payor = reader.GetInt("Payor"),
                     Comments = reader.GetStringNullable("Comments"),
                     Created = reader.GetDateTime("Created")
                 }).AsList();
            }
            return list;
        }

        public List<ManagedClaimPayment> GetManagedClaimPayments(Guid agencyId)
        {
            var payments = database.Find<ManagedClaimPayment>(p => p.AgencyId == agencyId && p.IsDeprecated == false).ToList();
            return payments;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsByClaimAndPatient(Guid agencyId, Guid patientId, Guid claimId)
        {
            var payments = database.Find<ManagedClaimPayment>(p => p.AgencyId == agencyId && p.PatientId == patientId
                && p.ClaimId == claimId && p.IsDeprecated == false).ToList();
            return payments;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsByPatient(Guid agencyId, Guid patientId)
        {
            var payments = database.Find<ManagedClaimPayment>(p => p.AgencyId == agencyId && p.PatientId == patientId
               && p.IsDeprecated == false).ToList();
            return payments;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsByClaim(Guid agencyId, Guid claimId)
        {
            var payments = database.Find<ManagedClaimPayment>(p => p.AgencyId == agencyId
                && p.ClaimId == claimId && p.IsDeprecated == false).ToList();
            return payments;
        }

        public ManagedClaimPayment GetManagedClaimPayment(Guid agencyId, Guid id)
        {
            var payment = database.Single<ManagedClaimPayment>(c => c.AgencyId == agencyId && c.Id == id && c.IsDeprecated == false);
            return payment;
        }

        public bool AddManagedClaimAdjustment(ManagedClaimAdjustment adjustment)
        {
            Check.Argument.IsNotNull(adjustment, "adjustment");
            bool result = false;
            try
            {
                adjustment.Created = DateTime.Now;
                adjustment.Modified = DateTime.Now;
                database.Add<ManagedClaimAdjustment>(adjustment);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }


        public bool DeleteManagedClaimAdjustment(Guid agencyId, Guid id)
        {

            bool result = false;
            if (!id.IsEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE managedclaimddjustments mca SET mca.IsDeprecated = 1 , mca.Modified = @modified  WHERE mca.AgenyId = @agencyid AND mca.Id = @id AND mca.IsDeprecated = 0");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                            .AddGuid("agencyid", agencyId)
                            .AddGuid("id", id)
                            .AddDateTime("modified", DateTime.Now)
                            .AsNonQuery();
                    }
                    return count > 0;
                }
                catch (Exception)
                {
                    return result;
                }
            }
            return result;
        }

        public bool UpdateManagedClaimAdjustment(ManagedClaimAdjustment adjustment)
        {
            bool result = false;
            if (adjustment != null)
            {
                try
                {
                    var adjustmentToEdit = database.Single<ManagedClaimAdjustment>(a => a.Id == adjustment.Id && a.IsDeprecated == false);
                    if (adjustmentToEdit != null)
                    {
                        adjustmentToEdit.Adjustment = adjustment.Adjustment;
                        adjustmentToEdit.TypeId = adjustment.TypeId;
                        adjustmentToEdit.Comments = adjustment.Comments;
                        adjustmentToEdit.Modified = DateTime.Now;
                        database.Update<ManagedClaimAdjustment>(adjustmentToEdit);
                        result = true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public List<ManagedClaimAdjustment> GetManagedClaimAdjustments(Guid agencyId)
        {
            var adjustments = database.Find<ManagedClaimAdjustment>(p => p.AgencyId == agencyId && p.IsDeprecated == false).ToList();
            return adjustments;
        }

        public List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByClaimAndPatient(Guid agencyId, Guid patientId, Guid claimId)
        {
            var adjustments = database.Find<ManagedClaimAdjustment>(p => p.AgencyId == agencyId && p.PatientId == patientId
                && p.ClaimId == claimId && p.IsDeprecated == false).ToList();
            return adjustments;
        }

        public List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByPatient(Guid agencyId, Guid patientId)
        {
            var adjustments = database.Find<ManagedClaimAdjustment>(p => p.AgencyId == agencyId && p.PatientId == patientId
               && p.IsDeprecated == false).ToList();
            return adjustments;
        }

        public List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByClaim(Guid agencyId, Guid claimId)
        {
            var adjustments = database.Find<ManagedClaimAdjustment>(p => p.AgencyId == agencyId
                && p.ClaimId == claimId && p.IsDeprecated == false).ToList();
            return adjustments;
        }

        public ManagedClaimAdjustment GetManagedClaimAdjustment(Guid AgencyId, Guid Id)
        {
            var adjustment = database.Single<ManagedClaimAdjustment>(c => c.AgencyId == AgencyId && c.Id == Id && c.IsDeprecated == false);
            return adjustment;
        }

        public long AddClaimData(ClaimData claimData)
        {
            long claimId = -1;
            if (claimData != null)
            {
                claimData.Created = DateTime.Now;
                claimData.Modified = DateTime.Now;
                try
                {
                    database.Add<ClaimData>(claimData);
                    claimId = claimData.Id;
                }
                catch (Exception)
                {
                    return claimId;
                }
            }
            return claimId;
        }

        public bool UpdateClaimData(ClaimData claimData)
        {
            bool result = false;
            if (claimData != null)
            {
                try
                {
                    var claimDataToEdit = database.Single<ClaimData>(c => c.AgencyId == claimData.AgencyId && c.Id == claimData.Id);
                    if (claimDataToEdit != null)
                    {
                        claimDataToEdit.Data = claimData.Data;
                        claimDataToEdit.BillIdentifers = claimData.BillIdentifers;
                        claimDataToEdit.ClaimType = claimData.ClaimType;
                        claimDataToEdit.Modified = DateTime.Now;
                        database.Update<ClaimData>(claimDataToEdit);
                        result = true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        //public bool DeleteClaimData(Guid agencyId, long claimId)
        //{
        //    bool result = false;
        //    try
        //    {
        //        var claimDataToDelete = database.Single<ClaimData>(c => c.AgencyId == agencyId && c.Id == claimId);
        //        if (claimDataToDelete != null)
        //        {
        //            database.Delete<ClaimData>(claimDataToDelete.Id);
        //            result = true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return result;
        //}

        public bool RemoveClaimData(Guid agencyId, long claimId)
        {
            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM claimdatas cd WHERE cd.AgencyId = @agencyid AND cd.Id = @id");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .Add<long>("id", claimId).AsNonQuery();

                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public ClaimData GetClaimData(Guid agencyId, int ansiId)
        {
            return database.Single<ClaimData>(c => c.AgencyId == agencyId && c.Id == ansiId && c.IsDeprecated == false);
        }

        public List<ClaimData> GetClaimDatas(Guid agencyId, string claimType, DateTime batchDate)
        {
            if (claimType.IsEqual("ALL"))
            {
                return database.Find<ClaimData>(c => c.AgencyId == agencyId).Where(c => c.Created.ToShortDateString().IsEqual(batchDate.Date.ToShortDateString()) && c.IsDeprecated == false).ToList();
            }
            return database.Find<ClaimData>(c => c.AgencyId == agencyId && c.ClaimType == claimType).Where(c => c.Created.ToShortDateString().IsEqual(batchDate.Date.ToShortDateString()) && c.IsDeprecated == false).ToList();
        }

        public int CreateNextClaimNumber(Guid ClaimId)
        {
            var script = @"INSERT INTO claimnumbers (EntityId) Values (@entityId); SELECT Id FROM claimnumbers WHERE EntityId = @entityId;";
            int id = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                id = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("entityId", ClaimId)
                    .SetMap(reader => reader.GetInt("Id"))
                .AsSingle();
            }
            return id;
        }

        public int GetClaimNumber(Guid ClaimId)
        {
            var script = @"SELECT Id
                                FROM claimnumbers 
                                    WHERE EntityId = @entityId";
            int id = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                id = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("entityId", ClaimId)
                    .SetMap(reader => reader.GetInt("Id"))
                .AsSingle();
            }
            if (id == 0)
            {
                id = this.CreateNextClaimNumber(ClaimId);
            }
            return id;
        }

        public Dictionary<string, string> GetManagedClaimJsonByColumns(Guid agencyId, Guid patientId, Guid id, params string[] columns)
        {
            return EntityHelper.SelectPatientEntityJsonByColumns<ManagedClaim>(agencyId, patientId, id, connectionStringName, columns);
        }

        #endregion
    }
}
