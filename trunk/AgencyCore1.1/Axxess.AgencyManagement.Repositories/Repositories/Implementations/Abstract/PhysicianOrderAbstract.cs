﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using System.Xml.Linq;
    using Axxess.Core;
    using Axxess.Core.Enums;

    public abstract class PhysicianOrderAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;
        protected AgencyServices Service { get; set; }
        
        #region Physician Order

        public bool AddOrder(PhysicianOrder order)
        {
            var result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    database.Add<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddMultipleOrder(List<PhysicianOrder> orders)
        {
            var result = false;
            try
            {
                if (orders != null && orders.Count > 0)
                {

                    database.AddMany<PhysicianOrder>(orders);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                var editOrder = database.Single<PhysicianOrder>(o => o.Id == order.Id && o.PatientId == order.PatientId);
                if (order != null && editOrder != null)
                {
                    editOrder.Modified = DateTime.Now;
                    editOrder.OrderDate = order.OrderDate;
                    editOrder.Summary = order.Summary;
                    editOrder.Text = order.Text;
                    editOrder.PhysicianId = order.PhysicianId;
                    editOrder.Status = order.Status;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    editOrder.IsOrderReadAndVerified = order.IsOrderReadAndVerified;
                    editOrder.SignatureDate = order.SignatureDate;
                    editOrder.SignatureText = order.SignatureText;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    if (order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                    {
                        editOrder.PhysicianSignatureText = order.PhysicianSignatureText;
                        editOrder.PhysicianSignatureDate = order.PhysicianSignatureDate;
                        editOrder.ReceivedDate = order.ReceivedDate;
                    }
                    editOrder.PhysicianData = order.PhysicianData;
                    return database.Update<PhysicianOrder>(editOrder) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderModel(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    return database.Update<PhysicianOrder>(order) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend)
        {

            bool result = false;
            try
            {
                var script = @"UPDATE physicianorders po set po.Status = @status, po.ReceivedDate = @datereceived, po.SentDate = @datesend  WHERE po.AgencyId = @agencyid AND po.Id = @id ";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", orderId)
                    .AddInt("status", status)
                    .AddDateTime("datereceived", dateReceived)
                    .AddDateTime("datesend", dateSend)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;


        }

        public bool MarkOrderAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {

            bool result = false;
            try
            {
                var script = @"UPDATE physicianorders po set po.IsDeprecated = @isDeprecated  WHERE po.AgencyId = @agencyid AND po.PatientId = @patientid AND po.Id = @id ";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AddInt("isdeprecated", isDeprecated ? 1 : 0)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            try
            {
                var script = @"UPDATE physicianorders po set po.UserId = @employeeId  WHERE po.AgencyId = @agencyid AND po.PatientId = @patientid AND po.Id = @id ";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AddGuid("employeeid", employeeId)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }


        public PhysicianOrder GetPhysicianOrder(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id );
        }

        public PhysicianOrder GetPhysicianOrder(Guid Id, Guid agencyId)
        {
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.Id == Id);
        }

        public PhysicianOrder GetPhysicianOrderOnly(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.Id == Id);
        }

        public PhysicianOrder GetPhysicianOrderOnly(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
        }


        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                            po.Id as Id, 
                                            po.PhysicianId as PhysicianId, 
                                            po.EpisodeId as EpisodeId, 
                                            po.OrderNumber as OrderNumber, 
                                            p.Id as PatientId , 
                                            p.FirstName as FirstName, 
                                            p.LastName as LastName, 
                                            p.MiddleInitial as MiddleInitial, 
                                            ap.FirstName as PhysicianFirstName,
                                            ap.LastName as PhysicianLastName, 
                                            ap.Credentials as PhysicianCredentials , 
                                            po.Status as Status, 
                                            po.OrderDate as OrderDate, 
                                            po.ReceivedDate as ReceivedDate,
                                            po.SentDate as SentDate, 
                                            po.PhysicianSignatureDate as PhysicianSignatureDate 
                                                FROM physicianorders po
                                                    INNER JOIN agencymanagement.patients p ON po.PatientId = p.Id 
                                                    INNER JOIN agencymanagement.agencyphysicians ap ON po.PhysicianId = ap.Id  
                                                        WHERE 
                                                            po.AgencyId = @agencyid AND 
                                                            p.{0}Status IN (1,2) AND 
                                                            po.IsDeprecated = 0 AND 
                                                            po.Id IN ( {1} ) AND 
                                                            po.OrderDate >= @startDate AND 
                                                            po.OrderDate <= @endDate", this.Service.ToString(), orderIds);
            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                                    po.Id as Id,
                                    po.PhysicianId as PhysicianId,
                                    po.EpisodeId as EpisodeId, 
                                    po.PatientId as PatientId, 
                                    po.OrderNumber as OrderNumber, 
                                    po.Status as Status,
                                    po.OrderDate as OrderDate,
                                    po.ReceivedDate as ReceivedDate,
                                    po.SentDate as SentDate
                                        FROM
                                            physicianorders po
                                                WHERE 
                                                    po.AgencyId = @agencyid AND
                                                    po.IsDeprecated = 0 AND 
                                                    po.Id IN ( {0} )  AND
                                                    po.OrderDate >= @startDate AND 
                                                    po.OrderDate <= @endDate", orderIds);

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                            po.Id as Id, 
                                            po.PhysicianId, 
                                            po.EpisodeId as EpisodeId, 
                                            po.OrderNumber as OrderNumber, 
                                            po.Status as Status , 
                                            po.OrderDate as OrderDate , 
                                            po.ReceivedDate as ReceivedDate, 
                                            po.SentDate as SentDate,
                                            ap.FirstName as PhysicianFirstName, 
                                            ap.LastName as PhysicianLastName
                                                FROM physicianorders po
                                                    INNER JOIN agencymanagement.agencyphysicians ap ON po.PhysicianId = ap.Id  
                                                        WHERE 
                                                            po.AgencyId = @agencyid AND 
                                                            po.PatientId = @patientId AND
                                                            po.IsDeprecated = 0 AND 
                                                            po.Id IN ( {0} ) AND 
                                                            po.OrderDate >= @startDate AND 
                                                            po.OrderDate <= @endDate", orderIds);
            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = patientId,
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                            po.Id as Id, 
                                            po.PhysicianId, 
                                            po.EpisodeId as EpisodeId, 
                                            po.OrderNumber as OrderNumber, 
                                            p.Id as PatientId , 
                                            p.FirstName as FirstName, 
                                            p.LastName as LastName, 
                                            p.MiddleInitial, 
                                            ap.FirstName as PhysicianFirstName, 
                                            ap.LastName as PhysicianLastName, 
                                            po.Status as Status, 
                                            po.OrderDate as OrderDate, 
                                            po.ReceivedDate as ReceivedDate, 
                                            po.SentDate as SentDate, 
                                            po.PhysicianSignatureDate as PhysicianSignatureDate
                                                FROM physicianorders po
                                                    INNER JOIN agencymanagement.patients p ON po.PatientId = p.Id 
                                                    INNER JOIN agencymanagement.agencyphysicians ap ON po.PhysicianId = ap.Id  
                                                        WHERE 
                                                            po.AgencyId = @agencyid AND 
                                                            p.{0}Status IN(1,2) AND
                                                            po.IsDeprecated = 0 AND 
                                                            po.Id IN ( {1} ) AND 
                                                            po.OrderDate >= @startDate AND 
                                                            po.OrderDate <= @endDate",this.Service.ToString(),orderIds);
            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase(),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.IsDeprecated == false).ToList();
        }

        public List<PhysicianOrder> GetAllPhysicianOrders()
        {
            return database.All<PhysicianOrder>().ToList();
        }

        #endregion

        #region Md.Axxessweb.com

        public List<PhysicianOrder> GetPhysicianOrdersByPhysician(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();
            var list = new List<PhysicianOrder>();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `physicianorders` po")
                .Where(string.Format("po.Status = {0}", status))
                .And("po.IsDeprecated = 0")
                .And(string.Format("po.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            using (var cmd = new FluentCommand<PhysicianOrder>(query.Build()))
            {
                list = cmd.SetConnection(connectionStringName).AsList();
            }

            return list;
        }

        #endregion
    }
}
