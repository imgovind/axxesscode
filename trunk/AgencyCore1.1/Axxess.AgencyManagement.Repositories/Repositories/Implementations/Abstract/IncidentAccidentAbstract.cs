﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using System.Xml.Linq;
    using Axxess.Core;

   public abstract class IncidentAccidentAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;

        #region Incident Report

        public bool AddIncident(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    incident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                incident.Created = DateTime.Now;
                incident.Modified = DateTime.Now;

                database.Add<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool AddMultipleIncident(List<Incident> incidents)
        {
            var result = false;
            if (incidents != null && incidents.Count > 0)
            {
                try
                {
                    database.AddMany<Incident>(incidents);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteIncident(Guid Id)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = true;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool RemoveIncident(Guid agencyId, Guid patientId, Guid Id)
        {

            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM incidents inc WHERE inc.AgencyId = @agencyid AND inc.PatientId = @patientid AND inc.Id = @id ");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id).AsNonQuery();

                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = isDeprecated;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                try
                {
                    incident.UserId = employeeId;
                    incident.Modified = DateTime.Now;
                    database.Update<Incident>(incident);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateIncident(Incident incident)
        {
            var result = false;
            var existingIncident = GetIncidentReport(incident.AgencyId, incident.Id);
            if (existingIncident != null)
            {
                existingIncident.Orders = incident.Orders;
                existingIncident.Description = incident.Description;
                existingIncident.ActionTaken = incident.ActionTaken;
                existingIncident.IncidentType = incident.IncidentType;
                existingIncident.MDNotified = incident.MDNotified;
                existingIncident.NewOrdersCreated = incident.NewOrdersCreated;
                existingIncident.FamilyNotified = incident.FamilyNotified;

                existingIncident.PhysicianId = incident.PhysicianId;
                existingIncident.IncidentDate = incident.IncidentDate;
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.Status = incident.Status;

                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    existingIncident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.FollowUp = incident.FollowUp;
                existingIncident.SignatureDate = incident.SignatureDate;
                existingIncident.SignatureText = incident.SignatureText;
                existingIncident.Modified = DateTime.Now;

                database.Update<Incident>(existingIncident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncidentModal(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public Incident GetIncidentReport(Guid agencyId, Guid incidentId)
        {
            return database.Single<Incident>(i => i.AgencyId == agencyId && i.Id == incidentId);
        }

        public IList<Incident> GetIncidents(Guid agencyId)
        {
            return database.Find<Incident>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        #endregion

    }
}
