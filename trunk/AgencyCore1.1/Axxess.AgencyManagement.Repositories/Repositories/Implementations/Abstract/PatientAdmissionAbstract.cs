﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Transactions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    using Axxess.AgencyManagement.Repositories;
    using System.Web.Mvc;

    public abstract class PatientAdmissionAbstract
    {
        #region Constructor

        protected SimpleRepository database;
        protected string connectionStringName;


        #endregion

        #region Patient Admission Members


        #region Patient Admission

        public bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
            try
            {
                if (managedDate != null)
                {
                    managedDate.Created = DateTime.Now;
                    managedDate.Modified = DateTime.Now;
                    database.Add<PatientAdmissionDate>(managedDate);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool PatientAdmissionEdit(Patient patient, Profile profile)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    var admissionToEdit = database.Single<PatientAdmissionDate>(p => p.AgencyId == patient.AgencyId && p.Id == profile.AdmissionId && p.PatientId == patient.Id);
                    if (admissionToEdit != null)
                    {
                        var patientToEdit = admissionToEdit.PatientData.ToObject<Patient>() ?? new Patient();
                        if (patientToEdit != null)
                        {
                            patientToEdit.FirstName = patient.FirstName;
                            patientToEdit.MiddleInitial = patient.MiddleInitial;
                            patientToEdit.LastName = patient.LastName;
                            patientToEdit.Gender = patient.Gender;
                            patientToEdit.DOB = patient.DOB;
                            patientToEdit.MaritalStatus = patient.MaritalStatus;
                            patientToEdit.Height = patient.Height;
                            patientToEdit.HeightMetric = patient.HeightMetric;
                            patientToEdit.Weight = patient.Weight;
                            patientToEdit.WeightMetric = patient.WeightMetric;
                            patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                            patientToEdit.MedicareNumber = patient.MedicareNumber;
                            patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                            patientToEdit.SSN = patient.SSN;
                            patientToEdit.Ethnicities = patient.Ethnicities;
                            //patientToEdit.PaymentSource = patient.PaymentSource;
                            //patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                            patientToEdit.AddressLine1 = patient.AddressLine1;
                            patientToEdit.AddressLine2 = patient.AddressLine2;
                            patientToEdit.AddressCity = patient.AddressCity;
                            patientToEdit.AddressStateCode = patient.AddressStateCode;
                            patientToEdit.AddressZipCode = patient.AddressZipCode;
                            patientToEdit.PhoneHome = patient.PhoneHome;
                            patientToEdit.PhoneMobile = patient.PhoneMobile;
                            patientToEdit.EmailAddress = patient.EmailAddress;
                            patientToEdit.PharmacyName = patient.PharmacyName;
                            patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                            patientToEdit.Triage = patient.Triage;
                            patientToEdit.EvacuationZone = patient.EvacuationZone;
                            patientToEdit.ServicesRequired = patient.ServicesRequired;
                            patientToEdit.DME = patient.DME;
                            patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                           
                            patientToEdit.IsDNR = patient.IsDNR;
                            patientToEdit.Comments = patient.Comments;
                           
                           
                            var profileToEdit = admissionToEdit.ProfileData.ToObject<Profile>() ?? new Profile();
                            if (profileToEdit != null)
                            {
                                profileToEdit.AgencyLocationId = profile.AgencyLocationId;
                                profileToEdit.StartofCareDate = profile.StartofCareDate;
                                profileToEdit.DischargeDate = profile.DischargeDate;
                                profileToEdit.CaseManagerId = profile.CaseManagerId;
                                profileToEdit.UserId = profile.UserId;
                                profileToEdit.PrimaryInsurance = profile.PrimaryInsurance;
                                if (profile.PrimaryInsurance >= 1000)
                                {
                                    profileToEdit.PrimaryHealthPlanId = profile.PrimaryHealthPlanId;
                                    profileToEdit.PrimaryGroupName = profile.PrimaryGroupName;
                                    profileToEdit.PrimaryGroupId = profile.PrimaryGroupId;
                                }
                                profileToEdit.SecondaryInsurance = profile.SecondaryInsurance;
                                if (profile.SecondaryInsurance >= 1000)
                                {
                                    profileToEdit.SecondaryHealthPlanId = profile.SecondaryHealthPlanId;
                                    profileToEdit.SecondaryGroupName = profile.SecondaryGroupName;
                                    profileToEdit.SecondaryGroupId = profile.SecondaryGroupId;
                                }
                                profileToEdit.TertiaryInsurance = profile.TertiaryInsurance;
                                if (profile.TertiaryInsurance >= 1000)
                                {
                                    profileToEdit.TertiaryHealthPlanId = profile.TertiaryHealthPlanId;
                                    profileToEdit.TertiaryGroupName = profile.TertiaryGroupName;
                                    profileToEdit.TertiaryGroupId = profile.TertiaryGroupId;
                                }
                                profileToEdit.DischargeReason = profile.DischargeReason;
                                profileToEdit.AdmissionId = profile.AdmissionId;
                                profileToEdit.StartofCareDate = profile.StartofCareDate;
                                profileToEdit.DischargeDate = profile.DischargeDate;
                                profileToEdit.PaymentSource = profile.PaymentSource;
                                profileToEdit.OtherPaymentSource = profile.OtherPaymentSource;

                                profileToEdit.AdmissionSource = profile.AdmissionSource;
                                profileToEdit.OtherReferralSource = profile.OtherReferralSource;
                                profileToEdit.ReferralDate = profile.ReferralDate;
                                profileToEdit.InternalReferral = profile.InternalReferral;

                                admissionToEdit.StartOfCareDate = profile.StartofCareDate;
                                admissionToEdit.DischargedDate = profile.DischargeDate;

                                
                                admissionToEdit.PatientData = patientToEdit.ToXml();
                                admissionToEdit.ProfileData = profileToEdit.ToXml();
                                patientToEdit.Modified = DateTime.Now;
                                if (database.Update<PatientAdmissionDate>(admissionToEdit) > 0)
                                {
                                    result = true;
                                }

                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool PatientAdmissionAdd(Patient patient, Profile profile, Patient exisitingPatientData, Profile exisitingProfileData)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    if (exisitingPatientData != null)
                    {
                        var admissionToAdd = new PatientAdmissionDate { 
                            Id = Guid.NewGuid(),
                            PatientId = patient.Id,
                            AgencyId = patient.AgencyId,
                            StartOfCareDate = profile.StartofCareDate,
                            DischargedDate = profile.DischargeDate,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,
                            IsDeprecated = false, 
                            IsActive = true,
                            Status = profile.Status
                        };

                        exisitingProfileData.AgencyLocationId = profile.AgencyLocationId;
                        exisitingProfileData.StartofCareDate = profile.StartofCareDate;
                        exisitingProfileData.DischargeDate = profile.DischargeDate;
                        exisitingProfileData.DischargeReason = profile.DischargeReason;
                        exisitingProfileData.CaseManagerId = profile.CaseManagerId;
                        exisitingProfileData.UserId = profile.UserId;
                        exisitingProfileData.PrimaryInsurance = profile.PrimaryInsurance;
                        if (profile.PrimaryInsurance >= 1000)
                        {
                            exisitingProfileData.PrimaryHealthPlanId = profile.PrimaryHealthPlanId;
                            exisitingProfileData.PrimaryGroupName = profile.PrimaryGroupName;
                            exisitingProfileData.PrimaryGroupId = profile.PrimaryGroupId;
                        }
                        else
                        {
                            exisitingProfileData.SecondaryInsurance = profile.SecondaryInsurance;
                        }
                        if (profile.SecondaryInsurance >= 1000)
                        {
                            exisitingProfileData.SecondaryHealthPlanId = profile.SecondaryHealthPlanId;
                            exisitingProfileData.SecondaryGroupName = profile.SecondaryGroupName;
                            exisitingProfileData.SecondaryGroupId = profile.SecondaryGroupId;
                        }
                        else
                        {
                            exisitingProfileData.TertiaryInsurance = profile.TertiaryInsurance;
                        }
                        if (profile.TertiaryInsurance >= 1000)
                        {
                            exisitingProfileData.TertiaryHealthPlanId = profile.TertiaryHealthPlanId;
                            exisitingProfileData.TertiaryGroupName = profile.TertiaryGroupName;
                            exisitingProfileData.TertiaryGroupId = profile.TertiaryGroupId;
                        }
                        else
                        {
                            exisitingProfileData.AdmissionId = admissionToAdd.Id;
                        }
                        exisitingProfileData.PaymentSource = profile.PaymentSource;
                        exisitingProfileData.OtherPaymentSource = profile.OtherPaymentSource;
                        exisitingProfileData.StartofCareDate = profile.StartofCareDate;
                        exisitingProfileData.DischargeDate = profile.DischargeDate;
                        exisitingProfileData.AdmissionSource = profile.AdmissionSource;
                        exisitingProfileData.OtherReferralSource = profile.OtherReferralSource;
                        exisitingProfileData.ReferralDate = profile.ReferralDate;
                        exisitingProfileData.InternalReferral = profile.InternalReferral;

                        admissionToAdd.StartOfCareDate = profile.StartofCareDate;
                        admissionToAdd.DischargedDate = profile.DischargeDate;

                        exisitingPatientData.FirstName = patient.FirstName;
                        exisitingPatientData.MiddleInitial = patient.MiddleInitial;
                        exisitingPatientData.LastName = patient.LastName;
                        exisitingPatientData.Gender = patient.Gender;
                        exisitingPatientData.DOB = patient.DOB;
                        exisitingPatientData.IsDNR = patient.IsDNR;
                        exisitingPatientData.MaritalStatus = patient.MaritalStatus;
                        exisitingPatientData.Height = patient.Height;
                        exisitingPatientData.HeightMetric = patient.HeightMetric;
                        exisitingPatientData.Weight = patient.Weight;
                        exisitingPatientData.WeightMetric = patient.WeightMetric;
                        exisitingPatientData.PatientIdNumber = patient.PatientIdNumber;
                        exisitingPatientData.MedicareNumber = patient.MedicareNumber;
                        exisitingPatientData.MedicaidNumber = patient.MedicaidNumber;
                        exisitingPatientData.SSN = patient.SSN;
                        exisitingPatientData.Ethnicities = patient.Ethnicities;
                        //exisitingPatientData.PaymentSource = patient.PaymentSource;
                        //exisitingPatientData.OtherPaymentSource = patient.OtherPaymentSource;
                        exisitingPatientData.AddressLine1 = patient.AddressLine1;
                        exisitingPatientData.AddressLine2 = patient.AddressLine2;
                        exisitingPatientData.AddressCity = patient.AddressCity;
                        exisitingPatientData.AddressStateCode = patient.AddressStateCode;
                        exisitingPatientData.AddressZipCode = patient.AddressZipCode;
                        exisitingPatientData.PhoneHome = patient.PhoneHome;
                        exisitingPatientData.PhoneMobile = patient.PhoneMobile;
                        exisitingPatientData.EmailAddress = patient.EmailAddress;
                        exisitingPatientData.PharmacyName = patient.PharmacyName;
                        exisitingPatientData.PharmacyPhone = patient.PharmacyPhone;
                        exisitingPatientData.Triage = patient.Triage;
                        exisitingPatientData.EvacuationZone = patient.EvacuationZone;
                        exisitingPatientData.ServicesRequired = patient.ServicesRequired;
                        exisitingPatientData.DME = patient.DME;
                        exisitingPatientData.ReferrerPhysician = patient.ReferrerPhysician;
                        
                        exisitingPatientData.Comments = patient.Comments;
                        admissionToAdd.PatientData = exisitingPatientData.ToXml();
                        admissionToAdd.ProfileData = exisitingProfileData.ToXml();
                        database.Add<PatientAdmissionDate>(admissionToAdd);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
           
            try
            {
                if (managedDate != null)
                {
                    var managedDateToEdit = database.Single<PatientAdmissionDate>(s => s.AgencyId == managedDate.AgencyId && s.PatientId == managedDate.PatientId && s.Id == managedDate.Id);
                    if (managedDateToEdit != null)
                    {
                        managedDateToEdit.StartOfCareDate = managedDate.StartOfCareDate;
                        managedDateToEdit.DischargedDate = managedDate.DischargedDate;
                        managedDateToEdit.PatientData = managedDate.PatientData;
                        managedDateToEdit.Status = managedDate.Status;
                        managedDateToEdit.Reason = managedDate.Reason;
                        managedDateToEdit.Modified = DateTime.Now;
                        if (database.Update<PatientAdmissionDate>(managedDateToEdit) > 0)
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate)
        {
            var result = false;
            if (managedDate != null)
            {
                try
                {
                    managedDate.Modified = DateTime.Now;
                    return database.Update<PatientAdmissionDate>(managedDate) > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool RemovePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientadmissiondates WHERE AgencyId = @agencyid AND  PatientId = @patientid AND Id=@id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id",Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
           
        }

        public bool RemovePatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientadmissiondates WHERE AgencyId = @agencyid AND  PatientId = @patientid ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            var managedDate = database.Single<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == Id);
            try
            {
                if (managedDate != null)
                {
                    managedDate.IsDeprecated = true;
                    managedDate.Modified = DateTime.Now;
                    return database.Update<PatientAdmissionDate>(managedDate) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var result = false;
            var managedDates = database.Find<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId);
            try
            {
                if (managedDates.IsNotNullOrEmpty())
                {
                    managedDates.ForEach(m => { m.IsDeprecated = true; m.Modified = DateTime.Now; });
                    return database.UpdateMany<PatientAdmissionDate>(managedDates) > 0;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateAdmissionForInsuranceIds(Guid agencyId, Guid patientId, Guid admissionId, string insuranceIds)
        {
            var count = 0;
            try
            {
                var script = @"UPDATE patientadmissiondates set InsuranceIds = @insuranceIds WHERE AgencyId = @agencyid AND PatientId = @patientId AND Id = @admissionId;";
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientId", patientId)
                          .AddGuid("admissionId", admissionId)
                         .AddString("insuranceIds", insuranceIds)
                         .AsNonQuery();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return count > 0;
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<PatientAdmissionDate>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.Id == Id);
        }


        public PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date)
        {
            PatientAdmissionDate managedDate = null;
            try
            {
                var script = @"SELECT
                            pad.Id, 
                            pad.Status,
                            pad.DischargedDate,
                            pad.StartOfCareDate 
                                FROM 
                                    patientadmissiondates pad
                                                WHERE
                                                    pad.AgencyId = @agencyId AND
                                                    pad.PatientId = @patientId AND
                                                    pad.StartOfCareDate <= @date AND
                                                    pad.IsDeprecated = 0  
                                                        ORDER BY pad.StartOfCareDate DESC 
                                                            LIMIT 1";
                using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
                {
                    managedDate = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("patientId", patientId)
                        .AddDateTime("date", date)
                    .SetMap(reader => new PatientAdmissionDate
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        Status = reader.GetInt("Status"),
                        DischargedDate = reader.GetDateTime("DischargedDate"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                    })
                    .AsSingle();
                }
            }
            catch (Exception)
            {
                return managedDate;
            }
            return managedDate;
        }

        public IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId)
        {

            var patientAdmissionDates = new List<PatientAdmissionDate>();
            try
            {
                var script = @"SELECT 
                        pad.Id, 
                        pad.Status,
                        pad.StartOfCareDate, 
                        pad.DischargedDate
                            FROM
                                patientadmissiondates pad
                                        WHERE 
                                            pad.AgencyId = @agencyid AND
                                            pad.PatientId = @patientid AND 
                                            pad.IsDeprecated = 0 
                                            ORDER BY pad.StartOfCareDate DESC ";



                using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
                {
                    patientAdmissionDates = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new PatientAdmissionDate
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        Status = reader.GetInt("Status"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        DischargedDate = reader.GetDateTime("DischargedDate")
                    })
                    .AsList();
                }
            }
            catch (Exception)
            {
                return patientAdmissionDates;
            }

            return patientAdmissionDates;
        }

        public IList<PatientAdmissionDate> GetAllPatientAdmissionDates(Guid agencyId, Guid patientId)
        {

            var patientAdmissionDates = new List<PatientAdmissionDate>();
            try
            {
                var script = @"SELECT 
                        pad.Id, 
                        pad.Status,
                        pad.StartOfCareDate, 
                        pad.DischargedDate,
                        pad.InsuranceIds,
                            FROM
                                patientadmissiondates pad
                                        WHERE 
                                            pad.AgencyId = @agencyid AND
                                            pad.PatientId = @patientid ";



                using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
                {
                    patientAdmissionDates = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new PatientAdmissionDate
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        Status = reader.GetInt("Status"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        DischargedDate = reader.GetDateTime("DischargedDate"),
                        InsuranceIds = reader.GetStringNullable("InsuranceIds")
                    })
                    .AsList();
                }
            }
            catch (Exception)
            {
                return patientAdmissionDates;
            }
            return patientAdmissionDates;
        }

        public List<PatientAdmission> GetPatientAdmissionsByDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientAdmission>();
            try
            {
                var script = string.Format(@"SELECT DISTINCT 
	                                pr.Id as Id,
	                                pr.FirstName as FirstName,
	                                pr.MiddleInitial, 
	                                pr.PrimaryInsurance,
	                                pr.LastName as LastName,
	                                pr.PatientIdNumber as PatientIdNumber,
	                                p.PhoneHome as PhoneHome,
	                                pad.`Status` as `Status`,
	                                p.Gender as Gender, 
	                                pad.StartOfCareDate as StartOfCareDate,
	                                pad.DischargedDate as DischargeDate,
	                                pr.InternalReferral as InternalReferral
		                                FROM 
		                                    patientadmissiondates pad
			                                    INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
			                                     INNER JOIN agencymanagement.patients p ON p.Id = pr.Id
				                                    WHERE
                                                        pr.AgencyId = @agencyid AND {0}
                                                        pr.IsDeprecated = 0 AND 
                                                        pr.InternalReferral != '00000000-0000-0000-0000-000000000000' AND
				                                        pad.IsDeprecated = 0 AND
                                                        pad.`Status` IN (1,2,3,4) AND
		                                                DATE(pad.StartOfCareDate) between DATE(@startDate) AND DATE(@endDate)",
                                !branchId.IsEmpty() ? " pr.AgencyLocationId = @branchId AND" : string.Empty);

                using (var cmd = new FluentCommand<PatientAdmission>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new PatientAdmission
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PhoneHome = reader.GetStringNullable("PhoneHome").ToPhone(),
                        StartOfCareDate = reader.GetDateTime("StartofCareDate"),
                        InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                        //Admit = reader.GetInt("Admit"),
                        Status = reader.GetInt("Status"),
                        DischargedDate = reader.GetDateTime("DischargeDate"),
                        InternalReferral = reader.GetGuidIncludeEmpty("InternalReferral"),
                    }).AsList();
                }
            }
            catch (Exception)
            {
                return list;
            }
            return list;
        }

        public List<PatientRoster> GetPatientByAdmissionMonthYear(Guid agencyId, Guid branchId, int status, int month, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            try
            {
                var statusScript = string.Empty;
                if (status == 0)
                {
                    statusScript = " AND pr.Status IN (1,2) ";
                }
                else
                {
                    statusScript = " AND pr.Status = @statusid ";
                }
                var branchScript = string.Empty;
                if (!branchId.IsEmpty())
                {
                    branchScript = " AND pr.AgencyLocationId = @branchId ";
                }

                var script = string.Format(
                        @"SELECT 
                        pr.FirstName as FirstName,
                        pr.LastName as LastName,
                        pr.MiddleInitial,
                        pr.PatientIdNumber as PatientIdNumber, 
                        pr.AdmissionSource as AdmissionSource,
                        pad.StartOfCareDate as StartOfCareDate, 
                        pr.OtherReferralSource,
                        pr.InternalReferral as InternalReferralId, 
                        pr.ReferrerPhysician as ReferrerPhysicianId, 
                        pr.ReferralDate,
                        pr.Status 
                            FROM
                                patientadmissiondates pad
                                    INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
                                        WHERE 
                                            pr.AgencyId = @agencyId AND
                                            month(pad.StartOfCareDate) = @month AND 
                                            year(pad.StartOfCareDate) = @year {0} {1} AND
                                            pr.IsDeprecated = 0  AND 
                                            pad.IsDeprecated = 0 
                                                GROUP BY 
                                                    pr.Id, pad.StartOfCareDate 
                                                        ORDER BY 
                                                             pr.LastName ASC, pr.FirstName ASC, pad.StartOfCareDate DESC ", statusScript, branchScript);
                using (var cmd = new FluentCommand<PatientRoster>(script))
                {
                    patientsByAdmission = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("branchId", branchId)
                        .AddInt("statusid", status)
                        .AddInt("month", month)
                        .AddInt("year", year)
                        .SetMap(reader => new PatientRoster
                        {
                            PatientStatus = reader.GetInt("Status"),
                            PatientId = reader.GetStringNullable("PatientIdNumber"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                            PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                            OtherReferralSource = reader.GetStringNullable("OtherReferralSource"),
                            InternalReferralId = reader.GetGuidIncludeEmpty("InternalReferralId"),
                            ReferrerPhysicianId = reader.GetGuidIncludeEmpty("ReferrerPhysicianId"),
                            ReferralDate = reader.GetDateTime("ReferralDate"),
                            PatientSoC = reader.GetDateTime("StartOfCareDate")
                        }).AsList();
                }
            }
            catch (Exception)
            {
                return patientsByAdmission;
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionMonthYearUnduplicated(Guid agencyId, Guid branchId, int status, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            try
            {
                var script = string.Format(@"SELECT 
                            pr.Id ,
                            pr.FirstName as FirstName ,
                            pr.LastName as LastName, 
                            pr.MiddleInitial as MiddleInitial,
                            pr.PatientIdNumber as PatientIdNumber  ,
                            pr.AdmissionSource as AdmissionSource , 
                            max(pad.StartOfCareDate) as StartOfCareDate
                                FROM
                                    patientadmissiondates pad
                                        INNER JOIN patientprofiles pr  ON pad.PatientId = pr.Id 
                                            WHERE 
                                                pr.AgencyId = @agencyId  AND
                                                year(pad.StartOfCareDate) = @year {0} {1} AND
                                                pr.IsDeprecated = 0 AND 
                                                pad.IsDeprecated = 0
                                                    GROUP BY pr.Id 
                                                        ORDER BY pr.LastName ASC , pr.FirstName ASC , pad.StartOfCareDate DESC ",
                                                                       status == 0 ? " AND pr.Status IN (1,2) " : " AND pr.Status = @statusid ",
                                                                      !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty);
                using (var cmd = new FluentCommand<PatientRoster>(script))
                {
                    patientsByAdmission = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("branchId", branchId)
                        .AddInt("statusid", status)
                        .AddInt("year", year)
                        .SetMap(reader => new PatientRoster
                        {
                            PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                            PatientId = reader.GetStringNullable("PatientIdNumber"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                            PatientSoC = reader.GetDateTime("StartOfCareDate")
                        }).AsList();
                }
            }
            catch (Exception)
            {
                return patientsByAdmission;
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var patientsByAdmission = new List<PatientRoster>();
            try
            {
                var script = string.Format(@"SELECT 
                            pr.Id ,
                            pr.FirstName as FirstName ,
                            pr.LastName as LastName ,
                            pr.MiddleInitial as MiddleInitial, 
                            pr.PatientIdNumber as PatientIdNumber  ,
                            pr.Status as Status ,  
                            pr.AdmissionSource as AdmissionSource  ,
                            pad.DischargedDate as DischargeDate ,
                            max(pad.StartOfCareDate) as StartOfCareDate 
                                FROM 
                                    patientadmissiondates pad 
                                        INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
                                            WHERE  
                                                pr.AgencyId = @agencyId  AND
                                                DATE(pad.StartOfCareDate) between DATE(@startDate) and DATE(@enddate)  {0} {1} AND 
                                                pr.IsDeprecated = 0 AND 
                                                pad.IsDeprecated = 0
                                                    GROUP BY pr.Id  
                                                        ORDER BY pr.LastName ASC , pr.FirstName ASC , pad.StartOfCareDate DESC ",
                                                                   status == 0 ? " AND pr.Status IN (1,2)" : " AND pr.Status = @statusid ",
                                                                  !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty);
                using (var cmd = new FluentCommand<PatientRoster>(script))
                {
                    patientsByAdmission = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("branchId", branchId)
                        .AddInt("statusid", status)
                        .AddDateTime("startdate", startDate)
                        .AddDateTime("enddate", endDate)
                        .SetMap(reader => new PatientRoster
                        {
                            PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                            PatientId = reader.GetStringNullable("PatientIdNumber"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                            PatientSoC = reader.GetDateTime("StartOfCareDate"),
                            PatientStatus = reader.GetInt("Status"),
                            PatientDischargeDate = reader.GetInt("Status") == (int)PatientStatus.Discharged && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate") : DateTime.MinValue
                        }).AsList();
                }
            }
            catch (Exception)
            {
                return patientsByAdmission;
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionYear(Guid agencyId, Guid branchId, int status, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            try
            {
                var script = string.Format(@"SELECT
                            pr.FirstName as FirstName , 
                            pr.LastName as LastName ,
                            pr.MiddleInitial as MiddleInitial,
                            pr.PatientIdNumber as PatientIdNumber  ,  
                            pr.AdmissionSource as AdmissionSource ,
                            pad.StartOfCareDate as StartOfCareDate 
                                FROM 
                                    patientadmissiondates pad
                                        INNER JOIN patientprofiles pr  ON pad.PatientId = pr.Id 
                                            WHERE 
                                                pr.AgencyId = @agencyId  AND 
                                                year(pad.StartOfCareDate) = @year {0} {1} AND
                                                pad.IsDeprecated = 0
                                                     GROUP BY pr.Id, pad.StartOfCareDate
                                                            ORDER BY pr.LastName ASC , pr.FirstName ASC , pad.StartOfCareDate DESC ",
                                                                status == 0 ? " AND pr.Status IN (1,2) " : " AND pr.Status = @statusid ",
                                                               !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId" : string.Empty);
                using (var cmd = new FluentCommand<PatientRoster>(script))
                {
                    patientsByAdmission = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("branchId", branchId)
                        .AddInt("statusid", status)
                        .AddInt("year", year)
                        .SetMap(reader => new PatientRoster
                        {
                            PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                            PatientId = reader.GetStringNullable("PatientIdNumber"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                            PatientSoC = reader.GetDateTime("StartOfCareDate")
                        }).AsList();
                }
            }
            catch (Exception)
            {
                return patientsByAdmission;
            }
            return patientsByAdmission;
        }

        public IList<SelectListItem> GetPatientAdmissionDateSelectList(Guid agencyId, Guid patientId)
        {
            var patientAdmissionDates = new List<SelectListItem>();
            try
            {
                var script = @"SELECT 
                        pad.Id as Id, 
                        pad.StartOfCareDate as StartOfCareDate, 
                        pad.Id = pr.AdmissionId as IsCurrent
                            FROM 
                                patientadmissiondates pad
                                    INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
                                            WHERE 
                                                pad.AgencyId = @agencyid AND
                                                pad.PatientId = @patientid AND 
                                                pad.IsDeprecated = 0 
                                                    ORDER BY pad.StartOfCareDate DESC ";

                using (var cmd = new FluentCommand<SelectListItem>(script))
                {
                    patientAdmissionDates = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new SelectListItem
                    {
                        Selected = reader.GetBoolean("IsCurrent"),
                        Text = reader.GetDateTime("StartOfCareDate").ToZeroFilled(),
                        Value = reader.GetGuid("Id").ToString(),
                    })
                    .AsList();
                }
            }
            catch (Exception)
            {
                return patientAdmissionDates;
            }
            return patientAdmissionDates;
        }
       
        //TODO: need discussion
        public Dictionary<string, int> GetPatientAdmits(Guid agencyId, Guid branchId)
        {
            var list = new Dictionary<string, int>();
            try
            {
                var script = string.Format(@"SELECT DISTINCT 
	                                pr.Id as Id,
	                                count(pad.Id) as Admit 
		                                FROM 
		                                  patientadmissiondates pad
			                                  INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
				                                    WHERE 
                                                        pr.IsDeprecated = 0 AND 
                                                        pr.InternalReferral != '00000000-0000-0000-0000-000000000000' AND
				                                        pad.IsDeprecated = 0 AND
		                                                pr.AgencyId = @agencyid AND {0}
                                                        pad.`Status` IN(1,2,3,4)
		       	                                            GROUP BY pad.PatientId",
                            !branchId.IsEmpty() ? " pr.AgencyLocationId = @branchId AND" : string.Empty);

                using (var cmd = new FluentCommand<int>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetDictonaryId("Id")
                    .SetMap(reader => reader.GetInt("Admit")).AsDictionary();
                }
            }
            catch (Exception)
            {
                return list;
            }
            return list;
        }

        public Dictionary<string, int> GetPatientAdmits(Guid agencyId, Guid branchId, List<Guid> patientIds)
        {
            var list = new Dictionary<string, int>();
            try
            {
                var script = string.Format(@"SELECT DISTINCT 
	                                pr.Id as Id,
	                                count(pad.Id) as Admit 
		                                FROM 
		                                  patientadmissiondates pad
			                                  INNER JOIN patientprofiles pr ON pad.PatientId = pr.Id 
				                                    WHERE 
				                                        pad.IsDeprecated = 0 AND
		                                                pad.AgencyId = @agencyid {0} AND
                                                        pad.PatientId IN({1})
		       	                                            GROUP BY pad.PatientId",
                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId  " : string.Empty, patientIds.ToCommaSeperatedList());

                using (var cmd = new FluentCommand<int>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetDictonaryId("Id")
                    .SetMap(reader => reader.GetInt("Admit")).AsDictionary();
                }
            }
            catch (Exception)
            {
                return list;
            }
            return list;
        }


        #endregion


        #endregion
    }
}
