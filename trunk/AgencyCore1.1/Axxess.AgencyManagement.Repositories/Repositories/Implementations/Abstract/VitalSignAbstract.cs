﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.DataProviders;
    using SubSonic.Linq.Structure;
    using SubSonic.Query;
    using SubSonic.Repository;

    public interface IVitalSignRepository
    {
        bool Add(VitalSignLog vitalSignLog);

        VitalSignLog Get(Guid id, Guid entityId, Guid patientId, Guid agencyId);

        VitalSignLog GetEntitiesVitalSign(Guid entityId, Guid patientId, Guid agencyId);

        List<VitalSignLogLean> GetEntitiesVitalSignLogsLean(Guid entityId, Guid patientId, Guid agencyId);

        bool AddOrUpdate(VitalSignLog log, ref VitalSignLog logToUpdate);

        bool Update(VitalSignLog log);

        bool UpdateModal(VitalSignLog logToUpdate, VitalSignLog log);

        bool RemoveModel(Guid agencyId, Guid patientId, Guid id);

        bool RestoreData(VitalSignLog oldLog, VitalSignLog newLog);

        bool ToggleDeprecation(Guid agencyId, Guid patientId, Guid id, bool isDeprecated);
    }

    public abstract class VitalSignAbstract : IVitalSignRepository
    {
        protected SimpleRepository database;

        private string connectionStringName;

        public VitalSignAbstract(SimpleRepository database, string connectionStringName)
        {
            this.database = database;
            this.connectionStringName = connectionStringName;
        }

        public bool Add(VitalSignLog vitalSignLog)
        {
            bool result = false;
            try
            {
                vitalSignLog.Id = Guid.NewGuid();
                vitalSignLog.Created = DateTime.Now;
                vitalSignLog.Modified = DateTime.Now;
                database.Add(vitalSignLog);
                result = true;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public VitalSignLog Get(Guid id, Guid entityId, Guid patientId, Guid agencyId)
        {
            return database.Single<VitalSignLog>(s => s.Id == id && s.EntityId == entityId && s.PatientId == patientId && s.AgencyId == agencyId);
        }

        public VitalSignLog GetEntitiesVitalSign(Guid entityId, Guid patientId, Guid agencyId)
        {
            return database.Single<VitalSignLog>(s => s.EntityId == entityId && s.PatientId == patientId && s.AgencyId == agencyId);
        }
       
        public List<VitalSignLogLean> GetEntitiesVitalSignLogsLean(Guid entityId, Guid patientId, Guid agencyId)
        {
            var script = @"SELECT 
                            Id, 
                            VisitDate,
                            Temp,
                            TempUnit,
                            Resp,
                            Weight,
                            WeightUnit,
                            Pulse,
                            Pulse2,
                            Pain,
                            BloodPressure,
                            BloodPressure2,
                            BloodPressure3,
                            BloodPressure4,
                            BloodPressure5,
                            BloodPressure6,
                            Comments
                                FROM 
                                    vitalsignlogs
                                        WHERE 
                                            AgencyId = @agencyid AND 
                                            PatientId = @patientid AND
                                            EntityId = @entityid AND 
                                            IsDeprecated = 0";

            var list = new List<VitalSignLog>();
            using (var cmd = new FluentCommand<VitalSignLog>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("entityid", entityId)
                        .SetMap(reader => new VitalSignLog
                        {
                            Id = reader.GetGuid("Id"),
                            PatientId = patientId,
                            EntityId = entityId,
                            AgencyId = agencyId,
                            VisitDate = reader.GetDateTimeWithMin("VisitDate"),
                            Temp = reader.GetDoubleNullable("Temp"),
                            TempUnit = reader.GetInt("TempUnit"),
                            Weight = reader.GetDoubleNullable("Weight"),
                            WeightUnit = reader.GetInt("WeightUnit"),
                            Resp = reader.GetIntNullable("Resp"),
                            Pulse = reader.GetIntNullable("Pulse"),
                            Pulse2 = reader.GetIntNullable("Pulse2"),
                            BloodPressure = reader.GetStringNullable("BloodPressure"),
                            BloodPressure2 = reader.GetStringNullable("BloodPressure2"),
                            BloodPressure3 = reader.GetStringNullable("BloodPressure3"),
                            BloodPressure4 = reader.GetStringNullable("BloodPressure4"),
                            BloodPressure5 = reader.GetStringNullable("BloodPressure5"),
                            BloodPressure6 = reader.GetStringNullable("BloodPressure6"),
                            Pain = reader.GetIntNullable("Pain"),
                            Comments = reader.GetStringNullable("Comments"),
                        })
                        .AsList();
            }
            return list.Select(vitalSignLog => new VitalSignLogLean(vitalSignLog)).ToList();
        }

        public List<VitalSignLog> GetPatientVitalSignLogsLean(Guid patientId, Guid agencyId)
        {
            var script = @"SELECT 
                            Id, 
                            EntityId,
                            VisitDate,
                            Temp,
                            TempUnit,
                            Resp,
                            Pain,
                            Pulse, 
                            Weight,
                            PulseLocation,
                            Pulse2,
                            PulseLocation2,
                            BloodPressure,
                            BloodPressure2,
                            BloodPressure3,
                            BloodPressure4,
                            BloodPressure5,
                            BloodPressure6
                                FROM 
                                    vitalsignlogs
                                        WHERE 
                                            AgencyId = @agencyid AND 
                                            PatientId = @patientid AND
                                            IsDeprecated = 0";

            var list = new List<VitalSignLog>();
            using (var cmd = new FluentCommand<VitalSignLog>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .SetMap(reader => new VitalSignLog
                        {
                            Id = reader.GetGuid("Id"),
                            PatientId = patientId,
                            EntityId = reader.GetGuid("EntityId"),
                            VisitDate = reader.GetDateTimeWithMin("VisitDate"),
                            Temp = reader.GetDoubleNullable("Temp"),
                            TempUnit = reader.GetInt("TempUnit"),
                            Resp = reader.GetIntNullable("Resp"),
                            Pulse = reader.GetIntNullable("Pulse"),
                            PulseLocation = reader.GetIntNullable("PulseLocation"),
                            Pulse2 = reader.GetIntNullable("Pulse2"),
                            PulseLocation2 = reader.GetIntNullable("PulseLocation2"),
                            BloodPressure = reader.GetStringNullable("BloodPressure"),
                            BloodPressure2 = reader.GetStringNullable("BloodPressure2"),
                            BloodPressure3 = reader.GetStringNullable("BloodPressure3"),
                            BloodPressure4 = reader.GetStringNullable("BloodPressure4"),
                            BloodPressure5 = reader.GetStringNullable("BloodPressure5"),
                            BloodPressure6 = reader.GetStringNullable("BloodPressure6"),
                            Weight = reader.GetDoubleNullable("Weight"),
                            Pain = reader.GetIntNullable("Pain")
                        })
                        .AsList();
            }
            return list;
        }

        public List<VitalSignLog> GetPatientVitalSignLogs(Guid patientId, Guid agencyId, List<Guid> tasks)
        {
            var script = string.Format(@"SELECT 
                            Id, 
                            EntityId,
                            VisitDate,
                            Pain,
                            Temp,
                            Weight,
                            WeightUnit,
                            TempUnit,
                            Resp,
                            Pulse,
                            PulseLocation,
                            Pulse2,
                            PulseLocation2,
                            BloodPressure,
                            BloodPressure2,
                            BloodPressure3,
                            BloodPressure4,
                            BloodPressure5,
                            BloodPressure6
                                FROM 
                                    vitalsignlogs
                                        WHERE 
                                            AgencyId = @agencyid AND 
                                            PatientId = @patientid AND
                                            EntityId in ({0}) AND
                                            IsDeprecated = 0", tasks.ToCommaSeperatedList());

            var list = new List<VitalSignLog>();
            using (var cmd = new FluentCommand<VitalSignLog>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .SetMap(reader => new VitalSignLog
                        {
                            Id = reader.GetGuid("Id"),
                            PatientId = patientId,
                            EntityId = reader.GetGuid("EntityId"),
                            VisitDate = reader.GetDateTimeWithMin("VisitDate"),
                            Temp = reader.GetDoubleNullable("Temp"),
                            TempUnit = reader.GetInt("TempUnit"),
                            Resp = reader.GetIntNullable("Resp"),
                            Pulse = reader.GetIntNullable("Pulse"),
                            Weight = reader.GetDoubleNullable("Weight"),
                            WeightUnit = reader.GetInt("WeightUnit"),
                            PulseLocation = reader.GetIntNullable("PulseLocation"),
                            Pulse2 = reader.GetIntNullable("Pulse2"),
                            PulseLocation2 = reader.GetIntNullable("PulseLocation2"),
                            BloodPressure = reader.GetStringNullable("BloodPressure"),
                            BloodPressure2 = reader.GetStringNullable("BloodPressure2"),
                            BloodPressure3 = reader.GetStringNullable("BloodPressure3"),
                            BloodPressure4 = reader.GetStringNullable("BloodPressure4"),
                            BloodPressure5 = reader.GetStringNullable("BloodPressure5"),
                            BloodPressure6 = reader.GetStringNullable("BloodPressure6"),
                            Pain = reader.GetIntNullable("Pain")
                        })
                        .AsList();
            }
            return list;
        }

        public List<VitalSignLog> GetPatientVitalSigns(Guid patientId, Guid agencyId)
        {
            return database.Find<VitalSignLog>(s => s.PatientId == patientId && s.AgencyId == agencyId).ToList();
        }

        /// <summary>
        /// Adds the log if it is new otherwise this will pull the existing vital signs and update them.
        /// </summary>
        /// <param name="log">The log containing the updated values</param>
        /// <param name="logToUpdate">Will out the existing vital signs log if one is found.</param>
        /// <returns></returns>
        public bool AddOrUpdate(VitalSignLog log, ref VitalSignLog logToUpdate)
        {
            if (log.IsNew)
            {
                return Add(log);
            }
            else
            {
                logToUpdate = log.Id.IsEmpty() || log.Id == log.EntityId ? GetEntitiesVitalSign(log.EntityId, log.PatientId, log.AgencyId) : Get(log.Id, log.EntityId, log.PatientId, log.AgencyId);
                return UpdateModal(logToUpdate, log);
            }
        }

        public bool RestoreData(VitalSignLog oldLog, VitalSignLog newLog)
        {
            if (oldLog.IsNull())
            {
                return RemoveModel(newLog.AgencyId, newLog.PatientId, newLog.Id);
            }
            return Update(oldLog);
        }

        public bool Update(VitalSignLog log)
        {
            log.Modified = DateTime.Now;
            return database.Update(log) > 0;
        }

        public bool UpdateModal(VitalSignLog logToUpdate, VitalSignLog log)
        {
            bool result = false;
            try
            {
                if (logToUpdate != null)
                {
                    logToUpdate.Weight = log.Weight;
                    logToUpdate.WeightUnit = log.WeightUnit;
                    logToUpdate.Temp = log.Temp;
                    logToUpdate.TempLocation = log.TempLocation;
                    logToUpdate.TempUnit = log.TempUnit;
                    logToUpdate.Resp = log.Resp;
                    logToUpdate.RespStatus = log.RespStatus;
                    logToUpdate.Pulse = log.Pulse;
                    logToUpdate.PulseLocation = log.PulseLocation;
                    logToUpdate.PulseStatus = log.PulseStatus;
                    logToUpdate.Pain = log.Pain;
                    logToUpdate.Pulse2 = log.Pulse2;
                    logToUpdate.PulseLocation2 = log.PulseLocation2;
                    logToUpdate.PulseStatus2 = log.PulseStatus2;
                    logToUpdate.OxygenSaturation = log.OxygenSaturation;
                    logToUpdate.OxygenSaturationType = log.OxygenSaturationType;
                    logToUpdate.BloodGlucose = log.BloodGlucose;
                    logToUpdate.BloodGlucoseType = log.BloodGlucoseType;
                    logToUpdate.BloodPressure = log.BloodPressure;
                    logToUpdate.BloodPressurePosition = log.BloodPressurePosition;
                    logToUpdate.BloodPressureSide = log.BloodPressureSide;
                    logToUpdate.BloodPressure2 = log.BloodPressure2;
                    logToUpdate.BloodPressurePosition2 = log.BloodPressurePosition2;
                    logToUpdate.BloodPressureSide2 = log.BloodPressureSide2;
                    logToUpdate.BloodPressure3 = log.BloodPressure3;
                    logToUpdate.BloodPressurePosition3 = log.BloodPressurePosition3;
                    logToUpdate.BloodPressureSide3 = log.BloodPressureSide3;
                    logToUpdate.BloodPressure4 = log.BloodPressure4;
                    logToUpdate.BloodPressurePosition4 = log.BloodPressurePosition4;
                    logToUpdate.BloodPressureSide4 = log.BloodPressureSide4;
                    logToUpdate.BloodPressure5 = log.BloodPressure5;
                    logToUpdate.BloodPressurePosition5 = log.BloodPressurePosition5;
                    logToUpdate.BloodPressureSide5 = log.BloodPressureSide5;
                    logToUpdate.BloodPressure6 = log.BloodPressure6;
                    logToUpdate.BloodPressurePosition6 = log.BloodPressurePosition6;
                    logToUpdate.BloodPressureSide6 = log.BloodPressureSide6;
                    logToUpdate.VisitDate = log.VisitDate;
                    logToUpdate.Comments = log.Comments;
                    logToUpdate.Modified = DateTime.Now;
                    result = database.Update(logToUpdate) > 0;
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool ToggleDeprecation(Guid agencyId, Guid patientId, Guid id, bool isDeprecated)
        {
            return EntityHelper.TogglePatientEntityDeprecation<VitalSignLog>(id, patientId, agencyId, isDeprecated, connectionStringName);
        }

        public bool RemoveModel(Guid agencyId, Guid patientId, Guid id)
        {
            return EntityHelper.RemovePatientEntity<VitalSignLog>(agencyId, patientId, id, connectionStringName);
        }
    }
}
