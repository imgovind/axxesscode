﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;

    using MongoDB.Bson;

    using SubSonic.DataProviders;
    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core;

    public abstract class PatientProfileAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;
        protected AgencyServices Service { get; set; }

        public bool Add(Profile profile)
        {
            bool result = false;
            try
            {
                if (profile != null)
                {
                    profile.Created = DateTime.Now;
                    database.Add<Profile>(profile);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Edit(Profile profile)
        {
            bool result = false;
            try
            {
                if (profile != null)
                {
                    var patientToEdit = database.Single<Profile>(p => p.Id == profile.Id && p.AgencyId == profile.AgencyId);
                    if (patientToEdit != null)
                    {
                        if (profile.PrimaryInsurance >= 1000)
                        {
                            patientToEdit.PrimaryHealthPlanId = profile.PrimaryHealthPlanId;
                            patientToEdit.PrimaryGroupName = profile.PrimaryGroupName;
                            patientToEdit.PrimaryGroupId = profile.PrimaryGroupId;
                            patientToEdit.PrimaryRelationship = profile.PrimaryRelationship;
                        }
                        else
                        {
                            patientToEdit.PrimaryHealthPlanId = patientToEdit.PrimaryGroupName = patientToEdit.PrimaryGroupId = string.Empty;
                        }

                        if (profile.SecondaryInsurance >= 1000)
                        {
                            patientToEdit.SecondaryHealthPlanId = profile.SecondaryHealthPlanId;
                            patientToEdit.SecondaryGroupName = profile.SecondaryGroupName;
                            patientToEdit.SecondaryGroupId = profile.SecondaryGroupId;
                            patientToEdit.SecondaryRelationship = profile.SecondaryRelationship;
                        }
                        else
                        {
                            patientToEdit.SecondaryHealthPlanId = patientToEdit.SecondaryGroupName = patientToEdit.SecondaryGroupId = string.Empty;
                        }

                        if (profile.TertiaryInsurance >= 1000)
                        {
                            patientToEdit.TertiaryHealthPlanId = profile.TertiaryHealthPlanId;
                            patientToEdit.TertiaryGroupName = profile.TertiaryGroupName;
                            patientToEdit.TertiaryGroupId = profile.TertiaryGroupId;
                            patientToEdit.TertiaryRelationship = profile.TertiaryRelationship;
                        }
                        else
                        {
                            patientToEdit.TertiaryHealthPlanId = patientToEdit.TertiaryGroupName = patientToEdit.TertiaryGroupId = string.Empty;
                        }

                        patientToEdit.AgencyLocationId = profile.AgencyLocationId;
                        patientToEdit.FirstName = profile.FirstName;
                        patientToEdit.LastName = profile.LastName;
                        patientToEdit.MiddleInitial = profile.MiddleInitial;

                        patientToEdit.StartofCareDate = profile.StartofCareDate;
                        if (profile.Status == (int)PatientStatus.Discharged)
                        {
                            patientToEdit.DischargeDate = profile.DischargeDate;
                            patientToEdit.DischargeReason = profile.DischargeReason;
                        }
                        patientToEdit.PrimaryInsurance = profile.PrimaryInsurance;
                        patientToEdit.SecondaryInsurance = profile.SecondaryInsurance;
                        patientToEdit.TertiaryInsurance = profile.TertiaryInsurance;


                        patientToEdit.Payer = profile.Payer;
                        patientToEdit.CaseManagerId = profile.CaseManagerId;
                        patientToEdit.AuditorId = profile.AuditorId;
                        patientToEdit.UserId = profile.UserId;
                        patientToEdit.AdmissionId = profile.AdmissionId;
                        patientToEdit.Modified = DateTime.Now;
                        return (database.Update<Profile>(patientToEdit) > 0);
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool Remove(Guid agencyId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientprofiles WHERE AgencyId = @agencyid AND  Id = @id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Admit(PendingPatient pending)
        {
            bool result = false;
            try
            {
                if (pending != null)
                {
                    var profile = database.Single<Profile>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId);
                    if (profile != null)
                    {
                        profile.AdmissionId = pending.AdmissionId;
                        profile.StartofCareDate = pending.StartofCareDate;
                        profile.CaseManagerId = pending.CaseManagerId;
                        profile.UserId = pending.UserId;
                        profile.AgencyLocationId = pending.AgencyLocationId;
                        profile.PrimaryInsurance = pending.PrimaryInsurance;
                        profile.SecondaryInsurance = pending.SecondaryInsurance;
                        profile.TertiaryInsurance = pending.TertiaryInsurance;
                        profile.Payer = pending.Payer;
                        profile.Status = (int)PatientStatus.Active;
                        profile.Modified = DateTime.Now;

                        return (database.Update<Profile>(profile) > 0);
                    }
                }

            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AdmitReferral(PendingPatient pending, Referral referral)
        {
            bool result = false;
            try
            {
                if (pending != null)
                {
                    var profile = new Profile
                    {
                        AdmissionId = pending.AdmissionId,
                        AgencyLocationId = pending.AgencyLocationId,
                        UserId = pending.UserId,
                        CaseManagerId = pending.CaseManagerId,
                        PrimaryInsurance = pending.PrimaryInsurance,
                        SecondaryInsurance = pending.SecondaryInsurance,
                        TertiaryInsurance = pending.TertiaryInsurance,
                        Payer = pending.Payer,
                        Status = (int)PatientStatus.Active,
                        StartofCareDate = pending.StartofCareDate,
                        Id = referral.Id,
                        AgencyId = referral.AgencyId,
                        FirstName = referral.FirstName,
                        MiddleInitial = referral.MiddleInitial,
                        LastName = referral.LastName,
                        Modified = DateTime.Now
                    };
                    database.Add<Profile>(profile);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            bool result = false;
            try
            {
                if (pending != null)
                {
                    var profile = database.Single<Profile>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId);
                    if (profile != null)
                    {
                        profile.NonAdmissionDate = pending.NonAdmitDate;
                        profile.NonAdmissionReason = pending.Reason;
                        profile.Status = (int)PatientStatus.NonAdmission;
                        profile.AdmissionId = pending.AdmissionId;
                        profile.Modified = DateTime.Now;
                        return database.Update<Profile>(profile) > 0;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DischargePatient(Guid agencyId, Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        {
            var result = false;
            try
            {
                var profile = database.Single<Profile>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (profile != null)
                {
                    profile.DischargeDate = dischargeDate;
                    profile.Status = (int)PatientStatus.Discharged;
                    profile.DischargeReasonId = dischargeReasonId;
                    profile.DischargeReason = dischargeReason;
                    profile.Modified = DateTime.Now;
                    return database.Update<Profile>(profile) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateModal(Profile profile)
        {
            bool result = false;
            try
            {
                if (profile != null)
                {
                    profile.Modified = DateTime.Now;
                    if (database.Update<Profile>(profile) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate)
        {
            var script = @"UPDATE patientprofiles pr set pr.Status = @status, pr.StartofCareDate = @startofcaredate, pr.Modified = @modified WHERE pr.AgencyId = @agencyid AND pr.Id = @id;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddInt("status", (int)PatientStatus.Active)
                     .AddDateTime("startofcaredate", startOfCareDate)
                     .AddDateTime("modified", DateTime.Now)
                     .AsNonQuery();
            }
            return count > 0;
        }

        public Profile GetProfileOnly(Guid agencyId, Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Single<Profile>(p => p.AgencyId == agencyId && p.Id == Id);
        }

        public bool SetStatus(Guid agencyId, Guid patientId, int status)
        {
            var script = @"UPDATE patientprofiles pr set pr.OldStatus = Status, pr.Status = @status,  pr.Modified = @modified WHERE pr.AgencyId = @agencyid AND pr.Id = @id;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddInt("status", status)
                     .AddDateTime("modified", DateTime.Now)
                     .AsNonQuery();
            }
            return count > 0;
        }

        //        public int GetPatientStatusCount(Guid agencyId, int statusId)
        //        {
        //            var script = @"SELECT
        //                            count(*) 
        //                                FROM 
        //                                    patients
        //                                        WHERE
        //                                            patients.AgencyId = @agencyid AND
        //                                            patients.Status = @statusid AND
        //                                            patients.IsDeprecated = 0";
        //            return new FluentCommand<int>(script)
        //                .SetConnection(connectionStringName)
        //                .AddGuid("agencyid", agencyId)
        //                .AddInt("statusid", statusId)
        //                .AsScalar();
        //        }

        //TODO: FIX Later

        public Guid GetPatientAssignedUserId(Guid agencyId, Guid patientId)
        {
            Guid userId = Guid.Empty;
            var script = string.Format(@"SELECT
                                          pr.UserId
                                            FROM 
                                              patientprofiles pr
                                                WHERE 
                                                    pr.AgencyId = @agencyId AND
                                                    pr.Id = @patientId");
            using (var cmd = new FluentCommand<Guid>(script))
            {
                userId = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => reader.GetGuid("UserId"))
                    .AsSingle();
            }
            return userId;
        }



        public Profile GetProfileVeryLean(Guid agencyId, Guid patientId)
        {
            Profile profile = null;
            var script = @"SELECT 
                                pr.FirstName, 
                                pr.LastName,
                                pr.MiddleInitial,
                                pr.IsDeprecated
                                            FROM 
                                              patientprofiles pr
                                                WHERE 
                                                    pr.AgencyId = @agencyId AND
                                                    pr.Id = @patientId  LIMIT  1";
            using (var cmd = new FluentCommand<Profile>(script))
            {
                profile = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => new Profile()
                    {
                        Id = patientId,
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated")
                    })
                    .AsSingle();
            }
            return profile;
        }

        public List<Patient> GetPatientLeanByUserId(Guid agencyId, Guid userId, bool includeNames)
        {
            string nameSelectScript = string.Empty;
            if (includeNames)
            {
                nameSelectScript = "pr.FirstName, pr.LastName, pr.MiddleInitial, ";
            }
            var list = new List<Patient>();
            var script = string.Format(@"SELECT {0}  pr.Id
                                            FROM 
                                              patientprofiles pr
                                                WHERE 
                                                    pr.AgencyId = @agencyId AND
                                                    pr.UserId = @userId", nameSelectScript);
            using (var cmd = new FluentCommand<Patient>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("userId", userId)
                    .SetMap(reader => new Patient()
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial")
                    })
                    .AsList();
            }
            return list;
        }

        public Patient GetPatientWithProfile(Guid agencyId, Guid Id)
        {
            var patient = new Patient();
            var query = string.Format(@"SELECT
                                    p.`Id`,
                                    p.`PatientIdNumber`,
                                    p.`FirstName`,
                                    p.`LastName`,
                                    p.`MiddleInitial`,
                                    p.`MedicareNumber`, 
                                    p.`MedicaidNumber`, 
                                    p.`SSN`, 
                                    p.`DOB`,
                                    p.`Gender`,
                                    p.`Height`,
                                    p.`HeightMetric`, 
                                    p.`Weight`,
                                    p.`WeightMetric`, 
                                    p.`Ethnicities`, 
                                    p.`MaritalStatus`, 
                                    p.`AddressLine1`,
                                    p.`AddressLine2`, 
                                    p.`AddressCity`, 
                                    p.`AddressStateCode`,
                                    p.`AddressZipCode`, 
                                    p.`PhoneHome`, 
                                    p.`PhoneMobile`,
                                    p.`EmailAddress`, 
                                    p.`PharmacyName`, 
                                    p.`PharmacyPhone`, 
                                    p.`DME`, 
                                    p.`OtherDME`,
                                    p.`Payer`,
                                    p.`ServicesRequired`,
                                    p.`Triage`, 
                                    p.`EvacuationZone`, 
                                    p.`PhotoId`, 
                                    p.`IsDNR`, 
                                    p.`ReferrerPhysician`,
                                   
                                    p.`IsHospitalized`,
                                    p.`HospitalizationId`,
                                    p.`Comments`, 
                                    p.`Services`,
                                    p.`{0}Status` as Status,

                                    pr.`AgencyLocationId`,
                                    pr.`UserId`,
                                    pr.`AdmissionId`, 
                                    pr.`CaseManagerId`, 
                                    pr.`AuditorId`, 
                                    pr.`StartofCareDate`,
                                    pr.`PaymentSource`,
                                    pr.`OtherPaymentSource`, 
                                    pr.`PrimaryInsurance`,
                                    pr.`PrimaryHealthPlanId`,
                                    pr.`PrimaryGroupName`, 
                                    pr.`PrimaryGroupId`, 
                                    pr.`PrimaryRelationship`, 
                                    pr.`SecondaryInsurance`, 
                                    pr.`SecondaryHealthPlanId`,
                                    pr.`SecondaryGroupName`, 
                                    pr.`SecondaryGroupId`, 
                                    pr.`SecondaryRelationship`, 
                                    pr.`TertiaryInsurance`,
                                    pr.`TertiaryHealthPlanId`,
                                    pr.`TertiaryGroupName`, 
                                    pr.`TertiaryGroupId`, 
                                    pr.`TertiaryRelationship`,
                                    pr.`NonAdmissionDate`,
                                    pr.`NonAdmissionReason`, 
                                    pr.`AdmissionSource`,
                                    pr.`ReferralDate`, 
                                    pr.`OtherReferralSource`,
                                    pr.`InternalReferral`, 
                                    pr.`DischargeDate`, 
                                    pr.`DischargeReasonId`, 
                                    pr.`DischargeReason`, 
                                    pr.`IsDeprecated`,
                                    pr.`Created`, 
                                    pr.`Modified`
                                        FROM 
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId= @agencyId AND
                                                    p.Id= @Id ", this.Service.ToString());
            using (var cmd = new FluentCommand<Patient>(query))
            {
                patient = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("Id", Id)
                    .SetMap(reader => new Patient
                    {
                        Id = reader.GetGuid("Id"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                        MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                        SSN = reader.GetStringNullable("SSN"),
                        DOB = reader.GetDateTime("DOB"),
                        Gender = reader.GetStringNullable("Gender"),
                        Height = (float)reader.GetDouble("Height"),
                        HeightMetric = reader.GetInt("HeightMetric"),
                        Weight = (float)reader.GetDouble("Weight"),
                        WeightMetric = reader.GetInt("WeightMetric"),
                        Ethnicities = reader.GetStringNullable("Ethnicities"),
                        MaritalStatus = reader.GetStringNullable("MaritalStatus"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        PharmacyName = reader.GetStringNullable("PharmacyName"),
                        PharmacyPhone = reader.GetStringNullable("PharmacyPhone"),
                        DME = reader.GetStringNullable("DME"),
                        OtherDME = reader.GetStringNullable("OtherDME"),
                        ServicesRequired = reader.GetStringNullable("ServicesRequired"),

                        Triage = reader.GetInt("Triage"),
                        EvacuationZone = reader.GetStringNullable("EvacuationZone"),
                        PhotoId = reader.GetGuid("PhotoId"),
                        IsDNR = reader.GetBoolean("IsDNR"),
                        ReferrerPhysician = reader.GetGuid("ReferrerPhysician"),

                        IsHospitalized = reader.GetBoolean("IsHospitalized"),
                        HospitalizationId = reader.GetGuid("HospitalizationId"),
                        Comments = reader.GetStringNullable("Comments"),
                        //Service=
                        Profile = new Profile
                        {
                            AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                            UserId = reader.GetGuid("UserId"),
                            AdmissionId = reader.GetGuid("AdmissionId"),
                            CaseManagerId = reader.GetGuid("CaseManagerId"),
                            AuditorId = reader.GetGuid("AuditorId"),
                            StartofCareDate = reader.GetDateTime("StartofCareDate"),
                            Status = reader.GetInt("Status"),
                            PaymentSource = reader.GetStringNullable("PaymentSource"),
                            OtherPaymentSource = reader.GetStringNullable("OtherPaymentSource"),
                            PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                            PrimaryHealthPlanId = reader.GetStringNullable("PrimaryHealthPlanId"),
                            PrimaryGroupName = reader.GetStringNullable("PrimaryGroupName"),
                            PrimaryGroupId = reader.GetStringNullable("PrimaryGroupId"),
                            PrimaryRelationship = reader.GetStringNullable("PrimaryRelationship"),
                            SecondaryInsurance = reader.GetInt("SecondaryInsurance"),
                            SecondaryHealthPlanId = reader.GetStringNullable("SecondaryHealthPlanId"),
                            SecondaryGroupName = reader.GetStringNullable("SecondaryGroupName"),
                            SecondaryGroupId = reader.GetStringNullable("SecondaryGroupId"),
                            SecondaryRelationship = reader.GetStringNullable("SecondaryRelationship"),
                            TertiaryInsurance = reader.GetInt("TertiaryInsurance"),
                            TertiaryHealthPlanId = reader.GetStringNullable("TertiaryHealthPlanId"),
                            TertiaryGroupName = reader.GetStringNullable("TertiaryGroupName"),
                            TertiaryGroupId = reader.GetStringNullable("TertiaryGroupId"),
                            TertiaryRelationship = reader.GetStringNullable("TertiaryRelationship"),
                            NonAdmissionDate = reader.GetDateTime("NonAdmissionDate"),
                            NonAdmissionReason = reader.GetStringNullable("NonAdmissionReason"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                            ReferralDate = reader.GetDateTime("ReferralDate"),
                            OtherReferralSource = reader.GetStringNullable("OtherReferralSource"),
                            InternalReferral = reader.GetGuid("InternalReferral"),
                            DischargeDate = reader.GetDateTime("DischargeDate"),
                            DischargeReasonId = reader.GetInt("DischargeReasonId"),
                            DischargeReason = reader.GetStringNullable("DischargeReason"),
                            IsDeprecated = reader.GetBoolean("IsDeprecated"),
                            Created = reader.GetDateTime("Created"),
                            Modified = reader.GetDateTime("Modified")

                        }
                    }).AsSingle();
            }
            return patient;
        }

        public Patient GetPatientWithProfileForClaim(Guid agencyId, Guid Id)
        {
            var patient = new Patient();
            const string query = @"SELECT
                            p.`Id`,
                            p.`PatientIdNumber`,
                            p.`FirstName`,
                            p.`LastName`,
                            p.`MiddleInitial`,
                            p.`MedicareNumber`, 
                            p.`MedicaidNumber`, 
                            p.`DOB`,
                            p.`AddressLine1`,
                            p.`AddressLine2`, 
                            p.`AddressCity`, 
                            p.`AddressStateCode`,
                            p.`AddressZipCode`, 
                            p.`Gender`,
                           
                            pr.`Status`,
                            pr.`AgencyLocationId`,
                            pr.`StartofCareDate`, 
                            pr.`PrimaryInsurance`,
                            pr.`PrimaryHealthPlanId`,
                            pr.`PrimaryGroupName`, 
                            pr.`PrimaryGroupId`, 
                            pr.`PrimaryRelationship`, 
                            pr.`SecondaryInsurance`, 
                            pr.`SecondaryHealthPlanId`,
                            pr.`SecondaryGroupName`, 
                            pr.`SecondaryGroupId`, 
                            pr.`SecondaryRelationship`, 
                            pr.`TertiaryInsurance`,
                            pr.`TertiaryHealthPlanId`,
                            pr.`TertiaryGroupName`, 
                            pr.`TertiaryGroupId`, 
                            pr.`TertiaryRelationship`,
                            pr.`AdmissionSource`
                                FROM 
                                    agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                        Where 
                                            p.AgencyId= @agencyId AND
                                            p.Id= @Id";
            using (var cmd = new FluentCommand<Patient>(query))
            {
                patient = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("Id", Id)
                    .SetMap(reader => new Patient
                    {
                        Id = reader.GetGuid("Id"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                        MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                        DOB = reader.GetDateTime("DOB"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        Gender = reader.GetString("Gender"),

                        //Service=
                        Profile = new Profile
                        {
                            AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                            StartofCareDate = reader.GetDateTime("StartofCareDate"),
                            Status = reader.GetInt("Status"),
                            PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                            PrimaryHealthPlanId = reader.GetStringNullable("PrimaryHealthPlanId"),
                            PrimaryGroupName = reader.GetStringNullable("PrimaryGroupName"),
                            PrimaryGroupId = reader.GetStringNullable("PrimaryGroupId"),
                            PrimaryRelationship = reader.GetStringNullable("PrimaryRelationship"),
                            SecondaryInsurance = reader.GetInt("SecondaryInsurance"),
                            SecondaryHealthPlanId = reader.GetStringNullable("SecondaryHealthPlanId"),
                            SecondaryGroupName = reader.GetStringNullable("SecondaryGroupName"),
                            SecondaryGroupId = reader.GetStringNullable("SecondaryGroupId"),
                            SecondaryRelationship = reader.GetStringNullable("SecondaryRelationship"),
                            TertiaryInsurance = reader.GetInt("TertiaryInsurance"),
                            TertiaryHealthPlanId = reader.GetStringNullable("TertiaryHealthPlanId"),
                            TertiaryGroupName = reader.GetStringNullable("TertiaryGroupName"),
                            TertiaryGroupId = reader.GetStringNullable("TertiaryGroupId"),
                            TertiaryRelationship = reader.GetStringNullable("TertiaryRelationship"),
                            AdmissionSource = reader.GetStringNullable("AdmissionSource")
                        }
                    }).AsSingle();
            }
            return patient;
        }

        public Patient GetPatientWithProfileForClaimLean(Guid agencyId, Guid Id)
        {
            var patient = new Patient();
            var query = string.Format(@"SELECT
                                     p.`Id`,
                                     p.`PatientIdNumber`,
                                     p.`FirstName`,
                                     p.`LastName`,
                                     p.`MiddleInitial`,
                                     p.`MedicareNumber`, 
                                     p.`MedicaidNumber`, 
                                     p.`MaritalStatus`, 
                                     p.`PhoneHome`, 
                                     p.`PhoneMobile`,
                                     p.`{0}Status` as Status,
                                   
                                     pr.`AgencyLocationId`
                                    
                                        FROM 
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId= @agencyId AND
                                                    p.Id= @Id  limit 0,1 ", this.Service.ToString());
            using (var cmd = new FluentCommand<Patient>(query))
            {
                patient = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("Id", Id)
                    .SetMap(reader => new Patient
                    {
                        Id = reader.GetGuid("Id"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                        MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                        MaritalStatus = reader.GetStringNullable("MaritalStatus"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile"),

                        //Service=
                        Profile = new Profile
                        {
                            AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                            Status = reader.GetInt("Status")

                        }
                    }).AsSingle();
            }
            return patient;
        }

        public List<Patient> GetPatientsWithProfile(Guid agencyId, int status)
        {
            var patients = new List<Patient>();
            var query = string.Format(@"SELECT
                                    p.`Id`,
                                    p.`PatientIdNumber`,
                                    p.`FirstName`,
                                    p.`LastName`,
                                    p.`MiddleInitial`,
                                    p.`MedicareNumber`, 
                                    p.`DOB`,
                                    p.`Gender`,
                                    p.`AddressLine1`,
                                    p.`AddressLine2`, 
                                    p.`AddressCity`, 
                                    p.`AddressStateCode`,
                                    p.`AddressZipCode`, 
                                    p.`PhoneHome`, 
                                    p.`Comments`, 
                                    p.`{0}Status` as Status,

                                    pr.`StartofCareDate`, 
                                    pr.`PrimaryInsurance`,
                                    pr.`PrimaryHealthPlanId`,
                                    pr.`PrimaryGroupName`, 
                                    pr.`PrimaryGroupId`, 
                                    pr.`PrimaryRelationship`, 
                                    pr.`NonAdmissionDate`,
                                    pr.`NonAdmissionReason`
                                        FROM 
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId= @agencyId AND
                                                    p.{0}Status = @status", this.Service.ToString());
            using (var cmd = new FluentCommand<Patient>(query))
            {
                patients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddInt("status", status)
                    .SetMap(reader => new Patient
                    {
                        Id = reader.GetGuid("Id"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                        DOB = reader.GetDateTime("DOB"),
                        Gender = reader.GetStringNullable("Gender"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        Comments = reader.GetStringNullable("Comments"),
                        Profile = new Profile
                        {
                            StartofCareDate = reader.GetDateTime("StartofCareDate"),
                            PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                            PrimaryHealthPlanId = reader.GetStringNullable("PrimaryHealthPlanId"),
                            PrimaryGroupName = reader.GetStringNullable("PrimaryGroupName"),
                            PrimaryGroupId = reader.GetStringNullable("PrimaryGroupId"),
                            PrimaryRelationship = reader.GetStringNullable("PrimaryRelationship"),
                            NonAdmissionDate = reader.GetDateTime("NonAdmissionDate"),
                            NonAdmissionReason = reader.GetStringNullable("NonAdmissionReason")

                        }
                    }).AsList();
            }
            return patients;
        }

        public IList<Patient> Find(Guid agencyId, Guid branchId, int statusId)
        {
            var patients = new List<Patient>();
            var query = string.Format(@"SELECT 
                                pr.Id,
                                pr.FirstName,
                                pr.LastName, 
                                pr.MiddleInitial,
                                pr.Status
                                    FROM
                                        patientprofiles pr
                                            Where 
                                                pr.AgencyId= @agencyId {0} {1} AND
                                                pr.IsDeprecated = 0 ",
                                                                     branchId.IsEmpty() ? "" : " AND pr.AgencyLocationId = @branchId ",
                                                                     statusId > 0 ? " AND pr.Status = @statusId " : " AND pr.Status IN (1,2) ");
            using (var cmd = new FluentCommand<Patient>(query))
            {
                patients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusId", statusId)
                    .SetMap(reader =>
                    {
                        Patient p = new Patient()
                        {
                            Id = reader.GetGuid("Id"),
                            FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        };
                        if (this.Service == AgencyServices.HomeHealth)
                        {
                            p.HomeHealthStatus = reader.GetInt("Status");
                        }
                        else if (this.Service == AgencyServices.PrivateDuty)
                        {
                            p.PrivateDutyStatus = reader.GetInt("Status");
                        }
                        return p;
                    }).AsList();
            }
            return patients;
        }

        public List<PatientData> All(Guid agencyId, Guid branchId, int status)
        {
            var query = string.Format(@"SELECT
                                    p.Id, 
                                    p.PatientIdNumber,
                                    pr.PrimaryHealthPlanId, 
                                    pr.MedicareNumber, 
                                    pr.PrimaryInsurance, 
                                    p.FirstName, 
                                    p.LastName, 
                                    p.MiddleInitial,
                                    p.DOB, 
                                    p.Gender,
                                    pr.Status, 
                                    p.AddressLine1,
                                    p.AddressLine2,
                                    p.AddressCity,
                                    p.AddressStateCode,
                                    p.AddressZipCode,
                                    p.PhoneHome, 
                                    p.PhoneMobile , 
                                    p.EmailAddress,
                                    p.IsDNR 
                                        FROM 
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId = @agencyid AND
                                                    pr.IsDeprecated = 0 {0} {1} {2} ",
                                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty,
                                                            status == 0 ? " " : string.Format(" AND pr.Status = @status ", this.Service.ToString(), status),
                                                            " AND p.Services & @serviceId = @serviceId ", this.Service.ToString());
            var list = new List<PatientData>();
            using (var cmd = new FluentCommand<PatientData>(query))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("serviceId", (int)this.Service)
                .AddInt("status", status)
                .SetMap(reader => new PatientData
                {
                    Id = reader.GetGuid("Id"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR"),
                    ServicePrefix = this.Service.ToArea()
                })
                .AsList();
            }
            return list;
        }

        public List<PendingPatient> GetPendingByAgencyId(Guid agencyId, Guid branchId)
        {
            var query = string.Format(@"SELECT
                                    p.Id,
                                    pr.Created,
                                    p.FirstName,
                                    p.LastName,
                                    p.PatientIdNumber,
                                    pr.PrimaryInsurance,
                                    pr.AgencyLocationId
                                        FROM
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId = @agencyid AND
                                                    pr.Status = @statusId {1} {2} ORDER BY p.LastName ASC, p.FirstName ASC ",
                                                                                                       this.Service.ToString(),
                                                                  !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty,
                                                                  " AND ((p.Services & @serviceId) = @serviceId) ");

            var list = new List<PendingPatient>();
            using (var cmd = new FluentCommand<PendingPatient>(query))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusId", (int)PatientStatus.Pending)
                .AddInt("serviceId", (int)this.Service)
                .SetMap(reader => new PendingPatient
                {
                    Id = reader.GetGuid("Id"),
                    Created = reader.GetDateTime("Created"),
                    DisplayName = string.Concat(reader.GetStringNullable("LastName"), ", ", reader.GetStringNullable("FirstName")),
                    Type = NonAdmitTypes.Patient,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    Service = this.Service.ToString(),
                    ServicePrefix = this.Service.ToArea()
                })
                .AsList();
            }
            return list;

        }

        public IList<PatientData> AllDeleted(Guid agencyId, Guid branchId)
        {
            var query = string.Format(@"SELECT
                            p.Id,
                            p.PatientIdNumber ,
                            p.FirstName,
                            p.LastName, 
                            p.MiddleInitial, 
                            p.DOB,
                            p.Gender,
                            pr.OldStatus, 
                            p.AddressLine1,
                            p.AddressLine2,
                            p.AddressCity,
                            p.AddressStateCode,
                            p.AddressZipCode,
                            p.PhoneHome, 
                            p.PhoneMobile,
                            pr.PrimaryHealthPlanId,
                            p.MedicareNumber, 
                            pr.PrimaryInsurance,
                            p.EmailAddress,
                            p.IsDNR 
                                FROM
                                    agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                        Where 
                                            p.AgencyId = @agencyid AND
                                            p.{0}Status = @statusId AND
                                            pr.IsDeprecated = 1 {1} {2} ",
                                                                         this.Service.ToString(),
                                                                           !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty,
                                                                           " AND p.Services & @serviceId = @serviceId ");
            var list = new List<PatientData>();
            using (var cmd = new FluentCommand<PatientData>(query))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("serviceId", (int)this.Service)
                 .AddInt("statusId", (int)PatientStatus.Deprecated)
                .SetMap(reader => new PatientData
                {
                    Id = reader.GetGuid("Id"),
                    StatusId = reader.GetInt("OldStatus"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsList();
            }
            return list;
        }

        public PatientProfileLean GetPatientPrintProfile(Guid agencyId, Guid patientId)
        {
            var query = string.Format(@"SELECT
                                            p.PatientIdNumber,
                                            p.FirstName,
                                            p.LastName,
                                            p.MiddleInitial,
                                            p.DOB,
                                            pr.StartofCareDate,
                                            p.MedicareNumber, 
                                            p.MedicaidNumber, 
                                            p.Gender, 
                                            pr.AgencyLocationId,
                                            p.IsDNR
                                                FROM
                                                    agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                        Where 
                                                            p.AgencyId = @agencyid AND
                                                            p.Id = @patientid limit 0,1");

            var patientPrintProfile = new PatientProfileLean();
            using (var cmd = new FluentCommand<PatientProfileLean>(query))
            {
                patientPrintProfile = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientProfileLean
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    DOB = reader.GetDateTime("DOB"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    Gender = reader.GetStringNullable("Gender"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsSingle();
            }
            return patientPrintProfile;

        }

        public PatientProfileLean GetPatientPrintProfileWithPharmacy(Guid agencyId, Guid patientId)
        {
            var query = string.Format(@"SELECT
                                    p.PatientIdNumber,
                                    p.FirstName,
                                    p.LastName,
                                    p.MiddleInitial,
                                    p.DOB,
                                    pr.`StartofCareDate`,
                                    p.`MedicareNumber`, 
                                    p.`MedicaidNumber`, 
                                    p.`PharmacyName`, 
                                    p.`PharmacyPhone`, 
                                    p.`Gender`, 
                                    pr.AgencyLocationId
                                        FROM
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId = @agencyid AND
                                                    p.Id = @patientid limit 0,1", this.Service.ToString());

            var patientPrintProfile = new PatientProfileLean();
            using (var cmd = new FluentCommand<PatientProfileLean>(query))
            {
                patientPrintProfile = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PatientProfileLean
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    DOB = reader.GetDateTime("DOB"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    Gender = reader.GetStringNullable("Gender"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    PharmacyName = reader.GetStringNullable("PharmacyName"),
                    PharmacyPhone = reader.GetStringNullable("PharmacyPhone")
                })
                .AsSingle();
            }
            return patientPrintProfile;

        }

        public PatientProfileLean GetPatientPrintProfileWithAddress(Guid agencyId, Guid patientId)
        {
            var query = string.Format(@"SELECT
                                    p.`PatientIdNumber`,
                                    p.`FirstName`,
                                    p.`LastName`,
                                    p.`MiddleInitial`,
                                    p.`AddressLine1`,
                                    p.`AddressLine2`, 
                                    p.`AddressCity`, 
                                    p.`AddressStateCode`,
                                    p.`AddressZipCode`, 
                                    p.`DOB`,
                                    p.`Gender`,
                                    pr.`StartofCareDate`,
                                    p.`MedicareNumber`, 
                                    pr.`PrimaryHealthPlanId`,
                                    p.`PhoneHome`,
                                    pr.`AgencyLocationId`
                                        FROM
                                            agencymanagement.patients p INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                Where 
                                                    p.AgencyId = @agencyid AND
                                                    p.Id = @patientid limit 0,1");

            var patientPrintProfile = new PatientProfileLean();
            using (var cmd = new FluentCommand<PatientProfileLean>(query))
            {
                patientPrintProfile = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientProfileLean
                {
                    AgencyId = agencyId,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    DOB = reader.GetDateTime("DOB"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    Gender = reader.GetStringNullable("Gender"),
                    PrimaryHealthPlanId = reader.GetStringNullable("PrimaryHealthPlanId"),
                    Address = new Address
                    {
                        Line1 = reader.GetStringNullable("AddressLine1"),
                        Line2 = reader.GetStringNullable("AddressLine2"),
                        City = reader.GetStringNullable("AddressCity"),
                        StateCode = reader.GetStringNullable("AddressStateCode"),
                        ZipCode = reader.GetStringNullable("AddressZipCode")
                    }
                })
                .AsSingle();
            }
            return patientPrintProfile;
        }

        public bool UpdatePatientForAdmissionDateId(Guid agencyId, Guid patientId, Guid admissionId)
        {
            var script = @"UPDATE patientprofiles set AdmissionId = @admissionid WHERE AgencyId = @agencyid AND Id = @id;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddGuid("admissionid", admissionId)
                     .AsNonQuery();
            }
            return count > 0;
        }


        public List<PatientSelection> GetPatientSelectionPaymentSource(Guid agencyId, List<Guid> branchIds, int statusId, int paymentSourceId, string name)
        {
            return GetPatientSelectionHelper(agencyId, branchIds, statusId, paymentSourceId, name, "PaymentSource", false, Guid.Empty);
            //            var query = new QueryBuilder(@"SELECT 
            //                                        p.Id, 
            //                                        p.PatientIdNumber,
            //                                        p.FirstName,
            //                                        p.LastName,
            //                                        p.MiddleInitial
            //                                            FROM agencymanagement.patients p 
            //                                                 INNER JOIN 
            //                                                      patientprofiles pr ON p.Id = pr.Id");
            //            query
            //                .Where("pr.AgencyId = @agencyid")
            //                .And(string.Format("p.{0}Status = @statusid", this.Service.ToString()));

            //            if (!branchId.IsEmpty())
            //            {
            //                query.And("pr.AgencyLocationId = @branchId ");
            //            }

            //            if (name.IsNotNullOrEmpty())
            //            {
            //                query.And(string.Format("(pr.FirstName like '%{0}%' OR pr.LastName like '%{0}%')", name));
            //            }
            //            if (paymentSourceId > 0)
            //            {
            //                query.And(string.Format("pr.PaymentSource like '%{0};%'", paymentSourceId));
            //            }
            //            query.And("(p.Services & @serviceid) = @serviceid");
            //            query.And("pr.IsDeprecated = 0").OrderBy("p.LastName", true);
            //            var list = new List<PatientSelection>();
            //            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            //            {
            //                list = cmd.SetConnection(connectionStringName)
            //                    .AddGuid("agencyid", agencyId)
            //                    .AddInt("statusid", statusId)
            //                    .AddInt("paymentsourceid", paymentSourceId)
            //                    .AddGuid("branchId", branchId)
            //                    .AddInt("serviceid", (int)this.Service)
            //                    .SetMap(reader => new PatientSelection
            //                    {
            //                        Id = reader.GetGuid("Id"),
            //                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
            //                        FirstName = reader.GetStringNullable("FirstName").Trim().ToUpper(),
            //                        LastName = reader.GetStringNullable("LastName").Trim().ToUpper(),
            //                        MI = reader.GetStringNullable("MiddleInitial").ToUpper()
            //                    })
            //                    .AsList();
            //            }
            //            return list;
        }

        public List<PatientSelection> GetPatientSelectionInsurance(Guid agencyId, List<Guid> branchIds, int statusId, int insuranceId, string name)
        {
            return GetPatientSelectionHelper(agencyId, branchIds, statusId, insuranceId, name, "Insurance", false, Guid.Empty);
            //            var query = new QueryBuilder(@"SELECT 
            //                                        p.Id, 
            //                                        p.PatientIdNumber,
            //                                        p.FirstName,
            //                                        p.LastName,
            //                                        p.MiddleInitial
            //                                            FROM agencymanagement.patients p 
            //                                                 INNER JOIN 
            //                                                      patientprofiles pr ON p.Id = pr.Id");
            //            query
            //                .Where("pr.AgencyId = @agencyid")
            //                .And(string.Format("p.{0}Status = @statusid", this.Service.ToString()));

            //            if (!branchId.IsEmpty())
            //            {
            //                query.And("pr.AgencyLocationId = @branchId ");
            //            }

            //            if (name.IsNotNullOrEmpty())
            //            {
            //                query.And(string.Format("(pr.FirstName like '%{0}%' OR pr.LastName like '%{0}%')", name));
            //            }
            //            if (paymentSourceId > 0)
            //            {
            //                query.And(string.Format("pr.PaymentSource like '%{0};%'", paymentSourceId));
            //            }
            //            query.And("(p.Services & @serviceid) = @serviceid");
            //            query.And("pr.IsDeprecated = 0").OrderBy("p.LastName", true);
            //            var list = new List<PatientSelection>();
            //            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            //            {
            //                list = cmd.SetConnection(connectionStringName)
            //                    .AddGuid("agencyid", agencyId)
            //                    .AddInt("statusid", statusId)
            //                    .AddInt("paymentsourceid", paymentSourceId)
            //                    .AddGuid("branchId", branchId)
            //                    .AddInt("serviceid", (int)this.Service)
            //                    .SetMap(reader => new PatientSelection
            //                    {
            //                        Id = reader.GetGuid("Id"),
            //                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
            //                        FirstName = reader.GetStringNullable("FirstName").Trim().ToUpper(),
            //                        LastName = reader.GetStringNullable("LastName").Trim().ToUpper(),
            //                        MI = reader.GetStringNullable("MiddleInitial").ToUpper()
            //                    })
            //                    .AsList();
            //            }
            //            return list;
        }

        public List<PatientSelection> GetPatientSelectionHelper(Guid agencyId, List<Guid> branchIds, int statusId, int payorValue, string name, string typeOfPayor, bool isAuditor, Guid auditorId)
        {
            var query = new QueryBuilder(@"SELECT 
                                        p.Id, 
                                        p.PatientIdNumber,
                                        p.FirstName,
                                        p.LastName,
                                        p.MiddleInitial
                                            FROM 
                                                agencymanagement.patients p 
                                                     INNER JOIN 
                                                         patientprofiles pr ON p.Id = pr.Id");
            query
                .Where("pr.AgencyId = @agencyid")
                .And(string.Format("p.{0}Status = @statusid", this.Service.ToString()));

            //if (!branchId.IsEmpty())
            //{
            //    query.And("pr.AgencyLocationId == @branchId ");
            //}
            if (branchIds.Count == 1)
            {
                query.And(string.Format("pr.AgencyLocationId = '{0}' ", branchIds[0]));
            }
            else
            {
                query.And(string.Format("pr.AgencyLocationId IN ({0}) ", branchIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ")));
            }

            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(pr.FirstName like '%{0}%' OR pr.LastName like '%{0}%')", name));
            }
            if (payorValue > 0)
            {
                if (typeOfPayor.IsEqual("Insurance"))
                {
                    query.And(" ( pr.PrimaryInsurance = @payorvalue || pr.SecondaryInsurance = @payorvalue || pr.TertiaryInsurance = @payorvalue ) ");
                }
                else
                {
                    query.And("pr.PaymentSource like '%@payorvalue;%'");
                }
            }
            if (isAuditor)
            {
                if (!auditorId.IsEmpty())
                {
                    query.And(string.Format("pr.AuditorId = '{0}'", auditorId));
                }
            }
            query.And("(p.Services & @serviceid) = @serviceid");
            query.And("pr.IsDeprecated = 0")
                .OrderBy("p.LastName", true);
            var list = new List<PatientSelection>();
            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            {
                list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddInt("statusid", statusId)
                    .AddInt("payorvalue", payorValue)
                    //.AddGuid("branchId", branchId)
                    .AddInt("serviceid", (int)this.Service)
                    .SetMap(reader => new PatientSelection
                    {
                        Id = reader.GetGuid("Id"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName").Trim().ToUpper(),
                        LastName = reader.GetStringNullable("LastName").Trim().ToUpper(),
                        MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                    })
                    .AsList();
            }
            return list;
        }

        public List<PatientSelection> GetAuditorPatientSelectionPaymentSource(Guid agencyId, List<Guid> branchIds, int statusId, int paymentSourceId, string name, Guid userId)
        {
            return GetPatientSelectionHelper(agencyId, branchIds, statusId, paymentSourceId, name, "PaymentSource", true, userId);

            //            var query = new QueryBuilder(@"SELECT
            //                                        p.Id,
            //                                        p.PatientIdNumber, 
            //                                        p.FirstName,
            //                                        p.LastName,
            //                                        p.MiddleInitial
            //                                            FROM
            //                                                agencymanagement.patients p INNER JOIN 
            //                                                      patientprofiles pr ON p.Id = pr.Id");
            //            query
            //                .Where("pr.AgencyId = @agencyid")
            //                .And(string.Format("p.{0}Status = @statusid", this.Service.ToString()));

            //            if (!branchId.IsEmpty())
            //            {
            //                query.And("pr.AgencyLocationId = @branchId ");
            //            }

            //            if (name.IsNotNullOrEmpty())
            //            {
            //                query.And(string.Format("(pr.FirstName like '%{0}%' OR pr.LastName like '%{0}%')", name));
            //            }
            //            if (paymentSourceId > 0)
            //            {
            //                query.And(string.Format("pr.PaymentSource like '%{0};%'", paymentSourceId));
            //            }
            //            query.And("(p.Services & @serviceid) = @serviceid");
            //            if (!userId.IsEmpty())
            //            {
            //                query.And(string.Format("pr.AuditorId = '{0}'", userId));
            //            }
            //            query
            //                .And("pr.IsDeprecated = 0")
            //                .OrderBy("p.LastName", true);
            //            var list = new List<PatientSelection>();
            //            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            //            {
            //                list = cmd.SetConnection(connectionStringName)
            //                    .AddGuid("agencyid", agencyId)
            //                    .AddInt("statusid", statusId)
            //                    .AddInt("paymentsourceid", paymentSourceId)
            //                    .AddGuid("branchId", branchId)
            //                    .AddInt("serviceid", (int)this.Service)
            //                    .SetMap(reader => new PatientSelection
            //                    {
            //                        Id = reader.GetGuid("Id"),
            //                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
            //                        FirstName = reader.GetStringNullable("FirstName").Trim().ToUpper(),
            //                        LastName = reader.GetStringNullable("LastName").Trim().ToUpper(),
            //                        MI = reader.GetStringNullable("MiddleInitial").ToUpper()
            //                    })
            //                    .AsList();
            //            }
            //            return list;
        }

        public List<PatientSelection> GetAuditorPatientSelectionInsurance(Guid agencyId, List<Guid> branchIds, int statusId, int insuranceId, string name, Guid userId)
        {
            return GetPatientSelectionHelper(agencyId, branchIds, statusId, insuranceId, name, "Insurance", true, userId);
        }

        //        public List<PatientSelection> GetPatientsWithUserAccess(Guid userId, Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name)
        //        {
        //            var query = new QueryBuilder(@"SELECT 
        //                        pr.Id,
        //                        pr.PatientIdNumber,
        //                        pr.FirstName, 
        //                        pr.LastName,
        //                        pr.MiddleInitial 
        //                            FROM 
        //                               agencymanagement.patientusers pu
        //                                    JOIN patientprofiles pr on pu.patientid = pr.Id");
        //            query.Where("pu.userid = @userId")
        //                .And("pr.AgencyId = @agencyId")
        //                .And("pr.Status = @statusId");

        //            if (!branchId.IsEmpty())
        //            {
        //                query.And("pr.AgencyLocationId = @branchId");
        //            }
        //            if (name.IsNotNullOrEmpty())
        //            {
        //                query.And(string.Format("pr.PaymentSource like '%{0};%'", paymentSourceId));
        //            }
        //            query
        //                .And("pr.IsDeprecated = 0")
        //                .OrderBy("pr.LastName", true);
        //             var list = new List<PatientSelection>();
        //             using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
        //             {
        //                 list = cmd.SetConnection(connectionStringName)
        //                 .AddGuid("userId", userId)
        //                 .AddGuid("agencyId", agencyId)
        //                 .AddInt("statusId", statusId)
        //                 .AddInt("paymentSouceId", paymentSourceId)
        //                 .AddGuid("branchId", branchId)
        //                 .SetMap(reader => new PatientSelection
        //                 {
        //                     Id = reader.GetGuid("Id"),
        //                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
        //                     FirstName = reader.GetStringNullable("FirstName").Trim().ToUpper(),
        //                     LastName = reader.GetStringNullable("LastName").Trim().ToUpper(),
        //                     MI = reader.GetStringNullable("MiddleInitial").ToUpper()
        //                 })
        //                 .AsList();
        //             }
        //             return list;
        //        }

        public List<PatientRoster> GetPatientRoster(Guid agencyId, Guid branchId, int statusId, int insuranceId, int payor)
        {
            var list = new List<PatientRoster>();
            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }
            var status = string.Empty;
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged || statusId == (int)PatientStatus.Pending)
            {
                status = string.Format(" AND p.{0}Status = {1} ", this.Service.ToString(), statusId);
            }
            else
            {
                status = string.Format(" AND p.{0}Status IN(1,2,3) ", this.Service.ToString());
            }
            var script = string.Format(@"SELECT 
                        p.Id as Id, 
                        p.FirstName as FirstName ,
                        p.MiddleInitial, 
                        p.LastName as LastName , 
                        p.PatientIdNumber as PatientIdNumber, 
                        p.MedicareNumber as MedicareNumber, 
                        p.DOB as DOB,
                        p.PhoneHome as PhoneHome ,
                        p.AddressLine1 as AddressLine1,
                        p.AddressLine2 as AddressLine2,
                        p.AddressCity as AddressCity,
                        p.AddressStateCode as AddressStateCode ,
                        p.AddressZipCode as AddressZipCode,
                        p.{0}Status as Status  ,
                        p.Triage as Triage , 
                        p.Gender as Gender, 
                        pr.StartofCareDate as StartofCareDate , 
                        pr.DischargeDate as DischargeDate,
                        pr.PrimaryInsurance,
                        pr.PaymentSource as PaymentSource,
                        pr.PrimaryHealthPlanId as PrimaryHealthPlanId
                            FROM
                                agencymanagement.patients p INNER JOIN 
                                  patientprofiles pr ON p.Id = pr.Id
                                            WHERE 
                                                p.AgencyId = @agencyid {1} {2} {3} AND
                                                pr.IsDeprecated = 0 ",
                                                     this.Service.ToString(),
                                                     status,
                                                     !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty,
                                                     insurance);

            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", DateTime.Now.Date)
                .SetMap(reader => new PatientRoster
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetStringNullable("PatientIdNumber"),
                    PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientGender = reader.GetStringNullable("Gender"),
                    PatientMedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    PatientDOB = reader.GetDateTime("DOB"),
                    PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                    PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                    PatientAddressCity = reader.GetStringNullable("AddressCity"),
                    PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientSoC = reader.GetDateTime("StartofCareDate"),
                    PatientInsurance = reader.GetStringNullable("PaymentSource"),
                    PatientInsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PatientInsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    PatientDischargeDate = reader.GetInt("Status") == (int)PatientStatus.Discharged && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate") : DateTime.MinValue,
                    Triage = reader.GetInt("Triage")
                }).AsList();
            }
            return list;
        }

        public List<PatientRoster> GetPatientRosterByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate, int payor)
        {
            var list = new List<PatientRoster>();
            var status = " AND pa.Status IN(1,2,3) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged || statusId == (int)PatientStatus.Pending)
            {
                status = string.Format(" AND pa.Status = {0} ", statusId);
            }
            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            var script = string.Format(@"SELECT DISTINCT 
                            p.Id as Id,
                            p.FirstName as FirstName , 
                            p.MiddleInitial,
                            pr.PrimaryInsurance, 
                            p.LastName as LastName ,
                            p.PatientIdNumber as PatientIdNumber, 
                            p.MedicareNumber as MedicareNumber,
                            p.DOB as DOB,
                            p.PhoneHome as PhoneHome ,
                            p.AddressLine1 as AddressLine1,
                            p.AddressLine2 as AddressLine2,
                            p.AddressCity as AddressCity,
                            p.AddressStateCode as AddressStateCode ,
                            p.AddressZipCode as AddressZipCode,
                            pa.Status as Status  ,
                            p.Triage as Triage , 
                            p.Gender as Gender, 
                            pr.PaymentSource as PaymentSource,
                            pr.PrimaryHealthPlanId as PrimaryHealthPlanId,
                            pa.StartOfCareDate as StartOfCareDate,
                            pa.DischargedDate as DischargeDate 
                                FROM
                                    patientadmissiondates pa
                                        INNER JOIN agencymanagement.patients p ON pa.PatientId = p.Id  
                                        INNER JOIN patientprofiles pr ON pr.Id = p.Id 
                                            WHERE
                                                p.AgencyId = @agencyid {0} {1} {2} AND
                                                pr.IsDeprecated = 0 AND
                                                pa.IsDeprecated = 0 AND 
                                                ((DATE(pa.StartOfCareDate) BETWEEN DATE(@startdate) AND DATE(@enddate)) OR DATEDIFF(pa.StartOfCareDate, @startdate) < 0) AND
                                                ((pa.DischargedDate != '0001-01-01' AND DATEDIFF(pa.StartOfCareDate, pa.DischargedDate) < 0 AND
                                                (DATEDIFF(pa.DischargedDate, @startdate) > 0 OR pa.Status = 1)) OR pa.DischargedDate = '0001-01-01')"
                , status
                , !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty, insurance);

            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientRoster
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetStringNullable("PatientIdNumber"),
                    PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientGender = reader.GetStringNullable("Gender"),
                    PatientMedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    PatientDOB = reader.GetDateTime("DOB"),
                    PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                    PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                    PatientAddressCity = reader.GetStringNullable("AddressCity"),
                    PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientSoC = reader.GetDateTime("StartofCareDate"),
                    PatientInsurance = reader.GetStringNullable("PaymentSource"),
                    PatientInsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PatientInsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    PatientDischargeDate = reader.GetInt("Status") == (int)PatientStatus.Discharged && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate") : DateTime.MinValue,
                    Triage = reader.GetInt("Triage")
                }).AsList();
            }
            return list;
        }

        public List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status)
        {
            var emergencyContactInfos = new List<EmergencyContactInfo>();
            var script = string.Format(@"SELECT 
                                p.FirstName as FirstName ,
                                p.LastName as LastName ,
                                p.MiddleInitial as MiddleInitial,
                                p.PatientIdNumber as PatientIdNumber ,
                                p.Triage as Triage , 
                                pec.FirstName as ContactFirstName, 
                                pec.LastName as ContactLastName ,
                                pec.Relationship as Relationship ,
                                pec.PrimaryPhone as PrimaryPhone ,
                                pec.EmailAddress as EmailAddress 
                                    FROM 
                                        agencymanagement.patientemergencycontacts pec
                                            INNER JOIN agencymanagement.patients p ON pec.PatientId = p.Id 
                                               INNER JOIN patientprofiles pr ON pr.Id = p.Id 
                                                    WHERE 
                                                        p.AgencyId = @agencyId  AND
                                                        pec.IsPrimary = 1  {0} {1} AND
                                                        pr.IsDeprecated = 0 
                                                             ORDER BY p.LastName ASC , p.FirstName ASC "
                , status == 0 ? string.Format(" AND p.{0}Status IN (1,2)  "
                , this.Service.ToString()) : string.Format(" AND p.{0}Status = @statusid "
                , this.Service.ToString())
                , !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<EmergencyContactInfo>(script))
            {
                emergencyContactInfos = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusid", status)
                    .SetMap(reader => new EmergencyContactInfo
                    {
                        PatientName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty),
                        Triage = reader.GetInt("Triage"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        ContactName = string.Format("{0}, {1}", reader.GetStringNullable("ContactLastName").ToUpperCase(), reader.GetStringNullable("ContactFirstName").ToUpperCase()),
                        ContactRelation = reader.GetStringNullable("Relationship"),
                        ContactPhoneHome = reader.GetStringNullable("PrimaryPhone").ToPhone(),
                        ContactEmailAddress = reader.GetStringNullable("EmailAddress")
                    })
                    .AsList();
            }
            return emergencyContactInfos;
        }

        public IList<Birthday> GetPatientBirthdays(Guid agencyId, Guid branchId, int month)
        {
            var birthdays = new List<Birthday>();
            var script = string.Format(@"SELECT 
                            p.FirstName ,
                            p.LastName ,
                            p.MiddleInitial,
                            p.PatientIdNumber ,
                            p.DOB , 
                            p.AddressLine1, 
                            p.AddressLine2 ,
                            p.AddressCity ,
                            p.AddressStateCode ,
                            p.AddressZipCode, 
                            p.PhoneHome ,
                            p.PhoneMobile ,
                            p.EmailAddress  
                                FROM
                                    agencymanagement.patients p INNER JOIN 
                                      patientprofiles pr ON p.Id = pr.Id
                                        WHERE 
                                            p.AgencyId = @agencyId  AND 
                                            month( p.DOB )= @month  {1} AND
                                            p.{0}Status = 1 AND 
                                            pr.IsDeprecated = 0
                                                ORDER BY p.LastName ASC , p.FirstName ASC "
                , this.Service.ToString()
                , !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<Birthday>(script))
            {
                birthdays = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddInt("month", month)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new Birthday
                    {
                        Name = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial") + "." : ""),
                        IdNumber = reader.GetStringNullable("PatientIdNumber"),
                        Date = reader.GetDateTime("DOB"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                    })
                    .AsList();
            }

            return birthdays;

        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid agencyId, Guid branchId, int status)
        {
            var adressBookEntrys = new List<AddressBookEntry>();
            var script = string.Format(@"SELECT 
                                    p.FirstName ,
                                    p.LastName ,
                                    p.MiddleInitial,
                                    p.PatientIdNumber , 
                                    p.AddressLine1, 
                                    p.AddressLine2 ,
                                    p.AddressCity ,
                                    p.AddressStateCode ,
                                    p.AddressZipCode,
                                    p.PhoneHome , 
                                    p.PhoneMobile ,
                                    p.EmailAddress  
                                        FROM
                                            agencymanagement.patients p
                                               INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                WHERE 
                                                    p.AgencyId = @agencyId   {0} {1}  AND
                                                    pr.IsDeprecated = 0
                                                        ORDER BY p.LastName ASC , p.FirstName ASC"
                , status == 0 ? string.Format(" AND p.{0}Status IN(1,2)"
                , this.Service.ToString()) : string.Format(" AND p.{0}Status = @statusid "
                , this.Service.ToString())
                , !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<AddressBookEntry>(script))
            {
                adressBookEntrys = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusid", status)
                    .SetMap(reader => new AddressBookEntry
                    {
                        Name = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial") + "." : ""),
                        IdNumber = reader.GetStringNullable("PatientIdNumber"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                    })
                    .AsList();
            }
            return adressBookEntrys;
        }

        public List<DischargePatient> GetDischargePatients(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var dischargePatients = new List<DischargePatient>();
            var script = string.Format(@"SELECT 
                    pr.FirstName , 
                    pr.LastName , 
                    pr.MiddleInitial,
                    pr.PatientIdNumber,
                    pr.DischargeReasonId,
                    pa.StartofCareDate, 
                    pa.DischargedDate, 
                    pa.PatientData 
                        FROM
                            patientprofiles pr 
                                JOIN patientadmissiondates pa on pr.Id = pa.PatientId 
                                    WHERE 
                                        pa.AgencyId = @agencyId AND
                                        pa.Status = 2 AND 
                                        pr.IsDeprecated = 0 AND
                                        pa.IsDeprecated = 0  AND 
                                            DATE(pa.DischargedDate) between DATE(@startdate) and DATE(@enddate) {0} 
                                                ORDER BY pr.LastName ASC, pr.FirstName ASC "
                , !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<DischargePatient>(script))
            {
                dischargePatients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new DischargePatient
                    {
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        StartofCareDate = reader.GetDateTime("StartofCareDate"),
                        PatientData = reader.GetStringNullable("PatientData"),
                        DischargeDate = reader.GetDateTime("DischargedDate"),
                        DischargeReasonId = reader.GetInt("DischargeReasonId")
                    }).AsList();
            }

            foreach (var patient in dischargePatients)
            {
                if (patient.DischargeReasonId > 0)
                {
                    string reason = EnumExtensions.GetDescription((DischargeReasons)patient.DischargeReasonId);
                    if (reason != null)
                    {
                        patient.DischargeReason = reason;
                    }
                }
                else
                {
                    var patientData = patient.DischargeReason.ToObject<Patient>();
                    if (patientData != null)
                    {
                        //  patient.DischargeReason = patientData.DischargeReason;
                    }
                }
                patient.PatientData = string.Empty;
            }
            return dischargePatients;
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid agencyId, Guid branchId, Guid userId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT 
                                        p.FirstName ,
                                        p.LastName ,
                                        p.MiddleInitial,
                                        p.PatientIdNumber ,  
                                        p.AddressLine1 ,
                                        p.AddressLine2 ,
                                        p.AddressCity ,
                                        p.AddressStateCode ,
                                        p.AddressZipCode ,
                                        pr.StartofCareDate  
                                            FROM 
                                                agencymanagement.patients p
                                                    INNER JOIN patientprofiles pr ON p.Id = pr.Id 
                                                        WHERE 
                                                            p.AgencyId = @agencyId AND
                                                            pr.UserId = @userId  {0} {1} AND
                                                            pr.IsDeprecated = 0
                                                                ORDER BY p.LastName ASC , p.FirstName ASC "
                                                                    , status == 0 ? string.Format(" AND p.{0}Status IN(1,2)"
                                                                    , this.Service.ToString()) : string.Format(" AND p.{0}Status = @statusid ", this.Service.ToString())
                                                                    , !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddGuid("userId", userId)
                     .AddInt("statusid", status)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientSoC = reader.GetDateTime("StartofCareDate")
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid agencyId, Guid branchId, Guid caseManagerId)
        {
            var patients = new List<PatientRoster>();
            var script = string.Format(@"SELECT 
                            p.FirstName ,
                            p.LastName , 
                            p.MiddleInitial,
                            p.PatientIdNumber ,
                            p.{0}Status as Status, 
                            p.AddressLine1 ,
                            p.AddressLine2 ,
                            p.AddressCity , 
                            p.AddressStateCode ,
                            p.AddressZipCode , 
                            pr.StartofCareDate  
                                FROM 
                                    agencymanagement.patients p
                                      INNER JOIN 
                                          patientprofiles pr ON p.Id = pr.Id
                                            WHERE  
                                                p.AgencyId = @agencyId AND
                                                pr.CaseManagerId = @caseManagerId  AND
                                                p.{0}Status IN(1,2) {1} AND
                                                pr.IsDeprecated = 0 
                                                    ORDER BY p.LastName ASC , p.FirstName ASC",
                                                            this.Service.ToString(),
                                                            !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddGuid("caseManagerId", caseManagerId)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientStatus = reader.GetInt("Status"),
                        PatientSoC = reader.GetDateTime("StartofCareDate")
                    }).AsList();
            }
            return patients;
        }

        public List<SurveyCensus> GetSurveyCensesByStatus(Guid agencyId, Guid branchId, int statusId, int insuranceId, int payor)
        {
            var list = new List<SurveyCensus>();
            var status = string.Empty;
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged)
            {
                status = string.Format(" AND p.{0}Status = @statusid ", this.Service.ToString());
            }
            else
            {
                status = string.Format(" AND p.{0}Status IN (1,2) ", this.Service.ToString());
            }

            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            var script = string.Format(@"SELECT 
                                p.Id as Id,
                                p.FirstName as FirstName ,
                                p.LastName as LastName,
                                p.MiddleInitial as MiddleInitial,
                                pr.PrimaryHealthPlanId as PrimaryHealthPlanId,
                                p.PatientIdNumber as PatientIdNumber,
                                p.MedicareNumber as MedicareNumber, 
                                p.DOB as DOB,
                                p.PhoneHome as PhoneHome , 
                                p.AddressLine1 as AddressLine1, 
                                p.AddressLine2 as AddressLine2,
                                p.AddressCity as AddressCity,
                                p.AddressStateCode as AddressStateCode ,
                                p.AddressZipCode as AddressZipCode,
                                pr.StartofCareDate as StartofCareDate ,
                                pr.CaseManagerId as CaseManagerId ,
                                p.SSN as SSN , 
                                p.Triage as Triage , 
                                p.Gender as Gender,
                                p.EvacuationZone as EvacuationZone,
                                p.Triage as Triage,
                                pr.PrimaryInsurance as PrimaryInsurance
                                    FROM 
                                        agencymanagement.patients p
                                            INNER JOIN patientprofiles pr ON pr.Id = p.Id
                                                WHERE
                                                    p.AgencyId = @agencyid AND 
                                                    pr.IsDeprecated = 0 {0} {1} {2} 
                                                        ORDER BY p.LastName ASC , p.FirstName ASC ",
                                                                                                   status,
                                                                                                   !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                                                                   insurance);

            using (var cmd = new FluentCommand<SurveyCensus>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchid", branchId)
                .AddInt("statusid", statusId)
                .SetMap(reader => new SurveyCensus
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    DOB = reader.GetDateTime("DOB"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    Gender = reader.GetStringNullable("Gender"),
                    SOC = reader.GetDateTime("StartofCareDate"),
                    Triage = reader.GetInt("Triage"),
                    SSN = reader.GetStringNullable("SSN"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    EvacuationZone = reader.GetStringNullable("EvacuationZone"),
                    CaseManagerId = reader.GetGuidIncludeEmpty("CaseManagerId")
                }).AsList();
            }
            return list;
        }

        public List<PatientWithPhysicanInfo> GetPatientPhysicianInfos(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<PatientWithPhysicanInfo>();
            var statusScript = string.Empty;
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged)
            {
                statusScript = string.Format(" AND p.{0}Status = @statusid ", this.Service.ToString());
            }
            else
            {
                statusScript = string.Format(" AND p.{0}Status IN (1,2) ", this.Service.ToString());
            }
            var script = string.Format(@"SELECT 
                                            p.Id as Id ,
                                            p.PatientIdNumber as PatientIdNumber ,
                                            p.FirstName as FirstName ,
                                            p.LastName as LastName ,
                                            p.MiddleInitial as MiddleInitial, 
                                            pr.UserId as  UserId
                                                FROM 
                                                    agencymanagement.patients p
                                                        INNER JOIN patientprofiles pr ON pr.Id = p.Id
                                                            WHERE
                                                                p.AgencyId = @agencyid AND
                                                                pr.IsDeprecated = 0  {0} {1} 
                                                                  ORDER BY p.Created DESC ",
                                                                                   statusScript,
                                                                                   !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty);
            using (var cmd = new FluentCommand<PatientWithPhysicanInfo>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchid", branchId)
                .AddInt("statusid", statusId)
                .SetMap(reader => new PatientWithPhysicanInfo
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    UserId = reader.GetGuidIncludeEmpty("UserId")
                }).AsList();
            }
            return list;
        }

        public List<PatientRoster> GetPatientByInsurance(Guid agencyId, Guid branchId, int insuranceId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT 
                                            p.Id ,
                                            p.FirstName ,
                                            p.LastName ,
                                            p.MiddleInitial,
                                            p.PatientIdNumber ,  
                                            p.AddressLine1 , 
                                            p.AddressLine2 , 
                                            p.AddressCity , 
                                            p.AddressStateCode ,
                                            p.AddressZipCode ,
                                            p.PhoneHome ,
                                            p.Gender 
                                                FROM 
                                                    agencymanagement.patients p
                                                      INNER JOIN patientprofiles pr ON p.Id = pr.Id
                                                        WHERE 
                                                            p.AgencyId = @agencyId AND
                                                            pr.PrimaryInsurance = @insuranceId  {0} {1} AND
                                                            pr.IsDeprecated = 0",
                                                                                status == 0 ? string.Format(" AND p.{0}Status IN(1,2) ", this.Service.ToString()) : string.Format(" AND p.{0}Status = @statusid ", this.Service.ToString()),
                                                                                !branchId.IsEmpty() ? "AND pr.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("insuranceId", insuranceId)
                    .AddInt("statusid", status)
                    .SetMap(reader => new PatientRoster
                    {
                        Id = reader.GetGuid("Id"),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PatientGender = reader.GetStringNullable("Gender")
                    })
                    .AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<BirthdayWidget> GetCurrentPatientBirthdays(Guid agencyId)
        {
            var script = string.Format(@"SELECT
                                            p.`Id`,
                                            p.`FirstName`,
                                            p.`LastName`,
                                            p.`DOB`,
                                            p.`PhoneHome`,
                                            (YEAR(CURDATE())-YEAR(p.`DOB`)) - (RIGHT(CURDATE(),5)<RIGHT(p.`DOB`,5)) AS Age,
                                            p.`Status`
                                                FROM
                                                    agencymanagement.patients p
                                                        WHERE 
                                                            p.`AgencyId` = @agencyid AND
                                                            p.`{0}Status` = @status AND
                                                            p.`{0}Status` != @excludestatus AND
                                                             MONTH(`DOB`) = @month 
                                                                ORDER BY Age DESC 
                                                                    LIMIT 0, 5", this.Service.ToString());
            return new FluentCommand<BirthdayWidget>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddInt("month", DateTime.Now.Month)
                .AddInt("status", (int)PatientStatus.Active)
                .AddInt("excludestatus", (int)PatientStatus.Deprecated)
                .SetMap(reader => new BirthdayWidget
                {
                    Id = reader.GetGuid("Id"),
                    Date = reader.GetDateTime("DOB"),
                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    Name = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
        }

        public List<Authorization> GetExpiringAuthorizaton(Guid agencyId, Guid branchId, int statusId, DateTime expiringDate)
        {
            var authorizations = new List<Authorization>();
            var script = string.Format(@"SELECT
                                            au.Id as Id ,
                                            au.Number1 as Number1,
                                            au.Number2 as Number2, 
                                            au.Number3 as Number3, 
                                            au.StartDate as StartDate,
                                            au.EndDate as EndDate ,
                                            au.Status as Status,
                                            pr.FirstName as FirstName,
                                            pr.LastName as LastName, 
                                            pr.MiddleInitial as MiddleInitial
                                                FROM 
                                                    authorizations au
                                                         INNER JOIN patientprofiles pr ON au.PatientId = pr.Id
                                                            WHERE 
                                                                pr.AgencyId = @agencyId {0} {1} AND
                                                                au.IsDeprecated = 0 AND
                                                                au.Service = @service AND
                                                                Date(au.EndDate) <=  DATE(@expiringdate)
                                                                    ORDER BY au.StartDate ASC",
                                                                              branchId.IsEmpty() ? "" : " AND pr.AgencyLocationId = @branchId ",
                                                                              statusId > 0 ? " AND pr.Status = @statusId " : "");
            using (var cmd = new FluentCommand<Authorization>(script))
            {
                authorizations = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusId", statusId)
                    .AddDateTime("expiringdate", expiringDate)
                    .AddInt("service", (int)this.Service)
                    .SetMap(reader => new Authorization
                    {
                        Id = reader.GetGuid("Id"),
                        Number1 = reader.GetStringNullable("Number1"),
                        Number2 = reader.GetStringNullable("Number2"),
                        Number3 = reader.GetStringNullable("Number3"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate"),
                        Status = reader.GetStringNullable("Status"),
                        DisplayName = string.Concat(reader.GetStringNullable("LastName").ToUpperCase(), ", ", reader.GetStringNullable("FirstName").ToUpperCase(), (reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? " " + reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty))
                    })
                    .AsList();
            }
            return authorizations;
        }

        public Dictionary<string, string> GetProfileJsonByColumns(Guid agencyId, Guid id, params string[] columns)
        {
            return EntityHelper.SelectEntityJsonByColumns<Profile>(agencyId, id, connectionStringName, columns);
        }

        public bool UpdatePatientForInsuranceIds(Guid agencyId, Guid patientId, string insuranceIds)
        {
            var script = @"UPDATE patientprofiles set InsuranceIds = @insuranceIds WHERE AgencyId = @agencyid AND Id = @id;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddString("insuranceIds", insuranceIds)
                     .AsNonQuery();
            }
            return count > 0;
        }

        public bool UpdatePatientForInsuranceIds(Guid agencyId, Guid patientId, Guid admissionId, string insuranceIds)
        {
            var script = @"UPDATE patientprofiles set InsuranceIds = @insuranceIds WHERE AgencyId = @agencyid AND Id = @id AND AdmissionId = @admissionId;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddGuid("admissionId", admissionId)
                     .AddString("insuranceIds", insuranceIds)
                     .AsNonQuery();
            }
            return count > 0;
        }


        public bool AddInsurance(PatientInsurance patientInsurance)
        {
            bool result = false;
            try
            {
                if (patientInsurance != null)
                {
                    patientInsurance.Created = DateTime.Now;
                    patientInsurance.Modified = DateTime.Now;
                    var data = database.Add<PatientInsurance>(patientInsurance);
                    patientInsurance.Id = (int)data;
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateInsuranceModel(PatientInsurance patientInsurance)
        {
            bool result = false;
            try
            {
                if (patientInsurance != null)
                {
                    patientInsurance.Modified = DateTime.Now;
                    return database.Update<PatientInsurance>(patientInsurance) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveInsurance(int id)
        {
            try
            {
                return database.Delete<PatientInsurance>(id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetPatientInsuranceIds(Guid agencyId, Guid patientId)
        {
            var insuranceIds = new object(); ;
            var script = string.Format(@"SELECT
                                          pr.InsuranceIds
                                            FROM 
                                              patientprofiles pr
                                                WHERE 
                                                    pr.AgencyId = @agencyId AND
                                                    pr.Id = @patientId LIMIT  1");
            using (var cmd = new FluentCommand<object>(script))
            {
                insuranceIds = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)

                    .SetMap(reader => reader.GetStringNullable("InsuranceIds"))
                    .AsSingle();
            }
            return insuranceIds.ToString();
        }

        public PatientInsurance GetPatientInsuranceOnly(Guid agencyId, Guid patientId, int id)
        {
            return database.Single<PatientInsurance>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == id);
        }

        public List<PatientInsurance> GetPatientInsurances(Guid agencyId, Guid patientId, string insuranceIds)
        {
            var insurances = new List<PatientInsurance>();
            var script = string.Format(@"SELECT
                                            Id,
                                            InsuranceId,
                                            HealthPlanId,
                                            GroupName,
                                            GroupId,
                                            RelationId,
                                            Type,
                                            ParentInsuranceId
                                                FROM 
                                                  patientinsurances pi
                                                    WHERE 
                                                        pi.AgencyId = @agencyId AND
                                                        pi.PatientId = @patientId AND
                                                        pi.InsuranceId IN (@ids) AND
                                                        pi.IsDeprecated = 0");
            using (var cmd = new FluentCommand<PatientInsurance>(script))
            {
                insurances = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddString("ids", insuranceIds)
                     .SetMap(reader => new PatientInsurance()
                     {
                         Id = reader.GetInt("Id"),
                         InsuranceId = reader.GetInt("InsuranceId"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         RelationId = reader.GetInt("RelationId", 0),
                         Type = reader.GetInt("Type", 0),
                         ParentInsuranceId = reader.GetInt("ParentInsuranceId", 0)

                     })
                    .AsList();
            }
            return insurances;
        }

        public List<PatientInsurance> GetPatientInsurances(Guid agencyId, Guid patientId, Guid admissionId)
        {
            var insurances = new List<PatientInsurance>();
            var script = string.Format(@"SELECT
                                            Id,
                                            InsuranceId,
                                            HealthPlanId,
                                            GroupName,
                                            GroupId,
                                            RelationId,
                                            Type,
                                            ActualStartDate,
                                            ActualEndDate,
                                            ParentInsuranceId,
                                            IsOpenStartOfAdmission,
                                            IsOpenEndOfAdmission
                                                FROM 
                                                  patientinsurances pi
                                                    WHERE 
                                                        pi.AgencyId = @agencyId AND
                                                        pi.PatientId = @patientId AND
                                                        pi.AdmissionId = @admissionId AND
                                                        pi.IsDeprecated = 0");
            using (var cmd = new FluentCommand<PatientInsurance>(script))
            {
                insurances = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("admissionId", admissionId)
                        .SetMap(reader => new PatientInsurance()
                     {
                         Id = reader.GetInt("Id"),
                         InsuranceId = reader.GetInt("InsuranceId"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         GroupName = reader.GetStringNullable("GroupName"),
                         GroupId = reader.GetStringNullable("GroupId"),
                         RelationId = reader.GetInt("RelationId", 0),
                         Type = reader.GetInt("Type", 0),
                         ActualStartDate = reader.GetDateTime("ActualStartDate"),
                         ActualEndDate = reader.GetDateTime("ActualEndDate"),
                         ParentInsuranceId = reader.GetInt("ParentInsuranceId", 0),
                         IsOpenStartOfAdmission = reader.GetBoolean("IsOpenStartOfAdmission"),
                         IsOpenEndOfAdmission = reader.GetBoolean("IsOpenEndOfAdmission")
                     }).AsList();
            }
            return insurances;
        }

        public List<PatientInsurance> GetPatientInsurances(Guid agencyId, Guid patientId, Guid admissionId, DateTime startDate, DateTime endDate)
        {
            var insurances = new List<PatientInsurance>();
            var script = string.Format(@"SELECT
                                            Id,
                                            InsuranceId,
                                            HealthPlanId,
                                            GroupName,
                                            GroupId,
                                            RelationId,
                                            Type,
                                            ActualStartDate,
                                            ActualEndDate,
                                            ParentInsuranceId,
                                            IsOpenStartOfAdmission,
                                            IsOpenEndOfAdmission
                                                FROM 
                                                  patientinsurances pi
                                                    WHERE 
                                                        pi.AgencyId = @agencyId AND
                                                        pi.PatientId = @patientId AND
                                                        pi.AdmissionId = @admissionId AND
                                                        DATE(pi.StartDate) < DATE(@enddate) AND
                                                        DATE(pi.EndDate) > DATE(@startdate) AND
                                                        pi.IsDeprecated = 0");
            using (var cmd = new FluentCommand<PatientInsurance>(script))
            {
                insurances = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("admissionId", admissionId)
                     .AddDateTime("startdate", startDate)
                      .AddDateTime("enddate", endDate)
                        .SetMap(reader => new PatientInsurance()
                        {
                            Id = reader.GetInt("Id"),
                            InsuranceId = reader.GetInt("InsuranceId"),
                            HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                            GroupName = reader.GetStringNullable("GroupName"),
                            GroupId = reader.GetStringNullable("GroupId"),
                            RelationId = reader.GetInt("RelationId", 0),
                            Type = reader.GetInt("Type", 0),
                            ActualStartDate = reader.GetDateTime("ActualStartDate"),
                            ActualEndDate = reader.GetDateTime("ActualEndDate"),
                            ParentInsuranceId = reader.GetInt("ParentInsuranceId", 0),
                            IsOpenStartOfAdmission = reader.GetBoolean("IsOpenStartOfAdmission"),
                            IsOpenEndOfAdmission = reader.GetBoolean("IsOpenEndOfAdmission")
                        }).AsList();
            }
            return insurances;
        }

        public List<PatientInsurance> GetPatientInsurancesLean(Guid agencyId, Guid patientId, Guid optionalAdmissionId, DateTime startDate, DateTime endDate)
        {
            var insurances = new List<PatientInsurance>();
            var script = string.Format(@"SELECT
                                            Id,
                                            InsuranceId,
                                            Type
                                                FROM 
                                                  patientinsurances pi
                                                    WHERE 
                                                        pi.AgencyId = @agencyId AND
                                                        pi.PatientId = @patientId AND
                                                        {0}
                                                        DATE(pi.StartDate) < DATE(@enddate) AND
                                                        DATE(pi.EndDate) > DATE(@startdate) AND
                                                        pi.IsDeprecated = 0", optionalAdmissionId.IsEmpty() ? string.Empty : " pi.AdmissionId = @admissionId AND ");
            using (var cmd = new FluentCommand<PatientInsurance>(script))
            {
                insurances = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("admissionId", optionalAdmissionId)
                     .AddDateTime("startdate", startDate)
                      .AddDateTime("enddate", endDate)
                        .SetMap(reader => new PatientInsurance()
                        {
                            Id = reader.GetInt("Id"),
                            InsuranceId = reader.GetInt("InsuranceId"),
                            Type = reader.GetInt("Type", 0)
                        }).AsList();
            }
            return insurances;
        }

        public List<PatientInsurance> GetPatientInsurancesLean(Guid agencyId, Guid patientId, Guid optionalAdmissionId, DateTime date)
        {
            var insurances = new List<PatientInsurance>();
            try
            {
                var script = string.Format(@"SELECT
                                                Id,
                                                InsuranceId,
                                                Type
                                                    FROM 
                                                      patientinsurances pi
                                                        WHERE 
                                                            pi.AgencyId = @agencyId AND
                                                            pi.PatientId = @patientId AND
                                                            {0}
                                                            DATE(@date) between DATE(pi.StartDate) and  DATE(pi.EndDate) AND
                                                            pi.IsDeprecated = 0", optionalAdmissionId.IsEmpty() ? string.Empty : " pi.AdmissionId = @admissionId AND ");
                using (var cmd = new FluentCommand<PatientInsurance>(script))
                {
                    insurances = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyId", agencyId)
                        .AddGuid("patientId", patientId)
                        .AddGuid("admissionId", optionalAdmissionId)
                        .AddDateTime("date", date)
                            .SetMap(reader => new PatientInsurance()
                            {
                                Id = reader.GetInt("Id"),
                                InsuranceId = reader.GetInt("InsuranceId"),
                                Type = reader.GetInt("Type", 0)
                            }).AsList();
                }
            }
            catch (Exception)
            {
                return insurances;
            }
            return insurances;
        }

        public bool IsPatientInsuranceExist(Guid agencyId, Guid patientId, Guid admissionId, int insuranceId, int insuranceType, DateTime startDate, DateTime endDate, int excludedPayorIdentifier)
        {
            var value = 0;
            var script = string.Format(@"SELECT 
                                             1 
                                                FROM 
                                                   patientinsurances pi 
                                                        WHERE 
                                                            pi.AgencyId = @agencyid AND
                                                            pi.PatientId = @patientid AND 
                                                            pi.AdmissionId = @admissionid AND 
                                                            pi.InsuranceId= @insuranceid AND
                                                            pi.Type = @insurancetype AND
                                                            DATE(pi.StartDate) < DATE(@enddate) AND
                                                            DATE(pi.EndDate) > DATE(@startdate) AND 
                                                            pi.IsDeprecated= 0
                                                                limit 1", excludedPayorIdentifier > 0 ? " pi.Id != @id AND " : string.Empty);
            using (var cmd = new FluentCommand<int>(script))
            {
                value = cmd.SetConnection(connectionStringName)
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientId", patientId)
                   .AddGuid("admissionid", admissionId)
                   .AddInt("insuranceid", insuranceId)
                   .AddInt("id", excludedPayorIdentifier)
                   .AddInt("insurancetype", insuranceType)
                   .AddDateTime("startdate", startDate)
                   .AddDateTime("enddate", endDate)
                   .AsScalar();
            }
            return value > 0;
        }

        #region Authorization

        public bool AddAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                if (authorization != null)
                {
                    authorization.Created = DateTime.Now;
                    authorization.Modified = DateTime.Now;
                    database.Add<Authorization>(authorization);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool EditAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                var authorizationToEdit = database.Single<Authorization>(a => a.AgencyId == authorization.AgencyId && a.PatientId == authorization.PatientId && a.Id == authorization.Id);
                if (authorizationToEdit != null)
                {
                    authorizationToEdit.StartDate = authorization.StartDate;
                    authorizationToEdit.EndDate = authorization.EndDate;
                    //authorizationToEdit.AgencyLocationId = authorization.AgencyLocationId;
                    authorizationToEdit.Status = authorization.Status;
                    authorizationToEdit.InsuranceId = authorization.InsuranceId;
                   
                    authorizationToEdit.Number1 = authorization.Number1;
                    authorizationToEdit.Number2 = authorization.Number2;
                    authorizationToEdit.Number3 = authorization.Number3;
                   
                    authorizationToEdit.SNVisit = authorization.SNVisit;
                    authorizationToEdit.SNVisitCountType = authorization.SNVisitCountType;
                   
                    authorizationToEdit.PTVisit = authorization.PTVisit;
                    authorizationToEdit.PTVisitCountType = authorization.PTVisitCountType;
                   
                    authorizationToEdit.OTVisit = authorization.OTVisit;
                    authorizationToEdit.OTVisitCountType = authorization.OTVisitCountType;
                    
                    authorizationToEdit.STVisit = authorization.STVisit;
                    authorizationToEdit.STVisitCountType = authorization.STVisitCountType;
                   
                    authorizationToEdit.MSWVisit = authorization.MSWVisit;
                    authorizationToEdit.MSWVisitCountType = authorization.MSWVisitCountType;
                   
                    authorizationToEdit.HHAVisit = authorization.HHAVisit;
                    authorizationToEdit.HHAVisitCountType = authorization.HHAVisitCountType;

                    authorizationToEdit.DieticianVisit = authorization.DieticianVisit;
                    authorizationToEdit.DieticianVisitCountType = authorization.DieticianVisitCountType;

                    authorizationToEdit.RNVisit = authorization.RNVisit;
                    authorizationToEdit.RNVisitCountType = authorization.RNVisitCountType;

                    authorizationToEdit.LVNVisit = authorization.LVNVisit;
                    authorizationToEdit.LVNVisitCountType = authorization.LVNVisitCountType;
                   
                    authorizationToEdit.Comments = authorization.Comments;

                    authorizationToEdit.Modified = DateTime.Now;
                    return database.Update<Authorization>(authorizationToEdit) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            try
            {
                var script = @"UPDATE authorizations set IsDeprecated = @isdeprecated, Modified = @date WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id;";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientId", patientId)
                         .AddGuid("id", Id)
                         .AddDateTime("date", DateTime.Now)
                         .AddBoolean("isdeprecated", true)
                         .AsNonQuery();
                }
                return count > 0;
                //var authorization = database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
                //if (authorization != null)
                //{
                //    authorization.IsDeprecated = true;
                //    authorization.Modified = DateTime.Now;
                //    return database.Update<Authorization>(authorization) > 0;
                //}
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Authorization GetAuthorization(Guid agencyId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.Id == Id);
        }

        public Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
        }

        public IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId)
        {
            //var authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && ((int)a.Service) == service && a.IsDeprecated == false);
            var authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);

            return authorizations.OrderBy(a => a.StartDate).ToList();
        }

        public IList<Authorization> GetAuthorizationsByStatus(Guid agencyId, Guid patientId, int insuranceId, string status, int service, DateTime startDate, DateTime endDate)
        {
            var authorizations = new List<Authorization>();
            var script = string.Format(@"SELECT
                                            Id ,
                                            Number1 ,
                                            Number2 , 
                                            Number3 , 
                                            StartDate ,
                                            EndDate 
                                                FROM 
                                                    authorizations 
                                                        WHERE 
                                                            AgencyId = @agencyId AND
                                                            PatientId = @patientId  AND
                                                            Insurance = @insuranceId AND
                                                            Status = @status  AND
                                                            DATE(StartDate) < DATE(@enddate) AND
                                                            DATE(EndDate) > DATE(@startdate)");

            // ((DATE(@startdate) between DATE(StartDate) and Date (EndDate)) OR ( DATE(@enddate) between DATE(StartDate) and Date (EndDate)) OR (DATE(StartDate) between  DATE(@startdate) and Date (@enddate)) OR (DATE(EndDate) between  DATE(@startdate) and Date (@enddate))) 
            using (var cmd = new FluentCommand<Authorization>(script))
            {
                authorizations = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddInt("insuranceId", insuranceId)
                    .AddString("status", status)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new Authorization
                    {
                        Id = reader.GetGuid("Id"),
                        Number1 = reader.GetStringNullable("Number1"),
                        Number2 = reader.GetStringNullable("Number2"),
                        Number3 = reader.GetStringNullable("Number3"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")
                    }).AsList();
            }
            if (authorizations.IsNotNullOrEmpty())
            {
                return authorizations.OrderBy(a => a.StartDate).ToList();
            }
            return authorizations;
        }

    

        #endregion

    }
}
