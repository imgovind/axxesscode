﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public abstract class PlanOfCareAbstract
    {

        #region Constructor

        protected SimpleRepository database;
        protected string connectionStringName;
        protected AgencyServices Service { get; set; }
      
        #endregion

        #region IPlanofCareRepository Member

        public bool Add(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                planofCare.Created = DateTime.Now;
                planofCare.Modified = DateTime.Now;
                database.Add(planofCare);
                return true;
            }
            return false;
        }

        public bool AddMultiple(List<PlanofCare> planofCares)
        {
            try
            {
                if (planofCares != null && planofCares.Count > 0)
                {
                    database.AddMany(planofCares);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool Update(PlanofCare planofCare)
        {
            bool result = false;
            try
            {
                if (planofCare != null)
                {
                    planofCare.Modified = DateTime.Now;
                    result = database.Update(planofCare) > 0;
                }
            }
            catch (Exception)
            {
                
            }
            return result;
        }

        public PlanofCare Get(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.PatientId == patientId);
        }

        //public List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");

        //    if (status > 0)
        //    {
        //        return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.Status == status && p.IsDeprecated == false).ToList();
        //    }
        //    return new List<PlanofCare>();
        //}

        //public List<PlanofCare> GetByPhysicianId(List<Guid> physicianIdentifiers, int status)
        //{
        //    var ids = new StringBuilder();

        //    physicianIdentifiers.ForEach(i =>
        //    {
        //        ids.AppendFormat("'{0}',", i.ToString());
        //    });

        //    var query = new QueryBuilder("select * from `planofcares` poc")
        //        .Where(string.Format("poc.Status = {0}", status))
        //        .And("poc.IsDeprecated = 0")
        //        .And(string.Format("poc.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

        //    return new FluentCommand<PlanofCare>(query.Build())
        //        .SetConnection(connectionStringName)
        //        .AsList();

        //}

        public List<PlanofCare> GetPlanofCares(Guid agencyId, string orderIds)
        {

            var script = string.Format(@"SELECT
                poc.Id as Id,
                poc.EpisodeId as EpisodeId,
                poc.OrderNumber as OrderNumber, 
                pr.Id as PatientId ,
                pr.FirstName as FirstName,
                pr.LastName as LastName, 
                pr.MiddleInitial as MiddleInitial, 
                poc.Status as Status ,
                poc.Created as Created , 
                poc.ReceivedDate as ReceivedDate,
                poc.SentDate as SentDate, 
                poc.PhysicianData as PhysicianData ,
                poc.PhysicianId as PhysicianId,
                poc.PhysicianSignatureDate as PhysicianSignatureDate 
                    FROM 
                        planofcares poc INNER JOIN patientprofiles pr ON poc.PatientId = pr.Id 
                            WHERE 
                                poc.AgencyId = @agencyid AND
                                pr.Status IN (1,2) AND 
                                poc.IsDeprecated = 0 AND
                                poc.Id IN ( {0} ) ", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDouble("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetPatientPlanofCares(Guid agencyId, Guid patientId, string orderIds)
        {
            var script = string.Format(@"SELECT
                poc.Id as Id,
                poc.EpisodeId as EpisodeId,
                poc.OrderNumber as OrderNumber,
                poc.Status as Status , 
                poc.Created as Created , 
                poc.ReceivedDate as ReceivedDate,
                poc.SentDate as SentDate, 
                poc.PhysicianData as PhysicianData ,
                poc.PhysicianId as PhysicianId  
                    FROM
                        planofcares poc
                            WHERE
                                poc.AgencyId = @agencyid AND
                                poc.PatientId= @patientId AND
                                poc.IsDeprecated = 0 AND
                                poc.Id IN ( {0} ) ", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = patientId,
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianData = reader.GetStringNullable("PhysicianData")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetPendingSignaturePlanofCares(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                            poc..Id as Id,
                            poc.EpisodeId as EpisodeId,
                            poc.OrderNumber as OrderNumber, 
                            pr.Id as PatientId ,
                            pr.FirstName as FirstName,
                            pr.LastName as LastName,
                            poc.Status as Status ,
                            poc.Created as Created , 
                            poc.ReceivedDate as ReceivedDate,
                            poc.SentDate as SentDate, 
                            poc.PhysicianData as PhysicianData ,
                            poc.PhysicianId as PhysicianId , 
                            poc.PhysicianSignatureDate as PhysicianSignatureDate 
                                FROM 
                                    planofcares poc INNER JOIN patientprofiles pr ON poc.PatientId = pr.Id 
                                        WHERE
                                            poc.AgencyId = @agencyid AND 
                                            poc.Status IN (1,2) AND
                                            poc.IsDeprecated = 0 AND
                                            poc.Id IN ( {0} ) AND 
                                            poc.Status IN (130,145 ) ", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

                })
                .AsList();
            }
            return list;
        }

        //public List<PlanofCare> GetPlanOfCareOrders(Guid agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.IsDeprecated == false).ToList();
        //}

        public List<PlanofCare> GetPatientPlanOfCare(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.IsDeprecated == false).ToList();
        }

        public bool MarkAsDeleted(Guid agencyId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE planofcares poc set poc.IsDeprecated = {0}, poc.Modified = @modified WHERE poc.AgencyId = @agencyid AND poc.PatientId = @patientid AND poc.Id = @eventid", isDeprecated ? 1 : 0);
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("eventid", eventId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var script = "UPDATE planofcares poc set poc.UserId = @employeeId, poc.Modified = @modified WHERE poc.AgencyId = @agencyid AND poc.Id = @eventid";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("employeeId", employeeId)
                    .AddGuid("eventid", eventId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<PlanofCare> GetPlanofCaresByStatus(Guid agencyId, int status, string orderIds)  //GetPlanofCaresStandAloneByStatus
        {
            var script = string.Format(@"SELECT 
                                    poc.Id as Id, 
                                    poc.EpisodeId as EpisodeId,
                                    poc.OrderNumber as OrderNumber, 
                                    pr.Id as PatientId ,
                                    pr.FirstName as FirstName,
                                    pr.LastName as LastName, 
                                    pr.MiddleInitial as MiddleInitial, 
                                    poc.Status as Status ,
                                    poc.Created as Created ,
                                    poc.ReceivedDate as ReceivedDate, 
                                    poc.SentDate as SentDate, 
                                    poc.PhysicianData as PhysicianData ,
                                    poc.PhysicianId as PhysicianId ,
                                    poc.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            planofcares poc INNER JOIN patientprofiles pr ON poc.PatientId = pr.Id 
                                                WHERE
                                                    poc.AgencyId = @agencyid AND
                                                    pr.Status IN (1,2) AND 
                                                    poc.IsDeprecated = 0 AND 
                                                    poc.Id IN ( {0} ) AND
                                                    poc.Status = @status", orderIds);
            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid assessmentId, string assessmentType)
        {
            return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.AssessmentId == assessmentId && p.AssessmentType == assessmentType && p.IsDeprecated == false);
        }

        /// <summary>
        /// WARNING: THIS METHOD WILL COMPLETLY REMOVE THE PLAN OF CARE FROM THE DATABASE
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool RemoveFully(Guid Id)
        {
            var result = false;
            try
            {
                result = database.Delete<PlanofCare>(Id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        #endregion


        public PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid episodeId, Guid assessmentId)
        {
            return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.EpisodeId == episodeId && p.AssessmentId == assessmentId  && p.IsDeprecated == false);
        }

    }
}
