﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class MultiDocumentAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;

        public bool RemoveModel<T>(Guid Id) where T : class, new()
        {
            var result = false;
            try
            {
                result = database.Delete<T>(Id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateEntityForReassigningUser(Guid agencyId, Guid userId, string script)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateEntityForReassigningUser(Guid agencyId, Guid patientId, Guid userId, string script)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("userid", userId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateEntityForDelete(Guid agencyId, Guid patientId, string script)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }
    }
}
