﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.Core.Enums;
using Axxess.Core;
using Axxess.AgencyManagement.Entities.Enums;
using SubSonic.Repository;

namespace Axxess.AgencyManagement.Repositories
{
    public abstract class EpisodeAbstract<E> where E : CarePeriod, new()
    {
        protected SimpleRepository database;
        protected string connectionStringName;
        protected AgencyServices Service { get; set; }
        #region PatientEpisode

        public bool AddEpisode(E patientEpisode)
        {
            var result = false;
            if (patientEpisode != null)
            {
                try
                {
                    patientEpisode.IsActive = true;
                    patientEpisode.Created = DateTime.Now;
                    patientEpisode.Modified = DateTime.Now;
                    database.Add<E>(patientEpisode);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateEpisode(E episode)
        {
            var result = false;
            try
            {
                if (episode != null)
                {
                    database.Update<E>(episode);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateEpisodesForDischarge(Guid agencyId, Guid patientId, E episodeContainDischargeDate, List<E> episodesAfterDischargeDate)
        {
            var result = false;
            try
            {
                var script = string.Empty;
                var count = 0;
                if (episodeContainDischargeDate != null)
                {
                    count++;
                    script += @"UPDATE patientepisodes pe set pe.IsLinkedToDischarge = 1, pe.EndDate = @enddate, pe.Modified = @modified  WHERE pe.AgencyId = @agencyid AND pe.PatientId = @patientid AND pe.Id = @episodeId;";

                }
                if (episodesAfterDischargeDate != null && episodesAfterDischargeDate.Count > 0)
                {
                    count++;
                    script += string.Format(@"UPDATE patientepisodes pe set pe.IsDischarged = 1, pe.Modified = @modified  WHERE pe.AgencyId = @agencyid AND pe.PatientId = @patientid AND pe.Id IN ({0});", episodesAfterDischargeDate.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                }
                if (script.IsNotNullOrEmpty())
                {
                    var resultCount = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        resultCount = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeId", episodeContainDischargeDate != null ? episodeContainDischargeDate.Id : Guid.Empty)
                        .AddDateTime("enddate", episodeContainDischargeDate != null ? episodeContainDischargeDate.EndDate : DateTime.Now.Date)
                        .AddDateTime("modified", DateTime.Now.Date)
                        .AsNonQuery();
                    }
                    return resultCount >= count;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEpisode(Guid agencyId, Patient patient, out E episodeDeleted)
        {
            try
            {
                var episode = database.Single<E>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.StartDate <= DateTime.Now && e.EndDate > DateTime.Now);
                episodeDeleted = episode;
                if (episode != null)
                {
                   return  database.Delete<E>(episode) >0;
                }
            }
            catch (Exception)
            {
                episodeDeleted = null;
                return false;
            }
            return true;
        }

        public bool RemoveEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM patientepisodes pe WHERE pe.AgencyId = @agencyid AND pe.PatientId = @patientid AND pe.Id = @id ");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", episodeId).AsNonQuery();

                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public E GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<E>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
        }

        public E GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT 
                            p.FirstName,
                            p.PatientIdNumber ,
                            p.LastName,
                            p.MiddleInitial,
                            pe.EndDate,
                            pe.StartDate 
                                FROM 
                                    patientepisodes pe INNER JOIN agencymanagement.patients p ON pe.PatientId = p.Id 
                                        WHERE
                                            pe.IsDischarged = 0 AND
                                            pe.IsActive = 1 AND 
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientId AND
                                            pe.Id = @episodeId ";

            var patientEpisode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeId", episodeId)
                .SetMap(reader => new E
                {
                    Id = episodeId,
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    DisplayName = string.Format("{0} ,{1} {2}", reader.GetStringNullable("LastName"), reader.GetStringNullable("FirstName"), reader.GetStringNullable("MiddleInitial").ToInitial())

                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public E GetNextEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.StartDate) > DATE(@date)  ORDER BY pe.StartDate ASC limit 0,1";
            var dateRange = new E();

            using (var cmd = new FluentCommand<E>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Comments = reader.GetStringNullable("Comments")
                })
                .AsSingle();
            }
            return dateRange;
        }

        public E GetNextEpisodeByStartDate(Guid agencyId, Guid patientId, DateTime startDate)
        {
            var script = @"SELECT
                            pe.Id, 
                            pe.EndDate,
                            pe.StartDate, 
                            pe.Comments 
                                FROM
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid  AND 
                                            pe.PatientId = @patientid AND 
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.StartDate) = DATE(@startdate) 
                                                 ORDER BY DATE(pe.StartDate) ASC LIMIT 1";

            var episode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                episode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return episode;
        }

        public E GetPreviousEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) < DATE(@date)  ORDER BY pe.StartDate DESC limit 0,1";
            var patientEpisode = new E();

            using (var cmd = new FluentCommand<E>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Comments = reader.GetStringNullable("Comments")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public E GetPreviousEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate)
        {
            var script = @"SELECT 
                            pe.Id, 
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) = DATE(@enddate) 
                                                ORDER BY DATE(pe.EndDate) DESC LIMIT 1";

            var episode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                episode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return episode;
        }

        public E GetCurrentEpisodeLean(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT 
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.IsActive = 1 AND 
                                                pe.IsDischarged = 0 AND
                                                pe.PatientId = @patientid AND
                                                DATE(curdate()) BETWEEN DATE(pe.StartDate) AND DATE(pe.EndDate) limit 0,1";

            var currentEpisode = new E();

            using (var cmd = new FluentCommand<E>(script))
            {
                currentEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Comments = reader.GetStringNullable("Comments")
                })
                .AsSingle();
            }

            return currentEpisode;
        }

        public E GetCurrentEpisodeLeanNoPatientInfo(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT 
                        pe.Id, 
                        pe.EndDate, 
                        pe.StartDate,
                        pe.Comments 
                            FROM
                                patientepisodes pe 
                                        WHERE 
                                            pe.AgencyId = @agencyid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            pe.PatientId = @patientid AND 
                                            curdate() BETWEEN pe.StartDate AND pe.EndDate limit 0,1";

            var currentEpisode = new E();

            using (var cmd = new FluentCommand<E>(script))
            {
                currentEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Comments = reader.GetStringNullable("Comments"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }

            return currentEpisode;
        }

        public E GetLastOrCurrentEpisode(Guid agencyId, Guid patientId, DateTime date)
        {
            var patientEpisode = new E();
            var script = @"SELECT 
                                pe.Id as Id ,
                                pe.EndDate as EndDate ,
                                pe.StartDate as StartDate ,
                                pe.Comments 
                                    FROM
                                        patientepisodes pe
                                                WHERE 
                                                    pe.AgencyId = @agencyid  AND
                                                    pe.PatientId = @patientid AND 
                                                    pe.IsActive = 1  AND 
                                                    pe.IsDischarged = 0 AND 
                                                    DATE(pe.StartDate) <= DATE(@date) 
                                                    ORDER BY
                                                        pe.StartDate DESC LIMIT 1 ";
            using (var cmd = new FluentCommand<E>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Comments = reader.GetStringNullable("Comments")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public E GetPatientEpisodeLean(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT
                        pe.EndDate,
                        pe.StartDate, 
                        pe.Comments 
                            FROM 
                                patientepisodes pe 
                                    WHERE 
                                        pe.AgencyId = @agencyid AND
                                        pe.PatientId = @patientid AND
                                        pe.Id = @episodeid AND 
                                        pe.IsActive = 1 AND
                                        pe.IsDischarged = 0 ";

            var patientEpisode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("episodeid", episodeId)
                    .SetMap(reader => new E
                    {
                        Id = episodeId,
                        PatientId = patientId,
                        EndDate = reader.GetDateTime("EndDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        Comments = reader.GetStringNullable("Comments")
                    })
                    .AsSingle();
            }
            return patientEpisode;
        }

        public E GetLastEpisodeLean(Guid agencyId, Guid patientId)
        {
            E newEpisodeDate = null;
            if (!patientId.IsEmpty())
            {
                var script = @"SELECT 
                                pe.Id as Id ,
                                pe.EndDate as EndDate ,
                                pe.StartDate as StartDate ,
                                pe.StartOfCareDate as StartOfCareDate ,
                                pe.AdmissionId as AdmissionId  
                                    FROM
                                        patientepisodes pe
                                                WHERE 
                                                    pe.AgencyId = @agencyid  AND
                                                    pe.PatientId = @patientid AND 
                                                    pe.IsActive = 1  AND 
                                                    pe.IsDischarged = 0
                                                    ORDER BY
                                                        pe.StartDate DESC LIMIT 1 ";
                using (var cmd = new FluentCommand<E>(script))
                {
                    newEpisodeDate = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new E
                    {
                        PatientId = patientId,
                        EndDate = reader.GetDateTime("EndDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        AdmissionId = reader.GetGuid("AdmissionId")

                    })
                    .AsSingle();
                }
            }
            return newEpisodeDate;
        }

        public List<E> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            DATE(pe.StartDate) > DATE(@date)";
            var list = new List<E>();

            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", dischargeDate.Date)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list;
        }

        public List<E> GetPatientAllEpisodes(Guid agencyId, Guid patientId)
        {
            return database.Find<E>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).ToList();
        }

        public List<E> GetPatientActiveEpisodesLean(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND 
                                                pe.PatientId = @patientid";
            var list = new List<E>();
            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Comments = reader.GetStringNullable("Comments")
                })
                .AsList();
            }
            return list;
        }

        public List<E> GetPatientActiveEpisodesLeanByDateRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            //DateRange pe = new DateRange() { StartDate = new DateTime(2013, 04, 02), EndDate = new DateTime(2013, 5, 31) };
            //bool btw = ((pe.StartDate >= startDate && pe.StartDate >= endDate) || (pe.EndDate >= startDate && pe.EndDate >= endDate)) || 
            //    ((pe.StartDate >= startDate && pe.StartDate >= endDate) && (pe.EndDate >= startDate && pe.EndDate >= endDate));

            var script = @"SELECT 
                            pe.Id,
                            pe.EndDate,
                            pe.StartDate, 
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            pe.PatientId = @patientid AND 
                                            DATE(pe.StartDate) <= DATE(@enddate) AND
                                            DATE(pe.EndDate) >= DATE(@startdate)";//(DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND DATE(@enddate)  between DATE(pe.StartDate) and DATE(pe.EndDate)) OR ( DATE(pe.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(pe.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<E>();
            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Comments = reader.GetStringNullable("Comments"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.OrderBy(e => e.StartDate).ToList();
        }

        public List<E> GetPatientsActiveEpisodesLeanByDateRange(Guid agencyId, List<Guid> patientIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                            pe.Id,
                            pe.PatientId,
                            pe.EndDate,
                            pe.StartDate, 
                            pe.Comments 
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            pe.PatientId IN ( @patientids ) AND 
                                            pe.StartDate <= DATE(@enddate) AND
                                            pe.EndDate >= DATE(@startdate)");

            //(DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) OR 
            //                                (DATE(@startdate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND DATE(@enddate) between DATE(pe.StartDate) and DATE(pe.EndDate)) OR 
            //                                (DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) AND DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate)))

            var list = new List<E>();
            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddString("patientids", patientIds.ToCommaSeperatedNoQuote())
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    Comments = reader.GetStringNullable("Comments"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list;
        }

//        public List<PatientSelection> GetPatientEpisodeData(Guid agencyId, Guid branchId, Guid userId, byte statusId)
//        {
//            var branchScript = !branchId.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty;
//            var script = string.Format(@"SELECT 
//                                            patients.Id as PatientId,
//                                            patients.FirstName as FirstName, 
//                                            patients.LastName as LastName, 
//                                            patients.MiddleInitial as MiddleInitial, 
//                                            patients.PatientIdNumber as PatientIdNumber,
//                                            patients.Status as Status, 
//                                            patients.DOB as DOB, 
//                                            patients.PhoneHome as PhoneHome
//                                                    FROM patientepisodes 
//                                                            INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                                                    WHERE patientepisodes.AgencyId = @agencyid AND 
//                                                                           patients.IsDeprecated = 0 AND 
//                                                                           patients.Status = @statusid {0} AND 
//                                                                           patientepisodes.IsActive = 1 AND 
//                                                                           patientepisodes.IsDischarged = 0 AND 
//                                                                           EXISTS (SELECT * FROM scheduleevents WHERE scheduleevents.EpisodeId = patientepisodes.Id AND scheduleevents.UserId = @userid  AND scheduleevents.IsDeprecated = 0 AND DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) ) ORDER BY LastName ASC ;"
//                , branchScript);


//            var list = new List<PatientSelection>();
//            using (var cmd = new FluentCommand<PatientSelection>(script))
//            {
//                list = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("userid", userId)
//                .AddInt("statusid", statusId)
//                .SetMap(reader => new PatientSelection
//                {
//                    Id = reader.GetGuid("PatientId"),
//                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                    MI = reader.GetStringNullable("MiddleInitial").ToUpperCase(),
//                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
//                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
//                    DOB = reader.GetDateTime("DOB"),
//                    PhoneNumber = reader.GetStringNullable("PhoneHome")
//                })
//                .AsList();
//            }
//            return list;
//        }

//        public List<PatientSelection> GetPatientEpisodeData(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId)
//        {
//            var insurance = string.Empty;
//            var list = new List<PatientSelection>();
//            if (insuranceId < 0)
//            {
//                return list;
//            }
//            else if (insuranceId == 0)
//            {
//                if (!branchId.IsEmpty())
//                {
//                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
//                    if (location != null && location.IsLocationStandAlone)
//                    {
//                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
//                        {
//                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", location.Payor);
//                        }
//                        else
//                        {
//                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
//                        }
//                    }
//                    else
//                    {
//                        var agency = database.Single<Agency>(l => l.Id == agencyId);
//                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
//                        {
//                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance => 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", agency.Payor);
//                        }
//                        else
//                        {
//                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
//                        }
//                    }
//                }
//            }
//            else
//            {
//                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} ) ", insuranceId);
//            }


//            var script = string.Format(@"SELECT 
//                                            patients.Id as PatientId,
//                                            patients.FirstName as FirstName, 
//                                            patients.LastName as LastName, 
//                                            patients.MiddleInitial as MiddleInitial, 
//                                            patients.PatientIdNumber as PatientIdNumber,
//                                            patients.Status as Status, 
//                                            patients.DOB as DOB, 
//                                            patients.PhoneHome as PhoneHome
//                                                    FROM patientepisodes 
//                                                            INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
//                                                                    WHERE patientepisodes.AgencyId = @agencyid AND 
//                                                                           patients.IsDeprecated = 0 AND 
//                                                                           patients.Status = @statusid {0} {1} AND 
//                                                                           patientepisodes.IsActive = 1 AND 
//                                                                           patientepisodes.IsDischarged = 0 AND 
//                                                                           EXISTS (SELECT * FROM scheduleevents WHERE scheduleevents.EpisodeId = patientepisodes.Id AND scheduleevents.UserId = @userid  AND scheduleevents.IsDeprecated = 0 AND DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) )  ORDER BY LastName ASC ;", insurance, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty);


//            using (var cmd = new FluentCommand<PatientSelection>(script))
//            {
//                list = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("userid", userId)
//                .AddInt("statusid", statusId)
//                .SetMap(reader => new PatientSelection
//                {
//                    Id = reader.GetGuid("PatientId"),
//                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                    MI = reader.GetStringNullable("MiddleInitial").ToUpperCase(),
//                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
//                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
//                    DOB = reader.GetDateTime("DOB"),
//                    PhoneNumber = reader.GetStringNullable("PhoneHome")
//                })
//                .AsList();
//            }
//            return list;
          
//        }

        public List<E> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<E>();
            var script = @"SELECT 
                            pe.Id as EpisodeId, 
                            pe.EndDate as EndDate,
                            pe.StartDate as StartDate
                                FROM
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND
                                                pe.PatientId = @patientid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND
                                                DATE(pe.StartDate) <= DATE(@enddate) AND
                                                DATE(pe.EndDate) >= DATE(@startdate)";
                                                //(pe.StartDate BETWEEN @startdate AND @enddate || pe.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN pe.StartDate AND pe.EndDate || @enddate BETWEEN pe.StartDate AND pe.EndDate || (pe.StartDate BETWEEN @startdate AND @enddate AND pe.EndDate BETWEEN @startdate AND @enddate)) 
                                                //    ORDER BY pe.StartDate ASC";

            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.OrderBy(l=>l.StartDate).ToList();
        }

        public DateRange GetCurrentEpisodeDateRange(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(@date) BETWEEN DATE(pe.StartDate) AND DATE(pe.EndDate) limit 0,1";
            var dateRange = new DateRange();

            using (var cmd = new FluentCommand<DateRange>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date",DateTime.Now)

                .SetMap(reader => new DateRange
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return dateRange;
        }

        public DateRange GetEpisodeDateRange(Guid agencyId, Guid patientId, Guid id)
        {
            var script = @"SELECT
                            pe.StartDate,
                            pe.EndDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.Id = @id  limit 0,1";
            var dateRange = new DateRange();
            using (var cmd = new FluentCommand<DateRange>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", id)
                .SetMap(reader => new DateRange
                {
                    Id = id.ToString(),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return dateRange;
        }

        public DateRange GetNextEpisodeDateRange(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            pe.DATE(StartDate) >= DATE(@date)  ORDER BY pe.StartDate ASC limit 0,1";
            var dateRange = new DateRange();

            using (var cmd = new FluentCommand<DateRange>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", DateTime.Now)
                .SetMap(reader => new DateRange
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return dateRange;
        }

        public DateRange GetPreviousEpisodeDateRange(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) < DATE(@date)  ORDER BY pe.StartDate DESC limit 0 , 1";
            var dateRange = new DateRange();

            using (var cmd = new FluentCommand<DateRange>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", DateTime.Now)
                .SetMap(reader => new DateRange
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return dateRange;
        }

        public List<EpisodeLean> GetPatientActiveEpisodesLeanWithNoDetail(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT  
                                pe.Id,
                                pe.IsActive,
                                pe.IsDischarged,
                                pe.EndDate, 
                                pe.StartDate
                                    FROM 
                                        patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND
                                                pe.PatientId = @patientId AND
                                                pe.IsDischarged = 0 AND 
                                                pe.IsActive = 1";

            return new FluentCommand<EpisodeLean>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
        }

        public List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT  
                                pe.Id,
                                pe.IsActive,
                                pe.IsDischarged,
                                pe.EndDate, 
                                pe.StartDate
                                    FROM 
                                        patientepisodes pe 
                                            WHERE 
                                                pe.AgencyId = @agencyid AND
                                                pe.PatientId = @patientId AND
                                                (pe.IsDischarged = 1 || pe.IsActive = 0)";

            return new FluentCommand<EpisodeLean>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
        }

        public List<EpisodeLean> GetPatientEpisodesByIds(Guid agencyId, Guid patientId, List<Guid> episodeIds)
        {
            var script =string.Format(@"SELECT  
                                pe.Id,
                                pe.IsActive,
                                pe.IsDischarged,
                                pe.EndDate, 
                                pe.StartDate
                                    FROM 
                                        patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND
                                                pe.PatientId = @patientId AND
                                                pe.Id IN ({0}) ", episodeIds.ToCommaSeperatedList());

            return new FluentCommand<EpisodeLean>(script)
                .SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
        }

        public bool IsEpisodeExist(Guid agencyId, Guid episodeId)
        {

            var script = @"SELECT 
                             1
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.Id = @episodeid  limit 1";

            var count = 0;
            using (var cmd = new FluentCommand<E>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("episodeid", episodeId)
                .AsScalar();
            }
            return count > 0;

            //var episode = database.Single<E>(e => e.AgencyId == agencyId && e.Id == episodeId);
            //if (episode != null)
            //{
            //    return true;
            //}
            //return false;
        }

        public bool IsEpisodeExist(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT 
                             1
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            pe.PatientId = @patientid AND
                                            DATE(pe.StartDate) <= DATE(@enddate) AND
                                            DATE(pe.EndDate) >= DATE(@startdate) limit 1"; 
                                           // (DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate))";

            var count = 0;
            using (var cmd = new FluentCommand<E>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AsScalar();
            }
            return count > 0;
        }

        public bool IsEpisodeExist(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, Guid excludedEpisodeIdOptional)
        {
            var script = string.Format(@"SELECT 
                              1
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid {0} AND
                                            pe.PatientId = @patientid AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            DATE(pe.StartDate) <= DATE(@enddate) AND
                                            DATE(pe.EndDate) >= DATE(@startdate) limit 1", !excludedEpisodeIdOptional.IsEmpty() ? " AND  pe.Id != @id " : string.Empty);

                                            //(DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate))";

            var count = 0;
            using (var cmd = new FluentCommand<E>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("id",excludedEpisodeIdOptional)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AsScalar();
            }
            return count > 0;
        }


        public bool ActivateEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            string script = @"UPDATE patientepisodes pe set pe.IsDischarged = 0, pe.IsActive = 1, pe.Modified = @modified WHERE pe.AgencyId = @agencyid AND pe.PatientId = @patientid AND pe.Id = @episodeId;";
            var resultCount = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                resultCount = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("modified", DateTime.Now.Date)
                .AsNonQuery();
            }
            return resultCount > 0;
        }

        public bool DeactivateEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            string script = @"UPDATE patientepisodes pe set pe.IsActive = 0, pe.Modified = @modified WHERE pe.AgencyId = @agencyid AND pe.PatientId = @patientid AND pe.Id = @episodeId;";
            var resultCount = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                resultCount = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("modified", DateTime.Now.Date)
                .AsNonQuery();
            }
            return resultCount > 0;
        }

        public List<E> GetPatientEpisodesLean(Guid agencyId, Guid patientId, bool? isActive)
        {
            string activeScript = "";
            if(isActive.HasValue)
            {
                if (isActive.Value)
                {
                    activeScript = "AND pe.IsActive = 1 AND pe.IsDischarged = 0";
                }
                else
                {
                    activeScript = "AND (pe.IsActive = 0 OR pe.IsDischarged = 1)";
                }
            }
            var script = string.Format(@"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.IsActive,
                            pe.IsDischarged
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.PatientId = @patientid
                                                {0}", activeScript);
            var list = new List<E>();
            using (var cmd = new FluentCommand<E>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged")
                })
                .AsList();
            }
            return list;
        }

        public E GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT
                            pe.Id as Id ,
                            pe.PatientId as PatientId ,
                            pe.StartDate as StartDate ,
                            pe.EndDate as EndDate ,
                            pad.StartOfCareDate as StartOfCareDate 
                                FROM 
                                    patientepisodes pe
                                        INNER JOIN patientadmissiondates pad ON pe.AdmissionId = pad.Id 
                                            WHERE
                                                pe.AgencyId = @agencyid AND
                                                pad.AgencyId = @agencyid AND
                                                pe.Id = @episodeid AND
                                                pe.PatientId = @patientid LIMIT 1 ";

            var patientEpisode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new E
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        /// <summary>
        /// Gets the admissions that belong to a patient and also whether or not it has any episodes
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public List<AdmissionEpisode> GetPatientAdmissonPeriods(Guid agencyId, Guid patientId)
        {
            var list = new List<AdmissionEpisode>();
            var script = string.Format(@"SELECT 
                pad.Id as Id, 
                pad.PatientId as PatientId ,
                pad.StartOfCareDate as StartOfCareDate,
                pad.DischargedDate as DischargedDate, 
                pad.Created as Created , 
                pad.Modified as Modified ,
                (pp.AdmissionId is not null) as IsCurrent,
                EXISTS(select id from patientepisodes pe where pad.PatientId = pe.PatientId AND pad.Id = pe.AdmissionId AND pad.AgencyId = @agencyid AND pad.IsDeprecated = 0 AND pe.IsActive = 1 AND pe.IsDischarged = 0) as HasEpisodes
                    FROM 
                        patientadmissiondates pad
                            LEFT JOIN patientprofiles pp on pad.Id = pp.AdmissionId AND pp.AgencyId = @agencyid && pp.Id = @patientid
                            WHERE 
                                pad.PatientId = @patientid AND 
                                pad.AgencyId = @agencyid AND
                                pad.IsActive = 1");

            using (var cmd = new FluentCommand<AdmissionEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AdmissionEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartOfCareDate = reader.GetDateTimeWithMin("StartOfCareDate"),
                    DischargedDate = reader.GetDateTimeWithMin("DischargedDate"),
                    Created = reader.GetDateTimeWithMin("Created"),
                    Modified = reader.GetDateTimeWithMin("Modified"),
                    HasDetails = reader.GetBoolean("HasEpisodes"),
                    IsCurrent = reader.GetBoolean("IsCurrent")
                }).AsEnumerable().OrderByDescending(m => m.StartOfCareDate).ThenByDescending(m => m.DischargedDate).ToList();
            }
            return list;
        }

        /// <summary>
        /// Gets the episodes that belong to a patient admission
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <param name="admissionId"></param>
        /// <returns></returns>
        public List<EpisodeLean> GetPatientAdmissonPeriodEpisodes(Guid agencyId, Guid patientId, Guid admissionId)
        {
            List<EpisodeLean> list;
            var script = string.Format(@"SELECT 
                Id,
                StartDate,
                EndDate
                    FROM 
                        patientepisodes
                            WHERE 
                                PatientId = @patientid AND 
                                AgencyId = @agencyid AND
                                AdmissionId = @admissionid AND
                                IsDischarged = 0 AND
                                IsActive = 1");

            using (var cmd = new FluentCommand<EpisodeLean>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("admissionid", admissionId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    StartDate = reader.GetDateTimeWithMin("StartDate"),
                    EndDate = reader.GetDateTimeWithMin("EndDate"),
                }).AsList();
            }
            return list;
        }

//        public List<AdmissionEpisode> PatientAdmissonPeriods(Guid agencyId, Guid patientId)
//        {
//            var list = new List<AdmissionEpisode>();
//            var script = string.Format(@"SELECT 
//                pad.Id as Id, 
//                pad.PatientId as PatientId ,
//                pad.StartOfCareDate as StartOfCareDate,
//                pad.DischargedDate as DischargedDate, 
//                pad.Created as Created , 
//                pad.Modified as Modified ,
//                pe.Id as EpisodeId ,
//                pe.EndDate as EndDate, 
//                pe.StartDate as StartDate 
//                    FROM 
//                        patientadmissiondates pad
//                            LEFT JOIN 
//                                patientepisodes pe ON ( pad.PatientId = pe.PatientId AND pad.Id = pe.AdmissionId AND pad.IsDeprecated = 0 AND pe.IsActive = 1  AND pe.IsDischarged = 0 ) 
//                                    WHERE 
//                                        pad.PatientId = @patientid AND 
//                                        pad.AgencyId = @agencyid   AND
//                                        pad.IsActive = 1   ");

//            using (var cmd = new FluentCommand<AdmissionEpisode>(script))
//            {
//                list = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .SetMap(reader => new AdmissionEpisode
//                {
//                    Id = reader.GetGuid("Id"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuidIncludeEmpty("EpisodeId"),
//                    StartOfCareDate = reader.GetDateTimeWithMin("StartOfCareDate"),
//                    DischargedDate = reader.GetDateTimeWithMin("DischargedDate"),
//                    EpisodeStartDate = reader.GetDateTimeWithMin("StartDate"),
//                    EpisodeEndDate = reader.GetDateTimeWithMin("EndDate"),
//                    Created = reader.GetDateTimeWithMin("Created"),
//                    Modified = reader.GetDateTimeWithMin("Modified"),
//                }).AsList();
//            }
//            return list;
//        }

        public E GetEpisodeFromDate(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id as Id,
                            pe.StartDate, 
                            pe.EndDate
                                FROM 
                                    patientepisodes pe
                                            WHERE
                                                pe.AgencyId = @agencyid AND
                                                pe.IsActive = 1 AND 
                                                pe.IsDischarged = 0 AND
                                                pe.PatientId = @patientid AND
                                                DATE(@date) BETWEEN DATE(pe.StartDate) AND DATE(pe.EndDate)
                                                    LIMIT 1 ";
            E episode = new E();
            using (var cmd = new FluentCommand<E>(script))
            {
                episode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new E()
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return episode;
        }

        public List<BirthdayWidget> GetUserBirthDayReport(Guid agencyId, Guid userId, byte statusId, int month)
        {
            var list = new List<BirthdayWidget>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                var script = string.Format(@"SELECT 
                                            p.Id as PatientId,
                                            p.FirstName as FirstName, 
                                            p.LastName as LastName, 
                                            p.MiddleInitial as MiddleInitial, 
                                            p.Status as Status, 
                                            p.DOB as DOB, 
                                            p.PhoneHome as PhoneHome
                                                    FROM patientepisodes pe
                                                            INNER JOIN agencymanagement.patients p ON pe.PatientId = p.Id 
                                                                    WHERE 
                                                                           pe.AgencyId = @agencyid AND 
                                                                           p.{0}Status = @statusid AND 
                                                                           MONTH(p.DOB)= @month AND
                                                                           pe.IsActive = 1 AND 
                                                                           pe.IsDischarged = 0 AND 
                                                                           EXISTS (SELECT * FROM scheduletasks st WHERE st.EpisodeId = pe.Id AND st.UserId = @userid  AND st.IsDeprecated = 0 AND DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) ) ORDER BY p.LastName ASC ;",

                    this.Service.ToString());



                using (var cmd = new FluentCommand<BirthdayWidget>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("userid", userId)
                    .AddInt("statusid", statusId)
                    .AddInt("month", month)
                    .SetMap(reader => new BirthdayWidget
                    {
                        Id = reader.GetGuid("PatientId"),
                        Date = reader.GetDateTime("DOB"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                        Name = string.Concat(reader.GetStringNullable("FirstName").ToUpperCase(), " ", reader.GetStringNullable("LastName").ToUpperCase())
                    })
                    .AsList();
                }
            }
            return list;
        }


        #endregion
    }
}
