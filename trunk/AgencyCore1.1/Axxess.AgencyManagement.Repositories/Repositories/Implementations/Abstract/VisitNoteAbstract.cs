﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SubSonic.DataProviders;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;


    public abstract class VisitNoteAbstract
    {
        protected SimpleRepository database;
        protected string connectionStringName;
        protected AgencyServices Service { get; set; }

        #region PatientVisitNote

        public bool AddMultipleVisitNote(List<PatientVisitNote> patientVisitNotes)
        {
            if (patientVisitNotes != null && patientVisitNotes.Count > 0)
            {
                try
                {
                    database.AddMany<PatientVisitNote>(patientVisitNotes);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateVisitNote(PatientVisitNote patientVisitNote) 
        {
            var result = false;
            try
            {
                if (patientVisitNote != null)
                {
                    patientVisitNote.Modified = DateTime.Now;
                    result = database.Update<PatientVisitNote>(patientVisitNote) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool UpdateVisitNoteWoundCare(Guid agencyId, Guid patientId, Guid eventId, bool isWoundCare)
        {
            bool result = false;
            try
            {
                var query = new SubSonic.Query.Update<PatientVisitNote>(ProviderFactory.GetProvider(connectionStringName))
                    .Set(s => s.IsWoundCare == isWoundCare)
                    .Where(w => w.AgencyId == agencyId && w.PatientId == patientId && w.Id == eventId);
                result = query.Execute() > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool MarkVisitNoteAsDeleted(Guid agencyId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            return EntityHelper.TogglePatientEntityDeprecation<PatientVisitNote>(eventId, patientId, agencyId, isDeprecated, connectionStringName);
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == noteId && p.PatientId == patientId);
        }

        public List<PatientVisitNote> GetVisitNotesByDisciplineTask(Guid agencyId, Guid patientId, DisciplineTasks task)
        {
            return database.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.NoteType == task.ToString() && p.PatientId == patientId && p.IsDeprecated == false).OrderByDescending(p => p.Created).ToList();
        }

        public PatientVisitNote GetCarePlanBySelectedEpisodeAndDisciplineTask(Guid agencyId, Guid patientId, Guid selectedEpisodeId, DisciplineTasks task)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.SelectedEpisodeId == selectedEpisodeId && p.NoteType == task.ToString() && p.IsDeprecated == false);
        }

        public List<PatientVisitNote> GetEvalOrders(Guid agencyId, Guid patientId, string evalIds)
        {
            var script = string.Format(@"SELECT 
                        pv.Id as Id, 
                        pv.PhysicianId as PhysicianId,
                        pv.EpisodeId as EpisodeId,
                        pv.OrderNumber as OrderNumber,
                        pv.NoteType as NoteType, 
                        pv.Status as Status,
                        pv.ReceivedDate as ReceivedDate,
                        pv.SentDate as SentDate, 
                        pv.PhysicianSignatureDate as PhysicianSignatureDate ,
                        ap.Credentials as PhysicianCredentials ,
                        ap.FirstName as PhysicianFirstName,
                        ap.LastName as PhysicianLastName, 
                        p.Id as PatientId,
                        p.FirstName as FirstName,
                        p.LastName as LastName,
                        p.MiddleInitial as MiddleInitial
                            FROM 
                                patientvisitnotes pv
                                    INNER JOIN agencymanagement.patients p ON pv.PatientId = p.Id 
                                        INNER JOIN agencymanagement.agencyphysicians ap ON pv.PhysicianId = ap.Id
                                            WHERE 
                                                pv.AgencyId = @agencyid AND
                                                p.{0}Status IN (1,2) AND
                                                pv.IsDeprecated = 0 AND
                                                pv.Id IN ({1}) {2}", this.Service.ToString(), evalIds, patientId.IsEmpty() ? string.Empty : " AND pv.PatientId = @patientid");

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimal("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    NoteType = reader.GetStringNullable("NoteType"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientVisitNote> GetNoteSupplies(Guid agencyId, Guid patientId, List<Guid> noteIds)
        {
            var script = string.Format(@"SELECT
                        pv.Id as Id ,
                        pv.Supply as Supply 
                            FROM 
                                patientvisitnotes pv
                                        WHERE 
                                            pv.AgencyId = @agencyid AND
                                            pv.PatientId = @patientid AND
                                            pv.Id IN ({0}) ", noteIds.Select(d => string.Format("'{0}'", d)).ToArray().Join(","));

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    Supply = reader.GetStringNullable("Supply")
                })
                .AsList();

            }
            return list;
        }

        #endregion

        public bool RemoveManyModels<T>(Guid agencyId, Guid patientId, List<Guid> items)
        {
            return EntityHelper.RemoveMultiplePatientEntities<T>(agencyId, patientId, items, connectionStringName);
        }
    }
}
