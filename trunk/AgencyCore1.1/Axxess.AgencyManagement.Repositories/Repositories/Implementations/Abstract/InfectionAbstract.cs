﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public abstract class InfectionAbstract
    {

        protected SimpleRepository database;
        protected string connectionStringName;

        #region Infection Report

        public bool AddInfection(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    infection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                infection.Created = DateTime.Now;
                infection.Modified = DateTime.Now;

                database.Add<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool AddMultipleInfection(List<Infection> infections)
        {
            var result = false;
            if (infections != null && infections.Count > 0)
            {
                try
                {
                    database.AddMany<Infection>(infections);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteInfection(Guid Id)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = true;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool RemoveInfection(Guid agencyId, Guid patientId, Guid Id)
        {

            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM infections inf WHERE inf.AgencyId = @agencyid AND inf.PatientId = @patientid AND inf.Id = @id ");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id).AsNonQuery();

                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = isDeprecated;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                try
                {
                    infection.UserId = employeeId;
                    infection.Modified = DateTime.Now;
                    database.Update<Infection>(infection);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateInfection(Infection infection)
        {
            var result = false;
            var existingInfection = GetInfectionReport(infection.AgencyId, infection.Id);
            if (existingInfection != null)
            {
                existingInfection.Orders = infection.Orders;
                existingInfection.Treatment = infection.Treatment;
                existingInfection.MDNotified = infection.MDNotified;
                existingInfection.NewOrdersCreated = infection.NewOrdersCreated;
                existingInfection.TreatmentPrescribed = infection.TreatmentPrescribed;

                existingInfection.PhysicianId = infection.PhysicianId;
                existingInfection.InfectionDate = infection.InfectionDate;
                existingInfection.Status = infection.Status;
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    existingInfection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                existingInfection.InfectionTypeOther = infection.InfectionTypeOther;
                existingInfection.FollowUp = infection.FollowUp;
                existingInfection.SignatureDate = infection.SignatureDate;
                existingInfection.SignatureText = infection.SignatureText;
                existingInfection.Modified = DateTime.Now;

                database.Update<Infection>(existingInfection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfectionModal(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public Infection GetInfectionReport(Guid agencyId, Guid infectionId)
        {
            return database.Single<Infection>(i => i.AgencyId == agencyId && i.Id == infectionId);
        }

        public IList<Infection> GetInfections(Guid agencyId)
        {
            return database.Find<Infection>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        #endregion
    }
}
