﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    public class ReferralRepository : IReferralRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        //private readonly string databaseIp;

        public ReferralRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
            //this.databaseIp = databaseIp;
        }

        #endregion

        #region IReferralRepository Members

        public bool Add(Referral referral)
        {
            var result = false;
            try
            {
                if (referral != null)
                {
                     referral.Encode();
                    if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                    {
                        var physicianList = new List<Physician>();

                        int i = 0;
                        foreach (var guid in referral.AgencyPhysicians)
                        {
                            if (!guid.IsEmpty())
                            {
                                physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                                i++;
                            }
                        }
                        referral.Physicians = physicianList.ToXml();
                    }
                    referral.Status = (byte)ReferralStatus.Pending;
                    referral.Created = DateTime.Now;
                    referral.Modified = DateTime.Now;
                    database.Add<Referral>(referral);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool NonAdmitReferral(PendingPatient pending)
        {
            bool result = false;

            if (pending != null)
            {
                var referral = database.Single<Referral>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId && p.IsDeprecated == false);
                if (referral != null)
                {
                    referral.NonAdmissionDate = pending.NonAdmitDate;
                    referral.NonAdmissionReason = pending.Reason;
                    referral.Comments = pending.Comments;
                    referral.Status = (int)ReferralStatus.NonAdmission;
                    referral.Modified = DateTime.Now;

                   return database.Update<Referral>(referral)>0;
                }
            }
            return result;
        }

        public List<Referral> GetAll(Guid agencyId, ReferralStatus status, AgencyServices service)
        {
            var query = new QueryBuilder(@"SELECT 
                                Id,
                                ReferralDate, 
                                AdmissionSource, 
                                MedicareNumber,
                                LastName, 
                                FirstName, 
                                PhoneHome,

                                AddressLine1,
                                AddressLine2, 
                                AddressCity, 
                                AddressStateCode,
                                AddressZipCode,

                                DOB,
                                Gender,
                                EmailAddress,
                                IsDNR
                                    FROM 
                                        referrals")
                                           .Where("AgencyId = @agencyid")
                                           .And("DeprecatedServices & @serviceid != @serviceid")
                                           .And("Services & @serviceid = @serviceid")
                                           .And(string.Format("{0}Status = @status", service.ToString()))
                                           .OrderBy("LastName", true);

            var list = new List<Referral>();
            using (var cmd = new FluentCommand<Referral>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)status)
                 .AddInt("serviceid", (int)service)
                .SetMap(reader => new Referral
                {
                    Id = reader.GetGuid("Id"),
                    ReferralDate = reader.GetDateTime("ReferralDate"),
                    AdmissionSource = reader.GetInt("AdmissionSource"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    DOB = reader.GetDateTime("DOB"),
                    Gender = reader.GetStringNullable("Gender"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsList();
            }
            return list;
        }

        public List<Referral> GetAllByCreatedUser(Guid agencyId, Guid createdById, ReferralStatus status, AgencyServices service)
        {
            var query = new QueryBuilder(@"SELECT 
                                Id,
                                ReferralDate, 
                                AdmissionSource, 
                                MedicareNumber,
                                LastName, 
                                FirstName, 
                                PhoneHome,

                                AddressLine1,
                                AddressLine2, 
                                AddressCity, 
                                AddressStateCode,
                                AddressZipCode,

                                DOB,
                                Gender,
                                EmailAddress,
                                IsDNR
                                    FROM 
                                        referrals")
                                          .Where("AgencyId = @agencyid")
                                          .And("CreatedById = @createdbyid")
                                          .And("DeprecatedServices & @serviceid != @serviceid")
                                          .And("Services & @serviceid = @serviceid")
                                          .And(string.Format("{0}Status = @status", service.ToString()))
                                          .OrderBy("LastName", true);

            var list = new List<Referral>();
            using (var cmd = new FluentCommand<Referral>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)status)
                 .AddInt("serviceid", (int)service)
                  .AddGuid("createdbyid", createdById)
                .SetMap(reader => new Referral
                {
                    Id = reader.GetGuid("Id"),
                    ReferralDate = reader.GetDateTime("ReferralDate"),
                    AdmissionSource = reader.GetInt("AdmissionSource"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    DOB = reader.GetDateTime("DOB"),
                    Gender = reader.GetStringNullable("Gender"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsList();
            }
            return list;
        }

        public List<ReferralData> All(Guid agencyId, ReferralStatus referralStatus, AgencyServices service)
        {
            var query = string.Format(@"SELECT 
                                rf.Id,
                                rf.FirstName, 
                                rf.LastName, 
                                rf.DOB,
                                rf.Gender,
                                rf.{0}Status as Status,
                                rf.CreatedById,
                                rf.AdmissionSource,
                                rf.ReferralDate ,
                                p.Id Is NOT NULL as IsPatientExist
                                    FROM 
                                        referrals rf
                                           LEFT JOIN patients p ON rf.Id = p.Id
                                                WHERE
                                                     rf.AgencyId = @agencyid AND 
                                                     (rf.DeprecatedServices & @serviceid != @serviceid) AND
                                                     (rf.Services & @serviceid = @serviceid) AND
                                                     rf.{0}Status = @status ORDER BY  rf.LastName ASC ", service.ToString());

            var list = new List<ReferralData>();
            using (var cmd = new FluentCommand<ReferralData>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)referralStatus)
                 .AddInt("serviceid", (int)service)
                .SetMap(reader => new ReferralData
                {
                    Id = reader.GetGuid("Id"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AdmissionSourceId = reader.GetInt("AdmissionSource"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    ReferralDate = reader.GetDateTime("ReferralDate"),
                    CreatedById = reader.GetGuid("CreatedById"),
                    IsPatientExist = reader.GetBoolean("IsPatientExist")

                })
                .AsList();
            }
            return list;
        }

        public List<ReferralData> AllByUser(Guid agencyId, Guid createdById, ReferralStatus status, AgencyServices service)
        {
            var query = string.Format(@"SELECT
                                        rf.Id, 
                                        rf.FirstName,
                                        rf.LastName,
                                        rf.DOB, 
                                        rf.Gender,
                                        rf.{0}Status as Status, 
                                        rf.CreatedById,
                                        rf.AdmissionSource,
                                        rf.ReferralDate ,
                                        p.Id Is NOT NULL as IsPatientExist
                                            FROM 
                                                referrals rf 
                                                    LEFT JOIN patients p ON rf.Id = p.Id
                                                            WHERE
                                                                 rf.AgencyId = @agencyid AND 
                                                                 rf.CreatedById = @createdbyid AND
                                                                 rf.DeprecatedServices & @serviceid != @serviceid AND
                                                                 rf.Services & @serviceid = @serviceid AND
                                                                 rf.{0}Status = @status ORDER BY  rf.LastName ASC ", service.ToString());

            var list = new List<ReferralData>();
            using (var cmd = new FluentCommand<ReferralData>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("createdbyid", createdById)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)status)
                .AddInt("serviceid", (int)service)
                .SetMap(reader => new ReferralData
                {
                    Id = reader.GetGuid("Id"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AdmissionSourceId = reader.GetInt("AdmissionSource"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    ReferralDate = reader.GetDateTime("ReferralDate"),
                    CreatedById = reader.GetGuid("CreatedById"),
                    IsPatientExist = reader.GetBoolean("IsPatientExist")
                })
                .AsList();
            }
            return list;
        }

        public List<NonAdmit> GetNonAdmitReferral(Guid agencyId, ReferralStatus status, AgencyServices service, bool isUserCanAdmit)
        {
            var query = new QueryBuilder(@"SELECT
                                            Id, 
                                            FirstName,
                                            LastName,
                                            MedicareNumber,
                                            PhoneHome, 
                                            NonAdmissionReason,
                                            NonAdmissionDate, 
                                            DOB,
                                            Gender ,
                                            AddressLine1,
                                            AddressLine2, 
                                            AddressCity, 
                                            AddressStateCode,
                                            AddressZipCode, 
                                            Comments
                                                FROM 
                                                    referrals")
                                                      .Where("AgencyId = @agencyid")
                                                      .And("DeprecatedServices & @serviceId != @serviceId")
                                                      .And(string.Format("{0}Status = @status",service.ToString()))
                                                      .And("Services & @serviceId = @serviceId")
                                                      .OrderBy("LastName", true);

            var list = new List<NonAdmit>();
            using (var cmd = new FluentCommand<NonAdmit>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)status)
                .AddInt("serviceId", (int)service)
                .SetMap(reader => new NonAdmit
                {
                    Id = reader.GetGuid("Id"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    Gender = reader.GetStringNullable("Gender"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    NonAdmissionReason = reader.GetStringNullable("NonAdmissionReason"),
                    NonAdmitDate = reader.GetDateTime("NonAdmissionDate"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    Comments = reader.GetStringNullable("Comments"),
                    Type = NonAdmitTypes.Referral,
                    NonAdmitTypeName = NonAdmitTypes.Referral.ToString(),
                    ServiceType=(int)service,
                    ServiceName=service.ToString(),
                    IsUserCanAdmit=isUserCanAdmit
                })
                .AsList();
            }
            return list;
        }

        public Referral Get(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
        }


        //public bool Delete(Guid agencyId, Guid id, Referral referralForDelete)
        //{
        //    var referral = database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
        //    if (referral != null)
        //    {
        //        if (referralForDelete.Services.Has(AgencyServices.HomeHealth))
        //        {
        //            if (referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
        //            {
        //            }
        //            else
        //            {
        //                referral.DeprecatedServices |= AgencyServices.HomeHealth;
        //            }
        //        }
        //        if (referralForDelete.Services.Has(AgencyServices.PrivateDuty))
        //        {
        //            if (referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
        //            {
        //            }
        //            else
        //            {
        //                referral.DeprecatedServices |= AgencyServices.PrivateDuty;
        //            }
        //        }
        //        referral.IsDeprecated = true;
        //        referral.Modified = DateTime.Now;
        //        return database.Update<Referral>(referral) > 0;
        //    }
        //    return false;
        //}

        public bool UpdateModal(Referral referral)
        {
            bool result = false;
            try
            {
                if (referral != null)
                {
                    return database.Update<Referral>(referral) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddEmergencyContact(ReferralEmergencyContact emergencyContactInfo)
        {
            var result = false;
            try
            {
                if (emergencyContactInfo != null)
                {
                    emergencyContactInfo.Id = Guid.NewGuid();
                    emergencyContactInfo.Encode();
                    database.Add<ReferralEmergencyContact>(emergencyContactInfo);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public IList<ReferralEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid referralId)
        {
            Check.Argument.IsNotEmpty(referralId, "referralId");
            return database.Find<ReferralEmergencyContact>(p => p.AgencyId == agencyId && p.ReferralId == referralId).OrderBy(p => p.IsPrimary == false).ToList();
        }

        public ReferralEmergencyContact GetEmergencyContact(Guid referralId, Guid emergencyContactId)
        {
            Check.Argument.IsNotEmpty(emergencyContactId, "emergencyContactId");
            return database.Single<ReferralEmergencyContact>(e => e.ReferralId == referralId && e.Id == emergencyContactId);
        }

        public bool EditEmergencyContact(Guid agencyId, ReferralEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            try
            {
                if (emergencyContact != null)
                {
                    emergencyContact.Encode();
                    var emergencyContactToEdit = database.Single<ReferralEmergencyContact>(e => e.ReferralId == emergencyContact.ReferralId && e.Id == emergencyContact.Id);
                    if (emergencyContactToEdit != null)
                    {
                        emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                        emergencyContactToEdit.LastName = emergencyContact.LastName;
                        emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                        emergencyContactToEdit.AlternatePhone = emergencyContact.AlternatePhone;
                        emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                        emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                        if (emergencyContact.IsPrimary)
                        {
                            if (SetPrimaryEmergencyContact(agencyId, emergencyContact.ReferralId, emergencyContact.Id))
                            {
                                emergencyContactToEdit.IsPrimary = true;
                            }
                        }
                        return database.Update<ReferralEmergencyContact>(emergencyContactToEdit) > 0;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid agencyId, Guid Id, Guid referralId)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  referralemergencycontacts WHERE AgencyId = @agencyid AND ReferralId = @referralid AND Id=@id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("referralid", referralId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid referralId, Guid emergencyContactId)
        {
            bool result = false;
            try
            {
                var referral = database.Single<Referral>(p => p.Id == referralId);
                if (referral != null)
                {
                    var emergencyContacts = database.Find<ReferralEmergencyContact>(p => p.ReferralId == referralId);
                    bool flag = false;
                    foreach (ReferralEmergencyContact contat in emergencyContacts)
                    {
                        if (contat.Id == emergencyContactId)
                        {
                            contat.IsPrimary = true;
                            flag = true;
                        }
                        else
                        {
                            contat.IsPrimary = false;
                        }
                    }
                    if (flag)
                    {
                        return database.UpdateMany<ReferralEmergencyContact>(emergencyContacts) > 0;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public ReferralEmergencyContact GetFirstEmergencyContactByReferral(Guid agencyId, Guid referralId)
        {
            var emergencyContact = new ReferralEmergencyContact();
            var script = string.Format(@"SELECT 
                                Id as Id,
                                FirstName as FirstName,
                                LastName as LastName ,
                                Relationship as Relationship ,
                                PrimaryPhone as PrimaryPhone ,
                                AlternatePhone as AlternatePhone ,
                                EmailAddress as EmailAddress 
                                    FROM
                                        referralemergencycontacts 
                                            WHERE 
                                                AgencyId = @agencyId  AND 
                                                ReferralId = @referralId 
                                                Order BY IsPrimary DESC 
                                                Limit 1");
            using (var cmd = new FluentCommand<ReferralEmergencyContact>(script))
            {
                emergencyContact = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("referralId", referralId)
                    .SetMap(reader => new ReferralEmergencyContact
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        Relationship = reader.GetStringNullable("Relationship"),
                        PrimaryPhone = reader.GetStringNullable("PrimaryPhone"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        AlternatePhone = reader.GetStringNullable("AlternatePhone")
                    })
                    .AsSingle();
            }
            return emergencyContact;
        }

        #endregion

        
    }
}
