﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public class PrivateDutyTaskRepository : TaskScheduleAbstract<PrivateDutyScheduleTask>
    {
        public PrivateDutyTaskRepository(SimpleRepository database,string connectionStringName)
            : base(database, connectionStringName)
        {
            base.Service = AgencyServices.PrivateDuty;
        }

        /// <summary>
        /// Gets the Scheduled Tasks between the start date and the end date
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId">The Id of the Patient(Optional)</param>
        /// <param name="userId">The Id of the User(Optional)</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="includeHidden">Use true to include tasks that are hidden from the schedule center.</param>
        /// <returns></returns>
        public List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDates(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, bool includeHidden)
        {
            string hiddenScript = " st.IsHidden = 0 AND ";
            if (includeHidden)
            {
                hiddenScript = "";
            }
            string patientScript = "";
            if (!patientId.IsEmpty())
            {
                patientScript = " st.PatientId = @patientId AND ";
            }
            string userScript = "";
            if (!userId.IsEmpty())
            {
                userScript = " st.UserId = @userId AND ";
            }
            string script = string.Format(@"SELECT 
                st.Id as Id ,
                st.PatientId as PatientId ,
                st.EpisodeId as EpisodeId ,
                st.UserId as UserId ,
                st.DisciplineTask as DisciplineTask , 
                st.EventStartTime as EventStartTime ,
                st.EventEndTime as EventEndTime ,
                st.VisitStartTime as VisitStartTime ,
                st.VisitEndTime as VisitEndTime ,
                st.IsAllDay as IsAllDay ,
                st.Status as Status ,
                st.Discipline as Discipline ,
                st.IsMissedVisit as IsMissedVisit , 
                st.Comments as Comments ,
                st.Asset as Asset ,
                st.Version as Version,
                pe.StartDate as StartDate,
                pe.EndDate as EndDate
                    FROM scheduletasks st
                        INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id
                            WHERE 
                                st.AgencyId = @agencyId AND
                                st.IsDeprecated = 0 AND
                                pe.IsDischarged = 0 AND
                                pe.IsActive = 1 AND
                                {0} {1} {2}
                                st.EventStartTime BETWEEN DATE(@startDate) AND DATE(@endDate)", hiddenScript, patientScript, userScript);
            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyId", agencyId)
                     .AddGuid("patientId", patientId)
                     .AddGuid("userId", userId)
                     .AddDateTime("startDate", startDate)
                     .AddDateTime("endDate", endDate)
                     .SetMap(reader => new PrivateDutyScheduleTask
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         UserId = reader.GetGuid("UserId"),
                         DisciplineTask = reader.GetInt("DisciplineTask"),
                         EventStartTime = reader.GetDateTime("EventStartTime"),
                         EventEndTime = reader.GetDateTime("EventEndTime"),
                         VisitStartTime = reader.GetDateTime("VisitStartTime"),
                         VisitEndTime = reader.GetDateTime("VisitEndTime"),
                         IsAllDay = reader.GetBoolean("IsAllDay"),
                         Status = reader.GetInt("Status", 0),
                         Discipline = reader.GetStringNullable("Discipline"),
                         IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                         Comments = reader.GetStringNullable("Comments"),
                         Asset = reader.GetStringNullable("Asset"),
                         Version = reader.GetInt("Version"),
                         StartDate = reader.GetDateTime("StartDate"),
                         EndDate = reader.GetDateTime("EndDate")
                     })
                     .AsList().OrderBy(e => e.VisitStartTime).ToList();
            }
            return list;
        }

        public override List<PrivateDutyScheduleTask> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid[] episodeIds, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var episodeScript = string.Empty;
            if (episodeIds != null && episodeIds.Length > 0)
            {
                if (episodeIds.Length == 1)
                {
                    episodeScript += string.Format(" AND st.EpisodeId = '{0}' ", episodeIds.FirstOrDefault());
                }
                else
                {
                    episodeScript += string.Format(" AND st.EpisodeId IN ( {0} )", episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
            }
            var dateRange = IsDateRange ? " AND DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {
                disciplineScript = string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'PT') = 0 OR STRCMP(st.Discipline, 'OT') = 0 OR STRCMP(st.Discipline , 'ST') = 0 )";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP(st.Discipline, 'ReportsAndNotes') = 0 ) ";
            }
            else if (discipline.IsEqual("Dietation"))
            {
                disciplineScript = " AND ( STRCMP(st.Discipline ,'Dietician') = 0 ";
            }
            else
            {
                disciplineScript = " AND st.Discipline = @discipline ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.Comments as Comments ,
                        st.Asset as Asset ,
                        st.Version as Version
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid {2} AND 
                                            st.IsDeprecated = 0 AND
                                            st.IsHidden = 0 AND
                                            st.DisciplineTask > 0 {0} {1} 
                                            ORDER BY DATE(st.EventStartTime) DESC  ", disciplineScript, dateRange, episodeScript);
            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddString("discipline", discipline)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Comments = reader.GetStringNullable("Comments"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }

        public override List<PrivateDutyScheduleTask> GetUsersScheduleTasksBetweenDatesByStatus(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, params int[] scheduleStatus)
        {
            var statuses = scheduleStatus.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            string script = string.Format(@"SELECT * 
                                FROM scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyId AND
                                        st.UserId = @userId AND
                                        st.PatientId = @patientId AND
                                        st.IsDeprecated = 0 AND
                                        st.Status in ({0}) AND
                                        st.EventStartTime BETWEEN @startDate AND @endDate", statuses);

            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyId", agencyId)
                     .AddGuid("userId", userId)
                     .AddGuid("patientId", patientId)
                     .AddDateTime("startDate", startDate)
                     .AddDateTime("endDate", endDate)
                     .AsList();
            }

            return list;
        }

        public override List<PrivateDutyScheduleTask> GetScheduleEventsVeryLeanByIds(Guid agencyId, Guid patientId, List<Guid> ids, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (ids != null && ids.Count > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.Id IN ( {0} ) ", ids.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND st.IsMissedVisit = 0 ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.IsBillable as IsBillable,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyId  AND 
                                            st.PatientId = @patientId  AND
                                            st.IsDeprecated = 0  {0} ", scheduleAdditionalScript);

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientId = patientId,
                    Id = reader.GetGuid("Id"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status", 0),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<PrivateDutyScheduleTask> GetPreviousNotes(Guid agencyId, PrivateDutyScheduleTask scheduledTask, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        st.Id,
                        st.PatientId,
                        st.UserId,
                        st.DisciplineTask, 
                        st.EventStartTime,
                        st.EventEndTime,
                        st.VisitStartTime,
                        st.VisitEndTime    
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.IsHidden = 0 AND
                                        st.AgencyId = @agencyid  AND 
                                        st.PatientId = @patientid  AND
                                        st.Id != @eventid  AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsDeprecated = 0  AND
                                        DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) AND
                                        st.DisciplineTask IN ({0}) AND
                                        st.Status IN ({1}) 
                                            ORDER BY DATE(st.EventStartTime) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","));
            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledTask.Id)
                .AddGuid("patientid", scheduledTask.PatientId)
                .AddDateTime("startdate", scheduledTask.EventStartTime.AddMonths(-4))
                .AddDateTime("enddate", scheduledTask.EventStartTime)
                .SetDictonaryId("EventId")
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime")
                })
                .AsList();
            }
            return list;
        }

        public override List<PrivateDutyScheduleTask> GetPreviousNotes(Guid agencyId, PrivateDutyScheduleTask scheduledTask, string[] disciplines, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        st.Id,
                        st.PatientId,
                        st.UserId,
                        st.DisciplineTask, 
                        st.EventStartTime,
                        st.EventEndTime,
                        st.VisitStartTime,
                        st.VisitEndTime    
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.IsHidden = 0 AND
                                        st.AgencyId = @agencyid  AND 
                                        st.PatientId = @patientid  AND
                                        st.Id != @eventid  AND
                                        st.IsMissedVisit = 0 AND 
                                        st.IsDeprecated = 0 AND 
                                        DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate)  AND
                                        st.DisciplineTask IN ({0}) AND
                                        st.Status IN ({1}) AND
                                        st.Discipline IN ({2}) 
                                            ORDER BY DATE(st.EventStartTime) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledTask.Id)
                .AddGuid("patientid", scheduledTask.PatientId)
                .AddDateTime("startdate", scheduledTask.EventStartTime.AddMonths(-4))
                .AddDateTime("enddate", scheduledTask.EventStartTime)
                .SetDictonaryId("EventId")
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime")
                })
                .AsList();
            }
            return list;
        }

        public override List<PrivateDutyScheduleTask> GetPatientScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid  AND
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            (DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) OR
                                            DATE(st.EventEndTime) between  DATE(pe.StartDate) and DATE(pe.EndDate)) AND                        
                                            (DATE(st.EventStartTime) between  DATE(@startdate) and DATE(@enddate) OR
                                            DATE(st.EventEndTime) between  DATE(@startdate) and DATE(@enddate)) AND                                           
                                            st.DisciplineTask IN ( {0} )", disciplineTasks.Select(s => s.ToString()).ToArray().Join(","));

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<PrivateDutyScheduleTask> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.Status as Status ,
                        CONCAT('Reason: ', m.Reason, '\r\n', 'Comments: ', m.Comments) as MissedVisitComments,
                        st.Comments as Comments,
                        pr.PatientIdNumber as PatientIdNumber,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName,
                        pr.UserId as PatientUserId
                                FROM 
                                    scheduletasks st
                                        INNER JOIN missedvisits m ON st.Id = m.Id
                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  
                                                WHERE
                                                        st.AgencyId = @agencyid {0} AND 
                                                        st.IsMissedVisit = 1 AND
                                                        st.IsDeprecated = 0 AND
                                                        pr.IsDeprecated = 0  AND 
                                                        pr.Status IN (1,2) AND 
                                                        st.IsDeprecated = 0  AND 
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0  AND 
                                                        (DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) OR
                                                        DATE(st.EventEndTime) between  DATE(pe.StartDate) and DATE(pe.EndDate)) AND                        
                                                        (DATE(st.EventStartTime) between  DATE(@startdate) and DATE(@enddate) OR
                                                        DATE(st.EventEndTime) between  DATE(@startdate) and DATE(@enddate))              
                                                         ORDER BY st.EventStartTime DESC ", additonalScript);

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientUserId = reader.GetGuidIncludeEmpty("PatientUserId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    Status = reader.GetInt("Status", 0),
                    MissedVisitComments = reader.GetStringNullable("MissedVisitComments"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsMissedVisit = true,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }

        public override List<PrivateDutyScheduleTask> GetScheduleByUserIdLean(Guid agencyId, Guid patientIdOptional, Guid userId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded)
        {
            throw new NotImplementedException();
        }

//        public override List<PrivateDutyScheduleTask> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
//        {
//            var additonalScript = string.Empty;

//            if (!branchId.IsEmpty())
//            {
//                additonalScript += " AND pr.AgencyLocationId = @branchId";
//            }
//            var script = string.Format(@"SELECT 
//                        st.Id as Id ,
//                        st.PatientId as PatientId ,
//                        st.EpisodeId as EpisodeId ,
//                        st.UserId as UserId ,
//                        st.DisciplineTask as DisciplineTask , 
//                        st.EventStartTime as EventStartTime ,
//                        st.EventEndTime as EventEndTime ,
//                        st.VisitStartTime as VisitStartTime ,
//                        st.VisitEndTime as VisitEndTime ,
//                        st.IsAllDay as IsAllDay ,
//                        st.Status as Status ,
//                        pr.FirstName as FirstName, 
//                        pr.LastName as LastName
//                                FROM 
//                                    scheduletasks st
//                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
//                                        INNER JOIN patientprofiles pr ON st.PatientId = pr.Id  
//                                                WHERE
//                                                    st.AgencyId = @agencyid {0} AND 
//                                                    st.IsMissedVisit = 0 AND
//                                                    st.IsDeprecated = 0 AND
//                                                    st.InPrintQueue = 1 AND
//                                                    pr.IsDeprecated = 0  AND 
//                                                    pr.Status IN (1,2) AND 
//                                                    st.IsDeprecated = 0  AND 
//                                                    pe.IsActive = 1 AND 
//                                                    pe.IsDischarged = 0  AND 
//                                                    (DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) OR
//                                                    DATE(st.EventEndTime) between  DATE(pe.StartDate) and DATE(pe.EndDate)) AND                        
//                                                    (DATE(st.EventStartTime) between  DATE(@startdate) and DATE(@enddate) OR
//                                                    DATE(st.EventEndTime) between  DATE(@startdate) and DATE(@enddate))
//                                                     ORDER BY st.EventStartTime DESC ", additonalScript);

//            var scheduleList = new List<PrivateDutyScheduleTask>();
//            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
//            {
//                scheduleList = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("branchId", branchId)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .SetMap(reader => new PrivateDutyScheduleTask
//                {
//                    Id = reader.GetGuid("Id"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    EventStartTime = reader.GetDateTime("EventStartTime"),
//                    EventEndTime = reader.GetDateTime("EventEndTime"),
//                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
//                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
//                    IsAllDay = reader.GetBoolean("IsAllDay"),
//                    Status = reader.GetInt("Status", 0),
//                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
//                })
//                .AsList();
//            }
//            return scheduleList;

//        }

        public override List<PrivateDutyScheduleTask> GetDeletedTasks(Guid agencyId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId,
                        st.EpisodeId as EpisodeId,
                        st.UserId as UserId,
                        st.DisciplineTask as DisciplineTask, 
                        st.EventStartTime as EventStartTime,
                        st.EventEndTime as EventEndTime,
                        st.VisitStartTime as VisitStartTime,
                        st.VisitEndTime as VisitEndTime,
                        st.IsAllDay as IsAllDay,
                        st.Status as Status,
                        st.Discipline as Discipline,
                        st.IsBillable as IsBillable,
                        st.IsMissedVisit as IsMissedVisit, 
                        st.Version as Version
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND 
                                            st.IsDeprecated = 1 AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 
                                                ORDER BY DATE(st.EventStartTime) DESC");

            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }

        public override List<PrivateDutyScheduleTask> GetScheduleTasksByStatusDisciplineAndRange(Guid agencyId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks)
        {
            var scheduleList = new List<PrivateDutyScheduleTask>();
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {

                var statusScript = string.Empty;
                if (patientStatus <= 0)
                {
                    statusScript = " AND  pr.Status IN (1,2)";
                }
                else
                {
                    statusScript = " AND pr.Status = @statusid";
                }
                string branchScript = string.Empty;
                if (!branchId.IsEmpty())
                {
                    branchScript = " AND pr.AgencyLocationId = @branchId ";
                }
                var additionalScript = string.Empty;
                if (disciplineTasks != null && disciplineTasks.Length > 0)
                {
                    additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
                }
                if (scheduleStatus != null && scheduleStatus.Length > 0)
                {
                    additionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }

                var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName, 
                        pr.PatientIdNumber as PatientIdNumber,
                        pr.MiddleInitial as MiddleInitial 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE
                                            st.AgencyId = @agencyid  AND 
                                            pr.IsDeprecated = 0 {0} {1} AND 
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                            DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) {2} ", statusScript, branchScript, additionalScript);


                using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
                {
                    scheduleList = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("statusid", patientStatus)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new PrivateDutyScheduleTask
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        EventStartTime = reader.GetDateTime("EventStartTime"),
                        EventEndTime = reader.GetDateTime("EventEndTime"),
                        VisitStartTime = reader.GetDateTime("VisitStartTime"),
                        VisitEndTime = reader.GetDateTime("VisitEndTime"),
                        IsAllDay = reader.GetBoolean("IsAllDay"),
                        Status = reader.GetInt("Status", 0),
                        Discipline = reader.GetStringNullable("Discipline"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                    })
                    .AsList();
                }
            }
            return scheduleList;
        }

        public override List<PrivateDutyScheduleTask> GetScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status)
        {
            
            //new int[] { (int)DisciplineTasks.FaceToFaceEncounter, (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485, (int)DisciplineTasks.PhysicianOrder, (int)DisciplineTasks.PTEvaluation, (int)DisciplineTasks.OTEvaluation, (int)DisciplineTasks.STEvaluation, (int)DisciplineTasks.PTReEvaluation, (int)DisciplineTasks.OTReEvaluation, (int)DisciplineTasks.STReEvaluation };
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND
                                            pr.IsDeprecated = 0 AND 
                                            pr.Status IN (1,2) AND
                                            st.IsDeprecated = 0  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) AND
                                            st.DisciplineTask IN ( {0} ) AND
                                            st.Status IN ( {1} ) {2}", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<PrivateDutyScheduleTask> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid  AND
                                            st.IsDeprecated = 0  AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            (DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) OR
                                            DATE(st.EventEndTime) between  DATE(pe.StartDate) and DATE(pe.EndDate)) AND                        
                                            (DATE(st.EventStartTime) between  DATE(@startdate) and DATE(@enddate) OR
                                             DATE(st.EventEndTime) between  DATE(@startdate) and DATE(@enddate)) AND                                           
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( {0} ))", disciplineTasks.Select(s => s.ToString()).ToArray().Join(","));

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override List<PrivateDutyScheduleTask> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> disciplineTasks, List<int> status)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND
                                            pr.IsDeprecated = 0 AND 
                                            pr.Status IN (1,2) AND
                                            st.IsDeprecated = 0  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND 
                                            DATE(st.EventStartTime) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) AND
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( {0} )) AND
                                            st.Status IN ( {1} ) {2}", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }

        public override string GetReturnReason(Guid agencyId, Guid patientId,  Guid eventId, Guid currentUserId)
        {
            var newComments = GetReturnCommentsWithUserInfo(agencyId,  eventId);
            return newComments.ToCommentText(string.Empty, currentUserId);
        }
        
        public override List<PrivateDutyScheduleTask> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty;
            if (patientStatus <= 0)
            {
                status = "AND  pr.Status IN (1,2)";
            }
            else
            {
                status = "AND pr.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = "AND pr.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = "AND st.IsMissedVisit = 0";
            }
            var scheduleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleStatusScript = string.Format("AND st.Status IN ( {0} )", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.BeforeMissedVisitStatus as BeforeMissedVisitStatus , 
                        st.Comments as Comments ,
                        pe.Comments as EpisodeNotes ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    LEFT JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            pr.IsDeprecated = 0 {2} AND
                                            st.IsDeprecated = 0 {3} AND
                                            pe.IsActive = 1 AND
                                            pe.IsDischarged = 0 AND
                                            DATE(st.EventStartTime) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) {0} {1}", scheduleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    BeforeMissedVisitStatus = reader.GetInt("BeforeMissedVisitStatus"),
                    Comments = reader.GetStringNullable("Comments"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }

        public PrivateDutyScheduleTask GetLastScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventStartTime as EventStartTime ,
                        st.EventEndTime as EventEndTime ,
                        st.VisitStartTime as VisitStartTime ,
                        st.VisitEndTime as VisitEndTime ,
                        st.IsAllDay as IsAllDay ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue  
                            FROM
                                scheduletasks st
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 AND
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventStartTime) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                        DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate) {0}  
                                            ORDER BY DATE(st.EventStartTime) DESC LIMIT 1 ", additionalScript);


            var scheduleEvent = new PrivateDutyScheduleTask();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                scheduleEvent = cmd.SetConnection(base.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventStartTime = reader.GetDateTime("EventStartTime"),
                    EventEndTime = reader.GetDateTime("EventEndTime"),
                    VisitStartTime = reader.GetDateTime("VisitStartTime"),
                    VisitEndTime = reader.GetDateTime("VisitEndTime"),
                    IsAllDay = reader.GetBoolean("IsAllDay"),
                    Status = reader.GetInt("Status", 0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = startDate,
                    EndDate = endDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public override IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                        pr.Id as PatientId , 
                        pe.Id as EpisodeId,
                        pe.IsDischarged as IsDischarged,
                        st.Id as Id,
                        st.DisciplineTask as DisciplineTask , 
                        st.IsMissedVisit as IsMissedVisit,
                        st.Status as Status , 
                        st.EventStartTime as EventStartTime ,
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patientprofiles pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId = @userid AND
                                            st.IsDeprecated = 0 AND 
                                            st.IsMissedVisit = {0} AND 
                                            st.DisciplineTask > 0 AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND
                                            pr.IsDeprecated = 0 AND
                                            pr.Status IN (1,2) AND 
                                            DATE(st.EventStartTime) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventStartTime) between DATE(@startdate) and DATE(@enddate)  {2} 
                                                ORDER BY DATE(st.EventStartTime) DESC LIMIT {1}", isMissedVisit, limit, additionalFilter);

            var list = new List<UserVisitWidget>();
            using (var cmd = new FluentCommand<UserVisitWidget>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", from)
                .AddDateTime("enddate", to)
                .SetMap(reader => new UserVisitWidget
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventId = reader.GetGuid("Id"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                    EventDate = reader.GetDateTime("EventStartTime").ToDateAndTime()
                })
                .AsList();
            }
            return list;
        }

        protected override string GetWhereAppSpecificSql(SqlSelectType sql)
        {
            string select = string.Empty;
            switch (sql)
            {
                case SqlSelectType.EventDate:
                    select = "st.EventStartTime";
                    break;
                case SqlSelectType.VisitDate:
                    select = "st.VisitStartTime";
                    break;
            }
            return select;
        }

        protected override string GetSelectAppSpecificSql(SqlSelectType sql)
        {
            string select = string.Empty;
            switch (sql)
            {
                case SqlSelectType.All:
                    select = " st.VisitStartTime as VisitStartTime, st.VisitEndTime as VisitEndTime, st.EventStartTime as EventStartTime, st.EventEndTime as EventEndTime, ";
                    break;
                case SqlSelectType.EventDate:
                    select = " st.EventStartTime ";
                    break;
            }
            return select;
        }

        protected override void GetSelectAppSpecificSql(PrivateDutyScheduleTask task, DataReader reader)
        {
            task.VisitStartTime = reader.GetDateTime("VisitStartTime");
            task.VisitEndTime = reader.GetDateTime("VisitEndTime");
            task.EventStartTime = reader.GetDateTime("EventStartTime");
            task.EventEndTime = reader.GetDateTime("EventEndTime");
        }

        protected override void GetSelectAppSpecificSql(UserVisit task, DataReader reader)
        {
            task.VisitDate = reader.GetDateTime("VisitStartTime");
            task.ScheduleDate = reader.GetDateTime("EventStartTime");
        }

        /// <summary>
        /// Gets the schedule tasks limited by a number and certian disciplines
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="patientId"></param>
        /// <param name="limit"></param>
        /// <param name="disciplineTasks">The types of disciplines to be pulled</param>
        /// <returns></returns>
        public List<PrivateDutyScheduleTask> GetScheduleEvents(Guid agencyId, Guid patientId, int limit, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                    st.Id as Id ,
                    st.UserId as UserId,
                    st.DisciplineTask as DisciplineTask  
                FROM scheduletasks st
                INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id  AND st.EpisodeId = st.EpisodeId
                WHERE 
                    pe.AgencyId = @agencyid AND 
                    st.AgencyId = @agencyid AND 
                    st.PatientId = @patientid AND 
                    st.IsMissedVisit = 0 AND 
                    st.IsDeprecated = 0 AND 
                    pe.IsActive = 1 AND 
                    pe.IsDischarged = 0 {0} ORDER BY st.EventStartTime DESC LIMIT 0,{1}", additionalScript, limit);
           
            var list = new List<PrivateDutyScheduleTask>();
            using (var cmd = new FluentCommand<PrivateDutyScheduleTask>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PrivateDutyScheduleTask
                {
                    Id = reader.GetGuid("Id"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list.GetRange(0, limit < list.Count ? limit : list.Count);
        }
    }
}
