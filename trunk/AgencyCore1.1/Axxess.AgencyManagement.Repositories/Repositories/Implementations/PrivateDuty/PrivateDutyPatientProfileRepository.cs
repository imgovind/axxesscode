﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public class PrivateDutyPatientProfileRepository : PatientProfileAbstract
    {
        public PrivateDutyPatientProfileRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.PrivateDuty;
        }

        public bool AddPayor(PrivatePayor payor)
        {
            bool result = false;
            try
            {
                if (payor != null)
                {
                    if (database.Add<PrivatePayor>(payor) != null)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdatePayorModal(PrivatePayor payor)
        {
            bool result = false;
            try
            {
                if (payor != null)
                {
                    payor.Modified = DateTime.Now;
                    return database.Update<PrivatePayor>(payor) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public PrivatePayor GetPayorOnly(Guid agencyId, Guid patientId, int Id)
        {
            return database.Single<PrivatePayor>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.Id == Id);
        }

        public IList<PrivatePayor> GetPatientBillingInformations(Guid agencyId, Guid patientId)
        {
            var billingInformations = new List<PrivatePayor>();
            var script = string.Format(@"SELECT 
                            prp.Id ,
                            prp.FirstName ,
                            prp.LastName ,
                            prp.MI ,
                            prp.AddressLine1, 
                            prp.AddressLine2 ,
                            prp.AddressCity ,
                            prp.AddressStateCode ,
                            prp.AddressZipCode, 
                            prp.PhoneHome ,
                            prp.PhoneMobile ,
                            prp.FaxNumber ,
                            prp.EmailAddress,  
                            prp.IsCurrentPayor,  
                            prp.Relationship,  
                            prp.OtherRelationship
                                FROM
                                   privatepayors prp
                                        WHERE 
                                            prp.AgencyId = @agencyId  AND 
                                            prp.PatientId = @patientId  AND 
                                            prp.IsDeprecated = 0");
            using (var cmd = new FluentCommand<PrivatePayor>(script))
            {
                billingInformations = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => new PrivatePayor
                    {
                        Id = reader.GetInt("Id"),
                        PatientId = patientId,
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MI = reader.GetStringNullable("MI"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        IsCurrentPayor = reader.GetBoolean("IsCurrentPayor"),
                        Relationship = reader.GetStringNullable("Relationship"),
                        OtherRelationship = reader.GetStringNullable("OtherRelationship")
                    })
                    .AsList();
            }

            return billingInformations;

        }

        public List<PrivatePayor> GetPatientPrivatePayorLean(Guid agencyId, Guid patientId)
        {
            var billingInformations = new List<PrivatePayor>();
            var script = string.Format(@"SELECT 
                            prp.Id ,
                            prp.FirstName ,
                            prp.LastName 
                                FROM
                                   privatepayors prp
                                        WHERE 
                                            prp.AgencyId = @agencyId  AND 
                                            prp.PatientId = @patientId  AND 
                                            prp.IsDeprecated = 0");
            using (var cmd = new FluentCommand<PrivatePayor>(script))
            {
                billingInformations = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => new PrivatePayor
                    {
                        Id = reader.GetInt("Id"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    })
                    .AsList();
            }

            return billingInformations;

        }

        public bool TogglePayor(Guid agencyId, Guid patientId, int Id, bool isDeprecated)
        {
            bool result = false;
            if (Id > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE privatepayors prp set prp.IsDeprecated = {0} WHERE prp.AgencyId = @agencyid AND prp.PatientId = @patientid  AND prp.Id  = @Id", isDeprecated ? 1 : 0);
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                         .AddInt("Id", Id)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

    }
}
