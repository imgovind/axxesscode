﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;

    public class PrivateDutyIncidentAccidentRepository :IncidentAccidentAbstract
    {
        public PrivateDutyIncidentAccidentRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            this.connectionStringName = connectionStringName;
        }
    }
}
