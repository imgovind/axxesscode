﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;
using Axxess.Core;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class PrivateDutyBillingRepository : BillingAbstract
    {
        public PrivateDutyBillingRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }


        protected override string GetManagedClaimsAppSpecificInsuranceOrPayorTypeFilter(int insuranceId, bool IsZeroInsuraceIdAll)
        {
            var filter = string.Empty;
            if (insuranceId == 18)
            {
                filter = string.Format(" AND mc.PayorType = {0} ", (int)PayorTypeMainCategory.PrivatePayor);
            }
            else
            {
                 filter = " AND mc.PrimaryInsuranceId = @insuranceId ";
                if (IsZeroInsuraceIdAll && insuranceId == 0)
                {
                    filter = string.Empty;
                }
            }
            return filter;
        }

        protected override string GetManagedClaimsPerPatientAppSpecificInsuranceOrPayorTypeFilter(int insuranceId)
        {
            var filter = string.Empty;
            if (insuranceId > 0)
            {
                if (insuranceId == 18)
                {
                    filter = string.Format(" AND mc.PayorType = {0} ", (int)PayorTypeMainCategory.PrivatePayor);
                }
                else
                {
                    filter = "AND mc.PrimaryInsuranceId = @insuranceId ";
                }
            }
            else if (insuranceId == -1)
            {
                filter = "AND mc.PrimaryInsuranceId <= 0 ";
            }
            else
            {
            }
            return filter;
        }

    }
}
