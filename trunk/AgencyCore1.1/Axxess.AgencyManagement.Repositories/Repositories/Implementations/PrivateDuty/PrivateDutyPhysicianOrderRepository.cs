﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core.Enums;

    public class PrivateDutyPhysicianOrderRepository : PhysicianOrderAbstract
    {
        public PrivateDutyPhysicianOrderRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.PrivateDuty;
        }
    }
}
