﻿namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.Repository;

    public class PrivateDutyVitalSignRepository : VitalSignAbstract
    {
        public PrivateDutyVitalSignRepository(SimpleRepository database, string connectionStringName)
            : base(database, connectionStringName)
        {
        }
    }
}
