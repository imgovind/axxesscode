﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;

   public class PrivateDutyPatientAdmissionRepository : PatientAdmissionAbstract
    {
       public PrivateDutyPatientAdmissionRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }
    }
}
