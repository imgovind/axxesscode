﻿namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.Repository;

    public class PrivateDutyMultiDocument : MultiDocumentAbstract
    {
        public PrivateDutyMultiDocument(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            this.connectionStringName = connectionStringName;
        }
    }
}
