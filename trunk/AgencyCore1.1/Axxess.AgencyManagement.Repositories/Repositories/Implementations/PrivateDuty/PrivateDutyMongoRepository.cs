﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Repositories.Extensions;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace Axxess.AgencyManagement.Repositories
{
    public class PrivateDutyMongoRepository : MongoAbstract
    {
        public PrivateDutyMongoRepository(MongoDatabase database) :
            base(database)
        {
        }

        #region VisitRates

        public bool AddPatientVisitRate(PatientBillRate data)
        {
            return db.AddData<PatientBillRate>("patientbillrate", data);
        }

        public bool UpdatePatientVisitRates(PatientBillRate data)
        {
            data.Modified = DateTime.Now;
            var collection = db.GetCollection<PatientBillRate>("patientbillrate");
            var query = Query.And(Query.EQ("AgencyId", data.AgencyId), Query.EQ("PatientId", data.PatientId));
            return collection.Update(query, Update.Replace<PatientBillRate>(data), UpdateFlags.Upsert).Ok;
        }

        public PatientBillRate GetPatientVisitRate(Guid agencyId, Guid patientId)
        {
            var collection = db.GetCollection<PatientBillRate>("patientbillrate");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("PatientId", patientId));
            return collection.FindOneAs<PatientBillRate>(query);
        }

        public List<ChargeRate> GetPatientVisitRates(Guid agencyId, Guid patientId)
        {
            var list = new List<ChargeRate>();
            var patientBillRate = GetPatientVisitRate(agencyId, patientId);
            if (patientBillRate != null)
            {
                list = patientBillRate.VisitRates;
            }
            return list;
        }

        #endregion

        #region Managed Claim Billing Address

        public bool AddManagedClaimBillingAddress(ManagedClaimBillingAddress data)
        {
            return db.AddData<ManagedClaimBillingAddress>("managedclaimbillingaddress", data);
        }

        public bool UpdateManagedClaimBillingAddress(ManagedClaimBillingAddress data)
        {
            return db.UpdateData<ManagedClaimBillingAddress>(data.AgencyId, data.PatientId, data.Id, "managedclaimbillingaddress", data);
        }

        public ManagedClaimBillingAddress GetManagedClaimBillingAddress(Guid agencyId, Guid id)
        {
            return db.GetData<ManagedClaimBillingAddress>(agencyId, id, "managedclaimbillingaddress");
        }

        #endregion
    }
}
