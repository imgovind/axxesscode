﻿namespace Axxess.AgencyManagement.Repositories
{
    using SubSonic.Repository;

    public class PrivateDutyAssessmentRepository : AssessmentAbstract
    {
        public PrivateDutyAssessmentRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }
    }
}