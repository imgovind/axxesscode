﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Infrastructure;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class PrivateDutyNoteRepository : VisitNoteAbstract
    {
        public PrivateDutyNoteRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
            base.Service = AgencyServices.PrivateDuty;
        }
    }
}
