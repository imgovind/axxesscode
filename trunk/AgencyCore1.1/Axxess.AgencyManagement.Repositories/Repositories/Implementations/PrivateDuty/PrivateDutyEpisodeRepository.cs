﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.Core;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class PrivateDutyEpisodeRepository : EpisodeAbstract<PrivateDutyCarePeriod>
    {
        #region Constructor

        public PrivateDutyEpisodeRepository(SimpleRepository database, string connectionStringName)
        {
            base.database = database;
            base.connectionStringName = connectionStringName;
        }

        #endregion


    }
}
