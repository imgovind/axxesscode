﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    using SubSonic.DataProviders;
    using SubSonic.Repository;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }

        public bool AddMany(List<Asset> assets)
        {
            bool result = false;
            try
            {
                if (assets != null && assets.Count>0)
                {
                    database.AddMany<Asset>(assets);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Remove(Guid id)
        {
            return database.Delete<Asset>(id) > 0;
        }

        public bool UpdateStatus(Guid agencyId, Guid id, bool isDeprecated)
        {
            var query = new SubSonic.Query.Update<Asset>(ProviderFactory.GetProvider("AgencyManagementConnectionString"))
                .Set(s => s.IsDeprecated == isDeprecated)
                .Set(s => s.Modified == DateTime.Now)
                .Where(w => w.AgencyId == agencyId && w.Id == id);
            return query.Execute() > 0;
        }

        public bool DeleteMany(Guid agencyId, List<Guid> assetIds)
        {
            bool result = false;
            if (assetIds != null && assetIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE assets set IsDeprecated = 1 WHERE AgencyId = @agencyid AND Id IN ( {0})", assetIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public List<Asset> GetAssetsNameAndId(Guid agencyId, List<Guid> assetIds)
        {
            var list = new List<Asset>();
            if (assetIds != null && assetIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"SELECT Id, FileName from assets  WHERE AgencyId = @agencyid AND Id IN ( {0})", assetIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    using (var cmd = new FluentCommand<Asset>(script))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .SetMap(reader => new Asset
                        {
                            Id = reader.GetGuid("Id"),
                            FileName = reader.GetStringNullable("FileName")
                        }).AsList();
                    }
                }
                catch (Exception)
                {
                    return list;
                }
            }
            return list;
        }

        public Asset Get(Guid agencyId,Guid id)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId && a.IsDeprecated == false);
        }

        public bool AddPatientDocument(Guid assetId, PatientDocument patientDocument)
        {
            bool result = false;
            if (patientDocument != null)
            {
                patientDocument.AssetId = assetId;
                patientDocument.Id = Guid.NewGuid();
                patientDocument.Created = DateTime.Now;
                patientDocument.Modified = DateTime.Now;
                database.Add<PatientDocument>(patientDocument);
                result = true;
            }
            return result;
        }
        
        public bool UpdatePatientDocument(PatientDocument patientDocument)
        {
            var result = false;
            try
            {
                if (patientDocument != null)
                {
                    database.Update<PatientDocument>(patientDocument);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeletePatientDocument(Guid id, Guid patientId, Guid agencyId)
        {
            var result = false;
            var patientDocument = database.Single<PatientDocument>(pa => pa.Id == id && pa.PatientId == patientId);
            if (patientDocument != null)
            {
                patientDocument.IsDeprecated = true;
                patientDocument.Modified = DateTime.Now;
                database.Update<PatientDocument>(patientDocument);
                result = UpdateStatus(agencyId, patientDocument.AssetId, true);
            }
            return result;
        }

        public PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId)
        {
            return database.Single<PatientDocument>(a => a.Id == id && a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);
        }

        public List<PatientDocument> GetPatientDocuments(Guid patientId)
        {
            return database.Find<PatientDocument>(p => p.PatientId == patientId && p.IsDeprecated == false).ToList();
        }

        #endregion
    }
}
