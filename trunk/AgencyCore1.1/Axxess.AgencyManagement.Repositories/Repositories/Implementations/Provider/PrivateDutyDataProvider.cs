﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core.Infrastructure.Data;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;

    public class PrivateDutyDataProvider : AgencyManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public PrivateDutyDataProvider()
        {
            this.database = new SimpleRepositoryExtended("PrivateDutyConnectionString");
            this.connectionStringName="PrivateDutyConnectionString";
        }

        #endregion

        #region PrivateDutyDataProvider Members

        private PrivateDutyTaskRepository taskRepository;
        public PrivateDutyTaskRepository TaskRepository
        {
            get
            {
                if (taskRepository == null)
                {
                    taskRepository = new PrivateDutyTaskRepository(this.database, this.connectionStringName);
                }
                return taskRepository;
            }
        }

        private PrivateDutyNoteRepository noteRepository;
        public PrivateDutyNoteRepository NoteRepository
        {
            get
            {
                if (noteRepository == null)
                {
                    noteRepository = new PrivateDutyNoteRepository(this.database, this.connectionStringName);
                }
                return noteRepository;
            }
        }

        private PrivateDutyMultiDocument multiDocument;
        public PrivateDutyMultiDocument MultiDocument
        {
            get
            {
                if (multiDocument == null)
                {
                    multiDocument = new PrivateDutyMultiDocument(this.database, this.connectionStringName);
                }
                return multiDocument;
            }
        }

        private PrivateDutyPlanOfCareRepository planOfCareRepository;
        public PrivateDutyPlanOfCareRepository PlanOfCareRepository
        {
            get
            {
                if (planOfCareRepository == null)
                {
                    planOfCareRepository = new PrivateDutyPlanOfCareRepository(this.database, this.connectionStringName);
                }
                return planOfCareRepository;
            }
        }

        private PrivateDutyEpisodeRepository episodeRepository;
        public PrivateDutyEpisodeRepository EpisodeRepository
        {
            get
            {
                if (episodeRepository == null)
                {
                    episodeRepository = new PrivateDutyEpisodeRepository(this.database, this.connectionStringName);
                }
                return episodeRepository;
            }
        }

        private PrivateDutyAssessmentRepository assessmentRepository;
        public PrivateDutyAssessmentRepository AssessmentRepository
        {
            get
            {
                if (assessmentRepository == null)
                {
                    assessmentRepository = new PrivateDutyAssessmentRepository(this.database, this.connectionStringName);
                }
                return assessmentRepository;
            }
        }

        private PrivateDutyPhysicianOrderRepository physicianOrderRepository;
        public PrivateDutyPhysicianOrderRepository PhysicianOrderRepository
        {
            get
            {
                if (physicianOrderRepository == null)
                {
                    physicianOrderRepository = new PrivateDutyPhysicianOrderRepository(this.database, this.connectionStringName);
                }
                return physicianOrderRepository;
            }
        }

        private PrivateDutyCommunicationNoteRepository communicationNoteRepository;
        public PrivateDutyCommunicationNoteRepository CommunicationNoteRepository
        {
            get
            {
                if (communicationNoteRepository == null)
                {
                    communicationNoteRepository = new PrivateDutyCommunicationNoteRepository(this.database, this.connectionStringName);
                }
                return communicationNoteRepository;
            }
        }

        private PrivateDutyIncidentAccidentRepository incidentAccidentRepository;
        public PrivateDutyIncidentAccidentRepository IncidentAccidentRepository
        {
            get
            {
                if (incidentAccidentRepository == null)
                {
                    incidentAccidentRepository = new PrivateDutyIncidentAccidentRepository(this.database, this.connectionStringName);
                }
                return incidentAccidentRepository;
            }
        }

        private PrivateDutyInfectionRepository infectionRepository;
        public PrivateDutyInfectionRepository InfectionRepository
        {
            get
            {
                if (infectionRepository == null)
                {
                    infectionRepository = new PrivateDutyInfectionRepository(this.database, this.connectionStringName);
                }
                return infectionRepository;
            }
        }

        private PrivateDutyPatientAdmissionRepository patientAdmissionRepository;
        public PrivateDutyPatientAdmissionRepository PatientAdmissionRepository
        {
            get
            {
                if (patientAdmissionRepository == null)
                {
                    patientAdmissionRepository = new PrivateDutyPatientAdmissionRepository(this.database, this.connectionStringName);
                }
                return patientAdmissionRepository;
            }
        }

        private PrivateDutyPatientProfileRepository patientProfileRepository;
        public PrivateDutyPatientProfileRepository PatientProfileRepository
        {
            get
            {
                if (patientProfileRepository == null)
                {
                    patientProfileRepository = new PrivateDutyPatientProfileRepository(this.database, this.connectionStringName);
                }
                return patientProfileRepository;
            }
        }

        private PrivateDutyBillingRepository billingRepository;
        public PrivateDutyBillingRepository BillingRepository
        {
            get
            {
                if (billingRepository == null)
                {
                    billingRepository = new PrivateDutyBillingRepository(this.database, this.connectionStringName);
                }
                return billingRepository;
            }
        }

        private PrivateDutyVitalSignRepository vitalSignRepository;
        public PrivateDutyVitalSignRepository VitalSignRepository
        {
            get
            {
                if (vitalSignRepository == null)
                {
                    vitalSignRepository = new PrivateDutyVitalSignRepository(this.database, this.connectionStringName);
                }
                return vitalSignRepository;
            }
        }

        private PrivateDutyMongoRepository mongoRepository;
        public PrivateDutyMongoRepository MongoRepository
        {
            get
            {
                if (mongoRepository == null)
                {
                    mongoRepository = new PrivateDutyMongoRepository(MongoManager.GetDataBase(AgencyServices.PrivateDuty.ToString()));
                }
                return mongoRepository;
            }
        }
        
        #endregion
    }

}

