﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;
    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Repositories;

    public class AgencyManagementDataProvider : IAgencyManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository baseDatabase;

        public AgencyManagementDataProvider()
        {
            this.baseDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None); 
            //new SimpleRepository(string.Format(CoreSettings.AgencyManagementConnectionString, ip), CoreSettings.AgencyManagementProviderName, SimpleRepositoryOptions.None);
        }

        #endregion

        #region AgencyCoreDataProvider Members

        private IReferralRepository referralRepository;
        public IReferralRepository ReferralRepository
        {
            get
            {
                if (referralRepository == null)
                {
                    referralRepository = new ReferralRepository(this.baseDatabase);
                }
                return referralRepository;
            }
        }

        private IPatientRepository patientRepository;
        public IPatientRepository PatientRepository
        {
            get
            {
                if (patientRepository == null)
                {
                    patientRepository = new PatientRepository(this.baseDatabase, "AgencyManagementConnectionString");
                }
                return patientRepository;
            }
        }

        private IPhysicianRepository physicianRepository;
        public IPhysicianRepository PhysicianRepository
        {
            get
            {
                if (physicianRepository == null)
                {
                    physicianRepository = new PhysicianRepository(this.baseDatabase);
                }
                return physicianRepository;
            }
        }

        private IUserRepository userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(this.baseDatabase, "AgencyManagementConnectionString");
                }
                return userRepository;
            }
        }

        private IAgencyRepository agencyRepository;
        public IAgencyRepository AgencyRepository
        {
            get
            {
                if (agencyRepository == null)
                {
                    agencyRepository = new AgencyRepository(this.baseDatabase);
                }
                return agencyRepository;
            }
        }

        private IMessageRepository messageRepository;
        public IMessageRepository MessageRepository
        {
            get
            {
                if (messageRepository == null)
                {
                    messageRepository = new MessageRepository(this.baseDatabase);
                }
                return messageRepository;
            }
        }

        private IAssetRepository assetRepository;
        public IAssetRepository AssetRepository
        {
            get
            {
                if (assetRepository == null)
                {
                    assetRepository = new AssetRepository(this.baseDatabase);
                }
                return assetRepository;
            }
        }


        #endregion
    }

}

