﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public class HHDataProvider : AgencyManagementDataProvider
    {

        #region Members and Properties

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public HHDataProvider()
        {
            this.database = new SimpleRepository("HomeHealthConnectionString");
            this.connectionStringName = "HomeHealthConnectionString";
        }

        #endregion

        #region HHDataProvider Members

        private HHTaskRepository taskRepository;
        public HHTaskRepository TaskRepository
        {
            get
            {
                if (taskRepository == null)
                {
                    taskRepository = new HHTaskRepository(this.database, this.connectionStringName);
                }
                return taskRepository;
            }
        }

        private HHNoteRepository noteRepository;
        public HHNoteRepository NoteRepository
        {
            get
            {
                if (noteRepository == null)
                {
                    noteRepository = new HHNoteRepository(this.database, this.connectionStringName);
                }
                return noteRepository;
            }
        }

        private HHMultiDocument multiDocument;
        public HHMultiDocument MultiDocument
        {
            get
            {
                if (multiDocument == null)
                {
                    multiDocument = new HHMultiDocument(this.database, this.connectionStringName);
                }
                return multiDocument;
            }
        }

        private HHEpisodeRepository hhEpisodeRepository;
        public HHEpisodeRepository EpisodeRepository
        {
            get
            {
                if (hhEpisodeRepository == null)
                {
                    hhEpisodeRepository = new HHEpisodeRepository(this.database, this.connectionStringName);
                }
                return hhEpisodeRepository;
            }
        }

        private HHAssessmentRepository hhAssessmentRepository;
        public HHAssessmentRepository AssessmentRepository
        {
            get
            {
                if (hhAssessmentRepository == null)
                {
                    hhAssessmentRepository = new HHAssessmentRepository(this.database, this.connectionStringName);
                }
                return hhAssessmentRepository;
            }
        }

        private HHPlanOfCareRepository hhPlanOfCareRepository;
        public HHPlanOfCareRepository PlanOfCareRepository
        {
            get
            {
                if (hhPlanOfCareRepository == null)
                {
                    hhPlanOfCareRepository = new HHPlanOfCareRepository(this.database, this.connectionStringName);
                }
                return hhPlanOfCareRepository;
            }
        }

        private HHBillingRepository hhBillingRepository;
        public HHBillingRepository BillingRepository
        {
            get
            {
                if (hhBillingRepository == null)
                {
                    hhBillingRepository = new HHBillingRepository(this.database, this.connectionStringName);
                }
                return hhBillingRepository;
            }
        }

        private HHPhysicianOrderRepository hhPhysicianOrderRepository;
        public HHPhysicianOrderRepository PhysicianOrderRepository
        {
            get
            {
                if (hhPhysicianOrderRepository == null)
                {
                    hhPhysicianOrderRepository = new HHPhysicianOrderRepository(this.database, this.connectionStringName);
                }
                return hhPhysicianOrderRepository;
            }
        }

        private HHCommunicationNoteRepository hhCommunicationNoteRepository;
        public HHCommunicationNoteRepository CommunicationNoteRepository
        {
            get
            {
                if (hhCommunicationNoteRepository == null)
                {
                    hhCommunicationNoteRepository = new HHCommunicationNoteRepository(this.database, this.connectionStringName);
                }
                return hhCommunicationNoteRepository;
            }
        }

        private HHIncidentAccidentRepository hhIncidentAccidentRepository;
        public HHIncidentAccidentRepository IncidentAccidentRepository
        {
            get
            {
                if (hhIncidentAccidentRepository == null)
                {
                    hhIncidentAccidentRepository = new HHIncidentAccidentRepository(this.database, this.connectionStringName);
                }
                return hhIncidentAccidentRepository;
            }
        }

        private HHInfectionRepository hhInfectionRepository;
        public HHInfectionRepository InfectionRepository
        {
            get
            {
                if (hhInfectionRepository == null)
                {
                    hhInfectionRepository = new HHInfectionRepository(this.database, this.connectionStringName);
                }
                return hhInfectionRepository;
            }
        }

        private FaceToFaceEncounterRepository faceToFaceEncounterRepository;
        public FaceToFaceEncounterRepository FaceToFaceEncounterRepository
        {
            get
            {
                if (faceToFaceEncounterRepository == null)
                {
                    faceToFaceEncounterRepository = new FaceToFaceEncounterRepository(this.database, this.connectionStringName);
                }
                return faceToFaceEncounterRepository;
            }
        }

        private HHPatientAdmissionRepository patientAdmissionRepository;
        public HHPatientAdmissionRepository PatientAdmissionRepository
        {
            get
            {
                if (patientAdmissionRepository == null)
                {
                    patientAdmissionRepository = new HHPatientAdmissionRepository(this.database, this.connectionStringName);
                }
                return patientAdmissionRepository;
            }
        }

        private HHPatientProfileRepository patientProfileRepository;
        public HHPatientProfileRepository PatientProfileRepository
        {
            get
            {
                if (patientProfileRepository == null)
                {
                    patientProfileRepository = new HHPatientProfileRepository(this.database, this.connectionStringName);
                }
                return patientProfileRepository;
            }
        }

        private HHVitalSignRepository vitalSignRepository;
        public HHVitalSignRepository VitalSignRepository
        {
            get
            {
                if (vitalSignRepository == null)
                {
                    vitalSignRepository = new HHVitalSignRepository(this.database, this.connectionStringName);
                }
                return vitalSignRepository;
            }
        }

        private HHMongoRepository mongoRepository;
        public HHMongoRepository MongoRepository
        {
            get
            {
                if (mongoRepository == null)
                {
                    mongoRepository = new HHMongoRepository(MongoManager.GetDataBase(AgencyServices.HomeHealth.ToString()));
                }
                return mongoRepository;
            }
        }

        #endregion

    }
}
