﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using SubSonic.Repository;


    public class PatientRepository : IPatientRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public PatientRepository(SimpleRepository database, string connectionStringName)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region IPatientRepository Members

        #region Patient

        public bool Add(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    patient.Created = DateTime.Now;
                    patient.Modified = DateTime.Now;
                    database.Add<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (pending != null)
                {
                    var patient = database.Single<Patient>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId);
                    if (patient != null)
                    {
                        //patient.AdmissionId = pending.AdmissionId;
                        //patient.StartofCareDate = pending.StartofCareDate;
                        //patient.ReferralDate = pending.ReferralDate;
                        //patient.CaseManagerId = pending.CaseManagerId;
                        //patient.UserId = pending.UserId;
                        //patient.AgencyLocationId = pending.AgencyLocationId;
                        //patient.PrimaryInsurance = pending.PrimaryInsurance;
                        //patient.SecondaryInsurance = pending.SecondaryInsurance;
                        //patient.TertiaryInsurance = pending.TertiaryInsurance;
                        //patient.Payer = pending.Payer;
                        //patient.Status = (int)PatientStatus.Active;

                        patient.PatientIdNumber = pending.PatientIdNumber;
                        patient.Modified = DateTime.Now;
                        if (database.Update<Patient>(patient) > 0)
                        {
                            patientOut = patient;
                            result = true;
                        }
                    }
                }

            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        //TODO: Make sure the referral information set on the profile
        public bool AdmitReferral(PendingPatient pending, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (pending != null)
                {
                    var referral = database.Single<Referral>(r => r.Id == pending.Id && r.AgencyId == pending.AgencyId);
                    if (referral != null)
                    {
                        var patient = new Patient
                        {
                            //AdmissionId = pending.AdmissionId,
                            //AgencyLocationId = pending.AgencyLocationId,
                            //UserId = pending.UserId,
                            //CaseManagerId = pending.CaseManagerId,
                            //PrimaryInsurance = pending.PrimaryInsurance,
                            //SecondaryInsurance = pending.SecondaryInsurance,
                            //TertiaryInsurance = pending.TertiaryInsurance,
                            //Payer = pending.Payer,
                            //Status = (int)PatientStatus.Active,
                            //StartofCareDate = pending.StartofCareDate,
                            Id = referral.Id,
                            PatientIdNumber = pending.PatientIdNumber,
                            AgencyId = referral.AgencyId,
                            ReferrerPhysician = referral.ReferrerPhysician,
                            //AdmissionSource = referral.AdmissionSource.ToString(),
                            //OtherReferralSource = referral.OtherReferralSource,
                            //InternalReferral = referral.InternalReferral,
                            //ReferralDate = pending.ReferralDate,
                            FirstName = referral.FirstName,
                            MiddleInitial = referral.MiddleInitial,
                            LastName = referral.LastName,
                            Gender = referral.Gender,
                            DOB = referral.DOB,
                            MaritalStatus = referral.MaritalStatus,
                            Height = referral.Height,
                            HeightMetric = referral.HeightMetric,
                            Weight = referral.Weight,
                            WeightMetric = referral.WeightMetric,
                            MedicareNumber = referral.MedicareNumber,
                            MedicaidNumber = referral.MedicaidNumber,
                            SSN = referral.SSN,
                            AddressLine1 = referral.AddressLine1,
                            AddressLine2 = referral.AddressLine2,
                            AddressCity = referral.AddressCity,
                            AddressStateCode = referral.AddressStateCode,
                            AddressZipCode = referral.AddressZipCode,
                            PhoneHome = referral.PhoneHome,
                            EmailAddress = referral.EmailAddress,
                            ServicesRequired = referral.ServicesRequired,
                            Ethnicities = string.Empty,
                            DME = referral.DME,
                            OtherDME = referral.OtherDME,
                            Comments = referral.Comments,
                            Modified = DateTime.Now
                        };
                        database.Add<Patient>(patient);
                        patientOut = patient;

                        referral.Status = (int)ReferralStatus.Admitted;

                        result = database.Update<Referral>(referral) > 0;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            bool result = false;
            try
            {
                if (pending != null)
                {
                    var patient = database.Single<Patient>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId);
                    if (patient != null)
                    {
                        //patient.NonAdmissionDate = pending.NonAdmitDate;
                        //patient.NonAdmissionReason = pending.Reason;
                        //patient.Status = (int)PatientStatus.NonAdmission;
                        //patient.AdmissionId = pending.AdmissionId;
                        patient.Comments = pending.Comments;
                        patient.Modified = DateTime.Now;
                        return database.Update<Patient>(patient) > 0;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DischargePatient(Guid agencyId, Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    //patient.DischargeDate = dischargeDate;
                    //patient.Status = (int)PatientStatus.Discharged;
                    //patient.DischargeReasonId = dischargeReasonId;
                    //patient.DischargeReason = dischargeReason;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Update(Patient patient, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (patient != null)
                {
                    patient.Modified = DateTime.Now;
                    if (database.Update<Patient>(patient) > 0)
                    {
                        patientOut = patient;
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateModal(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    patient.Modified = DateTime.Now;
                    if (database.Update<Patient>(patient) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Activate(Guid agencyId, Guid patientId)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    // patient.Status = (int)PatientStatus.Active;
                    patient.Modified = DateTime.Now;
                    if (database.Update<Patient>(patient) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    //patient.Status = (int)PatientStatus.Active;
                    //patient.StartofCareDate = startOfCareDate;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public bool Edit(Patient patientFromDB, Patient patientFromClient, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (patientFromDB != null && patientFromClient != null)
                {
                    //if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                    //{
                    //    patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                    //    patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                    //    patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                    //    patientToEdit.PrimaryRelationship = patient.PrimaryRelationship;
                    //}
                    //else patientToEdit.PrimaryHealthPlanId = patientToEdit.PrimaryGroupName = patientToEdit.PrimaryGroupId = string.Empty;

                    //if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                    //{
                    //    patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                    //    patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                    //    patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                    //    patientToEdit.SecondaryRelationship = patient.SecondaryRelationship;
                    //}
                    //else patientToEdit.SecondaryHealthPlanId = patientToEdit.SecondaryGroupName = patientToEdit.SecondaryGroupId = string.Empty;

                    //if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                    //{
                    //    patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                    //    patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                    //    patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                    //    patientToEdit.TertiaryRelationship = patient.TertiaryRelationship;
                    //}
                    //else patientToEdit.TertiaryHealthPlanId = patientToEdit.TertiaryGroupName = patientToEdit.TertiaryGroupId = string.Empty;

                    //patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                    //patientToEdit.StartofCareDate = patient.StartofCareDate;
                    //if (patient.Status == (int)PatientStatus.Discharged)
                    //{
                    //    patientToEdit.DischargeDate = patient.DischargeDate;
                    //    patientToEdit.DischargeReason = patient.DischargeReason;
                    //}
                    //patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                    //patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                    //patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                    //patientToEdit.CaseManagerId = patient.CaseManagerId;
                    //patientToEdit.UserId = patient.UserId;
                    //patientToEdit.AdmissionId = patient.AdmissionId;
                    patientFromDB.FirstName = patientFromClient.FirstName;
                    patientFromDB.LastName = patientFromClient.LastName;
                    patientFromDB.MiddleInitial = patientFromClient.MiddleInitial;
                    patientFromDB.Gender = patientFromClient.Gender;
                    patientFromDB.DOB = patientFromClient.DOB;
                    patientFromDB.MaritalStatus = patientFromClient.MaritalStatus;
                    patientFromDB.Height = patientFromClient.Height;
                    patientFromDB.HeightMetric = patientFromClient.HeightMetric;
                    patientFromDB.Weight = patientFromClient.Weight;
                    patientFromDB.WeightMetric = patientFromClient.WeightMetric;
                    patientFromDB.PatientIdNumber = patientFromClient.PatientIdNumber;
                    patientFromDB.MedicareNumber = patientFromClient.MedicareNumber;
                    patientFromDB.MedicaidNumber = patientFromClient.MedicaidNumber;
                    patientFromDB.SSN = patientFromClient.SSN;
                    patientFromDB.IsDNR = patientFromClient.IsDNR;

                    patientFromDB.Ethnicities = patientFromClient.Ethnicities;
                    //patientFromDB.PaymentSource = patientFromClient.PaymentSource;
                    //patientFromDB.OtherPaymentSource = patientFromClient.OtherPaymentSource;
                    patientFromDB.AddressLine1 = patientFromClient.AddressLine1;
                    patientFromDB.AddressLine2 = patientFromClient.AddressLine2;
                    patientFromDB.AddressCity = patientFromClient.AddressCity;
                    patientFromDB.AddressStateCode = patientFromClient.AddressStateCode;
                    patientFromDB.AddressZipCode = patientFromClient.AddressZipCode;
                    patientFromDB.PhoneHome = patientFromClient.PhoneHome;
                    patientFromDB.PhoneMobile = patientFromClient.PhoneMobile;
                    patientFromDB.EmailAddress = patientFromClient.EmailAddress;
                    patientFromDB.PharmacyName = patientFromClient.PharmacyName;
                    patientFromDB.PharmacyPhone = patientFromClient.PharmacyPhone;

                    patientFromDB.ReferrerPhysician = patientFromClient.ReferrerPhysician;
                   

                    patientFromDB.ServicesRequired = patientFromClient.ServicesRequired;
                    patientFromDB.DME = patientFromClient.DME;
                    //patientToEdit.Payer = patient.Payer;
                    patientFromDB.Triage = patientFromClient.Triage;
                    patientFromDB.EvacuationZone = patientFromClient.EvacuationZone;
                    patientFromDB.Comments = patientFromClient.Comments;
                  
                    patientFromDB.Modified = DateTime.Now;
                    if (database.Update<Patient>(patientFromDB) > 0)
                    {
                        patientOut = patientFromDB;
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (!patientId.IsEmpty() && !agencyId.IsEmpty() && !assetId.IsEmpty())
                {
                    var patient = database.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId );
                    if (patient != null)
                    {
                        patient.PhotoId = assetId;
                        patient.Modified = DateTime.Now;
                        database.Update<Patient>(patient);
                        patientOut = patient;
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool SetStatus(Guid agencyId, Guid patientId, PatientStatus status, AgencyServices service)
        {
            var script = @"UPDATE patients set @service = @status WHERE patients.AgencyId = @agencyid AND patients.Id = @id;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", patientId)
                     .AddString("service", service.ToString() + "Status")
                     .AddInt("status", (int)status)
                     .AsScalar();
            }
            return count > 0;
        }

        public bool RemovePatient(Guid agencyId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patients WHERE AgencyId = @agencyid AND  Id = @id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool DeprecatedPatient(Guid agencyId, Guid Id)
        {
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == Id);
                if (patient != null)
                {
                    //patient.IsDeprecated = true;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public string GetPatientNameById(Guid patientId, Guid agencyId)
        {
            var query = new QueryBuilder(string.Format(@"SELECT 
                                                            Id, 
                                                            FirstName, 
                                                            LastName, 
                                                            MiddleInitial 
                                                                FROM patients
                                                                    WHERE 
                                                                        AgencyId = @agencyid AND
                                                                        Id = @patientid"));
            var patient = new FluentCommand<Patient>(query.Build())
               .SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .SetMap(reader => new Patient
               {
                   Id = reader.GetGuid("Id"),
                   FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                   LastName = reader.GetString("LastName").Trim().ToUpper(),
                   MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper()
               }).AsSingle();

            string name = "";
            if (patient != null)
            {
                name = patient.DisplayNameWithMi;
            }
            return name;
        }

        public Patient GetPatientNameAndServicesById(Guid agencyId,Guid patientId)
        {
            var query = new QueryBuilder(string.Format(@"SELECT 
                                                            Id, 
                                                            FirstName, 
                                                            LastName, 
                                                            MiddleInitial,
                                                            Services 
                                                                FROM patients
                                                                    WHERE 
                                                                        AgencyId = @agencyid AND
                                                                        Id = @patientid"));
            return new FluentCommand<Patient>(query.Build())
               .SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .SetMap(reader => new Patient
               {
                   Id = reader.GetGuid("Id"),
                   FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                   LastName = reader.GetString("LastName").Trim().ToUpper(),
                   MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper(),
                   Services = (AgencyServices)reader.GetInt("Services"),
               }).AsSingle();
        }

        public Dictionary<Guid, string> GetPatientNamesByIds(Guid agencyId, List<Guid> patientIds)
        {
            var results = new Dictionary<Guid, string>();
            var ids = patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var query = new QueryBuilder(string.Format(@"SELECT 
                                                            Id, 
                                                            FirstName, 
                                                            LastName, 
                                                            MiddleInitial 
                                                                FROM patients
                                                                    WHERE 
                                                                        AgencyId = @agencyid AND
                                                                        Id in ({0})", ids));
            var list = new FluentCommand<Patient>(query.Build())
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Patient
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                    LastName = reader.GetString("LastName").Trim().ToUpper(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();

            if (list.IsNotNullOrEmpty())
            {
                results = list.ToDictionary(key => key.Id, value => value.DisplayNameWithMi);
            }
            return results;
        }

        public Patient GetPatientOnly(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == Id);
        }

        public IList<Patient> Find(Guid agencyId, int statusId, AgencyServices service)
        {
            var serviceScript = string.Empty;
            var statusScript = string.Empty;

            if (service == AgencyServices.None)
            {
                statusScript = string.Format(statusId > 0 ? " AND ( {0}Status = @statusId OR  {1}Status = @statusId ) " : " AND ( {0}Status IN (1,2) OR {1}Status IN (1,2) ) ", AgencyServices.HomeHealth.ToString(), AgencyServices.PrivateDuty.ToString());
            }
            else
            {
                serviceScript = " AND ((Services & @serviceId = Services) OR (Services & @serviceId = @serviceId))";
                if (statusId > 0)
                {
                    statusScript = string.Format(" AND {0}Status = @statusid ",service.ToString());
                }
                else if (service.IsAlone())
                {
                    statusScript = string.Format(" AND {0}Status IN (1,2) ", service.ToString());
                }
                else
                {
                    statusScript = string.Format(" AND ( {0}Status IN (1,2) OR {1}Status IN (1,2) ) ", AgencyServices.HomeHealth.ToString(), AgencyServices.PrivateDuty.ToString());
                }
            }
           
            var patients = new List<Patient>();
            var query = string.Format(@"SELECT
                                    Id,
                                    FirstName,
                                    LastName, 
                                    MiddleInitial
                                        FROM 
                                            patients
                                                Where 
                                                    AgencyId = @agencyId {0} {1} ",
                                                                      serviceScript,
                                                                      statusScript);
                using (var cmd = new FluentCommand<Patient>(query))
                {
                    patients = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyId", agencyId)
                        .AddInt("serviceId", (int)service)
                        .AddInt("statusid", statusId)
                        .SetMap(reader => new Patient
                        {
                            Id = reader.GetGuid("Id"),
                            FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                            LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                            MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial()
                        }).AsList();
                }
            return patients;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId, int status, AgencyServices service)
        {
            string statusScript = string.Empty;
            if (status <= 0)
            {
                statusScript = string.Format(" AND p.{0}Status IN (1,2) ", service.ToString());
            }
            else
            {
                statusScript = string.Format(" AND p.{0}Status = @status ", service.ToString());
            }

            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT
                            p.FirstName ,
                            p.LastName ,
                            p.MiddleInitial ,
                            p.PatientIdNumber ,  
                            p.AddressLine1 ,
                            p.AddressLine2 ,
                            p.AddressCity ,
                            p.AddressStateCode ,
                            p.AddressZipCode ,
                            p.PhoneHome ,
                            p.Gender
                                FROM 
                                    patients p
                                        INNER JOIN patientphysicians pp ON pp.PatientId = p.Id
                                            WHERE 
                                                p.AgencyId = @agencyId AND
                                                pp.PhysicianId = @agencyPhysicianId {0} AND
                                                p.{1}Status != @excludestatus AND
                                                p.Services & @service= @service  
                                                    ORDER BY p.LastName ASC , p.FirstName ASC", statusScript,service.ToString());
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("agencyPhysicianId", agencyPhysicianId)
                    .AddInt("status", status)
                    .AddInt("service", (int)service)
                    .AddInt("excludestatus",(int)PatientStatus.Deprecated)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PatientGender = reader.GetStringNullable("Gender")
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public string LastPatientId(Guid agencyId)
        {
            Patient patient = null;
            var query = new QueryBuilder("SELECT PatientIdNumber FROM patients").Where("patients.AgencyId = @agencyid and patients.IsDeprecated=0").OrderBy("Created", false).LimitTo(0, 1);

            using (var cmd = new FluentCommand<Patient>(query.Build()))
            {
                patient = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Patient
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsSingle();
            }

            if (patient != null && patient.PatientIdNumber.IsNotNullOrEmpty())
            {
                return patient.PatientIdNumber.Trim();
            }

            return string.Empty;
        }

        public bool IsPatientIdExist(Guid agencyId, string patientIdNumber)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.PatientIdNumber = @Idnumber AND (patients.HomeHealthStatus != @statusid OR patients.PrivateDutyStatus != @statusid ) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddString("Idnumber", patientIdNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsMedicareExist(Guid agencyId, string medicareNumber)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.MedicareNumber = @medicarenumber AND (patients.HomeHealthStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddString("medicarenumber", medicareNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsMedicaidExist(Guid agencyId, string medicaidNumber)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.MedicaidNumber = @medicaidnumber AND (patients.HomeHealthStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddString("medicaidnumber", medicaidNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsSSNExist(Guid agencyId, string ssn)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.SSN = @ssn AND (patients.HomeHealthStatus != @statusid OR patients.PrivateDutyStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddString("ssn", ssn)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsPatientIdExistForEdit(Guid agencyId, Guid patientId, string patientIdNumber)
        {
            var count = 0;
            const string script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.Id != @patientid AND patients.PatientIdNumber = @Idnumber AND (patients.HomeHealthStatus != @statusid OR patients.PrivateDutyStatus != @statusid ) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientid", patientId)
                   .AddString("Idnumber", patientIdNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsMedicareExistForEdit(Guid agencyId, Guid patientId, string medicareNumber)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.Id != @patientid AND patients.MedicareNumber = @medicarenumber AND (patients.HomeHealthStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientid", patientId)
                   .AddString("medicarenumber", medicareNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsMedicaidExistForEdit(Guid agencyId, Guid patientId, string medicaidNumber)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.Id != @patientid AND patients.MedicaidNumber = @medicaidnumber AND (patients.HomeHealthStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientid", patientId)
                   .AddString("medicaidnumber", medicaidNumber)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsSSNExistForEdit(Guid agencyId, Guid patientId, string ssn)
        {
            var count = 0;
            var script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.Id != @patientid AND patients.SSN = @ssn AND (patients.HomeHealthStatus != @statusid OR patients.PrivateDutyStatus != @statusid) limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                   .AddString("ssn", ssn)
                   .AddInt("statusId", (int)PatientStatus.Deprecated)
                   .AsScalar();
            }
            return count > 0;
        }

        public bool IsPatientExist(Guid agencyId, Guid patientId)
        {
            var count = 0;
            const string script = @"SELECT 1 FROM patients WHERE patients.AgencyId = @agencyid AND patients.Id = @patientid limit 1";
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientid", patientId)
                   .AsScalar();
            }
            return count > 0;
        }

        public long GetNextOrderNumber()
        {
            var orderNumber = new OrderNumber();
            orderNumber.Created = DateTime.Now;
            database.Add<OrderNumber>(orderNumber);
            return orderNumber.Id;
        }

        public AgencyServices GetAccessibleServices(Guid agencyId, Guid patientId)
        {
            AgencyServices service = AgencyServices.None;
            var script = string.Format(@"SELECT Services
                                            FROM 
                                                patients
                                                    WHERE 
                                                        AgencyId = @agencyid AND 
                                                        Id = @patientid limit 1");
            using (var cmd = new FluentCommand<AgencyServices>(script))
            {
                service = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => (AgencyServices)reader.GetInt("Services"))
                    .AsSingle();
            }
            return service;
        }

        public List<Patient> FindPatientVeryLeanByNameAndService(Guid agencyId, string term, int service)
        {
            string termScript = string.Empty;
            var splittedTerms = term.Split(' ');
            for (int i = 0; i < splittedTerms.Length; i++)
            {
                termScript += "LOWER(p.Name) like '%" + splittedTerms[i].ToLower() + "%'" + ((i + 1 < splittedTerms.Length) ?  " AND " : "");
            }
            var list = new List<Patient>();
            var script = string.Format(@"SELECT 
                                            p.Id, 
                                            p.FirstName,
                                            p.LastName, 
                                            p.MiddleInitial
                                        FROM 
                                            (SELECT Id, CONCAT(TRIM(LastName), ' ', TRIM(FirstName), IF(MiddleInitial <> '',CONCAT(' ',TRIM(MiddleInitial)),'')) as Name, FirstName, LastName, MiddleInitial FROM patients WHERE AgencyId = @agencyid AND @service = (Services & @service)) as p
                                        WHERE 
                                            {0}
                                        ORDER BY p.Name
                                        LIMIT 20", termScript);
            using (var cmd = new FluentCommand<Patient>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddInt("service", service)
                    .SetMap(reader => new Patient(){
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetString("FirstName"),
                        LastName = reader.GetString("LastName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial")
                    })
                .AsList();
            }
            return list;
        }

        public Guid GetCurrentPatientHospitalizationLog(Guid patientId, Guid agencyId)
        {
            Guid hospitalizationLogId;
            const string Script = @"Select HospitalizationId FROM patients WHERE AgencyId = @agencyid AND Id = @patientid limit 1";
            using (var cmd = new FluentCommand<Guid>(Script))
            {
                hospitalizationLogId = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => reader.GetGuidIncludeEmpty("HospitalizationId"))
                .AsSingle();
            }
            return hospitalizationLogId;
        }

        public bool ToggleHosipitalization(Guid patientId, Guid agencyId, Guid hospitalizationLogId, bool isHospitalized)
        {
            try
            {
                var script = @"UPDATE patients SET IsHospitalized = @hospitalized, HospitalizationId = @hospitalizationlogid, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<Guid>(script))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("hospitalizationlogid", hospitalizationLogId)
                    .AddBoolean("hospitalized", isHospitalized)
                    .AddDateTime("modified", DateTime.Now)  
                    .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Dictionary<string, string> GetPatientJsonByColumns(Guid agencyId, Guid id, params string[] columns)
        {
            return EntityHelper.SelectEntityJsonByColumns<Patient>(agencyId, id, connectionStringName, columns);
        }

        #endregion

        #region Physician Contact
        
        public bool Link(Guid patientId, Guid physicianId, bool isPrimary)
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                try
                {
                    var patientPhysician = new PatientPhysician
                    {
                        PatientId = patientId,
                        PhysicianId = physicianId,
                        IsPrimary = isPrimary
                    };
                    database.Add<PatientPhysician>(patientPhysician);
                    result = true;
                }
                catch (Exception )
                {
                    return false;
                }
            }
            return result;
        }

        public bool Unlink(Guid patientId, Guid physicianId)
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId && pp.PhysicianId == physicianId);
                result = true;
            }
            return result;
        }

        public bool UnlinkAll(Guid patientId)
        {
            var result = false;
            try
            {
                if (!patientId.IsEmpty())
                {
                    database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId);
                    result = true;
                }
            }
            catch (Exception )
            {
                return false;
            }
            return result;
        }

        public bool UpdateMultiplePatientPhysicians(List<PatientPhysician> patientPhysicians)
        {
            return database.UpdateMany(patientPhysicians) > 0;
        }

        public bool DeletePhysicianContact(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientphysicians WHERE PatientId = @patientid AND PhysicianId=@physicianid ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("patientid", patientId)
                    .AddGuid("physicianid", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;

            //bool result = false;
            //try
            //{
            //    var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == Id);
            //    if (patientPhysician != null)
            //    {
            //       return database.Delete<PatientPhysician>(patientPhysician.Id)>0;
            //    }
            //}
            //catch (Exception )
            //{
            //    return false;
            //}
            //return result;
        }

        public List<PatientPhysician> GetPatientPhysicians(Guid patientId)
        {
            return database.Find<PatientPhysician>(p => p.PatientId == patientId).ToList();
        }

        //public bool DoesPhysicianExist(Guid patientId, Guid physicianId)
        //{
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(physicianId, "physicianId");
        //    bool result = false;
        //    var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
        //    foreach (PatientPhysician physician in patientPhysician)
        //    {
        //        if (physician.PhysicianId == physicianId)
        //        {
        //            result = true;
        //            break;
        //        }
        //    }

        //    return result;
        //}

        public bool DoesPhysicianExist(Guid patientId, Guid physicianId)
        {
            var script = @"SELECT 
                             1
                                FROM 
                                    patientphysicians 
                                        WHERE 
                                            PatientId = @patientId AND
                                            PhysicianId = @physicianId
                                                limit 1";

            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("patientId", patientId)
                .AddGuid("physicianId", physicianId)
                .AsScalar();
            }
            return count > 0;
        }


        #endregion

        #region Emergency Contact

        public bool AddEmergencyContact(PatientEmergencyContact emergencyContactInfo)
        {
            var result = false;
            try
            {
                if (emergencyContactInfo != null)
                {
                    emergencyContactInfo.Id = Guid.NewGuid();
                    emergencyContactInfo.Encode();
                    database.Add<PatientEmergencyContact>(emergencyContactInfo);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            try
            {
                if (emergencyContact != null)
                {
                    var emergencyContactToEdit = database.Single<PatientEmergencyContact>(e => e.PatientId == emergencyContact.PatientId && e.Id == emergencyContact.Id);
                    if (emergencyContactToEdit != null)
                    {
                        emergencyContact.Encode();
                        emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                        emergencyContactToEdit.LastName = emergencyContact.LastName;
                        emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                        emergencyContactToEdit.AlternatePhone = emergencyContact.AlternatePhone;
                        emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                        emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                        if (emergencyContact.IsPrimary != emergencyContactToEdit.IsPrimary && emergencyContact.IsPrimary)
                        {
                            if (SetPrimaryEmergencyContact(agencyId, emergencyContact.PatientId, emergencyContact.Id))
                            {
                                emergencyContactToEdit.IsPrimary = true;
                            }
                        }
                        else
                        {
                            emergencyContactToEdit.IsPrimary = emergencyContact.IsPrimary;
                        }
                        return database.Update<PatientEmergencyContact>(emergencyContactToEdit) > 0;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateEmergencyContact(Guid agencyId, Guid patientId, PatientEmergencyContact emergencyContact)
        {
           
            bool result = false;
            if (emergencyContact != null)
            {
                emergencyContact.Encode();
                try
                {
                    var script = string.Format(@"UPDATE 
                                                    patientemergencycontacts
                                                      SET  
                                                        FirstName = @firstname,
                                                        LastName = @lastname,
                                                        PrimaryPhone = @primaryphone,
                                                        AlternatePhone = @alternatephone,
                                                        EmailAddress = @emailaddress,
                                                        Relationship = @relationship
                                                             WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id;");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("id", emergencyContact.Id)
                        .AddString("firstname", emergencyContact.FirstName)
                        .AddString("lastname", emergencyContact.LastName)
                        .AddString("primaryphone", emergencyContact.PrimaryPhone)
                        .AddString("alternatephone", emergencyContact.AlternatePhone)
                        .AddString("emailaddress", emergencyContact.EmailAddress)
                        .AddString("relationship", emergencyContact.Relationship)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception )
                {
                    return false;
                }
            }
            return result;
        }

        public bool RemoveEmergencyContact(Guid agencyId, Guid patientId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientemergencycontacts WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id=@id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception )
            {
                return false;
            }
            return result;
        }

        public bool RemoveEmergencyContacts(Guid agencyId, Guid patientId)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  patientemergencycontacts WHERE AgencyId = @agencyid AND PatientId = @patientid ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId)
        {

            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE patientemergencycontacts SET IsPrimary = CASE WHEN Id = @id THEN 1 ELSE 0 END WHERE AgencyId = @agencyid AND PatientId = @patientid ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", emergencyContactId)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
            //bool result = false;
            //try
            //{
            //    var patient = database.Single<Patient>(p => p.Id == patientId);
            //    if (patient != null)
            //    {
            //        var emergencyContacts = database.Find<PatientEmergencyContact>(p => p.PatientId == patientId);
            //        bool flag = false;
            //        foreach (PatientEmergencyContact contat in emergencyContacts)
            //        {
            //            if (contat.Id == emergencyContactId)
            //            {
            //                contat.IsPrimary = true;
            //                flag = true;
            //            }
            //            else
            //            {
            //                contat.IsPrimary = false;
            //            }
            //        }
            //        if (flag)
            //        {
            //            database.UpdateMany<PatientEmergencyContact>(emergencyContacts);
            //            result = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}
            //return result;
        }

        public PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactId)
        {
            Check.Argument.IsNotEmpty(emergencyContactId, "emergencyContactId");
            return database.Single<PatientEmergencyContact>(e => e.PatientId == patientId && e.Id == emergencyContactId);
        }

        public PatientEmergencyContact GetFirstEmergencyContactByPatient(Guid agencyId, Guid patientId)
        {
            var emergencyContact = new PatientEmergencyContact();
            var script = string.Format(@"SELECT 
                                FirstName as FirstName,
                                LastName as LastName ,
                                Relationship as Relationship ,
                                PrimaryPhone as PrimaryPhone ,
                                EmailAddress as EmailAddress 
                                    FROM
                                        patientemergencycontacts 
                                            WHERE 
                                                AgencyId = @agencyId  AND 
                                                PatientId = @patientId 
                                                    Order BY IsPrimary DESC 
                                                        Limit 1");
            using (var cmd = new FluentCommand<PatientEmergencyContact>(script))
            {
                emergencyContact = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => new PatientEmergencyContact
                    {
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        Relationship = reader.GetStringNullable("Relationship"),
                        PrimaryPhone = reader.GetStringNullable("PrimaryPhone"),
                        EmailAddress = reader.GetStringNullable("EmailAddress")
                    })
                    .AsSingle();
            }
            return emergencyContact;
        }

        public IList<PatientEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEmergencyContact>(p => p.AgencyId == agencyId && p.PatientId == patientId).OrderBy(p => p.IsPrimary == false).ToList();
        }


        #endregion

//        #region Authorization

//        public bool AddAuthorization(Authorization authorization)
//        {
//            var result = false;
//            try
//            {
//                if (authorization != null)
//                {
//                    authorization.Created = DateTime.Now;
//                    authorization.Modified = DateTime.Now;
//                    database.Add<Authorization>(authorization);
//                    result = true;
//                }
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return result;
//        }

//        public bool EditAuthorization(Authorization authorization)
//        {
//            var result = false;
//            try
//            {
//                var authorizationToEdit = database.Single<Authorization>(a => a.AgencyId == authorization.AgencyId && a.PatientId == authorization.PatientId && a.Id == authorization.Id);
//                if (authorizationToEdit != null)
//                {
//                    authorizationToEdit.StartDate = authorization.StartDate;
//                    authorizationToEdit.EndDate = authorization.EndDate;
//                    authorizationToEdit.AgencyLocationId = authorization.AgencyLocationId;
//                    authorizationToEdit.Status = authorization.Status;
//                    authorizationToEdit.Insurance = authorization.Insurance;
//                    authorizationToEdit.Number1 = authorization.Number1;
//                    authorizationToEdit.Number2 = authorization.Number2;
//                    authorizationToEdit.Number3 = authorization.Number3;
//                    authorizationToEdit.SNVisit = authorization.SNVisit;
//                    authorization.SNVisitCountType = authorization.SNVisitCountType;
//                    authorizationToEdit.PTVisit = authorization.PTVisit;
//                    authorizationToEdit.PTVisitCountType = authorization.PTVisitCountType;
//                    authorizationToEdit.OTVisit = authorization.OTVisit;
//                    authorizationToEdit.OTVisitCountType = authorization.OTVisitCountType;
//                    authorizationToEdit.STVisit = authorization.STVisit;
//                    authorizationToEdit.STVisitCountType = authorization.STVisitCountType;
//                    authorizationToEdit.MSWVisit = authorization.MSWVisit;
//                    authorizationToEdit.MSWVisitCountType = authorization.MSWVisitCountType;
//                    authorizationToEdit.HHAVisit = authorization.HHAVisit;
//                    authorizationToEdit.HHAVisitCountType = authorization.HHAVisitCountType;

//                    authorizationToEdit.DieticianVisit = authorization.DieticianVisit;
//                    authorizationToEdit.DieticianVisitCountType = authorization.DieticianVisitCountType;

//                    authorizationToEdit.RNVisit = authorization.RNVisit;
//                    authorizationToEdit.RNVisitCountType = authorization.RNVisitCountType;

//                    authorizationToEdit.LVNVisit = authorization.LVNVisit;
//                    authorizationToEdit.LVNVisitCountType = authorization.LVNVisitCountType;
//                    authorizationToEdit.Comments = authorization.Comments;

//                    authorizationToEdit.Modified = DateTime.Now;
//                    database.Update<Authorization>(authorizationToEdit);
//                    result = true;
//                }
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return result;
//        }

//        public bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id)
//        {
//            var result = false;
//            try
//            {
//                var authorization = database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
//                if (authorization != null)
//                {
//                    authorization.IsDeprecated = true;
//                    authorization.Modified = DateTime.Now;
//                    return database.Update<Authorization>(authorization) > 0;
//                }
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return result;
//        }

//        public Authorization GetAuthorization(Guid agencyId, Guid Id)
//        {
//            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.Id == Id);
//        }

//        public Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id)
//        {
//            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
//        }

//        public IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId, int service)
//        {
//            var authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && ((int)a.Service) == service && a.IsDeprecated == false);
//            return authorizations.OrderBy(a => a.StartDate).ToList();
//        }

//        public IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, int insuranceId, string status, int service, DateTime startDate, DateTime endDate)
//        {
//            var authorizations = new List<Authorization>();
//            var script = string.Format(@"SELECT
//                                Id ,
//                                Number1 ,
//                                Number2 , 
//                                Number3 , 
//                                StartDate ,
//                                EndDate 
//                                    FROM 
//                                        authorizations 
//                                            WHERE 
//                                                AgencyId = @agencyId AND
//                                                PatientId = @patientId  AND
//                                                Insurance = @insuranceId  AND
//                                                Status = @status  AND
//                                                ((DATE(@startdate) between DATE(StartDate) and Date (EndDate)) OR ( DATE(@enddate) between DATE(StartDate) and Date (EndDate)) OR (DATE(StartDate) between  DATE(@startdate) and Date (@enddate)) OR (DATE(EndDate) between  DATE(@startdate) and Date (@enddate))) 
//                                                    ORDER BY StartDate ASC");
//            using (var cmd = new FluentCommand<Authorization>(script))
//            {
//                authorizations = cmd.SetConnection("AgencyManagementConnectionString")
//                    .AddGuid("agencyId", agencyId)
//                    .AddGuid("patientId", patientId)
//                    .AddInt("insuranceId", insuranceId)
//                    .AddString("status", status)
//                    .AddDateTime("startdate", startDate)
//                    .AddDateTime("enddate", endDate)
//                    .SetMap(reader => new Authorization
//                    {
//                        Id = reader.GetGuid("Id"),
//                        Number1 = reader.GetStringNullable("Number1"),
//                        Number2 = reader.GetStringNullable("Number2"),
//                        Number3 = reader.GetStringNullable("Number3"),
//                        StartDate = reader.GetDateTime("StartDate"),
//                        EndDate = reader.GetDateTime("EndDate")
//                    })
//                    .AsList();
//            }
//            return authorizations;
//        }

//        #endregion

        #region Medication Profile

        public bool AddNewMedicationProfile(MedicationProfile medication)
        {
            var result = false;
            try
            {
                database.Add<MedicationProfile>(medication);
                result = true;
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public bool SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            try
            {
                var count = 0;
                var script = @"UPDATE medicationprofiles SET PharmacyName = @pharmacyname,PharmacyPhone=@pharmacyphone, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    count= cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", medicationProfile.AgencyId)
                    .AddGuid("patientid", medicationProfile.PatientId)
                    .AddGuid("id", medicationProfile.Id)
                    .AddDateTime("modified", DateTime.Now)
                    .AddString("pharmacyname", medicationProfile.PharmacyName)
                    .AddString("pharmacyphone", medicationProfile.PharmacyPhone)
                    .AsNonQuery();
                }
                return count > 0;

            }
            catch (Exception)
            {
                return false;
            }

            //try
            //{
            //    var medication = this.GetMedicationProfile(medicationProfile.Id, medicationProfile.AgencyId);
            //    if (medication != null)
            //    {
            //        medication.PharmacyName = medicationProfile.PharmacyName;
            //        medication.PharmacyPhone = medicationProfile.PharmacyPhone;
            //        medication.Modified = DateTime.Now;
            //        return database.Update<MedicationProfile>(medication) > 0;
            //    }
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
            //return false;
        }

        public bool UpdateMedication(MedicationProfile medicationProfile)
        {
            var result = false;
            try
            {
                if (medicationProfile != null)
                {
                    return database.Update<MedicationProfile>(medicationProfile) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        //public bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        //{
        //    try
        //    {
        //        var medicationProfile = this.GetMedicationProfile(Id, agencyId);
        //        string medicationString = string.Empty;
        //        if (medicationProfile != null)
        //        {
        //            medicationString = medicationProfile.Medication;
        //        }
        //        if (medicationString.IsNotNullOrEmpty() && medicationString != "")
        //        {
        //            var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
        //            if (existingList != null && existingList.Count > 0 && existingList.Exists(m => m.Id == medication.Id))
        //            {
        //                var med = existingList.Single(m => m.Id == medication.Id);
        //                if (med.StartDate.Date != medication.StartDate.Date || med.Route != medication.Route || med.Frequency != medication.Frequency || med.MedicationDosage != medication.MedicationDosage)
        //                {
        //                    medication.Id = Guid.NewGuid();
        //                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
        //                    medication.MedicationType = new MedicationType { Value = "C", Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
        //                    existingList.Add(medication);
        //                    med.MedicationCategory = "DC";
        //                    med.DCDate = DateTime.Now;
        //                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
        //                    database.Update<MedicationProfile>(medicationProfile);
        //                    return true;
        //                }
        //                else
        //                {
        //                    med.IsLongStanding = medication.IsLongStanding;
        //                    med.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
        //                    med.Classification = medication.Classification;
        //                    if (med.MedicationCategory == "DC")
        //                    {
        //                        med.DCDate = medication.DCDate;
        //                    }
        //                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
        //                    database.Update<MedicationProfile>(medicationProfile);
        //                    return true;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return false;
        //}

        //public bool UpdateMedicationForDischarge(Guid medId, Guid agencyId, Guid Id, DateTime dischargeDate)
        //{
        //    bool result = false;
        //    try
        //    {
        //        var medicationProfile = this.GetMedicationProfile(medId, agencyId);
        //        string medicationString = "";
        //        if (medicationProfile != null)
        //        {
        //            medicationString = medicationProfile.Medication;
        //        }
        //        if (medicationString.IsNotNullOrEmpty() && medicationString != "")
        //        {
        //            var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
        //            if (existingList.Exists(m => m.Id == Id))
        //            {
        //                var med = existingList.Single(m => m.Id == Id);
        //                med.DCDate = dischargeDate;
        //                med.LastChangedDate = DateTime.Now;
        //                med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
        //                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
        //                database.Update<MedicationProfile>(medicationProfile);
        //                result = true;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //    return result;
        //}

        public bool RemoveMedicationProfile(Guid agencyId, Guid patientId, Guid Id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  medicationprofiles WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id ;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        //public MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        //{
        //    var medicationProfile = this.GetMedicationProfile(Id, agencyId);
        //    if (medicationProfile != null)
        //    {
        //        medication.Id = Guid.NewGuid();
        //        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
        //        medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
        //        if (medicationProfile.Medication.IsNullOrEmpty())
        //        {
        //            var newList = new List<Medication>() { medication };
        //            medicationProfile.Medication = newList.ToXml();
        //            database.Update<MedicationProfile>(medicationProfile);
        //        }
        //        else
        //        {
        //            var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
        //            existingList.Add(medication);
        //            medicationProfile.Medication = existingList.ToXml();
        //            database.Update<MedicationProfile>(medicationProfile);
        //        }
        //    }
        //    return medicationProfile;
        //}

        //public MedicationProfile DeleteMedication(Guid medId, Guid agencyId, Medication medication)
        //{
        //    var medicationProfile = this.GetMedicationProfile(medId, agencyId);
        //    string medicationString = "";
        //    if (medicationProfile != null)
        //    {
        //        medicationString = medicationProfile.Medication;
        //    }
        //    if (medicationProfile != null)
        //    {
        //        medicationString = medicationProfile.Medication;
        //    }
        //    if (!medicationString.IsNotNullOrEmpty())
        //    {
        //        return medicationProfile;
        //    }
        //    else
        //    {
        //        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
        //        if (existingList.Exists(m => m.Id == medication.Id))
        //        {
        //            existingList.RemoveAll(m => m.Id == medication.Id);
        //            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
        //            database.Update<MedicationProfile>(medicationProfile);
        //        }
        //    }
        //    return medicationProfile;
        //}

        public MedicationProfile GetMedicationProfileByPatient(Guid patientId, Guid AgencyId)
        {
            var medProfiles = database.Find<MedicationProfile>(m => m.AgencyId == AgencyId && m.PatientId == patientId);
            if (medProfiles != null)
            {
                return medProfiles.FirstOrDefault();
            }
            return null;
        }

        public bool IsMedicationProfileExist(Guid AgencyId, Guid patientId)
        {
            return database.Exists<MedicationProfile>(m => m.AgencyId == AgencyId && m.PatientId == patientId);
        }

        public MedicationProfile GetMedicationProfile(Guid Id, Guid agencyId)
        {
            return database.Single<MedicationProfile>(m => m.AgencyId == agencyId && m.Id == Id);
        }

        #endregion

        #region  Medication Profile History

        public bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory)
        {
            try
            {
                database.Add<MedicationProfileHistory>(medicationHistory);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfileHistory)
        {
            try
            {
                if (medicationProfileHistory != null)
                {
                    medicationProfileHistory.Modified = DateTime.Now;
                    return database.Update<MedicationProfileHistory>(medicationProfileHistory) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId)
        {
            return database.Single<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.Id == Id);
        }

        public MedicationProfileHistory GetSignedMedicationAssocatiedToAssessment(Guid patientId, Guid assessmentId)
        {
            var medProfileHistories = database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AssociatedAssessment == assessmentId && m.IsDeprecated == false && m.SignedDate > DateTime.MinValue);
            DateTime greatestDate = DateTime.MinValue;
            MedicationProfileHistory newestMedProfileHistory = null;
            medProfileHistories.ForEach((m) =>
            {
                if (m.SignedDate > greatestDate)
                {
                    greatestDate = m.SignedDate;
                    newestMedProfileHistory = m;
                }
            });
            return newestMedProfileHistory;
        }

        public MedicationProfileHistory GetSignedMedicationProfileForPatientByEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            return database.Find<MedicationProfileHistory>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.EpisodeId == episodeId && m.IsDeprecated == false && m.SignedDate > DateTime.MinValue).SingleOrDefault();
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId)
        {
            return database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AgencyId == agencyId && m.IsDeprecated == false);
        }

        //public IList<MedicationProfileHistory> GetAllMedicationProfileHistory()
        //{
        //    return database.All<MedicationProfileHistory>().ToList();
        //}

        #endregion

        #region CarePlanOversight

        //public bool AddCPO(CarePlanOversight cpo)
        //{
        //    var result = false;
        //    if (cpo != null)
        //    {
        //        cpo.Id = Guid.NewGuid();
        //        cpo.Created = DateTime.Now;
        //        cpo.Modified = DateTime.Now;
        //        try
        //        {
        //            database.Add<CarePlanOversight>(cpo);
        //            result = true;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return result;
        //}

        //public bool DeleteCPO(Guid id)
        //{
        //    var cpo = database.Single<CarePlanOversight>(c => c.Id == id);
        //    if (cpo != null)
        //    {
        //        cpo.IsDeprecated = true;
        //        cpo.Modified = DateTime.Now;
        //        return database.Update<CarePlanOversight>(cpo) > 0;
        //    }
        //    return false;
        //}

        //public bool UpdateCPO(CarePlanOversight cpo)
        //{
        //    var result = false;
        //    if (cpo != null)
        //    {
        //        cpo.Modified = DateTime.Now;
        //        try
        //        {
        //            return database.Update<CarePlanOversight>(cpo) > 0;
        //        }
        //        catch (Exception)
        //        {
        //            return false;
        //        }
        //    }
        //    return result;
        //}

        #endregion

        #region Allergies

        public bool AddAllergyProfile(AllergyProfile allergyProfile)
        {
            if (allergyProfile != null)
            {
                allergyProfile.Created = DateTime.Now;
                allergyProfile.Modified = DateTime.Now;
                database.Add<AllergyProfile>(allergyProfile);
                return true;
            }
            return false;
        }

        public bool UpdateAllergyProfile(AllergyProfile allergyProfile)
        {
            if (allergyProfile != null)
            {
                allergyProfile.Modified = DateTime.Now;
                return database.Update<AllergyProfile>(allergyProfile) > 0;
            }
            return false;
        }

        public AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId)
        {
            var profile = database.Single<AllergyProfile>(m => m.AgencyId == agencyId && m.PatientId == patientId);

            if (profile == null)
            {
                profile = new AllergyProfile
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    PatientId = patientId,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Allergies = new List<Allergy>().ToXml()
                };
                database.Add<AllergyProfile>(profile);
            }

            return profile;
        }

        public AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId)
        {
            return database.Single<AllergyProfile>(a => a.AgencyId == agencyId && a.Id == profileId);
        }

        #endregion

        #region Hospitalization Log

        public bool AddHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                database.Add<HospitalizationLog>(transferLog);
                result = true;
            }
            return result;
        }

        public bool UpdateHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                return database.Update<HospitalizationLog>(transferLog) > 0;
            }
            return result;
        }

        public HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId)
        {
            return database.Single<HospitalizationLog>(t => t.AgencyId == agencyId && t.PatientId == patientId && t.Id == transferLogId);
        }

        public List<HospitalizationLog> GetHospitalizationLogs( Guid agencyId,Guid patientId)
        {
            return database.Find<HospitalizationLog>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.IsDeprecated == false).ToList();
        }

        public List<PatientHospitalizationData> GetHospitalizedPatients(Guid agencyId)
        {
            var list = new List<PatientHospitalizationData>();
            var script = @"SELECT 
                            patients.FirstName,
                            patients.LastName, 
                            patients.PatientIdNumber,
                            hospitalizationlogs.HospitalizationDate, 
                            hospitalizationlogs.SourceId,
                            hospitalizationlogs.Id,
                            hospitalizationlogs.LastHomeVisitDate, 
                            hospitalizationlogs.UserId, 
                            hospitalizationlogs.PatientId FROM `patients` 
                                INNER JOIN
                                    hospitalizationlogs ON patients.HospitalizationId = hospitalizationlogs.Id
                                        WHERE
                                            patients.AgencyId = @agencyid AND
                                            patients.IsHospitalized = 1 AND
                                            patients.IsDeprecated = 0 AND
                                            hospitalizationlogs.IsDeprecated = 0";

            using (var cmd = new FluentCommand<PatientHospitalizationData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientHospitalizationData
                {
                    Id = reader.GetGuid("Id"),
                    UserId = reader.GetGuid("UserId"),
                    SourceId = reader.GetInt("SourceId"),
                    PatientId = reader.GetGuid("PatientId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastHomeVisitDate = reader.GetDateTime("LastHomeVisitDate"),
                    HospitalizationDate = reader.GetDateTime("HospitalizationDate")
                })
                .AsList();
            }
            return list;
        }

        public bool MarkEndOfHospitalization(Guid id, Guid patientId, Guid agencyId, DateTime returnDate)
        {
            try
            {
                var script = @"UPDATE hospitalizationlogs SET ReturnDate = @returndate, Modified = @modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<Guid>(script))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", id)
                    .AddDateTime("modified", DateTime.Now)                    
                    .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

       #endregion

        #region UserAccess

        public bool AddPatientUser(PatientUser patientUser)
        {
            patientUser.Id = Guid.NewGuid();
            database.Add<PatientUser>(patientUser);
            return true;
        }

        public bool RemovePatientUser(PatientUser patientUser)
        {
            database.Delete<PatientUser>(patientUser.Id);
            return true;
        }

        public PatientUser GetPatientUser(Guid patientUserId)
        {
            return database.Single<PatientUser>(pu => pu.Id == patientUserId);
        }

        public List<PatientUser> GetPatientUsersByUser(Guid userId)
        {
            return database.Find<PatientUser>(pu => pu.UserId == userId).ToList();
        }

        public List<PatientUser> GetPatientUsersByPatients(List<Guid> patientIds)
        {
            var list = new List<PatientUser>();
            var ids = patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var script = string.Format(@"SELECT
                                           *
                                            FROM 
                                              patientusers
                                                WHERE 
                                                    PatientId IN ( {0} }", ids);
            using (var cmd = new FluentCommand<PatientUser>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AsList();
            }
            return list;
        }

        public List<PatientSelection> GetPatientsWithUserAccess(Guid userId, Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name)
        {
            var query = new QueryBuilder(@"SELECT
                    patients.Id,
                    patients.PatientIdNumber,
                    patients.FirstName,
                    patients.LastName,
                    patients.MiddleInitial
                        FROM 
                            patientusers 
                                JOIN 
                                    patients on patientusers.patientid = patients.id");
                                    query
                                        .Where("patientusers.userid = @userId")
                                        .And("patients.AgencyId = @agencyId")
                                        .And("patients.Status = @statusId");

            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId");
            }
            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("patients.PaymentSource like '%{0};%'", paymentSourceId));
            }
            query
                .And("patients.IsDeprecated = 0")
                .OrderBy("patients.LastName", true);

            return new FluentCommand<PatientSelection>(query.Build())
            .SetConnection("AgencyManagementConnectionString")
            .AddGuid("userId", userId)
            .AddGuid("agencyId", agencyId)
            .AddInt("statusId", statusId)
            .AddInt("paymentSouceId", paymentSourceId)
            .AddGuid("branchId", branchId)
            .SetMap(reader => new PatientSelection
            {
                Id = reader.GetGuid("Id"),
                PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                LastName = reader.GetString("LastName").Trim().ToUpper(),
                MI = reader.GetStringNullable("MiddleInitial").ToUpper()
            })
            .AsList();
        }
        
        #endregion

        #region PatientAccess

        public List<SelectedPatient> GetPatientAccessList(Guid agencyId, Guid userId)
        {
            var query =string.Format(@"SELECT 
                patients.id as PatientId,
                patients.FirstName as PatientFirstName, 
                patients.LastName as PatientLastName,
                COALESCE(pu.id, '{0}') as PatientUserId 
                    FROM 
                    patients 
                      LEFT JOIN (SELECT patientusers.Id, patientusers.patientid 
                        FROM patientusers
                             WHERE patientusers.userid = @userid) pu ON patients.id = pu.patientid
                                    WHERE
                                        patients.AgencyId = @agencyid AND 
                                        patients.IsDeprecated = false", Guid.Empty);
            var list = new List<SelectedPatient>();
            using (var cmd = new FluentCommand<SelectedPatient>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("userid", userId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new SelectedPatient
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        FirstName = reader.GetString("PatientFirstName"),
                        LastName = reader.GetString("PatientLastname"),
                        PatientUserId = reader.GetGuid("PatientUserId")
                    }).AsList();
            }
            return list;
        }
        
        #endregion
      
        #region Photo

        public Guid GetPhotoId(Guid agencyId, Guid patientId)
        {
            Guid photoId;
            try
            {
                var script = @"SELECT PhotoId FROM patients WHERE AgencyId = @agencyid AND Id = @patientid limit 1";
                using (var cmd = new FluentCommand<Guid>(script))
                {
                    photoId = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => reader.GetGuidIncludeEmpty("PhotoId"))
                    .AsSingle();
                }
            }
            catch (Exception)
            {
                return Guid.Empty;
            }
            return photoId;
        }

        public bool SetPhotoId(Guid agencyId, Guid patientId, Guid assetId)
        {
            bool result;
            try
            {
                var script = string.Format(@"UPDATE patients p set p.PhotoId = @assetid WHERE p.AgencyId = @agencyid AND p.Id = @patientid");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("assetid", assetId)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        #endregion

        #endregion

    }
}
