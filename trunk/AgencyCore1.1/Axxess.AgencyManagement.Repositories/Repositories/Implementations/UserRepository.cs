﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories.Extensions;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
  
    using SubSonic.Repository;
    using MongoDB.Driver;
    using MongoDB.Driver.Builders;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization;


    public class UserRepository : IUserRepository
    {
        #region Fields

        private readonly SimpleRepository database;
        private readonly MongoDatabase mongoDb;
        private readonly string connectionStringName;
        #endregion

        #region Constructors and Destructors

        public UserRepository(SimpleRepository database, string connectionStringName)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.mongoDb = MongoManager.GetDataBase("AgencyManagement");
            this.connectionStringName = connectionStringName;
        }

        #endregion

        #region Public Methods and Operators

        public bool AddModel(User user)
        {
            bool result = false;
            try
            {
                user.Created = DateTime.Now;
                user.Modified = DateTime.Now;
                database.Add(user);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }
       
        public bool AddUserProfile(UserProfile userProfile)
        {
            bool result = false;
            try
            {
                database.Add(userProfile);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveProfile(Guid agencyId, Guid userId)
        {
            return EntityHelper.RemoveEntity<UserProfile>(agencyId, userId, this.connectionStringName);
        }

        public bool AddLicense(License licenseItem)
        {
            try
            {
                var result = false;
                if (licenseItem != null)
                {
                    this.database.Add(licenseItem);
                    result = true;
                }
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNonUser(NonUser nonUser)
        {
            try
            {
                if (nonUser != null)
                {
                    nonUser.Id = Guid.NewGuid();
                    nonUser.Created = DateTime.Now;
                    nonUser.Modified = DateTime.Now;
                    this.database.Add(nonUser);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public IEnumerable<User> All()
        {
            return this.database.All<User>();
        }

        public bool Delete(Guid agencyId, Guid userId)
        {
            var user = this.database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);
            if (user != null)
            {
                user.IsDeprecated = true;
                user.Modified = DateTime.Now;
                var val = this.database.Update(user);
                if (val > 0)
                {
                    UserEngine.AddOrUpdate(agencyId, user);
                    return true;
                }
            }
            return false;
        }

        public bool Remove(Guid agencyId, Guid userId)
        {
            return EntityHelper.RemoveEntity<User>(agencyId, userId, this.connectionStringName);
        }
           

        public bool DeleteLicense(Guid licenseId, Guid userId, Guid agencyId)
        {
            return EntityHelper.ToggleUserEntityDeprecation<License>(licenseId, userId, agencyId, true, this.connectionStringName);
        }

        public bool DeleteNonUser(Guid id, Guid agencyId)
        {
            return EntityHelper.ToggleEntityDeprecation<NonUser>(id, agencyId, true, this.connectionStringName);
        }

        //public User Get(Guid id, Guid agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");

        //    if (!id.IsEmpty())
        //    {
        //        var user = this.database.Single<User>(u => u.AgencyId == agencyId && u.Id == id);
        //        if (user != null)
        //        {
        //            user.Profile = user.ProfileData.ToObject<UserProfile>();

        //            if (user.Permissions.IsNotNullOrEmpty())
        //            {
        //                user.PermissionsArray = user.Permissions.ToObject<List<string>>();
        //            }
        //            else
        //            {
        //                user.PermissionsArray = new List<string>();
        //            }

        //            if (user.Licenses.IsNotNullOrEmpty())
        //            {
        //                user.LicensesArray = user.Licenses.ToObject<List<License>>();
        //            }
        //            else
        //            {
        //                user.LicensesArray = new List<License>();
        //            }

        //            if (user.Rates.IsNotNullOrEmpty())
        //            {
        //                user.RatesArray = user.Rates.ToObject<List<UserRate>>();
        //            }
        //            else
        //            {
        //                user.RatesArray = new List<UserRate>();
        //            }
        //        }
        //        return user;
        //    }
        //    return null;
        //}

        public int GetActiveUserCount(Guid agencyId)
        {
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == (int)UserStatus.Active && u.IsDeprecated == false).Count();
        }

        public IList<AgencyLite> GetAgencies(Guid loginId)
        {
            var userAgencies = new List<AgencyLite>();

            var userAccounts = this.database.Find<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
            userAccounts.ForEach(
                ua =>
                    {
                        var agency = this.database.Single<Agency>(a => a.Id == ua.AgencyId && a.IsDeprecated == false);
                        if (agency != null)
                        {
                            userAgencies.Add(new AgencyLite { Id = agency.Id, UserId = ua.Id, Name = agency.Name, Title = ua.DisplayTitle, Date = ua.Created.ToShortDateString() });
                        }
                    });

            return userAgencies;
        }

//        public IList<User> GetAgencyUsers(Guid agencyId)
//        {
//            var users = new List<User>();
//            var userprofiles = new List<UserProfile>();

//            var script = string.Format(@"SELECT 
//                                users.Id as Id ,
//                                users.CustomId,
//                                users.FirstName as FirstName ,
//                                users.LastName as LastName ,
//                                users.EmailAddress as EmailAddress,
//                                users.Suffix as Suffix,
//                                users.Credentials as Credentials ,
//                                users.CredentialsOther as CredentialsOther ,
//                                users.TitleType as TitleType ,
//                                users.TitleTypeOther as  TitleTypeOther ,
//                                users.EmploymentType as EmploymentType ,
//                                users.Status as  Status,
//                                users.Comments as Comments ,
//                                users.IsDeprecated as IsDeprecated,
//                                userprofiles.AddressLine1 as AddressLine1,
//                                userprofiles.AddressLine2 as AddressLine2,
//                                userprofiles.AddressCity as AddressCity,
//                                userprofiles.AddressStateCode as AddressStateCode,
//                                userprofiles.AddressZipCode as AddressZipCode,
//                                userprofiles.PhoneHome as PhoneHome,
//                                userprofiles.Gender as Gender,
//                                userprofiles.PhoneMobile as PhoneMobile
//                                    FROM 
//                                         users INNER JOIN userprofiles ON users.Id = userprofiles.Id
//                                            WHERE 
//                                                users.AgencyId = @agencyid AND
//                                                users.IsDeprecated = 0
//                                                    ORDER BY 
//                                                        users.FirstName ASC , users.LastName ASC ");
//            using (var cmd = new FluentCommand<User>(script))
//            {
//                users =
//                    cmd.SetConnection(this.connectionStringName)
//                       .AddGuid("agencyid", agencyId)
//                       .SetMap(
//                           reader =>
//                           new User
//                               {
//                                   Id = reader.GetGuid("Id"),
//                                   CustomId = reader.GetStringNullable("CustomId"),
//                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                                   EmailAddress = reader.GetStringNullable("EmailAddress"),
//                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
//                                   Credentials = reader.GetStringNullable("Credentials"),
//                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                                   TitleType = reader.GetStringNullable("TitleType"),
//                                   TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
//                                   EmploymentType = reader.GetStringNullable("EmploymentType"),
//                                   Status = reader.GetInt("Status"),
//                                   Comments = reader.GetStringNullable("Comments"),
//                                   IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                                   Profile = new UserProfile
//                                   {
//                                       AddressLine1 = reader.GetStringNullable("AddressLine1"),
//                                       AddressLine2 = reader.GetStringNullable("AddressLine2"),
//                                       AddressCity = reader.GetStringNullable("AddressCity"),
//                                       AddressStateCode = reader.GetStringNullable("AddressStateCode"),
//                                       AddressZipCode = reader.GetStringNullable("AddressZipCode"),
//                                       Gender = reader.GetStringNullable("Gender"),
//                                       PhoneHome = reader.GetStringNullable("PhoneHome"),
//                                       PhoneMobile = reader.GetStringNullable("PhoneMobile"),
//                                   }
//                               })
//                       .AsList();
//            }
//            return users;
//        }




        public IList<User> GetAgencyUsers( Guid agencyId,string query)
        {
            Check.Argument.IsNotEmpty(query, "query");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.FirstName.Contains(query)).Take(15).ToList();
        }

        public IList<User> GetAgencyUsersOnly(Guid agencyId)
        {
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
        }

        //public IList<User> GetAll()
        //{
        //    IList<User> users = new List<User>();
        //    var agencies = this.database.Find<Agency>(a => a.IsDeprecated == false && a.IsSuspended == false).ToList();
        //    agencies.ForEach(
        //        a =>
        //            {
        //                var agencyUsers = GetAgencyUsers(a.Id);
        //                agencyUsers.ForEach(u => { users.Add(u); });
        //            });
        //    return users;
        //}

        public List<User> GetAllUsers(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<User>();
            var status = " AND users.Status IN (1,2) ";
            if (statusId == 1 || statusId == 2)
            {
                status = string.Format(" AND users.Status = {0} ", statusId);
            }
            var script = string.Format(@"SELECT 
                                users.Id as Id ,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Permissions as Permissions ,
                                users.Status as  Status,
                                users.Created as Created ,
                                users.CustomId, 
                                users.TitleType as TitleType ,
                                users.TitleTypeOther as  TitleTypeOther ,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} 
                                                    ORDER BY 
                                                        users.LastName ASC , users.FirstName ASC ", status, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                list =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("statusId", statusId)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   Id = reader.GetGuid("Id"), 
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(), 
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(), 
                                   CustomId = reader.GetStringNullable("CustomId"), 
                                   Permissions = reader.GetStringNullable("Permissions"), 
                                   Status = reader.GetInt("Status"), 
                                   TitleType = reader.GetStringNullable("TitleType"), 
                                   TitleTypeOther = reader.GetStringNullable("TitleTypeOther"), 
                                   Credentials = reader.GetStringNullable("Credentials"), 
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
                                   Created = reader.GetDateTime("Created")
                               })
                       .AsList();
            }
            return list;
        }

//        public Dictionary<string, User> GetAllUsers(Guid agencyId)
//        {
//            var list = new Dictionary<string, User>();

//            var script =
//                string.Format(
//                    @"SELECT 
//                        users.Id as Id ,
//                        users.FirstName as FirstName ,
//                        users.LastName as LastName , 
//                        users.Suffix as Suffix ,
//                        users.Permissions as Permissions ,
//                        users.Licenses as Licenses , 
//                        users.Status as  Status, 
//                        users.Created as Created ,
//                        users.TitleType as TitleType , 
//                        users.TitleTypeOther as  TitleTypeOther ,
//                        users.Credentials as Credentials ,
//                        users.CredentialsOther as CredentialsOther , 
//                        users.IsDeprecated as  IsDeprecated 
//                             FROM 
//                                users 
//                                    WHERE 
//                                        users.AgencyId = @agencyid
//                                             ORDER BY users.LastName ASC ,
//                                                      users.FirstName ASC ");
//            using (var cmd = new FluentCommand<User>(script))
//            {
//                list =
//                    cmd.SetConnection("AgencyManagementConnectionString")
//                       .AddGuid("agencyid", agencyId)
//                       .SetDictonaryId("Id")
//                       .SetMap(
//                           reader =>
//                           new User
//                               {
//                                   Id = reader.GetGuid("Id"), 
//                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(), 
//                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(), 
//                                   Permissions = reader.GetStringNullable("Permissions"), 
//                                   Licenses = reader.GetStringNullable("Licenses"), 
//                                   Status = reader.GetInt("Status"), 
//                                   TitleType = reader.GetStringNullable("TitleType"), 
//                                   TitleTypeOther = reader.GetStringNullable("TitleTypeOther"), 
//                                   Credentials = reader.GetStringNullable("Credentials"), 
//                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
//                                   Created = reader.GetDateTime("Created"), 
//                                   IsDeprecated = reader.GetBoolean("IsDeprecated"), 
//                                   Suffix = reader.GetStringNullable("Suffix")
//                               })
//                       .AsDictionary();
//            }
//            return list;
//        }

        public IList<User> GetAuditors(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            if (users.IsNotNullOrEmpty())
            {
                users.ForEach(
                    user =>
                    {
                        if (user.Roles.IsAuditor())
                        {
                            userList.Add(user);
                        }
                    });
            }
            return userList;
        }

        //public User GetByLoginId(Guid loginId)
        //{
        //    Check.Argument.IsNotEmpty(loginId, "loginId");

        //    var user = this.database.Single<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);

        //    if (user != null && user.Permissions.IsNotNullOrEmpty())
        //    {
        //        user.PermissionsArray = user.Permissions.ToObject<List<string>>();
        //    }

        //    if (user != null && user.ProfileData.IsNotNullOrEmpty())
        //    {
        //        user.Profile = user.ProfileData.ToObject<UserProfile>();
        //    }

        //    return user;
        //}

        public IList<User> GetCaseManagerUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(
                user =>
                    {
                        if (user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin() || user.Roles.IsDirectorOfNursing())
                        {
                            userList.Add(user);
                        }
                    });
            return userList;
        }

        public IList<User> GetClinicalUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(
                user =>
                    {
                        if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                        {
                            userList.Add(user);
                        }
                    });
            return userList;
        }

        public IList<User> GetEmployeeRoster(Guid agencyId, Guid branchId, int status)
        {
            List<User> users;
            var script = string.Format(@"SELECT users.CustomId, users.FirstName, users.LastName, users.Credentials, users.CredentialsOther, userprofiles.AddressLine1, userprofiles.AddressLine2, userprofiles.AddressCity, userprofiles.AddressStateCode, userprofiles.AddressZipCode, userprofiles.PhoneHome, userprofiles.Gender " + " FROM users INNER JOIN userprofiles ON users.Id = userprofiles.Id" + " WHERE  users.AgencyId = @agencyId " + " {0} {1} AND IsDeprecated = 0", status == 0 ? string.Empty : " AND Status = " + status, !branchId.IsEmpty() ? "AND AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyId", agencyId)
                       .AddGuid("branchId", branchId)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   FirstName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   Profile = new UserProfile
                                   {
                                       AddressLine1 = reader.GetStringNullable("AddressLine1"),
                                       AddressLine2 = reader.GetStringNullable("AddressLine2"),
                                       AddressCity = reader.GetStringNullable("AddressCity"),
                                       AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                                       AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                                       Gender = reader.GetStringNullable("Gender"),
                                       PhoneHome = reader.GetStringNullable("PhoneHome"),
                                    
                                   }
                               })
                       .AsList();
            }
            return users ?? new List<User>();
        }

        public IList<User> GetHHAUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(
                user =>
                    {
                        if (user.Roles.IsHHA())
                        {
                            userList.Add(user);
                        }
                    });
            return userList;
        }

        public IList<User> GetLVNUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(
                user =>
                    {
                        if (user.Roles.IsNurse())
                        {
                            userList.Add(user);
                        }
                    });
            return userList;
        }

        public License GetLicense(Guid licenseId, Guid userId, Guid agencyId)
        {
            return this.database.Single<License>(l => l.Id == licenseId && l.UserId == userId && l.AgencyId == agencyId && l.IsDeprecated == false);
        }

        public IList<License> GetLicenses(Guid agencyId, Guid userId)
        {
            return this.database.Find<License>(l => l.AgencyId == agencyId && l.UserId == userId && l.IsDeprecated == false).ToList();
        }

        public IList<License> GetLicenses(Guid agencyId)
        {
            List<License> list;
            const string NonUserScript = @"SELECT 
                                li.Id as Id,
                                li.UserId as UserId,
                                li.IsNonUser as IsNonUser,
                                nonusers.FirstName as FirstName,
                                nonusers.LastName as LastName,
                                li.LicenseType as LicenseType,
                                li.LicenseNumber as LicenseNumber,
                                li.IssueDate as IssueDate,
                                li.ExpireDate as ExpireDate,
                                li.AssetId as AssetId
                                    FROM 
                                        licenses li
                                            LEFT JOIN nonusers on li.UserId = nonusers.Id AND li.IsNonUser = 1
                                                WHERE 
                                                    li.AgencyId = @agencyid AND
                                                    li.IsNonUser = 1 AND
                                                    nonusers.IsDeprecated = 0 AND
                                                    li.IsDeprecated = 0";
            const string UserScript = @"SELECT 
                                 li.Id as Id,
                                 li.UserId as UserId,
                                 li.IsNonUser as IsNonUser,
                                 users.FirstName as FirstName,
                                 users.LastName as LastName,
                                 li.LicenseType as LicenseType,
                                 li.LicenseNumber as LicenseNumber,
                                 li.IssueDate as IssueDate,
                                 li.ExpireDate as ExpireDate,
                                 li.AssetId as AssetId
                                     FROM 
                                         licenses li
                                             LEFT JOIN users on li.UserId = users.Id AND li.IsNonUser = 0
                                                 WHERE 
                                                     li.AgencyId = @agencyid AND
                                                     li.IsNonUser = 0 AND
                                                     users.IsDeprecated = 0 AND
                                                     li.IsDeprecated = 0";

            using (var cmd = new FluentCommand<License>(NonUserScript + " UNION " + UserScript))
            {
                list =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .SetMap(
                           reader =>
                           new License
                               {
                                   Id = reader.GetGuid("Id"), 
                                   UserId = reader.GetGuid("UserId"), 
                                   FirstName = reader.GetStringNullable("FirstName"), 
                                   LastName = reader.GetStringNullable("LastName"), 
                                   LicenseNumber = reader.GetStringNullable("LicenseNumber"), 
                                   LicenseType = reader.GetStringNullable("LicenseType"), 
                                   IssueDate = reader.GetDateTime("IssueDate"), 
                                   ExpireDate = reader.GetDateTime("ExpireDate"), 
                                   IsNonUser = reader.GetBoolean("IsNonUser"), 
                                   AssetId = reader.GetGuid("AssetId")
                               })
                       .AsList();
            }
            return list ?? new List<License>();
        }

        /// <summary>
        /// Gets all licenses filtered by a branch id and the status of the user they belong to.
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="branchId"></param>
        /// <param name="statusId"></param>
        /// <returns>A list of license items</returns>
        public IList<License> GetLicensesByBranchAndStatus(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<License>();
            var status = " AND  users.Status IN ( 1,2 ) ";
            if (statusId == 1 || statusId == 2)
            {
                status = string.Format(" AND users.Status = {0} ", statusId);
            }
            var script = string.Format(@"SELECT 
                                li.Id as Id,
                                users.Id as UserId,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials,
                                users.CredentialsOther as CredentialsOther,
                                users.IsDeprecated as IsUserDeprecated,
                                users.CustomId as CustomId,
                                li.LicenseType as LicenseType,
                                li.LicenseNumber as LicenseNumber,
                                li.IssueDate as IssueDate,
                                li.ExpireDate as ExpireDate
                                    FROM 
                                        users 
                                            JOIN licenses li on li.UserId = users.Id
                                                WHERE 
                                                    users.AgencyId = @agencyid AND
                                                    users.IsDeprecated = 0 {0} {1} 
                                                        ORDER BY 
                                                            users.LastName ASC , users.FirstName ASC ", status, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<License>(script))
            {
                list = cmd.SetConnection(this.connectionStringName).AddGuid("agencyid", agencyId).AddGuid("branchId", branchId).AddInt("statusId", statusId).SetMap(
                    reader =>
                    {
                        var user = new User
                                       {
                                           Id = reader.GetGuid("UserId"),
                                           FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                           LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                           Suffix = reader.GetStringNullable("Suffix"),
                                           Credentials = reader.GetStringNullable("Credentials"),
                                           CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                           IsDeprecated = reader.GetBoolean("IsUserDeprecated"),
                                           CustomId = reader.GetStringNullable("CustomId")
                                       };
                        return new License
                        {
                            Id = reader.GetGuid("Id"),
                            UserId = user.Id,
                            DisplayName = user.DisplayName,
                            CustomId = user.CustomId,
                            LicenseNumber = reader.GetString("LicenseNumber"),
                            LicenseType = reader.GetString("LicenseType"),
                            IssueDate = reader.GetDateTime("IssueDate"),
                            ExpireDate = reader.GetDateTime("ExpireDate")
                        };
                    }).AsList();
            }
            return list;
        }

        public NonUser GetNonUser(Guid agencyId,Guid id)
        {
            return this.database.Single<NonUser>(u => u.AgencyId == agencyId && u.Id == id);
        }

        public IList<NonUser> GetNonUsers(Guid agencyId)
        {
            return this.database.Find<NonUser>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
        }

        public IList<User> GetOTUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            if (users.IsNotNullOrEmpty())
            {
                users.ForEach(
                    user =>
                    {
                        if (user.Roles.IsOT())
                        {
                            userList.Add(user);
                        }
                    });
            }
            return userList;
        }

        public IList<User> GetPTUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(
                user =>
                    {
                        if (user.Roles.IsPT())
                        {
                            userList.Add(user);
                        }
                    });
            return userList;
        }

        public List<User> GetRatedUserByBranch(Guid agencyId, Guid branchId, Guid userIdToExclude)
        {
            var list = new List<User>();
            var script = string.Format(@"SELECT 
                                            Id,
                                            FirstName,
                                            LastName,
                                            MiddleName ,
                                            Suffix,
                                            Credentials,
                                            CredentialsOther,
                                            IsDeprecated
                                                FROM 
                                                    users 
                                                        WHERE
                                                            AgencyId = @agencyid {0} {1}  AND 
                                                            IsDeprecated = 0 Order By FirstName ASC", !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty, !userIdToExclude.IsEmpty() ? " AND Id != @userid " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                list =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("locationid", branchId)
                       .AddGuid("userid", userIdToExclude)
                       .SetMap(
                           reader =>
                           new User
                           {
                               Id = reader.GetGuid("Id"),
                               FirstName = reader.GetStringNullable("FirstName"),
                               LastName = reader.GetStringNullable("LastName"),
                               MiddleName = reader.GetStringNullable("MiddleName"),
                               Suffix = reader.GetStringNullable("Suffix"),
                               Credentials = reader.GetStringNullable("Credentials"),
                               CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                               IsDeprecated = reader.GetBoolean("IsDeprecated")
                           })
                       .AsList();
            }
            return list;
        }

        public List<SelectedUser> GetUserAccessList(Guid agencyId, Guid patientId)
        {
            var query = string.Format(@"SELECT 
                                        users.id as UserId,
                                        users.FirstName as UserFirstName, 
                                        users.LastName as UserLastName,
                                        users.Credentials as UserCredentials, 
                                        users.CredentialsOther as UserCredentialsOther,
                                        users.Suffix as UserSuffix, 
                                        COALESCE(pu.id, '{0}') as PatientUserId
                                            FROM users
                                                LEFT JOIN (SELECT patientusers.Id, patientusers.userid FROM patientusers WHERE patientusers.patientid = @patientid) pu ON users.id = pu.userid
                                                    WHERE 
                                                        users.agencyid = @agencyid AND
                                                        users.IsDeprecated = false", Guid.Empty.ToString());
            var list = new List<SelectedUser>();
            using (var cmd = new FluentCommand<SelectedUser>(query))
            {
                list =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("patientid", patientId)
                       .AddGuid("agencyid", agencyId)
                       .SetMap(reader => new SelectedUser {
                           UserId = reader.GetGuid("UserId"),
                           FirstName = reader.GetString("UserFirstName"),
                           LastName = reader.GetString("UserLastName"), 
                           Suffix = reader.GetStringNullable("UserSuffix"),
                           Credentials = reader.GetStringNullable("UserCredentials"), 
                           CredentialsOther = reader.GetStringNullable("UserCredentialsOther"),
                           PatientUserId = reader.GetGuid("PatientUserId") })
                       .AsList();
            }

            return list;
        }

        public IList<UserAndNonUserLite> GetUsersAndNonUsers(Guid agencyId)
        {
            List<UserAndNonUserLite> list;
            const string UserScript =
                @"SELECT 
                    Id,
                    FirstName,
                    LastName,
                    Credentials,
                    CredentialsOther,
                    Suffix,
                    0 as IsNonUser
                        FROM 
                            users
                                WHERE 
                                    AgencyId = @agencyid AND
                                    IsDeprecated = 0";
            const string NonUserScript =
                @"SELECT 
                    Id,
                    FirstName,
                    LastName,
                    '' as Credentials,
                    '' as CredentialsOther,
                    '' as Suffix,
                    1 as IsNonUser
                        FROM 
                            nonusers
                                WHERE 
                                    AgencyId = @agencyid AND
                                    IsDeprecated = 0";

            using (var cmd = new FluentCommand<UserAndNonUserLite>(NonUserScript + " UNION " + UserScript))
            {
                list =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .SetMap(reader => new UserAndNonUserLite
                           {
                               Id = reader.GetGuid("Id"),
                               FirstName = reader.GetStringNullable("FirstName"),
                               LastName = reader.GetStringNullable("LastName"),
                               Credentials = reader.GetStringNullable("Credentials"),
                               CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                               Suffix = reader.GetStringNullable("Suffix"),
                               IsNonUser = reader.GetBoolean("IsNonUser")
                           })
                       .AsList();
            }
            return list ?? new List<UserAndNonUserLite>();
        }

        public User GetUserOnly(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return this.database.Single<User>(u => u.Id == id && u.AgencyId == agencyId);
        }

        public UserProfile GetUserProfileOnly(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return this.database.Single<UserProfile>(u => u.Id == id && u.AgencyId == agencyId);
        }

        //public int GetUserPatientCount(Guid agencyId, Guid userId, byte statusId)
        //{
        //    var script = @"SELECT COUNT(*) FROM patients WHERE AgencyId = @agencyid AND UserId == @userId AND Status = @statusid && IsDeprecated = 0";

        //    return new FluentCommand<int>(script).SetConnection(this.connectionStringName).AddGuid("agencyid", agencyId).AddGuid("userid", userId).AddInt("statusid", statusId).AsScalar();
        //}

        public List<User> GetUsersByIds(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<User>();
            if (userIds != null && userIds.Count > 0)
            {
                var ids = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT Id, FirstName, LastName, MiddleName " + " FROM users " + "WHERE AgencyId = @agencyid AND IsDeprecated = 0 AND Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<User>(script))
                {
                    list = cmd.SetConnection(this.connectionStringName).AddGuid("agencyid", agencyId).SetMap(reader => new User { Id = reader.GetGuid("Id"), FirstName = reader.GetStringNullable("FirstName"), LastName = reader.GetStringNullable("LastName"), MiddleName = reader.GetStringNullable("MiddleName"), }).AsList();
                }
            }
            return list;
        }

        public IList<User> GetUsersByLoginId(Guid loginId)
        {
            return this.database.Find<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
        }

        public IList<User> GetUsersByLoginId(Guid agencyId, Guid loginId)
        {
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.LoginId == loginId && u.IsDeprecated == false);
        }

        public IList<User> GetUsersByStatus(Guid agencyId, Guid branchId, int status)
        {
            var users = new List<User>();
            var userprofiles = new List<UserProfile>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = string.Format(" AND users.Status = {0} ", status);
            }
            var script = string.Format(@"SELECT 
                                users.Id as Id ,
                                users.CustomId,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.EmailAddress as EmailAddress,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.TitleType as TitleType ,
                                users.TitleTypeOther as  TitleTypeOther ,
                                users.EmploymentType as EmploymentType ,
                                users.Status as  Status,
                                users.Comments as Comments ,
                                users.IsDeprecated as IsDeprecated,
                                userprofiles.AddressLine1 as AddressLine1,
                                userprofiles.AddressLine2 as AddressLine2,
                                userprofiles.AddressCity as AddressCity,
                                userprofiles.AddressStateCode as AddressStateCode,
                                userprofiles.AddressZipCode as AddressZipCode,
                                userprofiles.PhoneHome as PhoneHome,
                                userprofiles.Gender as Gender,
                                userprofiles.PhoneMobile as PhoneMobile
                                    FROM 
                                         users INNER JOIN userprofiles ON users.Id = userprofiles.Id
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} 
                                                    ORDER BY 
                                                        users.FirstName ASC , users.LastName ASC ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   Id = reader.GetGuid("Id"),
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   EmailAddress = reader.GetStringNullable("EmailAddress"),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   TitleType = reader.GetStringNullable("TitleType"),
                                   TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                                   EmploymentType = reader.GetStringNullable("EmploymentType"),
                                   Status = reader.GetInt("Status"),
                                   Comments = reader.GetStringNullable("Comments"),
                                   IsDeprecated = reader.GetBoolean("IsDeprecated"),
                                   Profile = new UserProfile
                                   {
                                       AddressLine1 = reader.GetStringNullable("AddressLine1"),
                                       AddressLine2 = reader.GetStringNullable("AddressLine2"),
                                       AddressCity = reader.GetStringNullable("AddressCity"),
                                       AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                                       AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                                       Gender = reader.GetStringNullable("Gender"),
                                       PhoneHome = reader.GetStringNullable("PhoneHome"),
                                       PhoneMobile = reader.GetStringNullable("PhoneMobile"),
                                   }
                               })
                       .AsList();
            }
            return users;
        }

        public IList<User> GetUsersByStatusAndDOB(Guid agencyId, Guid branchId, int status, int month)
        {
            var users = new List<User>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = string.Format(" AND users.Status = {0} ", status);
            }
            var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.DOB as DOB ,
                                users.IsDeprecated as IsDeprecated ,
                                userprofiles.AddressLine1 as AddressLine1,
                                userprofiles.AddressLine2 as AddressLine2,
                                userprofiles.AddressStateCode as AddressStateCode,
                                userprofiles.AddressZipCode as AddressZipCode,
                                userprofiles.AddressCity as AddressCity,
                                userprofiles.PhoneHome as PhoneHome
                                    FROM 
                                        users  INNER JOIN userprofiles ON users.Id=userprofiles.Id
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} AND 
                                                MONTH(users.DOB) = @month 
                                                    ORDER BY 
                                                        users.FirstName ASC , users.LastName ASC ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("month", month)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   DOB = reader.GetDateTime("DOB"),
                                   IsDeprecated = reader.GetBoolean("IsDeprecated"),
                                   Profile = new UserProfile
                                   {
                                       AddressLine1 = reader.GetStringNullable("AddressLine1"),
                                       AddressLine2 = reader.GetStringNullable("AddressLine2"),
                                       AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                                       AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                                       AddressCity = reader.GetStringNullable("AddressCity"),
                                       PhoneHome = reader.GetStringNullable("PhoneHome"),
                                   }
                               })
                       .AsList();
            }
            return users;
        }

        public IList<User> GetUsersByStatusAndPermissions(Guid agencyId, Guid branchId, int status)
        {
            var users = new List<User>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = string.Format(" AND users.Status = {0} ", status);
            }
            var script = string.Format(@"SELECT 
                                users.CustomId,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.Permissions as Permissions,
                                users.IsDeprecated as IsDeprecated 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1}
                                                    ORDER BY 
                                                        users.FirstName ASC , users.LastName ASC ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   CustomId = reader.GetStringNullable("CustomId"), 
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(), 
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(), 
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(), 
                                   Credentials = reader.GetStringNullable("Credentials"), 
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
                                   Permissions = reader.GetStringNullable("Permissions"), 
                                   IsDeprecated = reader.GetBoolean("IsDeprecated")
                               })
                       .AsList();
            }
            return users;
        }

        public IList<User> GetUsersByStatusAndServicesLean(Guid agencyId, Guid branchId, int status, int service)
        {
            var users = new List<User>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = string.Format(" AND users.Status = {0} ", status);
            }

            var script = string.Format(@"SELECT 
                                users.Id as Id ,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.IsDeprecated as IsDeprecated 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} AND 
                                                AccessibleServices & @service = @service 
                                                    ORDER BY 
                                                        users.FirstName ASC , users.LastName ASC ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("service", service)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   Id = reader.GetGuid("Id"), 
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(), 
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(), 
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(), 
                                   Credentials = reader.GetStringNullable("Credentials"), 
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
                                   IsDeprecated = reader.GetBoolean("IsDeprecated")
                               })
                       .AsList();
            }
            return users;
        }

        public IList<User> GetUsersByStatusLean(Guid agencyId, Guid branchId, int status, int service)
        {
            var users = new List<User>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = " AND users.Status = @status ";
            }
            var serviceScript = string.Empty;
            if (Enum.IsDefined(typeof(AgencyServices), service) && service > 0)
            {
                serviceScript = " AND users.AccessibleServices & @service = @service ";
            }
            var script = string.Format(@"SELECT 
                                users.Id as Id ,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.IsDeprecated as IsDeprecated 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} {2}
                                                    ORDER BY 
                                                        users.FirstName ASC , users.LastName ASC ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty, serviceScript);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("service", service)
                       .SetMap(
                           reader =>
                           new User
                               {
                                   Id = reader.GetGuid("Id"), 
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(), 
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(), 
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(), 
                                   Credentials = reader.GetStringNullable("Credentials"), 
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
                                   IsDeprecated = reader.GetBoolean("IsDeprecated")
                               })
                       .AsList();
            }
            return users;
        }

        public IList<User> GetUsersOnly(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false);
        }

        public IList<User> GetUsersOnly(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return this.database.Find<User>(u => u.AgencyId == agencyId && u.Status == status && u.IsDeprecated == false);
        }

        public List<User> GetUsersWithCredentialsByIds(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<User>();
            if (userIds != null && userIds.Count > 0)
            {
                var ids = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            Id,
                                            LoginId,
                                            FirstName,
                                            LastName,
                                            MiddleName ,
                                            Suffix,
                                            Credentials,
                                            CredentialsOther,
                                            IsDeprecated
                                                FROM 
                                                    users 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<User>(script))
                {
                    list =
                        cmd.SetConnection(this.connectionStringName)
                           .AddGuid("agencyid", agencyId)
                           .SetMap(
                               reader =>
                               new User
                                   {
                                       Id = reader.GetGuid("Id"), 
                                       LoginId = reader.GetGuid("LoginId"),
                                       FirstName = reader.GetStringNullable("FirstName"), 
                                       LastName = reader.GetStringNullable("LastName"), 
                                       MiddleName = reader.GetStringNullable("MiddleName"), 
                                       Suffix = reader.GetStringNullable("Suffix"), 
                                       Credentials = reader.GetStringNullable("Credentials"), 
                                       CredentialsOther = reader.GetStringNullable("CredentialsOther"), 
                                       IsDeprecated = reader.GetBoolean("IsDeprecated")
                                   })
                           .AsList();
                }
            }
            return list;
        }

        public bool SetUserStatus(Guid agencyId, Guid userId, int status)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            bool result = false;

            var user = this.database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            if (user != null)
            {
                user.Status = status;
                user.Modified = DateTime.Now;
                this.database.Update(user);
                result = true;
            }

            return result;
        }

        public bool AddMultipleUserLocations(List<UserLocation> userLocations)
        {
            try
            {
                this.database.AddMany<UserLocation>(userLocations);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateMultipleUserLocations(List<UserLocation> userLocations)
        {
            try
            {
                return this.database.UpdateMany<UserLocation>(userLocations) == userLocations.Count;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveMultipleUserLocations(List<UserLocation> userLocations)
        {
            try
            {
                return this.database.DeleteMany<UserLocation>(userLocations) == userLocations.Count;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveMultipleUserLocations(Guid agencyId, Guid userId, List<Guid> locationIds)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"DELETE FROM  userlocations WHERE AgencyId = @agencyid AND UserId = @userId AND LocationId IN ( {0} ) ;",locationIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("userid", userId)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }



        public List<UserLocation> UserLocations(Guid agencyId, Guid userId, int acessibleServices)
        {
            var userLocations = new List<UserLocation>();
            var script = string.Format(@"SELECT 
                        uls.LocationId as LocationId,
                        als.Name as OfficeName,
                        uls.Services as Services,
                        als.Services as LocationServices,
                        als.IsMainOffice as IsMainOffice,
                        als.IsLocationStandAlone as IsLocationStandAlone,
                        als.Payor as Payor
                            FROM
                               userlocations uls INNER JOIN agencylocations als  ON uls.LocationId = als.Id
                                    WHERE 
                                        uls.AgencyId = @agencyid AND
                                        uls.UserId = @userid AND
                                        uls.IsDeprecated = 0 AND
                                        als.IsDeprecated = 0 AND
                                        ((uls.Services & als.Services & @acessibleServices) > 0)");

            using (var cmd = new FluentCommand<UserLocation>(script))
            {
                userLocations = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("userid", userId)
                 .AddInt("acessibleServices", acessibleServices)
                 .SetMap(reader => new UserLocation
                 {
                     LocationId = reader.GetGuid("LocationId"),
                     LocationName = reader.GetStringNullable("OfficeName"),
                     Services = (AgencyServices)(reader.GetInt("Services") & reader.GetInt("LocationServices") & acessibleServices),
                     IsMainOffice = reader.GetBoolean("IsMainOffice"),
                     IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                     Payor = reader.GetStringNullable("Payor")

                 }).AsList();
            }
            return userLocations;
        }

        public List<UserLocation> UserLocationsOnly(Guid agencyId, Guid userId)
        {
            return this.database.Find<UserLocation>(u => u.AgencyId == agencyId && u.UserId == userId).ToList();
        }

       

        public bool UpdateLicense(License licenseItem)
        {
            try
            {
                string additionalScript = licenseItem.UserId.IsEmpty() ? " FirstName = @firstname, LastName = @lastname, " : " LicenseNumber = @licensenumber, ";
                string script = @"UPDATE licenses SET " + additionalScript + " LicenseType = @licenseType, IssueDate = @issuedate, ExpireDate = @expiredate, Modified = @modified WHERE AgencyId = @agencyid AND UserId = @userid AND Id = @id";
                using (var cmd = new FluentCommand<Guid>(script))
                {
                    return
                        cmd.SetConnection(this.connectionStringName)
                           .AddGuid("agencyid", licenseItem.AgencyId)
                           .AddGuid("id", licenseItem.Id)
                           .AddGuid("userid", licenseItem.UserId)
                           .AddString("firstname", licenseItem.FirstName)
                           .AddString("lastname", licenseItem.LastName)
                           .AddString("licensenumber", licenseItem.LicenseNumber)
                           .AddString("licensetype", licenseItem.LicenseType)
                           .AddDateTime("issuedate", licenseItem.IssueDate)
                           .AddDateTime("expiredate", licenseItem.ExpireDate)
                           .AddDateTime("modified", DateTime.Now)
                           .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateModel(User user, bool isCacheRefresh)
        {
            bool result = false;
            if (user != null)
            {
                user.Modified = DateTime.Now;
                if (this.database.Update(user) > 0)
                {
                    result = true;
                    if (isCacheRefresh)
                    {
                        UserEngine.AddOrUpdate(user.AgencyId, user);
                    }
                }
            }
            return result;
        }

        public bool UpdateUserProfile(UserProfile userProfile)
        {
            try
            {
                return database.Update(userProfile) > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateNonUser(NonUser nonUser)
        {
            bool result = false;
            if (nonUser != null)
            {
                var editUser = this.database.Single<NonUser>(u => u.AgencyId == nonUser.AgencyId && u.Id == nonUser.Id);
                if (editUser != null)
                {
                    editUser.FirstName = nonUser.FirstName;
                    editUser.LastName = nonUser.LastName;
                    editUser.Modified = DateTime.Now;
                    result = this.database.Update(editUser) > 0;
                }
            }
            return result;
        }

        public bool UpdateProfile(UserProfile userProfile)
        {
            bool result = false;

            if (userProfile != null)
            {
                //var userInfo = this.database.Single<User>(u => u.AgencyId == user.AgencyId && u.Id == user.Id);
                var userProfileInfo = this.database.Single<UserProfile>(u => u.AgencyId == userProfile.AgencyId && u.Id == userProfile.Id);
                //if (userInfo != null && userProfileInfo!=null)
                  if (userProfileInfo != null)
                {
                    userProfileInfo.AddressLine1 = userProfile.AddressLine1;
                    userProfileInfo.AddressLine2 = userProfile.AddressLine2;
                    userProfileInfo.AddressCity = userProfile.AddressCity;
                    userProfileInfo.AddressZipCode = userProfile.AddressZipCode;
                    userProfileInfo.AddressStateCode = userProfile.AddressStateCode;
                    if (userProfile.HomePhoneArray != null && userProfile.HomePhoneArray.Count > 0)
                    {
                        userProfileInfo.PhoneHome = userProfile.HomePhoneArray.ToArray().PhoneEncode();
                    }
                    if (userProfile.MobilePhoneArray != null && userProfile.MobilePhoneArray.Count > 0)
                    {
                        userProfileInfo.PhoneMobile = userProfile.MobilePhoneArray.ToArray().PhoneEncode();
                    }

                   
                    if (this.database.Update(userProfileInfo) > 0)
                    {
                        UserEngine.AddOrUpdate(userProfileInfo.AgencyId, userProfileInfo);
                        result = true;
                    }
                }
            }
            return result;
        }

        #endregion

        // public bool Reassign(Guid agencyId, ScheduleEvent scheduleEvent, Guid userId)
        // {
        // bool result = false;
        // if (!agencyId.IsEmpty() && !userId.IsEmpty() && scheduleEvent != null)
        // {
        // var employeeEpisode = database.Single<UserSchedule>(e => e.PatientId == scheduleEvent.PatientId && e.UserId == scheduleEvent.UserId);
        // UserEvent newEvent = null;
        // try
        // {
        // if (employeeEpisode != null)
        // {
        // var events = employeeEpisode.Visits.ToString().ToObject<List<UserEvent>>();

        // newEvent = events.FirstOrDefault(e => e.EventId == scheduleEvent.EventId);
        // if (newEvent != null)
        // {
        // events.RemoveAll(e => e.EventId == newEvent.EventId);
        // }
        // employeeEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
        // if (newEvent != null)
        // {
        // newEvent.UserId = userId;
        // }
        // else
        // {
        // newEvent = new UserEvent
        // {
        // EventId = scheduleEvent.EventId,
        // PatientId = scheduleEvent.PatientId,
        // EpisodeId = scheduleEvent.EpisodeId,
        // EventDate = scheduleEvent.EventDate,
        // Discipline = scheduleEvent.Discipline,
        // DisciplineTask = scheduleEvent.DisciplineTask,
        // Status = scheduleEvent.Status,
        // IsMissedVisit = scheduleEvent.IsMissedVisit,
        // UserId = userId,
        // TimeIn = scheduleEvent.TimeIn,
        // TimeOut = scheduleEvent.TimeOut,
        // IsDeprecated = scheduleEvent.IsDeprecated,

        // };
        // }
        // AddUserEvent(agencyId, scheduleEvent.PatientId, userId, newEvent);
        // database.Update<UserSchedule>(employeeEpisode);
        // result = true;
        // }
        // else
        // {
        // newEvent = new UserEvent
        // {
        // EventId = scheduleEvent.EventId,
        // PatientId = scheduleEvent.PatientId,
        // EpisodeId = scheduleEvent.EpisodeId,
        // EventDate = scheduleEvent.EventDate,
        // Discipline = scheduleEvent.Discipline,
        // DisciplineTask = scheduleEvent.DisciplineTask,
        // Status = scheduleEvent.Status,
        // IsMissedVisit = scheduleEvent.IsMissedVisit,
        // UserId = userId,
        // TimeIn = scheduleEvent.TimeIn,
        // TimeOut = scheduleEvent.TimeOut,
        // IsDeprecated = scheduleEvent.IsDeprecated,

        // };
        // AddUserEvent(agencyId, scheduleEvent.PatientId, userId, newEvent);
        // result = true;
        // }
        // }
        // catch (Exception e)
        // {
        // return result;
        // }
        // }
        // return result;
        // }

        // public bool DeleteScheduleEvent(Guid patientId, Guid eventId, Guid userId)
        // {
        // bool result = false;
        // if (!eventId.IsEmpty() && !patientId.IsEmpty() && !userId.IsEmpty())
        // {
        // var userEpisode = database.Single<UserSchedule>(e => e.PatientId == patientId && e.UserId == userId);
        // if (userEpisode != null)
        // {
        // List<UserEvent> events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
        // events.RemoveAll(evnt => evnt.EventId == eventId);
        // userEpisode.Visits = events.ToXml();
        // database.Update<UserSchedule>(userEpisode);
        // result = true;
        // }
        // }
        // return result;
        // }

        // public bool RemoveScheduleEvent(Guid agencyId, Guid patientId, Guid eventId, Guid userId)
        // {
        // bool result = false;
        // if (!agencyId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && !userId.IsEmpty())
        // {
        // var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
        // if (userEpisode != null)
        // {
        // var events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
        // events.RemoveAll(ev => ev.EventId == eventId);
        // userEpisode.Visits = events.ToXml();
        // database.Update<UserSchedule>(userEpisode);
        // result = true;
        // }
        // }

        // return result;
        // }

        // public UserEvent GetEvent(Guid agencyId, Guid userId, Guid patientId, Guid eventId)
        // {
        // UserEvent evnt = null;
        // if (!agencyId.IsEmpty() && !userId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
        // {
        // var episode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
        // if (episode != null && episode.Visits.IsNotNullOrEmpty())
        // {
        // evnt = episode.Visits.ToObject<List<UserEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
        // }
        // }
        // return evnt;
        // }


        #region User Rates


        public bool AddUserPayorRates(UserPayorRates data)
        {
            return mongoDb.AddData<UserPayorRates>("userpayorrates", data);
        }

        public bool AddUserPayorRates(List<UserPayorRates> datas)
        {
            return mongoDb.AddDatas<UserPayorRates>("userpayorrates", datas);
        }

        public UserPayorRates GetUserPayorRate(Guid agencyId, Guid userId, int payorId)
        {
            var collection = mongoDb.GetCollection<UserPayorRates>("userpayorrates");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("UserId", userId), Query.EQ("PayorId", payorId));
            return collection.FindOneAs<UserPayorRates>(query);
        }

        public List<UserPayorRates> GetUserPayorRates(Guid agencyId, Guid userId, List<int> payorIds)
        {
            var collection = mongoDb.GetCollection<UserPayorRates>("userpayorrates");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("UserId", userId), Query.In("PayorId", new BsonArray(payorIds)));
            return collection.FindAs<UserPayorRates>(query).ToList();
        }

        public List<UserRate> GetPayorUserRates(Guid agencyId, Guid userId, int payorId)
        {
            var rates = new List<UserRate>();
            var collection = mongoDb.GetCollection<UserPayorRates>("userpayorrates");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("UserId", userId), Query.EQ("PayorId", payorId));
            var payorData= collection.FindOneAs<UserPayorRates>(query);
            if (payorData != null)
            {
                rates = payorData.PayorRates;
            }
            return rates;
        }

        public bool UpdateUserPayorRates(Guid agencyId, Guid userId, int payorId, UserPayorRates data)
        {
            var collection = mongoDb.GetCollection<UserPayorRates>("userpayorrates");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("UserId", userId), Query.EQ("PayorId", payorId));
            return collection.Update(query, Update.Replace<UserPayorRates>(data), UpdateFlags.Upsert).Ok;
        }

        public List<UserPayorRates> GetUserPayorRates(Guid agencyId, Guid userId)
        {
            var collection = mongoDb.GetCollection<UserPayorRates>("userpayorrates");
            var query = Query.And(Query.EQ("AgencyId", agencyId), Query.EQ("UserId", userId));
            return collection.FindAs<UserPayorRates>(query).ToList();
        }

        /// <summary>
        /// pulls from mysql old users rate 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUsersRates(Guid agencyId, Guid userId)
        {
            object rates = string.Empty;
            var script = string.Format(@"SELECT 
                                users.Rates as Rates 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.Id =  @userid limit 1");
            using (var cmd = new FluentCommand<object>(script))
            {
                rates =
                    cmd.SetConnection(this.connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("userid", userId)
                       .SetMap(
                           reader =>
                                reader.GetStringNullable("Rates")

                       ).AsSingle();
            }
            return rates!=null ? rates.ToString() : string.Empty;
        }


        #endregion
    }
}