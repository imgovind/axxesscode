﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Extensions;

    using SubSonic.Repository;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.IsDeprecated = !agency.IsDeprecated;
                agency.IsSuspended = !agency.IsSuspended;
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                return true;
            }
            return false;
        }

        public List<AgencyUser> GetUserNames()
        {
            var script = @"SELECT 
                        agencies.Id as AgencyId,
                        users.Id as UserId,
                        users.FirstName,
                        users.LastName, 
                        users.Suffix,
                        users.Credentials, 
                        users.CredentialsOther,
                        users.IsDeprecated
                            FROM 
                                agencies 
                                    INNER JOIN users ON agencies.Id = users.AgencyId 
                                        ORDER BY agencies.Id";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public List<AgencyUser> GetUserNames(Guid agencyId)
        {
            var script = @"SELECT
                            Id,
                            FirstName,
                            LastName, 
                            Suffix,
                            Credentials,
                            CredentialsOther, 
                            IsDeprecated
                                FROM 
                                    users 
                                        Where
                                             AgencyId = @agencyid";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("Id"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        //public Agency Get(Guid id)
        //{
        //    var agency = database.Single<Agency>(a => a.Id == id);
        //    if (agency != null)
        //    {
        //        agency.MainLocation = GetMainLocation(id);
        //    }

        //    return agency;
        //}

        public Agency GetAgencyOnly(Guid id)
        {
            return database.Single<Agency>(a => a.Id == id);
        }

        //public Agency GetAgencyWithMainLocation(Guid id)
        //{
        //    Agency agency= database.Single<Agency>(a => a.Id == id);
        //    if (agency != null)
        //    {
        //        agency.MainLocation = GetMainLocation(id);
        //    }
        //    return agency;
        //}

        //public List<Agency> GetPhysicianAgenies(Guid loginId)
        //{
        //    var agencies = new List<Agency>();
        //    var agencyIds=new List<Guid>();
        //    var agencyPhysicians = database.Find<AgencyPhysician>(a => a.LoginId == loginId).ToList();
        //    foreach (AgencyPhysician ap in agencyPhysicians)
        //    {
        //        if (!agencyIds.Contains(ap.AgencyId))
        //            agencyIds.Add(ap.AgencyId);
        //    }
        //    agencyIds.ForEach(a=>
        //        agencies.Add(database.Find<Agency>(ag=>ag.Id==a && ag.IsDeprecated==false).SingleOrDefault()));
        //    return agencies;
        //}
        
        //public Agency GetWithBranches(Guid agencyId)
        //{
        //    var agency = database.Single<Agency>(a => a.Id == agencyId);
        //    if (agency != null)
        //    {
        //        agency.Branches = database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
        //    }
        //    return agency;
        //}

        public bool Add(Agency agency)
        {
            if (agency != null)
            {
                agency.Id = Guid.NewGuid();
                agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;
                database.Add<Agency>(agency);
                return true;
            }
            return false;
        }

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public bool UpdateLocation(AgencyLocation agencyLocation)
        {
            var result = false;
            if (agencyLocation != null)
            {
                database.Update<AgencyLocation>(agencyLocation);
                result = true;
            }
            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public Dictionary<string, string> GetLocation(Guid agencyId, Dictionary<string, string> where, string[] columns)
        {
            return EntityHelper.SelectWhereByColumns<AgencyLocation>(agencyId, "AgencyManagementConnectionString", where, columns);
        }

        public Guid GetMainLocationId(Guid agencyId)
        {
            var id = Guid.Empty;

            var script = string.Format(@"SELECT 
                        Id as Id 
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        IsMainOffice = 1 AND 
                                        IsDeprecated = 0 
                                         ORDER BY Created ASC LIMIT 0,1 ");

            using (var cmd = new FluentCommand<Guid>(script))
            {
                id = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader =>
                    reader.GetGuid("Id")

                 ).AsSingle();
            }
            return id;
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).ToList();
        }

        public List<AgencyLocation> GetBranchesForUserControl(Guid agencyId, int service)
        {
            var list = new List<AgencyLocation>();
            if (service > 0)
            {

                var serviceScript = " AND (((Services & @service) > 0 ) OR ((Services & @service)  > 0)) ";
               // var serviceScript = " AND (((Services & @service) = @service) OR ((Services & @service) = Services)) ";
                var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name,
                        IsMainOffice as IsMainOffice,
                        IsLocationStandAlone as IsLocationStandAlone,
                        Services,
                        Payor
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid {0} AND
                                        IsDeprecated = 0 ORDER BY IsMainOffice DESC, NAME ASC ", serviceScript);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .AddInt("service", service)
                      .SetMap(reader => new AgencyLocation
                      {
                          Id = reader.GetGuid("Id"),
                          Name = reader.GetStringNullable("Name"),
                          IsMainOffice = reader.GetBoolean("IsMainOffice"),
                          IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                          Services = (AgencyServices)(reader.GetInt("Services") & service),
                          Payor = reader.GetStringNullable("Payor")

                      }).AsList();
                }
            }
            return list;
        }

        public Guid GetBranchForSelectionId(Guid agencyId, int service)
        {
            var serviceScript = string.Empty;
            if (Enum.IsDefined(typeof(AgencyServices), service) && service > 0)
            {
                serviceScript = " AND (Services & @service = @service OR Services & @service = Services) ";
            }

            var locationId = Guid.Empty;
            var script = string.Format(@"SELECT 
                        Id as Id
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid {0} AND
                                        IsDeprecated = 0 ORDER BY IsMainOffice DESC, NAME ASC limit 0,1 ", serviceScript);

            using (var cmd = new FluentCommand<Guid>(script))
            {
                locationId = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddInt("service", service)
                  .SetMap(reader =>
                      reader.GetGuid("Id")


                  ).AsSingle();
            }
            return locationId;
        }

        public AgencyLocation GetBranchForSelection(Guid agencyId, int service)
        {
            var serviceScript = string.Empty;
            if (Enum.IsDefined(typeof(AgencyServices), service) && service > 0)
            {
                serviceScript = " AND (Services & @service = @service OR Services & @service = Services) ";
            }

            AgencyLocation location=null;
            var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name,
                        IsLocationStandAlone as IsLocationStandAlone,
                        Services,
                        Payor
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid {0} AND
                                        IsDeprecated = 0 ORDER BY IsMainOffice DESC, NAME ASC limit 0,1 ", serviceScript);

            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddInt("service", service)
                  .SetMap(reader => new AgencyLocation
                  {
                      Id = reader.GetGuid("Id"),
                      Name = reader.GetStringNullable("Name"),
                      IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                      Payor = reader.GetStringNullable("Payor"),
                      Services = (AgencyServices)reader.GetInt("Services")

                  }).AsSingle();
            }
            return location;
        }

        public List<AgencyLocation> GetBranchesForUserControl(Guid agencyId)
        {
            var list = new List<AgencyLocation>();
            var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name,
                        IsLocationStandAlone as IsLocationStandAlone,
                        Services,
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        IsDeprecated = 0");

            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                  .SetMap(reader => new AgencyLocation
                  {
                      Id = reader.GetGuid("Id"),
                      Name = reader.GetStringNullable("Name"),
                      IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                      Services = (AgencyServices)reader.GetInt("Services")

                  }).AsList();
            }
            return list;
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
        }

        public AgencyLocation FindLocationOrMain(Guid agencyId, Guid Id)
        {
            AgencyLocation agencyLocation =null;
            if (!Id.IsEmpty())
            {
                agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id);
            }
            if (agencyLocation == null)
            {
                 agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
            }
            return agencyLocation;
        }

        //public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        //{
        //    var location = new AgencyLocation();
            
        //        var script = @"SELECT al.`Id`, al.`AgencyId`, al.`Name`, al.`CustomId`, al.`CBSA`, al.`MedicareProviderNumber`, al.`Cost`, al.`BillData`"+
        //                "al.`SubmitterId`, al.`SubmitterName`, al.`SubmitterPhone`, al.`SubmitterFax`, al.`Payor`, al.`BranchId`, al.`BranchIdOther`, al.`Ub04Locator81cca`," +
        //                "al.`TaxId`, `TaxIdType`, al.`NationalProviderNumber`, al.`MedicaidProviderNumber`, al.`ContactPersonFirstName`," +
        //                "al.`ContactPersonLastName`, al.`ContactPersonEmail`, al.`ContactPersonPhone`, al.`CahpsVendor`, al.`CahpsVendorId`, al.`CahpsSurveyDesignator`," +
        //                "al.`IsAxxessTheBiller`, al.`OasisAuditVendor`, al.`IsLocationStandAlone`, al.`UseServiceSupplies`, al.`UseStandardSupplyReimbursement`," +
        //                "al.`UseWoundcareSupplies`, al.`ServiceSuppliesRevenueCode`, al.`ServiceSuppliesDescription`, ai.`AddressLine1` as MedicareAddressLine1," +
        //                "ai.`AddressLine2` as MedicareAddressLine2, ai.`AddressCity` as MedicareAddressCity, ai.`AddressStateCode` as MedicareAddressStateCode, ai.`AddressZipCode` as MedicareAddressZipCode" +
        //            "FROM agencylocations al " +
        //            "LEFT JOIN agencymedicareinsurances ai ON ai.AgencyId = al.AgencyId AND ai.MedicareId = al.Payor " +
        //            "WHERE al.id = @id AND al.agencyid = @agencyid";

        //        using (var cmd = new FluentCommand<AgencyLocation>(script))
        //        {
        //            location = cmd.SetConnection("AgencyManagementConnectionString")
        //             .AddGuid("agencyid", agencyId)
        //             .AddGuid("id", Id)
        //             .SetMap(reader => new AgencyLocation
        //             {
        //                 Id = reader.GetGuid("Id"),
        //                 AgencyId = reader.GetGuid("AgencyId"),
        //                 Name = reader.GetStringNullable("Name"),
        //                 CustomId = reader.GetStringNullable("CustomId"),
        //                 CBSA = reader.GetStringNullable("CBSA"),
        //                 MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
        //                 Cost = reader.GetStringNullable("Cost"),
        //                 BillData = reader.GetStringNullable("BillData"),
        //                 SubmitterId = reader.GetStringNullable("SubmitterId"),
        //                 SubmitterName = reader.GetStringNullable("SubmitterName"),
        //                 SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
        //                 SubmitterFax = reader.GetStringNullable("SubmitterFax"),
        //                 Payor = reader.GetStringNullable("Payor"),
        //                 BranchId = reader.GetStringNullable("CustomId"),
        //                 BranchIdOther = reader.GetStringNullable("CustomId"),
        //                 Ub04Locator81cca = reader.GetStringNullable("CustomId"),
        //                 TaxId = reader.GetStringNullable("CustomId"),
        //                 TaxIdType = reader.GetStringNullable("CustomId"),
        //                 NationalProviderNumber = reader.GetStringNullable("CustomId"),
        //                 MedicaidProviderNumber = reader.GetStringNullable("CustomId"),
        //                 ContactPersonFirstName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonLastName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonEmail = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
                         
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 MedicareInsurance = new AgencyMedicareInsurance(){
        //                     AddressCity = reader.GetStringNullable(""),
        //                 },

        //             }).AsSingle();
        //        }
            
        //}

        public AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor)
        {
            return database.Single<AgencyMedicareInsurance>(l => l.AgencyId == agencyId && l.MedicareId == payor);
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
                if (existingLocation != null)
                {
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    //  existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                    existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                    if (existingLocation.IsLocationStandAlone)
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            var agency = this.GetAgencyOnly(location.AgencyId);
                            if (agency != null)
                            {

                                existingLocation.SubmitterId = agency.SubmitterId;
                                existingLocation.SubmitterName = agency.SubmitterName;
                                existingLocation.Payor = agency.Payor;
                                existingLocation.SubmitterPhone = agency.SubmitterPhone;
                                existingLocation.SubmitterFax = agency.SubmitterFax;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    existingLocation.BranchId = location.BranchId;
                    existingLocation.BranchIdOther = location.BranchIdOther;
                    existingLocation.Name = location.Name;
                    existingLocation.CustomId = location.CustomId;
                    existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    existingLocation.Comments = location.Comments;
                    existingLocation.Modified = DateTime.Now;
                    result = database.Update<AgencyLocation>(existingLocation) > 0;
                }
            }
            return result;
        }

        public bool EditLocationModal(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                try
                {
                    location.Modified = DateTime.Now;
                    result = database.Update<AgencyLocation>(location) > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool EditBranchLocator(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                //existingLocation.Cost = location.Cost;
                existingLocation.Ub04Locator81cca = location.Ub04Locator81cca;
                existingLocation.Modified = DateTime.Now;

                result = database.Update<AgencyLocation>(existingLocation) > 0;

            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public IList<Agency> AllAgencies()
        {
            var agencies = database.All<Agency>().ToList();
            if (agencies != null && agencies.Count > 0)
            {
                agencies.ForEach(a =>
                {
                    a.MainLocation = GetMainLocation(a.Id);
                });
            }
            return agencies.OrderBy(a => a.Name).ToList();
        }

        public bool Update(Agency agency)
        {
            return database.Update<Agency>(agency) > 0;
        }

        public List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds)
        {
            var list = new List<AgencyLocation>();
            if (locationIds != null && locationIds.Count > 0)
            {
                var ids = locationIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name 
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencyLocation
                     {
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name")

                     }).AsList();
                }
            }
            return list;
        }

        public List<AgencyLocation> AgencyLocationsMoreInfo(Guid agencyId, List<Guid> locationIds)
        {
            var list = new List<AgencyLocation>();
            if (locationIds != null && locationIds.Count > 0)
            {
                var ids = locationIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                                Id as Id ,
                                                Name as Name ,
                                                MedicareProviderNumber as MedicareProviderNumber,
                                                AddressLine1 as AddressLine1,
                                                AddressLine2 as AddressLine2,
                                                AddressCity as AddressCity,
                                                AddressStateCode as AddressStateCode,
                                                AddressZipCode as AddressZipCode,
                                                PhoneWork as PhoneWork,
                                                FaxNumber as FaxNumber,
                                                IsMainOffice as IsMainOffice,
                                                IsLocationStandAlone as IsLocationStandAlone,
                                                IsAxxessTheBiller as IsAxxessTheBiller
                                                    FROM
                                                        agencylocations 
                                                            WHERE 
                                                                AgencyId = @agencyid AND
                                                                Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencyLocation
                     {
                         AgencyId = agencyId,
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name"),
                         MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         PhoneWork = reader.GetStringNullable("PhoneWork"),
                         FaxNumber = reader.GetStringNullable("FaxNumber"),
                         IsMainOffice = reader.GetBoolean("IsMainOffice"),
                         IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                         IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller")

                     }).AsList();
                }
            }
            return list;
        }

//        public AgencyLocation GetFirstLoction(Guid agencyId)
//        {
//            var location = new AgencyLocation();
          
//                var script = string.Format(@"SELECT 
//                        Id as Id ,
//                        Name as Name 
//                            FROM
//                                agencylocations 
//                                    WHERE 
//                                        AgencyId = @agencyid AND
//                                        IsDeprecated = 0 
//                                         ORDER BY Created ASC LIMIT 0,1 ");

//                using (var cmd = new FluentCommand<AgencyLocation>(script))
//                {
//                    location = cmd.SetConnection("AgencyManagementConnectionString")
//                     .AddGuid("agencyid", agencyId)
//                     .SetMap(reader => new AgencyLocation
//                     {
//                         Id = reader.GetGuid("Id"),
//                         Name = reader.GetStringNullable("Name")

//                     }).AsSingle();
//                }
//            return location;
//        }

        public bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Created = DateTime.Now;
                medicare.Id = Guid.NewGuid();
                database.Add<AgencyMedicareInsurance>(medicare);
                return true;
            }
            return false;
        }

        public bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Modified = DateTime.Now;
                return database.Update<AgencyMedicareInsurance>(medicare) > 0;
            }
            return false;
        }

        public LocationPrintProfile AgencyNameWithLocationName(Guid agencyId, Guid locationId)
        {
            var location = new LocationPrintProfile();

            var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencylocations.Name as OfficeName
                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.Id = @locationid LIMIT 0,1");

            using (var cmd = new FluentCommand<LocationPrintProfile>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("locationid", locationId)
                 .SetMap(reader => new LocationPrintProfile
                 {
                     Name = reader.GetStringNullable("Name"),
                     OfficeName = reader.GetStringNullable("OfficeName")
                 }).AsSingle();
            }
            return location;
        }

        public LocationPrintProfile AgencyNameWithLocationAddress(Guid agencyId, Guid locationId)
        {
            var location = new LocationPrintProfile();
            
                var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencylocations.Name as OfficeName,
                        agencylocations.AddressLine1 as AddressLine1,
                        agencylocations.AddressLine2 as AddressLine2,
                        agencylocations.AddressCity as AddressCity,
                        agencylocations.AddressStateCode as AddressStateCode,
                        agencylocations.AddressZipCode as AddressZipCode,
                        agencylocations.PhoneWork as PhoneWork,
                        agencylocations.FaxNumber as FaxNumber
                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.Id = @locationid LIMIT 0,1");

                using (var cmd = new FluentCommand<LocationPrintProfile>(script))
                {
                    location = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("locationid",locationId)
                     .SetMap(reader => new LocationPrintProfile
                     {
                         //Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name"),
                         OfficeName = reader.GetStringNullable("OfficeName"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         PhoneWorkFormatted = reader.GetStringNullable("PhoneWork").ToPhone(),
                         FaxNumberFormatted = reader.GetStringNullable("FaxNumber").ToPhone()

                     }).AsSingle();
                }
               
            return location;
        }

        public LocationPrintProfile AgencyNameWithMainLocationAddress(Guid agencyId)
        {
            var location = new LocationPrintProfile();

            var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencylocations.Name as OfficeName,
                        agencylocations.AddressLine1 as AddressLine1,
                        agencylocations.AddressLine2 as AddressLine2,
                        agencylocations.AddressCity as AddressCity,
                        agencylocations.AddressStateCode as AddressStateCode,
                        agencylocations.AddressZipCode as AddressZipCode,
                        agencylocations.PhoneWork as PhoneWork,
                        agencylocations.FaxNumber as FaxNumber
                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.IsMainOffice = 1 LIMIT 0,1");

            using (var cmd = new FluentCommand<LocationPrintProfile>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new LocationPrintProfile
                 {
                     //Id = reader.GetGuid("Id"),
                     Name = reader.GetStringNullable("Name"),
                     OfficeName = reader.GetStringNullable("OfficeName"),
                     AddressLine1 = reader.GetStringNullable("AddressLine1"),
                     AddressLine2 = reader.GetStringNullable("AddressLine2"),
                     AddressCity = reader.GetStringNullable("AddressCity"),
                     AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                     PhoneWorkFormatted = reader.GetStringNullable("PhoneWork").ToPhone(),
                     FaxNumberFormatted = reader.GetStringNullable("FaxNumber").ToPhone()

                 }).AsSingle();
            }

            return location;
        }

        public LocationPrintProfile AgencyNameWithAddressAndMore(Guid agencyId, Guid locationId)
        {
            var location = new LocationPrintProfile();

            var script = string.Format(@"SELECT 
                        agencies.Name as Name ,
                        agencylocations.Name as OfficeName,
                        agencies.MedicareProviderNumber as MedicareProviderNumber ,
                        agencies.ContactPersonEmail as ContactPersonEmail ,
                        agencylocations.AddressLine1 as AddressLine1,
                        agencylocations.AddressLine2 as AddressLine2,
                        agencylocations.AddressCity as AddressCity,
                        agencylocations.AddressStateCode as AddressStateCode,
                        agencylocations.AddressZipCode as AddressZipCode,
                        agencylocations.PhoneWork as PhoneWork,
                        agencylocations.FaxNumber as FaxNumber
                            FROM
                               agencies INNER JOIN agencylocations  ON agencies.Id = agencylocations.AgencyId
                                    WHERE 
                                        agencies.Id = @agencyid AND
                                        agencylocations.Id = @locationid LIMIT 0,1");

            using (var cmd = new FluentCommand<LocationPrintProfile>(script))
            {
                location = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                  .AddGuid("locationid", locationId)
                 .SetMap(reader => new LocationPrintProfile
                 {
                     //Id = reader.GetGuid("Id"),
                     Name = reader.GetStringNullable("Name"),
                     OfficeName = reader.GetStringNullable("OfficeName"),
                     MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                     ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                     AddressLine1 = reader.GetStringNullable("AddressLine1"),
                     AddressLine2 = reader.GetStringNullable("AddressLine2"),
                     AddressCity = reader.GetStringNullable("AddressCity"),
                     AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                     PhoneWorkFormatted = reader.GetStringNullable("PhoneWork").ToPhone(),
                     FaxNumberFormatted = reader.GetStringNullable("FaxNumber").ToPhone()

                 }).AsSingle();
            }

            return location;
        }
      
        public AgencyServices GetServicesAccessibleForAgency(Guid agencyId)
        {
            AgencyServices services = AgencyServices.None;
            var script = string.Format(@"SELECT Services
                                            FROM agencies
                                                    WHERE Id = @agencyId");
            using (var cmd = new FluentCommand<AgencyServices>(script))
            {
                services = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyId", agencyId)
                .SetMap(reader => (AgencyServices)reader.GetInt("Services"))
                .AsSingle();
            }
            return services;
        }


        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    contact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    existingContact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingContact.PhoneExtension = contact.PhoneExtension;
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Comments = contact.Comments;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (!insurance.AgencyId.IsEmpty() && insurance.OldInsuranceId > 0)
                {
                    var oldInsurance = GetInsurance(insurance.OldInsuranceId, insurance.AgencyId);
                    if (oldInsurance != null && oldInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = oldInsurance.BillData;
                    }
                }
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == insuranceId );
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public IList<InsuranceLean> GetLeanInsurances(Guid agencyId)
        {
            var script = @"SELECT 
                agencyinsurances.Id, 
                agencyinsurances.PayorType, 
                agencyinsurances.Name,
                agencyinsurances.InvoiceType,
                agencyinsurances.ContactPersonFirstName, 
                agencyinsurances.ContactPersonLastName, 
                agencyinsurances.PhoneNumber,
                agencyinsurances.PayorId 
                    FROM 
                        agencyinsurances 
                            WHERE 
                                agencyinsurances.AgencyId = @agencyid AND 
                                agencyinsurances.IsDeprecated = 0";

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare=false
                })
                .AsList();
        }

        public AgencyInsurance GetFirstAgencyInsuranceByPayer(Guid agencyId, int[] payers)
        {
            AgencyInsurance agencyInsurance = null;
            var additionalScript = string.Empty;
            if (payers != null && payers.Length > 0)
            {
                additionalScript += string.Format(" AND ai.PayorType IN ( {0} ) ", payers.Select(d => d.ToString()).ToArray().Join(","));
                var script = string.Format(@"SELECT 
                ai.Id, 
                ai.PayorType, 
                ai.Name,
                ai.PayorId 
                    FROM 
                        agencyinsurances ai
                            WHERE 
                                ai.AgencyId = @agencyid AND 
                                ai.IsDeprecated = 0 {0} ORDER BY ai.Id ASC limit 0,1; ", additionalScript);

                using (var cmd = new FluentCommand<AgencyInsurance>(script))
                {
                    agencyInsurance = cmd.SetConnection("AgencyManagementConnectionString")
                      .AddGuid("agencyid", agencyId)
                      .SetMap(reader => new AgencyInsurance
                      {
                          Id = reader.GetInt("Id"),
                          Name = reader.GetStringNullable("Name"),
                          PayorType = reader.GetInt("PayorType"),
                          PayorId = reader.GetStringNullable("PayorId")
                      })
                      .AsSingle();
                }
            }
            return agencyInsurance;
        }

        public AgencyInsurance GetFirstInsurance(Guid agencyId, int[] excludedPayerTypes)
        {
            AgencyInsurance agencyInsurance = null;
            var additionalScript = string.Empty;
            if (excludedPayerTypes != null && excludedPayerTypes.Length > 0)
            {
                additionalScript += string.Format(" AND ai.PayorType NOT IN ( {0} ) ", excludedPayerTypes.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                ai.Id, 
                ai.PayorType, 
                ai.Name,
                ai.PayorId 
                    FROM 
                        agencyinsurances ai
                            WHERE 
                                ai.AgencyId = @agencyid AND 
                                ai.IsDeprecated = 0 {0} ORDER BY ai.Id  ASC limit 0,1  ", additionalScript);

            using (var cmd = new FluentCommand<AgencyInsurance>(script))
            {
                agencyInsurance = cmd.SetConnection("AgencyManagementConnectionString")
                  .AddGuid("agencyid", agencyId)
                  .SetMap(reader => new AgencyInsurance
                  {
                      Id = reader.GetInt("Id"),
                      Name = reader.GetStringNullable("Name"),
                      PayorType = reader.GetInt("PayorType"),
                      PayorId = reader.GetStringNullable("PayorId")
                  })
                  .AsSingle();
            }
            return agencyInsurance;
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I =>I.AgencyId == insurance.AgencyId && I.Id == insurance.Id );
            if (insurance != null && existingInsurance!=null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.HasContractWithAgency = insurance.HasContractWithAgency;
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderId = insurance.ProviderId;
                existingInsurance.ProviderSubscriberId = insurance.ProviderSubscriberId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.SubmitterId = insurance.SubmitterId;
                existingInsurance.BillType = insurance.BillType;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                existingInsurance.SubmitterName = insurance.SubmitterName;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.Modified = DateTime.Now;
                if (database.Update<AgencyInsurance>(existingInsurance) > 0)
                {
                    InsuranceEngine.AddOrUpdate(insurance.AgencyId, insurance);
                    result = true;
                }
              
            }
            return result;
        }

        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id  );
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                if (database.Update<AgencyInsurance>(insurance) > 0)
                {
                    InsuranceEngine.AddOrUpdate(agencyId, insurance);
                    return true;
                }
               
            }
            return false;
        }

        public bool IsMedicareHMO(Guid agencyId, int Id)
        {
            var result = false;
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id );
            if (insurance != null)
            {
                result= (insurance.PayorType == 2);
            }
            return result;
        }

        public bool EditInsuranceModal(AgencyInsurance insurance)
        {
            var result = false;
           
            if (insurance != null )
            {
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds)
        {
            var list = new List<InsuranceCache>();
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND ai.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));

                var script = string.Format(@"SELECT 
                        ai.Id,
                        ai.Name as Name,
                        ai.PayorType,
                        ai.IsDeprecated
                                FROM agencyinsurances ai
                                        WHERE ai.AgencyId = @agencyid {0}", additionalScript);

                using (var cmd = new FluentCommand<InsuranceCache>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                      .AddGuid("agencyid", agencyId)
                      .SetMap(reader => new InsuranceCache
                      {
                          Id = reader.GetInt("Id"),
                          Name = reader.GetStringNullable("Name"),
                          PayorType = reader.GetInt("PayorType"),
                          IsDeprecated = reader.GetBoolean("IsDeprecated")
                      })
                      .AsList();
                }
            }
            return list;
        }

        public List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds)
        {
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND ai.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT ai.Id,
                        ai.PayorType, 
                        ai.Name,
                        ai.InvoiceType, 
                        ai.ContactPersonFirstName, 
                        ai.ContactPersonLastName, 
                        ai.PhoneNumber,
                        ai.PayorId 
                                FROM agencyinsurances ai
                                        WHERE ai.AgencyId = @agencyid AND ai.IsDeprecated = 0 {0}", additionalScript);

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare = false
                })
                .AsList();
        }

        public List<AgencyInsurance> GetAgencyInsuranceSelection(Guid agencyId, List<int> onlyIncludedPayorTypes, List<int> excludedPayorTypes)
        {
            var list = new List<AgencyInsurance>();
            var additionalScript = string.Empty;
            if (onlyIncludedPayorTypes != null && onlyIncludedPayorTypes.Count > 0)
            {
                additionalScript += string.Format(" AND ai.PayorType IN ({0}) ", onlyIncludedPayorTypes.ToCommaSeperatedList());
            }
            if (excludedPayorTypes != null && excludedPayorTypes.Count > 0)
            {
                additionalScript += string.Format(" AND ai.PayorType NOT IN ({0}) ", excludedPayorTypes.ToCommaSeperatedList());
            }
            var script = string.Format(@"SELECT 
                ai.Id, 
                ai.PayorType, 
                ai.Name,
                ai.PayorId 
                    FROM 
                        agencyinsurances ai
                            WHERE 
                                ai.AgencyId = @agencyid AND 
                                ai.IsDeprecated = 0 {0}", additionalScript);

            using (var cmd = new FluentCommand<AgencyInsurance>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                  .AddGuid("agencyid", agencyId)
                  .SetMap(reader => new AgencyInsurance
                  {
                      Id = reader.GetInt("Id"),
                      Name = reader.GetStringNullable("Name"),
                      PayorType = reader.GetInt("PayorType"),
                      PayorId = reader.GetStringNullable("PayorId")
                  })
                  .AsList();
            }
            return list;
        }

        public List<AgencyInsurance> GetAgencyInsuranceSelection(Guid agencyId, bool isMedicareHMOOnly, List<int> excludedPayorTypes)
        {
            var list = new List<AgencyInsurance>();
            var additionalScript = string.Empty;
            if (isMedicareHMOOnly)
            {
                additionalScript += string.Format(" AND ai.PayorType = {0} ", (int)PayerTypes.MedicareHMO);
            }
            else
            {
                if (excludedPayorTypes != null && excludedPayorTypes.Count > 0)
                {
                    additionalScript += string.Format(" AND ai.PayorType NOT IN ({0}) ", excludedPayorTypes.ToCommaSeperatedList());
                }
            }
                var script = string.Format(@"SELECT 
                ai.Id, 
                ai.PayorType, 
                ai.Name,
                ai.PayorId 
                    FROM 
                        agencyinsurances ai
                            WHERE 
                                ai.AgencyId = @agencyid AND 
                                ai.IsDeprecated = 0 {0}", additionalScript);

                using (var cmd = new FluentCommand<AgencyInsurance>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                      .AddGuid("agencyid", agencyId)
                      .SetMap(reader => new AgencyInsurance
                      {
                          Id = reader.GetInt("Id"),
                          Name = reader.GetStringNullable("Name"),
                          PayorType = reader.GetInt("PayorType"),
                          PayorId = reader.GetStringNullable("PayorId")
                      })
                      .AsList();
                }
                return list;
        }
       

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
               
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.Find<AgencyHospital>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {

                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
                hospital.IsDeprecated = true;
                hospital.Modified = DateTime.Now;
                database.Update<AgencyHospital>(hospital);
                return true;
            }
            return false;
        }

        #endregion

        #region Template Methods

        public bool AddTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Created = DateTime.Now;
                template.Modified = DateTime.Now;
                database.Add<AgencyTemplate>(template);
                result = true;
                TemplateEngine.AddOrUpdate(template.AgencyId, template);
            }
            return result;
        }

        public IList<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }

        public AgencyTemplate GetTemplate(Guid agencyId, Guid id)
        {
            return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            //return GetTemplates(agencyId).SingleOrDefault(c => c.Id == id && c.IsDeprecated == false);
        }

        public bool UpdateTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Modified = DateTime.Now;
                result = database.Update<AgencyTemplate>(template) > 0;
                if (result)
                {
                    TemplateEngine.AddOrUpdate(template.AgencyId, template);
                }
            }
            return result;
        }

        public bool DeleteTemplate(Guid agencyId, Guid id)
        {
            bool result = false;
            var template = database.Single<AgencyTemplate>(c => c.AgencyId == agencyId && c.Id == id);
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                result = database.Update<AgencyTemplate>(template) > 0;
                if (result)
                {
                    TemplateEngine.Remove(template.AgencyId, template);
                }
            }
            return result;
        }

        public void InsertTemplates(Guid agencyId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Supply Methods

        public bool AddSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.IsDeprecated = false;
                supply.Created = DateTime.Now;
                supply.Modified = DateTime.Now;

                database.Add<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId)
        {
            return database.Find<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Description).ToList();
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit)
        {
            return database.GetPaged<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false && c.Description.Contains(searchTerm), "Description", 0, searchLimit);
        }

        public AgencySupply GetSupply(Guid agencyId, Guid id)
        {
            return database.Single<AgencySupply>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public IList<AgencySupply> GetSuppliesByIds(Guid agencyId, List<Guid> supplyIds)
        {
            var list = new List<AgencySupply>();
            if (supplyIds != null && supplyIds.Count > 0)
            {
                var ids = supplyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                string script = string.Format(@"SELECT Id, RevenueCode, UnitCost, Code
                                                FROM 
                                                    agencysupplies
                                                        WHERE 
                                                            AgencyId = @agencyid AND
                                                            Id IN ( {0} )", ids);

                using (var cmd = new FluentCommand<AgencySupply>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencySupply
                     {
                         Id = reader.GetGuid("Id"),
                         RevenueCode = reader.GetStringNullable("RevenueCode"),
                         UnitCost = reader.GetDouble("UnitCost"),
                         Code = reader.GetStringNullable("Code")
                     }).AsList();
                }
            }
            return list;
        }

        public bool UpdateSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public bool DeleteSupply(Guid agencyId, Guid id)
        {
            var supply = database.Single<AgencySupply>(c => c.AgencyId == agencyId && c.Id == id);
            if (supply != null)
            {
                supply.IsDeprecated = true;
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                return true;
            }
            return false;
        }

        public void InsertSupplies(Guid agencyId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Reports

        public bool AddReport(Report report)
        {
            bool result = false;
            if (report != null)
            {
                database.Add<Report>(report);
                result = true;
            }

            return result;
        }

        public Report GetReport(Guid agencyId, Guid reportId)
        {
            return database.Single<Report>(r => r.Id == reportId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateReport(Report report)
        {
            var result = false;
            if (report != null)
            {
                database.Update<Report>(report);
                result = true;
            }
            return result;
        }

        public IList<ReportLite> GetReports(Guid agencyId, int pageSize, int page)
        {
            var reports = new List<ReportLite>();
            var reportSQL = string.Format(@"SELECT `Id`, `UserId`, `AssetId`, `Type`, `Format`, `Status`, `IsDeprecated`, `Created`, `Completed` FROM `reports` " +
                "WHERE `AgencyId` = @agencyid AND `IsDeprecated` = 0 ORDER BY `Created` DESC LIMIT {0}, {1};", (page - 1) * pageSize, pageSize);

            using (var cmd = new FluentCommand<ReportLite>(reportSQL))
            {
                reports = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ReportLite {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        AssetId = reader.GetGuid("AssetId"),
                        Name = reader.GetString("Type"),
                        Format = reader.GetString("Format"),
                        Status = reader.GetString("Status"),
                        Created = reader.GetDateTime("Created") != DateTime.MinValue ? reader.GetDateTime("Created").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        Completed = reader.GetDateTime("Completed") != DateTime.MinValue ? reader.GetDateTime("Completed").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                    })
                    .AsList();
            }
            return reports;
        }

        public int GetTotalNumberOfReports(Guid agencyId)
        {
            int count = 0;
            var reportSQL = @"SELECT Count(Id) FROM `reports` " +
                "WHERE `AgencyId` = @agencyid AND `IsDeprecated` = 0";

            using (var cmd = new FluentCommand<int>(reportSQL))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AsScalar();
            }
            return count;
        }

        #endregion

        #region CustomerNotes

        public bool AddCustomerNote(CustomerNote note)
        {
            bool result = false;
            if (note != null)
            {
                note.Id = Guid.NewGuid();
                note.Created = DateTime.Now;
                note.Modified = DateTime.Now;
                note.IsDeprecated = false;
                database.Add<CustomerNote>(note);
                result = true;
            }

            return result;
        }

        public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        {
            return database.Single<CustomerNote>(r => r.Id == noteId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateCustomerNote(CustomerNote note)
        {
            var result = false;
            if (note != null)
            {
                database.Update<CustomerNote>(note);
                result = true;
            }
            return result;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        {
            return database.Find<CustomerNote>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.Created).ToList();
        }

        #endregion

        #region Medicare Eligibility Report

        public List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            List<MedicareEligibilitySummary> list = new List<MedicareEligibilitySummary>();
            var script = @"SELECT Id, AgencyId, Created " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.Created BETWEEN @startdate AND @enddate AND me.AgencyId = @agencyid AND me.IsDeprecated = 0";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Created = reader.GetDateTime("Created"),
                })
                .AsList();
            }
            return list;
        }

        public MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId)
        {
            MedicareEligibilitySummary report = new MedicareEligibilitySummary();
            var script = @"SELECT * " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.AgencyId = @agencyid  " +
                "AND me.Id = @id AND me.IsDeprecated = 0 Limit 0,1";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                report = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", reportId)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Report = reader.GetStringNullable("Report"),
                    Created = reader.GetDateTime("Created"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                }).AsSingle();
            }
            
            return report;
        }

        #endregion

        #region Adjustment Codes

        public bool AddAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Created = DateTime.Now;
                code.Modified = DateTime.Now;
                if (code.Id.IsEmpty())
                {
                    code.Id = Guid.NewGuid();
                }
                database.Add<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId)
        {
            IList<AgencyAdjustmentCode> codes = null;
            var cacheKey = string.Format("AdjustmentCodes_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out codes))
            {
                codes = database.Find<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Code).ToList();
                Cacher.Set(cacheKey, codes);
            }
            return codes;
        }

        public AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public bool UpdateAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public bool DeleteAdjustmentCode(Guid agencyId, Guid id)
        {
            bool result = false;
            var code = database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == id);
            if (code != null)
            {
                code.IsDeprecated = true;
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        #endregion

        #region Upload Type

        public bool AddUploadType(UploadType uploadType)
        {
            bool result = false;
            if (uploadType != null)
            {
                uploadType.Created = DateTime.Now;
                uploadType.Modified = DateTime.Now;
                uploadType.IsDeprecated = false;
                if (uploadType.Id.IsEmpty())
                {
                    uploadType.Id = Guid.NewGuid();
                }
                database.Add<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        public IList<UploadType> GetUploadTypes(Guid agencyId)
        {
            IList<UploadType> uploadTypes = null;
            var chacheKey = string.Format("UploadType_{0}", agencyId);
            if (!Cacher.TryGet(chacheKey, out uploadTypes))
            {
                uploadTypes = database.Find<UploadType>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Type).ToList();
                Cacher.Set(chacheKey, uploadTypes);
            }
            return uploadTypes;
        }

        public UploadType FindUploadType(Guid agencyId, Guid Id)
        {
            return database.Single<UploadType>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public bool UpdateUploadType(UploadType uploadType)
        {
            bool result = false;
            if (uploadType != null)
            {
                uploadType.Modified = DateTime.Now;
                database.Update<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        public bool DeleteUploadType(Guid agencyId, Guid id)
        {
            bool result = false;
            var uploadType = database.Single<UploadType>(c => c.AgencyId == agencyId && c.Id == id);
            if (uploadType != null)
            {
                uploadType.IsDeprecated = true;
                uploadType.Modified = DateTime.Now;
                database.Update<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        #endregion

        #region Teams

        public IList<AgencyTeam> GetTeams(Guid agencyId)
        {
            IList<AgencyTeam> teams = null;
            teams = database.Find<AgencyTeam>(t => t.AgencyId == agencyId && t.IsDeprecated == false).OrderBy(t => t.Name).ToList();
            return teams;
        }

        public AgencyTeam GetTeam(Guid agencyid, Guid id)
        {
            return database.Single<AgencyTeam>(t => t.AgencyId == agencyid && t.Id == id && t.IsDeprecated == false);
        }

        public bool AddTeam(AgencyTeam team)
        {
            if (team != null)
            {
                database.Add<AgencyTeam>(team);
            }
            return true;
        }

        public bool UpdateTeam(AgencyTeam team)
        {
            bool result = false;
            if (team != null)
            {
                database.Update<AgencyTeam>(team);
                result = true;
            }
            return result;
        }

        public bool DeleteTeam(Guid agencyId, Guid id)
        {
            bool result = false;
            var team = GetTeam(agencyId, id);
            if (team != null)
            {
                team.IsDeprecated = true;
                database.Update<AgencyTeam>(team);
                result = true;
            }
            return result;
        }

        public IList<SelectedUser> GetTeamUsers(Guid agencyId, Guid teamId, List<User> agencyUsers)
        {
            var users = GetTeam(agencyId, teamId).Users;
            var selectedUsers = users.IsNotNullOrEmpty() ? users.ToObject<List<Guid>>() : new List<Guid>();
            var selectedList = new List<SelectedUser>();
            foreach (var user in agencyUsers)
            {
                var selectedUser = new SelectedUser
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Credentials = user.Credentials,
                    CredentialsOther = user.CredentialsOther,
                    Suffix = user.Suffix,
                    UserId = user.Id,
                    Roles = user.Roles
                };
                if (selectedUsers.Contains(user.Id))
                {
                    selectedList.Add(selectedUser);
                }
            }
            //limit list to clinicians
            var clinician = new List<SelectedUser>();
            foreach (var user in selectedList)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinician.Add(user);
                }
            }
            clinician = clinician.OrderBy(a => a.DisplayName).ToList();
            return clinician;
        }

        public IList<SelectedUser> GetUserNotInTeam(Guid agencyId, Guid agencyTeamId, List<User> agencyUsers)
        {
            var list = new List<SelectedUser>();
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            string users = "";
            if (agencyTeam != null)
            {
                users = agencyTeam.Users;
            }
            var selectedUsers = users.IsNotNullOrEmpty() ? users.ToObject<List<Guid>>() : new List<Guid>();
            foreach (var user in agencyUsers)
            {
                var selectedUser = new SelectedUser
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Credentials = user.Credentials,
                    CredentialsOther = user.CredentialsOther,
                    Suffix = user.Suffix,
                    UserId = user.Id,
                    Roles = user.Roles
                };
                if (!selectedUsers.Contains(user.Id))
                {
                    list.Add(selectedUser);
                }
            }

            //limit list to clinicians
            var clinicians = new List<SelectedUser>();
            foreach (var user in list)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinicians.Add(user);
                }
            }
            clinicians = clinicians.OrderBy(a => a.DisplayName).ToList();
            return clinicians;
        }

        public bool AddUsersToTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers)
        {
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            if (selectedUsers != null && selectedUsers.Count > 0)
            {
                var teamUsers = agencyTeam.Users.ToObject<List<Guid>>() ?? new List<Guid>();
                foreach (var user in selectedUsers)
                {
                    if (!teamUsers.Contains(user))
                    {
                        teamUsers.Add(user);
                    }
                }
                agencyTeam.Users = teamUsers.ToXml();
            }
            database.Update<AgencyTeam>(agencyTeam);
            return true;
        }

        public bool RemoveUsersFromTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers)
        {
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            if (selectedUsers != null && selectedUsers.Count > 0)
            {
                var teamUsers = agencyTeam.Users.ToObject<List<Guid>>() ?? new List<Guid>();
                foreach (var user in selectedUsers)
                {
                    if (teamUsers.Contains(user))
                    {
                        teamUsers.Remove(user);
                    }
                }
                agencyTeam.Users = teamUsers.ToXml();
            }
            database.Update<AgencyTeam>(agencyTeam);
            return true;
        }

        public List<SelectedPatient> GetPatientsInTeam(Guid agencyId, Guid teamId)
        {
            var query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "RIGHT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false";
            var list = new List<SelectedPatient>();
            using (var cmd = new FluentCommand<SelectedPatient>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectedPatient
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        FirstName = reader.GetString("PatientFirstName"),
                        LastName = reader.GetString("PatientLastName"),
                        AgencyTeamId = reader.GetGuid("AgencyTeamId")
                    }).AsList();
            }
            list = list.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            return list;
        }

        public List<SelectedPatient> GetPatientsNotInTeam(Guid agencyId, Guid teamId)
        {
            var query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "LEFT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false " +
                "AND pt.Id is NULL";
            var list = new List<SelectedPatient>();
            using (var cmd = new FluentCommand<SelectedPatient>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectedPatient
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        FirstName = reader.GetString("PatientFirstName"),
                        LastName = reader.GetString("PatientLastName"),
                        AgencyTeamId = reader.GetGuid("AgencyTeamId")
                    }).AsList();
            }
            list = list.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            return list;
        }

        public bool AddPatientsToTeam(Guid agencyTeamId, List<Guid> patientIds)
        {
            foreach (var patientId in patientIds)
            {
                var patientTeam = new PatientTeam
                {
                    Id = Guid.NewGuid(),
                    PatientId = patientId,
                    TeamId = agencyTeamId
                };
                database.Add<PatientTeam>(patientTeam);
            }
            return true;
        }

        public bool RemovePatientsFromTeam(Guid agencyTeamId, List<Guid> patientIds)
        {
            foreach (var patientId in patientIds)
            {
                var patientTeam = database.Single<PatientTeam>(x => x.TeamId == agencyTeamId && x.PatientId == patientId);
                if (patientTeam != null)
                {
                    database.Delete<PatientTeam>(patientTeam.Id);
                }
            }
            return true;
        }

        public List<SelectedPatient> GetTeamAccess(Guid agencyId, Guid currentUserId)
        {
            var teams = GetTeams(agencyId);
            var userTeams = new List<AgencyTeam>();
            var accessablePatients = new List<SelectedPatient>();
            foreach (var team in teams)
            {
                var users = team.Users.ToObject<List<Guid>>();
                if (users != null && users.Contains(currentUserId))
                {
                    userTeams.Add(team);
                }
            }
            foreach (var team in userTeams)
            {
                accessablePatients.AddRange(GetPatientsInTeam(agencyId, team.Id));
            }
            return accessablePatients.Distinct().ToList();
        }


        #endregion
    }
}
