﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    public class PhysicianRepository : IPhysicianRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        

        public PhysicianRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
           
        }
        #endregion

        #region IPhysicianRepository Members

        public bool Add(AgencyPhysician physician)
        {
            var result = false;
            if (physician != null)
            {
                if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count == 3)
                {
                    physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                }
                if (physician.PhoneAltArray != null && physician.PhoneAltArray.Count == 3)
                {
                    physician.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                }
                if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count == 3)
                {
                    physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                }
                physician.Created = DateTime.Now;
                physician.Modified = DateTime.Now;
                database.Add<AgencyPhysician>(physician);
                result = true;
                PhysicianEngine.AddOrUpdate(physician.AgencyId, physician);
            }
            return result;
        }

        public List<CarePlanOversight> GetCPO(Guid physicianLoginId)
        {
            return database.Find<CarePlanOversight>(c => c.PhysicianLoginId == physicianLoginId && c.IsDeprecated == false).OrderByDescending(p => p.LogDate).ToList();
        }

        public CarePlanOversight GetCPOById(Guid id)
        {
            return database.Find<CarePlanOversight>(c => c.Id == id).FirstOrDefault();
        }

        public bool Edit(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                physician.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
              
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.LastName = physician.LastName;
                    physicianInfo.PhoneWork = physician.PhoneWork;
                    physicianInfo.PhoneAlternate = physician.PhoneAlternate;
                    physicianInfo.FaxNumber = physician.FaxNumber;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.LoginId = physician.LoginId;
                    physicianInfo.PhysicianAccess = physician.PhysicianAccess;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    if (database.Update<AgencyPhysician>(physicianInfo) > 0)
                    {
                        result = true;
                        PhysicianEngine.AddOrUpdate(physicianInfo.AgencyId, physicianInfo);
                    }
                }
            }
            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE agencyphysicians sc SET sc.IsDeprecated = 1,sc.Modified = @modified  WHERE AgencyId = @agencyid AND Id = @id;");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", id)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }

                if (count > 0)
                {
                    var physician = PhysicianEngine.GetOnlyFromCatch(id, agencyId);
                    if (physician != null)
                    {
                        physician.IsDeprecated = true;
                        PhysicianEngine.AddOrUpdate(agencyId, physician);
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
            //var physician = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == id);
            //if (physician != null)
            //{
            //    physician.IsDeprecated = true;
            //    physician.Modified = DateTime.Now;
            //    if (database.Update<AgencyPhysician>(physician) > 0)
            //    {
            //        PhysicianEngine.AddOrUpdate(physician.AgencyId, physician);
            //        return true;
            //    }
            //}
            //return false;
        }

        public bool Update(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.LastName = physician.LastName;
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.MiddleName = physician.MiddleName;
                    physicianInfo.Credentials = physician.Credentials;

                    if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count > 2)
                    {
                        physicianInfo.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                    }
                    if (physician.PhoneAltArray != null && physician.PhoneAltArray.Count > 2)
                    {
                        physicianInfo.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                    }
                    if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count > 2)
                    {
                        physicianInfo.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    physicianInfo.PhysicianAccess = physician.PhysicianAccess;
                    physicianInfo.LoginId = physician.LoginId;
                    if (physician.Licenses.IsNotNullOrEmpty())
                    {
                        physicianInfo.Licenses = physician.Licenses;
                    }
                    physicianInfo.Modified = DateTime.Now;
                    if (database.Update<AgencyPhysician>(physicianInfo) > 0)
                    {
                        result = true;
                        PhysicianEngine.AddOrUpdate(physicianInfo.AgencyId, physicianInfo);
                    }
                }
            }
            return result;
        }

        public AgencyPhysician Get(Guid physicianId, Guid agencyId)
        {
            return database.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId);
        }

        public IList<AgencyPhysician> GetByLoginId(Guid loginId)
        {
            return database.Find<AgencyPhysician>(p => p.LoginId == loginId && p.PhysicianAccess == true && p.IsDeprecated == false).ToList();
        }

        public IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId)
        {
            return database.Find<AgencyPhysician>(p => p.AgencyId == agencyId && p.IsDeprecated == false).OrderBy(p => p.FirstName).ToList();
        }

        public IList<AgencyPhysician> GetAgencyPhysiciansWithOutPecosVerification(Guid agencyId)
        {
            List<AgencyPhysician> list = new List<AgencyPhysician>();

            var script = @"SELECT 
                        ap.`Id`,
                        ap.`NPI`,
                        ap.`Credentials`,
                        ap.`FirstName`,
                        ap.`LastName`,
                        ap.`MiddleName`,
                        ap.`Gender`, 
                        ap.`AddressLine1`, 
                        ap.`AddressLine2`, 
                        ap.`AddressCity`, 
                        ap.`AddressStateCode`,
                        ap.`AddressZipCode`,
                        ap.`PhoneWork`,
                        ap.`PhoneAlternate`, 
                        ap.`FaxNumber`,
                        ap.`EmailAddress`,
                        ap.`PhysicianAccess`
                            FROM 
                                agencymanagement.agencyphysicians ap 
                                        WHERE
                                            ap.AgencyId = @agencyid AND
                                            ap.IsDeprecated = 0";


            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    NPI = reader.GetStringNullable("NPI"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Credentials = reader.GetStringNullable("Credentials"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    PhysicianAccess = reader.GetBoolean("PhysicianAccess")
                }).AsList();
            }
           
            return list;
        }

        public IList<AgencyPhysicianGridRow> GetAgencyPhysiciansForGrid(Guid agencyId)
        {
            var list = new List<AgencyPhysicianGridRow>();

            var script = @"SELECT 
                        ap.`Id`,
                        ap.`NPI`,
                        ap.`Credentials`,
                        ap.`FirstName`,
                        ap.`LastName`,
                        ap.`AddressLine1`, 
                        ap.`AddressLine2`, 
                        ap.`AddressCity`, 
                        ap.`AddressStateCode`,
                        ap.`AddressZipCode`,
                        ap.`PhoneWork`,
                        ap.`EmailAddress`,
                        ap.`PhysicianAccess`
                            FROM 
                                agencymanagement.agencyphysicians ap 
                                        WHERE
                                            ap.AgencyId = @agencyid AND
                                            ap.IsDeprecated = 0";


            using (var cmd = new FluentCommand<AgencyPhysicianGridRow>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyPhysicianGridRow(
                new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    NPI = reader.GetStringNullable("NPI"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    Credentials = reader.GetStringNullable("Credentials"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    PhysicianAccess = reader.GetBoolean("PhysicianAccess")
                })).AsList();
            }

            return list;
        }


        //public IList<AgencyPhysician> GetAllPhysicians()
        //{
        //    return database.Find<AgencyPhysician>(p => p.IsDeprecated == false).OrderBy(p => p.FirstName).ToList();
        //}

        //public bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode)
        //{
        //    bool result = false;
        //    if (zipcode.IsNotNullOrEmpty())
        //    {
        //        var agencyPhysician = database.Find<AgencyPhysician>(a => a.AgencyId == agencyId && a.NPI == NPI && a.AddressZipCode == zipcode && a.IsDeprecated == false);
        //        if (agencyPhysician.Count > 0) result = true;
        //    }
        //    return result;
        //}

        public bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode)
        {
            var script = @"SELECT 
                            1
                                FROM 
                                    agencyphysicians 
                                        WHERE 
                                            AgencyId = @agencyid  AND
                                            NPI = @npi AND 
                                            AddressZipCode = @zipcode AND 
                                            IsDeprecated = 0 limit 1";

            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddString("npi", NPI)
                .AddString("zipcode", zipcode)
                .AsScalar();
            }
            return count > 0;
        }


        public IList<AgencyPhysician> GetPatientPhysicians(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var physicians = new List<AgencyPhysician>();
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork  ,
                        agencyphysicians.FaxNumber as FaxNumber,
                        agencyphysicians.NPI as NPI ,
                        agencyphysicians.EmailAddress as EmailAddress,
                        patientphysicians.IsPrimary as IsPrimary
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        agencyphysicians.IsDeprecated = 0";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                physicians = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    Primary = reader.GetBoolean("IsPrimary"),
                    EmailAddress = reader.GetStringNullable("EmailAddress")
                })
                .AsList();
            }
            return physicians;

        }

        public AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId, Guid agencyId)
        {

            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var physicianContact = new AgencyPhysician();
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork  ,
                        agencyphysicians.FaxNumber as FaxNumber,
                        agencyphysicians.NPI as NPI ,
                        agencyphysicians.EmailAddress as EmailAddress,
                        patientphysicians.IsPrimary as IsPrimary
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        patientphysicians.PhysicianId = @physicianId AND
                                        agencyphysicians.IsDeprecated = 0 Limit 0,1";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                physicianContact = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("physicianId", physicianId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    Primary = reader.GetBoolean("IsPrimary"),
                    EmailAddress = reader.GetStringNullable("EmailAddress")
                })
                .AsSingle();
            }


            //Check.Argument.IsNotEmpty(physicianId, "physicianId");
            //Check.Argument.IsNotEmpty(patientId, "patientId");

            //var physicianContact = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == physicianId);
            //var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == physicianId);
            //physicianContact.Primary = patientPhysician.IsPrimary;

            return physicianContact;
        }

        public AgencyPhysician GetPatientPrimaryPhysician(Guid agencyId, Guid patientId)
        {
            AgencyPhysician patientPrimaryPhysician = null;
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork  ,
                        agencyphysicians.FaxNumber as FaxNumber,
                        agencyphysicians.NPI as NPI  
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        patientphysicians.IsPrimary = 1  AND
                                        agencyphysicians.IsDeprecated = 0
                                           LIMIT 1 ";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                patientPrimaryPhysician = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber")

                })
                .AsSingle();
            }
            return patientPrimaryPhysician;
        }

        public AgencyPhysician GetPatientPrimaryOrFirstPhysician(Guid agencyId, Guid patientId)
        {
            AgencyPhysician patientPrimaryPhysician = null;
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork ,
                        agencyphysicians.FaxNumber as FaxNumber ,
                        agencyphysicians.NPI as NPI  
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        agencyphysicians.IsDeprecated = 0
                                            ORDER BY  patientphysicians.IsPrimary DESC
                                                LIMIT 1 ";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                patientPrimaryPhysician = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber")

                })
                .AsSingle();
            }
            return patientPrimaryPhysician;
        }

        public Guid GetPatientPrimaryOrFirstPhysicianId(Guid agencyId, Guid patientId)
        {
            var Id = Guid.Empty;
            var script = @"SELECT 
                            agencyphysicians.Id as Id 
                                FROM
                                    agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                        WHERE 
                                            agencyphysicians.AgencyId = @agencyid  AND
                                            patientphysicians.PatientId = @patientid AND
                                            agencyphysicians.IsDeprecated = 0 
                                                ORDER BY  patientphysicians.IsPrimary DESC
                                                        LIMIT 1 ";

            using (var cmd = new FluentCommand<Guid>(script))
            {
                Id = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => 
                
                     reader.GetGuid("Id")
                )
                .AsSingle();
            }
            return Id;
        }

        public List<AgencyPhysician> GetPhysiciansByIdsLean(Guid agencyId, List<Guid> physicianIds)
        {
            var list = new List<AgencyPhysician>();
            if (physicianIds != null && physicianIds.Count > 0)
            {
                var ids = physicianIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                        agencyphysicians.Id as Id ,
                                        agencyphysicians.FirstName as FirstName ,
                                        agencyphysicians.LastName as LastName ,
                                        agencyphysicians.MiddleName as MiddleName,
                                        agencyphysicians.Credentials as Credentials,
                                        agencyphysicians.NPI as NPI,
                                        agencyphysicians.PhoneWork as PhoneWork,
                                        agencyphysicians.FaxNumber as FaxNumber,
                                        agencyphysicians.PhoneAlternate as PhoneAlternate,
                                        agencyphysicians.EmailAddress as EmailAddress,
                                        agencyphysicians.IsDeprecated as IsDeprecated 
                                            FROM 
                                                agencyphysicians 
                                                    WHERE 
                                                        agencyphysicians.AgencyId = @agencyid AND
                                                        agencyphysicians.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyPhysician>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencyPhysician
                     {
                         Id = reader.GetGuid("Id"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleName = reader.GetStringNullable("MiddleName"),
                         Credentials = reader.GetStringNullable("Credentials"),
                         NPI = reader.GetStringNullable("NPI"),
                         PhoneWork = reader.GetStringNullable("PhoneWork"),
                         FaxNumber = reader.GetStringNullable("FaxNumber"),
                         PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                         EmailAddress = reader.GetStringNullable("EmailAddress"),
                         IsDeprecated = reader.GetBoolean("IsDeprecated"),

                     }).AsList();
                }
            }
            return list;
        }

        public List<AgencyPhysician> GetPatientsPrimaryPhysician(Guid agencyId, List<Guid> patientIds)
        {
            var patientsPrimaryPhysician = new List<AgencyPhysician>();
            if (patientIds != null && patientIds.Count > 0)
            {
                var ids = patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        ap.Id as Id ,
                        ap.FirstName as FirstName ,
                        ap.LastName as LastName , 
                        ap.MiddleName as MiddleName ,
                        ap.Credentials as Credentials,
                        ap.PhoneWork as PhoneWork ,
                        ap.PhoneAlternate as PhoneAlternate ,
                        ap.FaxNumber as FaxNumber ,
                        ap.EmailAddress as EmailAddress ,
                        ap.NPI as NPI,
                        pp.PatientId as PatientId  
                            FROM
                                agencyphysicians ap INNER JOIN patientphysicians pp ON ap.Id = pp.PhysicianId  
                                    WHERE 
                                        ap.AgencyId = @agencyid  AND
                                        pp.PatientId IN ({0}) AND
                                        ap.IsDeprecated = 0 AND
                                        pp.IsPrimary = 1", ids);

                using (var cmd = new FluentCommand<AgencyPhysician>(script))
                {
                    patientsPrimaryPhysician = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyPhysician
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                        Credentials = reader.GetStringNullable("Credentials"),
                        PhoneWork = reader.GetStringNullable("PhoneWork"),
                        PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        NPI = reader.GetStringNullable("NPI"),
                        PatientId = reader.GetGuid("PatientId")
                        
                    })
                    .AsList();
                }
            }
            return patientsPrimaryPhysician;
        }


        #endregion
    }
}
