﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.ServiceModel;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;

    public class ReportService : BaseService, IReportService
    {
        #region IReportService Members

        private static readonly GrouperAgent GrouperAgent = new GrouperAgent();

        public List<List<string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear)
        {
            return CahpsExportByPaymentSources(agencyId, agencyLocationId, sampleMonth, sampleYear, new List<int>());
        }

        public List<List<string>> CahpsExportByPaymentSources(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var collection = new List<List<string>>();
            IDataSpecification dataSpecification = null;

            try
            {
                var agency = Reports.GetAgencyLocation(agencyId, agencyLocationId) ?? new AgencyData();
                if (!agency.IsLocationStandAlone)
                {
                    agency = Reports.GetAgency(agencyId) ?? new AgencyData();
                }
                agency.LocationId = agencyLocationId;

                if (agency != null)
                {
                    switch (agency.CahpsVendor)
                    {
                        case 1: // DSS Research
                            dataSpecification = new DssResearchDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)2: // Synovate Healthcare
                            dataSpecification = new SynovateDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case 3: // Novaetus
                            dataSpecification = new NovaetusDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)4: // Deyta
                        case (int)6: // Fields Research
                            dataSpecification = new DeytaDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)5: // Pinnacle
                            dataSpecification = new PinnacleDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)7: // PressGaney
                            dataSpecification = new PressGaneyDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)8: // Ocs
                            dataSpecification = new OcsDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)9: // Fazzi
                        case (int)11: // Beacon Touch
                            dataSpecification = new FazziDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)10: // Strategic Healthcare
                            dataSpecification = new ShpDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collection;
        }

        public List<Dictionary<string, string>> PPSEpisodeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var episodesInTheDateRange = episodeSchedules.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(startDate, endDate) || s.EndDate.IsBetween(startDate, endDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                        if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                        {
                            var episodesNotInTheDateRange = episodeSchedules.Where(s => s.EndDate.Date < startDate.Date && s.StartDate.Date < s.EndDate.Date).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList()) ?? new Dictionary<ScheduleEvent, List<ScheduleEvent>>();
                            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true);// new int[] { 13, 61, 89, 112 };
                            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };

                            var admissionIds = episodesInTheDateRange.Keys.Where(e => !e.AdmissionId.IsEmpty()).Select(s => s.AdmissionId).Distinct().ToList();
                            var admissions = Reports.GetPatientAdmissions(agencyId, admissionIds) ?? new List<AdmissionPeriod>();

                            var zipCodes = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }

                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = episodeSchedules.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();

                            var assessmentIds = episodeSchedules.Where(w => w.IsAssessmentAndNonOasis()).Select(s => s.Id).ToList();
                            var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);

                            episodesInTheDateRange.ForEach((key, value) =>
                            {
                                var assessmentTask = GetAssessmentTask(episodesInTheDateRange, episodesNotInTheDateRange, key, value, startDate, endDate);
                                var dictionaryResult = GetAssessmentQuestions(assessmentTask, assessmentDatas);
                                var hippsCode = assessmentTask != null ? assessmentTask.HippsCode : string.Empty;
                                var currentPPS = pps.FirstOrDefault(p => p.Time.Year == key.StartDate.Year);
                                var currentHHRG = hhrg.FirstOrDefault(h => h.HIPPS.IsEqual(hippsCode) && h.Time.Year == key.StartDate.Year);
                                var currentCBSA = cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(key.AddressZipCode.IsNotNullOrEmpty() ? key.AddressZipCode : agencyData.AddressZipCode));
                                var prospectivePayment = GetProspectivePayment(currentPPS, currentHHRG, currentCBSA);
                                var orderVisits = value.Where(v => v.IsBillable).OrderBy(v => v.VisitDate).ToList();
                                var firstBillableVisitDate = DateTime.MinValue;
                                if (orderVisits != null && orderVisits.Count > 0)
                                {
                                    var firstBillableVisit = orderVisits.FirstOrDefault();
                                    if (firstBillableVisit != null)
                                    {
                                        firstBillableVisitDate = firstBillableVisit.VisitDate;
                                    }
                                }
                                var admissionPeriod = admissions.FirstOrDefault(a => a.Id == key.AdmissionId);
                                var collection = new Dictionary<string, string>();
                                collection.Add("MRN", key.PatientIdNumber);
                                collection.Add("Patient", string.Concat(key.LastName, ", ", key.FirstName, (!string.IsNullOrEmpty(key.MiddleInitial) ? " " + key.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", key.MedicareNumber.IsNotNullOrEmpty() ? key.MedicareNumber : key.MedicaidNumber.IsNotNullOrEmpty() ? key.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Admission Date", admissionPeriod != null && admissionPeriod.StartOfCareDate.IsValid() ? admissionPeriod.StartOfCareDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Episode #", string.Empty);
                                collection.Add("Episode Type", GetEpisodeTiming(dictionaryResult));
                                collection.Add("NRS Severity Level", GetNrsSeverityLevel(hippsCode));
                                collection.Add("Begin Date", key.StartDate.IsValid() ? key.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", key.EndDate.IsValid() ? key.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", firstBillableVisitDate.IsValid() ? firstBillableVisitDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Status", admissionPeriod != null && admissionPeriod.Status == 1 ? "AD" : "DI");
                                collection.Add("Discharge Date", admissionPeriod != null && admissionPeriod.DischargedDate.IsValid() ? admissionPeriod.DischargedDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Original HHRG", hippsCode);
                                collection.Add("Original Therapy Visits", dictionaryResult != null ? dictionaryResult.AnswerOrEmptyString("M2200NumberOfTherapyNeed") : string.Empty);
                                collection.Add("Original Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                                collection.Add("Original HIPPS", hippsCode);
                                collection.Add("HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                                collection.Add("Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                                collection.Add("HIPPS", prospectivePayment != null ? prospectivePayment.HippsCode : string.Empty);
                                collection.Add("MSA/CBSA", prospectivePayment != null ? prospectivePayment.CbsaCode : string.Empty);
                                collection.Add("Wage Index", prospectivePayment != null ? prospectivePayment.WageIndex : string.Empty);
                                collection.Add("LUPA", "N");
                                collection.Add("PEP", "N");
                                collection.Add("SCIC", "N");
                                collection.Add("Outlier", "N");
                                collection.Add("Therapy Adj", string.Empty);
                                collectionList.Add(collection);
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collectionList;
        }

        public List<Dictionary<string, string>> PPSVisitInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.FirstOrDefault(), sg => sg.ToList()));
                        if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                        {
                            #region Loop
                            var months = GetMonthsBetweenAndAfter(startDate, endDate, 2);
                            eventsInTheDateRange.ForEach((pkey, pvalue) =>
                            {
                                if (pvalue != null && pvalue.Count > 0)
                                {
                                    pvalue.ForEach((ekey, evalue) =>
                                    {
                                        if (ekey != null && !ekey.EpisodeId.IsEmpty() && ekey.StartDate.IsValid() && ekey.EndDate.IsValid())
                                        {
                                            var collection = new Dictionary<string, string>();
                                            collection.Add("MRN", ekey.PatientIdNumber);
                                            collection.Add("Patient", string.Concat(ekey.LastName, ", ", ekey.FirstName, (!string.IsNullOrEmpty(ekey.MiddleInitial) ? " " + ekey.MiddleInitial + "." : string.Empty)));
                                            collection.Add("Policy #", ekey.MedicareNumber.IsNotNullOrEmpty() ? ekey.MedicareNumber : ekey.MedicaidNumber.IsNotNullOrEmpty() ? ekey.MedicaidNumber : string.Empty);
                                            collection.Add("Admission ID", string.Empty);
                                            collection.Add("Begin Date", ekey.StartDate.IsValid() ? ekey.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                            collection.Add("End Date", ekey.EndDate.IsValid() ? ekey.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                            collection.Add("First Billable", GetFirstBillableDate(evalue));
                                            foreach (var month in months)
                                            {
                                                collection.Add(string.Format("{0} {1}", month.Value, month.Key.ToString("yyyy")), GetVisitsInMonth(evalue, month.Key, true));
                                            }
                                            var nursingCount = 0;
                                            var ptCount = 0;
                                            var otCount = 0;
                                            var stCount = 0;
                                            var mswCount = 0;
                                            var hhaCount = 0;
                                            if (evalue != null && evalue.Count > 0)
                                            {
                                                evalue.ForEach(v =>
                                                {
                                                    if (v.IsBillable && v.IsCompleted())
                                                    {
                                                        if (v.Discipline.IsEqual("Nursing"))
                                                        {
                                                            nursingCount++;
                                                        }
                                                        else if (v.Discipline.IsEqual("PT"))
                                                        {
                                                            ptCount++;
                                                        }
                                                        else if (v.Discipline.IsEqual("OT"))
                                                        {
                                                            otCount++;
                                                        }
                                                        else if (v.Discipline.IsEqual("ST"))
                                                        {
                                                            stCount++;
                                                        }
                                                        else if (v.Discipline.IsEqual("MSW"))
                                                        {
                                                            mswCount++;
                                                        }
                                                        else if (v.Discipline.IsEqual("HHA"))
                                                        {
                                                            hhaCount++;
                                                        }
                                                    }
                                                });
                                            }
                                            var totalVisit = nursingCount + ptCount + stCount + otCount + mswCount + hhaCount;
                                            collection.Add("SN", nursingCount.ToString());
                                            collection.Add("PT", ptCount.ToString());
                                            collection.Add("OT", otCount.ToString());
                                            collection.Add("ST", stCount.ToString());
                                            collection.Add("SW", mswCount.ToString());
                                            collection.Add("HHA", hhaCount.ToString());
                                            collection.Add("Total Visits", totalVisit.ToString());
                                            collectionList.Add(collection);
                                        }
                                    });
                                }
                            });
                            Windows.EventLog.WriteEntry(string.Format("Collection Count :{0}", collectionList.Count), System.Diagnostics.EventLogEntryType.Warning);
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSChargeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {

                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var episodesInTheDateRange = episodeSchedules.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(startDate, endDate) || s.EndDate.IsBetween(startDate, endDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                        if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                        {
                            var episodesNotInTheDateRange = episodeSchedules.Where(s => s.EndDate.Date < startDate.Date && s.StartDate.Date > s.EndDate.Date).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList()) ?? new Dictionary<ScheduleEvent, List<ScheduleEvent>>();
                            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                            var zipCodes = episodesInTheDateRange.Keys.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }

                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = episodesInTheDateRange.Keys.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();
                            var months = GetMonthsBetweenAndAfter(startDate, endDate, 2);
                            episodesInTheDateRange.ForEach((key, value) =>
                            {
                                var assessmentTask = GetAssessmentTask(episodesInTheDateRange, episodesNotInTheDateRange, key, value, startDate, endDate);
                                var hippsCode = assessmentTask != null ? assessmentTask.HippsCode : string.Empty;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == key.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS.IsEqual(hippsCode) && h.Time.Year == key.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(key.AddressZipCode.IsNotNullOrEmpty() ? key.AddressZipCode : agencyData.AddressZipCode)));

                                var nursingCount = 0;
                                var ptCount = 0;
                                var otCount = 0;
                                var stCount = 0;
                                var mswCount = 0;
                                var hhaCount = 0;

                                var firstBillableVisitDate = DateTime.MinValue;
                                var visitsConsidered = new List<ScheduleEvent>();
                                if (value != null && value.Count > 0)
                                {
                                    var orderVisits = value.Where(v => v.IsBillable).OrderBy(v => v.VisitDate).ToList();
                                    if (orderVisits != null && orderVisits.Count > 0)
                                    {
                                        var firstBillableVisit = orderVisits.FirstOrDefault();
                                        if (firstBillableVisit != null)
                                        {
                                            firstBillableVisitDate = firstBillableVisit.VisitDate;
                                        }
                                        value.ForEach(v =>
                                        {
                                            if (v.IsBillable)
                                            {
                                                if (v.Discipline.IsEqual("Nursing"))
                                                {
                                                    nursingCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                                else if (v.Discipline.IsEqual("PT"))
                                                {
                                                    ptCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                                else if (v.Discipline.IsEqual("OT"))
                                                {
                                                    otCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                                else if (v.Discipline.IsEqual("ST"))
                                                {
                                                    stCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                                else if (v.Discipline.IsEqual("MSW"))
                                                {
                                                    mswCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                                else if (v.Discipline.IsEqual("HHA"))
                                                {
                                                    hhaCount++;
                                                    visitsConsidered.Add(v);
                                                }
                                            }
                                        });
                                    }
                                }
                                var unitAmount = 0.0;
                                var totalVisit = nursingCount + ptCount + stCount + otCount + mswCount + hhaCount;
                                if (totalVisit > 0 && prospectivePayment != null)
                                {
                                    unitAmount = prospectivePayment.TotalAmount / totalVisit;
                                }
                                var collection = new Dictionary<string, string>();
                                collection.Add("MRN", key.PatientIdNumber);
                                collection.Add("Patient", string.Concat(key.LastName, ", ", key.FirstName, (!string.IsNullOrEmpty(key.MiddleInitial) ? " " + key.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", key.MedicareNumber);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Begin Date", key.StartDate.IsValid() ? key.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", key.EndDate.IsValid() ? key.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", firstBillableVisitDate.IsValid() ? firstBillableVisitDate.ToString("MM/dd/yyyy") : string.Empty);

                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("Total Charges for {0} {1}", month.Value, month.Key.ToString("yyyy")), GetChargesByMonth(visitsConsidered, prospectivePayment, month.Key));
                                }

                                collection.Add("Total Charges for SN", string.Format("${0:#0.00}", nursingCount * unitAmount));
                                collection.Add("Total Charges for PT", string.Format("${0:#0.00}", ptCount * unitAmount));
                                collection.Add("Total Charges for OT", string.Format("${0:#0.00}", otCount * unitAmount));
                                collection.Add("Total Charges for ST", string.Format("${0:#0.00}", stCount * unitAmount));
                                collection.Add("Total Charges for SW", string.Format("${0:#0.00}", mswCount * unitAmount));
                                collection.Add("Total Charges for HHA", string.Format("${0:#0.00}", hhaCount * unitAmount));
                                collection.Add("MS", "$0.00");
                                collection.Add("Total Charges", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("Contractual Adjustments for {0} {1}", month.Value, month.Key.ToString("yyyy")), "$0.00");
                                }
                                collection.Add("Contractual Adjustment Total", "$0.00");

                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("CA Reimbursement for {0} {1}", month.Value, month.Key.ToString("yyyy")), "N/A");
                                }
                                collection.Add("Orig Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                                collection.Add("Adjustment", "0");
                                collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                collectionList.Add(collection);
                            });
                            Windows.EventLog.WriteEntry(string.Format("Collection Count: {0}", collectionList.Count), System.Diagnostics.EventLogEntryType.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSPaymentInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var episodesInTheDateRange = episodeSchedules.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(startDate, endDate) || s.EndDate.IsBetween(startDate, endDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                        if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                        {
                            var episodesNotInTheDateRange = episodeSchedules.Where(s => s.EndDate.Date < startDate.Date && s.StartDate.Date > s.EndDate.Date).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList()) ?? new Dictionary<ScheduleEvent, List<ScheduleEvent>>();
                            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                            var ids = episodesInTheDateRange.Keys.Select(s => s.EpisodeId).Distinct().ToList();
                            var raps = Reports.GetRaps(agencyData.Id, ids);
                            var finals = Reports.GetFinals(agencyData.Id, ids);

                            var zipCodes = episodesInTheDateRange.Keys.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }

                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = episodesInTheDateRange.Keys.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();
                            episodesInTheDateRange.ForEach((key, value) =>
                            {
                                var assessmentTask = GetAssessmentTask(episodesInTheDateRange, episodesNotInTheDateRange, key, value, startDate, endDate);
                                var hippsCode = assessmentTask != null ? assessmentTask.HippsCode : string.Empty;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == key.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS.IsEqual(hippsCode) && h.Time.Year == key.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(key.AddressZipCode.IsNotNullOrEmpty() ? key.AddressZipCode : agencyData.AddressZipCode)));

                                var collection = new Dictionary<string, string>();
                                collection.Add("Patient ID", key.PatientIdNumber);
                                collection.Add("Patient Name", string.Concat(key.LastName, ", ", key.FirstName, (!string.IsNullOrEmpty(key.MiddleInitial) ? " " + key.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", key.MedicareNumber.IsNotNullOrEmpty() ? key.MedicareNumber : key.MedicaidNumber.IsNotNullOrEmpty() ? key.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Begin Date", key.StartDate.Date > DateTime.MinValue.Date ? key.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", key.EndDate.Date > DateTime.MinValue.Date ? key.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", GetFirstBillableDate(value));

                                var rap = raps.FirstOrDefault(r => r.Id == key.EpisodeId && r.PatientId == key.PatientId && r.EpisodeId == key.EpisodeId);//Reports.GetRap(agencyData.Id, episode.PatientId, episode.Id);
                                if (rap != null)
                                {
                                    collection.Add("RAP Bill Date", rap.ClaimDate.Date > DateTime.MinValue.Date ? rap.ClaimDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("RAP Bill Amount", string.Format("${0:#0.00}", rap.ClaimAmount));
                                    collection.Add("RAP Expiration Date", key.EndDate.AddDays(59).ToString("MM/dd/yyyy"));
                                    collection.Add("RAP Payment Received", rap.PaymentDate.Date > DateTime.MinValue.Date ? rap.PaymentDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("RAP Payment Amount", string.Format("${0:#0.00}", rap.PaymentAmount));
                                }

                                var final = finals.FirstOrDefault(f => f.Id == key.EpisodeId && f.PatientId == key.PatientId && f.EpisodeId == key.EpisodeId);//Reports.GetFinal(agencyData.Id, episode.PatientId, episode.Id);
                                if (final != null)
                                {
                                    collection.Add("Final Claim Bill Date", final.ClaimDate.Date > DateTime.MinValue.Date ? final.ClaimDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("Final Claim Payment Received", final.PaymentDate.Date > DateTime.MinValue.Date ? final.PaymentDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("Final Claim Payment Amount", string.Format("${0:#0.00}", final.PaymentAmount));
                                }

                                collection.Add("CA/Other Amount", "$0.00");
                                collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                                collection.Add("Balance Due", GetBalanceDue(prospectivePayment != null ? prospectivePayment.TotalAmount : 0, rap, final));
                                collection.Add("Cost Amount", "$0.00");
                                collection.Add("Margin", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                collectionList.Add(collection);
                            });
                            Windows.EventLog.WriteEntry(string.Format("Collection Count: {0}", collectionList.Count), System.Diagnostics.EventLogEntryType.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<PatientsAndVisitsByAgeResult> PatientsAndVisitsByAge(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientsAndVisitsByAgeResult>();
            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLean(agencyId, agencyLocationId, startDate, endDate, false, false);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var patientIds = episodeSchedules.Select(s => s.PatientId).Distinct().ToList();
                    var patients = Reports.GetPatientDateOfBirths(agencyId, patientIds);
                    var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                    var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { 8, 66, 73, 90, 9, 62, 69 };
                    var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.Key, sg => sg.ToList()));
                    if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                    {
                        #region Loop

                        var tenYearVisits = new List<ScheduleEvent>();
                        var twentyYearVisits = new List<ScheduleEvent>();
                        var thirtyYearVisits = new List<ScheduleEvent>();
                        var fortyYearVisits = new List<ScheduleEvent>();
                        var fiftyYearVisits = new List<ScheduleEvent>();
                        var sixtyYearVisits = new List<ScheduleEvent>();
                        var seventyYearVisits = new List<ScheduleEvent>();
                        var eightyYearVisits = new List<ScheduleEvent>();
                        var ninetyYearVisits = new List<ScheduleEvent>();
                        var overNinetyYearVisits = new List<ScheduleEvent>();
                        eventsInTheDateRange.ForEach((pkey, pvalue) =>
                        {
                            if (pvalue != null && pvalue.Count > 0)
                            {
                                pvalue.ForEach((ekey, evalue) =>
                                {
                                    if (evalue != null && evalue.Count > 0 && evalue.Exists(s => s.EventDate.Date >= startDate.Date && s.EventDate <= endDate.Date))
                                    {
                                        var firstEvent = evalue.FirstOrDefault();
                                        if (firstEvent != null)
                                        {
                                            var patient = patients.SingleOrDefault(p => p.Id == firstEvent.PatientId);
                                            if (patient != null && patient.DOB.IsValid())
                                            {
                                                if (evalue.Exists(s => startOfCare.Contains(s.DisciplineTask)) || episodeSchedules.Exists(s => s.PatientId == pkey && s.EpisodeId != firstEvent.EpisodeId && s.EndDate.Date == firstEvent.StartDate.AddDays(-1) && s.EventDate >= firstEvent.StartDate.AddDays(-5) && s.EventDate <= firstEvent.StartDate.AddDays(-1) && last5DayAssessment.Contains(s.DisciplineTask)))
                                                {
                                                    int age = DateTime.Today.Year - patient.DOB.Year;
                                                    evalue.ForEach(v =>
                                                    {
                                                        if (v.EventDate.Date >= startDate.Date && v.EventDate <= endDate.Date && v.IsBillable)
                                                        {
                                                            if (age > 0 && age <= 10)
                                                            {
                                                                tenYearVisits.Add(v);
                                                            }
                                                            else if (age >= 11 && age <= 20)
                                                            {
                                                                twentyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 21 && age <= 30)
                                                            {
                                                                thirtyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 31 && age <= 40)
                                                            {
                                                                fortyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 41 && age <= 50)
                                                            {
                                                                fiftyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 51 && age <= 60)
                                                            {
                                                                sixtyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 61 && age <= 70)
                                                            {
                                                                seventyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 71 && age <= 80)
                                                            {
                                                                eightyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 81 && age <= 90)
                                                            {
                                                                ninetyYearVisits.Add(v);
                                                            }
                                                            else if (age >= 91 && age <= 1000)
                                                            {
                                                                overNinetyYearVisits.Add(v);
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        });

                        #endregion

                        #region Set Data

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "1.",
                            Description = "0-10 Years",
                            Patients = tenYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = tenYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "2.",
                            Description = "11-20 Years",
                            Patients = twentyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = twentyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "3.",
                            Description = "21-30 Years",
                            Patients = thirtyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = thirtyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "4.",
                            Description = "31-40 Years",
                            Patients = fortyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = fortyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "5.",
                            Description = "41-50 Years",
                            Patients = fiftyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = fiftyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "6.",
                            Description = "51-60 Years",
                            Patients = sixtyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = sixtyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "7.",
                            Description = "61-70 Years",
                            Patients = seventyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = seventyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "8.",
                            Description = "71-80 Years",
                            Patients = eightyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = eightyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "9.",
                            Description = "81-90 Years",
                            Patients = ninetyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = ninetyYearVisits.Count
                        });

                        list.Add(new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "10.",
                            Description = "91 Years and Older",
                            Patients = overNinetyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,
                            Visits = overNinetyYearVisits.Count
                        });
                        var lastData = new PatientsAndVisitsByAgeResult
                        {
                            LineNumber = "15.",
                            Description = "Total",
                            Patients = list.Sum(l => l.Patients),
                            Visits = list.Sum(l => l.Visits)
                        };

                        list.Add(lastData);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return list;
        }

        public List<DischargeByReasonResult> DischargesByReason(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DischargeByReasonResult>();
            try
            {
                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "41.",
                    Description = "Admitted to Hospital",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "42.",
                    Description = "Admitted to SN/IC Facility",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "43.",
                    Description = "Death",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "44.",
                    Description = "Family/Friends Assummed Responsibility",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "45.",
                    Description = "Lack of Funds",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "46.",
                    Description = "Lack of Progress",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "47.",
                    Description = "No Further Home Health Care Needed",
                    Discharges = 0
                });


                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "48.",
                    Description = "Patient Moved out of Area",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "49.",
                    Description = "Patient Refused Service",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "50.",
                    Description = "Physician Request",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "51.",
                    Description = "Transferred to Another HHA",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "52.",
                    Description = "Transferred to Home Care (Personal Care)",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "53.",
                    Description = "Transferred to Hospice",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "54.",
                    Description = "Transferred to Outpatient Rehabilitation",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "59.",
                    Description = "Other, Specify:",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "60.",
                    Description = "Total",
                    Discharges = 0
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrimaryPaymentSourceResult> VisitsByPrimaryPaymentSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var hmoVisits = 0;
            var noneVisits = 0;
            var otherVisits = 0;
            var medicalVisits = 0;
            var tricareVisits = 0;
            var medicareVisits = 0;
            var privatepayVisits = 0;
            var thirdpartyVisits = 0;
            var list = new List<PrimaryPaymentSourceResult>();

            try
            {
                string output = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Cost_{0}.txt", DateTime.Now.Ticks));
                using (System.IO.TextWriter textWriter = new System.IO.StreamWriter(output, false))
                {
                    var insurances = Reports.GetAgencyInsurances(agencyId);
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLean(agencyId, agencyLocationId, startDate, endDate, false, true);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                        var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { 8, 66, 73, 90, 9, 62, 69 };
                        var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.Key, sg => sg.ToList()));
                        if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                        {
                            #region Loop
                            textWriter.WriteLine("Patient Number: {0} ", eventsInTheDateRange.Count);
                            var totalEpisode = 0;
                            eventsInTheDateRange.ForEach((pkey, pvalue) =>
                            {
                                textWriter.WriteLine("Patient: {0} ", pkey);
                                var totalPatientEpisode = 0;
                                if (pvalue != null && pvalue.Count > 0)
                                {
                                    totalPatientEpisode = pvalue.Count;
                                    var validEpisodeTotal = 0;
                                    pvalue.ForEach((ekey, evalue) =>
                                    {
                                        if (evalue != null && evalue.Count > 0 && evalue.Exists(s => s.EventDate.Date >= startDate.Date && s.EventDate <= endDate.Date))
                                        {
                                            validEpisodeTotal++;
                                            var firstEvent = evalue.FirstOrDefault();
                                            if (firstEvent != null)
                                            {
                                                textWriter.WriteLine("Valid Episode: {0} - {1}", firstEvent.StartDate.ToShortDateString(), firstEvent.EndDate.ToShortDateString());
                                                textWriter.WriteLine("Admission Period Id: {0}", firstEvent.AdmissionId);
                                                var detail = firstEvent.EpisodeNotes.ToObject<EpisodeDetail>();
                                                if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                                {
                                                    if (evalue.Exists(s => startOfCare.Contains(s.DisciplineTask)) || episodeSchedules.Exists(s => s.PatientId == pkey && s.EpisodeId != firstEvent.EpisodeId && s.EndDate.Date == firstEvent.StartDate.AddDays(-1) && s.EventDate >= firstEvent.StartDate.AddDays(-5) && s.EventDate <= firstEvent.StartDate.AddDays(-1) && last5DayAssessment.Contains(s.DisciplineTask)))
                                                    {
                                                        textWriter.WriteLine("Primary Insurance: {0}", detail.PrimaryInsurance);
                                                        var visitCount = evalue.Count(v => v.EventDate.Date >= startDate.Date && v.EventDate <= endDate.Date && v.IsBillable);
                                                        textWriter.WriteLine("Visit Count: {0}", visitCount);
                                                        if (detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4"))
                                                        {
                                                            medicareVisits += visitCount;
                                                        }
                                                        else
                                                        {
                                                            if (insurances != null && insurances.Count > 0 && detail.PrimaryInsurance.IsInteger())
                                                            {
                                                                var insurance = insurances.Find(i => i.Id == detail.PrimaryInsurance.ToInteger());
                                                                if (insurance != null)
                                                                {
                                                                    textWriter.WriteLine("Primary Insurance Name: {0}", insurance.Name);
                                                                    textWriter.WriteLine("Primary Insurance Payor Type: {0}", insurance.PayorType);
                                                                    if (insurance.Name.ToLowerInvariant().IsEqual("medi-cal"))
                                                                    {
                                                                        medicalVisits += visitCount;
                                                                    }
                                                                    else
                                                                    {
                                                                        switch (insurance.PayorType)
                                                                        {
                                                                            case 7:
                                                                                tricareVisits += visitCount;
                                                                                break;
                                                                            case 8:
                                                                                thirdpartyVisits += visitCount;
                                                                                break;
                                                                            case 10:
                                                                                privatepayVisits += visitCount;
                                                                                break;
                                                                            case 2:
                                                                            case 4:
                                                                            case 9:
                                                                                hmoVisits += visitCount;
                                                                                break;
                                                                            case 12:
                                                                                noneVisits += visitCount;
                                                                                break;
                                                                            case 11:
                                                                                otherVisits += visitCount;
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            textWriter.WriteLine();
                                        }
                                    });
                                    textWriter.WriteLine("Total valid  Episode Count: {0}", validEpisodeTotal);
                                    textWriter.WriteLine("Total In-valid  Episode Count: {0}", totalPatientEpisode - validEpisodeTotal);

                                }
                                totalEpisode += totalPatientEpisode;
                                textWriter.WriteLine("Total Patient Episode Count: {0}", totalEpisode);
                                textWriter.WriteLine();
                            });
                            textWriter.WriteLine("Total Episode Count: {0}", totalEpisode);
                            textWriter.WriteLine();
                            #endregion
                        }
                    }
                }

                #region Set Data

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "91.",
                    Description = "Medicare",
                    Visits = medicareVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "92.",
                    Description = "Medi-Cal",
                    Visits = medicalVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "93.",
                    Description = "TRICARE(CHAMPUS)",
                    Visits = tricareVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "94.",
                    Description = "Other Third Party (Insurance, etc.)",
                    Visits = thirdpartyVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "95.",
                    Description = "Private (Self Pay)",
                    Visits = privatepayVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "96.",
                    Description = "HMO/PPO (Includes Medicare and Medi-Cal HMOs)",
                    Visits = hmoVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "97.",
                    Description = "No Reimbursement",
                    Visits = noneVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "99.",
                    Description = "Other (Includes MSSP)",
                    Visits = otherVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "100.",
                    Description = "Total",
                    Visits = medicareVisits + medicalVisits + tricareVisits + thirdpartyVisits + privatepayVisits + hmoVisits + noneVisits + otherVisits
                });

                #endregion
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<VisitByStaffTypeResult> VisitsByStaffType(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var otVisits = 0;
            var ptVisits = 0;
            var snVisits = 0;
            var stVisits = 0;
            var hhaVisits = 0;
            var mswVisits = 0;
            var dietVisits = 0;

            var list = new List<VisitByStaffTypeResult>();
            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLean(agencyId, agencyLocationId, startDate, endDate, false, false);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                    var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                    var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.Key, sg => sg.ToList()));
                    if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                    {
                        #region Loop

                        eventsInTheDateRange.ForEach((pkey, pvalue) =>
                        {
                            if (pvalue != null && pvalue.Count > 0)
                            {
                                pvalue.ForEach((ekey, evalue) =>
                                {
                                    if (evalue != null && evalue.Count > 0 && evalue.Exists(s => s.EventDate.Date >= startDate.Date && s.EventDate <= endDate.Date))
                                    {
                                        var firstEvent = evalue.FirstOrDefault();
                                        if (firstEvent != null)
                                        {
                                            if (evalue.Exists(s => startOfCare.Contains(s.DisciplineTask)) || episodeSchedules.Exists(s => s.PatientId == pkey && s.EpisodeId != firstEvent.EpisodeId && s.EndDate.Date == firstEvent.StartDate.AddDays(-1) && s.EventDate >= firstEvent.StartDate.AddDays(-5) && s.EventDate <= firstEvent.StartDate.AddDays(-1) && last5DayAssessment.Contains(s.DisciplineTask)))
                                            {
                                                evalue.ForEach(v =>
                                               {
                                                   if (v.EventDate.Date >= startDate.Date && v.EventDate <= endDate.Date && v.IsBillable)
                                                   {
                                                       if (v.Discipline.IsEqual("Nursing"))
                                                       {
                                                           snVisits++;
                                                       }

                                                       else if (v.Discipline.IsEqual("OT"))
                                                       {
                                                           otVisits++;
                                                       }
                                                       else if (v.Discipline.IsEqual("PT"))
                                                       {
                                                           ptVisits++;
                                                       }

                                                       else if (v.Discipline.IsEqual("MSW"))
                                                       {
                                                           mswVisits++;
                                                       }
                                                       else if (v.Discipline.IsEqual("ST"))
                                                       {
                                                           stVisits++;
                                                       }
                                                       else if (v.Discipline.IsEqual("HHA"))
                                                       {
                                                           hhaVisits++;
                                                       }
                                                       else if (v.Discipline.IsEqual("Dietician"))
                                                       {
                                                           dietVisits++;
                                                       }
                                                   }
                                               });
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        #endregion
                    }
                }
                #region Set Data

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "71.",
                    Description = "Home Health Aide",
                    Visits = hhaVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "72.",
                    Description = "Nutritionist (Diet Counseling)",
                    Visits = dietVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "73.",
                    Description = "Occupational Therapist",
                    Visits = otVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "74.",
                    Description = "Physical Therapist",
                    Visits = ptVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "75.",
                    Description = "Physician",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "76.",
                    Description = "Skilled Nursing",
                    Visits = snVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "77.",
                    Description = "Social Worker",
                    Visits = mswVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "78.",
                    Description = "Speech Pathologist/Audiologist",
                    Visits = stVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "79.",
                    Description = "Spiritual and Pastoral Care",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "84.",
                    Description = "Other",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "85.",
                    Description = "Total",
                    Visits = hhaVisits + otVisits + ptVisits + snVisits + stVisits + mswVisits + dietVisits
                });

                #endregion
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<ReferralSourceResult> AdmissionsByReferralSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ReferralSourceResult>();

            try
            {
                var admissionPeriods = Reports.GetPatientAdmissions(agencyId, startDate, endDate);
                var anotherAgencyAdmissions = GetAdmissionCount(admissionPeriods, 5);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "21.",
                    Description = "Another Home Health Agency",
                    Admissions = anotherAgencyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "22.",
                    Description = "Clinic",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "23.",
                    Description = "Family / Friend",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "24.",
                    Description = "Hospice",
                    Admissions = 0
                });

                var hospitalAdmissions = GetAdmissionCount(admissionPeriods, 3);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "25.",
                    Description = "Hospital (Discharge Planner, etc)",
                    Admissions = hospitalAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "26.",
                    Description = "Local Health Department",
                    Admissions = 0
                });

                var snfAdmissions = GetAdmissionCount(admissionPeriods, 4);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "27.",
                    Description = "Long Term Care Facility (SN/IC)",
                    Admissions = snfAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "28.",
                    Description = "MSSP",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "29.",
                    Description = "Payer (Insurance, HMO etc.)",
                    Admissions = 0
                });

                var phyAdmissions = GetAdmissionCount(admissionPeriods, 2);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "30.",
                    Description = "Physician",
                    Admissions = phyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "31.",
                    Description = "Self",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "32.",
                    Description = "Social Service Agency",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "34.",
                    Description = "Other",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "35.",
                    Description = "Total",
                    Admissions = hospitalAdmissions + snfAdmissions + phyAdmissions + anotherAgencyAdmissions
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrincipalDiagnosisResult> PatientsVisitsByPrincipalDiagnosis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PrincipalDiagnosisResult>();
            var line1 = new PatientVisit();
            var line2 = new PatientVisit();
            var line3 = new PatientVisit();
            var line4 = new PatientVisit();
            var line5 = new PatientVisit();
            var line6 = new PatientVisit();
            var line7 = new PatientVisit();
            var line8 = new PatientVisit();
            var line9 = new PatientVisit();
            var line10 = new PatientVisit();
            var line11 = new PatientVisit();
            var line12 = new PatientVisit();
            var line13 = new PatientVisit();
            var line14 = new PatientVisit();
            var line15 = new PatientVisit();
            var line16 = new PatientVisit();
            var line17 = new PatientVisit();
            var line18 = new PatientVisit();
            var line19 = new PatientVisit();
            var line20 = new PatientVisit();
            var line21 = new PatientVisit();
            var line22 = new PatientVisit();
            var line23 = new PatientVisit();
            var line24 = new PatientVisit();
            var line25 = new PatientVisit();
            var line26 = new PatientVisit();
            var line27 = new PatientVisit();
            var line28 = new PatientVisit();
            var line29 = new PatientVisit();
            var line30 = new PatientVisit();
            var line31 = new PatientVisit();
            var line32 = new PatientVisit();
            var line33 = new PatientVisit();
            var line34 = new PatientVisit();
            var lineTotal = new PatientVisit();
            var uniquePatients = new List<Guid>();
            var uniqueAdmissions = new List<Guid>();

            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var assessmentIds = episodeSchedules.Where(w => w.IsAssessmentAndNonOasis()).Select(s => s.Id).ToList();
                    var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);
                    var episodesInTheDateRange = episodeSchedules.Where(s => s.StartDate.IsBetween(startDate, endDate) || s.EndDate.IsBetween(startDate, endDate)).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                    if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                    {
                        var episodesNotInTheDateRange = episodeSchedules.Where(s => s.EndDate.Date < startDate.Date && s.StartDate.Date > s.EndDate.Date).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList()) ?? new Dictionary<ScheduleEvent, List<ScheduleEvent>>();
                        var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                        var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                        episodesInTheDateRange.ForEach((key, value) =>
                        {
                            if (!key.PatientId.IsEmpty() && key.StartDate.IsValid() && key.EndDate.IsValid())
                            {
                                var assessmentTask = GetAssessmentTask(episodesInTheDateRange, episodesNotInTheDateRange, key, value, startDate, endDate);
                                var dictionaryResult = GetAssessmentQuestions(assessmentTask, assessmentDatas);
                                var principalDiagnosis = string.Empty;
                                if (dictionaryResult != null && dictionaryResult.Count > 0 && dictionaryResult.ContainsKey("M1020ICD9M"))
                                {
                                    principalDiagnosis = dictionaryResult["M1020ICD9M"].Answer.ToLowerInvariant();
                                }
                                var found = false;

                                if (principalDiagnosis.IsNotNullOrEmpty())
                                {
                                    var visitCount = value.Where(s => s.IsBillable).Count();
                                    if (!found)
                                    {
                                        var rangeList1 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 1.0, End = 41.9 },
                                                new DiagnosisRange { Start = 45.0, End = 139.8 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList1))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line1.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line1.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        if (IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 42.0))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line2.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line2.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList3 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 162.2, End = 162.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList3)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.0)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 209.21)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 231.2))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line3.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line3.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList4 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 174.0, End = 174.9 },
                                                new DiagnosisRange { Start = 175.0, End = 175.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList4)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 198.2)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 198.81)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 233.0))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line4.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line4.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {

                                        var rangeList5 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 152.0, End = 154.0 },
                                                new DiagnosisRange { Start = 209.0, End = 209.17 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList5)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 159.0)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.4)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.5)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.8)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.3)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.4)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.7))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line5.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line5.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList6 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 140.0, End = 209.36 },
                                                new DiagnosisRange { Start = 230.0, End = 234.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList6))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line6.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line6.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList7 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 209.40, End = 209.79 },
                                                new DiagnosisRange { Start = 210.0, End = 229.9 },
                                                new DiagnosisRange { Start = 235.0, End = 238.9 },
                                                new DiagnosisRange { Start = 239.0, End = 239.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList7))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line7.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line7.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList8 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 249.0, End = 250.93 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList8))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line8.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line8.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList9 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 240.0, End = 246.9 },
                                                new DiagnosisRange { Start = 251.0, End = 279.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList9))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line9.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line9.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList10 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 280.0, End = 289.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList10))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line10.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line10.Visits += visitCount;
                                        }
                                    }


                                    if (!found)
                                    {
                                        var rangeList11 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 290.0, End = 319 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList11))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line11.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line11.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        if (IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 331.0))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line12.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line12.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList13 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 320.0, End = 330.9 },
                                                new DiagnosisRange { Start = 331.11, End = 389.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList13))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line13.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line13.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList14 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 391.1, End = 392.0 },
                                                new DiagnosisRange { Start = 393.0, End = 402.91 },
                                                new DiagnosisRange { Start = 404.00, End = 429.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList14))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line14.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line14.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList15 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 430, End = 438.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList15))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line15.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line15.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList16 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 403.0, End = 403.91 },
                                                new DiagnosisRange { Start = 440, End = 459.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList16)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 390)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 392.9))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line16.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line16.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList17 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 460, End = 519.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList17))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line17.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line17.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList18 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 520.0, End = 579.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList18))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line18.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line18.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList19 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 580.0, End = 608.9 },
                                                new DiagnosisRange { Start = 614.0, End = 629.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList19))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line19.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line19.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList20 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 610.0, End = 611.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList20))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line20.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line20.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList21 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 630, End = 679.14 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList21))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line21.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line21.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList22 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 680.0, End = 709.9 }
                                            };


                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList22))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line22.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line22.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList23 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 710.0, End = 739.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList23))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line23.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line23.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList24 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 740.0, End = 779.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList24))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line24.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line24.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList25 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 780.01, End = 795.6 },
                                                new DiagnosisRange { Start = 796.0, End = 799.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList25)
                                            || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 795.79))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line25.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line25.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList26 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 800.00, End = 829.1 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList26))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line26.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line26.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList27 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 830.0, End = 959.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList27))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line27.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line27.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList28 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 960.0, End = 995.94 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList28))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line28.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line28.Visits += visitCount;
                                        }
                                    }

                                    if (!found)
                                    {
                                        var rangeList29 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 996.00, End = 999.9 }
                                            };

                                        if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList29))
                                        {
                                            if (!uniqueAdmissions.Contains(key.AdmissionId))
                                            {
                                                line29.AdmissionIds.Add(key.AdmissionId);
                                                uniqueAdmissions.Add(key.AdmissionId);
                                                found = true;
                                            }
                                            line29.Visits += visitCount;
                                        }
                                    }

                                    if (principalDiagnosis.StartsWith("v"))
                                    {
                                        var vCode = principalDiagnosis.Replace("v", "").IsDouble() ? principalDiagnosis.Replace("v", "").ToDouble() : 0.0;
                                        if (!found)
                                        {
                                            var rangeList30 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 20.0, End = 26.9 },
                                                    new DiagnosisRange { Start = 28.0, End = 29.9 }
                                                };

                                            if (IsInDiagnosisRange(vCode, rangeList30))
                                            {
                                                if (!uniqueAdmissions.Contains(key.AdmissionId))
                                                {
                                                    line30.AdmissionIds.Add(key.AdmissionId);
                                                    uniqueAdmissions.Add(key.AdmissionId);
                                                    found = true;
                                                }
                                                line30.Visits += visitCount;
                                            }
                                        }

                                        if (!found)
                                        {
                                            if (IsDiagnosis(vCode, 30.1)
                                                || IsDiagnosis(vCode, 30.2)
                                                || IsDiagnosis(vCode, 31.1)
                                                || IsDiagnosis(vCode, 31.2)
                                                || IsDiagnosis(vCode, 32.1)
                                                || IsDiagnosis(vCode, 32.2)
                                                || IsDiagnosis(vCode, 33.1)
                                                || IsDiagnosis(vCode, 33.2)
                                                || IsDiagnosis(vCode, 34.1)
                                                || IsDiagnosis(vCode, 34.2)
                                                || IsDiagnosis(vCode, 35.1)
                                                || IsDiagnosis(vCode, 35.2)
                                                || IsDiagnosis(vCode, 36.1)
                                                || IsDiagnosis(vCode, 36.2)
                                                || IsDiagnosis(vCode, 37.1)
                                                || IsDiagnosis(vCode, 37.2)
                                                || IsDiagnosis(vCode, 39.1)
                                                || IsDiagnosis(vCode, 39.2)
                                                )
                                            {
                                                if (!uniqueAdmissions.Contains(key.AdmissionId))
                                                {
                                                    line31.AdmissionIds.Add(key.AdmissionId);
                                                    uniqueAdmissions.Add(key.AdmissionId);
                                                    found = true;
                                                }
                                                line31.Visits += visitCount;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList32 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 01.0, End = 07.9 },
                                                    new DiagnosisRange { Start = 09.0, End = 19.8 },
                                                    new DiagnosisRange { Start = 40.0, End = 49.9 }
                                                };

                                            if (IsInDiagnosisRange(vCode, rangeList32))
                                            {
                                                if (!uniqueAdmissions.Contains(key.AdmissionId))
                                                {
                                                    line32.AdmissionIds.Add(key.AdmissionId);
                                                    uniqueAdmissions.Add(key.AdmissionId);
                                                    found = true;
                                                }
                                                line32.Visits += visitCount;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList33 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 50.0, End = 58.9 }
                                                };

                                            if (IsInDiagnosisRange(vCode, rangeList33))
                                            {
                                                if (!uniqueAdmissions.Contains(key.AdmissionId))
                                                {
                                                    line33.AdmissionIds.Add(key.AdmissionId);
                                                    uniqueAdmissions.Add(key.AdmissionId);
                                                    found = true;
                                                }
                                                line33.Visits += visitCount;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList34 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 60.0, End = 91.99 }
                                                };

                                            if (IsInDiagnosisRange(vCode, rangeList34))
                                            {
                                                if (!uniqueAdmissions.Contains(key.AdmissionId))
                                                {
                                                    line34.AdmissionIds.Add(key.AdmissionId);
                                                    uniqueAdmissions.Add(key.AdmissionId);
                                                    found = true;
                                                }
                                                line34.Visits += visitCount;
                                            }
                                        }
                                    }
                                }
                            }

                        });

                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "1.",
                            Description = "Infectious and parasitic diseases (exclude HIV)",
                            Visits = line1.Visits,
                            Patients = line1.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "2.",
                            Description = "HIV infections",
                            Visits = line2.Visits,
                            Patients = line2.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "3.",
                            Description = "Malignant neoplasms: Lung",
                            Visits = line3.Visits,
                            Patients = line3.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "4.",
                            Description = "Malignant neoplasms: Breast",
                            Visits = line4.Visits,
                            Patients = line4.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "5.",
                            Description = "Malignant neoplasms: Intestines",
                            Visits = line5.Visits,
                            Patients = line5.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "6.",
                            Description = "Malignant neoplasms: All other sites, excluding those in lung, breast and intestines",
                            Visits = line6.Visits,
                            Patients = line6.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "7.",
                            Description = "Non-malignant neoplasms: All sites",
                            Visits = line7.Visits,
                            Patients = line7.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "8.",
                            Description = "Diabetes mellitus",
                            Visits = line8.Visits,
                            Patients = line8.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "9.",
                            Description = "Endocrine, metabolic, and nutritional diseases; Immunity disorders",
                            Visits = line9.Visits,
                            Patients = line9.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "10.",
                            Description = "Diseases of blood and blood forming organs",
                            Visits = line10.Visits,
                            Patients = line10.AdmissionIds.Count
                        });

                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "11.",
                            Description = "Mental disorder",
                            Visits = line11.Visits,
                            Patients = line11.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "12.",
                            Description = "Alzheimer's disease",
                            Visits = line12.Visits,
                            Patients = line12.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "13.",
                            Description = "Diseases of nervous system and sense organs",
                            Visits = line13.Visits,
                            Patients = line13.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "14.",
                            Description = "Diseases of cardiovascular system",
                            Visits = line14.Visits,
                            Patients = line14.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "15.",
                            Description = "Diseases of cerebrovascular system",
                            Visits = line15.Visits,
                            Patients = line15.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "16.",
                            Description = "Diseases of all other circulatory system",
                            Visits = line16.Visits,
                            Patients = line16.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "17.",
                            Description = "Diseases of respiratory system",
                            Visits = line17.Visits,
                            Patients = line17.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "18.",
                            Description = "Diseases of digestive system",
                            Visits = line18.Visits,
                            Patients = line18.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "19.",
                            Description = "Diseases of genitourinary system",
                            Visits = line19.Visits,
                            Patients = line19.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "20.",
                            Description = "Diseases of breast",
                            Visits = line20.Visits,
                            Patients = line20.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "21.",
                            Description = "Complications of pregnancy, childbirth, and the puerperium",
                            Visits = line21.Visits,
                            Patients = line21.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "22.",
                            Description = "Diseases of skin and subcutaneous tissue",
                            Visits = line22.Visits,
                            Patients = line22.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "23.",
                            Description = "Diseases of musculoskeletal system and connective tissue (include pathological fx, malunion fx, and nonunion fx)",
                            Visits = line23.Visits,
                            Patients = line23.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "24.",
                            Description = "Congenital anomalies and perinatal conditions (include birth fractures)",
                            Visits = line24.Visits,
                            Patients = line24.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "25.",
                            Description = "Symptoms, signs, and ill-defined conditions (exclude HIV positive test)",
                            Visits = line25.Visits,
                            Patients = line25.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "26.",
                            Description = "Fractures (exclude birth fx, pathological fx, malunion fx, nonunion fx)",
                            Visits = line26.Visits,
                            Patients = line26.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "27.",
                            Description = "All other injuries",
                            Visits = line27.Visits,
                            Patients = line27.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "28.",
                            Description = "Poisonings and adverse effects of external causes",
                            Visits = line28.Visits,
                            Patients = line28.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "29.",
                            Description = "Complications of surgical and medical care",
                            Visits = line29.Visits,
                            Patients = line29.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "30.",
                            Description = "Health services related to reproduction and development",
                            Visits = line30.Visits,
                            Patients = line30.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "31.",
                            Description = "Infants born outside hospital (infant care)",
                            Visits = line31.Visits,
                            Patients = line31.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "32.",
                            Description = "Health hazards related to communicable diseases",
                            Visits = line32.Visits,
                            Patients = line32.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "33.",
                            Description = "Other health services for specific procedures and aftercare",
                            Visits = line33.Visits,
                            Patients = line33.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "34.",
                            Description = "Visits for Evaluation and Assessment",
                            Visits = line34.Visits,
                            Patients = line34.AdmissionIds.Count
                        });
                        lineTotal.Visits = line1.Visits + line2.Visits + line3.Visits + line4.Visits + line5.Visits + line6.Visits + line7.Visits + line8.Visits + line9.Visits + line10.Visits + line11.Visits + line12.Visits + line13.Visits + line14.Visits + line15.Visits + line16.Visits + line17.Visits + line18.Visits + line19.Visits + line20.Visits + line21.Visits + line22.Visits + line23.Visits + line24.Visits + line25.Visits + line26.Visits + line27.Visits + line28.Visits + line29.Visits + line30.Visits + line31.Visits + line32.Visits + line33.Visits + line34.Visits;
                        lineTotal.Patients = line1.AdmissionIds.Count + line2.AdmissionIds.Count + line3.AdmissionIds.Count + line4.AdmissionIds.Count + line5.AdmissionIds.Count + line6.AdmissionIds.Count + line7.AdmissionIds.Count + line8.AdmissionIds.Count + line9.AdmissionIds.Count + line10.AdmissionIds.Count + line11.AdmissionIds.Count + line12.AdmissionIds.Count + line13.AdmissionIds.Count + line14.AdmissionIds.Count + line15.AdmissionIds.Count + line16.AdmissionIds.Count + line17.AdmissionIds.Count + line18.AdmissionIds.Count + line19.AdmissionIds.Count + line20.AdmissionIds.Count + line21.AdmissionIds.Count + line22.AdmissionIds.Count + line23.AdmissionIds.Count + line24.AdmissionIds.Count + line25.AdmissionIds.Count + line26.AdmissionIds.Count + line27.AdmissionIds.Count + line28.AdmissionIds.Count + line29.AdmissionIds.Count + line30.AdmissionIds.Count + line31.AdmissionIds.Count + line32.AdmissionIds.Count + line33.AdmissionIds.Count + line34.AdmissionIds.Count;
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "45.",
                            Description = "TOTAL",
                            Visits = lineTotal.Visits,
                            Patients = lineTotal.Patients
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public CostReportResult CostReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var report = new CostReportResult();
            var snMedicarePatients = new List<Guid>();
            var ptMedicarePatients = new List<Guid>();
            var otMedicarePatients = new List<Guid>();
            var stMedicarePatients = new List<Guid>();
            var mswMedicarePatients = new List<Guid>();
            var hhaMedicarePatients = new List<Guid>();

            var snNonMedicarePatients = new List<Guid>();
            var ptNonMedicarePatients = new List<Guid>();
            var otNonMedicarePatients = new List<Guid>();
            var stNonMedicarePatients = new List<Guid>();
            var mswNonMedicarePatients = new List<Guid>();
            var hhaNonMedicarePatients = new List<Guid>();

            var medicareDuplicated = new List<Guid>();
            var nonMedicareDuplicated = new List<Guid>();

            int hhaMedicareMinutes = 0;
            int hhaNonMedicareMinutes = 0;

            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLean(agencyId, agencyLocationId, startDate, endDate, true, true);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                    var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                    var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.Key, sg => sg.ToList()));
                    if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                    {
                        #region Loop

                        eventsInTheDateRange.ForEach((pkey, pvalue) =>
                        {
                            if (pvalue != null && pvalue.Count > 0)
                            {
                                pvalue.ForEach((ekey, evalue) =>
                                {
                                    if (evalue != null && evalue.Count > 0 && evalue.Exists(s => s.EventDate.Date >= startDate.Date && s.EventDate <= endDate.Date))
                                    {
                                        var firstEvent = evalue.FirstOrDefault();
                                        if (firstEvent != null)
                                        {
                                            if (firstEvent.EpisodeNotes.IsNotNullOrEmpty())
                                            {

                                                if (evalue.Exists(s => startOfCare.Contains(s.DisciplineTask)) || episodeSchedules.Exists(s => s.PatientId == pkey && s.EpisodeId != firstEvent.EpisodeId && s.EndDate.Date == firstEvent.StartDate.AddDays(-1) && s.EventDate >= firstEvent.StartDate.AddDays(-5) && s.EventDate <= firstEvent.StartDate.AddDays(-1) && last5DayAssessment.Contains(s.DisciplineTask)))
                                                {
                                                    var IsMedicare = false;
                                                    var detail = firstEvent.EpisodeNotes.ToObject<EpisodeDetail>();
                                                    if (detail != null && detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4"))
                                                    {
                                                        IsMedicare = true;
                                                    }
                                                    evalue.ForEach(v =>
                                                    {
                                                        if (v.EventDate.Date >= startDate.Date && v.EventDate <= endDate.Date && v.IsBillable)
                                                        {
                                                            if (IsMedicare)
                                                            {
                                                                medicareDuplicated.Add(v.PatientId);
                                                                if (v.Discipline.IsEqual("Nursing"))
                                                                {
                                                                    snMedicarePatients.Add(v.AdmissionId);
                                                                    report.SNMedicareVisits++;
                                                                }

                                                                else if (v.Discipline.IsEqual("OT"))
                                                                {
                                                                    otMedicarePatients.Add(v.AdmissionId);
                                                                    report.OTMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("PT"))
                                                                {
                                                                    ptMedicarePatients.Add(v.AdmissionId);
                                                                    report.PTMedicareVisits++;
                                                                }

                                                                else if (v.Discipline.IsEqual("MSW"))
                                                                {
                                                                    mswMedicarePatients.Add(v.AdmissionId);
                                                                    report.MSWMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("ST"))
                                                                {
                                                                    stMedicarePatients.Add(v.AdmissionId);
                                                                    report.STMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("HHA"))
                                                                {
                                                                    hhaMedicareMinutes += GetMinutesSpent(v);
                                                                    hhaMedicarePatients.Add(v.AdmissionId);
                                                                    report.HHAMedicareVisits++;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                nonMedicareDuplicated.Add(v.PatientId);
                                                                if (v.Discipline.IsEqual("Nursing"))
                                                                {
                                                                    snNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.SNNonMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("OT"))
                                                                {
                                                                    otNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.OTNonMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("PT"))
                                                                {
                                                                    ptNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.PTNonMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("MSW"))
                                                                {
                                                                    mswNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.MSWNonMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("ST"))
                                                                {
                                                                    stNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.STNonMedicareVisits++;
                                                                }
                                                                else if (v.Discipline.IsEqual("HHA"))
                                                                {
                                                                    hhaNonMedicareMinutes += GetMinutesSpent(v);
                                                                    hhaNonMedicarePatients.Add(v.AdmissionId);
                                                                    report.HHANonMedicareVisits++;
                                                                }
                                                            }

                                                        }
                                                    });
                                                }

                                            }
                                        }
                                    }
                                });
                            }
                        }
                        );

                        #endregion

                        #region Set Data

                        report.SNMedicarePatients = snMedicarePatients.Distinct().Count();
                        report.PTMedicarePatients = ptMedicarePatients.Distinct().Count();
                        report.OTMedicarePatients = otMedicarePatients.Distinct().Count();
                        report.STMedicarePatients = stMedicarePatients.Distinct().Count();
                        report.MSWMedicarePatients = mswMedicarePatients.Distinct().Count();
                        report.HHAMedicarePatients = hhaMedicarePatients.Distinct().Count();

                        report.SNNonMedicarePatients = snNonMedicarePatients.Distinct().Count();
                        report.PTNonMedicarePatients = ptNonMedicarePatients.Distinct().Count();
                        report.OTNonMedicarePatients = otNonMedicarePatients.Distinct().Count();
                        report.STNonMedicarePatients = stNonMedicarePatients.Distinct().Count();
                        report.MSWNonMedicarePatients = mswNonMedicarePatients.Distinct().Count();
                        report.HHANonMedicarePatients = hhaNonMedicarePatients.Distinct().Count();

                        report.SNTotalPatients = report.SNMedicarePatients + report.SNNonMedicarePatients;
                        report.PTTotalPatients = report.PTMedicarePatients + report.PTNonMedicarePatients;
                        report.OTTotalPatients = report.OTMedicarePatients + report.OTNonMedicarePatients;
                        report.STTotalPatients = report.STMedicarePatients + report.STNonMedicarePatients;
                        report.MSWTotalPatients = report.MSWMedicarePatients + report.MSWNonMedicarePatients;
                        report.HHATotalPatients = report.HHAMedicarePatients + report.HHANonMedicarePatients;

                        report.MedicareUnduplicated = medicareDuplicated.Distinct().Count();
                        report.NonMedicareUnduplicated = nonMedicareDuplicated.Distinct().Count();
                        report.TotalUnduplicated = report.MedicareUnduplicated + report.NonMedicareUnduplicated;

                        report.HHAMedicareHours = Convert.ToInt32(hhaMedicareMinutes / 60);
                        report.HHANonMedicareHours = Convert.ToInt32(hhaNonMedicareMinutes / 60);

                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return report;
        }



        public List<Dictionary<string, string>> TherapyManagement(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();

            var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
            if (agencyData != null)
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var assessmentIds = episodeSchedules.Where(w => w.IsAssessment()).Select(s => s.Id).ToList();
                    var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);
                    var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                    var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                    var eventsInTheDateRange = episodeSchedules.GroupBy(s => s.PatientId).ToDictionary(g => g.Key, g => g.GroupBy(sg => sg.EpisodeId).ToDictionary(sg => sg.FirstOrDefault() ?? new ScheduleEvent() { EpisodeId = sg.Key }, sg => sg.OrderByDescending(s => s.EventDate).ToList()));
                    if (eventsInTheDateRange != null && eventsInTheDateRange.Count > 0)
                    {
                        #region Loop

                        eventsInTheDateRange.ForEach((pkey, pvalue) =>
                        {
                            if (pvalue != null && pvalue.Count > 0)
                            {
                                var patientFirstEpisode = pvalue.Keys.ToList();

                                pvalue.ForEach((ekey, evalue) =>
                                {
                                    if (!ekey.PatientId.IsEmpty() && ekey.StartDate.IsValid() && ekey.EndDate.IsValid())
                                    {
                                        if (ekey != null && (ekey.StartDate.IsBetween(startDate, endDate) || ekey.EndDate.IsBetween(startDate, endDate)))
                                        {
                                            if (evalue != null && evalue.Count > 0)
                                            {
                                                var typeDescription = string.Empty;
                                                string mooo = "";
                                                var assessmentTask = GetAssessmentTask(pvalue, null, ekey, evalue, startDate, endDate);
                                                if (assessmentTask != null)
                                                {
                                                    typeDescription = assessmentTask.GetOasisType().GetDescription();
                                                    var dictionaryResult = GetAssessmentQuestions(assessmentTask, assessmentDatas);
                                                    if (dictionaryResult != null && dictionaryResult.Count > 0)
                                                    {
                                                        mooo = dictionaryResult.AnswerOrEmptyString("M2200NumberOfTherapyNeed");
                                                    }
                                                }
                                                var collection = new Dictionary<string, string>();
                                                collection.Add("MRN", ekey.PatientIdNumber);
                                                collection.Add("Patient", ekey.PatientName);
                                                collection.Add("Episode", ekey.StartDate.ToString("MM/dd/yyyy") + " - " + ekey.EndDate.ToString("MM/dd/yyyy"));
                                                collection.Add("Assessment", typeDescription);
                                                collection.Add("M2200", mooo);

                                                int otCount = 0;
                                                int ptCount = 0;
                                                int stCount = 0;
                                                int otCompletedCount = 0;
                                                int ptCompletedCount = 0;
                                                int stCompletedCount = 0;
                                                evalue.ForEach(evnt =>
                                                {
                                                    if (evnt.IsBillable)
                                                    {
                                                        if (evnt.IsPTNote())
                                                        {
                                                            if (evnt.IsCompleted())
                                                            {
                                                                ptCompletedCount++;
                                                            }
                                                            ptCount++;
                                                        }
                                                        else if (evnt.IsOTNote())
                                                        {

                                                            if (evnt.IsCompleted())
                                                            {
                                                                otCompletedCount++;
                                                            }
                                                            otCount++;
                                                        }
                                                        else if (evnt.IsSTNote())
                                                        {
                                                            if (evnt.IsCompleted())
                                                            {
                                                                stCompletedCount++;
                                                            }
                                                            stCount++;
                                                        }
                                                    }
                                                });
                                                collection.Add("Completed PT Visits", ptCompletedCount.ToString());
                                                collection.Add("Scheduled PT Visits", ptCount.ToString());
                                                collection.Add("Completed OT Visits", otCompletedCount.ToString());
                                                collection.Add("Scheduled OT Visits", otCount.ToString());
                                                collection.Add("Completed ST Visits", stCompletedCount.ToString());
                                                collection.Add("Scheduled ST Visits", stCount.ToString());
                                                int totalScheduled = ptCount + otCount + stCount;
                                                int totalCompleted = ptCompletedCount + otCompletedCount + stCompletedCount;
                                                collection.Add("Total Completed Visits", totalCompleted.ToString());
                                                collection.Add("Total Scheduled Visits", totalScheduled.ToString());
                                                collections.Add(collection);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        #endregion
                    }
                }
            }
            return collections;
        }

        public List<Dictionary<string, string>> HHRGReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collections = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASISForHHRG(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var assessmentIds = episodeSchedules.Where(w => w.IsAssessmentAndNonOasis()).Select(s => s.Id).ToList();
                        var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);
                        var episodesInTheDateRange = episodeSchedules.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(startDate, endDate) || s.EndDate.IsBetween(startDate, endDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                        if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                        {
                            var episodesNotInTheDateRange = episodeSchedules.Where(s => s.EndDate.Date < startDate.Date && s.StartDate.Date > s.EndDate.Date).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList()) ?? new Dictionary<ScheduleEvent, List<ScheduleEvent>>();
                            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true); //new int[] { 13, 61, 89, 112 };
                            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
                            var agencyLocationMedicareInsurance = Reports.GetMedicareInsurance(agencyData.Payor);
                            var insurances = new List<InsuranceData>();
                            if (agencyData.IsLocationStandAlone)
                            {
                                insurances = Reports.GetAgencyInsurancesByBranch(agencyId, agencyLocationId);
                            }
                            else
                            {
                                insurances = Reports.GetAgencyInsurances(agencyId);
                            }
                            var admissionIds = episodesInTheDateRange.Keys.Where(e => !e.AdmissionId.IsEmpty()).Select(s => s.AdmissionId).Distinct().ToList();
                            var admissions = Reports.GetPatientAdmissions(agencyId, admissionIds) ?? new List<AdmissionPeriod>();

                            var zipCodes = episodesInTheDateRange.Keys.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }

                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = episodesInTheDateRange.Keys.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();
                            episodesInTheDateRange.ForEach((key, value) =>
                            {
                                var assessmentTask = GetAssessmentTask(episodesInTheDateRange, episodesNotInTheDateRange, key, value, startDate, endDate);
                                var dictionaryResult = GetAssessmentQuestions(assessmentTask, assessmentDatas);

                                var hippsCode = assessmentTask != null ? assessmentTask.HippsCode : string.Empty;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == key.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS.IsEqual(hippsCode) && h.Time.Year == key.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(key.AddressZipCode.IsNotNullOrEmpty() ? key.AddressZipCode : agencyData.AddressZipCode)));

                                EpisodeDetail episodeDetail = key.EpisodeNotes.IsNotNullOrEmpty() ? key.EpisodeNotes.ToObject<EpisodeDetail>() : null;
                                var admissionPeriod = admissions.FirstOrDefault(a => a.Id == key.AdmissionId);

                                var collection = new Dictionary<string, string>();
                                collection.Add("MRN", key.PatientIdNumber);
                                collection.Add("Patient", string.Concat(key.LastName, ", ", key.FirstName, (!string.IsNullOrEmpty(key.MiddleInitial) ? " " + key.MiddleInitial + "." : string.Empty)));

                                int primaryInsurnaceInt = episodeDetail != null && episodeDetail.PrimaryInsurance.IsNotNullOrEmpty() ? episodeDetail.PrimaryInsurance.ToInteger() : 0;

                                if (primaryInsurnaceInt > 1000)
                                {
                                    var primaryInsurnace = insurances.FirstOrDefault(p => p.Id == primaryInsurnaceInt);
                                    collection.Add("Insurance", primaryInsurnace != null ? primaryInsurnace.Name : string.Empty);
                                }
                                else
                                {
                                    collection.Add("Insurance", agencyLocationMedicareInsurance.Name);
                                }
                                collection.Add("Admission Date", admissionPeriod != null && admissionPeriod.StartOfCareDate.IsValid() ? admissionPeriod.StartOfCareDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Episode Timing", GetEpisodeTiming(dictionaryResult));
                                collection.Add("Episode", key.StartDate.ToString("MM/dd/yyyy") + " - " + key.EndDate.ToString("MM/dd/yyyy"));
                                collection.Add("Status", admissionPeriod != null && admissionPeriod.Status == 1 ? "Active" : "Discharged");
                                collection.Add("Discharge Date", admissionPeriod != null && admissionPeriod.Status == 2 && admissionPeriod.DischargedDate.IsValid() ? admissionPeriod.DischargedDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Original Therapy Visit", dictionaryResult != null ? dictionaryResult.AnswerOrEmptyString("M2200NumberOfTherapyNeed").ToString() : string.Empty);
                                collection.Add("Original HIPPS", hippsCode);
                                collection.Add("Original HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                                collection.Add("Original Prospective Pay", prospectivePayment != null ? prospectivePayment.TotalAmount.ToString() : string.Empty);

                                int actualVisits = 0;
                                int actualCompletedVisits = 0;
                                if (value != null && value.Count > 0)
                                {
                                    value.ForEach((evnt) =>
                                    {
                                        if (evnt.IsBillable && (DisciplineTaskFactory.AllTherapy(true).Contains(evnt.DisciplineTask)))
                                        {
                                            if (evnt.IsCompleted())
                                            {
                                                actualCompletedVisits++;
                                            }
                                            actualVisits++;
                                        }
                                    });
                                }
                                Hipps hippsCodeNew = null;
                                prospectivePayment = null;
                                string actualVisitsString = actualCompletedVisits.ToString();
                                actualVisitsString = actualVisitsString.PadLeft(3, '0');
                                if (assessmentTask != null)
                                {
                                    string oasisFormat = "";
                                    if (assessmentTask.SubmissionFormat.IsNotNullOrEmpty())
                                    {
                                        oasisFormat = assessmentTask.SubmissionFormat.Remove(864, 3);
                                        oasisFormat = oasisFormat.Insert(864, actualVisitsString);
                                        hippsCodeNew = GrouperAgent.GetHippsCode(oasisFormat);
                                        if (hippsCodeNew != null && hippsCodeNew.Code.IsNotNullOrEmpty())
                                        {
                                            prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == key.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS.IsEqual(hippsCodeNew.Code) && h.Time.Year == key.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(key.AddressZipCode.IsNotNullOrEmpty() ? key.AddressZipCode : agencyData.AddressZipCode)));
                                        }
                                    }
                                }
                                collection.Add("Actual Completed Therapy Visit", actualCompletedVisits.ToString());
                                collection.Add("Actual Scheduled Therapy Visit", actualVisits.ToString());
                                collection.Add("Actual HIPPS", hippsCodeNew != null ? hippsCodeNew.Code : string.Empty);
                                collection.Add("Actual HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                                collection.Add("Actual Prospective Pay", prospectivePayment != null ? prospectivePayment.TotalAmount.ToString() : string.Empty);
                                collections.Add(collection);
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collections;
        }

        public List<Dictionary<string, string>> UnbilledVisitsForManagedClaims(Guid agencyId, Guid branchId, int insurance, int status, DateTime startDate, DateTime endDate)
        {
            var collections = new List<Dictionary<string, string>>();
            try
            {
                var allBilledVisitsIntheRange = new List<ScheduleEvent>();
                var managedClaims = Reports.GetManagedClaimsByInsurance(agencyId, branchId, insurance, startDate, endDate);
                if (managedClaims != null && managedClaims.Count > 0)
                {
                    managedClaims.ForEach(m =>
                    {
                        if (m.VerifiedVisits.IsNotNullOrEmpty())
                        {
                            var billedVisits = m.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                            if (billedVisits != null && billedVisits.Count > 0)
                            {
                                var filteredVisits = billedVisits.Where(s => s.VisitDate.IsBetween(startDate, endDate)).ToList();
                                if (filteredVisits != null && filteredVisits.Count > 0)
                                {
                                    allBilledVisitsIntheRange.AddRange(filteredVisits);
                                }
                            }
                        }
                    });
                }
                var scheduleEvents = Reports.GetEpisodeScheduleEventsForManagedClaimBill(agencyId, branchId, insurance, status, startDate, endDate, true, allBilledVisitsIntheRange);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(s =>
                    {
                        var collection = new Dictionary<string, string>();
                        collection.Add("MRN", s.PatientIdNumber);
                        collection.Add("Patient", string.Concat(s.LastName, ", ", s.FirstName, (!string.IsNullOrEmpty(s.MiddleInitial) ? " " + s.MiddleInitial + "." : string.Empty)));
                        collection.Add("Visit", s.DisciplineTask.ToString());
                        collection.Add("Employee", s.UserId.ToString());
                        collection.Add("Date", s.VisitDate.ToString("MM/dd/yyyy"));
                        collection.Add("Episode Range", string.Format("{0} - {1}", s.StartDate.ToString("MM/dd/yyyy"), s.EndDate.ToString("MM/dd/yyyy")));
                        collections.Add(collection);
                    });
                }
            }
            catch (Exception e)
            {
                Windows.EventLog.WriteEntry(e.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(e.ToString());
            }
            return collections;
        }

        #endregion

        #region Private Method

        private ScheduleEvent GetAssessmentTask(IDictionary<ScheduleEvent, List<ScheduleEvent>> episodesInTheDateRange, ScheduleEvent key, List<ScheduleEvent> value, DateTime startDate, DateTime endDate)
        {
            return GetAssessmentTask(episodesInTheDateRange, null, key, value, startDate, endDate);
        }

        private ScheduleEvent GetAssessmentTask(IDictionary<ScheduleEvent, List<ScheduleEvent>> episodesInTheDateRange, IDictionary<ScheduleEvent, List<ScheduleEvent>> episodesNotInTheDateRange, ScheduleEvent key, List<ScheduleEvent> value, DateTime startDate, DateTime endDate)
        {
            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true);
            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true);
            var assessmentTask = value.FirstOrDefault(s => !s.Id.IsEmpty() && startOfCare.Contains(s.DisciplineTask));
            if (assessmentTask == null)
            {
                if (key.StartDate.Date > startDate.Date && key.StartDate.Date <= endDate.Date)
                {
                    var previousEpisode = episodesInTheDateRange.FirstOrDefault(p => p.Key.PatientId == key.PatientId && p.Key.EpisodeId != key.EpisodeId && p.Key.EndDate.Date == key.StartDate.AddDays(-1).Date);
                    if (!previousEpisode.Equals(default(KeyValuePair<ScheduleEvent, List<ScheduleEvent>>)))
                    {
                        var episodeEvents = previousEpisode.Value;
                        if (episodeEvents != null && episodeEvents.Count > 0)
                        {
                            assessmentTask = episodeEvents.OrderByDescending(s => s.EventDate).LastOrDefault(s => !s.Id.IsEmpty() && last5DayAssessment.Contains(s.DisciplineTask) && s.EventDate.IsBetween(previousEpisode.Key.EndDate.AddDays(-5), previousEpisode.Key.EndDate));
                        }
                    }
                }
                else if (episodesNotInTheDateRange != null && episodesNotInTheDateRange.Count > 0 && key.StartDate.Date <= startDate.Date)
                {
                    var previousEpisode = episodesNotInTheDateRange.FirstOrDefault(p => p.Key.PatientId == key.PatientId && p.Key.EpisodeId != key.EpisodeId && p.Key.EndDate.Date == key.StartDate.AddDays(-1).Date);
                    if (!previousEpisode.Equals(default(KeyValuePair<ScheduleEvent, List<ScheduleEvent>>)))
                    {
                        var episodeEvents = previousEpisode.Value;
                        if (episodeEvents != null && episodeEvents.Count > 0)
                        {
                            assessmentTask = episodeEvents.OrderByDescending(s => s.EventDate).LastOrDefault(s => !s.Id.IsEmpty() && last5DayAssessment.Contains(s.DisciplineTask) && s.EventDate.IsBetween(previousEpisode.Key.EndDate.AddDays(-5), previousEpisode.Key.EndDate));
                        }
                    }
                }
            }
            return assessmentTask;
        }

        private IDictionary<string, Question> GetAssessmentQuestions(ScheduleEvent assessmentTask, List<AssessmentQuestionData> assessmentDatas)
        {
            IDictionary<string, Question> dictionaryResult = null;
            if (assessmentTask != null)
            {
                var assessmentData = assessmentDatas.FirstOrDefault(t => t.Id == assessmentTask.Id);
                if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                {
                    dictionaryResult = assessmentData.ToDictionary();
                }
            }
            return dictionaryResult;
        }

        private int GetMinutesSpent(ScheduleEvent scheduleEvent)
        {
            var timeIn = DateTime.Now;
            var timeOut = DateTime.Now;
            if (scheduleEvent.TimeOut.IsNotNullOrEmpty() && scheduleEvent.TimeOut.HourToDateTime(ref timeOut) && scheduleEvent.TimeIn.IsNotNullOrEmpty() && scheduleEvent.TimeIn.HourToDateTime(ref timeIn))
            {
                if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO;
                }
                else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO + 12 * 60;

                }
                else
                {
                    if (timeOut >= timeIn)
                    {
                        return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                    }
                }
            }
            return 0;
        }

        private bool IsDiagnosis(double principalDiagnosis, double diagnosis)
        {
            bool result = false;

            if (principalDiagnosis == diagnosis)
            {
                result = true;
            }
            return result;
        }

        private bool IsInDiagnosisRange(double principalDiagnosis, List<DiagnosisRange> rangeList)
        {
            bool result = false;

            foreach (var range in rangeList)
            {
                if (principalDiagnosis >= range.Start && principalDiagnosis <= range.End)
                {
                    return true;
                }
            }
            return result;
        }

        private int GetAdmissionCount(List<AdmissionPeriod> periods, int admissionSourceType)
        {
            var result = 0;
            if (periods != null)
            {
                periods.ForEach(period =>
                {
                    var profile = period.ProfileData.IsNotNullOrEmpty() ? period.ProfileData.ToObject<Profile>() : null;
                    if (profile != null && profile.AdmissionSource == admissionSourceType.ToString())
                    {
                        result++;
                    }
                });
            }
            return result;
        }

        private string GetEpisodeTiming(IDictionary<string, Question> assessmentData)
        {
            var timing = string.Empty;

            if (assessmentData != null)
            {
                var episodeTiming = assessmentData.AnswerOrEmptyString("M0110EpisodeTiming");
                if (episodeTiming.IsEqual("01"))
                {
                    timing = "Early";
                }
                if (episodeTiming.IsEqual("02"))
                {
                    timing = "Late";
                }
            }

            return timing;
        }

        private string GetNrsSeverityLevel(Assessment assessment)
        {
            var severityLevel = string.Empty;

            if (assessment != null && assessment.HippsCode.IsNotNullOrEmpty())
            {
                if (assessment.HippsCode.ToLower().EndsWith("s"))
                {
                    severityLevel = "1";
                }
                if (assessment.HippsCode.ToLower().EndsWith("t"))
                {
                    severityLevel = "2";
                }
                if (assessment.HippsCode.ToLower().EndsWith("u"))
                {
                    severityLevel = "3";
                }
                if (assessment.HippsCode.ToLower().EndsWith("v"))
                {
                    severityLevel = "4";
                }
                if (assessment.HippsCode.ToLower().EndsWith("w"))
                {
                    severityLevel = "5";
                }
                if (assessment.HippsCode.ToLower().EndsWith("x"))
                {
                    severityLevel = "6";
                }
            }

            return severityLevel;
        }

        private string GetNrsSeverityLevel(string hippsCode)
        {
            var severityLevel = string.Empty;

            if (hippsCode.IsNotNullOrEmpty())
            {
                hippsCode = hippsCode.ToLower();
                if (hippsCode.EndsWith("s"))
                {
                    severityLevel = "1";
                }
                if (hippsCode.EndsWith("t"))
                {
                    severityLevel = "2";
                }
                if (hippsCode.EndsWith("u"))
                {
                    severityLevel = "3";
                }
                if (hippsCode.EndsWith("v"))
                {
                    severityLevel = "4";
                }
                if (hippsCode.EndsWith("w"))
                {
                    severityLevel = "5";
                }
                if (hippsCode.EndsWith("x"))
                {
                    severityLevel = "6";
                }
            }

            return severityLevel;
        }

        private string GetFirstBillableDate(List<ScheduleEvent> scheduleEvents)
        {
            var date = string.Empty;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var schedule = scheduleEvents.Where(s => s.IsBillable).OrderBy(s => s.EventDate).FirstOrDefault();//&& (s.Status == 425 || s.Status == 220 || s.Status == 225)
                if (schedule != null && schedule.EventDate > DateTime.MinValue)
                {
                    return schedule.EventDate.ToString("MM/dd/yyyy");
                }
            }
            return date;
        }

        private Dictionary<DateTime, string> GetMonthsBetweenAndAfter(DateTime startDate, DateTime endDate, int extraMonth)
        {
            var list = new Dictionary<DateTime, string>();
            if (startDate.Date < endDate.Date)
            {
                var lastDate = endDate.AddMonths(extraMonth);
                var countMonth = endDate.Month - startDate.Month + (endDate.Year - startDate.Year) * 12;
                for (int i = 0; i <= countMonth + 2; i++)
                {
                    list.Add(startDate.AddMonths(i), startDate.AddMonths(i).ToString("MMM"));
                }
            }
            return list;
        }

        private string GetVisitsInMonth(List<ScheduleEvent> scheduleEvents, DateTime date, bool IsCompletedOnly)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s => s.EventDate.Year == date.Year && s.EventDate.Month == date.Month && s.IsBillable && (IsCompletedOnly ? s.IsCompleted() : true)).ToList().Count.ToString();
            }
            return result;
        }

        private string GetVisitsByDiscipline(List<ScheduleEvent> scheduleEvents, string discipline, bool IsCompletedOnly)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s => s.Discipline.IsEqual(discipline) && s.IsBillable && (IsCompletedOnly ? s.IsCompleted() : true)).ToList().Count.ToString();
            }
            return result;
        }

        private string GetTotalEpisodeVisits(List<ScheduleEvent> scheduleEvents)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s => s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        [Obsolete]
        private ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode)
        {
            var prospectivePayment = new ProspectivePayment();

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                var cbsaCode = Reports.GetCbsaCode(zipCode);
                var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = Reports.GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);

                    prospectivePayment.Hhrg = hhrg.HHRG;
                    prospectivePayment.HippsCode = hippsCode;
                    prospectivePayment.CbsaCode = cbsaCode.CBSA;
                    prospectivePayment.WageIndex = wageIndex.ToString();
                    prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                    prospectivePayment.LaborAmount = laborAmount;
                    prospectivePayment.NonLaborAmount = nonLaborAmount;
                    prospectivePayment.NonRoutineSuppliesAmount = supplyAmount;
                    prospectivePayment.TotalAmountWithoutSupplies = totalAmountWithoutSupplies;
                    prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;
                }
            }
            return prospectivePayment;
        }

        private ProspectivePayment GetProspectivePayment(PPSStandard ppsStandard, HippsAndHhrg hhrg, CbsaCode cbsaCode)
        {
            var prospectivePayment = new ProspectivePayment();
            if (cbsaCode != null && hhrg != null && ppsStandard != null)
            {
                var hippsCode = hhrg.HIPPS;
                var time = ppsStandard.Time;
                Double rate = 0;
                var isRural = false;
                var wageIndex = this.WageIndex(cbsaCode, time);

                if (cbsaCode.CBSA.IsNotNullOrEmpty())
                {
                    if (cbsaCode.CBSA.StartsWith("9"))
                    {
                        isRural = true;
                        rate = ppsStandard.RuralRate;
                    }
                    else
                    {
                        isRural = false;
                        rate = ppsStandard.UrbanRate;
                    }
                }

                var rateTimesWeight = rate * hhrg.HHRGWeight;
                var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);

                prospectivePayment.Hhrg = hhrg.HHRG;
                prospectivePayment.HippsCode = hippsCode;
                prospectivePayment.CbsaCode = cbsaCode.CBSA;
                prospectivePayment.WageIndex = wageIndex.ToString();
                prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                prospectivePayment.LaborAmount = laborAmount;
                prospectivePayment.NonLaborAmount = nonLaborAmount;
                prospectivePayment.NonRoutineSuppliesAmount = supplyAmount;
                prospectivePayment.TotalAmountWithoutSupplies = totalAmountWithoutSupplies;
                prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;
            }
            return prospectivePayment;
        }


        private double WageIndex(CbsaCode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        private string GetChargesByMonth(List<ScheduleEvent> scheduleEvents, ProspectivePayment prospectivePayment, DateTime date)
        {
            var result = "$0.00";
            if (scheduleEvents != null && scheduleEvents.Count > 0 && prospectivePayment != null && date.IsValid())
            {
                var monthVisits = GetVisitsInMonth(scheduleEvents, date, false);
                var billableVisits = GetTotalEpisodeVisits(scheduleEvents);
                if (monthVisits.ToInteger() > 0 && billableVisits.ToInteger() > 0)
                {
                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / billableVisits.ToInteger(), 2);
                    result = string.Format("${0:#0.00}", monthVisits.ToInteger() * unitAmount);
                }
            }

            return result;
        }

        public string GetBalanceDue(double totalAmount, Claim rap, Claim final)
        {
            var result = string.Format("${0:#0.00}", totalAmount);

            if (rap != null && totalAmount > 0 && rap.PaymentAmount > 0)
            {
                result = string.Format("${0:#0.00}", totalAmount - rap.PaymentAmount);

                if (final != null && final.PaymentAmount > 0)
                {
                    result = string.Format("${0:#0.00}", totalAmount - (rap.PaymentAmount + final.PaymentAmount));
                }
            }

            return result;
        }

        #endregion
    }
}