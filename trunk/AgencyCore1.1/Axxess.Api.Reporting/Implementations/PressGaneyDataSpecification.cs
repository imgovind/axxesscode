﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    internal class PressGaneyDataSpecification : Specification, IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal PressGaneyDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<List< string>> GetItems(List<int> paymentSources)
        {
            var result = new List<List<string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);
                var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLeanForCAHPS(agency.Id, agency.LocationId, sampleMonthFirst, sampleMonthLast,sampleLastMonthFirst, false, true, paymentSources != null && paymentSources.Count > 0 ? paymentSources.ToArray() : new int[] { });
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var assessmentIds = episodeSchedules.Where(w => w.IsAssessmentAndNonOasis()).Select(s => s.Id).ToList();
                    var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);
                    var gropuByPatientData = episodeSchedules.GroupBy(l => l.PatientId).ToDictionary(g => g.Key, g => g);
                    if (gropuByPatientData != null && gropuByPatientData.Count > 0)
                    {
                        var startOfCare = new int[] { 13, 61, 89, 112 };
                        var last5DayAssessment = new int[] { 8, 66, 73, 90, 9, 62, 69 };
                        var totalServed = 0;
                        gropuByPatientData.ForEach((pk, pvalue) =>
                        {
                            if (!pk.IsEmpty() && pvalue != null && pvalue.Count() > 0)
                            {
                                var pvalueList = pvalue.ToList();
                                if (pvalueList != null && pvalueList.Count > 0)
                                {
                                    var episodesInTheDateRange = pvalueList.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(sampleMonthFirst, sampleMonthLast) || s.EndDate.IsBetween(sampleMonthFirst, sampleMonthLast) || sampleMonthFirst.IsBetween(s.StartDate, s.EndDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                                    if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                                    {
                                        var firstEpisode = episodesInTheDateRange.OrderBy(v => v.Key.StartDate).FirstOrDefault();
                                        if (!firstEpisode.Equals(default(KeyValuePair<ScheduleEvent, List<ScheduleEvent>>)))
                                        {
                                            var assessmentTask = GetAssessmentTask(firstEpisode, pvalueList);
                                            if (assessmentTask != null)
                                            {
                                                totalServed++;
                                                var scheduleEventsInTheRange = pvalueList.Where(s => !s.Id.IsEmpty() && s.VisitDate.IsBetween(sampleMonthFirst, sampleMonthLast)).ToList() ?? new List<ScheduleEvent>();
                                                //var scheduleEvents = Reports.AllSkilledVisits(scheduleEventsInTheRange, sampleMonthFirst, sampleMonthLast);
                                                var skilledVisitCount = Reports.SkilledVisitCount(scheduleEventsInTheRange, sampleMonthFirst, sampleMonthLast);
                                                var lookbackVisitCount = Reports.SkilledVisitCount(pvalueList, sampleLastMonthFirst, sampleMonthLast);

                                                var assessmentData = GetAssessmentQuestions(assessmentTask, assessmentDatas);
                                                var collection = new List<string>();

                                                collection.Add(agency.CahpsSurveyDesignator);
                                                collection.Add(agency.CahpsVendorClientId);
                                                collection.Add(assessmentData.AnswerOrDefault("M0040FirstName", string.Empty));
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0040MI"));
                                                collection.Add(assessmentData.AnswerOrDefault("M0040LastName", string.Empty));
                                                collection.Add(firstEpisode.Key.AddressLine1.IsNotNullOrEmpty() ? firstEpisode.Key.AddressLine1 : string.Empty);
                                                collection.Add(firstEpisode.Key.AddressLine2.IsNotNullOrEmpty() ? firstEpisode.Key.AddressLine2 : string.Empty);
                                                collection.Add(firstEpisode.Key.AddressCity.IsNotNullOrEmpty() ? firstEpisode.Key.AddressCity : string.Empty);
                                                collection.Add(assessmentData.AnswerOrDefault("M0050PatientState", string.Empty));
                                                collection.Add(assessmentData.AnswerOrDefault("M0060PatientZipCode", string.Empty));
                                                collection.Add(firstEpisode.Key.PhoneHome.IsNotNullOrEmpty() ? firstEpisode.Key.PhoneHome : string.Empty);

                                                collection.Add(string.Empty);

                                                collection.Add(assessmentData.AnswerOrEmptyString("M0030SocDate").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentData.AnswerOrEmptyString("M0030SocDate").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);
                                                collection.Add(assessmentData.AnswerOrDefault("M0100AssessmentType", string.Empty).Replace(",on", ""));
                                                collection.Add(firstEpisode.Key.Status == 2 ? firstEpisode.Key.DischargeDate.ToString("MM/dd/yyyy") : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);

                                                collection.Add(GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));
                                                collection.Add(assessmentData.AnswerOrDefault("M0069Gender", string.Empty));
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0020PatientIdNumber"));
                                                collection.Add(assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "M");
                                                collection.Add(CheckForEndStageRenalDisiease(assessmentData));

                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");

                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "2");

                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                                                collection.Add(CheckForDualElligibility(assessmentData));

                                                collection.Add(assessmentData.AnswerOrDefault("M1020ICD9M", string.Empty).Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M1", string.Empty).Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M2", string.Empty).Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M3", string.Empty).Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M4", string.Empty).Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M5", string.Empty).Replace(".", ""));

                                                collection.Add("M");

                                                collection.Add(string.Format("{0}", scheduleEventsInTheRange != null && scheduleEventsInTheRange.Count > 0 && scheduleEventsInTheRange.Where(s => s.Discipline == "PT").ToList().Count > 0 ? 1 : 2));
                                                collection.Add(string.Format("{0}", scheduleEventsInTheRange != null && scheduleEventsInTheRange.Count > 0 && scheduleEventsInTheRange.Where(s => s.Discipline == "HHA").ToList().Count > 0 ? 1 : 2));
                                                collection.Add(string.Format("{0}", scheduleEventsInTheRange != null && scheduleEventsInTheRange.Count > 0 && scheduleEventsInTheRange.Where(s => s.Discipline == "MSW").ToList().Count > 0 ? 1 : 2));
                                                collection.Add(string.Format("{0}", scheduleEventsInTheRange != null && scheduleEventsInTheRange.Count > 0 && scheduleEventsInTheRange.Where(s => s.Discipline == "OT").ToList().Count > 0 ? 1 : 2));
                                                collection.Add(string.Format("{0}", 2));
                                                collection.Add(string.Format("{0}", scheduleEventsInTheRange != null && scheduleEventsInTheRange.Count > 0 && scheduleEventsInTheRange.Where(s => s.Discipline == "ST").ToList().Count > 0 ? 1 : 2));

                                                collection.Add(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : string.Empty);
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : string.Empty);

                                                collection.Add(string.Format("{0}", firstEpisode.Value != null && firstEpisode.Value.Count > 0 ? firstEpisode.Value.Where(s => s.IsSkilledCare()).ToList().Count.ToString() : ""));
                                                collection.Add(lookbackVisitCount.ToString());
                                                collection.Add(sampleMonthFirst.ToString("MM"));
                                                collection.Add(sampleYear.ToString());

                                                collection.Add(agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty);
                                                collection.Add(string.Empty);
                                                collection.Add(totalServed.ToString());
                                                collection.Add("$");

                                                result.Add(collection);
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        if (result.Count > 0)
                        {
                            var stringTotal = totalServed.ToString();
                            result.ForEach(r => r[69] = stringTotal);
                        }
                        var listTitle = new List<string>();
                        listTitle.Add("Survey_Designator");
                        listTitle.Add("Client_Number");
                        listTitle.Add("Pat_First_Name");
                        listTitle.Add("Pat_MI");
                        listTitle.Add("Pat_Last_Name");
                        listTitle.Add("Pat_Mailing_Address_1");
                        listTitle.Add("Pat_Mailing_Address_2");
                        listTitle.Add("Pat_Address_City");
                        listTitle.Add("Pat_Address_State");
                        listTitle.Add("Pat_Address_Zip");
                        listTitle.Add("Pat_Phone");

                        listTitle.Add("HH_Visit_Type");

                        listTitle.Add("SOC");
                        listTitle.Add("RFA");
                        listTitle.Add("Discharge_Date");
                        listTitle.Add("DOB");

                        listTitle.Add("Language");
                        listTitle.Add("Gender");
                        listTitle.Add("Pat_Med_Rec");
                        listTitle.Add("Surgical_DC");
                        listTitle.Add("ESRD");

                        listTitle.Add("Payer_None");
                        listTitle.Add("Payer_Medicare_FFS");
                        listTitle.Add("Payer_Medicare_HMO");
                        listTitle.Add("Payer_Medicaid_FFS");
                        listTitle.Add("Payer_Medicaid_HMO");
                        listTitle.Add("Payer_Workers_Comp");
                        listTitle.Add("Payer_Title_Programs");
                        listTitle.Add("Payer_Other_Government");
                        listTitle.Add("Payer_Private_Ins");
                        listTitle.Add("Payer_Private_HMO");
                        listTitle.Add("Payer_SelfPay");
                        listTitle.Add("Payer_Other");
                        listTitle.Add("Payer_Unknown");

                        listTitle.Add("HMO_Indicator");

                        listTitle.Add("Adm_NF");
                        listTitle.Add("Adm_SNF");
                        listTitle.Add("Adm_IPP_S");
                        listTitle.Add("Adm_LTCH");
                        listTitle.Add("Adm_IRF");
                        listTitle.Add("Adm_Psych");
                        listTitle.Add("Adm_Other");
                        listTitle.Add("Adm_None");

                        listTitle.Add("Dual_Eligible");

                        listTitle.Add("Primary_Diagnosis");
                        listTitle.Add("Other_Diagnosis_1");
                        listTitle.Add("Other_Diagnosis_2");
                        listTitle.Add("Other_Diagnosis_3");
                        listTitle.Add("Other_Diagnosis_4");
                        listTitle.Add("Other_Diagnosis_5");

                        listTitle.Add("Referral_Source");

                        listTitle.Add("PT");
                        listTitle.Add("HHA");
                        listTitle.Add("SS");
                        listTitle.Add("OT");
                        listTitle.Add("Comp_Homemaker");
                        listTitle.Add("ST");

                        listTitle.Add("ADL_Dress_Upper");
                        listTitle.Add("ADL_Dress_Lower");
                        listTitle.Add("ADL_Bathing");
                        listTitle.Add("ADL_Toileting");
                        listTitle.Add("ADL_Transferring");
                        listTitle.Add("ADL_Feed");

                        listTitle.Add("Num_Skilled_Visits");
                        listTitle.Add("Lookback_Period_Visits");
                        listTitle.Add("Sample_Month");
                        listTitle.Add("Sample_Year");

                        listTitle.Add("Branch_ID");
                        listTitle.Add("Team");
                        listTitle.Add("Number_Pat_Served");
                        listTitle.Add("EOR");
                        result.Insert(0, listTitle);
                    }
                }

                #region to be deleted

                //List<EpisodeData> servedEpisodes = null;
                //if (paymentSources != null && paymentSources.Count > 0)
                //{
                //    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast, paymentSources);
                //}
                //else
                //{
                //    servedEpisodes = Reports.GetEpisodesBetween(agency.Id,agency.LocationId, sampleMonthFirst, sampleMonthLast);
                //}
                //if (servedEpisodes != null && servedEpisodes.Count > 0)
                //{
                //    Windows.EventLog.WriteEntry(string.Format("Patient Episodes for Agency: {0}", servedEpisodes.Count), System.Diagnostics.EventLogEntryType.Information);
                //    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                //    if (patientIds != null && patientIds.Count > 0)
                //    {
                //        var totalServed = patientIds.Count;
                //        Windows.EventLog.WriteEntry(string.Format("Total Patients Served: {0}", totalServed), System.Diagnostics.EventLogEntryType.Information);
                //        patientIds.ForEach(patientId =>
                //        {
                //            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                //            if (episodes != null && episodes.Count > 0)
                //            {
                //                var scheduleEvents = Reports.AllSkilledVisits(episodes, sampleMonthFirst, sampleMonthLast);
                //                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                //                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);
                                
                //                var patientData = episodes.FirstOrDefault();
                //                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                //                if (assessment != null)
                //                {
                //                    var assessmentData = assessment.ToDictionary();
                //                    var collection = new Dictionary<string, string>();

                //                    collection.Add("Survey_Designator", agency.CahpsSurveyDesignator);
                //                    collection.Add("Client_Number", agency.CahpsVendorClientId);
                //                    collection.Add("Pat_First_Name", assessmentData.AnswerOrDefault("M0040FirstName", string.Empty));
                //                    collection.Add("Pat_MI", assessmentData.AnswerOrEmptyString("M0040MI"));
                //                    collection.Add("Pat_Last_Name", assessmentData.AnswerOrDefault("M0040LastName", string.Empty));
                //                    collection.Add("Pat_Mailing_Address_1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : string.Empty);
                //                    collection.Add("Pat_Mailing_Address_2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : string.Empty);
                //                    collection.Add("Pat_Address_City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : string.Empty);
                //                    collection.Add("Pat_Address_State", assessmentData.AnswerOrDefault("M0050PatientState", string.Empty));
                //                    collection.Add("Pat_Address_Zip", assessmentData.AnswerOrDefault("M0060PatientZipCode", string.Empty));
                //                    collection.Add("Pat_Phone", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : string.Empty);

                //                    collection.Add("HH_Visit_Type", string.Empty);

                //                    collection.Add("SOC", assessmentData.AnswerOrEmptyString("M0030SocDate").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentData.AnswerOrEmptyString("M0030SocDate").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);
                //                    collection.Add("RFA", assessmentData.AnswerOrDefault("M0100AssessmentType", string.Empty).Replace(",on", ""));
                //                    collection.Add("Discharge_Date", patientData.Status == 2 ? patientData.DischargeDate.ToString("MM/dd/yyyy") : string.Empty);
                //                    collection.Add("DOB", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);

                //                    collection.Add("Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));
                //                    collection.Add("Gender", assessmentData.AnswerOrDefault("M0069Gender", string.Empty));
                //                    collection.Add("Pat_Med_Rec", assessmentData.AnswerOrEmptyString("M0020PatientIdNumber"));
                //                    collection.Add("Surgical_DC", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "M");
                //                    collection.Add("ESRD", CheckForEndStageRenalDisiease(assessmentData));

                //                    collection.Add("Payer_None", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Medicare_FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Medicare_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Medicaid_FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Medicaid_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Workers_Comp", assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Title_Programs", assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Other_Government", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Private_Ins", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Private_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_SelfPay", assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Other", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer_Unknown", assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");

                //                    collection.Add("HMO_Indicator", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "2");

                //                    collection.Add("Adm_NF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_SNF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_IPP_S", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_LTCH", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_IRF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_Psych", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_Other", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Adm_None", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                //                    collection.Add("Dual_Eligible", CheckForDualElligibility(assessmentData));

                //                    collection.Add("Primary_Diagnosis", assessmentData.AnswerOrDefault("M1020ICD9M", string.Empty).Replace(".", ""));
                //                    collection.Add("Other_Diagnosis_1", assessmentData.AnswerOrDefault("M1022ICD9M1", string.Empty).Replace(".", ""));
                //                    collection.Add("Other_Diagnosis_2", assessmentData.AnswerOrDefault("M1022ICD9M2", string.Empty).Replace(".", ""));
                //                    collection.Add("Other_Diagnosis_3", assessmentData.AnswerOrDefault("M1022ICD9M3", string.Empty).Replace(".", ""));
                //                    collection.Add("Other_Diagnosis_4", assessmentData.AnswerOrDefault("M1022ICD9M4", string.Empty).Replace(".", ""));
                //                    collection.Add("Other_Diagnosis_5", assessmentData.AnswerOrDefault("M1022ICD9M5", string.Empty).Replace(".", ""));

                //                    collection.Add("Referral_Source", "M");

                //                    collection.Add("PT", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == "PT").ToList().Count > 0 ? 1 : 2));
                //                    collection.Add("HHA", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == "HHA").ToList().Count > 0 ? 1 : 2));
                //                    collection.Add("SS", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == "MSW").ToList().Count > 0 ? 1 : 2));
                //                    collection.Add("OT", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == "OT").ToList().Count > 0 ? 1 : 2));
                //                    collection.Add("Comp_Homemaker", string.Format("{0}", 2));
                //                    collection.Add("ST", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == "ST").ToList().Count > 0 ? 1 : 2));

                //                    collection.Add("ADL_Dress_Upper", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : string.Empty);
                //                    collection.Add("ADL_Dress_Lower", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : string.Empty);
                //                    collection.Add("ADL_Bathing", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : string.Empty);
                //                    collection.Add("ADL_Toileting", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : string.Empty);
                //                    collection.Add("ADL_Transferring", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : string.Empty);
                //                    collection.Add("ADL_Feed", assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : string.Empty);

                //                    collection.Add("Num_Skilled_Visits", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 ? scheduleEvents.Where(s => s.IsSkilledCare()).ToList().Count.ToString() : ""));
                //                    collection.Add("Lookback_Period_Visits", lookbackVisitCount);
                //                    collection.Add("Sample_Month", sampleMonthFirst.ToString("MM"));
                //                    collection.Add("Sample_Year", sampleYear.ToString());

                //                    collection.Add("Branch_ID", agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty);
                //                    collection.Add("Team", string.Empty);
                //                    collection.Add("Number_Pat_Served", totalServed.ToString());
                //                    collection.Add("EOR", "$");

                //                    result.Add(collection);
                //                }
                //            }
                //        });
                //    }
                //}
                //else
                //{
                //    Windows.EventLog.WriteEntry(string.Format("No Patients found for AgencyId: {0}", agency.Name), System.Diagnostics.EventLogEntryType.Warning);
                //}

                #endregion
            }
            else
            {
                Windows.EventLog.WriteEntry("Agency is null.", System.Diagnostics.EventLogEntryType.Error);
            }

            return result;
        }
        
        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "1";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "2";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        private string CheckForDualElligibility(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if ((data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1"))
                   && (data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1")))
                {
                    result = "1";
                }
            }

            return result;
        }

        #endregion
    }
}
