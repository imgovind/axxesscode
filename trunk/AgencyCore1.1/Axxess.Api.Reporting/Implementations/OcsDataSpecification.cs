﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    internal class OcsDataSpecification : Specification, IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal OcsDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<List<string>> GetItems(List<int> paymentSources)
        {
            var result = new List<List<string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);
                var episodeSchedules = Reports.GetEpisodeScheduleEventsWithPossibleAssessmentsLeanForCAHPS(agency.Id, agency.LocationId, sampleMonthFirst, sampleMonthLast, sampleLastMonthFirst, false, true, paymentSources != null && paymentSources.Count > 0 ? paymentSources.ToArray() : new int[] { });
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var assessmentIds = episodeSchedules.Where(w => w.IsAssessmentAndNonOasis()).Select(s => s.Id).ToList();
                    var assessmentDatas = Reports.GetManyAssessmentQuestions(assessmentIds);
                    var gropuByPatientData = episodeSchedules.GroupBy(l => l.PatientId).ToDictionary(g => g.Key, g => g);
                    if (gropuByPatientData != null && gropuByPatientData.Count > 0)
                    {
                        var startOfCare = new int[] { 13, 61, 89, 112 };
                        var last5DayAssessment = new int[] { 8, 66, 73, 90, 9, 62, 69 };
                        var totalServed = 0;
                        gropuByPatientData.ForEach((pk, pvalue) =>
                        {
                            if (!pk.IsEmpty() && pvalue != null && pvalue.Count() > 0)
                            {
                                var pvalueList = pvalue.ToList();
                                if (pvalueList != null && pvalueList.Count > 0)
                                {
                                    var episodesInTheDateRange = pvalueList.Where(s => s.StartDate.IsValid() && s.EndDate.IsValid() && (s.StartDate.IsBetween(sampleMonthFirst, sampleMonthLast) || s.EndDate.IsBetween(sampleMonthFirst, sampleMonthLast) || sampleMonthFirst.IsBetween(s.StartDate, s.EndDate))).GroupBy(g => g.EpisodeId).ToDictionary(g => g.FirstOrDefault() ?? new ScheduleEvent { EpisodeId = g.Key }, g => g.ToList());
                                    if (episodesInTheDateRange != null && episodesInTheDateRange.Count > 0)
                                    {
                                        var firstEpisode = episodesInTheDateRange.OrderBy(v => v.Key.StartDate).FirstOrDefault();
                                        if (!firstEpisode.Equals(default(KeyValuePair<ScheduleEvent, List<ScheduleEvent>>)))
                                        {
                                            var assessmentTask = GetAssessmentTask(firstEpisode, pvalueList);
                                            if (assessmentTask != null)
                                            {
                                                totalServed++;
                                                var skilledVisitCount = Reports.SkilledVisitCount(pvalueList, sampleMonthFirst, sampleMonthLast);
                                                var lookbackVisitCount = Reports.SkilledVisitCount(pvalueList, sampleLastMonthFirst, sampleMonthLast);

                                                var assessmentData = GetAssessmentQuestions(assessmentTask, assessmentDatas);
                                                var collection = new List<string>();

                                                collection.Add("M");
                                                collection.Add(agency.Name.IsNotNullOrEmpty() ? agency.Name : "M");
                                                collection.Add(agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "M");
                                                collection.Add(agency.NationalProviderNumber.IsNotNullOrEmpty() ? agency.NationalProviderNumber : "M");
                                                collection.Add(sampleMonthFirst.ToString("MM"));
                                                collection.Add(sampleYear.ToString());
                                                collection.Add(totalServed.ToString());
                                                collection.Add(totalServed.ToString());

                                                collection.Add(assessmentData.AnswerOrDefault("M0040FirstName", "M"));
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0040MI"));
                                                collection.Add(assessmentData.AnswerOrDefault("M0040LastName", "M"));
                                                collection.Add(assessmentData.AnswerOrDefault("M0069Gender", "M"));
                                                collection.Add( assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MMddyyyy") : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0030SocDate").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentData.AnswerOrEmptyString("M0030SocDate").ToDateTime().ToString("MMddyyyy") : "M");
                                                collection.Add(firstEpisode.Key.AddressLine1.IsNotNullOrEmpty() ? firstEpisode.Key.AddressLine1 : "M");
                                                collection.Add(firstEpisode.Key.AddressLine2.IsNotNullOrEmpty() ? firstEpisode.Key.AddressLine2 : "M");
                                                collection.Add(firstEpisode.Key.AddressCity.IsNotNullOrEmpty() ? firstEpisode.Key.AddressCity : "M");
                                                collection.Add(assessmentData.AnswerOrDefault("M0050PatientState", "M"));
                                                collection.Add(assessmentData.AnswerOrDefault("M0060PatientZipCode", "M"));
                                                collection.Add(firstEpisode.Key.PhoneHome.IsNotNullOrEmpty() ? firstEpisode.Key.PhoneHome : "M");
                                                collection.Add(assessmentData.AnswerOrDefault("M0020PatientIdNumber", "M"));

                                                collection.Add(skilledVisitCount.ToString());
                                                collection.Add(lookbackVisitCount.ToString());

                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");

                                                collection.Add(assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "2");

                                                collection.Add(CheckForDualElligibility(assessmentData));

                                                collection.Add(assessmentData.AnswerOrDefault("M1020ICD9M", "M").Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M1", "M").Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M2", "M").Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M3", "M").Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M4", "M").Replace(".", ""));
                                                collection.Add(assessmentData.AnswerOrDefault("M1022ICD9M5", "M").Replace(".", ""));

                                                collection.Add(assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "2");

                                                collection.Add(CheckForEndStageRenalDisiease(assessmentData));

                                                collection.Add(CheckForADLDeficits(assessmentData));
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : "M");
                                                collection.Add(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : "M");

                                                collection.Add(GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));

                                                result.Add(collection);
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        if (result.Count > 0)
                        {
                            var stringTotal = totalServed.ToString();
                            result.ForEach(r => r[6] = stringTotal);
                            result.ForEach(r => r[7] = stringTotal);
                        }
                        var listTitle = new List<string>();
                        listTitle.Add("Location Name");
                        listTitle.Add("Provider Name");
                        listTitle.Add("Provider Id");
                        listTitle.Add("NPI");
                        listTitle.Add("Sample Month");
                        listTitle.Add("Sample Year");
                        listTitle.Add("Number of Patients Served");
                        listTitle.Add("Number of Patients on files submitted to Vendor");

                        listTitle.Add("Patient First Name");
                        listTitle.Add("Patient Middle Initial");
                        listTitle.Add("Patient Last Name");
                        listTitle.Add("Gender");
                        listTitle.Add("Patient Date of Birth");
                        listTitle.Add("Start of Care Date");
                        listTitle.Add("Patient Mailing Address 1");
                        listTitle.Add("Patient Mailing Address 2");
                        listTitle.Add("Patient Address City");
                        listTitle.Add("Patient Address State");
                        listTitle.Add("Patient Address Zip Code");
                        listTitle.Add("Telephone Number");
                        listTitle.Add("Medical Record Number");

                        listTitle.Add("Number of skilled visits");
                        listTitle.Add("Lookback Period Visits");

                        listTitle.Add("Admission Source – NF");
                        listTitle.Add("Admission Source – SNF");
                        listTitle.Add("Admission Source – LTCH");
                        listTitle.Add("Admission Source – IRF");
                        listTitle.Add("Admission Source – Other");
                        listTitle.Add("Admission Source – NA");

                        listTitle.Add("Payer – None");
                        listTitle.Add("Payer – Medicare FFS");
                        listTitle.Add("Payer – Medicare HMO");
                        listTitle.Add("Payer – Medicaid FFS");
                        listTitle.Add("Payer – Medicaid HMO");
                        listTitle.Add("Payer – Workers’ Comp");
                        listTitle.Add("Payer – Title programs");
                        listTitle.Add("Payer – Other Government");
                        listTitle.Add("Payer – Private Ins.");
                        listTitle.Add("Payer – Private HMO");
                        listTitle.Add("Payer – Self-pay");
                        listTitle.Add("Payer – Other");
                        listTitle.Add("Payer – Unknown");

                        listTitle.Add("HMO Indicator");

                        listTitle.Add("DualEligible");

                        listTitle.Add("Primary Diagnosis ICD_A2");
                        listTitle.Add("Other diagnosis_B2");
                        listTitle.Add("Other diagnosis_C2");
                        listTitle.Add("Other diagnosis_D2");
                        listTitle.Add("Other diagnosis_E2");
                        listTitle.Add("Other diagnosis_F2");

                        listTitle.Add("Surgical Discharge");

                        listTitle.Add("ESRD");

                        listTitle.Add("ADL Deficits");
                        listTitle.Add("ADL_Dress Upper");
                        listTitle.Add("ADL_Dress Lower");
                        listTitle.Add("ADL_Bathing");
                        listTitle.Add("ADL_Toileting");
                        listTitle.Add("ADL_Transferring");
                        listTitle.Add("ADL_Feed");

                        listTitle.Add("Language");
                        result.Insert(0, listTitle);
                    }
                }

                #region to be deleted

                //List<EpisodeData> servedEpisodes = null;
                //if (paymentSources != null && paymentSources.Count > 0)
                //{
                //    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, agency.LocationId, sampleMonthFirst, sampleMonthLast, paymentSources);
                //}
                //else
                //{
                //    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, agency.LocationId, sampleMonthFirst, sampleMonthLast);
                //}
                //if (servedEpisodes != null && servedEpisodes.Count > 0)
                //{
                //    Windows.EventLog.WriteEntry(string.Format("Patient Episodes for Agency: {0}", servedEpisodes.Count), System.Diagnostics.EventLogEntryType.Information);
                //    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                //    if (patientIds != null && patientIds.Count > 0)
                //    {
                //        var totalServed = patientIds.Count;
                //        Windows.EventLog.WriteEntry(string.Format("Total Patients Served: {0}", totalServed), System.Diagnostics.EventLogEntryType.Information);
                //        patientIds.ForEach(patientId =>
                //        {
                //            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                //            if (episodes != null && episodes.Count > 0)
                //            {
                //                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                //                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);

                //                var patientData = episodes.FirstOrDefault();
                //                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                //                if (assessment != null)
                //                {
                //                    var assessmentData = assessment.ToDictionary();
                //                    var collection = new Dictionary<string, string>();

                //                    collection.Add("Location Name", "M");
                //                    collection.Add("Provider Name", agency.Name.IsNotNullOrEmpty() ? agency.Name : "M");
                //                    collection.Add("Provider Id", agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "M");
                //                    collection.Add("NPI", agency.NationalProviderNumber.IsNotNullOrEmpty() ? agency.NationalProviderNumber : "M");
                //                    collection.Add("Sample Month", sampleMonthFirst.ToString("MM"));
                //                    collection.Add("Sample Year", sampleYear.ToString());
                //                    collection.Add("Number of Patients Served", totalServed.ToString());
                //                    collection.Add("Number of Patients on files submitted to Vendor", totalServed.ToString());

                //                    collection.Add("Patient First Name", assessmentData.AnswerOrDefault("M0040FirstName", "M"));
                //                    collection.Add("Patient Middle Initial", assessmentData.AnswerOrEmptyString("M0040MI"));
                //                    collection.Add("Patient Last Name", assessmentData.AnswerOrDefault("M0040LastName", "M"));
                //                    collection.Add("Gender", assessmentData.AnswerOrDefault("M0069Gender", "M"));
                //                    collection.Add("Patient Date of Birth", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MMddyyyy") : "M");
                //                    collection.Add("Start of Care Date", assessmentData.AnswerOrEmptyString("M0030SocDate").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentData.AnswerOrEmptyString("M0030SocDate").ToDateTime().ToString("MMddyyyy") : "M");
                //                    collection.Add("Patient Mailing Address 1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : "M");
                //                    collection.Add("Patient Mailing Address 2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : "M");
                //                    collection.Add("Patient Address City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : "M");
                //                    collection.Add("Patient Address State", assessmentData.AnswerOrDefault("M0050PatientState", "M"));
                //                    collection.Add("Patient Address Zip Code", assessmentData.AnswerOrDefault("M0060PatientZipCode", "M"));
                //                    collection.Add("Telephone Number", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : "M");
                //                    collection.Add("Medical Record Number", assessmentData.AnswerOrDefault("M0020PatientIdNumber", "M"));

                //                    collection.Add("Number of skilled visits", skilledVisitCount);
                //                    collection.Add("Lookback Period Visits", lookbackVisitCount);

                //                    collection.Add("Admission Source – NF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Admission Source – SNF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Admission Source – LTCH", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Admission Source – IRF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Admission Source – Other", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Admission Source – NA", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                //                    collection.Add("Payer – None", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Medicare FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Medicare HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Medicaid FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Medicaid HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Workers’ Comp", assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Title programs", assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Other Government", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Private Ins.", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Private HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Self-pay", assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Other", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                //                    collection.Add("Payer – Unknown", assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");

                //                    collection.Add("HMO Indicator", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "2");

                //                    collection.Add("DualEligible", CheckForDualElligibility(assessmentData));

                //                    collection.Add("Primary Diagnosis ICD_A2", assessmentData.AnswerOrDefault("M1020ICD9M", "M").Replace(".", ""));
                //                    collection.Add("Other diagnosis_B2", assessmentData.AnswerOrDefault("M1022ICD9M1", "M").Replace(".", ""));
                //                    collection.Add("Other diagnosis_C2", assessmentData.AnswerOrDefault("M1022ICD9M2", "M").Replace(".", ""));
                //                    collection.Add("Other diagnosis_D2", assessmentData.AnswerOrDefault("M1022ICD9M3", "M").Replace(".", ""));
                //                    collection.Add("Other diagnosis_E2", assessmentData.AnswerOrDefault("M1022ICD9M4", "M").Replace(".", ""));
                //                    collection.Add("Other diagnosis_F2", assessmentData.AnswerOrDefault("M1022ICD9M5", "M").Replace(".", ""));

                //                    collection.Add("Surgical Discharge", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "2");

                //                    collection.Add("ESRD", CheckForEndStageRenalDisiease(assessmentData));

                //                    collection.Add("ADL Deficits", CheckForADLDeficits(assessmentData));
                //                    collection.Add("ADL_Dress Upper", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : "M");
                //                    collection.Add("ADL_Dress Lower", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : "M");
                //                    collection.Add("ADL_Bathing", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : "M");
                //                    collection.Add("ADL_Toileting", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : "M");
                //                    collection.Add("ADL_Transferring", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : "M");
                //                    collection.Add("ADL_Feed", assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : "M");

                //                    collection.Add("Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));

                //                    result.Add(collection);
                //                }
                //            }
                //        });
                //    }
                //}
                //else
                //{
                //    Windows.EventLog.WriteEntry(string.Format("No Patients found for AgencyId: {0}", agency.Name), System.Diagnostics.EventLogEntryType.Warning);
                //}
                #endregion
            }
            else
            {
                Windows.EventLog.WriteEntry("Agency is null.", System.Diagnostics.EventLogEntryType.Error);
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "1";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "2";
                }
                else if (language.ToLowerInvariant().Contains("chinese"))
                {
                    result = "3";
                }
                else if (language.ToLowerInvariant().Contains("russian"))
                {
                    result = "4";
                }
                else if (language.ToLowerInvariant().Contains("vietnamese"))
                {
                    result = "5";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        private string CheckForDualElligibility(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if ((data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1"))
                   && (data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1")))
                {
                    result = "1";
                }
            }

            return result;
        }

        #endregion
    }
}
