﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;

    public class Specification
    {
        protected ScheduleEvent GetAssessmentTask(KeyValuePair<ScheduleEvent, List<ScheduleEvent>> firstEpisode, List<ScheduleEvent> pValueList)
        {
            var startOfCare = DisciplineTaskFactory.SOCDisciplineTasks(true);
            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true);
            var scheduleEvents = firstEpisode.Value ?? new List<ScheduleEvent>();
            var assessmentTask = scheduleEvents.Where(s => !s.Id.IsEmpty() && startOfCare.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate).FirstOrDefault();
            if (assessmentTask == null)
            {
                assessmentTask = pValueList.Where(p => !p.Id.IsEmpty() && p.PatientId == firstEpisode.Key.PatientId && p.EpisodeId != firstEpisode.Key.EpisodeId && last5DayAssessment.Contains(p.DisciplineTask) && p.EndDate.Date == firstEpisode.Key.StartDate.AddDays(-1).Date && p.VisitDate.IsBetween(p.EndDate.Date.AddDays(-5), p.EndDate.Date)).OrderByDescending(s => s.VisitDate).FirstOrDefault();
            }
            return assessmentTask;
        }

        protected IDictionary<string, Question> GetAssessmentQuestions(ScheduleEvent assessmentTask, List<AssessmentQuestionData> assessmentDatas)
        {
            IDictionary<string, Question> dictionaryResult = null;
            if (assessmentTask != null)
            {
                var assessmentData = assessmentDatas.FirstOrDefault(t => t.Id == assessmentTask.Id);
                if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                {
                    dictionaryResult = assessmentData.ToDictionary();
                }
            }
            return dictionaryResult;
        }
    }
}
