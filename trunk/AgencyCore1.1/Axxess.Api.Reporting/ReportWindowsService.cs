﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    partial class ReportWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null; 

        public ReportWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "NewReportService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(ReportService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("Report Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("Report Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
