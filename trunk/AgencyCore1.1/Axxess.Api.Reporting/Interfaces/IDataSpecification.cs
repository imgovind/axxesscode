﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    interface IDataSpecification
    {
        List<List<string>> GetItems(List<int> paymentSources);
    }
}
