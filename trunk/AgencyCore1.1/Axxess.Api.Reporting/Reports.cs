﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.LookUp.Repositories;

    internal static class Reports
    {
        private static readonly IMongoRepository MongoRepository;
        private static readonly ILookupRepository LookupRepository;

        static Reports()
        {
            MongoRepository = new MongoDataProvider().MongoRepository;
            LookupRepository = new LookUpDataProvider().LookUpRepository;
        }

        #region Static Helper Methods

        internal static List<AssessmentQuestionData> GetManyAssessmentQuestions(List<Guid> ids)
        {
            return MongoRepository.GetManyAssessmentQuestions(ids);
        }

        internal static AgencyData GetAgency(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator`  FROM `agencies` WHERE `Id` = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static AgencyData GetAgencyLocation(Guid agencyId, Guid agencyLocationId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `AgencyId`, `Name`, `Payor`, `NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator` , `IsLocationStandAlone`, `AddressZipCode` FROM `agencylocations` WHERE `Id` = @agencylocationid  AND `AgencyId` = @agencyid ;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("AgencyId"),
                    LocationId = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    Payor = reader.GetInt("Payor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    AddressZipCode = reader.GetString("AddressZipCode")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static string NumberEligible(Guid agencyId, Guid agencyLocationId, DateTime sampleMonthLast)
        {
            int count = 0;
            var script = @"SELECT Count(`Id`) FROM `patientprofiles` WHERE `AgencyId` = @agencyid AND `AgencyLocationId` = @agencylocationid  AND `IsDeprecated` = 0 AND `Status` in (1, 2) " +
                "AND `PrimaryInsurance` in (1,2,3,4) AND (`Created` <= @month || `StartofCareDate` <= @month)";

            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("month", sampleMonthLast)
                .AsScalar();
            }

            return count.ToString();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsWithPossibleAssessmentsLean(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, bool isTimeInAndOutIncluded, bool isEpisodeDetailIncluded)
        {
            var last5DayAssessment = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { 8, 66, 73, 90, 9, 62, 69 };//13, 61, 89, 112,
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                st.Id as Id ,
                pr.Id as PatientId ,
                patientepisodes.Id as EpisodeId ,
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                {0} as TimeIn ,
                {1} as TimeOut , 
                st.Status as Status ,
                st.DisciplineTask as DisciplineTask , 
                st.Discipline as Discipline ,
                st.IsBillable as IsBillable , 
                st.IsMissedVisit as IsMissedVisit , 
                patientepisodes.AdmissionId as AdmissionId,
                patientepisodes.StartDate as StartDate ,
                patientepisodes.EndDate as EndDate,
                {2} as EpisodeNotes 
                    FROM
                        scheduletasks st 
                            INNER JOIN patientprofiles pr ON pr.Id = st.PatientId
                            INNER JOIN patientepisodes ON patientepisodes.PatientId = pr.Id 
                               WHERE
                                    pr.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND
                                    (( Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(@startdate) BETWEEN Date(patientepisodes.StartDate) AND Date(patientepisodes.EndDate) || Date(@enddate) BETWEEN Date(patientepisodes.StartDate) AND Date(patientepisodes.EndDate) || ( Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) AND Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) )  OR ( DATE(patientepisodes.EndDate) between DATE(@startdateback) and DATE(@enddateback) AND DATE(st.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) AND st.DisciplineTask IN ( {3}) ) ) AND 
                                    DATE(st.EventDate) BETWEEN DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0 "
                 , isTimeInAndOutIncluded ? "st.TimeIn" : "''"
                 , isTimeInAndOutIncluded ? "st.TimeOut" : "''"
                 , isEpisodeDetailIncluded ? "patientepisodes.Details " : "''"
                 , last5DayAssessment.ToCommaSeperatedList());


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddateback", startDate.AddDays(-1))
                .AddDateTime("startdateback", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Status = reader.GetInt("Status"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<Patient> GetPatientDateOfBirths(Guid agencyId, List<Guid> patientIds)
        {
            var list = new List<Patient>();
            var script = string.Format(@"SELECT Id, DOB FROM patients WHERE AgencyId = @agencyId AND Id in ({0})", patientIds.ToCommaSeperatedList());

            using (var cmd = new FluentCommand<Patient>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Patient
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("Id"),
                    DOB = reader.GetDateTime("DOB", DateTime.MinValue)
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static int SkilledVisitCount(List<ScheduleEvent> scheduleEvents, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
               var filteredScheduledEvents = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsSkilledCare() && (s.EventDate.Date >= startDate.Date && s.EventDate.Date <= endDate.Date)).ToList();
               if (filteredScheduledEvents != null && filteredScheduledEvents.Count > 0)
                {
                    list.AddRange(filteredScheduledEvents);
                }
            }
            return list.Count;
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsWithPossibleAssessmentsLeanForCAHPS(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, DateTime backStartDate, bool IsExactly, bool IsOver18Only, int[] paymentSources)
        {
            var oasisDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { 8, 66, 73, 90, 9, 62, 69 };
            var episodeFilterScript = string.Empty;
            var numberOfDays = startDate.Subtract(backStartDate).TotalDays;
            if (IsExactly)
            {
                episodeFilterScript = " ( Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate) ) ";
            }
            else
            {
                episodeFilterScript = "  ( Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(@startdate) BETWEEN Date(patientepisodes.StartDate) AND Date(patientepisodes.EndDate) || Date(@enddate) BETWEEN Date(patientepisodes.StartDate) AND Date(patientepisodes.EndDate) || ( Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) AND Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) ) ";
            }
            if (startDate > backStartDate)
            {
                episodeFilterScript += string.Format(" OR ( DATE(st.EventDate) between Date(@backstartdate) and Date(@startdate) ) ");
                if (numberOfDays < 65)
                {
                    episodeFilterScript += string.Format(" OR ( DATE(patientepisodes.EndDate) between DATE(@middlestartdate) and DATE(@backstartDate) AND DATE(st.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) AND st.DisciplineTask IN ( {0}) ) ", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
                }
            }

            episodeFilterScript = string.Format(" AND ({0})  " ,episodeFilterScript);
            var ageRestriction = string.Empty;
            if (IsOver18Only)
            {
                ageRestriction = " AND Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(p.DOB as DATETIME)))) >= 18  ";
            }
            var paymentSourceScript = string.Empty;
            if (paymentSources != null && paymentSources.Length > 0)
            {
                paymentSourceScript = string.Format(" AND pr.PaymentSource REGEXP '{0}'  ", paymentSources.Select(d => "[[:<:]]" + d + "[[:>:]]").ToArray().Join("|"));
            }
                   
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                p.Id as PatientId ,
                p.FirstName as FirstName,
                p.LastName as LastName, 
                pr.Status as PatientStatus,
                pr.PrimaryInsurance as PrimaryInsurance, 
                pr.DischargeDate as DischargeDate, 
                p.MiddleInitial as MiddleInitial,
                p.PatientIdNumber as PatientIdNumber,
                p.MedicareNumber as MedicareNumber,
                p.MedicaidNumber as MedicaidNumber,
                p.AddressLine1 as AddressLine1, 
                p.AddressLine2 as AddressLine2,
                p.AddressCity as AddressCity,
                p.AddressStateCode as AddressStateCode,
                p.AddressZipCode as AddressZipCode,
                p.PhoneHome as PhoneHome, 
                patientepisodes.Id as EpisodeId ,
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate,
                patientepisodes.AdmissionId as AdmissionId,
                st.Id as Id ,
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                st.Status as Status ,
                st.DisciplineTask as DisciplineTask , 
                st.Discipline as Discipline ,
                st.IsBillable as IsBillable ,
                assessments.HippsCode as HippsCode
                    FROM
                       agencymanagement.patients p
                            INNER JOIN patientprofiles pr ON pr.Id = p.Id
                            LEFT JOIN patientepisodes ON patientepisodes.PatientId = patients.Id 
                            LEFT JOIN scheduletasks st ON patientepisodes.Id = st.EpisodeId 
                            LEFT JOIN assessments ON assessments.EpisodeId = patientepisodes.Id AND assessments.Id = st.Id 
                                WHERE
                                    p.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 {0} {1} AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 {2} AND
                                    DATE(st.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0", paymentSourceScript, ageRestriction, episodeFilterScript);

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartdate",backStartDate)
                .AddDateTime("middlestartdate",numberOfDays<65?backStartDate.AddDays(65-numberOfDays):backStartDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    PatientStatus=reader.GetInt("PatientStatus"),
                    PrimaryInsurance=reader.GetInt("PrimaryInsurance"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    //EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    Id = reader.GetGuidIncludeEmpty("Id"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASIS(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true); //new int[] { 8, 66, 73, 90, 9, 62, 69 };
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                p.FirstName as FirstName,
                p.LastName as LastName, 
                p.MiddleInitial as MiddleInitial,
                p.PatientIdNumber as PatientIdNumber,
                p.MedicareNumber as MedicareNumber,
                p.MedicaidNumber as MedicaidNumber,
                p.AddressZipCode as AddressZipCode,
                p.Id as PatientId ,                
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate,
                patientepisodes.AdmissionId as AdmissionId,
                patientepisodes.Id as EpisodeId ,                
                st.Id as Id ,
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                st.Status as Status ,
                st.DisciplineTask as DisciplineTask , 
                st.Discipline as Discipline ,
                st.IsBillable as IsBillable ,
                assessments.HippsCode as HippsCode
                    FROM
                       agencymanagement.patients p
                            INNER JOIN patientprofiles pr ON pr.Id = p.Id
                            INNER JOIN patientepisodes ON patients.Id = patientepisodes.PatientId  
                            LEFT JOIN scheduletasks st ON patientepisodes.Id = st.EpisodeId  
                            LEFT JOIN assessments ON assessments.EpisodeId = patientepisodes.Id AND assessments.Id = st.Id 
                                WHERE
                                    patientepisodes.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND
                                    ((Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) OR ( DATE(patientepisodes.EndDate) between DATE(@backstartDate) and DATE(@startdate) AND DATE(st.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) AND st.DisciplineTask IN ( {0}) ) ) AND 
                                    ((st.Id IS NULL ) OR (DATE(st.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0))", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartDate", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    Id = reader.GetGuidIncludeEmpty("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    DisciplineTask = reader.GetInt("DisciplineTask",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable",false),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                p.Id as PatientId ,
                p.FirstName as FirstName,
                p.LastName as LastName, 
                p.MiddleInitial as MiddleInitial,
                p.PatientIdNumber as PatientIdNumber,
                p.MedicareNumber as MedicareNumber,
                p.MedicaidNumber as MedicaidNumber,
                p.AddressStateCode as AddressStateCode,
                patientepisodes.Id as EpisodeId ,
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate,
                patientepisodes.AdmissionId as AdmissionId,
                st.Id as Id ,
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                st.Status as Status ,
                st.DisciplineTask as DisciplineTask , 
                st.Discipline as Discipline ,
                st.IsBillable as IsBillable
                    FROM
                       agencymanagment.patients p
                            INNER JOIN patientprofiles pr ON pr.Id = p.Id
                            INNER JOIN patientepisodes ON patients.Id = patientepisodes.PatientId
                            LEFT JOIN  scheduletasks st ON patientepisodes.Id = st.EpisodeId 
                                WHERE
                                    pr.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND
                                    (Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate) ) AND 
                                    ((st.Id IS NULL ) OR 
                                    (DATE(st.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0))");

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressStateCode"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    Id = reader.GetGuidIncludeEmpty("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    DisciplineTask = reader.GetInt("DisciplineTask",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable",false)
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithPreviousOASISForHHRG(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var oasisDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { 8, 66, 73, 90, 9, 62, 69 };
         
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                p.FirstName as FirstName,
                p.LastName as LastName, 
                p.MiddleInitial as MiddleInitial,
                p.PatientIdNumber as PatientIdNumber,
                p.MedicareNumber as MedicareNumber,
                p.MedicaidNumber as MedicaidNumber,
                p.AddressZipCode as AddressZipCode,
                patientepisodes.EndDate as EndDate, 
                patientepisodes.StartDate as StartDate,
                patientepisodes.AdmissionId as AdmissionId,
                patientepisodes.Details as EpisodeNotes ,
                st.Id as Id ,
                p.Id as PatientId ,
                patientepisodes.Id as EpisodeId ,
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                st.Status as Status ,
                st.DisciplineTask as DisciplineTask , 
                st.Discipline as Discipline ,
                st.IsBillable as IsBillable ,
                assessments.SubmissionFormat as SubmissionFormat , 
                assessments.HippsCode as HippsCode
                    FROM
                        agencymanagment.patients p 
                            INNER JOIN patientprofiles pr ON pr.Id = p.Id
                            INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id 
                            LEFT JOIN scheduletasks ON st.EpisodeId = patientepisodes.Id
                            LEFT JOIN assessments ON assessments.EpisodeId = patientepisodes.Id AND assessments.Id = st.Id 
                                WHERE
                                    pr.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND
                                    ((Date(patientepisodes.StartDate) BETWEEN Date(@startdate) AND Date(@enddate) || Date(patientepisodes.EndDate) BETWEEN Date(@startdate) AND Date(@enddate)) OR ( DATE(patientepisodes.EndDate) between DATE(@backstartDate) and DATE(@startdate) AND DATE(st.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) AND st.DisciplineTask IN ( {0}) ) ) AND 
                                    ((st.Id IS NULL ) OR ( DATE(st.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0))", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .AddDateTime("backstartDate", startDate.AddDays(-65))
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    Id = reader.GetGuidIncludeEmpty("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate",DateTime.MinValue),
                    VisitDate = reader.GetDateTime("VisitDate", DateTime.MinValue),
                    Status = reader.GetInt("Status",0),
                    DisciplineTask = reader.GetInt("DisciplineTask",0),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable",false),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<Claim> GetRaps(Guid agencyId, List<Guid> rapIds)
        {
            var raps = new List<Claim>();
            var script = string.Format("SELECT * FROM `raps` WHERE `agencyid` = '{0}' AND `Id` IN ( {1}) ;", agencyId, rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                raps = cmd.SetConnection("HomeHealthConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return raps;
        }

        internal static List<Claim> GetFinals(Guid agencyId, List<Guid> finalIds)
        {
            var finals = new List<Claim>();
            var script = string.Format("SELECT * FROM `finals` WHERE `agencyid` = '{0}' AND `Id` IN ( {1} ) ;", agencyId, finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                finals = cmd.SetConnection("HomeHealthConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return finals;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var list = new List<AdmissionPeriod>();
            var script = @"select * from patientadmissiondates WHERE `AgencyId` = @agencyid and `StartOfCareDate` between @start and @end";

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("start", startDate)
                .AddDateTime("end", endDate)
                .AsList();
            }

            return list;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, List<Guid> Ids)
        {
            var list = new List<AdmissionPeriod>();
            var script = string.Format(@"select Id , PatientId , DischargedDate , StartOfCareDate , Status  from patientadmissiondates WHERE `AgencyId` = @agencyid AND `Id` IN ({0}) ", Ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new AdmissionPeriod
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     DischargedDate = reader.GetDateTime("DischargedDate"),
                     StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                     Status = reader.GetInt("Status")
                 })
                .AsList();
            }

            return list;
        }

        internal static List<InsuranceData> GetAgencyInsurances(Guid agencyId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }

        internal static List<InsuranceData> GetAgencyInsurancesByBranch(Guid agencyId, Guid branchId)
        {
            var list = new List<InsuranceData>();
            var script = string.Format("SELECT * FROM `agencyinsurances` WHERE `AgencyId` = '{0}';", agencyId);

            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name"),
                        PayorType = reader.GetInt("PayorType"),
                    }).AsList();
            }

            return list;
        }

        internal static InsuranceData GetMedicareInsurance(int id)
        {
            var script = string.Format("SELECT * FROM `insurances` WHERE `Id` = '{0}';", id);
            var insurance = new InsuranceData();
            using (var cmd = new FluentCommand<InsuranceData>(script))
            {
                insurance = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new InsuranceData
                    {
                        Id = reader.GetInt("Id"),
                        Name = reader.GetString("Name")
                    }).AsSingle();
            }
            return insurance;
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsForManagedClaimBill(Guid agencyId, Guid agencyLocationId, int insurance, int status, DateTime startDate, DateTime endDate, bool isBillableOnly, List<ScheduleEvent> allBilledVisitsIntheRange)
        {
            var isbillableScript = string.Empty;
            if (isBillableOnly)
            {
                isbillableScript = " st.IsBillable = 1 AND ";
            }
            var billedVisitScript = string.Empty;
            if (allBilledVisitsIntheRange != null && allBilledVisitsIntheRange.Count > 0)
            {
                billedVisitScript = string.Format(" st.Id NOT IN ({0}) AND ", allBilledVisitsIntheRange.ToCommaSeperatedList());
            }
            var statusScript = string.Empty;
            if(status != 0)
            {
                if(ScheduleStatusFactory.AllNoteNotYetStarted().Contains(status))
                {
                    statusScript = ScheduleStatusFactory.AllNoteNotYetDue().Contains(status) ? " st.Status = " + status + " AND st.EventDate < NOW() AND" : " st.Status = " + ScheduleStatusFactory.ConvertNotYetStartedToNotYetDue(status) + " AND st.EventDate >= NOW() AND ";
                }
                else
                {
                    statusScript = ScheduleStatusFactory.AllNoteNotYetStarted().Contains(status) ?   string.Format(" st.Status in ({0}) AND  ", ScheduleStatusFactory.AllNoteNotYetDue().ToCommaSeperatedList()) : " st.Status = @status AND "; 
                }
            }
            var list = new List<ScheduleEvent>();
            var script =
                string.Format(@"SELECT
                pr.FirstName as FirstName,
                pr.LastName as LastName, 
                pr.MiddleInitial as MiddleInitial,
                pr.PatientIdNumber as PatientIdNumber,
                st.Id as Id ,
                st.PatientId as PatientId ,
                st.EpisodeId as EpisodeId ,
                st.UserId as UserId , 
                st.EventDate as EventDate ,
                st.VisitDate as VisitDate ,
                st.DisciplineTask as DisciplineTask , 
                patientepisodes.StartDate as StartDate ,
                patientepisodes.EndDate as EndDate
                    FROM
                        scheduletasks st
                            INNER JOIN patientprofiles pr ON pr.Id = patientepisodes.PatientId
                            INNER JOIN patientepisodes ON st.EpisodeId = patientepisodes.Id
                                WHERE
                                   
                                    pr.AgencyId = @agencyid AND
                                    pr.AgencyLocationId = @agencylocationid  AND 
                                    {0}
                                    pr.Status IN (1,2) AND
                                    pr.IsDeprecated = 0 AND
                                    patientepisodes.IsActive = 1 AND
                                    patientepisodes.IsDischarged = 0 AND
                                    (pr.PrimaryInsurance = @insurance OR pr.SecondaryInsurance = @insurance OR pr.TertiaryInsurance = @insurance) AND 
                                    DATE(st.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate) AND
                                    DATE(st.EventDate) BETWEEN DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                    {1}
                                    {2}
                                    st.IsMissedVisit = 0 AND 
                                    st.IsDeprecated = 0 AND 
                                    st.DisciplineTask > 0 ", billedVisitScript, isbillableScript, statusScript);


            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddInt("insurance", insurance)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ManagedClaimData> GetManagedClaimsByInsurance(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                            managedclaims.Id as Id,
                            managedclaims.PatientId as PatientId,
                            managedclaims.EpisodeStartDate as StartDate,
                            managedclaims.EpisodeEndDate as EndDate,
                            managedclaims.VerifiedVisits as VerifiedVisits
                                FROM 
                                    managedclaims 
                                        INNER JOIN patientprofiles pr ON managedclaims.PatientId = pr.Id 
                                            WHERE 
                                                pr.AgencyId = @agencyid {0} AND
                                                pr.Status IN (1,2) AND
                                                pr.IsDeprecated = 0 AND 
                                                managedclaims.PrimaryInsuranceId = @insurance AND 
                                                (pr.PrimaryInsurance = @insurance OR pr.SecondaryInsurance = @insurance OR pr.TertiaryInsurance = @insurance) AND
                                                DATE(managedclaims.EpisodeEndDate) between DATE(@startdate) and DATE(@enddate)
                                                    ORDER BY  managedclaims.EpisodeStartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            var list = new List<ManagedClaimData>();
            using (var cmd = new FluentCommand<ManagedClaimData>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insurance", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ManagedClaimData
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     VerifiedVisits = reader.GetStringNullable("VerifiedVisits")
                     
                 })
                 .AsList();
            }
            return list;
        }

        internal static CbsaCode GetCbsaCode(string zipCode)
        {
            Axxess.LookUp.Domain.CBSACode code = LookupRepository.CbsaCode(zipCode);
            return code != null ? new CbsaCode()
            {
                CBSA = code.CBSA,
                CityName = code.CityName,
                CountyCode = code.CountyCode,
                CountyTitle = code.CountyTitle,
                Id = code.Id,
                StateCode = code.StateCode,
                StateName = code.StateName,
                Variance = code.Variance,
                WITwoEight = code.WITwoEight,
                WITwoEleven = code.WITwoEleven,
                WITwoNine = code.WITwoNine,
                WITwoSeven = code.WITwoSeven,
                WITwoTen = code.WITwoTen,
                WITwoTwelve = code.WITwoTwelve,
                Zip = code.Zip
            } : null;
            //var cbsaCode = new CbsaCode();
            //var script = string.Format("SELECT * FROM `cbsacodes` WHERE `zip` = '{0}' limit 0, 1;", zipCode);

            //using (var cmd = new FluentCommand<CbsaCode>(script))
            //{
            //    cbsaCode = cmd.SetConnection("AxxessLookupConnectionString")
            //        .SetMap(reader => new CbsaCode
            //        {
            //            CBSA = reader.GetStringNullable("CBSA"),
            //            WITwoTen = reader.GetDouble("WITwoTen"),
            //            WITwoEleven = reader.GetDouble("WITwoEleven"),
            //            WITwoTwelve = reader.GetDouble("WITwoTwelve")
            //        }).AsSingle();
            //}

            //return cbsaCode;
        }

        internal static HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            var code = LookupRepository.GetHhrgByHippsCodeAndYear(hippsCode, year);
            return code != null ? new HippsAndHhrg()
            {
                HHRG = code.HHRG,
                HHRGWeight = code.HHRGWeight,
                HIPPS = code.HIPPS,
                Id = code.Id,
                Time = code.Time
            } : null;
            //var result = new HippsAndHhrg();
            //var list = new List<HippsAndHhrg>();
            //var script = string.Format("SELECT * FROM `hippsandhhrgs` WHERE `HIPPS` = '{0}';", hippsCode);

            //using (var cmd = new FluentCommand<HippsAndHhrg>(script))
            //{
            //    list = cmd.SetConnection("AxxessLookupConnectionString")
            //        .SetMap(reader => new HippsAndHhrg
            //        {
            //            HHRG = reader.GetStringNullable("HHRG"),
            //            HIPPS = reader.GetStringNullable("HIPPS"),
            //            HHRGWeight = reader.GetDouble("HHRGWeight"),
            //            Time = reader.GetDateTime("Time")
            //        }).AsList();
            //}

            //if (list != null && list.Count > 0)
            //{
            //    result = list.Find(h => h.Time.Year == year);
            //}
            //return result;
        }

        internal static PPSStandard GetPPSStandardByYear(int year)
        {
            var pps = LookupRepository.GetPPSStandardByYear(year);
            return pps != null ? new PPSStandard()
            {
                Id = pps.Id,
                Labor = pps.Labor,
                NonLabor = pps.NonLabor,
                RuralRate = pps.RuralRate,
                RuralS = pps.RuralS,
                RuralT = pps.RuralT,
                RuralU = pps.RuralU,
                RuralV = pps.RuralV,
                RuralW = pps.RuralW,
                RuralX = pps.RuralX,
                S = pps.S,
                T = pps.T,
                Time = pps.Time,
                U = pps.U,
                UrbanRate = pps.UrbanRate,
                V = pps.V,
                W = pps.W,
                X = pps.X
            } : null;
            //var result = new PPSStandard();
            //var list = new List<PPSStandard>();

            //var script = @"SELECT * FROM `ppsstandards`;";
            //using (var cmd = new FluentCommand<PPSStandard>(script))
            //{
            //    list = cmd.SetConnection("AxxessLookupConnectionString")
            //        .SetMap(reader => new PPSStandard
            //        {
            //            Time = reader.GetDateTime("Time"),
            //            UrbanRate = reader.GetDouble("UrbanRate"),
            //            RuralRate = reader.GetDouble("RuralRate"),
            //            Labor = reader.GetDouble("Labor"),
            //            NonLabor = reader.GetDouble("NonLabor"),
            //            S = reader.GetDouble("S"),
            //            RuralS = reader.GetDouble("RuralS"),
            //            T = reader.GetDouble("T"),
            //            RuralT = reader.GetDouble("RuralT"),
            //            U = reader.GetDouble("U"),
            //            RuralU = reader.GetDouble("RuralU"),
            //            V = reader.GetDouble("V"),
            //            RuralV = reader.GetDouble("RuralV"),
            //            W = reader.GetDouble("W"),
            //            RuralW = reader.GetDouble("RuralW"),
            //            X = reader.GetDouble("X"),
            //            RuralX = reader.GetDouble("RuralX")
            //        }).AsList();
            //}

            //if (list != null && list.Count > 0)
            //{
            //    result = list.Find(h => h.Time.Year == year);
            //}

            //return result;
        }

        internal static List<CbsaCode> CbsaCodesByZip(string[] zipCodes)
        {
            return LookupRepository.CbsaCodes(zipCodes)
                .Select(s => new CbsaCode(){ 
                    CBSA = s.CBSA,
                    CityName = s.CityName,
                    CountyCode = s.CountyCode,
                    CountyTitle = s.CountyTitle,
                    Id = s.Id, 
                    StateCode = s.StateCode,
                    StateName = s.StateName,
                    Variance = s.Variance,
                    WITwoEight = s.WITwoEight,
                    WITwoEleven = s.WITwoEleven,
                    WITwoNine = s.WITwoNine,
                    WITwoSeven = s.WITwoSeven,
                    WITwoTen = s.WITwoTen,
                    WITwoTwelve = s.WITwoTwelve,
                    Zip = s.Zip
                }).ToList();
//            var list = new List<CbsaCode>();
//            if (zipCodes != null && zipCodes.Length > 0)
//            {
//                var script = string.Format(@"SELECT 
//                                        Id as Id, 
//                                        Zip as Zip, 
//                                        CBSA as CBSA, 
//                                        WITwoSeven as WITwoSeven,
//                                        WITwoEight as WITwoEight ,
//                                        WITwoNine as WITwoNine , 
//                                        WITwoTen as WITwoTen,
//                                        WITwoEleven as WITwoEleven,
//                                        WITwoTwelve as WITwoTwelve  
//                                          FROM 
//                                               cbsacodes 
//                                                        WHERE Zip IN ( {0} ) ", zipCodes.Select(z => "\'" + z + "\'").ToArray().Join(","));

//                using (var cmd = new FluentCommand<CbsaCode>(script))
//                {
//                    list = cmd.SetConnection("AxxessLookupConnectionString")
//                    .SetMap(reader => new CbsaCode
//                    {
//                        Id = reader.GetInt("Id"),
//                        Zip = reader.GetStringNullable("Zip"),
//                        CBSA = reader.GetStringNullable("CBSA"),
//                        WITwoSeven = reader.GetDouble("WITwoSeven"),
//                        WITwoEight = reader.GetDouble("WITwoEight"),
//                        WITwoNine = reader.GetDouble("WITwoNine"),
//                        WITwoTen = reader.GetDouble("WITwoTen"),
//                        WITwoEleven = reader.GetDouble("WITwoEleven"),
//                        WITwoTwelve = reader.GetDouble("WITwoTwelve")
//                    })
//                    .AsList();
//                }
//            }
//            return list;
        }

        internal static List<PPSStandard> PPSStandards(int[] years)
        {
            return LookupRepository.GetPPSStandardsByYears(years)
                .Select(s => new PPSStandard()
            {
                Id = s.Id,
                Labor = s.Labor,
                NonLabor = s.NonLabor,
                RuralRate = s.RuralRate,
                RuralS = s.RuralS,
                RuralT = s.RuralT,
                RuralU = s.RuralU,
                RuralV = s.RuralV,
                RuralW = s.RuralW,
                RuralX = s.RuralX,
                S = s.S,
                T = s.T,
                Time = s.Time,
                U = s.U,
                UrbanRate = s.UrbanRate,
                V = s.V,
                W = s.W,
                X = s.X
            }).ToList();

//            var list = new List<PPSStandard>();
//            if (years != null && years.Length > 0)
//            {
//                var script = string.Format(@"SELECT 
//                                        Id as Id, 
//                                        Time as Time, 
//                                        UrbanRate as UrbanRate, 
//                                        RuralRate as RuralRate,
//                                        Labor as Labor,
//                                        NonLabor as NonLabor, 
//                                        S as S,
//                                        RuralS as RuralS,
//                                        T as T, 
//                                        RuralT as RuralT,
//                                        U as U,
//                                        RuralU as RuralU, 
//                                        V as V,
//                                        RuralV as RuralV,
//                                        W as W,
//                                        RuralW as RuralW, 
//                                        X as X,
//                                        RuralX as RuralX  
//                                          FROM 
//                                               ppsstandards 
//                                                        WHERE year(Time) IN ( {0} ) ", years.Select(z => z.ToString()).ToArray().Join(","));

//                using (var cmd = new FluentCommand<PPSStandard>(script))
//                {
//                    list = cmd.SetConnection("AxxessLookupConnectionString")
//                    .SetMap(reader => new PPSStandard
//                    {
//                        Id = reader.GetInt("Id"),
//                        Time = reader.GetDateTime("Time"),
//                        UrbanRate = reader.GetDouble("UrbanRate"),
//                        RuralRate = reader.GetDouble("RuralRate"),
//                        Labor = reader.GetDouble("Labor"),
//                        NonLabor = reader.GetDouble("NonLabor"),
//                        S = reader.GetDouble("S"),
//                        RuralS = reader.GetDouble("RuralS"),
//                        T = reader.GetDouble("T"),
//                        RuralT = reader.GetDouble("RuralT"),
//                        U = reader.GetDouble("U"),
//                        RuralU = reader.GetDouble("RuralU"),
//                        V = reader.GetDouble("V"),
//                        RuralV = reader.GetDouble("RuralV"),
//                        W = reader.GetDouble("W"),
//                        RuralW = reader.GetDouble("RuralW"),
//                        X = reader.GetDouble("X"),
//                        RuralX = reader.GetDouble("RuralX")
//                    })
//                    .AsList();
//                }
//            }
//            return list;
        }

        internal static List<HippsAndHhrg> GetHhrgByHippsCodeAndYear(string[] hippsAndYears)
        {
           return LookupRepository.HippsAndHhrgs().Where(w => hippsAndYears.Contains(w.HIPPS + w.Time.Year))
               .Select(s => new HippsAndHhrg(){
                    HHRG = s.HHRG,
                    HHRGWeight = s.HHRGWeight,
                    HIPPS = s.HIPPS,
                    Id = s.Id,
                    Time = s.Time
               }).ToList();

//            var list = new List<HippsAndHhrg>();
//            if (hippsAndYears != null && hippsAndYears.Length > 0)
//            {
//                var script = string.Format(@"SELECT 
//                                        Id as Id, 
//                                        HHRG as HHRG, 
//                                        HIPPS as HIPPS, 
//                                        HHRGWeight as HHRGWeight,
//                                        Time as Time 
//                                          FROM 
//                                               hippsandhhrgs 
//                                                        WHERE concat(HIPPS,year(Time)) IN ( {0} ) ", hippsAndYears.Select(z => "\'" + z + "\'").ToArray().Join(","));

//                using (var cmd = new FluentCommand<HippsAndHhrg>(script))
//                {
//                    list = cmd.SetConnection("AxxessLookupConnectionString")
//                    .SetMap(reader => new HippsAndHhrg
//                    {
//                        Id = reader.GetInt("Id"),
//                        HHRG = reader.GetStringNullable("HHRG"),
//                        HIPPS = reader.GetStringNullable("HIPPS"),
//                        HHRGWeight = reader.GetDouble("HHRGWeight"),
//                        Time = reader.GetDateTime("Time")
//                    })
//                    .AsList();
//                }
//            }
//            return list;
        }

        #endregion

        #region Helper Functions

        internal static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month, 1, 0, 0, 0, 0);
        }

        internal static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month,
               DateTime.DaysInMonth(Year, (int)Month), 23, 59, 59, 999);
        }

        internal static DateTime GetStartOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetStartOfMonth(12, Year - 1);
            else
                return GetStartOfMonth(Month - 1, Year);
        }

        #endregion
    }
}
