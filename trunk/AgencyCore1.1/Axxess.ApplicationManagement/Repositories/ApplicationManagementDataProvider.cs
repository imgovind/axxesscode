﻿

namespace Axxess.ApplicationManagement.Repositories
{

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class ApplicationManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public ApplicationManagementDataProvider()
        {
            this.database = new SimpleRepository(CoreSettings.ApplicationManagementConnectionString, SimpleRepositoryOptions.None);
        }

        #endregion
    }
}
