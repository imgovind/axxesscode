﻿

namespace Axxess.ApplicationManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using SubSonic.Repository;
    using Axxess.ApplicationManagement.Domain;
    public class ApplicationManagementRepository : IApplicationManagementRepository
    {

        #region Constructor

        private readonly SimpleRepository database;
        //private readonly string databaseIp;

        public ApplicationManagementRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Methods
        public bool AddTherapyEmployors(TherapyEmployors therapyEmployors)
        {
            if (therapyEmployors != null)
            {
                therapyEmployors.Id = new Guid();
                therapyEmployors.Created = DateTime.Now;
                database.Add<TherapyEmployors>(therapyEmployors);
                return true;
            }
            return false;
        }

        public bool UpdateTherapyEmployors(TherapyEmployors therpyEmployors)
        {
            if (therpyEmployors != null)
            {
                database.Update<TherapyEmployors>(therpyEmployors);
                return true;
            }
            return false;
        }
        #endregion
    }
}
