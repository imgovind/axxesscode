﻿


namespace Axxess.ApplicationManagement.Repositories
{
    using Axxess.ApplicationManagement.Domain;
    public interface IApplicationManagementRepository
    {
        bool AddTherapyEmployors(TherapyEmployors therapyEmployors);
        bool UpdateTherapyEmployors(TherapyEmployors therpyEmployors);

    }
}
