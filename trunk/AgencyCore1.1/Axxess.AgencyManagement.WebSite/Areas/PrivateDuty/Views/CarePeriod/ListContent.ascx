﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PrivateDutyCarePeriod>>" %>
<span class="wintitle">List Care Periods | <%= ViewData["PatientName"].ToSafeString() %></span>
<% if (ViewData["ActiveInactive"].ToSafeString() != "False") { %>
	<% var activeCarePeriods = Model.Where(cp => cp.IsActive && !cp.IsDischarged).ToList(); %>
	<ul>
		<li class="ac"><h3>Active Care Periods</h3></li>
		<li>
			<span class="grid-third sortable">Start Date</span>
			<span class="grid-third sortable">End Date</span>
			<span>Actions</span>
		</li>
	</ul>
	<% if (activeCarePeriods.Count == 0) { %>
		<ol>
			<li class="no-hover"><h4>No Active Care Periods</h4></li>
		</ol>
	<% } else { %>
		<ol>
			<% foreach (var carePeriod in activeCarePeriods.OrderBy(cp => cp.StartDate).Reverse()) { %>
				<li guid="<%= carePeriod.Id %>" patient="<%= carePeriod.PatientId %>">
					<span class="grid-third"><%= carePeriod.StartDate.ToZeroFilled() %></span>
					<span class="grid-third"><%= carePeriod.EndDate.ToZeroFilled()%></span>
					<span><a class="edit-care-period link">Edit</a><a class="deactivate-care-period link">Deactivate</a></span>
				</li>
			<% } %>
		</ol>
    <% } %>
<% } %>
<% if (ViewData["ActiveInactive"].ToSafeString() == "False" || ViewData["ActiveInactive"] == null) { %>
	<% var inactiveCarePeriods = Model.Where(cp => !cp.IsActive || cp.IsDischarged).ToList(); %>
	<ul>
		<li class="ac"><h3>Inactive Care Periods</h3></li>
		<li>
			<span class="grid-third">Start Date</span>
			<span class="grid-third">End Date</span>
			<span>Actions</span>
		</li>
	</ul>
    <% if (inactiveCarePeriods.Count == 0) { %>
		<ol>
			<li class="no-hover"><h4>No Inactive Care Periods</h4></li>
		</ol>
    <% } else { %>
		<ol>
			<% foreach (var carePeriod in inactiveCarePeriods.OrderBy(cp => cp.StartDate).Reverse()) { %>
				<li guid="<%= carePeriod.Id %>" patient="<%= carePeriod.PatientId %>">
					<span class="grid-third"><%= carePeriod.StartDate.ToZeroFilled()%></span>
					<span class="grid-third"><%= carePeriod.EndDate.ToZeroFilled()%></span>
					<span><a class="activate-care-period link">Activate</a></span>
				</li>
			<% } %>
		</ol>
    <% } %>
<% } %>