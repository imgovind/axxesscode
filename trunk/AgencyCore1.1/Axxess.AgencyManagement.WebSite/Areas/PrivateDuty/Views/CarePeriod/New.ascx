﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "CarePeriod", FormMethod.Post, new { @id = "NewPrivateDutyCarePeriod_Form", @area = "PrivateDuty" })) { %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label for="NewPrivateDutyCarePeriod_PatientId" class="fl">Patient</label>
                <div class="fr">
    <%  if (Model.IsFromMainMenu) { %>
				    <input type="text" name="PatientId" service="2" value="<%= Model.PatientId.IsEmpty() ? string.Empty : Model.PatientId.ToSafeString() %>" id="NewPrivateDutyTask_PatientId" class="patient-picker required" default="<%= Model.PatientId.IsEmpty() ? string.Empty : Model.PatientId.ToSafeString() %>" />
	<%  } else { %>
	                <%= Model.DisplayName %>
	                <%= Html.Hidden("PatientId", Model.PatientId) %>
	<%  } %>
				</div>
			</div>
		</div>
    <%  if (!Model.PatientId.IsEmpty()) { %>
        <div class="column">
            <div class="row">
                <label for="NewPrivateDutyCarePeriod_AdmissionId" class="fl">Start of Care Date</label>
				<div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "NewPrivateDutyCarePeriod_AdmissionId", @class = "short" })%></div>
			</div>
		</div>
	<%  } %>
	</fieldset>
    <div class="content"><% Html.RenderPartial("Content/New", Model); %></div>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac"><%= Html.TextArea("Comments", new { @id = "NewPrivateDutyCarePeriod_Comments", @maxcharacters = "1000" }) %></div>
        </div>
    </fieldset>
    <%= Html.Hidden("AddAnother", "0", new { @id = "NewPrivateDutyCarePeriod_AddAnother" }) %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="save clear">Save &#38; Add Another</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>