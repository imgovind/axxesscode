﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PrivateDutyCarePeriod>>" %>
<div class="wrapper main">
    <fieldset class="ac grid-controls">
        <div class="filter">
            <label for="ListPrivateDutyCarePeriods_PatientId">Patient</label>
            <%= Html.TextBox("PatientId", ViewData["PatientId"].ToSafeString(), new { @id = "ListPrivateDutyCarePeriods_PatientId", prefilled = ViewData["PatientName"].ToSafeString(), @class = "patient-picker", @service = "2" })%>
        </div>
        <div class="filter">
            <label for="ListPrivateDutyCarePeriods_ActiveInactive">Show</label>
            <select id="ListPrivateDutyCarePeriods_ActiveInactive" name="ActiveInactive">
                <option value="true">Active Only</option>
                <option value="false">Inactive Only</option>
                <option value="" selected="selected">All</option>
            </select>
        </div>
    </fieldset>
    <div class="patient-care-periods acore-grid"><% Html.RenderPartial("ListContent", Model); %></div>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
</div>