﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<fieldset>
    <legend>Care Period Details</legend>
    <div class="column">
<%  if (Model.IsFromMainMenu) { %>
	    <div class="row">
			<label for="NewPrivateDutyCarePeriod_AdmissionId" class="fl">Start of Care Date</label>
			<div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "NewPrivateDutyCarePeriod_AdmissionId" })%></div>
		</div>
<%  } %>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_StartDate" class="fl">Start Date</label>
            <div class="fr"><%= Html.DatePicker("StartDate", Model.EndDate.AddDays(1), true, new { id = "NewPrivateDutyCarePeriod_StartDate", @class = "care-period-start" })%></div>
<%  if (Model.StartDate.IsValid()) { %>
            <div class="clr"></div>
            <em class="fr">Previous Care Period end<%= Model.StartDate > DateTime.Now ? "s" : "ed"%> <%= Model.EndDate.ToZeroFilled() %></em>
<%  } %>
        </div>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_PeriodLength" class="fl">Care Period Length</label>
            <div class="fr">
                <select id="NewPrivateDutyCarePeriod_PeriodLength" class="care-period-length">
                    <option value="14">14 Days</option>
                    <option value="30">30 Days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
                    <option value="120">120 Days</option>
                    <option value="specify" selected="selected">Specify End Date</option>
                </select>
            </div>
        </div>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_EndDate" class="fl">End Date</label>
            <div class="fr"><%= Html.DatePicker("EndDate", Model.EndDate.AddDays(60), true, new { id = "NewPrivateDutyCarePeriod_EndDate", @class = "care-period-end" })%> </div>
        </div>
    </div>
    <div class="column">
	     <div class="row">
            <label for="NewPrivateDuty_CaseManager" class="fl">Case Manager</label>
            <div class="fr"><%= Html.Users("CaseManagerId", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), "-- Select User --",Guid.Empty, (int)UserStatus.Active,(int) AgencyServices.PrivateDuty, new { @id = "NewPrivateDuty_CaseManagerId", @class = "user-selector required" })%></div>
        </div>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_PhysicianId" class="fl">Primary Physician</label>
            <div class="fr"><%= Html.TextBox("PhysicianId", Model.PrimaryPhysician, new { @id = "NewPrivateDutyCarePeriod_PhysicianId", @class = "physician-picker" })%></div>
        </div>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_PrimaryInsurance" class="fl">Primary Insurance</label>
            <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance, Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, false, new { @id = "NewPrivateDutyCarePeriod_PrimaryInsurance", @class = "Insurances required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="NewPrivateDutyCarePeriod_SecondaryInsurance" class="fl">Secondary Insurance</label>
            <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance, Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, false, new { @id = "NewPrivateDutyCarePeriod_SecondaryInsurance", @class = "Insurances" })%></div>
        </div>
    </div>
</fieldset>