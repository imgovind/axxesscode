﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PrivateDutyCarePeriod>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "CarePeriod", FormMethod.Post, new { @id = "EditPrivateDutyCarePeriod_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("AdmissionId", Model.AdmissionId)%>
    <fieldset>
        <legend>Edit Care Period</legend>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Patient</label>
                <div class="fr"><%= ViewData["PatientName"].ToSafeString() %></div>
            </div>
            <div class="row">
                <label for="EditPrivateDutyCarePeriod_StartDate" class="fl">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required care-period-start" name="StartDate" id="EditPrivateDutyCarePeriod_StartDate" value="<%= Model.StartDate.ToShortDateString() %>" /></div>
	<%  if (ViewData["PreviousEndDate"].ToSafeString().IsDate()) { %>
				<div class="clr"></div>
                <em class="fr">Previous Care Period end<%= ViewData["PreviousEndDate"].ToSafeString().ToDateTime() > DateTime.Now ? "s" : "ed" %> <%= ViewData["PreviousEndDate"].ToSafeString()%></em>
	<%  } %>
            </div>
            <div class="row">
                <label for="EditPrivateDutyCarePeriod_PeriodLength" class="fl">Care Period Length</label>
                <div class="fr">
                    <select id="EditPrivateDutyCarePeriod_PeriodLength" class="care-period-length">
                        <option value="14">14 Days</option>
                        <option value="30">30 Days</option>
                        <option value="60">60 Days</option>
                        <option value="90">90 Days</option>
                        <option value="120">120 Days</option>
                        <option value="specify" selected="selected">Specify End Date</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="EditPrivateDutyCarePeriod_EndDate" class="fl">End Date</label>
                <div class="fr"><input type="text" class="date-picker required care-period-end" name="EndDate" id="EditPrivateDutyCarePeriod_EndDate" value="<%= Model.EndDate.ToShortDateString() %>" /></div>
	<%  if (ViewData["NextStartDate"].ToSafeString().IsDate()) { %>
				<div class="clr"></div>
                <em class="fr">Next Care Period start<%= ViewData["NextStartDate"].ToSafeString().ToDateTime() > DateTime.Now ? "s" : "ed" %> <%= ViewData["NextStartDate"].ToSafeString()%></em>
	<%  } %>
            </div>
        </div>
        <div class="column">
	        <div class="row">
				<label for="EditPrivateDuty_CaseManager" class="fl">Case Manager</label>
				<div class="fr"><%= Html.Users("CaseManagerId", Model.CaseManagerId.IsEmpty() ? Model.CaseManagerId.ToString() : Guid.Empty.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "EditPrivateDuty_CaseManagerId", @class = "user-selector required" })%></div>
			</div>
			<div class="row">
				<label for="EditPrivateDutyCarePeriod_PhysicianId" class="fl">Primary Physician</label>
				<div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId, new { @id = "EditPrivateDutyCarePeriod_PhysicianId", @class = "physician-picker" })%></div>
			</div>
			<div class="row">
				<label for="EditPrivateDutyCarePeriod_PrimaryInsurance" class="fl">Primary Insurance</label>
				<div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance.ToString(), Guid.Empty, (int)AgencyServices.PrivateDuty, false, new { @id = "EditPrivateDutyCarePeriod_PrimaryInsurance", @class = "Insurances required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="EditPrivateDutyCarePeriod_SecondaryInsurance" class="fl">Secondary Insurance</label>
				<div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance.ToString(), Guid.Empty, (int)AgencyServices.PrivateDuty, false, new { @id = "EditPrivateDutyCarePeriod_SecondaryInsurance", @class = "Insurances" })%></div>
			</div>
			<div class="row">
				<ul class="checkgroup one-wide">
					<%= Html.CheckgroupOption("IsActive", "EditPrivateDutyCarePeriod_IsActive", "true", Model.IsActive, "Active", null)%>
				</ul>
			</div>
		</div>
        <div class="wide-column">
            <div class="row">
                <label for="EditPrivateDutyCarePeriod_Comments">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.Comments, new { @id = "EditPrivateDutyCarePeriod_Comments", @maxcharacters = "1000" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>