﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewTaskViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Schedule", FormMethod.Post, new { @id = "NewPrivateDutyTask_Form", @class = "mainform" })) { %>
    <fieldset>
        <legend>Employee Scheduler</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr">
    <%  if (Model.PatientId.IsEmpty()) { %><%= Html.PatientsForSchedule((int)AgencyServices.PrivateDuty, "PatientId", Guid.Empty, Model.PatientId.ToString(), 0, "-- Select Patient --", new { @id = "NewPrivateDutyTask_PatientId", @class = "required not-zero", @default = ViewData["PatientId"].ToSafeString() }) %>
    <%  } else { %><%= Model.PatientName %><%= Html.Hidden("PatientId", Model.PatientId, new { @id = "NewPrivateDutyTask_PatientId" })%><%  } %>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="NewPrivateDutyTask_DisciplineTask">Task</label>
                <div class="fr"><%= Html.MultipleDisciplineTasks("DisciplineTask", "", new { @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label class="fl" for="NewPrivateDutyTask_UserId">User/Employee</label>
                <div class="fr">
    <%  if (Model.UserId.IsEmpty()) { %>
                    <%= Html.Users("UserId", Model.UserId.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "NewPrivateDutyTask_UserId", @class = "required not-zero", @default = ViewData["UserId"].ToSafeString() }) %>
    <%  } else { %>
                    <%= Model.UserName %>
                    <%= Html.Hidden("UserId", Model.PatientId, new { @id = "NewPrivateDutyTask_UserId" })%>
    <%  } %>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="NewPrivateDutyTask_StartDate">Start Time</label>
                <div class="fr">
                    <%= Html.DatePicker("EventStartTime.Date", Model.StartDate, true, new { @class = "short startdate", @id = "NewPrivateDutyTask_StartDate" })%>
                    <span><%= Html.TimePicker("EventStartTime.Time", Model.StartTime, true, new { @class = "short", @id = "NewPrivateDutyTask_StartTime" })%></span>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="NewPrivateDutyTask_EndDate">End Time</label>
                <div class="fr">
                    <%= Html.DatePicker("EventEndTime.Date", DateTime.MinValue, true, new { @class = "short enddate", @id = "NewPrivateDutyTask_EndDate" })%>
                    <span><%= Html.TimePicker("EventEndTime.Time", true, new { @class = "short", @id = "NewPrivateDutyTask_EndTime" })%></span>
                </div>
            </div>
            <div class="row"> <label class="fl" for="NewPrivateDutyTask_PayorId">Payor</label><div class="fr"><select name="PayorId" class="required not-zero"></select></div></div>
            <div class="row">
                <div class="checkgroup one-wide">
                    <div class="option">
                        <div class="wrapper">
                            <input type="checkbox" name="IsAllDay" id="NewPrivateDutyTask_IsAllDay" value="true" />
                            <label for="NewPrivateDutyTask_IsAllDay">All Day Event</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong" for="NewPrivateDutyTask_Comments">Comments</label>
                <div><%= Html.TextArea("Comments", new { @id = "NewPrivateDutyTask_Comments", @maxcharacters = "1000" })%></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("AddAnother", "0", new { @id = "NewPrivateDutyTask_AddAnother" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="save clear">Save &#38; Add Another</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>
