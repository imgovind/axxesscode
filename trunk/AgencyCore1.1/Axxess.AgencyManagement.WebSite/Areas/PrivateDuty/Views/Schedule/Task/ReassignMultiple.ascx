﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("ReassignMultiple", "Schedule", FormMethod.Post, new { @id = "ReassignPrivateDutyTask_Form" })) { %>
    <fieldset>
    <legend>Reassign Tasks</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl" for="ReassignPrivateDutyTask_PatientId">Patient</label>
                <div class="fr"><%= Html.PatientsForSchedule((int)AgencyServices.PrivateDuty, "PatientId", Guid.Empty, Model.PatientId.ToSafeString(), 0, "-- Select Patient --", new { @id = "ReassignPrivateDutyTask_PatientId", @class = "required not-zero", @default = ViewData["PatientId"].ToSafeString() })%></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignPrivateDutyTask_OldUserId">Existing User/Employee</label>
                <div class="fr"><%= Html.Users("OldUserId", Model.UserId.ToSafeString(),"-- Select User --",Guid.Empty,(int)UserStatus.Active,(int)AgencyServices.PrivateDuty, new { @id = "ReassignPrivateDutyTask_OldUserId", @class = "required not-zero", @default = ViewData["UserId"].ToSafeString() })%></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignPrivateDutyTask_NewUserId">New User/Employee</label>
                <div class="fr"><%= Html.Users( "NewUserId", "","-- Select User --",Guid.Empty,(int)UserStatus.Active,(int)AgencyServices.PrivateDuty, new { @id = "ReassignPrivateDutyTask_NewUserId", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignPrivateDutyTask_StartDate">Date From</label>
                <div class="fr"><%= Html.DatePicker("StartDate", Model.StartDate, true, new { @id = "ReassignPrivateDutyTask_StartDate" }) %></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignPrivateDutyTask_EndDate">Date To</label>
                <div class="fr"><%= Html.DatePicker("EndDate", Model.EndDate, true, new { @id = "ReassignPrivateDutyTask_EndDate" }) %></div>
            </div>
            <div class="row">
                <div class="strong">Note:</div>
                <em>Only tasks that are not started and not yet due will be reassigned.</em>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Reassign</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>