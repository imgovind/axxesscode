﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  DateTime startDate = ViewData["StartDate"].ToString().ToDateTime(); %>
<%  DateTime endDate = ViewData["EndDate"].ToString().ToDateTime(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("CreateMultiple", "Schedule", FormMethod.Post, new { @id = "NewMultiplePrivateDutyTask_Form", @class = "mainform" })) { %>
    <fieldset>
    <legend>Schedule Employee</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl" for="NewMultiplePrivateDutyTask_PatientId">Patient</label>
                <div class="fr"><%= Html.PatientsForSchedule((int)AgencyServices.PrivateDuty, "PatientId", Guid.Empty, ViewData["PatientId"].ToSafeString(), 0, "-- Select Patient --", new { @id = "NewMultiplePrivateDutyTask_PatientId", @class = "required not-zero", @default = ViewData["PatientId"].ToSafeString() })%></div>
            </div>
            <div class="row">
                <label class="fl" for="NewMultiplePrivateDutyTask_UserId">User/Employee</label>
                <div class="fr"><%= Html.Users("UserId", ViewData["UserId"].ToSafeString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "NewMultiplePrivateDutyTask_UserId", @class = "required not-zero", @default = ViewData["UserId"].ToSafeString() })%></div>
            </div>
            <div class="row">
                <label class="fl" for="NewMultiplePrivateDutyTask_DisciplineTask">Task</label>
                <div class="fr"><%= Html.MultipleDisciplineTasks("DisciplineTask", string.Empty, new { @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <div class="cal full">
                    <table>
                        <thead class="ac">
                            <tr>
                                <th colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startDate)%></th>
                            </tr>
                            <tr>
                                <th>Su</th>
                                <th>Mo</th>
                                <th>Tu</th>
                                <th>We</th>
                                <th>Th</th>
                                <th>Fr</th>
                                <th>Sa</th>
                            </tr>
                        </thead>
                        <tbody>
    <%  for (DateTime currentDate = startDate.AddDays(-(int)startDate.DayOfWeek); currentDate < ((int)endDate.DayOfWeek == 0 ? endDate : endDate.AddDays(7 - (int)endDate.DayOfWeek)); currentDate = currentDate.AddDays(1)) { %>
        <%  if ((int)currentDate.DayOfWeek == 0) { %>
                            <tr>
        <%  } %>
        <%  if (currentDate >= startDate && currentDate < endDate) { %>
                            <td date="<%= currentDate.ToShortDateString() %>">
                                <div class="datelabel"><a><%= currentDate.Day %></a></div>
                            </td>
        <%  } else { %>
                            <td class="inactive"></td>
        <%  } %>
        <%  if ((int)currentDate.DayOfWeek == 6) { %>
                        </tr>
        <%  } %>
    <%  } %>
                        </tbody>
                    </table>
                </div>
                <input type="hidden" name="EventDates" id="NewMultiplePrivateDutyTask_EventDates" />
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsAllDay", "true", false, "All Day Event") %>
                </ul>
            </div>
            <div class="row">
                <label class="fl" for="NewMultiplePrivateDutyTask_StartTime">Start Time</label>
                <div class="fr"><input type="text" class="time-picker required" name="EventStartTime.Time" id="NewMultiplePrivateDutyTask_StartTime" /></div>
            </div>
            <div class="row">
                <label class="fl" for="NewMultiplePrivateDutyTask_EndTime">End Time</label>
                <div class="fr"><input type="text" class="time-picker required" name="EventEndTime.Time" id="NewMultiplePrivateDutyTask_EndTime" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
