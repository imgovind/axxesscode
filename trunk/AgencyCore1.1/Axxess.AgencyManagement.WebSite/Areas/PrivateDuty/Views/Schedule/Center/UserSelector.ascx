﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserSelection>>" %>
<%  Html.Telerik().Grid<UserSelection>(Model).Name("PrivateDutyScheduleSelectionGrid").Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.FirstName);
        columns.Bound(p => p.DisplayName).Hidden();
		columns.Bound(p => p.Id).HtmlAttributes(new { @class = "id" }).Hidden();
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("CenterUserGrid", "Schedule", new { branchId = Guid.Empty, statusId = 1, name = string.Empty, area = "PrivateDuty" })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("Schedule.Center.PrivateDuty.Selector.Init")
        .OnRowSelect("Schedule.Center.PrivateDuty.Selector.Select")
    ).NoRecordsTemplate("No Employees found.").Render(); %>