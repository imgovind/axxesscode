﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%var IdPrefix = ViewData.ContainsKey("IdPrefix") ? ViewData["IdPrefix"] : string.Empty;%>
<%  Html.Telerik().Grid<GridTask>().HtmlAttributes(new { @class = "scrollable" }).Name(IdPrefix + "PrivateDutyTask_Grid").Columns(columns =>{
        columns.Bound(s => s.TaskName)
            .ClientTemplate("<# if (IsUserCanEdit) { #><a class=\"<# if (IsMissedVisit) { #>missedvisit<# } else { #>edit<# } #> <# if (IsComplete) { #>complete<# } #>\"><#= TaskName #></a><# } else { #><#=TaskName#><# } #> ")
            .Title("Task")
            .Width(160)
            .HtmlAttributes(new { @class = "edit-task" }).Sortable(true);
        columns.Bound(s => s.DateIn).Format("{0:MM/dd/yyyy hh:mm tt}").Title("Start Time").HtmlAttributes(new { @class = "start-date" }).Width(145);
        columns.Bound(s => s.DateOut).Format("{0:MM/dd/yyyy hh:mm tt}").Title("End Time").HtmlAttributes(new { @class = "end-date" }).Width(145);
        columns.Bound(s => s.PatientName).Width(100).Title("Patient");
        columns.Bound(s => s.UserName).Width(100).Title("Employee");
        columns.Bound(s => s.StatusName).Width(100).Title("Status");
        columns.Bound(s => s.StatusComment)
            .ClientTemplate("<# if (StatusComment) { #><a class=\"img icon16 red note\" tooltip=\"<#= StatusComment #>\"></a><# } #>")
            .Title(" ").Width(25).Sortable(false);
        columns.Bound(s => s.Comments)
            .ClientTemplate("<# if (Comments) { #><a class=\"img icon16 yellow note\" tooltip=\"<#= Comments #>\"></a><# } #>")
            .Title(" ").Width(25).Sortable(false);
        columns.Bound(s => s.EpisodeNotes)
            .ClientTemplate("<# if (EpisodeNotes) { #><a class=\"img icon16 blue note\" tooltip=\"<#= EpisodeNotes #>\"></a><# } #>")
            .Title(" ").Width(25).Sortable(false);
        columns.Bound(s => s.IsUserCanPrint)
            .ClientTemplate("<# if (IsUserCanPrint) { #><a class=\"<#=Group#> img icon16 print\"></a><# } #>")
            .Title(" ").Width(22)
            .HtmlAttributes(new { @class = "centered-unpadded-cell" })
            .Sortable(false);
        columns.Bound(s => s.IsAttachmentExist)
            .ClientTemplate("<# if(IsAttachmentExist){#> <a class=\"img icon16 paperclip attachment\"></a><#}#>")
            .HeaderHtmlAttributes(new { @class = "attachmentload" })
            .HtmlAttributes(new { @class = "attachmentload" })
            .Title(" ").Width(30)
            .Sortable(false);
        //columns.Bound(s => s.IsMissedVisit)
        //    .ClientTemplate("<# if(!IsMissedVisit && DateIn <= new Date()){#><a class=\"mark-missedvisit\">Missed Visit Form</a><#}#>")
        //    .Title(" ").Width(120).Sortable(false);
        columns.Bound(s => s.Id)
            .ClientTemplate("<# if(IsUserCanEditDetail || IsUserCanReopen || IsUserCanReassign || IsUserCanRestore || IsUserCanDelete || (IsUserCanEdit && !IsMissedVisit)) #><div class='grid-action'><span class='img pointer right big'/></div>")
            //.Template(s => url(s))
            .Encoded(false)
            .Title("Action")
            //.Visible(isActionVisible)
            .HtmlAttributes(new { @class = "action menu-based" })
            .Title("Action")
			.Width(70)
            //.Width(widthIndex * 55)
            .Sortable(false);
        columns.Bound(s => s.EpisodeId).HtmlAttributes(new { @class = "eid" }).Hidden(true);
        columns.Bound(s => s.PatientId).HtmlAttributes(new { @class = "pid" }).Hidden(true);
        columns.Bound(s => s.Id).HtmlAttributes(new { @class = "id" }).Hidden(true);
        columns.Bound(s => s.Service).HtmlAttributes(new { @class = "service" }).Hidden(true);
        columns.Bound(s => s.Type).HtmlAttributes(new { @class = "type" }).Hidden(true);
        columns.Bound(s => s.IsComplete).Visible(false);
    })
    .DataBinding(dataBinding => dataBinding
        .Ajax()
        .Select("List", "Schedule", new { area = "PrivateDuty", patientId = Guid.Empty, userId = Guid.Empty, startDate = DateTime.Now, endDate = DateTime.Now }).OperationMode(GridOperationMode.Client))
    .ClientEvents(events => events
        .OnDataBinding("Schedule.Center.PrivateDuty.OnDataBinding")
        .OnRowDataBound("Schedule.Center.PrivateDuty.OnRowDataBound")
        .OnDataBound("Schedule.Center.PrivateDuty.OnDataBound"))
        //.RowAction(row =>
        //{
        //    if (row.DataItem.IsComplete)
        //    {
        //        if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed")) row.HtmlAttributes["class"] += " completed darkgreen";
        //        else row.HtmlAttributes.Add("class", "completed darkgreen");
        //    }
        //    if (row.DataItem.IsOrphaned)
        //    {
        //        if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned")) row.HtmlAttributes["class"] += " orphaned";
        //        else row.HtmlAttributes.Add("class", "orphaned");
        //    }
        //})
        //.Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order =>
        //{
        //    if (sortName == "TaskName")
        //    {
        //        if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
        //        else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        //    }
        //    else if (sortName == "DateIn")
        //    {
        //        if (sortDirection == "ASC") order.Add(o => o.DateIn).Ascending();
        //        else if (sortDirection == "DESC") order.Add(o => o.DateIn).Descending();
        //    }
        //    else if (sortName == "PatientName")
        //    {
        //        if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
        //        else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        //    }
        //    else if (sortName == "UserName")
        //    {
        //        if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
        //        else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        //    }
        //    else if (sortName == "StatusName")
        //    {
        //        if (sortDirection == "ASC") order.Add(o => o.StatusName).Ascending();
        //        else if (sortDirection == "DESC") order.Add(o => o.StatusName).Descending();
        //    }
        //})
        //)
        .Sortable()
        .Scrollable()
        .Footer(false)
        .Render(); %>