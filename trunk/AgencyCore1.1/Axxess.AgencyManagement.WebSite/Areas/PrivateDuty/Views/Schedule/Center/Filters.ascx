﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  var pageName = "PrivateDutyScheduleCenter";  %>
<div class="center-filter">
    <label>Branch</label>
    <div class="filter-fill text"><%= Html.BranchList("BranchId", "", (int)AgencyServices.PrivateDuty, true, new { @class = "ScheduleBranchId" })%></div>
</div>
<div class="center-filter patient-filters">
    <label>View</label>
    <div class="filter-fill">
        <select name="PatientStatus" class="ScheduleStatusDropDown">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
</div>
<div class="center-filter user-filters">
    <label>View</label>
    <div class="filter-fill">
        <select name="UserStatus" class="ScheduleStatusDropDown">
            <option value="1">Active Users</option>
            <option value="2">Inactive Users</option>
        </select>
    </div>
</div>
<div class="center-filter patient-filters">
    <label>Filter</label>
    <div class="filter-fill">
        <select name="PaymentSource" class="SchedulePaymentDropDown">
            <option value="0">All</option>
            <option value="1">Medicare (traditional)</option>
            <option value="2">Medicare (HMO/managed care)</option>
            <option value="3">Medicaid (traditional)</option>
            <option value="4">Medicaid (HMO/managed care)</option>
            <option value="5">Workers' compensation</option>
            <option value="6">Title programs</option>
            <option value="7">Other government</option>
            <option value="8">Private</option>
            <option value="9">Private HMO/managed care</option>
            <option value="10">Self Pay</option>
            <option value="11">Unknown</option>
        </select>
    </div>
</div>
<div class="center-filter">
    <label>Find</label>
    <div class="filter-fill text"><input id="txtSearch_Schedule_Selection" class="text" name="SearchText" value="" type="text" /></div>
</div>