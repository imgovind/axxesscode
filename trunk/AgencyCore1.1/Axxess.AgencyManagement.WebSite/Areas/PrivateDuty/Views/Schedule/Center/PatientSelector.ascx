﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSelection>>" %>
<%  Html.Telerik().Grid<PatientSelection>(Model).Name("PrivateDutyScheduleSelectionGrid").HtmlAttributes(new { @class = "patient-selector" }).Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
		columns.Bound(p => p.DisplayNameWithMi).HtmlAttributes(new { style = "font-size:0;" }).Hidden();
        columns.Bound(p => p.Id).HtmlAttributes(new { @class = "id" }).Hidden();
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("CenterPatientGrid", "Schedule", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = 0, name = string.Empty, area = "PrivateDuty" })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("Schedule.Center.PrivateDuty.Selector.Init")
        .OnDataBinding("Schedule.Center.PrivateDuty.Selector.OnDataBinding")
        .OnRowSelect("Schedule.Center.PrivateDuty.Selector.Select")
    ).NoRecordsTemplate("No Patients found.").Render(); %>