﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserTaskViewData>" %>
<span class="wintitle"><%= ((int)Current.AcessibleServices).IsPowerOfTwo() ? string.Empty : "Private Duty " %>Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="main">
            <ul class="buttons continuous ac">
                <li class="selected"><a class="schedule-by patient">Patient</a></li>
                <li><a class="schedule-by user">Employee</a></li>
            </ul>
            <div class="top"><% Html.RenderPartial("Center/Filters"); %></div>
            <div class="bottom  four-filter"><% Html.RenderPartial("Center/PatientSelector", ViewData["Patients"]); %></div>
        </div>
    </div>
    <div class="ui-layout-center">
        <div id="PDScheduleMainResult">
            <ul class="window-menu scheduler-toggle">
                <%if (Model.IsUserCanAdd) { %><li><a class="new-task">New Task</a></li><% } %>
                <%if (Model.IsUserCanAddEpisode || Model.IsUserCanViewEpisodeList) { %><li class="care-period-menu">
                    <a class="menu-trigger">Care Period</a>
                    <ul class="menu">
                        <%if (Model.IsUserCanAddEpisode) { %><li><a class="new-care-period">New Care Period</a></li><% } %>
                        <%if (Model.IsUserCanViewEpisodeList)
                          { %><li><a class="list-care-periods">List Care Periods</a></li><% } %>
                    </ul>
                </li><% } %>
                 <%if (Model.IsUserCanAdd)
                   { %><li><a class="schedule-employee">Schedule Employee</a></li><% } %>
                <%if (Model.IsUserCanReassign) { %><li><a class="reassign-schedule">Reassign Schedules</a></li><% } %>
                <%if (Model.IsUserCanPrint) { %><li><a class="print">Print Calendar</a></li><% } %>
            </ul>
            <div class="pd-wrapper main ac">
                <div class="pd-calendar<%= Model.IsUserCanAddEpisode ? " scheduler" : string.Empty %>"></div>
                <fieldset class="legend">
                    <ul>
                        <li><div class="scheduled">&#160;</div> Scheduled</li>
                        <li><div class="completed">&#160;</div> Completed</li>
                        <li><div class="missed">&#160;</div> Missed</li>
                    </ul>
                </fieldset>
            </div>
            <div class="list-collapsed">
                <a class="show-list">List Tasks</a>
                <a class="hide-list hidden">Hide List</a>
            </div>
            <div class="schedule-list">
              <%  Html.RenderPartial("Center/Grid"); %>
            </div>
        </div>
    </div>
</div>