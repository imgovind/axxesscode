﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserTaskViewData>" %>
<span class="wintitle"><%= ((int)Current.AcessibleServices).IsPowerOfTwo() ? string.Empty : "Private Duty " %>My Scheduled Task | <%= Current.UserFullName.Clean() %></span>
<div class="wrapper layout">
    <ul class="window-menu scheduler-toggle">
     <%if (Model.IsUserCanExport) { %>  <li><a class="export">Excel Export</a></li> <% } %>
     <%if (Model.IsUserCanPrint) { %> <li><a class="print">Print Calendar</a></li> <% } %>
    </ul>
    <span class="user" guid="<%= Current.UserId %>"><%= Current.UserFullName %></span>
    <div class="pd-wrapper main ac">
        <div class="pd-calendar<%= Model.IsUserCanAdd ? " scheduler" : string.Empty %>"></div>
        <fieldset class="legend">
            <ul>
                <li><div class="scheduled">&#160;</div> Scheduled</li>
                <li><div class="completed">&#160;</div> Completed</li>
                <li><div class="missed">&#160;</div> Missed</li>
            </ul>
        </fieldset>
    </div>
    <div class="list-collapsed">
        <a class="show-list">List Tasks</a>
        <a class="hide-list hidden">Hide List</a>
    </div>
    <div class="schedule-list">
        <%  Html.RenderPartial("Center/Grid", new ViewDataDictionary { { "IdPrefix", "MySchedule" } }); %>
    </div>
</div>