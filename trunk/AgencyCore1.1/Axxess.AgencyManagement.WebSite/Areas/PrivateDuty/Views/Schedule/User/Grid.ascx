﻿<%--<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ActivityTaskViewData>>" %>
<%  Html.Telerik().Grid(Model).Name("PrivateDutyUser_Grid").Columns(columns => {
        columns.Bound(s => s.Id).Visible(false).Title("");
        columns.Bound(s => s.Url).Title("Task").Template(t => Html.Link(t.TaskName, t.Url)).Width(160);
        columns.Bound(s => s.Date).Template(t => t.Date.ToString("MM/dd/yyyy h:mm tt")).Title("Scheduled Time").Width(140);
        columns.Bound(s => s.Patient).Width(100);
        columns.Bound(s => s.AssignedTo).Width(100);
        columns.Bound(s => s.Status).Width(100);
        columns.Bound(s => s.StatusComment).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false).Template(t => string.Format("<a class=\"img icon16 red note\">{0}</a>", t.StatusComment));
        columns.Bound(s => s.Comments).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Template(t => string.Format("<a class=\"img icon16 yellow note\">{0}</a>", t.Comments));
        columns.Bound(s => s.PrintUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Template(t => Html.PrintLink(t.PrintUrl));
        columns.Bound(s => s.AttachmentUrl).Title(" ").Width(22).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Template(t => t.AttachmentUrl);
        columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Template(a => a.ActionUrl).Sortable(false).Title("Action").Visible(!Current.IsAgencyFrozen).Width(160);
        columns.Bound(v => v.Id).Template(v => string.Format("<a class=\"link\" onclick=\"MissedVisit.PrivateDuty.New('{0}', '{1}')\">Missed Visit Form</a>", v.PatientId, v.Id)).Title(" ").Width(150).Sortable(false).Visible(!Current.IsAgencyFrozen);    
    })
    .RowAction(row =>
    {
        if (row.DataItem.IsComplete)
        {
            if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed"))
            {
                row.HtmlAttributes["class"] += " completed darkgreen";
            }
            else
            {
                row.HtmlAttributes.Add("class", "completed darkgreen");
            }
        }
        if (row.DataItem.IsOrphaned)
        {
            if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned"))
            {
                row.HtmlAttributes["class"] += " orphaned";
            }
            else
            {
                row.HtmlAttributes.Add("class", "orphaned");
            }
        }
    })
    .Sortable().Scrollable().Footer(false).Render(); %>--%>