﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<div class="window-menu">
    <ul>
        <li><a class="menu-trigger">New Payor</a></li>
        <li><a class="menu-trigger">New Message</a></li>
    </ul>
</div>
<div class="two-thirds">
    <div class="column">
        <span class="bigtext"><%=Model.Name%></span>
    </div>
    <div class="clr"></div>
    <div class="shortrow">
        <label class="fl">Address</label>
<%  if (Model.MainLocation != null) { %>
        <span><%= Model.MainLocation.AddressFirstRow %></span>
        <label class="fl"></label>
        <span><%= Model.MainLocation.AddressSecondRow %></span>
<%  } %>
    </div>
    <div class="clr"></div>
    <div class="shortrow">
        <label class="fl">Phone Number</label>
        <span><%=Model.ContactPersonPhoneFormatted%></span>
    </div>
    <div class="shortrow">
        <label class="fl">Primary Contact</label>
        <span><%=Model.ContactPersonDisplayName %></span>
    </div>
    <div class="shortrow">
        <label class="fl">Email</label>
        <span><a href="mailto:<%=Model.ContactPersonEmail %>" class="link"><%=Model.ContactPersonEmail %></a></span>
    </div>
</div>
<div class="one-third encapsulation">
    <h4>For This Agency</h4>
    <ul>
        <li><a>Patients Reject Report</a></li>
        <li><a>Patients Accept Report</a></li>
        <li><a>Billing Info</a></li>
    </ul>
</div>