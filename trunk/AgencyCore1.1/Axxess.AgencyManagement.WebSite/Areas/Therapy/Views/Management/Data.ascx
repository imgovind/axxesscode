﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyManagementCenterViewData>" %>
<%  if (Model!=null ) { %>
    <div class="top" id="patientTop">
        <%  Html.RenderPartial("~/Areas/Therapy/Views/Management/Info.ascx", Model.Agency); %>
    </div>
    <div class="bottom">
        <%  Html.RenderPartial("~/Areas/Therapy/Views/Management/Bottom.ascx", Model); %>
    </div>
    <%= Html.Hidden("AgencyManagementCenter_AgencyId", Model.Agency.Id) %>
   
<%  } else { %>
    <div class="abs center">No Patient Data Found</div>
<%  } %>