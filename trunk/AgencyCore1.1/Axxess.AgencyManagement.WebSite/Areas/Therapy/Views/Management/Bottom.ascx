﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyManagementCenterViewData>" %>
<div class="abs above activityFilterInputContainer">
    <div class="fl">
        <label>Show</label>
        <select name="Category" id="AgencyManagementCenter_CategoryFilter" class="MsgFilter">
            <option value="0">All</option>
            <option value="1">Referrals</option>
            <option value="2">Messages</option>
            <option value="3">Contacts</option>
        </select>
        <label>Status</label>
        <select name="Status" id="AgencyManagementCenter_StatusFilter" class="MsgFilter">
            <option value="0">New</option>
            <option value="1">Processed</option>
        </select>
    </div>
</div>
<div id="RefferalsGrid">
    <%  Html.RenderPartial("~/Areas/Therapy/Views/Management/GridContent/MsgList.ascx", Model); %>
</div>

<script type="text/javascript">
    $('.MsgFilter').on("change", function() { Agency.MessageTypeChange("<%=Model.Agency.Id %>", $('#AgencyManagementCenter_CategoryFilter').val(), $('#AgencyManagementCenter_StatusFilter').val()); });
</script>