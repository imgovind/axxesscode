﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyManagementCenterViewData>" %>
<span class="wintitle">Agency Management Center | <%= Current.AgencyName %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="row">
                <label>Find</label>
                <div><input id="txtSearch_Agency_Selection" class="text filterInput" name="SearchText" value="" type="text" /></div>
            </div>
            <div class="row">
                <label>State</label>
                <div><%= Html.States("AgencyManagementCenter_StateFilter", string.Empty, new { @id = "AgencyManagementCenter_StateFilter", @class = "address-state state-filter"}) %></div>
            </div>
            <div class="row">
                <label>Status</label>
                <div>
                    <select name="AgencyStatus" id="AgencyManagementCenter_AgencyStatus">
                        <option value="false">Active</option>
                        <option value="true">Deleted</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="bottom"><% Html.RenderPartial("~/Areas/Therapy/Views/Management/Agencies.ascx", Model); %></div>
    </div>
    <div id="AgencyMainResult" class="ui-layout-center">
<%  if (Model != null || Model.SelectedAgencyId != null) { %>
        <%  Html.RenderPartial("/Areas/Therapy/Views/Management/Data.ascx", Model ?? new AgencyManagementCenterViewData()); %>
        <script type="text/javascript">
            Agency.AgencyListFirstDataBound('<%=Model.SelectedAgencyId %>', 'AgencySelectionGrid');
        </script>
<%  } else { %>
        <div class="abs center"><p>No Agency Found.</p></div>
<%  } %>
    </div>
</div>