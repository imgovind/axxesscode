﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyManagementCenterViewData>" %>
<% Html.Telerik().Grid(Model.TherapyEmployers).Name("AgencySelectionGrid").Columns(columns =>
   {
       columns.Bound(p => p.AgencyName).Title("Agency Name").HtmlAttributes(new { @class = "searchN" });
       columns.Bound(p => p.AgencyId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
   })
   //.DataBinding(dataBinding => dataBinding.Ajax().Select("AllSort", "Patient", new { branchId = Guid.Empty, statusId = Model.PatientListStatus, paymentSourceId = 0 }))
   //.ClientEvents(events => events.OnDataBinding("Agency.OnTherapyEmployorDataBinding").OnDataBound("Patient.PatientListDataBound").OnRowSelected("Patient.OnPatientRowSelected"))
   //.Sortable()
   .ClientEvents(events=>events.OnRowSelected("Agency.OnAgencyRowSelected"))
   .DataKeys(keys=>{keys.Add(t=>t.Id).RouteKey("id");})
   .Selectable()
   .Scrollable()
   .Footer(false)
   //.Sortable(sr => sr.OrderBy(so => so.Add(s => s.AgencyName).Ascending()))
   .Render(); %>