﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyManagementCenterViewData>" %>
<%  Html.Telerik().Grid(Model.EbsMessages).Name("ReferralPatientsGrid").Columns(columns => {
        columns.Bound(p => p.Url).Template(s=>Html.Link(s.Title,s.Url)).ClientTemplate("<a onclick=\"<#=Url#>\"><#=Title#></a>").Title("Subject").Width(100);
        columns.Bound(p => p.TypeDisplay).Title("Type").Width(100);
        columns.Bound(p => p.TimeArrived).Width(40);
        columns.Bound(p => p.Id).Width(90).Sortable(false).Template(p => p.Action).ClientTemplate("<#=Action#>").Title("Action");
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AgencyReferralsGrid", "Agency", new { agencyId = Model.Agency.Id, status = 0, type=0, area = "Therapy" })).Sortable().Selectable().Scrollable().Footer(false).Render(); %>