﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyReferralPatient>" %>
<div id="referral-info" class="hidden">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Name</label>
                <div class="fr"><%=Model.DisplayName %></div>
            </div>
            <div class="row">
                <label class="fl">From Agency</label>
                <div class="fr"><%=Model.AgencyName %></div>
            </div>
            <div class="row">
                <label class="fl">Date of birth</label>
                <div class="fr"><%=Model.DOBFormat %></div>
            </div>
            <div class="row">
                <label class="fl">Gender</label>
                <div class="fr"><%=Model.Gender %></div>
            </div>
            <div class="row">
                <label class="fl">Comments</label>
                <div class="fr"><%=Model.Comments %></div>
            </div>
        </div>
    </fieldset>
</div>

