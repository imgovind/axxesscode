﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient</th>
				<th>TOB</th>
				<th>Episode</th>
			</tr>
        </thead>
	    <tbody id="claimsWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div class="widget-more">
    <a onclick="UserInterface.ShowBillingCenterRAP()">More RAPs &#187;</a>
    <a onclick="UserInterface.ShowBillingCenterFinal()">More Finals &#187;</a>
    <a onclick="UserInterface.ShowBillingCenterAllClaim()">All Claims &#187;</a>
</div>