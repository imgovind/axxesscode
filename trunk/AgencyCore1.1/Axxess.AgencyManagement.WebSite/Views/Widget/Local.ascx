﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="digital-clock"></div>
<div id="weather-image"></div>
<div id="weather-info">
    <div class="city"></div>
    <div class="temp"></div>
    <div class="desc"></div>
    <div class="wind"></div>
    <div class="range"></div>
    <div class="link"><a target="_blank">Full forecast</a></div>
</div>
<script type="text/javascript">
    $("#local-widget .widget-head h5").text(U.DisplayDate());
    $.simpleWeather({
        zipcode: '<%= Html.ZipCode() %>',
        unit: 'f',
        success: function(weather) {
            $(".city", "#weather-info").html(weather.city + ", " + weather.region);
            $(".temp", "#weather-info").html(weather.temp + "&#176; " + weather.units.temp);
            $(".desc", "#weather-info").html(weather.currently);
            $(".reange", "#weather-info").html("<strong>High</strong>: " + weather.high + "&#176; " + weather.units.temp + " <strong>Low</strong>: " + weather.low + "&#176; " + weather.units.temp);
            $(".wind", "#weather-info").html("<strong>Wind</strong>: " + weather.wind.direction + " " + weather.wind.speed + " " + weather.units.speed);
            $(".link > a", "#weather-info").attr("href", weather.link);
            $("#weather-image").css("background-image", "url(" + weather.image + ")");
        },
        error: function(error) {
            $("#weather-image").html("<p>" + error + "</p>");
        }
    });
    $('#digital-clock').jdigiclock();
</script>