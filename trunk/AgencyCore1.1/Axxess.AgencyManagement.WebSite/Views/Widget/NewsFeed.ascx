﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ OutputCache Duration="3600" VaryByParam="none" %>
<div id="newsfeed-widget"><%
if (AppSettings.GetRemoteContent.ToBoolean()) {
    try {
        IEnumerable<System.ServiceModel.Syndication.SyndicationItem> items = null;
        using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(UrlSettings.NewsFeedUrl)) {
            if (xmlReader.ReadState != System.Xml.ReadState.Error) {
                System.ServiceModel.Syndication.SyndicationFeed syndicationFeed = System.ServiceModel.Syndication.SyndicationFeed.Load(xmlReader); 
                items = syndicationFeed.Items; 
            }
        } %>
    <ul><%
        int i = 0;
        foreach (var item in items) {
            if (i < 2) { %> 
                <li class="post">
                    <a href="<%= item.Links[0].Uri.AbsoluteUri %>" target="_blank">
                        <span class="title" ><%= item.Title.Text.ToTitleCase() %></span>&#160;-&#160;
                        <span class="date"><%= item.PublishDate.ToString("d") %></span>
                        <br />
                        <span class="summary"><%= System.Text.RegularExpressions.Regex.Replace(item.Summary.Text, @"<(.|\n)*?>", "") %></span>
                    </a>
                </li><%
            }
            i++;
        } %>
    </ul><%   
   } catch (Exception ex) { %>
    <div class="ac"><span>No news or updates found.</span></div><%
   }
} else { %>
    <div class="ac"><span>No news or updates found.</span></div><%
} %>
</div>
<div class="widget-more"><a target="_blank" href="http://axxessweb.com/blog/">More &#187;</a></div>
    
