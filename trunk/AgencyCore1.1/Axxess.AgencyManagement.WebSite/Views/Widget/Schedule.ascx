﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th style="width: 40%;">Patient</th>
				<th style="width: 40%;">Task</th>
				<th style="width: 20%;">Date</th>
			</tr>
        </thead>
	    <tbody id="scheduleWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="userScheduleWidgetMore" class="widget-more"><a>More &#187;</a></div>
