﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient</th>
				<th>MRN</th>
				<th>Due Date</th>
			</tr>
        </thead>
	    <tbody id="recertUpcomingWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div onclick="UserInterface.ShowUpcomingRecerts()" id="upcomingRecertsMore" class="widget-more"><a>More &#187;</a></div>
