﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollDetailsViewData>" %>
<%  if (Model != null && Model.Details != null && Model.Details.Count > 0) { %>
<div class="payroll">
    <ul>
        <li class="ac">
            [ <a href="javascript:void(0);" onclick="$('#payrollSearchResultDetails').hide(); $('#payrollSearchResultDetail').hide(); $('#payrollMarkAsPaidButton').hide();$('#payrollSearchResult').show();">Back to Search Results</a> ]
            <% if (Model.IsUserCanExport) { %>
            [ <%= Html.ActionLink("Export All To Excel", "PayrollDetails", "Export", new { StartDate = Model.StartDate, EndDate = Model.EndDate ,payrollStatus= Model.PayrollStatus }, new { id = "ExportPayrollDetail_ExportLink" })%> ]
            <% } %>
            [ <a href="javascript:void(0);" onclick="U.GetAttachment('Payroll/SummaryDetailsPdf', { 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });">Print PDF</a> ]
        </li>
    </ul>
</div>
    <%  foreach (var detail in Model.Details) Html.RenderPartial("Detail", detail); %>
<%  } %>