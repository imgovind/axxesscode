﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<span class="wintitle">Payroll Summary | <%= Current.AgencyName %></span>
<div id="Payroll_SummaryContent" class="main wrapper ac">
<%  using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
    <fieldset class="grid-controls ac">
		<div class="button fr"><a class="save">Generate</a></div>
		<div class="filter">
			<label for="payrollStartDate">Date Range</label>
			<input type="text" class="date-picker required" id="payrollStartDate" name="payrollStartDate" value="<%= Model.ToShortDateString() %>" />
			<label for="payrollEndDate">&#8211;</label>
			<input type="text" class="date-picker required" id="payrollEndDate" name="payrollEndDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Today.ToShortDateString() %>" />
	   </div>
	   <div class="filter">       
			<label for="payrollStatus">Status</label>
			<select name="payrollStatus" id="payrollStatus">
				<option value="All">All</option>
				<option value="true">Paid</option>
				<option value="false" selected="selected">Unpaid</option>
			</select>
	   </div> 
    </fieldset>
    <div id="payrollSearchResult" class="payroll al"></div>
    <div id="payrollSearchResultDetails" class="al"></div>
    <div id="payrollSearchResultDetail" class="al"></div>
    <%= Html.Hidden("PayrollSearchResultView", string.Empty, new { @id = "markAsPaidButtonId"} ) %>
    <%  if (!Current.IsAgencyFrozen) { %>
    <ul id="payrollMarkAsPaidButton" class="buttons ac">
        <li><a onclick="Payroll.MarkAsPaid()">Mark As Paid</a></li>
        <li><a onclick="Payroll.MarkAsUnpaid()">Mark As Unpaid</a></li>
    </ul>
    <%  } %>
<%  } %>
</div>

