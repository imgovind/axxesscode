﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var signatureDate = Model.SignatureDate.ToDateTimeOrMin(); %>
<%  var date = signatureDate.IsValid() && signatureDate.IsBetween(Model.StartDate, maxDate) ? signatureDate : (Model != null &&  Model.VisitStartDate.IsValid() ? Model.VisitStartDate : Model.EndDate); %>
<fieldset>
    <legend>Electronic Signature</legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_Clinician" class="fl">Signature</label>
            <div class="fr"><%= Html.Password("Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_SignatureDate" class="fl">Date</label>
            <div class="fr"><%= Html.DatePicker("SignatureDate", date, false, new { @class = "complete-required", id = Model.Type + "SignatureDate" })%></div>
        </div>
    </div>
<%  if (Model.FooterSettings != null && Model.FooterSettings.NumberOfSignatures > 1) { %>
	<%  for (int sigNum = 2; sigNum <= Model.FooterSettings.NumberOfSignatures; sigNum++) { %>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_Clinician<%= sigNum %>" class="fl">Signature</label>
			<div class="fr"><%= Html.Password(Model.Type + "_Clinician" + sigNum, "", new { @id = Model.Type + "_Clinician" + sigNum })%></div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_SignatureDate<%= sigNum %>" class="fl">Date</label>
			<div class="fr"><%= Html.DatePicker(Model.Type + "_SignatureDate" + sigNum, data.AnswerValidDateOrEmptyString("SignatureDate" + sigNum), false, new { id = Model.Type + "_SignatureDate" + sigNum })%></div>
		</div>
	</div>
	<%  } %> 
<%  } %>
<%  if (Model.IsUserCanReturn) { %>
    <div class="wide-column">
        <div class="row narrowest">
            <ul class="checkgroup one-wide"><%= Html.CheckgroupOption("ReturnForSignature", Model.Type + "_ReturnForSignature", string.Empty, false, "Return to Clinician for Signature", null) %></ul>
        </div>
    </div>
<%  } %>
</fieldset>
<%  if (Model.FooterSettings != null && (Model.FooterSettings.CanAddSupplies || Model.FooterSettings.CanAddWoundCare || Model.IsUserCanScheduleVisits)) {  %>
<fieldset>
    <legend>Addendum</legend>
	<div class="wide-column">
	    <div class="row">
		    <ul class="buttons ac">
    <%  if (Model.FooterSettings.CanAddWoundCare) { %>
					<li><%= string.Format("<a onclick=\"WoundCare.Load('{0}','{1}','{2}')\" title=\"{3} Wound Care Flowsheet\">{3} Wound Care Flowsheet</a>",Model.PatientId, Model.EventId,Model.Service.ToArea(), Model.IsWoundCareExist ? "Edit" : "Add") %></li>
	<%  } %>
	<%  if (Model.FooterSettings.CanAddSupplies) { %>
					<li><%= string.Format("<a onclick=\"Supply.{0}.LoadWorkSheet('{1}','{2}')\" >{3} Supply Worksheet</a>", Model.Service.ToString(), Model.PatientId, Model.EventId, Model.IsSupplyExist ? "Edit" : "Add")%></li>
	<%  } %>
	<%  if (Model.IsUserCanScheduleVisits){ %>
					<li><a onclick="Schedule.Task.<%=Model.Service.ToString() %>.NewMultiple('<%= Model.EpisodeId %>','<%= Model.PatientId %>')" title="Schedule Supervisory Visit">Schedule Supervisory Visit</a></li>
	<% } %>
	        </ul>
		</div>
	</div>
</fieldset>
<%  } %>
<ul class="buttons ac send-commands">
    <li><a class="save" command="save">Save</a></li>
    <li><a class="save close" command="save">Save &#38; Exit</a></li>
    <li><a class="complete" command="complete">Complete</a></li>
<%  if (Model.IsUserCanApprove){ %>
    <li><a class="complete" command="approve">Approve</a></li>
    <%  if (Model.IsUserCanReturn) { %>
    <li><a command="return">Return</a></li>
    <%  } %>
<%  } %>
    <li><a class="close">Exit</a></li>
</ul>