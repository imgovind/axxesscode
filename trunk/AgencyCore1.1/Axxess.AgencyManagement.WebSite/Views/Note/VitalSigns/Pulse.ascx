﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.Where(v => v.Pulse.HasValue).OrderBy(v => v.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayAPulse = new List<string>(); %>
<%  var arrayRPulse = new List<string>(); %>
<h3 class="collapsable">
    Pulse
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Pulse" style=""></div>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-multi-value">Apical</span>
                <span class="vitals-multi-value">Radial</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.Date.ToString("MM/dd/yyyy h:mm tt") %></span>
                <span class="vitals-multi-value"><% for (int i = 0; i < item.ApicalPulses.Count; i++){
						arrayAPulse.Add("[" + item.Date.ToJavascriptUTC() + ", " + item.ApicalPulses[i].Value + "]"); %>
	                <%= item.ApicalPulses[i].Value + (i + 1 < item.ApicalPulses.Count ? ";" : string.Empty)%> 
	                <% } %></span>
                <span class="vitals-multi-value"><% for (int i = 0; i < item.RadialPulses.Count; i++){
						arrayRPulse.Add("[" + item.Date.ToJavascriptUTC() + ", " + item.RadialPulses[i].Value + "]"); %>
	                <%= item.RadialPulses[i].Value + (i + 1 < item.RadialPulses.Count ? ";" : string.Empty)%> 
	                <% } %></span>
                <span class="vitals-task"><%= item.Task %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No pulses found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
	$("#AcoreGraph_Pulse").Graph({
		Type: "scatter",
		Title: "Pulse",
		XAxisOptions: {
			title: {
				enabled: true,
				text: 'Date'
			},
			type: 'datetime',
			showLastLabel: true,
			dateTimeLabelFormats: {
				day: '%m/%e/%y',
				hour: '%l:%M %P'
			},
			minPadding: .075,
			maxPadding: .075
        },
		ChartOptions: {			
			marginBottom: 45
		},
		YAxisTitle: 'Pulse (bpm)',
        AllowDecimals: false,
		TooltipFormatter: function() {
			return "<b>" + this.series.name + "</b><br/>" + "Pulse: " + this.y + " bpm <br/>Date: " + Highcharts.dateFormat("%m/%e/%Y %l:%M %P", this.x);
		},
		YAxisData: [{
			name: 'Apical',
            color: 'rgba(223, 83, 83, .5)',
			data: [<%= arrayAPulse.ToArray().Join(",") %>]
		}, { name: 'Radial',
            color: 'rgba(119, 152, 191, .5)',
			data: [<%= arrayRPulse.ToArray().Join(",") %>]
		}]
	});
</script>