﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<span class="wintitle">Patient Vital Signs | <%= ViewData.ContainsKey("DisplayName") && ViewData["DisplayName"] != null ? ViewData["DisplayName"].ToString().Clean() : string.Empty %></span>
<%  var pagename = "Patient_VitalSignsCharts"; %>
<div id="<%= pagename %>_Tempurature" class="group"><% Html.RenderPartial("VitalSigns/Tempurature", Model); %></div>
<div id="<%= pagename %>_Weight" class="group"><% Html.RenderPartial("VitalSigns/Weight", Model); %></div>
<div id="<%= pagename %>_Respiratory" class="group"><% Html.RenderPartial("VitalSigns/Respiratory", Model); %></div>
<div id="<%= pagename %>_Pain" class="group"><% Html.RenderPartial("VitalSigns/Pain", Model); %></div>
<div id="<%= pagename %>_Pulse" class="group"><% Html.RenderPartial("VitalSigns/Pulse", Model); %></div>
<div id="<%= pagename %>_BloodSugar" class="group"><% Html.RenderPartial("VitalSigns/BloodSugar", Model); %></div>
<div id="<%= pagename %>_BloodPressure" class="group"><% Html.RenderPartial("VitalSigns/BloodPressure", Model); %></div>
<div id="<%= pagename %>_Report" class="group"><% Html.RenderPartial("VitalSigns/Report", Model); %></div>