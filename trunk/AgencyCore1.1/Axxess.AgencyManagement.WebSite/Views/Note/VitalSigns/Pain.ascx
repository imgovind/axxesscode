﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.Where(v => v.Pain.HasValue).OrderBy(v => v.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayPain = new List<double>(); %>
<%  var arrayPainDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.Pain.HasValue) { %>
        <%  arrayPain.Add(v.Pain.Value); %>
        <%  arrayPainDate.Add(v.Date.ToString("MM/dd/yyyy")); %>
    <%  } %>
<%  }); %>
<%  var painJson = arrayPain.ToJavascrptArray(); %>
<%  var datePainJson = arrayPainDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Pain Level
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Pain" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Pain</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.Date.ToString("MM/dd/yyyy") %></span>
                <span class="vitals-single-value"><%= item.Pain %></span>
                <span class="vitals-task"><%= item.Task %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No pains found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Pain").Graph({
        Type: "line",
        Title: "Pain Level",
        YAxisTitle: "Pain Level",
        XAxisData: <%= datePainJson %>,
        YAxisData: [{ name: "Pain", data: <%= painJson %> }]
    });
</script>