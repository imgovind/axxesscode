﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var pagename = "Patient_VitalSignsCharts"; %>
<%  var patientId = ViewData.ContainsKey("PatientId") ? ViewData["PatientId"] : Guid.Empty; %>
<div class="wrapper main">
    <ul class="fr buttons">
        <li><a class="grid-refresh">Refresh</a></li><br />
        <li class="vitals-report-show"><a class="export">Excel Export</a></li>
    </ul>
    <fieldset class="ac grid-controls">
        <div class="filter">
            <label for="<%= pagename %>_PatientId">Patient</label>
            <%= Html.TextBox("PatientId", ViewData["PatientId"].ToSafeString(), new { @id = pagename + "_PatientId", @class = "patient-picker", @service = "1" }) %>
        </div>
        <ul class="buttons continuous fr">
            <li class="selected"><a class="vitals-graphs">Vital Signs Charts</a></li>
            <li><a class="vitals-report">Vital Signs Log</a></li>
        </ul>
        <div class="filter vitals-report-show">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <input type="text" class="date-picker short" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
            <label for="<%= pagename %>_EndDate">&#8211;</label>
            <input type="text" class="date-picker short" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
        </div>
    </fieldset>
    <div id="<%= pagename %>_ContentId" class="vitals-content"><% Html.RenderPartial("VitalSigns/Graphs", Model); %></div>
</div>

