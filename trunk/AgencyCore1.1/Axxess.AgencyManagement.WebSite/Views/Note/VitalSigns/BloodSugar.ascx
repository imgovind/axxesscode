﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.Where(v => v.BS.HasValue).OrderBy(v => v.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayBSMax = new List<double>(); %>
<%--<%  var arrayBSMin = new List<double>(); %>--%>
<%  var arrayBSDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.BS.HasValue) { %>
        <%  arrayBSMax.Add(v.BS.Value); %>
        <%  arrayBSDate.Add(v.Date.ToString("MM/dd/yyyy")); %>
    <%  } %>
<%--    <%  if (v.BSMin > 0) { %>
        <%  arrayBSMin.Add(v.BSMin); %>
        <%  arrayBSDate.Add(v.Date.ToString("MM/dd/yyyy")); %>
    <%  } %>--%>
<%  }); %>
<%  var bsMaxJson = arrayBSMax.ToJavascrptArray(); %>
<%  var dateBSJson = arrayBSDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Blood Sugar
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_BloodSugar" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Blood Glucose</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.Date.ToString("MM/dd/yyyy")%></span>
                <span class="vitals-multi-value"><%= item.BS %></span>
                <span class="vitals-task"><%= item.Task %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No blood glucoses found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_BloodSugar").Graph({
        Type: "line",
        Title: "Blood Glucose Level",
        YAxisTitle: "Blood Glucose",
        XAxisData: <%= dateBSJson %>,
        YAxisData: [{ name: "BS", data: <% =bsMaxJson %> }]
    });
</script>