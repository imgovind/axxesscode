﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.Date).ToList() : new List<VitalSignLogListItem>(); %>
<h3 class="collapsable">
    Report
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="vitals-report-date">Date</span>
                <span class="grid-vitals-report-employee">Employee Name</span>
                <span class="vitals-report-task">Task</span>
                <span class="vitals-report-value">Min BP</span>
                <span class="vitals-report-value">Max BP</span>
                <span class="vitals-report-value">Temp</span>
                <span class="vitals-report-value-sm">Resp</span>
                <span class="vitals-report-value-sm">Pulse</span>
                <span class="vitals-report-value-sm">BS</span>
                <span class="vitals-report-value">Weight</span>
               <%-- <span class="vitals-report-value-sm">Pain</span>--%>
                <span class="vitals-report-value">O<sub>2</sub> Sat</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-report-date"><%= item.Date.ToString("MM/dd/yy h:mm tt") %></span>
                <span class="grid-vitals-report-employee"><%= item.EmployeeName %></span>
                <span class="vitals-report-task"><%= item.Task %></span>
                <span class="vitals-report-value"><%= item.MinBP %></span>
                <span class="vitals-report-value"><%= item.MaxBP %></span>
                <span class="vitals-report-value"><%= item.Temp %></span>
                <span class="vitals-report-value-sm"><%= item.Resp %></span>
                <span class="vitals-report-value-sm"><%= item.Pulse %></span>
                <span class="vitals-report-value-sm"><%= item.BS %></span>
                <span class="vitals-report-value"><%= item.Weight %> <%= item.WeightUnit %></span>
                <%--<span class="vitals-report-value-sm"><%= item.PainLevel %></span>--%>
                <span class="vitals-report-value"><%= item.OxygenSaturation %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No vital signs found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
