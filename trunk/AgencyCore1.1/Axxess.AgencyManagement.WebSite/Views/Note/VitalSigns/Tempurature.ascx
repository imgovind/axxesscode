﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.OrderBy(v => v.Date.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayTemp = new List<double>(); %>
<%  var arrayTempDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.TempNum > 0) { %>
        <%  arrayTemp.Add(v.TempNum); %>
        <%  arrayTempDate.Add(v.Date.ToString("MM/dd/yyyy")); %>
    <%  } %>
<%  }); %>
<%  var tempJson = arrayTemp.ToJavascrptArray(); %>
<%  var dateTempJson = arrayTempDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Temperature
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Tempurature" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Tempurature</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.Date.ToString("MM/dd/yyyy") %></span>
                <span class="vitals-single-value"><%= item.Temp %></span>
                <span class="vitals-task"><%= item.Task %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No temperatures found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Tempurature").Graph({
        Type: "line",
        Title: "Temperature",
        YAxisTitle: "Temperature",
        YAxisSuffix: "°",
        XAxisData: <%= dateTempJson %>,
        YAxisData: [{ name: "Tempurature", data: <%= tempJson %> }]
    });
</script>