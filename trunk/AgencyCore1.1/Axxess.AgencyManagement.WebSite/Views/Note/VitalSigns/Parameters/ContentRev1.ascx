﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
<fieldset>
	<legend>Vital Sign Parameters</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsVitalSignParameter", Model.Type + "_IsVitalSignParameter", "1", isVitalSignParameter != null && isVitalSignParameter.Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label class="fl">Systolic BP</label>
				<div class="fr">
					<label for="<%= Model.Type %>_SystolicBPGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.AnswerOrEmptyString("SystolicBPGreaterThan"), new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_SystolicBPLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.AnswerOrEmptyString("SystolicBPLessThan"), new { @id = Model.Type + "_SystolicBPLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Diastolic BP</label>
				<div class="fr">
					<label for="<%= Model.Type %>_DiastolicBPGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.AnswerOrEmptyString("DiastolicBPGreaterThan"), new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_DiastolicBPLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.AnswerOrEmptyString("DiastolicBPLessThan"), new { @id = Model.Type + "_DiastolicBPLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Pulse</label>
				<div class="fr">
					<label for="<%= Model.Type %>_PulseGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.AnswerOrEmptyString("PulseGreaterThan"), new { @id = Model.Type + "_PulseGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_PulseLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_PulseLessThan", data.AnswerOrEmptyString("PulseLessThan"), new { @id = Model.Type + "_PulseLessThan", @class = "shorter less" })%>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">Respiration</label>
				<div class="fr">
					<label for="<%= Model.Type %>_RespirationGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.AnswerOrEmptyString("RespirationGreaterThan"), new { @id = Model.Type + "_RespirationGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_RespirationLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_RespirationLessThan", data.AnswerOrEmptyString("RespirationLessThan"), new { @id = Model.Type + "_RespirationLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Temperature</label>
				<div class="fr">
					<label for="<%= Model.Type %>_TempGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_TempGreaterThan", data.AnswerOrEmptyString("TempGreaterThan"), new { @id = Model.Type + "_TempGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_TempLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_TempLessThan", data.AnswerOrEmptyString("TempLessThan"), new { @id = Model.Type + "_TempLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Weight</label>
				<div class="fr">
					<label for="<%= Model.Type %>_WeightGreaterThan">greater than (&#62;)</label>
					<%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.AnswerOrEmptyString("WeightGreaterThan"), new { @id = Model.Type + "_WeightGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_WeightLessThan">or less than (&#60;)</label>
					<%= Html.TextBox(Model.Type + "_WeightLessThan", data.AnswerOrEmptyString("WeightLessThan"), new { @id = Model.Type + "_WeightLessThan", @class = "shorter less" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>