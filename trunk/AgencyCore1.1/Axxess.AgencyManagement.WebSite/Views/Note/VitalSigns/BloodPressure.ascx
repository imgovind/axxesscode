﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.Where(v => v.BloodPressures.Count > 0).OrderBy(v => v.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayBPSystolic = new List<string>(); %>
<%  var arrayBPDiastolic = new List<string>(); %>
<h3 class="collapsable">
    Blood Pressure
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_BloodPressure" class=""></div>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-eighth">Date</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="grid-eighth">Blood Pressure</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var v in sortedVitalSigns) { %>
            <li>
                <span class="grid-eighth"><%= v.Date.ToString("MM/dd/yyyy") %></span>
                <%  v.BloodPressures.ForEach(bp => { %>
					<% arrayBPSystolic.Add("[" + v.Date.ToJavascriptUTC() + ", " + bp.Systolic + "]"); %>
					<% arrayBPDiastolic.Add("[" + v.Date.ToJavascriptUTC() + ", " + bp.Diastolic + "]"); %>
					<span class="grid-eighth"><%= bp.ToString() %></span>
				<%  }); %>
				<% for (int i = 0; i < 6 - v.BloodPressures.Count; i++) {%>
					<span class="grid-eighth"></span>
				<%  } %>
                <span class="vitals-task"><%= v.Task %></span>
            </li>
<%  } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
	$("#AcoreGraph_BloodPressure").Graph({
		Type: "scatter",
		Title: "Blood Pressure",
		XAxisOptions: {
			title: {
				enabled: true,
				text: 'Date'
			},
			type: 'datetime',
			showLastLabel: true,
			dateTimeLabelFormats: {
				day: '%m/%e/%y',
				hour: '%l:%M %P'
			},
			minPadding: .075,
			maxPadding: .075
        },
		YAxisTitle: 'Blood Pressure (mmHg)',
		TooltipFormatter: function() {
			return "<b>" + this.series.name + "</b><br/>" + "Blood Pressure: " +this.y + " mmHg <br/>Date: " + Highcharts.dateFormat("%m/%e/%Y %l:%M %P", this.x);
		},
		ChartOptions: {			
			marginBottom: 45
		},
		YAxisData: [{
			name: 'Systolic',
            color: 'rgba(223, 83, 83, .5)',
			data: [<%= arrayBPSystolic.ToArray().Join(",") %>]
		}, { name: 'Diastolic',
            color: 'rgba(119, 152, 191, .5)',
			data: [<%= arrayBPDiastolic.ToArray().Join(",") %>]
		}]
	});
</script>