﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Vital Sign Ranges</legend>
		<div class="column">
			<div class="row">
				<label class="fl">Systolic BP</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignBPMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBPMin", data.AnswerOrEmptyString("VitalSignBPMin"), new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignBPMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBPMax", data.AnswerOrEmptyString("VitalSignBPMax"), new { @id = Model.Type + "_SystolicBPLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Diastolic BP</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignBPDiaMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBPDiaMin", data.AnswerOrEmptyString("VitalSignBPDiaMin"), new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignBPDiaMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBPDiaMax", data.AnswerOrEmptyString("VitalSignBPDiaMax"), new { @id = Model.Type + "_DiastolicBPLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Pulse</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignHRMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignHRMin", data.AnswerOrEmptyString("VitalSignHRMin"), new { @id = Model.Type + "_PulseGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignHRMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignHRMax", data.AnswerOrEmptyString("VitalSignHRMax"), new { @id = Model.Type + "_PulseLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Respiration</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignRespMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignRespMin", data.AnswerOrEmptyString("VitalSignRespMin"), new { @id = Model.Type + "_RespirationGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignRespMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignRespMax", data.AnswerOrEmptyString("VitalSignRespMax"), new { @id = Model.Type + "_RespirationLessThan", @class = "shorter less" })%>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">Temperature</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignTempMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignTempMin", data.AnswerOrEmptyString("VitalSignTempMin"), new { @id = Model.Type + "_TempGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignTempMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignTempMax", data.AnswerOrEmptyString("VitalSignTempMax"), new { @id = Model.Type + "_TempLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Weight</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignWeightMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignWeightMin", data.AnswerOrEmptyString("VitalSignWeightMin"), new { @id = Model.Type + "_WeightGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignWeightMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignWeightMax", data.AnswerOrEmptyString("VitalSignWeightMax"), new { @id = Model.Type + "_WeightLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Blood Glucose</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignBGMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBGMin", data.AnswerOrEmptyString("VitalSignBGMin"), new { @id = Model.Type + "_WeightGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignBGMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignBGMax", data.AnswerOrEmptyString("VitalSignBGMax"), new { @id = Model.Type + "_WeightLessThan", @class = "shorter less" })%>
				</div>
			</div>
			<div class="row">
				<label class="fl">Pain</label>
				<div class="fr">
					<label for="<%= Model.Type %>_VitalSignPainMin">Min</label>
					<%= Html.TextBox(Model.Type + "_VitalSignPainMin", data.AnswerOrEmptyString("VitalSignPainMin"), new { @id = Model.Type + "_WeightGreaterThan", @class = "shorter less" })%>
					<label for="<%= Model.Type %>_VitalSignPainMax">Max</label>
					<%= Html.TextBox(Model.Type + "_VitalSignPainMax", data.AnswerOrEmptyString("VitalSignPainMax"), new { @id = Model.Type + "_WeightLessThan", @class = "shorter less" })%>
				</div>
			</div>
		</div>
</fieldset>