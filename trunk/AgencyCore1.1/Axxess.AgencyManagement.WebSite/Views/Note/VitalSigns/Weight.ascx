﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSignLogListItem>>" %>
<%  var sortedVitalSigns = Model != null && Model.Count > 0 ? Model.Where(v => v.Weight.HasValue).OrderBy(v => v.Date).Take(9).ToList() : new List<VitalSignLogListItem>(); %>
<%  var arrayWeight = new List<double>(); %>
<%  var arrayWeightDate = new List<string>(); %>
<%  sortedVitalSigns.ForEach(v => { %>
    <%  if (v.Weight > 0) { %>
        <% arrayWeight.Add(v.Weight.Value); %>
        <% arrayWeightDate.Add(v.Date.ToString("MM/dd/yyyy")); %>
    <%  } %>
<%  }); %>
<%  var weightJson = arrayWeight.ToJavascrptArray(); %>
<%  var dateWeightJson = arrayWeightDate.ToJavascrptArray(); %>
<h3 class="collapsable">
    Weight
    <span class="img icon less fr"></span>
</h3>
<fieldset>
    <div id="AcoreGraph_Weight" class="half-grid fl"></div>
    <div class="acore-grid half-grid fr">
        <ul>
            <li>
                <span class="vitals-date">Date</span>
                <span class="vitals-single-value">Weight</span>
                <span class="vitals-task">Task</span>
            </li>
        </ul>
        <ol>
<%  foreach (var item in sortedVitalSigns) { %>
            <li>
                <span class="vitals-date"><%= item.Date.ToString("MM/dd/yyyy") %></span>
                <span class="vitals-single-value"><%= item.Weight %> <%= item.WeightUnit %></span>
                <span class="vitals-task"><%= item.Task %></span>
            </li>
<%  } %>
	<% if(sortedVitalSigns.IsNullOrEmpty()) { %>
		<li class="no-hover"><h4>No weights found.</h4></li>
	<% } %>
        </ol>
    </div>
    <div class="clear"></div>
</fieldset>
<script type="text/javascript">
    $("#AcoreGraph_Weight").Graph({
        Type: "line",
        Title: "Weight",
        YAxisTitle: "Weight",
        YAxisSuffix: "lbs",
        XAxisData: <%= dateWeightJson %>,
        YAxisData: [{ name: "Weight", data: <%= weightJson %> }]
    });
</script>