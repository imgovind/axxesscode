﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Plan Details</legend>
	<div class="column">
		<div class="row">
			<label>Vital Signs</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_VitalSignsTemperature">Temperature</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_VitalSignsTemperature", data.AnswerOrEmptyString("VitalSignsTemperature"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_VitalSignsBloodPressure">Blood Pressure</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_VitalSignsBloodPressure", data.AnswerOrEmptyString("VitalSignsBloodPressure"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_VitalSignsHeartRate">Heart Rate</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_VitalSignsHeartRate", data.AnswerOrEmptyString("VitalSignsHeartRate"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_VitalSignsRespirations">Respirations</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_VitalSignsRespirations", data.AnswerOrEmptyString("VitalSignsRespirations"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_VitalSignsWeight">Weight</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_VitalSignsWeight", data.AnswerOrEmptyString("VitalSignsWeight"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="row">
			<label>Personal Care</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareBedBath">Bed Bath</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareBedBath", data.AnswerOrEmptyString("PersonalCareBedBath"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareAssistWithChairBath">Assist with Chair Bath</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareAssistWithChairBath", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareTubBath">Tub Bath</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareTubBath", data.AnswerOrEmptyString("PersonalCareTubBath"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareShower">Shower</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareShower", data.AnswerOrEmptyString("PersonalCareShower"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareShowerWithChair">Shower w/ Chair</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareShowerWithChair", data.AnswerOrEmptyString("PersonalCareShowerWithChair"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareShampooHair">Shampoo Hair</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareShampooHair", data.AnswerOrEmptyString("PersonalCareShampooHair"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareHairCare">Hair Care/Comb Hair</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareHairCare", data.AnswerOrEmptyString("PersonalCareHairCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareOralCare">Oral Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareOralCare", data.AnswerOrEmptyString("PersonalCareOralCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareSkinCare">Skin Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareSkinCare", data.AnswerOrEmptyString("PersonalCareSkinCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCarePericare">Pericare</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCarePericare", data.AnswerOrEmptyString("PersonalCarePericare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareNailCare">Nail Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareNailCare", data.AnswerOrEmptyString("PersonalCareNailCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareShave">Shave</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareShave", data.AnswerOrEmptyString("PersonalCareShave"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareAssistWithDressing">Assist with Dressing</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareAssistWithDressing", data.AnswerOrEmptyString("PersonalCareAssistWithDressing"), new { @class = "short more" })%>
			</div>
		</div>
		<%  if (Model.DisciplineTask == 75) { %>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_PersonalCareMedicationReminder">Medication Reminder</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_PersonalCareMedicationReminder", data.AnswerOrEmptyString("PersonalCareMedicationReminder"), new { @class = "short more" })%>
			</div>
		</div>
		<%} %>
	</div>
	<div class="column">
		<div class="row">
			<label>Elimination</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationAssistWithBedPan">Assist with Bed Pan/Urinal</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationAssistWithBedPan", data.AnswerOrEmptyString("EliminationAssistWithBedPan"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationAssistBSC">Assist with BSC</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationAssistBSC", data.AnswerOrEmptyString("EliminationAssistBSC"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationIncontinenceCare">Incontinence Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationIncontinenceCare", data.AnswerOrEmptyString("EliminationIncontinenceCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationEmptyDrainageBag">Empty Drainage Bag</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationEmptyDrainageBag", data.AnswerOrEmptyString("EliminationEmptyDrainageBag"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationRecordBowelMovement">Record Bowel Movement</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationRecordBowelMovement", data.AnswerOrEmptyString("EliminationRecordBowelMovement"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_EliminationCatheterCare">Catheter Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_EliminationCatheterCare", data.AnswerOrEmptyString("EliminationCatheterCare"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="row">
			<label>Activity</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityDangleOnSideOfBed">Dangle on Side of Bed</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityDangleOnSideOfBed", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityTurnPosition">Turn &amp; Position</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityTurnPosition", data.AnswerOrEmptyString("ActivityTurnPosition"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityAssistWithTransfer">Assit with Transfer</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityAssistWithTransfer", data.AnswerOrEmptyString("ActivityAssistWithTransfer"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityRangeOfMotion">Range of Motion</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityRangeOfMotion", data.AnswerOrEmptyString("ActivityRangeOfMotion"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityAssistWithAmbulation">Assist with Ambulation</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityAssistWithAmbulation", data.AnswerOrEmptyString("ActivityAssistWithAmbulation"), new { @class = "short more" })%>
			</div>
		</div>
		<%  if (Model.DisciplineTask == 75) { %>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_ActivityEquipmentCare">Equipment Care</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_ActivityEquipmentCare", data.AnswerOrEmptyString("ActivityEquipmentCare"), new { @class = "short more" })%>
			</div>
		</div>
		<%} %>
		<div class="row">
			<label>Household Task</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_HouseholdTaskMakeBed">Make Bed</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_HouseholdTaskMakeBed", data.AnswerOrEmptyString("HouseholdTaskMakeBed"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_HouseholdTaskChangeLinen">Change Linen</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_HouseholdTaskChangeLinen", data.AnswerOrEmptyString("HouseholdTaskChangeLinen"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_HouseholdTaskLightHousekeeping">Light Housekeeping</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_HouseholdTaskLightHousekeeping", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="row">
			<label>Nutrition</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_NutritionMealSetUp">Meal Set-up</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_NutritionMealSetUp", data.AnswerOrEmptyString("NutritionMealSetUp"), new { @class = "short more" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_NutritioAssistWithFeeding">Assist with Feeding</label>
			<div class="fr">
				<%= Html.CarePlanStatusSelect(Model.Type + "_NutritioAssistWithFeeding", data.AnswerOrEmptyString("NutritioAssistWithFeeding"), new { @class = "short more" })%>
			</div>
		</div>
	</div>
</fieldset>
