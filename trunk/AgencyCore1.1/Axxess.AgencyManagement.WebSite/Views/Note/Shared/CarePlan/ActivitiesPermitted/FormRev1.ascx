﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Activities Permitted</legend>
	<% string[] activitiesPermitted = data.AnswerArray("ActivitiesPermitted"); %>
	<input type="hidden" name="<%= Model.Type %>_ActivitiesPermitted" value="" />
	<div class="column">
		<div class="row">
			<div class="checkgroup two-wide">
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "1", activitiesPermitted.Contains("1"), "Complete bed rest")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "2", activitiesPermitted.Contains("2"), "Bed rest with BRP")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "3", activitiesPermitted.Contains("3"), "Up as tolerated")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "4", activitiesPermitted.Contains("4"), "Transfer bed-chair")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "5", activitiesPermitted.Contains("5"), "Exercise prescribed")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "6", activitiesPermitted.Contains("6"), "Partial weight bearing")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "7", activitiesPermitted.Contains("7"), "Independent at home")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "8", activitiesPermitted.Contains("8"), "Crutches")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "9", activitiesPermitted.Contains("9"), "Cane")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "10", activitiesPermitted.Contains("10"), "Wheelchair")%>
				<%= Html.CheckgroupOption(Model.Type + "_ActivitiesPermitted", "11", activitiesPermitted.Contains("11"), "Walker")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_ActivitiesPermitted", "12", activitiesPermitted.Contains("12"), "Other", Model.Type + "_ActivitiesPermittedOther", data.AnswerOrEmptyString("ActivitiesPermittedOther"))%>
			</div>
		</div>
	</div>
</fieldset>