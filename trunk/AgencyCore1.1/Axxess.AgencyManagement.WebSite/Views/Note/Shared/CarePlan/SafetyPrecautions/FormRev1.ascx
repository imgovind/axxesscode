﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Safety Precautions</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup">
				<%string[] safetyMeasure = data.AnswerArray("SafetyMeasures"); %>
				<input type="hidden" name="<%= Model.Type %>_SafetyMeasures" value="" />
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "1", safetyMeasure.Contains("1"), "Anticoagulant Precautions") %>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "2", safetyMeasure.Contains("2"), "Emergency Plan Developed")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "3", safetyMeasure.Contains("3"), "Fall Precautions")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "4", safetyMeasure.Contains("4"), "Keep Pathway Clear")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "5", safetyMeasure.Contains("5"), "Keep Side Rails Up")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "6", safetyMeasure.Contains("6"), "Neutropenic Precautions")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "8", safetyMeasure.Contains("8"), "Proper Position During Meals")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "9", safetyMeasure.Contains("9"), "Safety in ADLs")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "10", safetyMeasure.Contains("10"), "Seizure Precautions")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "11", safetyMeasure.Contains("11"), "Sharps Safety")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "12", safetyMeasure.Contains("12"), "Slow Position Change")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "13", safetyMeasure.Contains("13"), "Standard Precautions/Infection Control")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "14", safetyMeasure.Contains("14"), "Support During Transfer and Ambulation")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "15", safetyMeasure.Contains("15"), "Use of Assistive Devices")%>
				<%= Html.CheckgroupOption(Model.Type + "_SafetyMeasures", "7", safetyMeasure.Contains("7"), "O<sub>2</sub> Precautions")%>
				</div>			
		</div>
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_OtherSafetyMeasuresTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_OtherSafetyMeasures", data.AnswerOrEmptyString("OtherSafetyMeasures"), new { @id = Model.Type + "_OtherSafetyMeasures" })%>
	    	</div>
		</div>
	</div>
</fieldset>
