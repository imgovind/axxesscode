﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Functional Limitations</legend>
	<% string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
	<input name="<%= Model.Type %>_FunctionLimitations" value=" " type="hidden" />
	<div class="column">
		<div class="row">
			<div class="checkgroup two-wide">
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "1", functionLimitations.Contains("1"), "Amputation")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "2", functionLimitations.Contains("2"), "Bowel/Bladder Incontinence")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "3", functionLimitations.Contains("3"), "Contracture")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "4", functionLimitations.Contains("4"), "Hearing")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "5", functionLimitations.Contains("5"), "Paralysis")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "6", functionLimitations.Contains("6"), "Endurance")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "7", functionLimitations.Contains("7"), "Ambulation")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "8", functionLimitations.Contains("8"), "Speech")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "9", functionLimitations.Contains("9"), "Legally Blind")%>
				<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "A", functionLimitations.Contains("A"), "Dyspnea with Minimal Exertion")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_FunctionLimitations", "B", functionLimitations.Contains("B"), "Other", Model.Type + "_FunctionLimitationsOther", data.AnswerOrEmptyString("FunctionLimitationsOther"))%>
			</div>
		</div>
	</div>
</fieldset>