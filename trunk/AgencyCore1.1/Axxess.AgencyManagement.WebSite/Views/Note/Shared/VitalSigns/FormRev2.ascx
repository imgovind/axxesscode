﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isVitalSigns = data.AnswerArray("IsVitalSigns"); %>
<fieldset>
	<legend>Vital Signs</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsVitalSigns", Model.Type + "_IsVitalSigns", "1", isVitalSigns != null && isVitalSigns.Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<% Html.RenderPartial("~/Views/VitalSign/Content.ascx", Model != null && Model.VitalSignLog != null ? Model.VitalSignLog : new VitalSignLog(true)); %>
	</div>
</fieldset>