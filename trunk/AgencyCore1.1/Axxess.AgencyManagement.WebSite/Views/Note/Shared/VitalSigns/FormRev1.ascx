﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] isVitalSigns = data.AnswerArray("IsVitalSigns"); %>
<fieldset>
	<legend>Vital Signs</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsVitalSigns", Model.Type + "_IsVitalSigns", "1", isVitalSigns != null && isVitalSigns.Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
		<%if(DisciplineTaskFactory.AllUAP().Contains(Model.DisciplineTask)) {%>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignSBPVal" class="fl">Systolic BP</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignSBPVal", data.AnswerOrEmptyString("VitalSignSBPVal"), new { @class = "short less" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignDBPVal" class="fl">Diastolic BP</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignDBPVal", data.AnswerOrEmptyString("VitalSignDBPVal"), new { @class = "short less" })%>
				</div>
			</div>
			<%} else {%>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignBPVal" class="fl">Blood Pressure</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignBPVal", data.AnswerOrEmptyString("VitalSignBPVal"), new { @class = "short less" })%>
				</div>
			</div>
			<% } %>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignHRVal" class="fl">Pulse</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignHRVal", data.AnswerOrEmptyString("VitalSignHRVal"), new { @class = "short less" })%>
				</div>
			</div>
			
		</div>
		<div class="column">
		    <div class="row">
				<label for="<%= Model.Type %>_VitalSignTempVal" class="fl">Temperature</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignTempVal", data.AnswerOrEmptyString("VitalSignTempVal"), new { @class = "short less" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignRespVal" class="fl">Respiration</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignRespVal", data.AnswerOrEmptyString("VitalSignRespVal"), new { @class = "short less" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_VitalSignWeightVal" class="fl">Weight</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_VitalSignWeightVal", data.AnswerOrEmptyString("VitalSignWeightVal"), new { @class = "short less" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>