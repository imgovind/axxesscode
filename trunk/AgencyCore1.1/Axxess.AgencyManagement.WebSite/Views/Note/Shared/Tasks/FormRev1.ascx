﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Tasks</legend>
    <div class="column">
        <div class="row">
	        <label>Personal Care</label>
        </div>
        <div class="sub row">
            <label class="fl">Bed Bath</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareBedBath", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "2", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("2"), new { @id = Model.Type + "_PersonalCareBedBath2" })%>
                <label for="<%= Model.Type %>_PersonalCareBedBath2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "1", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("1"), new { @id = Model.Type + "_PersonalCareBedBath1" })%>
                <label for="<%= Model.Type %>_PersonalCareBedBath1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "0", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("0"), new { @id = Model.Type + "_PersonalCareBedBath0" })%>
                <label for="<%= Model.Type %>_PersonalCareBedBath0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with Chair Bath</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareAssistWithChairBath", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "2", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath2" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "1", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath1" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "0", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath0" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Tub Bath</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareTubBath", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "2", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("2"), new { @id = Model.Type + "_PersonalCareTubBath2" })%>
                <label for="<%= Model.Type %>_PersonalCareTubBath2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "1", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("1"), new { @id = Model.Type + "_PersonalCareTubBath1" })%>
                <label for="<%= Model.Type %>_PersonalCareTubBath1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "0", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("0"), new { @id = Model.Type + "_PersonalCareTubBath0" })%>
                <label for="<%= Model.Type %>_PersonalCareTubBath0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Shower</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareShower", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "2", data.AnswerOrEmptyString("PersonalCareShower").Equals("2"), new { @id = Model.Type + "_PersonalCareShower2" })%>
                <label for="<%= Model.Type %>_PersonalCareShower2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "1", data.AnswerOrEmptyString("PersonalCareShower").Equals("1"), new { @id = Model.Type + "_PersonalCareShower1" })%>
                <label for="<%= Model.Type %>_PersonalCareShower1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "0", data.AnswerOrEmptyString("PersonalCareShower").Equals("0"), new { @id = Model.Type + "_PersonalCareShower0" })%>
                <label for="<%= Model.Type %>_PersonalCareShower0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Shower w/Chair</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareShowerWithChair", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "2", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("2"), new { @id = Model.Type + "_PersonalCareShowerWithChair2" })%>
                <label for="<%= Model.Type %>_PersonalCareShowerWithChair2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "1", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("1"), new { @id = Model.Type + "_PersonalCareShowerWithChair1" })%>
                <label for="<%= Model.Type %>_PersonalCareShowerWithChair1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "0", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("0"), new { @id = Model.Type + "_PersonalCareShowerWithChair0" })%>
                <label for="<%= Model.Type %>_PersonalCareShowerWithChair0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Shampoo Hair</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareShampooHair", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "2", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("2"), new { @id = Model.Type + "_PersonalCareShampooHair2" })%>
                <label for="<%= Model.Type %>_PersonalCareShampooHair2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "1", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("1"), new { @id = Model.Type + "_PersonalCareShampooHair1" })%>
                <label for="<%= Model.Type %>_PersonalCareShampooHair1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "0", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("0"), new { @id = Model.Type + "_PersonalCareShampooHair0" })%>
                <label for="<%= Model.Type %>_PersonalCareShampooHair0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Hair Care/Comb Hair</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareHairCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "2", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("2"), new { @id = Model.Type + "_PersonalCareHairCare2" })%>
                <label for="<%= Model.Type %>_PersonalCareHairCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "1", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("1"), new { @id = Model.Type + "_PersonalCareHairCare1" })%>
                <label for="<%= Model.Type %>_PersonalCareHairCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "0", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("0"), new { @id = Model.Type + "_PersonalCareHairCare0" })%>
                <label for="<%= Model.Type %>_PersonalCareHairCare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Oral Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareOralCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "2", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("2"), new { @id = Model.Type + "_PersonalCareOralCare2" })%>
                <label for="<%= Model.Type %>_PersonalCareOralCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "1", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("1"), new { @id = Model.Type + "_PersonalCareOralCare1" })%>
                <label for="<%= Model.Type %>_PersonalCareOralCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "0", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("0"), new { @id = Model.Type + "_PersonalCareOralCare0" })%>
                <label for="<%= Model.Type %>_PersonalCareOralCare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Skin Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareSkinCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "2", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("2"), new { @id = Model.Type + "_PersonalCareSkinCare2" })%>
                <label for="<%= Model.Type %>_PersonalCareSkinCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "1", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("1"), new { @id = Model.Type + "_PersonalCareSkinCare1" })%>
                <label for="<%= Model.Type %>_PersonalCareSkinCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "0", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("0"), new { @id = Model.Type + "_PersonalCareSkinCare0" })%>
                <label for="<%= Model.Type %>_PersonalCareSkinCare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Pericare</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCarePericare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "2", data.AnswerOrEmptyString("PersonalCarePericare").Equals("2"), new { @id = Model.Type + "_PersonalCarePericare2" })%>
                <label for="<%= Model.Type %>_PersonalCarePericare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "1", data.AnswerOrEmptyString("PersonalCarePericare").Equals("1"), new { @id = Model.Type + "_PersonalCarePericare1" })%>
                <label for="<%= Model.Type %>_PersonalCarePericare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "0", data.AnswerOrEmptyString("PersonalCarePericare").Equals("0"), new { @id = Model.Type + "_PersonalCarePericare0" })%>
                <label for="<%= Model.Type %>_PersonalCarePericare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Nail Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareNailCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "2", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("2"), new { @id = Model.Type + "_PersonalCareNailCare2" })%>
                <label for="<%= Model.Type %>_PersonalCareNailCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "1", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("1"), new { @id = Model.Type + "_PersonalCareNailCare1" })%>
                <label for="<%= Model.Type %>_PersonalCareNailCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "0", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("0"), new { @id = Model.Type + "_PersonalCareNailCare0" })%>
                <label for="<%= Model.Type %>_PersonalCareNailCare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Shave</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareShave", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "2", data.AnswerOrEmptyString("PersonalCareShave").Equals("2"), new { @id = Model.Type + "_PersonalCareShave2" })%>
                <label for="<%= Model.Type %>_PersonalCareShave2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "1", data.AnswerOrEmptyString("PersonalCareShave").Equals("1"), new { @id = Model.Type + "_PersonalCareShave1" })%>
                <label for="<%= Model.Type %>_PersonalCareShave1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "0", data.AnswerOrEmptyString("PersonalCareShave").Equals("0"), new { @id = Model.Type + "_PersonalCareShave0" })%>
                <label for="<%= Model.Type %>_PersonalCareShave0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with Dressing</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareAssistWithDressing", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "2", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithDressing2" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithDressing2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "1", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithDressing1" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithDressing1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "0", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithDressing0" })%>
                <label for="<%= Model.Type %>_PersonalCareAssistWithDressing0">N/A</label>
            </div>
        </div>
         <%  if (Model.DisciplineTask == 54) { %>
        <div class="sub row">
            <label class="fl">Medication Reminder</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_PersonalCareMedicationReminder", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "2", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("2"), new { @id = Model.Type + "_PersonalCareMedicationReminder2" })%>
                <label for="<%= Model.Type %>_PersonalCareMedicationReminder2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "1", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("1"), new { @id = Model.Type + "_PersonalCareMedicationReminder1" })%>
                <label for="<%= Model.Type %>_PersonalCareMedicationReminder1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "0", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("0"), new { @id = Model.Type + "_PersonalCareMedicationReminder0" })%>
                <label for="<%= Model.Type %>_PersonalCareMedicationReminder0">N/A</label>
            </div>
        </div>
         <%  } %>
        <div class="row"><label>Nutrition</label></div>
        <div class="sub row">
            <label class="fl">Meal Set-up</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_NutritionMealSetUp", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "2", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("2"), new { @id = Model.Type + "_NutritionMealSetUp2" })%>
                <label for="<%= Model.Type %>_NutritionMealSetUp2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "1", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("1"), new { @id = Model.Type + "_NutritionMealSetUp1" })%>
                <label for="<%= Model.Type %>_NutritionMealSetUp1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "0", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("0"), new { @id = Model.Type + "_NutritionMealSetUp0" })%>
                <label for="<%= Model.Type %>_NutritionMealSetUp0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with Feeding</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_NutritioAssistWithFeeding", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "2", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("2"), new { @id = Model.Type + "_NutritioAssistWithFeeding2" })%>
                <label for="<%= Model.Type %>_NutritioAssistWithFeeding2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "1", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("1"), new { @id = Model.Type + "_NutritioAssistWithFeeding1" })%>
                <label for="<%= Model.Type %>_NutritioAssistWithFeeding1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "0", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("0"), new { @id = Model.Type + "_NutritioAssistWithFeeding0" })%>
                <label for="<%= Model.Type %>_NutritioAssistWithFeeding0">N/A</label>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row"><label>Elimination</label></div>
        <div class="sub row">
            <label class="fl">Assist with Bed Pan/Urinal</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationAssistWithBedPan", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("2"), new { @id = Model.Type + "_EliminationAssistWithBedPan2" })%>
                <label for="<%= Model.Type %>_EliminationAssistWithBedPan2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "1", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("1"), new { @id = Model.Type + "_EliminationAssistWithBedPan1" })%>
                <label for="<%= Model.Type %>_EliminationAssistWithBedPan1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "0", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("0"), new { @id = Model.Type + "_EliminationAssistWithBedPan0" })%>
                <label for="<%= Model.Type %>_EliminationAssistWithBedPan0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with BSC</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationAssistBSC", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "2", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("2"), new { @id = Model.Type + "_EliminationAssistBSC2" })%>
                <label for="<%= Model.Type %>_EliminationAssistBSC2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "1", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("1"), new { @id = Model.Type + "_EliminationAssistBSC1" })%>
                <label for="<%= Model.Type %>_EliminationAssistBSC1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "0", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("0"), new { @id = Model.Type + "_EliminationAssistBSC0" })%>
                <label for="<%= Model.Type %>_EliminationAssistBSC0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Incontinence Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationIncontinenceCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "2", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("2"), new { @id = Model.Type + "_EliminationIncontinenceCare2" })%>
                <label for="<%= Model.Type %>_EliminationIncontinenceCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "1", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("1"), new { @id = Model.Type + "_EliminationIncontinenceCare1" })%>
                <label for="<%= Model.Type %>_EliminationIncontinenceCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "0", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("0"), new { @id = Model.Type + "_EliminationIncontinenceCare0" })%>
                <label for="<%= Model.Type %>_EliminationIncontinenceCare0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Empty Drainage Bag</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationEmptyDrainageBag", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "2", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("2"), new { @id = Model.Type + "_EliminationEmptyDrainageBag2" })%>
                <label for="<%= Model.Type %>_EliminationEmptyDrainageBag2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "1", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("1"), new { @id = Model.Type + "_EliminationEmptyDrainageBag1" })%>
                <label for="<%= Model.Type %>_EliminationEmptyDrainageBag1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "0", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("0"), new { @id = Model.Type + "_EliminationEmptyDrainageBag0" })%>
                <label for="<%= Model.Type %>_EliminationEmptyDrainageBag0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Record Bowel Movement</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationRecordBowelMovement", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "2", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("2"), new { @id = Model.Type + "_EliminationRecordBowelMovement2" })%>
                <label for="<%= Model.Type %>_EliminationRecordBowelMovement2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "1", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("1"), new { @id = Model.Type + "_EliminationRecordBowelMovement1" })%>
                <label for="<%= Model.Type %>_EliminationRecordBowelMovement1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "0", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("0"), new { @id = Model.Type + "_EliminationRecordBowelMovement0" })%>
                <label for="<%= Model.Type %>_EliminationRecordBowelMovement0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Catheter Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_EliminationCatheterCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "2", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("2"), new { @id = Model.Type + "_EliminationCatheterCare2" })%>
                <label for="<%= Model.Type %>_EliminationCatheterCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "1", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("1"), new { @id = Model.Type + "_EliminationCatheterCare1" })%>
                <label for="<%= Model.Type %>_EliminationCatheterCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "0", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("0"), new { @id = Model.Type + "_EliminationCatheterCare0" })%>
                <label for="<%= Model.Type %>_EliminationCatheterCare0">N/A</label>
            </div>
        </div>
        <div class="row">Activity</div>
        <div class="sub row">
            <label class="fl">Dangle on Side of Bed</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityDangleOnSideOfBed", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "2", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("2"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed2" })%>
                <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "1", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("1"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed1" })%>
                <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "0", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("0"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed0" })%>
                <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Turn &#38; Position</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityTurnPosition", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "2", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("2"), new { @id = Model.Type + "_ActivityTurnPosition2" })%>
                <label for="<%= Model.Type %>_ActivityTurnPosition2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "1", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("1"), new { @id = Model.Type + "_ActivityTurnPosition1" })%>
                <label for="<%= Model.Type %>_ActivityTurnPosition1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "0", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("0"), new { @id = Model.Type + "_ActivityTurnPosition0" })%>
                <label for="<%= Model.Type %>_ActivityTurnPosition0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with Transfer</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityAssistWithTransfer", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "2", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithTransfer2" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithTransfer2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "1", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithTransfer1" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithTransfer1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "0", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithTransfer0" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithTransfer0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Assist with Ambulation</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityAssistWithAmbulation", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "2", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithAmbulation2" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithAmbulation2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "1", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithAmbulation1" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithAmbulation1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "0", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithAmbulation0" })%>
                <label for="<%= Model.Type %>_ActivityAssistWithAmbulation0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Range of Motion</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityRangeOfMotion", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "2", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("2"), new { @id = Model.Type + "_ActivityRangeOfMotion2" })%>
                <label for="<%= Model.Type %>_ActivityRangeOfMotion2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "1", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("1"), new { @id = Model.Type + "_ActivityRangeOfMotion1" })%>
                <label for="<%= Model.Type %>_ActivityRangeOfMotion1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "0", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("0"), new { @id = Model.Type + "_ActivityRangeOfMotion0" })%>
                <label for="<%= Model.Type %>_ActivityRangeOfMotion0">N/A</label>
            </div>
        </div>
         <%  if (Model.DisciplineTask == 54)
             { %>
        <div class="sub row">
            <label class="fl">Equipment Care</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_ActivityEquipmentCare", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "2", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("2"), new { @id = Model.Type + "_ActivityEquipmentCare2" })%>
                <label for="<%= Model.Type %>_ActivityEquipmentCare2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "1", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("1"), new { @id = Model.Type + "_ActivityEquipmentCare1" })%>
                <label for="<%= Model.Type %>_ActivityEquipmentCare1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "0", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("0"), new { @id = Model.Type + "_ActivityEquipmentCare0" })%>
                <label for="<%= Model.Type %>_ActivityEquipmentCare0">N/A</label>
            </div>
        </div>
        <% } %>
        <div class="row"><label>Household Task</label></div>
        <div class="sub row">
            <label class="fl">Make Bed</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_HouseholdTaskMakeBed", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "2", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("2"), new { @id = Model.Type + "_HouseholdTaskMakeBed2" })%>
                <label for="<%= Model.Type %>_HouseholdTaskMakeBed2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "1", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("1"), new { @id = Model.Type + "_HouseholdTaskMakeBed1" })%>
                <label for="<%= Model.Type %>_HouseholdTaskMakeBed1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "0", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("0"), new { @id = Model.Type + "_HouseholdTaskMakeBed0" })%>
                <label for="<%= Model.Type %>_HouseholdTaskMakeBed0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Change Linen</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_HouseholdTaskChangeLinen", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "2", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("2"), new { @id = Model.Type + "_HouseholdTaskChangeLinen2" })%>
                <label for="<%= Model.Type %>_HouseholdTaskChangeLinen2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "1", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("1"), new { @id = Model.Type + "_HouseholdTaskChangeLinen1" })%>
                <label for="<%= Model.Type %>_HouseholdTaskChangeLinen1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "0", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("0"), new { @id = Model.Type + "_HouseholdTaskChangeLinen0" })%>
                <label for="<%= Model.Type %>_HouseholdTaskChangeLinen0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Light Housekeeping</label>
            <div class="fr">
                <%= Html.Hidden(Model.Type + "_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "2", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("2"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping2" })%>
                <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping2">Completed</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "1", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("1"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping1" })%>
                <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping1">Refuse</label>
                <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "0", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("0"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping0" })%>
                <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping0">N/A</label>
            </div>
        </div>
        <div class="sub row">
            <label class="fl">Other</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_HouseholdTaskOther", data.AnswerOrEmptyString("HouseholdTaskOther"))%>
            </div>
        </div>
    </div>
</fieldset>