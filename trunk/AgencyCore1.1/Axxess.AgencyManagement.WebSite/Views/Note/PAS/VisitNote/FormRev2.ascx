﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PAS Note | <%= Model.PatientProfile.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "PASVisitForm" })) { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
        <legend>Frequency</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_PASFrequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_PASFrequency", data.AnswerOrEmptyString("PASFrequency"), new { @id = Model.Type + "_PASFrequency", @readonly = "readonly" }) %></div>
            </div>
        </div>
    </fieldset>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("PAS/VisitNote/ContentRev2", Model); %></div>
	 <%= Html.Partial("Bottom/View", Model) %>
<%  } %>
</div>