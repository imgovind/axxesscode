﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%= Html.Partial("VitalSigns/Parameters/ContentRev1", Model) %>
<%= Html.Partial("Shared/CarePlan/SafetyPrecautions/FormRev1")%>
<div class="inline-fieldset two-wide">
	<div>
	    <%= Html.Partial("Shared/CarePlan/FunctionalLimitations/FormRev1")%>
    </div>
	<div>
	    <%= Html.Partial("Shared/CarePlan/ActivitiesPermitted/FormRev1")%>
	</div>
</div>
        <%= Html.Partial("Shared/CarePlan/PlanDetails/FormRev1")%>
<fieldset>
    <legend>Comments</legend>
    <div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_CommentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment" })%>
	    	</div>
		</div>
    </div>
</fieldset>
