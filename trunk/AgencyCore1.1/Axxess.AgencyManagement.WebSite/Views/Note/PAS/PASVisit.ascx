﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PAS Note | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<% var signatureDate = data.AnswerOrMinDate("SignatureDate"); %>
<% var date = signatureDate.IsValid() && signatureDate.IsBetween(Model.StartDate, maxDate) ? signatureDate : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "PASVisitForm" })) {%>
    <%= Html.Partial("NoteTop", Model) %> 
    <fieldset>
        <legend>Details(old verison)</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl">Visit Date</label>
                <div class="fr"><input name="VisitDate" type="text" id ="<%= Model.Type %>_VisitDate" class="date-picker required" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString()%>" mindate="<%= Model.StartDate.ToShortDateString()%>"/></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeIn" class="fl">Time In</label>
                <div class="fr"><%= Html.TextBox("TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = Model.Type + "_TimeIn", @class = "complete-required time-picker" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeOut" class="fl">Time Out</label>
                <div class="fr"><%= Html.TextBox("TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = Model.Type + "_TimeOut", @class = "complete-required time-picker" })%></div>
            </div>
             <div class="row">
                <label for="<%= Model.Type %>_PASFrequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "PASFrequency", data.AnswerOrEmptyString("PASFrequency"), new { @id = Model.Type + "_PASFrequency", @readonly = "readonly" })%></div>
            </div>
             <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
        <div class="column">
    <%  if (Model.IsUserCanPreviousNotes) { %>
            <div class="row">
                <label for="<%= Model.Type %>_PreviousNotes" class="fl">Previous Notes</label>
                <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="<%= Model.Type %>_AssociatedMileage" class="fl">Associated Mileage</label>
               <div class="fr"><%= Html.TextBox("AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : string.Empty, new { @id = Model.Type + "_AssociatedMileage", @class = "decimal" })%></div>
            </div>
             <div class="row">
                <label for="<%= Model.Type %>_Surcharge" class="fl">Surcharge</label>
                <div class="fr"><%= Html.TextBox("Surcharge", data.ContainsKey("Surcharge") ? data["Surcharge"].Answer : string.Empty, new { @id = Model.Type + "_Surcharge", @class = "decimal" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_DNR" class="fl">DNR</label>
                 <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_DNR", data.AnswerOrEmptyString("DNR"))%>
                  </div>
                </div>
           <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
        <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
        <div class="column wide">
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><%= Model.CarePlanOrEvalUrl%></li>
                    </ul>
                </div>
            </div>
        </div>
        <%  } %> 
    </fieldset>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("PAS/PASVisitContent", Model); %></div>
	<%= Html.Partial("NoteBottom", Model) %>
<% } %>
</div>