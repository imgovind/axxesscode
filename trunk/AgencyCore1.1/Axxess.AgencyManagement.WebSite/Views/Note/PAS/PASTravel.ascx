﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PAS Travel Note | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = Model.Type + "_Form" })) {%>
   <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
	<legend>Comments</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_CommentTemplates")%>
                <%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { })%>
            </div>
		</div>
	</div>
</fieldset>
 <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>