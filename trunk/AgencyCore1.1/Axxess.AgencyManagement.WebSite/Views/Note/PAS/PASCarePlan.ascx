﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PAS Care Plan | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "PASCarePlanForm" })) { %>
   <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
      <fieldset>
        <legend>Frequency and Diet</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_PASFrequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_PASFrequency", data.AnswerOrEmptyString("PASFrequency"), new { @id = Model.Type + "_PASFrequency" })%></div>
            </div>
            </div>
            <div class="column">
            <div class="row">
            <div class="checkgroup two-wide">
					<div class="option">
						<div class="wrapper">
						    <% string[] diet = data.AnswerArray("IsDiet"); %>
							<%= string.Format("<input class='radio' id='{0}_IsDiet' name='{0}_IsDiet' value='1' type='checkbox' {1} />", Model.Type, diet.Contains("1").ToChecked()) %>
							<label for="<%= Model.Type %>_IsDiet">Diet</label>
							<input name="<%= Model.Type %>_IsDiet" value="" type="hidden" />
						</div>
						<div class="more ac">
							<%= Html.TextBox(Model.Type + "_Diet", data.AnswerOrEmptyString("Diet"), new { @id = Model.Type + "_Diet" })%>
						</div>
					</div>
                </div>
             </div>
            </div>
       </fieldset>
      
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("PAS/PASCarePlanContent", Model); %></div>
    <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>