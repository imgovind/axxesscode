﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NoteSupplyNewViewData>" %>
<%  var service = Model.Service.ToString(); %>
<%  var area = Model.Service.ToArea(); %>
<%  var currentSuppliesGrid = string.Format("{0}_SupplyGrid", service); %>
<div class="wrapper main blue">
	<div class="button fr"><a class="add-supply" url="<%= area%>/Note/NewNoteSupply">Add Supply</a></div>
	<div class="clr"></div>
	<div id="<%=service %>_SupplyGridContent">
		<%= Html.Hidden("EventId", Model.EventId, new { @id = "NoteSupply_EventId", @class = "input" })%>
		<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "NoteSupply_PatientId", @class = "input" })%>
		<%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = "NoteSupply_ServiceId", @class = "input" })%>
		<%  Html.Telerik().Grid<Supply>().Name(currentSuppliesGrid).HtmlAttributes(new { @class = "note-supply-grid", style = "top: 51px;" }).Columns(columns => {
				columns.Bound(s => s.Description);
				columns.Bound(s => s.Quantity).Width(105);
				columns.Bound(e => e.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(105);
				columns.Bound(t => t.UniqueIdentifier).ClientTemplate("<a class=\"edit\">Edit</a> | <a class=\"delete\">Delete</a>").Title("Action").Sortable(false).Width(100).HtmlAttributes(new { @class = "action" }).Visible(!Current.IsAgencyFrozen);
				columns.Bound(s => s.Code).Hidden(true);
				columns.Bound(s => s.UnitCost).Hidden(true);
				columns.Bound(s => s.UniqueIdentifier).Hidden(true).HtmlAttributes(new { @class = "uid" });
			}).DataBinding(dataBinding => dataBinding.Ajax().Select("GetNoteSupply", "Note", new { area = area, patientId = Model.PatientId, Id = Model.EventId }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
                .OnDataBound("Visit.Supply.OnDataBound")
                .OnDataBinding("U.OnTGridDataBinding")
                .OnError("U.OnTGridError")
            ).NoRecordsTemplate("No Supplies found.").Sortable().Scrollable().Footer(false).Render(); %>
	</div>
</div>