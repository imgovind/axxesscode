﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NoteSupplyEditViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateNoteSupply", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "editNoteSupplyForm", @class = "mainform" })) { %>
    <%= Html.Hidden("EventId", Model.EventId)%>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("UniqueIdentifier", Model.Supply.UniqueIdentifier)%>
    <%= Html.Hidden("OldUniqueIdentifier", Model.Supply.OldUniqueIdentifier)%>
    <%= Html.Hidden("Code", Model.Supply.Code)%>
    <%= Html.Hidden("UnitCost", Model.Supply.UnitCost)%>
    <%= Html.Hidden("RevenueCode", string.Empty)%>
    <fieldset>
        <legend>Edit Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Supply.Description, new { @class = "required long" })%></div>
            </div>
            <div class="row narrower">
                <label class="fl">Date</label>
                <div class="fr"><%= Html.DatePicker("DateForEdit", Model.Supply.DateForEdit, true, new { })%></div>
            </div>
            <div class="row narrower">
                <label class="fl">Quantity</label>
                <div class="fr"><%= Html.TextBox("Quantity", Model.Supply.Quantity, new { @class = "shorter required numeric" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
