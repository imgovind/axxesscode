﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">UAP Wound Care Note |
	<%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
	<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" }))
	 { %>
	<%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
	<div id="<%= Model.Type %>_ContentId">
		<% Html.RenderPartial("UAP/" + Model.Type + "Content", Model); %></div>
	  <%= Html.Partial("Bottom/View", Model) %>
	<% } %>
</div>
