﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Unlicensed Assistive Person Prep-Administration of Insulin Note | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
	<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "UAPInsulinPrepAdminVisitForm" })) { %>
	 <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
	<fieldset>
		<legend>UAP Visit Information</legend>
		<div class="column">
			
			<div class="row">
				<div class="checkgroup one-wide">
					<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_FootCheck", "1", data.AnswerOrEmptyString("FootCheck") == "1", "Foot Check")%>
				</div>
			</div>
		</div>
		<div class="column">
			
			<div class="row">
				<label for="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" class="fl">Last BM Date</label>
				<div class="fr"><input type="text" class="date-picker required" id="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" name="UAPInsulinPrepAdminVisit_GenericDigestiveLastBMDate" /></div>
			</div>
		</div>
	</fieldset>
	<div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("UAP/" + Model.Type + "Content", Model); %></div>
	<%= Html.Partial("Bottom/View", Model) %>
	<% } %>
</div>
