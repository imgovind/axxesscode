﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("VitalSigns/Parameters/ContentRev1", Model); %>
<% Html.RenderPartial("Shared/VitalSigns/FormRev1", Model); %>
<fieldset>
    <legend>Wound Care (Aseptic Technique and Sterile Supplies)</legend>
    <div class="column">
        <label>Cleansed With</label>
        <%  string[] cleansedWith = data.AnswerArray("CleansedWith"); %>
        <input name="<%= Model.Type %>_CleansedWith" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup three-wide">
                <%= Html.CheckgroupOption(Model.Type + "_CleansedWith", "1", cleansedWith.Contains("1"), "Sterile NS")%>
                <%= Html.CheckgroupOption(Model.Type + "_CleansedWith", "2", cleansedWith.Contains("2"), "Shur Clens")%>
                <%= Html.CheckgroupOption(Model.Type + "_CleansedWith", "3", cleansedWith.Contains("3"), "Wound Cleanser")%>
            </div>
        </div>
        <%  string[] rinsedWith = data.AnswerArray("RinsedWith"); %>
        <input name="<%= Model.Type %>_RinsedWith" value=" " type="hidden" />
        <label class="fl">Rinsed With:</label>
        <div class="row">
            <div class="checkgroup three-wide">
                <%= Html.CheckgroupOption(Model.Type + "_RinsedWith", "1", rinsedWith.Contains("1"), "Sterile NS")%>
                <%= Html.CheckgroupOption(Model.Type + "_RinsedWith", "2", rinsedWith.Contains("2"), "Sterile H20")%>
            </div>
        </div>
        <%  string[] patDryWith = data.AnswerArray("PatDryWith"); %>
        <input name="<%= Model.Type %>_PatDryWith" value=" " type="hidden" />
        <label class="fl">Pat Dry With:</label>
        <div class="row">
            <div class="checkgroup there-wide">
                <%= Html.CheckgroupOption(Model.Type + "_PatDryWith", "1", patDryWith.Contains("1"), "Sterile 4x4")%>
                <%= Html.CheckgroupOption(Model.Type + "_PatDryWith", "2", patDryWith.Contains("2"), "Sterile 2x2")%>
            </div>
        </div>
        <label>Medication Applied</label>
        <%  string[] medicationApplied = data.AnswerArray("MedicationApplied"); %>
        <input name="<%= Model.Type %>_MedicationApplied" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "1", medicationApplied.Contains("1"), "No Medication Applied")%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "2", medicationApplied.Contains("2"), "Triple Antibiotic Ointment")%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "3", medicationApplied.Contains("3"), "Bactroban")%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "4", medicationApplied.Contains("4"), "Bacitracin")%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "5", medicationApplied.Contains("5"), "Neosporin")%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationApplied", "6", medicationApplied.Contains("6"), "Wound Gel")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_MedicationApplied", "7", medicationApplied.Contains("7"), "Other", Model.Type + "_MedicationAppliedOther", data.AnswerOrEmptyString("MedicationAppliedOther"))%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label>Dressing Applied</label>
            <%  string[] dressingApplied = data.AnswerArray("DressingApplied"); %>
            <input name="<%= Model.Type %>_DressingApplied" value=" " type="hidden" />
            <div class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "1", dressingApplied.Contains("1"), "Telfa")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "2", dressingApplied.Contains("2"), "Dry 4x4 Gauze")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "3", dressingApplied.Contains("3"), "Dry 2x2 Gauze")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "4", dressingApplied.Contains("4"), "Wet to Dry with NS")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "5", dressingApplied.Contains("5"), "Fluffs")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "6", dressingApplied.Contains("6"), "Kerlix")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "7", dressingApplied.Contains("7"), "Kling")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "8", dressingApplied.Contains("8"), "Duoderm")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingApplied", "9", dressingApplied.Contains("9"), "Ace Wrap")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_DressingApplied", "10", dressingApplied.Contains("10"), "Other", Model.Type + "_DressingAppliedOther", data.AnswerOrEmptyString("DressingAppliedOther"))%>
            </div>
        </div>
        <label>Dressing Secured With</label>
        <%  string[] dressingSecured = data.AnswerArray("DressingSecured"); %>
        <input name="<%= Model.Type %>_DressingSecured" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup three-wide">
                <%= Html.CheckgroupOption(Model.Type + "_DressingSecured", "1", dressingSecured.Contains("1"), "Micropore tape")%>
                <%= Html.CheckgroupOption(Model.Type + "_DressingSecured", "2", dressingSecured.Contains("2"), "Medifix")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_DressingSecured", "3", dressingSecured.Contains("3"), "Other", Model.Type + "_DressingSecuredOther", data.AnswerOrEmptyString("DressingSecuredOther"))%>
            </div>
        </div>
        <label>Patient Unable to Perform Treatment due to</label>
        <%  string[] patientUnable = data.AnswerArray("PatientUnable"); %>
        <input name="<%= Model.Type %>_DressingSecured" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_PatientUnable", "1", patientUnable.Contains("1"), "Unable to Visualize Site")%>
                <%= Html.CheckgroupOption(Model.Type + "_PatientUnable", "2", patientUnable.Contains("2"), "Poor Manual Dexterity")%>
                <%= Html.CheckgroupOption(Model.Type + "_PatientUnable", "3", patientUnable.Contains("3"), "Unable to Reach Site Due to Limited Functional Mobility")%>
                <%= Html.CheckgroupOption(Model.Type + "_PatientUnable", "4", patientUnable.Contains("4"), "No Caregiver willing/available to assist")%>
                <%= Html.CheckgroupOption(Model.Type + "_PatientUnable", "5", patientUnable.Contains("5"), "Standard Universal Precautions Maintained")%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>RN Notification</legend>
    <div class="column">
        <%  string[] clinicianNotified = data.AnswerArray("ClinicianNotified"); %>
        <input name="<%= Model.Type %>_ClinicianNotified" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_ClinicianNotified", "Yes", clinicianNotified.Contains("Yes"), "RN Notified")%>
            </div>
        </div>
        <div class="row fl">
            <label>RN Name</label>
            <%= Html.Clinicians(Model.Type + "_ClinicianId", data.AnswerOrEmptyString("ClinicianId"), new { @id = Model.Type + "_ClinicianId", @class = "tall" })%>
        </div>
        <div class="row">
            <label>Notified Date</label>
            <input type="text" class="date-picker" name="<%= Model.Type %>_ClinicianNotifiedDate" value="<%= data.AnswerOrEmptyString("ClinicianNotifiedDate") %>" id="<%= Model.Type %>_ClinicianNotifiedDate" />
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_InstructionsGiven">Instructions Given</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_InstructionsGivenTemplates")%>
                <%= Html.TextArea(Model.Type + "_InstructionsGiven", data.ContainsKey("InstructionsGiven") ? data["InstructionsGiven"].Answer : string.Empty, new { @id = Model.Type + "_InstructionsGiven", @class = "tall" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>MD Notification</legend>
    <div class="wide-column">
        <%  string[] doctorNotified = data.AnswerArray("DoctorNotified"); %>
        <input name="<%= Model.Type %>_DoctorNotified" value=" " type="hidden" />
        <%  string[] newOrders = data.AnswerArray("NewOrders"); %>
        <input name="<%= Model.Type %>_NewOrders" value=" " type="hidden" />
        <div class="row">
            <div class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_DoctorNotified", "Yes", doctorNotified.Contains("Yes"), "MD Notified")%>
                <%= Html.CheckgroupOption(Model.Type + "_NewOrders", "Yes", newOrders.Contains("Yes"), "New Order(s)")%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Supplies</legend>
    <div class="wide-column">
        <%  string[] supplies = data.AnswerArray("Supplies"); %>
        <input name="<%= Model.Type %>_Supplies" value=" " type="hidden" />
        <div class="row ">
            <ul class="checkgroup six-wide">
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "1", supplies.Contains("1"), "Gloves")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "2", supplies.Contains("2"), "N/S")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "3", supplies.Contains("3"), "Wound Cleanser")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "4", supplies.Contains("4"), "4x4s")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "5", supplies.Contains("5"), "2x2s")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "6", supplies.Contains("6"), "CTA")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "7", supplies.Contains("7"), "Kerlix")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "8", supplies.Contains("8"), "Tape")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "9", supplies.Contains("9"), "Probe Covers")%>
                <%= Html.CheckgroupOption(Model.Type + "_Supplies", "10", supplies.Contains("10"), "Alcohol pads")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_Supplies", "11", supplies.Contains("11"), "Other", Model.Type + "_SuppliesOther", data.AnswerOrEmptyString("SuppliesOther"))%>
            </ul>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Comments</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_CommentTemplates")%>
                <%= Html.TextArea(Model.Type + "_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = Model.Type + "_Comment", @class = "tall" })%>
            </div>
        </div>
    </div>
</fieldset>