﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  Html.RenderPartial("VitalSigns/Parameters/ContentRev1", Model); %>
<%  Html.RenderPartial("Shared/VitalSigns/FormRev1", Model); %>
<fieldset>
	<legend>Blood Sugar Check</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup two-wide">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='UAPInsulinPrepAdminVisit_FFBS' name='UAPInsulinPrepAdminVisit_FFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FFBS").Equals("1").ToChecked())%>
						<label for="UAPInsulinPrepAdminVisit_FFBS">FFBS</label>
					</div>
					<div class="more">
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSDigit", data.AnswerOrEmptyString("FFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_FFBSDigit", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_FFBSDigit">digit</label>
						<div class="clr"></div>
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSHandGlucometer", data.AnswerOrEmptyString("FFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_FFBSHandGlucometer", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_FFBSHandGlucometer">hand via glucometer</label>
						<div class="clr"></div>
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSMgdl", data.AnswerOrEmptyString("FFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_FFBSMgdl", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_FFBSmgdl">mg/dl</label>
						<div class="clr"></div>
						<ul class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_FFBSPressureApplied", "1", data.AnswerOrEmptyString("FFBSPressureApplied").IsEqual("1"), "Pressure Applied")%>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='UAPInsulinPrepAdminVisit_NFFBS' name='UAPInsulinPrepAdminVisit_NFFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("NFFBS").Equals("1").ToChecked())%>
						<label for="UAPInsulinPrepAdminVisit_NFFBS">NFFBS</label>
					</div>
					<div class="more">
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSDigit", data.AnswerOrEmptyString("NFFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSDigit", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_NFFBSDigit">digit</label>
						<div class="clr"></div>
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", data.AnswerOrEmptyString("NFFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_NFFBSHandGlucometer">hand via glucometer</label>
						<div class="clr"></div>
						<%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSMgdl", data.AnswerOrEmptyString("NFFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSMgdl", @class = "shorter", @maxlength = "10" })%>
						<label for="UAPInsulinPrepAdminVisit_NFFBSmgdl">mg/dl</label>
						<div class="clr"></div>
						<ul class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_NFFBSPressureApplied", "1", data.AnswerOrEmptyString("NFFBSPressureApplied").IsEqual("1"), "Pressure Applied")%>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Tolerated</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<%  string[] bsTolerated = data.AnswerArray("BSTolerated"); %>
					<%= Html.CheckgroupRadioOption(Model.Type + "_BSTolerated", "1", bsTolerated.Contains("1"), "Well")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_BSTolerated", "2", bsTolerated.Contains("2"), "Poor")%>
				</ul>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="UAPInsulinPrepAdminVisit_LastMeal" class="fl">Last Meal</label>
			<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_LastMeal", data.AnswerOrEmptyString("LastMeal"), new { @id = "UAPInsulinPrepAdminVisit_LastMeal" })%></div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Insulin Administration</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup two-wide">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTech1' name='UAPInsulinPrepAdminVisit_AsepticTech' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTech").IsEqual("1") ? "checked=checked" : string.Empty)%>
						<label for="UAPInsulinPrepAdminVisit_AsepticTech1">Aseptic Tech.</label>
					</div>
					<div class="more">
						<label for="UAPInsulinPrepAdminVisit_AsepticTechType" class="fl">Type</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechType", data.AnswerOrEmptyString("AsepticTechType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechType", @maxlength = "30" })%></div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechAmount" class="fl">Amount</label>
						<div class="fr">
							<%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechAmount", data.AnswerOrEmptyString("AsepticTechAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechAmount", @maxlength = "5" })%>
							units
						</div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSite" class="fl">Site</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSite", data.AnswerOrEmptyString("AsepticTechSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSite" })%></div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechTime" class="fl">Time</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechTime", data.AnswerOrEmptyString("AsepticTechTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechTime", @maxlength = "10" })%></div>
						<div class="clr"></div>
						<ul class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_AsepticTechRoute", "1", data.AnswerOrEmptyString("AsepticTechRoute").IsEqual("1"), "Subcut. Route")%>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechSlide1' name='UAPInsulinPrepAdminVisit_AsepticTechSlide' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechSlide").IsEqual("1") ? "checked=checked" : string.Empty)%>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSlide1">Aseptic Tech.</label>
					</div>
					<div class="more">
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSlideType" class="fl">Per Sliding Scale</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideType", data.AnswerOrEmptyString("AsepticTechSlideType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideType", @maxlength = "30" })%></div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSlideAmount" class="fl">Amount</label>
						<div class="fr">
							<%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", data.AnswerOrEmptyString("AsepticTechSlideAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", @maxlength = "5" })%>
							units
						</div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSlideSite" class="fl">Site</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideSite", data.AnswerOrEmptyString("AsepticTechSlideSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideSite" })%></div>
						<div class="clr"></div>
						<label for="UAPInsulinPrepAdminVisit_AsepticTechSlideTime" class="fl">Time</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideTime", data.AnswerOrEmptyString("AsepticTechSlideTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideTime", @maxlength = "10" })%></div>
						<div class="clr"></div>
						<ul class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_AsepticTechSlideRoute", "1", data.AnswerOrEmptyString("AsepticTechSlideRoute").IsEqual("1"), "Subcut. Route")%>
						</ul>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label>Tolerated</label>
			<%  string[] insulinTolerated = data.AnswerArray("InsulinTolerated"); %>
			<ul class="checkgroup three-wide">
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_InsulinTolerated", "1", insulinTolerated.Contains("1"), "Well") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_InsulinTolerated", "2", insulinTolerated.Contains("2"), "Poor") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_InsulinTolerated", "3", insulinTolerated.Contains("3"), "Standard Precautions Maintained") %>
			</ul>
		</div>
		<div class="row">
			<label>P/A Due To</label>
			<%  string[] paDueTo = data.AnswerArray("PADueTo"); %>
			<ul class="checkgroup three-wide">
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_PADueTo", "1", paDueTo.Contains("1"), "Poor Vision") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_PADueTo", "2", paDueTo.Contains("2"), "Poor Manual Dexterity") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_PADueTo", "3", paDueTo.Contains("3"), "Fear") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_PADueTo", "4", paDueTo.Contains("4"), "Cognition") %>
				<%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_PADueTo", "5", paDueTo.Contains("5"), "No Willing Care Giver") %>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo6' name='UAPInsulinPrepAdminVisit_PADueTo' value='6' type='checkbox' {0} />", paDueTo.Contains("6").ToChecked())%>
						<label for="UAPInsulinPrepAdminVisit_PADueTo6">Other</label>
					</div>
					<div class="more">
						<label class="fl" for="UAPInsulinPrepAdminVisit_PADueToOther">Specify</label>
						<div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_PADueToOther", data.AnswerOrEmptyString("PADueToOther"), new { @id = "UAPInsulinPrepAdminVisit_PADueToOther" })%></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>RN Notification</legend>
			<div class="column">
				<div class="row">
					<ul class="checkgroup one-wide">
						<li class="option">
							<div class="wrapper">
								<%= string.Format("<input id='UAPInsulinPrepAdminVisit_ClinicianNotified' name='UAPInsulinPrepAdminVisit_ClinicianNotified' value='Yes' type='checkbox' {0} />", data.ContainsKey("ClinicianNotified") && data["ClinicianNotified"] != null && data["ClinicianNotified"].Answer.IsNotNullOrEmpty() && data["ClinicianNotified"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%>
								<label for="UAPInsulinPrepAdminVisit_ClinicianNotified">RN Notified</label>
							</div>
							<div class="more">
								<label for="UAPInsulinPrepAdminVisit_ClinicianId" class="fl">Clinician</label>
								<div class="fr"><%= Html.Clinicians("UAPInsulinPrepAdminVisit_ClinicianId", data.AnswerOrEmptyString("ClinicianId"), new { @id = "UAPInsulinPrepAdminVisit_ClinicianId" })%></div>
								<div class="clr"></div>
								<label for="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" class="fl">Notified Date</label>
								<div class="fr"><input type="text" class="date-picker" name="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" value="<%= data.ContainsKey("ClinicianNotifiedDate") && data["ClinicianNotifiedDate"] != null && data["ClinicianNotifiedDate"].Answer.IsNotNullOrEmpty() && data["ClinicianNotifiedDate"].Answer.IsValidDate() ? data["ClinicianNotifiedDate"].Answer : string.Empty %>" id="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" /></div>
								<div class="clr"></div>
							</div>
						</li>
					</ul>
				</div>
				<div class="row">
					<label for="UAPInsulinPrepAdminVisit_InstructionsGiven">Instructions Given</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_InstructionsGivenTemplates")%>
						<%= Html.TextArea(Model.Type + "_InstructionsGiven", data.AnswerOrEmptyString("InstructionsGiven"), new { @id = Model.Type + "UAPInsulinPrepAdminVisit_InstructionsGiven" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>MD Notification</legend>
			<div class="column">
				<div class="row">
					<ul class="checkgroup two-wide">
						<%= Html.CheckgroupOption(Model.Type + "_DoctorNotified", "Yes", data.AnswerOrEmptyString("DoctorNotified").IsEqual("Yes"), "MD Notified")%>
						<%= Html.CheckgroupOption(Model.Type + "_NewOrders", "Yes", data.AnswerOrEmptyString("NewOrders").IsEqual("Yes"), "New Order(s)")%>
					</ul>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Supplies</legend>
			<div class="column">
				<div class="row">
					<%  string[] supplies = data.AnswerArray("Supplies"); %>
					<ul class="checkgroup two-wide">
					    <%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_Supplies", "1", supplies.Contains("1"), "NS Gloves") %>
					    <%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_Supplies", "2", supplies.Contains("2"), "Alcohol Pads") %>
					    <%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_Supplies", "3", supplies.Contains("3"), "Probe Covers") %>
					    <%= Html.CheckgroupOption("UAPInsulinPrepAdminVisit_Supplies", "4", supplies.Contains("4"), "Insulin Syringe") %>
						<li class="option">
							<div class="wrapper">
								<%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies5' name='UAPInsulinPrepAdminVisit_Supplies' value='5' type='checkbox' {0} />", supplies.Contains("5").ToChecked())%>
								<label for="UAPInsulinPrepAdminVisit_Supplies5">Other</label>
							</div>
							<div class="more">
								<label class="fl">Specify</label>
								<%= Html.TextBox("UAPInsulinPrepAdminVisit_SuppliesOther", data.AnswerOrEmptyString("SuppliesOther"), new { @id = "UAPInsulinPrepAdminVisit_SuppliesOther", @class = "fr" })%>
								<div class="clr"></div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Comments</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates("UAPInsulinPrepAdminVisit_CommentTemplates")%>
	    		<%= Html.TextArea("UAPInsulinPrepAdminVisit_Comment", data.AnswerOrEmptyString("Comment"), new { @id = "UAPInsulinPrepAdminVisit_Comment" })%>
	    	</div>
		</div>
	</div>
</fieldset>