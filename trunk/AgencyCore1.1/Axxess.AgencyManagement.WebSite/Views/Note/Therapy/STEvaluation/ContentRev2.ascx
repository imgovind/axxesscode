﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Homebound Reason</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup two-wide">
                <input type="hidden" name="<%= Model.Type %>_GenericHomeboundReason" value="" />
                <% string[] genericHomeboundReason = data.AnswerArray("GenericHomeboundReason"); %>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "1", genericHomeboundReason.Contains("1"), "Needs assitance for all activites.") %>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "2", genericHomeboundReason.Contains("2"), "Residual weakness.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "3", genericHomeboundReason.Contains("3"), "Requires assistance to ambulate.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "4", genericHomeboundReason.Contains("4"), "Confusion, unable to go out of home alone.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "5", genericHomeboundReason.Contains("5"), "Unable to safely leave home unassisted.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "6", genericHomeboundReason.Contains("6"), "Severe SOB, SOB upon exertion.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "7", genericHomeboundReason.Contains("7"), "Unable to safely leave home unassisted.")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundReason", "8", genericHomeboundReason.Contains("8"), "Medical Restrictions")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericHomeboundReason", "9", genericHomeboundReason.Contains("9"), "Other", Model.Type + "_GenericHomeboundReasonOther", data.AnswerOrEmptyString("GenericHomeboundReasonOther"))%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Diagnosis</legend>
    <div class="column">
        <div class="row">
            <label>Orders for Evaluation Only?</label>
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "1", data.AnswerOrEmptyString("GenericOrdersForEvaluationOnly").Equals("1"), new { @id = Model.Type + "_GenericOrdersForEvaluationOnly1" })%>
                        <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly1">Yes</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "0", data.AnswerOrEmptyString("GenericOrdersForEvaluationOnly").Equals("0"), new { @id = Model.Type + "_GenericOrdersForEvaluationOnly0" })%>
                        <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly0">No</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_GenericIfNoOrdersAre">If No, orders are</label>
                        <%= Html.TextBox(Model.Type + "_GenericIfNoOrdersAre", data.AnswerOrEmptyString("GenericIfNoOrdersAre"), new { @id = Model.Type + "_GenericIfNoOrdersAre" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="fl">Medical Diagnosis/Treatment
                Diagnosis</label>
            <div class="ac ar">
                <%= Html.TextArea(Model.Type + "_GenericMedicalDiagnosis", data.AnswerOrEmptyString("GenericMedicalDiagnosis"), new { @id = Model.Type + "_GenericMedicalDiagnosis" })%><br />
                <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate">Onset</label>
                <input type="text" class="date-picker shortdate" name="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate"
                    value="<%= data.AnswerOrEmptyString("GenericMedicalDiagnosisOnsetDate") %>" id="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate" />
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericMedicalPrecautions">Medical Precautions</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericMedicalPrecautions", data.AnswerOrEmptyString("GenericMedicalPrecautions"), new { @id = Model.Type + "_GenericMedicalPrecautions" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericPriorLevelOfFunctioning">Prior Level of Functioning</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericPriorLevelOfFunctioning", data.AnswerOrEmptyString("GenericPriorLevelOfFunctioning"), new { @id = Model.Type + "_GenericPriorLevelOfFunctioning" })%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericLivingSituation">Living Situation/Support System</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericLivingSituation", data.AnswerOrEmptyString("GenericLivingSituation"), new { @id = Model.Type + "_GenericLivingSituation" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericPreviousMedicalHistory">Describe pertinent medical/social
                history and/or previous therapy provided</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericPreviousMedicalHistory", data.AnswerOrEmptyString("GenericPreviousMedicalHistory"), new { @id = Model.Type + "_GenericPreviousMedicalHistory" })%>
            </div>
        </div>
        <div class="row">
            <label>Safe Swallowing Evaluation?</label>
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "1", data.AnswerOrEmptyString("GenericIsSSE").Equals("1"), new { @id = Model.Type + "_GenericIsSSE1" })%>
                        <label for="<%= Model.Type %>_GenericIsSSE1">Yes</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_GenericGenericSSESpecify">Specify date, facility and physician</label>
                        <div class="ac">
                            <%= Html.TextArea(Model.Type + "_GenericGenericSSESpecify", data.AnswerOrEmptyString("GenericGenericSSESpecify"), new { @id = Model.Type + "_GenericGenericSSESpecify" })%>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "0", data.AnswerOrEmptyString("GenericIsSSE").Equals("0"), new { @id = Model.Type + "_GenericIsSSE0" })%>
                        <label for="<%= Model.Type %>_GenericIsSSE0">No</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label>Video Fluoroscopy?</label>
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "1", data.AnswerOrEmptyString("GenericIsVideoFluoroscopy").Equals("1"), new { @id = Model.Type + "_GenericIsVideoFluoroscopy1" })%>
                        <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy1">Yes</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_GenericVideoFluoroscopySpecify">Specify date, facility
                            and physician</label>
                        <div class="ac">
                            <%= Html.TextArea(Model.Type + "_GenericVideoFluoroscopySpecify", data.AnswerOrEmptyString("GenericVideoFluoroscopySpecify"), new { @id = Model.Type + "_GenericVideoFluoroscopySpecify" })%>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "0", data.AnswerOrEmptyString("GenericIsVideoFluoroscopy").Equals("0"), new { @id = Model.Type + "_GenericIsVideoFluoroscopy0" })%>
                        <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy0">No</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Diet</legend>
            <div class="column">
                <div class="row">
                    <label for="<%= Model.Type %>_GenericCurrentDietTexture" class="fl">Current Diet Texture</label>
                    <div class="ac">
                        <%= Html.TextArea(Model.Type + "_GenericCurrentDietTexture", data.AnswerOrEmptyString("GenericCurrentDietTexture"), new { @id = Model.Type + "_GenericCurrentDietTexture" })%>
                    </div>
                </div>
                <div class="row">
                    <label>Liquids</label>
                    <div class="checkgroup">
                        <input type="hidden" name="<%= Model.Type %>_GenericLiquids" value="" />
                        <% string[] liquids = data.AnswerArray("GenericLiquids"); %>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericLiquids", "1", liquids.Contains("1"), "Thin")%>
                        <%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericLiquids", "2", liquids.Contains("2"), "Thickened", Model.Type + "_GenericLiquidsThick", data.AnswerOrEmptyString("GenericLiquidsThick"))%>
                        <%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericLiquids", "3", liquids.Contains("3"), "Other", Model.Type + "_GenericLiquidsOther", data.AnswerOrEmptyString("GenericLiquidsOther"))%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <fieldset>
            <legend>Pain</legend>
            <div class="column">
                <div class="row">
                    <label for="<%= Model.Type %>_GenericPainDescription" class="fl">Describe</label>
                    <div class="ac">
                        <%= Html.TextArea(Model.Type + "_GenericPainDescription", data.AnswerOrEmptyString("GenericPainDescription"), new { @id = Model.Type + "_GenericPainDescription" })%><br />
                    </div>
                </div>
                <div class="row">
                    <label class="fl">Impact on Therapy Care Plan</label>
                    <div class="fr">
                        <div class="checkgroup two-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "1", data.AnswerOrEmptyString("GenericIsPainImpactCarePlan").Equals("1"), new { @id = Model.Type + "_GenericIsPainImpactCarePlan1" })%>
                                    <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan1">Yes</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "0", data.AnswerOrEmptyString("GenericIsPainImpactCarePlan").Equals("0"), new { @id = Model.Type + "_GenericIsPainImpactCarePlan0" })%>
                                    <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan0">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<fieldset>
    <legend>Speech/Language Evaluation</legend>
    <div class="wide-column">
        <div class="row ac">
            <em>4 &#8211; WFL (Within Functional Limits) &#160; 3 &#8211; Mild Impairment &#160;
                2 &#8211; Moderate Impairment &#160; 1 &#8211; Severe Impairment &#160; 0 &#8211;
                Unable to Assess/Did Not Test</em>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Cognition Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Orientation (Person/Place/Time)</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericOrientationScore", data.AnswerOrEmptyString("GenericOrientationScore"), new { @class = "short fr", @id = Model.Type + "_GenericOrientationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Attention Span</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericAttentionSpanScore", data.AnswerOrEmptyString("GenericAttentionSpanScore"), new { @class = "short fr", @id = Model.Type + "_GenericAttentionSpanScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Short Term Memory</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermMemoryScore", data.AnswerOrEmptyString("GenericShortTermMemoryScore"), new { @class = "short fr", @id = Model.Type + "_GenericShortTermMemoryScore" })%>
                </div>
            </div>
        </div>
        <div class="row ">
            <label class="fl">Long Term Memory</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermMemoryScore", data.AnswerOrEmptyString("GenericLongTermMemoryScore"), new { @class = "short fr", @id = Model.Type + "_GenericLongTermMemoryScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Judgment</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericJudgmentScore", data.AnswerOrEmptyString("GenericJudgmentScore"), new { @class = "short fr", @id = Model.Type + "_GenericJudgmentScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Problem Solving</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericProblemSolvingScore", data.AnswerOrEmptyString("GenericProblemSolvingScore"), new { @class = "short fr", @id = Model.Type + "_GenericProblemSolvingScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Organization</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericOrganizationScore", data.AnswerOrEmptyString("GenericOrganizationScore"), new { @class = "short fr", @id = Model.Type + "_GenericOrganizationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedOther" class="fl">Other</label><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedOther" })%>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOtherScore"), new { @class = "short fr", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericCognitionFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Speech/Voice Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Oral/Facial Exam</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericOralFacialExamScore", data.AnswerOrEmptyString("GenericOralFacialExamScore"), new { @class = "short fr", @id = Model.Type + "_GenericOralFacialExamScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericArticulationScore" class="fl">Articulation</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericArticulationScore", data.AnswerOrEmptyString("GenericArticulationScore"), new { @class = "short fr", @id = Model.Type + "_GenericArticulationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericProsodyScore" class="fl">Prosody</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericProsodyScore", data.AnswerOrEmptyString("GenericProsodyScore"), new { @class = "short fr", @id = Model.Type + "_GenericProsodyScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericVoiceRespirationScore" class="fl">Voice/Respiration</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericVoiceRespirationScore", data.AnswerOrEmptyString("GenericVoiceRespirationScore"), new { @class = "short fr", @id = Model.Type + "_GenericVoiceRespirationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericSpeechIntelligibilityScore" class="fl">Speech Intelligibility</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericSpeechIntelligibilityScore", data.AnswerOrEmptyString("GenericSpeechIntelligibilityScore"), new { @class = "short fr", @id = Model.Type + "_GenericSpeechIntelligibilityScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Other
                <%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedOther" })%></label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOtherScore"), new { @class = "short fr", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericSpeechFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Auditory Comprehension Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Word Discrimination</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericWordDiscriminationScore", data.AnswerOrEmptyString("GenericWordDiscriminationScore"), new { @class = "short fr", @id = Model.Type + "_GenericWordDiscriminationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">One Step Directions</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericOneStepDirectionsScore", data.AnswerOrEmptyString("GenericOneStepDirectionsScore"), new { @class = "short fr", @id = Model.Type + "_GenericOneStepDirectionsScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Two Step Directions</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericTwoStepDirectionsScore", data.AnswerOrEmptyString("GenericTwoStepDirectionsScore"), new { @class = "short fr", @id = Model.Type + "_GenericTwoStepDirectionsScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Complex Sentences</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericComplexSentencesScore", data.AnswerOrEmptyString("GenericComplexSentencesScore"), new { @class = "short fr", @id = Model.Type + "_GenericComplexSentencesScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Conversation</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericConversationScore", data.AnswerOrEmptyString("GenericConversationScore"), new { @class = "short fr", @id = Model.Type + "_GenericConversationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Speech Reading</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericSpeechReadingScore", data.AnswerOrEmptyString("GenericSpeechReadingScore"), new { @class = "short fr", @id = Model.Type + "_GenericSpeechReadingScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericACFEComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericACFEComment", data.AnswerOrEmptyString("GenericACFEComment"), new { @id = Model.Type + "_GenericACFEComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Swallowing Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Chewing Ability</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericChewingAbilityScore", data.AnswerOrEmptyString("GenericChewingAbilityScore"), new { @class = "short fr", @id = Model.Type + "_GenericChewingAbilityScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Oral Stage Management</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericOralStageManagementScore", data.AnswerOrEmptyString("GenericOralStageManagementScore"), new { @class = "short fr", @id = Model.Type + "_GenericOralStageManagementScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Pharyngeal Stage Management</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericPharyngealStageManagementScore", data.AnswerOrEmptyString("GenericPharyngealStageManagementScore"), new { @class = "short fr", @id = Model.Type + "_GenericPharyngealStageManagementScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Reflex Time</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericReflexTimeScore", data.AnswerOrEmptyString("GenericReflexTimeScore"), new { @class = "short fr", @id = Model.Type + "_GenericReflexTimeScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Other<%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOther" })%></label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOtherScore"), new { @class = "short fr", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericSwallowingFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Verbal Expression Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Augmentative Methods</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericAugmentativeMethodsScore", data.AnswerOrEmptyString("GenericAugmentativeMethodsScore"), new { @class = "short fr", @id = Model.Type + "_GenericAugmentativeMethodsScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Naming</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericNamingScore", data.AnswerOrEmptyString("GenericNamingScore"), new { @class = "short fr", @id = Model.Type + "_GenericNamingScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Appropriate</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericAppropriateScore", data.AnswerOrEmptyString("GenericAppropriateScore"), new { @class = "short fr", @id = Model.Type + "_GenericAppropriateScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Complex Sentences</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericVEFEComplexSentencesScore", data.AnswerOrEmptyString("GenericVEFEComplexSentencesScore"), new { @class = "short fr", @id = Model.Type + "_GenericVEFEComplexSentencesScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Conversation</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericVEFEConversationScore", data.AnswerOrEmptyString("GenericVEFEConversationScore"), new { @class = "short fr", @id = Model.Type + "_GenericVEFEConversationScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericVEFEComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericVEFEComment", data.AnswerOrEmptyString("GenericVEFEComment"), new { @id = Model.Type + "_GenericVEFEComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Reading Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Letters/Numbers</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericRFELettersNumbersScore", data.AnswerOrEmptyString("GenericRFELettersNumbersScore"), new { @class = "short fr", @id = Model.Type + "_GenericRFELettersNumbersScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Words</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericRFEWordsScore", data.AnswerOrEmptyString("GenericRFEWordsScore"), new { @class = "short fr", @id = Model.Type + "_GenericRFEWordsScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Simple Sentences</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericRFESimpleSentencesScore", data.AnswerOrEmptyString("GenericRFESimpleSentencesScore"), new { @class = "short fr", @id = Model.Type + "_GenericRFESimpleSentencesScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Complex Sentences</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericRFEComplexSentencesScore", data.AnswerOrEmptyString("GenericRFEComplexSentencesScore"), new { @class = "short fr", @id = Model.Type + "_GenericRFEComplexSentencesScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Paragraph</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericParagraphScore", data.AnswerOrEmptyString("GenericParagraphScore"), new { @class = "short fr", @id = Model.Type + "_GenericParagraphScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericRFEComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericRFEComment", data.AnswerOrEmptyString("GenericRFEComment"), new { @id = Model.Type + "_GenericRFEComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Writing Function Evaluated</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Letters/Numbers</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericWFELettersNumbersScore", data.AnswerOrEmptyString("GenericWFELettersNumbersScore"), new { @class = "short fr", @id = Model.Type + "_GenericWFELettersNumbersScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Words</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericWFEWordsScore", data.AnswerOrEmptyString("GenericWFEWordsScore"), new { @class = "short fr", @id = Model.Type + "_GenericWFEWordsScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Sentences</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericWFESentencesScore", data.AnswerOrEmptyString("GenericWFESentencesScore"), new { @class = "short fr", @id = Model.Type + "_GenericWFESentencesScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Spelling</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericWFESpellingScore", data.AnswerOrEmptyString("GenericWFESpellingScore"), new { @class = "short fr", @id = Model.Type + "_GenericWFESpellingScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Formulation</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericFormulationScore", data.AnswerOrEmptyString("GenericFormulationScore"), new { @class = "short fr", @id = Model.Type + "_GenericFormulationScore" })%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Simple Addition/Subtraction</label>
            <div class="fr">
                <div>
                    <%= Html.TextBox(Model.Type + "_GenericSimpleAdditionSubtractionScore", data.AnswerOrEmptyString("GenericSimpleAdditionSubtractionScore"), new { @class = "short fr", @id = Model.Type + "_GenericSimpleAdditionSubtractionScore" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericWFEComment">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericWFEComment", data.AnswerOrEmptyString("GenericWFEComment"), new { @id = Model.Type + "_GenericWFEComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Short Term Goals</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_GenericShortTermGoalsTemplates")%>
                        <%= Html.TextArea(Model.Type + "_GenericShortTermGoalsComment", data.AnswerOrEmptyString("GenericShortTermGoalsComment"), new { @id = Model.Type + "_GenericShortTermGoalsComment" })%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <fieldset>
            <legend>Long Term Goals</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_GenericLongTermGoalsTemplates")%>
                        <%= Html.TextArea(Model.Type + "_GenericLongTermGoalsComment", data.AnswerOrEmptyString("GenericLongTermGoalsComment"), new { @id = Model.Type + "_GenericLongTermGoalsComment" })%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<fieldset>
    <legend>Plan of Care</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup two-wide">
                <% string[] genericPlanOfCare = data.AnswerArray("GenericPlanOfCare"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericPlanOfCare" value="" />
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare1' name='{1}_GenericPlanOfCare' value='1' type='checkbox' {0} />", genericPlanOfCare.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare1">Evaluation (C1)</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare15' name='{1}_GenericPlanOfCare' value='15' type='checkbox' {0} />", genericPlanOfCare.Contains("15").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare15">Language Processing</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare2' name='{1}_GenericPlanOfCare' value='2' type='checkbox' {0} />", genericPlanOfCare.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare2">Establish Rehab Program</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare16' name='{1}_GenericPlanOfCare' value='16' type='checkbox' {0} />", genericPlanOfCare.Contains("16").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare16">Food Texture Recommendations</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare3' name='{1}_GenericPlanOfCare' value='3' type='checkbox' {0} />", genericPlanOfCare.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare3">Given to Patient</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare17' name='{1}_GenericPlanOfCare' value='17' type='checkbox' {0} />", genericPlanOfCare.Contains("17").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare17">Safe Swallowing Evaluation</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare4' name='{1}_GenericPlanOfCare' value='4' type='checkbox' {0} />", genericPlanOfCare.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare4">Attached to Chart</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare18' name='{1}_GenericPlanOfCare' value='18' type='checkbox' {0} />", genericPlanOfCare.Contains("18").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare18">Therapy to Increase Articulation,
                            Proficiency, Verbal Expression</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare5' name='{1}_GenericPlanOfCare' value='5' type='checkbox' {0} />", genericPlanOfCare.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare5">Patient/Family Education</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare19' name='{1}_GenericPlanOfCare' value='19' type='checkbox' {0} />", genericPlanOfCare.Contains("19").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare19">Lip, Tongue, Facial Exercises to
                            Improve Swallowing/Vocal Skills</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare6' name='{1}_GenericPlanOfCare' value='6' type='checkbox' {0} />", genericPlanOfCare.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare6">Voice Disorders</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare20' name='{1}_GenericPlanOfCare' value='20' type='checkbox' {0} />", genericPlanOfCare.Contains("20").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare20">Pain Management</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare7' name='{1}_GenericPlanOfCare' value='7' type='checkbox' {0} />", genericPlanOfCare.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare7">Speech Articulation Disorders</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare21' name='{1}_GenericPlanOfCare' value='21' type='checkbox' {0} />", genericPlanOfCare.Contains("21").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare21">Speech Dysphagia Instruction Program</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare8' name='{1}_GenericPlanOfCare' value='8' type='checkbox' {0} />", genericPlanOfCare.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare8">Dysphagia Treatments</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare22' name='{1}_GenericPlanOfCare' value='22' type='checkbox' {0} />", genericPlanOfCare.Contains("22").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare22">Care of Voice Prosthesis &#8212;
                            Removal, Cleaning, Site Maint</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare9' name='{1}_GenericPlanOfCare' value='9' type='checkbox' {0} />", genericPlanOfCare.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare9">Language Disorders</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare23' name='{1}_GenericPlanOfCare' value='23' type='checkbox' {0} />", genericPlanOfCare.Contains("23").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare23">Teach/Develop Comm. System</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare10' name='{1}_GenericPlanOfCare' value='10' type='checkbox' {0} />", genericPlanOfCare.Contains("10").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare10">Aural Rehabilitation (C6)</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare24' name='{1}_GenericPlanOfCare' value='24' type='checkbox' {0} />", genericPlanOfCare.Contains("24").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare24">Trach Inst. and Care</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare11' name='{1}_GenericPlanOfCare' value='11' type='checkbox' {0} />", genericPlanOfCare.Contains("11").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare11">Non-Oral Communication (C8)</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare25' name='{1}_GenericPlanOfCare' value='25' type='checkbox' {0} />", genericPlanOfCare.Contains("25").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare25">Other</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{1}_GenericPlanOfCare12' name='{1}_GenericPlanOfCare' value='12' type='checkbox' {0} />", genericPlanOfCare.Contains("12").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlanOfCare12">Alaryngeal Speech Skills</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericPatientDesiredOutcomes">Assessment</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericPatientDesiredOutcomes", data.AnswerOrEmptyString("GenericPatientDesiredOutcomes"), new { @id = Model.Type + "_GenericPatientDesiredOutcomes" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericShortTermOutcomes">Short Term Outcomes</label><br />
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericShortTermOutcomes", data.AnswerOrEmptyString("GenericShortTermOutcomes"), new { @id = Model.Type + "_GenericShortTermOutcomes" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericLongTermOutcomes">Long Term Outcomes</label><br />
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericLongTermOutcomes", data.AnswerOrEmptyString("GenericLongTermOutcomes"), new { @id = Model.Type + "_GenericLongTermOutcomes" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="fl">Frequency and
                Duration</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericFrequencyAndDuration", data.AnswerOrEmptyString("GenericFrequencyAndDuration"), new { @id = Model.Type + "_GenericFrequencyAndDuration" })%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericSafetyIssues">Safety Issues/ Instruction/ Education</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericSafetyIssues", data.AnswerOrEmptyString("GenericSafetyIssues"), new { @id = Model.Type + "_GenericSafetyIssues" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericEquipmentRecommendations" class="fl">Equipment
                Recommendations</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericEquipmentRecommendations", data.AnswerOrEmptyString("GenericEquipmentRecommendations"), new { @id = Model.Type + "_GenericEquipmentRecommendations" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericResponseToPOC">Patient/ Caregiver Response to Plan
                of Care</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericResponseToPOC", data.AnswerOrEmptyString("GenericResponseToPOC"), new { @id = Model.Type + "_GenericResponseToPOC" })%>
            </div>
        </div>
        <div class="row">
            <label class="fl">PF/Caregiver aware and agreeable to POC</label>
            <div class="fr">
                <div class="checkgroup two-wide">
                    <div class="option">
                        <div class="wrapper">
                            <%= Html.RadioButton(Model.Type + "_GenericCaregiverAgreeable", "1", data.AnswerOrEmptyString("GenericCaregiverAgreeable").Equals("1"), new { @id = Model.Type + "_GenericCaregiverAgreeable1" })%>
                            <label for="<%= Model.Type %>_GenericCaregiverAgreeable1">Yes</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= Html.RadioButton(Model.Type + "_GenericCaregiverAgreeable", "0", data.AnswerOrEmptyString("GenericCaregiverAgreeable").Equals("0"), new { @id = Model.Type + "_GenericCaregiverAgreeable0" })%>
                            <label for="<%= Model.Type %>_GenericCaregiverAgreeable0">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericAdditionalInformation">Comments</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenericAdditionalInformation", data.AnswerOrEmptyString("GenericAdditionalInformation"), new { @id = Model.Type + "_GenericAdditionalInformation" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Rehab Potential</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup">
                <%= Html.CheckgroupRadioOption(Model.Type + "_GenericRehabPotential", "2", data.AnswerOrEmptyString("GenericRehabPotential").Equals("2"), "Good")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GenericRehabPotential", "1", data.AnswerOrEmptyString("GenericRehabPotential").Equals("1"), "Fair")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GenericRehabPotential", "0", data.AnswerOrEmptyString("GenericRehabPotential").Equals("0"), "Poor")%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Discharge Plans</legend>
    <div class="wide-column">
        <div class="row">
            <label>Discharge discussed with</label>
            <div class="checkgroup four-wide">
                <input type="hidden" name="<%= Model.Type %>_GenericDischargeDiscussedWith" value="" />
                <% string[] genericDischargeDiscussedWith = data.AnswerArray("GenericDischargeDiscussedWith"); %>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDischargeDiscussedWith", "1", genericDischargeDiscussedWith.Contains("1"), "Patient Family")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDischargeDiscussedWith", "2", genericDischargeDiscussedWith.Contains("2"), "Case Manager")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDischargeDiscussedWith", "3", genericDischargeDiscussedWith.Contains("3"), "Physician")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericDischargeDiscussedWith", "4", genericDischargeDiscussedWith.Contains("4"), "Other", Model.Type + "_GenericDischargeDiscussedWithOther", data.AnswerOrEmptyString("GenericDischargeDiscussedWithOther"))%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Care Coordination</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup four-wide">
                <input type="hidden" name="<%= Model.Type %>_GenericCareCoordinationCheckGroup" value="" />
                <% string[] genericCareCoordination = data.AnswerArray("GenericCareCoordinationCheckGroup"); %>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "1", genericCareCoordination.Contains("1"), "Physician")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "2", genericCareCoordination.Contains("2"), "PT")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "3", genericCareCoordination.Contains("3"), "OT")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "4", genericCareCoordination.Contains("4"), "ST")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "5", genericCareCoordination.Contains("5"), "MSW")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericCareCoordinationCheckGroup", "6", genericCareCoordination.Contains("6"), "SN")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericCareCoordinationCheckGroup", "7", genericCareCoordination.Contains("7"), "Other", Model.Type + "_GenericCareCoordinationOther", data.AnswerOrEmptyString("GenericCareCoordinationOther"))%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericCareCoordination">Comments</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Plan for Next Visit</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericPlanForNextVisitTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericPlanForNextVisit", data.AnswerOrEmptyString("GenericPlanForNextVisit"), new { @id = Model.Type + "_GenericPlanForNextVisit" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Skilled Treatment Provided</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericTreatmentProvidedTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericTreatmentProvided", data.AnswerOrEmptyString("GenericTreatmentProvided"), new { @id = Model.Type + "_GenericTreatmentProvided" })%>
            </div>
        </div>
    </div>
</fieldset>
