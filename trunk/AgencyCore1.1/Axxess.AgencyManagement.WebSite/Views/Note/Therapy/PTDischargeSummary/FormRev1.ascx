﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">PT Discharge Summary |<%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
    <% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "PTDischargeSummaryForm" }))
       { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
        <legend>Discharge Information</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_DischargeDate" class="fl">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_DischargeDate" value="<%= data.AnswerOrEmptyString("DischargeDate").IsNotNullOrEmpty() && data["DischargeDate"].Answer.IsValidDate() ? data["DischargeDate"].Answer : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_DischargeDate" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_ReasonForDC" class="fl">Reason for Discharge</label>
                <div class="fr">
                    <%  var reasonForDC = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Goals Met", Value = "1" },
                                new SelectListItem { Text = "To Nursing Home", Value = "2" },
                                new SelectListItem { Text = "Deceased", Value = "3" },
                                new SelectListItem { Text = "Noncompliant", Value = "4" },
                                new SelectListItem { Text = "To Hospital", Value = "5" },
                                new SelectListItem { Text = "Moved from Service Area", Value = "6" },
                                new SelectListItem { Text = "Refused Care", Value = "7" },
                                new SelectListItem { Text = "No Longer Homebound", Value = "8" },
                                new SelectListItem { Text = "Other", Value = "9" }
                            }, "Value", "Text", data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : "0"); %>
                    <%= Html.DropDownList(Model.Type + "_ReasonForDC", reasonForDC, new { @id = Model.Type + "_ReasonForDC" })%>
                    <%= Html.TextBox(Model.Type + "_ReasonForDCOther",data.AnswerOrEmptyString("ReasonForDCOther"), new { @id = Model.Type + "_ReasonForDCOther", @style = "display:none;" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl">Notification of Discharge given to patient</label>
                <div class="fr">
                    <div class="checkgroup one-wide">
                        <%= Html.Hidden(Model.Type + "_IsNotificationDC", " ", new { @id = "" })%>
                        <div class="option">
                            <div class="wrapper">
                                <%= Html.RadioButton(Model.Type + "_IsNotificationDC", "1", data.AnswerOrEmptyString("IsNotificationDC") == "1" , new { @id = Model.Type + "_IsNotificationDCY" })%>
                                <label for="<%= Model.Type %>_IsNotificationDCY">Yes</label>
                            </div>
                            <div class="more">
                                <label for="<%= Model.Type %>_NotificationDate" class="fl">If Yes</label>
                                <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 days", Value = "1" },
                                    new SelectListItem { Text = "2 days", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                                <%= Html.DropDownList(Model.Type + "_NotificationDate", patientReceived, new { @id = Model.Type + "_NotificationDate", @class = "shorter fr"}) %>
                                <%= Html.TextBox(Model.Type + "_NotificationDateOther", data.AnswerOrEmptyString("NotificationDateOther"), new { @id = Model.Type + "_NotificationDateOther", @style = "display:none;", @class = "short fr" }) %>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <%=Html.CheckgroupRadioOption(Model.Type + "_IsNotificationDC","0",data.AnswerOrEmptyString("IsNotificationDC") == "0","No")%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Patient Condition and Outcomes</legend>
                <div class="column">
                    <div class="row">
                        <div class="checkgroup two-wide">
                            <%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
                            <input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "1", patientCondition.Contains("1"), "Stable")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "2", patientCondition.Contains("2"), "Improved")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "3", patientCondition.Contains("3"), "Unchanged")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "4", patientCondition.Contains("4"), "Unstable")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "5", patientCondition.Contains("5"), "Declined")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "6", patientCondition.Contains("6"), "Goals Met")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "7", patientCondition.Contains("7"), "Goals Not Met")%>
                            <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "8", patientCondition.Contains("8"), "Goals Partially Met")%>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Service(s) Provided</legend>
                <div class="column">
                    <div class="row">
                        <div class="checkgroup two-wide">
                            <%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
                            <input name="<%= Model.Type %>_ServiceProvided" value=" " type="hidden" />
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "1", serviceProvided.Contains("1"), "SN")%>
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "2", serviceProvided.Contains("2"), "PT")%>
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "3", serviceProvided.Contains("3"), "OT")%>
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "4", serviceProvided.Contains("4"), "ST")%>
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "5", serviceProvided.Contains("5"), "MSW")%>
                            <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "6", serviceProvided.Contains("6"), "HHA")%>
                            <%= Html.CheckgroupOptionWithOther(Model.Type + "_ServiceProvided", "7", serviceProvided.Contains("7"), "Other", Model.Type + "_ServiceProvidedOtherValue", data.AnswerOrEmptyString("ServiceProvidedOtherValue"))%>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Care Summary</legend>
                <div class="column">
                    <div class="row">
                        <em>(Care Given, Progress, Regress including Therapies)</em>
                        <div class="template-text">
                            <%= Html.ToggleTemplates(Model.Type + "_CareSummaryTemplates")%>
                            <%= Html.TextArea(Model.Type + "_CareSummary",data.AnswerOrEmptyString("CareSummary"), new { })%>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Condition of Discharge</legend>
                <div class="column">
                    <div class="row">
                        <em>(Include VS, BS, Functional and Overall Status)</em>
                        <div class="template-text">
                            <%= Html.ToggleTemplates(Model.Type + "_ConditionOfDischargeTemplates")%>
                            <%= Html.TextArea(Model.Type + "_ConditionOfDischarge", data.AnswerOrEmptyString("ConditionOfDischarge"), new { })%>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Discharge Details</legend>
        <div class="column">
            <div class="row">
                <label>Discharge Disposition: Where is the Patient after Discharge from your Agency?</label>
                <div class="checkgroup one-wide">
                    <%  string[] dischargeDisposition = data.AnswerArray("DischargeDisposition"); %>
                    <input name="<%= Model.Type %>_ServiceProvided" value=" " type="hidden" />
                    <%= Html.CheckgroupRadioOption(Model.Type + "_DischargeDisposition", "01", dischargeDisposition.Contains("01"), "Patient remained in the community (without formal assistive services)")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_DischargeDisposition", "02", dischargeDisposition.Contains("02"), "Patient remained in the community (with formal assistive services)")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_DischargeDisposition", "03", dischargeDisposition.Contains("03"), "Patient transferred to a non-institutional hospice)")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_DischargeDisposition", "04", dischargeDisposition.Contains("04"), "Unknown because patient moved to a geographic location not served by this agency")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_DischargeDisposition", "UK", dischargeDisposition.Contains("UK"), "Other unknown")%>
                </div>
            </div>
            <div class="row">
                <label>Discharge Instructions Given To</label>
                <div class="checkgroup four-wide">
                    <%  string[] dischargeInstructionsGivenTo = data.AnswerArray("DischargeInstructionsGivenTo"); %>
                    <input name="<%= Model.Type %>_DischargeInstructionsGivenTo" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_DischargeInstructionsGivenTo", "1", dischargeInstructionsGivenTo.Contains("1"), "Patient")%>
                    <%= Html.CheckgroupOption(Model.Type + "_DischargeInstructionsGivenTo", "2", dischargeInstructionsGivenTo.Contains("2"), "Caregiver")%>
                    <%= Html.CheckgroupOption(Model.Type + "_DischargeInstructionsGivenTo", "3", dischargeInstructionsGivenTo.Contains("3"), "N/A")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_DischargeInstructionsGivenTo", "4", dischargeInstructionsGivenTo.Contains("4"), "Other", Model.Type + "_DischargeInstructionsGivenToOther", data.AnswerOrEmptyString("DischargeInstructionsGivenToOther"))%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_DischargeInstructions">Discharge Instructions</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_DischargeInstructionsTemplates")%>
                    <%= Html.TextArea(Model.Type + "_DischargeInstructions", data.AnswerOrEmptyString("DischargeInstructions"), new { @class = "tall" }) %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_IsVerbalizedUnderstanding" class="fl">Verbalized understanding</label>
                <div class="fr"><%= Html.YesNoCheckGroup(Model.Type + "_IsVerbalizedUnderstanding", data.AnswerOrEmptyString("IsVerbalizedUnderstanding"))%></div>
            </div>
            <div class="row">
                <div class="checkgroup one-wide">
                    <%  string[] differentTasks = data.AnswerArray("DifferentTasks"); %>
                    <input name="<%= Model.Type %>_DifferentTasks" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_DifferentTasks", "1", differentTasks.Contains("1"), "All services notified and discontinued")%>
                    <%= Html.CheckgroupOption(Model.Type + "_DifferentTasks", "2", differentTasks.Contains("2"), "Order and summary completed")%>
                    <%= Html.CheckgroupOption(Model.Type + "_DifferentTasks", "3", differentTasks.Contains("3"), "Information provided to patient for continuing needs")%>
                    <%= Html.CheckgroupOption(Model.Type + "_DifferentTasks", "4", differentTasks.Contains("4"), "Physician notified")%>
                </div>
            </div>
        </div>
    </fieldset>
    <%= Html.Partial("Bottom/View", Model) %>
    <% } %>
</div>
