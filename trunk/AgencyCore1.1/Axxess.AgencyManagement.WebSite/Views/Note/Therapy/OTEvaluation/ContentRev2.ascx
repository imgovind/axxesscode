﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/LivingSituation/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/History/FormRev1", Model); %>
		<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, false, Model.Type)); %>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/ADLs/FormRev2", Model); %>
<% Html.RenderPartial("Therapy/Shared/OTPhysicalAssessment/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/CognitiveStatus/FormRev1", Model); %>
		<% Html.RenderPartial("Therapy/Shared/MotorComponents/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Sensory/FormRev1", Model); %>
		<fieldset>
			<legend>Assessment</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"),new { @id = Model.Type + "_GenericAssessmentMore" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Narrative</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), new { @id = Model.Type + "_GenericNarrative" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev3", Model); %>
<% Html.RenderPartial("Therapy/Shared/OTGoals/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/OTStandardizedTest/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Care Coordination</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Skilled Treatment Provided This Visit</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericSkilledTreatmentTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericSkilledTreatmentProvided", data.AnswerOrEmptyString("GenericSkilledTreatmentProvided"), new { @id = Model.Type + "_GenericSkilledTreatmentProvided" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/Notification/FormRev1", Model); %>
