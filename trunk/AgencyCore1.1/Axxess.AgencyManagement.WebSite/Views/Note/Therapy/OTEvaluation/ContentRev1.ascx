﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Prior Level of Function</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericPriorFunctionalTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus"})%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Pertinent Medical History</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericMedicalTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.AnswerOrEmptyString("GenericMedicalHistory"), new { @id = Model.Type + "_GenericMedicalHistory" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/ADLs/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/TreatmentCodes/FormRev1", Model); %>
		<fieldset>
			<legend>Neuromuscular Reeducation</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="fl">Sitting Balance Activities</label>
					<div class="fr ar">
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">Static</label>
						<%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "short", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label>
						<br />
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">Dynamic</label>
						<%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "short", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="fl">Standing Balance Activities</label>
					<div class="fr ar">
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">Static</label>
						<%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "short", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label>
						<br />
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">Dynamic</label>
						<%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "short", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
					</div>
				</div>
				<div class="row">
					<div class="checkgroup one-wide">
						<input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
						<% string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
						<%= Html.CheckgroupOption(Model.Type + "_GenericUEWeightBearing", "1", genericUEWeightBearing.Contains("1"), "UE Weight-Bearing Activities")%>
					</div>
				</div>
			</div>
		</fieldset>
		<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev1", Model); %>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/PhysicalAssessment/FormRev1", Model); %>
<fieldset>
	<legend>Assessment</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericRehabPotential" class="fl">Rehab Potential</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @id = Model.Type + "_GenericRehabPotential" }) %>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericPrognosis" class="fl">Prognosis</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericPrognosis", data.AnswerOrEmptyString("GenericPrognosis"), new { @id = Model.Type + "_GenericPrognosis" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup four-wide">
				<% string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
				<input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment1' name='{1}_GenericAssessment' value='1' type='checkbox' {0} />", genericAssessment.Contains("1").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment1">Decreased Strength</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment2' name='{1}_GenericAssessment' value='2' type='checkbox' {0} />", genericAssessment.Contains("2").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment2">Decreased ROM</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment3' name='{1}_GenericAssessment' value='3' type='checkbox' {0} />", genericAssessment.Contains("3").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment3">Decreased ADL</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment4' name='{1}_GenericAssessment' value='4' type='checkbox' {0} />", genericAssessment.Contains("4").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment4">Ind</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment5' name='{1}_GenericAssessment' value='5' type='checkbox' {0} />", genericAssessment.Contains("5").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment5">Decreased Mobility</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment6' name='{1}_GenericAssessment' value='6' type='checkbox' {0} />", genericAssessment.Contains("6").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment6">Pain</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericAssessment7' name='{1}_GenericAssessment' value='7' type='checkbox' {0} />", genericAssessment.Contains("7").ToChecked(), Model.Type) %>
						<label for="<%= Model.Type %>_GenericAssessment7">Decreased Endurance</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericAssessmentMore">Comments</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"), new { @id = Model.Type + "_GenericAssessmentMore" })%>
			</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/ShortTermGoals/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/LongTermGoals/FormRev1", Model); %>
	</div>
</div>
