﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Narrative</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"),new { @id = Model.Type + "_GenericNarrativeComment" })%>
            </div>
        </div>
    </div>
</fieldset>
