﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>W/C Mobility</legend>
	<% if(Model.IsNA) { %>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%  string[] isWCMobilityApplied = data.AnswerArray("GenericIsWCMobilityApplied"); %>
				<%= Html.Hidden(Model.Type + "isWCMobilityApplied", string.Empty)%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsWCMobilityApplied", Model.Type + "_GenericIsWCMobilityApplied1", "1", isWCMobilityApplied.Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<% } %>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericWCMobilityLevel">Level</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityLevel", data.AnswerOrEmptyString("GenericWCMobilityLevel"), new { })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericWCMobilityRamp">Uneven</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityUneven", data.AnswerOrEmptyString("GenericWCMobilityUneven"), new { })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericWCMobilityManeuver">Maneuver</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityManeuver", data.AnswerOrEmptyString("GenericWCMobilityManeuver"), new { })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericWCMobilityADL">ADL</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericWCMobilityADL", data.AnswerOrEmptyString("GenericWCMobilityADL"), new { })%></div>
			</div>
		</div>
	</div>
</fieldset>
