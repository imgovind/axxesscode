﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Vital Signs</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericBloodPressure" class="fl">Systolic BP</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericBloodPressure", data.AnswerOrEmptyString("GenericBloodPressure"), new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericBloodPressurePer" class="fl">Diastolic BP</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericBloodPressurePer", data.AnswerOrEmptyString("GenericBloodPressurePer"), new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericPulse" class="fl">Pulse</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericPulse", data.AnswerOrEmptyString("GenericPulse"), new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericTemp" class="fl">Temperature</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @class = "short less" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericResp" class="fl">Respiration</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericWeight" class="fl">Weight</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericBloodSugar" class="fl">Blood Glucose</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericBloodSugar", data.AnswerOrEmptyString("GenericBloodSugar"), new { @class = "short less" })%>
			</div>
		</div>
	</div>
</fieldset>