﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  string[] isVitalSigns = Model.Questions.AnswerArray("IsVitalSigns"); %>
<fieldset>
    <legend>Vital Signs</legend>
    <% if (Model.IsNA)
       { %>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsVitalSigns", Model.Type + "_IsVitalSigns", "1", isVitalSigns != null && isVitalSigns.Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <% } %>
    <div id="<%=Model.Type %>_VitalSignsContainer" class="collapsible-container">
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericBloodPressure" class="fl">Systolic BP</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericBloodPressure", Model.Questions.AnswerOrEmptyString("GenericBloodPressure"), new { @class = "short less" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericBloodPressurePer" class="fl">Diastolic BP</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericBloodPressurePer", Model.Questions.AnswerOrEmptyString("GenericBloodPressurePer"), new { @class = "short less" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericPulse" class="fl">Pulse</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericPulse", Model.Questions.AnswerOrEmptyString("GenericPulse"), new { @class = "short less" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTemp" class="fl">Temperature</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericTemp", Model.Questions.AnswerOrEmptyString("GenericTemp"), new { @class = "short less" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericResp" class="fl">Respiration</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericResp", Model.Questions.AnswerOrEmptyString("GenericResp"), new { @class = "short less" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericWeight" class="fl">Weight</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericWeight", Model.Questions.AnswerOrEmptyString("GenericWeight"), new { @class = "short less" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericO2Sat" class="fl">O<sub>2</sub> Saturation</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericO2Sat", Model.Questions.AnswerOrEmptyString("GenericO2Sat"), new { @class = "short less" })%>
                </div>
            </div>
        </div>
    </div>
</fieldset>
