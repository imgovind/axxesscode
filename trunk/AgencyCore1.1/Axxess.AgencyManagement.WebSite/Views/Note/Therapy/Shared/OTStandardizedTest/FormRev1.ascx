﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Standardized Test</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label class="fl">Katz Index</label>
			<%= Html.TextBox(Model.Type + "_GenericKatzIndex", data.AnswerOrEmptyString("GenericKatzIndex"), new { @class = "fr", @id = Model.Type + "_GenericKatzIndex" })%>
		</div>
		<div class="sub row">
			<label class="fl">9 Hole Peg Test</label>
			<%= Html.TextBox(Model.Type + "_Generic9HolePeg", data.AnswerOrEmptyString("Generic9HolePeg"), new { @class = "fr", @id = Model.Type + "_Generic9HolePeg" })%>
		</div>
		<div class="sub row">
			<label class="fl">Lawton &#38; Brody IADL Scale</label>
			<%= Html.TextBox(Model.Type + "_GenericLawtonBrody", data.AnswerOrEmptyString("GenericLawtonBrody"), new { @class = "fr", @id = Model.Type + "_GenericLawtonBrody" })%>
		</div>
		<div class="sub row">
			<label class="fl">Mini-Mental State Exam</label>
			<%= Html.TextBox(Model.Type + "_GenericMiniMentalState", data.AnswerOrEmptyString("GenericMiniMentalState"), new { @class = "fr", @id = Model.Type + "_GenericMiniMentalState" })%>
		</div>
		<div class="sub row">
			<label class="fl">Other<%= Html.TextBox(Model.Type + "_GenericOtherTest", data.AnswerOrEmptyString("GenericOtherTest"), new { @class = "short", @id = Model.Type + "_GenericOtherTest" })%></label>
			<%= Html.TextBox(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @class = "fr", @id = Model.Type + "_GenericStandardizedTestOther" })%>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label class="fl">Katz Index</label>
			<%= Html.TextBox(Model.Type + "_GenericKatzIndex1", data.AnswerOrEmptyString("GenericKatzIndex1"), new { @class = "fr", @id = Model.Type + "_GenericKatzIndex1" })%>
		</div>
		<div class="sub row">
			<label class="fl">9 Hole Peg Test</label>
			<%= Html.TextBox(Model.Type + "_Generic9HolePeg1", data.AnswerOrEmptyString("Generic9HolePeg1"), new { @class = "fr", @id = Model.Type + "_Generic9HolePeg1" })%>
		</div>
		<div class="sub row">
			<label class="fl">Lawton &#38; Brody IADL Scale</label>
			<%= Html.TextBox(Model.Type + "_GenericLawtonBrody1", data.AnswerOrEmptyString("GenericLawtonBrody1"), new { @class = "fr", @id = Model.Type + "_GenericLawtonBrody1" })%>
		</div>
		<div class="sub row">
			<label class="fl">Mini-Mental State Exam</label>
			<%= Html.TextBox(Model.Type + "_GenericMiniMentalState1", data.AnswerOrEmptyString("GenericMiniMentalState1"), new { @class = "fr", @id = Model.Type + "_GenericMiniMentalState1" })%>
		</div>
		<div class="sub row">
			<label class="fl">Other<%= Html.TextBox(Model.Type + "_GenericOtherTest1", data.AnswerOrEmptyString("GenericOtherTest1"), new { @class = "short", @id = Model.Type + "_GenericOtherTest1" })%></label>
			<%= Html.TextBox(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @class = "fr", @id = Model.Type + "_GenericStandardizedTestOther1" })%>
		</div>
	</div>
</fieldset>