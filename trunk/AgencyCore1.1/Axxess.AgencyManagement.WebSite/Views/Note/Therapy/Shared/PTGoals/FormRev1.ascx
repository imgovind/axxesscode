﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var noteDiscipline = data.AnswerOrEmptyString("DisciplineTask"); %>
<fieldset>
    <legend>PT Goals</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsGoalsApply", Model.Type + "IsGoalsApply", "1", data.AnswerOrEmptyString("IsGoalsApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <% string[] genericPTGoals = data.AnswerArray("GenericPTGoals"); %>
                <%= Html.Hidden(Model.Type + "_GenericPTGoals", string.Empty)%>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals1' name='{0}_GenericPTGoals' value='1' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("1") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals1">Patient will demonstrate ability to perform
                                home exercise program within </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals1Weeks", data.AnswerOrEmptyString("GenericPTGoals1Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals1Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals1Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals2' name='{0}_GenericPTGoals' value='2' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("2") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals2">Demonstrate effective pain management
                                utilizing </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals2Utilize", data.AnswerOrEmptyString("GenericPTGoals2Utilize"), new { @class = "short", @id = Model.Type + "_GenericPTGoals2Utilize", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals2Utilize">within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals2Weeks", data.AnswerOrEmptyString("GenericPTGoals2Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals2Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals2Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals3' name='{0}_GenericPTGoals' value='3' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("3") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals3">Patient will be able to perform sit to
                                supine/supine to sit with</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals3Assist", data.AnswerOrEmptyString("GenericPTGoals3Assist"), new { @class = "short", @id = Model.Type + "_GenericPTGoals3Assist", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals3Assist">assist with safe and effective
                                technique within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals3Weeks", data.AnswerOrEmptyString("GenericPTGoals3Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals3Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals3Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals4' name='{0}_GenericPTGoals' value='4' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("4") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals4">Improve bed mobility to independent within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals4Weeks", data.AnswerOrEmptyString("GenericPTGoals4Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals4Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals4Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals5' name='{0}_GenericPTGoals' value='5' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("5") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals5">Improve transfers to </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals5Assist", data.AnswerOrEmptyString("GenericPTGoals5Assist"), new { @class = "short", @id = Model.Type + "_GenericPTGoals5Assist", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals5Assist">assist using </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals5Within", data.AnswerOrEmptyString("GenericPTGoals5Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals5Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals5Within">within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals5Weeks", data.AnswerOrEmptyString("GenericPTGoals5Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals5Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals5Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals6' name='{0}_GenericPTGoals' value='6' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("6") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals6">Independent with safe transfer skills
                                within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals6Weeks", data.AnswerOrEmptyString("GenericPTGoals6Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals6Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals6Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals7' name='{0}_GenericPTGoals' value='7' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("7") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals7">Patient will improve</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals7Skill", data.AnswerOrEmptyString("GenericPTGoals7Skill"), new { @class = "short", @id = Model.Type + "_GenericPTGoals7Skill", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals7Skill">transfer skill to </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals7Assist", data.AnswerOrEmptyString("GenericPTGoals7Assist"), new { @class = "short", @id = Model.Type + "_GenericPTGoals7Assist", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals7Assist">assist using </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals7Within", data.AnswerOrEmptyString("GenericPTGoals7Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals7Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals7Within">device within </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals7Weeks", data.AnswerOrEmptyString("GenericPTGoals7Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals7Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals7Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals8' name='{0}_GenericPTGoals' value='8' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("8") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals8">Patient to be independent with safety
                                issues in</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals8Weeks", data.AnswerOrEmptyString("GenericPTGoals8Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals8Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals8Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals9' name='{0}_GenericPTGoals' value='9' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("9") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals9">Patient will be able to negotiate stairs
                                with</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals9Assist", data.AnswerOrEmptyString("GenericPTGoals9Assist"), new { @class = "short", @id = Model.Type + "_GenericPTGoals9Assist", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals9Assist">device with</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals9Within", data.AnswerOrEmptyString("GenericPTGoals9Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals9Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals9Within">assist within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals9Weeks", data.AnswerOrEmptyString("GenericPTGoals9Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals9Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals9Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals10' name='{0}_GenericPTGoals' value='10' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("10") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals10">Patient will be able to ambulate using</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals10Device", data.AnswerOrEmptyString("GenericPTGoals10Device"), new { @class = "short", @id = Model.Type + "_GenericPTGoals10Device", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals10Device">device at least</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals10Feet", data.AnswerOrEmptyString("GenericPTGoals10Feet"), new { @class = "short", @id = Model.Type + "_GenericPTGoals10Feet", @maxlength = "10" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals10Feet">feet with</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals10Within", data.AnswerOrEmptyString("GenericPTGoals10Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals10Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals10Within">assist with safe and effective
                                gait pattern on even/uneven surface within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals10Weeks", data.AnswerOrEmptyString("GenericPTGoals10Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals10Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals10Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals11' name='{0}_GenericPTGoals' value='11' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("11") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals11">Independent with ambulation without
                                device indoor/outdoor at least </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals11Feet", data.AnswerOrEmptyString("GenericPTGoals11Feet"), new { @class = "short", @id = Model.Type + "_GenericPTGoals11Feet", @maxlength = "10" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals11Feet">feet to allow community access within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals11Weeks", data.AnswerOrEmptyString("GenericPTGoals11Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals11Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals11Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals12' name='{0}_GenericPTGoals' value='12' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("12") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals12">Patient will be able to ambulate using</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals12Device", data.AnswerOrEmptyString("GenericPTGoals12Device"), new { @class = "short", @id = Model.Type + "_GenericPTGoals12Device", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals12Device">device at least</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals12Feet", data.AnswerOrEmptyString("GenericPTGoals12Feet"), new { @class = "short", @id = Model.Type + "_GenericPTGoals12Feet", @maxlength = "10" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals12Feet">feet on</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals12Within", data.AnswerOrEmptyString("GenericPTGoals12Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals12Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals12Within">surface to be able to perform
                                ADL within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals12Weeks", data.AnswerOrEmptyString("GenericPTGoals12Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals12Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals12Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals13' name='{0}_GenericPTGoals' value='13' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("13") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals13">Improve strength of</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals13Of", data.AnswerOrEmptyString("GenericPTGoals13Of"), new { @class = "short", @id = Model.Type + "_GenericPTGoals13Of", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals13Of">to</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals13To", data.AnswerOrEmptyString("GenericPTGoals13To"), new { @class = "short", @id = Model.Type + "_GenericPTGoals13To", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals13To">grade to improve</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals13Within", data.AnswerOrEmptyString("GenericPTGoals13Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals13Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals13Within">within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals13Weeks", data.AnswerOrEmptyString("GenericPTGoals13Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals13Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals13Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals14' name='{0}_GenericPTGoals' value='14' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("14") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals14">Increase muscle strength of </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals14Of", data.AnswerOrEmptyString("GenericPTGoals14Of"), new { @class = "short", @id = Model.Type + "_GenericPTGoals14Of", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals14Of">to</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals14To", data.AnswerOrEmptyString("GenericPTGoals14To"), new { @class = "short", @id = Model.Type + "_GenericPTGoals14To", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals14To">grade to improve gait pattern/stability
                                and decrease fall risk within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals14Weeks", data.AnswerOrEmptyString("GenericPTGoals14Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals14Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals14Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals15' name='{0}_GenericPTGoals' value='15' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("15") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals15">Increase trunk muscle strength to
                            </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals15To", data.AnswerOrEmptyString("GenericPTGoals15To"), new { @class = "short", @id = Model.Type + "_GenericPTGoals15To", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals15Feet">to improve postural control and
                                balance within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals15Weeks", data.AnswerOrEmptyString("GenericPTGoals15Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals15Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals15Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals16' name='{0}_GenericPTGoals' value='16' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("16") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals16">Increase trunk muscle strength to
                            </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals16To", data.AnswerOrEmptyString("GenericPTGoals16To"), new { @class = "short", @id = Model.Type + "_GenericPTGoals16To", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals16Feet">to improve postural control during
                                bed mobility and transfer within </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals16Weeks", data.AnswerOrEmptyString("GenericPTGoals16Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals16Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals16Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals17' name='{0}_GenericPTGoals' value='17' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("17") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals17">Patient will increase ROM of </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals17Joint", data.AnswerOrEmptyString("GenericPTGoals17Joint"), new { @class = "short", @id = Model.Type + "_GenericPTGoals17Joint", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals17Joint">joint to </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals17Degree", data.AnswerOrEmptyString("GenericPTGoals17Degree"), new { @class = "short", @id = Model.Type + "_GenericPTGoals17Degree", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals17Degree">degree of </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals17In", data.AnswerOrEmptyString("GenericPTGoals17In"), new { @class = "short", @id = Model.Type + "_GenericPTGoals17In", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals17In">in</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals17Weeks", data.AnswerOrEmptyString("GenericPTGoals17Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals17Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals17Weeks">weeks to</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals17IncreaseROMTo", data.AnswerOrEmptyString("GenericPTGoals17IncreaseROMTo"), new { @class = "short", @id = Model.Type + "_GenericPTGoals17IncreaseROMTo", @maxlength = "3" })%>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals18' name='{0}_GenericPTGoals' value='18' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("18") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals18">Demonstrate safe and effective use of
                                prosthesis/brace/splint within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals18Weeks", data.AnswerOrEmptyString("GenericPTGoals18Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals18Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals18Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals19' name='{0}_GenericPTGoals' value='19' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("19") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals19">Demonstrate safe and effective use of</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals19Within", data.AnswerOrEmptyString("GenericPTGoals19Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals19Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals19Within">DME within </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals19Weeks", data.AnswerOrEmptyString("GenericPTGoals19Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals19Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals19Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals20' name='{0}_GenericPTGoals' value='20' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("20") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals20">Patient will have increase in Tinetti
                                Performance Oriented Mobility Assessment score to</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals20Within", data.AnswerOrEmptyString("GenericPTGoals20Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals20Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals20Within">over 28 to reduce fall risk within
                            </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals20Weeks", data.AnswerOrEmptyString("GenericPTGoals20Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals20Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals20Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals21' name='{0}_GenericPTGoals' value='21' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("21") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals21">Patient will have improved </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals21Improve", data.AnswerOrEmptyString("GenericPTGoals21Improve"), new { @class = "short", @id = Model.Type + "_GenericPTGoals21Improve", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals21Improve">standardized test score to improve
                            </label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals21Within", data.AnswerOrEmptyString("GenericPTGoals21Within"), new { @class = "short", @id = Model.Type + "_GenericPTGoals21Within", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals21Within">within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals21Weeks", data.AnswerOrEmptyString("GenericPTGoals21Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals21Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals21Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericPTGoals22' name='{0}_GenericPTGoals' value='22' type='checkbox' {1} />", Model.Type, genericPTGoals != null && genericPTGoals.Contains("22") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.Type %>_GenericPTGoals22">Patient will have increase in Timed
                                Up and Go score to</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals22Seconds", data.AnswerOrEmptyString("GenericPTGoals22Seconds"), new { @class = "short", @id = Model.Type + "_GenericPTGoals22Seconds", @maxlength = "30" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals22Seconds">seconds to reduce fall risk and
                                improve mobility within</label>
                            <%= Html.TextBox(Model.Type + "_GenericPTGoals22Weeks", data.AnswerOrEmptyString("GenericPTGoals22Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericPTGoals22Weeks", @maxlength = "3" })%>
                            <label for="<%= Model.Type %>_GenericPTGoals22Weeks">weeks.</label>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericPTGoalsComments">Additional goals</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_GenericPTGoalsTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericPTGoalsComments", data.AnswerOrEmptyString("GenericPTGoalsComments"), new { @id = Model.Type + "_GenericPTGoalsComments" })%>
                </div>
            </div>
            <%if (noteDiscipline == "47"){ %>
            <div class="row">
                <div class="ac">
                    <label for="<%= Model.Type %>_GenericRehabPotential">Rehab Potential</label><br />
                    <%= Html.TextArea(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @id = Model.Type + "_GenericRehabPotential" }) %>
                </div>
            </div>
            <%} %>
        </div>
    </div>
    </div>
</fieldset>
