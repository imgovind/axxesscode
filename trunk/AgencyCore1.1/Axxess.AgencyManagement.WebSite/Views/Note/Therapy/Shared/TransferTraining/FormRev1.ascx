﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Transfer Training</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsTransferApply", Model.Type + "_IsTransferApply", "1", Model.Questions.AnswerOrEmptyString("IsTransferApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericTransferTraining" class="fl">Transfer Training</label>
				<div class="fr">
					x
					<%= Html.TextBox(Model.Type + "_GenericTransferTraining", data.AnswerOrEmptyString("GenericTransferTraining"), new { @id = Model.Type + "_GenericTransferTraining", @class = "short" })%>
					reps
				</div>
			</div>
			<div class="row">
				<label class="fl">Assistive Device</label>
				<div class="fr">
					<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTrainingAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTrainingAssistiveDevice"), new { })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericBedChairAssist" class="fl">Bed &#8212; Chair</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericBedChairAssist", data.AnswerOrEmptyString("GenericBedChairAssist"), new { })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericChairToiletAssist" class="fl">Chair &#8212; Toilet</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericChairToiletAssist", data.AnswerOrEmptyString("GenericChairToiletAssist"), new { })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericChairCarAssist" class="fl">Chair &#8212; Car</label>
				<div class="fr">
					<%= Html.TherapyAssistance(Model.Type + "_GenericChairCarAssist", data.AnswerOrEmptyString("GenericChairCarAssist"), new { })%>
				</div>
			</div>
			<div class="row">
				<label for="" class="fl">Sitting Balance Activities</label>
				<div class="fr">
					<label for="<%= Model.Type %>_GenericSittingStaticAssist">Static</label>
					<%= Html.StaticBalance(Model.Type + "_GenericSittingStaticAssist", data.AnswerOrEmptyString("GenericSittingStaticAssist"), new { @class = "fr" })%>
					<br />
					<label for="<%= Model.Type %>_GenericSittingDynamicAssist">Dynamic</label>
					<%= Html.DynamicBalance(Model.Type + "_GenericSittingDynamicAssist", data.AnswerOrEmptyString("GenericSittingDynamicAssist"), new { @class = "fr" })%>
				</div>
			</div>
			<div class="row">
				<label for="" class="fl">Standing Balance Activities</label>
				<div class="fr">
					<label for="<%= Model.Type %>_GenericStandingStaticAssist">Static</label>
					<%= Html.StaticBalance(Model.Type + "_GenericStandingStaticAssist", data.AnswerOrEmptyString("GenericStandingStaticAssist"), new { @class = "fr" })%>
					<br />
					<label for="<%= Model.Type %>_GenericStandingDynamicAssist">Dynamic</label>
					<%= Html.DynamicBalance(Model.Type + "_GenericStandingDynamicAssist", data.AnswerOrEmptyString("GenericStandingDynamicAssist"), new { @class = "fr" })%>
				</div>
			</div>
			<div class="row">
				<label>Comments</label>
				<div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_TransferTrainingTemplates")%>
					<%= Html.TextArea(Model.Type + "_GenericTransferTrainingComment", data.AnswerOrEmptyString("GenericTransferTrainingComment"), new { })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>