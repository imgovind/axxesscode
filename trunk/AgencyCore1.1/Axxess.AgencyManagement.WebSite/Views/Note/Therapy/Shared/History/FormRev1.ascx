﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Prior Level of Function</legend>
	<div class="wide-column">
		<div class="row">
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericPriorFunctionalTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus"})%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Pertinent Medical History</legend>
	<div class="wide-column">
		<div class="row">
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericMedicalTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.AnswerOrEmptyString("GenericMedicalHistory"), new { @id = Model.Type + "_GenericMedicalHistory" })%>
			</div>
		</div>
	</div>
</fieldset>
