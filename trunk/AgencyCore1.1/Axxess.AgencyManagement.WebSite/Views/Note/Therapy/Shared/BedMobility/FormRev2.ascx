﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Bed Mobility</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsBedApply", Model.Type + "IsBedApply", "1", data.AnswerOrEmptyString("IsBedApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceRight">Rolling to Right</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceRight" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistanceLeft">Rolling to Left</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceLeft" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistance">Sit Stand Sit</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySitStandSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistance">Sup to Sit</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySupToSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericBedMobilityComment">Comments</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_BedMobilityTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericBedMobilityComment", data.AnswerOrEmptyString("GenericBedMobilityComment"), new { @id = Model.Type + "_GenericBedMobilityComment" })%>
                </div>
            </div>
        </div>
    </div>
</fieldset>
