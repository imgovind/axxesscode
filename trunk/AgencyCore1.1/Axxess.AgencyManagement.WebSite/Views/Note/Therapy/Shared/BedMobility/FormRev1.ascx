﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Bed Mobility</legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistiveDevice">Rolling</label>
            <div class="fr ar">
                <label>Assistive Device</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericBedMobilityRollingAssistiveDevice" })%>
                <br />
                <label>% Assist</label>
                <%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "short", @id = Model.Type+"_GenericBedMobilityRollingAssist" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistiveDevice">Sit Stand Sit</label>
            <div class="fr ar">
                <label>Assistive Device</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice" })%>
                <br />
                <label>% Assist</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySitStandSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssist"), new { @class = "short", @id = Model.Type + "_GenericBedMobilitySitStandSitAssist" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistiveDevice">Sup to Sit</label>
            <div class="fr ar">
                <label>Assistive Device</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice" })%>
                 <br />
                <label>% Assist</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssist"), new { @class = "short", @id = Model.Type + "_GenericBedMobilitySupToSitAssist" })%>
            </div>
        </div>
    </div>
</fieldset>
