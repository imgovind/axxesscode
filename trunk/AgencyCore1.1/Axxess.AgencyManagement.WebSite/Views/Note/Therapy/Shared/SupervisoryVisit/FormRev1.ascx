﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] superVisory = data.AnswerArray("SuperVisory"); %>
<fieldset>
	<legend>Supervisory Visit</legend>
	<div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_SuperVisory", "1", superVisory.Contains("1"), "OT Assistant")%>
				<%= Html.CheckgroupOption(Model.Type + "_SuperVisory", "2", superVisory.Contains("2"), "Aide")%>
				<%= Html.CheckgroupOption(Model.Type + "_SuperVisory", "3", superVisory.Contains("3"), "Present")%>
				<%= Html.CheckgroupOption(Model.Type + "_SuperVisory", "4", superVisory.Contains("4"), "Not Present")%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Observation of</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericSupervisoryObservation", data.AnswerOrEmptyString("GenericSupervisoryObservation"), new { })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Teaching/Training of</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericSupervisoryTeaching", data.AnswerOrEmptyString("GenericSupervisoryTeaching"), new { })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Care plan reviewed/revised with assistant/aide during this visit</label>
			<div class="checkgroup two-wide">
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericSupervisoryCarePLanReviewed", "1", data.AnswerOrEmptyString("GenericSupervisoryCarePLanReviewed").Equals("1"), true, "Yes", Model.Type + "_GenericSupervisorySpecify", data.AnswerOrEmptyString("GenericSupervisorySpecify"))%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericSupervisoryCarePLanReviewed", "0", data.AnswerOrEmptyString("GenericSupervisoryCarePLanReviewed").Equals("0"), "No")%>
			</div>
		</div>
		<div class="row">
			<label class="fl">If OT assistant/aide is not present, then specify the date he/she was contacted regarding the updated care plan.</label>
			<div class="fr">
				<input type="text" class="date-picker shortdate" name="<%= Model.Type %>_GenericSupervisoryDate" value="<%= data.AnswerOrEmptyString("GenericSupervisoryDate") %>" id="<%= Model.Type %>_GenericSupervisoryDate" />
			</div>
		</div>
	</div>
</fieldset>