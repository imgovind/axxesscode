﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
 <legend>Transfer</legend>
<div class="column">
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferBedChairAssistiveDevice">Bed-Chair</label>
            <div class="fr ar">
                <label>Assistive Device</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferBedChairAssistiveDevice", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericTransferBedChairAssistiveDevice" })%>
                 <br />
                <label>% Assist</label>
                <%= Html.TextBox(Model.Type+"_GenericTransferBedChairAssist", data.AnswerOrEmptyString("GenericTransferBedChairAssist"), new { @class = "short", @id = Model.Type+"_GenericTransferBedChairAssist" })%> 
            </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferChairBedAssistiveDevice">Chair-Bed</label>
            <div class="fr ar">
               <label>Assistive Device</label>
               <%= Html.TextBox(Model.Type + "_GenericTransferChairBedAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericTransferChairBedAssistiveDevice" })%>
               <br />
               <label>% Assist</label>
               <%= Html.TextBox(Model.Type + "_GenericTransferChairBedAssist", data.AnswerOrEmptyString("GenericTransferChairBedAssist"), new { @class = "short", @id = Model.Type + "_GenericTransferChairBedAssist" })%> 
                
            </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferChairToWCAssistiveDevice">Chair to W/C</label>
            <div class="fr ar">
                <label>Assistive Device</label>
               <%= Html.TextBox(Model.Type+"_GenericTransferChairToWCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice"), new { @class = "short", @id = Model.Type+"_GenericTransferChairToWCAssistiveDevice" })%>
                 <br />
               <label>% Assist</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferChairToWCAssist", data.AnswerOrEmptyString("GenericTransferChairToWCAssist"), new { @class = "short", @id = Model.Type + "_GenericTransferChairToWCAssist" })%>
            </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistiveDevice">Toilet or BSC</label>
            <div class="fr ar">
                <label>Assistive Device</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice" })%>
                <br />
               <label>% Assist</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferToiletOrBSCAssist", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssist"), new { @class = "short", @id = Model.Type + "_GenericTransferToiletOrBSCAssist" })%> 
               
            </div>
        </div>
         </div>
        <div class="column">
         <div class="row">
           <label for="<%= Model.Type %>_GenericTransferCanVanAssistiveDevice">Car/Van</label>
            <div class="fr ar">
                <label>Assistive Device</label>
               <%= Html.TextBox(Model.Type + "_GenericTransferCanVanAssistiveDevice", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericTransferCanVanAssistiveDevice" })%>
                 <br />
                <label>% Assist</label>
              <%= Html.TextBox(Model.Type + "_GenericTransferCanVanAssist", data.AnswerOrEmptyString("GenericTransferCanVanAssist"), new { @class = "short", @id = Model.Type + "_GenericTransferCanVanAssist" })%> 
               
            </div>
        </div>
         <div class="row">
           <label for="<%= Model.Type %>_GenericTransferTubShowerAssistiveDevice">Tub/Shower</label>
            <div class="fr ar">
                <label>Assistive Device</label>
               <%= Html.TextBox(Model.Type + "_GenericTransferTubShowerAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice" })%>
                <br />
               <label>% Assist</label>
               <%= Html.TextBox(Model.Type + "_GenericTransferTubShowerAssist", data.AnswerOrEmptyString("GenericTransferTubShowerAssist"), new { @class = "short", @id = Model.Type + "_GenericTransferTubShowerAssist" })%>
               
            </div>
        </div>
        
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic">Sitting Balance</label>
            <div class="fr ar">
                 <label>Static</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceStatic", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic"), new { @class = "short", @id = Model.Type + "_GenericTransferSittingBalanceStatic" })%>
                <br />
               <label>% Dynamic</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceDynamic", data.AnswerOrEmptyString("GenericTransferSittingBalanceDynamic"), new { @class = "short", @id = Model.Type + "_GenericTransferSittingBalanceDynamic" })%> 
            </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_GenericTransferStandBalanceStatic">Stand Balance</label>
            <div class="fr ar">
                <label>Static</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceStatic", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic"), new { @class = "short", @id = Model.Type + "_GenericTransferStandBalanceStatic" })%>
                 <br />
               <label>% Dynamic</label>
                <%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceDynamic", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic"), new { @class = "short", @id = Model.Type + "_GenericTransferStandBalanceDynamic" })%> 
            </div>
        </div>       
</div>
</fieldset>
