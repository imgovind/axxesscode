﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Transfer</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsTransferApply", Model.Type + "IsTransferApply", "1", data.AnswerOrEmptyString("IsTransferApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferBedChairAssistance">Bed-Chair</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferBedChairAssistance", data.AnswerOrEmptyString("GenericTransferBedChairAssistance"), new { @id = Model.Type + "_GenericTransferBedChairAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferBedChairAssistiveDevice", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice"), new { @id = Model.Type + "_GenericTransferBedChairAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferChairBedAssistance">Chair-Bed</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairBedAssistance", data.AnswerOrEmptyString("GenericTransferChairBedAssistance"), new { @id = Model.Type + "_GenericTransferChairBedAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairBedAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairBedAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferChairToWCAssistance">Chair to W/C</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairToWCAssistance", data.AnswerOrEmptyString("GenericTransferChairToWCAssistance"), new { @id = Model.Type + "_GenericTransferChairToWCAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairToWCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistance">Toilet or BSC</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferToiletOrBSCAssistance", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferCanVanAssistance">Car/Van</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferCanVanAssistance", data.AnswerOrEmptyString("GenericTransferCanVanAssistance"), new { @id = Model.Type + "_GenericTransferCanVanAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferCanVanAssistiveDevice", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice"), new { @id = Model.Type + "_GenericTransferCanVanAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferTubShowerAssistance">Tub/Shower</label>
                <div class="fr ar">
                    <label>Assistance</label>
                    <%= Html.TherapyAssistance(Model.Type + "_GenericTransferTubShowerAssistance", data.AnswerOrEmptyString("GenericTransferTubShowerAssistance"), new { @id = Model.Type + "_GenericTransferTubShowerAssistance" })%>
                    <br />
                    <label>Assistive Device</label>
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTubShowerAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice"), new { @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic">Sitting Balance</label>
                <div class="fr ar">
                    <label>Static</label>
                    <%= Html.StaticBalance(Model.Type + "_GenericTransferSittingBalanceStatic", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic"), new { @id = Model.Type + "_GenericTransferSittingBalanceStatic" })%>
                    <br />
                    <label>Dynamic</label>
                    <%= Html.DynamicBalance(Model.Type + "_GenericTransferSittingDynamicAssist", data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist"), new { @id = Model.Type + "_GenericTransferSittingDynamicAssist" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferStandBalanceStatic">Stand Balance</label>
                <div class="fr ar">
                    <label>Static</label>
                    <%= Html.StaticBalance(Model.Type + "_GenericTransferStandBalanceStatic", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic"), new { @id = Model.Type + "_GenericTransferStandBalanceStatic" })%>
                    <br />
                    <label>Dynamic</label>
                    <%= Html.DynamicBalance(Model.Type + "_GenericTransferStandBalanceDynamic", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic"), new { @id = Model.Type + "_GenericTransferStandBalanceDynamic" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericTransferComment">Comments</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_GenericTransferTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericTransferComment", data.AnswerOrEmptyString("GenericTransferComment"), new { @id = Model.Type + "_GenericTransferComment" })%>
                </div>
            </div>
        </div>
    </div>
</fieldset>
