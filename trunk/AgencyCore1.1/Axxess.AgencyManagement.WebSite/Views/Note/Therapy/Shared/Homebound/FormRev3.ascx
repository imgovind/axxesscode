﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<% string[] genericHomeBoundStatus = Model.Questions.AnswerOrEmptyString("GenericHomeBoundStatus") != "" ? Model.Questions["GenericHomeBoundStatus"].Answer.Split(',') : Model.Questions.AnswerArray("GenericHomeboundStatusAssist"); %>
<fieldset>
    <legend>Homebound Reason</legend>
    <% if (Model.IsNA)
       { %>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsHomeboundApply", Model.Type + "_IsHomeboundApply", "1", Model.Questions.AnswerOrEmptyString("IsHomeboundApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <% } %>
    <div id="<%= Model.Type %>_HomeboundContainer" class="collapsible-container">
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup two-wide">
                    <input type="hidden" name="<%= Model.Type %>_GenericHomeBoundStatus" value="" />
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "7", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("7"), "Requires considerable and taxing effort.") %>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "8", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("8"), "Medical restriction.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "1", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1"), "Needs assist with transfer.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "2", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2"), "Needs assist with ambulation.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "3", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3"), "Needs assist leaving the home.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "4", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4"), "Unable to be up for long period.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "5", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("5"), "Severe SOB upon exertion.")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "6", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("6"), "Unsafe to go out of home alone.")%>
                </div>
            </div>
        </div>
    </div>
</fieldset>
