﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Homebound Status</legend>
	<div class="column">
		<div class="row">
			<div class="checkgroup two-wide">
				<input type="hidden" name="<%= Model.Type %>_GenericHomeboundStatusAssist" value="" />
				<% string[] homeboundStatusAssist = data.AnswerArray("GenericHomeboundStatusAssist"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundStatusAssist", "1", homeboundStatusAssist.Contains("1"), "Gait")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundStatusAssist", "2", homeboundStatusAssist.Contains("2"), "Leaving the Home")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundStatusAssist", "3", homeboundStatusAssist.Contains("3"), "Transfers")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericHomeboundStatusAssist", "4", homeboundStatusAssist.Contains("4"), "SOB/Endurance")%>
			</div>
		</div>
	</div>
</fieldset>
