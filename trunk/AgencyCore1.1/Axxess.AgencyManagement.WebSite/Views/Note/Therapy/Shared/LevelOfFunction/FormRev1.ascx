﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Prior Level Of Function</legend>
            <div class="column">
                <div class="row">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.Type + "_IsLOFApply", Model.Type + "IsLOFApply", "1", data.AnswerOrEmptyString("IsLOFApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
                    </ul>
                </div>
            </div>
            <div class="collapsible-container">
                <div class="column">
                    <div class="row">
                        <div class="template-text">
                            <%= Html.ToggleTemplates(Model.Type + "_PriorFunctionalTemplates")%>
                            <%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus" })%>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
	<div>
	    <fieldset>
	        <legend>Current Level Of Function</legend>
	        <div class="column">
	            <div class="row">
	                <ul class="checkgroup one-wide">
	                    <input type="hidden" name="<%= Model.Type %>_IsLOFCurrentApply" value="2" />
	                    <%= Html.CheckgroupOption(Model.Type + "_IsLOFCurrentApply", Model.Type + "IsLOFCurrentApply", "1", (!data.ContainsKey("IsLOFCurrentApply") && data.AnswerOrEmptyString("IsLOFApply").Contains("1")) || data.AnswerOrEmptyString("IsLOFCurrentApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
	                </ul>
	            </div>
	        </div>
	        <div class="collapsible-container">
	            <div class="column">
	                <div class="row">
	                    <div class="template-text">
	                        <%= Html.ToggleTemplates(Model.Type + "_CurrentFunctionalTemplates")%>
	                        <%= Html.TextArea(Model.Type + "_GenericCurrentFunctionalStatus", data.AnswerOrEmptyString("GenericCurrentFunctionalStatus"), new { @id = Model.Type + "_GenericCurrentFunctionalStatus"})%>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </fieldset>
	</div>
</div>