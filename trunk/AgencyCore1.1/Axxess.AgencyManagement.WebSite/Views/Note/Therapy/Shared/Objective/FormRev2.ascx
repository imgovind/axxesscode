﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  if (Model.Type.Substring(0, 1).Equals("O"))
	{ %>
<fieldset>
	<legend>Objective</legend>
	<div class="wide-column">
		<div class="row">
			<label class="fl">Objective: Skilled Intervention consisted of the following: Level of Assist</label>
			<div class="checkgroup four-wide">
				<% string[] objectiveLevelOfAssist = Model.Questions.AnswerArray("GenericObjectiveLevelOfAssist"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "1", objectiveLevelOfAssist.Contains("1"), "IND")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "2", objectiveLevelOfAssist.Contains("2"), "SUP")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "3", objectiveLevelOfAssist.Contains("3"), "SBA")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "4", objectiveLevelOfAssist.Contains("4"), "CGA")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "5", objectiveLevelOfAssist.Contains("5"), "MIN")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "6", objectiveLevelOfAssist.Contains("6"), "MOD")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "7", objectiveLevelOfAssist.Contains("7"), "MAX")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "8", objectiveLevelOfAssist.Contains("8"), "DEP")%>
			</div>
		</div>
	</div>
</fieldset>
<%  }
	else if (Model.Type.Substring(0, 1).Equals("P"))
	{ %>
<fieldset>
	<legend>Objective</legend>
	<% if (Model.IsNA)
	{ %>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsObjectiveApply", Model.Type + "_IsObjectiveApply", "1", Model.Questions.AnswerOrEmptyString("IsObjectiveApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<% } %>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<div class="checkgroup one-wide">
					<% string[] genericTherapeuticExercises = Model.Questions.AnswerArray("GenericTherapeuticExercises"); %>
					<%= Html.CheckgroupOption(Model.Type + "_GenericTherapeuticExercises", "1", genericTherapeuticExercises.Contains("1"), "Therapeutic Exercises")%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericROMTo" class="fl">ROM To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericROMTo", Model.Questions.AnswerOrEmptyString("GenericROMTo"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericROMToReps", Model.Questions.AnswerOrEmptyString("GenericROMToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericActiveTo" class="fl">Active To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericActiveTo", Model.Questions.AnswerOrEmptyString("GenericActiveTo"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericActiveToReps", Model.Questions.AnswerOrEmptyString("GenericActiveToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericAssistive" class="fl">Active/Assistive To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericAssistive", Model.Questions.AnswerOrEmptyString("GenericAssistive"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericAssistiveReps", Model.Questions.AnswerOrEmptyString("GenericAssistiveReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericManual" class="fl">Resistive, Manual, To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericManual", Model.Questions.AnswerOrEmptyString("GenericManual"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericManualReps", Model.Questions.AnswerOrEmptyString("GenericManualReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericResistiveWWeights" class="fl">Resistive, w/Weights, To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericResistiveWWeights", Model.Questions.AnswerOrEmptyString("GenericResistiveWWeights"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericResistiveWWeightsReps", Model.Questions.AnswerOrEmptyString("GenericResistiveWWeightsReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericStretchingTo" class="fl">Stretching To</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericStretchingTo", Model.Questions.AnswerOrEmptyString("GenericStretchingTo"), new { @class = "short" })%>
					x
					<%= Html.TextBox(Model.Type + "_GenericStretchingToReps", Model.Questions.AnswerOrEmptyString("GenericStretchingToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericObjectiveComment">Comment</label>
				<div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_ObjectiveTemplates")%>
					<%= Html.TextArea(Model.Type + "_GenericObjectiveComment", Model.Questions.AnswerOrEmptyString("GenericObjectiveComment"), new { })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
<%  } %>