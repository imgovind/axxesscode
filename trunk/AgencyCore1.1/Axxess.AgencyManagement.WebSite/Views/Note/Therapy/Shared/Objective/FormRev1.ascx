﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (Model.Type.Substring(0, 1).Equals("O") || Model.Type.Equals("COTAVisit")) { %>
<fieldset>
	<legend>Objective</legend>
	<div class="wide-column">
		<div class="row">
			<label class="fl">Objective: Skilled Intervention consisted of the following: Level of Assist</label>
			<div class="checkgroup four-wide">
				<% string[] objectiveLevelOfAssist = data.AnswerArray("GenericObjectiveLevelOfAssist"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "1", objectiveLevelOfAssist.Contains("1"), "IND")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "2", objectiveLevelOfAssist.Contains("2"), "SUP")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "3", objectiveLevelOfAssist.Contains("3"), "SBA")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "4", objectiveLevelOfAssist.Contains("4"), "CGA")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "5", objectiveLevelOfAssist.Contains("5"), "MIN")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "6", objectiveLevelOfAssist.Contains("6"), "MOD")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "7", objectiveLevelOfAssist.Contains("7"), "MAX")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericObjectiveLevelOfAssist", "8", objectiveLevelOfAssist.Contains("8"), "DEP")%>
			</div>
		</div>
	</div>
</fieldset>
<% } else if (Model.Type.Substring(0, 1).Equals("P")) { %>
<fieldset>
	<legend>Objective</legend>
	<div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<% string[] genericTherapeuticExercises = data.AnswerArray("GenericTherapeuticExercises"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTherapeuticExercises", "1", genericTherapeuticExercises.Contains("1"), "Therapeutic Exercises")%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericROMTo" class="fl">ROM To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericROMTo", data.AnswerOrEmptyString("GenericROMTo"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericROMToReps", data.AnswerOrEmptyString("GenericROMToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericActiveTo" class="fl">Active To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericActiveTo", data.AnswerOrEmptyString("GenericActiveTo"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericActiveToReps", data.AnswerOrEmptyString("GenericActiveToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericAssistive" class="fl">Active/Assistive To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericAssistive", data.AnswerOrEmptyString("GenericAssistive"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericAssistiveReps", data.AnswerOrEmptyString("GenericAssistiveReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericManual" class="fl">Resistive, Manual, To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericManual", data.AnswerOrEmptyString("GenericManual"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericManualReps", data.AnswerOrEmptyString("GenericManualReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericResistiveWWeights" class="fl">Resistive, w/Weights, To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericResistiveWWeights", data.AnswerOrEmptyString("GenericResistiveWWeights"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericResistiveWWeightsReps", data.AnswerOrEmptyString("GenericResistiveWWeightsReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericStretchingTo" class="fl">Stretching To</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericStretchingTo", data.AnswerOrEmptyString("GenericStretchingTo"), new { @class = "short" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericStretchingToReps", data.AnswerOrEmptyString("GenericStretchingToReps"), new { @class = "shorter less", @placeholder = "rep(s)" })%>
			</div>
		</div>
	</div>
</fieldset>
<%  } %>