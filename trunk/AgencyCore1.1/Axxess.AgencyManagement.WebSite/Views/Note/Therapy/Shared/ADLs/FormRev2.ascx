﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Functional Mobility</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Bed Mobility</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLBedMobilityAssistance", data.AnswerOrEmptyString("GenericADLBedMobilityAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLBedMobilityAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice"), null)%>
			</div>
		</div>
		<div class="row">
			<label>Bed/WC Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLBedWCTransfersAssistance", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLBedWCTransfersAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedWCTransfersAssistiveDevice"), null)%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Toilet Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletTransferAssistance", data.AnswerOrEmptyString("GenericADLToiletTransferAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice"), null)%>
			</div>
		</div>
		<div class="row">
			<label>Tub/Shower Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice"), null)%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label class="al" for="<%= Model.Type %>_ADLFunctionalComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLFunctionalCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLFunctionalComment", data.AnswerOrEmptyString("ADLFunctionalComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Selfcare/ADL Skills</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLSelfFeeding" class="fl">Self Feeding</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLSelfFeedingAssistance", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance"), new { })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLSelfFeedingAssistiveDevice", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLOralHygiene" class="fl">Oral Hygiene</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLOralHygieneAssistance", data.AnswerOrEmptyString("GenericADLOralHygieneAssistance"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLGrooming" class="fl">Grooming</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLGroomingAssistance", data.AnswerOrEmptyString("GenericADLGroomingAssistance"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLToileting" class="fl">Toileting</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletingAssistance", data.AnswerOrEmptyString("GenericADLToiletingAssistance"), new { })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLUBBathing" class="fl">UB Bathing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLUBBathingAssistance", data.AnswerOrEmptyString("GenericADLUBBathingAssistance"), new { })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLBBathing" class="fl">LB Bathing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLBBathingAssistance", data.AnswerOrEmptyString("GenericADLLBBathingAssistance"), new { })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLUBDressing" class="fl">UB Dressing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLUBdressingAssistance", data.AnswerOrEmptyString("GenericADLUBdressingAssistance"), new { })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBdressingAssistiveDevice"), new { })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLBDressing" class="fl">LB Dressing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLBdressingAssistance", data.AnswerOrEmptyString("GenericADLLBdressingAssistance"), new { })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBdressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBdressingAssistiveDevice"), new { })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_ADLSelfcareComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLSelfcareCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLSelfcareComment", data.AnswerOrEmptyString("ADLSelfcareComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Instrumental ADL</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLHousekeeping" class="fl">Housekeeping</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLHousekeepingAssistance", data.AnswerOrEmptyString("GenericADLHousekeepingAssistance"), new { @id = Model.Type + "_GenericADLHousekeepingAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLHousekeepingAssistiveDevice", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice"), new { @id = Model.Type + "_GenericADLHousekeepingAssistiveDevice" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMealPrep" class="fl">Meal prep</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMealPrepAssistance", data.AnswerOrEmptyString("GenericADLMealPrepAssistance"), new { @id = Model.Type + "_GenericADLMealPrepAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMealPrepAssistiveDevice", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice"), new { @id = Model.Type + "_GenericADLMealPrepAssistiveDevice" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLaundry" class="fl">Laundry</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLaundryAssistance", data.AnswerOrEmptyString("GenericADLLaundryAssistance"), new { @id = Model.Type + "_GenericADLLaundryAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLaundryAssistiveDevice", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice"), new { @id = Model.Type + "_GenericADLLaundryAssistiveDevice" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLTelephoneUse" class="fl">Telephone use</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLTelephoneUseAssistance", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTelephoneUseAssistiveDevice", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistiveDevice" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMoneyManagement" class="fl">Money management</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMoneyManagementAssistance", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMoneyManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistiveDevice" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMedicationManagement" class="fl">Medication management</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMedicationManagementAssistance", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMedicationManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistiveDevice" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_ADLInstrumentalComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLInstrumentalCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLInstrumentalComment", data.AnswerOrEmptyString("ADLInstrumentalComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLSittingBalanceStatic" class="fl">Sitting Balance</label>
			<div class="fr">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericADLSittingBalanceStatic", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic", @class = "fr" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericADLSittingDynamicAssist", data.AnswerOrEmptyString("GenericADLSittingDynamicAssist"), new { @id = Model.Type + "_GenericADLSittingDynamicAssist" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLStandBalanceStatic" class="fl">Stand Balance</label>
			<div class="fr">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericADLStandBalanceStatic", data.AnswerOrEmptyString("GenericADLStandBalanceStatic"), new { @id = Model.Type + "_GenericADLStandBalanceStatic", @class = "fr" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericADLStandBalanceDynamic", data.AnswerOrEmptyString("GenericADLStandBalanceDynamic"), new { @id = Model.Type + "_GenericADLStandBalanceDynamic" })%>
			</div>
		</div>
	</div>
</fieldset>
