﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Functional Mobility</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Bed Mobility</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLBedMobilityAssistance", data.AnswerOrEmptyString("GenericADLBedMobilityAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLBedMobilityAssistiveDevice", data.AnswerOrEmptyString("GenericADLBedMobilityAssistiveDevice"), null)%>
			</div>
		</div>
		<div class="row">
			<label>Bed/WC Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLWCTransfersAssistance", data.AnswerOrEmptyString("GenericADLWCTransfersAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLWCTransfersAssistiveDevice", data.AnswerOrEmptyString("GenericADLWCTransfersAssistiveDevice"), null)%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Toilet Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletTransferAssistance", data.AnswerOrEmptyString("GenericADLToiletTransferAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletTransferAssistiveDevice"), null)%>
			</div>
		</div>
		<div class="row">
			<label>Tub/Shower Transfers</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistance"), null)%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericADLTubShowerTransferAssistiveDevice"), null)%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label class="al" for="<%= Model.Type %>_ADLFunctionalComment">Comment</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLFunctionalCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLFunctionalComment", data.AnswerOrEmptyString("ADLFunctionalComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Selfcare/ADL Skills</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLSelfFeeding" class="fl">Self Feeding</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLSelfFeedingAssistance", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLSelfFeedingAssistanceReps", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLSelfFeedingAssistiveDevice", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLSelfFeedingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLSelfFeedingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLOralHygiene" class="fl">Oral Hygiene</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLOralHygieneAssistance", data.AnswerOrEmptyString("GenericADLOralHygieneAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLOralHygieneAssistanceReps", data.AnswerOrEmptyString("GenericADLOralHygieneAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLOralHygieneAssistiveDevice", data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLOralHygieneAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLOralHygieneAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLGrooming" class="fl">Grooming</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLGroomingAssistance", data.AnswerOrEmptyString("GenericADLGroomingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLGroomingAssistanceReps", data.AnswerOrEmptyString("GenericADLGroomingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLGroomingAssistiveDevice", data.AnswerOrEmptyString("GenericADLGroomingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLGroomingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLGroomingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLToileting" class="fl">Toileting</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLToiletingAssistance", data.AnswerOrEmptyString("GenericADLToiletingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLToiletingAssistanceReps", data.AnswerOrEmptyString("GenericADLToiletingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLToiletingAssistiveDevice", data.AnswerOrEmptyString("GenericADLToiletingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLToiletingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLToiletingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLUBBathing" class="fl">UB Bathing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLUBBathingAssistance", data.AnswerOrEmptyString("GenericADLUBBathingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLUBBathingAssistanceReps", data.AnswerOrEmptyString("GenericADLUBBathingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLUBBathingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLUBBathingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLBBathing" class="fl">LB Bathing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLBBathingAssistance", data.AnswerOrEmptyString("GenericADLLBBathingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLLBBathingAssistanceReps", data.AnswerOrEmptyString("GenericADLLBBathingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBBathingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLLBBathingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLBBathingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLUBDressing" class="fl">UB Dressing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLUBDressingAssistance", data.AnswerOrEmptyString("GenericADLUBDressingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLUBDressingAssistanceReps", data.AnswerOrEmptyString("GenericADLUBDressingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLUBDressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLUBDressingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLUBDressingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLUBDressingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLBDressing" class="fl">LB Dressing</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLBDressingAssistance", data.AnswerOrEmptyString("GenericADLLBDressingAssistance"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLLBDressingAssistanceReps", data.AnswerOrEmptyString("GenericADLLBDressingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLBDressingAssistiveDevice", data.AnswerOrEmptyString("GenericADLLBDressingAssistiveDevice"), new { })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericADLLBDressingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLBDressingAssistiveDeviceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_ADLSelfcareComment">Comment</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLSelfcareCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLSelfcareComment", data.AnswerOrEmptyString("ADLSelfcareComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Instrumental ADL</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLHousekeeping" class="fl">Housekeeping</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLHousekeepingAssistance", data.AnswerOrEmptyString("GenericADLHousekeepingAssistance"), new { @id = Model.Type + "_GenericADLHousekeepingAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLHousekeepingAssistanceReps", data.AnswerOrEmptyString("GenericADLHousekeepingAssistanceReps"), new { @id = Model.Type + "_GenericADLHousekeepingAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLHousekeepingAssistiveDevice", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDevice"), new { @id = Model.Type + "_GenericADLHousekeepingAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLHousekeepingAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLHousekeepingAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLHousekeepingAssistiveDeviceReps" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMealPrep" class="fl">Meal prep</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMealPrepAssistance", data.AnswerOrEmptyString("GenericADLMealPrepAssistance"), new { @id = Model.Type + "_GenericADLMealPrepAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMealPrepAssistanceReps", data.AnswerOrEmptyString("GenericADLMealPrepAssistanceReps"), new { @id = Model.Type + "_GenericADLMealPrepAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMealPrepAssistiveDevice", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDevice"), new { @id = Model.Type + "_GenericADLMealPrepAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMealPrepAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMealPrepAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLMealPrepAssistiveDeviceReps" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLaundry" class="fl">Laundry</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLLaundryAssistance", data.AnswerOrEmptyString("GenericADLLaundryAssistance"), new { @id = Model.Type + "_GenericADLLaundryAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLLaundryAssistanceReps", data.AnswerOrEmptyString("GenericADLLaundryAssistanceReps"), new { @id = Model.Type + "_GenericADLLaundryAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLLaundryAssistiveDevice", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDevice"), new { @id = Model.Type + "_GenericADLLaundryAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLLaundryAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLLaundryAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLLaundryAssistiveDeviceReps" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLTelephoneUse" class="fl">Telephone use</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLTelephoneUseAssistance", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistance"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLTelephoneUseAssistanceReps", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistanceReps"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLTelephoneUseAssistiveDevice", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDevice"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLTelephoneUseAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLTelephoneUseAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLTelephoneUseAssistiveDeviceReps" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMoneyManagement" class="fl">Money management</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMoneyManagementAssistance", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistance"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMoneyManagementAssistanceReps", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistanceReps"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMoneyManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMoneyManagementAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMoneyManagementAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLMoneyManagementAssistiveDeviceReps" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMedicationManagement" class="fl">Medication management</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericADLMedicationManagementAssistance", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistance"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistance" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMedicationManagementAssistanceReps", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistanceReps"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistanceReps" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericADLMedicationManagementAssistiveDevice", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDevice"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistiveDevice" })%>
				x<%= Html.TextBox(Model.Type + "_GenericADLMedicationManagementAssistiveDeviceReps", data.AnswerOrEmptyString("GenericADLMedicationManagementAssistiveDeviceReps"), new { @id = Model.Type + "_GenericADLMedicationManagementAssistiveDeviceReps" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_ADLInstrumentalComment">Comment</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_ADLInstrumentalCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_ADLInstrumentalComment", data.AnswerOrEmptyString("ADLInstrumentalComment"), new { @class = "tall" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLSittingBalanceStatic" class="fl">Sitting Balance</label>
			<div class="fr">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericADLSittingBalanceStatic", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic"), new { @id = Model.Type + "_GenericADLSittingBalanceStatic", @class = "fr" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericADLSittingDynamicAssist", data.AnswerOrEmptyString("GenericADLSittingDynamicAssist"), new { @id = Model.Type + "_GenericADLSittingDynamicAssist" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLStandBalanceStatic" class="fl">Stand Balance</label>
			<div class="fr">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericADLStandBalanceStatic", data.AnswerOrEmptyString("GenericADLStandBalanceStatic"), new { @id = Model.Type + "_GenericADLStandBalanceStatic", @class = "fr" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericADLStandBalanceDynamic", data.AnswerOrEmptyString("GenericADLStandBalanceDynamic"), new { @id = Model.Type + "_GenericADLStandBalanceDynamic" })%>
			</div>
		</div>
	</div>
</fieldset>
