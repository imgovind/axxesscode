﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Functional Mobility</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Bed Mobility</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericADLBedMobilityRolling" class="fl">Rolling</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilityRolling", data.AnswerOrEmptyString("GenericADLBedMobilityRolling"), new { @id = Model.Type + "_GenericADLBedMobilityRolling" })%></div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericADLBedMobilitySupineToSit" class="fl">Supine to Sit</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySupineToSit", data.AnswerOrEmptyString("GenericADLBedMobilitySupineToSit"), new { @id = Model.Type + "_GenericADLBedMobilitySupineToSit" })%></div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericADLBedMobilitySitToStand" class="fl">Sit to Stand</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySitToStand", data.AnswerOrEmptyString("GenericADLBedMobilitySitToStand"), new { @id = Model.Type + "_GenericADLBedMobilitySitToStand" })%></div>
		</div>
		<div class="row">
			<label class="fl">Transfers</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericADLTransfersTubShowerToilet" class="fl">Tub/Shower/Toilet</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLTransfersTubShowerToilet", data.AnswerOrEmptyString("GenericADLTransfersTubShowerToilet"), new { @id = Model.Type + "_GenericADLTransfersTubShowerToilet" })%></div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericADLTransfersBedToChairWheelchair" class="fl">Bed to Chair/Wheelchair</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLTransfersBedToChairWheelchair", data.AnswerOrEmptyString("GenericADLTransfersBedToChairWheelchair"), new { @id = Model.Type + "_GenericADLTransfersBedToChairWheelchair" }) %></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLWCMobility" class="fl">W/C Mobility</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLWCMobility", data.AnswerOrEmptyString("GenericADLWCMobility"), new { @id = Model.Type + "_GenericADLWCMobility" })%></div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Selfcare/ADL Skills</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLBathing" class="fl">Bathing</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLBathing", data.AnswerOrEmptyString("GenericADLBathing"), new { @id = Model.Type + "_GenericADLBathing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLBathing" class="fl">UE Dressing</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.AnswerOrEmptyString("GenericADLUEDressing"), new { @id = Model.Type + "_GenericADLUEDressing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLEDressing" class="fl">LE Dressing</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.AnswerOrEmptyString("GenericADLLEDressing"), new { @id = Model.Type + "_GenericADLLEDressing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLGrooming" class="fl">Grooming</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.AnswerOrEmptyString("GenericADLGrooming"), new { @id = Model.Type + "_GenericADLGrooming" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLToileting" class="fl">Toileting</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLToileting", data.AnswerOrEmptyString("GenericADLToileting"), new { @id = Model.Type + "_GenericADLToileting" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLFeeding" class="fl">Feeding</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.AnswerOrEmptyString("GenericADLFeeding"), new { @id = Model.Type + "_GenericADLFeeding" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="fl">Adaptive Equipment/Assistive Device Use or Needs</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.AnswerOrEmptyString("GenericADLAdaptiveEquipment"), new { @id = Model.Type + "_GenericADLAdaptiveEquipment" })%></div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Instrumental ADL</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMealPrep" class="fl">Meal Prep</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.AnswerOrEmptyString("GenericADLMealPrep"), new { @id = Model.Type + "_GenericADLMealPrep" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLHouseCleaning" class="fl">House Cleaning</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.AnswerOrEmptyString("GenericADLHouseCleaning"), new { @id = Model.Type + "_GenericADLHouseCleaning" })%></div>
		</div>
		<div class="row">
			<label class="fl">Sitting Balance</label>
			<div class="clr"></div>
			<div class="fr ar">
				<label>Static</label>
				<div class="checkgroup fr al long">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceStatic", "2", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("2"), "Good") %>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceStatic", "1", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("1"), "Fair") %>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceStatic", "0", data.AnswerOrEmptyString("GenericADLSittingBalanceStatic").Equals("0"), "Poor") %>
				</div>
				<br />
				<label>Dynamic</label>
				<div class="checkgroup fr al long">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceDynamic", "2", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("2"), "Good")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceDynamic", "1", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("1"), "Fair")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLSittingBalanceDynamic", "0", data.AnswerOrEmptyString("GenericADLSittingBalanceDynamic").Equals("0"), "Poor")%>
				</div>
			</div>
		</div>
		<div class="row">
			<label class="fl">Standing Balance</label>
			<div class="clr"></div>
			<div class="fr ar">
				<label>Static</label>
				<div class="checkgroup fr al long">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceStatic", "2", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("2"), "Good")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceStatic", "1", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("1"), "Fair")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceStatic", "0", data.AnswerOrEmptyString("GenericADLStandingBalanceStatic").Equals("0"), "Poor")%>
				</div>
				<br />
				<label>Dynamic</label>
				<div class="checkgroup fr al long">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceDynamic", "2", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("2"), "Good")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceDynamic", "1", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("1"), "Fair")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericADLStandingBalanceDynamic", "0", data.AnswerOrEmptyString("GenericADLStandingBalanceDynamic").Equals("0"), "Poor")%>
				</div>
			</div>
		</div>
	</div>
</fieldset>