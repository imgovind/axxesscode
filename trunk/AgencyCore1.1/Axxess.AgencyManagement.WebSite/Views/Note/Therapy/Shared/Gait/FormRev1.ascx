﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
 <legend>Gait</legend>
<div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="fl">Level</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericGaitLevelAssist", data.AnswerOrEmptyString("GenericGaitLevelAssist"), new { @id = Model.Type + "_GenericGaitLevelAssist" })%>
                <label>X</label>
                <%= Html.TextBox(Model.Type+"_GenericGaitLevelFeet", data.AnswerOrEmptyString("GenericGaitLevelFeet"), new { @class = "shorter", @id = Model.Type+"_GenericGaitLevelFeet" })%>
                <label>ft</label>
            </div>
        </div>
         <div class="row">
             <label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="fl">Unlevel</label>
             <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelAssist", data.AnswerOrEmptyString("GenericGaitUnLevelAssist"), new { @id = Model.Type + "_GenericGaitUnLevelAssist" })%>
                <label>X</label>
                <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet", data.AnswerOrEmptyString("GenericGaitUnLevelFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitUnLevelFeet" })%>
                <label> ft</label>
            </div>
        </div>
         <div class="row">
             <label for="<%= Model.Type %>_GenericGaitStepStairAssist" class="fl">Step/ Stair</label>
             <div class="fr">
               <%= Html.TextBox(Model.Type + "_GenericGaitStepStairAssist", data.AnswerOrEmptyString("GenericGaitStepStairAssist"), new { @id = Model.Type + "_GenericGaitStepStairAssist" })%>
                <label>X</label>
                 <%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet", data.AnswerOrEmptyString("GenericGaitStepStairFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitStepStairFeet" })%>
                <label> ft</label>
            </div>
        </div>
         <div class="row">
              <label for="<%= Model.Type %>_GenericGaitComment" class="strong">Comments</label>
              <%= Html.TextArea(Model.Type + "_GenericGaitComment", data.AnswerOrEmptyString("GenericGaitComment"), new { @id = Model.Type + "_GenericGaitComment" }) %>
        </div>
</div>
</fieldset>
