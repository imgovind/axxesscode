﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Gait Analysis</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsGaitApply", Model.Type + "IsGaitApply", "1", data.AnswerOrEmptyString("IsGaitApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="fl">Level</label>
                <div class="fr">
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitLevelAssist", data.AnswerOrEmptyString("GenericGaitLevelAssist"), new { @id = Model.Type + "_GenericGaitLevelAssist" })%>
                    <label>X</label>
                    <%= Html.TextBox(Model.Type+"_GenericGaitLevelFeet", data.AnswerOrEmptyString("GenericGaitLevelFeet"), new { @class = "shorter", @id = Model.Type+"_GenericGaitLevelFeet" })%>
                    <label>ft</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="fl">Unlevel</label>
                <div class="fr">
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitUnLevelAssist", data.AnswerOrEmptyString("GenericGaitUnLevelAssist"), new { @id = Model.Type + "_GenericGaitUnLevelAssist" })%>
                    <label>X</label>
                    <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet", data.AnswerOrEmptyString("GenericGaitUnLevelFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitUnLevelFeet" })%>
                    <label>ft</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="fl">Step/ Stair</label>
                <div class="fr">
                    <%= Html.TherapyAssistance(Model.Type + "_GenericGaitStepStairAssist", data.AnswerOrEmptyString("GenericGaitStepStairAssist"), new { @id = Model.Type + "_GenericGaitStepStairAssist" })%>
                    <label>X</label>
                    <%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet", data.AnswerOrEmptyString("GenericGaitStepStairFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitStepStairFeet" })%>
                    <label>ft</label>
                    <br />
                    <div class="checkgroup three-wide long">
                        <% string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
                        <input name="<%= Model.Type %>_GenericGaitStepStairRail" value=" " type="hidden" />
                        <%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "1", genericGaitStepStairRail.Contains("1"), "No Rail")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "2", genericGaitStepStairRail.Contains("2"), "1 Rail")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "3", genericGaitStepStairRail.Contains("3"), "2 Rails")%>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericGaitAnalysisAssistiveDevice" class="fl">Assistive
                    Device</label>
                <div class="fr">
                    <%= Html.TherapyAssistiveDevice(Model.Type + "_GenericGaitAnalysisAssistiveDevice", data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice"), new { @id = Model.Type + "_GenericGaitAnalysisAssistiveDevice" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericGaitComment">Gait Quality/Deviation</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_GaitCommentTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericGaitComment", data.AnswerOrEmptyString("GenericGaitComment"), new { @id = Model.Type + "_GenericGaitComment" })%>
                </div>
            </div>
        </div>
    </div>
</fieldset>

