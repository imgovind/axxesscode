﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Short Term Goals</legend>
	<div class="column">
		<div class="row">
			<table style="width: 100%; margin: 0; padding: 0 1em;">
				<colgroup>
					<col style="width:10%;"/>
					<col style="width:45%;"/>
					<col style="width:45%;"/>
				</colgroup>
				<thead>
					<tr>
						<th></th>
						<th class="ac">Goal</th>
						<th class="ac">Time Frame</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label>1.</label></td>
						<td class="ac"> 
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal1", data.AnswerOrEmptyString("GenericShortTermGoal1"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal1" }) %>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal1TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal1TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal1TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>2.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal2", data.AnswerOrEmptyString("GenericShortTermGoal2"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal2" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal2TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal2TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal2TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>3.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal3", data.AnswerOrEmptyString("GenericShortTermGoal3"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal3" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal3TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal3TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal3TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>4.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal4", data.AnswerOrEmptyString("GenericShortTermGoal4"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal4" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal4TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal4TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal4TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>5.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal5", data.AnswerOrEmptyString("GenericShortTermGoal5"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal5" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericShortTermGoal5TimeFrame", data.AnswerOrEmptyString("GenericShortTermGoal5TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericShortTermGoal5TimeFrame" })%>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericShortTermFrequency" class="float-left">Frequency</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericShortTermFrequency", data.AnswerOrEmptyString("GenericShortTermFrequency"), new { @id = Model.Type + "_GenericShortTermFrequency" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericShortTermDuration" class="float-left">Duration</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericShortTermDuration", data.AnswerOrEmptyString("GenericShortTermDuration"), new { @id = Model.Type + "_GenericShortTermDuration" })%>
			</div>
		</div>
	</div>
</fieldset>
