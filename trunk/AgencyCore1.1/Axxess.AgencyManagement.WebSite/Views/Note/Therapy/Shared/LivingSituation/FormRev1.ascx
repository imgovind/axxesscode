﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituationSupport = data.AnswerArray("GenericLivingSituationSupport"); %>
<fieldset>
	<legend>Living Situation</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Dwelling Level</label>
			<div class="fr">
				<div class="checkgroup two-wide">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLevel", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1"), "One") %>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLevel", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0"), "Multiple") %>
				</div>
			</div>
		</div>
		<div class="row">
			<label>Stairs</label>
			<div class="fr">
				<div class="checkgroup one-wide">
					<div class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1"), new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs1" })%>
							<label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1">Yes</label>
						</div>
						<div class="more">
							<label for="<%= Model.Type %>_GenericDMESuggestion" class="fl"># Steps</label>
							<div class="fr">
								<%= Html.TextBox(Model.Type + "_GenericHomeSafetyEvaluationStairsNumber", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairsNumber"), new { @class = "shorter less", @id = Model.Type + "_GenericHomeSafetyEvaluationStairsNumber" })%>
							</div>
							<div class="clr"></div>
							
						</div>
					</div>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationStairs", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0"), "No")%>
				</div>
			</div>
		</div>
		<div class="row">
			<label>Lives with</label>
			<div class="checkgroup two-wide">
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "2", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2"), "Alone") %>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "1", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1"), "Family") %>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "0", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0"), "Friends")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "3", data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("3"), "Significant Other")%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Support</label>
			<div class="checkgroup one-wide">
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericLivingSituationSupport1' name='{1}_GenericLivingSituationSupport' value='1' type='checkbox' {0} />", genericLivingSituationSupport.Contains("1").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericLivingSituationSupport1">Willing caregiver available</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericLivingSituationSupport2' name='{1}_GenericLivingSituationSupport' value='2' type='checkbox' {0} />", genericLivingSituationSupport.Contains("2").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericLivingSituationSupport2">Limited caregiver support</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericLivingSituationSupport3' name='{1}_GenericLivingSituationSupport' value='3' type='checkbox' {0} />", genericLivingSituationSupport.Contains("3").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericLivingSituationSupport3">No caregiver available</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericHomeSafetyBarriers" class="fl">Home Safety Barriers</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericHomeSafetyBarriers", data.AnswerOrEmptyString("GenericHomeSafetyBarriers"), new { @id = Model.Type + "_GenericHomeSafetyBarriers" })%></div>
		</div>
	</div>
</fieldset>
