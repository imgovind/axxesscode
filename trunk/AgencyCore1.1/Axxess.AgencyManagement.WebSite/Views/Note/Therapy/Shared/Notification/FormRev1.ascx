﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
<legend>Notification</legend>
    <div class="wide-column">
        <div class="row">
                <div class="checkgroup two-wide">
                    <% string[] notification = data.AnswerArray("Notification"); %>
                    <input name="<%= Model.Type %>_Notification" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_Notification", "0", notification.Contains("0"), "Patient agreed to POC")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Notification", "1", notification.Contains("1"), "Verbal order received from physician")%>
                </div>
            </div>
    </div>
</fieldset>
