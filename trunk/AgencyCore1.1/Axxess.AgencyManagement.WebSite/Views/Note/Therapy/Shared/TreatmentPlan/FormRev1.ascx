﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
<legend>Treatment Plan</legend>
<div class="column">
        <div class="row">
           <div class="checkgroup two-wide">
                <% string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
                <%= Html.Hidden(Model.Type + "_GenericTreatmentPlan", string.Empty, new { @id = Model.Type + "_GenericTreatmentPlan" })%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "1", genericTreatmentPlan.Contains("1"), "Thera Ex")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "2", genericTreatmentPlan.Contains("2"), "Bed Mobility Training")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "3", genericTreatmentPlan.Contains("3"), "Transfer Training")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "4", genericTreatmentPlan.Contains("4"), "Balance Training")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "5", genericTreatmentPlan.Contains("5"), "Gait Training")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "6", genericTreatmentPlan.Contains("6"), "HEP")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "7", genericTreatmentPlan.Contains("7"), "Electrotherapy")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "8", genericTreatmentPlan.Contains("8"), "Ultrasound")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "9", genericTreatmentPlan.Contains("9"), "Prosthetic Training")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "10", genericTreatmentPlan.Contains("10"), "Manual Therapy")%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTreatmentPlanOther" class="fl">Other</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"), new { @id = Model.Type + "_GenericTreatmentPlanOther" })%></div>
        </div>
    </div>
    </fieldset>
