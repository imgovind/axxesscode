﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<fieldset>
    <legend>Treatment Plan</legend>
    <div class="wide-column">
        <div class="row">
            <div class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_IsTreatmentApply", Model.Type + "_IsTreatmentApply", "1", data.AnswerOrEmptyString("IsTreatmentApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
            </div>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup two-wide">
                    <input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "1", genericTreatmentPlan.Contains("1"), "Therapeutic exercise") %>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "2", genericTreatmentPlan.Contains("2"), "Bed Mobility Training") %>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "3", genericTreatmentPlan.Contains("3"), "Transfer Training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "4", genericTreatmentPlan.Contains("4"), "Balance Training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "5", genericTreatmentPlan.Contains("5"), "Gait Training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "6", genericTreatmentPlan.Contains("6"), "Neuromuscular re-education")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "7", genericTreatmentPlan.Contains("7"), "Functional mobility training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "8", genericTreatmentPlan.Contains("8"), "Teach safe and effective use of adaptive/assist device")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "9", genericTreatmentPlan.Contains("9"), "Teach safe stair climbing skills")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "10", genericTreatmentPlan.Contains("10"), "Teach fall prevention/safety")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "11", genericTreatmentPlan.Contains("11"), "Establish/upgrade home exercise program")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "12", genericTreatmentPlan.Contains("12"), "Pt/caregiver education/training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "13", genericTreatmentPlan.Contains("13"), "Proprioceptive training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "14", genericTreatmentPlan.Contains("14"), "Postural control training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "15", genericTreatmentPlan.Contains("15"), "Teach energy conservation techniques")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "16", genericTreatmentPlan.Contains("16"), "Relaxation technique")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "17", genericTreatmentPlan.Contains("17"), "Teach safe and effective breathing technique")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "18", genericTreatmentPlan.Contains("18"), "Teach hip precaution")%>
                   
                    <div class="option">
                        <div class="wrapper">
                            <input id="<%= Model.Type %>_GenericTreatmentPlan19" name="<%= Model.Type %>_GenericTreatmentPlan"
                                type="checkbox" value="19" <%= genericTreatmentPlan.Contains("19").ToChecked() %> />
                            <label for="<%= Model.Type %>_GenericTreatmentPlan19">Electrical stimulation</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan19BodyParts">Body Parts</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan19Duration">Duration</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19Duration", data.AnswerOrEmptyString("GenericTreatmentPlan19Duration"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <input id="<%= Model.Type %>_GenericTreatmentPlan20" name="<%= Model.Type %>_GenericTreatmentPlan"
                                type="checkbox" value="20" <%= genericTreatmentPlan.Contains("20").ToChecked() %> />
                            <label for="<%= Model.Type %>_GenericTreatmentPlan20">Ultrasound</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan20BodyParts">Body Parts</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan20Duration">Duration</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Duration", data.AnswerOrEmptyString("GenericTreatmentPlan20Duration"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <input id="<%= Model.Type %>_GenericTreatmentPlan21" name="<%= Model.Type %>_GenericTreatmentPlan"
                                type="checkbox" value="21" <%= genericTreatmentPlan.Contains("21").ToChecked() %> />
                            <label for="<%= Model.Type %>_GenericTreatmentPlan21">TENS</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan21BodyParts">Body Parts</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan21BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan21BodyParts"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                            <label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan21Duration">Duration</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan21Duration", data.AnswerOrEmptyString("GenericTreatmentPlan21Duration"), new { })%>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "22", genericTreatmentPlan.Contains("22"), "Prothetic training")%>
                    <%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "23", genericTreatmentPlan.Contains("23"), "Pulse oximetry PRN")%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTreatmentPlanOther">Other</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_TreatmentPlanTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"), new { @class = "tall" })%>
                </div>
            </div>
            <%if (Model.DisciplineTask == 47)
              { %>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTreatmentPlanFrequencyDuration">Frequency &#38; Duration</label><br />
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_TreatmentPlanTemplates")%>
                    <%= Html.TextArea(Model.Type + "_GenericTreatmentPlanFrequencyDuration", data.AnswerOrEmptyString("GenericTreatmentPlanFrequencyDuration"), new { })%>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</fieldset>
