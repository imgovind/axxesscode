﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Treatment Plan</legend>
	<div class="wide-column">
		<div class="row">
			<% string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
			<input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
			<ul class="checkgroup two-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "1", genericTreatmentPlan.Contains("1"), "Therapeutic exercise") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "2", genericTreatmentPlan.Contains("2"), "Therapeutic activities (reaching, bending etc)") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "3", genericTreatmentPlan.Contains("3"), "Neuromuscular re-education")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "4", genericTreatmentPlan.Contains("4"), "Teach safe and effective use of adaptive/assist device")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "5", genericTreatmentPlan.Contains("5"), "Teach fall prevention/safety")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "6", genericTreatmentPlan.Contains("6"), "Establish/upgrade home exercise program")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "7", genericTreatmentPlan.Contains("7"), "Pt/caregiver education/training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "8", genericTreatmentPlan.Contains("8"), "Sensory integrative techniques")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "9", genericTreatmentPlan.Contains("9"), "Postural control training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "10", genericTreatmentPlan.Contains("10"), "Teach energy conservation techniques")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "11", genericTreatmentPlan.Contains("11"), "Wheelchair management training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "12", genericTreatmentPlan.Contains("12"), "Teach safe and effective breathing technique")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "13", genericTreatmentPlan.Contains("13"), "Teach work simplification")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "14", genericTreatmentPlan.Contains("14"), "Community/work integration")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "15", genericTreatmentPlan.Contains("15"), "Self care management training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "16", genericTreatmentPlan.Contains("16"), "Cognitive skills development/training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "17", genericTreatmentPlan.Contains("17"), "Teach task segmentation")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentPlan", "18", genericTreatmentPlan.Contains("18"), "Manual therapy techniques")%>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.Type %>_GenericTreatmentPlan19" name="<%= Model.Type %>_GenericTreatmentPlan" type="checkbox" value="19" <%= genericTreatmentPlan.Contains("19").ToChecked() %> />
						<label for="<%= Model.Type %>_GenericTreatmentPlan19">Electrical stimulation</label>
					</div>
					<div class="more">
						<label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan19BodyParts">Body Parts</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts"), new { })%></div>
						<div class="clr"></div>
						<label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan19Duration">Duration</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19Duration", data.AnswerOrEmptyString("GenericTreatmentPlan19Duration"), new { })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.Type %>_GenericTreatmentPlan20" name="<%= Model.Type %>_GenericTreatmentPlan" type="checkbox" value="20" <%= genericTreatmentPlan.Contains("20").ToChecked() %> />
						<label for="<%= Model.Type %>_GenericTreatmentPlan20">Ultrasound</label>
					</div>
					<div class="more">
						<label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan20BodyParts">Body Parts</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts"))%></div>
						<div class="clr"></div>
						<label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan20Dosage">Dosage</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Dosage", data.AnswerOrEmptyString("GenericTreatmentPlan20Dosage"))%></div>
						<div class="clr"></div>
						<label class="fl" for="<%= Model.Type %>_GenericTreatmentPlan20Duration">Duration</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Duration", data.AnswerOrEmptyString("GenericTreatmentPlan20Duration"))%></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericTreatmentPlanOther">Other</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_TreatmentPlanTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"), new { })%>
			</div>
		</div>
	</div>
</fieldset>
