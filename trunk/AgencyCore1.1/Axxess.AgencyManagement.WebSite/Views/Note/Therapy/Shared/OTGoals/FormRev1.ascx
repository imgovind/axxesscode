﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>OT Goals</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<% string[] genericOTGoals = data.AnswerArray("GenericOTGoals"); %>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals1' name='{1}_GenericOTGoals' value='1' type='checkbox' {0} />", genericOTGoals.Contains("1").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals1">Patient will be able to perform upper body dressing with <em class="underline">field 1</em> assist utilizing <em class="underline">field 2</em> device within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals1With", data.AnswerOrEmptyString("GenericOTGoals1With"), new { @id = Model.Type + "_GenericOTGoals1With", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals1Utilize", data.AnswerOrEmptyString("GenericOTGoals1Utilize"), new { @id = Model.Type + "_GenericOTGoals1Utilize", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals1Weeks", data.AnswerOrEmptyString("GenericOTGoals1Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals1Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals2' name='{1}_GenericOTGoals' value='2' type='checkbox' {0} />", genericOTGoals.Contains("2").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals2">Patient will be able to perform lower body dressing with <em class="underline">field 1</em> assist utilizing <em class="underline">field 2</em> device within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals2With", data.AnswerOrEmptyString("GenericOTGoals2With"), new { @id = Model.Type + "_GenericOTGoals2With", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals2Utilize", data.AnswerOrEmptyString("GenericOTGoals2Utilize"), new { @id = Model.Type + "_GenericOTGoals2Utilize", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals2Weeks", data.AnswerOrEmptyString("GenericOTGoals2Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals2Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals3' name='{1}_GenericOTGoals' value='3' type='checkbox' {0} />", genericOTGoals.Contains("3").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals3">Patient will be able to perform toilet hygiene with <em class="underline">field 1</em> assist within <em class="underline">field 2</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals3Assist", data.AnswerOrEmptyString("GenericOTGoals3Assist"), new { @id = Model.Type + "_GenericOTGoals3Assist", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals3Weeks", data.AnswerOrEmptyString("GenericOTGoals3Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals3Weeks", @maxlength = "3", @placeholder = "Field 2" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals4' name='{1}_GenericOTGoals' value='4' type='checkbox' {0} />", genericOTGoals.Contains("4").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals4">Patient will be able to perform grooming with <em class="underline">field 1</em> assist within <em class="underline">field 2</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals4Assist", data.AnswerOrEmptyString("GenericOTGoals4Assist"), new { @id = Model.Type + "_GenericOTGoals4Assist", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals4Weeks", data.AnswerOrEmptyString("GenericOTGoals4Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals4Weeks", @maxlength = "3", @placeholder = "Field 2" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals5' name='{1}_GenericOTGoals' value='5' type='checkbox' {0} />", genericOTGoals.Contains("5").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals5">Patient will be able to perform <em class="underline">field 1</em> (tasks) with <em class="underline">field 2</em> assist using <em class="underline">field 3</em> device within <em class="underline">field 4</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals5With", data.AnswerOrEmptyString("GenericOTGoals5With"), new { @id = Model.Type + "_GenericOTGoals5With", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals5Assist", data.AnswerOrEmptyString("GenericOTGoals5Assist"), new { @id = Model.Type + "_GenericOTGoals5Assist", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals5Within", data.AnswerOrEmptyString("GenericOTGoals5Within"), new { @id = Model.Type + "_GenericOTGoals5Within", @maxlength = "30", @placeholder = "Field 3" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals5Weeks", data.AnswerOrEmptyString("GenericOTGoals5Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals5Weeks", @maxlength = "3", @placeholder = "Field 4" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals6' name='{1}_GenericOTGoals' value='6' type='checkbox' {0} />", genericOTGoals.Contains("6").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals6">Improve strength of <em class="underline">field 1</em> to <em class="underline">field 2</em> grade to improve <em class="underline">field 3</em> within <em class="underline">field 4</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals6Of", data.AnswerOrEmptyString("GenericOTGoals6Of"), new { @id = Model.Type + "_GenericOTGoals6Of", @maxlength = "30", @placeholder = "Field 1" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals6To", data.AnswerOrEmptyString("GenericOTGoals6To"), new { @id = Model.Type + "_GenericOTGoals6To", @maxlength = "30", @placeholder = "Field 2" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals6Within", data.AnswerOrEmptyString("GenericOTGoals6Within"), new { @id = Model.Type + "_GenericOTGoals6Within", @maxlength = "30", @placeholder = "Field 3" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals6Weeks", data.AnswerOrEmptyString("GenericOTGoals6Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals6Weeks", @maxlength = "3", @placeholder = "Field 4" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals7' name='{1}_GenericOTGoals' value='7' type='checkbox' {0} />", genericOTGoals.Contains("7").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals7">Increase muscle strength of <em class="underline">field 1</em> to <em class="underline">field 2</em> grade to improve <em class="underline">field 3</em> (task) within <em class="underline">field 4</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals7Skill", data.AnswerOrEmptyString("GenericOTGoals7Skill"), new { @id = Model.Type + "_GenericOTGoals7Skill", @maxlength = "30", @placeholder = "Field 1" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals7Assist", data.AnswerOrEmptyString("GenericOTGoals7Assist"), new { @id = Model.Type + "_GenericOTGoals7Assist", @maxlength = "30", @placeholder = "Field 2" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals7Within", data.AnswerOrEmptyString("GenericOTGoals7Within"), new { @id = Model.Type + "_GenericOTGoals7Within", @maxlength = "30", @placeholder = "Field 3" })%></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals7Weeks", data.AnswerOrEmptyString("GenericOTGoals7Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals7Weeks", @maxlength = "3", @placeholder = "Field 4" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals8' name='{1}_GenericOTGoals' value='8' type='checkbox' {0} />", genericOTGoals.Contains("8").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals8">Increase trunk muscle strength to <em class="underline">field 1</em> to improve postural control and balance necessary to perform <em class="underline">field 2</em> (task) within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals8To", data.AnswerOrEmptyString("GenericOTGoals8To"), new { @id = Model.Type + "_GenericOTGoals8To", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals8Within", data.AnswerOrEmptyString("GenericOTGoals8Within"), new { @id = Model.Type + "_GenericOTGoals8Within", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals8Weeks", data.AnswerOrEmptyString("GenericOTGoals8Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals8Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals9' name='{1}_GenericOTGoals' value='9' type='checkbox' {0} />", genericOTGoals.Contains("9").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals9">Increase trunk muscle strength to <em class="underline">field 1</em> to improve postural control during bed mobility and transfer within <em class="underline">field 2</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals9Within", data.AnswerOrEmptyString("GenericOTGoals9Within"), new { @id = Model.Type + "_GenericOTGoals9Within", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals9Weeks", data.AnswerOrEmptyString("GenericOTGoals9Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals9Weeks", @maxlength = "3", @placeholder = "Field 2" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals10' name='{1}_GenericOTGoals' value='10' type='checkbox' {0} />", genericOTGoals.Contains("10").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals10">Patient will increase ROM of <em class="underline">field 1</em> joint to <em class="underline">field 2</em> degree of <em class="underline">field 3</em> within <em class="underline">field 4</em> weeks to improve <em class="underline">field 5</em> (task).</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals10Device", data.AnswerOrEmptyString("GenericOTGoals10Device"), new { @id = Model.Type + "_GenericOTGoals10Device", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals10Feet", data.AnswerOrEmptyString("GenericOTGoals10Feet"), new { @class = "short", @id = Model.Type + "_GenericOTGoals10Feet", @maxlength = "10", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals10Within", data.AnswerOrEmptyString("GenericOTGoals10Within"), new { @id = Model.Type + "_GenericOTGoals10Within", @maxlength = "30", @placeholder = "Field 3" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals10Weeks", data.AnswerOrEmptyString("GenericOTGoals10Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals10Weeks", @maxlength = "3", @placeholder = "Field 4" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals10DegreeOf", data.AnswerOrEmptyString("GenericOTGoals10DegreeOf"), new { @id = Model.Type + "_GenericOTGoals10DegreeOf", @maxlength = "30", @placeholder = "Field 5" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals11' name='{1}_GenericOTGoals' value='11' type='checkbox' {0} />", genericOTGoals.Contains("11").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals11">Patient/caregiver will be able to perform HEP safely and effectively within <em class="underline">field 1</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals11Weeks", data.AnswerOrEmptyString("GenericOTGoals11Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals11Weeks", @maxlength = "3", @placeholder = "Field 1" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals12' name='{1}_GenericOTGoals' value='12' type='checkbox' {0} />", genericOTGoals.Contains("12").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals12">Patient will learn <em class="underline">field 1</em> techniques to <em class="underline">field 2</em> within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals12Device", data.AnswerOrEmptyString("GenericOTGoals12Device"), new { @id = Model.Type + "_GenericOTGoals12Device", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals12Within", data.AnswerOrEmptyString("GenericOTGoals12Within"), new { @id = Model.Type + "_GenericOTGoals12Within", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals12Weeks", data.AnswerOrEmptyString("GenericOTGoals12Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals12Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals13' name='{1}_GenericOTGoals' value='13' type='checkbox' {0} />", genericOTGoals.Contains("13").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals13">Patient will improve <em class="underline">field 1</em> standardized test score to <em class="underline">field 2</em> to improve <em class="underline">field 3</em> within <em class="underline">field 4</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals13Of", data.AnswerOrEmptyString("GenericOTGoals13Of"), new { @id = Model.Type + "_GenericOTGoals13Of", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals13To", data.AnswerOrEmptyString("GenericOTGoals13To"), new { @id = Model.Type + "_GenericOTGoals13To", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals13Within", data.AnswerOrEmptyString("GenericOTGoals13Within"), new { @id = Model.Type + "_GenericOTGoals13Within", @maxlength = "30", @placeholder = "Field 3" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals13Weeks", data.AnswerOrEmptyString("GenericOTGoals13Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals13Weeks", @maxlength = "3", @placeholder = "Field 4" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals14' name='{1}_GenericOTGoals' value='14' type='checkbox' {0} />", genericOTGoals.Contains("14").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals14">Patient/caregiver will demonstrate proper use of brace/splint within <em class="underline">field 1</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals14Weeks", data.AnswerOrEmptyString("GenericOTGoals14Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals14Weeks", @maxlength = "3", @placeholder = "Field 1" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals15' name='{1}_GenericOTGoals' value='15' type='checkbox' {0} />", genericOTGoals.Contains("15").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals15">Patient/caregiver will demonstrate proper use of wheelchair with <em class="underline">field 1</em> assist within <em class="underline">field 2</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals15To", data.AnswerOrEmptyString("GenericOTGoals15To"), new { @id = Model.Type + "_GenericOTGoals15To", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals15Weeks", data.AnswerOrEmptyString("GenericOTGoals15Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals15Weeks", @maxlength = "3", @placeholder = "Field 2" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals16' name='{1}_GenericOTGoals' value='16' type='checkbox' {0} />", genericOTGoals.Contains("16").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals16">Patient will be able to perform upper body bathing with <em class="underline">field 1</em> assist utilizing <em class="underline">field 2</em> device within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals16With", data.AnswerOrEmptyString("GenericOTGoals16With"), new { @id = Model.Type + "_GenericOTGoals16With", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals16To", data.AnswerOrEmptyString("GenericOTGoals16To"), new { @id = Model.Type + "_GenericOTGoals16To", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals16Weeks", data.AnswerOrEmptyString("GenericOTGoals16Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals16Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericOTGoals17' name='{1}_GenericOTGoals' value='17' type='checkbox' {0} />", genericOTGoals.Contains("17").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericOTGoals17">Patient will be able to perform lower body bathing with <em class="underline">field 1</em> assist utilizing <em class="underline">field 2</em> device within <em class="underline">field 3</em> weeks.</label>
					</div>
					<div class="more">
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals17Joint", data.AnswerOrEmptyString("GenericOTGoals17Joint"), new { @id = Model.Type + "_GenericOTGoals17Joint", @maxlength = "30", @placeholder = "Field 1" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals17Degree", data.AnswerOrEmptyString("GenericOTGoals17Degree"), new { @id = Model.Type + "_GenericOTGoals17Degree", @maxlength = "30", @placeholder = "Field 2" }) %></div>
						<div class="fl"><%= Html.TextBox(Model.Type + "_GenericOTGoals17Weeks", data.AnswerOrEmptyString("GenericOTGoals17Weeks"), new { @class = "numeric short", @id = Model.Type + "_GenericOTGoals17Weeks", @maxlength = "3", @placeholder = "Field 3" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Frequency</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericOTGoalsFrequency", data.AnswerOrEmptyString("GenericOTGoalsFrequency"), new { @id = Model.Type + "_GenericOTGoalsFrequency", @maxlength = "50" })%></div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Duration</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_GenericOTGoalsDuration", data.AnswerOrEmptyString("GenericOTGoalsDuration"), new { @id = Model.Type + "_GenericOTGoalsDuration", @maxlength = "50" })%></div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericOTGoalsComments">Additional goals</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericOTGoalsCommentsTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericOTGoalsComments", data.AnswerOrEmptyString("GenericOTGoalsComments"), new { @id = Model.Type + "_GenericOTGoalsComments" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericRehabPotential">Rehab Potential</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericRehabPotentialTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @id = Model.Type + "_GenericRehabPotential" }) %>
			</div>
		</div>
	</div>
</fieldset>