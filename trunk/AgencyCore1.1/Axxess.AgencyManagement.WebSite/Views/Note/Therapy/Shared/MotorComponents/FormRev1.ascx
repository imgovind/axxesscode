﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericMotorComponents = data.AnswerArray("GenericMotorComponents"); %>
<fieldset>
	<legend>Motor Components (Enter Appropriate Response)</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Fine Motor Coordination</label>
			<div class="fr">
				<label class="fl">Right</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericFineMotorR", data.AnswerOrEmptyString("GenericFineMotorR"), new { @class = "shorter", @id = Model.Type + "_GenericFineMotorR" })%>
					<%= Html.Sensory(Model.Type + "_GenericFineMotorSensoryR", data.AnswerOrEmptyString("GenericFineMotorSensoryR"), new { @id = Model.Type + "_GenericFineMotorSensoryR", @class = "short more" })%>
				</div>
				<div class="clr">
				</div>
				<label class="fl">Left</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericFineMotorL", data.AnswerOrEmptyString("GenericFineMotorL"), new { @class = "shorter", @id = Model.Type + "_GenericFineMotorL" })%>
					<%= Html.Sensory(Model.Type + "_GenericFineMotorSensoryL", data.AnswerOrEmptyString("GenericFineMotorSensoryL"), new { @id = Model.Type + "_GenericFineMotorSensoryL", @class = "short more" })%>
				</div>
			</div>
		</div>
		<div class="row">
			<label class="fl">Gross Motor Coordination</label>
			<div class="fr">
				<label class="fl">Right</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericGrossMotorR", data.AnswerOrEmptyString("GenericGrossMotorR"), new { @class = "shorter", @id = Model.Type + "_GenericGrossMotorR" })%>
					<%= Html.Sensory(Model.Type + "_GenericGrossMotorSensoryR", data.AnswerOrEmptyString("GenericGrossMotorSensoryR"), new { @id = Model.Type + "_GenericGrossMotorSensoryR", @class = "short more" })%>
				</div>
				<div class="clr">
				</div>
				<label class="fl">Left</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericGrossMotorL", data.AnswerOrEmptyString("GenericGrossMotorL"), new { @class = "shorter", @id = Model.Type + "_GenericGrossMotorL" })%>
					<%= Html.Sensory(Model.Type + "_GenericGrossMotorSensoryL", data.AnswerOrEmptyString("GenericGrossMotorSensoryL"), new { @id = Model.Type + "_GenericGrossMotorSensoryL", @class = "short more" })%>
				</div>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "1", genericMotorComponents.Contains("1"), "Right Handed")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "2", genericMotorComponents.Contains("2"), "Left Handed")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "3", genericMotorComponents.Contains("3"), "Orthosis")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "4", genericMotorComponents.Contains("4"), "Used")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericMotorComponents", "5", genericMotorComponents.Contains("5"), "Needed", Model.Type + "_GenericMotorComponentsSpecify", data.AnswerOrEmptyString("GenericMotorComponentsSpecify"))%>
			</ul>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label>Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericMotorTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericMotorComments", data.AnswerOrEmptyString("GenericMotorComments"), new { @id = Model.Type + "_GenericMotorComments" })%>
			</div>
		</div>
	</div>
</fieldset>
