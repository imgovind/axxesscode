﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<fieldset>
	<legend>Functional Limitations/Problem Areas</legend>
	<div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "1", genericFunctionalLimitations.Contains("1"), "Upper body Rom/strength deficit.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "2", genericFunctionalLimitations.Contains("2"), "Pain affecting function.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "3", genericFunctionalLimitations.Contains("3"), "Impaired safety.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "4", genericFunctionalLimitations.Contains("4"), "Difficulty with dressing/grooming/bathing/hygiene/toileting.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "5", genericFunctionalLimitations.Contains("5"), "Difficulty with homemaking skills/money management/meal prep/laundry.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "6", genericFunctionalLimitations.Contains("6"), "Impaired problem solving skills/attention/concentration/sequencing.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "7", genericFunctionalLimitations.Contains("7"), "Impaired coordination.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "8", genericFunctionalLimitations.Contains("8"), "Visual deficit/disturbance/limitation.")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "9", genericFunctionalLimitations.Contains("9"), "Cognition (memory, safety awareness, judgement).")%>
			</div>
		</div>
	</div>
</fieldset>

