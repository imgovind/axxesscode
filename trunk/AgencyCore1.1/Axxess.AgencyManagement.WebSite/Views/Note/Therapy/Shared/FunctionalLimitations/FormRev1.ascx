﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] functionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<fieldset>
	<legend>Functional Limitations</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup two-wide">
                <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "1", functionalLimitations.Contains("1"), "Transfer")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "2", functionalLimitations.Contains("2"), "Gait")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "3", functionalLimitations.Contains("3"), "Strength")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "4", functionalLimitations.Contains("4"), "Bed Mobility")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "5", functionalLimitations.Contains("5"), "Safety Techniques")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "6", functionalLimitations.Contains("6"), "ADL&#8217;s")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "7", functionalLimitations.Contains("7"), "ROM")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "8", functionalLimitations.Contains("8"), "W/C Mobility")%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericFunctionalLimitationsOther" class="fl">Other</label>
			<div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericFunctionalLimitationsOther", data.AnswerOrEmptyString("GenericFunctionalLimitationsOther"), new { })%>
			</div>
		</div>
	</div>
</fieldset>