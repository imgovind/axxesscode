﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  string[] genericFunctionalLimitations = Model.Questions.AnswerArray("GenericFunctionalLimitations"); %>
<fieldset>
	<legend>Functional Limitations</legend>
	<% if (Model.IsNA) { %>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsLimitationsApply", Model.Type + "_IsLimitationsApply", "1", Model.Questions.AnswerOrEmptyString("IsLimitationsApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<% } %>
	<div id="<%=Model.Type %>_LimitationsContainer" class="collapsible-container">
		<div class="wide-column">
			<div class="row">
				<div class="checkgroup two-wide">
					<input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "1", genericFunctionalLimitations.Contains("1"), "ROM/Strength")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "2", genericFunctionalLimitations.Contains("2"), "Pain")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "3", genericFunctionalLimitations.Contains("3"), "Safety Techniques")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "4", genericFunctionalLimitations.Contains("4"), "W/C Mobility")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "5", genericFunctionalLimitations.Contains("5"), "Balance/Gait")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "6", genericFunctionalLimitations.Contains("6"), "Bed Mobility")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "7", genericFunctionalLimitations.Contains("7"), "Transfer")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "8", genericFunctionalLimitations.Contains("8"), "Increased fall risk")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "9", genericFunctionalLimitations.Contains("9"), "Coordination")%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
