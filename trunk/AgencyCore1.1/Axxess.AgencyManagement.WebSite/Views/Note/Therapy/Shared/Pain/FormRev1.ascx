﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Pain Assessment</legend>
	<% if (Model.IsNA) { %>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsPainApply", Model.Type + "IsPainApply", "1", data.AnswerOrEmptyString("IsPainApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<% } %>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericPainAssessmentLocation" class="fl">Pain Location</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.AnswerOrEmptyString("GenericPainAssessmentLocation")) %></div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericPainLevel" class="fl">Pain Level</label>
				<div class="fr">
					<%  var genericPainLevel = new SelectList(new[] {
						new SelectListItem { Text = "0 = No Pain", Value = "0" },
						new SelectListItem { Text = "1", Value = "1" },
						new SelectListItem { Text = "2", Value = "2" },
						new SelectListItem { Text = "3", Value = "3" },
						new SelectListItem { Text = "4", Value = "4" },
						new SelectListItem { Text = "Moderate Pain", Value = "5" },
						new SelectListItem { Text = "6", Value = "6" },
						new SelectListItem { Text = "7", Value = "7" },
						new SelectListItem { Text = "8", Value = "8" },
						new SelectListItem { Text = "9", Value = "9" },
						new SelectListItem { Text = "10", Value = "10" }
					}, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", "0"));%>
					<%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @class = "short" }) %>
				</div>
			</div>
			<div class="row ac">
				<img src="/Images/painscale.png" alt="Pain Scale Image" width="90%" style="max-width: 480px" /><br />
				<em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericPainAssessmentIncreasedBy" class="fl">Increased by</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericPainAssessmentIncreasedBy", data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy"))%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericPainAssessmentRelievedBy" class="fl">Relieved by</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericPainAssessmentRelievedBy", data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy"))%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
