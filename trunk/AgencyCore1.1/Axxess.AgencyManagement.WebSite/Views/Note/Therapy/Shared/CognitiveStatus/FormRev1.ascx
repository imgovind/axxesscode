﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Cognitive Status/Comprehension</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Deficit Area</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionShortTerm" class="fl">Short-term Memory</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionShortTerm", data.AnswerOrEmptyString("GenericComprehensionShortTerm"), new { @id = Model.Type + "_GenericComprehensionShortTerm" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionLongTerm" class="fl">Long-term Memory</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionLongTerm", data.AnswerOrEmptyString("GenericComprehensionLongTerm"), new { @id = Model.Type + "_GenericComprehensionLongTerm" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionConcentration" class="fl">Attention/Concentration</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionConcentration", data.AnswerOrEmptyString("GenericComprehensionConcentration"), new { @id = Model.Type + "_GenericComprehensionConcentration" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionAuditory" class="fl">Auditory Comprehension</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionAuditory", data.AnswerOrEmptyString("GenericComprehensionAuditory"), new { @id = Model.Type + "_GenericComprehensionAuditory" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionVisual" class="fl">Visual Comprehension</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionVisual", data.AnswerOrEmptyString("GenericComprehensionVisual"), new { @id = Model.Type + "_GenericComprehensionVisual" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionSelfControl" class="fl">Self-Control</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionSelfControl", data.AnswerOrEmptyString("GenericComprehensionSelfControl"), new { @id = Model.Type + "_GenericComprehensionSelfControl" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Deficit Area</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionSequencing" class="fl">Sequencing</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionSequencing", data.AnswerOrEmptyString("GenericComprehensionSequencing"), new { @id = Model.Type + "_GenericComprehensionSequencing" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionProblemSolving" class="fl">Problem Solving</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionProblemSolving", data.AnswerOrEmptyString("GenericComprehensionProblemSolving"), new { @id = Model.Type + "_GenericComprehensionProblemSolving" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionCopingSkills" class="fl">Coping Skills</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionCopingSkills", data.AnswerOrEmptyString("GenericComprehensionCopingSkills"), new { @id = Model.Type + "_GenericComprehensionCopingSkills" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionExpressNeeds" class="fl">Able to Express Needs</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionExpressNeeds", data.AnswerOrEmptyString("GenericComprehensionExpressNeeds"), new { @id = Model.Type + "_GenericComprehensionExpressNeeds" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionSafety" class="fl">Safety/Judgment</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionSafety", data.AnswerOrEmptyString("GenericComprehensionSafety"), new { @id = Model.Type + "_GenericComprehensionSafety" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericComprehensionInitiation" class="fl">Initiation of Activity</label>
			<div class="fr">
				<%= Html.Sensory(Model.Type + "_GenericComprehensionInitiation", data.AnswerOrEmptyString("GenericComprehensionInitiation"), new { @id = Model.Type + "_GenericComprehensionInitiation" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label>Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericComprehensionTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericComprehensionComment", data.AnswerOrEmptyString("GenericComprehensionComment"), new { @id = Model.Type + "_GenericComprehensionComment" })%>
			</div>
		</div>
	</div>
</fieldset>