﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSensoryAcuity = data.AnswerArray("GenericSensoryAcuity"); %>
<%  string[] genericSensoryTracking = data.AnswerArray("GenericSensoryTracking"); %>
<%  string[] genericSensoryVisual = data.AnswerArray("GenericSensoryVisual"); %>
<fieldset>
	<legend>Sensory/Perceptual Skills</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericSensoryArea1" class="fl">Area</label>
			<%= Html.TextBox(Model.Type + "_GenericSensoryArea1", data.AnswerOrEmptyString("GenericSensoryArea1"), new { @id = Model.Type + "_GenericSensoryArea1", @class = "fr" })%>
		</div>
		<div class="sub row">
			<label class="fl">Sharp/Dull</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensorySharpRight1" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpRight1", data.AnswerOrEmptyString("GenericSensorySharpRight1"), new { @id = Model.Type + "_GenericSensorySharpRight1", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensorySharpLeft1" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft1", data.AnswerOrEmptyString("GenericSensorySharpLeft1"), new { @id = Model.Type + "_GenericSensorySharpLeft1", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Light/Firm Touch</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryLightRight1" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightRight1", data.AnswerOrEmptyString("GenericSensoryLightRight1"), new { @id = Model.Type + "_GenericSensoryLightRight1", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryLightLeft1" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft1", data.AnswerOrEmptyString("GenericSensoryLightLeft1"), new { @id = Model.Type + "_GenericSensoryLightLeft1", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Proprioception</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionRight1" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight1", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight1"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight1", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionLeft1" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft1", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft1"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft1", @class = "fr" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSensoryArea2" class="fl">Area</label>
			<%= Html.TextBox(Model.Type + "_GenericSensoryArea2", data.AnswerOrEmptyString("GenericSensoryArea2"), new { @id = Model.Type + "_GenericSensoryArea2", @class = "fr" })%>
		</div>
		<div class="sub row">
			<label class="fl">Sharp/Dull</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensorySharpRight2" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpRight2", data.AnswerOrEmptyString("GenericSensorySharpRight2"), new { @id = Model.Type + "_GenericSensorySharpRight2", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensorySharpLeft2" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft2", data.AnswerOrEmptyString("GenericSensorySharpLeft2"), new { @id = Model.Type + "_GenericSensorySharpLeft2", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Light/Firm Touch</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryLightRight2" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightRight2", data.AnswerOrEmptyString("GenericSensoryLightRight2"), new { @id = Model.Type + "_GenericSensoryLightRight2", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryLightLeft2" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft2", data.AnswerOrEmptyString("GenericSensoryLightLeft2"), new { @id = Model.Type + "_GenericSensoryLightLeft2", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Proprioception</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionRight2" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight2", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight2"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight2", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionLeft2" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft2", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft2"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft2", @class = "fr" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericSensoryArea3" class="fl">Area</label>
			<%= Html.TextBox(Model.Type + "_GenericSensoryArea3", data.AnswerOrEmptyString("GenericSensoryArea3"), new { @id = Model.Type + "_GenericSensoryArea3", @class = "fr" })%>
		</div>
		<div class="sub row">
			<label class="fl">Sharp/Dull</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensorySharpRight3" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpRight3", data.AnswerOrEmptyString("GenericSensorySharpRight3"), new { @id = Model.Type + "_GenericSensorySharpRight3", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensorySharpLeft3" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft3", data.AnswerOrEmptyString("GenericSensorySharpLeft3"), new { @id = Model.Type + "_GenericSensorySharpLeft3", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Light/Firm Touch</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryLightRight3" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightRight3", data.AnswerOrEmptyString("GenericSensoryLightRight3"), new { @id = Model.Type + "_GenericSensoryLightRight3", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryLightLeft3" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft3", data.AnswerOrEmptyString("GenericSensoryLightLeft3"), new { @id = Model.Type + "_GenericSensoryLightLeft3", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Proprioception</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionRight3" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight3", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight3"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight3", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionLeft3" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft3", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft3"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft3", @class = "fr" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSensoryArea4" class="fl">Area</label>
			<%= Html.TextBox(Model.Type + "_GenericSensoryArea4", data.AnswerOrEmptyString("GenericSensoryArea4"), new { @id = Model.Type + "_GenericSensoryArea4", @class = "fr" })%>
		</div>
		<div class="sub row">
			<label class="fl">Sharp/Dull</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensorySharpRight4" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpRight4", data.AnswerOrEmptyString("GenericSensorySharpRight4"), new { @id = Model.Type + "_GenericSensorySharpRight4", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensorySharpLeft4" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensorySharpLeft4", data.AnswerOrEmptyString("GenericSensorySharpLeft4"), new { @id = Model.Type + "_GenericSensorySharpLeft4", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Light/Firm Touch</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryLightRight4" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightRight4", data.AnswerOrEmptyString("GenericSensoryLightRight4"), new { @id = Model.Type + "_GenericSensoryLightRight4", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryLightLeft4" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryLightLeft4", data.AnswerOrEmptyString("GenericSensoryLightLeft4"), new { @id = Model.Type + "_GenericSensoryLightLeft4", @class = "fr" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Proprioception</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionRight4" class="fl">Right</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionRight4", data.AnswerOrEmptyString("GenericSensoryProprioceptionRight4"), new { @id = Model.Type + "_GenericSensoryProprioceptionRight4", @class = "fr" })%>
				<div class="clr"></div>
				<label for="<%= Model.Type %>_GenericSensoryProprioceptionLeft4" class="fl">Left</label>
				<%= Html.Sensory(Model.Type + "_GenericSensoryProprioceptionLeft4", data.AnswerOrEmptyString("GenericSensoryProprioceptionLeft4"), new { @id = Model.Type + "_GenericSensoryProprioceptionLeft4", @class = "fr" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Visual Skills</label>
		</div>
		<div class="sub row">
			<label class="fl">Acuity</label>
			<div class="fr">
				<ul class="checkgroup two-wide longest">
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryAcuity", "1", genericSensoryAcuity.Contains("1"), "Intact")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryAcuity", "2", genericSensoryAcuity.Contains("2"), "Impaired")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryAcuity", "3", genericSensoryAcuity.Contains("3"), "Double")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryAcuity", "4", genericSensoryAcuity.Contains("4"), "Blurred")%>
				</ul>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Tracking</label>
			<div class="fr">
				<ul class="checkgroup two-wide longest">
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryTracking", "1", genericSensoryTracking.Contains("1"), "Unilaterally")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryTracking", "2", genericSensoryTracking.Contains("2"), "Bilaterally")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryTracking", "3", genericSensoryTracking.Contains("3"), "Smooth")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryTracking", "4", genericSensoryTracking.Contains("4"), "Jumpy")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryTracking", "5", genericSensoryTracking.Contains("5"), "Not Tracking")%>
				</ul>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Visual Field Cut or Neglect Suspected</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryVisual", "1", genericSensoryVisual.Contains("1"), "Right")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryVisual", "2", genericSensoryVisual.Contains("2"), "Left")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label>Impacting Function</label>
			<ul class="checkgroup two-wide">
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericSensoryImpactingFunction", "1", data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("1"), true, "Yes", Model.Type + "_GenericSensoryImpactingFunctionSpecify", data.AnswerOrEmptyString("GenericSensoryImpactingFunctionSpecify"))%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSensoryImpactingFunction", "0", data.AnswerOrEmptyString("GenericSensoryImpactingFunction").Equals("0"), "No")%>
			</ul>
		</div>
		<div class="row">
			<label>Referral Needed</label>
			<ul class="checkgroup two-wide">
				<li class="option">
					<div class="wrapper">
						<%= Html.RadioButton(Model.Type + "_GenericSensoryReferralNeeded", "1", data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("1"), new { @id = Model.Type + "_GenericSensoryReferralNeeded1" })%>
						<label for="<%= Model.Type %>_GenericSensoryReferralNeeded1">Yes</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericSensoryReferralNeededContact" class="fl">Who was contacted?</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSensoryReferralNeededContact", data.AnswerOrEmptyString("GenericSensoryReferralNeededContact"), new { @id = Model.Type + "_GenericSensoryReferralNeededContact" })%>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericSensoryReferralNeeded", "0", data.AnswerOrEmptyString("GenericSensoryReferralNeeded").Equals("0"), "No")%>
			</ul>
		</div>
	</div>
</fieldset>