﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<fieldset class="ac">
	<legend>Functional Mobility Key</legend>
	<div class="column narrowest">
		<div class="row">
			I = Independent
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			MI = Modified Independent
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			S = Supervision
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			VC = Verbal Cue
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			CGA = Contact Guard Assist
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			Min A = 25% Assist
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			Mod A = 50% Assist
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			Max A = 75% Assist
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			Total = 100% Assist
		</div>
	</div>
	<div class="column narrowest">
		<div class="row">
			Not Tested
		</div>
	</div>
</fieldset>
