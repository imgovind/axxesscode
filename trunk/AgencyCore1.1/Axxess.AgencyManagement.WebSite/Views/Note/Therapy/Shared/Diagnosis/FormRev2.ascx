﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Medical Diagnosis</legend>
	<% if (Model.IsNA) { %>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsMedicalApply", Model.Type + "IsMedicalApply", "1", data.AnswerOrEmptyString("IsMedicalApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<% } %>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label class="fl">Medical Diagnosis</label>
				<div class="fr ar">
					<%= Html.TextBox(Model.Type + "_GenericMedicalDiagnosis", data.AnswerOrEmptyString("GenericMedicalDiagnosis"), new { @id = Model.Type + "_GenericMedicalDiagnosis" }) %>
					<br />
					<label for="<%= Model.Type %>_MedicalDiagnosisDate">Onset</label>
					<input type="text" class="date-picker shortdate" name="<%= Model.Type %>_MedicalDiagnosisDate" value="<%= data.AnswerOrEmptyString("MedicalDiagnosisDate") %>" id="<%= Model.Type %>_MedicalDiagnosisDate" />
				</div>
			</div>
			<div class="row">
				<label class="fl"><% if (Model.Type.Contains("PT")) { %> PT Diagnosis <% } else if (Model.Type.Contains("OT")) { %> OT Diagnosis <% } %></label>
				<div class="fr ar">
					<%= Html.TextBox(Model.Type + "_GenericTherapyDiagnosis", data.AnswerOrEmptyString("GenericTherapyDiagnosis"), new { @id = Model.Type + "_GenericTherapyDiagnosis" })%>
					<br />
					<label for="<%= Model.Type %>_TherapyDiagnosisDate">Onset</label>
					<input type="text" class="date-picker shortdate" name="<%= Model.Type %>_TherapyDiagnosisDate" value="<%= data.AnswerOrEmptyString("TherapyDiagnosisDate") %>" id="<%= Model.Type %>_TherapyDiagnosisDate" />
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericMedicalDiagnosisComment">Comments</label>
				<div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_MedicalDiagnosisTemplates")%>
					<%= Html.TextArea(Model.Type + "_GenericMedicalDiagnosisComment", data.AnswerOrEmptyString("GenericMedicalDiagnosisComment"), new { @id = Model.Type + "_GenericMedicalDiagnosisComment" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
