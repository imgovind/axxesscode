﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] medicalDiagnosisOnset = data.AnswerArray("GenericMedicalDiagnosisOnset"); %>
<%  string[] therapyDiagnosisOnset = data.AnswerArray("GenericTherapyDiagnosisOnset"); %>
<fieldset>
	<legend>Diagnosis</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="fl">Medical Diagnosis</label>
			<div class="fr ar">
				<%= Html.TextBox(Model.Type + "_GenericMedicalDiagnosis", data.AnswerOrEmptyString("GenericMedicalDiagnosis"), new { @id = Model.Type + "_GenericMedicalDiagnosis" }) %><br />
				<input type="hidden" name="<%= Model.Type %>_GenericMedicalDiagnosisOnset" value="" />
				<%= string.Format("<input id='{1}_GenericMedicalDiagnosisOnset1' name='{1}_GenericMedicalDiagnosisOnset' value='1' type='checkbox' {0} />", medicalDiagnosisOnset.Contains("1").ToChecked(), Model.Type) %>
				<label for="<%= Model.Type %>_GenericMedicalDiagnosisOnset1" class="fl">Onset</label>
				<input type="text" class="date-picker shortdate" name="<%= Model.Type %>_MedicalDiagnosisDate" value="<%= data.AnswerOrDefault("MedicalDiagnosisDate", DateTime.Now.ToShortDateString()) %>" id="<%= Model.Type %>_MedicalDiagnosisDate" />
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="fl">Therapy Diagnosis</label>
			<div class="fr ar">
				<%= Html.TextBox(Model.Type + "_GenericTherapyDiagnosis", data.AnswerOrEmptyString("GenericTherapyDiagnosis"), new { @id = Model.Type + "_GenericTherapyDiagnosis" })%><br />
				<input type="hidden" name="<%= Model.Type %>_GenericTherapyDiagnosisOnset" value="" />
				<%= string.Format("<input id='{1}_GenericTherapyDiagnosisOnset1' name='{1}_GenericTherapyDiagnosisOnset' value='1' type='checkbox' {0} />", therapyDiagnosisOnset.Contains("1").ToChecked(), Model.Type) %>
				<label for="<%= Model.Type %>_GenericTherapyDiagnosisOnset1">Onset</label>
				<input type="text" class="date-picker shortdate" name="<%= Model.Type %>_TherapyDiagnosisDate" value="<%= data.AnswerOrDefault("TherapyDiagnosisDate", DateTime.Now.ToShortDateString()) %>" id="<%= Model.Type %>_TherapyDiagnosisDate" />
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericPrecautions" class="fl">Precautions</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericPrecautions", data.AnswerOrEmptyString("GenericPrecautions"), new { @id = Model.Type + "_GenericPrecautions" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSensation" class="fl">Sensation</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericSensation", data.AnswerOrEmptyString("GenericSensation"), new { @id = Model.Type + "_GenericSensation" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericMuscleTone" class="fl">Muscle Tone</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericMuscleTone", data.AnswerOrEmptyString("GenericMuscleTone"), new { @id = Model.Type + "_GenericMuscleTone" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADL" class="fl">ADL</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type+"_GenericADL", data.AnswerOrEmptyString("GenericADL"), new { @id = Model.Type + "_GenericADL" }) %>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericEndurance" class="fl">Endurance</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericEndurance", data.AnswerOrEmptyString("GenericEndurance"), new { @id = Model.Type + "_GenericEndurance" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericEdema" class="fl">Edema</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericEdema", data.AnswerOrEmptyString("GenericEdema"), new { @id = Model.Type + "_GenericEdema" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericCoordination" class="fl">Coordination</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericCoordination", data.AnswerOrEmptyString("GenericCoordination"), new { @id = Model.Type + "_GenericCoordination" })%>
			</div>
		</div>
	</div>
</fieldset>
