﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Treatment Codes</legend>
	<div class="column">
		<div class="row">
			<div class="checkgroup two-wide">
				<input type="hidden" name="<%= Model.Type %>_GenericTreatmentCodes" value="" />
				<%  string[] genericTreatmentCodes = data.AnswerArray("GenericTreatmentCodes"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "1", genericTreatmentCodes.Contains("1"), "B1 Evaluation")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "2", genericTreatmentCodes.Contains("2"), "B2 Thera Ex")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "3", genericTreatmentCodes.Contains("3"), "B3 Transfer Training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "4", genericTreatmentCodes.Contains("4"), "B4 Home Program")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "5", genericTreatmentCodes.Contains("5"), "B5 Gait Training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "6", genericTreatmentCodes.Contains("6"), "B1 B6 Chest PT")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "7", genericTreatmentCodes.Contains("7"), "B7 Ultrasound")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "8", genericTreatmentCodes.Contains("8"), "B8 Electrother")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "9", genericTreatmentCodes.Contains("9"), "B9 Prosthetic Training")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "10", genericTreatmentCodes.Contains("10"), "B10 Muscle Re-ed")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericTreatmentCodes", "11", genericTreatmentCodes.Contains("11"), "B11  Muscle Re-ed")%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Other</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTreatmentCodesOther", data.AnswerOrEmptyString("GenericTreatmentCodesOther"), new { @id = Model.Type + "_GenericTreatmentCodesOther" }) %>
			</div>
		</div>
	</div>
</fieldset>