﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Physical Assessment</legend>
	<div class="column">
		<table>
			<colgroup>
				<col width="5%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
			</colgroup>
			<thead class="strong">
				<tr>
					<th colspan="2"></th>
					<th colspan="2">ROM</th>
					<th colspan="2">Manual Muscle Test</th>
				</tr>
				<tr>
					<th class="al">Part</th>
					<th class="al">Action</th>
					<th>Right</th>
					<th>Left</th>
					<th>Right</th>
					<th>Left</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="al strong">Shoulder</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMRight", data.AnswerOrEmptyString("GenericShoulderFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMLeft", data.AnswerOrEmptyString("GenericShoulderFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthRight", data.AnswerOrEmptyString("GenericShoulderFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMRight", data.AnswerOrEmptyString("GenericShoulderExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMLeft", data.AnswerOrEmptyString("GenericShoulderExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthRight", data.AnswerOrEmptyString("GenericShoulderExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtensionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Abduction</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMRight", data.AnswerOrEmptyString("GenericShoulderAbductionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderAbductionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMLeft", data.AnswerOrEmptyString("GenericShoulderAbductionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderAbductionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthRight", data.AnswerOrEmptyString("GenericShoulderAbductionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderAbductionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthLeft", data.AnswerOrEmptyString("GenericShoulderAbductionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderAbductionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Int Rot</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMRight", data.AnswerOrEmptyString("GenericShoulderIntRotROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderIntRotROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMLeft", data.AnswerOrEmptyString("GenericShoulderIntRotROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderIntRotROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthRight", data.AnswerOrEmptyString("GenericShoulderIntRotStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderIntRotStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthLeft", data.AnswerOrEmptyString("GenericShoulderIntRotStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderIntRotStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Ext Rot</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMRight", data.AnswerOrEmptyString("GenericShoulderExtRotROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtRotROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMLeft", data.AnswerOrEmptyString("GenericShoulderExtRotROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtRotROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthRight", data.AnswerOrEmptyString("GenericShoulderExtRotStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtRotStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthLeft", data.AnswerOrEmptyString("GenericShoulderExtRotStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericShoulderExtRotStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td class="al strong">Elbow</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMRight", data.AnswerOrEmptyString("GenericElbowFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericElbowFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMLeft", data.AnswerOrEmptyString("GenericElbowFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericElbowFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthRight", data.AnswerOrEmptyString("GenericElbowFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericElbowFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthLeft", data.AnswerOrEmptyString("GenericElbowFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericElbowFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMRight", data.AnswerOrEmptyString("GenericElbowExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericElbowExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMLeft", data.AnswerOrEmptyString("GenericElbowExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericElbowExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthRight", data.AnswerOrEmptyString("GenericElbowExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericElbowExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthLeft", data.AnswerOrEmptyString("GenericElbowExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericElbowExtensionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td class="al strong">Finger</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMRight", data.AnswerOrEmptyString("GenericFingerFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericFingerFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMLeft", data.AnswerOrEmptyString("GenericFingerFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericFingerFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthRight", data.AnswerOrEmptyString("GenericFingerFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericFingerFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthLeft", data.AnswerOrEmptyString("GenericFingerFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericFingerFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMRight", data.AnswerOrEmptyString("GenericFingerExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericFingerExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMLeft", data.AnswerOrEmptyString("GenericFingerExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericFingerExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthRight", data.AnswerOrEmptyString("GenericFingerExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericFingerExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthLeft", data.AnswerOrEmptyString("GenericFingerExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericFingerExtensionStrengthLeft" })%>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="column">
		<table>
			<colgroup>
				<col width="5%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
				<col width="4%" />
			</colgroup>
			<thead class="strong header-hider">
				<tr>
					<th colspan="2"></th>
					<th colspan="2">ROM</th>
					<th colspan="2">Manual Muscle Test</th>
				</tr>
				<tr>
					<th class="al">Part</th>
					<th class="al">Action</th>
					<th>Right</th>
					<th>Left</th>
					<th>Right</th>
					<th>Left</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="al strong">Wrist</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristFlexionROMRight", data.AnswerOrEmptyString("GenericWristFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericWristFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristFlexionROMLeft", data.AnswerOrEmptyString("GenericWristFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericWristFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthRight", data.AnswerOrEmptyString("GenericWristFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericWristFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthLeft", data.AnswerOrEmptyString("GenericWristFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericWristFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td class="strong" style="visibility: hidden">Shoulder</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristExtensionROMRight", data.AnswerOrEmptyString("GenericWristExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericWristExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristExtensionROMLeft", data.AnswerOrEmptyString("GenericWristExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericWristExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthRight", data.AnswerOrEmptyString("GenericWristExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericWristExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthLeft", data.AnswerOrEmptyString("GenericWristExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericWristExtensionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td class="al strong">Trunk</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMRight", data.AnswerOrEmptyString("GenericTrunkFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMLeft", data.AnswerOrEmptyString("GenericTrunkFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthRight", data.AnswerOrEmptyString("GenericTrunkFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthLeft", data.AnswerOrEmptyString("GenericTrunkFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Rotation</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMRight", data.AnswerOrEmptyString("GenericTrunkRotationROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkRotationROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMLeft", data.AnswerOrEmptyString("GenericTrunkRotationROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkRotationROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthRight", data.AnswerOrEmptyString("GenericTrunkRotationStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkRotationStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthLeft", data.AnswerOrEmptyString("GenericTrunkRotationStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkRotationStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMRight", data.AnswerOrEmptyString("GenericTrunkExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMLeft", data.AnswerOrEmptyString("GenericTrunkExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthRight", data.AnswerOrEmptyString("GenericTrunkExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthLeft", data.AnswerOrEmptyString("GenericTrunkExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericTrunkExtensionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td class="al strong">Neck</td>
					<td class="al">Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMRight", data.AnswerOrEmptyString("GenericNeckFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Extension</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMRight", data.AnswerOrEmptyString("GenericNeckExtensionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckExtensionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMLeft", data.AnswerOrEmptyString("GenericNeckExtensionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckExtensionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthRight", data.AnswerOrEmptyString("GenericNeckExtensionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckExtensionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthLeft", data.AnswerOrEmptyString("GenericNeckExtensionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckExtensionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Lat Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMRight", data.AnswerOrEmptyString("GenericNeckLatFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLatFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckLatFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLatFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLatFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckLatFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLatFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">Long Flexion</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMRight", data.AnswerOrEmptyString("GenericNeckLongFlexionROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLongFlexionROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMLeft", data.AnswerOrEmptyString("GenericNeckLongFlexionROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLongFlexionROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthRight", data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLongFlexionStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthLeft", data.AnswerOrEmptyString("GenericNeckLongFlexionStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckLongFlexionStrengthLeft" })%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td class="al">
						Rotation
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckRotationROMRight", data.AnswerOrEmptyString("GenericNeckRotationROMRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckRotationROMRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckRotationROMLeft", data.AnswerOrEmptyString("GenericNeckRotationROMLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckRotationROMLeft" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthRight", data.AnswerOrEmptyString("GenericNeckRotationStrengthRight"), new { @class = "shorter", @id = Model.Type + "_GenericNeckRotationStrengthRight" })%>
					</td>
					<td class="ac">
						<%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthLeft", data.AnswerOrEmptyString("GenericNeckRotationStrengthLeft"), new { @class = "shorter", @id = Model.Type + "_GenericNeckRotationStrengthLeft" })%>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericPhysicalAssessmentComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericPhysicalAssessmentTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericPhysicalAssessmentComment", data.AnswerOrEmptyString("GenericPhysicalAssessmentComment"), new { @id = Model.Type + "_GenericPhysicalAssessmentComment" })%>
			</div>
		</div>
	</div>
</fieldset>
