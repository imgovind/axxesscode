﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Long Term Goals</legend>
	<div class="column">
		<div class="row">
			<table style="width: 100%; margin: 0; padding: 0 1em;">
				<colgroup>
					<col style="width:10%;"/>
					<col style="width:45%;"/>
					<col style="width:45%;"/>
				</colgroup>
				<thead>
					<tr>
						<th></th>
						<th class="ac">Goal</th>
						<th class="ac">Time Frame</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label>1.</label></td>
						<td class="ac"> 
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal1", data.AnswerOrEmptyString("GenericLongTermGoal1"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal1" }) %>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal1TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal1TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal1TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>2.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal2", data.AnswerOrEmptyString("GenericLongTermGoal2"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal2" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal2TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal2TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal2TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>3.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal3", data.AnswerOrEmptyString("GenericLongTermGoal3"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal3" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal3TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal3TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal3TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>4.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal4", data.AnswerOrEmptyString("GenericLongTermGoal4"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal4" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal4TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal4TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal4TimeFrame" })%>
						</td>
					</tr>
					<tr>
						<td><label>5.</label></td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal5", data.AnswerOrEmptyString("GenericLongTermGoal5"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal5" })%>
						</td>
						<td class="ac">
							<%= Html.TextBox(Model.Type + "_GenericLongTermGoal5TimeFrame", data.AnswerOrEmptyString("GenericLongTermGoal5TimeFrame"), new { @class = "partial-fill", @id = Model.Type + "_GenericLongTermGoal5TimeFrame" })%>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericLongTermFrequency" class="float-left">Frequency</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericLongTermFrequency", data.AnswerOrEmptyString("GenericLongTermFrequency"), new { @id = Model.Type + "_GenericLongTermFrequency" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericLongTermDuration" class="float-left">Duration</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericLongTermDuration", data.AnswerOrEmptyString("GenericLongTermDuration"), new { @id = Model.Type + "_GenericLongTermDuration" })%>
			</div>
		</div>
	</div>
</fieldset>
