﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.PatientProfile.DisplayName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
     <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("Therapy/STDischarge/ContentRev2", Model); %></div>
    <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>