﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericHomeboundReason = data.AnswerArray("GenericHomeboundReason"); %>
<%  string[] liquids = data.AnswerArray("GenericLiquids"); %>
<%  string[] genericReferralFor = data.AnswerArray("GenericReferralFor"); %>
<%  string[] genericPlanOfCare = data.AnswerArray("GenericPlanOfCare"); %>
<%  string[] genericDischargeDiscussedWith = data.AnswerArray("GenericDischargeDiscussedWith"); %>
<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
<fieldset>
	<legend>Patient&#8217;s Status upon Discharge</legend>
	<div class="wide-column">
		<div class="row ac">
			<label style="font-size: xx-small; font-style: italic;">4 &#8211; WFL (Within Functional Limits) &#160; 3 &#8211; Mild Impairment &#160; 2 &#8211; Moderate Impairment &#160; 1 &#8211; Severe Impairment &#160; 0 &#8211; Unable to Assess/Did Not Test</label>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Cognition Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericOrientationScore" class="fl">Orientation (Person/Place/Time)</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericOrientationScore", data.AnswerOrEmptyString("GenericOrientationScore"), new { @class = "shorter", @id = Model.Type + "_GenericOrientationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericAttentionSpanScore" class="fl">Attention Span</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericAttentionSpanScore", data.AnswerOrEmptyString("GenericAttentionSpanScore"), new { @class = "shorter", @id = Model.Type + "_GenericAttentionSpanScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericShortTermMemoryScore" class="fl">Short Term Memory</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericShortTermMemoryScore", data.AnswerOrEmptyString("GenericShortTermMemoryScore"), new { @class = "shorter", @id = Model.Type + "_GenericShortTermMemoryScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericLongTermMemoryScore" class="fl">Long Term Memory</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericLongTermMemoryScore", data.AnswerOrEmptyString("GenericLongTermMemoryScore"), new { @class = "shorter", @id = Model.Type + "_GenericLongTermMemoryScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericJudgmentScore" class="fl">Judgment</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericJudgmentScore", data.AnswerOrEmptyString("GenericJudgmentScore"), new { @class = "shorter", @id = Model.Type + "_GenericJudgmentScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericProblemSolvingScore" class="fl">Problem Solving</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericProblemSolvingScore", data.AnswerOrEmptyString("GenericProblemSolvingScore"), new { @class = "shorter", @id = Model.Type + "_GenericProblemSolvingScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericOrganizationScore" class="fl">Organization</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericOrganizationScore", data.AnswerOrEmptyString("GenericOrganizationScore"), new { @class = "shorter", @id = Model.Type + "_GenericOrganizationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedOtherScore" class="fl">Other</label><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedOther" })%>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedOtherScore"), new { @class = "shorter", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericCognitionFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericCognitionFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Swallowing Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericChewingAbilityScore" class="fl">Chewing Ability</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericChewingAbilityScore", data.AnswerOrEmptyString("GenericChewingAbilityScore"), new { @class = "shorter", @id = Model.Type + "_GenericChewingAbilityScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericOralStageManagementScore" class="fl">Oral Stage Management</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericOralStageManagementScore", data.AnswerOrEmptyString("GenericOralStageManagementScore"), new { @class = "shorter", @id = Model.Type + "_GenericOralStageManagementScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPharyngealStageManagementScore" class="fl">Pharyngeal Stage Management</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericPharyngealStageManagementScore", data.AnswerOrEmptyString("GenericPharyngealStageManagementScore"), new { @class = "shorter", @id = Model.Type + "_GenericPharyngealStageManagementScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericReflexTimeScore" class="fl">Reflex Time</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericReflexTimeScore", data.AnswerOrEmptyString("GenericReflexTimeScore"), new { @class = "shorter", @id = Model.Type + "_GenericReflexTimeScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedOtherScore" class="fl">Other</label><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOther" })%>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedOtherScore"), new { @class = "shorter", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericSwallowingFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSwallowingFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Speech/Voice Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericOralFacialExamScore" class="fl">Oral/Facial Exam</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericOralFacialExamScore", data.AnswerOrEmptyString("GenericOralFacialExamScore"), new { @class = "shorter", @id = Model.Type + "_GenericOralFacialExamScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericArticulationScore" class="fl">Articulation</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericArticulationScore", data.AnswerOrEmptyString("GenericArticulationScore"), new { @class = "shorter", @id = Model.Type + "_GenericArticulationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericProsodyScore" class="fl">Prosody</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericProsodyScore", data.AnswerOrEmptyString("GenericProsodyScore"), new { @class = "shorter", @id = Model.Type + "_GenericProsodyScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericVoiceRespirationScore" class="fl">Voice/Respiration</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericVoiceRespirationScore", data.AnswerOrEmptyString("GenericVoiceRespirationScore"), new { @class = "shorter", @id = Model.Type + "_GenericVoiceRespirationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSpeechIntelligibilityScore" class="fl">Speech Intelligibility</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSpeechIntelligibilityScore", data.AnswerOrEmptyString("GenericSpeechIntelligibilityScore"), new { @class = "shorter", @id = Model.Type + "_GenericSpeechIntelligibilityScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedOtherScore" class="fl">Other
						<%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOther", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOther"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedOther" })%></label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedOtherScore"), new { @class = "shorter", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericSpeechFunctionEvaluatedComment", data.AnswerOrEmptyString("GenericSpeechFunctionEvaluatedComment"), new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Auditory Comprehension Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericWordDiscriminationScore" class="fl">Word Discrimination</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericWordDiscriminationScore", data.AnswerOrEmptyString("GenericWordDiscriminationScore"), new { @class = "shorter", @id = Model.Type + "_GenericWordDiscriminationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericOneStepDirectionsScore" class="fl">One Step Directions</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericOneStepDirectionsScore", data.AnswerOrEmptyString("GenericOneStepDirectionsScore"), new { @class = "shorter", @id = Model.Type + "_GenericOneStepDirectionsScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTwoStepDirectionsScore" class="fl">Two Step Directions</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTwoStepDirectionsScore", data.AnswerOrEmptyString("GenericTwoStepDirectionsScore"), new { @class = "shorter", @id = Model.Type + "_GenericTwoStepDirectionsScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericComplexSentencesScore" class="fl">Complex Sentences</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericComplexSentencesScore", data.AnswerOrEmptyString("GenericComplexSentencesScore"), new { @class = "shorter", @id = Model.Type + "_GenericComplexSentencesScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericConversationScore" class="fl">Conversation</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericConversationScore", data.AnswerOrEmptyString("GenericConversationScore"), new { @class = "shorter", @id = Model.Type + "_GenericConversationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSpeechReadingScore" class="fl">Speech Reading</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSpeechReadingScore", data.AnswerOrEmptyString("GenericSpeechReadingScore"), new { @class = "shorter", @id = Model.Type + "_GenericSpeechReadingScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericACFEComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericACFEComment", data.AnswerOrEmptyString("GenericACFEComment"), new { @id = Model.Type + "_GenericACFEComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Verbal Expression Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericAugmentativeMethodsScore" class="fl">Augmentative Methods</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericAugmentativeMethodsScore", data.AnswerOrEmptyString("GenericAugmentativeMethodsScore"), new { @class = "shorter", @id = Model.Type + "_GenericAugmentativeMethodsScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericNamingScore" class="fl">Naming</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericNamingScore", data.AnswerOrEmptyString("GenericNamingScore"), new { @class = "shorter", @id = Model.Type + "_GenericNamingScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericAppropriateScore" class="fl">Appropriate</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericAppropriateScore", data.AnswerOrEmptyString("GenericAppropriateScore"), new { @class = "shorter", @id = Model.Type + "_GenericAppropriateScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericVEFEComplexSentencesScore" class="fl">Complex Sentences</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericVEFEComplexSentencesScore", data.AnswerOrEmptyString("GenericVEFEComplexSentencesScore"), new { @class = "shorter", @id = Model.Type + "_GenericVEFEComplexSentencesScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericVEFEConversationScore" class="fl">Conversation</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericVEFEConversationScore", data.AnswerOrEmptyString("GenericVEFEConversationScore"), new { @class = "shorter", @id = Model.Type + "_GenericVEFEConversationScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericVEFEComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericVEFEComment", data.AnswerOrEmptyString("GenericVEFEComment"), new { @id = Model.Type + "_GenericVEFEComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Reading Function Evaluated</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericRFELettersNumbersScore" class="fl">Letters/Numbers</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRFELettersNumbersScore", data.AnswerOrEmptyString("GenericRFELettersNumbersScore"), new { @class = "shorter", @id = Model.Type + "_GenericRFELettersNumbersScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericRFEWordsScore" class="fl">Words</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRFEWordsScore", data.AnswerOrEmptyString("GenericRFEWordsScore"), new { @class = "shorter", @id = Model.Type + "_GenericRFEWordsScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericRFESimpleSentencesScore" class="fl">Simple Sentences</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRFESimpleSentencesScore", data.AnswerOrEmptyString("GenericRFESimpleSentencesScore"), new { @class = "shorter", @id = Model.Type + "_GenericRFESimpleSentencesScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericRFEComplexSentencesScore" class="fl">Complex Sentences</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRFEComplexSentencesScore", data.AnswerOrEmptyString("GenericRFEComplexSentencesScore"), new { @class = "shorter", @id = Model.Type + "_GenericRFEComplexSentencesScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericParagraphScore" class="fl">Paragraph</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericParagraphScore", data.AnswerOrEmptyString("GenericParagraphScore"), new { @class = "shorter", @id = Model.Type + "_GenericParagraphScore" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericRFEComment">Comments</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_GenericRFEComment", data.AnswerOrEmptyString("GenericRFEComment"), new { @id = Model.Type + "_GenericRFEComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Writing Function Evaluated</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericWFELettersNumbersScore" class="fl">Letters/Numbers</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWFELettersNumbersScore", data.AnswerOrEmptyString("GenericWFELettersNumbersScore"), new { @class = "shorter", @id = Model.Type + "_GenericWFELettersNumbersScore" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericWFEWordsScore" class="fl">Words</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWFEWordsScore", data.AnswerOrEmptyString("GenericWFEWordsScore"), new { @class = "shorter", @id = Model.Type + "_GenericWFEWordsScore" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericWFESentencesScore" class="fl">Sentences</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWFESentencesScore", data.AnswerOrEmptyString("GenericWFESentencesScore"), new { @class = "shorter", @id = Model.Type + "_GenericWFESentencesScore" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericWFESpellingScore" class="fl">Spelling</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWFESpellingScore", data.AnswerOrEmptyString("GenericWFESpellingScore"), new { @class = "shorter", @id = Model.Type + "_GenericWFESpellingScore" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericFormulationScore" class="fl">Formulation</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericFormulationScore", data.AnswerOrEmptyString("GenericFormulationScore"), new { @class = "shorter", @id = Model.Type + "_GenericFormulationScore" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSimpleAdditionSubtractionScore" class="fl">Simple Addition/Subtraction</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericSimpleAdditionSubtractionScore", data.AnswerOrEmptyString("GenericSimpleAdditionSubtractionScore"), new { @class = "shorter", @id = Model.Type + "_GenericSimpleAdditionSubtractionScore" })%>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericWFEComment">Comments</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericWFEComment", data.AnswerOrEmptyString("GenericWFEComment"), new { @id = Model.Type + "_GenericWFEComment" })%>
			</div>
		</div>
	</div>
</fieldset>
<%--<table class="fixed nursing">
	<tbody>
		<tr>
			<th colspan="2">Patient&#8217;s status upon discharge<br />
			</th>
		</tr>
	</tbody>
</table>--%>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Reason for Discharge</legend>
			<div class="column">
				<div class="row">
					<div class="checkgroup two-wide">
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("1").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("2").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge2">Expired</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("3").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge3">Per pt/family request</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("4").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge4">No longer Homebound</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("5").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge5">Goals met</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("6").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge6">Prolonged on hold status</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("7").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge7">Moved out of service area</label>
							</div>
						</div>
						<div class="option">
							<div class="wrapper">
								<%= string.Format("<input id='{1}_GenericReasonForDischarge8' class='radio' name='{1}_GenericReasonForDischarge' value='8' type='checkbox' {0} />", data.AnswerArray("GenericReasonForDischarge").Contains("8").ToChecked(), Model.Type)%>
								<label for="<%= Model.Type %>_GenericReasonForDischarge8">Hospitalized</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Condition of Patient upon Discharge</legend>
			<div class="column">
				<div class="row">
					<em>(Include VS, BS, Functional and Overall Status)</em>
					<div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_ConditionOfDischargeTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_ConditionOfDischarge", data.AnswerOrEmptyString("ConditionOfDischarge"), new { @id = Model.Type + "_ConditionOfDischarge" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Skilled Treatment Provided this Visit</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_TreatmentProvidedTemplate")%>
	    				<%= Html.TextArea(Model.Type + "_TreatmentProvided", data.AnswerOrEmptyString("TreatmentProvided"), new { @id = Model.Type + "_TreatmentProvided" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Summary of Treatment Provided</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_SummaryTreatmentProvidedTemplate")%>
	    				<%= Html.TextArea(Model.Type + "_SummaryTreatmentProvided", data.AnswerOrEmptyString("SummaryTreatmentProvided"), new { @id = Model.Type + "_SummaryTreatmentProvided" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Summary of Progress Made</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_ProgressMadeTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_ProgressMade", data.AnswerOrEmptyString("ProgressMade"), new { @id = Model.Type + "_ProgressMade" })%>
	    	</div>
		</div>
	</div>
</fieldset>
