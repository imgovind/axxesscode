﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/PhysicalAssessment/FormRev2", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/BedMobility/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/WCMobility/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
    </div>
</div>
<% Html.RenderPartial("Therapy/Shared/Transfer/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/Gait/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Reason for Discharge</legend>
            <div class="column">
                <div class="row">
                    <div class="checkgroup two-wide">
                        <% string[] genericReasonForDischarge = data.AnswerArray("GenericReasonForDischarge"); %>
                        <input name="<%= Model.Type %>_GenericReasonForDischarge" value=" " type="hidden" />
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "1", genericReasonForDischarge.Contains("1"), "Reached Maximum Potential")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "2", genericReasonForDischarge.Contains("2"), "No Longer Homebound")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "3", genericReasonForDischarge.Contains("3"), "Per Patient/Family Request")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "4", genericReasonForDischarge.Contains("4"), "Prolonged On-Hold Status")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "5", genericReasonForDischarge.Contains("5"), "Goals Met")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "6", genericReasonForDischarge.Contains("6"), "Hospitalized")%>
                        <%= Html.CheckgroupOption(Model.Type + "_GenericReasonForDischarge", "7", genericReasonForDischarge.Contains("7"), "Expired")%>
                    </div>
                </div>
                <div class="row">
                    <label class="fl">Other</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.AnswerOrEmptyString("GenericReasonForDischargeOther"), new { @id = Model.Type+"_GenericReasonForDischargeOther" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev1", Model); %>
    </div>
</div>
<fieldset>
    <legend>Frequency</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Frequency</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type+"_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @id = Model.Type+"_GenericFrequency" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Duration</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type+"_GenericDuration", data.AnswerOrEmptyString("GenericDuration"), new { @id = Model.Type+"_GenericDuration" })%></div>
        </div>
    </div>
</fieldset>
<% Html.RenderPartial("Therapy/Shared/Narrative/FormRev1", Model); %>
