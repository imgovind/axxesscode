﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, true, Model.Type)); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Mental Assessment</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsMentalApply", Model.Type + "IsMentalApply", "1", data.AnswerOrEmptyString("IsMentalApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="wide-column">
					<div class="row">
						<label for="<%= Model.Type %>_GenericMentalAssessmentOrientation" class="fl">Orientation</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericMentalAssessmentLOC" class="fl">Level of Consciousness</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @id = Model.Type + "_GenericMentalAssessmentLOC" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericMentalAssessmentComment">Comments</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_MentalAssessmentTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericMentalAssessmentComment", data.AnswerOrEmptyString("GenericMentalAssessmentComment"), new { @id = Model.Type + "_GenericMentalAssessmentComment" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev2", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/LivingSituation/FormRev1", Model); %>
	</div>
</div>
<fieldset>
	<legend>DME</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsDMEApply", Model.Type + "IsDMEApply", "1", data.AnswerOrEmptyString("IsDMEApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericDMEAvailable" class="fl">Available</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @id = Model.Type + "_GenericDMEAvailable" }) %></div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericDMENeeds" class="fl">Needs</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @id = Model.Type + "_GenericDMENeeds" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericDMESuggestion" class="fl">Suggestion</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @id = Model.Type + "_GenericDMESuggestion" })%></div>
			</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Therapy/Shared/PhysicalAssessment/FormRev4", new VisitNoteSectionViewData(data, true, Model.Type)); %>
<% Html.RenderPartial("Therapy/Shared/LevelOfFunction/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, true, Model.Type)); %>
<fieldset>
	<legend>Prior Therapy Received</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_IsPTRApply", Model.Type + "IsPTRApply", "1", data.AnswerOrEmptyString("IsPTRApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="wide-column">
			<div class="row">
				<div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericPriorTherapyTemplates")%>
					<%= Html.TextArea(Model.Type + "_GenericPriorTherapyReceived", data.AnswerOrEmptyString("GenericPriorTherapyReceived"), new { @id = Model.Type + "_GenericPriorTherapyReceived" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/BedMobility/FormRev2", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Gait/FormRev2", Model); %>
	</div>
	<div>
		<fieldset>
			<legend>WB</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsWBApply", Model.Type + "IsWBApply", "1", data.AnswerOrEmptyString("IsWBApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<label calss="fl">Status</label>
						<div class="fr"><%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus", data.AnswerOrEmptyString("GenericWBStatus"), new { @id = Model.Type + "_GenericWBStatus" })%></div>
					</div>
					<div class="row">
						<label calss="fl">Other</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericWBStatusOther", data.AnswerOrEmptyString("GenericWBStatusOther"), new { @id = Model.Type + "_GenericWBStatusOther" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericWBSComment" calss="fl">Comments</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_WBSTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericWBSComment", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/Transfer/FormRev2", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/WCMobility/FormRev1", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
	<div>
		<fieldset>
			<legend>Assessment</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsAssessmentApply", Model.Type + "IsAssessmentApply", "1", data.AnswerOrEmptyString("IsAssessmentApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="wide-column">
					<div class="row">
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.AnswerOrEmptyString("GenericAssessmentComment"),new { @id = Model.Type + "_GenericAssessmentComment" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev2", Model); %>
<% Html.RenderPartial("Therapy/Shared/PTGoals/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Other Discipline Recommendation</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsRecommendationApply", Model.Type + "IsRecommendationApply", "1", data.AnswerOrEmptyString("IsRecommendationApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<div class="checkgroup four-wide">
							<% string[] genericDisciplineRecommendation = data.AnswerArray("GenericDisciplineRecommendation"); %>
							<input name="<%= Model.Type %>_GenericDisciplineRecommendation" value=" " type="hidden" />
							<%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "1", genericDisciplineRecommendation.Contains("1"), "OT")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "2", genericDisciplineRecommendation.Contains("2"), "MSW")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "3", genericDisciplineRecommendation.Contains("3"), "ST")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "4", genericDisciplineRecommendation.Contains("4"), "Podiatrist")%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Other</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.AnswerOrEmptyString("GenericDisciplineRecommendationOther"), new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericDisciplineRecommendationReason">Reason</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_DisciplineRecommendationTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericDisciplineRecommendationReason", data.AnswerOrEmptyString("GenericDisciplineRecommendationReason"), new { @id = Model.Type + "_GenericDisciplineRecommendationReason" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericRehabPotential">Rehab Potential</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_RehabTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @id = Model.Type + "_GenericRehabPotential" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericFrequencyAndDuration">Frequency &amp; Duration</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_FrequencyAndDurationTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericFrequencyAndDuration", data.AnswerOrEmptyString("GenericFrequencyAndDuration"), new { @id = Model.Type + "_GenericFrequencyAndDuration" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Standardized test</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsTestApply", Model.Type + "IsTestApply", "1", data.AnswerOrEmptyString("IsTestApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div calss="row">
						<label class="fl">Prior</label>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericTinettiPOMA" class="fl">Tinetti POMA</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericTinettiPOMA", data.AnswerOrEmptyString("GenericTinettiPOMA"), new { @id = Model.Type + "_GenericTinettiPOMA" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericTimedGetUp" class="fl">Timed Get Up &amp; Go Test</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTimedGetUp", data.AnswerOrEmptyString("GenericTimedGetUp"), new { @id = Model.Type + "_GenericTimedGetUp" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericFunctionalReach" class="fl">Functional Reach</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericFunctionalReach", data.AnswerOrEmptyString("GenericFunctionalReach"), new { @id = Model.Type + "_GenericFunctionalReach" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericStandardizedTestOther">Other</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_StandardizedTestTemplates1")%>
							<%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @id = Model.Type + "_GenericStandardizedTestOther" })%>
						</div>
					</div>
				</div>
				<div class="column">
					<div calss="row">
						<label class="fl">Current</label>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericTinettiPOMA1" class="fl">Tinetti POMA</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTinettiPOMA1", data.AnswerOrEmptyString("GenericTinettiPOMA1"), new { @id = Model.Type + "_GenericTinettiPOMA1" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericTimedGetUp1" class="fl">Timed Get Up &amp; Go Test</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTimedGetUp1", data.AnswerOrEmptyString("GenericTimedGetUp1"), new { @id = Model.Type + "_GenericTimedGetUp1" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericFunctionalReach1" class="fl">Functional Reach</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericFunctionalReach1", data.AnswerOrEmptyString("GenericFunctionalReach1"), new { @id = Model.Type + "_GenericFunctionalReach1" })%></div>
					</div>
					<div class="sub row">
						<label for="<%= Model.Type %>_GenericStandardizedTestOther1">Other</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_StandardizedTestTemplates2")%>
							<%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @id = Model.Type + "_GenericStandardizedTestOther1" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Care Coordination</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsCareApply", Model.Type + "IsCareApply", "1", data.AnswerOrEmptyString("IsCareApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="wide-column">
					<div class="row">
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Skilled Care Provided This Visit</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsSkilledCareApply", Model.Type + "IsSkilledCareApply", "1", data.AnswerOrEmptyString("IsSkilledCareApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="wide-column">
					<div class="row">
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericSkilledCareTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.AnswerOrEmptyString("GenericSkilledCareProvided"), new { @id = Model.Type + "_GenericSkilledCareProvided" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/Notification/FormRev1", Model); %>
