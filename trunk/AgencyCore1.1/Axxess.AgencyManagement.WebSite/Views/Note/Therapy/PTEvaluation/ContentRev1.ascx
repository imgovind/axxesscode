﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev1", Model); %>
    </div>
</div>
<% Html.RenderPartial("Therapy/Shared/History/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/PhysicalAssessment/FormRev3", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
    </div>
    <div>
        <fieldset>
            <legend>Home Safety Evaluation</legend>
            <div class="wide-column">
                <div class="row">
                    <label class="al">Level</label>
                    <div class="checkgroup two-wide">
                        <% string[] genericHomeSafetyEvaluationLevel = data.AnswerArray("GenericHomeSafetyEvaluationLevel"); %>
                        <input name="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel" value=" " type="hidden" />
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLevel", "1", genericHomeSafetyEvaluationLevel.Contains("1"), "One")%>
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLevel", "0", genericHomeSafetyEvaluationLevel.Contains("0"), "Multiple")%>
                    </div>
                </div>
                <div class="row">
                    <label class="al">Stairs</label>
                    <div class="checkgroup two-wide">
                        <% string[] genericHomeSafetyEvaluationStairs = data.AnswerArray("GenericHomeSafetyEvaluationStairs"); %>
                        <input name="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs" value=" " type="hidden" />
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationStairs", "1", genericHomeSafetyEvaluationStairs.Contains("1"), "Yes")%>
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationStairs", "0", genericHomeSafetyEvaluationStairs.Contains("0"), "No")%>
                    </div>
                </div>
                <div class="row">
                    <label class="al">Lives</label>
                    <div class="checkgroup two-wide">
                        <% string[] genericHomeSafetyEvaluationLives = data.AnswerArray("GenericHomeSafetyEvaluationLives"); %>
                        <input name="<%= Model.Type %>_GenericHomeSafetyEvaluationLives" value=" " type="hidden" />
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "2", genericHomeSafetyEvaluationLives.Contains("2"), "Alone")%>
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "1", genericHomeSafetyEvaluationLives.Contains("1"), "Family")%>
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationLives", "0", genericHomeSafetyEvaluationLives.Contains("0"), "Friends")%>
                    </div>
                </div>
                <div class="row">
                    <label class="al">Support</label>
                    <div class="checkgroup two-wide">
                        <% string[] genericHomeSafetyEvaluationSupport = data.AnswerArray("GenericHomeSafetyEvaluationSupport"); %>
                        <input name="<%= Model.Type %>_GenericHomeSafetyEvaluationSupport" value=" " type="hidden" />
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationSupport", "1", genericHomeSafetyEvaluationSupport.Contains("1"), "Retirement")%>
                        <%= Html.CheckgroupRadioOption(Model.Type + "_GenericHomeSafetyEvaluationSupport", "0", genericHomeSafetyEvaluationSupport.Contains("0"), "Assisted Living")%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Mental Assessment</legend>
            <div class="wide-column">
                <div class="row">
                    <label for="<%= Model.Type %>_GenericMentalAssessmentOrientation" class="fl">Orientation</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @id = Model.Type + "_GenericMentalAssessmentOrientation" })%></div>
                </div>
                <div class="row">
                    <label for="<%= Model.Type %>_GenericMentalAssessmentLOC" class="fl">LOC</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @id = Model.Type + "_GenericMentalAssessmentLOC" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <fieldset>
            <legend>DME</legend>
            <div class="wide-column">
                <div class="row">
                    <label for="<%= Model.Type %>_GenericDMEAvailable" class="fl">Available</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @id = Model.Type + "_GenericDMEAvailable" }) %></div>
                </div>
                <div class="row">
                    <label for="<%= Model.Type %>_GenericDMENeeds" class="fl">Needs</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @id = Model.Type + "_GenericDMENeeds" })%></div>
                </div>
                <div class="row">
                    <label for="<%= Model.Type %>_GenericDMESuggestion" class="fl">Suggestion</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @id = Model.Type + "_GenericDMESuggestion" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/BedMobility/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/Gait/FormRev1", Model); %>
    </div>
</div>
<% Html.RenderPartial("Therapy/Shared/Transfer/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>WBS</legend>
            <div class="wide-column">
                <div class="row">
                    <label for="<%= Model.Type %>_GenericWBSAssistiveDevice" class="fl">Assistive Device</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericWBSAssistiveDevice", data.AnswerOrEmptyString("GenericWBSAssistiveDevice"), new { @class = "short", @id = Model.Type + "_GenericWBSAssistiveDevice" })%>
                        %</div>
                </div>
                <div class="row">
                    <label for="<%= Model.Type %>_GenericWBSDescription" class="fl">Description</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericWBSDescription", data.AnswerOrEmptyString("GenericWBSDescription"), new { @id = Model.Type + "_GenericWBSDescription" })%></div>
                </div>
                <div class="row">
                    <label for="<%= Model.Type %>_GenericWBSPosture" class="fl">Posture</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericWBSPosture", data.AnswerOrEmptyString("GenericWBSPosture"), new { @id = Model.Type + "_GenericWBSPosture" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/WCMobility/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
    </div>
</div>
<fieldset>
    <legend>Assessment</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentCommentTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.AnswerOrEmptyString("GenericAssessmentComment"),new { @id = Model.Type + "_GenericAssessmentComment" })%>
            </div>
        </div>
    </div>
</fieldset>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/TreatmentCodes/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev1", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Therapy/Shared/ShortTermGoals/FormRev1", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Therapy/Shared/LongTermGoals/FormRev1", Model); %>
    </div>
</div>
<fieldset>
    <legend>Other Discipline Recommendation</legend>
    <div class="column">
        <div class="row">
            <div class="checkgroup four-wide">
                <% string[] genericDisciplineRecommendation = data.AnswerArray("GenericDisciplineRecommendation"); %>
                <input name="<%= Model.Type %>_GenericDisciplineRecommendation" value=" " type="hidden" />
                <%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "1", genericDisciplineRecommendation.Contains("1"), "OT")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "2", genericDisciplineRecommendation.Contains("2"), "MSW")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "3", genericDisciplineRecommendation.Contains("3"), "ST")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericDisciplineRecommendation", "4", genericDisciplineRecommendation.Contains("4"), "Podiatrist")%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericDisciplineRecommendationOther" class="fl">Other</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.AnswerOrEmptyString("GenericDisciplineRecommendationOther"), new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="fl">Reason</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationReason", data.AnswerOrEmptyString("GenericDisciplineRecommendationReason"), new { @id = Model.Type + "_GenericDisciplineRecommendationReason" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericFrequency" class="fl">Frequency</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @class = "shorter", @id = Model.Type + "_GenericFrequency" })%>
                <label for="<%= Model.Type %>_GenericFrequencyFor">X wk for</label>
                <%= Html.TextBox(Model.Type + "_GenericFrequencyFor", data.AnswerOrEmptyString("GenericFrequencyFor"), new { @class = "shorter", @id = Model.Type + "_GenericFrequencyFor" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericRehabPotential" class="fl">Rehab Potential</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.AnswerOrEmptyString("GenericRehabPotential"), new { @id = Model.Type + "_GenericRehabPotential" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericPrognosis" class="fl">Prognosis</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericPrognosis", data.AnswerOrEmptyString("GenericPrognosis"), new { @id = Model.Type + "_GenericPrognosis" })%></div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Care Coordination</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Skilled Care Provided This Visit</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_GenericSkilledCareProvidedTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.AnswerOrEmptyString("GenericSkilledCareProvided"), new { @id = Model.Type + "_GenericSkilledCareProvided" })%>
            </div>
        </div>
    </div>
</fieldset>
