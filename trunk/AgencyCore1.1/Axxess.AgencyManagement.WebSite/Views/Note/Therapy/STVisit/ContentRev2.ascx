﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Treatment Diagnosis/Problem</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericTreatmentDiagnosisTemplate")%>
	    				<%= Html.TextArea(Model.Type + "_GenericTreatmentDiagnosis", data.AnswerOrEmptyString("GenericTreatmentDiagnosis"), new { @id = Model.Type + "_GenericTreatmentDiagnosis" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Subjective</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericTreatmentOutcomeTemplate")%>
	    				<%= Html.TextArea(Model.Type + "_GenericTreatmentOutcome", data.AnswerOrEmptyString("GenericTreatmentOutcome"), new { @id = Model.Type + "_GenericTreatmentOutcome" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<fieldset>
	<legend>Skilled treatment provided this visit</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<% var stInstruction = data.AnswerArray("STInstruction"); %>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction1' name='{1}_STInstruction' value='1' type='checkbox' {0} />", stInstruction.Contains("1").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction1">Evaluation</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction2' name='{1}_STInstruction' value='2' type='checkbox' {0} />", stInstruction.Contains("2").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction2">Dysphagia treatments</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction3' name='{1}_STInstruction' value='3' type='checkbox' {0} />", stInstruction.Contains("3").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction3">Safe swallowing evaluation</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction4' name='{1}_STInstruction' value='4' type='checkbox' {0} />", stInstruction.Contains("4").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction4">Patient/Family education</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction5' name='{1}_STInstruction' value='5' type='checkbox' {0} />", stInstruction.Contains("5").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction5">Language disorders</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction6' name='{1}_STInstruction' value='6' type='checkbox' {0} />", stInstruction.Contains("6").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction6">Speech dysphagia instruction program</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction7' name='{1}_STInstruction' value='7' type='checkbox' {0} />", stInstruction.Contains("7").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction7">Voice disorders</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction8' name='{1}_STInstruction' value='8' type='checkbox' {0} />", stInstruction.Contains("8").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction8">Aural rehabilitation</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction9' name='{1}_STInstruction' value='9' type='checkbox' {0} />", stInstruction.Contains("9").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction9">Teach/Develop communication system</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction10' name='{1}_STInstruction' value='10' type='checkbox' {0} />", stInstruction.Contains("10").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction10">Speech articulation disorders</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction11' name='{1}_STInstruction' value='11' type='checkbox' {0} />", stInstruction.Contains("11").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction11">Non-oral communication</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction12' name='{1}_STInstruction' value='12' type='checkbox' {0} />", stInstruction.Contains("12").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction12">Pain Management</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction13' name='{1}_STInstruction' value='13' type='checkbox' {0} />", stInstruction.Contains("13").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction13">Alaryngeal speech skills</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction14' name='{1}_STInstruction' value='14' type='checkbox' {0} />", stInstruction.Contains("14").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction14">Language processing</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction15' name='{1}_STInstruction' value='15' type='checkbox' {0} />", stInstruction.Contains("15").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction15">Food texture recommendations</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_STInstruction16' name='{1}_STInstruction' value='16' type='checkbox' {0} />", stInstruction.Contains("16").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_STInstruction16">Establish home maintenance program</label>
					</div>
					<div class="more">
						<div class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_STInstruction", "16a", stInstruction.Contains("16a"), "Copy given to patient")%>
							<%= Html.CheckgroupOption(Model.Type + "_STInstruction", "16b", stInstruction.Contains("16b"), "Copy attached to chart")%>
						</div>
					</div>
				</li>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_STInstruction", "17", stInstruction.Contains("17"), "Other", Model.Type + "_STInstructionOther", data.AnswerOrEmptyString("STInstructionOther"))%>
			</ul>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Assessment(Observations, instructions and measurable outcomes)</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_ObservationOutcomesTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_ObservationOutcomes", data.AnswerOrEmptyString("ObservationOutcomes"), new { @id = Model.Type + "_ObservationOutcomes" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Progress made towards goals</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_ProgressMadeTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_ProgressMade", data.AnswerOrEmptyString("ProgressMade"), new { @id = Model.Type + "_ProgressMade" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Patient/caregiver response to treatment</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericResponseTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_GenericResponse", data.AnswerOrEmptyString("GenericResponse"), new { @id = Model.Type + "_GenericResponse" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Teaching</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_TeachingTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_Teaching", data.AnswerOrEmptyString("Teaching"), new { @id = Model.Type + "_Teaching" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Plan</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericPlanTemplate")%>
	    		<%= Html.TextArea(Model.Type + "_GenericPlan", data.AnswerOrEmptyString("GenericPlan"), new { @id = Model.Type + "_GenericPlan" })%>
	    	</div>
		</div>
	</div>
</fieldset>