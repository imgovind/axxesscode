﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSpeechTherapyDone = data.AnswerArray("GenericSpeechTherapyDone"); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Treatment Diagnosis/Problem</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericTreatmentDiagnosisTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericTreatmentDiagnosis", data.AnswerOrEmptyString("GenericTreatmentDiagnosis"), new { @id = Model.Type + "_GenericTreatmentDiagnosis" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Short Term Goals</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericShortTermGoalsTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericShortTermGoals", data.AnswerOrEmptyString("GenericShortTermGoals"), new { @id = Model.Type + "_GenericShortTermGoals" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Therapy Done</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup two-wide">
				<input type="hidden" name="<%= Model.Type %>_GenericSpeechTherapyDone" value="" />
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone1' name='{1}_GenericSpeechTherapyDone' value='1' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("1").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone1">Speech (C2)</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone2' name='{1}_GenericSpeechTherapyDone' value='2' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("2").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone2">Lip, tongue, facial, exercises to improve swallowing/vocal skills</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone3' name='{1}_GenericSpeechTherapyDone' value='3' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("3").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone3">Voice</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone4' name='{1}_GenericSpeechTherapyDone' value='4' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("4").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone4">Dysphagia Treatments (C4)</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone5' name='{1}_GenericSpeechTherapyDone' value='5' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("5").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone5">Fluency</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{1}_GenericSpeechTherapyDone6' name='{1}_GenericSpeechTherapyDone' value='6' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("6").ToChecked(), Model.Type)%>
						<label for="<%= Model.Type %>_GenericSpeechTherapyDone6">Language Disorder (C5)</label>
					</div>
				</li>
			</ul>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Results Of Therapy Session</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericResultsOfTherapySessionTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericResultsOfTherapySession", data.AnswerOrEmptyString("GenericResultsOfTherapySession"), new { @id = Model.Type + "_GenericResultsOfTherapySession" })%>
	    	</div>
		</div>
	</div>
</fieldset>