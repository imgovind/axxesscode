﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/FunctionalLimitations/FormRev2", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
</div>
<% if (Model.Version == 2) { %>
	<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, true, Model.Type)); %>
<% } %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Supervisory Visit</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsSupervisoryApply", Model.Type + "_IsSupervisoryApply", "1", data.AnswerOrEmptyString("IsSupervisoryApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<div class="checkgroup">
							<% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
							<input type="hidden" name="<%= Model.Type %>_GenericSupervisoryVisit" value=" " />
							<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "1", genericSupervisoryVisit.Contains("1"), "Supervisory Visit")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "2", genericSupervisoryVisit.Contains("2"), "LPTA Present")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "3", genericSupervisoryVisit.Contains("3"), "Aide Present")%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericSupervisoryVisitComment">Comment</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_SupervisoryVisitTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Subjective</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsSubjectiveApply", Model.Type + "_IsSubjectiveApply", "1", data.AnswerOrEmptyString("IsSubjectiveApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
					    <div class="template-text">
    						<%= Html.ToggleTemplates(Model.Type + "_SubjectiveTemplates")%>
	    					<%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"), new { })%>
	    				</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Objective/FormRev2", new VisitNoteSectionViewData(data, true, Model.Type)); %>
	</div>
	<div>
		<fieldset>
			<legend>Bed Mobility Training</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsBedApply", Model.Type + "_IsBedApply", "1", data.AnswerOrEmptyString("IsBedApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<label for="<%= Model.Type %>_GenericRolling" class="fl">Rolling</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericRolling", data.AnswerOrEmptyString("GenericRolling"), new { })%>
							x
							<%= Html.TextBox(Model.Type + "_GenericRollingReps", data.AnswerOrEmptyString("GenericRollingReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericSupSit" class="fl">Sup-Sit</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericSupSit", data.AnswerOrEmptyString("GenericSupSit"), new { })%>
							x
							<%= Html.TextBox(Model.Type + "_GenericSupSitReps", data.AnswerOrEmptyString("GenericSupSitReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericScootingToward" class="fl">Scooting Toward</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericScootingToward", data.AnswerOrEmptyString("GenericScootingToward"), new { })%>
							x
							<%= Html.TextBox(Model.Type + "_GenericScootingTowardReps", data.AnswerOrEmptyString("GenericScootingTowardReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericSitToStand" class="fl">Sit to Stand</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericSitToStand", data.AnswerOrEmptyString("GenericSitToStand"), new { })%>
							x
							<%= Html.TextBox(Model.Type + "_GenericSitToStandReps", data.AnswerOrEmptyString("GenericSitToStandReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericBedMobilityTrainingComment">Comment</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_BedMobilityTrainingTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericBedMobilityTrainingComment", data.AnswerOrEmptyString("GenericBedMobilityTrainingComment"), new { })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/TransferTraining/FormRev1", Model); %>
	</div>
	<div>
		<fieldset>
			<legend>Gait Training</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsGaitApply", Model.Type + "_IsGaitApply", "1", data.AnswerOrEmptyString("IsGaitApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div id="<%=Model.Type %>_GaitContainer" class="collapsible-container">
				<div class="column">
					<div class="row">
						<label for="<%= Model.Type %>_GenericAmbulationDistance" class="fl">Ambulation</label>
						<div class="fr">
							<span>Distance</span>
							<%= Html.TextBox(Model.Type + "_GenericAmbulationDistance", data.AnswerOrEmptyString("GenericAmbulationDistance"), new { @class = "shorter less" })%>
							ft x
							<%= Html.TextBox(Model.Type + "_GenericAmbulationReps", data.AnswerOrEmptyString("GenericAmbulationReps"), new { @class = "shorter less" })%>
							reps
						</div>
					</div>
					<div class="row">
						<label class="fl">Assistive Device</label>
						<div class="fr">
							<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericAmbulationAssistiveDevice", data.AnswerOrEmptyString("GenericAmbulationAssistiveDevice"), new { })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericAmbulationAssist" class="fl">Assistance</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericAmbulationAssist", data.AnswerOrEmptyString("GenericAmbulationAssist"), new { })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericGaitQualityComment">Gait Quality/Deviation</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericGaitQualityCommentTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericGaitQualityComment", data.AnswerOrEmptyString("GenericGaitQualityComment"), new { @class = "tall" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericNumberofSteps" class="fl">Stairs</label>
						<div class="fr">
							<span># of Steps</span>
							<%= Html.TextBox(Model.Type + "_GenericNumberofSteps", data.AnswerOrEmptyString("GenericNumberofSteps"), new { @id = Model.Type + "_GenericNumberofSteps", @class = "shorter less" })%>
							<div class="checkgroup two-wide">
								<% string[] genericRails = data.AnswerArray("GenericRails"); %>
								<input type="hidden" name="<%= Model.Type %>_GenericRails" value="" />
								<%= Html.CheckgroupOption(Model.Type + "_GenericRails", "1", genericRails.Contains("1"), "Rail 1") %>
								<%= Html.CheckgroupOption(Model.Type + "_GenericRails", "2", genericRails.Contains("2"), "Rail 2") %>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="fl">Assistive Device</label>
						<div class="fr">
							<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericRailAssistiveDevice", data.AnswerOrEmptyString("GenericRailAssistiveDevice"), new { @id = Model.Type + "_GenericRailAssistiveDevice" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericStairsAssist" class="fl">Assistance</label>
						<div class="fr">
							<%= Html.TherapyAssistance(Model.Type + "_GenericStairsAssist", data.AnswerOrEmptyString("GenericStairsAssist"), new { @id = Model.Type + "_GenericStairsAssist" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericStairsQualityComment">Quality/Deviation</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericStairsQualityCommentTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericStairsQualityComment", data.AnswerOrEmptyString("GenericStairsQualityComment"), new { @id = Model.Type + "_GenericStairsQualityComment", @class = "tall" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Teaching</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsTeachingApply", Model.Type + "_IsTeachingApply", "1", data.AnswerOrEmptyString("IsTeachingApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<div class="checkgroup">
							<% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
							<input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
							<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Patient")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "2", genericTeaching.Contains("2"), "Caregiver")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "3", genericTeaching.Contains("3"), "HEP")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "4", genericTeaching.Contains("4"), "Safe Transfer")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "5", genericTeaching.Contains("5"), "Safe Gait")%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericTeachingOther" class="fl">Other</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericTeachingComment">Comment</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_GenericTeachingCommentTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericTeachingComment", data.AnswerOrEmptyString("GenericTeachingComment"), new { })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Pain</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsPainApply", Model.Type + "_IsPainApply", "1", data.AnswerOrEmptyString("IsPainApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<label for="<%= Model.Type %>_GenericPriorIntensityOfPain" class="fl">Pain level prior to therapy</label>
						<div class="fr">
							<%  var genericPriorIntensityOfPain = new SelectList(new[] {
                                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                new SelectListItem { Text = "1", Value = "1" },
                                new SelectListItem { Text = "2", Value = "2" },
                                new SelectListItem { Text = "3", Value = "3" },
                                new SelectListItem { Text = "4", Value = "4" },
                                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                new SelectListItem { Text = "6", Value = "6" },
                                new SelectListItem { Text = "7", Value = "7" },
                                new SelectListItem { Text = "8", Value = "8" },
                                new SelectListItem { Text = "9", Value = "9" },
                                new SelectListItem { Text = "10", Value = "10" }
                            }, "Value", "Text", data.AnswerOrDefault("GenericPriorIntensityOfPain", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericPriorIntensityOfPain", genericPriorIntensityOfPain, new { @class = "short" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPostIntensityOfPain" class="fl">Pain level after therapy</label>
						<div class="fr">
							<%  var genericPostIntensityOfPain = new SelectList(new[] {
                                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                new SelectListItem { Text = "1", Value = "1" },
                                new SelectListItem { Text = "2", Value = "2" },
                                new SelectListItem { Text = "3", Value = "3" },
                                new SelectListItem { Text = "4", Value = "4" },
                                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                new SelectListItem { Text = "6", Value = "6" },
                                new SelectListItem { Text = "7", Value = "7" },
                                new SelectListItem { Text = "8", Value = "8" },
                                new SelectListItem { Text = "9", Value = "9" },
                                new SelectListItem { Text = "10", Value = "10" }
                            }, "Value", "Text", data.AnswerOrDefault("GenericPostIntensityOfPain", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericPostIntensityOfPain", genericPostIntensityOfPain, new { @class = "short" })%>
						</div>
					</div>
					<div class="row">
						<img src="/Images/painscale.png" alt="Pain Scale Image" width="90%" /><br />
						<em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPainLocation" class="fl">Location</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericPainLocation", data.AnswerOrEmptyString("GenericPainLocation"), new { @id = Model.Type + "_GenericPainLocation" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPainRelievedBy" class="fl">Relieved by</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericPainRelievedBy", data.AnswerOrEmptyString("GenericPainRelievedBy"), new { @id = Model.Type + "_GenericPainRelievedBy" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPainProfileComment">Comment</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_PainProfileTemplates")%>
							<%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment" })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Plan</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsPlanApply", Model.Type + "_IsPlanApply", "1", data.AnswerOrEmptyString("IsPlanApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
						<label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="fl">Continue Prescribed Plan</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @class = "fl" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="fl">Change Prescribed Plan</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @class = "fl" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPlanDischarge" class="fl">Plan Discharge</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @class = "fl" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_GenericPainProfileComment">Comment</label>
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_PlanTemplates" )%>
							<%= Html.TextArea(Model.Type + "_GenericPlanComment", data.AnswerOrEmptyString("GenericPlanComment"), new { })%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Assessment</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsAssessmentApply", Model.Type + "_IsAssessmentApply", "1", data.AnswerOrEmptyString("IsAssessmentApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
					    <div class="template-text">
    						<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates")%>
	    					<%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @class = "tallest" })%>
	    				</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Narrative</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsNarrativeApply", Model.Type + "_IsNarrativeApply", "1", data.AnswerOrEmptyString("IsNarrativeApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
					    <div class="template-text">
    						<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
	    					<%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), new { })%>
	    				</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Narrative</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%= Html.CheckgroupOption(Model.Type + "_IsProgressApply", Model.Type + "_IsProgressApply", "1", data.AnswerOrEmptyString("IsProgressApply").Contains("1"), "N/A", new { @class = "toggle-container" })%>
					</div>
				</div>
			</div>
			<div class="collapsible-container">
				<div class="column">
					<div class="row">
					    <div class="template-text">
    						<%= Html.ToggleTemplates(Model.Type + "_GenericProgressTemplates")%>
	    					<%= Html.TextArea(Model.Type + "_GenericProgress", data.AnswerOrEmptyString("GenericProgress"), new { })%>
	    				</div>
					</div>
					<div class="row">
						<div class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_GoalsMet", "1", data.AnswerOrEmptyString("GoalsMet").Contains("1"), "Goals Met")%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev2", Model); %>