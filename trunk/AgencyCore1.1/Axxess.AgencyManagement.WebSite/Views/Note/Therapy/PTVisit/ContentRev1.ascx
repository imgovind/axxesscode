﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Functional Limitations</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup two-wide">
						<% string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "1", genericFunctionalLimitations.Contains("1"), "ROM/Strength")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "2", genericFunctionalLimitations.Contains("2"), "Pain")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "3", genericFunctionalLimitations.Contains("3"), "Safety Techniques")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "4", genericFunctionalLimitations.Contains("4"), "W/C Mobility")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "5", genericFunctionalLimitations.Contains("5"), "Balance/Gait")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "6", genericFunctionalLimitations.Contains("6"), "Bed Mobility")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericFunctionalLimitations", "7", genericFunctionalLimitations.Contains("7"), "Transfer")%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Supervisory Visit</legend>
			<div class="column">
				<div class="row">
					<div class="checkgroup two-wide">
						<% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericSupervisoryVisit" value=" " />
						<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "1", genericSupervisoryVisit.Contains("1"), "Supervisory Visit")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "2", genericSupervisoryVisit.Contains("2"), "LPTA Present")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSupervisoryVisitComment">Comments</label>
					<div class="ac"><%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment" })%></div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Vital Signs</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericPriorBP" class="fl">Prior BP</label>
					<div class="fr"><%= Html.TextBox(Model.Type+"_GenericPriorBP", data.AnswerOrEmptyString("GenericPriorBP"), new { @class = "shorter", @id = Model.Type+"_GenericPriorBP" })%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPostBP" class="fl">Post BP</label>
					<div class="fr"><%= Html.TextBox(Model.Type+"_GenericPostBP", data.AnswerOrEmptyString("GenericPostBP"), new { @class = "shorter", @id = Model.Type+"_GenericPostBP" })%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPriorPulse" class="fl">Prior Pulse</label>
					<div class="fr"><%= Html.TextBox(Model.Type+"_GenericPriorPulse", data.AnswerOrEmptyString("GenericPriorPulse"), new { @class = "shorter", @id = Model.Type+"_GenericPriorPulse" })%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPostPulse" class="fl">Post Pulse</label>
					<div class="fr"><%= Html.TextBox(Model.Type+"_GenericPostPulse", data.AnswerOrEmptyString("GenericPostPulse"), new { @class = "shorter", @id = Model.Type+"_GenericPostPulse" })%></div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Subjective</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericSubjectiveTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"), new { @id = Model.Type + "_GenericSubjective" })%>
	    		    </div>
			    </div>
		    </div>
	    </fieldset>
    </div>
</div>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Objective/FormRev1", Model); %>
		<fieldset>
			<legend>Transfer Training</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericTransferTraining" class="fl">Transfer Training</label>
					<div class="fr">
						x
						<%= Html.TextBox(Model.Type + "_GenericTransferTraining", data.AnswerOrEmptyString("GenericTransferTraining"), new { @id = Model.Type + "_GenericTransferTraining", @class = "short" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label>Assistive Device</label>
					<div class="checkgroup two-wide">
						<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericAssistiveDevice", "1", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1"), true, "Yes", Model.Type + "_GenericAssistiveDeviceSpecify", data.AnswerOrEmptyString("GenericAssistiveDeviceSpecify"))%>
						<%= Html.CheckgroupRadioOption(Model.Type + "_GenericAssistiveDevice", "0", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0"), "No")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericBedChairAssist" class="fl">Bed &#8212; Chair</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericBedChairAssist", data.AnswerOrEmptyString("GenericBedChairAssist"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericChairToiletAssist" class="fl">Chair &#8212; Toilet</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericChairToiletAssist", data.AnswerOrEmptyString("GenericChairToiletAssist"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericChairCarAssist" class="fl">Chair &#8212; Car</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericChairCarAssist", data.AnswerOrEmptyString("GenericChairCarAssist"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="" class="fl">Sitting Balance Activities WB UE</label>
					<div class="fr">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericSBAWE", "1", data.AnswerOrEmptyString("GenericSBAWE").Equals("1"), "Yes")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericSBAWE", "0", data.AnswerOrEmptyString("GenericSBAWE").Equals("0"), "No")%>
						</div>
					</div>
					<div class="fr">
						<br />
						<label class="fl" for="<%= Model.Type %>_GenericSBAWEStaticAssist">Static</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSBAWEStaticAssist", data.AnswerOrEmptyString("GenericSBAWEStaticAssist"), new { @id = Model.Type + "_GenericSBAWEStaticAssist", @class = "short" })%>
							% Assist
						</div>
						<br />
						<div class="clr">
						</div>
						<label class="fl" for="<%= Model.Type %>_GenericSBAWEDynamicAssist">Dynamic</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSBAWEDynamicAssist", data.AnswerOrEmptyString("GenericSBAWEDynamicAssist"), new { @id = Model.Type + "_GenericSBAWEDynamicAssist", @class = "short" })%>
							% Assist
						</div>
					</div>
				</div>
				<div class="row">
					<label for="" class="fl">Standing Balance Activities</label>
					<div class="fr">
						<label class="fl" for="<%= Model.Type %>_GenericSBAStaticAssist">Static</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSBAStaticAssist", data.AnswerOrEmptyString("GenericSBAStaticAssist"), new { @id = Model.Type + "_GenericSBAStaticAssist", @class = "short" })%>
							% Assist
						</div>
						<br />
						<div class="clr">
						</div>
						<label class="fl" for="<%= Model.Type %>_GenericSBADynamicAssist">Dynamic</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSBADynamicAssist", data.AnswerOrEmptyString("GenericSBADynamicAssist"), new { @id = Model.Type + "_GenericSBADynamicAssist", @class = "short" })%>
							% Assist
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Bed Mobility Training</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericRolling" class="fl">Rolling</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRolling", data.AnswerOrEmptyString("GenericRolling"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericRollingReps", data.AnswerOrEmptyString("GenericRollingReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSupSit" class="fl">Sup-Sit</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSupSit", data.AnswerOrEmptyString("GenericSupSit"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericSupSitReps", data.AnswerOrEmptyString("GenericSupSitReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericScootingToward" class="fl">Scooting Toward</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericScootingToward", data.AnswerOrEmptyString("GenericScootingToward"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericScootingTowardReps", data.AnswerOrEmptyString("GenericScootingTowardReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSitToStand" class="fl">Sit to Stand</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericSitToStand", data.AnswerOrEmptyString("GenericSitToStand"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericSitToStandReps", data.AnswerOrEmptyString("GenericSitToStandReps"), new { @placeholder = "rep(s)", @class = "shorter less" })%>
					</div>
				</div>
				<div class="row">
					<label class="fl">Proper Foot Placement</label>
					<div class="fr">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericProperFootPlacement", "1", data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("1"), "Yes")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericProperFootPlacement", "0", data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("0"), "No")%>
						</div>
					</div>
				</div>
				<div class="row">
					<label class="fl">Proper Unfolding</label>
					<div class="fr">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericProperUnfolding", "1", data.AnswerOrEmptyString("GenericProperUnfolding").Equals("1"), "Yes")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericProperUnfolding", "0", data.AnswerOrEmptyString("GenericProperUnfolding").Equals("0"), "No")%>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Gait Training</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericGaitTrainingFt">Gait Training</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericGaitTrainingFt", data.AnswerOrEmptyString("GenericGaitTrainingFt"), new { @id = Model.Type + "_GenericGaitTrainingFt", @class = "shorter" })%>
						ft. X
						<%= Html.TextBox(Model.Type + "_GenericGaitTrainingFtX", data.AnswerOrEmptyString("GenericGaitTrainingFtX"), new { @id = Model.Type + "_GenericGaitTrainingFtX", @class = "short" })%>
					</div>
					<div class="checkgroup two-wide">
						<% string[] genericWalkDirection = data.AnswerArray("GenericWalkDirection"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericWalkDirection" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericWalkDirection", "1", genericWalkDirection.Contains("1"), "Walking Sideways")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericWalkDirection", "2", genericWalkDirection.Contains("2"), "Walking Backwards")%>
					</div>
				</div>
				<div class="row">
					<label class="fl">With Device</label>
					<div class="fr">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericGaitTrainingWithDevice", "1", data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("1"), "Yes")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericGaitTrainingWithDevice", "0", data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("0"), "No")%>
						</div>
						<%= Html.TextBox(Model.Type + "_GenericGaitTrainingWithDeviceAssist", data.AnswerOrEmptyString("GenericGaitTrainingWithDeviceAssist"), new { @id = Model.Type + "_GenericGaitTrainingWithDeviceAssist", @class = "short" })%>
						% Assist
					</div>
				</div>
				<div class="sub row">
					<label for="" class="fl">Turning</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTurningDistance", data.AnswerOrEmptyString("GenericTurningDistance"), new { @id = Model.Type + "_GenericTurningDistance", @class = "short" })%>
						Distance
					</div>
				</div>
				<div class="sub row">
					<label for="" class="fl">Uneven</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericUnevenAssist", data.AnswerOrEmptyString("GenericUnevenAssist"), new { @id = Model.Type + "_GenericUnevenAssist", @class = "short" })%>
						% Assist
						<%= Html.TextBox(Model.Type + "_GenericUnevenDevice", data.AnswerOrEmptyString("GenericUnevenDevice"), new { @id = Model.Type + "_GenericUnevenDevice", @class = "short" })%>
						Device
					</div>
				</div>
				<div class="sub row">
					<label for="" class="fl">Stairs/ Steps</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericStairsStepsAssist", data.AnswerOrEmptyString("GenericStairsStepsAssist"), new { @id = Model.Type + "_GenericStairsStepsAssist", @class = "short" })%>
						% Assist
						<%= Html.TextBox(Model.Type + "_GenericStairsStepsDevice", data.AnswerOrEmptyString("GenericStairsStepsDevice"), new { @id = Model.Type + "_GenericStairsStepsDevice", @class = "short" })%>
						Device
					</div>
				</div>
				<div class="sub row">
					<label for="" class="fl">W/C Training</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericWCTrainingAssist", data.AnswerOrEmptyString("GenericWCTrainingAssist"), new { @id = Model.Type + "_GenericWCTrainingAssist", @class = "short" })%>
						% Assist
					</div>
				</div>
				<div class="row">
					<div class="checkgroup">
						<%  string[] genericTherapyTraning = data.AnswerArray("GenericTherapyTraning"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericTherapyTraning" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "1", genericTherapyTraning.Contains("1"), "WB (L)")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "2", genericTherapyTraning.Contains("2"), "WB (R)")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "3", genericTherapyTraning.Contains("3"), "Hip/Knee Extension")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "4", genericTherapyTraning.Contains("4"), "Stance Phase (L)")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "5", genericTherapyTraning.Contains("5"), "Stance Phase (R)")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "6", genericTherapyTraning.Contains("6"), "Heel Strike")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "7", genericTherapyTraning.Contains("7"), "Step Length")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "8", genericTherapyTraning.Contains("8"), "Proper Posture")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTherapyTraning", "9", genericTherapyTraning.Contains("9"), "Speed")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericGaitTrainingOther" class="fl">Other</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericGaitTrainingOther", data.AnswerOrEmptyString("GenericGaitTrainingOther"), new { @id = Model.Type + "_GenericGaitTrainingOther" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Pain</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericPriorIntensityOfPain" class="fl">Pain level prior to therapy</label>
					<div class="fr">
						<%  var genericPriorIntensityOfPain = new SelectList(new[] {
                                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                new SelectListItem { Text = "1", Value = "1" },
                                new SelectListItem { Text = "2", Value = "2" },
                                new SelectListItem { Text = "3", Value = "3" },
                                new SelectListItem { Text = "4", Value = "4" },
                                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                new SelectListItem { Text = "6", Value = "6" },
                                new SelectListItem { Text = "7", Value = "7" },
                                new SelectListItem { Text = "8", Value = "8" },
                                new SelectListItem { Text = "9", Value = "9" },
                                new SelectListItem { Text = "10", Value = "10" }
                            }, "Value", "Text", data.AnswerOrDefault("GenericPriorIntensityOfPain", "0")); %>
						<%= Html.DropDownList(Model.Type + "_GenericPriorIntensityOfPain", genericPriorIntensityOfPain, new { @class = "short" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPostIntensityOfPain" class="fl">Pain level after therapy</label>
					<div class="fr">
						<%  var genericPostIntensityOfPain = new SelectList(new[] {
                                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                new SelectListItem { Text = "1", Value = "1" },
                                new SelectListItem { Text = "2", Value = "2" },
                                new SelectListItem { Text = "3", Value = "3" },
                                new SelectListItem { Text = "4", Value = "4" },
                                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                new SelectListItem { Text = "6", Value = "6" },
                                new SelectListItem { Text = "7", Value = "7" },
                                new SelectListItem { Text = "8", Value = "8" },
                                new SelectListItem { Text = "9", Value = "9" },
                                new SelectListItem { Text = "10", Value = "10" }
                            }, "Value", "Text", data.AnswerOrDefault("GenericPostIntensityOfPain", "0")); %>
						<%= Html.DropDownList(Model.Type + "_GenericPostIntensityOfPain", genericPostIntensityOfPain, new { @class = "short" })%>
					</div>
				</div>
				<div class="row">
					<img src="/Images/painscale.png" alt="Pain Scale Image" width="90%" /><br />
					<em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPainLocation" class="fl">Location</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainLocation", data.AnswerOrEmptyString("GenericPainLocation"), new { @id = Model.Type + "_GenericPainLocation" })%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPainRelievedBy" class="fl">Relieved by</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainRelievedBy", data.AnswerOrEmptyString("GenericPainRelievedBy"), new { @id = Model.Type + "_GenericPainRelievedBy" })%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPainProfileComment">Comments</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_PainProfileTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Teaching</legend>
			<div class="column">
				<div class="row">
					<div class="checkgroup">
						<% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Patient")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Caregiver")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "HEP")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Safe Transfer")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Safe Gait")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTeachingOther" class="fl">Other</label>
					<div class="fl"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther" })%></div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Assessment</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @id = Model.Type + "_GenericAssessment" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Plan</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="fl">Continue Prescribed Plan</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @id = Model.Type + "_GenericContinuePrescribedPlan"})%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="fl">Change Prescribed Plan</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @id = Model.Type + "_GenericChangePrescribedPlan"})%></div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPlanDischarge" class="fl">Plan Discharge</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @id = Model.Type + "_GenericPlanDischarge"})%></div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Narrative</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), new { @id = Model.Type + "_GenericNarrativeComment" })%>
	    	</div>
		</div>
	</div>
</fieldset>