﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if(Model.VitalSignNewVersion != 0 && Model.Version >= Model.VitalSignNewVersion) { %>
	<% Html.RenderPartial("Shared/VitalSigns/FormRev3", Model, ViewData); %>
<% } else { %>
	<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<% } %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Mental Assessment</legend>
			<div class="column">
				<div class="row">
					<label class="fl">Orientation</label>
					<%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.AnswerOrEmptyString("GenericMentalAssessmentOrientation"), new { @class = "fr", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%>
				</div>
				<div class="row">
					<label class="fl">Level of Consciousness</label>
					<%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.AnswerOrEmptyString("GenericMentalAssessmentLOC"), new { @class = "fr", @id = Model.Type + "_GenericMentalAssessmentLOC" })%>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericMentalAssessmentComment">Comments</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_MentalAssessmentTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericMentalAssessmentComment", data.AnswerOrEmptyString("GenericMentalAssessmentComment"), new { @id = Model.Type + "_GenericMentalAssessmentComment" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<fieldset>
			<legend>DME</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericDMEAvailable" class="fl">Available</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.AnswerOrEmptyString("GenericDMEAvailable"), new { @id = Model.Type + "_GenericDMEAvailable" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericDMENeeds" class="fl">Needs</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.AnswerOrEmptyString("GenericDMENeeds"), new { @id = Model.Type + "_GenericDMENeeds" })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericDMESuggestion" class="fl">Suggestion</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.AnswerOrEmptyString("GenericDMESuggestion"), new { @id = Model.Type + "_GenericDMESuggestion" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/LivingSituation/FormRev1", Model); %>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/PhysicalAssessment/FormRev4", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<% Html.RenderPartial("Therapy/Shared/LevelOfFunction/FormRev1", Model); %>
<fieldset>
	<legend>Prior Therapy Received</legend>
	<div class="wide-column">
		<div class="row">
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_PriorTherapyTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericPriorTherapyReceived", data.AnswerOrEmptyString("GenericPriorTherapyReceived"), new { @id = Model.Type + "_GenericPriorTherapyReceived" })%>
			</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1"); %>
<fieldset>
	<legend>Bed Mobility</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label class="fl">Rolling to Right</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceRight" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Rolling to Left</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceLeft" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sit Stand Sit</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySitStandSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sup to Sit</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySupToSitAssistance", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistance" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilityComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_BedMobilityTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericBedMobilityComment", data.AnswerOrEmptyString("GenericBedMobilityComment"), new { @id = Model.Type + "_GenericBedMobilityComment" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label class="fl">Rolling to Right</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceRight1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceRight1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceRight1" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceRight1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceRight1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Rolling to Left</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistanceLeft1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceLeft1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistanceLeft1" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft1", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDeviceLeft1"), new { @id = Model.Type + "_GenericBedMobilityRollingAssistiveDeviceLeft1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sit Stand Sit</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySitStandSitAssistance1", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistance1"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistance1" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice1", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice1"), new { @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sup to Sit</label>
			<div class="fr">
				<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilitySupToSitAssistance1", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistance1"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistance1" })%>
				<br />
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice1", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice1"), new { @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilityComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_BedMobilityTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericBedMobilityComment", data.AnswerOrEmptyString("GenericBedMobilityComment"), new { @id = Model.Type + "_GenericBedMobilityComment" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Transfers</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label class="fl">Bed-Chair</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferBedChairAssistance", data.AnswerOrEmptyString("GenericTransferBedChairAssistance"), new { @id = Model.Type + "_GenericTransferBedChairAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferBedChairAssistiveDevice", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice"), new { @id = Model.Type + "_GenericTransferBedChairAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Chair-Bed</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairBedAssistance", data.AnswerOrEmptyString("GenericTransferChairBedAssistance"), new { @id = Model.Type + "_GenericTransferChairBedAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairBedAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairBedAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Chair to W/C</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairToWCAssistance", data.AnswerOrEmptyString("GenericTransferChairToWCAssistance"), new { @id = Model.Type + "_GenericTransferChairToWCAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairToWCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Toilet or BSC</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferToiletOrBSCAssistance", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Car/Van</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferCanVanAssistance", data.AnswerOrEmptyString("GenericTransferCanVanAssistance"), new { @id = Model.Type + "_GenericTransferCanVanAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferCanVanAssistiveDevice", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice"), new { @id = Model.Type + "_GenericTransferCanVanAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Tub/Shower</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferTubShowerAssistance", data.AnswerOrEmptyString("GenericTransferTubShowerAssistance"), new { @id = Model.Type + "_GenericTransferTubShowerAssistance" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTubShowerAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice"), new { @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sitting Balance</label>
			<div class="fr ar">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericTransferSittingBalanceStatic", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic"), new { @id = Model.Type + "_GenericTransferSittingBalanceStatic" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericTransferSittingDynamicAssist", data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist"), new { @id = Model.Type + "_GenericTransferSittingDynamicAssist" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Stand Balance</label>
			<div class="fr ar">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericTransferStandBalanceStatic", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic"), new { @id = Model.Type + "_GenericTransferStandBalanceStatic" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericTransferStandBalanceDynamic", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic"), new { @id = Model.Type + "_GenericTransferStandBalanceDynamic" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTransferComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_TransferCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericTransferComment", data.AnswerOrEmptyString("GenericTransferComment"), new { @id = Model.Type + "_GenericTransferComment" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label class="fl">Bed-Chair</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferBedChairAssistance1", data.AnswerOrEmptyString("GenericTransferBedChairAssistance1"), new { @id = Model.Type + "_GenericTransferBedChairAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferBedChairAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferBedChairAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Chair-Bed</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairBedAssistance1", data.AnswerOrEmptyString("GenericTransferChairBedAssistance1"), new { @id = Model.Type + "_GenericTransferChairBedAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairBedAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferChairBedAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Chair to W/C</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferChairToWCAssistance1", data.AnswerOrEmptyString("GenericTransferChairToWCAssistance1"), new { @id = Model.Type + "_GenericTransferChairToWCAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferChairToWCAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Toilet or BSC</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferToiletOrBSCAssistance1", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistance1"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Car/Van</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferCanVanAssistance1", data.AnswerOrEmptyString("GenericTransferCanVanAssistance1"), new { @id = Model.Type + "_GenericTransferCanVanAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferCanVanAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferCanVanAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Tub/Shower</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericTransferTubShowerAssistance1", data.AnswerOrEmptyString("GenericTransferTubShowerAssistance1"), new { @id = Model.Type + "_GenericTransferTubShowerAssistance1" })%>
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTransferTubShowerAssistiveDevice1", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice1"), new { @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Sitting Balance</label>
			<div class="fr ar">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericTransferSittingBalanceStatic1", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic1"), new { @id = Model.Type + "_GenericTransferSittingBalanceStatic1" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericTransferSittingDynamicAssist1", data.AnswerOrEmptyString("GenericTransferSittingDynamicAssist1"), new { @id = Model.Type + "_GenericTransferSittingDynamicAssist1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Stand Balance</label>
			<div class="fr ar">
				<label>Static</label>
				<%= Html.StaticBalance(Model.Type + "_GenericTransferStandBalanceStatic1", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic1"), new { @id = Model.Type + "_GenericTransferStandBalanceStatic1" })%>
				<br />
				<label>Dynamic</label>
				<%= Html.DynamicBalance(Model.Type + "_GenericTransferStandBalanceDynamic1", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic1"), new { @id = Model.Type + "_GenericTransferStandBalanceDynamic1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTransferComment1">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_TransferComment1Templates")%>
				<%= Html.TextArea(Model.Type + "_GenericTransferComment1", data.AnswerOrEmptyString("GenericTransferComment1"), new { @id = Model.Type + "_GenericTransferComment1" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Gait Analysis</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitLevelAssist" class="fl">Level</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitLevelAssist", data.AnswerOrEmptyString("GenericGaitLevelAssist"), new { @id = Model.Type + "_GenericGaitLevelAssist" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitLevelFeet", data.AnswerOrEmptyString("GenericGaitLevelFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitLevelFeet" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="fl">Unlevel</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitUnLevelAssist", data.AnswerOrEmptyString("GenericGaitUnLevelAssist"), new { @id = Model.Type + "_GenericGaitUnLevelAssist" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet", data.AnswerOrEmptyString("GenericGaitUnLevelFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitUnLevelFeet" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitStepStairAssist" class="fl">Step/ Stair</label>
			<div class="fr ar">
				<div class="checkgroup three-wide long al">
					<% string[] genericGaitStepStairRail = data.AnswerArray("GenericGaitStepStairRail"); %>
					<%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "1", genericGaitStepStairRail.Contains("1"), "No Rail")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "2", genericGaitStepStairRail.Contains("2"), "1 Rail")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericGaitStepStairRail", "3", genericGaitStepStairRail.Contains("3"), "2 Rail")%>
				</div>
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitStepStairAssist", data.AnswerOrEmptyString("GenericGaitStepStairAssist"), new { @id = Model.Type + "_GenericGaitStepStairAssist" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet", data.AnswerOrEmptyString("GenericGaitStepStairFeet"), new { @class = "shorter", @id = Model.Type + "_GenericGaitStepStairFeet" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Assistive Device</label>
			<div class="fr">
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericGaitAnalysisAssistiveDevice", data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice"), new { @id = Model.Type + "_GenericGaitAnalysisAssistiveDevice" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitComment">Gait Quality/Deviation</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GaitCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericGaitComment", data.ContainsKey("GenericGaitComment") ? data["GenericGaitComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitLevelAssist1" class="fl">Level</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitLevelAssist1", data.AnswerOrEmptyString("GenericGaitLevelAssist1"), new { @id = Model.Type + "_GenericGaitLevelAssist1" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitLevelFeet1", data.AnswerOrEmptyString("GenericGaitLevelFeet1"), new { @class = "shorter", @id = Model.Type + "_GenericGaitLevelFeet1" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitUnLevelAssist1" class="fl">Unlevel</label>
			<div class="fr ar">
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitUnLevelAssist1", data.AnswerOrEmptyString("GenericGaitUnLevelAssist1"), new { @id = Model.Type + "_GenericGaitUnLevelAssist1" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet1", data.AnswerOrEmptyString("GenericGaitUnLevelFeet1"), new { @class = "shorter", @id = Model.Type + "_GenericGaitUnLevelFeet1" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitStepStairAssist1" class="fl">Step/ Stair</label>
			<div class="fr ar">
				<div class="checkgroup three-wide long al">
					<% string[] genericGaitStepStairRail1 = data.AnswerArray("GenericGaitStepStairRail1"); %>
					<div class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{1}_GenericGaitStepStairRail1x' name='{1}_GenericGaitStepStairRail1' value='1' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("1").ToChecked(), Model.Type)%>
							<label for="<%= Model.Type %>_GenericGaitStepStairRail1x">No Rail</label>
						</div>
					</div>
					<div class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{1}_GenericGaitStepStairRail2x' name='{1}_GenericGaitStepStairRail1' value='2' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("2").ToChecked(), Model.Type)%>
							<label for="<%= Model.Type %>_GenericGaitStepStairRail2x">1 Rail</label>
						</div>
					</div>
					<div class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{1}_GenericGaitStepStairRail3x' name='{1}_GenericGaitStepStairRail1' value='3' type='checkbox' {0} />", genericGaitStepStairRail1.Contains("3").ToChecked(), Model.Type)%>
							<label for="<%= Model.Type %>_GenericGaitStepStairRail3x">2 Rails</label>
						</div>
					</div>
				</div>
				<%= Html.TherapyAssistance(Model.Type + "_GenericGaitStepStairAssist1", data.AnswerOrEmptyString("GenericGaitStepStairAssist1"), new { @id = Model.Type + "_GenericGaitStepStairAssist1" })%>
				x
				<%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet1", data.AnswerOrEmptyString("GenericGaitStepStairFeet1"), new { @class = "shorter", @id = Model.Type + "_GenericGaitStepStairFeet1" })%>
				ft
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Assistive Device</label>
			<div class="fr">
				<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericGaitAnalysisAssistiveDevice1", data.AnswerOrEmptyString("GenericGaitAnalysisAssistiveDevice1"), new { @id = Model.Type + "_GenericGaitAnalysisAssistiveDevice1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericGaitComment1">Gait Quality/Deviation</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GaitComment1Templates")%>
				<%= Html.TextArea(Model.Type + "_GenericGaitComment1", data.ContainsKey("GenericGaitComment1") ? data["GenericGaitComment1"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment1" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>WB</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_GenericWBStatus">Status</label>
			<div class="fr">
				<%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus", data.AnswerOrEmptyString("GenericWBStatus"), new { @id = Model.Type + "_GenericWBStatus" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_GenericWBStatusOther">Other</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWBStatusOther", data.AnswerOrEmptyString("GenericWBStatusOther"), new {@id = Model.Type + "_GenericWBStatusOther" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericWBSComment">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_WBSCommentTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericWBSComment", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_GenericWBStatus1">Status</label>
			<div class="fr">
				<%= Html.WeightBearingStatus(Model.Type + "_GenericWBStatus1", data.AnswerOrEmptyString("GenericWBStatus1"), new { @id = Model.Type + "_GenericWBStatus1" })%>
			</div>
		</div>
		<div class="sub row">
			<label class="fl" for="<%= Model.Type %>_GenericWBStatusOther1">Other</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericWBStatusOther1", data.AnswerOrEmptyString("GenericWBStatusOther1"), new { @id = Model.Type + "_GenericWBStatusOther1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericWBSComment1">Comments</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_WBSComment1Templates")%>
				<%= Html.TextArea(Model.Type + "_GenericWBSComment1", data.AnswerOrEmptyString("GenericWBSComment"), new { @id = Model.Type + "_GenericWBSComment11" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Standardized Test</legend>
	<div class="column">
		<div class="row">
			<label>Prior</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTinettiPOMA" class="fl">Tinetti POMA</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTinettiPOMA", data.AnswerOrEmptyString("GenericTinettiPOMA"), new { @id = Model.Type + "_GenericTinettiPOMA" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTimedGetUp" class="fl">Timed Get Up & Go Test</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTimedGetUp", data.AnswerOrEmptyString("GenericTimedGetUp"), new { @id = Model.Type + "_GenericTimedGetUp" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericFunctionalReach" class="fl">Functional Reach</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericFunctionalReach", data.AnswerOrEmptyString("GenericFunctionalReach"), new { @id = Model.Type + "_GenericFunctionalReach" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericStandardizedTestOther">Other</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericStandardizedTestOtherTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther", data.AnswerOrEmptyString("GenericStandardizedTestOther"), new { @id = Model.Type + "_GenericStandardizedTestOther" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Current</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTinettiPOMA1" class="fl">Tinetti POMA</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTinettiPOMA1", data.AnswerOrEmptyString("GenericTinettiPOMA1"), new { @id = Model.Type + "_GenericTinettiPOMA1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTimedGetUp1" class="fl">Timed Get Up & Go Test</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTimedGetUp1", data.AnswerOrEmptyString("GenericTimedGetUp1"), new { @id = Model.Type + "_GenericTimedGetUp1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericFunctionalReach1" class="fl">Functional Reach</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericFunctionalReach1", data.AnswerOrEmptyString("GenericFunctionalReach1"), new { @id = Model.Type + "_GenericFunctionalReach1" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericStandardizedTestOther1">Other</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericStandardizedTestOther1Templates")%>
				<%= Html.TextArea(Model.Type + "_GenericStandardizedTestOther1", data.AnswerOrEmptyString("GenericStandardizedTestOther1"), new { @id = Model.Type + "_GenericStandardizedTestOther1" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Progress Towards Goals</legend>
	<div class="wide-column">
		<div class="row no-input">
			<label for="<%= Model.Type %>_GenericFactors">Indicate all factors influencing the patient&#8217;s progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericFactors", data.AnswerOrEmptyString("GenericFactors"), new { @id = Model.Type + "_GenericFactors" })%>
			</div>
		</div>
		<div class="row no-input">
			<label for="<%= Model.Type %>_GenericExpectations">Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician&#8217;s professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericExpectations", data.AnswerOrEmptyString("GenericExpectations"), new { @id = Model.Type + "_GenericExpectations" })%>
			</div>
		</div>
		<div class="row no-input">
			<label for="<%= Model.Type %>_GenericRecommendations">Indicate recommended modifications to the existing Interventions &#38; Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericRecommendations", data.AnswerOrEmptyString("GenericRecommendations"), new { @id = Model.Type + "_GenericRecommendations" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Plan of Care Status</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup two-wide">
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericChangedPOC", "1", data.AnswerOrEmptyString("GenericChangedPOC").Equals("1"), "POC Unchanged (Physician signature not required)")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericChangedPOC", "0", data.AnswerOrEmptyString("GenericChangedPOC").Equals("0"), "POC Changed (Physician signature required)")%>
			</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Care Coordination</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_CareCoordinationTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Skilled Care Provided This Visit</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_SkilledCareProvidedTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.AnswerOrEmptyString("GenericSkilledCareProvided"), new { @id = Model.Type + "_GenericSkilledCareProvided" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
