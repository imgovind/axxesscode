﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Diagnosis/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/LivingSituation/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/History/FormRev1", Model); %>
		<fieldset>
			<legend>Homebound Status</legend>
			<div class="column">
				<div class="row">
					<div class="checkgroup one-wide">
						<% string[] genericHomeBoundStatus = Model.Questions.AnswerOrEmptyString("GenericHomeBoundStatus") != "" ? Model.Questions["GenericHomeBoundStatus"].Answer.Split(',') : Model.Questions.AnswerArray("GenericHomeboundStatusAssist"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericHomeBoundStatus" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "1", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1"), "Needs assist with transfer.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "2", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2"), "Needs assist with ambulation.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "3", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3"), "Needs assist leaving the home.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "4", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4"), "Unable to be up for long period.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "5", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("5"), "Severe SOB upon exertion.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "6", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("6"), "Unsafe to go out of home alone.")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "7", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("7"), "Requires considerable and taxing effort.") %>
						<%= Html.CheckgroupOption(Model.Type + "_GenericHomeBoundStatus", "8", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("8"), "Medical restriction.")%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<% Html.RenderPartial("Therapy/Shared/ADLs/FormRev2", Model); %>
<% Html.RenderPartial("Therapy/Shared/OTPhysicalAssessment/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<% Html.RenderPartial("Therapy/Shared/CognitiveStatus/FormRev1", Model); %>
		<% Html.RenderPartial("Therapy/Shared/MotorComponents/FormRev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Sensory/FormRev1", Model); %>
		<fieldset>
			<legend>Assessment</legend>
			<div class="wide-column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentMoreTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.AnswerOrEmptyString("GenericAssessmentMore"), new { @id = Model.Type + "_GenericAssessmentMore" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Progress Towards Goals</legend>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericFactors">Indicate all factors influencing the patient&#8217;s progress or lack of progress related to the established Interventions and Goals. (Caregiver and/or environment; medication, adaptive equipment, decline in or unstable medical condition, exacerbation or stabilization of existing diagnosis etc.)</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericFactorsTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericFactors", data.AnswerOrEmptyString("GenericFactors"), new { @id = Model.Type + "_GenericFactors" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericExpectations">Indicate the expectation of progress toward established goals within the established timeframe. (document the clinician&#8217;s professional opinion as to the effectiveness of the established POC based on the patient response, to date, using objective references)</label>
            <div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericExpectationsTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericExpectations", data.AnswerOrEmptyString("GenericExpectations"), new { @id = Model.Type + "_GenericExpectations" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericRecommendations">Indicate recommended modifications to the existing Interventions & Goals, including timeframe and why are the therapists skills needed to achieve optimal outcomes.</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericRecommendationsTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericRecommendations", data.AnswerOrEmptyString("GenericRecommendations"), new { @id = Model.Type + "_GenericRecommendations" })%>
			</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Therapy/Shared/OTStandardizedTest/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Care Coordination</legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericCareCoordinationTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.AnswerOrEmptyString("GenericCareCoordination"), new { @id = Model.Type + "_GenericCareCoordination" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Skilled Treatment Provided This Visit </legend>
			<div class="column">
				<div class="row">
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericSkilledTreatmentTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericSkilledTreatmentProvided", data.AnswerOrEmptyString("GenericSkilledTreatmentProvided"), new { @id = Model.Type + "_GenericSkilledTreatmentProvided" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>