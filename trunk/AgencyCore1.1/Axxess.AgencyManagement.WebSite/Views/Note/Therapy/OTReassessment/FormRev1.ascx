﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.PatientProfile.DisplayName%></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" })) { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model) %>
    <fieldset>
		<legend>Reassessment Details</legend>
		<div class="column">
			<div class="row">
				<label for="<%=Model.Type %>_ReassessmentType" class="fl">Reassessment Type</label>
                <div class="fr">
					<% var type = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "11th Visit", Value = "11th Visit" },
							new SelectListItem { Text = "12th Visit", Value = "12th Visit" },
							new SelectListItem { Text = "13th Visit", Value = "13th Visit" },
							new SelectListItem { Text = "17th Visit", Value = "17th Visit" },
							new SelectListItem { Text = "18th Visit", Value = "18th Visit" },
							new SelectListItem { Text = "19th Visit", Value = "19th Visit" },
							new SelectListItem { Text = "30 day Visit", Value = "30 day Visit" },
							new SelectListItem { Text = "Other", Value = "Other" }
						}, "Value", "Text", data.AnswerOrDefault("ReassessmentType", "0")); %>
					<%= Html.DropDownList(Model.Type + "_ReassessmentType", type, new { @id = Model.Type + "_ReassessmentType"})%>
                </div>
			</div>
		</div>
    </fieldset>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("Therapy/OTReassessment/ContentRev" + Model.Version, Model); %></div>
    <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>