﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% if (Model.Version == 2) { %>
	<% Html.RenderPartial("Therapy/Shared/VitalSigns/FormRev2", new VisitNoteSectionViewData(data, false, Model.Type)); %>
<% } %>
<fieldset>
	<legend>Subjective</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericSubjectiveTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"), new { })%>
	    	</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<fieldset>
			<legend>Teaching</legend>
			<div class="column">
				<div class="row">
					<div class="checkgroup one-wide">
						<%  string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Patient/Family")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "2", genericTeaching.Contains("2"), "Caregiver")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "3", genericTeaching.Contains("3"), "Correct Use of Adaptive Equipment")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "4", genericTeaching.Contains("4"), "Safety Technique")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "5", genericTeaching.Contains("5"), "ADLs")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "6", genericTeaching.Contains("6"), "HEP")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "7", genericTeaching.Contains("7"), "Correct Use of Assistive Device")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTeachingOther" class="fl">Other</label>
					<div class="fr"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { placeholder = "modalities, DME/AE need, consults, etc" })%></div>
				</div>
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericTeachingCommentTemplates") %>
	    				<%= Html.TextArea(Model.Type + "_GenericTeachingComment", data.AnswerOrEmptyString("GenericTeachingComment"), new { })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/FunctionalLimitations/FormRev3", Model); %>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/MobilityKey/FormRev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/ADLs/FormRev3", Model); %>
	</div>
	<div>
		<fieldset>
			<legend>Therapeutic/Dynamic Activities</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericBedMobilityRollingAssistance" class="fl">Bed Mobility</label>
					<div class="fr">
						<%= Html.TherapyAssistance(Model.Type + "_GenericBedMobilityRollingAssistance", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistance"), new { })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssistanceReps", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
						<br />
						<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedMobilityRollingAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericBedWCTransferAssistance" class="fl">Bed/WC Transfer</label>
					<div class="fr">
						<%= Html.TherapyAssistance(Model.Type + "_GenericBedWCTransferAssistance", data.AnswerOrEmptyString("GenericBedWCTransferAssistance"), new { })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericBedWCTransferAssistanceReps", data.AnswerOrEmptyString("GenericBedWCTransferAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
						<br />
						<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericBedWCTransferAssistiveDevice", data.AnswerOrEmptyString("GenericBedWCTransferAssistiveDevice"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericToiletTransferAssistance" class="fl">Toilet Transfer</label>
					<div class="fr">
						<%= Html.TherapyAssistance(Model.Type + "_GenericToiletTransferAssistance", data.AnswerOrEmptyString("GenericToiletTransferAssistance"), new { })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericToiletTransferAssistanceReps", data.AnswerOrEmptyString("GenericToiletTransferAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
						<br />
						<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericToiletTransferAssistiveDevice", data.AnswerOrEmptyString("GenericToiletTransferAssistiveDevice"), new { })%>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericTubShowerTransferAssistance" class="fl">Tub/Shower Transfer</label>
					<div class="fr">
						<%= Html.TherapyAssistance(Model.Type + "_GenericTubShowerTransferAssistance", data.AnswerOrEmptyString("GenericTubShowerTransferAssistance"), new { })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericTubShowerTransferAssistanceReps", data.AnswerOrEmptyString("GenericTubShowerTransferAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
						<br />
						<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTubShowerTransferAssistiveDevice", data.AnswerOrEmptyString("GenericTubShowerTransferAssistiveDevice"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticOtherAssistance" class="fl">Other</label>
					<div class="fr">
						<%= Html.TherapyAssistance(Model.Type + "_GenericTherapeuticOtherAssistance", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistance"), new { })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticOtherAssistanceReps", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistanceReps"), new { @class = "shorter less", placeholder = "rep(s)" })%>
						<br />
						<%= Html.TherapyAssistiveDevice(Model.Type + "_GenericTherapeuticOtherAssistiveDevice", data.AnswerOrEmptyString("GenericTherapeuticOtherAssistiveDevice"), new { })%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Therapeutic Exercise</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericROMTo" class="fl">ROM To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericROMTo", data.AnswerOrEmptyString("GenericROMTo"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericROMToReps", data.AnswerOrEmptyString("GenericROMToReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericActiveTo" class="fl">Active To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericActiveTo", data.AnswerOrEmptyString("GenericActiveTo"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericActiveToReps", data.AnswerOrEmptyString("GenericActiveToReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericAssistive" class="fl">Active/Assistive To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericAssistive", data.AnswerOrEmptyString("GenericAssistive"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericAssistiveReps", data.AnswerOrEmptyString("GenericAssistiveReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericManual" class="fl">Resistive, Manual, To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericManual", data.AnswerOrEmptyString("GenericManual"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericManualReps", data.AnswerOrEmptyString("GenericManualReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericResistiveWWeights" class="fl">Resistive, w/Weights, To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericResistiveWWeights", data.AnswerOrEmptyString("GenericResistiveWWeights"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericResistiveWWeightsReps", data.AnswerOrEmptyString("GenericResistiveWWeightsReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericStretchingTo" class="fl">Stretching To</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericStretchingTo", data.AnswerOrEmptyString("GenericStretchingTo"), new { @class = "short" })%>
						x
						<%= Html.TextBox(Model.Type + "_GenericStretchingToReps", data.AnswerOrEmptyString("GenericStretchingToReps"), new { @class = "shorter less" })%>
						reps
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericObjectiveComment">Comment</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericObjectiveTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericObjectiveComment", data.AnswerOrEmptyString("GenericObjectiveComment"), new { @class = "tall" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<% Html.RenderPartial("Therapy/Shared/SupervisoryVisit/FormRev1", Model); %>
		<% Html.RenderPartial("Therapy/Shared/WCMobility/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
		<fieldset>
			<legend>Plan</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="fl">Continue Prescribed Plan</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="fl">Change Prescribed Plan</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPlanDischarge" class="fl">Plan Discharge</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { })%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericPainProfileComment">Comment</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericPlanTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericPlanComment", data.AnswerOrEmptyString("GenericPlanComment"), new { @class = "tall" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Assessment</legend>
			<div class="wide-column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Progress made towards goals</legend>
			<div class="wide-column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericProgressTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericProgress", data.AnswerOrEmptyString("GenericProgress"), new { })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<% Html.RenderPartial("Therapy/Shared/TreatmentPlan/FormRev3", Model); %>
