﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Homebound/FormRev3", new VisitNoteSectionViewData(data, false, Model.Type)); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/FunctionalLimitations/FormRev1", Model); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Therapy/Shared/Pain/FormRev1", new VisitNoteSectionViewData(data, false, Model.Type)); %>
	</div>
	<div>
		<% Html.RenderPartial("Therapy/Shared/Objective/FormRev1", Model); %>
		<fieldset>
			<legend>Teaching</legend>
			<div class="wide-column">
				<div class="row">
					<div class="checkgroup two-wide">
						<% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
						<input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "1", genericTeaching.Contains("1"), "Patient/Family")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "2", genericTeaching.Contains("2"), "Caregiver")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "3", genericTeaching.Contains("3"), "Correct Use of Adaptive Equipment")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "4", genericTeaching.Contains("4"), "Safety Technique")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "5", genericTeaching.Contains("5"), "ADLs")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "6", genericTeaching.Contains("6"), "HEP")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericTeaching", "7", genericTeaching.Contains("7"), "Correct Use of Assistive Device")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTeachingOther" class="fl">Other</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther", placeholder = "modalities, DME/AE need, consults, etc" })%></div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>ADL Training</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLBathing" class="fl">Bathing</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLBathing", data.AnswerOrEmptyString("GenericADLBathing"), new { @class = "shorter less", @id = Model.Type + "_GenericADLBathing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLUEDressing" class="fl">UE Dressing</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.AnswerOrEmptyString("GenericADLUEDressing"), new { @class = "shorter less", @id = Model.Type + "_GenericADLUEDressing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLLEDressing" class="fl">LE Dressing</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.AnswerOrEmptyString("GenericADLLEDressing"), new { @class = "shorter less", @id = Model.Type + "_GenericADLLEDressing" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLGrooming" class="fl">Grooming</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.AnswerOrEmptyString("GenericADLGrooming"), new { @class = "shorter less", @id = Model.Type + "_GenericADLGrooming" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLToileting" class="fl">Toileting</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLToileting", data.AnswerOrEmptyString("GenericADLToileting"), new { @class = "shorter less", @id = Model.Type + "_GenericADLToileting" })%></div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLFeeding" class="fl">Feeding</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.AnswerOrEmptyString("GenericADLFeeding"), new { @class = "shorter less", @id = Model.Type + "_GenericADLFeeding" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLMealPrep" class="fl">Meal Prep</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.AnswerOrEmptyString("GenericADLMealPrep"), new { @class = "shorter less", @id = Model.Type + "_GenericADLMealPrep" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLHouseCleaning" class="fl">House Cleaning</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.AnswerOrEmptyString("GenericADLHouseCleaning"), new { @class = "shorter less", @id = Model.Type + "_GenericADLHouseCleaning" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLHouseSafety" class="fl">House Safety</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLHouseSafety", data.AnswerOrEmptyString("GenericADLHouseSafety"), new { @class = "shorter less", @id = Model.Type + "_GenericADLHouseSafety" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="fl">Adaptive Equipment/Assistive Device Use or Needs</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.AnswerOrEmptyString("GenericADLAdaptiveEquipment"), new { @class = "shorter less", @id = Model.Type + "_GenericADLAdaptiveEquipment" })%></div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Neuromuscular Reeducation</legend>
			<div class="column">
				<%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
				<div class="row">
					<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="fl">Sitting Balance Activities</label>
					<div class="fr">
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="fl">Static</label>
						<span class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label>
						</span>
						<br />
						<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist" class="fl">Dynamic</label>
						<span class="fr">
							<%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
						</span>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="fl">Standing Balance Activities</label>
					<div class="fr">
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="fl">Static</label>
						<span class="fr">
							<%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "shorter less"})%>
							<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label>
						</span>
						<br />
						<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist" class="fl">Dynamic</label>
						<span class="fr">
							<%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
						</span>
					</div>
				</div>
				<div class="row">
					<div class="checkgroup one-wide">
						<input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
						<%= Html.CheckgroupOption(Model.Type + "_GenericUEWeightBearing", "1", genericUEWeightBearing.Contains("1"), "UE Weight-Bearing Activities") %>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Therapeutic Exercise</legend>
			<div class="wide-column">
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticExerciseROM" class="fl">ROM</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseROM"), new { @class = "short more"})%>
						<span>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMSet"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMReps"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseROMReps">reps</label>
						</span>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROM" class="fl">AROM/AAROM</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROM"), new { @class = "short more" })%>
						<span>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMSet"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROMSet">set(s)</label>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMReps"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROMReps">reps</label>
						</span>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticExerciseResistive" class="fl">Resistive (Type)</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistive", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistive"), new { @class = "short more" })%>
						<span>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveSet"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseResistiveSet">set(s)</label>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveReps"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseResistiveReps">reps</label>
						</span>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticExerciseStretching" class="fl">Stretching</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretching", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretching"), new { @class = "short more" })%>
						<span>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingSet"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseStretchingSet">set(s)</label>
							<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingReps"), new { @class = "shorter less" })%>
							<label for="<%= Model.Type %>_GenericTherapeuticExerciseStretchingReps">reps</label>
						</span>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_GenericTherapeuticExerciseOther" class="fl">Other</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseOther", data.AnswerOrEmptyString("GenericTherapeuticExerciseOther"), new { })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>Therapeutic/Dynamic Activities</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Bed Mobility</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilityRollingReps" class="fl">Rolling</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericBedMobilityRollingReps">X</label>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingReps", data.AnswerOrEmptyString("GenericBedMobilityRollingReps"), new { @class = "shorter less" })%>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericBedMobilityRollingAssist">% assist</label>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps" class="fl">Supine to Sit</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps">X</label>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitReps", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitReps"), new { @class = "shorter less" })%>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericBedMobilitySupineToSitAssist">% assist</label>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps" class="fl">Dynami Reaching</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps">X</label>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingReps", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingReps"), new { @class = "shorter less" })%>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingAssist", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingAssist">% assist</label>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericBedMobilityCoordReps" class="fl">Gross/Fine Motor Coord</label>
			<div class="fr">
				<label for="<%= Model.Type %>_GenericBedMobilityCoordReps">X</label>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordReps", data.AnswerOrEmptyString("GenericBedMobilityCoordReps"), new { @class = "shorter less" })%>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityCoordAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericBedMobilityCoordAssist">% assist</label>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericTransfersType" class="fl">Transfers</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_GenericTransfersType" class="fl">Type</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericTransfersType", data.AnswerOrEmptyString("GenericTransfersType"), new { @class = "short" })%>
				<label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps">X</label>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordReps"), new { @class = "shorter less" })%>
				<%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordAssist">% assist</label>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Correct Unfolding</label>
			<div class="fr">
				<%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "1", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("1"), new { @id = Model.Type + "_GenericCorrectUnfolding1" })%>
				<label for="<%= Model.Type %>_GenericCorrectUnfolding1" class="inline-radio">Yes</label>
				<%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "0", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("0"), new { @id = Model.Type + "_GenericCorrectUnfolding0" })%>
				<label for="<%= Model.Type %>_GenericCorrectUnfolding0" class="inline-radio">No</label>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Correct Foot Placement</label>
			<div class="fr">
				<%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "1", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("1") , new { @id = Model.Type + "_GenericCorrectFootPlacement1" })%>
				<label for="<%= Model.Type %>_GenericCorrectFootPlacement1" class="inline-radio">Yes</label>
				<%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "0", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("0") , new { @id = Model.Type + "_GenericCorrectFootPlacement0" })%>
				<label for="<%= Model.Type %>_GenericCorrectFootPlacement0" class="inline-radio">No</label>
			</div>
		</div>
		<div class="sub row">
			<label class="fl">Assistive Device</label>
			<div class="fr">
				<%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "1", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1"), new { @id = Model.Type + "_GenericAssistiveDevice1" })%>
				<label for="<%= Model.Type %>_GenericAssistiveDevice1" class="inline-radio">Yes</label>
				<%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "0", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0"), new { @id = Model.Type + "_GenericAssistiveDevice0" })%>
				<label for="<%= Model.Type %>_GenericAssistiveDevice0" class="inline-radio">No</label>
			</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Assessment</legend>
			<div class="wide-column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericAssessmentTemplates") %>
	    				<%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @class = "taller" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Narrative</legend>
			<div class="wide-column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), new { @class = "taller" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<fieldset>
	<legend>W/C Training</legend>
	<div class="wide-column">
		<div class="row">
			<label class="fl">Propulsion with</label>
		</div>
		<div class="row">
			<div class="checkgroup six-wide">
				<%  string[] genericPropulsionWith = data.AnswerArray("GenericPropulsionWith"); %>
				<input type="hidden" name="<%= Model.Type %>_GenericPropulsionWith" value="" />
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "1", genericPropulsionWith.Contains("1"), "RUE") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "2", genericPropulsionWith.Contains("2"), "LUE") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "3", genericPropulsionWith.Contains("3"), "BUE") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "4", genericPropulsionWith.Contains("4"), "RLE") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "5", genericPropulsionWith.Contains("5"), "LLE") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPropulsionWith", "6", genericPropulsionWith.Contains("6"), "BLE") %>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericDistanceFT" class="fl">Distance</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericDistanceFT", data.AnswerOrEmptyString("GenericDistanceFT"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericDistanceFT">ft</label>
				<label for="<%= Model.Type %>_GenericDistanceFTReps">x</label>
				<%= Html.TextBox(Model.Type + "_GenericDistanceFTReps", data.AnswerOrEmptyString("GenericDistanceFTReps"), new { @class = "shorter less" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericManagement" class="fl">Management</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericManagement", data.AnswerOrEmptyString("GenericManagement"), new { @class = "short more"  })%>
				<%= Html.TextBox(Model.Type + "_GenericManagementAssist", data.AnswerOrEmptyString("GenericManagementAssist"), new { @class = "shorter less" })%>
				<label for="<%= Model.Type %>_GenericManagementAssist">% assist</label>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Plan</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup two-wide">
				<%  string[] genericPlan = data.AnswerArray("GenericPlan"); %>
				<input type="hidden" name="<%= Model.Type %>_GenericPlan" value="" />
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlan", "1", genericPlan.Contains("1"), "Continue Prescribed Plan")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlan", "2", genericPlan.Contains("2"), "Plan Discharge")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlan", "3", genericPlan.Contains("3"), "In Progress")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlan", "4", genericPlan.Contains("4"), "As of Today")%>
				<div class="option">
					<div class="wrapper">
						<input id="<%= Model.Type %>_GenericPlan5" name="<%= Model.Type %>_GenericPlan" value="5" type="checkbox" class="fl" <%= genericPlan.Contains("5").ToChecked() %> />
						<span>
							<label for="<%= Model.Type %>_GenericPlan5">Patient/Family Notified</label>
							<%= Html.TextBox(Model.Type + "_GenericPlanPtDaysNotice", data.AnswerOrEmptyString("GenericPlanPtDaysNotice"), new { @class = "shortest less" })%>
							<label for="<%= Model.Type %>_GenericPlan5">Days Prior to Discharge</label>
						</span>
					</div>
				</div>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericPlan", "6", genericPlan.Contains("6"), "Change Prescribed Plan", Model.Type + "GenericPlanChangePrescribed", data.AnswerOrEmptyString("GenericPlanChangePrescribed"))%>
			</div>
		</div>
		<div class="row">
			<div class="fl">
				<label>Agency Notification</label>
				<%= Html.TextBox(Model.Type + "_GenericPlanAgencyDaysNotice", data.AnswerOrDefault("GenericPlanAgencyDaysNotice", "5"), new { @class = "shortest less" })%>
				<label>Days Prior?</label>
			</div>
			<div class="fr">
				<div class="checkgroup two-wide">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericPlanIsAgencyNotification", "1", genericPlan.Contains("1"), "Yes")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericPlanIsAgencyNotification", "2", genericPlan.Contains("0"), "No")%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
