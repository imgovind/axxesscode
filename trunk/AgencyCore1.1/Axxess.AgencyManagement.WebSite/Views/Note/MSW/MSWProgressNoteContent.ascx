﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %><%
 var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
 string[] genericLivingSituationChanges = data.AnswerArray("GenericLivingSituationChanges");
 string[] genericMentalStatus = data.AnswerArray("GenericMentalStatus");
 string[] genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus");
 string[] genericVisitGoals = data.AnswerArray("GenericVisitGoals");
 string[] genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions");
 string[] genericFollowUpPlan = data.AnswerArray("GenericFollowUpPlan");                                                                                          
%>
<fieldset>
    <legend>Living Situation</legend>
    <div class="column">
        <div class="row">
            <ul class="checkgroup one-wide">
				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericIsLivingSituationChange", "2", data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("2"), "Unchanged from Last Visit")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericIsLivingSituationChange", "1", data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("1"), true, "Changed", Model.Type + "_GenericLivingSituationChanged", data.AnswerOrEmptyString("GenericLivingSituationChanged"))%>
            </ul>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericLivingSituationChanges" value="" />
            <ul class="checkgroup one-wide">
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericLivingSituationChanges", "1", genericLivingSituationChanges.Contains("1"), "Update to Primary Caregiver", Model.Type + "_GenericUpdateToPrimaryCaregiver", data.AnswerOrEmptyString("GenericUpdateToPrimaryCaregiver"))%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericLivingSituationChanges", "2", genericLivingSituationChanges.Contains("2"), "Changes to Environmental Conditions", Model.Type + "_GenericChangesToEnvironmentalConditions", data.AnswerOrEmptyString("GenericChangesToEnvironmentalConditions"))%>
            </ul>
        </div>
    </div>
</fieldset>
<fieldset>
	<legend>Psychosocial Assessment</legend>
	<div class="column">
		<div class="row">
			<input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
			<label class="al">Mental Status (check all that apply)</label>
			<ul class="checkgroup">
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "1", genericMentalStatus.Contains("1"), "Alert")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "2", genericMentalStatus.Contains("2"), "Forgetful")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "3", genericMentalStatus.Contains("3"), "Confused")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "4", genericMentalStatus.Contains("4"), "Disoriented")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "5", genericMentalStatus.Contains("5"), "Oriented")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "6", genericMentalStatus.Contains("6"), "Lethargic")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "7", genericMentalStatus.Contains("7"), "Poor Short Term Memory")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "8", genericMentalStatus.Contains("8"), "Unconscious")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "9", genericMentalStatus.Contains("9"), "Cannot Determine")%>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
			<label class="al">Emotional Status (check all that apply)</label>
			<ul class="checkgroup">
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "1", genericEmotionalStatus.Contains("1"), "Stable")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "2", genericEmotionalStatus.Contains("2"), "Tearful")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "3", genericEmotionalStatus.Contains("3"), "Stressed")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "4", genericEmotionalStatus.Contains("4"), "Angry")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "5", genericEmotionalStatus.Contains("5"), "Sad")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "6", genericEmotionalStatus.Contains("6"), "Withdrawn")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "7", genericEmotionalStatus.Contains("7"), "Fearful")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "8", genericEmotionalStatus.Contains("8"), "Anxious")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "9", genericEmotionalStatus.Contains("9"), "Flat Affect")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericEmotionalStatus", "10", genericEmotionalStatus.Contains("10"), "Other", Model.Type + "_GenericEmotionalStatusOther", data.AnswerOrEmptyString("GenericEmotionalStatusOther"))%>
			</ul>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericPsychosocialAssessmentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.AnswerOrEmptyString("GenericPsychosocialAssessment"), new { @id = Model.Type + "_GenericPsychosocialAssessment" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
    <legend>Visit Goals</legend>
    <div class="wide-column">
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericGoals" value="" />
            <ul class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "1", genericVisitGoals.Contains("1"), "Adequate Support System")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "2", genericVisitGoals.Contains("2"), "Improved Client/Family Coping")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "3", genericVisitGoals.Contains("3"), "Normal Grieving Process")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "4", genericVisitGoals.Contains("4"), "Appropriate Goals for Care Set by Client/Family")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "5", genericVisitGoals.Contains("5"), "Appropriate Community Resource Referrals")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "6", genericVisitGoals.Contains("6"), "Stable Placement Setting")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericVisitGoals", "7", genericVisitGoals.Contains("7"), "Mobilization of Financial Resources")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericVisitGoals", "8", genericVisitGoals.Contains("8"), "Other", Model.Type + "_GenericVisitGoalsOther", data.AnswerOrEmptyString("GenericVisitGoalsOther"))%>
			</ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericVisitGoalDetails" class="al">Visit Goal Details</label>
            <div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericVisitGoalDetailsTemplates")%>
                <%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.AnswerOrEmptyString("GenericVisitGoalDetails"), new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "tall" })%>
            </div>
        </div>
		<div class="row">
			<label>Planned Interventions</label>
			<input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
			<ul class="checkgroup two-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "1", genericPlannedInterventions.Contains("1"), "Psychosocial Assessment")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "2", genericPlannedInterventions.Contains("2"), "Develop Appropriate Support System")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "3", genericPlannedInterventions.Contains("3"), "Counseling regarding Disease Process &#38; Management")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "4", genericPlannedInterventions.Contains("4"), "Community Resource Planning &#38; Outreach")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "5", genericPlannedInterventions.Contains("5"), "Counseling regarding Family Coping")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "6", genericPlannedInterventions.Contains("6"), "Stabilize Current Placement")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "7", genericPlannedInterventions.Contains("7"), "Crisis Intervention")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "8", genericPlannedInterventions.Contains("8"), "Determine/Locate Alternative Placement")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "9", genericPlannedInterventions.Contains("9"), "Long-range Planning &#38; Decision Making")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "10", genericPlannedInterventions.Contains("10"), "Financial Counseling and/or Referrals")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericPlannedInterventions", "11", genericPlannedInterventions.Contains("11"), "Other", Model.Type + "_GenericPlannedInterventionsOther", data.AnswerOrEmptyString("GenericPlannedInterventionsOther"))%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericInterventionDetails">Intervention Details</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericInterventionDetailsTemplates")%>
				<%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.AnswerOrEmptyString("GenericInterventionDetails"), new { @id = Model.Type + "_GenericInterventionDetails", @class = "tall" })%>
			</div>
		</div>
    </div>
</fieldset>
<fieldset>
    <legend>Progress Towards Goals</legend>
    <div class="column">
	    <div class="row">
            <label for="<%= Model.Type %>_GenericProgressTowardsGoals" class="fl">Progress Towards Goals</label>
            <div class="fr">
                <%  var genericProgressTowardsGoals = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "None", Value = "None" },
                        new SelectListItem { Text = "Slight", Value = "Slight" },
                        new SelectListItem { Text = "Fair", Value = "Fair" },
                        new SelectListItem { Text = "Moderate", Value = "Moderate" },
                        new SelectListItem { Text = "Good", Value = "Good" },
                        new SelectListItem { Text = "Excellent", Value = "Excellent" }
                    }, "Value", "Text", data.AnswerOrEmptyString("GenericProgressTowardsGoals"));%>
                <%= Html.DropDownList(Model.Type + "_GenericProgressTowardsGoals", genericProgressTowardsGoals, new { @id = Model.Type + "_GenericProgressTowardsGoals" })%>
            </div>
        </div>
    </div>
    <div class="wide-column">
		<div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericFollowUpPlan" value="" />
            <ul class="checkgroup four-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericFollowUpPlan", "1", genericFollowUpPlan.Contains("1"), "Follow-up Visit")%>
	            <%= Html.CheckgroupOption(Model.Type + "_GenericFollowUpPlan", "2", genericFollowUpPlan.Contains("2"), "Confer with Team")%>
	            <%= Html.CheckgroupOption(Model.Type + "_GenericFollowUpPlan", "3", genericFollowUpPlan.Contains("3"), "Provide External Referral")%>
	            <%= Html.CheckgroupOption(Model.Type + "_GenericFollowUpPlan", "4", genericFollowUpPlan.Contains("4"), "Discharge SW Services")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericProgressTowardsGoalsDetails">Details</label>
            <div class="template-text">
	            <%= Html.ToggleTemplates(Model.Type + "_GenericProgressTowardsGoalsDetailsTemplates")%>
	            <%= Html.TextArea(Model.Type + "_GenericProgressTowardsGoalsDetails", data.AnswerOrEmptyString("GenericProgressTowardsGoalsDetails"), new { @id = Model.Type + "_GenericProgressTowardsGoalsDetails", @class = "tall" })%>
            </div>
        </div>
    </div>
</fieldset>