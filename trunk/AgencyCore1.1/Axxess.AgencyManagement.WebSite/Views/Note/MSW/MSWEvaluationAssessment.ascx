﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genericLivingSituation = data.AnswerArray("GenericLivingSituation"); %>
<% string[] genericReasonForReferral = data.AnswerArray("GenericReasonForReferral"); %>
<% string[] genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<% string[] genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<% string[] genericIdentifiedProblems = data.AnswerArray("GenericIdentifiedProblems"); %>
<% string[] genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<% string[] genericGoals = data.AnswerArray("GenericGoals"); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" })) { %>
     <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
        <div class="inline-fieldset two-wide">
            <div>
	            <fieldset>
					<legend>Living Situation</legend>
					<div class="column">
						<div class="row">
							<label for="<%= Model.Type %>_GenericPrimaryCaregiver" class="fl">Primary Caregiver</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPrimaryCaregiver", data.AnswerOrEmptyString("GenericPrimaryCaregiver"), new { @id = Model.Type + "_GenericPrimaryCaregiver" })%></div>
						</div>
						<div class="row">
							<label for="<%= Model.Type %>_GenericQualityOfCare" for="<%= Model.Type %>_GenericQualityOfCare" class="fl ">The quality of care that patient receives at home</label>
							<div class="fr">
								<%  var genericQualityOfCare = new SelectList(new[] {
										new SelectListItem { Text = "", Value = "" },
										new SelectListItem { Text = "Good", Value = "Good" },
										new SelectListItem { Text = "Adequate", Value = "Adequate" },
										new SelectListItem { Text = "Marginal", Value = "Marginal" },
										new SelectListItem { Text = "Inadequate", Value = "Inadequate" }
									}, "Value", "Text", data.AnswerOrEmptyString("GenericQualityOfCare"));%>
								<%= Html.DropDownList(Model.Type + "_GenericQualityOfCare", genericQualityOfCare, new { @id = Model.Type + "_GenericQualityOfCare" })%>
							</div>
						</div>
						<div class="row">
							<label for="<%= Model.Type %>_GenericEnvironmentalConditions" class="fl">Environmental Conditions</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericEnvironmentalConditions", data.AnswerOrEmptyString("GenericEnvironmentalConditions"), new { @id = Model.Type + "_GenericEnvironmentalConditions" })%></div>
						</div>
						<div class="row">
							<input type="hidden" name="<%= Model.Type %>_GenericLivingSituation" value="" />
							<label class="ac">Patient Lives (check all that apply)</label>
							<ul class="checkgroup two-wide">
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "1", genericLivingSituation.Contains("1"), "Alone")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "2", genericLivingSituation.Contains("2"), "With Friend/Family")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "3", genericLivingSituation.Contains("3"), "With Dependent")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "4", genericLivingSituation.Contains("4"), "With Spouse/Partner")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "5", genericLivingSituation.Contains("5"), "With Religious Community")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "6", genericLivingSituation.Contains("6"), "Assisted Living")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "7", genericLivingSituation.Contains("7"), "With Partner")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "8", genericLivingSituation.Contains("8"), "Has Paid Caregiver (&#62; 10 hours)")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericLivingSituation", "9", genericLivingSituation.Contains("9"), "Has Live-In, Paid Caregiver")%>
								<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericLivingSituation", "10", genericLivingSituation.Contains("10"), "Other", Model.Type + "_GenericLivingSituationOther", data.AnswerOrEmptyString("GenericLivingSituationOther"))%>
							</ul>
						</div>
					</div>
				</fieldset>
            </div>
			<div>
				<fieldset>
					<legend>Reason(s) for Referral</legend>
					<input type="hidden" name="<%= Model.Type %>_GenericReasonForReferral" value="" />
					<div class="column">
						<div class="row">
							<ul class="checkgroup one-wide">
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "1", genericReasonForReferral.Contains("1"), "Assessment for Psychosocial Coping")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "2", genericReasonForReferral.Contains("2"), "Lives Alone, No Identified Caregiver")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "3", genericReasonForReferral.Contains("3"), "Counseling regarding Disease Process or Management")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "4", genericReasonForReferral.Contains("4"), "Solo Caregiver for Minor Children and/or Other Dependents")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "5", genericReasonForReferral.Contains("5"), "Family/Caregiver Coping Support")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "6", genericReasonForReferral.Contains("6"), "Reported Noncompliance to Medical Plan of Care")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "7", genericReasonForReferral.Contains("7"), "Hospice Eligibility")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "8", genericReasonForReferral.Contains("8"), "Suspected Negligence or Abuse")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "9", genericReasonForReferral.Contains("9"), "Financial/Practical Resources")%>
								<%= Html.CheckgroupOption(Model.Type + "_GenericReasonForReferral", "10", genericReasonForReferral.Contains("10"), "Assistance with Advanced Directive / DPOA/DNR")%>
								<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericReasonForReferral", "11", genericReasonForReferral.Contains("11"), "Other", Model.Type + "_GenericReasonForReferralOther", data.AnswerOrEmptyString("GenericReasonForReferralOther"))%>
							</ul>
						</div>
					</div>
				</fieldset>
			</div>	            
        </div>
        <fieldset>
			<legend>Psychosocial Assessment</legend>
			<div class="column">
				<div class="row">
					<input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
					<label class="al">Mental Status (check all that apply)</label>
					<ul class="checkgroup">
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "1", genericMentalStatus.Contains("1"), "Alert")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "2", genericMentalStatus.Contains("2"), "Forgetful")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "3", genericMentalStatus.Contains("3"), "Confused")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "4", genericMentalStatus.Contains("4"), "Disoriented")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "5", genericMentalStatus.Contains("5"), "Oriented")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "6", genericMentalStatus.Contains("6"), "Lethargic")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "7", genericMentalStatus.Contains("7"), "Poor Short Term Memory")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "8", genericMentalStatus.Contains("8"), "Unconscious")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatus", "9", genericMentalStatus.Contains("9"), "Cannot Determine")%>
					</ul>
				</div>
			</div>
			<div class="column">
				<div class="row">
					<input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
					<label class="al">Emotional Status (check all that apply)</label>
					<ul class="checkgroup">
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "1", genericEmotionalStatus.Contains("1"), "Stable")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "2", genericEmotionalStatus.Contains("2"), "Tearful")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "3", genericEmotionalStatus.Contains("3"), "Stressed")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "4", genericEmotionalStatus.Contains("4"), "Angry")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "5", genericEmotionalStatus.Contains("5"), "Sad")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "6", genericEmotionalStatus.Contains("6"), "Withdrawn")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "7", genericEmotionalStatus.Contains("7"), "Fearful")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "8", genericEmotionalStatus.Contains("8"), "Anxious")%>
						<%= Html.CheckgroupOption(Model.Type + "_GenericEmotionalStatus", "9", genericEmotionalStatus.Contains("9"), "Flat Affect")%>
						<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericEmotionalStatus", "10", genericEmotionalStatus.Contains("10"), "Other", Model.Type + "_GenericEmotionalStatusOther", data.AnswerOrEmptyString("GenericEmotionalStatusOther"))%>
					</ul>
				</div>
			</div>
			<div class="wide-column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_GenericPsychosocialAssessmentTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.AnswerOrEmptyString("GenericPsychosocialAssessment"), new { @id = Model.Type + "_GenericPsychosocialAssessment" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
		<fieldset id="<%= Model.Type %>_FinacialAssessment">
        <legend>Finacial Assessment</legend>
        <div class="column">
            <div class="row ac">Income Sources</div>
            <div class="row">
                <label class="fl">Employment</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericEmployment", data.AnswerOrEmptyString("GenericEmployment"), new { @id = Model.Type + "_GenericEmployment", @class = "shorter" }) %>
					<span>
						<label for="<%= Model.Type %>_GenericEmploymentAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericEmploymentAmount", data.AnswerOrEmptyString("GenericEmploymentAmount"), new { @id = Model.Type + "_GenericEmploymentAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
                </div>
            </div>
            <div class="row">
                <label class="fl">Pt Social Security</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericPtSocialSecurity", data.AnswerOrEmptyString("GenericPtSocialSecurity"), new { id = Model.Type + "_GenericPtSocialSecurity", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericPtSocialSecurityAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericPtSocialSecurityAmount", data.AnswerOrEmptyString("GenericPtSocialSecurityAmount"), new { @id = Model.Type + "_GenericPtSocialSecurityAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Spouse Social Security</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericSpouseSocialSecurity", data.AnswerOrEmptyString("GenericSpouseSocialSecurity"), new { id = Model.Type + "_GenericSpouseSocialSecurity", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericSpouseSocialSecurityAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericSpouseSocialSecurityAmount", data.AnswerOrEmptyString("GenericSpouseSocialSecurityAmount"), new { @id = Model.Type + "_GenericSpouseSocialSecurityAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Pt SSI</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericPtSSI", data.AnswerOrEmptyString("GenericPtSSI"), new { id = Model.Type + "_GenericPtSSI", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericPtSSIAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericPtSSIAmount", data.AnswerOrEmptyString("GenericPtSSIAmount"), new { @id = Model.Type + "_GenericPtSSIAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Spouse SSI</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericSpouseSSI", data.AnswerOrEmptyString("GenericSpouseSSI"), new { id = Model.Type + "_GenericSpouseSSI", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericSpouseSSIAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericSpouseSSIAmount", data.AnswerOrEmptyString("GenericSpouseSSIAmount"), new { @id = Model.Type + "_GenericSpouseSSIAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Pensions</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericPensions", data.AnswerOrEmptyString("GenericPensions"), new { id = Model.Type + "_GenericPensions", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericPensionsAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericPensionsAmount", data.AnswerOrEmptyString("GenericPensionsAmount"), new { @id = Model.Type + "_GenericPensionsAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Other Income</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericOtherIncome", data.AnswerOrEmptyString("GenericOtherIncome"), new { id = Model.Type + "_GenericOtherIncome", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericOtherIncomeAmount", data.AnswerOrEmptyString("GenericOtherIncomeAmount"), new { @id = Model.Type + "_GenericOtherIncomeAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Food Stamps</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericFoodStamps", data.AnswerOrEmptyString("GenericFoodStamps"), new { id = Model.Type + "_GenericFoodStamps", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericFoodStampsAmount", data.AnswerOrEmptyString("GenericFoodStampsAmount"), new { @id = Model.Type + "_GenericFoodStampsAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTotalIncomeAmount" class="fl">Total</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericTotalIncomeAmount", data.AnswerOrEmptyString("GenericTotalIncomeAmount"), new { @id = Model.Type + "_GenericTotalIncomeAmount", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row ac">Assets</div>
            <div class="row">
                <label class="fl">Saving Account</label>
                <div class="fr">
                    <%= Html.YesNoNaSelectList(Model.Type + "_GenericSavingsAccount", data.AnswerOrEmptyString("GenericSavingsAccount"), new { id = Model.Type + "_GenericSavingsAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericSavingsAccountAmount", data.AnswerOrEmptyString("GenericSavingsAccountAmount"), new { @id = Model.Type + "_GenericSavingsAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Owns Home (Value)</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericOwnsHomeAccount", data.AnswerOrEmptyString("GenericOwnsHomeAccount"), new { id = Model.Type + "_GenericOwnsHomeAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericOwnsHomeAccountAmount", data.AnswerOrEmptyString("GenericOwnsHomeAccountAmount"), new { @id = Model.Type + "_GenericOwnsHomeAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Owns Other Property (Value)</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericOwnsOtherPropertyAccount", data.AnswerOrEmptyString("GenericOwnsOtherPropertyAccount"), new { id = Model.Type + "_GenericOwnsOtherPropertyAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericOwnsOtherPropertyAccountAmount", data.AnswerOrEmptyString("GenericOwnsOtherPropertyAccountAmount"), new { @id = Model.Type + "_GenericOwnsOtherPropertyAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">VA Aid &#38; Assistance</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericVAAidAccount", data.AnswerOrEmptyString("GenericVAAidAccount"), new { id = Model.Type + "_GenericVAAidAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericVAAidAccountAmount", data.AnswerOrEmptyString("GenericVAAidAccountAmount"), new { @id = Model.Type + "_GenericVAAidAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Spouse SSI</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericSpouseAssetSSIAccount", data.AnswerOrEmptyString("GenericSpouseAssetSSIAccount"), new { id = Model.Type + "_GenericSpouseAssetSSIAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericSpouseAssetSSIAccountAmount", data.AnswerOrEmptyString("GenericSpouseAssetSSIAccountAmount"), new { @id = Model.Type + "_GenericSpouseAssetSSIAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label class="fl">Other Assets</label>
                <div class="fr">
					<%= Html.YesNoNaSelectList(Model.Type + "_GenericOtherAssetsAccount", data.AnswerOrEmptyString("GenericOtherAssetsAccount"), new { id = Model.Type + "_GenericOtherAssetsAccount", @class = "shorter" })%>
					<span>
						<label for="<%= Model.Type %>_GenericOtherIncomeAmount">$</label>
						<%= Html.TextBox(Model.Type + "_GenericOtherAssetsAccountAmount", data.AnswerOrEmptyString("GenericOtherAssetsAccountAmount"), new { @id = Model.Type + "_GenericOtherAssetsAccountAmount", @class = "shorter", placeholder = "Amount", status = "Amount" })%>
					</span>
				</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericTotalAssetsAmount" class="fl">Total</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_GenericTotalAssetsAmount", data.AnswerOrEmptyString("GenericTotalAssetsAmount"), new { @id = Model.Type + "_GenericTotalAssetsAmount", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Transportation for Medical Care Provided by</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericTransportationProvidedBy", data.AnswerOrEmptyString("GenericTransportationProvidedBy"), new { @id = Model.Type + "_GenericTransportationProvidedBy", @class = "short" })%></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
				<label class="al">Identified Problems</label>
                <input type="hidden" name="<%= Model.Type %>_GenericIdentifiedProblems" value="" />
                <ul class="checkgroup two-wide">
					<%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "1", genericIdentifiedProblems.Contains("1"), "Patient needs a meal prepared or delivered daily")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "2", genericIdentifiedProblems.Contains("2"), "Patient/family reported noncompliant to medical plan of care")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "3", genericIdentifiedProblems.Contains("3"), "Patient needs assistance with housekeeping/shopping")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "4", genericIdentifiedProblems.Contains("4"), "Patient needs assistance with advanced directive/DPOA/DNR")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "5", genericIdentifiedProblems.Contains("5"), "Patient needs daily contact to check on him/her")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "6", genericIdentifiedProblems.Contains("6"), "Patient needs assistance with medical/insurance forms")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "7", genericIdentifiedProblems.Contains("7"), "Patient needs assistance with alert device (ERS, PRS)")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "8", genericIdentifiedProblems.Contains("8"), "Patient needs assistance with entitlement forms")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "9", genericIdentifiedProblems.Contains("9"), "Patient needs transportation assistance to medical care")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "10", genericIdentifiedProblems.Contains("10"), "Patient needs alternative living arrangements")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "11", genericIdentifiedProblems.Contains("11"), "Patient needs alternative living arrangements")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericIdentifiedProblems", "12", genericIdentifiedProblems.Contains("12"), "Psychosocial counseling indicated")%>
                </ul>
            </div>
            <div class="row">
	            <label>Provide further information</label>
	            <div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "__GenericFurtherInformationTemplates")%>
	                <%= Html.TextArea(Model.Type + "_GenericFurtherInformation", data.AnswerOrEmptyString("GenericFurtherInformation"), new { @id = Model.Type + "_GenericFurtherInformation", @class = "tall" })%>
				</div>
            </div>
            <div class="row">
	            <label>Identified Strengths and Supports</label>
	            <div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericIdentifiedStrengthsTemplates")%>
	                <%= Html.TextArea(Model.Type + "_GenericIdentifiedStrengths", data.AnswerOrEmptyString("GenericIdentifiedStrengths"), new { @id = Model.Type + "_GenericIdentifiedStrengths", @class = "tall" })%>
				</div>
            </div>
            <div class="row">
                <label>Planned Interventions</label>
                <input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
                <ul class="checkgroup two-wide">
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "1", genericPlannedInterventions.Contains("1"), "Psychosocial Assessment")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "2", genericPlannedInterventions.Contains("2"), "Develop Appropriate Support System")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "3", genericPlannedInterventions.Contains("3"), "Counseling regarding Disease Process &#38; Management")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "4", genericPlannedInterventions.Contains("4"), "Community Resource Planning &#38; Outreach")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "5", genericPlannedInterventions.Contains("5"), "Counseling regarding Family Coping")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "6", genericPlannedInterventions.Contains("6"), "Stabilize Current Placement")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "7", genericPlannedInterventions.Contains("7"), "Crisis Intervention")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "8", genericPlannedInterventions.Contains("8"), "Determine/Locate Alternative Placement")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "9", genericPlannedInterventions.Contains("9"), "Long-range Planning &#38; Decision Making")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericPlannedInterventions", "10", genericPlannedInterventions.Contains("10"), "Financial Counseling and/or Referrals")%>
					<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericPlannedInterventions", "11", genericPlannedInterventions.Contains("11"), "Other", Model.Type + "_GenericPlannedInterventionsOther", data.AnswerOrEmptyString("GenericPlannedInterventionsOther"))%>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericInterventionDetails">Intervention Details</label>
                <div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericInterventionDetailsTemplates")%>
	                <%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.AnswerOrEmptyString("GenericInterventionDetails"), new { @id = Model.Type + "_GenericInterventionDetails", @class = "tall" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericPlanOfCare">Plan of Care</label>
                <div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericPlanOfCareTemplates")%>
	                <%= Html.TextArea(Model.Type + "_GenericPlanOfCare", data.AnswerOrEmptyString("GenericPlanOfCare"), new { @id = Model.Type + "_GenericPlanOfCare", @class = "tall" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
	    <legend>Goals</legend>
        <div class="column">
	        <div class="row">
		        <label class="fl">Frequency</label>
				<%= Html.TextBox(Model.Type + "_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @class = "fr long", @id = Model.Type + "_GenericFrequency", @maxlength = "50" })%>
	        </div>
	    </div>
	    <div class="column">
	        <div class="row">
		        <label class="fl">Duration</label>
		        <%= Html.TextBox(Model.Type + "_GenericDuration", data.AnswerOrEmptyString("GenericDuration"), new { @class = "fr long", @id = Model.Type + "_GenericDuration", @maxlength = "50" })%> 
	    
	        </div>
	    </div>
	    <div class="wide-column">
            <div class="row">
	            <input type="hidden" name="<%= Model.Type %>_GenericGoals" value="" />
	            <ul class="checkgroup two-wide">
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "1", genericGoals.Contains("1"), "Adequate Support System")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "2", genericGoals.Contains("2"), "Improved Client/Family Coping")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "3", genericGoals.Contains("3"), "Normal Grieving Process")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "4", genericGoals.Contains("4"), "Appropriate Goals for Care Set by Client/Family")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "5", genericGoals.Contains("5"), "Appropriate Community Resource Referrals")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "6", genericGoals.Contains("6"), "Stable Placement Setting")%>
	                <%= Html.CheckgroupOption(Model.Type + "_GenericGoals", "7", genericGoals.Contains("7"), "Mobilization of Financial Resources")%>
					<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericGoals", "8", genericGoals.Contains("8"), "Other", Model.Type + "_GenericGoalsOther", data.AnswerOrEmptyString("GenericGoalsOther"))%>
				</ul>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericVisitGoalDetails" class="al">Visit Goal Details</label>
                <div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericVisitGoalDetailsTemplates")%>
	                <%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.AnswerOrEmptyString("GenericVisitGoalDetails"), new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "tall" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>
<script>
	$("#<%= Model.Type %>_FinacialAssessment select").each(function() {
		U.ShowIfSelectEquals($(this), "1", $(this).next());
	});
</script>