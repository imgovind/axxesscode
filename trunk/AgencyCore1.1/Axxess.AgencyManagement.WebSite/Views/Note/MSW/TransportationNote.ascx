﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Driver/Transportation Log | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" }))
    { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
     <legend>Appointment Information</legend>
     <div class="column">
        <div class="row">
	            <label for="<%= Model.Type %>_PickUpDate" class="fl">Pick Up Date</label>
				<div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_PickUpDate" value="<%= data.AnswerOrMinDate("PickUpDate").ToZeroFilled() %>" id="<%= Model.Type %>_PickUpDate" /></div>
            </div>
            <div class="row">
				<label for="<%= Model.Type %>_PickUpTime" class="fl">Pick Up Time</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_PickUpTime", data.AnswerOrEmptyString("PickUpTime"), new { @id = Model.Type + "_PickUpTime", @class = "time-picker" })%></div>
            </div>
     </div>
     <div class="column">
     
     <div class="row">
				<label for="<%= Model.Type %>_AppointmentTime" class="fl">Appointment Time</label>
				<div class="fr"><%= Html.TextBox(Model.Type + "_AppointmentTime", data.AnswerOrEmptyString("AppointmentTime"), new { @id = Model.Type + "_AppointmentTime", @class = "time-picker" })%></div>
            </div>
            <div class="row">
				<label for="<%= Model.Type %>_AppointmentType" class="fl">Appointment Type</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_AppointmentType", data.AnswerOrEmptyString("AppointmentType"), new { @id = Model.Type + "_AppointmentType" })%></div>
            </div>
    </div>
    
    </fieldset>
    <div class="inline-fieldset two-wide">
	    <div>
			<fieldset>
				<legend>Pick Up Location</legend>
				<div class="wide-column">
					 <div class="row">
						<label for="<%= Model.Type %>_PickUpAddressLine1" class="fl">Address Line 1</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_PickUpAddressLine1", data.AnswerOrEmptyString("PickUpAddressLine1"), new { @id = Model.Type + "_PickUpAddressLine1", @maxlength = "50" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_PickUpAddressLine2" class="fl"> Address Line 2</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_PickUpAddressLine2", data.AnswerOrEmptyString("PickUpAddressLine2"), new { @id = Model.Type + "_PickUpAddressLine2", @maxlength = "50" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_PickUpAddressCity" class="fl">City</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_PickUpAddressCity", data.AnswerOrEmptyString("PickUpAddressCity"), new { @id = Model.Type + "_PickUpAddressCity", @maxlength = "50", @class = "address-city" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_PickUpAddressStateCode" class="fl">State, Zip</label>
						<div class="fr">
							<%= Html.States(Model.Type + "_PickUpAddressStateCode", data.AnswerOrEmptyString("PickUpAddressStateCode"), new { @id = Model.Type + "_PickUpAddressStateCode", @class = "address-state short" })%>
							<%= Html.TextBox(Model.Type + "_PickUpAddressZipCode", data.AnswerOrEmptyString("PickUpAddressZipCode"), new { @id = Model.Type + "_PickUpAddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Drop Off Location</legend>
				<div class="wide-column">
					<div class="row">
						<label for="<%= Model.Type %>_DropOffAddressLine1" class="fl">Address Line 1</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_DropOffAddressLine1", data.AnswerOrEmptyString("DropOffAddressLine1"), new { @id = Model.Type + "_DropOffAddressLine1", @maxlength = "50" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_DropOffAddressLine2" class="fl"> Address Line 2</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_DropOffAddressLine2", data.AnswerOrEmptyString("DropOffAddressLine2"), new { @id = Model.Type + "_DropOffAddressLine2", @maxlength = "50" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_DropOffAddressCity" class="fl">City</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_DropOffAddressCity", data.AnswerOrEmptyString("DropOffAddressCity"), new { @id = Model.Type + "_DropOffAddressCity", @maxlength = "50", @class = "address-city" })%></div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_DropOffAddressStateCode" class="fl">State, Zip</label>
						<div class="fr">
							<%= Html.States(Model.Type + "_DropOffAddressStateCode", data.AnswerOrEmptyString("DropOffAddressStateCode"), new { @id = Model.Type + "_DropOffAddressStateCode", @class = "address-state short" })%>
							<%= Html.TextBox(Model.Type + "_DropOffAddressZipCode", data.AnswerOrEmptyString("DropOffAddressZipCode"), new { @id = Model.Type + "_DropOffAddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
    </div>
    <fieldset>
	    <legend>Mileage Log</legend>
	    <div class="column">
		    <div class="row">
			    <label class="fl">Start</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_StartMileage", data.AnswerOrEmptyString("StartMileage"), new { @id = Model.Type + "_StartMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
		    <div class="row">
			    <label class="fl">Pick up from Home</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_PickUpHomeMileage", data.AnswerOrEmptyString("PickUpHomeMileage"), new { @id = Model.Type + "_PickUpHomeMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
		    <div class="row">
			    <label class="fl">Drop off at Appointment</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_DropOffApptMileage", data.AnswerOrEmptyString("DropOffApptMileage"), new { @id = Model.Type + "_DropOffApptMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
		    <div class="row">
			    <label class="fl">Return for Pickup</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_ReturnPickUpMileage", data.AnswerOrEmptyString("ReturnPickUpMileage"), new { @id = Model.Type + "_ReturnPickUpMileage", @class = "decimal short" })%>
					<span>miles</span>
				</div>
		    </div>
	    </div>
	     <div class="column">
		    <div class="row">
			    <label class="fl">Return to Home</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_ReturnHomeMileage", data.AnswerOrEmptyString("ReturnHomeMileage"), new { @id = Model.Type + "_ReturnHomeMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
		    <div class="row">
			    <label class="fl">Return to Office</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_ReturnOfficeMileage", data.AnswerOrEmptyString("ReturnOfficeMileage"), new { @id = Model.Type + "_ReturnOfficeMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
		    <div class="row">
			    <label class="fl">Total</label>
			    <div class="fr">
				    <%= Html.TextBox(Model.Type + "_TotalMileage", data.AnswerOrEmptyString("TotalMileage"), new { @id = Model.Type + "_TotalMileage", @class = "decimal short" })%>
					<span>miles</span>
			    </div>
		    </div>
	    </div>
    </fieldset>
      <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>