﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  bool showSendAsOrder = Model.IsUserCanMarkAsOrder; %>
<%  bool showCarePlanOrEvalUrlSection = Model.IsCarePlanVisible;  %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Hidden("PatientId", Model.PatientId, new { id = Model.Type + "_PatientId"})%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { id = Model.Type + "_EpisodeId" })%>
<%= Html.Hidden("EventId", Model.EventId, new { id = Model.Type + "_EventId" })%>
<%= Html.Hidden("EventDate", Model.EventStartDate, new { id = Model.Type + "_EventDate" })%>
<%= Html.Hidden("LocationId", Model.LocationId, new { id = Model.Type + "_LocationId" })%>
<%= Html.Hidden("Type", Model.Type)%>
<%= Html.Hidden("Version", Model.Version)%>
<%= Html.Hidden("Service", Model.Service)%>
<%  if (Model.HasReturnReasons) { %><%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.EpisodeId, Model.PatientId, Model.EpisodeId, "placeholder", Model.Service))  %><%  } %>
<fieldset>
    <legend>
        <%= Model.TypeName %> <%= Model.Version > 1 ? "(Version " + Model.Version + ")" : string.Empty %>
<%  if (Model.HasReturnReasons) { %>
        <%= Html.ReturnCommentsLink(Model.Service, Model.EventId, Model.EpisodeId, Model.PatientId, true) %>
<%  } %>
    </legend>
    <div class="column">
        <div class="row no-input">
	        <label class="fl">Patient</label>
            <div class="fr"><%= Model.PatientProfile != null ? Model.PatientProfile.DisplayName : string.Empty %> (<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : string.Empty %>)</div>
        </div>
<%  if (Model.CanChangeDisciplineType) { %>
        <div class="row">
	        <label class="fl">Task</label>
            <div class="fr">
                <span class="button"><a class="load-disciplinetask">Load Discipline Task</a></span>
                <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask" })%>
            </div>
        </div>
<%  } else { %>
        <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask" })%>
<%  } %>
    </div>
    <div class="column">
	    <div class="row no-input">
		    <%= Html.CarePeriodLabel((int)Model.Service, new { @for = Model.Type + "_EpsPeriod", @class = "fl"}) %>
			<div class="fr"><%= Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() %></div>
		</div>
<%  if (showSendAsOrder) { %>
        <div class="row">
	        <ul class="checkgroup one-wide">
	            <%= Html.CheckgroupOption("SendAsOrder", "1", false, "Send as an order") %>
	        </ul>
        </div>
<%  } %>
       <%if (Model.IsAllergiesVisible || showCarePlanOrEvalUrlSection)
         { %>
        <div class="row">
            <%if(Model.IsAllergiesVisible){ %><span class="button"><a href="javascript:void(0)" onclick="Allergy.List('<%= Model.PatientId %>', '<%= Model.Service %>');">View Current Allergies</a></span><%} %>
		    <%if (showCarePlanOrEvalUrlSection) { %><div class="ancillary-button"> <a href="javascript:void(0)" careplanid='<%= Model.CarePlanOrEvalId%>'  class="care-plan-print <%= Model.CarePlanOrEvalGroup%>" >View Care Plan</a> </div><%} %>
        </div>
        <%} %>
    </div>
</fieldset>
<fieldset>
	<legend>Visit Details</legend>
	<div class="column">
	   <% if (Model.IsSelectedEpisodeVisible){ %>
	    <div class="row">
            <%= Html.CarePeriodLabel((int)Model.Service, new { @for = Model.Type + "_SelectedEpisodeId", @class = "fl", value = "Associated " })%>
            <div class="fr">
                <%= Html.PatientEpisodes((int)Model.Service, "SelectedEpisodeId", Model.SelectedEpisodeId != Guid.Empty ? Model.SelectedEpisodeId.ToString() : Model.EpisodeId.ToString(), Model.PatientId, "-- Select Episode --", new { @id = Model.Type + "_SelectedEpisodeId", @class = "required not-zero" })%>
            </div>
        </div>
        <%}%>
        <%if(Model.IsVisitParameterVisible){ %>
		<div class="row">
			<label for="<%= Model.Type %>_VisitDate" class="fl">Visit Date</label>
			<div class="fr"><%= Html.DatePicker("VisitDate",Model.VisitStartDate.IsValid()? Model.VisitStartDate.ToString("MM/dd/yyyy"): string.Empty, Model.StartDate.ToZeroFilled(), Model.EndDate.AddDays(30).ToZeroFilled(), true, new { id = Model.Type + "_VisitDate" }) %></div>
		</div>
		<%} %>
		 <%if(Model.IsTimeIntervalVisible){ %>
		<div class="row">
			<label for="<%= Model.Type %>_TimeIn" class="fl">Time In</label>
			<div class="fr"><%= Html.TextBoxFor(t => t.TimeIn, new { @id = Model.Type + "_TimeIn", @class = "time-picker complete-required" }) %></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_TimeOut" class="fl">Time Out</label>
			<div class="fr"><%= Html.TextBoxFor(t => t.TimeOut, new { @id = Model.Type + "_TimeOut", @class = "time-picker complete-required" }) %></div>
		</div>
		<%} %>
		<%if(Model.IsDNRVisible){ %>
		  <div class="row">
            <label for="<%= Model.Type %>_DNR" class="fl">DNR</label>
            <div class="fr">
                <%= Html.DNR(Model.Type + "_DNR", data.AnswerOrDefault("DNR", ""), true, new { @id = Model.Type + "_DNR" })%>
            </div>
        </div>
        <%} %>
		
	</div>
	<div class="column">
	      
		<% if (Model.IsUserCanLoadPreviousNotes){ %>
		<div class="row">
			<label for="<%= Model.Type %>_PreviousNotes" class="fl">Previous Notes</label>
			<div class="fr"><%= Html.TogglePreviousNotes("PreviousNotes", Model.Type + "_PreviousNotes") %></div>
		</div>
		<% } %>
		<%if (Model.IsPhysicianVisible){ %>
		    <div class="row">
                <label for="<%= Model.Type %>_PhysicianDropDown" class="fl">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : data.AnswerOrEmptyString("PhysicianId"), new { @id = Model.Type + "_PhysicianDropDown", @class = "physician-picker" })%></div>
            </div>
        <%} %>
        <%if (Model.IsPhysicianDateVisible)
          { %>
		<div class="row">
			<label for="<%= Model.Type %>_LastVisitDate" class="fl">Last Physician Visit Date</label>
			<div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_LastVisitDate" value="<%= data.AnswerOrEmptyString("LastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("LastVisitDate") : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" id="<%= Model.Type %>_LastVisitDate" /></div>
		</div>
		<%} %>
		 <%if(Model.IsTravelParameterVisible){ %>
		<div class="row">
			<label for="<%= Model.Type %>_AssociatedMileage" class="fl">Associated Mileage</label>
			<div class="fr"><%= Html.TextBoxFor(t => t.AssociatedMileage, new { @id = Model.Type + "_AssociatedMileage", @class = "decimal", @maxlength = "7" })%></div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Surcharge" class="fl">Surcharge</label>
			<div class="fr"><%= Html.TextBoxFor(t => t.Surcharge, new { @id = Model.Type + "_Surcharge", @class = "currency", @maxlength = "7" })%></div>
		</div>
		<%} %>
		
        <%if(Model.IsAllergiesVisible && Model.IsAllergiesSavable){ %>
        <div class="row">
			<label class="fl">Allergies</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_AllergiesDescription", data.AnswerOrEmptyString("AllergiesDescription"), new { @class = "allergy-text short more" })%>
				<span class="img icon16 edit" onclick="Allergy.List('<%= Model.PatientId %>', '<%= Model.Service %>');"></span>
				<span class="img icon16 refresh" onclick="Allergy.GetCurrent('<%= Model.PatientId %>', '[name=<%= Model.Type %>_AllergiesDescription]');"></span>
			</div>
		</div>
		<%} %>
	</div>
</fieldset>
<% if (Model.IsDiagnosisVisible && data.AnswerOrEmptyString("PrimaryDiagnosis").IsNotNullOrEmpty()){ %>
<fieldset>
    <legend>Priority Diagnoses</legend>
    <div class="column">
	    <div class="row">
		    <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl">Primary Diagnosis</label>
		    <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
		    <div class="fr">
			    <span>
				    <%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
			    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
			    <% if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()){ %>
			    <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
			    <% } %>
		    </div>
	    </div>
    </div>
    <div class="column">
	    <div class="row">
		    <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl">Secondary Diagnosis</label>
		    <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
		    <div class="fr">
			    <span>
				    <%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
			    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
			    <% if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()){ %>
			    <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
			    <% } %>
		    </div>
	    </div>
    </div>
</fieldset>
<% } %>