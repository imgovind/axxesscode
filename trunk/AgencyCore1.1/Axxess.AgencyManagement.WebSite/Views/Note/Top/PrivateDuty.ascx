﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  bool showSendAsOrder = Model.IsUserCanMarkAsOrder; %>
<%  bool showCarePlanOrEvalUrlSection = Model.IsCarePlanVisible;  %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Hidden("PatientId", Model.PatientId, new { id = Model.Type + "_PatientId"})%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { id = Model.Type + "_EpisodeId" })%>
<%= Html.Hidden("EventId", Model.EventId, new { id = Model.Type + "_EventId" })%>
<%= Html.Hidden("EventDate", Model.EventStartDate, new { id = Model.Type + "_EventDate" })%>
<%= Html.Hidden("LocationId", Model.LocationId, new { id = Model.Type + "_LocationId" })%>
<%= Html.Hidden("Type", Model.Type)%>
<%= Html.Hidden("Version", Model.Version)%>
<%= Html.Hidden("Service", Model.Service)%>
<%  if (Model.HasReturnReasons){ %>
<%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.EpisodeId, Model.PatientId, Model.EpisodeId, "placeholder", Model.Service))  %>
<%  } %>
<fieldset>
    <legend>
        <%= Model.TypeName %>
        <%= Model.Version > 1 ? "(Version " + Model.Version + ")" : string.Empty %>
        <%  if (Model.HasReturnReasons){ %>
        <%= Html.ReturnCommentsLink(Model.Service, Model.EventId, Model.EpisodeId, Model.PatientId, true) %>
        <%  } %>
    </legend>
    <div class="column">
        <div class="row no-input">
            <label class="fl">Patient</label>
            <div class="fr">
                <%= Model.PatientProfile != null ? Model.PatientProfile.DisplayName : string.Empty %>
                (<%= Model.PatientProfile != null ? Model.PatientProfile.PatientIdNumber : string.Empty %>)</div>
        </div>
        <%  if (Model.CanChangeDisciplineType){ %>
        <div class="row">
            <label class="fl">Task</label>
            <div class="fr">
                <span class="button"><a class="load-disciplinetask">Load Discipline Task</a></span>
                <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask" })%>
            </div>
        </div>
        <%  } else { %>
        <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
        <%  } %>
    </div>
    <div class="column">
        <div class="row no-input">
            <%= Html.CarePeriodLabel((int)Model.Service, new { @for = Model.Type + "_EpsPeriod", @class = "fl"}) %>
            <div class="fr"><%= Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() %></div>
        </div>
        <%  if (showSendAsOrder){ %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption("SendAsOrder", "1", false, "Send as an order") %>
            </ul>
        </div>
        <%  } %>
        <%  if (showCarePlanOrEvalUrlSection){ %>
        <div class="row">
            <div class="ancillary-button">
                <a href="javascript:void(0)" careplanid='<%= Model.CarePlanOrEvalId%>' class="care-plan-print <%= Model.CarePlanOrEvalGroup%>">View Care Plan</a>
            </div>
        </div>
        <%  } %>
    </div>
</fieldset>
<fieldset>
    <legend>Visit Details</legend>
    <div class="column">
        <% if (Model.IsSelectedEpisodeVisible){ %>
        <div class="row">
            <%= Html.CarePeriodLabel((int)Model.Service, new { @for = Model.Type + "_SelectedEpisodeId", @class = "fl", value = "Associated " })%>
            <div class="fr">
                <%= Html.PatientEpisodes((int)Model.Service, "SelectedEpisodeId", Model.SelectedEpisodeId.ToString(), Model.PatientId, "-- Select Care Period --", new { @id = Model.Type + "_SelectedEpisodeId", @class = "required not-zero" })%>
            </div>
        </div>
        <% } %>
        <%if (Model.IsVisitParameterVisible){ %>
        <div class="row">
            <label class="fl" for="<%= Model.Type %>_StartDate">Actual Visit Date/Time In</label>
            <div class="fr">
                <%= Html.DatePicker("VisitStartTime.Date", Model.VisitStartDate, false, new { @class = "short complete-required", @id = Model.Type + "_VisitDate" })%>
                <%= Html.TimePicker("VisitStartTime.Time", Model.VisitStartDate, false, new { @class = "short complete-required", @id = Model.Type + "_VisitStartTime" })%>
            </div>
        </div>
        <div class="row">
            <label class="fl" for="<%= Model.Type %>_EndDate">Actual Visit Date/Time Out</label>
            <div class="fr">
                <%= Html.DatePicker("VisitEndTime.Date", Model.VisitEndDate, false, new { @class = "short complete-required", @id = Model.Type + "_EndDate" })%>
                <%= Html.TimePicker("VisitEndTime.Time", Model.VisitEndDate, false, new { @class = "short complete-required", @id = Model.Type + "_VisitEndTime" })%>
            </div>
        </div>
        <%} %>
        <%if (Model.IsDNRVisible){ %>
        <div class="row">
            <label for="<%= Model.Type %>_DNR" class="fl">DNR</label>
            <div class="fr"><%= Html.DNR(Model.Type + "_DNR", data.AnswerOrDefault("DNR", ""), true, new { @id = Model.Type + "_DNR" })%>
            </div>
        </div>
        <%} %>
    </div>
    <div class="column">
        <% if (Model.IsUserCanLoadPreviousNotes){ %>
        <div class="row">
            <label for="<%= Model.Type %>_PreviousNotes" class="fl">Previous Notes</label>
            <div class="fr"><%= Html.TogglePreviousNotes("PreviousNotes", Model.Type + "_PreviousNotes") %></div>
        </div>
        <% } %>
        <%if (Model.IsPhysicianVisible){ %>
        <div class="row">
            <label for="<%= Model.Type %>_PhysicianDropDown" class="fl">Physician</label>
            <div class="fr"><%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : data.AnswerOrEmptyString("PhysicianId"), new { @id = Model.Type + "_PhysicianDropDown", @class = "physician-picker" })%></div>
        </div>
        <%} %>
        <%if (Model.IsPhysicianDateVisible){ %>
        <div class="row">
            <label for="<%= Model.Type %>_LastVisitDate" class="fl">Last Physician Visit Date</label>
            <div class="fr">
                <input type="text" class="date-picker" name="<%= Model.Type %>_LastVisitDate" value="<%= data.AnswerOrEmptyString("LastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("LastVisitDate") : string.Empty %>"
                    maxdate="<%= Model.EndDate.ToShortDateString() %>" id="<%= Model.Type %>_LastVisitDate" /></div>
        </div>
        <%} %>
        <%if (Model.IsTravelParameterVisible){ %>
        <div class="row">
            <label for="<%= Model.Type %>_AssociatedMileage" class="fl">Associated Mileage</label>
            <div class="fr"><%= Html.TextBoxFor(t => t.AssociatedMileage, new { @id = Model.Type + "_AssociatedMileage", @class = "decimal", @maxlength = "7" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_Surcharge" class="fl">Surcharge</label>
            <div class="fr"><%= Html.TextBoxFor(t => t.Surcharge, new { @id = Model.Type + "_Surcharge", @class = "currency", @maxlength = "7" })%></div>
        </div>
        <%} %>
        <%if (Model.IsAllergiesVisible){ %>
        <div class="row">
            <label class="fl">Allergies</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_AllergiesDescription", data.AnswerOrEmptyString("AllergiesDescription"), new { @class = "allergy-text short more" })%>
                <span class="img icon16 edit" onclick="Allergy.List('<%= Model.PatientId %>', '<%= Model.Service %>');"></span>
                <span class="img icon16 refresh" onclick="Allergy.GetCurrent('<%= Model.PatientId %>', '[name=<%= Model.Type %>_AllergiesDescription]');"></span>
            </div>
        </div>
        <%} %>
    </div>
</fieldset>
