﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>General</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_Height1" class="fl">Height</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Height1", data.AnswerOrEmptyString("Height1"), new { @class = "shorter", @id = Model.Type + "_Height1", @maxlength = "2" })%>'
				<%= Html.TextBox(Model.Type + "_Height2", data.AnswerOrEmptyString("Height2"), new { @class = "shorter", @id = Model.Type + "_Height2", @maxlength = "2" })%>"
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Weight" class="fl">Weight</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Weight", data.AnswerOrEmptyString("Weight"), new { @class = "short more", @id = Model.Type + "_Weight" })%><label>lbs</label>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_BMI" class="fl">BMI</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_BMI", data.AnswerOrEmptyString("BMI"), new { @id = Model.Type + "_BMI" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_WeightStatus" class="fl">Weight Status</label>
			<div class="fr">
				<%  var weightStatusLevel = new SelectList(new[] {
								new SelectListItem { Text = "-- Select Weight Status --", Value = "" },
								new SelectListItem { Text = "Underweight", Value = "Underweight" },
								new SelectListItem { Text = "Healthy Weight", Value = "Healthy Weight" },
								new SelectListItem { Text = "Overweight", Value = "Overweight" },
								new SelectListItem { Text = "Obese", Value = "Obese" },
								new SelectListItem { Text = "Morbidly Obese", Value = "Morbidly Obese" }
							}, "Value", "Text", data.AnswerOrEmptyString("WeightStatus"));%>
				<%= Html.DropDownList(Model.Type + "_WeightStatus", weightStatusLevel, new { @id = Model.Type + "_WeightStatus" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_WeightStatus" class="fl">Past Medical History</label>
			<div class="fr">
				<%  var medicalHistoryLevel = new SelectList(new[] {
							new SelectListItem { Text = "-- Select Medical Histroy --", Value = "" },
							new SelectListItem { Text = "DM", Value = "DM" },
							new SelectListItem { Text = "CKD", Value = "CKD" },
							new SelectListItem { Text = "HTN", Value = "HTN" },
							new SelectListItem { Text = "CVA", Value = "CVA" },
							new SelectListItem { Text = "COPD", Value = "COPD" },
							new SelectListItem { Text = "Cancer", Value = "Cancer" }
						}, "Value", "Text", data.AnswerOrEmptyString("MedicalHistory"));%>
				<%= Html.DropDownList(Model.Type + "_MedicalHistory", medicalHistoryLevel, new { @id = Model.Type + "_MedicalHistory" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_SurgicalHistory" class="fl">Surgical History</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_SurgicalHistory", data.AnswerOrEmptyString("SurgicalHistory"), new { @id = Model.Type + "_SurgicalHistory" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_CurrentDiagnosis" class="fl">Current Diagnosis</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_CurrentDiagnosis", data.AnswerOrEmptyString("CurrentDiagnosis"), new { @id = Model.Type + "_CurrentDiagnosis" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_PatientLabData" class="fl">Patient Lab Data</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_PatientLabData", data.AnswerOrEmptyString("PatientLabData"), new { @id = Model.Type + "_PatientLabData" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_PertinentMedications" class="fl">Pertinent Medications</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_PertinentMedications", data.AnswerOrEmptyString("PertinentMedications"), new { @id = Model.Type + "_PertinentMedications" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Nutritional Needs</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_FoodAllergies" class="fl">Food Allergies</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_FoodAllergies", data.AnswerOrEmptyString("FoodAllergies"), new { @id = Model.Type + "_FoodAllergies" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_FoodIntolerances" class="fl">Food Intolerances</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_FoodIntolerances", data.AnswerOrEmptyString("FoodIntolerances"), new { @id = Model.Type + "_FoodIntolerances" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Dentition" class="fl">Dentition</label>
			<div class="fr">
				<%  var dentitionLevel = new SelectList(new[] {
						new SelectListItem { Text = "-- Select Dentition --", Value = "" },
						new SelectListItem { Text = "Adequate", Value = "Adequate" },
						new SelectListItem { Text = "Missing", Value = "Missing" },
						new SelectListItem { Text = "Partial", Value = "Partial" },
						new SelectListItem { Text = "Dentures", Value = "Dentures" },
						new SelectListItem { Text = "Edentulous", Value = "Edentulous" }
					}, "Value", "Text", data.AnswerOrEmptyString("Dentition"));%>
				<%= Html.DropDownList(Model.Type + "_Dentition", dentitionLevel, new { @id = Model.Type + "_Dentition" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_ChewingAbility" class="fl">Chewing Ability</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_ChewingAbility", data.AnswerOrEmptyString("ChewingAbility"), new { @id = Model.Type + "_ChewingAbility" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_SwallowingAbility" class="fl">Swallowing Ability</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_SwallowingAbility", data.AnswerOrEmptyString("SwallowingAbility"), new { @id = Model.Type + "_SwallowingAbility" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_CurrentDiet" class="fl">Current Diet/Texture</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_CurrentDiet", data.AnswerOrEmptyString("CurrentDiet"), new { @id = Model.Type + "_CurrentDiet" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_LiquidConsistency" class="fl">Liquid Consistency</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_LiquidConsistency", data.AnswerOrEmptyString("LiquidConsistency"), new { @id = Model.Type + "_LiquidConsistency" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_PreviousDiets" class="fl">Previous Diets</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_PreviousDiets", data.AnswerOrEmptyString("PreviousDiets"), new { @id = Model.Type + "_PreviousDiets" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_HasClient" class="fl">Has client been counseled on diet? </label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_HasClient", data.AnswerOrEmptyString("HasClient"), new { @id = Model.Type + "_HasClient" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_IsClient" class="fl">Is client taking any vitamins/minerals? </label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_IsClient", data.AnswerOrEmptyString("IsClient"), new { @id = Model.Type + "_IsClient" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_OralNutritionalSupplements" class="fl">Oral nutritional supplements? </label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_OralNutritionalSupplements", data.AnswerOrEmptyString("OralNutritionalSupplements"), new { @id = Model.Type + "_OralNutritionalSupplements" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_TubeFeeding" class="fl">Is client receiving a tube feeding? </label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_TubeFeeding", data.AnswerOrEmptyString("TubeFeeding"), new { @class = "short", @id = Model.Type + "_TubeFeedingkj" })%>
				<%  var feedingLevel = new SelectList(new[] {
							new SelectListItem { Text = "---", Value = "" },
							new SelectListItem { Text = "Formula", Value = "Formula" },
							new SelectListItem { Text = "Rate", Value = "Rate" },
							new SelectListItem { Text = "Flushes", Value = "Flushes" },
							new SelectListItem { Text = "Route", Value = "Route" }
						}, "Value", "Text", data.AnswerOrEmptyString("FeedingType"));%>
				<%= Html.DropDownList(Model.Type + "_FeedingType", feedingLevel, new { @id = Model.Type + "_FeedingType", @class = "shorter" })%>
			</div>
		</div>
		<div class="row">
			<div class="fl">
				<%  var formulaLevel = new SelectList(new[] {
						new SelectListItem { Text = "---", Value = "" },
						new SelectListItem { Text = "Volume", Value = "Volume" },
						new SelectListItem { Text = "Calories", Value = "Calories" },
						new SelectListItem { Text = "Protein", Value = "Protein" },
						new SelectListItem { Text = "Free water", Value = "Free water" },
						new SelectListItem { Text = "RDA", Value = "RDA" }
					}, "Value", "Text", data.AnswerOrEmptyString("FormulaType"));%>
				<%= Html.DropDownList(Model.Type + "_FormulaType", formulaLevel, new { @id = Model.Type + "_FormulaType", @class = "short less" })%>
				<label for="<%= Model.Type %>_TubeFeeding">provided by formula</label>
				<%= Html.TextBox(Model.Type + "_ProvidedByFormula", data.AnswerOrEmptyString("ProvidedByFormula"), new { @class = "short more", @id = Model.Type + "_ProvidedByFormula" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Diet history</label>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_Breakfast" class="fl">Breakfast</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Breakfast", data.AnswerOrEmptyString("Breakfast"), new { @id = Model.Type + "_Breakfast" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_Lunch" class="fl">Lunch</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Lunch", data.AnswerOrEmptyString("Lunch"), new { @id = Model.Type + "_Lunch" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_Dinner" class="fl">Dinner</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Dinner", data.AnswerOrEmptyString("Dinner"), new { @id = Model.Type + "_Dinner" })%>
			</div>
		</div>
		<div class="sub row">
			<label for="<%= Model.Type %>_Snacks" class="fl">Snacks</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Snacks", data.AnswerOrEmptyString("Snacks"), new { @id = Model.Type + "_Snacks" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_EstimatedCaloricNeeds" class="fl">Estimated Caloric Needs</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_EstimatedCaloricNeeds", data.AnswerOrEmptyString("EstimatedCaloricNeeds"), new { @id = Model.Type + "_EstimatedCaloricNeeds" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_EstimatedProteinNeeds" class="fl">Estimated Protein Needs</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_EstimatedProteinNeeds", data.AnswerOrEmptyString("EstimatedProteinNeeds"), new { @id = Model.Type + "_EstimatedProteinNeeds" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_EstimatedFluidNeeds" class="fl">Estimated Fluid Needs</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_EstimatedFluidNeeds", data.AnswerOrEmptyString("EstimatedFluidNeeds"), new { @id = Model.Type + "_EstimatedFluidNeeds" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Is current diet/TF adequate to meet caloric/protein/fluid needs?</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<%= Html.CheckgroupOption(Model.Type + "_IfAdequateToMeet", "1", data.AnswerOrEmptyString("IfAdequateToMeet").Equals("1"), "Yes")%>
					<%= Html.CheckgroupOption(Model.Type + "_IfAdequateToMeet", "0", data.AnswerOrEmptyString("IfAdequateToMeet").Equals("0"), "No")%>
				</ul>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Integumentary</legend>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_SkinCondition" class="fl">Skin condition</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_SkinCondition", data.AnswerOrEmptyString("SkinCondition"), new { @id = Model.Type + "_SkinCondition" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Stasis" class="fl">Stasis ulcer/location</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Stasis", data.AnswerOrEmptyString("Stasis"), new { @id = Model.Type + "_Stasis" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Diabetic" class="fl">Diabetic ulcer/location</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_Diabetic", data.AnswerOrEmptyString("Diabetic"), new { @id = Model.Type + "_Diabetic" })%>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label for="<%= Model.Type %>_SkinTurgor" class="fl">Skin Turgor</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_SkinTurgor", data.AnswerOrEmptyString("SkinTurgor"), new { @id = Model.Type + "_SkinTurgor" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_Mucous" class="fl">Mucous Membranes</label>
			<div class="fr">
				<%  var mucousLevel = new SelectList(new[] {
						new SelectListItem { Text = "-- Select Moisture --", Value = "" },
						new SelectListItem { Text = "Moist", Value = "Moist" },
						new SelectListItem { Text = "Dry", Value = "Dry" }
					}, "Value", "Text", data.AnswerOrEmptyString("MucousMembranes"));%>
				<%= Html.DropDownList(Model.Type + "_MucousMembranes", mucousLevel, new { @id = Model.Type + "_MucousMembranes" })%>
			</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Assessment</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_AssessmentTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_Assessment", data.AnswerOrEmptyString("Assessment"), new { @id = Model.Type + "_Assessment" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Plan</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
					    <%= Html.ToggleTemplates(Model.Type + "_PlanTemplates")%>
					    <%= Html.TextArea(Model.Type + "_Plan", data.AnswerOrEmptyString("Plan"), new { @id = Model.Type + "_Plan" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
