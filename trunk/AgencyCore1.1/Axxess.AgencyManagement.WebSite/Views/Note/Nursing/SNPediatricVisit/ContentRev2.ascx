﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/VitalSigns/Rev1", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/PediatricsIntegumentary", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/PediatricsCardiovascular", Model); %>
<% Html.RenderPartial("Nursing/Sections/PediatricsMusculoskeletal", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/PediatricsGastrointestinal", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/PediatricsGenitourinary", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/PediatricsPulmonary", Model); %>
<% Html.RenderPartial("Nursing/Sections/PediatricsPain", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Care Plan</legend>
			<div class="column">
				<div class="row">
					<label for="<%= Model.Type %>_DischargeInstructions">Significant Clinical Findings Relative to Diagnosis and Care Plan</label>
					<div class="template-text">
						<%= Html.ToggleTemplates(Model.Type + "_GenericCarePlanTemplates")%>
						<%= Html.TextArea(Model.Type + "_GenericCarePlan", data.AnswerOrEmptyString("GenericCarePlan"), new { @class = "tall" })%>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Drug</legend>
			<div class="column">
				<div class="row">
					<label class="fl">Drug</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDrug", data.AnswerOrEmptyString("GenericDrug"), new { @id = Model.Type + "_GenericDrug", @maxlength = "30" })%>
					</div>
				</div>
				<div class="row">
					<label class="fl">Dose</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDose", data.AnswerOrEmptyString("GenericDose"), new { @id = Model.Type + "_GenericDose",  @maxlength = "30" })%>
					</div>
				</div>
				<div class="row">
					<label class="fl">Route</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericRoute", data.AnswerOrEmptyString("GenericRoute"), new { @id = Model.Type + "_GenericRoute", @maxlength = "30" })%>
					</div>
				</div>
				<div class="row">
					<label class="fl">Time</label>
					<div class="fr">
						<%= Html.TextBox(Model.Type + "_GenericDrugTime", data.AnswerOrEmptyString("GenericDrugTime"), new { @id = Model.Type + "_GenericDrugTime", @class = "time-picker complete-required", @maxlength = "30" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<fieldset>
			<legend>Narrative</legend>
			<div class="column">
				<div class="row">
				    <div class="template-text">
    					<%= Html.ToggleTemplates(Model.Type + "_NarrativeTemplates")%>
	    				<%= Html.TextArea(Model.Type + "_Narrative", data.AnswerOrEmptyString("Narrative"), new { @class = "tall" })%>
	    			</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Interventions</legend>
			<div class="column">
				<div class="row">
					<label class="al">Skilled Intervention or training provided</label>
					<div class="fr">
						<div class="checkgroup two-wide">
							<% string[] skilledInterventionProvided = data.AnswerArray("SkilledInterventionProvided"); %>
							<%= Html.Hidden(Model.Type + "_SkilledInterventionProvided", string.Empty, new { @id = Model.Type + "_SkilledInterventionProvided" })%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_SkilledInterventionProvided", "1", skilledInterventionProvided.Contains("1"), "Yes")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_SkilledInterventionProvided", "0", skilledInterventionProvided.Contains("0"), "No")%>
						</div>
					</div>
				</div>
				<div class="row">
					<label for="<%= Model.Type %>_SkilledInterventionExplain">Explain</label>
					<div class="ac">
						<%= Html.TextArea(Model.Type + "_SkilledInterventionExplain", data.AnswerOrEmptyString("SkilledInterventionExplain"), new { @id = Model.Type + "_SkilledInterventionExplain", @class = "tall" })%>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		
	</div>
	<div>
		
	</div>
</div>
<fieldset>
	<legend>Case supervisory visit made</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup two-wide">
				<% string[] supervisoryVisit = data.AnswerArray("SupervisoryVisit"); %>
				<%= Html.Hidden(Model.Type + "_SupervisoryVisit", string.Empty, new { @id = Model.Type + "_SupervisoryVisit" })%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_SupervisoryVisit", "1", supervisoryVisit.Contains("1"), "Yes")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_SupervisoryVisit", "0", supervisoryVisit.Contains("0"), "No")%>
			</div>
		</div>
	</div>
</fieldset>
