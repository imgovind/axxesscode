﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Safety</legend>
    <div class="wide-column">
        <div class="row">
            <label class="al">Safety Measures</label>
            <div class="checkgroup three-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety0' name='{0}_Safety' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety0">Fire/electrical safety</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety1' name='{0}_Safety' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety1">Burn prevention</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety2' name='{0}_Safety' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety2">Poisoning prevention</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety3' name='{0}_Safety' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety3">Falls prevention</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety4' name='{0}_Safety' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety4">Water safety</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety5' name='{0}_Safety' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety5">Siderails up</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety6' name='{0}_Safety' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety6">Clear pathways</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety7' name='{0}_Safety' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety7">Suffocation precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety8' name='{0}_Safety' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety8">Aspiration precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety9' name='{0}_Safety' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety9">Elevate head of bed</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety10' name='{0}_Safety' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety10">Seizure precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety11' name='{0}_Safety' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("11").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety11">Transfer/ambulation safety</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety12' name='{0}_Safety' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("12").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety12">Wheelchair precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety13' name='{0}_Safety' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("13").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety13">Proper use of assistive devices</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety14' name='{0}_Safety' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("14").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety14">Universal precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety15' name='{0}_Safety' value='15' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("15").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety15">Sharps and/or supplies disposals</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety16' name='{0}_Safety' value='16' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("16").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety16">Cardiac prevention</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety17' name='{0}_Safety' value='17' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("17").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety17">Diabetic precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety18' name='{0}_Safety' value='18' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("18").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety18">Oxygen safety/precautions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Safety19' name='{0}_Safety' value='19' type='checkbox' {1} />", Model.Type, data.AnswerArray("Safety").Contains("19").ToChecked())%>
                        <label for="<%= Model.Type %>_Safety19">Bleeding precautions</label>
                    </div>
                </div>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_Safety", "20", data.AnswerArray("Safety").Contains("20"), "Other", Model.Type + "_SafetyOther", data.AnswerOrEmptyString("SafetyOther"))%>
            </div>
        </div>
    </div>
</fieldset>
