﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Hematology</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsHematologyApply", string.Empty, new { @id = Model.Type + "IsHematologyApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsHematologyApply", Model.Type + "IsHematologyApply", "1", data.AnswerArray("IsHematologyApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Hematology0' name='{0}_Hematology' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Hematology0">Anemia</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Hematology1' name='{0}_Hematology' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Hematology1">Bilirubin</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_HematologyBilirubin" class="fl">Results</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_HematologyBilirubin", data.AnswerOrEmptyString("HematologyBilirubin"), new { @id = Model.Type + "_HematologyBilirubin"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Hematology2' name='{0}_Hematology' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Hematology").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Hematology2">Other</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_HematologyOther" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_HematologyOther", data.AnswerOrEmptyString("HematologyOther"), new { @id = Model.Type + "_HematologyOther" })%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</fieldset>
