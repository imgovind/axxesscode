﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Living Arrangements/Caregiver</legend>
    <div class="column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_LivingArrange", "0", data.AnswerArray("LivingArrange").Contains("0"), "House") %>
                <%= Html.CheckgroupOption(Model.Type + "_LivingArrange", "1", data.AnswerArray("LivingArrange").Contains("1"), "Apartment") %>
                <%= Html.CheckgroupOption(Model.Type + "_LivingArrange", "2", data.AnswerArray("LivingArrange").Contains("2"), "New environment") %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_PrimaryLanguage">Primary language</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_PrimaryLanguage", data.AnswerOrEmptyString("PrimaryLanguage"), new { @id = Model.Type + "_PrimaryLanguage"})%></div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_PrimaryLanguageOption", "0", data.AnswerArray("PrimaryLanguageOption").Contains("0"), "Language barrier")%>
                <%= Html.CheckgroupOption(Model.Type + "_PrimaryLanguageOption", "1", data.AnswerArray("PrimaryLanguageOption").Contains("1"), "Needs interpreter")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_PrimaryLanguageOption2' name='{0}_PrimaryLanguageOption' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_PrimaryLanguageOption2">Learning barrier</label>
                    </div>
                    <div class="more">
                        <div class="fr">
                            <%  var learningBarrierLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Mental", Value = "Mental" },
                                    new SelectListItem { Text = "Psychosocial", Value = "Psychosocial" },
                                    new SelectListItem { Text = "Physical", Value = "Physical" },
                                    new SelectListItem { Text = "Functional", Value = "Functional" }
                                }, "Value", "Text", data.AnswerOrDefault("LearningBarrierLevel", ""));%>
                            <%= Html.DropDownList(Model.Type + "_LearningBarrierLevel", learningBarrierLevel, new { @id = Model.Type + "_LearningBarrierLevel" })%>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_PrimaryLanguageOption3' name='{0}_PrimaryLanguageOption' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_PrimaryLanguageOption3">Able to read/write</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_EducationalLevel" class="fl">Educational level</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_EducationalLevel", data.AnswerOrEmptyString("EducationalLevel"), new { @id = Model.Type + "_EducationalLevel"})%></div>
                        <div class="clr"></div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_PrimaryLanguageOption4' name='{0}_PrimaryLanguageOption' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_PrimaryLanguageOption4">Spiritual/Cultural implications that impact care</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_SpiritualResource" class="fl">Spiritual resource</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_SpiritualResource", data.AnswerOrEmptyString("SpiritualResource"), new { @id = Model.Type + "_SpiritualResource" })%></div>
                        <div class="clr"></div>
                        <label for="<%= Model.Type %>_SpiritualPhone" class="fl">Phone No</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_SpiritualPhone", data.AnswerOrEmptyString("SpiritualPhone"), new { @id = Model.Type + "_SpiritualPhone" })%></div>
                        <div class="clr"></div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_PrimaryLanguageOption5' name='{0}_PrimaryLanguageOption' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryLanguageOption").Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_PrimaryLanguageOption5">Siblings</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_SiblingSpecify" class="fl">Specify</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_SiblingSpecify", data.AnswerOrEmptyString("SiblingSpecify"), new { @id = Model.Type + "_SiblingSpecify", @class = "fr" })%></div>
                        <div class="clr"></div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CaregiverName" class="fl">Primary caregiver</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_CaregiverName", data.AnswerOrEmptyString("CaregiverName"), new { @id = Model.Type + "_CaregiverName"})%></div>
        </div>
        <div class="row">
            <label>Relationship/Health status</label>
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.Type + "_PrimaryCaregiver", "0", data.AnswerArray("PrimaryCaregiver").Contains("0"), "Assists with ADLs")%>
                <%= Html.CheckgroupOption(Model.Type + "_PrimaryCaregiver", "1", data.AnswerArray("PrimaryCaregiver").Contains("1"), "Provides physical care")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_PrimaryCaregiver2' name='{0}_PrimaryCaregiver' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("PrimaryCaregiver").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_PrimaryCaregiver2">Other</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_PrimaryCaregiverOther" class="fl">Specify</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_PrimaryCaregiverOther", data.AnswerOrEmptyString("PrimaryCaregiverOther"), new { @id = Model.Type + "_PrimaryCaregiverOther"})%></div>
                        <div class="clr"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>