﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] genitourinary = data.AnswerArray("Genitourinary"); %>
<%= Html.Hidden(Model.Type + "_Genitourinary", string.Empty, new { @id = Model.Type + "_Genitourinary" })%>
<% string[] genitourinaryUrine = data.AnswerArray("GenitourinaryUrine"); %>
<%= Html.Hidden(Model.Type + "_GenitourinaryUrine", string.Empty, new { @id = Model.Type + "_GenitourinaryUrine" })%>
<fieldset>
    <legend>Genitourinary</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsGenitourinaryApply", string.Empty, new { @id = Model.Type + "IsGenitourinaryApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsGenitourinaryApply", Model.Type + "IsGenitourinaryApply", "1", data.AnswerArray("IsGenitourinaryApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Genitourinary0' name='{0}_Genitourinary' value='0' type='checkbox' {1} />", Model.Type, genitourinary.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Genitourinary0">Diapers/day</label>
                    </div>
                    <div class="more">
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_GenitourinaryDiapers", data.AnswerOrEmptyString("GenitourinaryDiapers"), new { @id = Model.Type + "_GenitourinaryDiapers" })%></div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Genitourinary1' name='{0}_Genitourinary' value='1' type='checkbox' {1} />", Model.Type, genitourinary.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Genitourinary1">Toilet trained</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="fr">
                                <%  var toiletTrainedTimeLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Day", Value = "Day" },
                                            new SelectListItem { Text = "Night", Value = "Night" },
                                            new SelectListItem { Text = "Both", Value = "Both" }
                                        }, "Value", "Text", data.AnswerOrDefault("ToiletTrainedTime", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ToiletTrainedTime", toiletTrainedTimeLevel, new { @id = Model.Type + "_ToiletTrainedTime" })%></div>
                        </div>
                        <div class="clear" />
                        <div>
                            <div class="fr">
                                <%  var toiletTrainedUnitLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Bladder", Value = "Bladder" },
                                            new SelectListItem { Text = "Bowel", Value = "Bowel" },
                                            new SelectListItem { Text = "Both", Value = "Both" }
                                        }, "Value", "Text", data.AnswerOrDefault("ToiletTrainedUnit", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ToiletTrainedUnit", toiletTrainedUnitLevel, new { @id = Model.Type + "_ToiletTrainedUnit" })%></div>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Urine</label>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_GenitourinaryColor" class="fl">Color</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenitourinaryColor", data.AnswerOrEmptyString("GenitourinaryColor"), new { @id = Model.Type + "_GenitourinaryColor" })%></div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_GenitourinaryAmt" class="fl">Amt</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenitourinaryAmt", data.AnswerOrEmptyString("GenitourinaryAmt"), new { @id = Model.Type + "_GenitourinaryAmt" })%></div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_GenitourinaryOdor" class="fl">Odor</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenitourinaryOdor", data.AnswerOrEmptyString("GenitourinaryOdor"), new { @id = Model.Type + "_GenitourinaryOdor" })%></div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_GenitourinaryFreq" class="fl">Frequency</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenitourinaryFreq", data.AnswerOrEmptyString("GenitourinaryFreq"), new { @id = Model.Type + "_GenitourinaryFreq" })%></div>
        </div>
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitourinaryUrine0' name='{0}_GenitourinaryUrine' value='0' type='checkbox' {1} />", Model.Type, genitourinaryUrine.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitourinaryUrine0">Burning</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitourinaryUrine1' name='{0}_GenitourinaryUrine' value='1' type='checkbox' {1} />", Model.Type, genitourinaryUrine.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitourinaryUrine1">Itching</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitourinaryUrine2' name='{0}_GenitourinaryUrine' value='2' type='checkbox' {1} />", Model.Type, genitourinaryUrine.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitourinaryUrine2">Enuresis</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_GenitourinaryEnuresis" class="fl">Bedtime ritual</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_GenitourinaryEnuresis", data.AnswerOrEmptyString("GenitourinaryEnuresis"), new { @id = Model.Type + "_GenitourinaryEnuresis" })%></div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitourinaryUrine3' name='{0}_GenitourinaryUrine' value='3' type='checkbox' {1} />", Model.Type, genitourinaryUrine.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitourinaryUrine3">Catheter</label>
                    </div>
                    <div class="more">
                        <div>
                            <label for="<%= Model.Type %>_GenitourinaryCatheter" class="fl">Type/brand</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_GenitourinaryCatheter", data.AnswerOrEmptyString("GenitourinaryCatheter"), new { @id = Model.Type + "_GenitourinaryCatheter" })%></div>
                        </div>
                        <div class="clear" />
                        <div>
                            <div>
                                <div class="checkgroup two-wide">
                                    <% string[] genitourinaryCatheterType = data.AnswerArray("GenitourinaryCatheterType"); %>
                                    <%= Html.Hidden(Model.Type + "_GenitourinaryCatheterType", string.Empty, new { @id = Model.Type + "_GenitourinaryCatheterType" })%>
                                    <%= Html.CheckgroupOption(Model.Type + "_GenitourinaryCatheterType", "0", genitourinaryCatheterType.Contains("0"), "Foley")%>
                                    <%= Html.CheckgroupOption(Model.Type + "_GenitourinaryCatheterType", "1", genitourinaryCatheterType.Contains("1"), "Straight catheter")%>
                                    <%= Html.CheckgroupOption(Model.Type + "_GenitourinaryCatheterType", "2", genitourinaryCatheterType.Contains("2"), "External")%>
                                </div>
                            </div>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitourinaryUrine4' name='{0}_GenitourinaryUrine' value='4' type='checkbox' {1} />", Model.Type, genitourinaryUrine.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitourinaryUrine">Other</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_GenitourinaryOther" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_GenitourinaryOther", data.AnswerOrEmptyString("GenitourinaryOther"), new { @id = Model.Type + "_GenitourinaryOther" })%></div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</fieldset>
