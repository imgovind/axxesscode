﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Advance Directives</legend>
    <div class="column">
        <div class="row">
            <ul class="checkgroup two-wide">
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "0", data.AnswerArray("AdvanceDirective").Contains("0"), "Do not resuscitate")%>
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "1", data.AnswerArray("AdvanceDirective").Contains("1"), "Do not hospitalize")%>
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "2", data.AnswerArray("AdvanceDirective").Contains("2"), "Organ donor")%>
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "3", data.AnswerArray("AdvanceDirective").Contains("3"), "Education needed")%>
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "4", data.AnswerArray("AdvanceDirective").Contains("4"), "Copies on file")%>
                <%= Html.CheckgroupOption(Model.Type + "_AdvanceDirective", "5", data.AnswerArray("AdvanceDirective").Contains("5"), "Funeral arrangements made")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_AdvanceDirectiveOther" class="fl">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_AdvanceDirectiveOther", data.AnswerOrEmptyString("AdvanceDirectiveOther"), new { @id = Model.Type + "_AdvanceDirectiveOther"})%></div>
        </div>
    </div>
</fieldset>
