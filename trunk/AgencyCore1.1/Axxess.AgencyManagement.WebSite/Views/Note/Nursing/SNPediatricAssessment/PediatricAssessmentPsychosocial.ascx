﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] psychosocial = data.AnswerArray("Psychosocial"); %>
<%= Html.Hidden(Model.Type + "_Psychosocial", string.Empty, new { @id = Model.Type + "_Psychosocial" })%>
<fieldset>
    <legend>Psychosocial</legend>
    <div class="wide-column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsPsychosocialApply", string.Empty, new { @id = Model.Type + "IsPsychosocialApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsPsychosocialApply", Model.Type + "IsPsychosocialApply", "1", data.AnswerArray("IsPsychosocialApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial0' name='{0}_Psychosocial' value='0' type='checkbox' {1} />", Model.Type, psychosocial.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial0">Angry</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial1' name='{0}_Psychosocial' value='1' type='checkbox' {1} />", Model.Type, psychosocial.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial1">Flat affect</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial2' name='{0}_Psychosocial' value='2' type='checkbox' {1} />", Model.Type, psychosocial.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial2">Discouraged</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial3' name='{0}_Psychosocial' value='3' type='checkbox' {1} />", Model.Type, psychosocial.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial3">Withdrawn</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial4' name='{0}_Psychosocial' value='4' type='checkbox' {1} />", Model.Type, psychosocial.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial4">Difficulty coping</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial5' name='{0}_Psychosocial' value='5' type='checkbox' {1} />", Model.Type, psychosocial.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial5">Disorganized</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial6' name='{0}_Psychosocial' value='6' type='checkbox' {1} />", Model.Type, psychosocial.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial6">Recent family change</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup three-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_RecentChange0' name='{0}_RecentChange' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("0").ToChecked())%>
                                    <label for="<%= Model.Type %>_RecentChange0">Birth</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_RecentChange1' name='{0}_RecentChange' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("1").ToChecked())%>
                                    <label for="<%= Model.Type %>_RecentChange1">Death</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_RecentChange2' name='{0}_RecentChange' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("2").ToChecked())%>
                                    <label for="<%= Model.Type %>_RecentChange2">Moved</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_RecentChange3' name='{0}_RecentChange' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("3").ToChecked())%>
                                    <label for="<%= Model.Type %>_RecentChange3">Divorce</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_RecentChange4' name='{0}_RecentChange' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("RecentChange").Contains("4").ToChecked())%>
                                    <label for="<%= Model.Type %>_RecentChange4">Other</label>
                                </div>
                                <div class="more">
                                    <label for="<%= Model.Type %>_RecentChangeOther">Specify</label>
                                    <div class="fr">
                                        <%= Html.TextBox(Model.Type + "_RecentChangeOther", data.AnswerOrEmptyString("RecentChangeOther"), new { @id = Model.Type + "_RecentChangeOther", @class = "short" })%>
                                    </div>
                                    <div class="clear" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial7' name='{0}_Psychosocial' value='7' type='checkbox' {1} />", Model.Type, psychosocial.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial7">Suicidal</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup two-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_PsychosocialSui0' name='{0}_PsychosocialSui' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("PsychosocialSui").Contains("0").ToChecked())%>
                                    <label for="<%= Model.Type %>_PsychosocialSui0">Ideation</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_PsychosocialSui1' name='{0}_PsychosocialSui' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("PsychosocialSui").Contains("1").ToChecked())%>
                                    <label for="<%= Model.Type %>_PsychosocialSui1">Verbalized</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial8' name='{0}_Psychosocial' value='8' type='checkbox' {1} />", Model.Type, psychosocial.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial8">Depressed</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="checkgroup two-wide">
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialDepressed0' name='{0}_PsychosocialDepressed' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("PsychosocialDepressed").Contains("0").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialDepressed0">Recent</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialDepressed1' name='{0}_PsychosocialDepressed' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("PsychosocialDepressed").Contains("1").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialDepressed1">Long term</label>
                                    </div>
                                </div>
                            </div>
                            <div class="clear" />
                        </div>
                        <div>
                            <label for="<%= Model.Type %>_DepressedReason" class="fl">Due to</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_DepressedReason", data.AnswerOrEmptyString("DepressedReason"), new { @id = Model.Type + "_DepressedReason" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial9' name='{0}_Psychosocial' value='9' type='checkbox' {1} />", Model.Type, psychosocial.Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial9">Substance use</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="checkgroup three-wide">
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialSubstance0' name='{0}_PsychosocialSubstance' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("0").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialSubstance0">Drugs</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialSubstance1' name='{0}_PsychosocialSubstance' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("1").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialSubstance1">Alcohol</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialSubstance2' name='{0}_PsychosocialSubstance' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialSubstance").Contains("2").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialSubstance2">Tobacco</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Psychosocial10' name='{0}_Psychosocial' value='10' type='checkbox' {1} />", Model.Type, psychosocial.Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_Psychosocial10">Evidence of abuse</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="checkgroup two-wide">
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialAbuse0' name='{0}_PsychosocialAbuse' value='0' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("0").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialAbuse0">Potential</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialAbuse1' name='{0}_PsychosocialAbuse' value='1' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("1").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialAbuse1">Actual</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialAbuse2' name='{0}_PsychosocialAbuse' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("2").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialAbuse2">Verbal/Emotional</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialAbuse3' name='{0}_PsychosocialAbuse' value='3' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("3").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialAbuse3">Physical</label>
                                    </div>
                                </div>
                                <div class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_PsychosocialAbuse4' name='{0}_PsychosocialAbuse' value='4' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("PsychosocialAbuse").Contains("4").ToChecked())%>
                                        <label for="<%= Model.Type %>_PsychosocialAbuse4">Financial</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_PsychosocialFindings" class="fl">Describe findings</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_PsychosocialFindings", data.AnswerOrEmptyString("PsychosocialFindings"), new { @id = Model.Type + "_PsychosocialFindings" })%>
            </div>
        </div>
        <div class="row">
            <label  class="fl">Describe relationships with the following</label>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_RelationshipWithParents" class="fl">Parents</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_RelationshipWithParents", data.AnswerOrEmptyString("RelationshipWithParents"), new { @id = Model.Type + "_RelationshipWithParents" })%>
            </div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_RelationshipWithSiblings"class="fl">Siblings</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_RelationshipWithSiblings", data.AnswerOrEmptyString("RelationshipWithSiblings"), new { @id = Model.Type + "_RelationshipWithSiblings" })%>
            </div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_RelationshipWithPeers" class="fl">Peers</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_RelationshipWithPeers", data.AnswerOrEmptyString("RelationshipWithPeers"), new { @id = Model.Type + "_RelationshipWithPeers" })%>
            </div>
        </div>
        <div class="row">
            <div>
                <label class="al">Child care arrangements</label>
                <div class="checkgroup two-wide">
                    <% string[] childCareArran = data.AnswerArray("ChildCareArran"); %>
                    <%= Html.Hidden(Model.Type + "_ChildCareArran", string.Empty, new { @id = Model.Type + "_ChildCareArran" })%>
                    <%= Html.CheckgroupOption(Model.Type + "_ChildCareArran", "0", childCareArran.Contains("0"), "Daycare")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ChildCareArran", "1", childCareArran.Contains("1"), "Home")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ChildCareArran", "2", childCareArran.Contains("2"), "Private sitter")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ChildCareArran", "3", childCareArran.Contains("3"), "Family member")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_ChildCareArran", "4", childCareArran.Contains("4"), "Other", Model.Type + "_ChildCareArranOther", data.AnswerOrEmptyString("ChildCareArranOther"))%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_SchoolBehavior" class="fl">Behavior at day care/school</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_SchoolBehavior", data.AnswerOrEmptyString("SchoolBehavior"), new { @id = Model.Type + "_SchoolBehavior" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_RestPattern" class="fl">Usual sleep/rest pattern</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_RestPattern", data.AnswerOrEmptyString("RestPattern"), new { @id = Model.Type + "_RestPattern" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_SleepingArran" class="fl">Sleeping arrangements</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_SleepingArran", data.AnswerOrEmptyString("SleepingArran"), new { @id = Model.Type + "_SleepingArran" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_PsychosocialOther" class="fl">Other</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_PsychosocialOther", data.AnswerOrEmptyString("PsychosocialOther"), new { @id = Model.Type + "_PsychosocialOther" })%>
            </div>
        </div>
    </div>
  </div>
</fieldset>
