﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] heartSounds = data.AnswerArray("HeartSounds"); %>
<%= Html.Hidden(Model.Type + "_HeartSounds", string.Empty, new { @id = Model.Type + "_HeartSounds" })%>
<% string[] cardiovascular = data.AnswerArray("Cardiovascular"); %>
<%= Html.Hidden(Model.Type + "_Cardiovascular", string.Empty, new { @id = Model.Type + "_Cardiovascular" })%>
<fieldset>
    <legend>Cardiovascular</legend>
    <div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsCardiovascularApply", string.Empty, new { @id = Model.Type + "IsCardiovascularApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsCardiovascularApply", Model.Type + "IsCardiovascularApply", "1", data.AnswerArray("IsCardiovascularApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
        <div class="column">       
            <div class="row">
                <label class="al">Heart sounds</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_HeartSounds", "0", heartSounds.Contains("0"), "Regular")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_HeartSounds1' name='{0}_HeartSounds' value='1' type='checkbox' {1} />", Model.Type, heartSounds.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_HeartSounds1">Irregular</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HeartSoundsIrreg" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HeartSoundsIrreg", data.AnswerOrEmptyString("HeartSoundsIrreg"), new { @id = Model.Type + "_HeartSoundsIrreg", @maxlength = "30" })%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_Cardiovascular", "0", cardiovascular.Contains("0"), "Palpitations")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Cardiovascular1' name='{0}_Cardiovascular' value='1' type='checkbox' {1} />", Model.Type, cardiovascular.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_Cardiovascular1">Pulse deficit</label>
                        </div>
                        <div class="more">
                            <label for="CardiovascularPulseDeficit" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_CardiovascularPulseDeficit", data.AnswerOrEmptyString("CardiovascularPulseDeficit"), new { @id = Model.Type + "_CardiovascularPulseDeficit", @maxlength = "30" })%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.Type + "_Cardiovascular", "2", cardiovascular.Contains("2"), "Edema")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Cardiovascular", "3", cardiovascular.Contains("3"), "JVD")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Cardiovascular", "4", cardiovascular.Contains("4"), "Fatigue")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Cardiovascular5' name='{0}_Cardiovascular' value='5' type='checkbox' {1} />", Model.Type, cardiovascular.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_Cardiovascular5">Cyanosis</label>
                        </div>
                        <div class="more">
                            <label for="CardiovascularCyanosis" class="fl">Site</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_CardiovascularCyanosis", data.AnswerOrEmptyString("CardiovascularCyanosis"), new { @id = Model.Type + "_CardiovascularCyanosis", @maxlength = "30" })%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Cardiovascular6' name='{0}_Cardiovascular' value='6' type='checkbox' {1} />", Model.Type, cardiovascular.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_Cardiovascular6">Cap refill</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_CardiovascularCap" class="fl">Specify</label>
                            <div class="fr">
                                <%  var genericCapLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Less than 3 sec", Value = "Less than 3 sec" },
                                        new SelectListItem { Text = "More than 3 sec", Value = "More than 3 sec" }
                                    }, "Value", "Text", data.AnswerOrDefault("CardiovascularCap", ""));%>
                                <%= Html.DropDownList(Model.Type + "_CardiovascularCap", genericCapLevel, new { @id = Model.Type + "_CardiovascularCap" })%>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Cardiovascular7' name='{0}_Cardiovascular' value='7' type='checkbox' {1} />", Model.Type, cardiovascular.Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_Cardiovascular7">Pulses</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_CardiovascularPulses" class="fl">Specify</label>
                            <div class="fr">
                                <%  var genericPulsesLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "LDP", Value = "LDP" },
                                        new SelectListItem { Text = "LPT", Value = "LPT" },
                                        new SelectListItem { Text = "RDP", Value = "RDP" },
                                        new SelectListItem { Text = "RPT", Value = "RPT" }
                                    }, "Value", "Text", data.AnswerOrDefault("CardiovascularPulses", ""));%>
                                <%= Html.DropDownList(Model.Type + "_CardiovascularPulses", genericPulsesLevel, new { @id = Model.Type + "_CardiovascularPulses" })%>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Cardiovascular8' name='{0}_Cardiovascular' value='8' type='checkbox' {1} />", Model.Type, cardiovascular.Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_Cardiovascular8">Other</label>
                        </div>
                        <div class="more">
                            <label for="CardiovascularOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_CardiovascularOther", data.AnswerOrEmptyString("CardiovascularOther"), new { @id = Model.Type + "_CardiovascularOther", @maxlength = "30" })%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</fieldset>
