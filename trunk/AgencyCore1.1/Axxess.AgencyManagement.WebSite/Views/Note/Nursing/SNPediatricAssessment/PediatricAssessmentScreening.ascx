﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Screening/Early detection</legend>
    <div class="column">
        <div class="row">
            <label class="al">TB skin test</label>
            <div class="checkgroup two-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_SkinTest1' name='{0}_SkinTest' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("SkinTest").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinTest1">Yes</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_SkinTestYes" class="fl">When/Results</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_SkinTestYes", data.AnswerOrEmptyString("SkinTestYes"), new { @id = Model.Type + "_SkinTestYes" })%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_SkinTest0' name='{0}_SkinTest' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("SkinTest").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinTest0">No</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="al">Lead screening</label>
            <div class="checkgroup two-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_LeadScreening1' name='{0}_LeadScreening' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("LeadScreening").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_LeadScreening1">Yes</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_LeadScreeningYes" class="fl">When/Results</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_LeadScreeningYes", data.AnswerOrEmptyString("LeadScreeningYes"), new { @id = Model.Type + "_LeadScreeningYes" })%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_LeadScreening0' name='{0}_LeadScreening' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("LeadScreening").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_LeadScreening0">No</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_ScreeningOther" class="fl">Other</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_ScreeningOther", data.AnswerOrEmptyString("ScreeningOther"), new { @id = Model.Type + "_ScreeningOther" })%></div>
        </div>
    </div>
</fieldset>
