﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] neurologicalOrient = data.AnswerArray("NeurologicalOrient"); %>
<%  string[] neurologicalCognitive = data.AnswerArray("NeurologicalCognitive"); %>
<%  var neSelectItems = new[] {
        new SelectListItem { Text = "----", Value = "" },
        new SelectListItem { Text = "N - Normal", Value = "0" },
        new SelectListItem { Text = "A - Abnormal", Value = "1" },
        new SelectListItem { Text = "NA - Not applicable", Value = "2" }}; %>
<%= Html.Hidden(Model.Type + "_NeurologicalOrient", string.Empty, new { @id = Model.Type + "_NeurologicalOrient" })%>
<%= Html.Hidden(Model.Type + "_NeurologicalCognitive", string.Empty, new { @id = Model.Type + "_NeurologicalCognitive" })%>
<fieldset>
    <legend>Neurological</legend>
    <div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsNeurologicalApply", string.Empty, new { @id = Model.Type + "IsNeurologicalApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsNeurologicalApply", Model.Type + "IsNeurologicalApply", "1", data.AnswerArray("IsNeurologicalApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
        <div class="wide-column">
            <div class="row ac">
                <em>Reflexes: N-normal, A-abnormal, NA-not applicable</em>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_NEURooting" class="fl">Rooting</label>
                <div class="fr">
                    <%  var nEURooting = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEURooting", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEURooting", nEURooting, new { @id = Model.Type + "_NEURooting" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUSucking" class="fl">Sucking</label>
                <div class="fr">
                    <%  var nEUSucking = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUSucking", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUSucking", nEUSucking, new { @id = Model.Type + "_NEUSucking" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUOrienting" class="fl">Orienting</label>
                <div class="fr">
                    <%  var nEUOrienting = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUOrienting", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUOrienting", nEUOrienting, new { @id = Model.Type + "_NEUOrienting" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUBabinski" class="fl">Babinski&#8217;s</label>
                <div class="fr">
                    <%  var nEUBabinski = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUBabinski", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUBabinski", nEUBabinski, new { @id = Model.Type + "_NEUBabinski" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUBlinking" class="fl">Blinking</label>
                <div class="fr">
                    <%  var nEUBlinking = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUBlinking", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUBlinking", nEUBlinking, new { @id = Model.Type + "_NEUBlinking" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUPalmar" class="fl">Palmar</label>
                <div class="fr">
                    <%  var nEUPalmar = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUPalmar", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUPalmar", nEUPalmar, new { @id = Model.Type + "_NEUPalmar" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_ReflexOther" class="fl">Other(list with results)</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_ReflexOther", data.AnswerOrEmptyString("ReflexOther"), new { @id = Model.Type + "_ReflexOther" })%></div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_NeurologicalOrient0' name='{0}_NeurologicalOrient' value='0' type='checkbox' {1} />", Model.Type, neurologicalOrient.Contains("0").ToChecked())%>
                            <label for="<%= Model.Type %>_NeurologicalOrient0">Oriented</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <label for="<%= Model.Type %>_NeurologicalOrientContent" calss="fl">x</label>
                                <%= Html.TextBox(Model.Type + "_NeurologicalOrientContent", data.AnswerOrEmptyString("NeurologicalOrientContent"), new { @id = Model.Type + "_NeurologicalOrientContent" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.Type + "_NeurologicalOrient", "1", neurologicalOrient.Contains("1"), "Disoriented") %>
                </ul>
            </div>
            <div class="row">
                <label class="al">Cognitive development problems</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_NeurologicalCognitive", "0", neurologicalCognitive.Contains("0"), "Concepts") %>
                    <%= Html.CheckgroupOption(Model.Type + "_NeurologicalCognitive", "1", neurologicalCognitive.Contains("1"), "Logic") %>
                    <%= Html.CheckgroupOption(Model.Type + "_NeurologicalCognitive", "2", neurologicalCognitive.Contains("2"), "Impaired decision-making ability") %>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_NeurologicalCognitive3' name='{0}_NeurologicalCognitive' value='3' type='checkbox' {1} />", Model.Type, neurologicalCognitive.Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_NeurologicalCognitive3">Memory loss</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var memoryLossLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Short term", Value = "Short term" },
                                        new SelectListItem { Text = "Long term", Value = "Long term" }
                                    }, "Value", "Text", data.AnswerOrDefault("MemoryLossLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_MemoryLossLevel", memoryLossLevel, new { @id = Model.Type + "_MemoryLossLevel" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_NeurologicalCognitive4' name='{0}_NeurologicalCognitive' value='4' type='checkbox' {1} />", Model.Type, neurologicalCognitive.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_NeurologicalCognitive4">Stuporous/Hallucinations</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var stuporousLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Visual", Value = "Visual" },
                                            new SelectListItem { Text = "Auditory", Value = "Auditory" }
                                        }, "Value", "Text", data.AnswerOrDefault("StuporousLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_StuporousLevel", stuporousLevel, new { @id = Model.Type + "_StuporousLevel" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_NeurologicalCognitive5' name='{0}_NeurologicalCognitive' value='5' type='checkbox' {1} />", Model.Type, neurologicalCognitive.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_NeurologicalCognitive5">Headache</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HeadacheLocation">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HeadacheLocation", data.AnswerOrEmptyString("HeadacheLocation"), new { @id = Model.Type + "_HeadacheLocation" })%></div>
                            <div class="clr"></div>
                            <label for="<%= Model.Type %>_HeadacheFreq">Freq</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HeadacheFreq", data.AnswerOrEmptyString("HeadacheFreq"), new { @id = Model.Type + "_HeadacheFreq" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label class="al">Infant motor skills</label>
                <% string[] infantMotor = data.AnswerArray("InfantMotor"); %>
                <%= Html.Hidden(Model.Type + "_InfantMotor", string.Empty, new { @id = Model.Type + "_InfantMotor" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_InfantMotor", "0", infantMotor.Contains("0"), "Lifts head")%>
                    <%= Html.CheckgroupOption(Model.Type + "_InfantMotor", "1", infantMotor.Contains("1"), "Crawls/creeps")%>
                </ul>
            </div>
            <div class="row">
                <label class="al">Rolls over</label>
                <%  string[] rollsOver = data.AnswerArray("RollsOver"); %>
                <%= Html.Hidden(Model.Type + "_RollsOver", string.Empty, new { @id = Model.Type + "_RollsOver" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_RollsOver", "0", rollsOver.Contains("0"), "Stomach to back")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RollsOver", "1", rollsOver.Contains("1"), "Back to stomach")%>
                </ul>
            </div>
            <div class="row">
                <label class="al">Sits</label>
                <%  string[] sits = data.AnswerArray("Sits"); %>
                <%= Html.Hidden(Model.Type + "_Sits", string.Empty, new { @id = Model.Type + "_Sits" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_Sits", "0", sits.Contains("0"), "With assistance")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Sits", "1", sits.Contains("1"), "Without assistance")%>
                </ul>
            </div>
            <div class="row">
                <label class="al">Stands</label>
                <%  string[] stands = data.AnswerArray("Stands"); %>
                <%= Html.Hidden(Model.Type + "_Stands", string.Empty, new { @id = Model.Type + "_Stands" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_Stands", "0", stands.Contains("0"), "With assistance")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Stands", "1", stands.Contains("1"), "Without assistance")%>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_NEUPlantar" class="fl">Plantar</label>
                <div class="fr">
                    <%  var nEUPlantar = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUPlantar", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUPlantar", nEUPlantar, new { @id = Model.Type + "_NEUPlantar", @class = "shortOption" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUSteppingDancing" class="fl">Stepping/Dancing</label>
                <div class="fr">
                    <%  var nEUSteppingDancing = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUSteppingDancing", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUSteppingDancing", nEUSteppingDancing, new { @id = Model.Type + "_NEUSteppingDancing", @class = "shortOption" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUMoros" class="fl">Moro&#8217;s/Startle</label>
                <div class="fr">
                    <%  var nEUMoros = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUMoros", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUMoros", nEUMoros, new { @id = Model.Type + "_NEUMoros", @class = "shortOption" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUTonicneck" class="fl">Tonic neck</label>
                <div class="fr">
                    <%  var nEUTonicneck = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUTonicneck", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUTonicneck", nEUTonicneck, new { @id = Model.Type + "_NEUTonicneck", @class = "shortOption" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_NEUKneejerk" class="fl">Knee jerk</label>
                <div class="fr">
                    <%  var nEUKneejerk = new SelectList(neSelectItems, "Value", "Text", data.AnswerOrDefault("NEUKneejerk", ""));%>
                    <%= Html.DropDownList(Model.Type + "_NEUKneejerk", nEUKneejerk, new { @id = Model.Type + "_NEUKneejerk", @class = "shortOption" })%>
                </div>
            </div>
            <div class="row">
                <label class="al">Motor Skills</label>
                <%  string[] motorSkills = data.AnswerArray("MotorSkills"); %>
                <%= Html.Hidden(Model.Type + "_MotorSkills", string.Empty, new { @id = Model.Type + "_MotorSkills" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "0", motorSkills.Contains("0"), "Walks")%>
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "1", motorSkills.Contains("1"), "Runs")%>
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "2", motorSkills.Contains("2"), "Jumps")%>
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "3", motorSkills.Contains("3"), "Hops")%>
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "4", motorSkills.Contains("4"), "Skips")%>
                    <%= Html.CheckgroupOption(Model.Type + "_MotorSkills", "5", motorSkills.Contains("5"), "Balance")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_MotorSkills6' name='{0}_MotorSkills' value='6' type='checkbox' {1} />", Model.Type, motorSkills.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_MotorSkills6">Motor change</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var motorChangeLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Fine", Value = "Fine" },
                                            new SelectListItem { Text = "Gross", Value = "Gross" }
                                        }, "Value", "Text", data.AnswerOrDefault("MotorChangeLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_MotorChangeLevel", motorChangeLevel, new { @id = Model.Type + "_MotorChangeLevel" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_MotorSkills7' name='{0}_MotorSkills' value='7' type='checkbox' {1} />", Model.Type, motorSkills.Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_MotorSkills7">Tremors</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var tremorsLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Fine", Value = "Fine" },
                                            new SelectListItem { Text = "Gross", Value = "Gross" },
                                            new SelectListItem { Text = "Paralysis", Value = "Paralysis" }
                                        }, "Value", "Text", data.AnswerOrDefault("TremorsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_TremorsLevel", tremorsLevel, new { @id = Model.Type + "_TremorsLevel" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_MotorSkills8' name='{0}_MotorSkills' value='8' type='checkbox' {1} />", Model.Type, motorSkills.Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_MotorSkills8">Weakness</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var weaknessLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "UE", Value = "UE" },
                                        new SelectListItem { Text = "LE", Value = "LE" }
                                    }, "Value", "Text", data.AnswerOrDefault("WeaknessLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_WeaknessLevel", weaknessLevel, new { @id = Model.Type + "_WeaknessLevel", @class = "short" })%>
                                <label for="<%= Model.Type %>_WeaknessLocation">Location</label>
                                <%= Html.TextBox(Model.Type + "_WeaknessLocation", data.AnswerOrEmptyString("WeaknessLocation"), new { @id = Model.Type + "_WeaknessLocation", @class = "short" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label class="fl">Hand grips</label>
            </div>
            <div class=" sub row">
                <div class="fr">
                    <%  var handGripsLevel = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Equal", Value = "Equal" },
                                new SelectListItem { Text = "Unequal", Value = "Unequal" }
                            }, "Value", "Text", data.AnswerOrDefault("HandGripsLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_HandGripsLevel", handGripsLevel, new { @id = Model.Type + "_HandGripsLevel", @class = "short" })%>
                    <label for="<%= Model.Type %>_HandGripsSpecify1">Specify</label>
                    <%= Html.TextBox(Model.Type + "_HandGripsSpecify1", data.AnswerOrEmptyString("HandGripsSpecify1"), new { @id = Model.Type + "_HandGripsSpecify1", @class = "short"})%>
                </div>
                <div class="clr"></div>
            </div>
            <div class="sub row">
                <div class="fr">
                    <%  var handGripsLevel2 = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Strong", Value = "Strong" },
                                new SelectListItem { Text = "Weak", Value = "Weak" }
                            }, "Value", "Text", data.AnswerOrDefault("HandGripsLevel2", ""));%>
                    <%= Html.DropDownList(Model.Type + "_HandGripsLevel2", handGripsLevel2, new { @id = Model.Type + "_HandGripsLevel2", @class = "short" })%>
                    <label for="<%= Model.Type %>_HandGripsSpecify2">Specify</label>
                    <%= Html.TextBox(Model.Type + "_HandGripsSpecify2", data.AnswerOrEmptyString("HandGripsSpecify2"), new { @id = Model.Type + "_HandGripsSpecify2", @class = "short"})%>
                </div>
                <div class="clr"></div>
            </div>
            <div class="row">
                <%  string[] handGrips = data.AnswerArray("HandGrips"); %>
                <%= Html.Hidden(Model.Type + "_HandGrips", string.Empty, new { @id = Model.Type + "_HandGrips" })%>
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_HandGrips0' name='{0}_HandGrips' value='0' type='checkbox' {1} />", Model.Type, handGrips.Contains("0").ToChecked())%>
                            <label for="<%= Model.Type %>_HandGrips0">Sensory loss</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HandGripsSpecify3" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HandGripsSpecify3", data.AnswerOrEmptyString("HandGripsSpecify3"), new { @id = Model.Type + "_HandGripsSpecify3" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_HandGrips1' name='{0}_HandGrips' value='1' type='checkbox' {1} />", Model.Type, handGrips.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_HandGrips1">Numbness</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HandGripsSpecify4" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HandGripsSpecify4", data.AnswerOrEmptyString("HandGripsSpecify4"), new { @id = Model.Type + "_HandGripsSpecify4" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label class="fl">Communication patterns/Ability</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_CommunicationPatternsDescription", data.AnswerOrEmptyString("CommunicationPatternsDescription"), new { @id = Model.Type + "_CommunicationPatternsDescription"})%>
                </div>
            </div>
            <div class="row">
                <%  string[] communicationPatterns = data.AnswerArray("CommunicationPatterns"); %>
                <%= Html.Hidden(Model.Type + "_CommunicationPatterns", string.Empty, new { @id = Model.Type + "_CommunicationPatterns" })%>
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_CommunicationPatterns0' name='{0}_CommunicationPatterns' value='0' type='checkbox' {1} />", Model.Type, communicationPatterns.Contains("0").ToChecked())%>
                            <label for="<%= Model.Type %>_CommunicationPatterns0">Unequal pupils</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var unequalPupilLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "R", Value = "R" },
                                            new SelectListItem { Text = "L", Value = "L" },
                                            new SelectListItem { Text = "Perrla", Value = "Perrla" }
                                        }, "Value", "Text", data.AnswerOrDefault("UnequalPupilLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_UnequalPupilLevel", unequalPupilLevel, new { @id = Model.Type + "_UnequalPupilLevel" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_CommunicationPatterns1' name='{0}_CommunicationPatterns' value='1' type='checkbox' {1} />", Model.Type, communicationPatterns.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_CommunicationPatterns1">Psychotropic drug use</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_PsychotropicDrugUse" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_PsychotropicDrugUse", data.AnswerOrEmptyString("PsychotropicDrugUse"), new { @id = Model.Type + "_PsychotropicDrugUse" })%></div>
                            <div class="clr"></div>
                            <label for="<%= Model.Type %>_PsychotropicDrugUseUnit" class="fl">Dose/Freq</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_PsychotropicDrugUseUnit", data.AnswerOrEmptyString("PsychotropicDrugUseUnit"), new { @id = Model.Type + "_PsychotropicDrugUseUnit" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_CommunicationPatterns2' name='{0}_CommunicationPatterns' value='2' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("CommunicationPatterns").Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_CommunicationPatterns2">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_CommunicationPatternsOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_CommunicationPatternsOther", data.AnswerOrEmptyString("CommunicationPatternsOther"), new { @id = Model.Type + "_CommunicationPatternsOther" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</fieldset>