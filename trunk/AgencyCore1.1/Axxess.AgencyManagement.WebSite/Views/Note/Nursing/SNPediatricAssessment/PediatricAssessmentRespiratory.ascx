﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] respiratoryBreath = data.AnswerArray("RespiratoryBreath"); %>
<%  string[] respiratorySkin = data.AnswerArray("RespiratorySkin"); %>
<%= Html.Hidden(Model.Type + "_RespiratoryBreath", string.Empty, new { @id = Model.Type + "_RespiratoryBreath" })%>
<%= Html.Hidden(Model.Type + "_RespiratorySkin", string.Empty, new { @id = Model.Type + "_RespiratorySkin" })%>
<fieldset>
    <legend>Respiratory</legend>
    <div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsRespiratoryApply", string.Empty, new { @id = Model.Type + "IsRespiratoryApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsRespiratoryApply", Model.Type + "IsRespiratoryApply", "1", data.AnswerArray("IsRespiratoryApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ChestCircumference" class="fl">Chest circumference</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_ChestCircumference", data.AnswerOrEmptyString("ChestCircumference"), new { @id = Model.Type + "_ChestCircumference" })%></div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%  string[] respiratoryChest = data.AnswerArray("RespiratoryChest"); %>
                    <%= Html.Hidden(Model.Type + "_RespiratoryChest", string.Empty, new { @id = Model.Type + "_RespiratoryChest" })%>
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChest", "0", respiratoryChest.Contains("0"), "Retractions")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChest", "1", respiratoryChest.Contains("1"), "Dyspnea")%>
                </ul>
            </div>
            <div class="row">
                <label class="al">Breath sounds</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryBreath", "0", respiratoryBreath.Contains("0"), "Clear")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryBreath", "1", respiratoryBreath.Contains("1"), "Crackles")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryBreath", "2", respiratoryBreath.Contains("2"), "Wheeze")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryBreath", "3", respiratoryBreath.Contains("3"), "Absent")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratoryBreath4' name='{0}_RespiratoryBreath' value='4' type='checkbox' {1} />", Model.Type, respiratoryBreath.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratoryBreath4">Cough</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var genericCoughLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Dry", Value = "Dry" },
                                            new SelectListItem { Text = "Acute", Value = "Acute" },
                                            new SelectListItem { Text = "Chronic", Value = "Chronic" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryCough", "")); %>
                                <%= Html.DropDownList(Model.Type + "_RespiratoryCough", genericCoughLevel, new { @id = Model.Type + "_RespiratoryCough" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratoryBreath5' name='{0}_RespiratoryBreath' value='5' type='checkbox' {1} />", Model.Type, respiratoryBreath.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratoryBreath5">Productive</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var genericProductiveLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Thick", Value = "Thick" },
                                            new SelectListItem { Text = "Thin", Value = "Thin" },
                                            new SelectListItem { Text = "Difficult", Value = "Difficult" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryProductive", ""));%>
                                <%= Html.DropDownList(Model.Type + "_RespiratoryProductive", genericProductiveLevel, new { @id = Model.Type + "_RespiratoryProductive", @class = "short" })%>
                                <label for="<%= Model.Type %>_RespiratoryProductiveColor">Color</label>
                                <%= Html.TextBox(Model.Type + "_RespiratoryProductiveColor", data.AnswerOrEmptyString("RespiratoryProductiveColor"), new { @id = Model.Type + "_RespiratoryProductiveColor", @class = "short"})%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label class="al">Skin</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_RespiratoryBreath", "0", respiratorySkin.Contains("0"), "Temp. Change")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin1' name='{0}_RespiratorySkin' value='1' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin1">Color change</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_RespiratorySkinSpecify" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_RespiratorySkinSpecify", data.AnswerOrEmptyString("RespiratorySkinSpecify"), new { @id = Model.Type + "_RespiratorySkinSpecify" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin2' name='{0}_RespiratorySkin' value='2' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin2">Percussion</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%  var genericPercussionLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Resonant", Value = "Resonant" },
                                            new SelectListItem { Text = "Tympanic", Value = "Tympanic" },
                                            new SelectListItem { Text = "Dull", Value = "Dull" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryPercussion", ""));%>
                                <%= Html.DropDownList(Model.Type + "_RespiratoryPercussion", genericPercussionLevel, new { @id = Model.Type + "_RespiratoryPercussion" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin3' name='{0}_RespiratorySkin' value='3' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin3">Chart lobe</label>
                        </div>
                        <div class="more">
                            <ul class="checkgroup five-wide">
                                <%  string[] respiratoryChart = data.AnswerArray("RespiratoryChart"); %>
                                <%= Html.Hidden(Model.Type + "_RespiratoryChart", string.Empty, new { @id = Model.Type + "_RespiratoryChart" })%>
                                <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChart", "0", respiratoryChart.Contains("0"), "R")%>
                                <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChart", "1", respiratoryChart.Contains("1"), "L")%>
                                <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChart", "2", respiratoryChart.Contains("2"), "Lat.")%>
                                <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChart", "3", respiratoryChart.Contains("3"), "Ant.")%>
                                <%= Html.CheckgroupOption(Model.Type + "_RespiratoryChart", "4", respiratoryChart.Contains("4"), "Post.")%>
                            </ul>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin4' name='{0}_RespiratorySkin' value='4' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin4">O2 Sat.</label>
                        </div>
                        <div class="more">
                            <div class="fr"><%= Html.TextBox(Model.Type + "_RespiratoryO2Sat", data.AnswerOrEmptyString("RespiratoryO2Sat"), new { @id = Model.Type + "_RespiratoryO2Sat" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin5' name='{0}_RespiratorySkin' value='5' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin5">O2 Use</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_RespiratoryO2Use", data.AnswerOrEmptyString("RespiratoryO2Use"), new { @id = Model.Type + "_RespiratoryO2Use", @class = "numeric short" })%>
                                <%  var genericO2UseLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Mask", Value = "Mask" },
                                            new SelectListItem { Text = "Nasal", Value = "Nasal" },
                                            new SelectListItem { Text = "Trach", Value = "Trach" },
                                            new SelectListItem { Text = "Gas", Value = "Gas" },
                                            new SelectListItem { Text = "Liquid", Value = "Liquid" },
                                            new SelectListItem { Text = "Concentrator", Value = "Concentrator" }
                                        }, "Value", "Text", data.AnswerOrDefault("RespiratoryO2UseLevel", ""));%>
                                <label for="<%= Model.Type %>_RespiratoryO2Use">L/min. by</label>
                                <%= Html.DropDownList(Model.Type + "_RespiratoryO2UseLevel", genericO2UseLevel, new { @id = Model.Type + "_RespiratoryO2UseLevel", @class = "short" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_RespiratorySkin6' name='{0}_RespiratorySkin' value='6' type='checkbox' {1} />", Model.Type, respiratorySkin.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_RespiratorySkin6">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_RespiratoryO2Use" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_RespiratoryOther", data.AnswerOrEmptyString("RespiratoryOther"), new { @id = Model.Type + "_RespiratoryOther" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</fieldset>
