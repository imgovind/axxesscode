﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Pain</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsPainApply", string.Empty, new { @id = Model.Type + "IsPainApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsPainApply", Model.Type + "IsPainApply", "1", data.AnswerArray("IsPainApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
           <label for="<%= Model.Type %>_PainOrigin" class="fl">Origin</label>
           <div class="fr">
           <%= Html.TextBox(Model.Type + "_PainOrigin", data.AnswerOrEmptyString("PainOrigin"), new { @id = Model.Type + "_PainOrigin"})%>
           </div>
        </div>
        <div class="row">
           <label for="<%= Model.Type %>_PainOnset" class="fl">Onset</label>
           <div class="fr">
           <%= Html.TextBox(Model.Type + "_PainOnset", data.AnswerOrEmptyString("PainOnset"), new { @id = Model.Type + "_PainOnset"})%>
           </div>
        </div>
        <div class="row">
           <label for="<%= Model.Type %>_PainLocation" class="fl">Location</label>
           <div class = "fr">
           <%= Html.TextBox(Model.Type + "_PainLocation", data.AnswerOrEmptyString("PainLocation"), new { @id = Model.Type + "_PainLocation"})%>
           </div>
        </div>
        <div class="row">
           <label  for="<%= Model.Type %>_PainQuality" class="fl">Quality</label>
           <div class = "fr">
           <%= Html.TextBox(Model.Type + "_PainQuality", data.AnswerOrEmptyString("PainQuality"), new { @id = Model.Type + "_PainQuality"})%>
           </div>
        </div>
        <div class="row">
           <label for="<%= Model.Type %>_GenericPainLevel" class="fl">Intensity</label>
           <div class = "fr">
            <%  var genericPainLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "0 = No Pain", Value = "None" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "Moderate" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", ""));%>
                    <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel" }) %>
           </div>
        </div>
       <div class="row">
                <img src="/Images/painscale.png" alt="Pain Scale Image" width="100%" /><br />
                <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
        </div>
        <div class="row">
           <label  for="<%= Model.Type %>_PainHistory" class="fl">Pain management history</label>
           <div class = "fr">
           <%= Html.TextBox(Model.Type + "_PainHistory", data.AnswerOrEmptyString("PainHistory"), new { @id = Model.Type + "_PainHistory"})%>
           </div>
        </div>
        <div class="row">
           <label  for="<%= Model.Type %>_PainRegimen" class="fl">Present pain management regimen</label>
           <div class = "fr">
           <%= Html.TextBox(Model.Type + "_PainRegimen", data.AnswerOrEmptyString("PainRegimen"), new { @id = Model.Type + "_PainRegimen"})%>
           </div>
        </div>
        <div class="row">
           <label  for="<%= Model.Type %>_PainEffective" class="fl">Effectiveness</label>
           <div class = "fr">
          <%= Html.TextBox(Model.Type + "_PainEffective", data.AnswerOrEmptyString("PainEffective"), new { @id = Model.Type + "_PainEffective"})%>
           </div>
        </div>
        <div class="row">
           <label  for="<%= Model.Type %>_PainOther" class="fl">Other</label>
           <div class = "fr">
           <%= Html.TextBox(Model.Type + "_PainOther", data.AnswerOrEmptyString("PainOther"), new { @id = Model.Type + "_PainOther"})%>
           </div>
        </div>  
    </div>
  </div>
</fieldset>
