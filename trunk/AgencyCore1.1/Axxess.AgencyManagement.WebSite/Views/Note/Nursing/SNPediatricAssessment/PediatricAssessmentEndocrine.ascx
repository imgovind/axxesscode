﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] endocrine = data.AnswerArray("Endocrine"); %>
<%= Html.Hidden(Model.Type + "_Endocrine", string.Empty, new { @id = Model.Type + "_Endocrine" })%>
<fieldset>
    <legend>Endocrine</legend>
    <div class="column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.Hidden(Model.Type + "_IsEndocrineApply", string.Empty, new { @id = Model.Type + "IsEndocrineApplyHidden" })%>
                <%= Html.CheckgroupOption(Model.Type + "_IsEndocrineApply", Model.Type + "IsEndocrineApply", "1", data.AnswerArray("IsEndocrineApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
            </ul>
        </div>
    </div>
    <div class="collapsible-container">
        <div class="column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine0' name='{0}_Endocrine' value='0' type='checkbox' {1} />", Model.Type, endocrine.Contains("0").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine0">Hypothyroidism</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine1' name='{0}_Endocrine' value='1' type='checkbox' {1} />", Model.Type, endocrine.Contains("1").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine1">Hyperthyroidism</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine2' name='{0}_Endocrine' value='2' type='checkbox' {1} />", Model.Type, endocrine.Contains("2").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine2">Fatigue</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine3' name='{0}_Endocrine' value='3' type='checkbox' {1} />", Model.Type, endocrine.Contains("3").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine3">Intolerance to heat/cold</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine4' name='{0}_Endocrine' value='4' type='checkbox' {1} />", Model.Type, endocrine.Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine4">Diabetes</label>
                        </div>
                        <div class="more" id="<%= Model.Type %>_Endocrine4More">
                            <label for="EndocrineDiabetesDate" class="fl">Onset</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineDiabetesDate", data.AnswerOrEmptyString("EndocrineDiabetesDate"), new { @id = Model.Type + "_EndocrineDiabetesDate", @class = "date-picker", @maxlength = "50" })%></div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine5' name='{0}_Endocrine' value='5' type='checkbox' {1} />", Model.Type, endocrine.Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine5">Diet/Oral control</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineDietControl", data.AnswerOrEmptyString("EndocrineDietControl"), new { @id = Model.Type + "_EndocrineDietControl", @class = "numeric short" })%>
                                <%  var dateUnitLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Months", Value = "months" },
                                            new SelectListItem { Text = "Years", Value = "Years" }
                                        }, "Value", "Text", data.AnswerOrDefault("DateUnitLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_DateUnitLevel", dateUnitLevel, new { @id = Model.Type + "_DateUnitLevel", @class = "short" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine6' name='{0}_Endocrine' value='6' type='checkbox' {1} />", Model.Type, endocrine.Contains("6").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine6">Med/dose/freq</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineMed", data.AnswerOrEmptyString("EndocrineMed"), new { @id = Model.Type + "_EndocrineMed", @maxlength = "30" })%></div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine7' name='{0}_Endocrine' value='7' type='checkbox' {1} />", Model.Type, endocrine.Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine7">Insulin/dose/freq</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineInsulin", data.AnswerOrEmptyString("EndocrineInsulin"), new { @id = Model.Type + "_EndocrineInsulin", @maxlength = "30" })%></div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine8' name='{0}_Endocrine' value='8' type='checkbox' {1} />", Model.Type, endocrine.Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine8">Hyperglycemia</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine9' name='{0}_Endocrine' value='9' type='checkbox' {1} />", Model.Type, endocrine.Contains("9").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine9">Hypoglycemia</label>
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine10' name='{0}_Endocrine' value='10' type='checkbox' {1} />", Model.Type, endocrine.Contains("10").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine10">Blood sugar range</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineBlood", data.AnswerOrEmptyString("EndocrineBlood"), new { @id = Model.Type + "_EndocrineBlood", @maxlength = "30" })%></div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine11' name='{0}_Endocrine' value='11' type='checkbox' {1} />", Model.Type, endocrine.Contains("11").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine11">Self-care/self observational tasks</label>
                        </div>
                        <div class="more">
                            <label for="EndocrineSelf" class="fl">Specify</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineSelf", data.AnswerOrEmptyString("EndocrineSelf"), new { @id = Model.Type + "_EndocrineSelf", @maxlength = "30" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                    <div class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Endocrine12' name='{0}_Endocrine' value='12' type='checkbox' {1} />", Model.Type, endocrine.Contains("12").ToChecked())%>
                            <label for="<%= Model.Type %>_Endocrine12">Other</label>
                        </div>
                        <div class="more">
                            <label for="EndocrineOther" class="fl">Specify</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_EndocrineOther", data.AnswerOrEmptyString("EndocrineOther"), new { @id = Model.Type + "_EndocrineOther", @maxlength = "30" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</fieldset>
