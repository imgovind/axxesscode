﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var teachingSelectItems = new[] {
            new SelectListItem { Text = "------", Value = "" },
            new SelectListItem { Text = "I - Instruct", Value = "I" },
            new SelectListItem { Text = "R - Reinstruct ", Value = "R" },
            new SelectListItem { Text = "D - Demonstrate", Value = "D" },
            new SelectListItem { Text = "U - Understands Instructions", Value = "U" }};%>
<fieldset>
    <legend>Teaching/Training</legend>
    <div class="wide-column">
        <div class="row ac">
            <em>I-Instruct; R-Reinstruct; D-Demonstrate; U-Understands Instructions</em>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_DietLevel" class="fl">Diet</label>
            <div class="fr">
                <%  var dietLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("DietLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_DietLevel", dietLevel, new { @id = Model.Type + "_DietLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_DietResponse", data.AnswerOrEmptyString("DietResponse"), new { @id = Model.Type + "_DietResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_MedicationLevel" class="fl">Medication(s)</label>
            <div class="fr">
                <%  var MedicationLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("MedicationLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_MedicationLevel", MedicationLevel, new { @id = Model.Type + "_MedicationLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_MedicationResponse", data.AnswerOrEmptyString("MedicationResponse"), new { @id = Model.Type + "_MedicationResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_DiseaseLevel" class="fl">Disease process</label>
            <div class="fr">
                <%  var DiseaseLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("DiseaseLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_DiseaseLevel", DiseaseLevel, new { @id = Model.Type + "_DiseaseLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_DiseaseResponse", data.AnswerOrEmptyString("DiseaseResponse"), new { @id = Model.Type + "_DiseaseResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_SafetyLevel" class="fl">Safety</label>
            <div class="fr">
                <%  var SafetyLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("SafetyLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_SafetyLevel", SafetyLevel, new { @id = Model.Type + "_SafetyLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_SafetyResponse", data.AnswerOrEmptyString("SafetyResponse"), new { @id = Model.Type + "_SafetyResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_SSLevel" class="fl">S/S to report</label>
            <div class="fr">
                <%  var SSLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("SSLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_SSLevel", SSLevel, new { @id = Model.Type + "_SSLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_SSResponse", data.AnswerOrEmptyString("SSResponse"), new { @id = Model.Type + "_SSResponse", @class = "short" })%>
            </div>
        </div>
        </div>
        <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_WellLevel" class="fl">Well child care</label>
            <div class="fr">
                <%  var WellLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("WellLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_WellLevel", WellLevel, new { @id = Model.Type + "_WellLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_WellResponse", data.AnswerOrEmptyString("WellResponse"), new { @id = Model.Type + "_WellResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_ActivitiesLevel" class="fl">Activities</label>
            <div class="fr">
                <%  var ActivitiesLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("ActivitiesLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_ActivitiesLevel", ActivitiesLevel, new { @id = Model.Type + "_ActivitiesLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_ActivitiesResponse", data.AnswerOrEmptyString("ActivitiesResponse"), new { @id = Model.Type + "_ActivitiesResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_TreatmentsLevel" class="fl">Treatments</label>
            <div class="fr">
                <%  var TreatmentsLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("TreatmentsLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_TreatmentsLevel", TreatmentsLevel, new { @id = Model.Type + "_TreatmentsLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_TreatmentsResponse", data.AnswerOrEmptyString("TreatmentsResponse"), new { @id = Model.Type + "_TreatmentsResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_InjuryLevel" class="fl">Injury prevention</label>
            <div class="fr">
                <%  var InjuryLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("InjuryLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_InjuryLevel", InjuryLevel, new { @id = Model.Type + "_InjuryLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_InjuryResponse", data.AnswerOrEmptyString("InjuryResponse"), new { @id = Model.Type + "_InjuryResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GrowthLevel" class="fl">Growth</label>
            <div class="fr">
                <%  var GrowthLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("GrowthLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_GrowthLevel", GrowthLevel, new { @id = Model.Type + "_GrowthLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_GrowthResponse", data.AnswerOrEmptyString("GrowthResponse"), new { @id = Model.Type + "_GrowthResponse", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <div class="fl">
                <%= Html.TextBox(Model.Type + "_TeachingOther", data.AnswerOrEmptyString("TeachingOther"), new { @id = Model.Type + "_TeachingOther", @class = "short" })%>
           </div>
            <div class="fr">
                <%  var OtherLevel = new SelectList(teachingSelectItems, "Value", "Text", data.AnswerOrDefault("OtherLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_OtherLevel", OtherLevel, new { @id = Model.Type + "_OtherLevel", @class = "shortOption" })%>
                <%= Html.TextBox(Model.Type + "_OtherResponse", data.AnswerOrEmptyString("OtherResponse"), new { @id = Model.Type + "_OtherResponse", @class = "short" })%>
            </div>
        </div>
    </div>
</fieldset>
