﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var hnpSelectItems = new[] {
    new SelectListItem { Text = "-- Select Status --", Value = "" },
    new SelectListItem { Text = "H - History Of", Value = "0" },
    new SelectListItem { Text = "N - Negative", Value = "1" },
    new SelectListItem { Text = "P - Present Problem", Value = "2" }}; %>
<fieldset>
    <legend>Childhood History</legend>
    <div class="wide-column">
        <div class="row ac">
            <em>H-history of; N-negative; P-present problem</em>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_CHThrush" class="fl">Thrush</label>
            <div class="fr">
                <%  var cHThrush = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHThrush", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHThrush", cHThrush, new { @id = Model.Type + "_CHThrush" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHApnea" class="fl">Apnea</label>
            <div class="fr">
                <%  var cHApnea = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHApnea", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHApnea", cHApnea, new { @id = Model.Type + "_CHApnea" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHConjunctivitis" class="fl">Conjunctivitis</label>
            <div class="fr">
                <%  var cHConjunctivitis = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHConjunctivitis", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHConjunctivitis", cHConjunctivitis, new { @id = Model.Type + "_CHConjunctivitis" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHCroup" class="fl">Croup</label>
            <div class="fr">
                <%  var cHCroup = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHCroup", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHCroup", cHCroup, new { @id = Model.Type + "_CHCroup" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHPica" class="fl">Pica</label>
            <div class="fr">
                <%  var cHPica = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHPica", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHPica", cHPica, new { @id = Model.Type + "_CHPica" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHRubella" class="fl">Rubella</label>
            <div class="fr">
                <%  var cHRubella = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHRubella", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHRubella", cHRubella, new { @id = Model.Type + "_CHRubella" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHRubeola" class="fl">Rubeola</label>
            <div class="fr">
                <%  var cHRubeola = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHRubeola", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHRubeola", cHRubeola, new { @id = Model.Type + "_CHRubeola" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHScarlet" class="fl">Scarlet Fever</label>
            <div class="fr">
                <%  var cHScarlet = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHScarlet", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHScarlet", cHScarlet, new { @id = Model.Type + "_CHScarlet" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHMumps" class="fl">Mumps</label>
            <div class="fr">
                <%  var cHMumps = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHMumps", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHMumps", cHMumps, new { @id = Model.Type + "_CHMumps" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHChickenpox" class="fl">Chickenpox</label>
            <div class="fr">
                <%  var cHChickenpox = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHChickenpox", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHChickenpox", cHChickenpox, new { @id = Model.Type + "_CHChickenpox" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHHepatitis" class="fl">Hepatitis</label>
            <div class="fr">
                <%  var cHHepatitis = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHHepatitis", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHHepatitis", cHHepatitis, new { @id = Model.Type + "_CHHepatitis" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHSickleCell" class="fl">Sickle Cell</label>
            <div class="fr">
                <%  var cHSickleCell = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHSickleCell", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHSickleCell", cHSickleCell, new { @id = Model.Type + "_CHSickleCell" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHLeadPoisoning" class="fl">Lead Poisoning</label>
            <div class="fr">
                <%  var cHLeadPoisoning = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHLeadPoisoning", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHLeadPoisoning", cHLeadPoisoning, new { @id = Model.Type + "_CHLeadPoisoning" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHHIV" class="fl">HIV</label>
            <div class="fr">
                <%  var cHHIV = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHHIV", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHHIV", cHHIV, new { @id = Model.Type + "_CHHIV" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHPneumonia" class="fl">Pneumonia</label>
            <div class="fr">
                <%  var cHPneumonia = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHPneumonia", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHPneumonia", cHPneumonia, new { @id = Model.Type + "_CHPneumonia" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHAsthma" class="fl">Asthma</label>
            <div class="fr">
                <%  var cHAsthma = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHAsthma", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHAsthma", cHAsthma, new { @id = Model.Type + "_CHAsthma" })%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_CHStrep" class="fl">Strep throat</label>
            <div class="fr">
                <%  var cHStrep = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHStrep", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHStrep", cHStrep, new { @id = Model.Type + "_CHStrep" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHSinusitis" class="fl">Sinusitis</label>
            <div class="fr">
                <%  var cHSinusitis = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHSinusitis", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHSinusitis", cHSinusitis, new { @id = Model.Type + "_CHSinusitis" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHNosebleeds" class="fl">Nosebleeds</label>
            <div class="fr">
                <%  var cHNosebleeds = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHNosebleeds", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHNosebleeds", cHNosebleeds, new { @id = Model.Type + "_CHNosebleeds" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHFractures" class="fl">Fracture(s)</label>
            <div class="fr">
                <%  var cHFractures = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHFractures", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHFractures", cHFractures, new { @id = Model.Type + "_CHFractures" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHBurns" class="fl">Burn(s)</label>
            <div class="fr">
                <%  var cHBurns = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHBurns", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHBurns", cHBurns, new { @id = Model.Type + "_CHBurns" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHOtitismedia" class="fl">Otitis media</label>
            <div class="fr">
                <%  var cHOtitismedia = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHOtitismedia", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHOtitismedia", cHOtitismedia, new { @id = Model.Type + "_CHOtitismedia" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHFrequentEar" class="fl">Frequent ear infection</label>
            <div class="fr">
                <%  var cHFrequentEar = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHFrequentEar", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHFrequentEar", cHFrequentEar, new { @id = Model.Type + "_CHFrequentEar" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHTonsillitis" class="fl">Tonsillitis</label>
            <div class="fr">
                <%  var cHTonsillitis = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHTonsillitis", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHTonsillitis", cHTonsillitis, new { @id = Model.Type + "_CHTonsillitis" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHFrequentSore" class="fl">Frequent sore throat</label>
            <div class="fr">
                <%  var cHFrequentSore = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHFrequentSore", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHFrequentSore", cHFrequentSore, new { @id = Model.Type + "_CHFrequentSore" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHBleedingProblems" class="fl">Bleeding problems</label>
            <div class="fr">
                <%  var cHBleedingProblems = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHBleedingProblems", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHBleedingProblems", cHBleedingProblems, new { @id = Model.Type + "_CHBleedingProblems" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHRheumaticFever" class="fl">Rheumatic fever</label>
            <div class="fr">
                <%  var cHRheumaticFever = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHRheumaticFever", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHRheumaticFever", cHRheumaticFever, new { @id = Model.Type + "_CHRheumaticFever" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHHeadaches" class="fl">Headaches</label>
            <div class="fr">
                <%  var cHHeadaches = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHHeadaches", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHHeadaches", cHHeadaches, new { @id = Model.Type + "_CHHeadaches" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHSeizuresGrand" class="fl">Seizures-grand mal</label>
            <div class="fr">
                <%  var cHSeizuresGrand = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHSeizuresGrand", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHSeizuresGrand", cHSeizuresGrand, new { @id = Model.Type + "_CHSeizuresGrand" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHSeizuresPetit" class="fl">Seizures-petit mal</label>
            <div class="fr">
                <%  var cHSeizuresPetit = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHSeizuresPetit", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHSeizuresPetit", cHSeizuresPetit, new { @id = Model.Type + "_CHSeizuresPetit" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_CHFrequentColds" class="fl">Frequent colds</label>
            <div class="fr">
                <%  var cHFrequentColds = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHFrequentColds", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHFrequentColds", cHFrequentColds, new { @id = Model.Type + "_CHFrequentColds" })%>
            </div>
        </div>
        <div class="row">
            <div class="fl">
                <label for="<%= Model.Type %>_ChildhoodHistoryOther">Other</label>
                <%= Html.TextBox(Model.Type + "_ChildhoodHistoryOther", data.AnswerOrEmptyString("ChildhoodHistoryOther"), new { @id = Model.Type + "_ChildhoodHistoryOther", @class = "short" })%></div>
            <div class="fr">
                <%  var cHOther = new SelectList(hnpSelectItems, "Value", "Text", data.AnswerOrDefault("CHOther", ""));%>
                <%= Html.DropDownList(Model.Type + "_CHOther", cHOther, new { @id = Model.Type + "_CHOther" })%>
            </div>
        </div>
    </div>
</fieldset>
