﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Nose/Throat/Mouth</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsNoseApply", string.Empty, new { @id = Model.Type + "IsNoseApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsNoseApply", Model.Type + "IsNoseApply", "1", data.AnswerArray("IsNoseApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup two-wide">
                <% string[] nose = data.AnswerArray("Nose"); %>
                <%= Html.Hidden(Model.Type + "_Nose", string.Empty, new { @id = Model.Type + "_Nose" })%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "0", nose.Contains("0"), "Congestio")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "1", nose.Contains("1"), "Dysphagla")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "2", nose.Contains("2"), "Hoarseness")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "3", nose.Contains("3"), "Lesions")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "4", nose.Contains("4"), "Sore throat")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "5", nose.Contains("5"), "Masses/Tumors")%>
                <%= Html.CheckgroupOption(Model.Type + "_Nose", "6", nose.Contains("6"), "Palate intact")%>
            </div>
        </div>
        <span class="row">
            
        </span>
        <div class="row">
            <label class="fl">Teeth present</label>
            <div class="fr">
                <div class="checkgroup two-wide">
                    <% string[] teeth = data.AnswerArray("Teeth"); %>
                    <%= Html.Hidden(Model.Type + "_Teeth", string.Empty, new { @id = Model.Type + "_Teeth" })%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_Teeth", "0", teeth.Contains("0"), "Yes")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_Teeth", "1", teeth.Contains("1"), "No")%>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_OralHygiene" class="fl">Oral hygiene practices</label>
            <%= Html.TextBox(Model.Type + "_OralHygiene", data.AnswerOrEmptyString("OralHygiene"), new { @id = Model.Type + "_OralHygiene", @class = "fr" })%>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_DentistVisit" class="fl">Dentist visits:frequency</label>
            <%= Html.TextBox(Model.Type + "_DentistVisit", data.AnswerOrEmptyString("DentistVisit"), new { @id = Model.Type + "_DentistVisit", @class = "fr" })%>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_NoseOther" class="fl">Other</label>
            <%= Html.TextBox(Model.Type + "_NoseOther", data.AnswerOrEmptyString("NoseOther"), new { @id = Model.Type + "_NoseOther", @class = "fr" })%>
        </div>
    </div>
    </div>
</fieldset>
