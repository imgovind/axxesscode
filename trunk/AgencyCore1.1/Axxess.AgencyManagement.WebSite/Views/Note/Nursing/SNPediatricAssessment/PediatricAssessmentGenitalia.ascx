﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Genitalia</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsGenitaliaApply", string.Empty, new { @id = Model.Type + "IsGenitaliaApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsGenitaliaApply", Model.Type + "IsGenitaliaApply", "1", data.AnswerArray("IsGenitaliaApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaCircumcised0' name='{0}_GenitaliaCircumcised' value='0' type='checkBox' {1} />", Model.Type, data.AnswerOrEmptyString("GenitaliaCircumcised").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaCircumcised0">Circumcised</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaCircumcised1' name='{0}_GenitaliaCircumcised' value='1' type='checkBox' {1} />", Model.Type, data.AnswerOrEmptyString("GenitaliaCircumcised").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaCircumcised1">Uncircumcised</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="al">Scrotum</label>
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaScrotum0' name='{0}_GenitaliaScrotum' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaScrotum").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaScrotum0">WNL</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaScrotum1' name='{0}_GenitaliaScrotum' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaScrotum").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaScrotum1">Swollen</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="al">Testes</label>
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaTest0' name='{0}_GenitaliaTest' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaTest").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaTest0">Descended</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenitaliaTest1' name='{0}_GenitaliaTest' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("GenitaliaTest").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenitaliaTest1">Undescended</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup three-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_TestUndescended0' name='{0}_TestUndescended' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("0").ToChecked())%>
                                    <label for="<%= Model.Type %>_TestUndescended0">Right</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_TestUndescended1' name='{0}_TestUndescended' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("1").ToChecked())%>
                                    <label for="<%= Model.Type %>_TestUndescended1">Left</label>
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_TestUndescended2' name='{0}_TestUndescended' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("TestUndescended").Contains("2").ToChecked())%>
                                    <label for="<%= Model.Type %>_TestUndescended2">Bilateral</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Puberty0' name='{0}_Puberty' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Puberty").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Puberty0">Puberty</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Puberty1' name='{0}_Puberty' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Puberty").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Puberty1">Menarche</label>
                    </div>
                    <div class="more">
                        <div>
                            <label for="<%= Model.Type %>_MenarcheAge" class="fl">Age</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_MenarcheAge", data.AnswerOrEmptyString("MenarcheAge"), new { @id = Model.Type + "_MenarcheAge"})%>
                            </div>
                            <div class="clear" />
                        </div>
                        <div>
                            <label for="<%= Model.Type %>_MenarcheLMP" class="fl">LMP</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_MenarcheLMP", data.AnswerOrEmptyString("MenarcheLMP"), new { @id = Model.Type + "_MenarcheLMP"})%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="fl">Pregnancy</label>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_PregnancyGravida" class="fl">Gravida</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_PregnancyGravida", data.AnswerOrEmptyString("PregnancyGravida"), new { @id = Model.Type + "_PregnancyGravida"})%>
            </div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_PregnancyPara" class="fl">Para</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_PregnancyPara", data.AnswerOrEmptyString("PregnancyPara"), new { @id = Model.Type + "_PregnancyPara"})%>
            </div>
        </div>
        <div class="sub row">
            <label for="<%= Model.Type %>_PregnancyEDC" class="fl">EDC</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_PregnancyEDC", data.AnswerOrEmptyString("PregnancyEDC"), new { @id = Model.Type + "_PregnancyEDC"})%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenitaliaOther" class="fl">Other</label>
            <div class="ac">
                <%= Html.TextArea(Model.Type + "_GenitaliaOther", data.AnswerOrEmptyString("GenitaliaOther"), new { @id = Model.Type + "_GenitaliaOther" })%>
            </div>
        </div>
    </div>
   </div>
</fieldset>
