﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Newborn/Infant</legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_NewbornScreenResults" class="fl">Newborn screen results</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_NewbornScreenResults", data.AnswerOrEmptyString("NewbornScreenResults"), new { @id = Model.Type + "_NewbornScreenResults" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GestationalAge" class="fl">Gestational age at birth</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GestationalAge", data.AnswerOrEmptyString("GestationalAge"), new { @id = Model.Type + "_GestationalAge", @class = "numeric shorter" })%>
                <label>weeks</label></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_BirthWeight" class="fl">Birth wt.</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_BirthWeight", data.AnswerOrEmptyString("BirthWeight"), new { @id = Model.Type + "_BirthWeight", @class = "numeric shorter" })%>
                <label>lb.</label>
                <%= Html.TextBox(Model.Type + "_BirthOz", data.AnswerOrEmptyString("BirthOz"), new { @id = Model.Type + "_BirthOz", @class = "numeric shorter" })%>
                <label>oz.</label></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_BirthLength" class="fl">Length</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_BirthLength", data.AnswerOrEmptyString("BirthLength"), new { @id = Model.Type + "_BirthLength", @class = "numeric shorter" })%>
                <label>in.</label></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_HeadCircumference" class="fl">Head circumference</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_HeadCircumference", data.AnswerOrEmptyString("HeadCircumference"), new { @id = Model.Type + "_HeadCircumference" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="al">Fontanels</label>
            <div class="checkgroup two-wide">
                <% string[] fontanels = data.AnswerArray("Fontanels"); %>
                <%= Html.Hidden(Model.Type + "_Fontanels", string.Empty, new { @id = Model.Type + "_Fontanels" })%>
                <%= Html.CheckgroupOption(Model.Type + "_Fontanels", "0", fontanels.Contains("0"), "Anterior")%>
                <%= Html.CheckgroupOption(Model.Type + "_Fontanels", "1", fontanels.Contains("1"), "Posterior")%>
            </div>
        </div>
        <div class="row">
            <label class="al">Umbilicus</label>
            <div class="checkgroup two-wide">
                <% string[] umbilicus = data.AnswerArray("Umbilicus"); %>
                <%= Html.Hidden(Model.Type + "_Umbilicus", string.Empty, new { @id = Model.Type + "_Umbilicus" })%>
                <%= Html.CheckgroupOption(Model.Type + "_Umbilicus", "0", umbilicus.Contains("0"), "Healed")%>
                <%= Html.CheckgroupOption(Model.Type + "_Umbilicus", "1", umbilicus.Contains("1"), "Hernia")%>
                <%= Html.CheckgroupOption(Model.Type + "_Umbilicus", "2", umbilicus.Contains("2"), "Inverted")%>
                <%= Html.CheckgroupOption(Model.Type + "_Umbilicus", "3", umbilicus.Contains("3"), "Everted")%>
            </div>
        </div>
    </div>
</fieldset>
