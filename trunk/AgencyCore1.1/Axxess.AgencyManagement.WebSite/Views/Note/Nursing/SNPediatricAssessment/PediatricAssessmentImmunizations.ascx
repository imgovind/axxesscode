﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] immunizations = data.AnswerArray("Immunizations"); %>
<%= Html.Hidden(Model.Type + "_Immunizations", string.Empty, new { @id = Model.Type + "_Immunizations" })%>
<fieldset>
    <legend>Immunizations</legend>
    <div class="column">
        <div class="row">
            <div class="checkgroup three-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations0' name='{0}_Immunizations' value='0' type='checkbox' {1} />", Model.Type, immunizations.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations0">DPT</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations1' name='{0}_Immunizations' value='1' type='checkbox' {1} />", Model.Type, immunizations.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations1">Measles</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations2' name='{0}_Immunizations' value='2' type='checkbox' {1} />", Model.Type, immunizations.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations2">Polio</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations3' name='{0}_Immunizations' value='3' type='checkbox' {1} />", Model.Type, immunizations.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations3">DT</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations4' name='{0}_Immunizations' value='4' type='checkbox' {1} />", Model.Type, immunizations.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations4">Mumps</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations5' name='{0}_Immunizations' value='5' type='checkbox' {1} />", Model.Type, immunizations.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations5">HBV</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations6' name='{0}_Immunizations' value='6' type='checkbox' {1} />", Model.Type, immunizations.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations6">MMR</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations7' name='{0}_Immunizations' value='7' type='checkbox' {1} />", Model.Type, immunizations.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations7">Rubella</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations8' name='{0}_Immunizations' value='8' type='checkbox' {1} />", Model.Type, immunizations.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations8">Hib</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Immunizations9' name='{0}_Immunizations' value='9' type='checkbox' {1} />", Model.Type, immunizations.Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_Immunizations9">Other</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_ImmunizationsOther" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_ImmunizationsOther", data.AnswerOrEmptyString("ImmunizationsOther"), new { @id = Model.Type + "_ImmunizationsOther" })%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
