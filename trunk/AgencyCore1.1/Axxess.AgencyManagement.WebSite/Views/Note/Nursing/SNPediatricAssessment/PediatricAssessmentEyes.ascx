﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Eyes</legend>
    <div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsEyesApply", string.Empty, new { @id = Model.Type + "IsEyesApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsEyesApply", Model.Type + "IsEyesApply", "1", data.AnswerArray("IsEyesApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
           <ul class="checkgroup two-wide">
                <% string[] eyes = data.AnswerArray("Eyes"); %>
                <%= Html.Hidden(Model.Type + "_Eyes", string.Empty, new { @id = Model.Type + "_Eyes" })%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "0", eyes.Contains("0"), "Glasses")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "CR", eyes.Contains("CR"), "Contacts R")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "CL", eyes.Contains("CL"), "Contacts L")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "PR", eyes.Contains("PR"), "Prosthesis R")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "PL", eyes.Contains("PL"), "Prosthesis L")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "3", eyes.Contains("3"), "Jaundice")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "4", eyes.Contains("4"), "Blurred vision")%>
                <%= Html.CheckgroupOption(Model.Type + "_Eyes", "5", eyes.Contains("5"), "Legally blind")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_Eyes", "6", eyes.Contains("6"), "Infections", Model.Type + "_EyesInfections", data.AnswerOrEmptyString("EyesInfections"))%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_Eyes", "7", eyes.Contains("7"), "Other", Model.Type + "_EyesOther", data.AnswerOrEmptyString("EyesOther"))%>
            </ul>
        </div>
    </div>
    </div>
</fieldset>
