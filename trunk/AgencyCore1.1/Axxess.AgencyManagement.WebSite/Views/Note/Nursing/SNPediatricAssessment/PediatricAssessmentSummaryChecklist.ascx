﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Summary checklist</legend>
    <div class="column">
        <div class="row">
            <label class="al">Medication status</label>
            <div class="checkgroup one-wide">
                <% string[] medicationStatus = data.AnswerArray("MedicationStatus"); %>
                <%= Html.Hidden(Model.Type + "_MedicationStatus", string.Empty, new { @id = Model.Type + "_MedicationStatus" })%>
                <%= Html.CheckgroupOption(Model.Type + "_MedicationStatus", "0", medicationStatus.Contains("0"), "Medication regimen completed/reviewed")%>
            </div>
        </div>
        <div class="row">
            <label class="al">Check if any of the following were identified</label>
            <div class="checkgroup one-wide">
                <% string[] summaryChecklist = data.AnswerArray("SummaryChecklist"); %>
                <%= Html.Hidden(Model.Type + "_SummaryChecklist", string.Empty, new { @id = Model.Type + "_SummaryChecklist" })%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "0", summaryChecklist.Contains("0"), "Potential adverse effects/drug reactions")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "1", summaryChecklist.Contains("1"), "Ineffective drug therapy")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "2", summaryChecklist.Contains("2"), "Significant side effects")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "3", summaryChecklist.Contains("3"), "Significant drug interactions")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "4", summaryChecklist.Contains("4"), "Duplicate drug therapy")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "5", summaryChecklist.Contains("5"), "Non-compliance with drug therapy")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "6", summaryChecklist.Contains("6"), "No change")%>
                <%= Html.CheckgroupOption(Model.Type + "_SummaryChecklist", "7", summaryChecklist.Contains("7"), "Order obtained")%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="al">Billable supplies recorded?</label>
            <div class="fr">
                <div class="checkgroup two-wide">
                    <% string[] billableSupplies = data.AnswerArray("BillableSupplies"); %>
                    <%= Html.Hidden(Model.Type + "_BillableSupplies", string.Empty, new { @id = Model.Type + "_BillableSupplies" })%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_BillableSupplies", "0", billableSupplies.Contains("0"), "Yes")%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_BillableSupplies", "1", billableSupplies.Contains("1"), "No")%>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="al">Care coordination</label>
            <div class="checkgroup one-wide">
                <% string[] careCoordination = data.AnswerArray("CareCoordination"); %>
                <%= Html.Hidden(Model.Type + "_CareCoordination", string.Empty, new { @id = Model.Type + "_CareCoordination" })%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "0", careCoordination.Contains("0"), "Physician")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "1", careCoordination.Contains("1"), "PT")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "2", careCoordination.Contains("2"), "OT")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "3", careCoordination.Contains("3"), "ST")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "4", careCoordination.Contains("4"), "SS")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "5", careCoordination.Contains("5"), "SN")%>
                <%= Html.CheckgroupOption(Model.Type + "_CareCoordination", "6", careCoordination.Contains("6"), "Aide")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CareCoordination", "7", careCoordination.Contains("7"), "Other", Model.Type + "_CareCoordiOther", data.AnswerOrEmptyString("CareCoordiOther"))%>
            </div>
        </div>
    </div>
</fieldset>
