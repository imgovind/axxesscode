﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Appliances</legend>
    <div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsAppliancesApply", string.Empty, new { @id = Model.Type + "IsAppliancesApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsAppliancesApply", Model.Type + "IsAppliancesApply", "1", data.AnswerArray("IsAppliancesApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
        <div class="column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "0", data.AnswerArray("Appliances").Contains("0"), "Crutch(es)")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "1", data.AnswerArray("Appliances").Contains("1"), "Wheelchair")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "2", data.AnswerArray("Appliances").Contains("2"), "Cane")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "3", data.AnswerArray("Appliances").Contains("3"), "Walker")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances4' name='{0}_Appliances' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("4").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances4">Brace/Orthotics</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_AppliancesBrace" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_AppliancesBrace", data.AnswerOrEmptyString("AppliancesBrace"), new { @id = Model.Type + "_AppliancesBrace"})%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances5' name='{0}_Appliances' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("5").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances5">Transfer equipment</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <label for="<%= Model.Type %>_TransferEquipmentLevel" class="fl">Level</label>
                                <%  var transferEquipmentLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Board", Value = "Board" },
                                            new SelectListItem { Text = "Lift", Value = "Lift" }
                                        }, "Value", "Text", data.AnswerOrDefault("TransferEquipmentLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_TransferEquipmentLevel", transferEquipmentLevel, new { @id = Model.Type + "_TransferEquipmentLevel" })%>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "6", data.AnswerArray("Appliances").Contains("6"), "Bedside commode")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances7' name='{0}_Appliances' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("7").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances7">Prosthesis</label>
                        </div>
                        <div class="more">
                            <div class="fr">
                                <label for="<%= Model.Type %>_ProsthesisLevel" class="fl">Level</label>
                                <%  var prosthesisLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "RUE", Value = "RUE" },
                                        new SelectListItem { Text = "RLE", Value = "RLE" },
                                        new SelectListItem { Text = "LUE", Value = "LUE" },
                                        new SelectListItem { Text = "LLE", Value = "LLE" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("ProsthesisLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ProsthesisLevel", prosthesisLevel, new { @id = Model.Type + "_ProsthesisLevel", @class = "short" })%>
                                <%= Html.TextBox(Model.Type + "_ProsthesisOther", data.AnswerOrEmptyString("ProsthesisOther"), new { @id = Model.Type + "_ProsthesisOther", @class = "short" })%>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances8' name='{0}_Appliances' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("8").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances8">Grab bars</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_GrabbarsLevel" class="fl">Level</label>
                            <div class="fr">
                                <%  var grabbarsLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Bathroom", Value = "Bathroom" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("GrabbarsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_GrabbarsLevel", grabbarsLevel, new { @id = Model.Type + "_GrabbarsLevel", @class = "short" })%>
                                <%= Html.TextBox(Model.Type + "_GrabbarsOther", data.AnswerOrEmptyString("GrabbarsOther"), new { @id = Model.Type + "_GrabbarsOther", @class = "short" })%>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances9' name='{0}_Appliances' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("9").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances9">Hospital bed</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HospitalBedLevel" class="fl">Level</label>
                            <div class="fr">
                                <%  var hospitalBedLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "Semi-elec", Value = "Semi-elec" },
                                        new SelectListItem { Text = "Crank", Value = "Crank" },
                                        new SelectListItem { Text = "Other", Value = "Other" }
                                    }, "Value", "Text", data.AnswerOrDefault("HospitalBedLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_HospitalBedLevel", hospitalBedLevel, new { @id = Model.Type + "_HospitalBedLevel", @class = "short" })%>
                                <%= Html.TextBox(Model.Type + "_HospitalBedOther", data.AnswerOrEmptyString("HospitalBedOther"), new { @id = Model.Type + "_HospitalBedOther", @class = "short" })%>
                            </div>
                            <div class="clear"></div>
                            <label for="<%= Model.Type %>_Overlays" class="fl">Overlays</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_Overlays", data.AnswerOrEmptyString("Overlays"), new { @id = Model.Type + "_Overlays"})%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances10' name='{0}_Appliances' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("10").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances10">Oxygen</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_HMECo" class="fl">HME Co.</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HMECo", data.AnswerOrEmptyString("HMECo"), new { @id = Model.Type + "_HMECo"})%></div>
                            <div class="clr"></div>
                            <label for="<%= Model.Type %>_HMERep" class="fl">HME Rep.</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HMERep", data.AnswerOrEmptyString("HMERep"), new { @id = Model.Type + "_HMERep"})%></div>
                            <div class="clr"></div>
                            <label for="<%= Model.Type %>_HMEPhone" class="fl">HME Phone</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_HMEPhone", data.AnswerOrEmptyString("HMEPhone"), new { @id = Model.Type + "_HMEPhone"})%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "11", data.AnswerArray("Appliances").Contains("11"), "Fire Alarm")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Appliances", "12", data.AnswerArray("Appliances").Contains("12"), "Smoke Alarm")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances13' name='{0}_Appliances' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("13").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances13">Equipment needs</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_EquipmentSpecify" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_EquipmentSpecify", data.AnswerOrEmptyString("EquipmentSpecify"), new { @id = Model.Type + "_EquipmentSpecify"})%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_Appliances14' name='{0}_Appliances' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("Appliances").Contains("14").ToChecked())%>
                            <label for="<%= Model.Type %>_Appliances14">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.Type %>_EquipmentOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.Type + "_EquipmentOther", data.AnswerOrEmptyString("EquipmentOther"),new { @id = Model.Type + "_EquipmentOther" })%></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
   </div>
</fieldset>
