﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] gastrointestinalEatting = data.AnswerArray("GastrointestinalEatting"); %>
<%= Html.Hidden(Model.Type + "_GastrointestinalEatting", string.Empty, new { @id = Model.Type + "_GastrointestinalEatting" })%>
<% string[] gastrointestinalAppetite = data.AnswerArray("GastrointestinalAppetite"); %>
<%= Html.Hidden(Model.Type + "_GastrointestinalAppetite", string.Empty, new { @id = Model.Type + "_GastrointestinalAppetite" })%>
<% string[] gastrointestinal = data.AnswerArray("Gastrointestinal"); %>
<%= Html.Hidden(Model.Type + "_Gastrointestinal", string.Empty, new { @id = Model.Type + "_Gastrointestinal" })%>
<fieldset>
    <legend>Gastrointestinal</legend>
    <div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsGastrointestinalApply", string.Empty, new { @id = Model.Type + "IsGastrointestinalApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsGastrointestinalApply", Model.Type + "IsGastrointestinalApply", "1", data.AnswerArray("IsGastrointestinalApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GastrointestinalRequirements" class="fl">Nutritional requirements for age (diet)</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GastrointestinalRequirements", data.AnswerOrEmptyString("GastrointestinalRequirements"), new { @id = Model.Type + "_GastrointestinalRequirements"})%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GastrointestinalMealPattern" class="fl">Meal patterns</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GastrointestinalMealPattern", data.AnswerOrEmptyString("GastrointestinalMealPattern"), new { @id = Model.Type + "_GastrointestinalMealPattern"})%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GastrointestinalEatting" class="fl">Eatting behaviors</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GastrointestinalEattingBehaviors", data.AnswerOrEmptyString("GastrointestinalEattingBehaviors"), new { @id = Model.Type + "_GastrointestinalEattingBehaviors" })%></div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GastrointestinalEatting0' name='{0}_GastrointestinalEatting' value='0' type='checkbox' {1} />", Model.Type, gastrointestinalEatting.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GastrointestinalEatting0">Eatting disorder</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup three-wide">
                            <% string[] gastrointestinalEattingDisorder = data.AnswerArray("GastrointestinalEattingDisorder"); %>
                            <%= Html.Hidden(Model.Type + "_GastrointestinalEattingDisorder", string.Empty, new { @id = Model.Type + "_GastrointestinalEattingDisorder" })%>
                            <%= Html.CheckgroupOption(Model.Type + "_GastrointestinalEattingDisorder", "0", gastrointestinalEattingDisorder.Contains("0"), "Anorexia")%>
                            <%= Html.CheckgroupOption(Model.Type + "_GastrointestinalEattingDisorder", "1", gastrointestinalEattingDisorder.Contains("1"), "Bulimia")%>
                            <%= Html.CheckgroupOptionWithOther(Model.Type + "_GastrointestinalEattingDisorder", "2", gastrointestinalEattingDisorder.Contains("2"), "Other", Model.Type + "_EattingDisorderOther", data.AnswerOrEmptyString("EattingDisorderOther"))%>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_AppetiteLevel" class="fl">Appetite</label>
            <div class="fr">
                <%  var appetiteLevel = new SelectList(new[] {
                                new SelectListItem { Text = "------", Value = "" },
                                new SelectListItem { Text = "Good", Value = "Good" },
                                new SelectListItem { Text = "Fair", Value = "Fair" },
                                new SelectListItem { Text = "Poor", Value = "Poor" }
                            }, "Value", "Text", data.AnswerOrDefault("AppetiteLevel", ""));%>
                <%= Html.DropDownList(Model.Type + "_AppetiteLevel", appetiteLevel, new { @id = Model.Type + "_AppetiteLevel" })%>
            </div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GastrointestinalAppetite0' name='{0}_GastrointestinalAppetite' value='0' type='checkbox' {1} />", Model.Type, gastrointestinalAppetite.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GastrointestinalAppetite0">Weight change</label>
                    </div>
                    <div class="more">
                        <div class="fr">
                            <%  var weightChangeLevel = new SelectList(new[] {
                                            new SelectListItem { Text = "------", Value = "" },
                                            new SelectListItem { Text = "Gain", Value = "Gain" },
                                            new SelectListItem { Text = "Loss", Value = "Loss" }
                                        }, "Value", "Text", data.AnswerOrDefault("WeightChangeLevel", ""));%>
                            <%= Html.DropDownList(Model.Type + "_WeightChangeLevel", weightChangeLevel, new { @id = Model.Type + "_WeightChangeLevel", @class = "shorter" })%>
                            <%= Html.TextBox(Model.Type + "_WeightChange", data.AnswerOrEmptyString("WeightChange"), new { @id = Model.Type + "_WeightChange", @class = "numeric shorter" })%>
                            <label for="<%= Model.Type %>_WeightChangeTime">lb./oz. x</label>
                            <%= Html.TextBox(Model.Type + "_WeightChangeTime", data.AnswerOrEmptyString("WeightChangeTime"), new { @id = Model.Type + "_WeightChangeTime", @class = "numeric shorter" })%>
                            <%  var weightChangeTimeLevel = new SelectList(new[] {
                                        new SelectListItem { Text = "------", Value = "" },
                                        new SelectListItem { Text = "wk", Value = "wk" },
                                        new SelectListItem { Text = "mo", Value = "mo" },
                                        new SelectListItem { Text = "yr", Value = "yr" }
                                    }, "Value", "Text", data.AnswerOrDefault("WeightChangeTimeLevel", ""));%>
                            <%= Html.DropDownList(Model.Type + "_WeightChangeTimeLevel", weightChangeTimeLevel, new { @id = Model.Type + "_WeightChangeTimeLevel", @class = "shorter" })%>
                            <div class="clear" />
                        </div>
                        <div class="clear" />
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GastrointestinalAppetite1' name='{0}_GastrointestinalAppetite' value='1' type='checkbox' {1} />", Model.Type, gastrointestinalAppetite.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GastrointestinalAppetite1">Increase fluids</label>
                    </div>
                    <div class="more">
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_IncreaseFluids", data.AnswerOrEmptyString("IncreaseFluids"), new { @id = Model.Type + "_IncreaseFluids", @class = "short" })%>
                            <label for="<%= Model.Type %>_IncreaseFluids" class="fr">amt</label>
                        </div>
                        <div class="clear" />
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GastrointestinalAppetite2' name='{0}_GastrointestinalAppetite' value='2' type='checkbox' {1} />", Model.Type, gastrointestinalAppetite.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_GastrointestinalAppetite2">Restrict fluids</label>
                    </div>
                    <div class="more">
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_RestrictFluids", data.AnswerOrEmptyString("RestrictFluids"), new { @id = Model.Type + "_RestrictFluids", @class = "short" })%>
                            <label for="<%= Model.Type %>_RestrictFluids">amt</label>
                        </div>
                        <div class="clear" />
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GastrointestinalAppetite3' name='{0}_GastrointestinalAppetite' value='3' type='checkbox' {1} />", Model.Type, gastrointestinalAppetite.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_GastrointestinalAppetite3">Nausea/Vomiting</label>
                    </div>
                    <div class="more">
                        <div>
                            <label for="<%= Model.Type %>_NauseaFrequency" class="fl">Freq</label>
                            <%= Html.TextBox(Model.Type + "_NauseaFrequency", data.AnswerOrEmptyString("NauseaFrequency"), new { @id = Model.Type + "_NauseaFrequency", @class = "fr" })%>
                        </div>
                        <div class="clear" />
                        <div>
                            <label for="<%= Model.Type %>_NauseaAmt" class="fl">amt</label>
                            <%= Html.TextBox(Model.Type + "_NauseaAmt", data.AnswerOrEmptyString("NauseaAmt"), new { @id = Model.Type + "_NauseaAmt", @class = "fr" })%>
                        </div>
                        <div class="clear" />
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_LastBMDate" class="fl">Last BM</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_LastBMDate", data.AnswerOrEmptyString("LastBMDate"), new { @id = Model.Type + "_LastBMDate", @class = "date-picker" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GastrointestinalFrequency" class="fl">Usual frequency</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GastrointestinalFrequency", data.AnswerOrEmptyString("GastrointestinalFrequency"), new { @id = Model.Type + "_GastrointestinalFrequency" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Gastrointestinal0' name='{0}_Gastrointestinal' value='0' type='checkbox' {1} />", Model.Type, gastrointestinal.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Gastrointestinal0">Diarrhea</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup two-wide">
                            <% string[] gastrointestinalDiarrhea = data.AnswerArray("GastrointestinalDiarrhea"); %>
                            <%= Html.Hidden(Model.Type + "_GastrointestinalDiarrhea", string.Empty, new { @id = Model.Type + "_GastrointestinalDiarrhea" })%>
                            <%= Html.CheckgroupOption(Model.Type + "_GastrointestinalDiarrhea", "0", gastrointestinalDiarrhea.Contains("0"), "Black/Watery")%>
                            <%= Html.CheckgroupOption(Model.Type + "_GastrointestinalDiarrhea", "1", gastrointestinalDiarrhea.Contains("1"), "Less than 3x/day")%>
                            <%= Html.CheckgroupOption(Model.Type + "_GastrointestinalDiarrhea", "2", gastrointestinalDiarrhea.Contains("2"), "More than 3x/day")%>
                        </div>
                        <div>
                            <div class="fr">
                                <%  var diarrheaLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Mucus", Value = "Mucus" },
                                    new SelectListItem { Text = "Pain", Value = "Pain" },
                                    new SelectListItem { Text = "Foul odor", Value = "Foul odor" },
                                    new SelectListItem { Text = "Frothy", Value = "Frothy" }
                                }, "Value", "Text", data.AnswerOrDefault("DiarrheaLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_DiarrheaLevel", diarrheaLevel, new { @id = Model.Type + "_DiarrheaLevel", @class = "short" })%>
                                <label for="<%= Model.Type %>_DiarrheaAmount">Amount</label>
                                <%= Html.TextBox(Model.Type + "_DiarrheaAmount", data.AnswerOrEmptyString("DiarrheaAmount"), new { @id = Model.Type + "_DiarrheaAmount", @class = "short" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Gastrointestinal1' name='{0}_Gastrointestinal' value='1' type='checkbox' {1} />", Model.Type, gastrointestinal.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Gastrointestinal1">Abnormal stools</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="fr">
                                <%  var abnormalStoolsLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Clay", Value = "Clay" },
                                    new SelectListItem { Text = "Tarry", Value = "Tarry" },
                                    new SelectListItem { Text = "Fresh blood", Value = "Fresh blood" }
                                }, "Value", "Text", data.AnswerOrDefault("AbnormalStoolsLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_AbnormalStoolsLevel", abnormalStoolsLevel, new { @id = Model.Type + "_AbnormalStoolsLevel", @class = "short" })%>
                                <label for="<%= Model.Type %>_AbnormalStoolsDescribe">Describe</label>
                                <%= Html.TextBox(Model.Type + "_AbnormalStoolsDescribe", data.AnswerOrEmptyString("AbnormalStoolsDescribe"), new { @id = Model.Type + "_AbnormalStoolsDescribe", @class = "short" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Gastrointestinal2' name='{0}_Gastrointestinal' value='2' type='checkbox' {1} />", Model.Type, gastrointestinal.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Gastrointestinal2">Constipation</label>
                    </div>
                    <div class="more">
                        <div>
                            <div class="fr">
                                <%  var constipationLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Chronic", Value = "Chronic" },
                                    new SelectListItem { Text = "Acute", Value = "Acute" },
                                    new SelectListItem { Text = "Occasional", Value = "Occasional" }
                                }, "Value", "Text", data.AnswerOrDefault("ConstipationLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_ConstipationLevel", constipationLevel, new { @id = Model.Type + "_ConstipationLevel" })%>
                            </div>
                        </div>
                        <div class="clear" />
                        <div class="checkgroup one-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <% string[] constipation = data.AnswerArray("Constipation"); %>
                                    <%= Html.Hidden(Model.Type + "_Constipation", string.Empty, new { @id = Model.Type + "_Constipation" })%>
                                    <%= string.Format("<input id='{0}_Constipation0' name='{0}_Constipation' value='0' type='checkbox' {1} />", Model.Type, constipation.Contains("0").ToChecked())%>
                                    <label for="<%= Model.Type %>_Constipation0">Lax/enema use</label>
                                </div>
                                <div class="more">
                                    <div>
                                        <label for="<%= Model.Type %>_EnemaType" class="fl">Type</label>
                                        <%= Html.TextBox(Model.Type + "_EnemaType", data.AnswerOrEmptyString("EnemaType"), new { @id = Model.Type + "_EnemaType", @class = "fr" })%>
                                    </div>
                                    <div class="clear" />
                                    <div>
                                        <label for="<%= Model.Type %>_EnemaFreq" class="fl">Freq</label>
                                        <%= Html.TextBox(Model.Type + "_EnemaFreq", data.AnswerOrEmptyString("EnemaFreq"), new { @id = Model.Type + "_EnemaFreq", @class = "fr" })%>
                                    </div>
                                    <div class="clear" />
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Gastrointestinal3' name='{0}_Gastrointestinal' value='3' type='checkbox' {1} />", Model.Type, gastrointestinal.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_Gastrointestinal3">Flatulence</label>
                    </div>
                    <div class="more">
                        <div>
                            <label for="<%= Model.Type %>_AbdominalDistentionLevel" class="fl">Abdominal distention</label>
                            <div class="fr">
                                <%  var abdominalDistentionLevel = new SelectList(new[] {
                                    new SelectListItem { Text = "------", Value = "" },
                                    new SelectListItem { Text = "Cramping", Value = "Cramping" },
                                    new SelectListItem { Text = "Pain", Value = "Pain" }
                                }, "Value", "Text", data.AnswerOrDefault("AbdominalDistentionLevel", ""));%>
                                <%= Html.DropDownList(Model.Type + "_AbdominalDistentionLevel", abdominalDistentionLevel, new { @id = Model.Type + "_AbdominalDistentionLevel" })%>
                            </div>
                            <div class="clear" />
                        </div>
                        <div>
                            <label for="<%= Model.Type %>_AbdominalDistentionFreq" class="fl">Freq</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_AbdominalDistentionFreq", data.AnswerOrEmptyString("AbdominalDistentionFreq"), new { @id = Model.Type + "_AbdominalDistentionFreq" })%>
                            </div>
                            <div class="clear" />
                        </div>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Gastrointestinal4' name='{0}_Gastrointestinal' value='4' type='checkbox' {1} />", Model.Type, gastrointestinal.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_Gastrointestinal4">Impaction</label>
                    </div>
                    <div class="more">
                        <div class="checkgroup one-wide">
                            <div class="option">
                                <div class="wrapper">
                                    <% string[] gastrointestinalImpaction = data.AnswerArray("GastrointestinalImpaction"); %>
                                    <%= Html.Hidden(Model.Type + "_GastrointestinalImpaction", string.Empty, new { @id = Model.Type + "_GastrointestinalImpaction" })%>
                                    <%= string.Format("<input id='{0}_GastrointestinalImpaction0' name='{0}_GastrointestinalImpaction' value='0' type='checkbox' {1} />", Model.Type, gastrointestinalImpaction.Contains("0").ToChecked())%>
                                    <label for="<%= Model.Type %>_GastrointestinalImpaction0">Flatulence</label>
                                </div>
                                <div class="more">
                                    <label for="<%= Model.Type %>_FlatulenceFreq" class="fl">Freq</label>
                                    <div class="fr">
                                        <%= Html.TextBox(Model.Type + "_FlatulenceFreq", data.AnswerOrEmptyString("FlatulenceFreq"), new { @id = Model.Type + "_FlatulenceFreq" })%>
                                    </div>
                                    <div class="clear" />
                                </div>
                            </div>
                            <div class="option">
                                <div class="wrapper">
                                    <%= string.Format("<input id='{0}_GastrointestinalImpaction1' name='{0}_GastrointestinalImpaction' value='1' type='checkbox' {1} />", Model.Type, gastrointestinalImpaction.Contains("1").ToChecked())%>
                                    <label for="<%= Model.Type %>_GastrointestinalImpaction1">Ascites</label>
                                </div>
                                <div class="more">
                                    <div>
                                        <div class="fr">
                                            <label for="<%= Model.Type %>_AscitesGirth">Girth</label>
                                            <%= Html.TextBox(Model.Type + "_AscitesGirth", data.AnswerOrEmptyString("AscitesGirth"), new { @id = Model.Type + "_AscitesGirth", @class = "numeric shorter" })%>
                                            <label for="<%= Model.Type %>_AscitesGirth">inches</label>
                                            <%  var ascitesLevel = new SelectList(new[] {
                                                new SelectListItem { Text = "------", Value = "" },
                                                new SelectListItem { Text = "Firm", Value = "Firm" },
                                                new SelectListItem { Text = "Tender", Value = "Tender" }
                                            }, "Value", "Text", data.AnswerOrDefault("AscitesLevel", ""));%>
                                            <%= Html.DropDownList(Model.Type + "_AscitesLevel", ascitesLevel, new { @id = Model.Type + "_AscitesLevel", @class = "shorter" })%>
                                            <label for="<%= Model.Type %>_AscitesQuads">x</label>
                                            <%= Html.TextBox(Model.Type + "_AscitesQuads", data.AnswerOrEmptyString("AscitesQuads"), new { @id = Model.Type + "_AscitesQuads", @class = "numeric shorter" })%>
                                            <label for="<%= Model.Type %>_AscitesQuads">quads</label>
                                        </div>
                                        <div class="clear" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <div>
                <label for="<%= Model.Type %>_BowelSoundsLevel" class="fl">Bowel sounds</label>
                <div class="fr">
                    <%  var bowelSoundsLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "Active", Value = "Active" },
                            new SelectListItem { Text = "Hyperactive", Value = "Hyperactive" }
                        }, "Value", "Text", data.AnswerOrDefault("BowelSoundsLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_BowelSoundsLevel", bowelSoundsLevel, new { @id = Model.Type + "_BowelSoundsLevel", @class = "short" })%>
                    <label for="<%= Model.Type %>_BowelSoundsQuads">x</label>
                    <%= Html.TextBox(Model.Type + "_BowelSoundsQuads", data.AnswerOrEmptyString("BowelSoundsQuads"), new { @id = Model.Type + "_BowelSoundsQuads", @class = "numeric shorter" })%>
                    <label for="<%= Model.Type %>_BowelSoundsQuads">quads</label>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row">
            <div>
                <label for="<%= Model.Type %>_ColostomyLevel" class="fl">Colostomy</label>
                <div class="fr">
                    <%  var colostomyLevel = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "Sigmoid", Value = "Sigmoid" },
                        new SelectListItem { Text = "Transverse", Value = "Transverse" }
                    }, "Value", "Text", data.AnswerOrDefault("ColostomyLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_ColostomyLevel", colostomyLevel, new { @id = Model.Type + "_ColostomyLevel", @class = "short" })%>
                    <label for="<%= Model.Type %>_ColostomyDate">Date</label>
                    <%= Html.TextBox(Model.Type + "_ColostomyDate", data.AnswerOrEmptyString("ColostomyDate"), new { @id = Model.Type + "_ColostomyDate", @class = "date-picker" })%>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row">
            <div class="fr">
                <%  var colostomyLevel2 = new SelectList(new[] {
                        new SelectListItem { Text = "------", Value = "" },
                        new SelectListItem { Text = "Rebound", Value = "Rebound" },
                        new SelectListItem { Text = "Hot", Value = "Hot" },
                        new SelectListItem { Text = "Red", Value = "Red" },
                        new SelectListItem { Text = "Discolored", Value = "Discolored" }
                    }, "Value", "Text", data.AnswerOrDefault("ColostomyLevel2", ""));%>
                <%= Html.DropDownList(Model.Type + "_ColostomyLevel2", colostomyLevel2, new { @id = Model.Type + "_ColostomyLevel2" })%>
            </div>
        </div>
    </div>
  </div>
</fieldset>
