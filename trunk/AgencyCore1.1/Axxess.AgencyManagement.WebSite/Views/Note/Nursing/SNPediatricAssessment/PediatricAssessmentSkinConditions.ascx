﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Skin conditions/Wounds</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsWoundsApply", string.Empty, new { @id = Model.Type + "IsWoundsApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsWoundsApply", Model.Type + "IsWoundsApply", "1", data.AnswerArray("IsWoundsApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <label class="al">Check all that apply</label>
            <div class="checkgroup three-wide">
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound0' name='{0}_SkinWound' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound0">Mongolian spots</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound1' name='{0}_SkinWound' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound1">Itch</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound2' name='{0}_SkinWound' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound2">Rash</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound3' name='{0}_SkinWound' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound3">Dry</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound4' name='{0}_SkinWound' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound4">Scaling</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound5' name='{0}_SkinWound' value='5' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound5">Incision</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound6' name='{0}_SkinWound' value='6' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound6">Wounds</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound7' name='{0}_SkinWound' value='7' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound7">Lesions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound8' name='{0}_SkinWound' value='8' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound8">Sutures</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound9' name='{0}_SkinWound' value='9' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound9">Staples</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound10' name='{0}_SkinWound' value='10' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound10">Abrasions</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound11' name='{0}_SkinWound' value='11' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("11").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound11">Lacerations</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound12' name='{0}_SkinWound' value='12' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("12").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound12">Bruises</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound13' name='{0}_SkinWound' value='13' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("13").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound13">Ecchymosis</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound14' name='{0}_SkinWound' value='14' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("14").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound14">Edema</label>
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                         <%= string.Format("<input id='{0}_SkinWound15' name='{0}_SkinWound' value='15' type='checkbox' {1} />", Model.Type, data.AnswerArray("SkinWound").Contains("15").ToChecked())%>
                        <label for="<%= Model.Type %>_SkinWound15">Hemangiomas</label>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="row">
            <label for="<%= Model.Type %>_PallorLevel" class="fl">Pallor</label>
            <div class="fr">
               <%  var pallorLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "Jaundice", Value = "Jaundice" },
                            new SelectListItem { Text = "Redness", Value = "Redness" }
                        }, "Value", "Text", data.AnswerOrDefault("PallorLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_PallorLevel", pallorLevel, new { @id = Model.Type + "_PallorLevel" })%>
             </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_TurgorLevel" class="fl">Turgor</label>
            <div class="fr">
               <%  var turgorLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "Good", Value = "Good" },
                            new SelectListItem { Text = "Poor", Value = "Poor" }
                        }, "Value", "Text", data.AnswerOrDefault("TurgorLevel", ""));%>
                    <%= Html.DropDownList(Model.Type + "_TurgorLevel", turgorLevel, new { @id = Model.Type + "_TurgorLevel" })%>
             </div>
        </div>
         <div class="row">
            <label for="<%= Model.Type %>_SkinWoundOther" class="fl">Other</label>
            <div class="fr">
               <%= Html.TextBox(Model.Type + "_SkinWoundOther", data.AnswerOrEmptyString("SkinWoundOther"), new { @id = Model.Type + "_SkinWoundOther" })%>
             </div>
        </div>
    </div>
    </div>
</fieldset>
