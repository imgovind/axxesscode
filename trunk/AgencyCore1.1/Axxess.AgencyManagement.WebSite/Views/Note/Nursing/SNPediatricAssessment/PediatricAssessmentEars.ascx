﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Ears</legend>
    <div class="column">
	    <div class="row">
		    <ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsEarsApply", string.Empty, new { @id = Model.Type + "IsEarsApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsEarsApply", Model.Type + "IsEarsApply", "1", data.AnswerArray("IsEarsApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
        <div class="column">
            <div class="row">
                <ul class="checkgroup two-wide">
                    <% string[] ear = data.AnswerArray("Ear"); %>
                    <%= Html.Hidden(Model.Type + "_Ear", string.Empty, new { @id = Model.Type + "_Ear" })%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "HOHR", ear.Contains("HOHR"), "HOH R")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "HOHL", ear.Contains("HOHL"), "HOH L")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "DeafR", ear.Contains("DeafR"), "Deaf R")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "DeafL", ear.Contains("DeafL"), "Deaf L")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "AidR", ear.Contains("AidR"), "Hearing aid R")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "AidL", ear.Contains("AidL"), "Hearing aid L")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "3", ear.Contains("3"), "Vertigo")%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "4", ear.Contains("4"), "Tinnitus")%>
                </ul>
            </div>
            <div class="row">
                <label class="al">Infections</label>
                <ul class="checkgroup two-wide">
                    <% string[] earInfections = data.AnswerArray("EarInfections"); %>
                    <%= Html.Hidden(Model.Type + "_EarInfections", string.Empty, new { @id = Model.Type + "_EarInfections" })%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_EarInfections", "0", earInfections.Contains("0"),true, "Yes", Model.Type + "_EarInfectionsYes", data.AnswerOrEmptyString("EarInfectionsYes"))%>
                    <%= Html.CheckgroupRadioOption(Model.Type + "_EarInfections", "1", earInfections.Contains("1"), "No")%>
                </ul>
            </div>
            <div class="row">
                <ul class="checkgroup two-wide">
                    <%= Html.Hidden(Model.Type + "_Ear", string.Empty, new { @id = Model.Type + "_Ear" })%>
                    <%= Html.CheckgroupOption(Model.Type + "_Ear", "5", ear.Contains("5"), "P.E. tubes present")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_Ear", "6", ear.Contains("6"), "Other", Model.Type + "_EarOther", data.AnswerOrEmptyString("EarOther"))%>
                </ul>
            </div>
        </div>
    </div>
</fieldset>
