﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] head = data.AnswerArray("Head"); %>
<%= Html.Hidden(Model.Type + "_Head", string.Empty, new { @id = Model.Type + "_Head" })%>
<fieldset>
    <legend>Head/Neck</legend>
       <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsHeadApply", string.Empty, new { @id = Model.Type + "IsHeadApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsHeadApply", Model.Type + "IsHeadApply", "1", data.AnswerArray("IsHeadApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Head0' name='{0}_Head' value='0' type='checkbox' {1} />", Model.Type, head.Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Head0">Injuries/Wounds</label>
                    </div>
                    <div class="more">
                        <label class="fl" for="<%= Model.Type %>_Head0">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_HeadInjuries", data.AnswerOrEmptyString("HeadInjuries"), new { @id = Model.Type + "_HeadInjuries", @maxlength = "30" })%></div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Head1' name='{0}_Head' value='1' type='checkbox' {1} />", Model.Type, head.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Head1">Masses/Nodes</label>
                    </div>
                    <div class="more">
                        <div>
                            <label class="fl" for="<%= Model.Type %>_Head1">Site</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_HeadMassesSite", data.AnswerOrEmptyString("HeadMassesSite"), new { @id = Model.Type + "_HeadMassesSite", @maxlength = "30" })%>
                            </div>
                        </div>
                        <div class="clear" />
                        <div>
                            <label class="fl" for="<%= Model.Type %>_Head1">Size</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.Type + "_HeadMassesSize", data.AnswerOrEmptyString("HeadMassesSize"), new { @id = Model.Type + "_HeadMassesSize", @maxlength = "30" })%>
                            </div>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Head2' name='{0}_Head' value='2' type='checkbox' {1} />", Model.Type, head.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Head2">Other</label>
                    </div>
                    <div class="more">
                        <label class="fl" for="<%= Model.Type %>_Head2">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_HeadOther", data.AnswerOrEmptyString("HeadOther"), new { @id = Model.Type + "_HeadOther", @maxlength = "30" })%></div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</fieldset>
