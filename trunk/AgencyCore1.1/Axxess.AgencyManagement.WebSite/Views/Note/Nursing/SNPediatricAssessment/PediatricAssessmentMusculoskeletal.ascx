﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Musculoskeletal</legend>
    <div class="column">
		<div class="row">
			<div class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_IsMusculoskeletalApply", string.Empty, new { @id = Model.Type + "IsMusculoskeletalApplyHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_IsMusculoskeletalApply", Model.Type + "IsMusculoskeletalApply", "1", data.AnswerArray("IsMusculoskeletalApply").Contains("1"), "No Problem", new { @class = "toggle-container" })%>
			</div>
		</div>
	</div>
	<div class="collapsible-container">
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_Posture">Posture</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_Posture", data.AnswerOrEmptyString("Posture"), new { @id = Model.Type + "_Posture"})%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_Strength">Strength</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_Strength", data.AnswerOrEmptyString("Strength"), new { @id = Model.Type + "_Strength"})%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_Endurance">Endurance</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_Endurance", data.AnswerOrEmptyString("Endurance"), new { @id = Model.Type + "_Endurance"})%>
            </div>
        </div>
        <div class="row">
            <div class="checkgroup one-wide">
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Musculoskeletal0' name='{0}_Musculoskeletal' value='0' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("0").ToChecked())%>
                        <label for="<%= Model.Type %>_Musculoskeletal0">Scoliosis</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_ScoliosisType" class="fl">Type</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_ScoliosisType", data.AnswerOrEmptyString("ScoliosisType"), new { @id = Model.Type + "_ScoliosisType"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Musculoskeletal1' name='{0}_Musculoskeletal' value='1' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_Musculoskeletal1">Swollen/Painful joints</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_Swollen" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_Swollen", data.AnswerOrEmptyString("Swollen"), new { @id = Model.Type + "_Swollen"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Musculoskeletal2' name='{0}_Musculoskeletal' value='2' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_Musculoskeletal2">Fracture</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_ScoliosisType" class="fl">Location</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_Fracture", data.AnswerOrEmptyString("Fracture"), new { @id = Model.Type + "_Fracture"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Musculoskeletal3' name='{0}_Musculoskeletal' value='3' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_Musculoskeletal3">Decreased ROM</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_DecreasedROM" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_DecreasedROM", data.AnswerOrEmptyString("DecreasedROM"), new { @id = Model.Type + "_DecreasedROM"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
                <div class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_Musculoskeletal4' name='{0}_Musculoskeletal' value='4' type='checkbox' {1} />", Model.Type, data.AnswerArray("Musculoskeletal").Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_Musculoskeletal4">Other</label>
                    </div>
                    <div class="more">
                        <label for="<%= Model.Type %>_MuscuOther" class="fl">Specify</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_MuscuOther", data.AnswerOrEmptyString("MuscuOther"),new { @id = Model.Type + "_MuscuOther"})%>
                        </div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>
</fieldset>
