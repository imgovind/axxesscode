﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var ss = Model.Type;%>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentNewborn", Model); %>
<% Html.RenderPartial("Nursing/Sections/VitalSigns/Rev1", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentImmunizations", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentScreening", Model); %>
    </div>
</div>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentChildhoodHistory", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentEyes", Model); %>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentEars", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentNose", Model); %>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentHead", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentEndocrine", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentCardiovascular", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentRespiratory", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentGenitourinary", Model); %>
    </div>
</div>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentGastrointestinal", Model); %>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentNeurological", Model); %>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentPsychosocial", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentPain", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentGenitalia", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentMusculoskeletal", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentSkinConditions", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentAppliances", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentLivingArrangements", Model); %>
    </div>
</div>
<div class="inline-fieldset two-wide">
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentHematology", Model); %>
    </div>
    <div>
        <% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentAdvanceDirectives", Model); %>
    </div>
</div>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentSafety", Model); %>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentTeachingTraining", Model); %>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Skilled care provided this visit</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_SkilledCareProvidedTemplates")%>
                        <%= Html.TextArea(Model.Type + "_SkilledCareProvided", data.AnswerOrEmptyString("SkilledCareProvided"),new { @id = Model.Type + "_SkilledCareProvided" })%>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Prognosis/Learning potential</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_LearningPotentialTemplates")%>
                        <%= Html.TextArea(Model.Type + "_LearningPotential", data.AnswerOrEmptyString("LearningPotential"), new { @id = Model.Type + "_LearningPotential" })%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div>
        <fieldset>
            <legend>Summary of growth and development for age</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_SummaryOfGrowthTemplates")%>
                        <%= Html.TextArea(Model.Type + "_SummaryOfGrowth", data.AnswerOrEmptyString("SummaryOfGrowth"), new { @id = Model.Type + "_SummaryOfGrowth"})%>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Discharge plans</legend>
            <div class="column">
                <div class="row">
                    <div class="template-text">
                        <%= Html.ToggleTemplates(Model.Type + "_DischrgePlanTemplates")%>
                        <%= Html.TextArea(Model.Type + "_DischrgePlan", data.AnswerOrEmptyString("DischrgePlan"),new { @id = Model.Type + "_DischrgePlan"})%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<% Html.RenderPartial("Nursing/SNPediatricAssessment/PediatricAssessmentSummaryChecklist", Model); %>
