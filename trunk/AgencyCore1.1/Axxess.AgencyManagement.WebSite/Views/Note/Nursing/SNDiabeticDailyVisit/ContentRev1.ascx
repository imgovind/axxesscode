﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% Html.RenderPartial("Nursing/Sections/VitalSigns/Rev1", Model); %>
<% Html.RenderPartial("Nursing/Sections/Cardiovascular", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/DiabeticCare", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/Respiratory", Model); %>
		<% Html.RenderPartial("Nursing/Sections/DiabeticNarrative", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/DiabeticSupervisory", Model); %>

