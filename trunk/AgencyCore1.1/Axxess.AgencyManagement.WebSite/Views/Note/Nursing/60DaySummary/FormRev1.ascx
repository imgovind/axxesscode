﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">60 Day Summary/Case Conference | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
    <% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new {@area=Model.Service.ToArea(), @id = "SixtyDaySummaryForm" })){ %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
        <legend>Homebound Status</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup two-wide">
                    <% string[] homeboundStatus = data.AnswerArray("HomeboundStatus"); %>
                    <input name="<%= Model.Type %>_HomeboundStatus" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "2", homeboundStatus.Contains("2"), "Exhibits considerable &#38; taxing effort to leave home")%>
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "3", homeboundStatus.Contains("3"), "Requires the assistance of another to get up and move safely")%>
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "4", homeboundStatus.Contains("4"), "Severe Dyspnea")%>
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "5", homeboundStatus.Contains("5"), "Unable to safely leave home unassisted")%>
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "6", homeboundStatus.Contains("6"), "Unsafe to leave home due to cognitive or psychiatric impairments")%>
                    <%= Html.CheckgroupOption(Model.Type + "_HomeboundStatus", "7", homeboundStatus.Contains("7"), "Unable to leave home due to medical restriction(s)")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_HomeboundStatus", "8", homeboundStatus.Contains("8"), "Other", "_HomeboundStatusOther", data.AnswerOrEmptyString("HomeboundStatusOther"))%>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Condition</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup five-wide">
                    <% string[] patientCondition = data.AnswerArray("PatientCondition"); %>
                    <input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "1", patientCondition.Contains("1"), "Stable")%>
                    <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "2", patientCondition.Contains("2"), "Improved")%>
                    <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "3", patientCondition.Contains("3"), "Unchanged")%>
                    <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "4", patientCondition.Contains("4"), "Unstable")%>
                    <%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "5", patientCondition.Contains("5"), "Declined")%>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Service(s) Provided</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup seven-wide">
                    <% string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
                    <input name="<%= Model.Type %>_ServiceProvided" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "1", serviceProvided.Contains("1"), "SN")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "2", serviceProvided.Contains("2"), "PT")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "3", serviceProvided.Contains("3"), "OT")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "4", serviceProvided.Contains("4"), "ST")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "5", serviceProvided.Contains("5"), "MSW")%>
                    <%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "6", serviceProvided.Contains("6"), "HHA")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_ServiceProvided", "7", serviceProvided.Contains("7"), "Other", "_ServiceProvidedOtherValue", data.AnswerOrEmptyString("ServiceProvidedOtherValue"))%>
                </ul>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("VitalSigns/Ranges/ContentRev2", Model); %>
    <fieldset>
        <legend>Summary of Care Provided</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_SummaryOfCareProvidedTemplates")%>
                    <%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.AnswerOrEmptyString("SummaryOfCareProvided"), new { })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient&#8217;s Current Condition</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_PatientCurrentConditionTemplates")%>
                    <%= Html.TextArea(Model.Type + "_PatientCurrentCondition", data.AnswerOrEmptyString("PatientCurrentCondition"), new { })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Goals</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.Type + "_GoalsTemplates")%>
                    <%= Html.TextArea(Model.Type + "_Goals", data.AnswerOrEmptyString("Goals"), new { })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Recomended Services</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup seven-wide">
                    <% string[] recommendedService = data.AnswerArray("RecommendedService"); %>
                    <input name="<%= Model.Type %>_RecommendedService" value="" type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "1", recommendedService.Contains("1"), "SN")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "2", recommendedService.Contains("2"), "PT")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "3", recommendedService.Contains("3"), "OT")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "4", recommendedService.Contains("4"), "ST")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "5", recommendedService.Contains("5"), "MSW")%>
                    <%= Html.CheckgroupOption(Model.Type + "_RecommendedService", "6", recommendedService.Contains("6"), "HHA")%>
                    <%= Html.CheckgroupOptionWithOther(Model.Type + "_RecommendedService", "7", recommendedService.Contains("7"), "Other", Model.Type +"_RecommendedServiceOther", data.AnswerOrEmptyString("RecommendedServiceOther"))%>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Notifications</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <% string[] summarySentToPhysician = data.AnswerArray("SummarySentToPhysician"); %>
                    <input name="<%= Model.Type %>_SummarySentToPhysician" value=" " type="hidden" />
                    <%= Html.CheckgroupOption(Model.Type + "_SummarySentToPhysician", "Yes", summarySentToPhysician.Contains("Yes"), "Summary Sent To Physician")%>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl" for="<%= Model.Type %>_SummarySentBy">Sent By</label>
                <div class="fr"><%= Html.Users(Model.Type + "_SummarySentBy", data.AnswerOrEmptyString("SummarySentBy"), "-- Select User --", Model.LocationId, (int)UserStatus.Active, (int)Model.Service, new { @id = Model.Type + "_SummarySentBy" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl" for="<%= Model.Type %>_SummarySentDate">Date Sent</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_SummarySentDate" value="<%= data.AnswerValidDateOrEmptyString("SummarySentDate")%>" id="<%= Model.Type %>_SummarySentDate" /></div>
            </div>
        </div>
    </fieldset>
     <%= Html.Partial("Bottom/View", Model) %>
    <% } %>
</div>
