﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%@ Import Namespace="NPOI.SS.Formula.Functions" %>
<div class="inline-fieldset two-wide">
	<div>
		<fieldset>
			<legend>Vital Signs</legend>
			<% Html.RenderPartial("~/Views/VitalSign/Content.ascx", Model != null && Model.VitalSignLog != null ? Model.VitalSignLog : new VitalSignLog(true)); %>
		</fieldset>
		<% Html.RenderPartial("Nursing/Sections/PainProfile", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/Skin", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Respiratory", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/Cardiovascular", Model); %>
<% Html.RenderPartial("Nursing/Sections/Neurological", Model); %>
<% Html.RenderPartial("Nursing/Sections/Musculoskeletal", Model); %>
<% Html.RenderPartial("Nursing/Sections/Gastrointestinal", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Nutrition", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/Genitourinary", Model); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/DiabeticCare", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/IV", Model); %>
		<% Html.RenderPartial("Nursing/Sections/InfectionControl", Model); %>
		<% Html.RenderPartial("Nursing/Sections/CareCoordination", Model); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/CarePlan", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/DischargePlanning", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/Interventions", Model); %>
<% Html.RenderPartial("Nursing/Sections/Narrative", Model); %>
<%if (Model.DisciplineTask == 42) { %>
<% Html.RenderPartial("Nursing/Sections/Supervisory", new VisitNoteSectionViewData(Model.Questions, false, Model.Type)); %>
<% } %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Response", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/Phlebotomy", Model); %>
