﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% var area = Model.Service.ToArea(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("WoundCareSave", "Note", FormMethod.Post, new { area , @id = "WoundCareForm" })) { %>
	<%= Html.Hidden("PatientGuid", Model.PatientId)%>
	<%= Html.Hidden("Id", Model.EventId)%>
    <%= Html.Hidden("Action", "MainSave") %>
	<%= Html.Hidden("Type", "WoundCare")%>
    <fieldset>
        <legend>Wound Graph</legend>
        <div class="wide-column">
            <div class="row ac wound-care" wounds='<%= Model.WoundCareJson %>'></div>
            <div class="row">
                <label for="">Treatment Performed</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericTreatmentPerformed", data.AnswerOrEmptyString("GenericTreatmentPerformed"), new { @id = Model.TypeName + "_GenericTreatmentPerformed", @status = "(Optional) Treatment Performed" })%></div>
            </div>
            <div class="row">
                <label for="">Narrative</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), new { @id = Model.TypeName + "_GenericNarrative", @status = "(Optional) Narrative" })%></div>
            </div>
        </div>
    </fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
<%  } %>
</div>
<script type="text/javascript">
    var wounds = eval($(".wound-care", "#WoundCareForm").attr("wounds"));
    $(".wound-care", "#WoundCareForm").WoundChart({
        controller: "<%= area %>/Note",
        id: "<%= Model.EventId %>",
        patientId: "<%= Model.PatientId %>",
        type: "<%= Model.TypeName %>",
        wounds: wounds
    });
</script>