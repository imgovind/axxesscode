﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%-- This view is used for Coordination Of Care and Transfer Summary --%>
<span class="wintitle">
	<%= Model.TypeName %>|<%= Model.PatientProfile.DisplayName%></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
<% string[] patientCondition = data.AnswerArray("PatientCondition"); %>
<% string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
<% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" }))
   { %>
<div class="wrapper main note">
	 <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Functional Limitations</legend>
				<input type="hidden" name="<%= Model.Type %>_FunctionLimitations" value="" />
				<div class="column">
					<div class="row">
						<div class="checkgroup one-wide">
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "1", functionLimitations.Contains("1"), "Amputation")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "5", functionLimitations.Contains("5"), "Paralysis")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "9", functionLimitations.Contains("9"), "Legally Blind")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "2", functionLimitations.Contains("2"), "Bowel/Bladder Incontinence")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "6", functionLimitations.Contains("6"), "Endurance")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "A", functionLimitations.Contains("A"), "Dyspnea with Minimal Exertion")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "3", functionLimitations.Contains("3"), "Contracture")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "7", functionLimitations.Contains("7"), "Ambulation")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "4", functionLimitations.Contains("4"), "Hearing")%>
							<%= Html.CheckgroupOption(Model.Type + "_FunctionLimitations", "8", functionLimitations.Contains("8"), "Speech")%>
							<%= Html.CheckgroupOptionWithOther(Model.Type + "_FunctionLimitations", "B", functionLimitations.Contains("B"), "Other", Model.Type + "_FunctionLimitationsOther", data.AnswerOrEmptyString("FunctionLimitationsOther"))%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Patient Condition</legend>
				<input type="hidden" name="<%= Model.Type %>_PatientCondition" value="" />
				<div class="column">
					<div class="row">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "1", patientCondition.Contains("1"), "Stable")%>
							<%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "2", patientCondition.Contains("2"), "Improved")%>
							<%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "3", patientCondition.Contains("3"), "Unchanged")%>
							<%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "4", patientCondition.Contains("4"), "Unstable")%>
							<%= Html.CheckgroupOption(Model.Type + "_PatientCondition", "5", patientCondition.Contains("5"), "Declined")%>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Service(s) Provided</legend>
				<input type="hidden" name="<%= Model.Type %>_ServiceProvided" value="" />
				<div class="column">
					<div class="row">
						<div class="checkgroup two-wide">
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "1", serviceProvided.Contains("1"), "SN")%>
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "2", serviceProvided.Contains("2"), "PT")%>
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "3", serviceProvided.Contains("3"), "OT")%>
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "4", serviceProvided.Contains("4"), "ST")%>
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "5", serviceProvided.Contains("5"), "MSW")%>
							<%= Html.CheckgroupOption(Model.Type + "_ServiceProvided", "6", serviceProvided.Contains("6"), "HHA")%>
							<%= Html.CheckgroupOptionWithOther(Model.Type + "_ServiceProvided", "7", serviceProvided.Contains("7"), "Other", Model.Type + "_ServiceProvidedOtherValue", data.AnswerOrEmptyString("ServiceProvidedOtherValue"))%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
	<% Html.RenderPartial("VitalSigns/Ranges/ContentRev1", Model); %>
	<% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Transfer Facility Information</legend>
				<div class="column">
					<div class="row">
						<label for="<%= Model.Type %>_Facility" class="fl">Facility</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_Facility", data.AnswerOrEmptyString("Facility"), new { @id = Model.Type + "_Facility" })%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_Phone1" class="fl">Phone</label>
						<div class="fr">
							<%= Html.PhoneOrFax(Model.Type + "_Phone", data.AnswerOrEmptyString("Phone1"), data.AnswerOrEmptyString("Phone2"), data.AnswerOrEmptyString("Phone3"), Model.Type + "_Phone", "autotext")%>
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.Type %>_Contact" class="fl">Contact</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_Contact", data.AnswerOrEmptyString("Contact"), new { @id = Model.Type + "_Contact" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Services Providing</label>
					</div>
					<div class="row">
						<div class="template-text">
							<%= Html.ToggleTemplates(Model.Type + "_ServicesProvidingTemplates")%>
							<%= Html.TextArea(Model.Type + "_ServicesProviding", data.AnswerOrEmptyString("ServicesProviding"), new { @id = Model.Type + "_ServicesProviding" })%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Medicare Review</legend>
				<div class="column">
					<div class="row">
						<% Html.RenderPartial("Nursing/Sections/MedicareReview", Model); %>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Summary Of Care Provided By HHA</legend>
				<div class="wide-column">
				    <div class="row">
				        <div class="template-text">
        					<%= Html.ToggleTemplates(Model.Type + "_SummaryOfCareProvidedTemplates")%>
		        			<%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.AnswerOrEmptyString("SummaryOfCareProvided"), new { })%>
				        </div>
				    </div>
				</div>
			</fieldset>
		</div>
	</div>
	<%= Html.Partial("Bottom/View", Model) %>
	<% } %>
</div>
