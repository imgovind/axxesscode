﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<% using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" })) { %>
   <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
	    <legend>Initial Summary of Care</legend>
	        <div class="wide-column">
	            <div class="row">
                    <div class="template-text">
					    <%= Html.ToggleTemplates(Model.Type + "_CareSummaryTemplates")%>
	                    <%= Html.TextArea(Model.Type + "_CareSummary", data.AnswerOrEmptyString("CareSummary"), new { @id = Model.Type + "_CareSummary", @class = "tall" })%>
                    </div>
	            </div>
	        </div>
	 </fieldset>
     <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>