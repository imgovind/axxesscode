﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientVisitNote>" %>
<% var noteType = Model.NoteType; %>
<div id="<%=noteType %>_SupplyGridContent">
    <% var currentSuppliesGrid = string.Format("{0}_SupplyGrid", noteType); %>
    <div class="buttons fl">
        <a href="javascript:void(0);" onclick="UserInterface.ShowNewNoteSupply()">Add Supply</a>
    </div>
    
    <%= Html.Telerik()
        .Grid<Supply>()
        .Name(currentSuppliesGrid)
        .DataKeys(keys => { keys.Add(M => M.UniqueIdentifier).RouteKey("UniqueIdentifier");})
        .ToolBar(commands => { commands.Insert().ButtonType(GridButtonType.Text); })
        .DataBinding(dataBinding => {
            dataBinding.Ajax()
                .Select("GetNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id })
                .Insert("AddNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id })
                .Update("EditNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id })
                .Delete("DeleteNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id });
            })
        .Columns(columns => {
            columns.Bound(s => s.Description);
            columns.Bound(s => s.Quantity).Width(105);
                columns.Bound(e => e.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(105);
                columns.Command(commands => {
                    commands.Edit();
                    commands.Delete();
                }).Width(180).Title("Action");
            columns.Bound(s => s.Code).HeaderHtmlAttributes(new { style = "display: none" }).HtmlAttributes(new { style = "display: none" });
            columns.Bound(s => s.UnitCost).HeaderHtmlAttributes(new { style = "display: none" }).HtmlAttributes(new { style = "display: none" });
            columns.Bound(s => s.UniqueIdentifier).HeaderHtmlAttributes(new { style = "display: none" }).HtmlAttributes(new { style = "display: none" });
            
            
        })
        .Editable(editing => editing.Mode(GridEditMode.InLine))
        .ClientEvents(events => events.OnEdit("Supply.OnSupplyEdit"))
        .Sortable()
        .Footer(false)
    %>
</div>
<script type="text/javascript">$("#<%=currentSuppliesGrid %> .t-grid-toolbar a.t-grid-add").text("Add New Supply"); $("<%=currentSuppliesGrid %> .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '60px !important', 'bottom': '0' });</script>

