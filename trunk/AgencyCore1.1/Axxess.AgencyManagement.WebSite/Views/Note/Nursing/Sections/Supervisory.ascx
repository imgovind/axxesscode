﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteSectionViewData>" %>
<fieldset>
	<legend>Supervisory Visit</legend>
<%  if (Model.IsNA) { %>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "isSupervisoryApplied", string.Empty, new { @id = Model.Type + "_GenericIsSupervisoryAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsSupervisoryApplied", Model.Type + "_GenericIsSupervisoryApplied1", "1", Model.Questions.AnswerArray("GenericIsSupervisoryApplied").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
<%  } %>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<ul class="checkgroup two-wide">
					<%  string[] genericSupervisoryVisit = Model.Questions.AnswerArray("GenericSupervisoryVisit"); %>
					<%= Html.Hidden(Model.Type + "_GenericSupervisoryVisit", string.Empty, new { @id = Model.Type + "_GenericSupervisoryVisitHidden" })%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "1", genericSupervisoryVisit.Contains("1"), "LVN present") %>
					<%= Html.CheckgroupOption(Model.Type + "_GenericSupervisoryVisit", "2", genericSupervisoryVisit.Contains("2"), "HHA present") %>
				</ul>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericSupervisoryVisitComment">Comments</label>
				<div class="template-text">
					<%= Html.ToggleTemplates(Model.Type + "_GenericSupervisoryVisitCommentTemplates")%>
					<%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", Model.Questions.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
