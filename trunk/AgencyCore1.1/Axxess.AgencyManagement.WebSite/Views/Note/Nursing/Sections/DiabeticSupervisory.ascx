﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>HHA PRN Supervisory Visit</legend>
	<div class="column">
		<div class="row">
			<label class="fl">HHA Present</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_HhaPresent", "1", data.AnswerOrEmptyString("HhaPresent").Equals("1"), new { @id = Model.Type + "_HhaPresent1" })%>
							<label for="<%= Model.Type %>_HhaPresent1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_HhaPresent", "0", data.AnswerOrEmptyString("HhaPresent").Equals("0"), new { @id = Model.Type + "_HhaPresent0" })%>
							<label for="<%= Model.Type %>_HhaPresent0">No</label>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="fl">Aide following Care Plan</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "1", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("1"), new { @id = Model.Type + "_AideFollowCarePlan1" })%>
							<label for="<%= Model.Type %>_AideFollowCarePlan1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "0", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("0"), new { @id = Model.Type + "_AideFollowCarePlan0" })%>
							<label for="<%= Model.Type %>_AideFollowCarePlan0">No</label>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label class="strong">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), new { @id = Model.Type + "_GenericComments" })%></div>
		</div>
	</div>
</fieldset>