﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>



 <div class="column">
    <div class="row">
        <label class="fl">Was poc sent with patient?</label>
            <div class="fr">
	            <%= Html.YesNoCheckGroup(Model.Type + "_GenericIsPOCSentWithPatient", data.AnswerOrEmptyString("GenericIsPOCSentWithPatient"))%>
	        </div>
    </div>
     <div class="row">
        <label class="fl">Was medication sent with patient?</label>
            <div class="fr">
	            <%= Html.YesNoCheckGroup(Model.Type + "_GenericIsMedicationSentWithPatient", data.AnswerOrEmptyString("GenericIsMedicationSentWithPatient"))%>
	        </div>
    </div>
 </div>