﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Cardiovascular</legend>
    <div class="column">
        <div class="row">
            <label class="al">Rhythm</label>
            <ul class="checkgroup two-wide">
                <% string[] cardiorhythm = data.AnswerArray("Cardiorhythm"); %>
                <%= Html.Hidden(Model.Type + "_Cardiorhythm", string.Empty, new { @id = Model.Type + "_Cardiorhythm" })%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_Cardiorhythm", "0", cardiorhythm.Contains("0"), "Regular")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_Cardiorhythm", "1", cardiorhythm.Contains("1"), "Irregular")%>
            </ul>
        </div>
        <div class="row">
            <label class="al">Rate</label>
            <ul class="checkgroup two-wide">
                <% string[] cardioRate = data.AnswerArray("CardioRate"); %>
                <%= Html.Hidden(Model.Type + "_CardioRate", string.Empty, new { @id = Model.Type + "_CardioRate" })%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_CardioRate", "0", cardioRate.Contains("0"), "Strong")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_CardioRate", "1", cardioRate.Contains("1"), "Weak")%>
            </ul>
        </div>
        <div class="row">
            <label class="al">Capillary Refill</label>
            <ul class="checkgroup three-wide">
                <% string[] cardioCapillaryRefill = data.AnswerArray("CardioCapillaryRefill"); %>
                <%= Html.Hidden(Model.Type + "_CardioCapillaryRefill", string.Empty, new { @id = Model.Type + "_CardioCapillaryRefill" })%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_CardioCapillaryRefill", "0", cardioCapillaryRefill.Contains("0"), "< 1 sec")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_CardioCapillaryRefill", "1", cardioCapillaryRefill.Contains("1"), "< 3 sec")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_CardioCapillaryRefill", "2", cardioCapillaryRefill.Contains("2"), "< 6 sec")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_IntegComment" class="fl">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_CardioComments", data.AnswerOrEmptyString("CardioComments"), new { @id = Model.Type + "_CardioComments", @class = "tall" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Pulse</label>
        </div>
        <div class="sub row">
            <label class="al">Femoral</label>
            <ul class="checkgroup two-wide">
                <% string[] cardioFemoral = data.AnswerArray("CardioFemoral"); %>
                <%= Html.Hidden(Model.Type + "_CardioFemoral", string.Empty, new { @id = Model.Type + "_CardioFemoral" })%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioFemoral", "0", cardioFemoral.Contains("0"), "L", Model.Type + "_CardioFemoralL", data.AnswerOrEmptyString("CardioFemoralL"))%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioFemoral", "1", cardioFemoral.Contains("1"), "R", Model.Type + "_CardioFemoralR", data.AnswerOrEmptyString("CardioFemoralR"))%>
            </ul>
        </div>
        <div class="sub row">
            <label class="al">Radial</label>
            <ul class="checkgroup two-wide">
                <% string[] cardioRadial = data.AnswerArray("CardioRadial"); %>
                <%= Html.Hidden(Model.Type + "_CardioRadial", string.Empty, new { @id = Model.Type + "_CardioRadial" })%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioRadial", "0", cardioRadial.Contains("0"), "L", Model.Type + "_CardioRadialL", data.AnswerOrEmptyString("CardioRadialL"))%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioRadial", "1", cardioRadial.Contains("1"), "R", Model.Type + "_CardioRadialR", data.AnswerOrEmptyString("CardioRadialR"))%>
            </ul>
        </div>
        <div class="sub row">
            <label class="al">Pedal</label>
            <ul class="checkgroup two-wide">
                <% string[] cardioPedal = data.AnswerArray("CardioPedal"); %>
                <%= Html.Hidden(Model.Type + "_CardioPedal", string.Empty, new { @id = Model.Type + "_CardioPedal" })%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioPedal", "0", cardioPedal.Contains("0"), "L", Model.Type + "_CardioPedalL", data.AnswerOrEmptyString("CardioPedalL"))%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioPedal", "1", cardioPedal.Contains("1"), "R", Model.Type + "_CardioPedalR", data.AnswerOrEmptyString("CardioPedalR"))%>
            </ul>
        </div>
        <div class="row">
            <ul class="checkgroup two-wide">
                <% string[] cardioPulse = data.AnswerArray("CardioPulse"); %>
                <%= Html.Hidden(Model.Type + "_CardioPulse", string.Empty, new { @id = Model.Type + "_CardioPulse" })%>
                <%= Html.CheckgroupOption(Model.Type + "_CardioPulse", "0", cardioPulse.Contains("0"), "Cyanosis(general)")%>
                <%= Html.CheckgroupOption(Model.Type + "_CardioPulse", "1", cardioPulse.Contains("1"), "Circumoral cyanosis")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_CardioPulse", "2", cardioPulse.Contains("2"), "Edema", Model.Type + "_CardioPulseEdema", data.AnswerOrEmptyString("CardioPulseEdema"))%>
            </ul>
        </div>
    </div>
</fieldset>
