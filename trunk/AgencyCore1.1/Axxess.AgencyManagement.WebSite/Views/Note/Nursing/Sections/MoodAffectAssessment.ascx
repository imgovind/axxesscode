﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Mood/Affect Assessment</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsMoodAffectAssessmentApplied", string.Empty, new { @id = Model.Type + "_GenericIsMoodAffectAssessmentAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsMoodAffectAssessmentApplied", Model.Type + "_GenericIsMoodAffectAssessmentApplied1", "1", data.AnswerArray("GenericIsMoodAffectAssessmentApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
<%  var questions = new List<string>() { "Anxious", "Inappropriate Affect", "Flat Affect", "Elevated Mood", "Depressed Mood", "Labile Mood" }; %>
<%  int count = 1; %>
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMoodAffectAssessment<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMoodAffectAssessment" + count, data.AnswerOrEmptyString("GenericMoodAffectAssessment" + count), new { @id = Model.Type + "_GenericMoodAffectAssessment" + count, @class = "short" })%></div>
			</div>
	<%  count++; %>
<%  } %>
			<div class="row">
				<label for="<%= Model.Type %>_GenericMoodAffectAssessmentComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericMoodAffectAssessmentComment", data.AnswerOrEmptyString("GenericMoodAffectAssessmentComment"), new { @id = Model.Type + "_GenericMoodAffectAssessmentComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
