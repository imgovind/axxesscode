﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
<%= Html.Hidden(Model.Type + "_GenericDigestive", string.Empty, new { @id = Model.Type + "_GenericDigestiveHidden" })%>
<fieldset>
	<legend>Gastrointestinal</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<%= Html.CheckgroupOption(Model.Type + "_GenericDigestive", "1", genericDigestive.Contains("1"), "WNL (Within Normal Limits)")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestive2' name='{0}_GenericDigestive' value='2' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestive2">Bowel Sounds</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveBowelSoundsType" class="fl">Specify</label>
						<div class="fr">
							<%  var bowelSounds = new SelectList(new[] {
									new SelectListItem { Text = "", Value = "0" },
									new SelectListItem { Text = "Present/WNL (Within Normal Limits) x4 quadrants", Value = "1" },
									new SelectListItem { Text = "Hyperactive", Value = "2" },
									new SelectListItem { Text = "Hypoactive", Value = "3" },
									new SelectListItem { Text = "Absent", Value = "4" }
								}, "Value", "Text", data.AnswerOrDefault("GenericDigestiveBowelSoundsType", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = Model.Type + "_GenericDigestiveBowelSoundsType" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestive3' name='{0}_GenericDigestive' value='3' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestive3">Abdominal Palpation</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericAbdominalPalpation" class="fl">Specify</label>
						<div class="fr">
							<%  var abdominalPalpation = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "0" },
								new SelectListItem { Text = "Soft/WNL (Within Normal Limits)", Value = "1" },
								new SelectListItem { Text = "Firm", Value = "2" },
								new SelectListItem { Text = "Tender", Value = "3" },
								new SelectListItem { Text = "Other", Value = "4" }
							}, "Value", "Text", data.AnswerOrDefault("GenericAbdominalPalpation", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericAbdominalPalpation", abdominalPalpation, new { @id = Model.Type + "_GenericAbdominalPalpation" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericDigestive", "4", genericDigestive.Contains("4"), "Bowel Incontinence")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericDigestive", "5", genericDigestive.Contains("5"), "Nausea")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericDigestive", "6", genericDigestive.Contains("6"), "Vomiting")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericDigestive", "7", genericDigestive.Contains("7"), "GERD")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestive8' name='{0}_GenericDigestive' value='8' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("8").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestive8">Abd Girth</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveAbdGirthLength" class="fl">Specify</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDigestiveAbdGirthLength", data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength"), new { @id = Model.Type + "_GenericDigestiveAbdGirthLength", @maxlength = "5" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label>Elimination</label>
			<ul class="checkgroup one-wide">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestive11' name='{0}_GenericDigestive' value='11' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("11").ToChecked())%>
						<label for="<%= Model.Type %>_GenericDigestive11">Last BM</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveLastBMDate" class="fl">Date</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDigestiveLastBMDate", data.AnswerOrEmptyString("GenericDigestiveLastBMDate"), new { @id = Model.Type + "_GenericDigestiveLastBMDate", @class = "date-picker", @maxlength = "10" })%></div>
						<div class="clr"></div>
						<ul class="checkgroup four-wide">
							<%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
							<%= Html.Hidden(Model.Type + "_GenericDigestiveLastBM", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMHidden" })%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericDigestiveLastBM", "1", genericDigestiveLastBM.Contains("1"), "WNL (Within Normal Limits)")%>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericDigestiveLastBM2' name='{0}_GenericDigestiveLastBM' value='2' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("2").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericDigestiveLastBM2">Abnormal Stool</label>
								</div>
								<div class="more">
									<ul class="checkgroup two-wide">
										<%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
										<%= Html.Hidden(Model.Type + "_GenericDigestiveLastBMAbnormalStool", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMAbnormalStoolHidden" })%>
            							<%= Html.CheckgroupOption(Model.Type + "_GenericDigestiveLastBMAbnormalStool", "1", genericDigestiveLastBM.Contains("1"), "Gray")%>
            							<%= Html.CheckgroupOption(Model.Type + "_GenericDigestiveLastBMAbnormalStool", "2", genericDigestiveLastBM.Contains("2"), "Tarry")%>
            							<%= Html.CheckgroupOption(Model.Type + "_GenericDigestiveLastBMAbnormalStool", "3", genericDigestiveLastBM.Contains("3"), "Black")%>
            							<%= Html.CheckgroupOption(Model.Type + "_GenericDigestiveLastBMAbnormalStool", "4", genericDigestiveLastBM.Contains("4"), "Fresh Blood")%>
									</ul>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericDigestiveLastBM3' name='{0}_GenericDigestiveLastBM' value='3' type='checkbox' {1} />", Model.Type, genericDigestiveLastBM.Contains("3").ToChecked())%>
									<label for="<%= Model.Type %>_GenericDigestiveLastBM3">Constipation</label>
								</div>
								<div class="more">
									<ul class="checkgroup one-wide">
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMConstipationType", "Chronic", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic"), "Chronic")%>
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMConstipationType", "Acute", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute"), "Acute")%>
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMConstipationType", "Occasional", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional"), "Occasional")%>
									</ul>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericDigestiveLastBM4' name='{0}_GenericDigestiveLastBM' value='4' type='checkbox' {1} />", Model.Type, genericDigestiveLastBM.Contains("4").ToChecked())%>
									<label for="<%= Model.Type %>_GenericDigestiveLastBM4">Diarrhea</label>
								</div>
								<div class="more">
									<ul class="checkgroup one-wide">
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMDiarrheaType", "Chronic", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic"), "Chronic")%>
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMDiarrheaType", "Acute", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute"), "Acute")%>
            							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDigestiveLastBMDiarrheaType", "Occasional", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional"), "Occasional")%>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label>Ostomy</label>
			<ul class="checkgroup three-wide">
				<%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
				<%= Html.Hidden(Model.Type + "_GenericDigestiveOstomy", string.Empty, new { @id = Model.Type + "_GenericDigestiveOstomyHidden" })%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestiveOstomy1' name='{0}_GenericDigestiveOstomy' value='1' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestiveOstomy1">Ostomy Type</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveOstomyType" class="fl">Specify</label>
						<div class="fr">
							<%  var ostomy = new SelectList(new[] {
									new SelectListItem { Text = "", Value = "0" },
									new SelectListItem { Text = "N/A", Value = "1" },
									new SelectListItem { Text = "Ileostomy ", Value = "2" },
									new SelectListItem { Text = "Colostomy", Value = "3" },
									new SelectListItem { Text = "Other", Value = "4" }
								}, "Value", "Text", data.AnswerOrDefault("GenericDigestiveOstomyType", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericDigestiveOstomyType", ostomy, new { @id = Model.Type + "_GenericDigestiveOstomyType" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestiveOstomy2' name='{0}_GenericDigestiveOstomy' value='2' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestiveOstomy2">Stoma Appearance</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveStomaAppearance" class="fl">Specify</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDigestiveStomaAppearance", data.AnswerOrEmptyString("GenericDigestiveStomaAppearance"), new { @id = Model.Type + "_GenericDigestiveStomaAppearance" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericDigestiveOstomy3' name='{0}_GenericDigestiveOstomy' value='3' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericDigestiveOstomy3">Surrounding Skin</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericDigestiveSurSkinType" class="fl">Specify</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDigestiveSurSkinType", data.AnswerOrEmptyString("GenericDigestiveSurSkinType"), new { @id = Model.Type + "_GenericDigestiveSurSkinType" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericGastrointestinalComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericGastrointestinalComment", data.AnswerOrEmptyString("GenericGastrointestinalComment"), new { @class = "tall", @id = Model.Type + "_GenericGastrointestinalComment" })%></div>
		</div>
	</div>
</fieldset>

