﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset id="<%= Model.Type %>_Response">
	<legend>Response to Teaching/Interventions</legend>
	<div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%  string[] responseToTeachingInterventions = data.AnswerArray("GenericResponseToTeachingInterventions"); %>
				<%= Html.Hidden(Model.Type + "_GenericResponseToTeachingInterventions", string.Empty)%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions1' name='{0}_GenericResponseToTeachingInterventions' value='1' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("1").ToChecked()) %>
                        <span>
                            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">Verbalize</label>
                            <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsUVI", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsUVI"), new { @id = Model.Type + "_GenericResponseToTeachingInterventionsUVI", @class = "short" }) %>
                            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">understanding of verbal instructions/teaching given.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
						<%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions2' name='{0}_GenericResponseToTeachingInterventions' value='2' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">Able to return</label>
                            <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsCDP", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsCDP"), new { @id = Model.Type + "_GenericResponseToTeachingInterventionsCDP", @class = "short" })%>
                            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">correct demonstration of procedure/technique instructed on.</label>
                        </span>
                    </div>
                </li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericResponseToTeachingInterventions", "3", responseToTeachingInterventions.Contains("3"), "Treatment/Procedure well tolerated by patient")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericResponseToTeachingInterventions", "4", responseToTeachingInterventions.Contains("4"), "Infusion well tolerated by patient")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericResponseToTeachingInterventions", "5", responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("5"), "Other", Model.Type + "_GenericResponseToTeachingInterventionsOther", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsOther"))%>
			</ul>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
    Oasis.InitInterventionsAndGoals($("#<%= Model.Type %>_Response"));
</script>