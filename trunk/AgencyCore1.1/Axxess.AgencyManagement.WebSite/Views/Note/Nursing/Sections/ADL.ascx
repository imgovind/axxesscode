﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>ADL Level</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Overall ADL Status</label>
			<div class="fr">
				<%  var adl = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericADL1", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericADL1", adl, new { @id = Model.Type + "_GenericADL1" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Dressing</label>
			<div class="fr">
				<%  var adl2 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericADL2", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericADL2", adl2, new { @id = Model.Type + "_GenericADL2" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Motivation</label>
			<div class="fr">
				<%  var adl3 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericADL3", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericADL3", adl3, new { @id = Model.Type + "_GenericADL3" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Personal Hygiene</label>
			<div class="fr">
				<%  var adl4 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericADL4", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericADL4", adl4, new { @id = Model.Type + "_GenericADL4" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Sleeping Habits</label>
			<div class="fr">
				<%  var adl5 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Insomnia", Value = "Insomnia" },
					}, "Value", "Text", data.AnswerOrDefault("GenericADL5", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericADL5", adl5, new { @id = Model.Type + "_GenericADL5" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericADLComment">Comments</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericADLComment", data.AnswerOrEmptyString("GenericADLComment"), new { @id = Model.Type + "_GenericADLComment" })%>
			</div>
		</div>
	</div>
</fieldset>
