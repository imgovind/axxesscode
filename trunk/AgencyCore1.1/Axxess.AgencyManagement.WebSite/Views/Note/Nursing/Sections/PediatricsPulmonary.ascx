﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Pulmonary</legend>
	<div class="column">
		<div class="row">
			<label class="al">Respiration</label>
			<ul class="checkgroup one-wide">
				<% string[] pulmResp = data.AnswerArray("PulmResp"); %>
				<%= Html.Hidden(Model.Type + "_PulmResp", string.Empty, new { @id = Model.Type + "_PulmResp" })%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_PulmResp", "0", pulmResp.Contains("0"), "Diminished", Model.Type + "_PulmRespDiminished", data.AnswerOrEmptyString("PulmRespDiminished"))%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_PulmResp", "1", pulmResp.Contains("1"), "Wheezes", Model.Type + "_PulmRespWheezes", data.AnswerOrEmptyString("PulmRespWheezes"))%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_PulmResp", "2", pulmResp.Contains("2"), "Crackles", Model.Type + "_PulmRespCrackles", data.AnswerOrEmptyString("PulmRespCrackles"))%>
			</ul>
		</div>
		<div class="row">
			<label class="al">Cough</label>
			<ul class="checkgroup two-wide">
				<% string[] pulmCough = data.AnswerArray("PulmCough"); %>
				<%= Html.Hidden(Model.Type + "_PulmCough", string.Empty, new { @id = Model.Type + "_PulmCough" })%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "0", pulmCough.Contains("0"), "Non-productive")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_PulmCough", "1", pulmCough.Contains("1"), "Productive", Model.Type + "_PulmCoughProductive", data.AnswerOrEmptyString("PulmCoughProductive"))%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "2", pulmCough.Contains("2"), "Retraction")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "3", pulmCough.Contains("3"), "Nasal flare")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "4", pulmCough.Contains("4"), "Grunting")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "5", pulmCough.Contains("5"), "Cyanosis")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmCough", "6", pulmCough.Contains("6"), "Paradoxical excursions")%>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Apnea monitor</label>
		</div>
		<div class="sub row">
			<label class="fl">Settings</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_PulmCoughApnea", data.AnswerOrEmptyString("PulmCoughApnea"), new { @id = Model.Type + "_PulmCoughApnea",@maxlength = "30" })%></div>
		</div>
		<div class="row">
			<label class="al">Oxygen Use</label>
			<ul class="checkgroup three-wide">
				<% string[] pulmOxygenUse = data.AnswerArray("PulmOxygenUse"); %>
				<%= Html.Hidden(Model.Type + "_PulmOxygenUse", string.Empty, new { @id = Model.Type + "_PulmOxygenUse" })%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_PulmOxygenUse", "0", pulmOxygenUse.Contains("0"), "Yes")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_PulmOxygenUse", "1", pulmOxygenUse.Contains("1"), "No")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_PulmOxygenUse", "2", pulmOxygenUse.Contains("2"),true, "Liter/min", Model.Type + "_PulmOxygenUseLiter", data.AnswerOrEmptyString("PulmOxygenUseLiter"))%>
			</ul>
		</div>
		<div class="row">
			<label class="al">Via</label>
			<ul class="checkgroup three-wide">
				<% string[] pulmOxygenUseVia = data.AnswerArray("PulmOxygenUseVia"); %>
				<%= Html.Hidden(Model.Type + "_PulmOxygenUseVia", string.Empty, new { @id = Model.Type + "_PulmOxygenUseVia" })%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmOxygenUseVia", "0", pulmOxygenUseVia.Contains("0"), "Nasal cannula")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmOxygenUseVia", "1", pulmOxygenUseVia.Contains("1"), "Tracheostomy")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmOxygenUseVia", "2", pulmOxygenUseVia.Contains("2"), "Mask")%>
			</ul>
		</div>
		<div class="row">
			<label class="al">Pulmonary treatments</label>
			<ul class="checkgroup two-wide">
				<% string[] pulmPulmonary = data.AnswerArray("PulmPulmonary"); %>
				<%= Html.Hidden(Model.Type + "_PulmOxygenUse", string.Empty, new { @id = Model.Type + "_PulmOxygenUse" })%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmPulmonary", "0", pulmPulmonary.Contains("0"), "Nebulizers")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmPulmonary", "1", pulmPulmonary.Contains("1"), "Aerosol")%>
				<%= Html.CheckgroupOption(Model.Type + "_PulmPulmonary", "2", pulmPulmonary.Contains("2"), "Clapping &#38; postural drainage")%>
			</ul>
		</div>
	</div>
</fieldset>
