﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Infection Control</legend>
	<div class="column">
<%  if (Model.IsUserCanCreateInfection) { %>
		<div class="row">
			<div class="fr">
    	        <div class="ancillary-button"><%= string.Format("<a onclick=\"Infection." + Model.Service.ToString() + ".New('" + Model.PatientId + "')\" title=\"New Infection Log\">Add Infection Log</a>", Model.PatientId) %></div>
            </div>
		</div>
<%  } %>
		<div class="row">
			<ul class="checkgroup one-wide">
				<%  string[] infectionControl = data.AnswerArray("InfectionControl"); %>
				<%= Html.Hidden(Model.Type + "_InfectionControl", string.Empty, new { @id = Model.Type + "_InfectionControlHidden" })%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_InfectionControl1' name='{0}_InfectionControl' value='1' type='checkbox' {1} />", Model.Type, infectionControl.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_InfectionControl1">Universal Precautions Observed</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_InfectionControl2' name='{0}_InfectionControl' value='2' type='checkbox' {1} />", Model.Type, infectionControl.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_InfectionControl2">Sharps/Waste Disposal</label>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label class="fl">New Infection</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "1", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("1"), new { @id = Model.Type + "_GenericIsNewInfection1" }) %>
							<label for="<%= Model.Type %>_GenericIsNewInfection1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "0", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("0"), new { @id = Model.Type + "_GenericIsNewInfection0" }) %>
							<label for="<%= Model.Type %>_GenericIsNewInfection0">No</label>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericInfectionControlComment" class="strong">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericInfectionControlComment", data.AnswerOrEmptyString("GenericInfectionControlComment"), new { @id = Model.Type + "_GenericInfectionControlComment" })%></div>
		</div>
	</div>
</fieldset>
