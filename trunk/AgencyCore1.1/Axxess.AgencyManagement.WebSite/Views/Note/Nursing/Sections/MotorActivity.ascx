﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Motor Activity</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsMotorActivityApplied", string.Empty, new { @id = Model.Type + "_GenericIsMotorActivityAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsMotorActivityApplied", Model.Type + "_GenericIsMotorActivityApplied1", "1", data.AnswerArray("GenericIsMotorActivityApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity1">Increased Amount</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity1", data.AnswerOrEmptyString("GenericMotorActivity1"), new { @id = Model.Type + "_GenericMotorActivity1", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity2">Decreased Amount</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity2", data.AnswerOrEmptyString("GenericMotorActivity2"), new { @id = Model.Type + "_GenericMotorActivity2", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity3">Agitation</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity3", data.AnswerOrEmptyString("GenericMotorActivity3"), new { @id = Model.Type + "_GenericMotorActivity3", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity4">Tics</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity4", data.AnswerOrEmptyString("GenericMotorActivity4"), new { @id = Model.Type + "_GenericMotorActivity4", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity5">Tremor</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity5", data.AnswerOrEmptyString("GenericMotorActivity5"), new { @id = Model.Type + "_GenericMotorActivity5", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity6">Peculiar Posturing</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity6", data.AnswerOrEmptyString("GenericMotorActivity6"), new { @id = Model.Type + "_GenericMotorActivity6", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity7">Unusual Gait</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity7", data.AnswerOrEmptyString("GenericMotorActivity7"), new { @id = Model.Type + "_GenericMotorActivity7", @class = "short" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericMotorActivity8">Repetitive Acts</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericMotorActivity8", data.AnswerOrEmptyString("GenericMotorActivity8"), new { @id = Model.Type + "_GenericMotorActivity8", @class = "short" })%></div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericMoodAffectAssessmentComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericMotorActivityComment", data.AnswerOrEmptyString("GenericMotorActivityComment"), new { @id = Model.Type + "_GenericMotorActivityComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
