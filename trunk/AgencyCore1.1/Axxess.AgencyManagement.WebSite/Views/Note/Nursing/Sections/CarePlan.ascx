<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Care Plan</legend>
	<div class="column">
<%  if (!Model.CarePlanOrEvalId.IsEmpty()) { %>
	    <div class="row ac">
			<div class="button"><a href="javascript:void(0)" careplanid='<%= Model.CarePlanOrEvalId %>' class="care-plan-print <%= Model.CarePlanOrEvalGroup %>">View Care Plan</a></div>
		</div>
<%  } %>
		<div class="row">
			<label for="GenericCarePlan" class="fl">Care Plan</label>
			<div class="fr">
				<%  var carePlan = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "No changes at this time", Value = "No changes at this time" },
						new SelectListItem { Text = "Reviewed w/Pt or CG", Value = "Reviewed w/Pt or CG" },
						new SelectListItem { Text = "Revised w/Pt or CG", Value = "Revised w/Pt or CG" },
						new SelectListItem { Text = "Other", Value = "Other" }
					}, "Value", "Text", data.AnswerOrDefault("GenericCarePlan", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericCarePlan", carePlan, new { @id = Model.Type + "_GenericCarePlan" }) %>
			</div>
		</div>
		<div class="row">
			<label for="GenericCarePlanProgressTowardsGoals" class="fl">Progress towards goals</label>
			<div class="fr">
				<%  var progressTowardsGoals = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Good", Value = "Good" },
						new SelectListItem { Text = "Fair", Value = "Fair" },
						new SelectListItem { Text = "Poor", Value = "Poor" }
					}, "Value", "Text", data.AnswerOrDefault("GenericCarePlanProgressTowardsGoals", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericCarePlanProgressTowardsGoals", progressTowardsGoals, new { @id = Model.Type + "_GenericCarePlanProgressTowardsGoals" }) %>
			</div>
		</div>
		<div class="row">
			<label class="fl">New/Changed Orders or Updates to Care Plan</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "1", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("1"), new { @id = Model.Type + "_GenericCarePlanIsChanged1" }) %>
							<label for="<%= Model.Type %>_GenericCarePlanIsChanged1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "0", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("0"), new { @id = Model.Type + "_GenericCarePlanIsChanged0" }) %>
							<label for="<%= Model.Type %>_GenericCarePlanIsChanged0">No</label>
						</div>
					</li>
				</ul>
			</div>
		</div>
<%  if (Model.IsUserCanCreateOrder || Model.IsUserCanCreateIncident) { %>
	<%  if (Model.IsUserCanCreateOrder ) { %>
		<div class="row">
			<div class="fr">
                <div class="ancillary-button"><a onclick="PhysicianOrder.<%= Model.Service.ToString() %>.New('<%= Model.PatientId %>')" status="Add New Order">Add New Order</a></div>
            </div>
        </div>
	<%  } %>
	<%  if (Model.IsUserCanCreateIncident) { %>
		<div class="row">
			<div class="fr">
    			<div class="ancillary-button fr"><%= string.Format("<a onclick=\"Incident." + Model.Service.ToString() + ".New('" + Model.PatientId + "')\" title=\"New Incident Log\">Add Incident Log</a>", Model.PatientId)%></div>
            </div>
		</div>
	<%  } %>
<%  } %>
		<div class="row">
			<label for="<%= Model.Type %>_GenericCarePlanComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericCarePlanComment", data.AnswerOrEmptyString("GenericCarePlanComment"), new { @id = Model.Type + "_GenericCarePlanComment" })%></div>
		</div>
	</div>
</fieldset>
