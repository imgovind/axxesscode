﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>G.I. Bowel Functions</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Status</label>
			<div class="fr">
				<%  var nutritionStatus = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Regulated", Value = "Regulated" },
						new SelectListItem { Text = "Irregular", Value = "Irregular" },
					}, "Value", "Text", data.AnswerOrDefault("GenericGIBowelFunctions1", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericGIBowelFunctions1", nutritionStatus, new { @id = Model.Type + "_GenericGIBowelFunctions1" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Cathartic Required</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericGIBowelFunctionsCatharticRequired", "1", data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("1"), "Yes")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericGIBowelFunctionsCatharticRequired", "0", data.AnswerOrEmptyString("GenericGIBowelFunctionsCatharticRequired").Equals("0"), "No")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericGIBowelFunctionsComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericGIBowelFunctionsComment", data.AnswerOrEmptyString("GenericGIBowelFunctionsComment"), new { @id = Model.Type + "_GenericGIBowelFunctionsComment" })%></div>
		</div>
	</div>
</fieldset>
