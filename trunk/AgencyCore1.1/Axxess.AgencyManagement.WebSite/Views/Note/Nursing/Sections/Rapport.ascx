﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Rapport</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Patient with Family</label>
			<div class="fr">
				<%  var rapport1 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericRapportPatientFamily", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericRapportPatientFamily", rapport1, new { @id = Model.Type + "_GenericRapportPatientFamily" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Familty with Patient</label>
			<div class="fr">
				<%  var rapport2 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericRapportFamilyPatient", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericRapportFamilyPatient", rapport2, new { @id = Model.Type + "_GenericRapportFamilyPatient" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Patient with RN</label>
			<div class="fr">
				<%  var rapport3 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericRapportPatientRN", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericRapportPatientRN", rapport3, new { @id = Model.Type + "_GenericRapportPatientRN" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Family with RN</label>
			<div class="fr">
				<%  var rapport4 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericRapportFamilyRN", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericRapportFamilyRN", rapport4, new { @id = Model.Type + "_GenericRapportFamilyRN" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericRapportComment">Comments</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericRapportComment", data.AnswerOrEmptyString("GenericRapportComment"), new { @id = Model.Type + "_GenericRapportComment" })%>
			</div>
		</div>
	</div>
</fieldset>
