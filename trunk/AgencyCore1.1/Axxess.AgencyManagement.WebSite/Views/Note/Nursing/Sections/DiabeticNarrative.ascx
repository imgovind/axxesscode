﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Comments/Teaching</legend>
	<div class="column">
		<div class="row">
			<label>Narrative</label>
			<div class="template-text">
				<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates") %>
				<%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), new { @id = Model.Type + "_GenericNarrativeComment" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Tolerated Cares</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_ToleratedCares", "1", data.AnswerOrEmptyString("ToleratedCares").Equals("1"), new { @id = Model.Type + "_ToleratedCares1" })%>
							<label for="<%= Model.Type %>_ToleratedCares1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_ToleratedCares", "0", data.AnswerOrEmptyString("ToleratedCares").Equals("0"), new { @id = Model.Type + "_ToleratedCares0" })%>
							<label for="<%= Model.Type %>_ToleratedCares0">No</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="clr"></div>
			<%= Html.TextArea(Model.Type + "_ToleratedCaresDescribe", data.AnswerOrEmptyString("ToleratedCaresDescribe"), 3, 30, new { @id = Model.Type + "_ToleratedCaresDescribe", @class = "short", @placeholder = "Describe..." })%>
		</div>
		<div class="row">
			<label class="fl">Patient Goals Met this Visit</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GoalsMet", "1", data.AnswerOrEmptyString("GoalsMet").Equals("1"), new { @id = Model.Type + "_GoalsMet1" })%>
							<label for="<%= Model.Type %>_GoalsMet1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_GoalsMet", "0", data.AnswerOrEmptyString("GoalsMet").Equals("0"), new { @id = Model.Type + "_GoalsMet0" })%>
							<label for="<%= Model.Type %>_GoalsMet0">No</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="clr"></div>
			<%= Html.TextArea(Model.Type + "_GoalsMetSpecify", data.AnswerOrEmptyString("GoalsMetSpecify"), 3, 30, new { @id = Model.Type + "_GoalsMetSpecify", @class = "short", @placeholder = "Specify..." })%>
		</div>
		<div class="row">
			<label class="fl">Care Plan Revised</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "1", data.AnswerOrEmptyString("RevisedCarePlan").Equals("1"), new { @id = Model.Type + "_RevisedCarePlan1" })%>
							<label for="<%= Model.Type %>_RevisedCarePlan1">Yes</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "0", data.AnswerOrEmptyString("RevisedCarePlan").Equals("0"), new { @id = Model.Type + "_RevisedCarePlan0" })%>
							<label for="<%= Model.Type %>_RevisedCarePlan0">No</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="clr"></div>
			<%= Html.TextArea(Model.Type + "_RevisedCarePlanSpecify", data.AnswerOrEmptyString("RevisedCarePlanSpecify"), 3, 30, new { @id = Model.Type + "_RevisedCarePlanSpecify", @class = "short", @placeholder = "Specify..." })%>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericPatientResponse">Patient Response</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericPatientResponse", data.AnswerOrEmptyString("GenericPatientResponse"), new { @class = "tall", @id = Model.Type + "_GenericPatientResponse" })%>
			</div>
		</div>
	</div>
</fieldset>
