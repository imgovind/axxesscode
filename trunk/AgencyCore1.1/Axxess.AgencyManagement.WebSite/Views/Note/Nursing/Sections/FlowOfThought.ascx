﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Flow Of Thought</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsFlowOfThoughtApplied", string.Empty, new { @id = Model.Type + "_GenericIsFlowOfThoughtAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsFlowOfThoughtApplied", Model.Type + "_GenericIsFlowOfThoughtApplied1", "1", data.AnswerArray("GenericIsFlowOfThoughtApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
<%  var questions = new List<string>() { "Blocking", "Circumstantial", "Tangential", "Perseveration", "Flight of Ideas", "Loose Association", "Indecisive" }; %>
<%  int count = 1; %>
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericFlowOfThought<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericFlowOfThought" + count, data.AnswerOrEmptyString("GenericFlowOfThought" + count), new { @id = Model.Type + "_GenericFlowOfThought" + count, @class = "short" })%></div>
	<%  count++; %>
			</div>
<%  } %>
			<div class="row">
				<label for="<%= Model.Type %>_GenericFlowOfThoughtComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericFlowOfThoughtComment", data.AnswerOrEmptyString("GenericFlowOfThoughtComment"), new { @id = Model.Type + "_GenericFlowOfThoughtComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>