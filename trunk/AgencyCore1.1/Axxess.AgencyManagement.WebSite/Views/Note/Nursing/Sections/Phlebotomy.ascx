﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Phlebotomy</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsPhlebotomyApplied", string.Empty, new { @id = Model.Type + "_GenericIsPhlebotomyAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsPhlebotomyApplied", Model.Type + "_GenericIsPhlebotomyApplied1", "1", data.AnswerArray("GenericIsPhlebotomyApplied").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericPhlebotomyLabs" class="fl">Labs</label>
				<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPhlebotomyLabs", data.AnswerOrEmptyString("GenericPhlebotomyLabs"), new { @id = Model.Type + "_GenericPhlebotomyLabs" })%></div>
			</div>
			<div class="row">
				<label class="fl">Venipuncture</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericPhlebotomyVenipuncture", data.AnswerOrEmptyString("GenericPhlebotomyVenipuncture"), new { @class = "shorter", @id = Model.Type + "_GenericPhlebotomyVenipuncture" })%>
					<label>(# of attempts) to</label>
					<%= Html.TextBox(Model.Type + "_GenericPhlebotomySite", data.AnswerOrEmptyString("GenericPhlebotomySite"), new { @class = "oe", @id = Model.Type + "_GenericPhlebotomySite" })%>
					<label>(site) with</label>
					<%= Html.TextBox(Model.Type + "_GenericPhlebotomyGA", data.AnswerOrEmptyString("GenericPhlebotomyGA"), new { @class = "shorter", @id = Model.Type + "_GenericPhlebotomyGA" })%>
					<label>ga. needle/vacutainer using aseptic technique. Applied pressure for</label>
					<%= Html.TextBox(Model.Type + "_GenericPhlebotomyMinutes", data.AnswerOrEmptyString("GenericPhlebotomyMinutes"), new { @class = "shorter", @id = Model.Type + "_GenericPhlebotomyMinutes" })%>
					<label>minutes.</label>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<ul class="checkgroup two-wide">
					<%  string[] phlebotomyActivity = data.AnswerArray("GenericPhlebotomyActivity"); %>
					<%= Html.Hidden(Model.Type + "_GenericPhlebotomyActivity", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyActivityHidden" })%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyActivity", "1", phlebotomyActivity.Contains("1"), "Pt tolerated procedure")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyActivity", "2", phlebotomyActivity.Contains("2"), "No bruising")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyActivity", "3", phlebotomyActivity.Contains("3"), "No bleeding")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyActivity", "4", phlebotomyActivity.Contains("4"), "Sharps Disposal")%>
				</ul>
			</div>
			<div class="row">
				<label>Delivered to</label>
				<ul class="checkgroup two-wide">
					<%  string[] phlebotomyDeliveredTo = data.AnswerArray("GenericPhlebotomyDeliveredTo"); %>
					<%= Html.Hidden(Model.Type + "_GenericPhlebotomyDeliveredTo", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyDeliveredToHidden" })%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyDeliveredTo", "1", phlebotomyDeliveredTo.Contains("1"), "Lab")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyDeliveredTo", "2", phlebotomyDeliveredTo.Contains("2"), "Hospital")%>
					<%= Html.CheckgroupOption(Model.Type + "_GenericPhlebotomyDeliveredTo", "3", phlebotomyDeliveredTo.Contains("3"), "MD")%>
					<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericPhlebotomyDeliveredTo", "4", phlebotomyDeliveredTo.Contains("4"), "Other", Model.Type + "_GenericPhlebotomyDeliveredToOther", data.AnswerOrEmptyString("GenericPhlebotomyDeliveredToOther"))%>
				</ul>
			</div>
		</div>
		<div class="wide-column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericPhlebotomyComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericPhlebotomyComment", data.AnswerOrEmptyString("GenericPhlebotomyComment"), new { @class = "tall", @id = Model.Type + "_GenericPhlebotomyComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
	Visit.PhlebotomyLab("#<%= Model.Type %>_GenericPhlebotomyLabs");
</script>