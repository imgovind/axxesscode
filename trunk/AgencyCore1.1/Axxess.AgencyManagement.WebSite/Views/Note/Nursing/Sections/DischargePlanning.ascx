﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Discharge Planning</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Discharge Planning Discussed</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
    				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDischargePlanningDiscussed", "1", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("1"), "Yes") %>
    				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDischargePlanningDiscussed", "0", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("0"), "No") %>
				</ul>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericDischargePlanningDiscussedWith" class="fl">Discharge Planning Discussed with</label>
			<div class="fr">
				<%  var dischargePlanningDiscussed = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Patient", Value = "Patient" },
						new SelectListItem { Text = "Caregiver", Value = "Caregiver" },
						new SelectListItem { Text = "Physician", Value = "Physician" },
						new SelectListItem { Text = "Pt/CG", Value = "Pt/CG" },
						new SelectListItem { Text = "Pt/CG/Physician", Value = "Pt/CG/Physician" },
						new SelectListItem { Text = "N/A", Value = "N/A" }
					}, "Value", "Text", data.AnswerOrDefault("GenericDischargePlanningDiscussedWith", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericDischargePlanningDiscussedWith", dischargePlanningDiscussed, new { @id = Model.Type + "_GenericDischargePlanningDiscussedWith" }) %>
			</div>
		</div>
		<div class="row">
			<label class="fl">Reason for Discharge</label>
			<div class="fr">
				<%  var reasonForDischarge = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Goals Met", Value = "Goals Met" },
						new SelectListItem { Text = "Transfer", Value = "Transfer" },
						new SelectListItem { Text = "Deceased", Value = "Deceased" },
						new SelectListItem { Text = "Other", Value = "Other" }
					}, "Value", "Text", data.AnswerOrDefault("GenericDischargeReasonForDischarge", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericDischargeReasonForDischarge", reasonForDischarge, new { @id = Model.Type + "_GenericDischargeReasonForDischarge" }) %>
			</div>
		</div>
		<div class="row">
			<label class="fl">Physician Notified of Discharge</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
    				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "1", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("1"), "Yes")%>
    				<%= Html.CheckgroupRadioOption(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "0", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("0"), "No")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<ul class="checkgroup one-wide">
				<%  string[] pateintReceivedDischarge = data.AnswerArray("GenericPateintReceivedDischarge"); %>
				<%= Html.Hidden(Model.Type + "_GenericPateintReceivedDischarge", string.Empty, new { @id = Model.Type + "_GenericPateintReceivedDischargeHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericPateintReceivedDischarge", "1", pateintReceivedDischarge.Contains("1"), "Patient received discharge notice per agency policy.") %>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericDischargeComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericDischargeComment", data.AnswerOrEmptyString("GenericDischargeComment"), new { @id = Model.Type + "_GenericDischargeComment" })%></div>
		</div>
	</div>
</fieldset>
