﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Sensorium</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsSensoriumApplied", string.Empty, new { @id = Model.Type + "_GenericIsSensoriumAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsSensoriumApplied", Model.Type + "_GenericIsSensoriumApplied1", "1", data.AnswerArray("GenericIsSensoriumApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label>Orientation Impaired</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumOrientation1">Time</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation1", data.AnswerOrEmptyString("GenericSensoriumOrientation1"), new { @id = Model.Type + "_GenericSensoriumOrientation1", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumOrientation2">Place</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation2", data.AnswerOrEmptyString("GenericSensoriumOrientation2"), new { @id = Model.Type + "_GenericSensoriumOrientation2", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumOrientation3">Person</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation3", data.AnswerOrEmptyString("GenericSensoriumOrientation3"), new { @id = Model.Type + "_GenericSensoriumOrientation3", @class = "short" })%></div>
			</div>
			<div class="row">
				<label>Memory</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory1">Clouding of Consiousness</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory1", data.AnswerOrEmptyString("GenericSensoriumMemory1"), new { @id = Model.Type + "_GenericSensoriumMemory1", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory2">Inablity to Concentrate</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory2", data.AnswerOrEmptyString("GenericSensoriumMemory2"), new { @id = Model.Type + "_GenericSensoriumMemory2", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory3">Amnesia</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory3", data.AnswerOrEmptyString("GenericSensoriumMemory3"), new { @id = Model.Type + "_GenericSensoriumMemory3", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory4">Poor Recent Memory</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory4", data.AnswerOrEmptyString("GenericSensoriumMemory4"), new { @id = Model.Type + "_GenericSensoriumMemory4", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory5">Poor Remote Memory</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory5", data.AnswerOrEmptyString("GenericSensoriumMemory5"), new { @id = Model.Type + "_GenericSensoriumMemory5", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericSensoriumMemory6">Confabulation</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory6", data.AnswerOrEmptyString("GenericSensoriumMemory6"), new { @id = Model.Type + "_GenericSensoriumMemory6", @class = "short" })%></div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericSensoriumComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericSensoriumComment", data.AnswerOrEmptyString("GenericSensoriumComment"), new { @id = Model.Type + "_GenericSensoriumComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
