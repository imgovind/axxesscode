﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Home Bound Status</legend>
	<div class="wide-column">
		<div class="row">
			<label>Homebound Status</label>
			<ul class="checkgroup two-wide">
				<%  string[] homeBoundReason = data.AnswerArray("HomeBoundReason"); %>
				<%= Html.Hidden(Model.Type + "_HomeBoundReason", string.Empty, new { @id = Model.Type + "_HomeBoundReasonHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "2", homeBoundReason.Contains("2"), "Exhibits considerable &#38; taxing effort to leave home")%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "3", homeBoundReason.Contains("3"), "Requires the assistance of another to get up and moving safely")%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "4", homeBoundReason.Contains("4"), "Severe Dyspnea")%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "5", homeBoundReason.Contains("5"), "Unable to safely leave home unassisted")%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "6", homeBoundReason.Contains("6"), "Unsafe to leave home due to cognitive or psychiatric impairments")%>
				<%= Html.CheckgroupOption(Model.Type + "_HomeBoundReason", "7", homeBoundReason.Contains("7"), "Unable to leave home due to medical restriction(s)")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_HomeBoundReason", "8", homeBoundReason.Contains("8"), "Other", Model.Type + "_HomeBoundReasonOther", data.AnswerOrEmptyString("HomeBoundReasonOther"))%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_HomeEnvironment" class="fl">Home Environment</label>
			<div class="fr">
				<%  var homeEnvironment = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "0" },
						new SelectListItem { Text = "No issues identified", Value = "1" },
						new SelectListItem { Text = "Lack of Finances", Value = "2" },
						new SelectListItem { Text = "Lack of CG/Family Support", Value = "3" },
						new SelectListItem { Text = "Poor Home Environment", Value = "4" },
						new SelectListItem { Text = "Cluttered/Soiled Living Conditions", Value = "5" },
						new SelectListItem { Text = "Other", Value = "6" }
					}, "Value", "Text", data.AnswerOrDefault("HomeEnvironment", "0")); %>
				<%= Html.DropDownList(Model.Type + "_HomeEnvironment", homeEnvironment, new { @id = Model.Type + "_HomeEnvironment" }) %>
			</div>
		</div>
	</div>
</fieldset>
