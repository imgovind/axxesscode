﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Intellect</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsIntellectApplied", string.Empty, new { @id = Model.Type + "_GenericIsIntellectAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsIntellectApplied", Model.Type + "_GenericIsIntellectApplied1", "1", data.AnswerArray("GenericIsIntellectApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
<%  var questions = new List<string>() { "Above Normal", "Below Normal", "Paucity of Knowledge", "Vocabulary Poor", "Serial Sevens Done Poorly", "Poor Abstraction" }; %>
<%  int count = 1; %>
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericIntellect<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericIntellect" + count, data.AnswerOrEmptyString("GenericIntellect" + count), new { @id = Model.Type + "_GenericIntellect" + count })%></div>
			</div>
	<%  count++; %>
<%  } %>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIntellectComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericIntellectComment", data.AnswerOrEmptyString("GenericIntellectComment"), new { @id = Model.Type + "_GenericIntellectComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>