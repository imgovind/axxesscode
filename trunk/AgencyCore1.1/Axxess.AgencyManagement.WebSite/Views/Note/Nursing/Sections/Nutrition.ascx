﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
<fieldset>
	<legend>Nutrition</legend>
	<div class="column">
		<div class="row">
			<%= Html.Hidden(Model.Type + "_GenericNutrition", string.Empty, new { @id = Model.Type + "_GenericNutritionHidden" })%>
			<ul class="checkgroup one-wide">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition1">WNL (Within Normal Limits)</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition3">Decreased Appetite</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition2">Dysphagia</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition4">Weight</label>
					</div>
					<div class="more">
						<ul class="checkgroup two-wide">
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id ='{0}_GenericNutritionWeightLoss' type='radio' value='Loss' name='{0}_GenericNutritionWeightGainLoss' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionWeightLoss">Loss</label>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id ='{0}_GenericNutritionWeightGain' type='radio' value='Gain' name='{0}_GenericNutritionWeightGainLoss' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionWeightGain">Gain</label>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition5">Diet</label>
					</div>
					<div class="more">
						<ul class="checkgroup two-wide">
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericNutritionDietAdequate' type='radio' value='Adequate' name='{0}_GenericNutritionDietAdequate' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionDietAdequate">Adequate</label>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericNutritionDietInadequate' type='radio' value='Inadequate' name='{0}_GenericNutritionDietAdequate' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionDietInadequate">Inadequate</label>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("9").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition9">Diet Type</label>
					</div>
					<div class="more">
                        <label for="<%= Model.Type %>_GenericNutritionDietType" class="fl">Specify</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.Type + "_GenericNutritionDietType" })%></div>
                        <div class="clr"></div>
                    </div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("6").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition6">Enteral Feeding</label>
					</div>
					<div class="more">
						<ul class="checkgroup three-wide">
							<%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
							<%= Html.Hidden(Model.Type + "_GenericNutritionEnteralFeeding", string.Empty, new { @id = Model.Type + "_GenericNutritionEnteralFeedingHidden" })%>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionEnteralFeeding1">NG</label>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionEnteralFeeding2">PEG</label>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericNutritionEnteralFeeding3">Dobhoff</label>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("7").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition7">Tube Placement Checked</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("8").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNutrition8">Residual Checked</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNutritionResidualCheckedAmount" class="fl">Amount</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.Type + "_GenericNutritionResidualCheckedAmount", @class = "decimal" }) %>
							<label for="<%= Model.Type %>_GenericNutritionResidualCheckedAmount">ml</label>
						</div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericNutritionComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericNutritionComment", data.AnswerOrEmptyString("GenericNutritionComment"), new { @id = Model.Type + "_GenericNutritionComment" })%></div>
		</div>
	</div>
</fieldset>