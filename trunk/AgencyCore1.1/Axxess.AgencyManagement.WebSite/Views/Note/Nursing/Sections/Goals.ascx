﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] goals = data.AnswerArray("GenericGoals"); %>
<fieldset>
	<legend>Goals</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsGoalsApplied", string.Empty, new { @id = Model.Type + "_GenericIsGoalsAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsGoalsApplied", Model.Type + "_GenericIsGoalsApplied1", "1", data.AnswerArray("GenericIsGoalsApplied").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="wide-column">
			<div class="row">
				<ul class="checkgroup one-wide">
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='1' name='{0}_GenericGoals' id='{0}_GenericGoals1' {1}>", Model.Type, goals.Contains("1").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals1">Make daily social contacts as evidenced by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals1Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals1Weeks", data.AnswerOrEmptyString("GenericGoals1Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='2' name='{0}_GenericGoals' id='{0}_GenericGoals2' {1}>", Model.Type, goals.Contains("2").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals2">Exhibit stable weight, nutrition, hydration status w/weight gain of <em class="underline">field 1</em> lbs by <em class="underline">field 2</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals2Weight" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals2Weight", data.AnswerOrEmptyString("GenericGoals2Weight"), new { }) %></div>
						    <label for="<%= Model.Type %>_GenericGoals2Weeks" class="fl">Field 2</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals2Weeks", data.AnswerOrEmptyString("GenericGoals2Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='3' name='{0}_GenericGoals' id='{0}_GenericGoals3' {1}>", Model.Type, goals.Contains("3").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals3">Improve interpersonal relationships by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals3Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals3Weeks", data.AnswerOrEmptyString("GenericGoals3Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='4' name='{0}_GenericGoals' id='{0}_GenericGoals4' {1}>", Model.Type, goals.Contains("4").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals4">Demonstrate coping strategies by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals4Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals4Weeks", data.AnswerOrEmptyString("GenericGoals4Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='5' name='{0}_GenericGoals' id='{0}_GenericGoals5' {1}>", Model.Type, goals.Contains("5").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals5">Decrease neurotic behavior by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals5Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals5Weeks", data.AnswerOrEmptyString("GenericGoals5Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='6' name='{0}_GenericGoals' id='{0}_GenericGoals6' {1}>", Model.Type, goals.Contains("6").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals6">Verbalize a decrease in depression by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals6Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals6Weeks", data.AnswerOrEmptyString("GenericGoals6Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='7' name='{0}_GenericGoals' id='{0}_GenericGoals7' {1}>", Model.Type, goals.Contains("7").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals7">Verbalize absense of suicidal ideation, intent and plan by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals6Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals6Weeks", data.AnswerOrEmptyString("GenericGoals7Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='8' name='{0}_GenericGoals' id='{0}_GenericGoals8' {1}>", Model.Type, goals.Contains("8").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals8">Will not harm self as evidenced by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals8Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals8Weeks", data.AnswerOrEmptyString("GenericGoals8Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='9' name='{0}_GenericGoals' id='{0}_GenericGoals9' {1}>", Model.Type, goals.Contains("9").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals9">Verbalize absense of violent ideation by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals9Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals9Weeks", data.AnswerOrEmptyString("GenericGoals9Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='10' name='{0}_GenericGoals' id='{0}_GenericGoals10' {1}>", Model.Type, goals.Contains("10").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals10">Verbalize s/sx suicidality, crisis intervention, when to call physician/911 by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals10Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals10Weeks", data.AnswerOrEmptyString("GenericGoals10Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='11' name='{0}_GenericGoals' id='{0}_GenericGoals11' {1}>", Model.Type, goals.Contains("11").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals11">Exhibit elevated mood as evidenced by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals11Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals11Weeks", data.AnswerOrEmptyString("GenericGoals11Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='12' name='{0}_GenericGoals' id='{0}_GenericGoals12' {1}>", Model.Type, goals.Contains("12").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals12">Demonstrate 2 coping skills as evidenced by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals12Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals12Weeks", data.AnswerOrEmptyString("GenericGoals12Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='13' name='{0}_GenericGoals' id='{0}_GenericGoals13' {1}>", Model.Type, goals.Contains("13").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals13">Make daily social contacts as evidenced by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals13Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals13Weeks", data.AnswerOrEmptyString("GenericGoals13Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='14' name='{0}_GenericGoals' id='{0}_GenericGoals14' {1}>", Model.Type, goals.Contains("14").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals14">Exhibit goal directed thoughts as evidence by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals14Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals14Weeks", data.AnswerOrEmptyString("GenericGoals14Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='15' name='{0}_GenericGoals' id='{0}_GenericGoals15' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals15").IsNotNullOrEmpty() || goals.Contains("15")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals15">Maintain stable weight, nutrition, hydration status w/weight gain of <em class="underline">field 1</em> lbs by <em class="underline">field 2</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals15Weight" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals15Weight", data.AnswerOrEmptyString("GenericGoals15Weight"), new { }) %></div>
						    <label for="<%= Model.Type %>_GenericGoals15Weeks" class="fl">Field 2</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals15Weeks", data.AnswerOrEmptyString("GenericGoals15Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='16' name='{0}_GenericGoals' id='{0}_GenericGoals16' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals16").IsNotNullOrEmpty() || goals.Contains("16")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals16">Achieve sx control of CV &amp; CP status w/meds &amp; relaxation skills AEB by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals6Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals6Weeks", data.AnswerOrEmptyString("GenericGoals16Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='17' name='{0}_GenericGoals' id='{0}_GenericGoals17' {1}>", Model.Type, goals.Contains("17").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals17">Achieve GI/GU managements as evidence by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals17Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals17Weeks", data.AnswerOrEmptyString("GenericGoals17Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='18' name='{0}_GenericGoals' id='{0}_GenericGoals18' {1}>", Model.Type, goals.Contains("18").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals18">Verbalize bowel management as evidence by: <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals18Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals18Weeks", data.AnswerOrEmptyString("GenericGoals18Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='19' name='{0}_GenericGoals' id='{0}_GenericGoals19' {1}>", Model.Type, goals.Contains("19").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals19">Verbalize and achieve symptom control of sleep disturbance of <em class="underline">field 1</em> h/noc by <em class="underline">field 2</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals19Noc" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals19Noc", data.AnswerOrEmptyString("GenericGoals19Noc"), new { }) %></div>
						    <label for="<%= Model.Type %>_GenericGoals19Weeks" class="fl">Field 2</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals19Weeks", data.AnswerOrEmptyString("GenericGoals19Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='20' name='{0}_GenericGoals' id='{0}_GenericGoals20' {1}>", Model.Type, goals.Contains("20").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals20">Exhibit control of anxiety w/med &amp; relaxation skills AEB by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals20Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals20Weeks", data.AnswerOrEmptyString("GenericGoals20Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='21' name='{0}_GenericGoals' id='{0}_GenericGoals21' {1}>", Model.Type, goals.Contains("21").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals21">Exhibit control of thought disorder as evidenced by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals21Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals21Weeks", data.AnswerOrEmptyString("GenericGoals21Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='22' name='{0}_GenericGoals' id='{0}_GenericGoals22' {1}>", Model.Type, goals.Contains("22").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals22">Achieve mobility/safety management as evidenced by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals22Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals22Weeks", data.AnswerOrEmptyString("GenericGoals22Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='23' name='{0}_GenericGoals' id='{0}_GenericGoals23' {1}>", Model.Type, goals.Contains("23").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals23">Achieve maximum level of self-care as evidenced by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals23Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals23Weeks", data.AnswerOrEmptyString("GenericGoals23Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='24' name='{0}_GenericGoals' id='{0}_GenericGoals24' {1}>", Model.Type, goals.Contains("24").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals24">Verbalized medications, use schedule &amp; side effecs and patient will take as ordered by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals24Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals24Weeks", data.AnswerOrEmptyString("GenericGoals24Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='25' name='{0}_GenericGoals' id='{0}_GenericGoals25' {1}>", Model.Type, goals.Contains("25").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals25">Verbalize adequate knowledge of disease process &amp; know when to notify physician by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals25Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals25Weeks", data.AnswerOrEmptyString("GenericGoals25Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='26' name='{0}_GenericGoals' id='{0}_GenericGoals26' {1}>", Model.Type, goals.Contains("26").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals26">97 Verbalize goal-directed thoughts, really based orientation &#38;; congruent thinking by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals26Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals26Weeks", data.AnswerOrEmptyString("GenericGoals26Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='27' name='{0}_GenericGoals' id='{0}_GenericGoals27' {1}>", Model.Type, goals.Contains("27").ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals27">Exhibit decreased hyperactivity &#38; safe behaviors as evidence by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals27Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals27Weeks", data.AnswerOrEmptyString("GenericGoals27Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='28' name='{0}_GenericGoals' id='{0}_GenericGoals28' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals28").IsNotNullOrEmpty() || goals.Contains("28")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals28">Refrain from boastful/delusional behaviors &#38; from interrupting conversations by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals28Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals28Weeks", data.AnswerOrEmptyString("GenericGoals28Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='29' name='{0}_GenericGoals' id='{0}_GenericGoals29' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals29").IsNotNullOrEmpty() || goals.Contains("29")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals29">Demonstrate positive coping mechanisms &amp; verbalize 2 realistic goals by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals29Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals29Weeks", data.AnswerOrEmptyString("GenericGoals29Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='30' name='{0}_GenericGoals' id='{0}_GenericGoals30' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals30").IsNotNullOrEmpty() || goals.Contains("30")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals30">Experience no untoward ECT complications by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals17Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals17Weeks", data.AnswerOrEmptyString("GenericGoals30Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='31' name='{0}_GenericGoals' id='{0}_GenericGoals31' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals32").Contains("31") || goals.Contains("31")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals31">Verbalize 2 management techniques of disease by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals31Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals31Weeks", data.AnswerOrEmptyString("GenericGoals31Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input type='checkbox' value='32' name='{0}_GenericGoals' id='{0}_GenericGoals32' {1}>", Model.Type, (data.AnswerOrEmptyString("GenericGoals32").Contains("32") || goals.Contains("32")).ToChecked())%>
							<label for="<%= Model.Type %>_GenericGoals32">Verbalize 2 s/sx of illness by <em class="underline">field 1</em> weeks.</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericGoals32Weeks" class="fl">Field 1</label>
							<div class="fr"><%= Html.TextBox(Model.Type + "_GenericGoals32Weeks", data.AnswerOrEmptyString("GenericGoals32Weeks"), new { }) %></div>
							<div class="clr"></div>
						</div>
					</li>
					<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericGoals", "33", data.AnswerOrEmptyString("GenericGoals33").IsNotNullOrEmpty() || goals.Contains("33"), "Other", Model.Type + "_GenericGoalsOther", data.AnswerOrEmptyString("GenericGoalsOther"))%>
				</ul>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericGoalsComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericGoalsComment", data.AnswerOrEmptyString("GenericGoalsComment"), new { @id = Model.Type + "_GenericGoalsComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
