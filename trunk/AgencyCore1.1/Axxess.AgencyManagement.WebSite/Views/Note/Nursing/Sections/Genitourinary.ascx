﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Genitourinary</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%  string[] genericGU = data.AnswerArray("GenericGU"); %>
				<%= Html.Hidden(Model.Type + "_GenericGU", string.Empty, new { @id = Model.Type + "_GenericGUHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "1", genericGU.Contains("1"), "WNL (Within Normal Limits)")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "2", genericGU.Contains("2"), "Incontinence")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "3", genericGU.Contains("3"), "Bladder Distention")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "4", genericGU.Contains("4"), "Discharge")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "5", genericGU.Contains("5"), "Frequency")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "6", genericGU.Contains("6"), "Dysuria")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "7", genericGU.Contains("7"), "Retention")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "8", genericGU.Contains("8"), "Urgency")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericGU", "9", genericGU.Contains("9"), "Oliguria")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericGU10' name='{0}_GenericGU' value='10' type='checkbox' {1} />", Model.Type, genericGU.Contains("10").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericGU10">Catheter/Device</label>
					</div>
					<div class="more">
                        <label for="<%= Model.Type %>_GenericGUCatheterList" class="fl">Device Type</label>
						<div class="fr">
							<%  var genericGUCatheterList = new SelectList(new[] {
						            new SelectListItem { Text = "", Value = "" },
						            new SelectListItem { Text = "N/A", Value = "N/A" },
						            new SelectListItem { Text = "Foley Catheter ", Value = "Foley Catheter" },
						            new SelectListItem { Text = "Condom Catheter", Value = "Condom Catheter" },
						            new SelectListItem { Text = "Suprapubic Catheter", Value = "Suprapubic Catheter" },
						            new SelectListItem { Text = "Urostomy", Value = "Urostomy" },
						            new SelectListItem { Text = "Other", Value = "Other" }
					            }, "Value", "Text", data.AnswerOrDefault("GenericGUCatheterList", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericGUCatheterList", genericGUCatheterList, new { }) %>
						</div>
						<div class="clr"></div>
						<label for="<%= Model.Type %>_GenericGUCatheterLastChanged" class="fl">Last Changed</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericGUCatheterLastChanged", data.AnswerOrEmptyString("GenericGUCatheterLastChanged"), new { @id = Model.Type + "_GenericGUCatheterLastChanged", @maxlength = "10", @class = "date-picker" }) %></div>
						<div class="clr"></div>
						<label for="<%= Model.Type %>_GenericGUCatheterFrequency" class="fl">Size</label>
						<div class="fr">
							<%= Html.TextBox(Model.Type + "_GenericGUCatheterFrequency", data.AnswerOrEmptyString("GenericGUCatheterFrequency"), new { @id = Model.Type + "_GenericGUCatheterFrequency", @class = "shorter", @maxlength = "5" }) %>
							<label for="<%= Model.Type %>_GenericGUCatheterFrequency">Fr</label>
							<%= Html.TextBox(Model.Type + "_GenericGUCatheterAmount", data.AnswerOrEmptyString("GenericGUCatheterAmount"), new { @id = Model.Type + "_GenericGUCatheterAmount", @class = "shorter", @maxlength = "5" }) %>
							<label for="<%= Model.Type %>_GenericGUCatheterAmount">ml</label>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericGU11' name='{0}_GenericGU' value='11' type='checkbox' {1} />", Model.Type, genericGU.Contains("11").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericGU11">Urine</label>
					</div>
					<div class="more">
						<%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
						<%= Html.Hidden(Model.Type + "_GenericGUUrine", string.Empty, new { @id = Model.Type + "_GenericGUUrineHidden" })%>
						<ul class="checkgroup">
            				<%= Html.CheckgroupOption(Model.Type + "_GenericGUUrine", "6", genericGUUrine.Contains("6"), "Clear")%>
            				<%= Html.CheckgroupOption(Model.Type + "_GenericGUUrine", "1", genericGUUrine.Contains("1"), "Cloudy")%>
            				<%= Html.CheckgroupOption(Model.Type + "_GenericGUUrine", "2", genericGUUrine.Contains("2"), "Odorous")%>
            				<%= Html.CheckgroupOption(Model.Type + "_GenericGUUrine", "3", genericGUUrine.Contains("3"), "Sediment")%>
            				<%= Html.CheckgroupOption(Model.Type + "_GenericGUUrine", "4", genericGUUrine.Contains("4"), "Hematuria")%>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericGUUrine5' name='{0}_GenericGUUrine' value='5' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("5").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericGUUrine5">Other</label>
								</div>
								<div class="more">
                                    <label for="<%= Model.Type %>_GenericGUOtherText" class="fl">Specify</label>
                                    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericGUOtherText", data.AnswerOrEmptyString("GenericGUOtherText"), new { @id = Model.Type + "_GenericGUOtherText", @maxlength = "20" }) %></div>
                                    <div class="clr"></div>
								</div>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericGenitourinaryComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericGenitourinaryComment", data.AnswerOrEmptyString("GenericGenitourinaryComment"), new { @id = Model.Type + "_GenericGenitourinaryComment" })%></div>
		</div>
	</div>
</fieldset>
