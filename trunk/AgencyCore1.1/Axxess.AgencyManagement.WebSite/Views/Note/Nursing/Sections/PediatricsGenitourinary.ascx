﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Genitourinary</legend>
    <div class="column">
        <div class="row">
            <label class="fl">Catheter</label>
        </div>
        <div class="sub row">
	        <ul class="checkgroup two-wide">
                <% string[] guCatheter = data.AnswerArray("GUCatheter"); %>
                <%= Html.Hidden(Model.Type + "_GUCatheter", string.Empty, new { @id = Model.Type + "_GUCatheter" })%>
                <%= Html.CheckgroupOption(Model.Type + "_GUCatheter", "0", guCatheter.Contains("0"), "Foley")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_GUCatheter", "1", guCatheter.Contains("1"), "Other", Model.Type + "_GUCatheterOther", data.AnswerOrEmptyString("GUCatheterOther"))%>
            </ul>
        </div>
        <div class="sub row">
            <label class="fl">Type/Size</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GUTypeSize", data.AnswerOrEmptyString("GUTypeSize"), new { @id = Model.Type + "_GUTypeSize", @maxlength = "30" })%></div>
        </div>
        <div class="sub row">
            <label class="fl">Last changed</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GULastChanged", data.AnswerOrEmptyString("GULastChanged"), new { @id = Model.Type + "_GULastChanged",@maxlength = "30" })%></div>
        </div>
        <div class="row">
            <label class="fl">Ostomy</label>
        </div>
        <div class="sub row">
            <label class="fl">Type/Appliance</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GUTypeAppliance", data.AnswerOrEmptyString("GUTypeAppliance"), new { @id = Model.Type + "_GUTypeAppliance", @maxlength = "30" })%></div>
        </div>
        <div class="sub row">
            <ul class="checkgroup two-wide">
                <% string[] guOstomy = data.AnswerArray("GUOstomy"); %>
                <%= Html.Hidden(Model.Type + "_GUOstomy", string.Empty, new { @id = Model.Type + "_GUOstomy" })%>
                <%= Html.CheckgroupOption(Model.Type + "_GUOstomy", "0", guOstomy.Contains("0"), "Diapers")%>
                <%= Html.CheckgroupOption(Model.Type + "_GUOstomy", "1", guOstomy.Contains("1"), "Training pants")%>
                <%= Html.CheckgroupOption(Model.Type + "_GUOstomy", "2", guOstomy.Contains("2"), "Toilet trained")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_IntegComment" class="fl">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GUComments", data.AnswerOrEmptyString("GUComments"), new { @id = Model.Type + "_GUComments", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
