﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Diabetic Care</legend>
	<div class="column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsDiabeticCareApplied", string.Empty, new { @id = Model.Type + "_GenericIsDiabeticCareAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsDiabeticCareApplied", Model.Type + "_GenericIsDiabeticCareApplied1", "1", data.AnswerArray("GenericIsDiabeticCareApplied").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row no-input">
				<label>Blood Sugar</label>
			</div>
			<div class="sub row">
				<label for="<%= Model.Type %>_GenericBloodSugarAMLevelText" class="fl">AM</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericBloodSugarAMLevelText", data.AnswerOrEmptyString("GenericBloodSugarAMLevelText"), new { @class = "shorter less", @id = Model.Type + "_GenericBloodSugarAMLevelText" })%>
					<label for="<%= Model.Type %>_GenericBloodSugarAMLevel">mg/dl</label>
					<%  var diabeticCareBsAm = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "Random", Value = "Random" },
							new SelectListItem { Text = "Fasting", Value = "Fasting" }
						}, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarAMLevel", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericBloodSugarAMLevel", diabeticCareBsAm, new { @id = Model.Type + "_GenericBloodSugarAMLevel", @class = "short less" })%>
				</div>
			</div>
			<div class="sub row">
				<label for="<%= Model.Type %>_GenericBloodSugarLevelText" class="fl">Noon</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericBloodSugarLevelText", data.AnswerOrEmptyString("GenericBloodSugarLevelText"), new { @class = "shorter less", @id = Model.Type + "_GenericBloodSugarLevelText" })%>
					<label for="<%= Model.Type %>_GenericBloodSugarLevel">mg/dl</label>
					<%  var diabeticCareBsNoon = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "Random", Value = "Random" },
							new SelectListItem { Text = "Fasting", Value = "Fasting" }
						}, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarLevel", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericBloodSugarLevel", diabeticCareBsNoon, new { @id = Model.Type + "_GenericBloodSugarLevel", @class = "short less" })%>
				</div>
			</div>
			<div class="sub row">
				<label for="<%= Model.Type %>_GenericBloodSugarPMLevelText" class="fl">PM</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericBloodSugarPMLevelText", data.AnswerOrEmptyString("GenericBloodSugarPMLevelText"), new { @class = "shorter less", @id = Model.Type + "_GenericBloodSugarPMLevelText" })%>
					<label for="<%= Model.Type %>_GenericBloodSugarPMLevel">mg/dl</label>
					<%  var diabeticCareBsPm = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "Random", Value = "Random" },
							new SelectListItem { Text = "Fasting", Value = "Fasting" }
						}, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarPMLevel", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericBloodSugarPMLevel", diabeticCareBsPm, new { @id = Model.Type + "_GenericBloodSugarPMLevel", @class = "short less" })%>
				</div>
			</div>
			<div class="sub row">
				<label for="<%= Model.Type %>_GenericBloodSugarHSLevelText" class="fl">HS</label>
				<div class="fr">
					<%= Html.TextBox(Model.Type + "_GenericBloodSugarHSLevelText", data.AnswerOrEmptyString("GenericBloodSugarHSLevelText"), new { @class = "shorter less", @id = Model.Type + "_GenericBloodSugarHSLevelText" })%>
					<label for="<%= Model.Type %>_GenericBloodSugarHSLevel">mg/dl</label>
					<%  var diabeticCareBsHs = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "Random", Value = "Random" },
							new SelectListItem { Text = "Fasting", Value = "Fasting" }
						}, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarHSLevel", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericBloodSugarHSLevel", diabeticCareBsHs, new { @id = Model.Type + "_GenericBloodSugarHSLevel", @class = "short less" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericBloodSugarCheckedBy" class="fl">Performed by</label>
				<div class="fr">
					<%  var diabeticCarePerformedby = new SelectList(new[] {
							new SelectListItem { Text = "", Value = "" },
							new SelectListItem { Text = "Patient", Value = "Patient" },
							new SelectListItem { Text = "SN", Value = "SN" },
							new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
						}, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarCheckedBy", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = Model.Type + "_GenericBloodSugarCheckedBy" }) %>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericBloodSugarSiteText" class="fl">Site</label>
				<div class="fr"><%= Html.TextBox(Model.Type + "_GenericBloodSugarSiteText", data.AnswerOrEmptyString("GenericBloodSugarSiteText"), new { @id = Model.Type + "_GenericBloodSugarSiteText" })%></div>
                <div class="clr"></div>
                <div class="fr">
					<%= Html.Hidden(Model.Type + "_GenericBloodSugarSite") %>
                    <ul class="checkgroup two-wide">
                        <li class="option">
                            <div class="wrapper">
						        <%= string.Format("<input id ='{0}_GenericBloodSugarLevelRandom' type='radio' value='Left' name='{0}_GenericBloodSugarSite' {1} />", Model.Type, data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Left").ToChecked()) %>
						        <label for="<%= Model.Type %>_GenericBloodSugarLevelRandom">Left</label>
                            </div>
                        </li>
                        <li class="option">
                            <div class="wrapper">
						        <%= string.Format("<input id ='{0}_GenericBloodSugarLevelFasting' type='radio' value='Right' name='{0}_GenericBloodSugarSite' {1} />", Model.Type, data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Right").ToChecked())%>
						        <label for="<%= Model.Type %>_GenericBloodSugarLevelFasting">Right</label>
                            </div>
                        </li>
                    </ul>
				</div>
			</div>
			<div class="row">
				<label>Diabetic Management</label>
				<ul class="checkgroup two-wide">
					<%  string[] diabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
					<%= Html.Hidden(Model.Type + "_GenericDiabeticCareDiabeticManagement", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareDiabeticManagementHidden" })%>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement1' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='1' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("1").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement1">Diet</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement2' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='2' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("2").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement2">Oral Hypoglycemic</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement3' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='3' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("3").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement3">Exercise</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement4' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='4' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("4").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement4">Insulin</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="row">
				<label>Insulin Administered by</label>
				<ul class="checkgroup two-wide">
					<%  string[] diabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
					<%= Html.Hidden(Model.Type + "_GenericDiabeticCareInsulinAdministeredby", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareInsulinAdministeredbyHidden" })%>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby1' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='1' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("1").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby1">N/A</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby2' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='2' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("2").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby2">Patient</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby3' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='3' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("3").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby3">Caregiver</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby4' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='4' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("4").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby4">SN</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="row">
				<label>S&#38;S of Hyperglycemia</label>
				<ul class="checkgroup two-wide">
					<%  string[] genericHyperglycemia = data.AnswerArray("GenericDiabeticCareHyperglycemia"); %>
					<%= Html.Hidden(Model.Type + "_GenericDiabeticCareHyperglycemia", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareHyperglycemiaHidden" })%>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia1' name='{0}_GenericDiabeticCareHyperglycemia' value='1' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("1").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia1">Fatigue</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia2' name='{0}_GenericDiabeticCareHyperglycemia' value='2' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("2").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia2">Blurred Vision</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia3' name='{0}_GenericDiabeticCareHyperglycemia' value='3' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("3").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia3">Polydipsia</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia4' name='{0}_GenericDiabeticCareHyperglycemia' value='4' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("4").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia4">Polyuria</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia5' name='{0}_GenericDiabeticCareHyperglycemia' value='5' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("5").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia5">Polyphagia</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia6' name='{0}_GenericDiabeticCareHyperglycemia' value='6' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("6").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia6">Other</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemiaOther" class="fl">Specify</label>
						    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericDiabeticCareHyperglycemiaOther", data.AnswerOrEmptyString("GenericDiabeticCareHyperglycemiaOther"), new { @id = Model.Type + "_GenericDiabeticCareHyperglycemiaOther" })%></div>
						    <div class="clr"></div>
						</div>
					</li>
				</ul>
			</div>
			<div class="row">
				<label>S&#38;S of Hypoglycemia</label>
				<ul class="checkgroup two-wide">
					<%  string[] genericHypoglycemia = data.AnswerArray("GenericDiabeticCareHypoglycemia"); %>
					<%= Html.Hidden(Model.Type + "_GenericDiabeticCareHypoglycemia", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareHypoglycemiaHidden" })%>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia1' name='{0}_GenericDiabeticCareHypoglycemia' value='1' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("1").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia1">Anxious</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia2' name='{0}_GenericDiabeticCareHypoglycemia' value='2' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("2").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia2">Dizziness</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia3' name='{0}_GenericDiabeticCareHypoglycemia' value='3' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("3").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia3">Fatigue</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia4' name='{0}_GenericDiabeticCareHypoglycemia' value='4' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("4").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia4">Perspiration</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia5' name='{0}_GenericDiabeticCareHypoglycemia' value='5' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("5").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia5">Weakness</label>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia6' name='{0}_GenericDiabeticCareHypoglycemia' value='6' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("6").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia6">Other</label>
						</div>
						<div class="more">
						    <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemiaOther" class="fl">Specify</label>
						    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericDiabeticCareHypoglycemiaOther", data.AnswerOrEmptyString("GenericDiabeticCareHypoglycemiaOther"), new { @id = Model.Type + "_GenericDiabeticCareHypoglycemiaOther" })%></div>
						    <div class="clr"></div>
						</div>
					</li>
				</ul>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericInsulinType1">Insulin</label>
				<div class="ac">
                    <label for="<%= Model.Type %>_GenericInsulinType1">Type</label>
                    <%= Html.TextBox(Model.Type + "_GenericInsulinType1", data.AnswerOrEmptyString("GenericInsulinType1"), new { @id = Model.Type + "_GenericInsulinType1", @class = "short" })%>
				    <label for="<%= Model.Type %>GenericInsulinDose1">Dose</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinDose1", data.AnswerOrEmptyString("GenericInsulinDose1"), new { @id = Model.Type + "_GenericInsulinDose1", @class = "shortest" })%>
				    <label for="<%= Model.Type %>GenericInsulinSite1">Site</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinSite1", data.AnswerOrEmptyString("GenericInsulinSite1"), new { @id = Model.Type + "_GenericInsulinSite1", @class = "shorter" })%>
				    <label for="<%= Model.Type %>GenericInsulinRoute1">Route</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinRoute1", data.AnswerOrEmptyString("GenericInsulinRoute1"), new { @id = Model.Type + "_GenericInsulinRoute1", @class = "shorter" })%>
                </div>
			    <div class="ac">
				    <label for="<%= Model.Type %>_GenericInsulinType2">Type</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinType2", data.AnswerOrEmptyString("GenericInsulinType2"), new { @id = Model.Type + "_GenericInsulinType2", @class = "short" })%>
				    <label for="<%= Model.Type %>GenericInsulinDose2">Dose</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinDose2", data.AnswerOrEmptyString("GenericInsulinDose2"), new { @id = Model.Type + "_GenericInsulinDose2", @class = "shortest" })%>
				    <label for="<%= Model.Type %>GenericInsulinSite2">Site</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinSite2", data.AnswerOrEmptyString("GenericInsulinSite2"), new { @id = Model.Type + "_GenericInsulinSite2", @class = "shorter" })%>
				    <label for="<%= Model.Type %>GenericInsulinRoute2">Route</label>
				    <%= Html.TextBox(Model.Type + "_GenericInsulinRoute2", data.AnswerOrEmptyString("GenericInsulinRoute2"), new { @id = Model.Type + "_GenericInsulinRoute2", @class = "shorter" })%>
			    </div>
            </div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericDiabeticCareComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericDiabeticCareComment", data.AnswerOrEmptyString("GenericDiabeticCareComment"), new { @id = Model.Type + "_GenericDiabeticCareComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
    $("#<%= Model.Type %>_GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
</script>

