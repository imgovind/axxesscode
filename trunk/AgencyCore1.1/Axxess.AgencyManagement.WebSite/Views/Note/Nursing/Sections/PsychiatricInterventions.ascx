﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Interventions</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup two-wide">
				<%  string[] interventions = data.AnswerArray("GenericPsychiatricInterventions"); %>
				<%= Html.Hidden(Model.Type + "_GenericPsychiatricInterventions", string.Empty, new { @id = Model.Type + "_GenericPsychiatricInterventionsHidden" })%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "1", interventions.Contains("1"), "Suicidal and safety precautions")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "2", interventions.Contains("2"), "Relaxation, imagery and deep breathing exercises")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "3", interventions.Contains("3"), "Problem solving, positive coping, decision making and stress management technique")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "4", interventions.Contains("4"), "Recognition of s/sx complications of crisis and when to call MD")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "5", interventions.Contains("5"), "Reality/congruent thinking techniques")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "6", interventions.Contains("6"), "Anger management")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "7", interventions.Contains("7"), "Emergency and crisis intervention")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "8", interventions.Contains("8"), "Recognition of thoughts and verbally express painful ones")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "9", interventions.Contains("9"), "Recognition of cardiovascular and neurological side effects of medication")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "10", interventions.Contains("10"), "Ability to focus thoughts on feelings and verbally express painful ones")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "11", interventions.Contains("11"), "Positive feedback to reality and realistic feelings")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "12", interventions.Contains("12"), "Importance of supportive thearpy, reality testing, and positive feedback, validation and confrontation")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "13", interventions.Contains("13"), "Relaxation and stress management techniques")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "14", interventions.Contains("14"), "Relationship between feelings and behavior, impulse control behaviors")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "15", interventions.Contains("15"), "Entry back into community and importance of interacting with others in the environment")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "16", interventions.Contains("16"), "Grieving process and bereavement counseling")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "17", interventions.Contains("17"), "Calming techniques for agitation")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "18", interventions.Contains("18"), "Instructs time planning skills to prevent being overwhelmed")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "19", interventions.Contains("19"), "Instruct recognition of exacerbation of illness, hallucinations, and delusions, inappropriate thought patterns and disorganization")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "20", interventions.Contains("20"), "Instruct exploration of painful or anxious feelings and/or identifying ambivalent feelings")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "21", interventions.Contains("21"), "Instruct importance of providing positive reinforcement for positive actions")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "22", interventions.Contains("22"), "Instruct need for concrete realites and focus on thoughts")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "23", interventions.Contains("23"), "Instruct need for supportive psychotherapist")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "25", interventions.Contains("25"), "Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria&#8217;s, grandiosity, depression and/or despondence")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "26", interventions.Contains("26"), "Instruct in s/sx of lithium toxicity")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "27", interventions.Contains("27"), "Instruct positive coping skills to deal with disease and symptoms")%>
                <%= Html.CheckgroupOption(Model.Type + "_GenericMotorComponents", "28", interventions.Contains("28"), "Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria&#8217;s, grandiosity, depression and/or despondence")%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericPsychiatricInterventionsComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericPsychiatricInterventionsComment", data.AnswerOrEmptyString("GenericPsychiatricInterventionsComment"), new { @id = Model.Type + "_GenericPsychiatricInterventionsComment" })%></div>
		</div>
	</div>
</fieldset>
