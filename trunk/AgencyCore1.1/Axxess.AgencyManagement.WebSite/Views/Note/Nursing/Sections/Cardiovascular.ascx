﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
<%= Html.Hidden(Model.Type + "_GenericCardioVascular", string.Empty, new { @id = Model.Type + "_GenericCardioVascularHidden" })%>
<fieldset>
	<legend>Cardiovascular</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
			    <%= Html.CheckgroupOption(Model.Type + "_GenericCardioVascular", "1", genericCardioVascular.Contains("1"), "WNL (Within Normal Limits)")%>
			    <li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular2' name='{0}_GenericCardioVascular' value='2' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular2">Heart Rhythm</label>
					</div>
					<div class="more">
					    <label class="fl" for="<%= Model.Type %>_GenericHeartSoundsType">Type</label>
					    <div class="fr">
						    <%  var heartRhythm = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "0" },
								    new SelectListItem { Text = "Regular/WNL (Within Normal Limits)", Value = "1" },
								    new SelectListItem { Text = "Tachycardia", Value = "2" },
								    new SelectListItem { Text = "Bradycardia", Value = "3" },
								    new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
								    new SelectListItem { Text = "Other", Value = "5" }
							    }, "Value", "Text", data.AnswerOrDefault("GenericHeartSoundsType", "0")); %>
						    <%= Html.DropDownList(Model.Type + "_GenericHeartSoundsType", heartRhythm, new { @id = Model.Type + "_GenericHeartSoundsType" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular3' name='{0}_GenericCardioVascular' value='3' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular3">Cap. Refill</label>
					</div>
					<div class="more">
						<label class="fl" for="<%= Model.Type %>_GenericCapRefillLessThan3">Specify</label>
					    <div class="fr">
						    <%  var CapRefill = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "" },
								    new SelectListItem { Text = "<3 sec", Value = "1" },
								    new SelectListItem { Text = ">3 sec", Value = "0" }
							    }, "Value", "Text", data.AnswerOrEmptyString("GenericCapRefillLessThan3")); %>
						    <%= Html.DropDownList(Model.Type + "_GenericCapRefillLessThan3", CapRefill, new { @id = Model.Type + "_GenericCapRefillLessThan3" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular4' name='{0}_GenericCardioVascular' value='4' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular4">Pulses</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericCardiovascularRadial" class="fl">Radial</label>
						<%  var radialPulse = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "0" },
								new SelectListItem { Text = "1+ Weak", Value = "1" },
								new SelectListItem { Text = "2+ Normal", Value = "2" },
								new SelectListItem { Text = "3+ Strong", Value = "3" },
								new SelectListItem { Text = "4+ Bounding", Value = "4" }
							}, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularRadial", "0")); %>
						<div class="fr"><%= Html.DropDownList(Model.Type + "_GenericCardiovascularRadial", radialPulse, new { @id = Model.Type + "_GenericCardiovascularRadial" })%></div>
						<div class="clr"></div>
						<label class="fl" for="<%= Model.Type %>_GenericCardiovascularRadialPosition">Position</label>
						<div class="fr"><%= Html.EnumDropDown<VitalPosition>(Model.Type + "_GenericCardiovascularRadialPosition", true, new { value = data.AnswerOrEmptyString("GenericCardiovascularRadialPosition") })%></div>
						<div class="clr"></div>
						<label for="<%= Model.Type %>_GenericCardiovascularPedal" class="fl">Pedal</label>
						<% var pedalPulse = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "0" },
								new SelectListItem { Text = "1+ Weak", Value = "1" },
								new SelectListItem { Text = "2+ Normal", Value = "2" },
								new SelectListItem { Text = "3+ Strong", Value = "3" },
								new SelectListItem { Text = "4+ Bounding", Value = "4" }
							}, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularPedal", "0")); %>
						<div class="fr"><%= Html.DropDownList(Model.Type + "_GenericCardiovascularPedal", pedalPulse, new { @id = Model.Type + "_GenericCardiovascularPedal" }) %></div>
						<div class="clr"></div>
						<label class="fl" for="<%= Model.Type %>_GenericCardiovascularPedalPosition">Position</label>
						<div class="fr"><%= Html.EnumDropDown<VitalPosition>(Model.Type + "_GenericCardiovascularPedalPosition", true, new { value = data.AnswerOrEmptyString("GenericCardiovascularPedalPosition") })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular5' name='{0}_GenericCardioVascular' value='5' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular5">Edema</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericEdemaLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericEdemaLocation", data.AnswerOrEmptyString("GenericEdemaLocation"), new { @id = Model.Type + "_GenericEdemaLocation", @maxlength = "50" })%></div>
						<div class="clr"></div>
						<ul class="checkgroup one-wide">
							<%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
							<%= Html.Hidden(Model.Type + "_GenericPittingEdemaType", string.Empty, new { @id = Model.Type + "_GenericPittingEdemaTypeHidden" })%>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericPittingEdemaType1' name='{0}_GenericPittingEdemaType' value='1' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("1").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericPittingEdemaType1">Non-Pitting</label>
								</div>
								<div class="more">
									<label for="<%= Model.Type %>_GenericEdemaNonPitting" class="fl">Specify</label>
									<div class="fr">
										<%  var nonPitting = new SelectList(new[] {
											    new SelectListItem { Text = "", Value = "0" },
											    new SelectListItem { Text = "N/A", Value = "1" },
											    new SelectListItem { Text = "Mild", Value = "2" },
											    new SelectListItem { Text = "Moderate", Value = "3" },
											    new SelectListItem { Text = "Severe", Value = "4" }
										    }, "Value", "Text", data.AnswerOrDefault("GenericEdemaNonPitting", "0")); %>
										<%= Html.DropDownList(Model.Type + "_GenericEdemaNonPitting", nonPitting, new { @id = Model.Type + "_GenericEdemaNonPitting" }) %>
		    						</div>
		    						<div class="clr"></div>
									<ul class="checkgroup three-wide">
									    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericEdemaNonPittingPosition", "0", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0"), "Bilateral")%>
									    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericEdemaNonPittingPosition", "1", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1"), "Left")%>
									    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericEdemaNonPittingPosition", "2", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2"), "Right")%>
									</ul>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericPittingEdemaType2' name='{0}_GenericPittingEdemaType' value='2' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("2").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericPittingEdemaType2">Pitting</label>
								</div>
								<div class="more">
									<label for="<%= Model.Type %>_GenericEdemaPitting" class="fl">Specify</label>
									<div class="fr">
    									<%  var pitting = new SelectList(new[] {
	        									new SelectListItem { Text = "", Value = "0" },
			        							new SelectListItem { Text = "1+", Value = "1" },
					        					new SelectListItem { Text = "2+", Value = "2" },
							        			new SelectListItem { Text = "3+", Value = "3" },
									        	new SelectListItem { Text = "4+", Value = "4" }
        									}, "Value", "Text", data.AnswerOrDefault("GenericEdemaPitting", "0")); %>
		    							<%= Html.DropDownList(Model.Type + "_GenericEdemaPitting", pitting, new { @id = Model.Type + "_GenericEdemaPitting" }) %>
		    						</div>
		    						<div class="clr"></div>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular6' name='{0}_GenericCardioVascular' value='6' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("6").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular6">Heart Sound</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericHeartSound" class="fl">Specify</label>
						<div class="fr">
						    <%  var heartSound = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "0" },
								    new SelectListItem { Text = "Distant", Value = "1" },
								    new SelectListItem { Text = "Faint", Value = "2" },
								    new SelectListItem { Text = "Reg", Value = "3" },
								    new SelectListItem { Text = "Murmur", Value = "4" }
							    }, "Value", "Text", data.AnswerOrDefault("GenericHeartSound", "0")); %>
						    <%= Html.DropDownList(Model.Type + "_GenericHeartSound", heartSound, new { @id = Model.Type + "_GenericHeartSound" })%>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCardioVascular7' name='{0}_GenericCardioVascular' value='7' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("7").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCardioVascular7">Neck Veins</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeckVeins" class="fl">Specify</label>
						<div class="fr">
    						<%  var neckVeins = new SelectList(new[] {
	    							new SelectListItem { Text = "", Value = "0" },
		    						new SelectListItem { Text = "Flat", Value = "1" },
			    					new SelectListItem { Text = "Distended", Value = "2" },
				    				new SelectListItem { Text = "Cyanosis", Value = "3" },
					    			new SelectListItem { Text = "Other", Value = "4" }
						    	}, "Value", "Text", data.AnswerOrDefault("GenericNeckVeins", "0")); %>
						    <%= Html.DropDownList(Model.Type + "_GenericNeckVeins", neckVeins, new { @id = Model.Type + "_GenericNeckVeins" })%>
						</div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericCardiovascularComment" class="strong">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericCardiovascularComment", data.AnswerOrEmptyString("GenericCardiovascularComment"), new { @id = Model.Type + "_GenericCardiovascularComment", @class = "tall" })%></div>
		</div>
	</div>
</fieldset>