﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Pain</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Under 3 years of age</label>
		</div>
		<div class="row">
			<label class="al">Facial Expression</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<% string[] painFacialExpression = data.AnswerArray("PAINFacialExpression"); %>
					<%= Html.Hidden(Model.Type + "_PAINFacialExpression", string.Empty, new { @id = Model.Type + "_PAINFacialExpression" })%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINFacialExpression", "0", painFacialExpression.Contains("0"), "Relaxed")%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINFacialExpression", "1", painFacialExpression.Contains("1"), "Grimace")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="al">Cry</label>
			<ul class="checkgroup three-wide">
				<% string[] painCry = data.AnswerArray("PAINCry"); %>
				<%= Html.Hidden(Model.Type + "_PAINCry", string.Empty, new { @id = Model.Type + "_PAINCry" })%>
				<%= Html.CheckgroupOption(Model.Type + "_PAINCry", "0", painCry.Contains("0"), "No cry")%>
				<%= Html.CheckgroupOption(Model.Type + "_PAINCry", "1", painCry.Contains("1"), "Whimper")%>
				<%= Html.CheckgroupOption(Model.Type + "_PAINCry", "2", painCry.Contains("2"), "Vigorous")%>
			</ul>
		</div>
		<div class="row">
			<label class="al">Breathing patterns</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<% string[] painBreathing = data.AnswerArray("PAINBreathing"); %>
					<%= Html.Hidden(Model.Type + "_PAINBreathing", string.Empty, new { @id = Model.Type + "_PAINBreathing" })%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINBreathing", "0", painBreathing.Contains("0"), "Relaxed")%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINBreathing", "1", painBreathing.Contains("1"), "Changed")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="al">Arms</label>
			<div class="fr">
				<ul class="checkgroup two-wide longer">
					<% string[] painArms = data.AnswerArray("PAINArms"); %>
					<%= Html.Hidden(Model.Type + "_PAINArms", string.Empty, new { @id = Model.Type + "_PAINArms" })%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINArms", "0", painArms.Contains("0"), "Relaxed")%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINArms", "1", painArms.Contains("1"), "Flexed/extended")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="al">Legs</label>
			<div class="fr">
				<ul class="checkgroup two-wide longer">
					<% string[] painLegs = data.AnswerArray("PAINLegs"); %>
					<%= Html.Hidden(Model.Type + "_PAINLegs", string.Empty, new { @id = Model.Type + "_PAINLegs" })%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINLegs", "0", painLegs.Contains("0"), "Relaxed")%>
					<%= Html.CheckgroupOption(Model.Type + "_PAINLegs", "1", painLegs.Contains("1"), "Flexed/extended")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="al">State of Arousal</label>
			<ul class="checkgroup two-wide">
				<% string[] painState = data.AnswerArray("PAINState"); %>
				<%= Html.Hidden(Model.Type + "_PAINState", string.Empty, new { @id = Model.Type + "_PAINState" })%>
				<%= Html.CheckgroupOption(Model.Type + "_PAINState", "0", painState.Contains("0"), "Sleeping/awake")%>
				<%= Html.CheckgroupOption(Model.Type + "_PAINState", "1", painState.Contains("1"), "Fussy")%>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label class="fl">Over 3 years of age</label>
		</div>
		<div class="row ac">
			<img style="width:80%;height:80%" src="/Images/painscale.png" alt="Pain Scale Image" /><br />
			<em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
		</div>
		<div class="row">
			<label class="fl">Pain Level</label>
			<div class="fr">
				<%  var genericPainLevel = new SelectList(new[] {
                            new SelectListItem { Text = "------", Value = "" },
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", ""));%>
				<%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel" }) %>
			</div>
		</div>
		<div class="row">
			<label class="fl">Frequency</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_PAINFrequency", data.AnswerOrEmptyString("PAINFrequency"), new { @id = Model.Type + "_PAINFrequency",@maxlength = "30" })%></div>
		</div>
		<div class="row">
			<label class="fl">Location</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_PAINLocation", data.AnswerOrEmptyString("PAINLocation"), new { @id = Model.Type + "_PAINLocation",@maxlength = "30" })%></div>
		</div>
		<div class="row">
			<label class="fl">Pain relief measures</label>
			<div class="fr"><%= Html.TextBox(Model.Type + "_PAINReliefMeasures", data.AnswerOrEmptyString("PAINReliefMeasures"), new { @id = Model.Type + "_PAINReliefMeasures", @maxlength = "30" })%></div>
		</div>
		<div class="row">
			<label class="fl">Effective</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<% string[] painEffective = data.AnswerArray("PAINEffective"); %>
					<%= Html.Hidden(Model.Type + "_PAINEffective", string.Empty, new { @id = Model.Type + "_PAINEffective" })%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_PAINEffective", "1", painEffective.Contains("1"), "Yes")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_PAINEffective", "0", painEffective.Contains("0"), "No")%>
				</ul>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_PAINComments" class="fl">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_PAINComments", data.AnswerOrEmptyString("PAINComments"), new { @id = Model.Type + "_PAINComments", @class = "tall" })%></div>
		</div>
	</div>
</fieldset>
