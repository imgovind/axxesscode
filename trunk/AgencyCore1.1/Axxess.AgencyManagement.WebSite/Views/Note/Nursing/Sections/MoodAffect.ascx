﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Mood/Affect</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Overall Mood Status</label>
			<div class="fr">
				<%  var moodAffect = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericMoodAffect1", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericMoodAffect1", moodAffect, new { @id = Model.Type + "_GenericMoodAffect1" })%>
			</div>
		</div>
		<div class="row">
			<ul class="checkgroup two-wide">
				<% string[] genericMoodAffect2 = data.AnswerArray("GenericMoodAffect2"); %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "1", genericMoodAffect2.Contains("1"), "Flat")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "2", genericMoodAffect2.Contains("2"), "Depressed")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "3", genericMoodAffect2.Contains("3"), "Combative")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "4", genericMoodAffect2.Contains("4"), "Agitated")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "5", genericMoodAffect2.Contains("5"), "Anxious")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMoodAffect", "6", genericMoodAffect2.Contains("6"), "Negative")%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericMoodAffectComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericMoodAffectComment", data.AnswerOrEmptyString("GenericMoodAffectComment"), new { @id = Model.Type + "_GenericMoodAffectComment" })%></div>
		</div>
	</div>
</fieldset>
