﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericNeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
<%= Html.Hidden(Model.Type + "_GenericNeurologicalStatus", string.Empty, new { @id = Model.Type + "_GenericNeurologicalStatusHidden" })%>
<fieldset>
	<legend>Neurological</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus1' name='{0}_GenericNeurologicalStatus' value='1' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus1">LOC</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeurologicalLOC" class="fl">Specify</label>
						<div class="fr">
							<% var LOC = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "" },
								new SelectListItem { Text = "Alert", Value = "Alert" },
								new SelectListItem { Text = "Lethargic", Value = "Lethargic" },
								new SelectListItem { Text = "Comatose", Value = "Comatose" },
								new SelectListItem { Text = "Disoriented", Value = "Disoriented" },
								new SelectListItem { Text = "Other", Value = "Other" }
							}, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalLOC", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericNeurologicalLOC", LOC, new { @id = Model.Type + "_GenericNeurologicalLOC" })%>
						</div>
						<div class="clr"></div>
						<label>Orientation</label>
						<ul class="checkgroup three-wide">
							<%  string[] genericNeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
							<%= Html.Hidden(Model.Type + "_GenericNeurologicalOriented", string.Empty, new { @id = Model.Type + "_GenericNeurologicalOrientedHidden" })%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalOriented", "1", genericNeurologicalOriented.Contains("1"), "Person")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalOriented", "2", genericNeurologicalOriented.Contains("2"), "Place")%>
							<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalOriented", "3", genericNeurologicalOriented.Contains("3"), "Time")%>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus2' name='{0}_GenericNeurologicalStatus' value='2' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus2">Pupils</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeurologicalPupils" class="fl">Specify</label>
						<div class="fr">
							<%  var pupils = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "0" },
								new SelectListItem { Text = "PERRLA/WNL (Within Normal Limits)", Value = "1" },
								new SelectListItem { Text = "Sluggish", Value = "2" },
								new SelectListItem { Text = "Non-Reactive", Value = "3" },
								new SelectListItem { Text = "Other", Value = "4" }
							}, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalPupils", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericNeurologicalPupils", pupils, new { @id = Model.Type + "_GenericNeurologicalPupils" }) %>
						</div>
						<div class="clr"></div>
						<ul class="checkgroup three-wide">
						    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericNeurologicalPupilsPosition", "0", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("0"), "Bilateral") %>
						    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericNeurologicalPupilsPosition", "1", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("1"), "Left") %>
						    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericNeurologicalPupilsPosition", "2", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("2"), "Right") %>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus3' name='{0}_GenericNeurologicalStatus' value='3' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus3">Vision</label>
					</div>
					<div class="more">
						<%  string[] genericNeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
						<%= Html.Hidden(Model.Type + "_GenericNeurologicalVisionStatus", string.Empty, new { @id = Model.Type + "_GenericNeurologicalVisionStatusHidden" })%>
						<ul class="checkgroup one-wide">
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "1", genericNeurologicalVisionStatus.Contains("1"), "WNL (Within Normal Limits)")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "2", genericNeurologicalVisionStatus.Contains("2"), "Blurred Vision")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "3", genericNeurologicalVisionStatus.Contains("3"), "Cataracts")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "4", genericNeurologicalVisionStatus.Contains("4"), "Wears Corrective Lenses")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "5", genericNeurologicalVisionStatus.Contains("5"), "Glaucoma")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalVisionStatus", "6", genericNeurologicalVisionStatus.Contains("6"), "Legally Blind")%>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus4' name='{0}_GenericNeurologicalStatus' value='4' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus4">Speech</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeurologicalSpeech" class="fl">Specify</label>
					    <div class="fr">
						    <%  var speech = new SelectList(new[] {
							    	new SelectListItem { Text = "", Value = "0" },
								    new SelectListItem { Text = "Clear", Value = "1" },
    								new SelectListItem { Text = "Slurred", Value = "2" },
	    							new SelectListItem { Text = "Aphasic", Value = "3" },
		    						new SelectListItem { Text = "Other", Value = "4" }
			    				}, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalSpeech", "0")); %>
				    		<%= Html.DropDownList(Model.Type + "_GenericNeurologicalSpeech", speech, new { @id = Model.Type + "_GenericNeurologicalSpeech" }) %>
				    	</div>
				    	<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus5' name='{0}_GenericNeurologicalStatus' value='5' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus5">Paralysis</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeurologicalStatus5" class="fl">Location</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericNeurologicalParalysisLocation", data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation"), new { @id = Model.Type + "_GenericNeurologicalParalysisLocation", @maxlength = "30" }) %></div>
					    <div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalStatus", "6", genericNeurologicalStatus.Contains("6"), "Quadriplegia")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalStatus", "7", genericNeurologicalStatus.Contains("7"), "Paraplegia")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalStatus", "8", genericNeurologicalStatus.Contains("8"), "Seizures") %>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus9' name='{0}_GenericNeurologicalStatus' value='9' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("9").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus9">Tremors</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericNeurologicalTremorsLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericNeurologicalTremorsLocation", data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation"), new { @id = Model.Type + "_GenericNeurologicalTremorsLocation" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalStatus", "10", genericNeurologicalStatus.Contains("10"), "Dizziness")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericNeurologicalStatus", "11", genericNeurologicalStatus.Contains("11"), "Headache")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericNeurologicalStatus12' name='{0}_GenericNeurologicalStatus' value='12' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("12").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericNeurologicalStatus12">Behavior Status</label>
					</div>
					<div class="more">
						<%  string[] genericBehaviorStatus = data.AnswerArray("GenericBehaviorStatus"); %>
						<%= Html.Hidden(Model.Type + "_GenericBehaviorStatus", string.Empty, new { @id = Model.Type + "_GenericBehaviorStatusHidden" })%>
						<ul class="checkgroup one-wide">
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "1", genericBehaviorStatus.Contains("1"), "WNL (Within Normal Limits)") %>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "2", genericBehaviorStatus.Contains("2"), "Difficulty coping w/ Altered Health Status")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "3", genericBehaviorStatus.Contains("3"), "Withdrawn")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "4", genericBehaviorStatus.Contains("4"), "Combative")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "5", genericBehaviorStatus.Contains("5"), "Expresses Depression")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "6", genericBehaviorStatus.Contains("6"), "Irritability")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "7", genericBehaviorStatus.Contains("7"), "Impaired Decision Making")%>
						    <%= Html.CheckgroupOption(Model.Type + "_GenericBehaviorStatus", "8", genericBehaviorStatus.Contains("8"), "Other")%>
						</ul>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label>HOH</label>
			<ul class="checkgroup three-wide">
				<%= Html.CheckgroupRadioOption(Model.Type + "_NeurologicalHOHPosition", "0", data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("0"), "Bilateral")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_NeurologicalHOHPosition", "1", data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("1"), "Left")%>
				<%= Html.CheckgroupRadioOption(Model.Type + "_NeurologicalHOHPosition", "2", data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("2"), "Right")%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericNeurologicalComment" class="strong">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericNeurologicalComment", data.AnswerOrEmptyString("GenericNeurologicalComment"), new { @class = "tall", @id = Model.Type + "_GenericNeurologicalComment" })%></div>
		</div>
	</div>
</fieldset>

