﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericMusculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
<%= Html.Hidden(Model.Type + "_GenericMusculoskeletal", string.Empty, new { @id = Model.Type + "_GenericMusculoskeletalHidden" })%>
<fieldset>
	<legend>Musculoskeletal</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<%= Html.CheckgroupOption(Model.Type + "_GenericMusculoskeletal", "1", genericMusculoskeletal.Contains("1"), "WNL (Within Normal Limits)")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal2' name='{0}_GenericMusculoskeletal' value='2' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal2">Grip Strength</label>
					</div>
				    <div class="more">
						<label for="<%= Model.Type %>_GenericMusculoskeletalHandGrips" class="fl">Specify</label>
				        <div class="fr">
						    <%  var handGrips = new SelectList(new[] {
							    new SelectListItem { Text = "", Value = "0" },
							    new SelectListItem { Text = "Strong", Value = "1" },
							    new SelectListItem { Text = "Weak", Value = "2" },
							    new SelectListItem { Text = "Other", Value = "3" }
						    }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalHandGrips", "0")); %>
						    <%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalHandGrips", handGrips, new { @id = Model.Type + "_GenericMusculoskeletalHandGrips" }) %>
					    </div>
					    <div class="clr"></div>
						<ul class="checkgroup three-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericMusculoskeletalHandGripsPosition", "0", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("0"), "Bilateral") %>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericMusculoskeletalHandGripsPosition", "1", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("1"), "Left") %>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericMusculoskeletalHandGripsPosition", "2", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition").Equals("2"), "Right") %>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal3' name='{0}_GenericMusculoskeletal' value='3' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal3">Impaired Motor Skill</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericMusculoskeletalImpairedMotorSkills" class="fl">Specify</label>
						<div class="fr">
							<%  var impairedMotorSkills = new SelectList(new[] {
									new SelectListItem { Text = "", Value = "0" },
									new SelectListItem { Text = "N/A", Value = "1" },
									new SelectListItem { Text = "Fine", Value = "2" },
									new SelectListItem { Text = "Gross", Value = "3" }
								}, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalImpairedMotorSkills", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalImpairedMotorSkills", impairedMotorSkills, new { @id = Model.Type + "_GenericMusculoskeletalImpairedMotorSkills" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal4' name='{0}_GenericMusculoskeletal' value='4' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal4">Limited ROM</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericLimitedROMLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericLimitedROMLocation", data.AnswerOrEmptyString("GenericLimitedROMLocation"), new { @id = Model.Type + "_GenericLimitedROMLocation" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal5' name='{0}_GenericMusculoskeletal' value='5' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal5">Mobility</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericMusculoskeletalMobility" class="fl">Specify</label>
						<div class="fr">
							<%  var mobility = new SelectList(new[] {
									new SelectListItem { Text = "", Value = "0" },
									new SelectListItem { Text = "WNL (Within Normal Limits)", Value = "1" },
									new SelectListItem { Text = "Ambulatory", Value = "2" },
									new SelectListItem { Text = "Ambulatory w/assistance", Value = "3" },
									new SelectListItem { Text = "Chair fast", Value = "4" },
									new SelectListItem { Text = "Bedfast", Value = "5" },
									new SelectListItem { Text = "Non-ambulatory", Value = "6" }
								}, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalMobility", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericMusculoskeletalMobility", mobility, new { @id = Model.Type + "_GenericMusculoskeletalMobility" }) %>
						</div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal6' name='{0}_GenericMusculoskeletal' value='6' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("6").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal6">Type Assistive Device</label>
					</div>
					<div class="more">
						<ul class="checkgroup two-wide">
							<%  string[] genericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
							<%= Html.Hidden(Model.Type + "_GenericAssistiveDevice", string.Empty, new { @id = Model.Type + "_GenericAssistiveDeviceHidden" })%>
				            <%= Html.CheckgroupOption(Model.Type + "_GenericAssistiveDevice", "1", genericAssistiveDevice.Contains("1"), "Cane")%>
				            <%= Html.CheckgroupOption(Model.Type + "_GenericAssistiveDevice", "2", genericAssistiveDevice.Contains("2"), "Crutches")%>
				            <%= Html.CheckgroupOption(Model.Type + "_GenericAssistiveDevice", "3", genericAssistiveDevice.Contains("3"), "Walker")%>
				            <%= Html.CheckgroupOption(Model.Type + "_GenericAssistiveDevice", "4", genericAssistiveDevice.Contains("4"), "Wheelchair")%>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='{0}_GenericAssistiveDevice5' name='{0}_GenericAssistiveDevice' value='5' type='checkbox' {1} />", Model.Type, genericAssistiveDevice.Contains("5").ToChecked()) %>
									<label for="<%= Model.Type %>_GenericAssistiveDevice5">Other</label>
								</div>
								<div class="more">
									<label for="<%= Model.Type %>_GenericAssistiveDeviceOther" class="fl">Specify</label>
									<div class="fr"><%= Html.TextBox(Model.Type + "_GenericAssistiveDeviceOther", data.AnswerOrEmptyString("GenericAssistiveDeviceOther"), new { @id = Model.Type + "_GenericAssistiveDeviceOther" }) %></div>
									<div class="clr"></div>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal7' name='{0}_GenericMusculoskeletal' value='7' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("7").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal7">Contracture</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericContractureLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericContractureLocation", data.AnswerOrEmptyString("GenericContractureLocation"), new { @id = Model.Type + "_GenericContractureLocation" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMusculoskeletal", "8", genericMusculoskeletal.Contains("8"), "Weakness")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal9' name='{0}_GenericMusculoskeletal' value='9' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("9").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal9">Joint Pain</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericJointPainLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericJointPainLocation", data.AnswerOrEmptyString("GenericJointPainLocation"), new { @id = Model.Type + "_GenericJointPainLocation"})%></div>
						<div class="clr"></div>
					</div>
				</li>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMusculoskeletal", "10", genericMusculoskeletal.Contains("10"), "Poor Balance")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericMusculoskeletal", "11", genericMusculoskeletal.Contains("11"), "Joint Stiffness")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal12' name='{0}_GenericMusculoskeletal' value='12' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("12").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal12">Amputation</label>
					</div>
					<div class="more">
						<label for="<%= Model.Type %>_GenericAmputationLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericAmputationLocation", data.AnswerOrEmptyString("GenericAmputationLocation"), new { @id = Model.Type + "_GenericAmputationLocation" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericMusculoskeletal13' name='{0}_GenericMusculoskeletal' value='13' type='checkbox' {1} />", Model.Type, genericMusculoskeletal.Contains("13").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericMusculoskeletal13">Weight Bearing Restrictions</label>
					</div>
					<div class="more">
						<ul class="checkgroup two-wide">
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericWeightBearingRestriction", "0", data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("0"), "Full")%>
							<%= Html.CheckgroupRadioOption(Model.Type + "_GenericWeightBearingRestriction", "1", data.AnswerOrEmptyString("GenericWeightBearingRestriction").Equals("1"), "Partial")%>
						</ul>
						<label for="<%= Model.Type %>_GenericWeightBearingRestrictionLocation" class="fl">Location</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericWeightBearingRestrictionLocation", data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation")) %></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericMusculoskeletalComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericMusculoskeletalComment", data.AnswerOrEmptyString("GenericMusculoskeletalComment"), new { }) %></div>
		</div>
	</div>
</fieldset>