﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Insight and Judgment</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsInsightAndJudgmentApplied", string.Empty, new { @id = Model.Type + "_GenericIsInsightAndJudgmentAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsInsightAndJudgmentApplied", Model.Type + "_GenericIsInsightAndJudgmentApplied1", "1", data.AnswerArray("GenericIsInsightAndJudgmentApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
<%  var questions = new List<string>() { "Poor Insight", "Poor Judgment", "Unrealistic Regarding Degree of Illness", "Doesn't Know Why He Is Here", "Unmotivated for Treatment", "Unrealistic Regarding Goals" }; %>
<%  int count = 1; %>
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericInsightAndJudgment<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericInsightAndJudgment" + count, data.AnswerOrEmptyString("GenericInsightAndJudgment" + count), new { @id = Model.Type + "_GenericInsightAndJudgment" + count })%></div>
			</div>
    <%  count++; %>
<%  } %>
			<div class="row">
				<label for="<%= Model.Type %>_GenericInsightAndJudgmentComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericInsightAndJudgmentComment", data.AnswerOrEmptyString("GenericInsightAndJudgmentComment"), new { @id = Model.Type + "_GenericInsightAndJudgmentComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>