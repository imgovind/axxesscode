﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>General Appearance</legend>
		<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsAppearanceApplied", string.Empty, new { @id = Model.Type + "_GenericIsAppearanceAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsAppearanceApplied", Model.Type + "_GenericIsAppearanceApplied1", "1", data.AnswerArray("GenericIsAppearanceApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label>Facial Expressions</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceFacial1">Sad</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial1", data.AnswerOrEmptyString("GenericAppearanceFacial1"), new { @id = Model.Type + "_GenericAppearanceFacial1", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceFacial2">Expressionless</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial2", data.AnswerOrEmptyString("GenericAppearanceFacial2"), new { @id = Model.Type + "_GenericAppearanceFacial2", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceFacial3">Hostile</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial3", data.AnswerOrEmptyString("GenericAppearanceFacial3"), new { @id = Model.Type + "_GenericAppearanceFacial3", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceFacial4">Worried</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial4", data.AnswerOrEmptyString("GenericAppearanceFacial4"), new { @id = Model.Type + "_GenericAppearanceFacial4", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceFacial5">Avoids Gaze</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceFacial5", data.AnswerOrEmptyString("GenericAppearanceFacial5"), new { @id = Model.Type + "_GenericAppearanceFacial5", @class = "short" })%>
				</div>
			</div>
			<div class="row">
				<label>Dress</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceDress1">Meticulous</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress1", data.AnswerOrEmptyString("GenericAppearanceDress1"), new { @id = Model.Type + "_GenericAppearanceDress1", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceDress2">Clothing, Hygiene Poor</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress2", data.AnswerOrEmptyString("GenericAppearanceDress2"), new { @id = Model.Type + "_GenericAppearanceDress2", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceDress3">Eccentric</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress3", data.AnswerOrEmptyString("GenericAppearanceDress3"), new { @id = Model.Type + "_GenericAppearanceDress3", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceDress4">Seductive</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress4", data.AnswerOrEmptyString("GenericAppearanceDress4"), new { @id = Model.Type + "_GenericAppearanceDress4", @class = "short" })%>
				</div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericAppearanceDress5">Exposed</label>
				<div class="fr">
					<%= Html.GeneralStatusDropDown(Model.Type + "_GenericAppearanceDress5", data.AnswerOrEmptyString("GenericAppearanceDress5"), new { @id = Model.Type + "_GenericAppearanceDress5", @class = "short" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericAppearanceComment">Comments</label>
				<div class="ac">
					<%= Html.TextArea(Model.Type + "_GenericAppearanceComment", data.AnswerOrEmptyString("GenericAppearanceComment"), new { @id = Model.Type + "_GenericAppearanceComment" })%>
				</div>
			</div>
		</div>
	</div>
</fieldset>
