﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Communication</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Overall Communication Status</label>
			<div class="fr">
				<%  var communication = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" },
					}, "Value", "Text", data.AnswerOrDefault("GenericCommunication1", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericCommunication1", communication, new { @id = Model.Type + "_GenericCommunication1" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl" for="<%= Model.Type %>_GenericCommunicationSocialization">Socialization</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericCommunicationSocialization", data.AnswerOrEmptyString("GenericCommunicationSocialization"), new { @id = Model.Type + "_GenericCommunicationSocialization" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl" for="<%= Model.Type %>_GenericCommunicationSomatization">Somatization</label>
			<div class="fr">
				<%= Html.TextBox(Model.Type + "_GenericCommunicationSomatization", data.AnswerOrEmptyString("GenericCommunicationSomatization"), new { @id = Model.Type + "_GenericCommunicationSomatization" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Ventilates Feelings</label>
			<div class="fr">
				<%  var communication2 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Good", Value = "Good" },
						new SelectListItem { Text = "Fair", Value = "Fair" },
						new SelectListItem { Text = "Poor", Value = "Poor" },
					}, "Value", "Text", data.AnswerOrDefault("GenericCommunicationVentilatesFeelings", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericCommunicationVentilatesFeelings", communication2, new { @id = Model.Type + "_GenericCommunicationVentilatesFeelings" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericCommunicationComment">Comments</label>
			<div class="ac">
				<%= Html.TextArea(Model.Type + "_GenericCommunicationComment", data.AnswerOrEmptyString("GenericCommunicationComment"), new { @id = Model.Type + "_GenericCommunicationComment" })%>
			</div>
		</div>
	</div>
</fieldset>
