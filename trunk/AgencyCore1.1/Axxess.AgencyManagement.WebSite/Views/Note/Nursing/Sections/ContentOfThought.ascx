﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var questions = new List<string>() { "Suicidal Thoughts", "Suicidal Plans", "Assaultive Ideas", "Homicidal Thoughts", "Homicidal Plans", "Antisocial Attitudes", "Suspiciousness", "Poverty of Content", "Phobias", "Obsessions", "Compulsions", "Feelings of Unreality", "Feels Persecuted", "Thoughts of Running Away", "Somatic Complaints", "Ideas of Guilt", "Ideas of Hopelessness" }; %>
<%  var questions2 = new List<string>() { "Ideas of Worthlessness", "Excessive Religiosity", "Sexual Preoccupation", "Blames Others" }; %>
<fieldset>
	<legend>Content of Thought</legend>
	<div class="wide-column">
	    <div class="row">
		    <ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsContentOfThoughtApplied", string.Empty, new { @id = Model.Type + "_GenericIsContentOfThoughtAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsContentOfThoughtApplied", Model.Type + "_GenericIsContentOfThoughtApplied1", "1", data.AnswerArray("GenericIsContentOfThoughtApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
<%  int count = 1; %>
		<div class="column">
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThought<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThought" + count, data.AnswerOrEmptyString("GenericContentOfThought" + count), new { @id = Model.Type + "_GenericContentOfThought" + count, @class = "short" })%></div>
			</div>
	<%  count++; %>
<%  } %>
		</div>
		<div class="column">
<%  foreach (string question in questions2) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThought<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThought" + count, data.AnswerOrEmptyString("GenericContentOfThought" + count), new { @id = Model.Type + "_GenericContentOfThought" + count, @class = "short" })%></div>
			</div>
	<%  count++; %>
<%  } %>
			<div class="row">
				<label>Illusions</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtIllusions1">Present</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtIllusions1", data.AnswerOrEmptyString("GenericContentOfThoughtIllusions1"), new { @id = Model.Type + "_GenericContentOfThoughtIllusions1", @class = "short" })%></div>
			</div>
			<div class="row">
				<label>Hallucinations</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations1">Auditory</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations1", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations1"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations1", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations2">Visual</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations2", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations2"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations2", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtHallucinations3">Other</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtHallucinations3", data.AnswerOrEmptyString("GenericContentOfThoughtHallucinations3"), new { @id = Model.Type + "_GenericContentOfThoughtHallucinations3", @class = "short" })%></div>
			</div>
			<div class="row">
				<label>Delusions</label>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions1">Of Persecution</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions1", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions1"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions1", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions2">Of Grandeur</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions2", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions2"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions2", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions3">Of Reference</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions3", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions3"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions3", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions4">Of Influence</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions4", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions4"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions4", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions5">Somatic</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions5", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions5"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions5", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions6">Are Systematized</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions6", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions6"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions6", @class = "short" })%></div>
			</div>
			<div class="sub row">
				<label class="fl" for="<%= Model.Type %>_GenericContentOfThoughtDelusions7">Other</label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericContentOfThoughtDelusions7", data.AnswerOrEmptyString("GenericContentOfThoughtDelusions7"), new { @id = Model.Type + "_GenericContentOfThoughtDelusions7", @class = "short" })%></div>
			</div>
		</div>
		<div class="wide-column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericContentOfThoughtComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericContentOfThoughtComment", data.AnswerOrEmptyString("GenericContentOfThoughtComment"), new { @id = Model.Type + "_GenericContentOfThoughtComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>
