﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Speech</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsSpeechApplied", string.Empty, new { @id = Model.Type + "_GenericIsSpeechAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsSpeechApplied", Model.Type + "_GenericIsSpeechApplied1", "1", data.AnswerArray("GenericIsSpeechApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
<%  var questions = new List<string>() { "Excessive Amount", "Reduced Amount", "Speech", "Slowed", "Loud", "Soft", "Mute", "Slurred", "Stuttering" }; %>
<%  int count = 1; %>
<%  foreach (string question in questions) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericSpeech<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericSpeech" + count, data.AnswerOrEmptyString("GenericSpeech" + count), new { @id = Model.Type + "_GenericSpeech" + count, @class = "short" })%></div>
			</div>
	<%  count++; %>
<%  } %>
			<div class="row">
				<label for="<%= Model.Type %>_GenericSpeechComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericSpeechComment", data.AnswerOrEmptyString("GenericSpeechComment"), new { @id = Model.Type + "_GenericSpeechComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>