﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Skin</legend>
	<div class="column">
		<div class="row ar">
			<div class="ancillary-button"><%= string.Format("<a onclick=\"WoundCare.Load('{0}','{1}','{2}')\" title=\"{3} Wound Care Flowsheet\">{3} Wound Care Flowsheet</a>", Model.PatientId, Model.EventId,Model.Service.ToArea(), Model.IsWoundCareExist ? "Edit" : "Add") %></div>
		</div>
		<div class="row">
			<label class="al">Color</label>
			<%  string[] skinColor = data.AnswerArray("GenericSkinColor"); %>
			<%= Html.Hidden(Model.Type + "_GenericSkinColor", string.Empty, new { @id = Model.Type + "_GenericSkinColorHidden" }) %>
			<ul class="checkgroup one-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinColor", "1", skinColor.Contains("1"), "Pink/WNL (Within Normal Limits)") %>
			</ul>
			<ul class="checkgroup two-wide">
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinColor", "2", skinColor.Contains("2"), "Pallor")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinColor", "3", skinColor.Contains("3"), "Jaundice") %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinColor", "4", skinColor.Contains("4"), "Cyanotic") %>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericSkinColor", "5", skinColor.Contains("5"), "Other", Model.Type + "_GenericSkinColorOther", data.AnswerOrEmptyString("GenericSkinColorOther"))%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSkinTemp" class="fl">Temp</label>
			<div class="fr">
				<%  var skinTemp = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Warm", Value = "Warm" },
						new SelectListItem { Text = "Cool", Value = "Cool" },
						new SelectListItem { Text = "Clammy", Value = "Clammy" },
						new SelectListItem { Text = "Other", Value = "Other" }
					}, "Value", "Text", data.AnswerOrDefault("GenericSkinTemp", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericSkinTemp", skinTemp, new { @id = Model.Type + "_GenericSkinTemp" }) %>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSkinTurgor" class="fl">Turgor</label>
			<div class="fr">
				<%  var skinTurgor = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Good/WNL (Within Normal Limits)", Value = "Good" },
						new SelectListItem { Text = "Fair", Value = "Fair" },
						new SelectListItem { Text = "Poor", Value = "Poor" }
					}, "Value", "Text", data.AnswerOrDefault("GenericSkinTurgor", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericSkinTurgor", skinTurgor, new { @id = Model.Type + "_GenericSkinTurgor" }) %>
			</div>
		</div>
		<div class="row">
			<label class="al">Condition</label>
			<ul class="checkgroup three-wide">
				<%  string[] skinCondition = data.AnswerArray("GenericSkinCondition"); %>
				<%= Html.Hidden(Model.Type + "_GenericSkinCondition", string.Empty, new { @id = Model.Type + "_GenericSkinConditionHidden" }) %>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "1", skinCondition.Contains("1"), "Dry")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "2", skinCondition.Contains("2"), "Diaphoretic")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "3", skinCondition.Contains("3"), "Wound")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "4", skinCondition.Contains("4"), "Ulcer")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "5", skinCondition.Contains("5"), "Incision")%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericSkinCondition", "6", skinCondition.Contains("6"), "Rash")%>
				<%= Html.CheckgroupOptionWithOther(Model.Type + "_GenericSkinCondition", "7", skinCondition.Contains("7"), "Other", Model.Type + "_GenericSkinConditionOther", data.AnswerOrEmptyString("GenericSkinConditionOther"))%>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericSkinComment" class="fl">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericSkinComment", data.AnswerOrEmptyString("GenericSkinComment"), new { @id = Model.Type + "_GenericSkinComment" })%></div>
		</div>
	</div>
</fieldset>
