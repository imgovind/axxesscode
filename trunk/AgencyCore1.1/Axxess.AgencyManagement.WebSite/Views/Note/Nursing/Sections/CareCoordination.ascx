﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Care Coordination</legend>
	<div class="column">
		<div class="row">
			<div class="fr">
				<div class="ancillary-button"><%= string.Format("<a onclick=\"Medication.Profile('{0}','{1}')\" title=\"View Patient Medications\">Update Med Profile</a>", Model.PatientId, Model.Service.ToString())%></div>
			</div>
		</div>
		<div class="row">
			<label>Care Coordination with</label>
			<ul class="checkgroup four-wide">
				<%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
				<%= Html.Hidden(Model.Type + "_GenericCareCoordination", string.Empty, new { @id = Model.Type + "_GenericCareCoordinationHidden" })%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination1' name='{0}_GenericCareCoordination' value='1' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination1">SN</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination2' name='{0}_GenericCareCoordination' value='2' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination2">PT</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination3' name='{0}_GenericCareCoordination' value='3' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination3">ST</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination4' name='{0}_GenericCareCoordination' value='4' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination4">MSW</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination5' name='{0}_GenericCareCoordination' value='5' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination5">HHA</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination6' name='{0}_GenericCareCoordination' value='6' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("6").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination6">MD</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericCareCoordination7' name='{0}_GenericCareCoordination' value='7' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("7").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericCareCoordination7">Other</label>
					</div>
					<div class="more" id="<%= Model.Type %>_GenericCareCoordination7More">
                        <label for="<%= Model.Type %>_GenericCareCoordinationOther" class="fl">Specify</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericCareCoordinationOther", data.AnswerOrEmptyString("GenericCareCoordinationOther"), new { @id = Model.Type + "_GenericCareCoordinationOther" }) %></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericCareCoordinationRegarding" class="strong">Regarding</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericCareCoordinationRegarding", data.AnswerOrEmptyString("GenericCareCoordinationRegarding"), new { @class = "taller", @id = Model.Type + "_GenericCareCoordinationRegarding" })%></div>
		</div>
	</div>
</fieldset>
