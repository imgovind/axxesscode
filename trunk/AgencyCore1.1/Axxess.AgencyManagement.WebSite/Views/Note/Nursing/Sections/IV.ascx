﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>IV</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "isIVApplied", string.Empty, new { @id = Model.Type + "_GenericIsIVAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsIVApplied", Model.Type + "_GenericIsIVApplied1", "1", data.AnswerArray("GenericIsIVApplied").Contains("1"), "N/A", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVAccess" class="fl">IV Access</label>
				<div class="fr">
					<%  var IVAccess = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Saline Lock", Value = "Saline Lock" },
                            new SelectListItem { Text = "PICC Line", Value = "PICC Line" },
                            new SelectListItem { Text = "Central Line", Value = "Central Line" },
                            new SelectListItem { Text = "Port-A-Cath", Value = "Port-A-Cath" },
                            new SelectListItem { Text = "Med-A-Port", Value = "Med-A-Port" },
                            new SelectListItem { Text = "Other", Value = "Other" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVAccess", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericIVAccess", IVAccess, new { @id = Model.Type + "_GenericIVAccess" }) %>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVLocation" class="fl">IV Location</label>
				<div class="fr"><%= Html.TextBox(Model.Type + "_GenericIVLocation", data.AnswerOrEmptyString("GenericIVLocation"), new { @id = Model.Type + "_GenericIVLocation" })%></div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVConditionOfIVSite" class="fl">Condition of IV Site</label>
				<div class="fr">
					<%  var IVConditionOfIVSite = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "WNL (Within Normal Limits)", Value = "WNL" },
                            new SelectListItem { Text = "Phlebitis", Value = "Phlebitis" },
                            new SelectListItem { Text = "Redness", Value = "Redness" },
                            new SelectListItem { Text = "Swelling", Value = "Swelling" },
                            new SelectListItem { Text = "Pallor ", Value = "Pallor" },
                            new SelectListItem { Text = "Warmth", Value = "Warmth" },
                            new SelectListItem { Text = "Bleeding", Value = "Bleeding" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfIVSite", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericIVConditionOfIVSite", IVConditionOfIVSite, new { @id = Model.Type + "_GenericIVConditionOfIVSite" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVConditionOfDressing" class="fl">Condition of Dressing</label>
				<div class="fr">
					<%  var IVConditionOfDressing = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Dry and Intact/WNL (Within Normal Limits)", Value = "Dry & Intact/WNL" },
                            new SelectListItem { Text = "Bloody", Value = "Bloody" },
                            new SelectListItem { Text = "Soiled", Value = "Soiled" },
                            new SelectListItem { Text = "Other", Value = "Other" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfDressing", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericIVConditionOfDressing", IVConditionOfDressing, new { @id = Model.Type + "_GenericIVConditionOfDressing" }) %>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVSiteDressing" class="fl">IV site Dressing Changed performed on this visit</label>
				<div class="fr">
					<%  var IVSiteDressing = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "N/A", Value = "N/A" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressing", "0")); %>
					<%= Html.DropDownList(Model.Type + "_GenericIVSiteDressing", IVSiteDressing, new { @id = Model.Type + "_GenericIVSiteDressing" }) %>
				</div>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericFlush">Flush</label>
				<ul class="checkgroup two-wide">
					<li class="option">
						<div class="wrapper">
							<%= string.Format("<input id ='{0}_GenericFlush1' type='radio' value='1' name='{0}_GenericFlush' {1} />", Model.Type, data.AnswerOrEmptyString("GenericFlush").Equals("1").ToChecked()) %>
							<label for="<%= Model.Type %>_GenericFlush1">Yes</label>
						</div>
						<div class="more">
							<label for="<%= Model.Type %>_GenericIVAccessFlushed">Flushed with</label>
							<%= Html.TextBox(Model.Type + "_GenericIVAccessFlushed", data.AnswerOrEmptyString("GenericIVAccessFlushed"), new { @class = "shorter decimals", @id = Model.Type + "_GenericIVAccessFlushed" })%>
							<label for="<%= Model.Type %>_GenericIVSiteDressingUnit">/ml of</label>
							<%  var IVSiteDressingUnit = new SelectList(new[] {
									new SelectListItem { Text = "", Value = "" },
									new SelectListItem { Text = "Normal Saline", Value = "Normal Saline" },
									new SelectListItem { Text = "Heparin", Value = "Heparin" },
									new SelectListItem { Text = "Other", Value = "Other" }
								}, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressingUnit", "0")); %>
							<%= Html.DropDownList(Model.Type + "_GenericIVSiteDressingUnit", IVSiteDressingUnit, new { @id = Model.Type + "_GenericIVSiteDressingUnit", @class = "short" }) %>
							<div class="clr"></div>
						</div>
					</li>
					<%= Html.CheckgroupRadioOption(Model.Type + "_GenericFlush", "0", data.AnswerOrEmptyString("GenericFlush").Equals("0"), "No")%>
				</ul>
			</div>
			<div class="row">
				<label for="<%= Model.Type %>_GenericIVComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericIVComment", data.AnswerOrEmptyString("GenericIVComment"), new { @id = Model.Type + "_GenericIVComment" }) %></div>
			</div>
		</div>
	</div>
</fieldset>