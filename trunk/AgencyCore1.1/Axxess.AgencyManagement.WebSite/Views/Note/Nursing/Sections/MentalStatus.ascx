﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Mental Status</legend>
	<div class="column">
		<div class="row">
			<label class="fl">Overall Mental Status</label>
			<div class="fr">
				<%  var mentalStatus = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Improved", Value = "Improved" },
						new SelectListItem { Text = "Same", Value = "Same" },
						new SelectListItem { Text = "Regressed", Value = "Regressed" }
					}, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus1", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericMentalStatus1", mentalStatus, new { @id = Model.Type + "_GenericMentalStatus1", @class = "short" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Level of Consiousness</label>
			<div class="fr">
				<%  var mentalStatus2 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Alert", Value = "Alert" },
						new SelectListItem { Text = "Confused", Value = "Confused" },
						new SelectListItem { Text = "Disoriented", Value = "Disoriented" },
					}, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus2", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericMentalStatus2", mentalStatus2, new { @id = Model.Type + "_GenericMentalStatus2", @class = "short" })%>
			</div>
		</div>
		<div class="row">
			<label class="fl">Hallucinations/Delusions</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusHallucinations", "1", data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("1"), "Yes") %>
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusHallucinations", "0", data.AnswerOrEmptyString("GenericMentalStatusHallucinations").Equals("0"), "No") %>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="fl">Suicidal Tendencies</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusSuicidal", "1", data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("1"), "Yes")%>
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusSuicidal", "0", data.AnswerOrEmptyString("GenericMentalStatusSuicidal").Equals("0"), "No")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="fl">Extrapyramidal SX</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusExtrapyramidal", "1", data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("1"), "Yes")%>
				    <%= Html.CheckgroupRadioOption(Model.Type + "_GenericMentalStatusExtrapyramidal", "0", data.AnswerOrEmptyString("GenericMentalStatusExtrapyramidal").Equals("0"), "No")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="fl">Oriented</label>
			<div class="fr">
				<ul class="checkgroup three-wide long">
					<%  string[] genericMentalStatusOriented = data.AnswerArray("GenericMentalStatusOriented"); %>
				    <%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatusOriented", "0", genericMentalStatusOriented.Contains("0"), "Time") %>
				    <%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatusOriented", "1", genericMentalStatusOriented.Contains("1"), "Place") %>
				    <%= Html.CheckgroupOption(Model.Type + "_GenericMentalStatusOriented", "2", genericMentalStatusOriented.Contains("2"), "Person") %>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="fl">Insight PT/Family</label>
			<div class="fr">
				<%  var mentalStatus3 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Good", Value = "Good" },
						new SelectListItem { Text = "Fair", Value = "Fair" },
						new SelectListItem { Text = "Poor", Value = "Poor" },
					}, "Value", "Text", data.AnswerOrDefault("GenericMentalStatus3", "0")); %>
				<%= Html.DropDownList(Model.Type + "_GenericMentalStatus3", mentalStatus3, new { @id = Model.Type + "_GenericMentalStatus3", @class = "short" })%>
			</div>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericMentalStatusComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericMentalStatusComment", data.AnswerOrEmptyString("GenericMentalStatusComment"), new { @id = Model.Type + "_GenericMentalStatusComment" })%></div>
		</div>
	</div>
</fieldset>
