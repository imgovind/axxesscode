﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Interventions</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<% string[] interventions = data.AnswerArray("GenericInterventions"); %>
				<%= Html.Hidden(Model.Type + "_GenericInterventions", string.Empty, new { @id = Model.Type + "_GenericInterventionsHidden" })%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions1' name='{0}_GenericInterventions' value='1' type='checkbox' {1} />", Model.Type, interventions.Contains("1").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions1">Skilled Observation/Assessment</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericSkilledObservation" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericSkilledObservation", data.AnswerOrEmptyString("GenericSkilledObservation"), new { @id = Model.Type + "_GenericSkilledObservation" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions2' name='{0}_GenericInterventions' value='2' type='checkbox' {1} />", Model.Type, interventions.Contains("2").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions2">Instructed on Safety Precautions</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericSafetyPrecautions" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericSafetyPrecautions", data.AnswerOrEmptyString("GenericSafetyPrecautions"), new { @id = Model.Type + "_GenericSafetyPrecautions" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions3' name='{0}_GenericInterventions' value='3' type='checkbox' {1} />", Model.Type, interventions.Contains("3").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions3">Medication Administration</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericMedicationAdminstration" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericMedicationAdminstration", data.AnswerOrEmptyString("GenericMedicationAdminstration"), new { @id = Model.Type + "_GenericMedicationAdminstration" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions4' name='{0}_GenericInterventions' value='4' type='checkbox' {1} />", Model.Type, interventions.Contains("4").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions4">Diabetic Monitoring/Care</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericDiabeticMonitoringCare" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericDiabeticMonitoringCare", data.AnswerOrEmptyString("GenericDiabeticMonitoringCare"), new { @id = Model.Type + "_GenericDiabeticMonitoringCare" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions5' name='{0}_GenericInterventions' value='5' type='checkbox' {1} />", Model.Type, interventions.Contains("5").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions5">Foley Change</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericFoleyChange" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericFoleyChange", data.AnswerOrEmptyString("GenericFoleyChange"), new { @id = Model.Type + "_GenericFoleyChange" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions6' name='{0}_GenericInterventions' value='6' type='checkbox' {1} />", Model.Type, interventions.Contains("6").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions6">IV Tubing Change</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericIVTubingChange" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericIVTubingChange", data.AnswerOrEmptyString("GenericIVTubingChange"), new { @id = Model.Type + "_GenericIVTubingChange" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions7' name='{0}_GenericInterventions' value='7' type='checkbox' {1} />", Model.Type, interventions.Contains("7").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions7">Patient/CG teaching</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPatientCGTeaching" class="fl">Instructions</label>
						<div class="fr"><%= Html.TextBox(Model.Type + "_GenericPatientCGTeaching", data.AnswerOrEmptyString("GenericPatientCGTeaching"), new { @id = Model.Type + "_GenericPatientCGTeaching" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions8' name='{0}_GenericInterventions' value='8' type='checkbox' {1} />", Model.Type, interventions.Contains("8").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions8">Instructed on Emergency Preparedness</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericInstructedEmergencyPreparedness" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericInstructedEmergencyPreparedness", data.AnswerOrEmptyString("GenericInstructedEmergencyPreparedness"), new { @id = Model.Type + "_GenericInstructedEmergencyPreparedness" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions9' name='{0}_GenericInterventions' value='9' type='checkbox' {1} />", Model.Type, interventions.Contains("9").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions9">Prep./Admin. Insulin</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPrepAdminInsulin" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPrepAdminInsulin", data.AnswerOrEmptyString("GenericPrepAdminInsulin"), new { @id = Model.Type + "_GenericPrepAdminInsulin" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions10' name='{0}_GenericInterventions' value='10' type='checkbox' {1} />", Model.Type, interventions.Contains("10").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions10">Administer Enteral nutrition</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericAdministerEnteralNutrition" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericAdministerEnteralNutrition", data.AnswerOrEmptyString("GenericAdministerEnteralNutrition"), new { @id = Model.Type + "_GenericAdministerEnteralNutrition" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions11' name='{0}_GenericInterventions' value='11' type='checkbox' {1} />", Model.Type, interventions.Contains("11").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions11">Glucometer calibration</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericGlucometerCalibration" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericGlucometerCalibration", data.AnswerOrEmptyString("GenericGlucometerCalibration"), new { @id = Model.Type + "_GenericGlucometerCalibration" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions12' name='{0}_GenericInterventions' value='12' type='checkbox' {1} />", Model.Type, interventions.Contains("12").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions12">Trachea care</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericTracheaCare" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericTracheaCare", data.AnswerOrEmptyString("GenericTracheaCare"), new { @id = Model.Type + "_GenericTracheaCare" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions13' name='{0}_GenericInterventions' value='13' type='checkbox' {1} />", Model.Type, interventions.Contains("13").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions13">IV Site Dressing Change</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericIVSiteDressingChange" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericIVSiteDressingChange", data.AnswerOrEmptyString("GenericIVSiteDressingChange"), new { @id = Model.Type + "_GenericIVSiteDressingChange" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions14' name='{0}_GenericInterventions' value='14' type='checkbox' {1} />", Model.Type, interventions.Contains("14").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions14">IM Injection / SQ Injection</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericIMSQInjection" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericIMSQInjection", data.AnswerOrEmptyString("GenericIMSQInjection"), new { @id = Model.Type + "_GenericIMSQInjection" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions15' name='{0}_GenericInterventions' value='15' type='checkbox' {1} />", Model.Type, interventions.Contains("15").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions15">Peg/GT Tube Site care</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPegGTTubeSiteCare" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPegGTTubeSiteCare", data.AnswerOrEmptyString("GenericPegGTTubeSiteCare"), new { @id = Model.Type + "_GenericPegGTTubeSiteCare" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions16' name='{0}_GenericInterventions' value='16' type='checkbox' {1} />", Model.Type, interventions.Contains("16").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions16">Foot care performed</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericFootCarePerformed" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericFootCarePerformed", data.AnswerOrEmptyString("GenericFootCarePerformed"), new { @id = Model.Type + "_GenericFootCarePerformed" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions17' name='{0}_GenericInterventions' value='17' type='checkbox' {1} />", Model.Type, interventions.Contains("17").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions17">Wound Care / Dressing Change</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericWoundCareDressingChange" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericWoundCareDressingChange", data.AnswerOrEmptyString("GenericWoundCareDressingChange"), new { @id = Model.Type + "_GenericWoundCareDressingChange" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions18' name='{0}_GenericInterventions' value='18' type='checkbox' {1} />", Model.Type, interventions.Contains("18").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions18">IV Site Change</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericIVSiteChange" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericIVSiteChange", data.AnswerOrEmptyString("GenericIVSiteChange"), new { @id = Model.Type + "_GenericIVSiteChange" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions19' name='{0}_GenericInterventions' value='19' type='checkbox' {1} />", Model.Type, interventions.Contains("19").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions19">Foley Irrigation</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericFoleyIrrigation" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericFoleyIrrigation", data.AnswerOrEmptyString("GenericFoleyIrrigation"), new { @id = Model.Type + "_GenericFoleyIrrigation" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions20' name='{0}_GenericInterventions' value='20' type='checkbox' {1} />", Model.Type, interventions.Contains("20").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions20">Diet Teaching</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericDietTeaching" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericDietTeaching", data.AnswerOrEmptyString("GenericDietTeaching"), new { @id = Model.Type + "_GenericDietTeaching" })%></div>
						<div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions21' name='{0}_GenericInterventions' value='21' type='checkbox' {1} />", Model.Type, interventions.Contains("21").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions21">Venipuncture/Lab</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericVenipuncture" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericVenipuncture", data.AnswerOrEmptyString("GenericVenipuncture"), new { @id = Model.Type + "_GenericVenipuncture" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions22' name='{0}_GenericInterventions' value='22' type='checkbox' {1} />", Model.Type, interventions.Contains("22").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions22">Instructed on Medication</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericInstructedOnMedication" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericInstructedOnMedication", data.AnswerOrEmptyString("GenericInstructedOnMedication"), new { @id = Model.Type + "_GenericInstructedOnMedication" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericInterventions23' name='{0}_GenericInterventions' value='23' type='checkbox' {1} />", Model.Type, interventions.Contains("23").ToChecked()) %>
						<label for="<%= Model.Type %>_GenericInterventions23">Instructed on Disease Process to include</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericInstructedOnDiseaseProcess" class="fl">Instructions</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericInstructedOnDiseaseProcess", data.AnswerOrEmptyString("GenericInstructedOnDiseaseProcess"), new { @id = Model.Type + "_GenericInstructedOnDiseaseProcess" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<div class="row">
			<label for="<%= Model.Type %>_GenericInterventionsComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericInterventionsComment", data.AnswerOrEmptyString("GenericInterventionsComment"), new { @class = "tall", @id = Model.Type + "_GenericInterventionsComment" })%></div>
		</div>
	</div>
</fieldset>