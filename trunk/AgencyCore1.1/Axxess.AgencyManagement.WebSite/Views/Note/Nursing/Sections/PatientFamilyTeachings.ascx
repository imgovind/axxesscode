﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] teachings = data.AnswerArray("GenericPatientFamilyTeachings"); %>
<%  string[] teachings2 = data.AnswerArray("GenericPatientFamilyTeachingsNutrition"); %>
<%  string[] teachings3 = data.AnswerArray("GenericPatientFamilyTeachingsTherapyProvided"); %>
<%= Html.Hidden(Model.Type + "_GenericPatientFamilyTeachings", string.Empty, new { @id = Model.Type + "_GenericPatientFamilyTeachingsHidden" })%>
<fieldset>
	<legend>Patient/Family Teaching</legend>
	<div class="column">
		<div class="row">
			<ul class="checkgroup two-wide">
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachings", "1", teachings.Contains("1"), "Medication Regime")%>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericPatientFamilyTeachings2' name='{0}_GenericPatientFamilyTeachings' value='2' type='checkbox' {1} />", Model.Type, teachings.Contains("2").ToChecked())%>
						<label for="<%= Model.Type %>_GenericPatientFamilyTeachings2">Action/Side Effects</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings2Text" class="fl">Specify</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings2Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings2Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings2Text" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericPatientFamilyTeachings3' name='{0}_GenericPatientFamilyTeachings' value='3' type='checkbox' {1} />", Model.Type, teachings.Contains("3").ToChecked())%>
						<label for="<%= Model.Type %>_GenericPatientFamilyTeachings3">S/S Disease Process</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings3Text" class="fl">Specify</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings3Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings3Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings3Text" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericPatientFamilyTeachings4' name='{0}_GenericPatientFamilyTeachings' value='4' type='checkbox' {1} />", Model.Type, teachings.Contains("4").ToChecked())%>
						<label for="<%= Model.Type %>_GenericPatientFamilyTeachings4">S/S of Complications</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPatientFamilyTeachings4Text" class="fl">Specify</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachings4Text", data.AnswerOrEmptyString("GenericPatientFamilyTeachings4Text"), new { @id = Model.Type + "_GenericPatientFamilyTeachings4Text" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachings", "5", teachings.Contains("5"), "Extrapyramidal Symptoms")%>
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachings", "6", teachings.Contains("6"), "Safety Measures")%>
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachings", "7", teachings.Contains("7"), "Relaxation Techniques")%>
			</ul>
		</div>
	</div>
	<div class="column">
		<div class="row">
			<label>Nutrition</label>
			<ul class="checkgroup two-wide">
				<li class="option">
					<div class="wrapper">
						<%= string.Format("<input id='{0}_GenericPatientFamilyTeachingsNutrition1' name='{0}_GenericPatientFamilyTeachingsNutrition' value='1' type='checkbox' {1} />", Model.Type, teachings2.Contains("1").ToChecked())%>
						<label for="<%= Model.Type %>_GenericPatientFamilyTeachingsNutrition1">Diet</label>
					</div>
					<div class="more">
					    <label for="<%= Model.Type %>_GenericPatientFamilyTeachingsNutritionDiet" class="fl">Specify</label>
					    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPatientFamilyTeachingsNutritionDiet", data.AnswerOrEmptyString("GenericPatientFamilyTeachingsNutritionDiet"), new { @id = Model.Type + "_GenericPatientFamilyTeachingsNutritionDiet" })%></div>
					    <div class="clr"></div>
					</div>
				</li>
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachingsNutrition", "2", teachings2.Contains("2"), "Proper Fluid Intake")%>
			</ul>
		</div>
		<div class="row">
			<label>Therapy Provided</label>
			<ul class="checkgroup two-wide">
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachingsTherapyProvided", "1", teachings3.Contains("1"), "Supportive")%>
			    <%= Html.CheckgroupOption(Model.Type + "_GenericPatientFamilyTeachingsTherapyProvided", "2", teachings3.Contains("2"), "Reality")%>
			</ul>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="<%= Model.Type %>_GenericPatientFamilyTeachingsComment">Comments</label>
			<div class="ac"><%= Html.TextArea(Model.Type + "_GenericPatientFamilyTeachingsComment", data.AnswerOrEmptyString("GenericPatientFamilyTeachingsComment"), new { @id = Model.Type + "_GenericPatientFamilyTeachingsComment" })%></div>
		</div>
	</div>
</fieldset>
