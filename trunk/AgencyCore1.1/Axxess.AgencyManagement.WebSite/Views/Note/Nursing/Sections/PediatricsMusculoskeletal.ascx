﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Musculoskeletal</legend>
    <div class="column">
        <div class="row">
            <label class="al">Muscle Tone</label>
            <ul class="checkgroup three-wide">
                <% string[] muscTone = data.AnswerArray("MUSCTone"); %>
                <%= Html.Hidden(Model.Type + "_MUSCTone", string.Empty, new { @id = Model.Type + "_MUSCTone" })%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "0", muscTone.Contains("0"), "Good")%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "1", muscTone.Contains("1"), "Fair")%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "2", muscTone.Contains("2"), "Poor")%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "3", muscTone.Contains("3"), "Relaxed")%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "4", muscTone.Contains("4"), "Flaccid")%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCTone", "5", muscTone.Contains("5"), "Spastic")%>
            </ul>
        </div>
        <div class="row">
            <label class="al">Extremity Movement</label>
            <ul class="checkgroup two-wide">
                <% string[] muscMovement = data.AnswerArray("MUSCMovement"); %>
                <%= Html.Hidden(Model.Type + "_MUSCMovement", string.Empty, new { @id = Model.Type + "_MUSCMovement" })%>
                <%= Html.CheckgroupOption(Model.Type + "_MUSCMovement", "0", muscMovement.Contains("0"), "Normal")%>
                <%= Html.CheckgroupOptionWithOther(Model.Type + "_MUSCMovement", "1", muscMovement.Contains("1"), "Abnormal", Model.Type + "_MUSCMovementAbnormal", data.AnswerOrEmptyString("MUSCMovementAbnormal"))%>
            </ul>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_IntegComment" class="fl">Comments/Problems</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_MUSCComments", data.AnswerOrEmptyString("MUSCComments"), new { @id = Model.Type + "_MUSCComments", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
