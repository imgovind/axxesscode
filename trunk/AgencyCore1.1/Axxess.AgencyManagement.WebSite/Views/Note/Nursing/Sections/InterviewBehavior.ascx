﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Interview Behavior</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup one-wide">
				<%= Html.Hidden(Model.Type + "_GenericIsInterviewBehaviorApplied", string.Empty, new { @id = Model.Type + "_GenericIsInterviewBehaviorAppliedHidden" })%>
				<%= Html.CheckgroupOption(Model.Type + "_GenericIsInterviewBehaviorApplied", Model.Type + "_GenericIsInterviewBehaviorApplied1", "1", data.AnswerArray("GenericIsInterviewBehaviorApplied").Contains("1"), "WNL for Patient", new { @class = "toggle-container" })%>
			</ul>
		</div>
	</div>
	<div class="collapsible-container">
<%  int count = 1; %>
		<div class="column">
<%  var questions1 = new List<string>() { "Angry Outbursts", "Irritable", "Impulsive", "Hostile", "Silly", "Sensitive", "Apathetic", "Withdrawn", "Evasive", "Passive" }; %>
<%  foreach (string question in questions1) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericInterviewBehavior<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericInterviewBehavior" + count, data.AnswerOrEmptyString("GenericInterviewBehavior" + count), new { @id = Model.Type + "_GenericInterviewBehavior" + count, @class = "short" })%></div>
	<%  count++; %>
			</div>
<%  } %>
		</div>
		<div class="column">
<%  var questions2 = new List<string>() { "Aggressive", "Naive", "Overly Dramatic", "Manipulative", "Dependent", "Uncooperative", "Demanding", "Negativistic", "Callous", "Mood Swings" }; %>
<%  foreach (string question in questions2) { %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_GenericInterviewBehavior<%= count %>"><%= question %></label>
				<div class="fr"><%= Html.GeneralStatusDropDown(Model.Type + "_GenericInterviewBehavior" + count, data.AnswerOrEmptyString("GenericInterviewBehavior" + count), new { @id = Model.Type + "_GenericInterviewBehavior" + count, @class = "short" })%></div>
	<%  count++; %>
			</div>
<%  } %>
		</div>
		<div class="wide-column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericInterviewBehaviorComment">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.Type + "_GenericInterviewBehaviorComment", data.AnswerOrEmptyString("GenericInterviewBehaviorComment"), new { @id = Model.Type + "_GenericInterviewBehaviorComment" })%></div>
			</div>
		</div>
	</div>
</fieldset>