﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
	<legend>Integumentary</legend>
	<div class="column">
		<div class="row">
			<label class="al">Turgor</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<% string[] integTurgor = data.AnswerArray("IntegTurgor"); %>
					<%= Html.Hidden(Model.Type + "_IntegTurgor", string.Empty, new { @id = Model.Type + "_IntegTurgor" })%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_IntegTurgor", "0", integTurgor.Contains("0"), "Elastic")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_IntegTurgor", "1", integTurgor.Contains("1"), "Inelastic")%>
				</ul>
			</div>
		</div>
		<div class="row">
			<label class="al">Skin Temp</label>
			<div class="fr">
				<ul class="checkgroup two-wide">
					<% string[] integSkinTemp = data.AnswerArray("IntegSkinTemp"); %>
					<%= Html.Hidden(Model.Type + "_IntegSkinTemp", string.Empty, new { @id = Model.Type + "_IntegSkinTemp" })%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinTemp", "0", integSkinTemp.Contains("0"), "Warm")%>
					<%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinTemp", "1", integSkinTemp.Contains("1"), "Cool")%>
				</ul>
			</div>
	</div>
	    <div class="row">
		    <label class="al">Skin Condition</label>
		    <ul class="checkgroup three-wide">
			    <% string[] integSkinCondition = data.AnswerArray("IntegSkinCondition"); %>
			    <%= Html.Hidden(Model.Type + "_IntegSkinCondition", string.Empty, new { @id = Model.Type + "_IntegSkinCondition" })%>
			    <%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinCondition", "0", integSkinCondition.Contains("0"), "Diaphoretic")%>
			    <%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinCondition", "1", integSkinCondition.Contains("1"), "Dry")%>
			    <%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinCondition", "2", integSkinCondition.Contains("2"), "Normal")%>
			    <%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinCondition", "3", integSkinCondition.Contains("3"), "Rash")%>
			    <%= Html.CheckgroupRadioOption(Model.Type + "_IntegSkinCondition", "4", integSkinCondition.Contains("4"), "Excoriation")%>
		    </ul>
	    </div>
	    <div class="row">
		    <label class="al">Edema</label>
		    <ul class="checkgroup two-wide">
			    <% string[] integEdema = data.AnswerArray("IntegEdema"); %>
			    <%= Html.Hidden(Model.Type + "_IntegEdema", string.Empty, new { @id = Model.Type + "_IntegEdema" })%>
			    <%= Html.CheckgroupOption(Model.Type + "_IntegEdema", "0", integEdema.Contains("0"), "None")%>
			    <%= Html.CheckgroupOptionWithOther(Model.Type + "_IntegEdema", "1", integEdema.Contains("1"), "Present", Model.Type + "_IntegEdemaPresent", data.AnswerOrEmptyString("IntegEdemaPresent"))%>
		    </ul>
	    </div>
	    <div class="row">
		    <label for="<%= Model.Type %>_IntegIncisions" class="fl">Incisions/Dressings</label>
		    <div class="ac"><%= Html.TextArea(Model.Type + "_IntegIncisions", data.AnswerOrEmptyString("IntegIncisions"), new { @id = Model.Type + "_IntegIncisions", @class = "tall" })%></div>
	    </div>
	    <div class="row">
		    <label for="<%= Model.Type %>_IntegComment" class="fl">Comments</label>
		    <div class="ac"><%= Html.TextArea(Model.Type + "_IntegComment", data.AnswerOrEmptyString("IntegComment"), new { @id = Model.Type + "_IntegComment", @class = "tall" })%></div>
	    </div>
	</div>
</fieldset>
