﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Gastrointestinal</legend>
    <div class="column">
        <div class="row">
            <label class="al">Abdomen</label>
            <ul class="checkgroup two-wide">
                <% string[] giAbdomen = data.AnswerArray("GIAbdomen"); %>
                <%= Html.Hidden(Model.Type + "_GIAbdomen", string.Empty, new { @id = Model.Type + "_GIAbdomen" })%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "0", giAbdomen.Contains("0"), "Soft")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "1", giAbdomen.Contains("1"), "Flat")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "2", giAbdomen.Contains("2"), "Firm")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "3", giAbdomen.Contains("3"), "Tender")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "4", giAbdomen.Contains("4"), "Rigid")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "5", giAbdomen.Contains("5"), "Distended")%>
                <%= Html.CheckgroupRadioOption(Model.Type + "_GIAbdomen", "6", giAbdomen.Contains("6"), "Cramping")%>
            </ul>
        </div>
        <div class="row">
            <label class="fl">Bowel sounds/patterns</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GIBowel", data.AnswerOrEmptyString("GIBowel"), new { @id = Model.Type + "_GIBowel", @maxlength = "30" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_IntegComment" class="fl">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GIComments", data.AnswerOrEmptyString("GIComments"), new { @id = Model.Type + "_GIComments", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
