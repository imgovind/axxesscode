﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<% Html.RenderPartial("Nursing/Sections/VitalSigns/Rev1", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/PainProfile", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/MentalStatus", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/PatientFamilyTeachings", Model); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/MoodAffect", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Communication", Model); %>
		<% Html.RenderPartial("Nursing/Sections/CareCoordination", Model); %>
		<% Html.RenderPartial("Nursing/Sections/DischargePlanning", Model); %>
		<% Html.RenderPartial("Nursing/Sections/NutritionStatus", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Supervisory", new VisitNoteSectionViewData(data, true, Model.Type)); %>
		
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
		<% Html.RenderPartial("Nursing/Sections/ADL", Model); %>
		<% Html.RenderPartial("Nursing/Sections/CarePlan", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Rapport", Model); %>
		<% Html.RenderPartial("Nursing/Sections/GIBowelFunctions", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Response", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/PsychiatricInterventions", Model); %>
<% Html.RenderPartial("Nursing/Sections/Goals", Model); %>
<fieldset>
	<legend>Rehab Potential</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup">
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='0' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialFair' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("0").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialFair">Fair</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='1' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialGood' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("1").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialGood">Good</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='2' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialExcellent' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("2").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialExcellent">Excellent</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Narrative &#38; Teaching</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), new { @id = Model.Type + "_GenericNarrativeComment" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Physician Communication</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericPhysicianCommunicationTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericPhysicianCommunication", data.AnswerOrEmptyString("GenericPhysicianCommunication"), new { @id = Model.Type + "_GenericPhysicianCommunication" })%>
	    	</div>
		</div>
	</div>
</fieldset>