﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Appearance", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Speech", Model); %>
		<% Html.RenderPartial("Nursing/Sections/MoodAffectAssessment", Model); %>
		
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/MotorActivity", Model); %>
		<% Html.RenderPartial("Nursing/Sections/FlowOfThought", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Sensorium", Model); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Intellect", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/InsightAndJudgment", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/InterviewBehavior", Model); %>
<% Html.RenderPartial("Nursing/Sections/ContentOfThought", Model); %>
<fieldset>
	<legend>Comments/Narrative</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), new { @id = Model.Type + "_GenericComments" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
