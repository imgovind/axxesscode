﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Appearance", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Speech", Model); %>
		<% Html.RenderPartial("Nursing/Sections/MoodAffectAssessment", Model); %>
		
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/MotorActivity", Model); %>
		<% Html.RenderPartial("Nursing/Sections/FlowOfThought", Model); %>
		<% Html.RenderPartial("Nursing/Sections/Sensorium", Model); %>
	</div>
</div>
<div class="inline-fieldset two-wide">
	<div>
		<% Html.RenderPartial("Nursing/Sections/Intellect", Model); %>
	</div>
	<div>
		<% Html.RenderPartial("Nursing/Sections/InsightAndJudgment", Model); %>
	</div>
</div>
<% Html.RenderPartial("Nursing/Sections/InterviewBehavior", Model); %>
<% Html.RenderPartial("Nursing/Sections/ContentOfThought", Model); %>
<% Html.RenderPartial("Nursing/Sections/PsychiatricInterventions", Model); %>
<% Html.RenderPartial("Nursing/Sections/Goals", Model); %>
<fieldset>
	<legend>Rehab Potential</legend>
	<div class="wide-column">
		<div class="row">
			<div class="checkgroup">
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='0' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialFair' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("0").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialFair">Fair</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='1' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialGood' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("1").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialGood">Good</label>
					</div>
				</div>
				<div class="option">
					<div class="wrapper">
						<%= string.Format("<input type='radio' value='2' name='{0}_GenericRehabPotential' id='{0}_GenericRehabPotentialExcellent' {1}>", Model.Type, data.AnswerOrEmptyString("GenericRehabPotential").Equals("2").ToChecked())%>
						<label for="<%= Model.Type %>_GenericRehabPotentialExcellent">Excellent</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Comments/Narrative</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_GenericNarrativeCommentTemplates")%>
	    		<%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), new { @id = Model.Type + "_GenericComments" })%>
	    	</div>
		</div>
	</div>
</fieldset>
<% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
