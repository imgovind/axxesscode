﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Partial("VitalSigns/Parameters/ContentRev1", Model) %>
<%= Html.Partial("Shared/CarePlan/SafetyPrecautions/FormRev1")%>
<div class="inline-fieldset two-wide">
	<div>
	  <%= Html.Partial("Shared/CarePlan/FunctionalLimitations/FormRev1")%>
	</div>
	<div>
	    <%= Html.Partial("Shared/CarePlan/ActivitiesPermitted/FormRev1")%>
	</div>
</div>
<%= Html.Partial("Shared/CarePlan/PlanDetails/FormRev1")%>
<fieldset>
    <legend>Comments/Additional Instructions</legend>
    <div class="wide-column">
		<div class="row">
		    <div class="template-text">
			    <%= Html.ToggleTemplates(Model.Type + "_CommentTemplates")%>
			    <%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "tallest" })%>
			</div>
		</div>
    </div>
</fieldset>
<fieldset>
    <legend>Notifications</legend>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup two-wide">
                <li class="required-notification option">
                    <div class="wrapper">
						<%= string.Format("<input id='{0}_ReviewedWithHHA' name='{0}_ReviewedWithHHA' value='Yes' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ReviewedWithHHA").Equals("Yes").ToChecked())%>
						<label for="<%= Model.Type %>_ReviewedWithHHA">Reviewed with Home Health Aide</label>
                    </div>
                </li>
                <li class="required-notification option">
                    <div class="wrapper">
						<%= string.Format("<input id='{0}_ReviewedWithPatient' name='{0}_ReviewedWithPatient' value='Yes' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ReviewedWithPatient").Equals("Yes").ToChecked())%>
						<label for="<%= Model.Type %>_ReviewedWithPatient">Patient oriented with Care Plan</label>
					</div>
				</li>
            </ul>
        </div>
    </div>
</fieldset>   


