﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">HHA Supervisory Visit | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { area = Model.Service.ToArea(), @id = Model.Type + "Form" })) { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
	        <legend>Health Aide Details</legend>
	            <div class="column">
	                <div class="row">
	                    <label for="<%= Model.Type %>_HealthAide" class="fl">Health Aide</label>
                        <div class="fr"><%= Html.HHAides("HealthAide", data.AnswerOrEmptyString("HealthAide"), new { @id = Model.Type + "_HealthAide" })%></div>
	                </div>
	            </div>
	            <div class="column">
	               <div class="row">
	                    <label class="fl">Aide Present</label>
	                    <div class="fr"><%= Html.YesNoCheckGroup(Model.Type + "_AidePresent", data.AnswerOrEmptyString("AidePresent"))%></div>
	                </div>
	            </div>  
	</fieldset>
	<fieldset>
	    <legend>Evaluation</legend>
	    <div class="wide-column">
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">1.</span>
                        <label>Arrives for assigned visits as scheduled</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_ArriveOnTime", data.AnswerOrEmptyString("ArriveOnTime"))%>
                  </div>
		    </div>
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">2.</span>
                        <label>Follows client&#8217;s plan of care</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_FollowPOC", data.AnswerOrEmptyString("FollowPOC"))%>
                  </div>
		    </div>
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">3.</span>
                        <label>Demonstrates positive and helpful attitude towards the client and others</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_HasPositiveAttitude", data.AnswerOrEmptyString("HasPositiveAttitude"))%>
                  </div>
		    </div>
		   <div class="row">
		         <div class="fl">
                        <span class="alphali">4.</span>
                        <label>Informs Nurse Supervisor of client needs and changes in condition as appropriate</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_InformChanges", data.AnswerOrEmptyString("InformChanges"))%>
                  </div>
		    </div>
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">5.</span>
                        <label>Aide Implements Universal Precautions per agency policy</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_IsUniversalPrecautions", data.AnswerOrEmptyString("IsUniversalPrecautions"))%>
                  </div>
		    </div>
		   <div class="row">
		         <div class="fl">
                        <span class="alphali">6.</span>
                        <label>Any changes made to client plan of care at this time</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_POCChanges", data.AnswerOrEmptyString("POCChanges"))%>
                  </div>
            </div>
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">7.</span>
                        <label>Patient/CG satisfied with care and services provided by aide</label>
                    </div>
		          <div class="fr">
                    <%= Html.YesNoCheckGroup(Model.Type + "_IsServicesSatisfactory", data.AnswerOrEmptyString("IsServicesSatisfactory"))%>
                  </div>
		    </div>
		    <div class="row">
		         <div class="fl">
                        <span class="alphali">8.</span>
                        <label>Additional Comments/Findings</label>
                  </div>
             </div>
             <div class="row">
		         <div class="template-text">
		            <%= Html.ToggleTemplates(Model.Type + "_AdditionalCommentsTemplates")%>
                    <%= Html.TextArea(Model.Type + "_AdditionalComments", data.AnswerOrEmptyString("AdditionalComments"), new { @class = "tall" })%>
                  </div>
		    </div>
		</div>
    </fieldset>
     <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>