﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Home Maker Note | <%= Model.PatientProfile.DisplayName %></span>
<%  var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "HomeMakerNoteForm" })) { %>
    <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
<div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("HHA/HomeMaker/ContentRev1", Model); %></div>
     <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>