﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] lightHousekeeping = data.AnswerArray("LightHousekeeping"); %>
<%  string[] personalCare = data.AnswerArray("PersonalCare"); %>
<%  string[] swellingInHandsFeet = data.AnswerArray("SwellingInHandsFeet"); %>
<%  string[] changesInSkinCare = data.AnswerArray("ChangesInSkinCare"); %>
<fieldset>
    <legend>Tasks</legend>
	<div class="column">
	    <div class="row">
	        <input type="hidden" name="<%= Model.Type %>_LightHousekeeping" value="" />
			<div class="sub row">
			    <label class="al">Assignment Light Housekeeping</label>
			    <ul class="checkgroup one-wide">
			        <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "1", lightHousekeeping.Contains("1"), "Vacuuming")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "2", lightHousekeeping.Contains("2"), "Dust/Damp mop")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "3", lightHousekeeping.Contains("3"), "Kitchen")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "4", lightHousekeeping.Contains("4"), "Dishes")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "5", lightHousekeeping.Contains("5"), "Bedroom")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "6", lightHousekeeping.Contains("6"), "Bathroom")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "7", lightHousekeeping.Contains("7"), "Make/Change Bed")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "8", lightHousekeeping.Contains("8"), "Empty Commode")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "9", lightHousekeeping.Contains("9"), "Empty Trash")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "10", lightHousekeeping.Contains("10"), "Wash Clothes")%>
				</ul>
			</div>
			<div class="sub row">
			    <label class="al">Nutrition</label>
			    <ul class="checkgroup one-wide">
			        <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "11", lightHousekeeping.Contains("11"), "Meal Preparation")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "12", lightHousekeeping.Contains("12"), "Assist with Feeding")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "13", lightHousekeeping.Contains("13"), "Limit/Encourage Fluids")%>
			    </ul>
			</div>
			<div class="sub row">
			    <label class="al">Errands</label>
			    <ul class="checkgroup one-wide">
			        <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "14", lightHousekeeping.Contains("14"), "Shopping")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "15", lightHousekeeping.Contains("15"), "Prescription pickup")%>
				    <%= Html.CheckgroupOption(Model.Type + "_LightHousekeeping", "16", lightHousekeeping.Contains("16"), "Appointment Accompaniment")%>
				</ul>
			</div>
		</div>
	</div>
	<div class="column">
	    <div class="row">
		    <input type="hidden" name="<%= Model.Type %>_PersonalCare" value="" />
			<div class="sub row">
			    <label class="al">Assignment Personal Care</label>
			    <ul class="checkgroup one-wide">
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "1", personalCare.Contains("1"), "Tub Bathe")%>
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "2", personalCare.Contains("2"), "Sponge Bathe")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "3", personalCare.Contains("3"), "Bed Bathe")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "4", personalCare.Contains("4"), "Shower")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "5", personalCare.Contains("5"), "Hair Care")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "6", personalCare.Contains("6"), "Nail Care")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "7", personalCare.Contains("7"), "Shampoo")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "8", personalCare.Contains("8"), "Shave")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "9", personalCare.Contains("9"), "Mouth Care")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "10", personalCare.Contains("10"), "Dressing")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "11", personalCare.Contains("11"), "Oral Care - Brush/Dentures")%>
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "12", personalCare.Contains("12"), "Elimination Assist")%>
			    </ul>
		    </div>
			<div class="sub row">
    			<label class="al">Activity</label>
			    <ul class="checkgroup one-wide">
			        <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "13", personalCare.Contains("13"), "Ambulation Assist/WC/Walker/Cane")%>
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "14", personalCare.Contains("14"), "Mobility Assist")%>
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "15", personalCare.Contains("15"), "Exercise")%>
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "16", personalCare.Contains("16"), "Assist with Medication")%>
				    <%= Html.CheckgroupOption(Model.Type + "_PersonalCare", "17", personalCare.Contains("17"), "ROM")%>
				</ul>
			</div>
		</div>
	</div>
</fieldset>
<div class="inline-fieldset two-wide">
    <div>
        <fieldset>
            <legend>Changes in</legend>
            <div class="column">
                <div class="row">
                    <input type="hidden" name="<%= Model.Type %>_ChangesInSkinCare" value="" />
					<label>Swelling in</label>
				    <ul class= "checkgroup three-wide">
					    <%= Html.CheckgroupRadioOption(Model.Type + "_ChangesInSkinCare", "1", changesInSkinCare.Contains("1"), "Broken")%>
				        <%= Html.CheckgroupRadioOption(Model.Type + "_ChangesInSkinCare", "2", changesInSkinCare.Contains("2"), "Itchy")%>
				        <%= Html.CheckgroupRadioOption(Model.Type + "_ChangesInSkinCare", "3", changesInSkinCare.Contains("3"), "N/A")%>
				    </ul>
				</div>
				<div class="row">
				    <input type="hidden" name="<%= Model.Type %>_SwellingInHandsFeet" value="" />
					<label>Skin Care</label>
					<ul class="checkgroup two-wide">
					    <%= Html.CheckgroupOption(Model.Type + "_SwellingInHandsFeet", "1", swellingInHandsFeet.Contains("1"), "Hands/Feet")%>
				        <%= Html.CheckgroupOption(Model.Type + "_SwellingInHandsFeet", "2", swellingInHandsFeet.Contains("2"), "Abdomen")%>
				        <%= Html.CheckgroupOption(Model.Type + "_SwellingInHandsFeet", "3", swellingInHandsFeet.Contains("3"), "Right Leg")%>
				        <%= Html.CheckgroupOption(Model.Type + "_SwellingInHandsFeet", "4", swellingInHandsFeet.Contains("4"), "Left Leg")%>
					</ul>
				</div>
			</div>
		</fieldset>
	</div>
    <div>
        <fieldset>
		    <legend>Universal Precautions</legend>
			<div class="column">
			    <div class="row">
				    <div class="template-text">
				        <%= Html.ToggleTemplates(Model.Type + "_UniversalPrecautionCommentsTemplates")%>
                        <%= Html.TextArea(Model.Type + "_UniversalPrecautionComments", data.AnswerOrEmptyString("UniversalPrecautionComments"), new { @class = "taller" })%>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<fieldset>
    <legend>Comments</legend>
	<div class="wide-column">
	    <div class="row">
	        <div class="template-text">
                <%= Html.ToggleTemplates(Model.Type + "_HomeMakerNoteCommentsTemplates")%>
                <%= Html.TextArea(Model.Type + "_HomeMakerNoteComments", data.AnswerOrEmptyString("HomeMakerNoteComments"), new { @class = "tall" })%>
            </div>
        </div>
    </div>
</fieldset>