﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= Html.Partial("VitalSigns/Parameters/ContentRev1") %>
<%= Html.Partial("Shared/VitalSigns/FormRev2") %>
<%= Html.Partial("Shared/Tasks/FormRev1") %>
<fieldset>
	<legend>Comments</legend>
	<div class="wide-column">
		<div class="row">
		    <div class="template-text">
    			<%= Html.ToggleTemplates(Model.Type + "_CommentTemplates")%>
                <%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @class = "taller" })%>
            </div>
		</div>
	</div>
</fieldset>