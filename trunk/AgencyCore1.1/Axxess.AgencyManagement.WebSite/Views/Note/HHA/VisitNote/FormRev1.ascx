﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Home Health Aide Visit | <%= Model.PatientProfile.DisplayName %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Note", FormMethod.Post, new { @id = "HHAideVisitForm" })) { %>
   <%= Html.Partial("Top/" + Model.Service.ToString(), Model)%>
    <fieldset>
		<legend>HHA Details</legend>
		<div class="column">
			<div class="row no-input">
				<label for="<%= Model.Type %>_HHAFrequency" class="fl">Frequency</label>
                <div class="fr">
                    <%= Html.Hidden(Model.Type + "_HHAFrequency", data.AnswerOrEmptyString("HHAFrequency"), new { @id = Model.Type + "_HHAFrequency", @readonly = "readonly" })%>
					<%= data.AnswerOrDefault("HHAFrequency", "No Frequency") %>
                </div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= Model.Type %>_GenericDigestiveLastBMDate" class="fl">Last BM Date</label>
                <div class="fr">
                    <%= Html.DatePicker(Model.Type + "_GenericDigestiveLastBMDate", data.AnswerValidDateOrEmptyString("GenericDigestiveLastBMDate"), false, new { id = Model.Type + "_GenericDigestiveLastBMDate" })%>
                </div>
			</div>
		</div>
    </fieldset>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("HHA/VisitNote/ContentRev" + Model.Version, Model); %></div>
    <%= Html.Partial("Bottom/View", Model) %>
<% } %>
</div>