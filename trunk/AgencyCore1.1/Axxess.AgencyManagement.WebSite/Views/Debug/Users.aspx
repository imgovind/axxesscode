﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<Axxess.Api.Contracts.SingleUser>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Users</title>
    <style type="text/css">
        body 
        {
            font-family: Arial, Sans-Serif, Garamond;
            font-size: 11px;
            line-height: 1.6em;
        }
        /* ------------------ styling for the tables  ------------------   */
        #type-a
        {
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            border: 1px solid #69c;
            margin-bottom: 20px;
        }
        #type-a th
        {
            padding: 12px 17px 12px 17px;
            font-weight: normal;
            font-size: 14px;
            color: #039;
            border-bottom: 1px dashed #69c;
        }
        #type-a td
        {
            padding: 7px 17px 7px 17px;
            color: #669;
        }
        #type-a tbody tr:hover td
        {
            color: #339;
            background: #d0dafd;
        }

        #type-b
        {
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            border: 1px solid #69c;
            margin-bottom: 20px;
        }
        #type-b th
        {
            padding: 12px 17px 12px 17px;
            font-weight: normal;
            font-size: 14px;
            color: #039;
            border-bottom: 1px dashed #69c;
        }
        #type-b td
        {
            padding: 7px 17px 7px 17px;
            color: #669;
        }
        #type-b tbody tr:hover td
        {
            color: #339;
            background: #d0dafd;
        }
        .modalWindow
        {
            background-color:#fff;
            padding:10px;
            width:550px
        }
    </style>
</head>
<body>
    <div>
<% var users = Model; %>
<% if (users != null) { %>
    <%= string.Format("AXDALCEWEB1001 (10.0.5.41) with {0} users", users.Where(x => x.ServerName == "AXDALCEWEB1001").Count())%><br />
    <%= string.Format("AXDALCEWEB1002 (10.0.5.42) with {0} users", users.Where(x => x.ServerName == "AXDALCEWEB1002").Count())%><br />
    <%= string.Format("AXDALCEWEB1003 (10.0.5.43) with {0} users", users.Where(x => x.ServerName == "AXDALCEWEB1003").Count())%><br />
    <%= string.Format("AXDALCEWEB1004 (10.0.5.44) with {0} users", users.Where(x => x.ServerName == "AXDALCEWEB1004").Count())%><br /><br />
    <%= string.Format("User Count {0}", users.Count)%>
<% } %>
<br /><br />
<table id="type-a" border="0">
    <thead>
        <tr>
            <th style="width: 15%; text-align: left;">Email</th>
            <th style="width: 15%; text-align: left;">Name</th>
            <th style="width: 19%; text-align: left;">Agency</th>
            <th style="width: 7%; text-align: left;">IP Address</th>
            <th style="width: 13%; text-align: left;">Server</th>
            <th style="width: 17%; text-align: left;">Session Id</th>
            <th style="width: 14%; text-align: left;">Last Activity</th>
        </tr>
    </thead>
    <tbody>
<% if (users != null && users.Count > 0) {
        users.ForEach(user => { %>
        <tr><td><%= user.EmailAddress %></td><td><%= user.FullName%></td><td><%= user.AgencyName %></td><td><%= user.IpAddress %></td><td><%= user.ServerName %></td><td><%= user.SessionId %></td><td><%= user.LastSecureActivity.ToString("MM/dd/yyyy hh:mm:ss:tt") %></td></tr>
<% });
} else { %>
        <tr><td colspan="7" align="center" style="color: Red;">There are no users in the system.</td></tr>
<% } %>
    </tbody>
    <tfoot>
        <tr><td align="center" colspan="7"><em>Total: <%= users != null ? users.Count : 0%></em></td></tr>
    </tfoot>
</table>
    </div>
</body>
</html>
