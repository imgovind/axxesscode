﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">Edit Referral | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Referral", FormMethod.Post, new { @id = "editReferralForm", @class = "mainform" }))
    { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditReferral_Id" }) %>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="EditReferral_Physician" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("ReferrerPhysician", Model.ReferrerPhysician.IsEmpty() ? string.Empty : Model.ReferrerPhysician.ToString(), new { @id = "EditReferral_Physician", @class = "physician-picker" })%><br />
					<% if(Model.IsUserCanAddPhysicain) { %>
                    <div class="ancillary-button"><a onclick="Physician.New(true);">New Physician</a></div>
                    <% } %>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_AdmissionSource" class="fl">Admission Source</label>
                <div class="fr"><%= Html.AdmissionSources("AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "EditReferral_AdmissionSource", @class = "AdmissionSource required not-zero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditReferral_OtherReferralSource" class="fl">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "EditReferral_OtherReferralSource", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_Date" class="fl">Referral Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" value="<%= Model.ReferralDate.ToShortDateString() %>" id="EditReferral_Date" /></div>
            </div>
            <div class="row">
                <label for="EditReferral_InternalReferral" class="fl">Internal Referral Source</label>
                <div class="fr"><%= Html.Users("InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = "EditReferral_InternalReferral", @class = "user-selector valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="EditReferral_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditReferral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_MiddleInitial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", Model.MiddleInitial, new { @id = "EditReferral_MiddleInitial", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditReferral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_Gender" class="fl">Gender</label>
                <div class="fr">
                    <%= Html.Gender("Gender", Model.Gender, new { @id = "EditReferral_Gender", @class = "required" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_DateOfBirth" class="fl">Date of Birth</label>
                <div class="fr"><%= Html.TextBox("DOB", Model.DOB.ToZeroFilled(), new { @id = "EditReferral_DateOfBirth", @class = "required date-picker" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_MaritalStatus" class="fl">Marital Status</label>
                <div class="fr">
                     <%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status--", "0", new { @id = "EditReferral_MaritalStatus", @class = "input_wrapper" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Height" class="fl">Height</label>
                <div class="fr">
                    <%= Html.TextBox("Height", Model.Height, new { @id = "EditReferral_Height", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.HeightMetric("HeightMetric", Model.HeightMetric, new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Weight" class="fl">Weight</label>
                <div class="fr">
                    <%= Html.TextBox("Weight", Model.Weight, new { @id = "EditReferral_Weight", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.WeightMetric("WeightMetric",Model.WeightMetric, new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Assign" class="fl">Assign to Clinician</label>
                <div class="fr"><%= Html.Users("UserId", Model.UserId.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = "EditReferral_Assign", @class = "user-selector required valid" })%></div>
            </div>
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="UserInterface.ShowPatientEligibility($('#EditReferral_MedicareNo').val(), $('#EditReferral_LastName').val(), $('#EditReferral_FirstName').val(), $('#EditReferral_DateOfBirth').val(), $('input[name=Gender]:checked').val())">Verify Medicare Eligibility</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditReferral_MedicareNo" class="fl">Medicare No</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "EditReferral_MedicareNo", @maxlength = "12", @class = "MedicareNo" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_MedicaidNo" class="fl">Medicaid No</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "EditReferral_MedicaidNo", @maxlength = "20", @class = "MedicaidNo" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_SSN" class="fl">SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "EditReferral_SSN", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditReferral_AddressLine1", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditReferral_AddressLine2", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditReferral_AddressCity", @maxlength = "50", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditReferral_AddressStateCode", @class = "address-state required not-zero short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditReferral_AddressZipCode", @class = "numeric required zip shorter", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_HomePhone1" class="fl">Home Phone</label>
                <div class="fr">
                <%= Html.PhoneOrFax("PhoneHomeArray", Model.PhoneHome, "EditReferral_HomePhone", "required")%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Email" class="fl">Email Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditReferral_Email", @class = "email" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_IsDNR" class="fl">DNR</label>
                <div class="fr">
                    <%= Html.DNR("IsDNR", Model.IsDNR.ToString(),false, new { @id = "EditReferral_IsDNR" })%>
                </div>
            </div>
            <% if (Current.AcessibleServices.IsAlone()) { %>
				<% if (Model.Services.Has(AgencyServices.HomeHealth)) { %>
					<%= Html.Hidden("ServiceProvided", "HomeHealth", new { @id = "EditReferral_ServiceProvided1" }) %>
                <% } else if(Model.Services.Has(AgencyServices.PrivateDuty)) { %>
					<%= Html.Hidden("ServiceProvided", "PrivateDuty", new { @id = "EditReferral_ServiceProvided2" }) %>
				<% } %>
            <% } else { %>
                <div class="row">
					<label class="fl">Service Provided</label>
					<div class="fr">
						<%  if (!Model.Services.Has(AgencyServices.HomeHealth) || (Model.Services.Has(AgencyServices.HomeHealth) && (ReferralStatusFactory.PossibleToAdmit().Contains(Model.HomeHealthStatus) && !Model.DeprecatedServices.Has(AgencyServices.HomeHealth)))) { %>
						<ul class="checkgroup one-wide">
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id ='EditReferral_ServiceProvided1' type='checkbox' value='HomeHealth' class='required' name='ServiceProvided' {0} />", Model.Services.Has(AgencyServices.HomeHealth) ? "checked='checked'" : "") %>
									<label for="EditReferral_ServiceProvided1">Home Health Service</label>
								</div>
							</li>
						</ul>
						<%  } else { %>
						<label>Home Health Service: <%= string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.HomeHealth) ? "Deleted" : ((ReferralStatus)Model.HomeHealthStatus).ToString()) %></label>
						<%= Html.Hidden("ServiceProvided", "HomeHealth", new { @id = "EditReferral_ServiceProvided1" }) %>
						<%  } %>
						<%  if (!Model.Services.Has(AgencyServices.PrivateDuty) || (Model.Services.Has(AgencyServices.PrivateDuty) && (ReferralStatusFactory.PossibleToAdmit().Contains(Model.PrivateDutyStatus) && !Model.DeprecatedServices.Has(AgencyServices.PrivateDuty)))) { %>
						<ul class="checkgroup one-wide">
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id='EditReferral_ServiceProvided2' type='checkbox' value='PrivateDuty' class='required' name='ServiceProvided' {0} />", Model.Services.Has(AgencyServices.PrivateDuty) ? "checked='checked'" : "")%>
									<label for="NewReferral_ServiceProvided2">Private Duty Service</label>
								</div>
							</li>
						</ul>
						<%  } else { %>
						<label>Private Duty Service: <%= string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.PrivateDuty) ? "Deleted" : ((ReferralStatus)Model.PrivateDutyStatus).ToString()) %></label>
						<%= Html.Hidden("ServiceProvided", "PrivateDuty", new { @id = "EditReferral_ServiceProvided2" }) %>
						<%  } %>
					 </div>
				</div>
			<%  } %>
        </div>
    </fieldset>
    <%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(Model.ServicesRequired, "EditReferral", "six-wide"))%>
	<%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(Model != null ? Model.DME : null, Model != null ? Model.OtherDME : string.Empty, "EditReferral"))%>
    <fieldset>
        <legend>Primary Emergency Contact</legend>
        <%= Model.EmergencyContact != null ? Html.Hidden("EmergencyContact.Id", Model.EmergencyContact.Id) : MvcHtmlString.Empty %>
        <div class="column">
            <div class="row">
                <label for="EditReferral_EmergencyContactFirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.FirstName", Model.EmergencyContact != null ? Model.EmergencyContact.FirstName : "", new { @id = "EditReferral_EmergencyContactFirstName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_EmergencyContactLastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.LastName", Model.EmergencyContact != null ? Model.EmergencyContact.LastName : "", new { @id = "EditReferral_EmergencyContactLastName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_EmergencyContactRelationship" class="fl">Relationship</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.Relationship", Model.EmergencyContact != null ? Model.EmergencyContact.Relationship : "", new { @id = "EditReferral_EmergencyContactRelationship" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditReferral_EmergencyContactPhonePrimary1" class="fl">Primary Phone</label>
                <div class="fr">
                 <%= Html.PhoneOrFax("EmergencyContact.PhonePrimaryArray", Model.EmergencyContact != null ? Model.EmergencyContact.PrimaryPhone:string.Empty, "EditReferral_EmergencyContactPhonePrimary", "")%>
                 </div>
            </div>
            <div class="row">
                <label for="EditReferral_EmergencyContactPhoneAlternate1" class="fl">Alternate Phone</label>
                <div class="fr">
                 <%= Html.PhoneOrFax("EmergencyContact.PhoneAlternateArray", Model.EmergencyContact != null ? Model.EmergencyContact.AlternatePhone : string.Empty, "EditReferral_EmergencyContactPhoneAlternate", "")%>
                 </div>
            </div>
            <div class="row">
                <label for="EditReferral_EmergencyContactEmail" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.EmailAddress", Model.EmergencyContact != null ? Model.EmergencyContact.EmailAddress : "", new { @id = "EditReferral_EmergencyContactEmail", @class = "email", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physicians</legend>
        <div class="column">
            <div class="row">
                <label for="EditReferral_Physicians" class="fl">Physician</label>
                <div class="fl"><%= Html.TextBox("Physicians", string.Empty, new { @id = "EditReferral_Physicians", @class = "physician-picker" })%></div>
                <div class="ac fr">
                    <div class="button"><a class="addphysician" id="EditReferral_AddPhysician">Add Physician</a></div>
                </div>
            </div>
        </div>
		<% if (Model.IsUserCanAddPhysicain)
     { %>
        <div class="column">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                </div>
            </div>
        </div>
        <% } %>
        <div class="wide-column">
            <div class="row">
                <%= Html.Telerik().Grid<ReferalPhysician>().Name("EditReferral_PhysicianGrid").HtmlAttributes(new { @class = "position-relative" }).Columns(columns => {
                    columns.Bound(p => p.FirstName);
                    columns.Bound(p => p.LastName);
                    columns.Bound(p => p.WorkPhone).Width(75);
					columns.Bound(p => p.FaxNumber).Width(75);
					columns.Bound(p => p.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email");
					columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Referral.DeletePhysician('<#=Id#>','" + Model.Id + "')\">Delete</a><# if(!IsPrimary) #><a class=\"link\" onclick=\"Referral.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');\">Make Primary</a>").Title("Action").Sortable(false).Width(155);
                }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Referral", new { ReferralId = Model.Id }).OperationMode(GridOperationMode.Client))
                    .NoRecordsTemplate("No Physicians found.")
					.ClientEvents(events => events.OnDataBinding("U.OnTGridDataBinding").OnDataBound("U.OnTGridDataBound").OnError("U.OnTGridError"))
                	.Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates("EditReferral_CommentsTemplates") %>
                    <textarea id="EditReferral_Comments" name="Comments" class="taller" maxcharacters="2000"><%= Model.Comments %></textarea>
                </div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog) { %>
    	<a class="fr img icon32 log" onclick="Log.LoadReferralLog('<%= Model.Id %>')" />
	<% } %>
    <ul class="buttons ac">
        <li><a class="complete">Save</a></li> 
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>