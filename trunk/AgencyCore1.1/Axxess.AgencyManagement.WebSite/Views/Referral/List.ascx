﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Existing Referral List | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  var pageName = "ReferralsList";  %>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %> 
	<ul class="fr buttons ac">
	<%  if (!Current.IsAgencyFrozen && Model.NewPermissions != AgencyServices.None) { %>
		<li><a service="<%=(int)Model.NewPermissions %>" class="new servicepermission">New Referral</a></li>
		<div class="clr"></div>
	<%  } %>
	<%  if (Model.ExportPermissions != AgencyServices.None) { %>
		<li><a url="Export/Referrals" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li>
	<%  } %>
	</ul>
	<fieldset class="grid-controls ac">
		<div class="button fr"><a class="grid-refresh">Refresh</a></div>
		<div class="filter"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%></div>
		<div class="filter grid-search"></div>
	</fieldset>
    <%  var action = "<# if (IsUserCanEdit) { #><a onclick=\"Referral.Edit('<#=Id#>')\" class=\"link\">Edit</a><# } #><# if (IsUserCanDelete){ #><a onclick=\"Referral.Delete('<#= Id #>')\" class=\"deleteReferral link\">Delete</a><# } #><# if (IsUserCanAdmit) { #><a onclick=\"Referral.Demographic.Admit.Open('<#=Id#>')\" class=\"link\">Admit</a><# } #><# if (IsUserCanNonAdmit) { #><a onclick=\"Referral.NonAdmit('<#=Id#>')\" class=\"link\">Non-Admit</a><# } #>"; %>
    <%  var visible = !Current.IsAgencyFrozen; %>
	<div id="<%= pageName %>GridContainer">
		<%= Html.Telerik().Grid<ReferralData>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
                columns.Bound(r => r.ReferralDate).ClientTemplate("<#= U.FormatGridDate(ReferralDate) #>").Title("Referral Date").Width(100).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
                columns.Bound(r => r.DisplayName).Title("Name");
                columns.Bound(r => r.AdmissionSource).Title("Referral Source");
                columns.Bound(r => r.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(100);
                columns.Bound(r => r.Gender).Title("Gender").Width(75);
                columns.Bound(r => r.Status).Title("Status").Width(75);
                columns.Bound(r => r.CreatedBy).Title("Created By").Width(175);
                columns.Bound(r => r.IsUserCanPrint).ClientTemplate("<# if (IsUserCanPrint) { #><a onclick=\"U.GetAttachment('Referral/ReferralPdf',{id:'<#= Id #>'})\" class='img icon16 print'></a><# } #>").Title("").HtmlAttributes(new { @class = "print-action" }).HeaderHtmlAttributes(new { @class = "print-action" }).Width(30).Sortable(false);
                columns.Bound(r => r.Id).ClientTemplate(action).Title("Action").Width(235).HtmlAttributes(new { @class = "action" }).HeaderHtmlAttributes(new { @class = "action" }).Sortable(false).Visible(visible);
            }).NoRecordsTemplate("No Referrals found.").DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Referral", new { ServiceId = (int)Model.Service }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Referral.OnDataBound")
                .OnError("U.OnTGridError")
		    ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>
<%  } %>