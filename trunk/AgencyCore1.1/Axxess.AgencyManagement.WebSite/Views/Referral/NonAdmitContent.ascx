﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>
<input type="hidden" name="profile.Index" value="<%=Model %>" />
<input type="hidden" name="profile[<%=Model %>].ServiceType" value="<%=Model %>" />
<div class="row">
    <label for="NonAdmit_Referral_Date<%=Model %>" class="fl">Date</label>
    <div class="fr"><input type="text" class="date-picker required" name="profile[<%=Model %>].NonAdmitDate" id="NonAdmit_Referral_Date<%=Model %>" /></div>
</div>
<div class="row">
    <label class="fl">Reason</label>
    <div class="fr">
        <select id="NonAdmit_Referral_Reason<%=Model %>" name="profile[<%=Model %>].Reason" class="multiple  required">
            <option value="">- Select Reason -</option>
            <option value="Inappropriate For Home Care">Inappropriate For Home Care</option>
            <option value="Referral Refused Service">Referral Refused Service</option>
            <option value="Out of Service Area">Out of Service Area</option>
            <option value="On Service with another agency">On Service with another agency</option>
            <option value="Not a Provider">Not a Provider</option>
            <option value="Not Homebound">Not Homebound</option>
            <option value="Redirected to alternate care facility">Redirected to alternate care facility</option>
            <option value="Other">Other (specify in Comments)</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="Comment">Comments</label>
    <div class="ac"><%= Html.TextArea("profile[" + Model + "].Comments", "", new { @id = "NonAdmit_Referral_Comments" + Model }) %></div>
</div>
