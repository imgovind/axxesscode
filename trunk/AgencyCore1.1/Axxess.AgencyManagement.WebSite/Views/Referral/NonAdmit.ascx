﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<div class="wrapper main">
<% using (Html.BeginForm("AddNonAdmit", "Referral", FormMethod.Post, new { @id = "newNonAdmitReferralForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Referral_Id" })%>
    <%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Referral_IsAdmit" })%>
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
    <% if (Model.TotalServiceToProcess > 0) { %>
        <% if (Model.TotalServiceToProcess == 1) { %>
            <div class="row">
                <label class="fl">Service</label>
                <div class="fr"><%= Model.CurrentServices == AgencyServices.HomeHealth ? AgencyServices.HomeHealth.GetDescription() : (Model.CurrentServices == AgencyServices.PrivateDuty ? AgencyServices.PrivateDuty.GetDescription() : string.Empty)%></div>
            </div>
            <div class="row">
                <%= Html.Hidden("ServiceProvided", Model.CurrentServices.ToString())%>
                <label class="fl">Current Status</label>
                <div class="fr"><%= Model.CurrentServices == AgencyServices.HomeHealth? Model.HHStatusName : (Model.CurrentServices == AgencyServices.PrivateDuty ? Model.PDStatusName : string.Empty) %></div>
            </div>
        <% } else { %>
            <div class="row">
                <label class="fl">Service To Non-Admit</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='NonAdmit_Referral_ServiceProvided1' type='checkbox' value='HomeHealth' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.HomeHealth) ? "checked='checked'" : "") %>
                <label for="NonAdmit_Referral_ServiceProvided1">Home Health Service</label>
            </div>
            <div class="row">
                <label class="fl">Current Status</label>
                <%= Model.HHStatusName %>
            </div>
            <div class="row">
                <%= string.Format("<input id='NonAdmit_Referral_ServiceProvided2' type='checkbox' value='PrivateDuty' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.PrivateDuty) ? "checked='checked'" : "") %>
                <label for="NonAdmit_Referral_ServiceProvided1">Private Duty Service</label>
            </div>
            <div class="row">
                <label class="fl">Current Status</label>
                <%= Model.PDStatusName %>
            </div>
        <% } %>
        <% if (Model.CurrentServices.Has(AgencyServices.HomeHealth)) { %>
            <div id="NonAdmit_Referral_ServiceContainerHomeHealth"><% Html.RenderPartial("NonAdmitContent", 1); %></div>
        <% } %>
        <% if (Model.CurrentServices.Has(AgencyServices.PrivateDuty)) { %>
            <div id="NonAdmit_Referral_ServiceContainerPrivateDuty"><% Html.RenderPartial("NonAdmitContent", 2);%></div>
        <% } %>
    <% } else { %>
            <div class="row">
                <label class="fl">There is no service to Non-Admit</label>
            </div>
        <% if(Model.Services.Has(AgencyServices.HomeHealth)) { %>
            <div class="row">
                <label class="fl">Home Health</label>
                <label class="fr"><%= string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.HomeHealth) ? "Deleted" : Model.HHStatusName) %></label>
            </div>
        <% } %>
        <% if (Model.Services.Has(AgencyServices.PrivateDuty)) { %>
            <div class="row">
                <label class="fl">Private Duty</label>
                <label class="fr"><%= string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.PrivateDuty) ? "Deleted" : Model.PDStatusName) %></label>
            </div>
        <% } %>
    <% } %>
		</div>
	</fieldset>
    <ul class="buttons ac">
	    <% if (Model.TotalServiceToProcess > 0) { %>
        <li><a class="save close">Save</a></li>
		<% } %>
        <li><a class="close">Close</a></li>
    </ul>
<% } %>
</div>