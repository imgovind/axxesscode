﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<div class="wrapper main">
<% using (Html.BeginForm("Delete", "Referral", FormMethod.Post, new { @id = "deleteNonAdmitReferralForm" }))   { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "DeleteNonAdmit_Referral_Id" })%>
    <%  if (Model.TotalServiceToProcess > 0) { %>
    <fieldset>
        <legend><%= Model.DisplayName %></legend>
        <%  if (Model.TotalServiceToProcess == 1) { %>
        <div class="column">
            <div class="row">
                <label class="fl">Service</label>
                <div class="fr">
                    <label><%= Model.CurrentServices == AgencyServices.HomeHealth ? AgencyServices.HomeHealth.GetDescription() : (Model.CurrentServices == AgencyServices.PrivateDuty ? AgencyServices.PrivateDuty.GetDescription() : string.Empty) %></label>
                </div>
            </div>
        </div>
        <div class="column">
            <%= Html.Hidden("ServiceProvided", Model.CurrentServices.ToString())%>
            <div class="row">
                <label class="fl">Current Status</label>
                <div class="fr">
                    <label><%= Model.CurrentServices == AgencyServices.HomeHealth ? Model.HHStatusName : (Model.CurrentServices == AgencyServices.PrivateDuty ? Model.PDStatusName:string.Empty) %></label>
                </div>
            </div>
        </div>
        <%  } else { %>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Service to Delete</label>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <li class="option">
                            <div class="wrapper">
                                <%= string.Format("<input id='DeleteNonAdmit_Referral_ServiceProvided1' type='checkbox' value='HomeHealth' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.HomeHealth) ? "checked='checked'" : "") %>
                                <label for="DeleteNonAdmit_Referral_ServiceProvided1">
                                    Home Health Service
                                    <em>Current Status: <%= Model.HHStatusName %></em>    
                                </label>
                            </div>
                        </li>
                        <li class="option">
                            <div class="wrapper">
                                <%= string.Format("<input id='DeleteNonAdmit_Referral_ServiceProvided2' type='checkbox' value='PrivateDuty' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.PrivateDuty) ? "checked='checked'" : "") %>
                                <label for="DeleteNonAdmit_Referral_ServiceProvided1">
                                    Private Duty Service
                                    <em>Current Status: <%= Model.PDStatusName %></em>
                                </label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <%  } %>
        <div class="wide-column">
            <div class="row ac">
                <label>Are you sure you want to delete this referral?</label> 
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Yes</a></li>
        <li><a class="close">No</a></li>
    </ul>
    <%  } else { %>
    <fieldset>
        <legend><%= Model.DisplayName %></legend>
        <div class="wide-column">
            <div class="row ac">
                <p>There is no service to delete</p>
            </div>
        </div>
        <div class="column">
        <%  if (Model.Services.Has(AgencyServices.HomeHealth)) { %>
            <div class="row">
                <label class="fl">Home Health</label>
                <div class="fr">
                    <label><%= string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.HomeHealth) ? "Deleted" : Model.HHStatusName) %></label>
                </div>
            </div>
        <%  } %>
        <%  if (Model.Services.Has(AgencyServices.PrivateDuty)) { %>
            <div class="row">
                <label class="fl">Private Duty</label>
                <div class="fr">
                    <label><%=string.Format("{0}", Model.DeprecatedServices.Has(AgencyServices.PrivateDuty) ? "Deleted" : Model.PDStatusName) %></label>
                </div>
            </div>
        <%  } %>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
    <%  } %>
<%  } %>
</div>