﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceAndGuidViewData>" %>
<span class="wintitle">New Referral | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Referral", FormMethod.Post, new { @id = "NewReferral_Form", @class = "mainform" })) { %>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_Physician" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("ReferrerPhysician", "", new { @id = "NewReferral_Physician", @class = "physician-picker" })%><br />
					<% if(Model.IsUserCanAddPhysicain) { %>
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="New_Referral_AdmissionSource" class="fl">Admission Source:</label>
                <div class="fr"><%= Html.AdmissionSources("AdmissionSource", string.Empty, new { @id = "New_Referral_AdmissionSource", @class = "admission-source required not-zero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewReferral_OtherReferralSource" class="fl">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", string.Empty, new { @id = "NewReferral_OtherReferralSource", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_Date" class="fl">Referral Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" id="NewReferral_Date" /></div>
            </div>
            <div class="row">
                <label for="New_Referral_InternalReferral" class="fl">Internal Referral Source</label>
                <div class="fr"><%= Html.Users("InternalReferral", string.Empty, "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = "New_Referral_InternalReferral", @class = "user-selector" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewReferral_FirstName", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_MiddleInitial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", string.Empty, new { @id = "NewReferral_MiddleInitial", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewReferral_LastName", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_Gender" class="fl">Gender</label>
                <div class="fr"><%= Html.Gender("Gender", string.Empty, new { @class = "required", @id = "NewReferral_Gender" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_DateOfBirth" class="fl">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" id="NewReferral_DateOfBirth" /></div>
            </div>
            <div class="row">
                <label for="NewReferral_MaritalStatus" class="fl">Marital Status</label>
                <div class="fr"><%= Html.MaritalStatus("MaritalStatus", "0", true, "-- Select Marital Status --", "0", new { @id = "NewReferral_MaritalStatus" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_Height" class="fl">Height</label>
                <div class="fr">
                    <%= Html.TextBox("Height", string.Empty, new { @id = "NewReferral_Height", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.HeightMetric("HeightMetric",0,  new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Weight" class="fl">Weight</label>
                <div class="fr">
                    <%= Html.TextBox("Weight", string.Empty, new { @id = "NewReferral_Weight", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.WeightMetric("WeightMetric", 0, new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Assign" class="fl">Assign to Clinician</label>
                <div class="fr"><%= Html.Clinicians("UserId", string.Empty, new { @id = "NewReferral_Assign", @class = "user-selector required" })%></div>
            </div>
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="UserInterface.ShowPatientEligibility($('#NewReferral_MedicareNo').val(),$('#NewReferral_LastName').val(),$('#NewReferral_FirstName').val(),$('#NewReferral_DateOfBirth').val(),$('input[name=Gender]:checked').val())">Verify Medicare Eligibility</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewReferral_MedicareNo" class="fl">Medicare No</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", " ", new { @id = "NewReferral_MedicareNo", @maxlength = "20", @class = "MedicareNo" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_MedicaidNo" class="fl">Medicaid No</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", " ", new { @id = "NewReferral_MedicaidNo", @maxlength = "20", @class = "MedicaidNo" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_SSN" class="fl">SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", "", new { @id = "NewReferral_SSN", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewReferral_AddressLine1", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewReferral_AddressLine2", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewReferral_AddressCity", @maxlength = "50", @class = "address-city required" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressStateCode" class="fl">State, Zip:</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", "", new { @id = "NewReferral_AddressStateCode", @class = "address-state short required not-zero" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewReferral_AddressZipCode", @class = "numeric zip shorter required", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_HomePhone1" class="fl">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneHomeArray", "", "NewReferral_HomePhone", "required")%></div>
            </div>
            <div class="row">
                <label for="NewReferral_Email" class="fl">Email Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewReferral_Email", @class = "email", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_IsDNR" class="fl">DNR</label>
                <div class="fr"><%= Html.DNR("IsDNR", string.Empty,false, new { @id = "NewReferral_IsDNR" })%></div>
            </div>
    <%  if (Model.Service.IsAlone()) { %>
            <%= Html.Hidden("ServiceProvided", Model.Service.ToString())%>
    <%  } else { %>
            <div class="row">
                <label class="fl">Service Provided</label>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption("ServiceProvided", "HomeHealth", false, "Home Health Service") %>
                        <%= Html.CheckgroupOption("ServiceProvided", "PrivateDuty", false, "Private Duty Service") %>
                    </ul>
                </div>
            </div>
    <%  } %>
        </div>
    </fieldset>
	<%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(null, "NewReferral", "six-wide"))%>
	<%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(null, string.Empty, "NewReferral"))%>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_PhysicianDropDown1" class="fl">Primary Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown1", @class = "physician-picker short" })%>
                    <a class="img icon16 plus" onclick="Patient.Demographic.AddPhysRow(this)"></a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown2" class="fl">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewReferral_PhysicianDropDown2", @class = "physician-picker short" })%>
                    <a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"></a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown3" class="fl">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewReferral_PhysicianDropDown3", @class = "physician-picker short" })%>
                    <a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"></a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown4" class="fl">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewReferral_PhysicianDropDown4", @class = "physician-picker short" })%>
                    <a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"></a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown5" class="fl">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewReferral_PhysicianDropDown5", @class = "physician-picker short" })%>
                    <a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"></a>
                </div>
            </div>
        </div>
		<% if(Model.IsUserCanAddPhysicain) { %>
        <div class="column">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                </div>
            </div>
        </div>
    <%  } %>
    </fieldset>
    <fieldset>
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_EmergencyContactFirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.FirstName", string.Empty, new { @id = "NewReferral_EmergencyContactFirstName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_EmergencyContactLastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.LastName", string.Empty, new { @id = "NewReferral_EmergencyContactLastName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_EmergencyContactRelationship" class="fl">Relationship</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.Relationship", string.Empty, new { @id = "NewReferral_EmergencyContactRelationship" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewReferral_EmergencyContactPhonePrimary1" class="fl">Primary Phone</label>
                <div class="fr"> <%= Html.PhoneOrFax("EmergencyContact.PhonePrimaryArray", string.Empty, "NewReferral_EmergencyContactPhonePrimary", string.Empty)%></div>
            </div>
            <div class="row">
                <label for="NewReferral_EmergencyContactPhoneAlternate1" class="fl">Alternate Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("EmergencyContact.PhoneAlternateArray", string.Empty, "NewReferral_EmergencyContactPhoneAlternate", string.Empty)%></div>
            </div>
            <div class="row">
                <label for="NewReferral_EmergencyContactEmail" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.EmailAddress", string.Empty, new { @id = "NewReferral_EmergencyContactEmail", @class = "email", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates("NewReferral_CommentsTemplates")%>
                    <textarea id="NewReferral_Comments" name="Comments" maxcharacters="2000"></textarea>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add Referral</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>
