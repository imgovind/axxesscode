﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<fieldset>
    <legend>Home Health</legend>
    <input type="hidden" name="profile.Index" value="1" />
    <input type="hidden" name="profile[1].ServiceType" value="1" />
    <%= Html.Hidden("profile[1].Id", Model.Id, new { @id = "Admit_Referral_HHProfileId" })%>
    <div class="column">
        <div class="row">
            <label for="Edit_Referral_MedicareNo" class="fl">Medicare No</label>
            <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Referral_MedicareNo", @maxlength = "12", @class = "MedicareNo" })%></div>
        </div>
        <div class="row">
            <label for="Edit_Referral_MedicaidNo" class="fl">Medicaid No</label>
            <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Referral_MedicaidNo", @maxlength = "20", @class = "MedicaidNo" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_StartOfCareDate" class="fl">
                <span class="green">(M0030)</span>
                Start of Care Date
            </label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[1].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Admit_Referral_StartOfCareDate" /></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_EpisodeStartDate" class="fl">Episode Start Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[1].EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Referral_EpisodeStartDate" /></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_CaseManager" class="fl">Case Manager</label>
            <div class="fr"><%= Html.CaseManagers("profile[1].CaseManagerId", string.Empty, new { @id = "Admit_Referral_CaseManager", @class = "user-selector required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_Assign" class="fl">Assign to Clinician</label>
            <div class="fr"><%= Html.Clinicians("profile[1].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Referral_Assign", @class = "required not-zero user-selector" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_LocationId" class="fl">Agency Branch</label>
            <div class="fr">
                <%= Html.BranchOnlyList("profile[1].AgencyLocationId", string.Empty, (int)AgencyServices.HomeHealth, new { @id = "Admit_Referral_LocationId", @class = "branch-location required not-zero", @service = (int)AgencyServices.HomeHealth })%><br />
                <div class="ancillary-button"><a onclick="UserInterface.ShowPatientEligibility($('#Edit_Referral_MedicareNo').val(),$('#Edit_Referral_LastName').val(),$('#Edit_Referral_FirstName').val(),$('#Edit_Referral_DateOfBirth').val(),$('input[name=Gender]:checked').val())">Verify Medicare Eligibility</a></div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="Edit_Referral_AdmissionSource" class="fl">Admission Source</label>
            <div class="fr"><%= Html.AdmissionSources("AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "Edit_Referral_AdmissionSource", @class = "admission-source required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="Edit_Referral_OtherReferralSource" class="fl">Other Referral Source</label>
            <div class="fr"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Referral_OtherReferralSource", @maxlength = "50" })%></div>
        </div>
        <div class="row">
            <label for="Edit_Referral_Date" class="fl">Referral Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" value="<%= Model.ReferralDate.ToShortDateString() %>" id="Edit_Referral_Date" /></div>
        </div>
        <div class="row">
            <label for="Edit_Referral_InternalReferral" class="fl">Internal Referral Source</label>
            <div class="fr"><%= Html.Users("InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "Edit_Referral_InternalReferral", @class = "user-selector" })%></div>
        </div>
        <div class="row">
            <label class="fl">State Surveyor/Auditor</label>
            <div class="fr"><%= Html.Auditors("profile[1].AuditorId", "", new { @id = "Admit_Referral_AuditorId", @class = "user-selector" })%></div>
            <div class="clr"></div>
            <div class="ac"><em>Hint: A user account must be created for the surveyor/auditor before you can grant them access to patient records.</em></div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption("profile[1].IsFaceToFaceEncounterCreated", "true", false, "Create a face to face encounter<br /><em>(this is applicable for SOC date after 01/01/2011)</em>") %>
            </ul>
        </div>
    </div>
    <div class="wide-column">
        <%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(Model.PaymentSource, Model.OtherPaymentSource, "Admit_Referral_", "profile[1].", false, "two-wide")) %>
        <div class="row">
            <label>Insurance/Payor</label>
        </div>
        <div class="inline-row three-wide">
            <div>
                <div class="row">
                    <label for="Admit_Referral_PrimaryInsurance" class="fl">Primary</label>
                    <div class="fr"><%= Html.InsurancesByBranch("profile[1].PrimaryInsurance", "", Guid.Empty, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Referral_PrimaryInsurance", @class = "insurance required not-zero", @identifiers = "Primary| |profile[1]." })%></div>
                </div>
                <div id="Admit_Referral_PrimaryInsuranceContent" class="row hidden"><%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Primary", NamePrefix = "profile[1].", HealthPlanId = Model.PrimaryHealthPlanId, GroupName = Model.PrimaryGroupName, GroupId = Model.PrimaryGroupId, Relationship = Model.PrimaryRelationship })%></div>
            </div>
            <div>
                <div class="row">
                    <label for="Admit_Referral_SecondaryInsurance" class="fl">Secondary</label>
                    <div class="fr"><%= Html.InsurancesByBranch("profile[1].SecondaryInsurance", "", Guid.Empty, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Referral_SecondaryInsurance", @class = "insurance", @identifiers = "Secondary| |profile[1]." })%></div>
                </div>
                <div id="Admit_Referral_SecondaryInsuranceContent" class="row hidden"><%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Secondary", NamePrefix = "profile[1].", HealthPlanId = Model.SecondaryHealthPlanId, GroupName = Model.SecondaryGroupName, GroupId = Model.SecondaryGroupId, Relationship = Model.SecondaryRelationship })%></div>
            </div>
            <div>
                <div class="row">
                    <label for="Admit_Referral_TertiaryInsurance" class="fl">Tertiary</label>
                    <div class="fr"><%= Html.InsurancesByBranch("profile[1].TertiaryInsurance", "", Guid.Empty, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Referral_TertiaryInsurance", @class = "insurance", @identifiers = "Tertiary| |profile[1]." })%></div>
                </div>
                <div id="Admit_Referral_TertiaryInsuranceContent" class="row hidden"><%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Tertiary", NamePrefix = "profile[1].", HealthPlanId = Model.TertiaryHealthPlanId, GroupName = Model.TertiaryHealthPlanId, GroupId = Model.TertiaryGroupId, Relationship = Model.TertiaryRelationship })%></div>
            </div>
        </div>
    </div>
</fieldset>
