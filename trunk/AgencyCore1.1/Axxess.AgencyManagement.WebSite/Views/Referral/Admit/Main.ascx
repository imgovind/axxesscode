﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">Patient Admit  | <%= Model.DisplayName %></span>
<%  if (!Model.IsPatientExist) { %>
<%  if (Model.TotalServiceToProcess > 0) { %>
<div class="wrapper main">
	<%  using (Html.BeginForm("AdmitReferral", "Patient", FormMethod.Post, new { @id = "newAdmitReferralForm", @class = "tabform" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Referral_Id" })%>
	<%= Html.Hidden("IsAdmit", "true", new { @id = "Admit_Referral_IsAdmit" })%>
	<%= Html.Hidden("TotalServiceToAdmit", "true", new { @id = "Admit_Referral_TotalServiceToAdmit" })%>
	<fieldset>
		<legend>Referral Source</legend>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_Physician" class="fl">Physician</label>
				<div class="fr">
					<%= Html.TextBox("ReferrerPhysician", Model.ReferrerPhysician.IsEmpty() ? string.Empty : Model.ReferrerPhysician.ToString(), new { @id = "Admit_Referral_Physician", @class = "physician-picker" })%><br />
					<% if(Model.IsUserCanAddPhysicain) { %>
					<div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
					<% } %>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Patient Demographics</legend>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_FirstName" class="fl">First Name</label>
				<div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Admit_Referral_FirstName", @maxlength = "50", @class = "required" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_MiddleInitial" class="fl">MI</label>
				<div class="fr"><%= Html.TextBox("MiddleInitial", Model.MiddleInitial, new { @id = "Admit_Referral_MiddleInitial", @class = "shortest", @maxlength = "1" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_LastName" class="fl">Last Name</label>
				<div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Admit_Referral_LastName", @maxlength = "50", @class = "required" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_Gender" class="fl">Gender</label>
				<div class="fr"><%= Html.Gender("Gender", Model.Gender, new { @id = "Admit_Referral_Gender", @class = "required" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_DateOfBirth" class="fl">Date of Birth</label>
				<div class="fr"><%= Html.TextBox("DOB", Model.DOB.ToShortDateString(), new { @id = "Admit_Referral_DateOfBirth", @class = "required date-picker" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_MaritalStatus" class="fl">Marital Status</label>
				<div class="fr"><%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status --", "0", new { @id = "Admit_Referral_MaritalStatus" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_PatientID" class="fl"><span class="green">(M0020)</span> Patient ID</label>
				<div class="fr"><%= Html.TextBox("PatientIdNumber", "", new { @id = "Admit_Referral_PatientID", @class = "required", @maxlength = "30" })%></div>
				<div class="clr"></div>
				<div class="ac"><em>Last Patient MRN used: <%= Model.LastUsedPatientId %></em></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_SSN" class="fl">SSN</label>
				<div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Admit_Referral_SSN", @maxlength = "9", @class = "numeric ssn" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_IsDNR" class="fl">DNR</label>
				<div class="fr"><%= Html.DNR("IsDNR", Model.IsDNR.ToString(),false, new { @id = "Admit_Referral_IsDNR" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_Height" class="fl">Height</label>
				<div class="fr">
					<%= Html.TextBox("Height", Model.Height, new { @id = "Admit_Referral_Height", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.HeightMetric("HeightMetric", Model.HeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
			<div class="row">
				<label for="Admit_Referral_Weight" class="fl">Weight</label>
				<div class="fr">
					<%= Html.TextBox("Weight", Model.Weight, new { @id = "Admit_Referral_Weight", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.WeightMetric("WeightMetric", Model.WeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
		</div>
	</fieldset>
	<%  if (Model.CurrentServices.Has(AgencyServices.HomeHealth)) { %>
	<div id="Admit_Referral_HomeHealth"><% Html.RenderPartial("Admit/HomeHealth", Model.Profile); %></div>
	<%  } %>
	<%  if (Model.CurrentServices.Has(AgencyServices.PrivateDuty)) { %>
	<div id="Admit_Referral_PrivateDuty"><% Html.RenderPartial("Admit/PrivateDuty", Model.Profile); %></div>
	<%  } %>
	<%= Html.Partial("Patient/Ethnicities", new ArrayIdViewData(null, "Admit_Referral"))%>
	<fieldset>
		<legend>Patient Address</legend>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_AddressLine1" class="fl">Address Line 1</label>
				<div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Admit_Referral_AddressLine1", @maxlength = "50", @class = "required" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_AddressLine2" class="fl">Address Line 2</label>
				<div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Admit_Referral_AddressLine2", @maxlength = "50" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_AddressCity" class="fl">City</label>
				<div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Admit_Referral_AddressCity", @maxlength = "50", @class = "address-city required" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_AddressStateCode" class="fl">State, Zip</label>
				<div class="fr">
					<%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "Admit_Referral_AddressStateCode", @class = "address-state required not-zero short" })%>
					<%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Admit_Referral_AddressZipCode", @class = "numeric required zip shorter", @maxlength = "9" })%>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_HomePhone1" class="fl">Home Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("PhoneHomeArray", Model.PhoneHome, "Admit_Referral_HomePhone", "required") %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_MobilePhone1" class="fl">Mobile Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("PhoneMobileArray", string.Empty, "Admit_Referral_MobilePhone", string.Empty) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_Email" class="fl">Email Address</label>
				<div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Admit_Referral_Email", @class = "email" })%></div>
			</div>
		</div>
	</fieldset>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Pharmacy</legend>
				<div class="column">
					<div class="row">
						<label for="Admit_Referral_PharmacyName" class="fl">Name</label>
						<div class="fr"><%= Html.TextBox("PharmacyName", string.Empty, new { @id = "Admit_Referral_PharmacyName", @maxlength = "100" })%></div>
					</div>
					<div class="row">
						<label for="Admit_Referral_PharmacyPhone1" class="fl">Phone</label>
						<div class="fr"><%= Html.PhoneOrFax("PharmacyPhoneArray", string.Empty, "Admit_Referral_PharmacyPhone", string.Empty) %></div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Evacuation Zone</legend>
				<div class="column">
					<div class="row">
						<label class="fl">Evacuation Zone</label>
						<div class="fr"><%= Html.EvacuationZone("EvacuationZone", "", new { @id = "NewPatient_EvacuationZone" })%></div>
					</div>
				</div>
			</fieldset>
			<%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(Model.ServicesRequired, "Admit_Referral", string.Empty))%>
		</div>
		<div>
			<%= Html.Partial("Patient/Triage", new ValueIdViewData<int>(0, "Admit_Referral"))%>

		</div>
	</div>
	<%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(Model.DME, Model.OtherDME, "Admit_Referral"))%>
	<fieldset>
		<legend>Primary Emergency Contact</legend>
		<%= Model.EmergencyContact != null ? Html.Hidden("EmergencyContact.Id", Model.EmergencyContact.Id) : MvcHtmlString.Empty %>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_EmergencyContactFirstName" class="fl">First Name</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.FirstName", Model.EmergencyContact != null ? Model.EmergencyContact.FirstName : "", new { @id = "Admit_Referral_EmergencyContactFirstName", @maxlength = "100" }) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_EmergencyContactLastName" class="fl">Last Name</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.LastName", Model.EmergencyContact != null ? Model.EmergencyContact.LastName : "", new { @id = "Admit_Referral_EmergencyContactLastName", @maxlength = "100" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_EmergencyContactRelationship" class="fl">Relationship</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.Relationship", Model.EmergencyContact != null ? Model.EmergencyContact.Relationship : "", new { @id = "Admit_Referral_EmergencyContactRelationship" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_EmergencyContactPhonePrimary1" class="fl">Primary Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("EmergencyContact.PhonePrimaryArray", Model.EmergencyContact != null ? Model.EmergencyContact.PrimaryPhone : "", "Admit_Referral_EmergencyContactPhonePrimary", string.Empty) %></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_EmergencyContactPhoneAlternate1" class="fl">Alternate Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("EmergencyContact.PhoneAlternateArray", Model.EmergencyContact != null ? Model.EmergencyContact.AlternatePhone : "", "Admit_Referral_EmergencyContactPhoneAlternate", string.Empty)%></div>
			</div>
			<div class="row">
				<label for="Admit_Referral_EmergencyContactEmail" class="fl">Email</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.EmailAddress", Model.EmergencyContact != null ? Model.EmergencyContact.EmailAddress : "", new { @id = "Admit_Referral_EmergencyContactEmail", @class = "email", @maxlength = "100" }) %></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Physicians</legend>
		<div class="column">
			<div class="row">
				<label for="Admit_Referral_PhysicianSelector" class="fl">Physician</label>
				<div class="fr"><%= Html.TextBox("Physicians", "", new { @id = "Admit_Referral_PhysicianSelector", @class = "physician-picker" })%></div>
				<div class="clr"></div>
				<div class="ac">
					<div class="button"><a id="Admit_Referral_NewPhysician" class="addphysician">Add Physician</a></div>
				</div>
			</div>
		</div>
		<% if(Model.IsUserCanAddPhysicain) { %>
		<div class="column">
			<div class="row">
				<div class="fr">
					<div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
				</div>
			</div>
		</div>
		<% } %>
		<div class="wide-column">
			<div class="row">
				<%= Html.Telerik().Grid<ReferalPhysician>().Name("Admit_Referral_PhysicianGrid").HtmlAttributes(new { @class = "position-relative" }).Columns(columns => {
					columns.Bound(p => p.FirstName);
					columns.Bound(p => p.LastName);
					columns.Bound(p => p.WorkPhone);
					columns.Bound(p => p.FaxNumber);
					columns.Bound(p => p.EmailAddress);
					columns.Bound(p => p.Id).ClientTemplate("<a onclick=\"Referral.DeletePhysician('<#=Id#>','" + Model.Id + "');\" class=\"link\">Delete</a> | <a onclick=\"Referral.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');\" class=<#= !IsPrimary ? \"link\" : \"hidden link\" #>><#=IsPrimary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
					}).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Referral", new { ReferralId = Model.Id })).Sortable().Footer(false) %>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Comments</legend>
		<div class="wide-column">
			<div class="row">
			    <div class="template-text">
				    <%= Html.ToggleTemplates("Admit_Referral_CommentsTemplates")%>
				    <textarea id="Admit_Referral_Comments" name="Comments" class="taller" maxcharacters="2000"><%= Model.Comments %></textarea>
				</div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<%  if (!Model.CurrentServices.IsNull()) { %>
		<li><a class="complete">Admit Patient</a></li>
		<%  } %>
		<li><a class="close">Cancel</a></li>
	</ul>
	<%  } %>
	<%  } else { %>
	<fieldset>
		<div class="column">
			<div class="row">
				<label class="fl">The referral do not have any service to admit</label>
			</div>
		</div>
		<div class="column">
			<%  if (Current.AcessibleServices.Has(AgencyServices.HomeHealth)) { %>
			<div class="row">
				<label class="fl">Home Health Service</label>
				<div class="fr"><%= string.Format("{0}", Model.Services.Has(AgencyServices.HomeHealth) ? (Model.DeprecatedServices.Has(AgencyServices.HomeHealth) ? "Deleted" : ((ReferralStatus)Model.HomeHealthStatus).ToString()) : "Not Set") %></div>
			</div>
			<%  } %>
			<%  if (Current.AcessibleServices.Has(AgencyServices.PrivateDuty)) { %>
			<div class="row">
				<label class="fl">Private Duty Service</label>
				<div class="fr"><%= string.Format("{0}", Model.Services.Has(AgencyServices.PrivateDuty) ? (Model.DeprecatedServices.Has(AgencyServices.PrivateDuty) ? "Deleted" : ((ReferralStatus)Model.PrivateDutyStatus).ToString()) : "Not Set") %></div>
			</div>
			<%  } %>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="close">Close</a></li>
	</ul>
	<%  } %>
	<%  } else { %>
	<%  using (Html.BeginForm("AdmitService", "Patient", FormMethod.Post, new { @id = "newAdmitExisitngPatientReferralForm", @class = "tabform" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Referral_Id" })%>
	<%= Html.Hidden("IsAdmit", "true", new { @id = "Admit_Referral_IsAdmit" })%>
	<%= Html.Hidden("TotalServiceToAdmit", "true", new { @id = "Admit_Referral_TotalServiceToAdmit" })%>
	<%  if (Model.CurrentServices.Has(AgencyServices.HomeHealth)) { %>
	<div id="Admit_Referral_HomeHealth"><% Html.RenderPartial("Admit/HomeHealth", Model.Profile); %></div>
	<%  } %>
	<%  if (Model.CurrentServices.Has(AgencyServices.PrivateDuty)) { %>
	<div id="Admit_Referral_PrivateDuty"><% Html.RenderPartial("Admit/PrivateDuty", Model.Profile); %></div>
	<%  } %>
	<ul class="buttons ac">
		<%  if (!Model.CurrentServices.IsNull()) { %>
		<li><a class="complete">Admit Patient Service</a></li>
		<%  } %>
		<li><a class="close">Cancel</a></li>
	</ul>
	<%  } %>
	<%  } %>
</div>