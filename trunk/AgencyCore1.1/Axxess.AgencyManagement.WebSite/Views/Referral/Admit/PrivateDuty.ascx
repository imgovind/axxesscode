﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<fieldset>
    <legend>Private Duty</legend>
    <input type="hidden" name="profile.Index" value="2" />
    <input type="hidden" name="profile[2].ServiceType" value="2" />
    <%= Html.Hidden("profile[2].Id", Model.Id, new { @id = "Admit_Referral_PdProfileId" })%>
    <div class="column">
        <div class="row">
            <label for="Admit_Referral_PdStartOfCareDate" class="fl"><span class="green">(M0030)</span> Start of Care Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[2].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Admit_Referral_PdStartOfCareDate" /></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdEpisodeStartDate" class="fl">Care Period Start Date</label>
            <div class="fr"><input type="text" class="date-picker required care-period-start" name="profile[2].EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Referral_PdEpisodeStartDate" /></div>
        </div>
        <div class="row">
            <label for="EditPrivateDutyCarePeriod_PeriodLength" class="fl">Care Period Length</label>
            <div class="fr">
                <select id="EditPrivateDutyCarePeriod_PeriodLength" class="care-period-length">
                    <option value="14">14 Days</option>
                    <option value="30">30 Days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
                    <option value="120">120 Days</option>
                    <option value="specify" selected="selected">Specify End Date</option>
                </select>
            </div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdEpisodeEndDate" class="fl">Care Period End Date</label>
            <div class="fr"><input type="text" class="date-picker required care-period-end" name="profile[2].EpisodeEndDate" id="Admit_Referral_PdEpisodeEndDate" /></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdCaseManager" class="fl">Case Manager</label>
            <div class="fr"><%= Html.CaseManagers("profile[2].CaseManagerId", "", new { @id = "Admit_Referral_PdCaseManager", @class = "user-selector required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdAssign" class="fl">Assign to Clinician</label>
            <div class="fr"><%= Html.Clinicians("profile[2].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Referral_PdAssign", @class = "required not-zero user-selector" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="Admit_Referral_PdLocationId" class="fl">Agency Branch</label>
            <div class="fr"><%= Html.BranchOnlyList("profile[2].AgencyLocationId", string.Empty, (int)AgencyServices.PrivateDuty, new { @id = "Admit_Referral_PdLocationId", @class = "branch-location required not-zero" , @service=(int)AgencyServices.PrivateDuty}) %></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdPatientReferralDate" class="fl"><span class="green">(M0104)</span> Referral Date</label>
            <div class="fr"><input type="text" class="date-picker" name="profile[2].ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "" %>" id="Admit_Referral_PdPatientReferralDate" /></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdAdmissionSource" class="fl">Admission Source</label>
            <div class="fr"><%= Html.AdmissionSources("profile[2].AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "Admit_Referral_PdAdmissionSource", @class = "admission-source required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdOtherReferralSource" class="fl">Other Referral Source</label>
            <div class="fr"><%= Html.TextBox("profile[2].OtherReferralSource", Model.OtherReferralSource, new { @id = "Admit_Referral_PdOtherReferralSource", @maxlength = "30" })%></div>
        </div>
        <div class="row">
            <label for="Admit_Referral_PdInternalReferral" class="fl">Internal Referral Source</label>
            <div class="fr"><%= Html.Users("profile[2].InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "Admit_Referral_PdInternalReferral", @class = "user-selector" })%></div>
        </div>
        <div class="row">
            <label class="fl">State Surveyor/Auditor</label>
            <div class="fr"><%= Html.Auditors("profile[2].AuditorId", "", new { @id = "Admit_Referral_PdAuditorId", @class = "user-selector" })%></div>
            <div class="clr"></div>
            <div class="ac"><em>Hint: A user account must be created for the surveyor/auditor before you can grant them access to patient records</em></div>
        </div>
    </div>
    <div class="wide-column">
        <%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(Model.PaymentSource, Model.OtherPaymentSource, "Admit_Referral_Pd", "profile[2].", false))%>
        <div class="row">
            <label>Insurance/Payor</label>
        </div>
	<div class="inline-row three-wide">
			<div>
				<div class="row">
					<label for="Admit_Referral_PdPrimaryInsurance" class="fl">Primary</label>
					<div class="fr"><%= Html.InsurancesByBranch("profile[2].PrimaryInsurance", "", Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Referral_PdPrimaryInsurance", @class = "insurance required not-zero", @identifiers = "Primary|Pd|profile[2]." })%></div>
				</div>
				<div id="Admit_Referral_PdPrimaryInsuranceContent" class="row hidden">
					<%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Primary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
				</div>
			</div>
			<div>
				<div class="row">
					<label for="Admit_Referral_PdSecondaryInsurance" class="fl">Secondary</label>
					<div class="fr"><%= Html.InsurancesByBranch("profile[2].SecondaryInsurance", "", Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Referral_PdSecondaryInsurance", @class = "insurance", @identifiers = "Secondary|Pd|profile[2]." })%></div>
				</div>
				<div id="Admit_Referral_PdSecondaryInsuranceContent" class="row hidden">
					<%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Secondary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
				</div>
			</div>
			<div>
				<div class="row">
					<label for="Admit_Referral_PdTertiaryInsurance" class="fl">Tertiary</label>
					<div class="fr"><%= Html.InsurancesByBranch("profile[2].TertiaryInsurance", "", Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Referral_PdTertiaryInsurance", @class = "insurance", @identifiers = "Tertiary|Pd|profile[2]." })%></div>
				</div>
				<div id="Admit_Referral_PdTertiaryInsuranceContent" class="row hidden">
					<%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Referral", InsuranceType = "Tertiary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
				</div>
			</div>
		</div>
    </div>
</fieldset>
