﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OrderViewData>" %>
<span class="wintitle">New Order | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  if (Model.IsUserCanAdd) { %>
    <%  using (Html.BeginForm("Add", "Order", FormMethod.Post, new { @id = "NewOrder_Form", area = Model.Service.ToArea() })) { %>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Regarding</legend>
                <div class="column">
                    <div class="row">
                        <label for="NewOrder_PatientName" class="fl">Patient Name</label>
                        <div class="fr"><%= Html.PatientsFilteredByService((int)Model.Service, "PatientId", Model != null && Model.PatientId != Guid.Empty ? Model.PatientId.ToString() : "", new { @id = "NewOrder_PatientName", @class = "required not-zero" })%></div>
                    </div>
                    <div class="row">
                        <%= Html.CarePeriodLabel((int)Model.Service, new { @for = "NewOrder_EpisodeList", @class = "fl" })%>
                        <div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model != null && Model.PatientId != Guid.Empty ? Model.PatientId : Guid.Empty, Model != null ? Model.EpisodeId.ToString() : Guid.Empty.ToString(), new { @id = "NewOrder_EpisodeList", @class = "required not-zero episode-dropdown" })%></div>
                    </div>
                    <div class="row">
                        <label for="NewOrder_Date" class="fl">Date</label>
                        <div class="fr"><input type="text" class="date-picker required" name="OrderDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="NewOrder_Date" /></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Physician</legend>
                <div class="column">
                    <div class="row">
                        <label for="NewOrder_PhysicianDropDown" class="fl">Physician</label>
                        <div class="fr">
                            <%= Html.TextBox("PhysicianId", Model != null && Model.PhysicianId != Guid.Empty ? Model.PhysicianId.ToString() : string.Empty, new { @id = "NewOrder_PhysicianDropDown", @class = "physician-picker" }) %><br />
							<% if(Model.IsUserCanAddPhysicain) { %>
                            <div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                            <% } %>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Order Text</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewOrder_Summary" class="fl">Summary/Title</label>
                <div class="fr"><%= Html.TextBox("Summary", "", new { @id = "NewOrder_Summary", @class = "required longest", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates("NewOrder_OrderTemplates") %>
                    <textarea id="NewOrder_Text" name="Text"></textarea>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup ac">
                    <li class="option al">
                        <div class="wrapper">
                            <%= Html.CheckBox("IsOrderReadAndVerified", false, new { @id = "NewOrder_IsOrderReadAndVerified" })%>
                            <label for="NewOrder_IsOrderReadAndVerified">Order read back and verified.</label>
                        </div>
                    </li>
                </ul>
            </div>
       </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="NewOrder_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @id = "NewOrder_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewOrder_ClinicianSignatureDate" class="fl">Signature Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="NewOrder_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Order_Status" })%>
    <%= Html.Hidden("SaveStatus", 110, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 115, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Create Order</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("add a new phyician order")%>
<%  } %>
</div>