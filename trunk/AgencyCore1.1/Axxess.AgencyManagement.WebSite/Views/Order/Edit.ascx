﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianOrder>" %>
<div class="wrapper main note">
<%  if (Model.IsUserCanEdit) { %>
    <%  if (Model != null) { %>
        <%  using (Html.BeginForm("Update", "Order", FormMethod.Post, new { @id = "editOrderForm", area = Model.Service.Has(AgencyServices.HomeHealth) ? string.Empty : Model.Service.ToString() })) { %>
            <%  if (Model != null) Model.SignatureDate = DateTime.Today; %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Order_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Order_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Order_UserId" })%>
    <% if (Model.IsUserCanSeeSticky)
       { %>
    <%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.Id, Model.PatientId, Model.EpisodeId, Model.StatusComment, Model.Service))%>
    <%} %>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
	            <legend>Regarding</legend>
				<div class="column">
					<div class="row no-input">
					    <label for="Edit_Order_PatientName" class="fl">Patient Name</label>
					    <div class="fr"><%= Model.DisplayName %></div>
					</div>
			<%  if (Model.EpisodeId.IsEmpty()) { %>
					<div class="row">
						<%= Html.CarePeriodLabel((int)Model.Service, new { @for = "Edit_Order_EpisodeList", @class = "fl" })%>
						<div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.PatientId, Guid.Empty.ToString(), new { @id = "Edit_Order_EpisodeList", @class = "required not-zero" })%></div>
					</div>
			<%  } else { %>
					<div class="row no-input">
						<%= Html.CarePeriodLabel((int)Model.Service, new { @class = "fl" })%>
						<div class="fr"><%= Model.EpisodeStartDate + "-" + Model.EpisodeEndDate %></div>
					    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Order_EpisodeId" }) %>
					</div>
			<%  } %>
					<div class="row">
						<ul class="checkgroup one-wide">
							<li class="option">
								<div class="wrapper">
									<%= Html.CheckBox("IsOrderForNextEpisode", Model.IsOrderForNextEpisode, new { @id = "Edit_Order_IsOrderForNextEpisode" })%>
									<label for="Edit_Order_IsOrderForNextEpisode">This order is for the next <%= Model.Service == AgencyServices.HomeHealth ? "episode" : "care period" %>.</label>
								</div>
							</li>
						</ul>
					</div>
					<div class="row">
						<label for="Edit_Order_Date" class="fl">Date</label>
						<div class="fr"><input type="text" class="date-picker required" name="OrderDate" value="<%= Model.OrderDate.ToShortDateString() %>" id="Edit_Order_Date" /></div>
					</div>
				</div>
			</fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Physician</legend>
				<div class="column">
					<div class="row">
						<label for="Edit_Order_PhysicianDropDown" class="fl">Physician</label>
						<div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Edit_Order_PhysicianDropDown", @class = "physician-picker" })%></div>
						<div class="clr"></div>
			<%  if (Model.IsUserCanSeeSticky) { %>
						<div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
			<%  } %>
					</div>
				</div>
        	</fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Order Text</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Order_Summary">Summary/Title</label>
                <div class="fr"><%= Html.TextBox("Summary", Model.Summary, new { @id = "Edit_Order_Summary", @class = "longest", @maxlength = "70" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Order_Text">Order Description</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("TextTemplates")%>
                    <%= Html.TextArea("Text", Model.Text, new { @id = "Edit_Order_Text", @class = "tallest" })%>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup ac">
                    <li class="option al">
                        <div class="wrapper">
                            <%= Html.CheckBox("IsOrderReadAndVerified", Model.IsOrderReadAndVerified, new { @id = "Edit_Order_IsOrderReadAndVerified" })%>
                            <label for="Edit_Order_IsOrderReadAndVerified">Order read back and verified.</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "Edit_Order_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignatureDate" class="fl">Signature Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="Edit_Order_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("SaveStatus", 110, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 115, new { @disabled = "true" })%>
    <%= Html.Hidden("Status", Model.Status, new { @id = "Edit_Order_Status" })%>
    <ul class="buttons ac">
        <li><a class="save">Save</a></li>
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
        <%  } %>
    <%  } else { %>
    <%= Html.ErrorMessage("Order Not Found", "This order does not exist. Please close this window and try a different order.") %>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("edit a phyician order")%>
<%  } %>
</div>