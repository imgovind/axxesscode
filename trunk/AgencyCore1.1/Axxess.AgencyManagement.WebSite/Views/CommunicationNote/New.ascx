﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceAndGuidViewData>" %>
<span class="wintitle"><% if(!Current.AcessibleServices.IsAlone()) { %> <%= Model.Service.GetDescription() %><% } %> New Communication Note | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  if (Model.IsUserCanAdd) { %>
    <%  using (Html.BeginForm("Add", "CommunicationNote", FormMethod.Post, new { area = Model.Service.ToArea(), @class = "mainform", @id = "NewCommunicationNote_Form" })) { %>
    <%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = "NewCommunicationNote_ServiceId", @disabled = "disabled" })%>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Regarding</legend>
                <div class="column">
                    <div class="row">
                        <label for="NewCommunicationNote_PatientName" class="fl">Patient Name</label>
                        <div class="fr"><%= Html.PatientsFilteredByService((int)Model.Service, "PatientId", Model.Id.ToString(), new { @id = "NewCommunicationNote_PatientName", @class = "required not-zero" }) %></div>
                    </div>
                    <div class="row">
                        <%= Html.CarePeriodLabel((int)Model.Service, new { @for = "NewCommunicationNote_EpisodeList", @class = "fl" })%>
                        <div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.Id, Guid.Empty.ToString(), new { @id = "NewCommunicationNote_EpisodeList", @class = "required not-zero episode-dropdown" })%></div>
                    </div>
                    <div class="row">
                        <label for="NewCommunicationNote_Date" class="fl">Date</label>
                        <div class="fr"><%= Html.DatePicker("Created", DateTime.Now, DateTime.Now, true, new { id = "NewCommunicationNote_Date" })%></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Physician</legend>
                <div class="column">
                    <div class="row">
                        <label for="NewCommunicationNote_PhysicianDropDown" class="fl">Physician</label>
                        <div class="fr">
                            <%= Html.TextBox("PhysicianId", string.Empty, new { @id = "NewCommunicationNote_PhysicianDropDown", @class = "physician-picker " })%><br />
							<% if(Model.IsUserCanAddPhysicain) { %>
                            <div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                            <% } %>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Communication Text</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates("NewCommunicationNote_Templates") %>
                    <%= Html.TextArea("Text", string.Empty, new { @id = "NewCommunicationNote_Text", @maxcharacters = "5000", @class = "required" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Messaging</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.CheckBox("SendAsMessage", false, new { @id = "NewCommunicationNote_SendAsMessage", @class ="send-as-message" }) %>
                            <label for="NewCommunicationNote_SendAsMessage">Send note as Message</label>
                        </div>
                        <div class="more"><%= Html.Recipients("NewCommunicationNote", new List<Guid>()) %></div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="NewCommunicationNote_ClinicianSignature" class="fl">Staff Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @class = "complete-required", @id = "NewCommunicationNote_ClinicianSignature" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewCommunicationNote_ClinicianSignatureDate" class="fl">Signature Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="NewCommunicationNote_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = "New_CommunicationNote_ServiceId" })%>
    <%= Html.Hidden("Status", "", new { @id = "New_CommunicationNote_Status" })%>
    <%= Html.Hidden("SaveStatus", 415, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 420, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("add a new communication note")%>
<%  } %>
</div>