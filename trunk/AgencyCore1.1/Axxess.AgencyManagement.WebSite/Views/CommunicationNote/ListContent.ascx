﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNoteViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
    <%  var sortName = Model.SortColumn; %>
    <%  var sortDirection = Model.SortDirection; %>
    <%  var notAuditor = !Current.IfOnlyRole(AgencyRoles.Auditor); %>
    <%= Html.Filter("SortParams", string.Format("{0}-{1}", sortName, sortDirection), new { @class = "sort-parameters" })%>
    <%  var area = Model.Service.ToArea(); %>
    <%  var print = "Acore.OpenPrintView({{'Url':'" + area + "/CommunicationNote/PrintPreview/{0}/{1}','PdfUrl':'" + area + "/CommunicationNote/Pdf','PdfData':{{ 'patientId':'{0}', 'eventId':'{1}' }} }});"; %>
    <%  var action = new List<string>(); %>
    <%  if (Model.IsUserCanEdit) action.Add("<a class='link' onclick=\"CommunicationNote.Edit('{0}','{1}','" + area + "')\">Edit</a>"); %>
    <%  if (Model.IsUserCanDelete) action.Add("<a class='link' onclick=\"CommunicationNote." + Model.Service.ToString() + ".Delete('{0}','{1}')\">Delete</a>"); %>
    <%  var actionUrl = action.ToArray().Join(""); %>
    <%  var isVisible = notAuditor && !Current.IsAgencyFrozen && action.IsNotNullOrEmpty(); %>
<%= Html.Telerik().Grid(Model.Notes).Name("AgencyCommunicationNotes_Grid").HtmlAttributes(new { @class = "args aggregated" }).Columns(columns => {
        columns.Bound(c => c.DisplayName).Title("Patient Name").Width(12).Hidden(Model.IsSinglePatient);
        columns.Bound(c => c.UserDisplayName).Title("Employee Name").Width(12);
        columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Width(6).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
        columns.Bound(c => c.StatusName).Title("Status").Width(10);
        columns.Bound(c => c.Id).Title(" ").Template(s => Html.PrintLink(string.Format(print, s.PatientId, s.Id))).Sortable(false).Width(1).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Visible(Model.IsUserCanPrint);
        columns.Bound(c => c.Id).Sortable(false).Template(c => string.Format(actionUrl, c.PatientId, c.Id)).Title("Action").Width(4).Visible(isVisible);
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        if (sortName == "DisplayName" && !Model.IsSinglePatient) {
            if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
        } else if (sortName == "UserDisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.UserDisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserDisplayName).Descending();
        } else if (sortName == "Created") {
            if (sortDirection == "ASC") order.Add(o => o.Created).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Created).Descending();
        } else if (sortName == "StatusName") {
            if (sortDirection == "ASC") order.Add(o => o.StatusName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.StatusName).Descending();
        }
    })).NoRecordsTemplate("No Communication Notes found.").Scrollable(scrolling => scrolling.Enabled(true)) %>
<%  } %>