﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>
<span class="wintitle"><%= Model.Service.GetDescription() %> | Edit Communication Note | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  if (Model.IsUserCanEdit) { %>
	<%  using (Html.BeginForm("Update", "CommunicationNote", FormMethod.Post, new { area = Model.Service.ToArea(), @class = "mainform", @id = "editCommunicationNoteForm" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = "EditCommunicationNote_Id" })%>
	<%= Html.Hidden("UserId", Model.UserId, new { @id = "EditCommunicationNote_UserId" })%>
	<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditCommunicationNote_PatientId" })%>
	<%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.Id, Model.PatientId, Model.EpisodeId, Model.StatusComment, Model.Service))%>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Regarding</legend>
				<div class="column">
					<div class="row no-input">
						<label for="EditCommunicationNote_PatientName" class="fl">Patient Name</label>
						<div class="fr"><%= Model != null && Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName : string.Empty %></div>
					</div>
					<div class="row no-input">
						<%= Html.CarePeriodLabel((int)Model.Service, new { @for = "EditCommunicationNote_EpisodeList", @class = "fl" })%>
		                <div class="fr">
        <%  if (Model.EpisodeId.IsEmpty()){ %>
						    <%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.PatientId, Guid.Empty.ToString(), new { @id = "EditCommunicationNote_EpisodeList", @class = "required not-zero" })%>
		<%  } else { %>
							<span id="EditCommunicationNote_EpisodeList"><%= string.Format("{0} - {1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></span>
							<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "EditCommunicationNote_EpisodeId" })%>
		<%  } %>
						</div>
					</div>
					<div class="row">
						<label for="EditCommunicationNote_Date" class="fl">Date</label>
						<div class="fr"><%= Html.DatePicker("Created", Model.Created, true, new { id = "EditCommunicationNote_Date" })%></div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Physician</legend>
				<div class="column">
					<div class="row">
						<label for="EditCommunicationNote_PhysicianDropDown" class="fl">Physician</label>
						<div class="fr">
							<%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : string.Empty, new { @id = "EditCommunicationNote_PhysicianDropDown", @class = "physician-picker " })%><br />
        <%  if (Model.IsUserCanAddPhysicain) { %>
							<div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
		<%  } %>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
	<fieldset>
		<legend>Communication Text</legend>
		<div class="wide-column">
			<div class="row">
				<div class="template-text">
					<%= Html.ToggleTemplates("EditCommunicationNote_Templates")%>
					<%= Html.TextArea("Text", Model.Text, new { @id = "EditCommunicationNote_Text", @maxcharacters = "5000" })%>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Messaging</legend>
		<div class="wide-column">
			<div class="row">
				<ul class="checkgroup one-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.CheckBox("SendAsMessage", false, new { @id = "EditCommunicationNote_SendAsMessage", @class = "send-as-message" })%>
							<label for="EditCommunicationNote_SendAsMessage">Send note as Message</label>
						</div>
						<div class="more" style='<%= Model.RecipientArray.IsNotNullOrEmpty() ? "display:block;": "" %>'>
							<%= Html.Recipients("EditCommunicationNote", Model.RecipientArray)%>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Electronic Signature</legend>
		<div class="column">
			<div class="row">
				<label for="EditCommunicationNote_ClinicianSignature" class="fl">Staff Signature</label>
				<div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @class = "complete-required", @id = "EditCommunicationNote_ClinicianSignature" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="EditCommunicationNote_ClinicianSignatureDate" class="fl">Signature Date</label>
				<div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="EditCommunicationNote_ClinicianSignatureDate" /></div>
			</div>
		</div>
	</fieldset>
	<%= Html.Hidden("Status", "", new { @id = "EditCommunicationNote_Status" })%>
	<%= Html.Hidden("SaveStatus", 415, new { @disabled = "true" })%>
	<%= Html.Hidden("CompleteStatus", 420, new { @disabled = "true" })%>
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="complete">Complete</a></li>
		<li><a class="close">Close</a></li>
	</ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("edit a communication note")%>
<%  } %>
</div>