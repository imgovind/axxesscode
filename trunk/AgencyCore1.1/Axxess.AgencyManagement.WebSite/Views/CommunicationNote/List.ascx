﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNoteViewData>" %>
<%@ Import Namespace="NPOI.SS.Formula.Functions" %>
<span class="wintitle">Communication Notes | <%= Current.AgencyName + (Model.IsSinglePatient?" |"+ Model.DisplayName :string.Empty) %> </span>
    <%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
        <%  var pageName = "AgencyCommunicationNotes"; %>
        <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %>
    <ul class="fr buttons">
        <%  if (Model.NewPermissions != AgencyServices.None){ %>
		<li><a class="new-communication-note servicepermission" service="<%=(int)Model.NewPermissions %>">New Comm. Note</a></li><div class="clr"></div>
        <%  } %>
        <%  if (Model.ExportPermissions != AgencyServices.None){ %>
		<li><a service="<%=(int)Model.ExportPermissions %>" area="<%=area %>" class="export servicepermission" url="/Export/CommunicationNotes">Excel Export</a></li><div class="clr"></div>
        <%  } %>
        <li><a area="<%=area %>" url="/CommunicationNote/ListContent" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service", @id = pageName + "_ServiceId" })%></div>
        <%  if (!Model.IsSinglePatient) { %>
        <div class="filter optional">
	        <label for="<%= pageName%>_BranchId">Branch</label>
			<%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService, new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter optional">
            <label for="<%= pageName%>_Status">Patient Status</label>
            <select id="<%= pageName%>_Status" name="Status" class="shorter">
                <option value="0">All</option>
                <option value="1" selected="selected">Active</option>
                <option value="2">Discharged</option>
            </select>
        </div>
        <%  } else { %>
        <div class="filter">
            <%= Html.Hidden("BranchId", Model.LocationId.ToString(), new {  @id = pageName + "_BranchId" })%>
            <%= Html.Hidden("Status", 0, new { @id = pageName + "_Status" })%>
        </div>
        <%  } %>
        <div class="filter">
            <% if(Model.PatientId.IsEmpty()) { %>
            <label for="<%= pageName%>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-60), false, new { @id = pageName + "_StartDate", @class = "short"}) %>
            <label for="<%= pageName%>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, false, new { @id = pageName + "_EndDate", @class = "short" })%>
            <% } else { %>
                <%= Html.CarePeriodLabel((int)Model.Service, new { @for = pageName + "_EpisodeId" }) %>
                <%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.PatientId, Model.EpisodeId.ToString(), new { id = pageName + "_EpisodeId", @class = "long required"}) %>
                <%= Html.Hidden("StartDate", DateTime.MinValue) %>
                <%= Html.Hidden("EndDate", DateTime.MinValue) %>
            <% } %>
        </div>
        <div class="filter"><%= Html.Hidden("PatientId", Model.PatientId, new { @id = pageName + "_PatientId" })%></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div class="clr"/>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("ListContent", Model); %></div>
</div>
   
<%  }else { %>
<div class="wrapper main"><%= Html.NotAuthorized("view the list of communication notes") %></div>
<%  } %> 