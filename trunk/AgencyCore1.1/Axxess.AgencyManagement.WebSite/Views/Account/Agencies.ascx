﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<AgencyLite>>" %>
<% if (Model != null)
   { %>
<div id="agencySelectionLinks">
    <div><h1>Select Agency Profile</h1></div>
    <ul> 
    <% foreach(var agency in Model) { %>
        <li><a onclick="Logon.Select('<%= agency.Id %>', '<%= agency.UserId %>');" title="<%= agency.Name %>"><%= agency.Name%>
        <em>Your title is <%= agency.Title %>.</em>
        <span>Profile Created on <%= agency.Date %></span></a></li> 
    <% } %>
    </ul> 
</div>
<% } %>