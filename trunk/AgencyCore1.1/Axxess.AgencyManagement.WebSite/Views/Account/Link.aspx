﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Reset Password - Axxess Home Health Management System</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<%= Html.Telerik()
			.StyleSheetRegistrar()
			.DefaultGroup(group => group
			.Add("account.css")
			.Add("sprite.css")
			.Add("alerts.css")
			.Combined(true)
			.Compress(true)
			.CacheDurationInDays(1)
			.Version(Current.AssemblyVersion))
		%>
		<link href="/Images/favicon.ico" rel="shortcut icon" />
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css" rel="stylesheet"/>
	</head>
	<body>
		<div class="centered-window">
			<img src="/Images/Login/logo.png" class="logo">
			<div class="form-wrapper">
				<% if (Model.UserId != Guid.Empty) { %>
					<div class="form-wrapper-header"><span class="img icon22 axxess"></span><span class="title">Axxess&#8482; Account Link</span></div>
					<div id="messages"></div>
					<%= string.Format("{0} has been linked to your account.", Model.AgencyName) %>
					<%= Html.Hidden("UserId", Model.UserId, new { @id = "Link_User_Id" })%>
					<%= Html.Hidden("LoginId", Model.LoginId, new { @id = "Link_User_LoginId" })%>
					<%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Link_User_AgencyId" })%>
					<div class="padded">
						<table>
							<tbody>
								<tr>
									<td class="label"><label>Name</label></td>
									<td><%= Html.TextBoxFor(a => a.Name, new { disabled = "disabled" })%></td>
								</tr>
								<tr>
									<td class="label"><label>Email</label></td>
									<td><%= Html.TextBoxFor(a => a.EmailAddress, new { disabled = "disabled" })%></td>
								</tr>
								<tr>
									<td class="label"><label>Agency Name</label></td>
									<td><%= Html.TextBoxFor(a => a.AgencyName, new { disabled = "disabled" })%></input></td>
								</tr>
								<tr>
									<td colspan="2" class="label">
										<label class="hint">Note: Your Password and Electronic Signature will remain the same.</label>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<input type="button" onclick="window.location.replace('/Login');" value="Login"/>
				<% } else { %>
					<div class="form-wrapper-header"><span class="img icon22 axxess"></span><span class="title">Not Found</span></div>
					<div class="notification warning">You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.</div>
				<% } %>
			</div>
		</div>
		<div id="Agency_Selection_Container" class="agencySelection hidden"></div>
		<% Html.Telerik().ScriptRegistrar().jQuery(false)
			 .DefaultGroup(group => group
				 .Add("jquery-1.10.1.min.js")
				 .Add("Plugins/Other/form.min.js")
				 .Add("Plugins/Other/validate.min.js")
				 .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
				 .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
				 .Compress(true).Combined(true)
				 .CacheDurationInDays(1)
				 .Version(Current.AssemblyVersion))
			.OnDocumentReady(() =>
			{ 
		%>
		Link.Init();
		<% 
			}).Render(); %>
	</body>
</html>