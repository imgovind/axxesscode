﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NonUser>" %>
<span class="wintitle">Edit Non-Software User | <%= Current.AgencyName %></span>
<div id="editNonUserContent" class="wrapper main">
<%  using (Html.BeginForm("Update", "NonUser", FormMethod.Post, new { @id = "editNonUserForm" })) { %>
	<%= Html.Hidden("Id", Model.Id) %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditNonUser_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditNonUser_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditNonUser_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditNonUser_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>