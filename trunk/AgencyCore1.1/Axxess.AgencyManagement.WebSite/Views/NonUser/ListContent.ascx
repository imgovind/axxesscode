﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserListViewData<NonUser>>" %>
<%  var pageName = "NonUserList";  %>
<%= Html.Filter("SortParams", string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]), new { @class = "sort-parameters" })%>
<% float count = Model.IsUserCanEdit.ToInteger() + Model.IsUserCanDelete.ToInteger(); %>
<% Func<NonUser, object> actionFunc = delegate(NonUser u)
   {
		var action = "";
		if (Model.IsUserCanEdit)
		{
			action += string.Concat("<a href=\"javascript:void(0);\" onclick=\"NonUser.Edit('", u.Id, "')\" class=\"link\">Edit</a>");
		}
		if (Model.IsUserCanDelete)
		{
			action += string.Concat("<a href=\"javascript:void(0);\" onclick=\"NonUser.Delete('", u.Id, "','", u.DisplayName, "')\" class=\"link\">Delete</a>");
		}
		return action;
   };%>
<%= Html.Telerik().Grid(Model.Users).Name(pageName + "_Grid").HtmlAttributes(new { @class = "aggregated" }).Columns(columns => {
		columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
		columns.Bound(u => u.Id).Width(95).Sortable(false).Template(actionFunc).Visible(count > 0 && !Current.IsAgencyFrozen).Title("Action");
	}).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "DisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
        }
    })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>