﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Non-Software User | <%= Current.AgencyName %></span>
<div id="newNonUserContent" class="wrapper main">
<%  using (Html.BeginForm("Add", "NonUser", FormMethod.Post, new { @id = "newNonUserForm" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewNonUser_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", "", new { @id = "NewNonUser_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewNonUser_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", "", new { @id = "NewNonUser_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>