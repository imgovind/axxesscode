﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserListViewData<NonUser>>" %>
<span class="wintitle">List Non-Software Users | <%= Current.AgencyName %></span>
<%  var pageName = "NonUserList";  %>
<div class ="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
	<ul class="fr buttons">
<%  if (Model.IsUserCanAdd) { %>
    	<li><a onclick="NonUser.New()">New Non-Software User</a></li>
    	<div class="clr"></div>
<%  } %>
<%  if (Model.IsUserCanExport) { %>
		<li><a url="Export/NonUsers" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac medium">
        <div class="button fr"><a url="NonUser/ListContent" class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("ListContent", Model); %></div>
</div>