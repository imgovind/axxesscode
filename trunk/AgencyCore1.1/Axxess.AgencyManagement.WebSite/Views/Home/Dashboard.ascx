﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Dashboard | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%= Html.Hidden("PerferredService", Current.PreferredService.ToString()) %>
    <ul id="widget-column-1" class="widgets">
        <li class="widget stationary" id="intro-widget">
            <h5 class="widget-head">Hello,&#160;<%= Current.DisplayName %>!</h5>
            <% Html.RenderPartial("~/Views/Widget/CustomMessage.ascx"); %>
        </li>
        <li class="widget stationary" id="news-widget">
            <h5 class="widget-head">News/Updates</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/NewsFeed.ascx"); %>    
            </div>
        </li>
        <% if(Current.AcessibleServices.Has(AgencyServices.HomeHealth)){ %>
            <%  if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
            <li class="widget" id="recertsdue-widget">
                <h5 class="widget-head">Past Due Recerts</h5>
                <div class="widget-content">
                    <% Html.RenderPartial("~/Views/Widget/RecertsPastDue.ascx"); %>
                </div>
            </li>
            <%  } %>
        <%} %>
    </ul>
    <ul id="widget-column-2" class="widgets">
        <li class="widget" id="local-widget">
            <h5 class="widget-head">Local</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Local.ascx"); %>
            </div>
        </li>
        <%  if (!Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) && !Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
        <li class="widget" id="schedule-widget">
            <h5 class="widget-head">My Scheduled Tasks</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Schedule.ascx"); %>
            </div>
        </li>
        <%  } %>
        <% if(Current.AcessibleServices.Has(AgencyServices.HomeHealth)){ %>
        <%  if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        <li class="widget" id="upcomingrecert-widget">
            <h5 class="widget-head">Upcoming Recerts</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/RecertsUpcoming.ascx"); %>
            </div>
        </li>
        <%  } %>
        <%  } %>
    </ul>
    <ul id="widget-column-3" class="widgets">
        <li class="widget" id="messages-widget">
            <h5 class="widget-head">Messages</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Messages.ascx"); %>
            </div>
        </li>
        <%  if (Current.Permissions.IsInPermission(AgencyServices.HomeHealth | AgencyServices.PrivateDuty, ParentPermission.Patient, PermissionActions.ViewList) && !Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) && !Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
        <li class="widget" id="birthday-widget">
            <h5 class="widget-head">Patient Birthdays</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Birthday.ascx"); %>
            </div>
        </li>
        <%  } %>
        <% if(Current.AcessibleServices.Has(AgencyServices.HomeHealth)){ %>
        <%  if (Current.Permissions.IsInPermission(AgencyServices.HomeHealth, ParentPermission.MedicareClaim, PermissionActions.ViewClaimHistory) && !Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer) && !Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
        <li class="widget" id="claims-widget">
            <h5 class="widget-head">Outstanding Claims</h5>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Claims.ascx"); %>
            </div>
        </li>
        <%  } %>
        <% } %>
    </ul>
</div>
<script type="text/javascript"> Home.Init(); </script>