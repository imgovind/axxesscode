﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Recent Software Updates</span>
<div id="softwareUpdates" class="standard-chart">
    <ul>
        <li>
            <span class="recentchangesdate">Date</span>
            <span class="recentchangescomments">Comments</span>
        </li>
    </ul>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">12/06/2012</span>
            <span class="recentchangescomments">
		        <strong>Major Enhancement: </strong>Medicare Secondary Payer Claim Billing - Ability to bill Medicare when Medicare is not responsible for paying first. See <a target="_blank" href="http://www.cms.gov/Medicare/Coordination-of-Benefits/MedicareSecondPayerandYou/index.html">CMS.gov</a> for more information on Medicare Secondary Payer & You.
		        <br /><strong>Major Enhancement: </strong>Added Patient Document Upload feature to the Patient Charts.
		        <br /><strong>Enhancement: </strong>Flowing the Pulse Oximetry field in Skilled Nurse Visit note to the O2 Saturation column in the vital signs log.
		        <br /><strong>Enhancement: </strong>User-Interface improvements to the Medicare claims history page.
		        <br /><strong>Enhancement: </strong>Added the Quick Search box to the User List. Go to Admin -> Lists -> Users, type User name in the Quick Search box to filter by name.
		        <br /><strong>Enhancement: </strong>Added the Status Column to the Excel Export for the Past Due Visit Report.
		        <br /><strong>Enhancement: </strong>User-Interface improvements to the Login and Forgot Password pages.
		        <br /><strong>Minor Enhancement: </strong>Maximize vertical spacing on the Non-Admitted Patients List.
		        <br /><strong>Minor Enhancement: </strong>Non-software user licenses can be deleted properly in the License manager. Go to Admin -> License Manager.
		        <br /><strong>Minor Enhancement: </strong>Message widget dropdown delete functionality restored.
		        <br /><strong>Minor Enhancement: </strong>The <em>Not Yet Started</em> Status in the "Visit by Status" Report pulls past due visit documents.
		        <br /><strong>Minor Enhancement: </strong>Sixty-Day Summary documents account for temperatures with the "F" appended to the end.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">10/08/2012</span>
            <span class="recentchangescomments">
		        <strong>New Feature: </strong>Added a new report called <i>Patient Admissions by Internal Referrer</i> under the statistical reports.
		        <br /><strong>Enhancement: </strong>Revamp of the Return Reason process (Red Sticky Notes) in the QA Center.
		        <br /><strong>Enhancement: </strong>Wound care attachments in OASIS Assessments and Skilled Nursing notes can be deleted from the details page.
		        <br /><strong>Enhancement: </strong>Added Load Previous Note feature to the Skilled Nursing Psych Note and Skilled Nursing Psych Assessment.
		        <br /><strong>Enhancement: </strong>Completed Missed Visit notes are displayed in red font in both the Patient Charts and the Schedule Center.   
	            <br /><strong>Enhancement: </strong>OT Evaluation Note layout issue resolved for smaller screen resolutions.  
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">10/03/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Emergency Contact Information added to the New/Edit Referral Page.
	            <br /><strong>Enhancement: </strong>In the New/Edit Contact pages, an Extension Number field added to the Contact Office Phone.
	            <br /><strong>Enhancement: </strong>Added the ability to load pay rate information from one user to another user.  
	            <br /><strong>Enhancement: </strong>In Managed Care Claims, added the ability to post multiple payments and multiple adjustments to claims.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">09/29/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Missed Visit documents can be reviewed in the QA Center once it is completed by a clinician.
	            <br /><strong>Enhancement: </strong>On the dashboard, patient names are hyperlinked to open the Patient Charts.
	            <br /><strong>Enhancement: </strong>On the dashboard, Task names are hyperlinked to open the documents for edit/update.
                <br /><strong>Enhancement: </strong>OASIS assessments and Visit Notes optimized for faster load times.
	            <br /><strong>Enhancement: </strong>On the Contact List, contacts with comments will show a yellow sticky note. Go to View -> List -> Contacts. 
	            <br /><strong>Enhancement: </strong>Fixed the issue with exporting the Infection log to Microsoft Excel. Go to View -> Lists -> Infection Logs, click on Excel Export button.
	            <br /><strong>Enhancement: </strong>Added the ability to send PT Discharge documents to the Order Management center. Check 'Send as Order' checkbox if you want to send to the Physician.
	            <br /><strong>Enhancement: </strong>The Physician Credentials appear after the Physician&#8217;s name in the physician list. Go to View -> Lists -> Physicians.
	            <br /><strong>Enhancement: </strong>Updates to the COTA Visit Note to include a Narrative section and expanded the Assessment box.
	            <br /><strong>Enhancement: </strong>Updates to the ST Visit Notes and ST Discharge Notes to make it more consistent with the other Therapy Notes.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">09/10/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature: </strong>Ability to print Messages from your inbox in the message center.
                <br /><strong>New Feature: </strong>Added a new report called <i>Visits by Type</i>, which displays any/all documents based on patients and clinicians.
                <br /><strong>New Feature: </strong>New Skilled Nursing Pediatric Assessment Note.
                <br /><strong>Enhancement: </strong>Significantly improved the column spacing on all reports.
                <br /><strong>Enhancement: </strong>Added Insurance, Policy Number, Address, and Gender columns to Non-Admission Patients. Also added the ability to export the Non-Admission list to Excel.
                <br /><strong>Enhancement: </strong>Added templates to all Comment sections in the PT and OT Visit Notes, Evaluation Notes and Reassessment Notes. 
                <br /><strong>Enhancement: </strong>Added templates to the Comments section in the New and Edit Referral pages.
                <br /><strong>Enhancement: </strong>Added Physician&#8217;s PECOS status to the physician list in View -> List -> Physicians.
                <br /><strong>Enhancement: </strong>Added Evacuation Zone column to the Patient Survey Census Report
                <br /><strong>Enhancement: </strong>Automatically Refresh the Patient Charts when the Primary Physician is changed in the Patient Profile page.
                <br /><strong>Enhancement: </strong>Added Patient&#8217;s Middle Initial to the patient list in the Communication Note.
                <br /><strong>Enhancement: </strong>Added a Date Filter to the Reassign Task page to limit the reassignment of tasks to a selected date period.
                <br /><strong>Enhancement: </strong>Added blank forms for the SN Pediatric Visit Note and the SN Pediatric Assessment Note. Go to View -> Blank Forms.
                <br /><strong>Enhancement: </strong>Added Payment Date column to Remittance Advices. Go to Billing -> Medicare/Medicare HMO -> Remittance Advice.
                <br /><strong>Enhancement: </strong>Automatically complete the City and State fields whenever a Zip Code is entered throughout the system.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">08/02/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature: </strong>New version of the Skilled Nursing Pediatric Note introduced.
                <br /><strong>New Feature: </strong>Added a new schedule report called <i>Visits By Status</i>, which displays scheduled visits based on status.
                <br /><strong>New Feature: </strong>Added a new billing report called <i>Unbilled Managed Care Claims</i>
                <br /><strong>New Feature: </strong>The Red Sticky Note can be edited in the Details page.
                <br /><strong>New Feature: </strong>Skilled Nursing Insulin Noon/HS Visit added to the list of items that can be scheduled. 
                <br /><strong>Enhancement: </strong>In the QA Center, added a date range filter for the items to be reviewed.
                <br /><strong>Enhancement: </strong>Added the ability to edit the gender field in the Add/Edit User page.
                <br /><strong>Enhancement: </strong>In the PT/OT Evaluations, ReEvaluations, and ReAssessments notes, the "Send as an Order" checkbox is checked by default.
                <br /><strong>Enhancement: </strong>Added "Verify Insurance" tab to the managed care claim page. This allows billers to update pertinent insurance information such as claim modifiers within that particular claim.
                <br /><strong>Enhancement: </strong>The Print Queue utilizes the printer icon instead of hyperlinking the name of the document to be printed.
                <br /><strong>Enhancement: </strong>Added the PT/OT Reassessments to the blank forms page.
                <br /><strong>Enhancement: </strong>Added the ability to check Medicare Eligibility for referals in the new and edit referal pages.
                <br /><strong>Enhancement: </strong>In the Print Queue page, added a date range filter.
                <br /><strong>Enhancement: </strong>In the Schedule Center, the list of discipline tasks has been sorted alphabetically.
                <br /><strong>Enhancement: </strong>Added more editable fields to the Manage Company Information page to allow agencies more control over their information.
                <br /><strong>Enhancement: </strong>DNR (Do Not Resuscitate) field added to the New/Edit Patient page and the New/Edit Referral page. This information is displayed in the patient profile as well.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">07/05/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature: </strong>Added a new billing report called <i>Managed Care Claims History By Status</i>, which displays Managed Care Claims based on status.
                <br /><strong>Enhancement: </strong>In the managed care claims history page, the claim amount reflects the total charges of the claim (supply/visit totals) instead of the HIPPS Code payment amount determined by the corresponding OASIS assessment.
                <br /><strong>Enhancement: </strong>In the message inbox, added the ability to page through messages.
                <br /><strong>Enhancement: </strong>Added Prior Level of Function section to the PT Evaluation/ReEvaluation/ReAssessment.
                <br /><strong>Enhancement: </strong>Added the ability to check Medicare Eligibility for patients in the edit patient profile page with one click.
                <br /><strong>Enhancement: </strong>From the list of Insurances/Payors, agencies can enter/edit addresses for Medicare Intermediaries. This will flow directly into the UB-04 Form for RAP/Final Claims.
            </span>
        </li>
    </ol>
    <ol>
        <li class="even">
            <span class="recentchangesdate">06/26/2012</span>
            <span class="recentchangescomments">
                <strong>New Feature: </strong>Under the Billing Menu, there is a new feature called &lsquo;All Insurances/Payors&#8217;. This page allows you to create Medicare and all non-Medicare claims for all your branches/locations in a central place.
                <br /><strong>Enhancement: </strong>In the Start-of-Care, Recertification, and Resumption-of-Care OASIS assessments, the allergy section has been updated to be more consistent with the allergy profile in the patient charts. 
                <br /><strong>Enhancement: </strong>Final Claims with wound care supplies using the revenue code 0623 will include an additional line item reporting the total charges of the wound care supplies. Per <i>Medicare Benefit Policy Manual</i> (CMS Pub 100-02, Ch. 7, § 50.4.1), agencies can report non-routine wound care supplies using the revenue code 0623.
                <br /><strong>Enhancement: </strong>HCFA-1500 Form updated to include the insurance provider address at the top of the page. In addition, several fields have been prefilled with default values.
                <br /><strong>Enhancement: </strong>In OASIS assessments and skilled nursing notes, pictures uploaded to the wound sheet are now available under the task details. Click on the details link in the patient charts or schedule center to view the wound care pictures even if the OASIS assessment or skilled nursing note has been completed.
            </span>
        </li>
    </ol>
    <ol>
        <li class="odd">
            <span class="recentchangesdate">06/04/2012</span>
            <span class="recentchangescomments">
                <strong>Enhancement: </strong>Increased the font size on all reports.
                <br /><strong>Enhancement: </strong>In the New User page, you can set permissions for the new user based on another user&#8217;s permissions.
                <br /><strong>New Feature: </strong>Added a new report called <i>Employee Permissions</i>, which displays a list of permissions set for all your users.
            </span>
        </li>
    </ol>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last")
    })
</script>