﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VitalSignLog>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "VitalSign", FormMethod.Post, new { @id = "VitalSign_Form" })) { %>
	<%= Html.Hidden("EntityId", Model.EntityId) %>
	<%= Html.Hidden("PatientId", Model.PatientId) %>
	<fieldset>
		<legend>Details</legend>
		<%= Html.Partial("Detail", Model) %>
	</fieldset>
	<fieldset>
		<legend>Vital Signs</legend>
		<%= Html.Partial("Content", Model) %>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>