﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VitalSignLog>" %>
<div class="column">
	<div class="row">
		<label class="fl">Date</label>
		<div class="fr">
			<%= Html.DatePicker("VisitDate.Date", Model.VisitDate, true) %>
		</div>
	</div>
</div>
<div class="column">
	<div class="row">
		<label class="fl">Time</label>
		<div class="fr">
			<input name="VisitDate.Time" class="time-picker required" value="<%= Model.VisitDate.IsValid() ? Model.VisitDate.ToShortTimeString() : string.Empty %>" />
		</div>
	</div>
</div>