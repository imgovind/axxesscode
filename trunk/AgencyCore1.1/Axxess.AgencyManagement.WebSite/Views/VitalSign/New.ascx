﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientNameViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "VitalSign", FormMethod.Post, new { @id = "VitalSign_Form" })) { %>
	<%= Html.Hidden("EntityId", Model.EntityId) %>
	<%= Html.Hidden("PatientId", Model.PatientId) %>
	<fieldset>
		<legend>Details</legend>
		<%= Html.Partial("Detail", new VitalSignLog(true)) %>
	</fieldset>
	<fieldset>
		<legend>Vital Signs</legend>
		<%= Html.Partial("Content", new VitalSignLog(true)) %>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save">Save</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>