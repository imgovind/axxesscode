﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VitalSignLog>" %>
<div id="VitalSign_Content">
	<input type="hidden" name="Id" id="VitalSigns_Id" value="<%= Model.Id %>"/>
	<%= Html.Hidden("IsNew", Model.IsNew) %>
	<input type="hidden" id="VitalSign_VitalSignsChanged" name="VitalSignsChanged" value="false" />
	<div class="column vat" style="min-width: 375px">
		<div class="row">
			<label for="Resp" class="fl">Resp</label>
			<div class="fr">
				<input name="Resp" class="shorter decimal" value="<%= Model.Resp %>" />
				<%= Html.EnumDropDown<RegularIrregular>("RespStatus", true, new { @class = "shorter" })%>
			</div>
		</div>
		<div class="row">
			<label for="Weight" class="fl">Weight</label>
			<div class="fr">
				<input name="Weight" class="shorter decimal" value="<%= Model.Weight %>" />
				<%= Html.EnumDropDown<WeightUnit>("WeightUnit", false, new { @class = "shorter" })%>
			</div>
		</div>
		<div class="row">
			<label for="Temp" class="fl">Temperature</label>
			<div class="fr">
				<input name="Temp" class="shorter decimal" value="<%= Model.Temp %>" />
				<%= Html.EnumDropDown<TemperatureUnit>("TempUnit", false, new { @class = "shorter less" })%>
				<%= Html.EnumDropDown<TemperatureLocation>("TempLocation", true, new { @class = "shorter" })%>
			</div>
		</div>
		<div id="VitalSign_Pulses">
			<div class="row">
				<label for="Pulse" class="fl">Pulse</label>
				<div class="fr">
					<input name="Pulse" size="3" class="shorter less numeric" value="<%= Model.Pulse %>" />
					<%= Html.EnumDropDown<PulseLocation>("PulseLocation", true, new { @class = "shorter" }) %>
					<%= Html.EnumDropDown<RegularIrregular>("PulseStatus", true, new { @class = "shorter" })%>
					<span class="img none icon16" <%= Model.Pulse2 > 0 ? string.Empty : "style='display:none'"%> />
				</div>
			</div>
			<div class="row removable<%= Model.Pulse2 > 0 ? string.Empty : " hidden" %>">
				<label for="Pulse" class="fl">Pulse</label>
				<div class="fr">
					<input name="Pulse2" size="3" class="shorter less numeric" value="<%= Model.Pulse2 %>"  />
					<%= Html.EnumDropDown<PulseLocation>("PulseLocation2", true, new { @class = "shorter" }) %>
					<%= Html.EnumDropDown<RegularIrregular>("PulseStatus2", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row" <%= Model.Pulse2 > 0 ? "style='display:none'" : ""%>>
				<div class="fr">
				    <a class="add-button img icon16 plus img-margin"></a>
				    <a class="add-button">Add Pulse</a>
				</div>
			</div>
		</div>
	</div>
	<div class="column vat" style="min-width: 375px">
		<div class="row">
			<label for="BloodGlucose" class="fl">Blood Glucose</label>
			<div class="fr">
				<input name="BloodGlucose" width="4" class="shortest numeric" value="<%= Model.BloodGlucose %>" />
				<%= Html.EnumDropDown<BloodGlucoseType>("BloodGlucoseType", true, new { @class = "short less" })%>
			</div>
		</div>
		<div class="row">
			<label for="OxygenSaturation" class="fl">Oxygen Saturation</label>
			<div class="fr">
				<input name="OxygenSaturation" class="shortest decimal" value="<%= Model.OxygenSaturation %>" />%
				<%= Html.EnumDropDown<OxygenSaturationType>("OxygenSaturationType", false, new { @class = "short" })%>
			</div>
		</div>
		<div id="VitalSign_BloodPressures">
			<div class="row">
				<label for="BloodPressure" class="fl">Blood Pressure</label>
				<div class="fr">
					<% var areOtherBloodPressureSet = Model.BloodPressure2.IsNotNullOrEmpty()
						|| Model.BloodPressure3.IsNotNullOrEmpty() || Model.BloodPressure4.IsNotNullOrEmpty()
						|| Model.BloodPressure5.IsNotNullOrEmpty() || Model.BloodPressure6.IsNotNullOrEmpty(); %>
					<input name="BloodPressure" class="shorter less blood-pressure" value="<%= Model.BloodPressure %>" />
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition", true, new { @class = "shorter" })%>
					<span class="img none icon16" <%= areOtherBloodPressureSet ? string.Empty : "style='display:none'"%> />
				</div>
			</div>
			<div class="row removable<%= Model.BloodPressure2.IsNotNullOrEmpty() ? string.Empty : " hidden" %>">
				<label for="BloodPressureTwo" class="fl">Blood Pressure</label>
				<div class="fr">
					<input name="BloodPressure2" class="shorter less blood-pressure" value="<%= Model.BloodPressure2 %>" />
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide2", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition2", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row removable<%= Model.BloodPressure3.IsNotNullOrEmpty() ? string.Empty : " hidden" %>">
				<label for="BloodPressureThree" class="fl">Blood Pressure</label>
				<div class="fr">
					<input name="BloodPressure3" class="shorter less blood-pressure" value="<%= Model.BloodPressure3 %>"/>
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide3", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition3", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row removable<%= Model.BloodPressure4.IsNotNullOrEmpty() ? string.Empty : " hidden" %>">
				<label for="BloodPressureFour" class="fl">Blood Pressure</label>
				<div class="fr">
					<input name="BloodPressure4" class="shorter less blood-pressure" value="<%= Model.BloodPressure4 %>"/>
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide4", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition4", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row removable<%= Model.BloodPressure5.IsNotNullOrEmpty() ? string.Empty : " hidden" %>">
				<label for="BloodPressureFive" class="fl">Blood Pressure</label>
				<div class="fr">
					<input name="BloodPressure5" class="shorter less blood-pressure" value="<%= Model.BloodPressure5 %>"/>
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide5", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition5", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row removable<%= Model.BloodPressure6.IsNotNullOrEmpty() ? string.Empty : " hidden" %>">
				<label for="BloodPressureSix" class="fl">Blood Pressure</label>
				<div class="fr">
					<input name="BloodPressure6" class="shorter less blood-pressure" value="<%= Model.BloodPressure6 %>"/>
					<%= Html.EnumDropDown<BloodPressureSide>("BloodPressureSide6", true, new { @class = "shorter" })%>
					<%= Html.EnumDropDown<BloodPressurePosition>("BloodPressurePosition6", true, new { @class = "shorter" })%>
					<a class="remove-button img icon16 ecks"></a>
				</div>
			</div>
			<div class="row" <%= Model.BloodPressure6.HasValue() ? "style='display:none'" : ""%>>
				<div class="fr">
				    <a class="add-button img icon16 plus img-margin"></a>
				    <a class="add-button">Add Blood Pressure</a>
				</div>
			</div>
		</div>
	</div>
	<div class="wide-column">
		<div class="row">
			<label for="Comments">Comments</label>
            <div class="ac"><%= Html.TextArea("Comments") %></div>
		</div>
	</div>
</div>
