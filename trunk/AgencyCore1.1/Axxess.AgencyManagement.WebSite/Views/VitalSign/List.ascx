﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EntityVitalSignViewData>" %>
<div class="wrapper main">
<%  if (!Current.IsAgencyFrozen) { %>
    <div class="button fl"><a onclick="VitalSign.New('<%= Model.PatientId %>','<%= Model.EntityId %>')">New Vital Sign</a></div>
<%  } %>
	<div class="button"><a onclick="$(this).closest('div.main').find('.t-grid').data('tGrid').rebind()">Refresh</a></div>
	<div class="button fr"><a onclick="Patient.VitalSigns.Load('<%= Model.PatientId %>')">Vital Sign Charts</a></div>
	<div class="clr"></div>
	<%  Html.Telerik().Grid<VitalSignLogLean>().Name("List_EntityVitalSigns").HtmlAttributes(new { @class = "position-relative", @style = "height:15em" }).DataKeys(keys => keys.Add(o => o.Id).RouteKey("id")).Columns(columns => {
		    columns.Bound(v => v.Date).Format("{0:MM/dd/yyyy}").Title("Date");
			columns.Bound(v => v.Date).Format("{0:HH:mm tt}").Title("Time");
			columns.Bound(v => v.Temp).ClientTemplate("<#= Temp != null ? Temp + (TempUnit == 1 ? ' °F' : ' °C') : '' #>").Title("Temperature");
			columns.Bound(v => v.Resp).Title("Respiration");
			columns.Bound(v => v.Pulse);
			columns.Bound(v => v.BloodPressure);
			columns.Bound(v => v.Id).Title("Actions").ClientTemplate("<a onclick=\"VitalSign.Edit('<#=PatientId#>','<#=EntityId#>','<#=Id#>')\" class=\"link\">Edit</a> | <a onclick=\"VitalSign.Delete('<#=PatientId#>','<#=EntityId#>','<#=Id#>')\" class=\"link\">Delete</a>").Width(100);
		}).DataBinding(dataBinding => dataBinding.Ajax().Select("EntityListContent", "VitalSign", new { patientId = Model.PatientId, entityId = Model.EntityId }).OperationMode(GridOperationMode.Client)).ClientEvents(evnts => evnts
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Vital Signs found for this " + Model.EntityName + ".").Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true)).Render(); %>
</div>