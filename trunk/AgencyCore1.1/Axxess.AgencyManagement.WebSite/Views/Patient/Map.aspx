﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Patient>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%= Html.Telerik()
            .StyleSheetRegistrar()
            .DefaultGroup(group => group
            .Add("globals.css")
            .Add("centers.css")
            .Add("forms.css")
            .Combined(false)
            .Compress(true)
            .CacheDurationInDays(1)
            .Version(Current.AssemblyVersion)) %>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=<%= AppSettings.GoogleAPIKey %>&sensor=false"></script>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.10.1.min.js")
            .Add("Patient/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Map.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "Forms.js")
            .Add("Plugins/Other/layout.min.js")
            .Compress(true)
            .Combined(false)
            .CacheDurationInDays(1)
            .Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Map.Init()
    <%  }).Render(); %>
</head>
<body>
    <div class="wrapper layout">
        <div class="ui-layout-west">
            <div class="top ac">
                <fieldset class="address-block">
                    <label>Starting Address</label>
                    <%= Html.MapAddress("Start", Current.UserAddress, new {}) %>
                    <div class="start-specify">
                        <input type="text" name="StartAddress" value="" placeholder="Address" />
                    </div>
                </fieldset>
                <fieldset class="address-block">
                    <label>Patient&#8217;s Address</label>
                    <input type="text" name="EndAddress" value="<%= Model.AddressFull %>" />
                </fieldset>
                <div id="Map_Recalculate" class="button"><a>Map It</a></div>
            </div>
            <div id="PatientMapDirections" class="bottom"></div>
        </div>
        <div id="PatientMap" class="ui-layout-center"></div>
    </div>
</body>
</html>