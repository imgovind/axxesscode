﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Maps | <%= Model.DisplayName %></span>
<iframe class="full-screen" src="/Patient/MapIframe?id=<%= Model.Id %>"></iframe>