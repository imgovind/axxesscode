﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientSelectionViewData>" %>
<%  Html.Telerik().Grid(Model.Patients).Name("PatientSelectionGrid").HtmlAttributes(new { @class = "patient-list" }).Columns(columns =>
    {
        columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" });
        columns.Bound(p => p.ShortName).Title("First Name").HtmlAttributes(new { @class = "searchF" });
        columns.Bound(p => p.Id).HtmlAttributes(new { @class = "pid" }).Hidden(true);
		columns.Bound(p => p.PatientIdNumber).Hidden(true);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AllSort", "PatientProfile", new { branchId = Guid.Empty, statusId = Model.PatientListStatus, paymentSourceId = 0, servicesId = 1 })).ClientEvents(events => events
       .OnDataBinding("Patient.Charts.PatientSelector.OnDataBinding")
       .OnDataBound("Patient.Charts.PatientSelector.OnDataBound")
       .OnRowSelect("Patient.Charts.PatientSelector.OnRowSelected")
    ).Sortable().Selectable().Scrollable().Footer(false).Sortable(sr => sr.OrderBy(so => so.Add(s => s.LastName).Ascending())).Render(); %>