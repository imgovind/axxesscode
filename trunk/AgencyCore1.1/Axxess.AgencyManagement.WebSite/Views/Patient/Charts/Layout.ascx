﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientChartsViewData>" %>
<span class="wintitle">Patient Charts | <%= Current.AgencyName %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top ac"><% Html.RenderPartial("Charts/Filters", Model); %></div>
        <div class="bottom service">
           <%= Html.Hidden("CurrentPatientId", Model != null && Model.SelectionViewData != null ? Model.SelectionViewData.CurrentPatientId : Guid.Empty, new { @id = "PatientCharts_CurrentPatientId" })%>
           <% Html.RenderPartial("Charts/Patients", Model.SelectionViewData); %>
       </div>
    </div>
    <div id="PatientMainResult" class="ui-layout-center">
<%  if (Model != null && Model.SelectionViewData != null && Model.SelectionViewData.Count > 0) { %>
    <%  Html.RenderPartial(string.Format("~/Views/PatientProfile/Charts/Data.ascx", Model.ApplicationName), Model.PatientScheduleEvent ?? new PatientScheduleEventViewData()); %>
<%  } else { %>
        <script type="text/javascript">
            $("#PatientMainResult").empty().html(U.MessageWarn("No Patients", "No Patients found that fit your search criteria."));
        </script>
<%  } %>
    </div>
</div>
