﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEmergencyContact>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("EditEmergencyContact", "Patient", FormMethod.Post, new { @id = "editEmergencyContactForm", @class = "mainform" })) { %>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_EmergencyContactPatientID" })%>
<%= Html.Hidden("Id", Model.Id, new { @id ="Edit_EmergencyContactID" })%>
<fieldset>
    <div class="column">
        <div class="row">
            <label for="Edit_EmergencyContact_FirstName" class="fl">First Name</label>
            <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_EmergencyContact_FirstName", @maxlength = "100" })%></div>
        </div>
        <div class="row">
            <label for="Edit_EmergencyContact_LastName" class="fl">Last Name</label>
            <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_EmergencyContact_LastName", @maxlength = "100" })%></div>
        </div>
        <div class="row">
            <label for="Edit_EmergencyContact_Relationship" class="fl">Relationship</label>
            <div  class="fr"><%= Html.TextBox("Relationship", Model.Relationship, new { @id = "Edit_EmergencyContact_Relationship" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="Edit_EmergencyContact_PrimaryPhoneArray1" class="fl">Primary Phone</label>
            <div class="fr"><%= Html.PhoneOrFax("PhonePrimaryArray", Model.PrimaryPhone, "Edit_EmergencyContact_PrimaryPhoneArray", "required" )%></div>
        </div>
        <div class="row">
            <label for="Edit_EmergencyContact_AlternatePhoneArray1" class="fl">Alternate Phone</label>
            <div class="fr"><%= Html.PhoneOrFax("PhoneAlternateArray", Model.AlternatePhone, "Edit_EmergencyContact_AlternatePhoneArray", "") %></div>
        </div>
        <div class="row">
            <label for="Edit_EmergencyContact_Email" class="fl">Email</label>
            <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_EmergencyContact_Email", @class = "email", @maxlength = "100" }) %></div>
        </div>
        <div class="row">
            <label for="Edit_EmergencyContact_SetPrimary" class="fl">Set Primary</label>
            <div class="fl"><%= Html.CheckBox("IsPrimary",Model.IsPrimary ,new { @id = "Edit_EmergencyContact_SetPrimary" })%></div>
        </div>
    </div>
</fieldset>
<ul class="buttons ac">
    <li><a class="save close">Save</a></li>
    <li><a class="close">Close</a></li>
</ul>
<%  } %>
</div>