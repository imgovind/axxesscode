﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("NewEmergencyContact", "Patient", FormMethod.Post, new { @id = "newEmergencyContactForm", @class = "mainform" })) { %>
    <%= Html.Hidden("PatientId", Model, new { @id = "New_EmergencyContact_PatientID" })%>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_EmergencyContact_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "New_EmergencyContact_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_EmergencyContact_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "New_EmergencyContact_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_EmergencyContact_Relationship" class="fl">Relationship</label>
                <div class="fr"><%= Html.TextBox("Relationship", string.Empty, new { @id = "New_EmergencyContact_Relationship" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_EmergencyContact_PrimaryPhoneArray1" class="fl">Primary Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhonePrimaryArray", string.Empty, "New_EmergencyContact_PrimaryPhoneArray", "required")%></div>
            </div>
            <div class="row">
                <label for="New_EmergencyContact_AlternatePhoneArray1" class="fl">Alternate Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneAlternateArray", string.Empty, "New_EmergencyContact_AlternatePhoneArray", "")%></div>
            </div>
            <div class="row">
                <label for="New_EmergencyContact_Email" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "New_EmergencyContact_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_EmergencyContact_SetPrimary" class="fl">Set Primary</label>
                <div class="fl"><%= Html.CheckBox("IsPrimary", true, new { @id = "New_EmergencyContact_SetPrimary" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>