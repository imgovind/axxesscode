﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">User Access | <%= Model.DisplayName %></span>
<%= Html.Hidden("PatientId", Model.Id, new { @id = "UserAccessPatientId" }) %>
<div class="wrapper main blue">
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
        <div class="fl">Note: Changes are saved automatically.</div>
    </fieldset>
    <%= Html.Telerik().Grid<SelectedUser>().Name("List_PatientUserAccess").HtmlAttributes(new { @class = "grid-container" }).Columns(columns => {
            columns.Bound(u => u.UserId).Hidden(true);
            columns.Bound(u => u.Selected).Title("").ClientTemplate("<input type=\"checkbox\" onclick=\"Patient.UserAccess.UserClick(this)\" value=\"<#= PatientUserId #>\"/>").Width(40).Sortable(false);
            columns.Bound(u => u.DisplayName).Title("Name");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetUserAccess","Patient", new { patientId = Model.Id }).OperationMode(GridOperationMode.Client)).ClientEvents(c => c
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
            .OnRowDataBound("Patient.UserAccess.OnRowBound")
        ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
</div>