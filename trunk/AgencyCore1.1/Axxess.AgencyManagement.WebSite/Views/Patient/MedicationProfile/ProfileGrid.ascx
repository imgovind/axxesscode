﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OasisMedicationProfileViewData>" %>
<ul class="buttons fl">
    <%if (Model.Profile.IsUserCanAdd){ %> <li><a onclick="Medication.Add('<%= Model.Profile.Id %>','<%= Model.AssessmentType %>')">Add Medication</a></li><%} %>
	<%if (Model.Profile.IsUserCanSign){ %> <li><a onclick="Medication.Snapshot('<%= Model.Profile.PatientId%>','<%= Model.AssessmentType %>','<%= Model.Service.ToArea() %>')">Sign Medication Profile</a></li><%} %>
    <%if (Model.Profile.IsUserCanViewList){ %><li><a onclick="Acore.OpenPrintView({ Url: '<%= Model.Service.ToArea() %>/PatientProfile/MedicationProfilePrint/<%=Model.Profile.PatientId %>', PdfUrl: '<%=Model.Service.ToArea() %>/PatientProfile/MedicationProfilePdf', PdfData: { id: '<%=Model.Profile.PatientId %>' } })">Print Medication Profile</a></li><%} %>
</ul>
<ul class="buttons fr">
	<li class="red"><a onclick="Medication.DrugDrugInteractions('<%= Model.Profile.Id %>', '<%= Model.Service.ToArea() %>')">Drug Interactions</a></li>
</ul>
<div class="clr"></div>
<div id="<%= string.Format("{0}_activeMeds", Model.AssessmentType) %>" class="acore-grid">
    <ul>
        <li class="ac"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="grid-check">LS</span>
            <span class="grid-eleventh">Start Date</span>
            <span class="grid-quarter">Medication &#38; Dosage</span>
            <span class="grid-tenth">Frequency</span>
            <span class="grid-tenth">Route</span>
            <span class="grid-tenth">Type</span>
            <span class="grid-sixth">Classification</span>
            <span>Action</span>
        </li>
    </ul>
    <ol>
<%  if (Model != null && Model.Profile != null && Model.Profile.Medication != null) { %>
    <%  int i = 1; %>
    <%  var medications = Model.Profile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable).ToList(); %>
    <%  foreach (var med in medications) { %>
        <li>
            <span class="grid-check"><input name="LongStanding" disabled="true" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
            <span class="grid-eleventh"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="grid-quarter"><%= med.MedicationDosage %></span>
            <span class="grid-tenth"><%= med.Frequency %></span>
            <span class="grid-tenth"><%= med.Route %></span>
            <span class="grid-tenth"><%= med.MedicationType.Text %></span>
            <span class="grid-sixth"><%= med.Classification %></span>
            <span class="grid-seventh">
				<% if (Model.Profile.IsUserCanEdit) { %><a class="link" onclick="Medication.Edit('<%= Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Edit</a><% } %>
				<% if (Model.Profile.IsUserCanDelete) { %><a class="link" onclick="Medication.Delete('<%=Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Delete</a><% } %>
				<% if (Model.Profile.IsUserCanDiscontinue) { %><a class="link" onclick="Medication.Discontinue('<%= Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Discontinue</a><% } %>
            </span>
        </li>
        <%  i++; %>
    <%  } if(medications.Count == 0){ %>
        <li class="no-hover"><h4>No Active Medications</h4></li>
	<%  } %>
<%  } else { %>
        <li class="no-hover"><h4>No Active Medications</h4></li>
<%  } %>
    </ol>
</div>
<div id="<%= string.Format("{0}_dischargeMeds", Model.AssessmentType) %>" class="acore-grid">
    <ul>
        <li class="ac"><h3>Discontinued Medication(s)</h3></li>
        <li>
            <span class="grid-check">LS</span>
            <span class="grid-eleventh">Start Date</span>
            <span class="grid-quarter">Medication &#38; Dosage</span>
            <span class="grid-tenth">Frequency</span>
            <span class="grid-tenth">Route</span>
            <span class="grid-eleventh">Type</span>
            <span class="grid-tenth">Classification</span>
            <span class="grid-eleventh">D/C Date</span>
            <span>Action</span>
        </li>
    </ul>
    <ol class="dc">
<%  if (Model != null && Model.Profile != null && Model.Profile.Medication != null) { %>
    <%  int j = 1; %>
    <%  var medications = Model.Profile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable).ToList(); %>
    <%  foreach (var med in medications) { %>
        <li>
            <span class="grid-check"><input name="LongStanding" disabled="true" type="checkbox" value="<%= med.Id %>" id="DcLongStanding<%= j %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
            <span class="grid-eleventh"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="grid-quarter"><%= med.MedicationDosage %></span>
            <span class="grid-tenth"><%= med.Frequency %></span>
            <span class="grid-tenth"><%= med.Route %></span>
            <span class="grid-eleventh"><%= med.MedicationType.Text %></span>
            <span class="grid-tenth"><%= med.Classification %></span>
            <span class="grid-eleventh"><%= med.DCDateFormated %></span>
            <span class="grid-seventh">
             <% if (Model.Profile.IsUserCanEdit) { %><a class="link" onclick="Medication.Edit('<%= Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Edit</a><% } %>
             <% if (Model.Profile.IsUserCanDelete) { %><a class="link" onclick="Medication.Delete('<%= Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Delete</a><% } %>
             <% if (Model.Profile.IsUserCanReactivate) { %><a class="link" onclick="Medication.Activate('<%= Model.Id %>','<%= med.Id %>','<%= Model.AssessmentType %>')">Activate</a><% } %>
             </span>
        </li>
        <%  j++; %>
    <%  } if(medications.Count == 0){ %>
        <li class="no-hover"><h4>No D/C Medications</h4></li>
	<%  } %>
<%  } else { %>
        <li class="no-hover"><h4>No D/C Medications</h4></li>
<%  } %>
    </ol>
</div>
<ul class="buttons ac">
    <li><a onclick="Medication.Refresh('<%= Model.Profile.Id %>','<%= Model.AssessmentType %>')">Refresh Medications</a></li>
</ul>
<div class="clr"></div>