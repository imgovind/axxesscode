<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdatePatientMedication", "Patient", FormMethod.Post, new { @id = "editMedicationForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Medication_Id" })%>
    <%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Medication_ProfileId" })%>
    <fieldset class="editmed">
        <legend>Edit Medication</legend>
        <div class="wide-column">
            <div class="row">
	            <div class="checkgroup one-wide">
		            <div class="option">
			            <div class="wrapper">
							<%= Html.CheckBox("IsLongStanding", Model.IsLongStanding, new { @id = "Edit_Medication_IsLongStanding" })%>
							<label for="Edit_Medication_IsLongStanding">Long Standing</label>
			            </div>
		            </div>
	            </div>
            </div>
             <div class="row" id="Edit_Medication_StartDateRow">
				<label for="Edit_Medication_StartDate" class="fl">Start Date</label>
				<div class="fr"><input type="text" class="date-picker" name="StartDate" id="Edit_Medication_StartDate" /></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_MedicationDosage" class="fl">Medication &#38; Dosage</label>
                <div class="fr"><%= Html.TextBox("MedicationDosage", Model.MedicationDosage.IsNotNullOrEmpty() ? Model.MedicationDosage : string.Empty, new { @id = "Edit_Medication_MedicationDosage", @class = "long required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Classification" class="fl">Classification</label>
                <div class="fr"><%= Html.TextBox("Classification", Model.Classification.IsNotNullOrEmpty() ? Model.Classification : string.Empty, new { @id = "Edit_Medication_Classification", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Frequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox("Frequency", Model.Frequency.IsNotNullOrEmpty() ? Model.Frequency : string.Empty, new { @id = "Edit_Medication_Frequency", @class = "Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Route" class="fl">Route</label>
                <div class="fr"><%= Html.TextBox("Route", Model.Route.IsNotNullOrEmpty() ? Model.Route : string.Empty, new { @id = "Edit_Medication_Route", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Type" class="fl">Type</label>
                <div class="fr">
                    <%  var medicationTypes = new SelectList(new[] { 
					        new SelectListItem { Text = "New", Value = "N" },
					        new SelectListItem { Text = "Changed", Value = "C" },
					        new SelectListItem { Text = "Unchanged", Value = "U" }
				        }, "Value", "Text", Model.MedicationType.Value.IsNotNullOrEmpty() ? Model.MedicationType.Value : "N" ); %>
                    <%= Html.DropDownList("medicationType", medicationTypes)%>
                </div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>