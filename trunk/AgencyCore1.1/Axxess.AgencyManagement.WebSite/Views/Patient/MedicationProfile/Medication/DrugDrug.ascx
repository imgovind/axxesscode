﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<%  using (Html.BeginForm("DrugDrugInteractionsPdf", "PatientProfile", FormMethod.Post, new { @id = "drugDrugInteractionForm", @area = "", }))
    { %>
<%= Html.Hidden("patientId", Model.PatientId)%>
<div class="note">
    <fieldset>
        <legend>Instructions</legend>
        <div class="wide-column">
            <div class="row">Please select the medications below that you will like to check for interactions and click on the 'Check Interactions' button.</div>
        </div>
    </fieldset>
</div>
<div id="MedProfile_DrugDrug" class="acore-grid selectable">
    <ul>
        <li class="ac"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="grid-check"><input id="selectAllDrugs" type="checkbox" /></span>
            <span class="grid-quarter">Medication &#38; Dosage</span>
            <span class="grid-fifth">Frequency</span>
            <span class="grid-fifth">Route</span>
            <span class="grid-quarter">Classification</span>
        </li>
    </ul>
    <ol>
    <%  if (Model != null) { %>
        <%  var medications = Model.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
        <%  if (medications.IsNotNullOrEmpty()) { %>
            <%  foreach (var med in medications) { %>
                <%  if (med.LexiDrugId.IsNotNullOrEmpty()) { %>
        <li>
		    <span class="grid-check"><input name="drugsSelected" type="checkbox" value="<%= med.LexiDrugId %>" /></span>
			<span class="grid-quarter"><%= med.MedicationDosage %></span>
			<span class="grid-fifth"><%= med.Frequency %></span>
			<span class="grid-fifth"><%= med.Route %></span>
			<span class="grid-quarter"><%= med.Classification%></span>
		</li>
				<%  } else { %>
		<li>
			<span>The drug <%= med.MedicationDosage %> has lost it.</span>
		</li>
				<%  } %>
			<%  } %>
        <%  } else { %>
        <li class="ac"><span class="darkred">No Active Medications</span></li>
        <%  } %>
    <%  } %>
    </ol>
</div>
<ul class="buttons ac">
    <li><a class="download-file">Check Interactions</a></li>
    <li><a class="close">Close</a></li>
</ul>
<%  } %>
<script type="text/javascript">
    $("#selectAllDrugs").on("change", function() {
        $("#MedProfile_DrugDrug input[name='drugsSelected']").each(function() {
            $(this).prop("checked", $("#selectAllDrugs").prop("checked"))
        })
    });
    $("#drugDrugInteractionForm .acore-grid ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last");
    });
</script>