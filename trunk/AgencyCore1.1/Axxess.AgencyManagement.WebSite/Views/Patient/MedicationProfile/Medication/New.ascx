<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("InsertNewMedication", "Patient", FormMethod.Post, new { @id = "newMedicationForm" })) { %>
    <%= Html.Hidden("medicationProfileId", Model, new { @id = "New_Medication_ProfileId", @default = Model })%>
    <%= Html.Hidden("LexiDrugId", "", new { @id = "New_Medication_DrugId" })%>   
    <fieldset class="newmed">
        <legend>New Medication</legend>
        <div class="wide-column">
            <div class="row">
	            <ul class="checkgroup one-wide">
		            <li class="option">
			            <div class="wrapper">
							<%= Html.CheckBox("IsLongStanding", false, new { @id = "New_Medication_IsLongStanding" }) %>
							<label for="New_Medication_IsLongStanding">Long Standing</label>
			            </div>
		            </li>
	            </ul>
            </div>
            <div class="row" id="New_Medication_StartDateRow">
				<label for="New_Medication_StartDate" class="fl">Start Date</label>
				<div class="fr"><input type="text" class="date-picker" name="StartDate" id="New_Medication_StartDate" /></div>
            </div>
            <div class="row">
                <label for="New_Medication_MedicationDosage" class="fl">Medication &#38; Dosage</label>
                <div class="fr"><%= Html.TextBox("MedicationDosage", "", new { @id = "New_Medication_MedicationDosage", @class = "long required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="New_Medication_Classification" class="fl">Classification</label>
                <div class="fr"><%= Html.TextBox("Classification", "", new { @id = "New_Medication_Classification", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_Medication_Frequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox("Frequency", "", new { @id = "New_Medication_Frequency", @class = "Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_Medication_Route" class="fl">Route</label>
                <div class="fr"><%= Html.TextBox("Route", "", new { @id = "New_Medication_Route", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_Medication_Type" class="fl">Type</label>
                <div class="fr">
                    <%  var medicationTypes = new SelectList(new[] { 
                            new SelectListItem { Text = "New", Value = "N" },
                            new SelectListItem { Text = "Changed", Value = "C" },               
                            new SelectListItem { Text = "Unchanged", Value = "U" }               
                        }, "Value", "Text");%>
                    <%= Html.DropDownList("medicationType", medicationTypes)%>
                </div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
		<li><a class="save close">Save &#38; Exit</a></li>
		<li><a class="save clear">Save &#38; Add Another</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
<%  } %>
</div>