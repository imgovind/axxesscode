﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationListViewData>" %>
<%  var canUserEdit = Model.IsUserCanEdit; %>
<%  var canUserDelete = Model.IsUserCanDelete; %>
<%  var canUserRestore = Model.IsUserCanReactivate; %>
<%  var canUserDiscontinue = Model.IsUserCanDiscontinue; %>
<div id="MedProfile_activeMeds" class="acore-grid">
    <ul>
        <li class="ac"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="grid-check">LS</span>
            <span class="grid-eleventh">Start Date</span>
            <span class="grid-quarter">Medication &#38; Dosage</span>
            <span class="grid-tenth">Frequency</span>
            <span class="grid-tenth">Route</span>
            <span class="grid-tenth">Type</span>
            <span class="grid-sixth">Classification</span>
<%  if ((canUserEdit || canUserDelete || canUserDiscontinue)) { %>
            <span>Action</span>
<%  } %>
        </li>
    </ul>
    <ol>
<%  if (Model != null && Model.List != null) { %>
    <%  int i = 1; %>
    <%  var activeMedications = Model.List.Where(m => m.MedicationCategory == "Active").ToList(); %>
    <%  if (activeMedications.IsNotNullOrEmpty()) { %>
        <%  foreach (var med in activeMedications) { %>
        <li>
            <span class="grid-check"><input name="LongStanding" disabled="true" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
            <span class="grid-eleventh"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="grid-quarter"><%= med.MedicationDosage %></span>
            <span class="grid-tenth"><%= med.Frequency %></span>
            <span class="grid-tenth"><%= med.Route %></span>
            <span class="grid-tenth"><%= med.MedicationType.Text %></span>
            <span class="grid-sixth"><%= med.Classification %></span>
            <span>
	            <%  if (canUserEdit) { %><a onclick="Medication.Edit('<%=Model.Id %>','<%= med.Id %>')" class="link">Edit</a><%  } %>
	            <%  if (canUserDelete) { %><a onclick="Medication.Delete('<%=Model.Id %>','<%= med.Id %>')" class="link">Delete</a><%  } %>
			    <%  if (canUserDiscontinue) { %><a onclick="Medication.Discontinue('<%= Model.Id %>','<%= med.Id %>')" class="link">Discontinue</a><%  } %>
            </span>
        </li>
            <%  i++; %>
        <%  } %>
    <%  } else { %>
        <li class="no-hover"><h4>No Active Medications</h4></li>
    <%  } %>
<%  } %>
    </ol>
</div>
<div id="MedProfile_dischargeMeds" class="acore-grid">
    <ul>
        <li class="ac"><h3>Discontinued Medication(s)</h3></li>
        <li>
            <span class="grid-check">LS</span>
            <span class="grid-eleventh">Start Date</span>
            <span class="grid-quarter">Medication &#38; Dosage</span>
            <span class="grid-tenth">Frequency</span>
            <span class="grid-tenth">Route</span>
            <span class="grid-eleventh">Type</span>
            <span class="grid-tenth">Classification</span>
            <span class="grid-eleventh">D/C Date</span>
<%  if ((canUserEdit || canUserDelete || canUserRestore)) { %>
            <span>Action</span>
<%  } %>
        </li>
    </ul>
    <ol class="dc">
<%  if (Model != null && Model.List != null) { %>
    <%  int j = 1; %>
    <%	var inactiveMedications = Model.List.Where(m => m.MedicationCategory == "DC").ToList(); %>
    <%  if (inactiveMedications.IsNotNullOrEmpty()) { %>
        <%  foreach (var med in inactiveMedications) { %>
        <li>
            <span class="grid-check"><input name="LongStanding" disabled="true" type="checkbox" value="<%= med.Id %>" id="DcLongStanding<%= j %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
            <span class="grid-eleventh"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="grid-quarter"><%= med.MedicationDosage %></span>
            <span class="grid-tenth"><%= med.Frequency %></span>
            <span class="grid-tenth"><%= med.Route %></span>
            <span class="grid-eleventh"><%= med.MedicationType.Text %></span>
            <span class="grid-tenth"><%= med.Classification %></span>
            <span class="grid-eleventh"><%= med.DCDateFormated %></span>
            <span>
				<%  if(canUserEdit) { %><a onclick="Medication.Edit('<%=Model.Id %>','<%= med.Id %>')" class="link">Edit</a><%  } %>
				<%  if (canUserDelete) { %><a onclick="Medication.Delete('<%=Model.Id %>','<%= med.Id %>')" class="link">Delete</a><%  } %>
				<%  if (canUserRestore) { %><a onclick="Medication.Activate('<%= Model.Id %>','<%= med.Id %>')" class="link">Activate</a><%  } %>
            </span>
        </li>
            <%  j++; %>
        <%  } %>
    <%  } else { %>
        <li class="no-hover"><h4>No Discontinued Medications</h4></li>
    <%  } %>
<%  } %>
    </ol>
</div>