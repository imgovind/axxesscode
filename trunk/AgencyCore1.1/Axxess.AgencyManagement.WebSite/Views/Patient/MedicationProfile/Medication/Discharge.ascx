﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<div class="wrapper main">
	<form id="dischargeMedicationProfileForm" action="Patient/UpdateMedicationStatus" method="post">
		<%= Html.Hidden("medProfileId", Model.ProfileId)%>
		<%= Html.Hidden("medicationId", Model.Id)%>
		<%= Html.Hidden("medicationCategory", "DC")%>
		<fieldset>
			<legend>Discontinue Medication</legend>
			<div class="wide-column">
				<div class="row no-input">
					<label>Medication</label>
					<div class="fr"><%= Model.MedicationDosage %></div>
				</div>
				<div class="row">
					<label for="Discharge_Medication_Date">D/C Date</label>
					<div class="fr"><input type="text" class="date-picker required" name="dischargeDate" id="Discharge_Medication_Date" /></div>
				</div>
			</div>
		</fieldset>
		<input type="hidden" id="Discharge_Medication_AssessmentType" />
		<ul class="buttons ac">
			<li><a class="save close">Discontinue</a></li>
			<li><a class="close">Cancel</a></li>
		</ul>
	</form>
</div>