﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile | <%= Model.DisplayName %></span>
<div class="wrapper main note">
<%= Html.Hidden("Id", Model.MedicationProfile.Id, new  {@id = "medicationProfileId" })%>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<%  var area = ViewContext.RouteData.DataTokens["area"].ToSafeString(); %>
<%  var service = (int)area.ToEnum(AgencyServices.HomeHealth); %>
    <fieldset>
        <legend>Medication Profile</legend>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Current Episode</label>
                <div class="fr"><%= string.Format(" {0} – {1}", Model != null && Model.StartDate.IsValid() ? Model.StartDate.ToShortDateString() : string.Empty, Model != null && Model.StartDate.IsValid() ? Model.EndDate.ToShortDateString() : "No current episode") %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Allergies</label>
                <div class="fr"><%= Model != null ? Model.AllergyProfile.ToSafeString() : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Primary Diagnosis</label>
                <div class="fr"><%= Model != null ? Model.PrimaryDiagnosis : string.Empty %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Secondary Diagnosis</label>
                <div class="fr"><%= Model != null ? Model.SecondaryDiagnosis : string.Empty %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Pharmacy Name</label>
                <div class="fr"><%= Model != null ? Model.PharmacyName : string.Empty %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Pharmacy Phone</label>
                <div class="fr"><%= Model != null ? Model.PharmacyPhone.ToPhone() : string.Empty %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons fl">
		<% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen) { %>
			<% if(Model.IsUserCanAdd){ %>
				<li><a onclick="Medication.Add('<%= Model.MedicationProfile.Id %>')">Add Medication</a></li>
			<% } %>
			<% if(Model.IsUserCanSign){ %>
				<li><a onclick="Medication.Snapshot('<%= Model.PatientId%>', undefined, '<%= area %>')">Sign Medication Profile</a></li>
			<% } %>
		<% } %>
		<% if(Model.IsUserCanPrint){ %>
			<li><a onclick="Acore.OpenPrintView({ Url: '<%= area %>/PatientProfile/MedicationProfilePrint/<%=Model.PatientId %>', PdfUrl: '<%= area %>/PatientProfile/MedicationProfilePdf', PdfData: { 'id': '<%=Model.PatientId %>' } })">Print Medication Profile</a></li>
		<% } %>
    </ul>
    <ul class="buttons fr">
        <li class="red"><a onclick="Medication.DrugDrugInteractions('<%= Model.MedicationProfile.Id %>', '<%= area %>')">Drug Interactions</a></li>
        <li><a onclick="Medication.SnapshotHistory('<%= Model.PatientId%>')">Signed Medication Profiles</a></li>
    </ul>
    <div class="clr"></div>
    <div id="MedProfile_medications"><%  Html.RenderPartial("~/Views/Patient/MedicationProfile/Medication/List.ascx", new MedicationListViewData { Id = Model.MedicationProfile.Id, IsUserCanAdd = Model.MedicationProfile.IsUserCanAdd, IsUserCanDelete = Model.MedicationProfile.IsUserCanDelete, IsUserCanEdit = Model.MedicationProfile.IsUserCanEdit, IsUserCanReactivate = Model.MedicationProfile.IsUserCanReactivate, IsUserCanDiscontinue = Model.MedicationProfile.IsUserCanDiscontinue, List = Model.MedicationProfile.Medication.ToObject<List<Medication>>() }); %></div>
    <% if (Model.IsUserCanViewLog) { %>
    <a class="fr img icon32 log" onclick="Log.LoadMedicationLog('<%= Model.MedicationProfile.PatientId %>')"></a>
    <% } %>
    <ul class="buttons ac">
        <li><a onclick="Medication.Refresh('<%= Model.MedicationProfile.Id %>')">Refresh Medications</a></li>
    </ul>
</div>