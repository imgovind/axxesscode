﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<%  var area = ViewContext.RouteData.DataTokens["area"].ToSafeString(); %>
<%  var service = (int)area.ToEnum(AgencyServices.HomeHealth); %>
<span class="wintitle">Medication Profile Snapshot | <%= Model.DisplayName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("SignMedicationHistory", "Patient", new { area = "" }, FormMethod.Post, new { @id = "newMedicationProfileSnapShotForm"})) { %>
    <%= Html.Hidden("ProfileId", Model.MedicationProfile.Id)%>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <%= Html.Hidden("Service", service) %>
    <fieldset>
	    <legend>Medication Profile SnapShot</legend>
		<div class="column">
		    <div class="row no-input">
			    <label class="fl">Patient Name</label>
				<div class="fr"><%= Model.DisplayName %></div>
			</div>
	<%  if (service != 2) { %>    
            <div class="row">
                <label for="MedProfile_EpisodeRange" class="fl">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes(service, "EpisodeId", Model.EpisodeId.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "MedProfileSnapShot_EpisodeRange", @class = "required not-zero" })%></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="MedProfile_PrimaryDiagnosis" class="fl">Primary Diagnosis</label>
                <div class="fr"><%= Html.TextBox("PrimaryDiagnosis", Model.PrimaryDiagnosis, new { @id = "MedProfile_PrimaryDiagnosis" })%></div>
            </div>
            <div class="row">
                <label for="MedProfile_PharmacyName" class="fl">Pharmacy Name</label>
                <div class="fr"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "MedProfile_PharmacyName" })%></div>
            </div>
		</div>
		<div class="column">
			<div class="row">
                <label for="MedProfile_Physician" class="fl">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", (Model != null && !Model.PhysicianId.IsEmpty()) ? Model.PhysicianId.ToString() : string.Empty, new { @id = "MedProfile_Physician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <label for="MedProfile_SecondaryDiagnosis" class="fl">Secondary Diagnosis</label>
                <div class="fr"><%= Html.TextBox("SecondaryDiagnosis", Model.SecondaryDiagnosis, new { @id = "MedProfile_SecondaryDiagnosis" })%></div>
            </div>
            <div class="row">
                <label for="MedProfile_PharmacyPhone1" class="fl">Pharmacy Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PharmacyPhoneArray", Model.PharmacyPhone, "MedProfile_PharmacyPhone", "") %></div>
            </div>
		</div>
	</fieldset>
	<fieldset>
	    <legend>Allergies</legend>
		<div class="wide-column">
		    <div class="row ac"><%= Html.TextArea("Allergies", Model != null ? Model.AllergyProfile.ToSafeString() : string.Empty, new { @id = "MedProfileSnapShot_Allergies", @class = "allergy-text", @readonly = "readonly" })%></div>
			<div class="row">
			    <div class="button"><a onclick="Allergy.List('<%= Model.PatientId %>')">Open Allergy Profile</a></div>
			</div>
		</div>
	</fieldset>
	<div class="clr"></div>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Medication(s)</h3></li>
            <li>
                <span class="grid-check">LS</span>
                <span class="grid-eleventh">Start Date</span>
                <span class="grid-quarter">Medication &#38; Dosage</span>
                <span class="grid-tenth">Frequency</span>
                <span class="grid-tenth">Route</span>
                <span class="grid-tenth">Type</span>
                <span class="grid-sixth">Classification</span>
            </li>
        </ul>
        <ol>
    <%  if (Model != null) { %>
        <%  int i = 1; %>
        <%  var medications = Model.MedicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable).ToList(); %>
        <%  foreach (var med in medications) { %>
		    <li>
		        <span class="grid-check"><input name="LongStanding" disabled="disabled" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding.ToChecked() %> /></span>
                <span class="grid-eleventh"><%= med.StartDate.ToZeroFilled() %></span>
                <span class="grid-quarter"><%= med.MedicationDosage %></span>
                <span class="grid-tenth"><%= med.Frequency %></span>
                <span class="grid-tenth"><%= med.Route %></span>
                <span class="grid-tenth"><%= med.MedicationType.Text %></span>
                <span class="grid-sixth"><%= med.Classification %></span>
            </li>
            <%  i++; %>
        <%  } %>
        <%  if (medications.IsNullOrEmpty()) { %>
            <li class="no-hover"><h4>No Active Medications found.</h4></li>
        <%  } %>
    <%  } %>
        </ol>
    </div>
    <fieldset>
	    <legend>Signature</legend>
		<p class="al normal">
		    <strong>Drug Regimen Review Acknowledgment:</strong>
		    I have reviewed all the listed medications for potential adverse effects, drug reactions, including ineffective
		    drug therapy, significant side effects, significant drug interactions, duplicate drug therapy, and noncompliance
		    with drug therapy.
		</p>
		<br />
	    <div class="column">
			<div class="row">
				<label for="Signature" class="fl">Clinician Signature</label>
				<div class="fr"><%= Html.Password("Signature", "", new { @id = "MedicationProfile_ClinicianSignature", @class = "required" }) %></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="SignedDate" class="fl">Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="SignedDate" id="MedicationProfile_ClinicianSignatureDate" /></div>
			</div>
		</div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Sign</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>