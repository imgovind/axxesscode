﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<%  var canUserEdit = Model.IsUserCanEdit; %>
<%  var canUserDelete = Model.IsUserCanDelete; %>
<span class="wintitle">Signed Medication Profiles | <%= Model.DisplayName %></span>
<div class="wrapper">
    <%= Html.Telerik().Grid<MedicationProfileHistory>().Name("List_MedicationProfileSnapshotHistory_Grid").DataKeys(keys => keys.Add(m => m.Id)).DataBinding(dataBinding => dataBinding.Ajax()
           .Select("MedicationSnapshotHistory", "Patient", new { PatientId= this.Model.Id, Area = "" })
                           .Update("UpdateMedicationSnapshotHistory", "Patient", new { PatientId = this.Model.Id, Area = "" })
                           .Delete("DeleteMedicationSnapshotHistory", "Patient", new { PatientId = this.Model.Id, Area = "" })
        ).Columns(columns => {
            columns.Bound(m => m.UserName).Title("Signed By").Width(250).Sortable(true).ReadOnly();
            columns.Bound(m => m.SignedDate).Template(m => m.SignedDateFormatted).ClientTemplate("<#=SignedDateFormatted #>").Title("Signed Date").Width(120).Sortable(true);
			columns.Bound(m => m.Id).Visible(Model.IsUserCanPrint).Width(15).HtmlAttributes( new { @class = "centered-unpadded-cell" }).ReadOnly().ClientTemplate("<a onclick=\"Acore.OpenPrintView({Url:'<#= Area #>/PatientProfile/MedicationProfileSnapshotPrint/<#= Id #>',PdfUrl:'<#= Area #>/PatientProfile/MedicationProfileSnapshotPdf',PdfData:{id:'<#= Id #>'}})\" class=\"img icon16 print\"></a>").Sortable(false).Title("");
            columns.Command(commands => {
				if (canUserEdit) commands.Edit();
			    if (canUserDelete) commands.Delete();
		    }).Width(150).Title("Action").Visible((canUserEdit || canUserDelete));
		}).ClientEvents(e => e
			.OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
			.OnComplete("Medication.OnComplete")
			.OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Signed Medication Profiles could be found.").Editable(editing => editing.Mode(GridEditMode.InLine)).Scrollable().Sortable() %>
</div>