﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<form id="dischargeMedicationProfileForm" action="/Patient/UpdateMedicationStatus" method="post">
    <fieldset>
        <legend>Discharge Medication</legend>
        <div class="wide-column">
            <div class="row">
                <label for="">Enter the discharge date</label>
                <div class="fr"><input type="text" class="date-picker" name="dischargeDate" id="MedicationProfile_DischargeDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
</form>