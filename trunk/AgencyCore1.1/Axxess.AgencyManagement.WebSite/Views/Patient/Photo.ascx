﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddPhoto", "Patient", FormMethod.Post, new { @id = "changePatientPhotoForm" })) { %>
    <%= Html.Hidden("patientId", Model.Id)%>
    <fieldset>
        <legend>Change Photo</legend>
        <div class="wide-column">
            <div class="row ac">
    <%  if (!Model.PhotoId.IsEmpty()) { %>
                <img src="/Asset/<%= Model.PhotoId.ToString() %>" alt="User Photo" />
    <%  } else { %>
                <img src="/Images/blank_user.jpeg" alt="User Photo" />
    <%  } %>
            </div>
            <div class="row">
                <label for="Change_Patient_Photo" class="fl">Upload Photo</label>
                <div class="fr"><input id="Change_Patient_Photo" type="file" name="photo" /></div>
            </div>
        </div>
    </fieldset>
    <div id="changePatientPhotoError" class="error-message"></div>
    <ul class="buttons ac">
        <li><a class="save close">Upload</a></li>
    <%  if (!Model.PhotoId.IsEmpty()) { %>
        <li><a onclick="Patient.Demographic.RemovePhoto('<%= Model.Id %>')">Remove</a></li>
    <%  } %>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>