﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle"><%= Model.ServiceName %> | Authorization List | <%= Model.DisplayName %> </span>
<div class="wrapper main blue">
    <ul class="fr buttons ac">
<%  if (Model.IsUserCanAdd) { %>
        <li><a class="add-authorization">Add Authorization</a></li>
        <div class="clr"></div>
<%  } %>
<%  if (Model.IsUserCanExport) { %>
        <li><a class="export" area="<%= Model.Service.ToArea() %>" url="/Export/Authorizations">Excel Export</a></li>
<% } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a area="<%= Model.Service.ToArea() %>" url="/Authorization/ListContent" class="grid-refresh">Refresh</a></div>
	<%--	<div class="filter ignore-grid-binding">
<%  if (!(Model.Services & Current.AcessibleServices).IsAlone()) { %>
		    <label for="Authorizations_ServiceId">Service</label>
			<%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", service, Model.Services, true, new { @id = "Authorizations_ServiceId" })%>
<%  } else { %>
			<%= Html.Hidden("ServiceId", (int)(Model.Services & Current.AcessibleServices), new { @Id = "Authorizations_ServiceId" })%>
<%  } %>
		</div>--%>
        <div class="filter"> <%= Html.Hidden("ServiceId", (int)Model.Service, new { @Id = "Authorizations_ServiceId" })%></div>
		<div class="filter"><%= Html.Hidden("PatientId", Model.Id) %></div>
		<div class="filter grid-search"></div>
    </fieldset>
<%  var isVisible =(Model.IsUserCanEdit || Model.IsUserCanDelete); %>
<%  var action = new List<string>(); %>
<%  var width = 0; %>
<%  if (Model.IsUserCanEdit) { %>
    <%  action.Add("<a class=\"edit\">Edit</a>"); %>
    <%  width += 70; %>
<%  } %>
<%  if (Model.IsUserCanDelete) { %>
    <%  action.Add("<a class=\"delete\">Delete</a>"); %>
    <%  width += 70; %>
<%  } %>
<%  var count = action.Count; %>
<%  var url = action.ToArray().Join(" | "); %>
    <%= Html.Telerik().Grid<Authorization>().Name("List_Authorizations").Columns(columns => {
            columns.Bound(a => a.Number1).Title("Authorization Number 1").Width(150).Sortable(false);
            columns.Bound(a => a.Number2).Title("Authorization Number 2").Width(150).Sortable(false);
            columns.Bound(a => a.Number3).Title("Authorization Number 3").Width(150).Sortable(false);
            columns.Bound(a => a.Branch).Width(180).Sortable(false);
			columns.Bound(a => a.StartDate).ClientTemplate("<#= U.FormatGridDate(StartDate) #>").Width(80);
			columns.Bound(a => a.EndDate).ClientTemplate("<#= U.FormatGridDate(EndDate) #>").Width(80);
            columns.Bound(a => a.Status).Sortable(false).Width(80);
            columns.Bound(a => a.Id).ClientTemplate("<a class=\"img icon16 print\" />").Title(" ").Sortable(false).Width(30).Visible(Model.IsUserCanPrint);
            columns.Bound(a => a.Id).ClientTemplate(url).Title("Action").Sortable(false).Width(width).Visible(isVisible);
        }).DataBinding(dataBinding => dataBinding
            .Ajax()
            .Select("ListContent", "Authorization", new { patientId = Model.Id, area = Model.Service.ToArea() })
            .OperationMode(GridOperationMode.Client))
                .ClientEvents(evnts => evnts
                                .OnRowDataBound("Patient.Authorization.OnRowDataBound")
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("U.OnTGridDataBound")
                .OnError("U.OnTGridError")
        ).Footer(false).NoRecordsTemplate("No Authorizations found.").Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>