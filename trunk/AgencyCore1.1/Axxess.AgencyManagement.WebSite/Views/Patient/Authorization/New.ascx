﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">New Authorization | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Authorization", FormMethod.Post, new { @id = "newAuthorizationForm", area = "" })) { %>
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_PatientId" class="fl">Patient Name</label>
                <div class="fr"><%= Html.PatientsFilteredByService((int)Current.AcessibleServices, "PatientId", Model.Id.ToString(), new { @id = "NewAuthorization_PatientId", @class = "required nozero" }) %></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_StartDate" class="fl">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" id="NewAuthorization_StartDate" /></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_EndDate" class="fl">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" id="NewAuthorization_EndDate" /></div>
            </div>
            <div id="NewAuthorization_Service">
                <%= Html.Partial("ServiceRow", new ServiceViewData(Model.RequestedService != AgencyServices.None ? Model.RequestedService : Current.PreferredService, Model.Services != AgencyServices.None ? Model.Services : Current.AcessibleServices))%>
            </div>
           <%-- <div class="row">
                <label for="NewAuthorization_LocationId" class="fl">Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("AgencyLocationId", Model.Profile != null ? Model.Profile.AgencyLocationId.ToString() : Guid.Empty.ToString(), 0, new { @id = "NewAuthorization_LocationId", @class = "branch-location" }) %></div>
            </div>--%>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_Status" class="fl">Status</label>
                <div class="fr"><%= Html.AuthorizationStatus("Status", Model.Profile != null ? Model.Profile.Status.ToString() : "0", new { @id = "NewAuthorization_Status" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_Insurance" class="fl">Insurance</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("InsuranceId", Model.Profile != null ? Model.Profile.PrimaryInsurance.ToString() : "0", (int)AgencyServices.HomeHealth, false, true, "-- Select Insurance --", new { @id = "NewAuthorization_Insurance", @class = "insurance required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber1" class="fl">Authorization Number 1</label>
                <div class="fr"><%= Html.TextBox("Number1", string.Empty, new { @id = "NewAuthorization_AuthNumber1", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber2" class="fl">Authorization Number 2</label>
                <div class="fr"><%= Html.TextBox("Number2", string.Empty, new { @id = "NewAuthorization_AuthNumber2", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber3" class="fl">Authorization Number 3</label>
                <div class="fr"><%= Html.TextBox("Number3", string.Empty, new { @id = "NewAuthorization_AuthNumber3", @maxlength = "30" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
        <%  var countVisitType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "1"); %>
        <%  var countHourType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "2"); %>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_SNVisit" class="fl">SN Count</label>
                <div class="fr">
                    <%= Html.TextBox("SNVisit", "", new { @id = "NewAuthorization_SNVisit", @class = "numeric shorter", @maxlength = "4" }) %>
                    <%= Html.DropDownList("SNVisitCountType", countVisitType, new { @id = "NewAuthorization_SNVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_PTVisit" class="fl">PT Count</label>
                <div class="fr">
                    <%= Html.TextBox("PTVisit", "", new { @id = "NewAuthorization_PTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("PTVisitCountType", countVisitType, new { @id = "NewAuthorization_PTVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_OTVisit" class="fl">OT Count</label>
                <div class="fr">
                    <%= Html.TextBox("OTVisit", "", new { @id = "NewAuthorization_OTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("OTVisitCountType", countVisitType, new { @id = "NewAuthorization_OTVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_STVisit" class="fl">ST Count</label>
                <div class="fr">
                    <%= Html.TextBox("STVisit", "", new { @id = "NewAuthorization_STVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("STVisitCountType", countVisitType, new { @id = "NewAuthorization_STVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_MSWVisit" class="fl">MSW Count</label>
                <div class="fr">
                    <%= Html.TextBox("MSWVisit", "", new { @id = "NewAuthorization_MSWVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("MSWVisitCountType", countVisitType, new { @id = "NewAuthorization_MSWVisitCountType", @class = "short" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_HHAVisit" class="fl">HHA Count</label>
                <div class="fr">
                    <%= Html.TextBox("HHAVisit", "", new { @id = "NewAuthorization_HHAVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("HHAVisitCountType", countVisitType, new { @id = "NewAuthorization_HHAVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_DieticianVisit" class="fl">Dietician Count</label>
                <div class="fr">
                    <%= Html.TextBox("DieticianVisit", "", new { @id = "NewAuthorization_DieticianVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("DieticianVisitCountType", countVisitType, new { @id = "NewAuthorization_DieticianVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_RNVisit" class="fl">RN Count</label>
                <div class="fr">
                    <%= Html.TextBox("RNVisit", "", new { @id = "NewAuthorization_RNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("RNVisitCountType", countHourType, new { @id = "NewAuthorization_RNVisitCountType", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_LVNVisit" class="fl">LVN Count</label>
                <div class="fr">
                    <%= Html.TextBox("LVNVisit", "", new { @id = "NewAuthorization_LVNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("LVNVisitCountType", countHourType, new { @id = "NewAuthorization_LVNVisitCountType", @class = "short" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="NewAuthorization_Comments" name="Comments" cols="5" rows="6" maxcharacters="500"></textarea>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>