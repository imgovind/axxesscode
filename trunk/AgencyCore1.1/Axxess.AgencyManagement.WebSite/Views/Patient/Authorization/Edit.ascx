﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<span class="wintitle">Edit Authorization | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Authorization", FormMethod.Post, new { area=Model.Service.ToArea(), @id = "editAuthorizationForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Authorization_Id" })%> 
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Authorization_PatientId" })%>     
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_PatientId" class="fl">Patient Name</label>
                <div class="fr"><label><%=Model.DisplayName %></label></div>
            </div>
            <div class="row">
                <label for="Edit_Authorization_StartDate" class="fl">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" id="Edit_Authorization_StartDate" /></div>
            </div>
            <div class="row">
                <label for="Edit_Authorization_EndDate" class="fl">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate.ToShortDateString() %>" id="Edit_Authorization_EndDate" /></div>
            </div>
            <%= Html.Partial("ServiceRow", new ServiceViewData(Model.Service, Model.PatientServices)) %>
          <%--  <div class="row">
                <label for="Edit_Authorization_LocationId" class="fl">Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("AgencyLocationId", Model != null ? Model.AgencyLocationId.ToString() : Guid.Empty.ToString(), 0, new { @id = "Edit_Authorization_LocationId", @class = "branch-location" })%></div>
            </div>--%>
        </div>
        <div class="column vat">
            <div class="row">
                <label for="Edit_Authorization_Insurance" class="fl">Insurance</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("InsuranceId", Model.InsuranceId.ToString(), (int)AgencyServices.HomeHealth, false, true, "-- Select Insurance --", new { @id = "Edit_Authorization_Insurance", @class = "insurance required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Authorization_AuthNumber1" class="fl">Authorization Number 1</label>
                <div class="fr"><%= Html.TextBox("Number1", Model.Number1, new { @id = "Edit_Authorization_AuthNumber1", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Authorization_AuthNumber2" class="fl">Authorization Number 2</label>
                <div class="fr"><%= Html.TextBox("Number2", Model.Number2, new { @id = "Edit_Authorization_AuthNumber2", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Authorization_AuthNumber3" class="fl">Authorization Number 3</label>
                <div class="fr"><%= Html.TextBox("Number3", Model.Number3, new { @id = "Edit_Authorization_AuthNumber3", @maxlength = "30" })%></div>
            </div>
			<div class="row">
			    <label for="Edit_Authorization_Status" class="fl">Status</label>
			    <div class="fr"><%= Html.AuthorizationStatus("Status", Model.Status, new { @id = "Edit_Authorization_Status" })%></div>
			</div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
        <div class="column">
            <table class="fixed">
                <col width="60" />
                <col width="35" />
                <col width="150" />
                <tbody>
                    <tr>
                        <td><label for="Edit_Authorization_SNVisit">SN Count</label></td>
                        <td><%= Html.TextBox("SNVisit", Model.SNVisit, new { @id = "Edit_Authorization_SNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("SNVisitCountType", Model.SNVisitCountType, new { @id = "Edit_Authorization_SNVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_PTVisit">PT Count</label></td>
                        <td><%= Html.TextBox("PTVisit", Model.PTVisit, new { @id = "Edit_Authorization_PTVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("PTVisitCountType",  Model.PTVisitCountType, new { @id = "Edit_Authorization_PTVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_OTVisit">OT Count</label></td>
                        <td><%= Html.TextBox("OTVisit", Model.OTVisit, new { @id = "Edit_Authorization_OTVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("OTVisitCountType", Model.OTVisitCountType, new { @id = "Edit_Authorization_OTVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_STVisit">ST Count</label></td>
                        <td><%= Html.TextBox("STVisit", Model.STVisit, new { @id = "Edit_Authorization_STVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("STVisitCountType", Model.SNVisitCountType, new { @id = "Edit_Authorization_STVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_MSWVisit">MSW Count</label></td>
                        <td><%= Html.TextBox("MSWVisit", Model.MSWVisit, new { @id = "Edit_Authorization_MSWVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("MSWVisitCountType", Model.MSWVisitCountType, new { @id = "Edit_Authorization_MSWVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="column">
            <table class="fixed">
                <col width="60" />
                <col width="35" />
                <col width="150" />
                <tbody>
                    <tr>
                        <td><label for="Edit_Authorization_HHAVisit">HHA Count</label></td>
                        <td><%= Html.TextBox("HHAVisit", Model.HHAVisit, new { @id = "Edit_Authorization_HHAVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("HHAVisitCountType", Model.HHAVisitCountType, new { @id = "Edit_Authorization_HHAVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_DieticianVisit">Dietician Count</label></td>
                        <td><%= Html.TextBox("DieticianVisit", Model.DieticianVisit, new { @id = "Edit_Authorization_DieticianVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("DieticianVisitCountType", Model.DieticianVisitCountType, new { @id = "Edit_Authorization_DieticianVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_RNVisit">RN Count</label></td>
                        <td><%= Html.TextBox("RNVisit", Model.RNVisit, new { @id = "Edit_Authorization_RNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("RNVisitCountType", Model.RNVisitCountType, new { @id = "Edit_Authorization_RNVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                    <tr>
                        <td><label for="Edit_Authorization_LVNVisit">LVN Count</label></td>
                        <td><%= Html.TextBox("LVNVisit", Model.LVNVisit, new { @id = "Edit_Authorization_LVNVisit", @class = "numeric sn", @maxlength = "4" })%></td>
                        <td><%= Html.AuthorizationType("LVNVisitCountType", Model.LVNVisitCountType, new { @id = "Edit_Authorization_LVNVisitCountType", @class = "shortselect" })%></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac"><%= Html.TextArea("Comments", Model.Comments, 6, 5, new { @id = "Edit_Authorization_Comments", @maxcharacters = "500" })%></div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>