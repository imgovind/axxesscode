﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<fieldset>
    <legend>Home Health Service</legend>
    <input type="hidden" name="profile.Index" value="1" />
    <input type="hidden" name="profile[1].ServiceType" value="1" />
    <div class="column">
        <div class="row">
            <label for="NewPatient_MedicareNumber" class="fl"><span class="green">(M0063)</span> Medicare Number</label>
            <div class="fr"><%= Html.TextBox("profile[1].MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : string.Empty, new { @id = "NewPatient_MedicareNumber", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="NewPatient_MedicaidNumber" class="fl"><span class="green">(M0065)</span> Medicaid Number</label>
            <div class="fr">
                <%= Html.TextBox("profile[1].MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : string.Empty, new { @id = "NewPatient_MedicaidNumber", @maxlength = "20" })%><br />
                <div class="ancillary-button"><a onclick="UserInterface.ShowPatientEligibility($('#NewPatient_MedicareNumber').val(), $('#NewPatient_LastName').val(), $('#NewPatient_FirstName').val(), $('#NewPatient_DOB').val(), $('input[name=Gender]:checked').val())">Verify Medicare Eligibility</a></div>
            </div>
        </div>
        <div class="row">
            <label for="NewPatient_Assign" class="fl">Assign to Clinician/Case Manager</label>
            <div class="fr"><%= Html.Clinicians("profile[1].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : string.Empty, new { @id = "NewPatient_Assign", @class = "required not-zero user-selector" })%></div>
        </div>
        <div class="row">
            <label for="NewPatient_LocationId" class="fl">Agency Branch</label>
            <div class="fr"><%= Html.BranchList("profile[1].AgencyLocationId", string.Empty, (int)AgencyServices.HomeHealth, new { @id = "NewPatient_LocationId", @class = "branch-location homehealth required not-zero", @service=(int)AgencyServices.HomeHealth })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="NewPatient_StartOfCareDate" class="fl"><span class="green">(M0030)</span> Start of Care Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[1].StartOfCareDate" id="NewPatient_StartOfCareDate"  /></div>
        </div>
        <div class="row">
            <label for="NewPatient_EpisodeStartDate" class="fl">Episode Start Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[1].EpisodeStartDate" id="NewPatient_EpisodeStartDate" /></div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <input id="NewPatient_CreateEpisode" type="checkbox" checked="checked" value="true" name="profile[1].ShouldCreateEpisode" />
                        <label for="NewPatient_CreateEpisode">
                            Create Episode &#38; Schedule Start of Care Visit<br />
                            <em>(If this box is unchecked, patient will be added to list of pending admissions)</em>                    
                        </label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="NewPatient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="profile[1].IsFaceToFaceEncounterCreated" />
                        <label for="NewPatient_IsFaceToFaceEncounterCreated">
                            Create a Face to Face Encounter<br />
                            <em>(This is applicable for SOC date after 01/01/2011)</em>
                        </label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="wide-column">
		<%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(null, string.Empty, "NewPatient_", "profile[1].", false))%>
        <div class="row ac">
            <label>Insurance/Payor Information</label>
        </div>
        <div class="row">
            <div class="inline-row three-wide">
                <div>
                    <div class="row">
                        <label for="NewPatient_PrimaryInsurance" class="fl">Primary</label>
                        <div class="fr"><%= Html.InsurancesByBranch("profile[1].PrimaryInsurance", string.Empty, Guid.Empty, (int)AgencyServices.HomeHealth, true, new { @id = "NewPatient_PrimaryInsurance", @class = "insurance required not-zero" })%></div>
                    </div>
                    <div id="NewPatient_PrimaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Primary", NamePrefix = "profile[1].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty }) %>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="NewPatient_SecondaryInsurance" class="fl">Secondary</label>
                        <div class="fr"><%= Html.InsurancesByBranch("profile[1].SecondaryInsurance", string.Empty, Guid.Empty, (int)AgencyServices.HomeHealth, true, new { @id = "NewPatient_SecondaryInsurance", @class = "insurance" })%></div>
                    </div>
                    <div id="NewPatient_SecondaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Secondary", NamePrefix = "profile[1].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
	                    
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="NewPatient_TertiaryInsurance" class="fl">Tertiary</label>
                        <div class="fr"><%= Html.InsurancesByBranch("profile[1].TertiaryInsurance", string.Empty, Guid.Empty,(int)AgencyServices.HomeHealth, true, new { @id = "NewPatient_TertiaryInsurance", @class = "insurance" })%></div>
                    </div>
                    <div id="NewPatient_TertiaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Tertiary", NamePrefix = "profile[1].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </fieldset>