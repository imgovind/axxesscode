﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
 <fieldset>
    <legend>Private Duty Service</legend>
    <input type="hidden" name="profile.Index" value="2" />
    <input type="hidden" name="profile[2].ServiceType" value="2" />
    <div class="column">
        <div class="row">
            <label for="NewPatient_PdMedicareNumber" class="fl"><span class="green">(M0063)</span> Medicare Number</label>
            <div class="fr"><%= Html.TextBox("profile[2].MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : string.Empty, new { @id = "NewPatient_PdMedicareNumber", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="NewPatient_PdMedicaidNumber" class="fl"><span class="green">(M0065)</span> Medicaid Number</label>
            <div class="fr"><%= Html.TextBox("profile[2].MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : string.Empty, new { @id = "NewPatient_PdMedicaidNumber", @maxlength = "20" })%></div>
        </div>
        <div class="row">
            <label for="NewPatient_PdAssign" class="fl">Assign to Clinician/Case Manager</label>
            <div class="fr"><%= Html.Clinicians("profile[2].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : string.Empty, new { @id = "NewPatient_PdAssign", @class = "required not-zero user-selector" })%></div>
        </div>
        <div class="row">
            <%  var htmlAttributesPdBranch = new Dictionary<string, string> {{ "id", "" }, { "class", "" }}; %>
            <label for="NewPatient_PdLocationId" class="fl">Agency Branch</label>
            <div class="fr"><%= Html.BranchList("profile[2].AgencyLocationId", string.Empty, (int)AgencyServices.PrivateDuty, new { @id = "NewPatient_PdLocationId", @class = "branch-location privateduty required not-zero", @service = (int)AgencyServices.PrivateDuty })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="NewPatient_PdStartOfCareDate" class="fl">Start of Care Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="profile[2].StartOfCareDate" id="NewPatient_PdStartOfCareDate"  /></div>
        </div>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <input id="NewPatient_PdCreateEpisode" type="checkbox" checked="checked" value="true" name="profile[2].ShouldCreateEpisode" />
                        <label for="NewPatient_PdCreateEpisode">
                            Create Care Period<br />
                            <em>(If this box is unchecked, patient will be added to list of pending admissions)</em>
                        </label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="NewPatient_PdEpisodeStartDate" class="fl">Care Period Start Date</label>
            <div class="fr"><input type="text" class="date-picker required care-period-start" name="profile[2].EpisodeStartDate" id="NewPatient_PdEpisodeStartDate" /></div>
        </div>
        <div class="row">
            <label for="NewPatient_PeriodLength" class="fl">Care Period Length</label>
            <div class="fr">
                <select id="NewPatient_PeriodLength" class="care-period-length">
                    <option value="14">14 Days</option>
                    <option value="30">30 Days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
                    <option value="120">120 Days</option>
                    <option value="specify" selected="selected">Specify End Date</option>
                </select>
            </div>
        </div>
        <div class="row">
            <label for="NewPatient_PdEpisodeEndDate" class="fl">Care Period End Date</label>
            <div class="fr"><input type="text" class="date-picker required care-period-end" name="profile[2].EpisodeEndDate" id="NewPatient_PdEpisodeEndDate" /></div>
        </div>
    </div>
    <div class="wide-column">
		<%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(null, string.Empty, "NewPatient_Pd", "profile[2].", false))%>
        <div class="row ac">
            <label>Insurance/Payor Information</label>
        </div>
        <div class="row">
            <div class="inline-row three-wide">
                <div>
                    <div class="row">
                        <label for="NewPatient_PdPrimaryInsurance" class="fl">Primary</label>
						<div class="fr"><%= Html.InsurancesByBranch("profile[2].PrimaryInsurance", string.Empty, Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "NewPatient_PdPrimaryInsurance", @class = "insurance required not-zero" })%></div>
                    </div>
                    <div id="NewPatient_PdPrimaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Primary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="NewPatient_PdSecondaryInsurance" class="fl">Secondary</label>
						<div class="fr"><%= Html.InsurancesByBranch("profile[2].SecondaryInsurance", "", Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "NewPatient_PdSecondaryInsurance", @class = "insurance"})%></div>
					</div>
					<div id="NewPatient_PdSecondaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Secondary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
					</div>
				</div>
				<div>
				    <div class="row">
				        <label for="NewPatient_PdTertiaryInsurance" class="fl">Tertiary</label>
				        <div class="fr"><%= Html.InsurancesByBranch("profile[2].TertiaryInsurance", "", Guid.Empty, (int)AgencyServices.PrivateDuty, true, new { @id = "NewPatient_PdTertiaryInsurance", @class = "insurance" })%></div>
				    </div>
				    <div id="NewPatient_PdTertiaryInsuranceContent" class="hidden">
	                    <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "NewPatient", InsuranceType = "Tertiary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = string.Empty, GroupName = string.Empty, GroupId = string.Empty, Relationship = string.Empty })%>
				    </div>
				</div>
			</div>
		</div>
	</div>
</fieldset>
   