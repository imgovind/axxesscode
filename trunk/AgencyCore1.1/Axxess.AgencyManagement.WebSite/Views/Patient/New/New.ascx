﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">New Patient | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Patient", FormMethod.Post, new { @id = "NewPatient_Form", @class = "mainform" })) { %>
	<fieldset>
		<legend>Patient Demographics</legend>
		<div class="column">
			<div class="row">
				<label for="NewPatient_FirstName" class="fl">
				    <span class="green">(M0040)</span>
				    First Name
				</label>
				<div class="fr"><%= Html.TextBox("FirstName", (Model != null && Model.FirstName.IsNotNullOrEmpty()) ? Model.FirstName : string.Empty, new { @id = "NewPatient_FirstName", @class = "required", @maxlength = "50" }) %></div>
			</div>
			<div class="row">
				<label for="NewPatient_MiddleInitial" class="fl">
				    <span class="green">(M0040)</span>
				    MI
				</label>
				<div class="fr"><%= Html.TextBox("MiddleInitial", (Model != null && Model.MiddleInitial.IsNotNullOrEmpty()) ? Model.MiddleInitial : string.Empty, new { @id = "NewPatient_MiddleInitial", @class = "shortest", @maxlength = "1" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_LastName" class="fl">
				    <span class="green">(M0040)</span>
				    Last Name
				</label>
				<div class="fr"><%= Html.TextBox("LastName", (Model != null && Model.LastName.IsNotNullOrEmpty()) ? Model.LastName : string.Empty, new { @id = "NewPatient_LastName", @class = "required", @maxlength = "50" }) %></div>
			</div>
			<div class="row">
				<label for="NewPatient_Gender" class="fl">
				    <span class="green">(M0069)</span>
				    Gender
				</label>
				<div class="fr"><%= Html.Gender("Gender", Model.Gender, new { @class = "required" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_DOB" class="fl">
				    <span class="green">(M0066)</span>
				    Date of Birth
				</label>
				<div class="fr"><input name="DOB" type="text" class="date-picker required" value="<%= Model != null && Model.DOB != DateTime.MinValue ? Model.DOBFormatted : string.Empty %>" id="NewPatient_DOB" /></div>
			</div>
			<div class="row">
				<label for="NewPatient_MaritalStatus" class="fl">Marital Status</label>
				<div class="fr"><%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status --", "0", new { @id = "NewPatient_MaritalStatus" }) %></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="NewPatient_PatientIdNumber" class="fl">
				    <span class="green">(M0020)</span>
				    Patient ID/MR Number
				</label>
				<div class="fr"><%= Html.TextBox("PatientIdNumber", "", new { @id = "NewPatient_PatientIdNumber", @class = "required", @maxlength = "30" })%></div>
				<div class="clr"></div>
				<div class="ac"><em>Last Patient MRN used: <%=Model.LastUsedPatientId %></em></div>
			</div>
			<div class="row">
				<label for="NewPatient_SSN" class="fl">
				    <span class="green">(M0064)</span>
				    SSN
				</label>
				<div class="fr"><%= Html.TextBox("SSN", (Model != null && Model.SSN.IsNotNullOrEmpty()) ? Model.SSN : string.Empty, new { @id = "NewPatient_SSN", @class = "ssn numeric", @maxlength = "9" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_IsDNR" class="fl">DNR</label>
				<div class="fr"><%= Html.DNR("IsDNR", Model.IsDNR.ToString(),false, new { @id = "NewPatient_IsDNR" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_Height" class="fl">Height</label>
				<div class="fr">
					<%= Html.TextBox("Height", Model.Height, new { @id = "NewPatient_Height", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.HeightMetric("HeightMetric", Model.HeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
			<div class="row">
				<label for="NewPatient_Weight" class="fl">Weight</label>
				<div class="fr">
					<%= Html.TextBox("Weight", Model.Weight, new { @id = "NewPatient_Weight", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.WeightMetric("WeightMetric", Model.WeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
    <%  if (!Model.IsOnlyOneService) { %>
			<div class="row">
				<label class="fl">Service Provided</label>
				<div class="fr">
					<ul class="checkgroup one-wide">
						<%= Html.CheckgroupOption("ServiceProvided", "HomeHealth", false, "Home Health Service") %>
						<%= Html.CheckgroupOption("ServiceProvided", "PrivateDuty", false, "Private Duty Service") %>
					</ul>
				</div>
			</div>
	<%  } else { %>
			<%= Html.Hidden("ServiceProvided", Model.CurrentServices.ToString()) %>
	<%  } %>
		</div>
	</fieldset>
	<%  if (Model.CurrentServices.Has(AgencyServices.HomeHealth)) { %>
	<div id="NewPatient_ServiceContainerHomeHealth"><% Html.RenderPartial("New/HomeHealth", Model); %></div>
	<%  } %>
	<%  if (Model.CurrentServices.Has(AgencyServices.PrivateDuty)) { %>
	<div id="NewPatient_ServiceContainerPrivateDuty"><% Html.RenderPartial("New/PrivateDuty", Model); %></div>
	<%  } %>
	<%= Html.Partial("Patient/Ethnicities", new ArrayIdViewData(null, "NewPatient")) %>
	<fieldset>
		<legend>Patient Address</legend>
		<div class="column">
			<div class="row">
				<label for="NewPatient_AddressLine1" class="fl">Address Line 1</label>
				<div class="fr"><%= Html.TextBox("AddressLine1", (Model != null && Model.AddressLine1.IsNotNullOrEmpty()) ? Model.AddressLine1.ToString() : "", new { @id = "NewPatient_AddressLine1", @class = "required", @maxlength = "50" }) %></div>
			</div>
			<div class="row">
				<label for="NewPatient_AddressLine2" class="fl">Address Line 2</label>
				<div class="fr"><%= Html.TextBox("AddressLine2", (Model != null && Model.AddressLine2.IsNotNullOrEmpty()) ? Model.AddressLine2.ToString() : "", new { @id = "NewPatient_AddressLine2", @maxlength = "50" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_AddressCity" class="fl">City</label>
				<div class="fr"><%= Html.TextBox("AddressCity", (Model != null && Model.AddressCity.IsNotNullOrEmpty()) ? Model.AddressCity.ToString() : "", new { @id = "NewPatient_AddressCity", @class = "address-city required", @maxlength = "50" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_AddressStateCode" class="fl">
				    <span class="green">(M0050)</span>
				    State,
				    <span class="green">(M0060)</span>
				    Zip
				</label>
				<div class="fr">
					<%= Html.States("AddressStateCode", (Model != null && Model.AddressStateCode.IsNotNullOrEmpty()) ? Model.AddressStateCode.ToString() : "", new { @id = "NewPatient_AddressStateCode", @class = "address-state required not-zero short" })%>
					<%= Html.TextBox("AddressZipCode", (Model != null && Model.AddressZipCode.IsNotNullOrEmpty()) ? Model.AddressZipCode.ToString() : "", new { @id = "NewPatient_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "9" })%>
				</div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="NewPatient_HomePhone1" class="fl">Home Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("PhoneHomeArray", Model != null ? Model.PhoneHome : null, "NewPatient_HomePhone", "required")%></div>
			</div>
			<div class="row">
				<label for="NewPatient_MobilePhone1" class="fl">Mobile Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("PhoneMobileArray", "", "NewPatient_MobilePhone", "")%></div>
			</div>
			<div class="row">
				<label for="NewPatient_Email" class="fl">Email</label>
				<div class="fr"><%= Html.TextBox("EmailAddress", (Model != null && Model.EmailAddress.IsNotNullOrEmpty()) ? Model.EmailAddress : string.Empty, new { @id = "NewPatient_Email", @maxlength = "50" })%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
	<%  var physicians = Model != null && Model.Physicians.IsNotNullOrEmpty() ? Model.Physicians.ToObject<List<Physician>>().OrderByDescending(p => p.IsPrimary).ToList() : new List<Physician>(); %>
		<legend>Physician Information</legend>
		<div class="column">
			<div class="row">
				<label for="NewPatient_PhysicianDropDown1" class="fl">Primary Physician</label>
				<div class="fr">
					<%= Html.TextBox("AgencyPhysicians", physicians.Count > 0 ? physicians[0].Id.ToString() : string.Empty, new { @id = "NewPatient_PhysicianDropDown1", @class = "physician-picker short" })%>
					<a class="img icon16 plus" onclick="Patient.Demographic.AddPhysRow(this)"></a>
				</div>
			</div>
			<div class="row <%= physicians.Count > 1 ? string.Empty : "hidden" %>">
				<label for="NewPatient_PhysicianDropDown2" class="fl">Additional Physician</label>
				<div class="fr">
					<%= Html.TextBox("AgencyPhysicians", physicians.Count > 1 ? physicians[1].Id.ToString() : string.Empty, new { @id = "NewPatient_PhysicianDropDown2", @class = "physician-picker short" })%>
					<a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"></a>
				</div>
			</div>
			<div class="row <%= physicians.Count > 2 ? string.Empty : "hidden" %>">
				<label for="NewPatient_PhysicianDropDown3" class="fl">Additional Physician</label>
				<div class="fr">
					<%= Html.TextBox("AgencyPhysicians", physicians.Count > 2 ? physicians[2].Id.ToString() : string.Empty, new { @id = "NewPatient_PhysicianDropDown3", @class = "physician-picker short" })%>
					<a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)" />
				</div>
			</div>
			<div class="row <%= physicians.Count > 3 ? string.Empty : "hidden" %>">
				<label for="NewPatient_PhysicianDropDown4" class="fl">Additional Physician</label>
				<div class="fr">
					<%= Html.TextBox("AgencyPhysicians", physicians.Count > 3 ? physicians[3].Id.ToString() : string.Empty, new { @id = "NewPatient_PhysicianDropDown4", @class = "physician-picker short" })%>
					<a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)" />
				</div>
			</div>
			<div class="row <%= physicians.Count > 4 ? string.Empty : "hidden" %>">
				<label for="NewPatient_PhysicianDropDown5" class="fl">Additional Physician</label>
				<div class="fr">
					<%= Html.TextBox("AgencyPhysicians", physicians.Count > 4 ? physicians[4].Id.ToString() : string.Empty, new { @id = "NewPatient_PhysicianDropDown5", @class = "physician-picker short" })%>
					<a class="img icon16 ecks" onclick="Patient.Demographic.RemPhysRow(this)"/>
				</div>
			</div>
		</div>
		<% if(Model.IsUserCanAddPhysicain) { %>
		<div class="column">
			<div class="row">
				<div class="fr">
					<div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
				</div>
			</div>
		</div>
	<%  } %>
	</fieldset>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Pharmacy Information</legend>
				<div class="column">
					<div class="row">
						<label for="NewPatient_PharmacyName" class="fl">Name</label>
						<div class="fr"><%= Html.TextBox("PharmacyName", "", new { @id = "NewPatient_PharmacyName", @maxlength = "100" }) %></div>
					</div>
					<div class="row">
						<label for="NewPatient_PharmacyPhone1" class="fl">Phone</label>
						<div class="fr"><%= Html.PhoneOrFax("PharmacyPhoneArray", "", "NewPatient_PharmacyPhone", "")%></div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Evacuation Zone</legend>
				<div class="column">
					<div class="row">
						<label for="NewPatient_EvacuationZone" class="fl">Evacuation Zone</label>
						<div class="fr"><%= Html.EvacuationZone("EvacuationZone", "", new { @id = "NewPatient_EvacuationZone" })%></div>
					</div>
				</div>
			</fieldset>
			<%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(Model != null ? Model.ServicesRequired : null, "NewPatient", string.Empty))%>
		</div>
		<div><%= Html.Partial("Patient/Triage", new ValueIdViewData<int>(0, "NewPatient")) %></div>
	</div>
	<%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(Model != null ? Model.DME : null, Model != null ? Model.OtherDME : string.Empty, "NewPatient"))%>
	<fieldset>
		<legend>Primary Emergency Contact</legend>
		<div class="column">
			<div class="row">
				<label for="NewPatient_EmergencyContactFirstName" class="fl">First Name</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.FirstName", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.FirstName : string.Empty, new { @id = "NewPatient_EmergencyContactFirstName", @maxlength = "100" }) %></div>
			</div>
			<div class="row">
				<label for="NewPatient_EmergencyContactLastName" class="fl">Last Name</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.LastName", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.LastName : string.Empty, new { @id = "NewPatient_EmergencyContactLastName", @maxlength = "100" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_EmergencyContactRelationship" class="fl">Relationship</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.Relationship", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.Relationship : string.Empty, new { @id = "NewPatient_EmergencyContactRelationship" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="NewPatient_EmergencyContactPhonePrimary1" class="fl">Primary Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("EmergencyContact.PhonePrimaryArray", Model.EmergencyContact != null ? Model.EmergencyContact.PrimaryPhone : string.Empty, "NewPatient_EmergencyContactPhonePrimary", "")%></div>
			</div>
			<div class="row">
				<label for="NewPatient_EmergencyContactPhoneAlternate1" class="fl">Alternate Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("EmergencyContact.PhoneAlternateArray", Model.EmergencyContact != null ? Model.EmergencyContact.AlternatePhone : string.Empty, "NewPatient_EmergencyContactPhoneAlternate", "")%></div>
			</div>
			<div class="row">
				<label for="NewPatient_EmergencyContactEmail" class="fl">Email</label>
				<div class="fr"><%= Html.TextBox("EmergencyContact.EmailAddress", Model != null && Model.EmergencyContact != null ? Model.EmergencyContact.EmailAddress : string.Empty, new { @id = "NewPatient_EmergencyContactEmail", @class = "email", @maxlength = "100" })%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Referral Source</legend>
		<div class="column">
			<div class="row">
				<label for="NewPatient_ReferrerPhysician" class="fl">Physician</label>
				<div class="fr">
					<%= Html.TextBox("ReferrerPhysician", (Model != null && !Model.ReferrerPhysician.IsEmpty()) ? Model.ReferrerPhysician.ToString() : "", new { @id = "NewPatient_ReferrerPhysician", @class = "physician-picker" })%>
					<% if(Model.IsUserCanAddPhysicain) { %>
					<br />
					<div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
	<%  } %>
				</div>
			</div>
			<div class="row">
				<label for="NewPatient_AdmissionSource" class="fl">Admission Source</label>
				<div class="fr"><%= Html.AdmissionSources("AdmissionSource", (Model != null && Model.AdmissionSource > 0) ? Model.AdmissionSource.ToString() : "", new { @id = "NewPatient_AdmissionSource", @class = "admission-source required not-zero" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="NewPatient_OtherReferralSource" class="fl">Other Referral Source</label>
				<div class="fr"><%= Html.TextBox("OtherReferralSource", (Model != null && Model.OtherReferralSource.IsNotNullOrEmpty()) ? Model.OtherReferralSource : "", new { @id = "NewPatient_OtherReferralSource", @maxlength = "30" })%></div>
			</div>
			<div class="row">
				<label for="NewPatient_PatientReferralDate" class="fl">
				    <span class="green">(M0104)</span>
				    Referral Date
				</label>
				<div class="fr"><input type="text" class="date-picker required" name="ReferralDate" id="NewPatient_PatientReferralDate" /></div>
			</div>
			<div class="row">
				<label for="NewPatient_InternalReferral" class="fl">Internal Referral Source</label>
				<div class="fr"><%= Html.Users("InternalReferral", (Model != null && !Model.InternalReferral.IsEmpty()) ? Model.InternalReferral.ToString() : "", "-- Select User --",Guid.Empty,(int)UserStatus.Active,0, new { @id = "NewPatient_InternalReferral", @class = "user-selector" })%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Comments</legend>
		<div class="wide-column">
			<div class="row">
			    <div class="template-text">
    				<%= Html.ToggleTemplates("NewPatient_CommentsTemplates")%>
	    			<%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : "", new { @id = "NewPatient_Comments", @maxcharacters = "500" })%>
	    		</div>
			</div>
		</div>
	</fieldset>
	<%= Html.Hidden("Status", 1, new { @id = "NewPatient_Status" })%>     
	<%= Html.Hidden("SaveStatus", 3, new { @disabled = "true" })%>
	<%= Html.Hidden("CompleteStatus", 1, new { @disabled = "true" })%>
	<%= Html.Hidden("ReferralId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : Guid.Empty.ToString(), new { @id = "NewPatient_ReferralId" })%>     
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="complete">Admit Patient</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
	<%  } %>
</div>