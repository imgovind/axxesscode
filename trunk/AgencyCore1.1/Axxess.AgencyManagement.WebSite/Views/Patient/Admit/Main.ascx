﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Admit | <%= Model.DisplayName %></span>
<div id="Admit_Patient_TabStrip" class="wrapper blue main horizontal-tabs maintab">
  <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
<%  var count = 1; %>
    <ul class="horizontal-tab-list">
        <li class="ac"><a name="Info" href="#Admit_Patient_Info" url="Patient/Pending">Step 1<br />Demographics</a></li>
<%  if (Model.Services.Has(AgencyServices.HomeHealth) && PatientStatusFactory.PossibleAdmit().Contains(Model.HomeHealthStatus)) { %>
    <%  count++; %>
        <li class="ac" ><a name="HomeHealth" href="PatientProfile/Pending">Step <%= count %><br />Home Health</a></li>
<%  } %>
<%  if (Model.Services.Has(AgencyServices.PrivateDuty) && PatientStatusFactory.PossibleAdmit().Contains(Model.PrivateDutyStatus)) { %>
    <%  count++; %>
        <li class="ac"><a name="PrivateDuty" href="PrivateDuty/PatientProfile/Pending">Step <%= count %><br />Private Duty</a></li>
<%  } %>
    </ul>
    <div id="Admit_Patient_Info" class="tab-content"><%  Html.RenderPartial("Admit/Info", Model); %></div>
</div>