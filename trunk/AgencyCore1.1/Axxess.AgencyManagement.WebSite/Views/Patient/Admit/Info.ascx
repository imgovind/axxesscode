﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("VerifyPending", "Patient", FormMethod.Post, new { @id = "newAdmitPatientForm", @class = "tabform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Patient_Id" })%>
    <%= Html.Hidden("IsAdmit", "true", new { @id = "Admit_Patient_IsAdmit" })%>
    <%= Html.Hidden("TotalServiceToAdmit", "true", new { @id = "Admit_Patient_TotalServiceToAdmit" })%>
    <fieldset>
        <legend>Admit Patient</legend>
        <div class="column">
            <div class="row">
                <label for="Admit_Patient_FirstName" class="fl"><span class="green">(M0040)</span> First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Admit_Patient_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_MiddleInitial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", (Model.MiddleInitial), new { @id = "Admit_Patient_MiddleInitial", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Admit_Patient_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label class="fl" for="Admit_Patient_Gender"><span class="green">(M0069)</span> Gender</label>
                <div class="fr"><%= Html.Gender("Gender", Model.Gender , new { @id = "Admit_Patient_Gender", @class = "required " })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Admit_Patient_PatientID" class="fl"><span class="green">(M0020)</span> MR Number</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Admit_Patient_PatientID", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_DOB" class="fl"><span class="green">(M0066)</span> Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Admit_Patient_DOB" /></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_MaritalStatus" class="fl">Marital Status</label>
                <div class="fr"><%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status --", "0", new { @id = "Admit_Patient_MaritalStatus" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_Physician" class="fl">Primary Physician</label>
                <div class="fr"><%= Html.TextBox("Physician.Id", Model.Physician != null ? Model.Physician.Id : Guid.Empty, new { @id = "Admit_Patient_PrimaryPhysician", @class = "physician-picker required" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save next">Verify &#38; Next</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>


