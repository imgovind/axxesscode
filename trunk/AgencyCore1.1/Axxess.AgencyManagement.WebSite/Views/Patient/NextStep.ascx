﻿<div class="wrapper main">
    <div>
        <h3>Patient saved successfully.</h3>
        What would you like to do next?
    </div>
    <ul id="NextStep_Options"></ul>
</div>
<script type="text/javascript">
    $("#window_ModalWindow").addClass("modal-gray");
    $("#NextStep_Options").append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon green-arrow" })
        ).append(
            $("<a/>").text("Add Another Patient").on("click", function() {
                Acore.CloseDialog();
                UserInterface.CloseWindow('NewPatient');
                Patient.Demographic.New();
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon green-arrow" })
        ).append(
            $("<a/>").text("Go to Patient Charts").on("click", function() {
                Acore.CloseDialog();
                UserInterface.CloseWindow('NewPatient');
                UserInterface.ShowPatientCharts()
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon green-arrow" })
        ).append(
            $("<a/>").text("Close Window").on("click", function() {
                Acore.CloseDialog();
                UserInterface.CloseWindow('NewPatient')
            })
        )
    );
</script>
