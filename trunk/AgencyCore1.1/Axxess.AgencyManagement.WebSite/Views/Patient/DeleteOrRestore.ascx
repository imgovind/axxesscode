﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%  if (Model.FormAction == FormAction.Delete || Model.FormAction == FormAction.Restore) { %>
<div class="wrapper main">
    <%  var action = Model.FormAction == FormAction.Delete ? FormAction.Delete.ToString() : FormAction.Restore.ToString(); %>
    <%  using (Html.BeginForm(action, "Patient", FormMethod.Post, new { @id = action.ToLowerCase() + "PatientForm", @class = "formsubmit" })) { %>
        <%= Html.Hidden("Id", Model.Id, new { @id = action + "_PatientId" })%>
        <%  if (Model.TotalServiceToProcess > 0) { %>
        <fieldset>
        <%  if (Model.TotalServiceToProcess == 1) { %>
			<%= Html.Hidden("ServiceProvided", Model.CurrentServices.ToString())%>
    
			<legend><%= Model.DisplayName %></legend>
			<div class="column">
				<div class="row">
					<label class="fl">Service</label>
					<div class="fr">
						<label><%= Model.CurrentServices == AgencyServices.HomeHealth ? AgencyServices.HomeHealth.GetDescription() : (Model.CurrentServices == AgencyServices.PrivateDuty ? AgencyServices.PrivateDuty.GetDescription() : string.Empty) %></label>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="row">
					<label class="fl">Current Status</label>
					<div class="fr">
						<label><%= Model.CurrentServices == AgencyServices.HomeHealth ? Model.HHStatusName : (Model.CurrentServices == AgencyServices.PrivateDuty ? Model.PDStatusName : string.Empty) %></label>
					</div>
				</div>
			</div>
				<%  } else { %>
			<legend><%= Model.DisplayName %></legend>
			<div class="wide-column">
				<div class="row">
					<label class="fl">Service to Delete</label>
					<div class="fr">
						<ul class="checkgroup one-wide">
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id ='{0}_Patient_ServiceProvided1' type='checkbox' value='HomeHealth' class='required' name='ServiceProvided' {1} />", action, Model.CurrentServices.Has(AgencyServices.HomeHealth) ? "checked='checked'" : "") %>
									<label for="<%=action %>_Patient_ServiceProvided1">
										Home Health Service
										<em>Current Status: <%= Model.HHStatusName %></em>    
									</label>
								</div>
							</li>
							<li class="option">
								<div class="wrapper">
									<%= string.Format("<input id ='{0}_Patient_ServiceProvided2' type='checkbox' value='PrivateDuty' class='required' name='ServiceProvided' {1} />", action, Model.CurrentServices.Has(AgencyServices.PrivateDuty) ? "checked='checked'" : "") %>
									<label for="<%=action %>_Patient_ServiceProvided1">
										Private Duty Service
										<em>Current Status: <%= Model.PDStatusName %></em>
									</label>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
            <%  } %>
			<div class="wide-column">
				<div class="row ac">
					<label>Are you sure you want to <%=action.ToLowerCase() %> this patient?</label> 
				</div>
			</div>
		</fieldset>
		<ul class="buttons ac">
			<li><a class="save close">Yes</a></li>
			<li><a class="close">No</a></li>
		</ul>
        <%  } else { %>
    <fieldset>
        <legend><%= Model.DisplayName %></legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">There is no patient service to <%=action.ToLowerCase() %></label>
            </div>
        </div>
            <%  if (Model.Services.Has(AgencyServices.HomeHealth)) { %>
        <div class="column">
            <div class="row">
                <label class="fl">Home Health</label>
                <div class="fr">
                    <label><%= Model.HHStatusName %></label>
                </div>
            </div>
        </div>
            <%  } %>
            <%  if (Model.Services.Has(AgencyServices.PrivateDuty)) { %>
        <div class="column">
            <div class="row">
                <label class="fl">Private Duty</label>
                <div class="fr">
                    <label><%= Model.PDStatusName %></label>
                </div>
            </div>
        </div>
            <%  } %>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
        <%  } %>
    <%  } %>
<%  } %>
</div>