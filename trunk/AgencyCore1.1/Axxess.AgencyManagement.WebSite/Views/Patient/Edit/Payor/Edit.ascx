﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PrivatePayor>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdatePayor", "PatientProfile", FormMethod.Post, new { area = AgencyServices.PrivateDuty.ToArea(), @id = "editPatientPayorForm" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_PatientPayor_PatientId" })%>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_PatientPayor_Id" })%>
    <fieldset>
        <legend>Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_PatientPayor_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_PatientPayor_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_PatientPayor_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
           <div class="row">
                <label for="Edit_PatientPayor_MiddleName" class="fl">Middle Initial</label>
                <div class="fr"><%= Html.TextBox("MI", Model.MI, new { @id = "Edit_PatientPayor_MiddleName", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="New_PatientPayor_Relationship" class="fl">Relation to Patient</label>
                <div class="fr">
                    <%= Html.Relationships("Relationship", Model.Relationship, true, "-- Select Relationship --", new { @id = "Edit_PatientPayor_Relationship", @class = "required not-zero relationship" })%><br />
                    <%= Html.TextBox("OtherRelationship", Model.OtherRelationship, new { @id = "New_PatientPayor_OtherRelationship", @maxlength = "75", @class = "otherrelationship " })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_PatientPayor_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_PatientPayor_AddressLine1", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_PatientPayor_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_PatientPayor_AddressCity", @maxlength = "75", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "Edit_PatientPayor_AddressStateCode", @class = "address-state nonzero short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "New_User_AddressZipCode", @class = "numeric zip required shorter", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_PatientPayor_HomePhoneArray1" class="fl">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("HomePhoneArray", Model.PhoneHome, "Edit_PatientPayor_HomePhoneArray", "")%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_MobilePhoneArray1" class="fl">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("MobilePhoneArray", Model.PhoneMobile, "Edit_PatientPayor_MobilePhoneArray", "")%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_FaxPhoneArray1" class="fl">Fax Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxPhoneArray", Model.PhoneMobile, "Edit_PatientPayor_FaxPhoneArray", "")%></div>
            </div>
            <div class="row">
                <label for="Edit_PatientPayor_EmailAddress" class="fl">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_PatientPayor_EmailAddress", @class = "email", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>