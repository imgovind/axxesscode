﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<div class="wrapper main">
<% var area = AgencyServices.PrivateDuty.ToArea();%>
<%  if (Model.Status != (int)PatientStatus.Deprecated) { %>
    <%  using (Html.BeginForm("Update", "PatientProfile", FormMethod.Post, new { @area = AgencyServices.PrivateDuty.ToString(), @id = "editPrivateDutyForm", @class = "tabform" })) { %>
    <fieldset>
        <legend>Private Duty</legend>
        <%  if (Model.IsUnloadButtonVisible) { %>
            <div class="button"><a onclick="Patient.Demographic.Edit.UnloadProfile('PrivateDuty');">Unload Private Duty Profile</a></div>
		<%  } %>
        <input type="hidden" name="profile.Index" value="2" />
        <input type="hidden" name="profile[2].ServiceType" value="2" />
        <%= Html.Hidden("profile[2].Id", Model.Id, new { @id = "EditPatient_PdProfileId" })%>
        <%= Html.Hidden("profile[2].AdmissionId", Model.AdmissionId, new { @id = "EditPatient_AdmissionId" })%> 
        <div class="column">
            <div class="row">
                <label for="EditPatient_PdStartOfCareDate" class="fl"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="profile[2].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="EditPatient_PdStartOfCareDate" /></div>
            </div>
        <%  if (Model.Status == (int)PatientStatus.Discharged) { %>
            <div class="row">
                <label for="EditPatient_PdDischargeDate" class="fl">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="profile[2].DischargeDate" value="<%= !Model.DischargeDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DischargeDate.ToString("MM/dd/yyyy") : string.Empty %>" id="EditPatient_PdDischargeDate" /></div>
            </div>
        <%  } %>
            <div class="row">
                <label for="EditPatient_PdCaseManager" class="fl">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("profile[2].CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "EditPatient_PdCaseManager", @class = "user-selector required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdAssign" class="fl">Assign to Clinician</label>
                <div class="fr"><%= Html.Clinicians("profile[2].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "EditPatient_PdAssign", @class = "required not-zero user-selector" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdLocationId" class="fl">Agency Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("profile[2].AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, (int)AgencyServices.PrivateDuty, new { @id = "EditPatient_PdLocationId", @class = "branch-location required not-zero", @service = (int)AgencyServices.PrivateDuty })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdPatientReferralDate" class="fl"><span class="green">(M0104)</span>Referral Date</label>
                <div class="fr"><input type="text" class="date-picker" name="profile[2].ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "" %>" id="EditPatient_PdPatientReferralDate" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PdAdmissionSource" class="fl">Admission Source</label>
                <div class="fr"><%= Html.AdmissionSources("profile[2].AdmissionSource", Model.AdmissionSource, new { @id = "EditPatient_PdAdmissionSource", @class = "admission-source required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdOtherReferralSource" class="fl">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("profile[2].OtherReferralSource", Model.OtherReferralSource, new { @id = "EditPatient_PdOtherReferralSource", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdInternalReferral" class="fl">Internal Referral Source</label>
                <div class="fr"><%= Html.Users("profile[2].InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "EditPatient_PdInternalReferral", @class = "user-selector" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PdAuditorId" class="fl">State Surveyor/Auditor</label>
                <div class="fr"><%= Html.Auditors("profile[2].AuditorId", Model.AuditorId.ToString(), new { @id = "EditPatient_PdAuditorId", @class = "user-selector" })%></div>
            </div>
            <div class="row">
                <p>Hint: <em>A user account must be created for the surveyor/auditor before you can grant them access to patient records.</em></p>
            </div>
        </div>
    </fieldset>
    <%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(Model.PaymentSource, Model.OtherPaymentSource, "EditPatient_", "profile[2].", true, "two-wide"))%>
    <%--<fieldset>
        <legend>Insurance/Payor</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PdPrimaryInsurance" class="fl">Primary</label>
                <div class="fr"><%= Html.InsurancesByBranch("profile[2].PrimaryInsurance", Model.PrimaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "EditPatient_PdPrimaryInsurance", @class = "insurance required not-zero" })%></div>
            </div> 
            <div id="EditPatient_PdPrimaryInsuranceContent" class="<%= Model.PrimaryInsurance >= 1000 ? "" : "hidden" %>">
                <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Primary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.PrimaryHealthPlanId, GroupName = Model.PrimaryGroupName, GroupId = Model.PrimaryGroupId, Relationship = Model.PrimaryRelationship }) %>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PdSecondaryInsurance" class="fl">Secondary</label>
                <div class="fr"><%= Html.InsurancesByBranch("profile[2].SecondaryInsurance", Model.SecondaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "EditPatient_PdSecondaryInsurance", @class = "insurance"})%></div>
            </div>
            <div id="EditPatient_PdSecondaryInsuranceContent" class="<%= Model.SecondaryInsurance >= 1000 ? "" : "hidden" %>">
                <%  Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Secondary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.SecondaryHealthPlanId, GroupName = Model.SecondaryGroupName, GroupId = Model.SecondaryGroupId, Relationship = Model.SecondaryRelationship }); %>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PdTertiaryInsurance" class="fl">Tertiary</label>
                <div class="fr"><%= Html.InsurancesByBranch("profile[2].TertiaryInsurance", Model.TertiaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "EditPatient_PdTertiaryInsurance", @class = "insurance" })%></div>
            </div>
            <div id="EditPatient_PdTertiaryInsuranceContent" class="<%= Model.TertiaryInsurance >= 1000 ? "" : "hidden" %>">
                <%  Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Tertiary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.TertiaryHealthPlanId, GroupName = Model.TertiaryGroupName, GroupId = Model.TertiaryGroupId, Relationship = Model.TertiaryRelationship }); %>
            </div>
        </div>
    </fieldset>--%>
     <fieldset>
        <legend>Insurances</legend>
        <div class="wide-column">
            <div class="row">
                <div class="fl">
                    <div >
                      <ul class="ac">
                          <li class="ancillary-button"><a>Add Payor(Patient Itself)</a></li>
                          <li class="ancillary-button"><a onclick="Patient.Insurance.New( { PatientId:'<%= Model.Id%>',AdmissionId:'<%= Model.AdmissionId%>',StartOfCareDate:'<%= Model.StartofCareDate%>',DischargeDate:'<%= Model.DischargeDate%>'},'PrivateDuty')">Add Other Payor(Insurance)</a></li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="wide-column">
           <div class="row">
              <%var input = string.Format("{{ PatientId:'{0}', AdmissionId:'{1}' , Id:'<#=Id#>'}}", Model.Id, Model.AdmissionId); %>
                <%= Html
                .Telerik()
                .Grid<PatientInsurance>()
                .Name("EditPatient_PatientInsuranceGrid")
                .HtmlAttributes(new { @class = "position-relative patientinsurances" })
                .Columns(columns =>
                                {
                                    columns.Bound(c => c.Name);
                                    columns.Bound(c => c.TypeName);
                                    columns.Bound(c => c.HealthPlanId);
                                    columns.Bound(c => c.GroupName);
                                    columns.Bound(c => c.GroupId);
                                    columns.Bound(c => c.ActualStartDate)
                                        .ClientTemplate("<#= IsOpenStartOfAdmission? 'Open' : U.FormatGridDate(ActualStartDate) #>")
                                        .Title("Start Date");
                                    columns.Bound(c => c.ActualEndDate)
                                        .ClientTemplate("<#= IsOpenEndOfAdmission? 'Open': U.FormatGridDate(ActualEndDate) #>")
                                        .Title("End Date");
                                    columns.Bound(c => c.Id)
                                        .ClientTemplate("<a class='link' onclick=\"Patient.Insurance.Edit(" + input + ",'" + area + "')\">Edit</a><a class='link' onclick=\"Patient.Insurance.Delete(" + input + ",'" + area + "')\">Delete</a>")
                                        .Title("Action")
                                        .Width(105)
                                        .Sortable(false);
                                    columns.Bound(c => c.Id)
                                        .ClientTemplate("<a class='link' onclick=\"Patient.Authorization.LoadList('" + Model.Id + "','" + area + "')\">View</a><a class='link' onclick=\"Patient.Insurance.Delete(" + input + ",'" + area + "')\">Add</a>")
                                        .Title("Authorizaton")
                                        .Width(105)
                                        .Sortable(false);
                                }).DataBinding(dataBinding => dataBinding
                                    .Ajax()
                                    .Select("Insurances", "PatientProfile", new { area = area, PatientId = Model.Id, AdmissionId = Model.AdmissionId })
                                    .OperationMode(GridOperationMode.Client))
                                .NoRecordsTemplate("No Insurances found.")
                    .ClientEvents(events => events
                        .OnDataBinding("U.OnTGridDataBinding")
                        .OnDataBound("U.OnTGridDataBound")
                        .OnError("U.OnTGridError"))
                //.Groupable(grouping => grouping.Groups(groups =>
                //{
                //    groups.Add(g => g.InsuranceId);
                //}))
                    .Sortable()
                    .Footer(false)%>
            </div>
            <%--<div class="row">
           <%= Html
            .Telerik()
            .Grid<PatientInsuranceViewData>()
            .Name("EditPatient_PatientInsuranceGrid")
            .HtmlAttributes(new { @class = "position-relative patientinsurances" })
            .Columns(columns =>
            {
                columns.Bound(e => e.Name).Width(140);
                //columns.Bound(e => e.LastName).Width(140);
                //columns.Bound(e => e.Title).Width(200);
                //columns.Bound(e => e.Country).Width(200);
                //columns.Bound(e => e.City);
            })
            .DataBinding(dataBinding => dataBinding
                .Ajax()
                .Select("Insurances", "PatientProfile", new { area = AgencyServices.PrivateDuty.ToArea(), PatientId = Model.Id })
                .OperationMode(GridOperationMode.Client))
            .DetailView(detailView => detailView.ClientTemplate(
                
            {
                %>
                    <% Html.Telerik().Grid(e.Insurances)
                           .Name("Orders_" + e.InsuranceId)
                           .Columns(columns =>
                            {
                                columns.Bound(o => o.TypeName).Width(101);
                                columns.Bound(o => o.IsCurrentlyActive).Width(140);
                                columns.Bound(o => o.StartDate).Width(200);
                                columns.Bound(o => o.EndDate).Width(200);
                            })%>
                        <%   }))%> 
            </div>--%>
        </div>
    </fieldset>
        <%  if (Model.Status == (int)PatientStatus.Discharged) { %> 
    <fieldset>
        <legend>Discharged Reason</legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="EditPatient_PdDischargeReason" name="profile[2].DischargeReason"><%= Model.DischargeReason %></textarea>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <fieldset>
        <legend>Private Duty</legend>
        <div class="wide-column">
            <div class="row">
                <label>The patient could not be admittied for this service, the patient is <%= string.Format("{0}.",Model.IsDeprecated ? "deleted" : ((PatientStatus)Model.Status).ToString()) %></label>
            </div>
        </div>
    </fieldset>
<%  } %>
</div>