﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPatient_Id" })%>  
   <fieldset>
        <legend>Physicians</legend>
        <div class="column two-thirds">
            <div class="row">
                <label for="EditPatient_PhysicianSelector" class="fl">Physician</label>
                <div class="fl"><%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "EditPatient_PhysicianSelector", @class = "physician-picker" })%></div>
                <div class="ac fl">
                    <div class="button"><a id="EditPatient_AddPhysician" class="addphysician">Add Physician</a></div>
                </div>
            </div>
        </div>
		<% if(Model.IsUserCanAdd) { %>
        <div class="column one-third">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                </div>
            </div>
        </div>
        <% } %>
        <div class="wide-column">
            <div class="row">
                <%= Html.Telerik().Grid<AgencyPhysicianLeanGridRow>()
                    .Name("EditPatient_PhysicianGrid").HtmlAttributes(new { @class = "position-relative patientphysicians" }).Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.WorkPhone);
                        columns.Bound(c => c.FaxNumber);
                        columns.Bound(c => c.Email).ClientTemplate("<a href='mailto:<#=Email#>'><#=Email#></a>");
						columns.Bound(c => c.Id).ClientTemplate("<a class='link' onclick=\"Patient.Demographic.DeletePhysician('<#=Id#>','" + Model.Id + "')\">Delete</a><# if(!IsPrimary) { #><a onclick=\"Patient.Demographic.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "')\" class='link'>Make Primary</a><# } #>").Title("Action").Width(145).Sortable(false);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Patient", new {area ="", PatientId = Model.Id }).OperationMode(GridOperationMode.Client))
                    .NoRecordsTemplate("No Physicians found.")
					.ClientEvents(events => events.OnDataBinding("U.OnTGridDataBinding").OnDataBound("U.OnTGridDataBound").OnError("U.OnTGridError"))
                	.Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
</div>