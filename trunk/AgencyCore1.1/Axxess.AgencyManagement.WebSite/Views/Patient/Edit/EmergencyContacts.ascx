﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model, new { @id = "EditPatient_Id" })%>  
    <fieldset>
        <legend>Emergency Contact</legend>
        <div class="wide-column">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Patient.Demographic.LoadNewEmergencyContact('<%=Model %>')">New Emergency Contact</a></div>
                </div>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<PatientEmergencyContactGridRow>().Name("EditPatient_EmergencyContact_Grid").HtmlAttributes(new { @class = "position-relative  emergencycontacts" }).Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PrimaryPhone);
                        columns.Bound(c => c.Relationship);
						columns.Bound(c => c.Email).ClientTemplate("<a href='mailto:<#=Email#>'><#=Email#></a>");
                        columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Patient.Demographic.LoadEditEmergencyContact('" + Model + "','<#=Id#>');\">Edit</a><a class=\"link\" onclick=\"Patient.Demographic.DeleteEmergencyContact('<#=Id#>','" + Model + "')\">Delete</a>").Title("Action").Width(100);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetEmergencyContacts", "Patient", new { area = "", PatientId = Model }).OperationMode(GridOperationMode.Client))
                    .NoRecordsTemplate("No Emergency Contacts found.")
                	.ClientEvents(events => events.OnDataBinding("U.OnTGridDataBinding").OnDataBound("U.OnTGridDataBound").OnError("U.OnTGridError"))
                	.Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
 </div>