﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%@ Import Namespace="NPOI.SS.Formula.Functions" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Patient", FormMethod.Post, new { @action = "Patient/Update", @id = "editPatientForm", @class = "tabform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPatient_Id" })%>  
     <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_FirstName" class="fl"><span class="green">(M0040)</span> First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditPatient_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_MiddleInitial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "EditPatient_MiddleInitial", @class = "shortest", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditPatient_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_Gender" class="fl"><span class="green">(M0069)</span> Gender</label>
                <div class="fr"><%= Html.Gender("Gender",Model.Gender, new { @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_DOB" class="fl"><span class="green">(M0066)</span> Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="EditPatient_DOB" /></div>
            </div>
            <div class="row">
                <label for="EditPatient_MaritalStatus" class="fl">Marital Status</label>
                <div class="fr"><%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status --", "0", new { @id = "EditPatient_MaritalStatus" }) %></div>
            </div>
            <div class="row">
                <label for="EditPatient_PatientID" class="fl"><span class="green">(M0020)</span> MRN</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "EditPatient_PatientID", @class = "required", @maxlength = "30" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_Height" class="fl">Height</label>
                <div class="fr">
                    <%= Html.TextBox("Height", Model.Height, new { @id = "EditPatient_Height", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.HeightMetric("HeightMetric",Model.HeightMetric, new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_Weight" class="fl">Weight</label>
                <div class="fr">
                    <%= Html.TextBox("Weight", Model.Weight, new { @id = "EditPatient_Weight", @class = "numeric short", @maxlength = "3" })%>
                    <%= Html.WeightMetric("EditPatient_WeightMetric",Model.WeightMetric, new { @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_MedicareNumber" class="fl"><span class="green">(M0063)</span> Medicare Number</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "EditPatient_MedicareNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_MedicaidNumber" class="fl"><span class="green">(M0065)</span> Medicaid Number</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "EditPatient_MedicaidNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_SSN" class="fl"><span class="green">(M0064)</span> SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "EditPatient_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_IsDNR" class="fl">DNR</label>
                <div class="fr">
                    <%= Html.DNR("IsDNR", Model.IsDNR.ToString(),false, new { @id = "EditPatient_IsDNR" })%>
                </div>
            </div>
        </div>
    </fieldset>
	<%= Html.Partial("Patient/Ethnicities", new ArrayIdViewData(Model.Ethnicities, "EditPatient")) %>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditPatient_AddressLine1", @class = "required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditPatient_AddressLine2", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditPatient_AddressCity", @class = "address-city required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressStateCode" class="fl">
                    <span class="green">(M0050)</span>
                    State,
                    <span class="green">(M0060)</span>
                    Zip
                </label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditPatient_AddressStateCode", @class = "address-state required short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditPatient_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "9" }) %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneHomeArray", Model.PhoneHome, "EditPatient_HomePhone", "required")%></div>
            </div>
            <div class="row">
                <label class="fl">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneMobileArray", Model.PhoneMobile, "EditPatient_MobilePhone", "" )%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_Email" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = "EditPatient_Email", @class = "email", @maxlength = "50" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Pharmacy</legend>
                <div class="column">
                    <div class="row">
                        <label for="EditPatient_PharmacyName" class="fl">Name</label>
                        <div class="fr"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "EditPatient_PharmacyName", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="EditPatient_PharmacyPhone1" class="fl">Phone</label>
                        <div class="fr">
                         <%= Html.PhoneOrFax("PharmacyPhoneArray", Model.PharmacyPhone, "EditPatient_PharmacyPhone", "")%>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Evacuation Zone</legend>
                <div class="column">
                    <div class="row">
                        <label for="EditPatient_EvacuationZone" class="fl">Evacuation Zone</label>
                        <div class="fr"><%= Html.EvacuationZone("EvacuationZone", Model.EvacuationZone, new { @id = "EditPatient_EvacuationZone" })%></div>
                    </div>
                </div>
            </fieldset>
            <%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(Model.ServicesRequired, "EditPatient", string.Empty))%>
        </div>
        <div>
           <%= Html.Partial("Patient/Triage", new ValueIdViewData<int>(Model.Triage, "EditPatient")) %>
        </div>
    </div>
    <%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(Model.DME, Model.OtherDME, "EditPatient")) %>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_ReferralPhysician" class="fl">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician", Model.ReferrerPhysician.ToString(), new { @id = "EditPatient_ReferrerPhysician", @class = "physician-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
	                <%= Html.ToggleTemplates("EditPatient_CommentsTemplates")%>
                    <textarea id="EditPatient_Comments" name="Comments" class="taller" maxcharacters="500"><%= Model.Comments %></textarea>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>