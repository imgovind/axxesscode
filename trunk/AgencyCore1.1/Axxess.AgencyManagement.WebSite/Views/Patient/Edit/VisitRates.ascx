﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model, new { @id = "EditPatient_Id" })%>  
    <fieldset>
        <legend>Visit Rates</legend>
        <div class="wide-column">
            <div class="row narrow">
                <p>To apply the visit information from the selected insurance, click on the &#8220;Apply&#8221; button on the left.</p>
                <label for="EditPatient_OldInsuranceId" class="fl">Choose Existing Insurance</label>
                <div class="fr">
	                <%= Html.InsurancesNoneMedicareTraditional("OldInsuranceId", "0", (int)AgencyServices.PrivateDuty, false, true, "-- Select Insurance --", new { @id = "EditPatient_OldInsuranceId" })%>
                    <div class="button"><a onclick="Patient.VisitRate.ReplaceAll('<%= Model %>',$('#EditPatient_OldInsuranceId').val())">Apply</a></div>
                </div>
            </div>
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Patient.VisitRate.New('<%= Model %>')">Add Visit Information</a></div>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <%= Html.Telerik().Grid<ChargeRate>().HtmlAttributes(new { @class = "position-relative" }).Name("EditPatient_VisitRatesGrid").DataKeys(keys => keys.Add(r => r.Id).RouteKey("Id")).Columns(columns => {
                        columns.Bound(e => e.DisciplineTaskName).Title("Task");
                        columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                        columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(90);
                        columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                        columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(50).HeaderHtmlAttributes(new { @class = "rate-header" });
                        columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
                        columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(70);
				        columns.Bound(e => e.TimeLimit).ClientTemplate("<#= IsTimeLimit && TimeLimitHour != 0 && TimeLimitMin != 0 && ChargeType != \"1\" ? $.telerik.formatString('{0:H:mm}', TimeLimit) : '' #>").Title("Time Limit").Width(75);
                        columns.Bound(e => e.Id).ClientTemplate("<a onclick=\"Patient.VisitRate.Edit('" + Model + "','<#= Id #>')\" class=\"img icon16 edit\"></a> | <a onclick=\"Patient.VisitRate.Delete('" + Model + "','<#= Id #>')\" class=\"img icon16 delete\"></a> ").Title("Action").Width(60);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("VisitRatesData", "PatientProfile", new { area = AgencyServices.PrivateDuty.ToArea(), PatientId = Model })).ClientEvents(events => events
                        .OnDataBinding("U.OnTGridDataBinding")
                        .OnDataBound("U.OnTGridDataBound")
                        .OnError("U.OnTGridError")
                    ).Sortable().Scrollable().NoRecordsTemplate("No rates to display.").Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Billing Information (Payors)</legend>
        <div class="wide-column">
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Patient.Insurance.Payor.New('<%= Model %>')">New Payor</a></div>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <%= Html.Telerik().Grid<PrivatePayor>().Name("EditPatient_BillingInformationGrid").HtmlAttributes(new { @class = "position-relative" }).Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PhoneHomeFormatted).Title("Home Phone").Width(105); 
                        columns.Bound(c => c.PhoneMobileFormatted).Title("Mobile Phone").Width(105);
                        columns.Bound(c => c.FaxNumberFormatted).Title("Fax Number").Width(105);
                        columns.Bound(c => c.Id).ClientTemplate("<a onclick=\"Patient.Insurance.Payor.Edit('" + Model + "','<#= Id #>')\" class=\"img icon16 edit\"></a> | <a onclick=\"Patient.Payor.Delete('" + Model + "','<#= Id #>')\" class=\"img icon16 delete\"></a>").Title("Action").Width(65);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Payors", "PatientProfile", new { area = AgencyServices.PrivateDuty.ToArea(), PatientId = Model })).ClientEvents(events => events
                        .OnDataBinding("U.OnTGridDataBinding")
                        .OnDataBound("U.OnTGridDataBound")
                        .OnError("U.OnTGridError")
                    ).Sortable().Scrollable().NoRecordsTemplate("No payors to display.").Footer(false) %>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
</div>