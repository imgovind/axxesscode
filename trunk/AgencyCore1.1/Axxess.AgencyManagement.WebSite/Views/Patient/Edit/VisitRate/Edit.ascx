﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateVisitRate", "PatientProfile", FormMethod.Post, new { @id = "editPatientVisitRate", @class = "mainform" })) { %>
    <%= Html.Hidden("PatientId", ViewData["PatientId"].ToSafeString(), new { @id = "Edit_VisitRate_PatientId" })%>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_VisitRate_Id" })%>
    <%  Html.RenderPartial("~/Views/Rate/EditContent.ascx", Model, new ViewDataDictionary { { "IdPrefix", "Edit_VisitRate" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
