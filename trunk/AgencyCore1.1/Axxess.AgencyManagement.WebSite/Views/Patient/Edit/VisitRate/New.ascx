﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewBillDataViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddVisitRate", "PatientProfile", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newPatientVisitRate", @class = "mainform" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "New_VisitRate_PatientId" })%>
    <%  Html.RenderPartial("~/Views/Rate/NewContent.ascx", new ChargeRate { InsuranceId = Model.InsuranceId, IsMedicareHMO = Model.IsMedicareHMO, IsTraditionalMedicare = Model.IsTraditionalMedicare, ExistingNotes = Model.ExistingNotes }, new ViewDataDictionary { { "IdPrefix", "New_VisitRate" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>

