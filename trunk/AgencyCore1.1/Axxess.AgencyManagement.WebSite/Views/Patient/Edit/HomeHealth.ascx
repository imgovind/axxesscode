﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<div class="wrapper main">
<%  if (Model.Status != (int)PatientStatus.Deprecated) { %>
    <%  using (Html.BeginForm("Update", "PatientProfile", FormMethod.Post, new { @area = "", @id = "editHomeHealthForm", @class = "tabform" })) { %>
    <fieldset>
        <legend>Home Health</legend>
        <input type="hidden" name="profile.Index" value="1" />
        <input type="hidden" name="profile[1].ServiceType" value="1" />
        <%= Html.Hidden("profile[1].Id", Model.Id, new { @id = "EditPatient_HHProfileId" })%>
        <div class="column">
            <div class="row">
                <label for="EditPatient_StartOfCareDate" class="fl"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="profile[1].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="EditPatient_StartOfCareDate" /></div>
            </div>
        <%  if (Model.Status == (int)PatientStatus.Discharged) { %>
            <div class="row">
                <label for="EditPatient_DischargeDate" class="fl">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="profile[1].DischargeDate" value="<%= !Model.DischargeDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DischargeDate.ToString("MM/dd/yyyy") : string.Empty %>" id="EditPatient_DischargeDate" /></div>
            </div>
        <%  } %>
            <div class="row">
                <label for="EditPatient_CaseManager" class="fl">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("profile[1].CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "EditPatient_CaseManager", @class = "user-selector required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_Assign" class="fl">Assign to Clinician</label>
                <div class="fr"><%= Html.Clinicians("profile[1].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "EditPatient_Assign", @class = "user-selector required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_LocationId" class="fl">Agency Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("profile[1].AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, (int)AgencyServices.HomeHealth, new { @id = "EditPatient_LocationId", @class = "branch-location required not-zero", @service = (int)AgencyServices.HomeHealth })%></div>
            </div>
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="UserInterface.ShowPatientEligibility($('#EditPatient_MedicareNumber').val(),$('#EditPatient_LastName').val(),$('#EditPatient_FirstName').val(),$('#EditPatient_DOB').val(),$('input[name=Gender]:checked').val())">Verify Medicare Eligibility</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PatientReferralDate" class="fl"><span class="green">(M0104)</span> Referral Date</label>
                <div class="fr"><input type="text" class="date-picker" name="profile[1].ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : string.Empty %>" id="EditPatient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="EditPatient_AdmissionSource" class="fl">Admission Source</label>
                <div class="fr"><%= Html.AdmissionSources("profile[1].AdmissionSource", Model.AdmissionSource, new { @id = "EditPatient_AdmissionSource", @class = "admission-source required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_OtherReferralSource" class="fl">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("profile[1].OtherReferralSource", Model.OtherReferralSource, new { @id = "EditPatient_OtherReferralSource", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_InternalReferral" class="fl">Internal Referral Source</label>
                <div class="fr"><%= Html.Users("profile[1].InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "EditPatient_InternalReferral", @class = "user-selector" })%></div>
            </div>
            <div class="row">
                <label class="fl">State Surveyor/Auditor</label>
                <div class="fr"><%= Html.Auditors("profile[1].AuditorId", Model.AuditorId.ToString(), new { @id = "EditPatient_AuditorId", @class = "user-selector" })%></div>
            </div>
            <div class="row">
                <p>Hint: <em>A user account must be created for the surveyor/auditor before you can grant them access to patient records.</em></p>
            </div>
        </div>
    </fieldset>
    <%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(Model.PaymentSource, Model.OtherPaymentSource, "EditPatient_", "profile[1].", true, "two-wide"))%>
    <fieldset>
        <legend>Insurance/Payor</legend>
	    <div class="column">
		    <div class="row">
			    <label for="EditPatient_PrimaryInsurance" class="fl">Primary</label>
			    <div class="fr"><%= Html.InsurancesByBranch("profile[1].PrimaryInsurance", Model.PrimaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "EditPatient_PrimaryInsurance", @class = "insurance required not-zero"})%></div>
            </div> 
            <div id="EditPatient_PrimaryInsuranceContent" class="<%= Model.PrimaryInsurance >= 1000 ? "" : "hidden" %>">
                <%= Html.Partial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Primary", NamePrefix = "profile[1].", HealthPlanId = Model.PrimaryHealthPlanId, GroupName = Model.PrimaryGroupName, GroupId = Model.PrimaryGroupId, Relationship = Model.PrimaryRelationship }) %>
            </div> 
	    </div>
	    <div class="column">
		    <div class="row">
			    <label for="EditPatient_SecondaryInsurance" class="fl">Secondary</label>
			    <div class="fr"><%= Html.InsurancesByBranch("profile[1].SecondaryInsurance", Model.SecondaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "EditPatient_SecondaryInsurance", @class = "insurance" })%></div>
            </div>
            <div id="EditPatient_SecondaryInsuranceContent" class="<%= Model.SecondaryInsurance >= 1000 ? "" : "hidden" %>">
				<%  Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Secondary", NamePrefix = "profile[1].", HealthPlanId = Model.SecondaryHealthPlanId, GroupName = Model.SecondaryGroupName, GroupId = Model.SecondaryGroupId, Relationship = Model.SecondaryRelationship }); %>
            </div>
	    </div>
	    <div class="column">
		    <div class="row">
		        <label for="EditPatient_TertiaryInsurance" class="fl">Tertiary</label>
			    <div class="fr"><%= Html.InsurancesByBranch("profile[1].TertiaryInsurance", Model.TertiaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "EditPatient_TertiaryInsurance", @class = "insurance" })%></div>
            </div>
            <div id="EditPatient_TertiaryInsuranceContent" class="<%= Model.TertiaryInsurance >= 1000 ? "" : "hidden" %>">
		        <%  Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "EditPatient", InsuranceType = "Tertiary", NamePrefix = "profile[1].", HealthPlanId = Model.TertiaryHealthPlanId, GroupName = Model.TertiaryGroupName, GroupId = Model.TertiaryGroupId, Relationship = Model.TertiaryRelationship }); %>
		    </div>
		</div>
	</fieldset>
        <%  if (Model.Status == (int)PatientStatus.Discharged) { %>
    <fieldset>
        <legend>Discharged Reason</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="EditPatient_DischargeReason" name="profile[1].DischargeReason" cols="5" rows="6"><%= Model.DischargeReason%></textarea>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <fieldset>
        <legend>Home Health</legend>
        <div class="wide-column">
            <div class="row">
                <label>The patient could not be admittied for this service, the patient is <%= string.Format("{0}.", Model.IsDeprecated ? "deleted" : ((PatientStatus)Model.Status).ToString()) %></label>
            </div>
        </div>
    </fieldset>
<%  } %>
</div>