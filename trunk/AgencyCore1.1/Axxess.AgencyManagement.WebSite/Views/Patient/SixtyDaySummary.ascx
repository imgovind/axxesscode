﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">Patient 60 Day Summary | <%= Model.DisplayName %></span>
<div class="wrapper main blue">
	<fieldset class="grid-controls ac">
		<div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search no-left-margin rel"></div>
    </fieldset>
	<%= Html.Telerik().Grid<VisitNoteViewData>().Name("List_PatientSixtyDaySummary").DataKeys(keys => keys.Add(o => o.EventId).RouteKey("id")).Columns(columns => {
            columns.Bound(v => v.UserDisplayName).Title("Employee Name").Sortable(false).ReadOnly();
            columns.Bound(v => v.SortableVisitDate).Title("Visit Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.SignatureDate).Title("Signature Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name").Sortable(false).ReadOnly();
            columns.Bound(v => v.EventId).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Title(" ").ClientTemplate("<a class='img icon16 print notes'></a>").Width(30).Visible(Model.IsUserCanPrint);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("SixtyDaySummaryList", "Note", new { patientId = Model.Id }).OperationMode(GridOperationMode.Client)).ClientEvents(evnts => evnts
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnRowDataBound("Visit.SixtyDaySummary.OnRowDataBound")
            .OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Sixty Day Summaries found.").Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>