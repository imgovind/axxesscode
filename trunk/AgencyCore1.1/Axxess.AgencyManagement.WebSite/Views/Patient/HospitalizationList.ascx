﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">Hospitalized Patients | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None) { %>
<div class="wrapper main blue">
	<ul class="fr buttons ac">
    <%  if (Model.IsUserCanAdd) { %>
	    <li><a onclick="HospitalizationLog.Add('<%= Guid.Empty %>')">New Hospitalization</a></li>
	    <div class="clr"></div>
	<%  } %>
	<%  if (Model.IsUserCanExport) { %>
		<li><a url="Export/HospitalizationLog" class="export">Excel Export</a></li>
		<div class="clr"></div>
	<%  } %>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
    </fieldset>
    <div class="clr"></div>
    <%= Html.Telerik().Grid<PatientHospitalizationData>().Name("PatientHospitalizedList_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
            columns.Bound(p => p.DisplayName).Title("Patient");
            columns.Bound(p => p.Source).Title("Source");
            columns.Bound(p => p.HospitalizationDate).Title("Hospitalization Date").ClientTemplate("<#= U.FormatGridDate(HospitalizationDate) #>").Width(150);
            columns.Bound(p => p.LastHomeVisitDate).Title("Last Home Visit Date").ClientTemplate("<#= U.FormatGridDate(LastHomeVisitDate) #>").Width(150);
            columns.Bound(p => p.User).Title("User");
            columns.Bound(p => p.Id).Width(100).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowPatientHospitalizationLogs('<#=PatientId#>');\" class=\"link\">Show Logs</a>").Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("HospitalizationGrid", "Patient")).NoRecordsTemplate("No Hospitalization Logs found.").Sortable().ClientEvents(events => events
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } %>