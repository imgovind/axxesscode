﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<HospitalizationLog>>" %>
<ul>
    <li>
        <span class="grid-third">Source</span>
        <span class="grid-quarter">User</span>
        <span class="grid-sixth">Date</span>
        <span class="grid-check"></span>
<%  if (!Current.IsAgencyFrozen) { %>
        <span>Action</span>
<%  } %>
    </li>
</ul>
<%  if (Model != null) { %>
    <%  int i = 1; %>
    <%  var logs = Model.OrderBy(l => l.Created).ToList(); %>
    <%  if (logs.IsNotNullOrEmpty()) { %>
<ol>
        <%  foreach (var log in logs) { %>
    <li>
        <span class="grid-third"><%= log.Source%></span>
        <span class="grid-quarter"><%= log.UserName%></span>
        <span class="grid-sixth"><%= log.HospitalizationDate.ToZeroFilled()%></span>
        <%if(log.IsUserCanPrint){ %>
        <span class="grid-check"><%= "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/HospitalizationLogPdf', { 'patientId': '" + log.PatientId + "', 'hospitalizationLogId': '" + log.Id + "' });\"><span class=\"img icon print\"></span></a>" %></span>
        <%} %>
        <%if (log.IsUserCanEdit || log.IsUserCanDelete) {%>
        <span>
            <%  if (log.IsUserCanEdit) { %>
                <a class="link" onclick="HospitalizationLog.Edit('<%=log.PatientId %>','<%= log.Id %>')">Edit</a> |
            <%  } %>
            <%  if (log.IsUserCanDelete)
                { %>
                <a class="link" onclick="HospitalizationLog.Delete('<%=log.PatientId %>','<%= log.Id %>')">Delete</a>
            <%  }%>
        </span>
        <%} %>
    </li>
            <%  i++; %>
        <%  } %>
</ol>
    <%  } %>
<%  } else { %>
<h4>No Hospitalization Logs Found</h4>
<%  } %>