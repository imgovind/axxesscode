﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationViewData>" %>
<span class="wintitle">Hospitalization Logs | <%= Current.AgencyName %></span>
<div class="wrapper main note">
    <fieldset>
        <legend>Hospitalization Logs</legend>
        <div class="wide-column">
            <div class="row narrow no-input">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.IsUserCanAdd) { %>
    <div class="button fl"><a onclick="HospitalizationLog.Add('<%= Model.PatientId %>')">Add Hospitalization Log</a></div>
    <%  } %>
    <%  if (Model.IsUserCanExport) { %>
    <div class="button fr"><a onclick="U.GetAttachment('Export/PatientHospitalizationLog', { patientId: '<%= Model.PatientId %>' })">Excel Export</a></div>
    <%  } %>
    <div class="clr"></div>
    <div id="Hospitalization_list" class="acore-grid"><% Html.RenderPartial("~/Views/Patient/Hospitalization/Logs.ascx", Model.Logs); %></div>

</div>