﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationLog>" %>
<span class="wintitle">Edit Hospitalization Log | <%= Current.AgencyName %></span>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main note">
    <%  using (Html.BeginForm("UpdateHospitalizationLog", "Patient", FormMethod.Post, new { @id = "editHospitalizationLogForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditHospitalizationLog_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditHospitalizationLog_PatientId" })%>
    <fieldset>
        <legend>New Hospitalization Log</legend>
        <div class="column">
            <div class="row">
				<label class="fl">Patient</label>
				<div class="fr"><%= Model.PatientName %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
				<label for="NewHospitalizationLog_User" class="fl">User</label>
				<div class="fr"><%= Html.Users("UserId", string.Empty, "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = "NewHospitalizationLog_User", @class = "required not-zero" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>OASIS M0903</legend>
                <div class="wide-column">
                    <div class="row narrower">
					    <label for="EditHospitalizationLog_M0903LastHomeVisitDate" class="fl"><span class="green">(M0903)</span> Date of Last (Most Recent) Home Visit</label>
						<div class="fr"><%= Html.TextBox("M0903LastHomeVisitDate", string.Empty, new { @id = "EditHospitalizationLog_M0903LastHomeVisitDate", @class = "date-picker" })%></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M0906</legend>
                <div class="wide-column">
                    <div class="row narrower">
					    <label for="EditHospitalizationLog_M0906DischargeDate" class="fl"><span class="green">(M0906)</span> Transfer Date: Enter the date of the transfer of the patient</label>
						<div class="fr"><%= Html.TextBox("M0906DischargeDate", string.Empty, new { @id = "EditHospitalizationLog_M0906DischargeDate", @class = "date-picker" })%></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M2410</legend>
                <div class="wide-column">
                    <div class="row narrower">
				        <div class="fl"><span class="green">(M2410)</span> To which Inpatient Facility has the patient been admitted?</div>
				        <%= Html.OasisCheckgroupRadioOptions("M2410TypeOfInpatientFacility", "EditHospitalizationLog", data, new[] { "01", "02", "03", "04" }, new [] { "Hospital", "Rehabilitation facility", "Nursing home", "Hospice" }, "two-wide")%>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M2440</legend>
                <div class="wide-column">
                    <div class="row narrower">
                        <div><span class="green">(M2440)</span> For what Reason(s) was the patient Admitted to a Nursing Home?</div>
                        <ul class="checkgroup two-wide">
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedTherapy", "EditHospitalizationLog", data, "1", "Therapy services", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedRespite", "EditHospitalizationLog", data, "2", "Respite care", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedHospice", "EditHospitalizationLog", data, "3", "Hospice care", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedPermanent", "EditHospitalizationLog", data, "4", "Permanent placement", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedUnsafe", "EditHospitalizationLog", data, "5", "Unsafe for care at home", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedOther", "EditHospitalizationLog", data, "6", "Other", true)%>
					        <%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedUnknown", "EditHospitalizationLog", data, "7", "Unknown", true)%>
                        </ul>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>OASIS M2430</legend>
        <div class="wide-column">
            <div class="row">
                <div><span class="green">(M2430)</span> Reason for Hospitalization: For what reason(s) did the patient require hospitalization?</div>
                <ul class="checkgroup two-wide">
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMed", "EditHospitalizationLog", data, "1", "Improper medication administration, medication side effects, toxicity, anaphylaxis", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationFall", "EditHospitalizationLog", data, "2", "Injury caused by fall", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationInfection", "EditHospitalizationLog", data, "3", "Respiratory infection (e.g., pneumonia, bronchitis)", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationOtherRP", "EditHospitalizationLog", data, "4", "Other respiratory problem", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHeartFail", "EditHospitalizationLog", data, "5", "Heart failure (e.g., fluid overload)", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationCardiac", "EditHospitalizationLog", data, "6", "Cardiac dysrhythmia (irregular heartbeat)", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMyocardial", "EditHospitalizationLog", data, "7", "Myocardial infarction or chest pain", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHeartDisease", "EditHospitalizationLog", data, "8", "Other heart disease", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationStroke", "EditHospitalizationLog", data, "9", "Stroke (CVA) or TIA", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHypo", "EditHospitalizationLog", data, "10", "Hypo/Hyperglycemia, diabetes out of control", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationGI", "EditHospitalizationLog", data, "11", "GI bleeding, obstruction, constipation, impaction", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationDehMal", "EditHospitalizationLog", data, "12", "Dehydration, malnutrition", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUrinaryInf", "EditHospitalizationLog", data, "13", "Urinary tract infection", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationIV", "EditHospitalizationLog", data, "14", "IV catheter-related infection or complication", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationWoundInf", "EditHospitalizationLog", data, "15", "Wound infection or deterioration", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUncontrolledPain", "EditHospitalizationLog", data, "16", "Uncontrolled pain", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMental", "EditHospitalizationLog", data, "17", "Acute mental/behavioral health problem", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationDVT", "EditHospitalizationLog", data, "18", "Deep vein thrombosis, pulmonary embolus", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationScheduled", "EditHospitalizationLog", data, "19", "Permanent placement", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationOther", "EditHospitalizationLog", data, "20", "Other than above reasons", true)%>
					<%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUK", "EditHospitalizationLog", data, "UK", "Reason unknown", true)%>
               </ul>
            </div>
		</div>
	</fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
                    <%= Html.ToggleTemplates("GenericDischargeNarrative")%>
                    <%= Html.TextArea("GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), new { @class = "taller", @id = "EditHospitalizationLog_GenericDischargeNarrative", @maxcharacters = "1500" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
    <%  } %>

</div>