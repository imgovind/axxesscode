﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Hospitalization Log | <%= Current.AgencyName %></span>
<div class="wrapper main note">
    <%  using (Html.BeginForm("InsertHospitalizationLog", "Patient", FormMethod.Post, new { @id = "newHospitalizationLogForm" })) { %>
    <fieldset>
        <legend>New Hospitalization Log</legend>
        <div class="column">
            <div class="row">
				<label for="NewHospitalizationLog_PatientName" class="fl">Patient</label>
				<div class="fr"><%= Html.PatientsFilteredByService((int)Current.AcessibleServices, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : string.Empty, new { @id = "NewHospitalizationLog_PatientName", @class = "required not-zero" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
				<label for="NewHospitalizationLog_User" class="fl">User</label>
				<div class="fr"><%= Html.Users("UserId", string.Empty, "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = "NewHospitalizationLog_User", @class = "required not-zero" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>OASIS M0903</legend>
                <div class="wide-column">
                    <div class="row narrower">
					    <label for="NewHospitalizationLog_M0903LastHomeVisitDate" class="fl"><span class="green">(M0903)</span> Date of Last (Most Recent) Home Visit</label>
						<div class="fr"><%= Html.TextBox("M0903LastHomeVisitDate", string.Empty, new { @id = "NewHospitalizationLog_M0903LastHomeVisitDate", @class = "date-picker" }) %></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M0906</legend>
                <div class="wide-column">
                    <div class="row narrower">
					    <label for="NewHospitalizationLog_M0906DischargeDate" class="fl"><span class="green">(M0906)</span> Transfer Date: Enter the date of the transfer of the patient</label>
						<div class="fr"><%= Html.TextBox("M0906DischargeDate", string.Empty, new { @id = "NewHospitalizationLog_M0906DischargeDate", @class = "date-picker" })%></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M2410</legend>
                <div class="wide-column">
                    <div class="row narrower">
						<div class="fl"><span class="green">(M2410)</span> To which Inpatient Facility has the patient been admitted?</div>
						<%= Html.OasisCheckgroupRadioOptions("M2410TypeOfInpatientFacility", "NewHospitalizationLog", null, new[] { "01", "02", "03", "04" }, new [] { "Hospital", "Rehabilitation facility", "Nursing home", "Hospice" }, "two-wide")%>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>OASIS M2440</legend>
                <div class="wide-column">
                    <div class="row narrower">
						<div class="fl"><span class="green">(M2440)</span> For what Reason(s) was the patient Admitted to a Nursing Home?</div>
						<ul class="checkgroup two-wide">
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedTherapy", "NewHospitalizationLog", null, "1", "Therapy services", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedRespite", "NewHospitalizationLog", null, "2", "Respite care", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedHospice", "NewHospitalizationLog", null, "3", "Hospice care", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedPermanent", "NewHospitalizationLog", null, "4", "Permanent placement", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedUnsafe", "NewHospitalizationLog", null, "5", "Unsafe for care at home", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedOther", "NewHospitalizationLog", null, "6", "Other", true)%>
							<%= Html.OasisCheckgroupOption("M2440ReasonPatientAdmittedUnknown", "NewHospitalizationLog", null, "7", "Unknown", true)%>
					   </ul>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>OASIS M2430</legend>
        <div class="wide-column">
            <div class="row">
                <label><span class="green">(M2430)</span> Reason for Hospitalization: For what reason(s) did the patient require hospitalization?</label>
                <ul class="checkgroup two-wide">
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMed", "NewHospitalizationLog", null, "1", "Improper medication administration, medication side effects, toxicity, anaphylaxis", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationFall", "NewHospitalizationLog", null, "2", "Injury caused by fall", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationInfection", "NewHospitalizationLog", null, "3", "Respiratory infection (e.g., pneumonia, bronchitis)", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationOtherRP", "NewHospitalizationLog", null, "4", "Other respiratory problem", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHeartFail", "NewHospitalizationLog", null, "5", "Heart failure (e.g., fluid overload)", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationCardiac", "NewHospitalizationLog", null, "6", "Cardiac dysrhythmia (irregular heartbeat)", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMyocardial", "NewHospitalizationLog", null, "7", "Myocardial infarction or chest pain", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHeartDisease", "NewHospitalizationLog", null, "8", "Other heart disease", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationStroke", "NewHospitalizationLog", null, "9", "Stroke (CVA) or TIA", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationHypo", "NewHospitalizationLog", null, "10", "Hypo/Hyperglycemia, diabetes out of control", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationGI", "NewHospitalizationLog", null, "11", "GI bleeding, obstruction, constipation, impaction", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationDehMal", "NewHospitalizationLog", null, "12", "Dehydration, malnutrition", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUrinaryInf", "NewHospitalizationLog", null, "13", "Urinary tract infection", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationIV", "NewHospitalizationLog", null, "14", "IV catheter-related infection or complication", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationWoundInf", "NewHospitalizationLog", null, "15", "Wound infection or deterioration", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUncontrolledPain", "NewHospitalizationLog", null, "16", "Uncontrolled pain", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationMental", "NewHospitalizationLog", null, "17", "Acute mental/behavioral health problem", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationDVT", "NewHospitalizationLog", null, "18", "Deep vein thrombosis, pulmonary embolus", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationScheduled", "NewHospitalizationLog", null, "19", "Permanent placement", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationOther", "NewHospitalizationLog", null, "20", "Other than above reasons", true)%>
				    <%= Html.OasisCheckgroupOption("M2430ReasonForHospitalizationUK", "NewHospitalizationLog", null, "UK", "Reason unknown", true)%>
				</ul>
			</div>
		</div>
	</fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
            <div class="row">
                <div class="template-text">
    				<%= Html.ToggleTemplates("GenericDischargeNarrative")%>
	    			<%= Html.TextArea("GenericDischargeNarrative", string.Empty, new { @class = "taller", @id = "NewHospitalizationLog_GenericDischargeNarrative", @maxcharacters = "1500" }) %>
	    		</div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
    <%  } %>
</div>