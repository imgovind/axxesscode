﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using(Html.BeginForm("AddDocument", "Patient", FormMethod.Post, new { @id="UploadDocument_Form"})) { %>
    <%= Html.Hidden("PatientId", Model.Id, new { @name = "PatientId" })%>
    <fieldset>
        <legend>Upload Document</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.DisplayNameWithMi %></div>
            </div>
            <div class="row">
                <label class="fl">Document Name</label>
                <div class="fr"><input type="text" class="required" name="Filename" id="UploadDocument_Filename" maxlength="200" /></div>
            </div>
            <div class="row">
                <label for="UploadDocument_Cassification" class="fl">Upload Type</label>
                <div id="UploadDocument_UploadType" class="fr"> <%= Html.UploadTypes("UploadTypeId", "0", "--Select Type--", new { @class = "required not-zero" }) %></div>
            </div>
            <div class="row">
                <label for="UploadDocument_Asset" class="fl">New Document</label>
                <div class="fr"><input id="UploadDocument_Asset" type="file" class="required" name="Asset" /></div>
                <div class="clr"></div>
                <div class="ac"><em>5MB file size limit</em></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Upload</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>