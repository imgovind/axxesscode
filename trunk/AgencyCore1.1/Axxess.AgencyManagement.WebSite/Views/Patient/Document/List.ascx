﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Documents List | <%= Current.DisplayName%></span>
<div class="wrapper main blue">
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
	<div id="ListPatientDocuments_GridContainer">
	    <%= Html.Telerik().Grid<PatientDocument>().Name("List_PatientDocuments").HtmlAttributes(new { @class = "grid-container" }).Columns(columns => {
                columns.Bound(c => c.Filename).ClientTemplate("<a href=\"/Asset/<#=AssetId#>\"><#=Filename#></a>").Title("Document Name").Sortable(true);
                columns.Bound(c => c.TypeName).Title("Type");            
                columns.Bound(c => c.CreatedDateFormatted).Title("Created").Width(120);
                columns.Bound(c => c.ModifiedDateFormatted).Title("Modified").Width(120);
				columns.Bound(c => c.Id).Title("Action").ClientTemplate("<a class=\"link\" onclick=\"Patient.Document.Edit('<#=PatientId#>','<#=Id#>')\">Edit</a><a class=\"link\" onclick=\"Patient.Document.Delete('<#=PatientId#>','<#=Id#>')\">Delete</a>");
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetDocuments", "Patient", new { patientId = Model.Id }))
	    	.ClientEvents(events => events.OnDataBinding("U.OnTGridDataBinding").OnDataBound("U.OnTGridDataBound").OnError("U.OnTGridError"))
			.Sortable().Footer(false).NoRecordsTemplate("No documents found.").Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>