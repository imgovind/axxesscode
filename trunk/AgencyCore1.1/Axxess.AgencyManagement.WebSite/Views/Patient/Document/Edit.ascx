﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDocument>" %>
<div class="wrapper main">
<%  using(Html.BeginForm("UpdateDocument", "Patient", FormMethod.Post, new { @id="EditDocument_Form"})) { %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @name = "PatientId" })%>
    <%= Html.Hidden("Id", Model.Id, new { @name = "Id" })%>
    <fieldset>
        <legend>Edit Patient Document</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="fl">Document Name</label>
                <div class="fr"><%= Html.TextBox("Filename", Model.Filename, new { @class = "required", @maxlength = "200" }) %></div>
            </div>
            <div class="row">
                <label class="fl">Upload Type</label>
                <div id="EditPatient_Type" class="fr"><%= Html.UploadTypes("UploadTypeId", Model.UploadTypeId.ToString(), "--Select Type--", new { @class = "required not-zero" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a id="EditDocument_Submit" class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>