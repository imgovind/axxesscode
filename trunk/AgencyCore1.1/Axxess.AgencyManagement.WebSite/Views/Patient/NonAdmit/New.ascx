﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddNonAdmit", "Patient", FormMethod.Post, new { @id = "newNonAdmitPatientForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Patient_Id" })%>
    <%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Patient_IsAdmit" })%>
    <fieldset>
        <legend>Non-Admit</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.DisplayName%></div>
            </div>
<%  if (Model.IsOnlyOneService) { %>
            <%= Html.Hidden("ServiceProvided", Model.CurrentServices.ToString())%>
<%  } else { %>
            <div class="row">
                <label class="fl">Service to Non-Admit</label>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <li class="option">
                            <div class="wrapper">
                                <%= string.Format("<input id='NonAdmit_Patient_ServiceProvided1' type='checkbox' value='HomeHealth' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.HomeHealth) ? "checked='checked'" : "")%>
                                <label for="NonAdmit_Patient_ServiceProvided1">
                                    Home Health Service
                                    <em>Current Status: <%= (PatientStatus)Model.HomeHealthStatus%></em>    
                                </label>
                            </div>
                        </li>
                        <li class="option">
                            <div class="wrapper">
                                <%= string.Format("<input id='NonAdmit_Patient_ServiceProvided2' type='checkbox' value='PrivateDuty' class='required' name='ServiceProvided' {0} />", Model.CurrentServices.Has(AgencyServices.PrivateDuty) ? "checked='checked'" : "")%>
                                <label for="NonAdmit_Patient_ServiceProvided1">
                                    Private Duty Service
                                    <em>Current Status: <%= (PatientStatus)Model.PrivateDutyStatus%></em>    
                                </label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
<%  } %>
        </div>
    </fieldset>
<%  if (Model.CurrentServices.Has(AgencyServices.HomeHealth)) { %>
    <fieldset id="NonAdmit_Patient_ServiceContainerHomeHealth">
        <legend>Home Health</legend>
        <div class="wide-column"><% Html.RenderPartial("NonAdmit/NonAdmitContent", 1);%></div>
    </fieldset>
<%  } %>
<%  if (Model.CurrentServices.Has(AgencyServices.PrivateDuty)) { %>
    <fieldset id="NonAdmit_Patient_ServiceContainerPrivateDuty">
        <legend>Private Duty</legend>
        <div class="wide-column"><% Html.RenderPartial("NonAdmit/NonAdmitContent", 2);%></div>
    </fieldset>
<%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>