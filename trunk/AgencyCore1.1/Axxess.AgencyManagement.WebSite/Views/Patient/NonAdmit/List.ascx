﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>
<span class="wintitle">Non-Admitted Patients | <%= Current.AgencyName %></span>
<%  var pageName = "PatientNonAdmit";  %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="fr buttons">
<%  if (!Current.IsAgencyFrozen && Current.HasRight(Permissions.ManagePatients)) { %>
        <li><a onclick="Patient.Demographic.New()">New Patient</a></li><br />
<%  } %>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
		<li><a url="Patient/NonAdmitExport" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <ul class="buttons fr">
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
        <div class="filter">
<%  if (!Current.AcessibleServices.IsAlone()) { %>
            <label for="<%= pageName %>_ServiceId">Service</label>
            <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", true, new { @class = "service input", @id = pageName + "_ServiceId" }) %>
<%  } else { %>
            <%= Html.Hidden("ServiceId", (int)Current.PreferredService, new { @class = "input", @id = pageName + "_ServiceId" }) %>
<%  } %>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer">
        <%= Html.Telerik().Grid<NonAdmit>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
				columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(2).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
				columns.Bound(p => p.DisplayName).Title("Patient").Width(4);
				columns.Bound(p => p.InsuranceName).Title("Insurance").Width(4).Sortable(false);
				columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(4).Sortable(false);
				columns.Bound(p => p.AddressFull).Title("Address").Width(6).Sortable(false);
				columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(3);
				columns.Bound(p => p.Phone).Title("Phone").Width(3).Sortable(false);
				columns.Bound(p => p.Gender).Width(2);
				columns.Bound(p => p.NonAdmissionReason).Width(5).Title("Non-Admit Reason");
				columns.Bound(p => p.NonAdmitDate).ClientTemplate("<#= U.FormatGridDate(NonAdmitDate) #>").Title("Non-Admit Date").Width(3);
				columns.Bound(p => p.Comments).Width(1).Sortable(false).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a tooltip=\"<#=CommentsCleaned#>\" class=\"note img icon16\" href=\"javascript:void(0);\" />"); 
				columns.Bound(p => p.Id).Width(2).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"<#=NonAdmitTypeName#>.Demographic.Admit.Open('<#=Id#>');\" class=\"\">Admit</a>").Title("Action").Visible(!Current.IsAgencyFrozen);
			}).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("NonAdmitList", "Patient", new { ServiceId = Model})).ClientEvents(evnts => evnts.OnRowDataBound("U.DataItemToolTip").OnDataBinding("U.OnTGridDataBinding").OnDataBound("U.OnTGridDataBound").OnError("U.OnTGridError")).NoRecordsTemplate("<h1 class='blue'>No Non-Admitted Patients found.</h1>").Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
	</div>
</div>