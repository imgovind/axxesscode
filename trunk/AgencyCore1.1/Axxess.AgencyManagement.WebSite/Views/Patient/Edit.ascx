﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Edit Patient | <%= Model.DisplayName %></span>
<div id="EditPatient_Tabs" class="tabs vertical-tabs vertical-tabs-left">
    <ul class="vertical-tab-list">
        <li><a href="#EditPatient_Information" action="Patient/Information">Information</a></li>
<%  if (Model.Services.Has(AgencyServices.HomeHealth) && Current.AcessibleServices.Has(AgencyServices.HomeHealth) && Model.HomeHealthStatus != (int)PatientStatus.Deprecated) { %>
        <li><a href="#EditPatient_HomeHealth" action="PatientProfile/Edit">Home Health</a></li>
<%  } %>
<%  if (Model.Services.Has(AgencyServices.PrivateDuty) && Current.AcessibleServices.Has(AgencyServices.PrivateDuty) && Model.PrivateDutyStatus != (int)PatientStatus.Deprecated)
    { %>
        <li><a href="#EditPatient_PrivateDuty" action="PrivateDuty/PatientProfile/Edit">Private Duty</a></li>
        <li><a href="#EditPatient_VisitRates" action="PrivateDuty/PatientProfile/VisitRates">Self Visit Rates &#38; Payors</a></li>
<%  } %>
        <li><a href="#EditPatient_Physicians" action="Patient/Physicians">Physicians</a></li>
        <li><a href="#EditPatient_EmergencyContacts" action="Patient/EmergencyContacts">Emergency Contacts</a></li>
    </ul>
    <div id="EditPatient_Information" class="tab-content"><% Html.RenderPartial("~/Views/Patient/Edit/Information.ascx", Model); %></div>
<%  if (Model.Services.Has(AgencyServices.HomeHealth)&& Current.AcessibleServices.Has(AgencyServices.HomeHealth) && Model.HomeHealthStatus != (int)PatientStatus.Deprecated) { %>
    <div id="EditPatient_HomeHealth" class="tab-content"></div>
<%  } %>
<%  if (Model.Services.Has(AgencyServices.PrivateDuty) && Current.AcessibleServices.Has(AgencyServices.PrivateDuty) && Model.PrivateDutyStatus != (int)PatientStatus.Deprecated)
    { %>
    <div id="EditPatient_PrivateDuty" class="tab-content"></div>
    <div id="EditPatient_VisitRates" class="tab-content"></div>
<%  } %>
    <div id="EditPatient_Physicians" class="tab-content"></div>
    <div id="EditPatient_EmergencyContacts" class="tab-content"></div>
</div>