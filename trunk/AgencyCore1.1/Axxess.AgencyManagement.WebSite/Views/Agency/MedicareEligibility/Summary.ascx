﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibilitySummary>>" %>
<%  string pagename = "MedicareEligibility"; %>
<%  var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"],ViewData["SortDirection"]); %>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
                <div class="fr">
                    <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                    <label for="<%= pagename %>_EndDate">&#8211;</label>
                    <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row ac">
                <div class="button"><%= string.Format("<a onclick=\"Agency.RebindMedicareEligibilitySummaryGridContent('{0}','MedicareEligibilityContent',{{StartDate:$('#{0}_StartDate').val(),EndDate:$('#{0}_EndDate').val()}},'{1}')\">Generate Summaries</a>", pagename, sortParams) %></div>
            </div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer" class="report-grid" style="top:125px"><% Html.RenderPartial("MedicareEligibility/Content", Model); %></div>
</div>