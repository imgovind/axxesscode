﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Template | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%  using (Html.BeginForm("Add", "Template", FormMethod.Post, new { @id = "newTemplateForm", @class = "mainform" })) { %>
    <fieldset>
        <legend>New Template</legend>
        <div class="wide-column">
            <div class="row narrow">
                <label for="NewTemplate_Title" class="fl">Name</label>
                <div class="fr"><%= Html.TextBox("Title", string.Empty, new { @id = "NewTemplate_Title", @class = "required longest", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewTemplate_Text">Text</label>
                <div class="ac"><textarea id="NewTemplate_Text" name="Text" class="taller" maxcharacters="5000"></textarea></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
</div>