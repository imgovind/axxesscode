﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">List Templates | <%= Current.AgencyName %></span>
<%  if (Model.IsUserCanViewList && Model.AvailableService != AgencyServices.None) { %>
<div class="wrapper main blue">
	<ul class="fr buttons ac">
        <%  if (Model.IsUserCanAdd) { %>
		<li><a class="new">New Template</a></li><div class="clr"></div>
        <%  } %>
        <%  if (Model.IsUserCanExport) { %>
		<li><a url="Export/Templates" class="export">Excel Export</a></li>
		<div class="clr"></div>
        <%  } %>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
    </fieldset>
    <div class="clr"></div>
    <%  var isVisible = (Model.IsUserCanDelete || Model.IsUserCanEdit); %>
    <%  var action = new List<string>(); %>
    <%  if (Model.IsUserCanEdit) action.Add("<a onclick=\"Template.Edit('<#=Id#>')\">Edit</a>"); %>
    <%  if (Model.IsUserCanDelete) action.Add("<a onclick=\"Template.Delete('<#=Id#>')\">Delete</a>"); %>
    <%  var count = action.Count; %>
    <%  var url = action.ToArray().Join(" | "); %>
    <%= Html.Telerik().Grid<AgencyTemplate>().Name("List_Template").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
            columns.Bound(t => t.Title).Title("Name").Sortable(true);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
            columns.Bound(t => t.Id).ClientTemplate(url).Title("Action").Sortable(false).Width(count*50).Visible(isVisible);
        }).NoRecordsTemplate("No Templates found.").DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Template").OperationMode(GridOperationMode.Client)).ClientEvents(c => c
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } else { %>
<div class="wrapper main"><%= Html.NotAuthorized("view the list of templates") %></div>
<%  } %> 