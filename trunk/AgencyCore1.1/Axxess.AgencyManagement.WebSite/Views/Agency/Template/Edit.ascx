﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTemplate>" %>
<span class="wintitle">Edit Template | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%  using (Html.BeginForm("Update", "Template", FormMethod.Post, new { @id = "editTemplateForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Template_Id" }) %>
    <fieldset>
        <legend>Edit Template</legend>
        <div class="wide-column">
            <div class="row narrow">
                <label for="EditTemplate_Title" class="fl">Name</label>
                <div class="fr"><%= Html.TextBox("Title", Model.Title, new { @id = "EditTemplate_Title", @class = "required longest", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="EditTemplate_Text">Text</label>
                <div class="ac"><textarea id="EditTemplate_Text" name="Text" class="taller" maxcharacters="5000"><%= Model.Text %></textarea></div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog){ %>
    	<a class="fr img icon32 log" onclick="Log.LoadTemplateLog('<%= Model.Id %>')"/>
    <%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
    <%  } %>

</div>