﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<fieldset>
    <legend>UB-04</legend>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditLocationCost_Id" })%>
    <div class="column">
        <div class="row">
            <label class="fl">Payor</label>
            <div class="fr"><%= Model.InsuranceName %></div>
        </div>
        <div class="row">
            <label class="fl">Submitter Name</label>
            <div class="fr"><%= Model.SubmitterName %></div>
        </div>
        <div class="row">
            <label class="fl">Contact Person Name</label>
            <div class="fr"><%= string.Format("{0}, {1}", Model.ContactPersonLastName, Model.ContactPersonFirstName) %></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl">Submitter Id</label>
            <div class="fr"><%= Model.SubmitterId %></div>
        </div>
        <div class="row">
            <label class="fl">Submitter Phone</label>
            <div class="fr"><%= Model.SubmitterPhone.ToPhone() %></div>
        </div>
        <div class="row">
            <label class="fl">Contact Person Phone</label>
            <div class="fr"><%= Model.ContactPersonPhone.ToPhone() %></div>
        </div>
    </div>
    <div class="wide-column">
        <div class="row narrower">
            <label for="Agency_Locator1_Code1" class="fl">UB04 Locator 81CCa</label>
            <%= Html.Hidden("Ub04Locator81", "Locator1")%>
            <div class="fr">
                <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "Agency_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "Agency_Locator1_Code2", @class = "short", @maxlength = "10" })%>
                <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "Agency_Locator1_Code3", @class = "short", @maxlength = "12" })%>
            </div>
        </div>
        <div class="row narrower">
            <label for="Agency_Locator2_Code1" class="fl">UB04 Locator 81CCb</label>
            <%= Html.Hidden("Ub04Locator81", "Locator2")%>
            <div class="fr">
                <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "Agency_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "Agency_Locator2_Code2", @class = "short", @maxlength = "10" })%>
                <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "Agency_Locator2_Code3", @class = "short", @maxlength = "12" })%>
            </div>
        </div>
        <div class="row narrower">
            <label for="Agency_Locator3_Code1" class="fl">UB04 Locator 81CCc</label>
            <%= Html.Hidden("Ub04Locator81", "Locator3")%>
            <div class="fr">
                <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "Agency_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "Agency_Locator3_Code2", @class = "short", @maxlength = "10" })%>
                <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "Agency_Locator3_Code3", @class = "short", @maxlength = "12" })%>
            </div>
        </div>
        <div class="row narrower">
            <label for="Agency_Locator4_Code1" class="fl">UB04 Locator 81CCd</label>
            <%= Html.Hidden("Ub04Locator81", "Locator4")%>
            <div class="fr">
                <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "Agency_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "Agency_Locator4_Code2", @class = "short", @maxlength = "10" })%>
                <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "Agency_Locator4_Code3", @class = "short", @maxlength = "12" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Medicare Address</legend>
    <div class="wide-column">
        <div class="row ac">
            <em>Not Required</em>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="EditInsurance_MedicareAddressLine1" class="fl">Address Line 1</label>
            <div class="fr"><%= Html.TextBox("MedicareAddressLine1", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressLine1 : string.Empty, new { @id = "EditInsurance_MedicareAddressLine1", @maxlength = "75" })%></div>
        </div>
        <div class="row">
            <label for="EditInsurance_MedicareAddressLine2" class="fl">Address Line 2</label>
            <div class="fr"><%= Html.TextBox("MedicareAddressLine2", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressLine2 : string.Empty, new { @id = "EditInsurance_MedicareAddressLine2", @maxlength = "75" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="EditInsurance_MedicareAddressCity" class="fl">City</label>
            <div class="fr"><%= Html.TextBox("MedicareAddressCity", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressCity : string.Empty, new { @id = "EditInsurance_MedicareAddressCity", @class = "address-city" })%></div>
        </div>
        <div class="row">
            <label for="EditInsurance_MedicareAddressStateCode" class="fl">State, Zip</label>
            <div class="fr">
                <%= Html.States("MedicareAddressStateCode", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressStateCode : string.Empty, new { @id = "EditInsurance_MedicareAddressStateCode", @class = "address-state short" })%>
                <%= Html.TextBox("MedicareAddressZipCode", Model.MedicareInsurance != null ? Model.MedicareInsurance.AddressZipCode : string.Empty, new { @id = "EditInsurance_MedicareAddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Visit Rates</legend>
    <div class="wide-column">
<%  if (Model.IsUserCanAdd) { %>
       <div class="row">
            <div class="fr">
                <div class="ancillary-button"><a onclick="UserInterface.ShowNewLocationBillData('<%= Model.Id %>')">Add Visit Information</a></div>
            </div>
        </div>
<%  } %>
        <div class="row">
<%  var isVisible =(Model.IsUserCanEdit || Model.IsUserCanDelete); %>
<%  var action = new List<string>(); %>
<%  var width = 0; %>
<%  if (Model.IsUserCanEdit) { %>
    <%  action.Add("<a class=\"link\" onclick=\"UserInterface.ShowEditLocationBillData($('#EditLocationCost_Id').val(),'<#=Id#>')\"> Edit </a>"); %>
    <%  width += 50; %>
<%  } %>
<%  if (Model.IsUserCanDelete) { %>
    <%  action.Add("<a class=\"link\" onclick=\"Agency.DeleteLocationBillData($('#EditLocationCost_Id').val(),'<#=Id#>')\">Delete</a>"); %>
    <%  width += 50; %>
<%  } %>
<%  var count = action.Count; %>
<%  var url = action.ToArray().Join(" "); %>
            <%= Html.Telerik().Grid<ChargeRate>().HtmlAttributes(new { @class = "position-relative" }).Name("EditLocation_BillDatas").DataKeys(keys => keys.Add(r => r.Id).RouteKey("Id")).Columns(columns => {
                    columns.Bound(e => e.DisciplineTaskName).Title("Task");
                    columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                    columns.Bound(e => e.RevenueCode).Title("Rev. Code").Width(75);
                    columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                    columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(70);
                    columns.Bound(e => e.Modifier).Title("Modifier").Width(80);
                    columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(85);
                    columns.Bound(e => e.Id).ClientTemplate(url).Title("Action").Sortable(false).Width(width).Visible(isVisible);
                }).DataBinding(dataBinding => dataBinding.Ajax().Select("LocationBillDatas", "Agency", new { locationId = Model.Id })).Sortable().Footer(false) %>
        </div>
    </div>
</fieldset>