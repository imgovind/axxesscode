﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SaveLocationBillData", "Agency", FormMethod.Post, new { @id = "newLocationBillData" })) { %>
    <%= Html.Hidden("LocationId", Model, new { @id = "New_LocationBillData_LocationId" })%>
    <fieldset class="newmed">
        <legend>New Visit Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="New_LocationBillData_Task" class="fl">Task</label>
                <div class="fr"><%=Html.LocationCostDisciplineTask("Id", 0, Model, true, new { @id = "New_LocationBillData_Task", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="New_LocationBillData_Description" class="fl">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", string.Empty, new { @id = "New_LocationBillData_Description", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="New_LocationBillData_RevenueCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", string.Empty, new { @id = "New_LocationBillData_RevenueCode", @class = "Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_LocationBillData_HCPCS" class="fl">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", string.Empty, new { @id = "New_LocationBillData_HCPCS", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_LocationBillData_ChargeRate" class="fl">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", string.Empty, new { @id = "New_LocationBillData_ChargeRate", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_LocationBillData_Modifier" class="fl">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", string.Empty, new { @id = "New_LocationBillData_Modifier", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", string.Empty, new { @id = "New_LocationBillData_Modifier2", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", string.Empty, new { @id = "New_LocationBillData_Modifier3", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", string.Empty, new { @id = "New_LocationBillData_Modifier4", @class = "insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
