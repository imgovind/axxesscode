﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">List Contact | <%= Current.AgencyName%></span>
<%  if (Model.AvailableService != AgencyServices.None) { %>
<div class="wrapper main blue">
    <ul class="fr buttons">
    <%  if (Model.IsUserCanAdd) { %>
		<li><a class="new">New Contact</a></li><br />
	<%  } %>
    <%  if (Model.IsUserCanExport) { %>
        <li><a url="Export/Contacts" class="export">Excel Export</a></li>
    <%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyContactGridRow>().Name("List_Contact").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
            columns.Bound(c => c.Name).Width(150).Sortable(true);
            columns.Bound(c => c.Company).Sortable(true);
            columns.Bound(c => c.Type).Width(150).Sortable(true);
            columns.Bound(c => c.Phone).Width(160).Sortable(false).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
			columns.Bound(c => c.Fax).Width(120).Sortable(false);
			columns.Bound(c => c.Email).ClientTemplate("<a href='mailto:<#= Email #>'><#= Email #></a>").Width(150).Sortable(true);
			columns.Bound(c => c.Comments).ClientTemplate("<a class=\"img icon16 yellow note\"><#= Comments #></a>").Title("").Sortable(false).Width(35);
			columns.Bound(c => c.Id).ClientTemplate("<# if ('" + Model.IsUserCanEdit.ToString() + "' == 'True') { #><a class=\"link\" onclick=\"Contact.Edit('<#= Id #>')\">Edit</a><# } if ('" + Model.IsUserCanDelete.ToString() + "' == 'True') { #><a class=\"link\" onclick=\"Contact.Delete('<#= Id #>')\">Delete</a><# } #>").Title("Action").Sortable(false).Width((Model.IsUserCanEdit.ToInteger() + Model.IsUserCanDelete.ToInteger()) * 50).Visible(!Current.IsAgencyFrozen && (Model.IsUserCanEdit || Model.IsUserCanDelete));
	    }).ClientEvents(c => c
            .OnRowDataBound("U.DataItemToolTip")
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Contacts found.").DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Contact").OperationMode(GridOperationMode.Client)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } %>