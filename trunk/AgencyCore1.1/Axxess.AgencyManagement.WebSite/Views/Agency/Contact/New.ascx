﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Contact | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Contact", FormMethod.Post, new { @id = "newContactForm", @class = "mainform" })) { %>
    <fieldset>
        <legend>Contact Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewContact_CompanyName">Company Name</label>
                <div class="fr"><%= Html.TextBox("CompanyName", string.Empty, new { @id = "NewContact_CompanyName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_FirstName">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewContact_FirstName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_LastName">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewContact_LastName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_Email">Contact Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewContact_EmailAddress", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_Type">Contact Type</label>
                <div class="fr"><%= Html.ContactTypes("ContactType", string.Empty, new { @id = "NewContact_Type", @class = "ContactType required not-zero" })%></div>
                <div class="clr"></div>
                <div class="sub row" id="NewContact_ContactTypeOther">
                    <label for="ContactTypeOther" class="fl">Specify</label>
                    <div class="fr"><%= Html.TextBox("ContactTypeOther", string.Empty, new { @maxlength = "100" })%></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewContact_AddressLine1">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewContact_AddressLine1", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_AddressLine2">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewContact_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_AddressCity">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewContact_AddressCity", @maxlength = "75", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_AddressStateCode">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", string.Empty, new { @id = "NewContact_AddressStateCode", @class = "address-state required not-zero short" })%>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewContact_AddressZipCode", @class = "numeric zip required shorter", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewContact_PhonePrimary1">Office Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhonePrimaryArray", string.Empty, "NewContact_PhonePrimary", "required") %></div>
            </div>
            <div class="row">
                <label for="NewContact_PhoneExtension">Office Phone Extension</label>
                <div class="fr"><%= Html.TextBox("PhoneExtension", string.Empty, new { @id = "NewContact_PhoneExtension", @maxlength = "4", @class = "numeric phone-long" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_PhoneAlternate1">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneAlternateArray", string.Empty, "NewContact_PhoneAlternate", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="NewContact_Fax1">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", string.Empty, "NewContact_Fax", string.Empty)%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac"><%= Html.TextArea("Comments", string.Empty, new { @id = "NewContact_Comments", @maxcharacters = "1000" })%></div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>