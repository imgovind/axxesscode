﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyContact>" %>
<span class="wintitle">Edit Contact | <%= Model.DisplayName %></span>
<div class="wrapper main">
<% using (Html.BeginForm("Update", "Contact", FormMethod.Post, new { @id = "editContactForm", @class = "mainform" }))
   { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditContact_Id" }) %>
    <fieldset>
        <legend>Contact Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditContact_CompanyName">Company Name</label>
                <div class="fr"><%= Html.TextBox("CompanyName", Model.CompanyName, new { @id = "EditContact_CompanyName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_FirstName">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditContact_FirstName", @maxlength = "75", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_LastName">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditContact_LastName", @maxlength = "75", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_Email">Contact Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditContact_EmailAddress", @class = "email", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_Type">Contact Type</label>
                <div class="fr"><%= Html.ContactTypes("ContactType", Model.ContactType, new { @id = "EditContact_Type", @class = "ContactType required not-zero" }) %></div>
                <div class="clr"></div>
                <div class="sub row" id="EditContact_ContactTypeOther">
                    <label for="ContactTypeOther" class="fl">Specify</label>
                    <div class="fr"><%= Html.TextBox("ContactTypeOther", Model.ContactTypeOther, new { @maxlength = "100" }) %></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditContact_AddressLine1">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditContact_AddressLine1", @maxlength = "75", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_AddressLine2">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditContact_AddressLine2", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_AddressCity">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditContact_AddressCity", @maxlength = "75", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_AddressStateCode">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditContact_AddressStateCode", @class = "address-state required not-zero short" }) %>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditContact_AddressZipCode", @class = "numeric required zip shorter", @maxlength = "9" }) %>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_PhonePrimary1">Office Phone</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("PhonePrimaryArray", Model.PhonePrimary, "EditContact_PhonePrimary", "")%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_PhoneExtension">Office Phone Extension</label>
                <div class="fr"><%= Html.TextBox("PhoneExtension", Model.PhoneExtension, new { @id = "EditContact_PhoneExtension", @maxlength = "4", @class = "numeric phone-long" }) %></div>
            </div>
            <div class="row">
                <label for="EditContact_PhoneAlternate1">Mobile Phone</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("PhoneAlternateArray", Model.PhoneAlternate, "EditContact_PhoneAlternate", "")%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_Fax1">Fax Number</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("FaxNumberArray", Model.FaxNumber, "EditContact_Fax", "")%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : "", new { @id = "EditContact_Comments", @maxcharacters = "1000" }) %>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog)
       { %>
    	<a class="fr img icon32 log" onclick="Log.LoadContactLog('<%= Model.Id %>')"/>
    <% } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>