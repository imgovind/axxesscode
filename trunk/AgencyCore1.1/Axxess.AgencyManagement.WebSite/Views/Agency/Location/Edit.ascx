﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Location | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Location", FormMethod.Post, new { @id = "editLocationForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Location_Id" })%>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Location_Name" class="fl">Location Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Location_Name", @class = "required", @maxlength = "20" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Location_CustomId" class="fl">Custom ID</label>
                <div class="fr"><%= Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_Location_CustomId" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Location_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Location_AddressLine1", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Location_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Location_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Location_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Location_AddressCity", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Location_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States( "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Location_AddressStateCode", @class = "address-state required short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Location_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Location_PhoneArray1" class="fl">Primary Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneArray", Model.PhoneWork, "Edit_Location_PhoneArray1", "required") %></div>
            </div>
            <div class="row">
                <label for="Edit_Location_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", Model.FaxNumber, "Edit_Location_FaxNumberArray1", string.Empty) %></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Location_Comments">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Location_Comments" })%></div>
            </div>
        </div>                
    </fieldset>
	<a class="fr img icon32 log" onclick="Log.LoadLocationLog('<%= Model.Id %>')"></a>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>