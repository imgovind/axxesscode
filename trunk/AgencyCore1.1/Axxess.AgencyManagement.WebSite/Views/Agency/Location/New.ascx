﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Location", FormMethod.Post, new { @id = "newLocationForm" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Location_Name" class="fl">Location Name</label>
                <div class="fr"><%= Html.TextBox("Name", "", new { @id = "New_Location_Name", @class = "required", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="New_Location_CustomId" class="fl">Custom Id</label>
                <div class="fr"><%= Html.TextBox("CustomId", "", new { @id = "New_Location_CustomId", @class = "required" }) %></div>
            </div>
        </div>   
        <div class="column">
            <div class="row">
                <label for="New_Location_MedicareProviderNumber" class="fl">Medicare Provider Number</label>
                <div class="fr"><%= Html.TextBox("MedicareProviderNumber", "", new { @id = "New_Location_MedicareProviderNumber", @class = "required", @maxlength = "10" }) %></div>
            </div>
        </div>
    </fieldset>  
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row">
                <label for="New_Location_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Location_AddressLine1", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="New_Location_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Location_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="New_Location_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "New_Location_AddressCity", @class = "address-city required" })%></div>
            </div>
            <div class="row">
                <label for="New_Location_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", "", new { @id = "New_Location_AddressStateCode", @class = "address-state required short" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "New_Location_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "5" }) %>
                </div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="New_Location_PhoneArray1" class="fl">Primary Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneArray", "", "New_Location_PhoneArray1", "required") %></div>
            </div>
            <div class="row">
                <label for="New_Location_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", "", "New_Location_FaxNumberArray1", "") %></div>
            </div> 
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsMainOffice", "1", false, "This Location is the Main Office") %>
                </ul>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="New_Location_Comments">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", "", new { @id = "New_Location_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
<%  } %>
</div>