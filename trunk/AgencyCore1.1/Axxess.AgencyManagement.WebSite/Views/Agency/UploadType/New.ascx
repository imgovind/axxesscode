﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Upload Type | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using(Html.BeginForm("Add", "UploadType", FormMethod.Post, new { @id = "newUploadTypeForm" })) { %>
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="New_UploadType_Type" class="fl">Type</label>
                <div class="fr"><%= Html.TextBox("Type", string.Empty, new { @id = "New_UploadType_Type", @class = "required", @maxlength = "200"}) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>