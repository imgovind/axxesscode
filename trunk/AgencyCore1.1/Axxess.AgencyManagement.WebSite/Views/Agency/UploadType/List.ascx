﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">List Upload Type | <%=Current.AgencyName %></span>
<div class="wrapper main blue">
	<ul class="fr buttons ac">
<%  if (!Current.IsAgencyFrozen && Model.NewPermissions != AgencyServices.None) { %>
        <li><a onclick="UserInterface.ShowNewUploadType()">New Upload Type</a></li><div class="clr"></div>
<%  } %>
        <div class="clr"></div><div class="clr"></div>
<%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a url="Export/UploadTypes" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
<%  var canEdit = Model.EditPermissions != AgencyServices.None; var canDelete = Model.DeletePermissions != AgencyServices.None; %>
<%= Html.Telerik().Grid<UploadType>().Name("List_UploadType").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
        columns.Bound(t => t.Type).Title("Type").Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
        columns.Bound(t => t.CreatedDateString).Title("Created").Width(120);
        columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
		columns.Bound(t => t.Id).ClientTemplate("<#if(" + canEdit.ToString().ToLower() + ")#><a class=\"link\" onclick=\"UserInterface.ShowEditUploadType('<#=Id#>')\">Edit</a><#if(" + canDelete.ToString().ToLower() + ")#><a class=\"link\" onclick=\"UploadType.Delete('<#=Id#>')\">Delete</a>").Title("Action").Sortable(false).Width(50 * (canEdit.ToInteger() + canDelete.ToInteger())).Visible(!Current.IsAgencyFrozen);
    }).NoRecordsTemplate("No Upload Types found.").DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "UploadType").OperationMode(GridOperationMode.Client)).ClientEvents(events => events
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnError("U.OnTGridError")
    ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
