﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UploadType>" %>
<span class="wintitle">Edit Upload Type | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "UploadType", FormMethod.Post, new { @id = "editUploadTypeForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_UploadType_Id" }) %>
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_UploadType_Type" class="fl">Type</label>
                <div class="fr"><%= Html.TextBox("Type", Model.Type, new { @id = "Edit_UploadType_Type", @class = "required", @maxlength = "100"}) %></div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog){ %>
    	<a class="fr img icon32 log" onclick="Log.LoadUploadTypeLog('<%= Model.Id %>')"/>
    <%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>