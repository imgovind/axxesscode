﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Visit Rates | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("EditCost", "Agency", FormMethod.Post, new { @id = "editVisitCostForm" })){ %>
    <fieldset>
        <legend>Branch</legend>
        <div class="wide-column">
            <div class="row narrower">
	            <label for="EditVisitRate_LocationId" class="fl">Agency Branch</label>
	            <div class="fr"><%= Html.BranchOnlyList("AgencyLocationId", Model.Id.ToString(), 0, new { @id = "EditVisitRate_LocationId", @class = "branch-location required not-zero" }) %></div>
	        </div>
	    </div>
	</fieldset>
    <div id="EditVisitRate_Container"><% Html.RenderPartial("~/Views/Agency/VisitRateContent.ascx", Model); %></div>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
<script type="text/javascript">
    Agency.LoadVisitRateContent($('#Edit_VisitRate_LocationId').val());
    $('#Edit_VisitRate_LocationId').on("change", function() { Agency.LoadVisitRateContent($(this).val());});
</script>