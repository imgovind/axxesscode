﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
	<% using (Html.BeginForm("CheckSignature", "Agency", FormMethod.Post)){ %>
		<fieldset class="editmed">
			<legend>Verify Signature</legend>
			<div class="wide-column">
				<div class="row">
					Before proceeding, enter your signature in the textbox provided below to confirm that you are authorized to
					make changes to your company information.
				</div>
				<div class="row">
					<label for="checkSignature" class="fl">Signature</label>
					<div class="fr"><%= Html.Password("signature", string.Empty, new { @id = "checkSignature", @class = "required" })%></div>
				</div>
			</div>
		</fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Proceed</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <% } %>
</div>

