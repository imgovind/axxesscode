﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">List of Supplies | <%= Current.AgencyName %></span>
<%  if (Model.IsUserCanViewList && Model.AvailableService != AgencyServices.None) { %>
<div class="wrapper main blue">
	<ul class="fr buttons ac">
    <%  if (Model.IsUserCanAdd) { %>
		<li><a class="new">New Supply</a></li><div class="clr"></div>
    <%  } %>
    <%  if (Model.IsUserCanExport) { %>
		<li><a url="Export/Supplies" class="export">Excel Export</a></li>
		<div class="clr"></div>
    <%  } %>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
    </fieldset>
    <div class="clr"></div>
    <%  var isVisible =  (Model.IsUserCanEdit || Model.IsUserCanDelete); %>
    <%  var action = new List<string>(); %>
    <%  if (Model.IsUserCanEdit) action.Add("<a onclick=\"Supply.Edit('<#=Id#>')\">Edit</a>"); %>
    <%  if (Model.IsUserCanDelete) action.Add("<a onclick=\"Supply.Delete('<#=Id#>')\">Delete</a>"); %>
    <%  var count = action.Count; %>
    <%  var url = action.ToArray().Join(" | "); %>
    <%= Html.Telerik().Grid<AgencySupply>().Name("List_Supply").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
            columns.Bound(t => t.Description).Sortable(true);
            columns.Bound(t => t.Code).Title("HCPCS").Sortable(true).Width(65);
            columns.Bound(t => t.RevenueCode).Title("Rev Code").Sortable(true).Width(75);
            columns.Bound(t => t.UnitCost).Format("${0:0.00}").Sortable(true).Width(70).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
			columns.Bound(t => t.Created).Format("{0:MM/dd/yyyy}").Title("Created").Sortable(true).Width(100);
            columns.Bound(t => t.Id).ClientTemplate(url).Title("Action").Sortable(false).Width(50*count).Visible(isVisible);
        }).NoRecordsTemplate("No Supplies found.").DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Supply").OperationMode(GridOperationMode.Client)).Sortable().ClientEvents(events => events
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } else { %>
<div class="wrapper main"><%= Html.NotAuthorized("view the list of supplies") %></div>
<%  } %> 