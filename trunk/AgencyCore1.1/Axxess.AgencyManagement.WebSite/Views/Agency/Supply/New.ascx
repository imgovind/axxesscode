﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Supply | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%  using (Html.BeginForm("Add", "Supply", FormMethod.Post, new { @id = "newSupplyForm" , @class = "mainform"})) { %>
    <fieldset>
        <legend>New Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewSupply_Description" class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", "", new { @id = "NewSupply_Description", @class = "required longer", @maxlength = "350" }) %></div>
            </div>
            <div class="row">
                <label for="NewSupply_HcpcsCode" class="fl">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", "", new { @id = "NewSupply_HcpcsCode", @class = "shorter", @maxlength = "6" }) %></div>
            </div>
            <div class="row">
                <label for="NewSupply_RevCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", "", new { @id = "NewSupply_RevCode", @class = "shorter", @maxlength = "6" }) %></div>
            </div>
            <div class="row">
                <label for="NewSupply_UnitCost" class="fl">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", "", new { @id = "NewSupply_UnitCost", @class = "shorter currency", @maxlength = "6" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>

</div>