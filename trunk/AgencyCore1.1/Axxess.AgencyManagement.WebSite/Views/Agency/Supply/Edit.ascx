﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySupply>" %>
<span class="wintitle">Edit Supply | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%  using (Html.BeginForm("Update", "Supply", FormMethod.Post, new { @id = "editSupplyForm" , @class = "mainform"})) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Supply_Id" }) %>
    <fieldset>
        <legend>Edit Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label for="EditSupply_Description" class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Description, new { @id = "EditSupply_Description", @class = "required longer", @maxlength = "350" }) %></div>
            </div>
            <div class="row">
                <label for="EditSupply_HcpcsCode" class="fl">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "EditSupply_HcpcsCode", @class = "shorter", @maxlength = "6" }) %></div>
            </div>
            <div class="row">
                <label for="EditSupply_RevCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "EditSupply_RevCode", @class = "shorter", @maxlength = "6" }) %></div>
            </div>
            <div class="row">
                <label for="EditSupply_UnitCost" class="fl">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", String.Format("{0:0.00}",Model.UnitCost), new { @id = "EditSupply_UnitCost", @class = "currency shorter", @maxlength = "6" }) %></div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog){ %>
    	<a class="fr img icon32 log" onclick="Log.LoadSupplyLog('<%= Model.Id %>')"/>
    <%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
    <%  } %>

</div>