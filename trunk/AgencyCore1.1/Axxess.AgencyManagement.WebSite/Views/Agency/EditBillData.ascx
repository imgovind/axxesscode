﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateLocationBillData", "Agency", FormMethod.Post, new { @id = "editLocationBillData" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("LocationId", Model.LocationId, new { @id = "Edit_LocationBillData_LocationId" })%>
    <fieldset class="newmed">
        <legend>Edit Visit Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_LocationBillData_Task" class="fl">Task</label>
                <div class="fr"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_Description" class="fl">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", Model.PreferredDescription, new { @id = "Edit_LocationBillData_Description", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_RevenueCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "Edit_LocationBillData_RevenueCode", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_HCPCS" class="fl">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "Edit_LocationBillData_HCPCS", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_ChargeRate" class="fl">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", Model.Charge, new { @id = "Edit_LocationBillData_ChargeRate", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_LocationBillData_Modifier" class="fl">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", Model.Modifier, new { @id = "Edit_LocationBillData_Modifier", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", Model.Modifier2, new { @id = "Edit_LocationBillData_Modifier2", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", Model.Modifier3, new { @id = "Edit_LocationBillData_Modifier3", @class = "insurance-modifier", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", Model.Modifier4, new { @id = "Edit_LocationBillData_Modifier4", @class = "insurance-modifier", @maxlength = "2" })%>
                </div>
            </div>
         </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
