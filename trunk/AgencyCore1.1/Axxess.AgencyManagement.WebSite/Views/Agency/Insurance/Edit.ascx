﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<span class="wintitle">Edit Insurance | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Insurance", FormMethod.Post, new { @id = "editInsuranceForm" , @class = "mainform"})) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditInsurance_Id" })%>
    <%  var data = Model != null ? Model.ToChargeRateDictionary() : new Dictionary<string, ChargeRate>(); %>
    <%  var locator = Model != null ? Model.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_Name" class="fl">Insurance/Payor Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "EditInsurance_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_PayorType" class="fl">Payor Type</label>
                <div class="fr"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", Convert.ToString(Model.PayorType), new { @id = "EditInsurance_PayorType", @class = "required not-zero" })%></div>
            </div>
            <div id="EditInsurance_IsUsingPatientRatesRow" class="row usingpatientratesrow">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsUsingPatientRates", "true", false, "This insurance will use the patient&#8217;s visit rates") %>
                </ul>
            </div>
            <div class="row">
                <label for="EditInsurance_InvoiceType" class="fl">Invoice Type</label>
                <div class="fr">
                    <%  var invoiceType = new SelectList(new[] {
                            new SelectListItem { Text = "UB-04", Value = "1" },
                            new SelectListItem { Text = "HCFA 1500", Value = "2" },
                            new SelectListItem { Text = "Invoice", Value = "3" }
                        }, "Value", "Text", Convert.ToString(Model.InvoiceType)); %>
                    <%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "EditInsurance_InvoiceType" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInsurance_BillType" class="fl">Bill Type</label>
                <div class="fr">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Institutional", Value = "institutional" },
                            new SelectListItem { Text = "Professional", Value = "professional" }
                        }, "Value", "Text", Model.BillType); %>
                    <%= Html.DropDownList("BillType", billType, new { @id = "EditInsurance_BillType" })%>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("HasContractWithAgency", "true", Model.HasContractWithAgency, "Agency has a contract with this insurance") %>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_PayorId" class="fl">Payor Id</label>
                <div class="fr"><%= Html.TextBox("PayorId", Convert.ToString(Model.PayorId), new { @id = "EditInsurance_PayorId", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ProviderId" class="fl">Provider ID/Code</label>
                <div class="fr"><%= Html.TextBox("ProviderId", Model.ProviderId, new { @id = "EditInsurance_ProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_OtherProviderId" class="fl">Other Provider ID</label>
                <div class="fr"><%= Html.TextBox("OtherProviderId", Model.OtherProviderId, new { @id = "EditInsurance_OtherProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ProviderSubscriberId" class="fl">Provider Subscriber ID</label>
                <div class="fr"><%= Html.TextBox("ProviderSubscriberId", Model.ProviderSubscriberId, new { @id = "EditInsurance_ProviderSubscriberId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_SubmitterId" class="fl">Submitter ID</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "EditInsurance_SubmitterId", @maxlength = "40" })%></div>
            </div>
        </div>
        <div id="EditInsurance_Ub04Locators" class="wide-column ub04locators <%= Model.InvoiceType == 1 ? "" : "hidden" %>">
            <div class="row narrower">
                <label for="EditInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCa</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="fr">
                    <%= Html.TextBox("Locator1_Code1", locator.ContainsKey("Locator1") ? locator["Locator1"].Code1 : string.Empty, new { @id = "EditInsurance_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator1_Code2", locator.ContainsKey("Locator1") ? locator["Locator1"].Code2 : string.Empty, new { @id = "EditInsurance_Locator1_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator1_Code3", locator.ContainsKey("Locator1") ? locator["Locator1"].Code3 : string.Empty, new { @id = "EditInsurance_Locator1_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="EditInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCb</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator2")%>
                <div class="fr">
                    <%= Html.TextBox("Locator2_Code1", locator.ContainsKey("Locator2") ? locator["Locator2"].Code1 : string.Empty, new { @id = "EditInsurance_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator2_Code2", locator.ContainsKey("Locator2") ? locator["Locator2"].Code2 : string.Empty, new { @id = "EditInsurance_Locator2_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator2_Code3", locator.ContainsKey("Locator2") ? locator["Locator2"].Code3 : string.Empty, new { @id = "EditInsurance_Locator2_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="EditInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCc</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator3")%>
                <div class="fr">
                    <%= Html.TextBox("Locator3_Code1", locator.ContainsKey("Locator3") ? locator["Locator3"].Code1 : string.Empty, new { @id = "EditInsurance_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator3_Code2", locator.ContainsKey("Locator3") ? locator["Locator3"].Code2 : string.Empty, new { @id = "EditInsurance_Locator3_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator3_Code3", locator.ContainsKey("Locator3") ? locator["Locator3"].Code3 : string.Empty, new { @id = "EditInsurance_Locator3_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="EditInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCd</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator4")%>
                <div class="fr">
                    <%= Html.TextBox("Locator4_Code1", locator.ContainsKey("Locator4") ? locator["Locator4"].Code1 : string.Empty, new { @id = "EditInsurance_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator4_Code2", locator.ContainsKey("Locator4") ? locator["Locator4"].Code2 : string.Empty, new { @id = "EditInsurance_Locator4_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator4_Code3", locator.ContainsKey("Locator4") ? locator["Locator4"].Code3 : string.Empty, new { @id = "EditInsurance_Locator4_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsAxxessTheBiller", "true", false, "Claims are electronically submitted to your clearing house through Axxess") %>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_ClearingHouse" class="fl">Clearing House</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" }
                        }, "Value", "Text", Model.ClearingHouse); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "EditInsurance_ClearingHouse" })%>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div id="EditInsurance_EdiInformation" class="clearinghouseediInformation">
            <div class="column">
                <div class="row">
                    <label for="EditInsurance_InterchangeReceiverId" class="fl">Interchange Receiver ID</label>
                    <div class="fr"><%= Html.InterchangeReceiver("InterchangeReceiverId", Model.InterchangeReceiverId, new { @id = "EditInsurance_InterchangeReceiverId" }) %></div>
                </div>
                <div class="row">
                    <label for="EditInsurance_ClearingHouseSubmitterId" class="fl">Clearing House Submitter ID</label>
                    <div class="fr"><%= Html.TextBox("ClearingHouseSubmitterId", Model.ClearingHouseSubmitterId, new { @id = "EditInsurance_ClearingHouseSubmitterId", @maxlength = "40" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="EditInsurance_SubmitterName" class="fl">Submitter Name</label>
                    <div class="fr"><%= Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "EditInsurance_SubmitterName", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="EditInsurance_SubmitterPhone1" class="fl">Submitter Phone</label>
                    <div class="fr"><%= Html.PhoneOrFax("SubmitterPhoneArray", Model.SubmitterPhone, "EditInsurance_SubmitterPhoneArray", string.Empty) %></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditInsurance_AddressLine1", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditInsurance_AddressLine2", @maxlength = "75" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditInsurance_AddressCity", @class = "address-city" }) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditInsurance_AddressStateCode", @class = "address-state short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditInsurance_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" }) %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_ContactPersonFirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "EditInsurance_ContactPersonFirstName", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ContactPersonLastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "EditInsurance_ContactPersonLastName", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ContactEmailAddress" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("ContactEmailAddress", Model.ContactEmailAddress, new { @id = "EditInsurance_ContactEmailAddress", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_PhoneNumberArray1" class="fl">Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneNumberArray", Model.PhoneNumber, "EditInsurance_PhoneNumberArray", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="EditInsurance_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", Model.FaxNumber, "EditInsurance_FaxNumberArray", string.Empty)%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_CurrentBalance" class="fl">Current Balance</label>
                <div class="fr"><%= Html.TextBox("CurrentBalance", Model.CurrentBalance, new { @id = "EditInsurance_CurrentBalance", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_WorkWeekStartDay" class="fl">Work Week Begins</label>
                <div class="fr">
                    <%  var workWeekStartDay = new SelectList(new[] {
                            new SelectListItem { Text = "Sunday", Value = "1" },
                            new SelectListItem { Text = "Monday", Value = "2" }
                        }, "Value", "Text", Convert.ToString(Model.WorkWeekStartDay));%>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "EditInsurance_WorkWeekStartDay" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInsurance_IsAuthReq" class="fl">Visit Authorization Required</label>
                <div class="fr">
                    <%  var IsVisitAuthorizationRequired = new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "true" },
                            new SelectListItem { Text = "No", Value = "false" }
                        }, "Value", "Text", Model.IsVisitAuthorizationRequired);%>
                    <%= Html.DropDownList("IsVisitAuthorizationRequired", IsVisitAuthorizationRequired, new { @id = "EditInsurance_IsAuthReq" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visit Information</legend>
        <div class="column">
    <%  if (Model.IsUserCanEdit) { %>
            <div class="row">
                <label for="EditInsurance_OldInsuranceId" class="fl">Duplicate Rates From</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("OldInsuranceId", "0", (int)AgencyServices.HomeHealth,false, true, "-- Select Insurance --", new { @id = "EditInsurance_OldInsuranceId" })%></div>
            </div>
            <div class="row ac">
                <div class="button"><a onclick="Insurance.VisitInfoReplace('<%=Model.Id %>',$('#EditInsurance_OldInsuranceId').val())">Apply</a></div>
            </div>
    <%  } %>
        </div>
        <div class="column">
    <%  if (Model.IsUserCanAdd) { %>
            <div class="row">
                <div class="fr">
                    <div class="ancillary-button"><a onclick="Insurance.Rate.New('<%= Model.Id %>')">Add Visit Information</a></div>
                </div>
            </div>
    <%  } %>
        </div>
        <div class="wide-column">
            <div class="row">
    <%  var isVisible =(Model.IsUserCanEdit || Model.IsUserCanDelete); %>
    <%  var action = new List<string>(); %>
    <%  var width = 0; %>
    <%  if (Model.IsUserCanEdit) { %>
        <%  action.Add("<a class=\"link\" onclick=\"Insurance.Rate.Edit('" + Model.Id + "','<#=Id#>')\">Edit</a>"); %>
        <%  width += 70; %>
    <%  } %>
    <%  if (Model.IsUserCanDelete) { %>
        <%  action.Add("<a class=\"link\" onclick=\"Insurance.Rate.Delete('" + Model.Id + "','<#=Id#>')\">Delete</a>"); %>
        <%  width += 70; %>
    <%  } %>
    <%  var url = action.ToArray().Join(" "); %>
			    <%= Html.Telerik().Grid<ChargeRate>().HtmlAttributes(new { @class = "position-relative insurance-bill-rates" }).Name("EditInsurance_BillDatas").DataKeys(keys => keys.Add(r => r.Id).RouteKey("Id")).Columns(columns => {
                        columns.Bound(e => e.DisciplineTaskName).Title("Task");
                        columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                        columns.Bound(e => e.RevenueCode).Title("Rev. Code").Width(75);
                        columns.Bound(e => e.Code).Title("HCPCS").Width(60);
                        columns.Bound(e => e.ExpectedRate).Format("{0:$#0.00;-$#0.00}").Title("Expected").Width(70);
                        columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(60).HeaderHtmlAttributes(new { @class = "rate-header" });
                        columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
                        columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(75);
				        columns.Bound(e => e.TimeLimit).ClientTemplate("<#= IsTimeLimit && TimeLimitHour != 0 && TimeLimitMin != 0 && ChargeType != \"1\" ? $.telerik.formatString('{0:H:mm}', TimeLimit) : '' #>").Title("Time Limit").Width(80);
                        columns.Bound(e => e.Id).ClientTemplate(url).Title("Action").Sortable(false).Width(width).Visible(isVisible);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("BillDatas", "Agency", new { InsuranceId = Model.Id })).NoRecordsTemplate("No Rates found.").ClientEvents(c => c
                        .OnDataBinding("U.OnTGridDataBinding")
                        .OnDataBound("U.OnTGridDataBound")
                        .OnError("U.OnTGridError")
			        ).Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <%  if (Model.IsUserCanViewLog) {%>
   	<a class="fr img icon32 log" onclick="Log.LoadInsuranceLog('<%= Model.Id %>')"/>
   	<%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>