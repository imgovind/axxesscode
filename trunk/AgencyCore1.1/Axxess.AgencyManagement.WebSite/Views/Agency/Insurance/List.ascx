﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">List Insurance | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None) { %>
<div class="wrapper main blue">
    <%  if (Model.IsUserCanAdd) { %>
	<div class="fr button"><a onclick="Insurance.New()">New Insurance</a></div>
    <%  } %>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <%  var isVisible = (Model.IsUserCanEdit || Model.IsUserCanDelete); %>
    <%  var action = new List<string>(); %>
    <%  var tm = string.Empty; %>
    <%  var actionMedicare = string.Empty; %>
    <%  var width = 0; %>
    <%  if (Model.IsUserCanEdit)
        { %>
        <%  action.Add("<a class=\"edit link\">Edit</a>"); %>
        <%  tm = "<a class=\"tradtionalmedicare edit link\">Edit Visit Rate</a>"; %>
        <%  width += 100; %>
    <%  } %>
    <%  if (Model.IsUserCanDelete)
        { %>
        <%  action.Add("<a class=\"delete link\">Delete</a>"); %>
        <%  width += 70; %>
    <%  } %>
    <%  var count = action.Count; %>
    <%  var url = action.ToArray().Join(""); %>
    <%  actionMedicare = "<# if (IsTradtionalMedicare) { #> " + tm + "<# } else { #>" + url + "<# } #>"; %>
    <%  Html.Telerik().Grid<InsuranceLean>().Name("List_AgencyInsurance").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
			columns.Bound(c => c.Name).Title("Insurance Name").Sortable(true);
			columns.Bound(c => c.PayerTypeName).Title("Payer Type").Sortable(true);
			columns.Bound(c => c.PayorId).Title("Payor Id").Width(150).Sortable(true);
			columns.Bound(c => c.InvoiceTypeName).Title("Invoice Type").Width(110).Sortable(true);
			columns.Bound(c => c.PhoneNumber).Title("Phone").Width(120).Sortable(false);
			columns.Bound(c => c.ContactPerson).Title("Contact Person").Width(180).Sortable(false).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
            columns.Bound(c => c.Id).ClientTemplate(actionMedicare).Title("Action").Sortable(false).Width(width).Visible(isVisible);
            columns.Bound(c => c.Id).HtmlAttributes(new { @class = "id" }).Hidden(true);
		}).NoRecordsTemplate("No Insurances found.").DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Insurance").OperationMode(GridOperationMode.Client)).ClientEvents(c => c
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnRowDataBound("Insurance.OnRowDataBound")
            .OnError("U.OnTGridError")
        ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Render(); %>
</div>
<%  } %>