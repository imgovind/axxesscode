﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SaveBillData", "Agency", FormMethod.Post, new { @id = "newInsuranceBillData", @class = "mainform" })) { %>
    <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = "New_BillData_InsuranceId" })%>
    <%  Html.RenderPartial("~/Views/Rate/NewContent.ascx", Model, new ViewDataDictionary { { "IdPrefix", "New_BillData" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
		<li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>

