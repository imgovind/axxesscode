﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateBillData", "Agency", FormMethod.Post, new { @id = "editInsuranceBillData", @class = "mainform" })) { %>
    <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = "Edit_BillData_InsuranceId" })%>
    <%= Html.Hidden("Id", Model.Id) %>
    <%  Html.RenderPartial("~/Views/Rate/EditContent.ascx", Model , new ViewDataDictionary { { "IdPrefix", "Edit_BillData" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
		<li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>