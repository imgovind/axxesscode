﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Insurance/Payor | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Insurance", FormMethod.Post, new { @id = "newInsuranceForm", @class = "mainform" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_Name" class="fl">Insurance/Payor Name</label>
                <div class="fr"><%= Html.TextBox("Name", string.Empty, new { @id = "NewInsurance_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_PayorType" class="fl">Payor Type</label>
                <div class="fr"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", string.Empty, new { @id = "NewInsurance_PayorType", @class = "required not-zero" })%></div>
            </div>
            <div id="NewInsurance_IsUsingPatientRatesRow" class="row usingpatientratesrow">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsUsingPatientRates", "true", false, "This insurance will use the patient&#8217;s visit rates") %>
                </ul>
            </div>
            <div class="row">
                <label for="NewInsurance_InvoiceType" class="fl">Invoice Type</label>
                <div class="fr">
                    <%  var invoiceType = new SelectList(new[] {
                            new SelectListItem { Text = "UB-04", Value = "1" },
                            new SelectListItem { Text = "HCFA 1500", Value = "2" },
                            new SelectListItem { Text = "Invoice", Value = "3" }
                        }, "Value", "Text", "1"); %>
                    <%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "NewInsurance_InvoiceType" }) %>
                </div>
            </div>
            <div class="row">
                <label for="NewInsurance_BillType" class="fl">Bill Type</label>
                <div class="fr">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Institutional", Value = "institutional" },
                            new SelectListItem { Text = "Professional", Value = "professional" }
                        }, "Value", "Text", "institutional"); %>
                    <%= Html.DropDownList("BillType", billType, new { @id = "NewInsurance_BillType" }) %>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("HasContractWithAgency", "true", false, "Agency has a contract with this insurance")%>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_PayorId" class="fl">Payor ID</label>
                <div class="fr"><%= Html.TextBox("PayorId", string.Empty, new { @id = "NewInsurance_PayorId", @class = "required", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ProviderId" class="fl">Provider ID/Code</label>
                <div class="fr"><%= Html.TextBox("ProviderId", string.Empty, new { @id = "NewInsurance_ProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_OtherProviderId" class="fl">Other Provider ID</label>
                <div class="fr"><%= Html.TextBox("OtherProviderId", string.Empty, new { @id = "NewInsurance_OtherProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ProviderSubscriberId" class="fl">Provider Subscriber ID</label>
                <div class="fr"><%= Html.TextBox("ProviderSubscriberId", string.Empty, new { @id = "NewInsurance_ProviderSubscriberId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_SubmitterId" class="fl">Submitter ID</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", string.Empty, new { @id = "NewInsurance_SubmitterId", @maxlength = "40" })%></div>
            </div>
        </div>
        <div class="wide-column ub04locators">
            <div class="row narrower">
                <label for="NewInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCa</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="fr">
                    <%= Html.TextBox("Locator1_Code1", string.Empty, new { @id = "NewInsurance_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator1_Code2", string.Empty, new { @id = "NewInsurance_Locator1_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator1_Code3", string.Empty, new { @id = "NewInsurance_Locator1_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="NewInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCb</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator2")%>
                <div class="fr">
                    <%= Html.TextBox("Locator2_Code1", string.Empty, new { @id = "NewInsurance_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator2_Code2", string.Empty, new { @id = "NewInsurance_Locator2_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator2_Code3", string.Empty, new { @id = "NewInsurance_Locator2_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="NewInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCc</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator3")%>
                <div class="fr">
                    <%= Html.TextBox("Locator3_Code1", string.Empty, new { @id = "NewInsurance_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator3_Code2", string.Empty, new { @id = "NewInsurance_Locator3_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator3_Code3", string.Empty, new { @id = "NewInsurance_Locator3_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="NewInsurance_Ub04Locator81cca" class="fl">UB04 Locator 81CCd</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator4")%>
                <div class="fr">
                    <%= Html.TextBox("Locator4_Code1", string.Empty, new { @id = "NewInsurance_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator4_Code2", string.Empty, new { @id = "NewInsurance_Locator4_Code2", @class = "short", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator4_Code3", string.Empty, new { @id = "NewInsurance_Locator4_Code3", @class = "short", @maxlength = "12" })%>
                </div>
            </div>
       </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("IsAxxessTheBiller", "true", false, "Claims are electronically submitted to your clearing house through Axxess")%>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_ClearingHouse" class="fl">Clearing House</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                        }, "Value", "Text", "0"); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "NewInsurance_ClearingHouse" }) %>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div id="NewInsurance_EdiInformation" class="clearinghouseediInformation">
            <div class="column">
                <div class="row">
                    <label for="NewInsurance_InterchangeReceiverId" class="fl">Interchange Receiver ID</label>
                    <div class="fr"><%= Html.InterchangeReceiver("InterchangeReceiverId", "28", new { @id = "NewInsurance_InterchangeReceiverId" }) %></div>
                </div>
                <div class="row">
                    <label for="NewInsurance_ClearingHouseSubmitterId" class="fl">Clearing House Submitter ID</label>
                    <div class="fr"><%= Html.TextBox("ClearingHouseSubmitterId", string.Empty, new { @id = "NewInsurance_ClearingHouseSubmitterId", @maxlength = "40" }) %></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="NewInsurance_SubmitterName" class="fl">Submitter Name</label>
                    <div class="fr"><%= Html.TextBox("SubmitterName", string.Empty, new { @id = "NewInsurance_SubmitterName", @maxlength = "100" }) %></div>
                </div>
                <div class="row">
                    <label for="NewInsurance_SubmitterPhone1" class="fl">Submitter Phone</label>
                    <div class="fr"><%= Html.PhoneOrFax("SubmitterPhoneArray", string.Empty, "NewInsurance_SubmitterPhone", string.Empty) %></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewInsurance_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewInsurance_AddressLine2", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewInsurance_AddressCity", @maxlength = "75", @class = "address-city" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", string.Empty, new { @id = "NewInsurance_AddressStateCode", @class = "address-state short" })%>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewInsurance_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_ContactPersonFirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", string.Empty, new { @id = "NewInsurance_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ContactPersonLastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", string.Empty, new { @id = "NewInsurance_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ContactEmailAddress" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("ContactEmailAddress", string.Empty, new { @id = "NewInsurance_ContactEmailAddress", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_PhoneNumberArray1" class="fl">Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneNumberArray", string.Empty, "NewInsurance_PhoneNumberArray", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="NewInsurance_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", string.Empty, "NewInsurance_FaxNumberArray", string.Empty) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_CurrentBalance" class="fl">Current Balance</label>
                <div class="fr"><%= Html.TextBox("CurrentBalance", string.Empty, new { @id = "NewInsurance_CurrentBalance" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_WorkWeekStartDay" class="fl">Work Week Begins</label>
                <div class="fr">
                    <%  var workWeekStartDay = new SelectList(new[] {
                            new SelectListItem { Text = "Sunday", Value = "1" },
                            new SelectListItem { Text = "Monday", Value = "2" }
                        }, "Value", "Text"); %>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "NewInsurance_WorkWeekStartDay" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewInsurance_VisitAuthReq" class="fl">Visit Authorization Required</label>
                <div class="fr">
                    <%  var VisitAuthReq = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text"); %>
                    <%= Html.DropDownList("IsVisitAuthorizationRequired", VisitAuthReq, new { @id = "NewInsurance_VisitAuthReq" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Load Visit Information from Existing Insurance</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_OldInsuranceId" class="fl">Choose existing Insurance</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("OldInsuranceId", "0", (int)AgencyServices.HomeHealth, false, true, "-- Select Insurance --", new { @id = "NewInsurance_OldInsuranceId" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>
