﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <fieldset>
        <legend>After-Hours Support</legend>
        <div class="column">
            <div class="row">
                <label class="fl">Email</label>
                <label class="fr"><a href="mailto:support@axxessconsult.com" title="E-mail Support">support@axxessconsult.com</a></label>
            </div>
            <div class="row">
                <label class="fl">Facebook</label>
                <label class="fr"><a href="http://www.facebook.com/axxessusers" title="Facebook Community Support" target="_blank">www.facebook.com/axxessusers</a></label>
            </div>
            <div class="row">
                <label class="fl">Phone</label>
                <label class="fr">(214) 447-0355</label>
            </div>
            <div class="row">
                <label class="fl">7pm – 10pm CST Monday through Friday</label>
            </div>
            <div class="row">
                <label class="fl">9am – 1pm CST on Weekends </label>
            </div>
        </div>
    </fieldset>
</div>