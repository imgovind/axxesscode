﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Hospital | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Hospital", FormMethod.Post, new { @id = "newHospitalForm" })) { %>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewHospital_Name" class="fl">Hospital Name</label>
                <div class="fr"><%= Html.TextBox("Name", string.Empty, new { @id = "NewHospital_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewHospital_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewHospital_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewHospital_AddressCity", @class = "address-city required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressZipCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", string.Empty, new { @id = "NewHospital_AddressStateCode", @class = "address-state required not-zero short" })%>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewHospital_AddressZipCode", @class = "numeric required zip shorter", @maxlength = "9" })%>
                </div>
            </div>
         </div>   
        <div class="column">   
            <div class="row">
                <label for="NewHospital_ContactPersonFirstName" class="fl">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", string.Empty, new { @id = "NewHospital_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_ContactPersonLastName" class="fl">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", string.Empty, new { @id = "NewHospital_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_EmailAddress" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewHospital_EmailAddress", @class = "email", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_PhoneArray1" class="fl">Primary Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneArray", string.Empty, "NewHospital_PhoneArray", "required") %></div>
            </div>
            <div class="row">
                <label for="NewHospital_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", string.Empty, "NewHospital_FaxNumberArray", string.Empty) %></div>
            </div> 
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment">Comment</label>
                <div class="ac"><%= Html.TextArea("Comment", string.Empty)%></div>
            </div>
        </div>
    </fieldset> 
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>