﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SingleServiceGridViewData>" %>
<span class="wintitle">List Hospital | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <ul class="fr buttons">
<%  if (Model.IsUserCanAdd) { %>
	    <li><a onclick="Hospital.New()">New Hospital</a></li><div class="clr"></div>
<%  } %>
<%  if (Model.IsUserCanExport) { %>
        <li><a url="Export/Hospitals" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyHospitalGridRow>().Name("List_AgencyHospital").HtmlAttributes(new {  @class = "grid-container aggregated" }).Columns(columns => {
		    columns.Bound(c => c.HospitalName).Width(150).Sortable(true);
			columns.Bound(c => c.ContactPerson).Sortable(true);
			columns.Bound(c => c.Address).Sortable(false);
			columns.Bound(c => c.Phone).Width(120).Sortable(false).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
			columns.Bound(c => c.FaxNumber).Width(120).Sortable(false);
			columns.Bound(c => c.Email).ClientTemplate("<a href='mailto:<#= Email #>'><#= Email #></a>").Sortable(true);
            columns.Bound(c => c.Id).ClientTemplate("<# if ('" + Model.IsUserCanEdit.ToString() + "' == 'True') { #><a class=\"link\" onclick=\"Hospital.Edit('<#= Id #>')\">Edit</a><# } #><# if ('" + Model.IsUserCanDelete.ToString() + "' == 'True') { #><a class=\"link\" onclick=\"Hospital.Delete('<#=Id#>')\">Delete</a><# } #>").Title("Action").Sortable(false).Width((Model.IsUserCanEdit.ToInteger() + Model.IsUserCanDelete.ToInteger()) * 50).Visible((Model.IsUserCanDelete || Model.IsUserCanEdit));
		}).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Hospital").OperationMode(GridOperationMode.Client)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).NoRecordsTemplate("No Hospitals found.").ClientEvents(events => events
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ) %>
</div>