﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyHospital>" %>
<span class="wintitle">Edit Hospital | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Hospital", FormMethod.Post, new { @id = "editHospitalForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditHospital_Id" })%>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditHospital_Name" class="fl">Hospital Name:</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "EditHospital_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"> <%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditHospital_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"> <%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditHospital_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressCity" class="fl">City</label>
                <div class="fr"> <%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditHospital_AddressCity", @class = "address-city required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditHospital_AddressStateCode", @class = "address-state required short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditHospital_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "9" })%>
                </div>
            </div>
        </div>   
        <div class="column">
            <div class="row">
                <label for="EditHospital_ContactPersonFirstName" class="fl">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "EditHospital_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_ContactPersonLastName" class="fl">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "EditHospital_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_EmailAddress" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditHospital_EmailAddress", @class = "email", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_PhoneArray1" class="fl">Primary Phone</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("PhoneArray", Model.Phone, "EditHospital_PhoneArray", "required")%>
                </div>
            </div>
            <div class="row">
                <label for="EditHospital_FaxNumberArray1" class="fl">Fax Number</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("FaxNumberArray", Model.FaxNumber, "EditHospital_FaxNumberArray", "")%>
                </div>
            </div> 
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="EditHospital_Comments">Comment</label>
                <div class="ac"><%= Html.TextArea("Comment", Model.Comment, new { @id = "EditHospital_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog){ %>
    	<a class="fr img icon32 log" onclick="Log.LoadHospitalLog('<%= Model.Id %>')"/>
    <% } %>
    <ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>