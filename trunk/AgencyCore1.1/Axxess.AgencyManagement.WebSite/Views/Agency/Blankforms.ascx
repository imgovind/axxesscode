﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <fieldset>
        <legend>OASIS Forms</legend>
<%  if (Current.AcessibleServices.Has(AgencyServices.HomeHealth)) { %>
        <div class="column">
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','Nursing')">OASIS-C Start of Care</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.StartOfCare %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','Nursing')">OASIS-C Resumption of Care</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.ResumptionOfCare %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','Nursing')">OASIS-C Recertification</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Recertification %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','Nursing')">OASIS-C Follow-Up</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.FollowUp %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Transfer %>','Nursing')">OASIS-C Transfer Not Discharged</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Transfer %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Transfer %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferDischarge %>','Nursing')">OASIS-C Transfer and Discharged</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.TransferDischarge %>','PT')">(PT)</a>
                </span>
            </div>
        </div>
<%  } %>
        <div class="column">
<%  if (Current.AcessibleServices.Has(AgencyServices.HomeHealth)) { %>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Death %>','Nursing')">OASIS-C Death at Home</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Death %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Death %>','OT')">(OT)</a>
                </span>
            </div>
            <div class="row">
                <span>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Discharge %>','Nursing')">OASIS-C Discharge from Agency</a>
                    &#160;&#8212;&#160;
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Discharge %>','PT')">(PT)</a>
                    <a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.Discharge %>','OT')">(OT)</a>
                </span>
            </div>
<%  } %>
            <div class="row"><a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOASISStartOfCare %>','Nursing')">Non-OASIS Start of Care</a></div>
            <div class="row"><a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOASISRecertification %>','Nursing')">Non-OASIS Recertification</a></div>
            <div class="row"><a class="link" onclick="Oasis.Assessment.BlankPrint('<%= (int)AssessmentType.NonOASISDischarge %>','Nursing')">Non-OASIS Discharge from Agency</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="U.GetAttachment('FaceToFaceEncounter/Blank')">Face to Face Encounter</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="U.GetAttachment('Order/Blank')">Physician Order</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.TransferSummary %>">Transfer Summary</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.CoordinationOfCare %>">Coordination of Care</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SixtyDaySummary %>">60 Day Summary</a></div>
            <div class="row"><a class="link" onclick="U.GetAttachment('MissedVisit/Blank')">Missed Visit Form</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SNVPsychNurse %>">Skilled Nurse Psych Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SNPediatricVisit %>">Skilled Nurse Pediatric Visit</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.LVNSupervisoryVisit %>">LVN Supervisory Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.DischargeSummary %>">SN Discharge Summary</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SkilledNurseVisit %>">Skilled Nurse Visit</a></div>
            <div class="row"><a class="link" onclick="U.GetAttachment('Note/WoundCareBlank')">Wound Care</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SNPsychAssessment %>">Skilled Nurse Psych Assessment</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.SNPediatricAssessment %>">Skilled Nurse Pediatric Assessment</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Home Health Aide Forms</legend>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.HHAideCarePlan %>">HHA Care Plan</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.HHAideVisit %>">HHA Visit Note</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.HHAideSupervisoryVisit %>">HHA Supervisory Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.HomeMakerNote %>">Home Maker Note</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PASCarePlan %>">PAS Care Plan</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PASVisit %>">PAS Note</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PASTravel %>">PAS Travel Note</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.UAPWoundCareVisit %>">UAP Wound Care Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.UAPInsulinPrepAdminVisit %>">UAP Insulin Prep/Admin Visit</a></div>
        </div>
    </fieldset>
    <fieldset>    
        <legend>Medical Social Work Forms</legend>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.MSWEvaluationAssessment %>">MSW Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.MSWProgressNote %>">MSW Progress Note</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.MSWVisit %>">MSW Visit</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.MSWAssessment %>">MSW Assessment</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.MSWDischarge %>">MSW Discharge</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.DriverOrTransportationNote %>">Driver/Transportation Log</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Therapy Forms</legend>
        <div class="column"> 
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTDischarge %>">PT Discharge</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTEvaluation %>">PT Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTReEvaluation %>">PT Re-Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTMaintenance %>">PT Maintenance</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTVisit %>">PT Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTReassessment %>">PT Reassessment</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTAVisit %>">PTA Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.PTDischargeSummary %>">PT Discharge Summary</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTEvaluation %>">OT Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTReEvaluation %>">OT Re-Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTDischarge %>">OT Discharge</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTMaintenance %>">OT Maintenance</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTVisit %>">OT Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.OTReassessment %>">OT Reassessment</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.COTAVisit %>">COTA Visit</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STEvaluation %>">ST Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STReEvaluation %>">ST Re-Evaluation</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STReassessment %>">ST Reassessment</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STMaintenance %>">ST Maintenance</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STDischarge %>">ST Discharge</a></div>
            <div class="row"><a class="note-blank link" notetype="<%=(int)DisciplineTasks.STVisit %>">ST Visit</a></div>
        </div>
    </fieldset>
</div>