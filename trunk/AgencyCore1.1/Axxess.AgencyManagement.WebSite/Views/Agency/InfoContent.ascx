﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_AgencyInfo_Id" })%>
<div class="column">
    <div class="row">
        <label for="Edit_AgencyInfo_Name" class="fl">Name</label>
        <div class="fr"><%=Html.TextBox("Location.Name", Model.Name, new { @id = "Edit_AgencyInfo_Name", @class = "required", @maxlength = "50" })%></div>
    </div>
    <div class="row">
        <label for="Edit_AgencyInfo_AddressLine1" class="fl">Address Line 1</label>
        <div class="fr"><%= Html.TextBox("Location.AddressLine1", Model.AddressLine1.IsNotNullOrEmpty() ? Model.AddressLine1 : string.Empty, new { @id = "Edit_AgencyInfo_AddressLine1", @maxlength = "75" })%></div>
    </div>
    <div class="row">
        <label for="Edit_AgencyInfo_AddressLine2" class="fl">Address Line 2</label>
        <div class="fr"><%= Html.TextBox("Location.AddressLine2", Model.AddressLine2.IsNotNullOrEmpty() ? Model.AddressLine2 : string.Empty, new { @id = "Edit_AgencyInfo_AddressLine2", @maxlength = "75" })%></div>
    </div>
    <div class="row">
        <label for="Edit_AgencyInfo_AddressCity" class="fl">City</label>
        <div class="fr"><%= Html.TextBox("Location.AddressCity", Model.AddressCity.IsNotNullOrEmpty() ? Model.AddressCity : string.Empty, new { @id = "Edit_AgencyInfo_AddressCity", @maxlength = "75", @class = "address-city" })%></div>
    </div>
    <div class="row">
        <label for="Edit_AgencyInfo_AddressStateCode" class="fl">State, Zip</label>
        <div class="fr">
            <%= Html.States("Location.AddressStateCode", Model.AddressStateCode.IsNotNullOrEmpty() ? Model.AddressStateCode : string.Empty, new { @id = "Edit_AgencyInfo_AddressStateCode", @class = "address-state short" })%>
            <%= Html.TextBox("Location.AddressZipCode", Model.AddressZipCode.IsNotNullOrEmpty() ? Model.AddressZipCode : string.Empty, new { @id = "Edit_AgencyInfo_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
        </div>
    </div>
</div>
<div class="column">
    <div class="row">
	    <label for="Edit_Location_PhoneArray1" class="fl">Primary Phone</label>
		<div class="fr"><%= Html.PhoneOrFax("Location.PhoneArray", Model.PhoneWork, "Edit_Location_PhoneArray", "")%></div>
    </div>
    <div class="row">
	    <label for="Edit_Location_FaxNumberArray1" class="fl">Fax Number</label>
	    <div class="fr"><%= Html.PhoneOrFax("Location.FaxNumberArray", Model.FaxNumber, "Edit_Location_FaxNumberArray", "")%></div>
    </div>
</div>
