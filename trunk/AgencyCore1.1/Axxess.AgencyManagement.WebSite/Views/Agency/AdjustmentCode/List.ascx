﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">List Adjustment Codes | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
	<ul class="fr buttons">
<%  if (!Current.IsAgencyFrozen && Model.NewPermissions != AgencyServices.None) { %>
        <li><a onclick="UserInterface.ShowNewAdjustmentCode()">New Adjustment Code</a></li><br />
<%  } %>
<%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a url="Export/AdjustmentCodes" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
        <div class="filter grid-search"></div>
    </fieldset>
<%  var canEdit = Model.EditPermissions != AgencyServices.None; var canDelete = Model.DeletePermissions != AgencyServices.None; %>
<%= Html.Telerik().Grid<AgencyAdjustmentCode>().Name("List_AdjustmentCode").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
        columns.Bound(t => t.Code).Width(200);
        columns.Bound(t => t.Description);
        columns.Bound(t => t.CreatedDateString).Title("Created").Width(120);
        columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
		columns.Bound(t => t.Id).ClientTemplate("<# if (" + canEdit.ToString().ToLower() + ") #><a class=\"link\" onclick=\"UserInterface.ShowEditAdjustmentCode('<#= Id #>')\">Edit</a><# if (" + canDelete.ToString().ToLower() + ") #><a onclick=\"AdjustmentCode.Delete('<#= Id #>')\" class=\"link deleteAdjustmentCode\">Delete</a>").Title("Action").Sortable(false).Width(50 * (canEdit.ToInteger() + canDelete.ToInteger())).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "AdjustmentCode").OperationMode(GridOperationMode.Client)).Sortable().NoRecordsTemplate("No Adjustment Codes found.").ClientEvents(events => events
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnError("U.OnTGridError")
    ).Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>