﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyAdjustmentCode>" %>
<span class="wintitle">Edit Adjustment Code | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "AdjustmentCode", FormMethod.Post, new { @id = "editAdjustmentCodeForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_AdjustmentCode_Id" }) %>
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_AdjustmentCode_Code" class="fl">Code</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "Edit_AdjustmentCode_Code", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_AdjustmentCode_Description" class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Description, new { @id = "Edit_AdjustmentCode_Description", @class = "required", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.IsUserCanViewLog) { %>
	<a class="fr img icon32 log" onclick="Log.LoadAdjustmentCodeLog('<%= Model.Id %>')"></a>
	<%  } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>