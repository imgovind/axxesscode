﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Adjustment Code | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "AdjustmentCode", FormMethod.Post, new { @id = "newAdjustmentCodeForm" })) { %>
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="New_AdjustmentCode_Code" class="fl">Code</label>
                <div class="fr"><%= Html.TextBox("Code", string.Empty, new { @id = "New_AdjustmentCode_Code", @class = "required", @maxlength = "100"})%></div>
            </div>
            <div class="row">
                <label for="New_AdjustmentCode_Description" class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", string.Empty, new { @id = "New_AdjustmentCode_Description", @class = "required", @maxlength = "100" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>