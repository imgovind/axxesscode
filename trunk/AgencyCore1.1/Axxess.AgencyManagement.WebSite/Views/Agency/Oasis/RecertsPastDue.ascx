﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Past Due Recerts | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None) { %>
    <%  string pageName = "AgencyPastDueRecerts"; %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %>
    <ul class="fr buttons ac">
    <%  if (Model.ExportPermissions.Has(Model.Service)) { %>
    	<li><a url="Export/RecertsPastDue" class="export">Excel Export</a></li>
    	<div class="clr"></div>
    <%  } %>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pageName %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.Service, new { @id = pageName + "_BranchId", @class = "location" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_InsuranceId">Insurance</label>
            <%= Html.Insurances("InsuranceId", Model.SubId.ToString(), (int)Model.Service, false, true, "All", new { @id = pageName + "_InsuranceId", @class = "insurance " + AgencyServices.HomeHealth.ToString().ToLowerCase() })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate">Due Date From </label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), true, new { id = pageName + "_StartDate", @class = "short" })%>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer">
        <%= Html.Telerik().Grid<RecertEvent>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
                columns.Bound(r => r.PatientIdNumber).Title("MRN").Width(120).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
    			columns.Bound(r => r.PatientName).Width(160).Title("Patient");
                columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Width(120);
                columns.Bound(r => r.StatusName).Title("Status").Width(120);
                columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(70);
                columns.Bound(r => r.DateDifference).Title("Past Dates").Width(60);
            }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("RecertsPastDue", "Schedule", new { BranchId = Model.Id, InsuranceId = Model.SubId , StartDate = DateTime.Now.AddDays(-60) })).NoRecordsTemplate("No Past Due Recertifications found.").Sortable().ClientEvents(c => c
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("U.OnTGridDataBound")
                .OnError("U.OnTGridError")
            ).NoRecordsTemplate("No Recertifications Past Due found.").Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>   
<%  } %>