﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Upcoming Recerts | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None) { %>
    <%  string pageName = "UpcomingRecerts"; %>
<div class="wrapper main blue">
	<ul class="buttons fr ac">
	<%  if (Model.ExportPermissions.Has(Model.Service)) { %>
	    <li><a url="Export/RecertsUpcoming" class="export">Excel Export</a></li>
		<div class="clr"></div>
	<%  } %>
	    <li><a class="grid-refresh">Refresh</a></li>
	</ul>
	<fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pageName %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.Service, new { @id = pageName + "_BranchId", @class = "location" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_InsuranceId">Insurance</label>
            <%= Html.Insurances("InsuranceId", Model.SubId.ToString(), (int)Model.Service, false, true, "All", new { @id = pageName + "_InsuranceId", @class = "insurance " + AgencyServices.HomeHealth.ToString().ToLowerCase() })%>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<RecertEvent>().Name("UpcomingRecerts_Grid").HtmlAttributes(new { @class = "grid-container  aggregated", @style = "top:85px;" }).Columns(columns => {
            columns.Bound(r => r.PatientIdNumber).Title("MRN").Width(120);
			columns.Bound(r => r.PatientName).Width(160).Title("Patient").Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
            columns.Bound(r => r.AssignedTo).Width(120).Title("Employee Responsible");
            columns.Bound(r => r.StatusName).Width(100).Title("Status");
            columns.Bound(r => r.TargetDate).Width(60).Format("{0:MM/dd/yyyy}").Title("Due Date");
        }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("RecertsUpcoming", "Schedule", new { BranchId = Model.Id, InsuranceId = Model.SubId })).NoRecordsTemplate("No Upcoming Recertifications found.").ClientEvents(c => c
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Recertifications Upcoming found.").Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<%  } %>