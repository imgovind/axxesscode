﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<span class="wintitle">Manage Company Information | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateAgency", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Agency_CompanyName">Company Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Agency_CompanyName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_TaxId">Tax Id</label>
                <div class="fr"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_TaxIdType">Tax Id Type</label>
                <div class="fr">
                    <%  var taxIdTypes = new SelectList(new[] {
                            new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" },
                            new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" },
                            new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" }
                        }, "Value", "Text", Model.TaxIdType); %>
                    <%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "Edit_Agency_TaxIdType" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Agency_CahpsVendor">CAHPS Vendor</label>
                <div class="fr"><%= Html.CahpsVendors("CahpsVendor", Model.CahpsVendor.ToString(), new { @id = "Edit_Agency_CahpsVendor" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Agency_NationalProviderNo">National Provider Number</label>
                <div class="fr"><%= Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "short" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_MedicareProviderNo">Medicare Provider Number</label>
                <div class="fr"><%= Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "short" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_MedicaidProviderNo">Medicaid Provider Number</label>
                <div class="fr"><%= Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "short" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code</label>
                <div class="fr"><%= Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "20" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Contact Person</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Agency_ContactPersonFirstName">First Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "required names" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_ContactPersonLastName">Last Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Agency_ContactPersonLastName", @class = "required names", @maxlength = "30" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Agency_ContactPersonEmail">E-mail</label>
                <div class="fr"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = "Edit_Agency_ContactPersonEmail", @class = "required ", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Agency_ContactPhoneArray1">Phone</label>
				<div class="fr"><%= Html.PhoneOrFax("ContactPhoneArray", Model.ContactPersonPhone, "Edit_Agency_ContactPhoneArray", string.Empty)%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location</legend>
        <div class="wide-column">
            <div class="row narrower">
                <label for="Edit_AgencyInfo_LocationId">Agency Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("AgencyLocationId", Model.MainLocation != null ? Model.MainLocation.Id.ToString() : string.Empty, 0, new { @id = "Edit_AgencyInfo_LocationId", @class = "branch-location required" })%></div>
            </div>
            <div class="row" id="Edit_AgencyInfo_Container"><% Html.RenderPartial("~/Views/Agency/InfoContent.ascx", Model.MainLocation ?? new AgencyLocation()); %></div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>