﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianListViewData>" %>
<%  var pagename = "PhysicianList"; %>
<span class="wintitle">Physician List |	<%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <ul class="fr buttons ac">
<%  if (Model.IsUserCanAdd) { %>
		<li><a class="new">New Physician</a></li>
		<div class="clr"></div>
<%  } %>
<%  if (Model.IsUserCanExport) { %>
        <li><a url="Export/Physicians" class="export">Excel Export</a></li>
<%  } %>
    </ul>
	<fieldset class="grid-controls ac">
	    <div class="button fr"><a url="Physician/ListContent" class="grid-refresh">Refresh</a></div>
		<div class="filter grid-search"></div>
		<%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
	</fieldset>
	<div id="PhysicianListGridContainer"><% Html.RenderPartial("ListContent", Model); %></div>
</div>