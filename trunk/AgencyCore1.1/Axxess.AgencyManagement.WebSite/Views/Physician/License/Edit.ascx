﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicainLicense>" %>
<%  using (Html.BeginForm("UpdateLicense", "Physician", FormMethod.Post, new { @id = "editPhysicianLicenseForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_PhysicianLicense_Id" })%>
    <%= Html.Hidden("PhysicianId", Model.PhysicianId, new { @id = "Edit_PhysicianLicense_PhysicianId" })%>
<fieldset>
    <legend>Edit License</legend>
    <div class="widecolumn">
        <div class="row">
            <label for="Edit_PhysicianLicense_LicenseNumber" class="strong">License Number</label>
            <div class="fr"><%= Html.TextBox("LicenseNumber", Model.LicenseNumber, new { @id = "Edit_PhysicianLicense_LicenseNumber", @class = "required", @maxlength = "25" })%></div>
        </div>
         <div class="row">
            <label for="Edit_PhysicianLicense_State" class="strong">State</label>
            <div class="fr"><%= Html.States("State", Model.State, new { @id = "Edit_PhysicianLicense_State", @class = "required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="Edit_PhysicianLicense_InitDate" class="strong">Issue Date</label>
            <div class="fr"><%= Html.DatePicker("InitiationDate", Model.InitiationDate, true, new { @id = "Edit_PhysicianLicense_InitDate", @title = "Issue Date" })%></div>
        </div>
        <div class="row">
            <label for="Edit_PhysicianLicense_ExpDate" class="strong">Expiration Date</label>
            <div class="fr"><%= Html.DatePicker("ExpirationDate", Model.ExpirationDate, true, new { @id = "Edit_PhysicianLicense_ExpDate" }) %></div>
        </div>
    </div>
</fieldset>
<ul class="buttons ac">
    <li><a class="save close">Save</a></li>
    <li><a class="close">Close</a></li>
</ul>
<%  } %>
