﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%  using (Html.BeginForm("AddLicense", "Physician", FormMethod.Post, new { @id = "newPhysicianLicenseForm" })) { %>
    <%= Html.Hidden("PhysicianId", Model, new { @id = "New_PhysicianLicense_Id", @default = Model })%>
<fieldset>
    <legend>New License</legend>
    <div class="widecolumn">
        <div class="row">
            <label for="New_PhysicianLicense_LicenseNumber" class="strong">License Number</label>
            <div class="fr"><%= Html.TextBox("LicenseNumber", "", new { @id = "New_PhysicianLicense_LicenseNumber", @class = "required", @maxlength = "25" })%></div>
        </div>
         <div class="row">
            <label for="New_PhysicianLicense_State" class="strong">State</label>
            <div class="fr"><%= Html.States("State", "", new { @id = "New_PhysicianLicense_State", @class = "required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="New_PhysicianLicense_InitDate" class="strong">Issue Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="InitiationDate" id="New_PhysicianLicense_InitDate" /></div>
        </div>
        <div class="row">
            <label for="New_PhysicianLicense_ExpDate" class="strong">Expiration Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="ExpirationDate" id="New_PhysicianLicense_ExpDate" /></div>
        </div>
    </div>
</fieldset>
<ul class="buttons ac">
    <li><a class="save close">Save</a></li>
    <li><a class="save clear">Save &#38; Add Another</a></li>
    <li><a class="close">Close</a></li>
</ul>
<%  } %>
