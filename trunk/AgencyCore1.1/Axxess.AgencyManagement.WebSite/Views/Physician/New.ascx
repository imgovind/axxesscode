﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Physician | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Physician", FormMethod.Post, new { @id = "newPhysicianForm" })) { %>
    <fieldset>
        <legend>Search Physician</legend>
        <div class="wide-column">
            <div class="row narrowest">
                <label for="NewPhysician_NpiSearch" class="fl">NPI Number</label>
                <div class="fr"><input type="text" id="NewPhysician_NpiSearch" name="NewPhysician_NpiSearch" maxlength="10" class="numeric" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewPhysician_FirstName", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_MiddleIntial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleName", string.Empty, new { @id = "NewPhysician_MiddleIntial", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewPhysician_LastName", @class = "required", @maxlength = "50" })%></div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="NewPhysician_Credentials" class="fl">Credentials</label>
                <div class="fr"><%= Html.TextBox("Credentials", string.Empty, new { @id = "NewPhysician_Credentials", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_NpiNumber" class="fl">NPI No</label>
                <div class="fr"><%= Html.TextBox("NPI", string.Empty, new { @id = "NewPhysician_NpiNumber", @class = "numeric required", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label class="fl">PECOS Verification</label>
                <div class="fr"><label id="NewPhysician_PecosCheck">Not Checked</label></div>
                
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewPhysician_AddressLine1", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewPhysician_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewPhysician_AddressCity", @class = "address-city required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", "", new { @id = "NewPhysician_AddressStateCode", @class = "address-state required not-zero short" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewPhysician_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "5" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_Phone1" class="fl">Primary Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneWorkArray", string.Empty, "NewPhysician_Phone", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AltPhone1" class="fl">Alternate Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("PhoneAltArray", string.Empty, "NewPhysician_AltPhone", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="NewPhysician_Fax1" class="fl">Fax</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxNumberArray", string.Empty, "ewPhysician_Fax", string.Empty) %></div>
            </div>
            <div class="row">
                <label for="NewPhysician_Email" class="fl">E-mail</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewPhysician_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption("PhysicianAccess", "true", false, "Grant Physician Access<br /><em>(E-mail Address Required)</em>") %>
                </ul>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>