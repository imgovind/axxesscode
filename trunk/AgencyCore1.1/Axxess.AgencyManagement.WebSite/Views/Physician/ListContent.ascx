﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianListViewData>" %>
<% var isCanUserEdit = Model.IsUserCanEdit; %>
<% var isCanUserDelete = Model.IsUserCanDelete; %>
<%= Html.Filter("SortParams", string.Format("{0}-{1}",Model.SortColumn,Model.SortDirection), new { @class = "sort-parameters" })%>
<%= Html.Telerik()
        .Grid(Model.List)
        .Name("PhysicianList_Grid")
        .HtmlAttributes(new { @class = "aggregated" }) 
        .Columns(columns => {
			columns.Bound(p => p.NPI).Width(18);
			columns.Bound(p => p.Name).Width(30);
			columns.Bound(p => p.Email).Template(p => "<a href='mailto:" + p.Email + "'>" + p.Email + "</a>").Width(35);
			columns.Bound(p => p.Address).Width(60).Sortable(false)
				.Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
			columns.Bound(p => p.PhoneNumber).Width(24).Sortable(false);
			columns.Bound(p => p.PhysicianAccess).Width(20).Template(p => "<span class='img icon16 " + (p.PhysicianAccess ? "check" : "ecks") + "'></span>").Sortable(false);
			columns.Bound(p => p.PECOS).Width(9).Template(p => "<span class='img icon16 " + (p.PECOS ? "check" : "ecks") + "'></span>").Sortable(false);
			columns.Bound(p => p.Id).Width(21).Template(p => (isCanUserEdit ? "<a class=\"link\" onclick=\"Physician.Edit('" + p.Id + "')\">Edit</a>" : string.Empty) + (isCanUserDelete ? "<a class=\"link\" onclick=\"Physician.Delete('" + p.Id + "')\">Delete</a>" : string.Empty)).Width((isCanUserEdit.ToInteger() + isCanUserDelete.ToInteger()) * 11).Sortable(false).Title("Action").Visible((isCanUserEdit || isCanUserDelete));
        })
        .NoRecordsTemplate("No Physicians found.")
        .Sortable(sorting =>
			 sorting.SortMode(GridSortMode.SingleColumn)
				 .OrderBy(order =>
				 {
					 var sortName = Model.SortColumn ;
					 var sortDirection = Model.SortDirection;
					 if (sortName == "NPI")
					 {
						 if (sortDirection == "ASC")
						 {
							 order.Add(o => o.NPI).Ascending();
						 }
						 else if (sortDirection == "DESC")
						 {
							 order.Add(o => o.NPI).Descending();
						 }
					 }
					 else if (sortName == "Name")
					 {
						 if (sortDirection == "ASC")
						 {
							 order.Add(o => o.Name).Ascending();
						 }
						 else if (sortDirection == "DESC")
						 {
							 order.Add(o => o.Name).Descending();
						 }
					 }
					 else if (sortName == "Email")
					 {
						 if (sortDirection == "ASC")
						 {
							 order.Add(o => o.Email).Ascending();
						 }
						 else if (sortDirection == "DESC")
						 {
							 order.Add(o => o.Email).Descending();
						 }
					 }

				 }))
        .Scrollable(scrolling => scrolling.Enabled(true)) %>