﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<div class="row">
    <div class="fr">
        <div class="ancillary-button"><a onclick="Physician.License.New('<%= Model.Id %>')">Add License</a></div>
    </div>
</div>
<div class="row">
	<% var licenses = Model.Licenses.IsNotNullOrEmpty() ? Model.Licenses.ToObject<List<PhysicainLicense>>() : new List<PhysicainLicense>(); %>
    <%= Html.Telerik().Grid<PhysicainLicense>(licenses)
    	.HtmlAttributes(new { @class = "position-relative" })
    	.Name("List_Physician_Licenses").DataKeys(keys => keys.Add(m => m.Id))
    	.Columns(columns => {
            columns.Bound(l => l.LicenseNumber).Sortable(false);
            columns.Bound(l => l.State).ReadOnly().Sortable(false);
            columns.Bound(l => l.InitiationDateFormatted).Title("Issue Date").Sortable(false);
            columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
			columns.Bound(l => l.Id).Template(l => "<a class=\"link\" onclick=\"Physician.License.Edit('" + l.Id + "','" + Model.Id + "')\">Edit</a><a class=\"link\" onclick=\"Physician.License.Delete('" + l.Id + "','" + Model.Id + "')\">Delete</a>").ClientTemplate("<a class=\"link\" onclick=\"Physician.License.Edit('<#=Id#>','" + Model.Id + "')\">Edit</a><a class=\"link\" onclick=\"Physician.License.Delete('<#=Id#>','" + Model.Id + "')\">Delete</a>").Sortable(false).Width(135).Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("LicenseList", "Physician", new { physicianId = Model.Id }))
    	.NoRecordsTemplate("No licenses found.").Sortable()
		.ClientEvents(events => events.OnDataBinding("U.OnDataBindingStopAction").OnDataBound("U.OnTGridDataBound"))
    	.Footer(false).Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script>
	U.StopTGridFirstAction($("#List_Physician_Licenses"));
</script>