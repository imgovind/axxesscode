﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Edit Physician | <%= Model.DisplayName %></span>
<div class="wrapper main">
<% using (Html.BeginForm("Update", "Physician", FormMethod.Post, new { @id = "editPhysicianForm" }))
   { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditPhysician_Id" }) %>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditPhysician_FirstName", @class = "required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="EditPhysician_MiddleIntial" class="fl">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleName", Model.MiddleName, new { @id = "EditPhysician_MiddleIntial", @class = "shortest", @maxlength = "1" }) %></div>
            </div>
            <div class="row">
                <label for="EditPhysician_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditPhysician_LastName", @class = "required", @maxlength = "50" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_Credentials" class="fl">Credentials</label>
                <div class="fr"><%= Html.TextBox("Credentials", Model.Credentials, new { @id = "EditPhysician_Credentials", @maxlength = "40" }) %></div>
            </div>
            <div class="row">
                <label for="EditPhysician_NpiNumber" class="fl">NPI</label>
                <div class="fr"><%= Html.TextBox("NPI", Model.NPI, new { @id = "EditPhysician_NpiNumber", @class = "numeric", @maxlength = "10" }) %></div>
            </div>
            <div class="row">
                <label class="fl">PECOS Verification</label>
                <div id="EditPhysician_PecosCheck" class="fr"><span class='img icon16 <%= Model.IsPecosVerified ? "check" : "ecks" %>'></span></div>
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditPhysician_AddressLine1", @class = "required", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditPhysician_AddressLine2", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditPhysician_AddressCity", @class = "address-city required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressStateCode" class="fl"> State, Zip</label>
                <div class="fr">
                    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "EditPhysician_AddressStateCode", @class = "address-state required short" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditPhysician_AddressZipCode", @class = "required numeric zip shorter", @maxlength = "9" }) %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_Phone1" class="fl">Primary Phone</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("PhoneWorkArray", Model.PhoneWork, "EditPhysician_Phone", "required") %>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_AltPhone1" class="fl">Alternate Phone</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("PhoneAltArray", Model.PhoneAlternate, "EditPhysician_AltPhone", "") %>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_Fax1" class="fl">Fax Number</label>
                <div class="fr">
	                <%= Html.PhoneOrFax("FaxNumberArray", Model.FaxNumber, "EditPhysician_Fax", "") %>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_Email" class="fl">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditPhysician_Email", @class = "email", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.CheckBox("PhysicianAccess", Model.PhysicianAccess, new { @id = "EditPhysician_PhysicianAccess" }) %>
                            <label for="EditPhysician_PhysicianAccess">
                                Grant Physician Access
                                <br/>
                                <em>Email Address Required</em>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>License</legend>
        <div class="wide-column"><% Html.RenderPartial("Licenses", Model); %></div>
    </fieldset>
    <% if (Model.IsUserCanViewLog) { %>
	<a class="fr img icon32 log" onclick="Log.LoadPhysicianLog('<%= Model.Id %>')"/>
	<% } %>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
