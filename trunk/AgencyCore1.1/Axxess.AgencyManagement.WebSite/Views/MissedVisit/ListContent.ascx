﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisitTaskViewData>" %>
<%  var sortName = Model.SortColumn; %>
<%  var sortDirection = Model.SortDirection; %>
<%= Html.Filter("GroupName", Model.GroupName, new { @class = "grouping" })%>
<%= Html.Filter("SortParams", string.Format("{0}-{1}", sortName, sortDirection), new { @class = "sort-parameters" })%>
<%  var actions = new List<string>(); %>
<%  if (Model.IsUserCanDelete) actions.Add("<a class=\"delete-task link\">Delete</a>"); %>
<%  if (Model.IsUserCanRestore) actions.Add("<a class=\"restore link\">Restore</a>"); %>
<%  var count = actions.Count; %>
<%  var actionUrl = actions.ToArray().Join(""); %>
<%= Html.Telerik().Grid(Model.Activities).Name("MissedVisits_Grid").HtmlAttributes(new { @class = "aggregated" })   .Columns(columns => {
        columns.Bound(v => v.MRN).Width(90);
		columns.Bound(v => v.PatientName);
        columns.Bound(v => v.TaskName).Template(v => string.Format("<a class=\"link {1}\">{0}</a>", v.TaskName, v.IsComplete ? "complete" : string.Empty)).Title("Task").Width(220);
		columns.Bound(v => v.UserName);
		columns.Bound(v => v.Status).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
		columns.Bound(v => v.EventDate).Format("{0:MM/dd/yyyy}").Width(105);
		columns.Bound(v => v.Time).Width(105).Visible(Model.ShowTime);
		columns.Bound(v => v.RedNote).Title(" ").Width(30).Template(v => v.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\">{0}</a>", v.RedNote) : string.Empty).Visible(Model.IsUserCanSeeStickyNote).Sortable(false);
        columns.Bound(v => v.YellowNote).Title(" ").Width(30).Template(v => v.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\" tooltip=\"{0}\"></a>", v.YellowNote) : string.Empty).Visible(Model.IsUserCanSeeStickyNote).Sortable(false);
        columns.Bound(v => v.EventId).Template(v => actionUrl).Title("Action").HtmlAttributes(new { @class = "action" }).Width(count * 57).Visible(!Current.IsAgencyFrozen && count > 0).Sortable(false);
        columns.Bound(s => s.EventId).HtmlAttributes(new { @class = "id" }).Hidden();
		columns.Bound(s => s.PatientId).HtmlAttributes(new { @class = "pid" }).Hidden();
		columns.Bound(s => s.EventId).Template(s => Model.Service.ToString()).HtmlAttributes(new { @class = "service" }).Hidden();
	}).NoRecordsTemplate("No Missed Visits found.").Groupable(settings => settings.Groups(groups => {
        if (Model.GroupName == "PatientName") groups.Add(s => s.PatientName);
        else if (Model.GroupName == "EventDate") groups.Add(s => s.EventDate);
        else if (Model.GroupName == "UserName") groups.Add(s => s.UserName);
        else if (Model.GroupName == "TaskName") groups.Add(s => s.TaskName);
        else groups.Add(s => s.EventDate);
    })).Scrollable().Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "MRN") {
            if (sortDirection == "ASC") order.Add(o => o.MRN).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.MRN).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "Status") {
            if (sortDirection == "ASC") order.Add(o => o.Status).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Status).Descending();
        }
    })) %>