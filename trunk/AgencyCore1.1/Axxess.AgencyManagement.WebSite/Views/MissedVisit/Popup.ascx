﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<fieldset>
    <div class="wide-column">
        <div class="row no-input"><label for="Missed_Visit_Info_Name" class="fl">Patient Name</label><div class="fr"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_TypeofVisit" class="fl">Type of Visit</label><div class="fr"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_DateofVisit" class="fl">Date of Visit</label><div class="fr"><%= Model != null ? Model.Date.ToShortDateString():string.Empty %></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_UserName" class="fl">Assigned To</label><div class="fr"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_OrderGenerated" class="fl">Order Generated</label><div class="fr"><%= (Model != null && Model.IsOrderGenerated) ? "Yes" : "No"%></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_PhysicianOfficeNotified" class="fl">Physician Office Notified</label><div class="fr"><%= (Model != null && Model.IsPhysicianOfficeNotified) ? "Yes" : "No"%></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_Reason" class="fl">Reason</label><div class="fr"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div></div>
        <div class="row no-input"><label for="Missed_Visit_Info_Comments" class="fl">Comments</label><div class="fr"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div></div>
    </div>
</fieldset>