﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "MissedVisit", FormMethod.Post, new { @id = "editMissedVisitForm", @class = "mainform" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditMissedVisit_EventId" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "EditMissedVisit_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditMissedVisit_PatientId" })%>
<%= Html.Hidden("Date", Model.EventDate, new { @id = "EditMissedVisit_Date" })%>
    <fieldset>
        <legend>Missed Visit Details</legend>
        <div class="wide-column">
            <div class="row narrower">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row narrower">
                <label class="fl">Visit Type</label>
                <div class="fr"><%= Model.VisitType %></div>
            </div>
            <div class="row narrower">
                <label class="fl">Scheduled Date</label>
                <div class="fr"><%= Model.EventDate %></div>
            </div>
            <div class="row narrower">
                <label class="fl">Order Generated</label>
                <div class="fr">
                    <%= Html.RadioButton("IsOrderGenerated", "true", new { @id = "EditMissedVisit_IsOrderGeneratedY" })%>
                    <label for="EditMissedVisit_IsOrderGeneratedY" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsOrderGenerated", "false", new { @id = "EditMissedVisit_IsOrderGeneratedN" })%>
                    <label for="EditMissedVisit_IsOrderGeneratedN" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row narrower">
                <label class="fl">Physician Notified</label>
                <div class="fr">
                    <%= Html.RadioButton("IsPhysicianOfficeNotified", "true", new { @id = "EditMissedVisit_IsPhysicianOfficeNotifiedY" })%>
                    <label for="EditMissedVisit_IsPhysicianOfficeNotifiedY" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("IsPhysicianOfficeNotified", "false", new { @id = "EditMissedVisit_IsPhysicianOfficeNotifiedN" })%>
                    <label for="EditMissedVisit_IsPhysicianOfficeNotifiedN" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row narrower">
                <label for="EditMissedVisit_Reason" class="fl">Reason</label>
                <div class="fr">
                    <%  var reasons = new SelectList(new[] {
                            new SelectListItem { Text = "-- Select Reason --", Value = "" },
                            new SelectListItem { Text = "Cancellation of Care", Value = "Cancellation of Care" },
                            new SelectListItem { Text = "Doctor - Clinic Appointment", Value = "Doctor - Clinic Appointment" },
                            new SelectListItem { Text = "Family - Caregiver Able to Assist Patient", Value = "Family - Caregiver Able to Assist Patient" },
                            new SelectListItem { Text = "No Answer to Locked Door", Value = "No Answer to Locked Door" },
                            new SelectListItem { Text = "No Answer to Phone Call", Value = "No Answer to Phone Call" },
                            new SelectListItem { Text = "Patient - Family Uncooperative", Value = "Patient - Family Uncooperative" },
                            new SelectListItem { Text = "Patient Hospitalized", Value = "Patient Hospitalized" },
                            new SelectListItem { Text = "Patient Unable to Answer Door", Value = "Patient Unable to Answer Door" },
                            new SelectListItem { Text = "Therapy on Hold", Value = "Therapy on Hold" },
                            new SelectListItem { Text = "Other (Specify)", Value = "Other" }
                        }, "Value", "Text", Model.Reason);%>
                    <%= Html.DropDownList("Reason", reasons, new { @id = "EditMissedVisit_Reason" })%>
                </div>
            </div>
            <div class="row narrower">
                <label for="EditMissedVisit_Comments">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.Comments, new { @id = "EditMissedVisit_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
            <div class="row">
                <label for="EditMissedVisit_ClinicianSignature" class="fl">Signature</label>
                <div class="fr"><%= Html.Password("Signature", string.Empty, new { @id = "EditMissedVisit_ClinicianSignature", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditMissedVisit_ClinicianSignatureDate" class="fl">Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="SignatureDate" value="<%= Model.EventDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="EditMissedVisit_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="complete">Submit</a></li>
        <li><a class="close">Close</a></li>
    </ul>
</div>
<% } %>