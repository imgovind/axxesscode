﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisitTaskViewData>" %>
<span class="wintitle">List Missed Visits | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  var pageName = "MissedVisits";  %>
<div class="wrapper main blue">
    <%  var area = Model.Service.ToArea();%>
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="buttons fr ac">
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a service="<%= (int)Model.ExportPermissions %>" class="export servicepermission" area="<%=area %>" url="/Export/MissedVisits">Excel Export</a></li><div class="clr"></div>
    <%  } %>
        <li><a area="<%=area %>" url="/MissedVisit/ListContent" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service", @id = pageName + "_ServiceId" })%></div>
		<div class="filter">
			<label for="<%= pageName %>_BranchId">Branch</label>
			<%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService, new { @id = pageName + "_BranchId", @class = "onzero" })%>
		</div>
		<div class="filter">
			<label for="<%= pageName %>_StartDate">Date Range</label>
			<%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-89), false, new { @class = "short", @id = pageName + "_StartDate" }) %>
			<label for="<%= pageName %>_EndDate">&#8211;</label>
			<%= Html.DatePicker("EndDate", DateTime.Now, false, new { @class = "short", @id = pageName + "_EndDate" }) %>
		</div>
		<ul class="buttons ac">
			<li><a grouping="Patient" class="group-button">Group By Patient</a></li>
			<li><a grouping="ScheduledDate" class="group-button">Group By Scheduled Date</a></li>
			<li><a grouping="Employee" class="group-button">Group By Employee</a></li>
			<li><a grouping="TaskName" class="group-button">Group By Task</a></li>
		</ul>
	</fieldset>
</div>
<div class="clr"></div>
<div id="<%= pageName %>GridContainer" init="MissedVisit.InitListContent"><% Html.RenderPartial("ListContent",Model); %></div>
<%  } %>