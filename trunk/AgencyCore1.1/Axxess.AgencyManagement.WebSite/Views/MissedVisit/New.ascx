﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITask>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "MissedVisit", FormMethod.Post, new { @id = "newMissedVisitForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "New_MissedVisit_EventId" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "New_MissedVisit_EpisodeId" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "New_MissedVisit_PatientId" })%>
    <fieldset>
        <legend>Missed Visit Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="fl">Type of Visit</label>
                <div class="fr"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label class="fl">Date of Visit</label>
                <div class="fr"><%= Model.EventDate.ToShortDateString() %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_Order" class="fl">Order Generated</label>
                <div class="fr">
                    <%= Html.DropDownList("IsOrderGenerated", new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "true" },
                            new SelectListItem { Text = "No", Value = "false" }
                        }, "Value", "Text", string.Empty).AsEnumerable(), new { @id = "New_MissedVisit_Order", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_MissedVisit_PhysicianNotified" class="fl">Physician Office Notified</label>
                <div class="fr">
                    <%= Html.DropDownList("IsPhysicianOfficeNotified", new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "true" },
                            new SelectListItem { Text = "No", Value = "false" }
                        }, "Value", "Text", string.Empty).AsEnumerable(), new { @id = "New_MissedVisit_PhysicianNotified", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_MissedVisit_Reason" class="fl">Reason</label>
                <div class="fr">
                    <select id="New_MissedVisit_Reason" name="Reason" class="selectDropDown">
                        <option value="">-- Select --</option>
                        <option value="Cancellation of Care">Cancellation of Care</option>
                        <option value="Doctor - Clinic Appointment">Doctor - Clinic Appointment</option>
                        <option value="Family - Caregiver Able to Assist Patient">Family - Caregiver Able to Assist Patient</option>
                        <option value="No Answer to Locked Door">No Answer to Locked Door</option>
                        <option value="No Answer to Phone Call">No Answer to Phone Call</option>
                        <option value="Patient - Family Uncooperative">Patient - Family Uncooperative</option>
                        <option value="Patient Hospitalized">Patient Hospitalized</option>
                        <option value="Patient Unable to Answer Door">Patient Unable to Answer Door</option>
                        <option value="Therapy on Hold">Therapy on Hold</option>
                        <option value="Other">Other (Specify)</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", string.Empty, new { @id = "New_MissedVisit_Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignature" class="fl">Signature</label>
                <div class="fr"><%= Html.Password("Signature", "", new { @id = "New_MissedVisit_ClinicianSignature", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignatureDate" class="fl">Signature Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="SignatureDate" value="<%= Model.EventDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="New_MissedVisit_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="complete">Submit</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>