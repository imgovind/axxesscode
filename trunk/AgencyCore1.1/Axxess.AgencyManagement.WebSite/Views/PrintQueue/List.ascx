﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceTaskViewData>" %>
<span class="wintitle">Print Queue | <%= Current.AgencyName %></span>
<%  string pagename = "PrintQueue"; %>
<%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" }) %> 
    <ul class="fr buttons">
<%  if (Model.ExportPermissions != AgencyServices.None) { %>
	    <li><a service="<%= (int)Model.ExportPermissions %>" class="export servicepermission" area="<%= area %>" url="/PrintQueue/Export">Excel Export</a></li>
	    <div class="clr"></div>
<%  } %>
        <li><a class="grid-refresh" area="<%= area %>" url="/PrintQueue/Content">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pagename, new { @id = pagename + "_ServiceId" }) %></div>
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService, new { @id = pagename + "_BranchId" }) %>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-7), false, new { @id = pagename + "_StartDate", @class = "short" })%>
            <label for="<%= pagename %>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, false, new { @id = pagename + "_EndDate", @class = "short" })%>
        </div>
        <div class="clr"></div>
        <ul class="buttons ac">
            <li><a status="Organize Grid by Patient" class="group-button" grouping="PatientName">Group By Patient</a></li>
            <li><a status="Organize Grid by Date" class="group-button" grouping="EventDate">Group By Date</a></li>
            <li><a status="Organize Grid by Task Type" class="group-button" grouping="DisciplineTaskName">Group By Task</a></li>
            <li><a status="Organize Grid by Clinician" class="group-button" grouping="UserName">Group By Clinician</a></li>
        </ul>
    </fieldset>
    <form method="post" action="/PrintQueue/MarkAsPrinted" class="markasprinted">
        <div id="<%=pagename %>GridContainer" class="content" init="Agency.PrintQueue.InitContent"><% Html.RenderPartial("Content", Model); %></div>
    </form>
</div>