﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceTaskViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
<%  string pagename = "PrintQueue"; %>
<%  var groupName = Model.GroupName; %>
<%  var sortName = Model.SortColumn; %>
<%  var sortDirection = Model.SortDirection; %>
<%  var sortParams = string.Format("{0}-{1}", sortName, sortDirection); %>
<%= Html.Filter("GroupName", groupName, new { @id = pagename + "_GroupName", @class = "grouping" })%>
<%= Html.Filter("SortParams", sortParams, new { @id = pagename + "_SortName", @class = "sort-parameters" })%>
<%  Html.Telerik().Grid(Model.Tasks).Name(pagename + "_Grid").HtmlAttributes(new { @class = "button-bottom-bar padded-footer" }).Columns(columns => {
        columns.Bound(s => s.Id).Width("2%").Template(s => string.Format("<input name='Id' type='checkbox' value='{0}'/>", s.Id)).ClientTemplate("<input name='Id' type='checkbox' value='<#= Id #>'/>").Title("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false).Visible(!Current.IsAgencyFrozen);
        columns.Bound(s => s.PatientName).Width("18%").Title("Patient");
        columns.Bound(s => s.EventDate).Title("Date").Format("{0:MM/dd/yyyy}").Width("9%").Aggregate(aggregates => aggregates.Count()).FooterTemplate(template => template.Count != null ? "Total: " + template.Count : string.Empty);
        columns.Bound(s => s.DisciplineTaskName).Title("Task").Width("22%").FooterTemplate("<div class='buttons abs-bottom'><ul><li><a class='save'>Mark as Printed</a></li></ul></div>");
        columns.Bound(s => s.StatusName).Width("16%");
        columns.Bound(s => s.UserName).Title("Employee").Width("18%");
        columns.Bound(s => s.Type).Template(s => "<a class=\"" + s.Type + "  img icon16 print\" />").Width("2%").Title("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false);
		columns.Bound(s => s.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "id" }).Sortable(false).Hidden();
		columns.Bound(s => s.PatientId).HeaderHtmlAttributes(new { style = "font-size:0;" }).Width(0).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Sortable(false).Hidden();
        columns.Bound(s => s.Id).Template(s => Model.Service).HeaderHtmlAttributes(new { style = "font-size:0;" }).Width(0).HtmlAttributes(new { style = "font-size:0;", @class = "service" }).Sortable(false).Hidden();
    }).Groupable(settings => settings.Groups(groups => {
        if (groupName == "PatientName") groups.Add(s => s.PatientName);
        else if (groupName == "EventDate") groups.Add(s => s.EventDate);
        else if (groupName == "DisciplineTaskName") groups.Add(s => s.DisciplineTaskName);
        else if (groupName == "UserName") groups.Add(s => s.UserName);
        else groups.Add(s => s.EventDate);
    })).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "DisciplineTaskName") {
            if (sortDirection == "ASC") order.Add(o => o.DisciplineTaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisciplineTaskName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).NoRecordsTemplate("No records to display.").Scrollable().Sortable().Render(); %>
<%  } %>