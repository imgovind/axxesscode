﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceAndGuidViewData>" %>
<span class="wintitle">Physician Face-to-face Encounter | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  if (Model.IsUserCanAdd) { %>
    <%  using (Html.BeginForm("Add", "FaceToFaceEncounter", FormMethod.Post, new { @id = "newFaceToFaceEncounterForm" })) { %>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Regarding</legend>
                <div class="column">
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_PatientName" class="fl">Patient Name</label>
                        <div class="fr"><%= Html.PatientsForSchedule((int)AgencyServices.HomeHealth, "PatientId",Guid.Empty, !Model.Id.IsEmpty() ? Model.Id.ToString() : "" , 0, "-- Select Patient --", new { @id = "New_FaceToFaceEncounter_PatientName", @class = "required not-zero" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_EpisodeId" class="fl">Episode Associated</label>
                        <div class="fr"> <%= Html.PatientEpisodes((int)AgencyServices.HomeHealth, "EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_FaceToFaceEncounter_EpisodeId", @class = "required not-zero episode-dropdown" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_Date" class="fl">Request Date</label>
                        <div class="fr"><input type="text" class="date-picker required" name="RequestDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="New_FaceToFaceEncounter_Date" /></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Physician</legend>
                <div class="column">
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_PhysicianDropDown" class="fl">Physician</label>
                        <div class="fr">
                            <%= Html.TextBox("PhysicianId", "", new { @id = "New_FaceToFaceEncounter_PhysicianDropDown", @class = "physician-picker required not-zero" })%><br />
                            <div class="fr ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Face to Face</legend>
        <div class="wide-column"> 
            <div class="row">
                <ul class="checkgroup two-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.RadioButton("Certification", "1", true, new { @id = "New_FaceToFaceEncounter_Certification1" }) %>
                            <label for="New_FaceToFaceEncounter_Certification1">POC Certifying Physician</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.RadioButton("Certification", "2", new { @id = "New_FaceToFaceEncounter_Certification2" }) %>
                            <label for="New_FaceToFaceEncounter_Certification2">Non POC Certifying Physician</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <em>
                    <strong>* Note:</strong> Completing this document creates a Physician Face to Face Encounter request document that
                    must be submitted to the physician. The physician will be required to certify that the patient is homebound and the
                    home health services provided are medically necessary. The physician will certify by signing the face to face
                    encounter document and returning to the home health agency.
                </em>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "115", new { @id = "New_FaceToFaceEncounter_Status" })%>
    <ul class="buttons ac">
        <li><a class="save close">Submit</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("add a new physician face to face encounter")%>
<%  } %>
</div>