﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ErrorPageViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<title>Axxess&#8482; AgencyCore&#8482; | 404 - Page Not Found</title>
		<style type="text/css">
			html{height:100%;}
			body{overflow:hidden;background:#d7d7d7;}
			body>div{width: 36em;height: auto;position:fixed;top:45%;left:45%;background-color:#fff;margin:-6em 0 0 -16em;text-align:center;-moz-border-radius:1em;-webkit-border-radius:1em;border-radius:1em;font:1em 'Lucida Grande',Arial,'Liberation Sans',FreeSans,sans-serif;padding:1em;-moz-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-webkit-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-o-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-khtml-box-shadow: 0 10px 25px #333, 0 0 4px 3px #aaa inset;box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;}
			#logo{float:left;margin:.75em 0;}
			em{position:absolute;bottom:1em;right:1em;font-size:.7em;}
			.axxesslogo{float:left;background:url(/Images/axxess.png) no-repeat 0 0;display:inline-block;height:140px;width:194px;margin:20px;}
			.erroricon{width:32px;height:32px;display:inline-block;background:url(/Images/sprite.png) no-repeat -128px -48px;vertical-align:middle;}
		</style>
	</head>
	<body>
		<div>
			<span class="axxesslogo"></span>
			<h1><span class="erroricon"></span> Page Not Found</h1>
			<hr />
			<p>The page you were looking for could not be found. You may have mistyped the address or clicked on an expired link. Click <a href='/Login'>here</a> to return to the Login.</p>
			<em>HTTP 404</em>
			<p><%= Model.message %></p>
		</div>
	</body>
</html>
