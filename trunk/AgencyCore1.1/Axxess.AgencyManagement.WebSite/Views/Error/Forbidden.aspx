﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<title>Axxess&#8482; AgencyCore&#8482; | 403 - Forbidden</title>
		<style type="text/css">
			html{height:100%;}
			body{overflow:hidden;background:#d7d7d7;}
			body>div{width: 36em;height: auto;position:fixed;top:45%;left:45%;background-color:#fff;margin:-6em 0 0 -16em;text-align:center;-moz-border-radius:1em;-webkit-border-radius:1em;border-radius:1em;font:1em 'Lucida Grande',Arial,'Liberation Sans',FreeSans,sans-serif;padding:1em;-moz-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-webkit-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-o-box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;-khtml-box-shadow: 0 10px 25px #333, 0 0 4px 3px #aaa inset;box-shadow:0 10px 25px #333, 0 0 4px 3px #aaa inset;}
			#logo{float:left;margin:.75em 0;}
			em{position:absolute;bottom:1em;right:1em;font-size:.7em;}
			.axxesslogo{float:left;background:url(/Images/axxess.png) no-repeat 0 0;display:inline-block;height:140px;width:194px;margin:20px;}
			.erroricon{width:32px;height:32px;display:inline-block;background:url(/Images/sprite.png) no-repeat -128px -48;vertical-align:middle;}
		</style>
	</head>
	<body>
		<div>
			<span class="axxesslogo"></span>
			<h1><span class="erroricon"></span> Forbidden</h1>
			<hr />
			<p>You are forbidden from accessing this resource.</p>
			<em>HTTP 403</em>
		</div>
	</body>
</html>
