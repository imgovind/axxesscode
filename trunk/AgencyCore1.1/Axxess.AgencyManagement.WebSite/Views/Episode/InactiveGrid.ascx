﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeLeanViewData>" %>
<div class="acore-grid">
    <ul>
        <li class="ac"><h3>Inactive Episode(s)</h3></li>
        <li>
            <span class="grid-threequarters">Episode Range</span>
            <span>Action</span>
        </li>
    </ul>
	<%  if (Model != null && Model.List != null && Model.List.Count > 0)
     { %>
    <%  int i = 1; %>
		<ol>
		<%  foreach (var episode in Model.List) { %>
			<li>
				<span class="grid-threequarters"><%= episode.Range %></span>
				<span>
            <%  if (Model.IsUserCanReactivate) { %>
					<a class="link" onclick="Episode.HomeHealth.Activate('<%=episode.Id %>','<%= episode.PatientId %>')">Activate</a> |
			<%  } %>
            <%  if (Model.IsUserCanEdit) { %>
                    <a class="link" onclick="Episode.HomeHealth.Edit('<%=episode.Id %>','<%= episode.PatientId %>')">Edit</a>
			<%  } %>
				</span>
			</li>
			<%  i++; %>
		<%  } %>
		</ol>
	<% } else { %>
	<ol>
		<li>
			<h4>No Inactive Episodes Found</h4>
		</li>
	</ol>
	<% } %>
</div>