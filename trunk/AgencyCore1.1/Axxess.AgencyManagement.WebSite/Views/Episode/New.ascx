﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<%= Html.Hidden("New_Episode_PreviousEndDate", Model.EndDate != DateTime.MinValue ? Model.EndDate.ToShortDateString() : "") %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Add", "Episode", FormMethod.Post, new { @id = "newEpisodeForm", @class = "mainform" }))
        { %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label for="New_Episode_PatientId" class="fl">Patient</label>
                <label class="fr"><%= Model.DisplayName %></label>
                <%= Html.Hidden("PatientId", Model.PatientId.ToString(), new { @id = "New_Episode_PatientId"}) %>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Episode_AdmissionId" class="fl">Start of Care</label>
                <div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "New_Episode_AdmissionId" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label for="New_Episode_TargetDate" class="fl">Episode Start</label>
                <div class="fr">
                    <input type="text" class="date-picker required" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>" id="New_Episode_StartDate" /><br />
        <%  if (Model != null && Model.EndDate != DateTime.MinValue) { %>
                    <em id="newEpisodeTip">Last Episode End Date is <%= Model.EndDate.ToShortDateString() %></em>
        <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="New_Episode_EndDate" class="fl">Episode End</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="New_Episode_EndDate" /></div>
            </div>
            <div class="row">
                <label for="New_Episode_CaseManager" class="fl">Case Manager</label>
                <div class="fr"><%= Html.Users("CaseManagerId", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "New_Episode_CaseManagerId", @class = "user-selector required" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Episode_PrimaryPhysician" class="fl">Primary Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", Model.PrimaryPhysician, new { @id = "New_Episode_PhysicianId", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <label for="New_Episode_PrimaryInsurance" class="fl">Primary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance, Model.AgencyLocationId, (int)AgencyServices.HomeHealth, false, new { @id = "New_Episode_PrimaryInsurance", @class = "Insurances required not-zero" }) %></div>
            </div>
            <div class="row">
                <label for="New_Episode_SecondaryInsurance" class="fl">Secondary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance, Model.AgencyLocationId, (int)AgencyServices.HomeHealth, false, new { @id = "New_Episode_SecondaryInsurance", @class = "Insurances" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>
            Comments
            <span class="img icon16 note blue"></span>
            <em>(Blue Sticky Note)</em>
        </legend>
        <div class="wide-column">
            <div class="row ac"><textarea id="New_Episode_Comments" name="Comments" class="taller"></textarea></div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <ul class="buttons ac">
        <li><a class="save close wait-confirmation">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
</div>