﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeLeanViewData>" %>
<div id="InactiveEpisodesContent" class="inactiveepisodes wrapper main">
   <% Html.RenderPartial("InactiveGrid", Model); %>
</div>
