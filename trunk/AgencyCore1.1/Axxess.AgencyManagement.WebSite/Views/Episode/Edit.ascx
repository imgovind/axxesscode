﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Update", "Episode", FormMethod.Post, new { @id = "editEpisodeForm" , @class="mainform"})) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("PreviousEndDate", Model.PreviousEpisode != null ? Model.PreviousEpisode.EndDate.ToShortDateString() : "")%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @Id = "Edit_Episode_PatientId" })%>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label for="New_Episode_PatientId" class="fl">Patient</label>
                <label class="fr"><%= Model.DisplayName%></label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Episode_StartOfCareDate" class="fl">Start of Care</label>
                <div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "Edit_Episode_AdmissionId" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Episode_TargetDate" class="fl">Episode Start</label>
                <div class="fr">
                    <input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>"  id="Edit_Episode_StartDate" /><br />
                     <%  if (Model.PreviousEpisode != null) { %><em>Previous Episode End Date is <%= Model.PreviousEpisode.EndDate.ToShortDateString()%></em><%  } %>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Episode_VisitDate" class="fl">Episode End</label>
                <div class="fr">
                    <input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDateFormatted %>" id="Edit_Episode_EndDate" /><br />
                    <%  if (Model.NextEpisode != null) { %><em>Next Episode Start Date is <%= Model.NextEpisode.StartDate.ToShortDateString()%></em><%  } %>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.CheckBox("IsActive", !Model.IsActive, new { @id = "Edit_Episode_IsActive" })%>
                            <label for="Edit_Episode_IsActive">Inactivate Episode</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Episode_CaseManager" class="fl">Case Manager</label>
                <div class="fr"><%= Html.Users("CaseManagerId", Model.CaseManagerId.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "Edit_Episode_CaseManagerId", @class = "user-selector required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Episode_PrimaryInsurance" class="fl">Primary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, false, new { @id = "Edit_Episode_PrimaryInsurance", @class = "Insurances required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Episode_SecondaryInsurance" class="fl">Secondary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance.ToString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, false, new { @id = "Edit_Episode_SecondaryInsurance", @class = "Insurances" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Episode_PrimaryPhysician" class="fl">Primary Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId, new { @id = "Edit_Episode_PhysicianId", @class = "physician-picker" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>
            Comments
            <span class="img icon16 note blue"></span>
            <em>(Blue Sticky Note)</em>
        </legend>
        <div class="wide-column">
            <div class="row ac"><textarea id="Edit_Episode_Comments" name="Comments" class="taller"><%= Model.Comments%></textarea></div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog) { %><a class="fr img icon32 log" onclick="Log.LoadEpisodeLog('<%= Model.Id %>','<%= Model.PatientId %>')" ></a><%  } %>
    <ul class="buttons ac">
        <li><a class="save close wait-confirmation">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
    <%  } %>
</div>