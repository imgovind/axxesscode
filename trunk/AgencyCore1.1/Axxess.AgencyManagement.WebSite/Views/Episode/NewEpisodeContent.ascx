﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<%= Html.Hidden("TopMenuNew_Episode_PreviousEndDate", Model.EndDate != DateTime.MinValue ? Model.EndDate.ToShortDateString() : "")%>
<fieldset>
    <legend>Details</legend>
    <div class="column">
        <div class="row">
            <label for="TopMenuNew_Episode_AdmissionId" class="fl">Start of Care Date</label>
            <div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "TopMenuNew_Episode_AdmissionId" }) %></div>
        </div>
        <div class="row">
            <label for="TopMenuNew_Episode_StartDate" class="fl">Episode Start Date</label>
            <div class="fr">
                <input type="text" class="date-picker required" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>"  id="TopMenuNew_Episode_StartDate" /><br />
<%  if (Model != null && Model.EndDate != DateTime.MinValue) { %>
                <em id="Em1">Last Episode End Date is <%= Model.EndDate.ToShortDateString() %></em>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="TopMenuNew_Episode_EndDate" class="fl">Episode End Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="TopMenuNew_Episode_EndDate" /></div>
        </div>
        <div class="row">
            <label for="TopMenuNew_Episode_CaseManager" class="fl">Case Manager</label>
            <div class="fr"><%= Html.Users("CaseManagerId", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "TopMenuNew_Episode_CaseManagerId", @class = "required" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="TopMenuNew_Episode_PrimaryPhysician" class="fl">Primary Physician</label>
            <div class="fr"><%= Html.TextBox("PhysicianId", Model.PrimaryPhysician, new { @id = "TopMenuNew_Episode_PhysicianId", @class = "physician-picker" })%></div></div>
        <div class="row">
            <label for="TopMenuNew_Episode_PrimaryInsurance" class="fl">Primary Insurance</label>
            <div class="fr"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance, (int)Model.Service, true, true, "-- Select Insurance --", new { @id = "TopMenuNew_Episode_PrimaryInsurance", @class = "required not-zero" })%></div>
        </div>
        <div class="row">
            <label for="TopMenuNew_Episode_SecondaryInsurance" class="fl">Secondary Insurance</label>
            <div class="fr"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance, (int)Model.Service, true, true, "-- Select Insurance --", new { @id = "TopMenuNew_Episode_SecondaryInsurance" })%></div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>
        Comments
        <span class="img icon16 note blue"></span>
        <em>(Blue Sticky Note)</em>
    </legend>
    <div class="wide-column">
        <div class="row ac"><textarea id="TopMenuNew_Episode_Comments" name="Comments"></textarea></div>
    </div>
</fieldset>
