﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Add", "Episode", FormMethod.Post, new { @id = "topMenuNewEpisodeForm", @class = "mainform" }))
        { %>
    <fieldset>
        <legend>Patient</legend>
        <div class="wide-column">
            <div class="row narrower">
                <label for="TopMenuNew_Episode_PatientId" class="fl">Patient</label>
                <div class="fr"><%= Html.PatientsForSchedule((int)Model.Service, "PatientId", Guid.Empty, Guid.Empty.ToString(), 0, "-- Select Patient --", new { @id = "TopMenuNew_Episode_PatientId", @class = "required not-zero" }) %></div>
            </div>
        </div>
    </fieldset>
    <div id="topMenuNewEpisodeContent"><% Html.RenderPartial("NewEpisodeContent", Model); %></div>
    <div class="clr"></div>
    <ul class="buttons ac">
        <li><a class="save close wait-confirmation">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
</div>