﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">List of Not Exported OASIS Assessments | <%= Current.AgencyName %></span>
<%  if (Model.ViewListPermissions != AgencyServices.None) { %>
    <%  var visible = !Current.IsAgencyFrozen; %>
    <%  string pageName = "NotExportedOasis"; %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="buttons fr ac">
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a url="Export/NotExportedOasis" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li><br />
    <%  } %>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pageName %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(),(int) Model.Service, new { @id = pageName + "_BranchId", @class = "location" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_Status">Status</label>
            <select id="<%= pageName %>_Status" name="StatusId" class="input">
                <option value="0">All</option>
                <option value="1" selected="selected">Active</option>
                <option value="2">Discharged</option>
            </select>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate">Exported Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), true, new { id = pageName + "_StartDate", @class = "short" })%>
            <label for="<%= pageName %>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, true, new { id = pageName + "_EndDate", @class = "short" })%>
        </div>
    </fieldset>  
    <div id="<%= pageName %>GridContainer">    
        <%  Html.Telerik().Grid<AssessmentExport>().Name(pageName+"_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
                columns.Bound(o => o.PatientName).Title("Patient").Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
                columns.Bound(o => o.AssessmentName).Title("Assessment");
                columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Assessment Date");
                columns.Bound(o => o.EpisodeRange).Width(150).Title("Episode");
                columns.Bound(o => o.Insurance);
                columns.Bound(o => o.IsUserCanReopen).Title("Action").Width(80).ClientTemplate("").HtmlAttributes(new { @class = "action" }).Sortable(false).Visible(visible);
            }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("NotExportedGrid", "Oasis", new { BranchId = Model.Id, StatusId = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).NoRecordsTemplate("No OASIS found.").ClientEvents(c => c
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Oasis.Export.OnDataBound")
                .OnRowDataBound("Oasis.Export.OnRowDataBound")
                .OnError("U.OnTGridError")
            ).Scrollable().Sortable().Render(); %>
    </div>
</div>
<%  } %>