﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentExportViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
<%  string pagename = "OasisExport"; %>
<%  Html.Telerik().Grid(Model.Assessments).Name(pagename + "_Grid").HtmlAttributes(new { @class = "grid-container", @style = (Model.GeneratePermissions != AgencyServices.None? "bottom:30px":"") }).Columns(columns => {
        columns.Bound(o => o.AssessmentId).Template(t => { %><%= (!t.IsValidated || t.IsSubmissionEmpty) ? string.Empty : string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", t.AssessmentId)%><% }).Title("").Width(30).Visible(Model.GeneratePermissions.Has(Model.Service)).HtmlAttributes(new { style = "text-align:center" }).Sortable(false).HeaderTemplate(ht => "<input id='" + pagename + "_SelectAll' class='selectall' type='checkbox' style='margin:0'/>");
        columns.Bound(o => o.Index).Title("#").Width(30);
        columns.Bound(o => o.PatientName).Title("Patient Name");
        columns.Bound(o => o.AssessmentName).Title("Assessment Type").Sortable(true);
        columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Title("Assessment Date").Width(125).Sortable(true);
        columns.Bound(o => o.EpisodeRange).Width(160).Title("Episode").Sortable(true);
        columns.Bound(o => o.Insurance).Sortable(true);
        columns.Bound(o => o.AssessmentId).Template(t => (!t.IsValidated || t.IsSubmissionEmpty) ? string.Format("<a onclick=\"OasisValidation.ExportRegenerate('{0}','{1}','{2}')\">Refresh</a>", t.AssessmentId, t.PatientId, t.TaskName) : string.Format("{2:00} ( <a href=\"javaScript:void(0);\" onclick=\"Oasis.HomeHealth.ShowCorrectionNumberModal('{0}','{1}','{2}');\"> Edit </a>  )", t.AssessmentId, t.PatientId, t.CorrectionNumber)).Title("Correction #").Width(100).Visible(!Current.IsAgencyFrozen && Model.GeneratePermissions.Has(Model.Service)).Sortable(false);
    }).Footer(false).NoRecordsTemplate("No OASIS found.").Scrollable().Sortable(sorting =>sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = Model.SortColumn;
        var sortDirection = Model.SortDirection;
        if (sortName == "Index") {
            if (sortDirection == "ASC") order.Add(o => o.Index).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Index).Descending();
        } else if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "AssessmentName") {
            if (sortDirection == "ASC") order.Add(o => o.AssessmentName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.AssessmentName).Descending();
        } else if (sortName == "Insurance") {
            if (sortDirection == "ASC") order.Add(o => o.Insurance).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Insurance).Descending();
        } else if (sortName == "EpisodeRange") {
            if (sortDirection == "ASC") order.Add(o => o.EpisodeRange).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EpisodeRange).Descending();
        } else if (sortName == "AssessmentDate") {
            if (sortDirection == "ASC") order.Add(o => o.AssessmentDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.AssessmentDate).Descending();
        }
    })).Render(); %>
    <%  if (Model.GeneratePermissions != AgencyServices.None) { %>
<div id="<%=pagename %>_BottomPanel" class="abs bottom blue wrapper">
    <ul class="buttons ac">
        <li><a class="generate servicepermission" service="<%= (int)Model.GeneratePermissions %>">Generate OASIS File</a></li>
        <%  if (!Current.IsAgencyFrozen) { %>
		<li><a class="marksubmit servicepermission" service="<%=(int)Model.GeneratePermissions %>" value="Exported">Mark Selected As Exported</a></li>
		<li><a class="marksubmit servicepermission" service="<%=(int)Model.GeneratePermissions %>" value="CompletedNotExported">Mark Selected As Completed (Not Exported)</a></li>
	    <%  } %>
	</ul>
</div>
    <%  } %>
<%  } %>