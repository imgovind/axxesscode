﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OasisExport>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("CorrectionChange", "Oasis", FormMethod.Post, new { @id = "oasisCorrectionChange", @class = "mainform"})) { %>
<%= Html.Hidden("Id", Model.AssessmentId) %>
<%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <div class="wide-column">
            <div class="row">The correction number should only be changed if you are retransmitting an OASIS assessment that was previously accepted and needs to be retransmitted because of corrections you made. If it is the first resubmission , use correction number 01, 2nd resubmission use number 02 , 3rd resubmission use number 03, etc.</div>
            <div class="row">
                <label class="fl">Correction Number</label>
                <div class="fr">
    <%  var selectionList = new List<SelectListItem>(); %>
    <%  for (int i = 0; i <= 10; i++) selectionList.Add(new SelectListItem { Text = string.Format("{0:00}", i), Value = i.ToString() }); %>
                    <%= Html.DropDownList("CorrectionNumber", new SelectList(selectionList, "Value", "Text", Model.CorrectionNumber.ToString())) %>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <em>If an OASIS assessment was rejected and needs to be retransmitted after corrections have been made, use correction number 00.</em>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a id="oasisExportCorrectionNumber" class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>