﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="wrapper main note">
<%  using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "NonOasisSignatureForm" , @class = "mainform" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("OasisValidationType", Model.TypeName)%>
    <%= Html.Hidden("oasisPageName", Model.TypeName)%>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
    <%  if (Model.TypeName != "NonOASISDischarge") { %>
            <div class="row">
                <label for="<%= Model.TypeName %>_TimeIn" class="fl">Time In</label>
                <div class="fr"><input type="text" class="time-picker" name="TimeIn" id="<%= Model.TypeName %>_TimeIn" value="<%= Model.TimeIn.IsNotNullOrEmpty() ? Model.TimeIn : string.Empty %>" /></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="<%= Model.TypeName %>_ValidationClinician" class="fl">Signature</label>
                <div class="fr"><%= Html.Password("ValidationClinician", "", new { @id = Model.TypeName + "_ValidationClinician", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
    <%  if (Model.TypeName != "NonOASISDischarge") { %>
            <div class="row">
                <label for="<%= Model.TypeName %>_TimeIn" class="fl">Time Out</label>
                <div class="fr"><input type="text" class="time-picker" name="TimeOut" id="<%= Model.TypeName %>_TimeOut" value="<%= Model.TimeOut.IsNotNullOrEmpty() ? Model.TimeOut : string.Empty %>" /></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="<%= Model.TypeName %>_SignatureDate" class="fl">Signature Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ValidationSignatureDate" id="<%= Model.TypeName %>_ValidationSignatureDate" value="<%= Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>