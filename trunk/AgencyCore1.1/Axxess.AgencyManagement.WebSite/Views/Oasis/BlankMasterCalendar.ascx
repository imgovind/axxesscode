﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EpisodeDate>" %>
<%  var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%  var startDate = Model.StartDate; %>
<%  var endDate = Model.EndDate; %>
<%  var currentDate = Model.StartDate.AddDays(-startWeekDay); %>
<div class="cal big oasis-calendar">
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Sun</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
            </tr>
        </thead>
        <tbody>
<%  for (int i = 1; i <= 10; i++) { %>
            <tr>
                <th>Week<%= i %></th>
    <%  int addedDate = (i - 1) * 7; %>
    <%  for (int j = 0; j <= 6; j++) { %>
        <%  var specificDate = currentDate.AddDays(j + addedDate); %>
        <%  if (specificDate < startDate || specificDate > endDate) { %>
                <td></td>
        <%  } else { %>
            <%  if (j == 6) { %>
                <td class="lastTd">
            <%  } else { %>
                <td>
            <%  } %>
                    <span><%= string.Format("{0:MM/dd}", specificDate) %></span>
                </td>
        <%  } %>
    <%  } %>
            </tr>
<%  } %>
        </tbody>
    </table>
</div>