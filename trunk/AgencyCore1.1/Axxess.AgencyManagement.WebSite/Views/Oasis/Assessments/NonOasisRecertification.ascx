﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary();
	var area = Model.Service.ToArea(); %>
<span class="wintitle"><%= Model.TaskName.IsNotNullOrEmpty() ? Model.TaskName : " Non-OASIS Recertification"%>  | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
     <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
     <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
     <%= Html.Hidden("AssessmentType", Model.TypeName, new { @class = "tabinput" })%>
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' name='{1}' area='{2}'>Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString(), area)%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>History &#38; Diagnoses</a>", area, AssessmentCategory.PatientHistory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Prognosis</a>", area, AssessmentCategory.Prognosis.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Supportive Assistance</a>", area, AssessmentCategory.SupportiveAssistance.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Sensory Status</a>", area, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Pain</a>", area, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Integumentary Status</a>", area, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Respiratory Status</a>", area, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Endocrine</a>", area, AssessmentCategory.Endocrine.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Cardiac</a>", area, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Elimination Status</a>", area, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Nutrition</a>", area, AssessmentCategory.Nutrition.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Neuro/Behavioral</a>", area, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>ADL/IADLs</a>", area, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Supplies Worksheet</a>", area, AssessmentCategory.SuppliesDme.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Medications</a>", area, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Therapy Need &#38; Plan of Care</a>", area, AssessmentCategory.TherapyNeed.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Discipline &#38; Treatment</a>", area, AssessmentCategory.OrdersDisciplineTreatment.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="tab-content">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
</div>