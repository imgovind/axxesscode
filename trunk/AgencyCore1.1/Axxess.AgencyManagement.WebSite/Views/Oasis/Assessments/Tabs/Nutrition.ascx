<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "NutritionForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Nutrition.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Nutrition</legend>
        <%  string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutrition", "", new { @id = Model.TypeName + "_GenericNutritionHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Within Normal Limits">
                            <%= string.Format("<input id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition1">WNL (Within Normal Limits)</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Dysphagia">
                            <%= string.Format("<input id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition2">Dysphagia</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Appetite">
                            <%= string.Format("<input id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition3">Appetite</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Weight">
                            <%= string.Format("<input id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition4">Weight Change</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionWeightGainLoss" class="fl">Specify</label>
                            <div class="fr">
                                <%  var GenericNutritionWeightGainLoss = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Loss", Value = "Loss" },
                                        new SelectListItem { Text = "Gain ", Value = "Gain" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNutritionWeightGainLoss", GenericNutritionWeightGainLoss, new { @id = Model.TypeName + "_GenericNutritionWeightGainLoss", @status = "(Optional) Nutrition, Weight Loss/Gain" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Diet">
                            <%= string.Format("<input id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition5">Diet</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionDietAdequate" class="fl">Adequate?</label>
                            <div class="fr">
                                <%  var GenericNutritionDietAdequate = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Adequate", Value = "Adequate" },
                                        new SelectListItem { Text = "Inadequate ", Value = "Inadequate" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericNutritionDietAdequate"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNutritionDietAdequate", GenericNutritionDietAdequate, new { @id = Model.TypeName + "_GenericNutritionDietAdequate", @status = "(Optional) Nutrition, Adequate Diet" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Diet Type">
                            <%= string.Format("<input id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition9">Diet Type</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionDietType" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.TypeName + "_GenericNutritionDietType", @maxlength = "100", @title = "(Optional) Nutrition, Diet Type" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding">
                            <%= string.Format("<input id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition6">Enteral Feeding</label>
                        </div>
                        <div class="more">
                            <%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionEnteralFeeding", "", new { @id = Model.TypeName + "_GenericNutritionEnteralFeedingHidden" })%>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, NG">
                                        <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding1">NG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, PEG">
                                        <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding2">PEG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, Dobhoff">
                                        <%= string.Format("<input title='' id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding3">Dobhoff</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Tube Placement Checked">
                            <%= string.Format("<input id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition7">Tube Placement Checked</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Residual Checked">
                            <%= string.Format("<input id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("8").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition8" class="inline-radio">Residual Checked</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount" class="fl">Amount</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.TypeName + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.TypeName + "_GenericNutritionResidualCheckedAmount", @class = "shorter numeric", @maxlength = "5", @title = "(Optional) Nutrition, Residual Checked Amount" })%>
                                <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">ml</label>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNutritionComments", data.AnswerOrEmptyString("GenericNutritionComments"), 10, 50, new { @id = Model.TypeName + "_GenericNutritionCommentsComments", @title = "(Optional) Nutrition Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset id="<%= Model.TypeName %>_NutritionHealthScreen">
        <legend>Nutritional Health Screen</legend>
        <%  string[] genericNutritionalHealth = data.AnswerArray("GenericNutritionalHealth"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutritionalHealth", "", new { @id = Model.TypeName + "_GenericNutritionalHealthHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "1", genericNutritionalHealth.Contains("1"), "<span class=\"fr\">15</span>Without reason, has lost more than 10 lbs, in the last 3 months") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "2", genericNutritionalHealth.Contains("2"), "<span class=\"fr\">10</span>Has an illness or condition that made pt change the type and/or amount of food eaten") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "3", genericNutritionalHealth.Contains("3"), "<span class=\"fr\">10</span>Has open decubitus, ulcer, burn or wound") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "4", genericNutritionalHealth.Contains("4"), "<span class=\"fr\">10</span>Eats fewer than 2 meals a day") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "5", genericNutritionalHealth.Contains("5"), "<span class=\"fr\">10</span>Has a tooth/mouth problem that makes it hard to eat") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "6", genericNutritionalHealth.Contains("6"), "<span class=\"fr\">10</span>Has 3 or more drinks of beer, liquor or wine almost every day") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "7", genericNutritionalHealth.Contains("7"), "<span class=\"fr\">10</span>Does not always have enough money to buy foods needed") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "8", genericNutritionalHealth.Contains("8"), "<span class=\"fr\">5</span>Eats few fruits or vegetables, or milk products") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "9", genericNutritionalHealth.Contains("9"), "<span class=\"fr\">5</span>Eats alone most of the time") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "10", genericNutritionalHealth.Contains("10"), "<span class=\"fr\">5</span>Takes 3 or more prescribed or OTC medications a day") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "11", genericNutritionalHealth.Contains("11"), "<span class=\"fr\">5</span>Is not always physically able to cook and/or feed self and has no caregiver to assist") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNutritionalHealth", "12", genericNutritionalHealth.Contains("12"), "<span class=\"fr\">5</span>Frequently has diarrhea or constipation") %>
                </ul>
            </div>
            <div class="row narrowest no-input ac">
                <%  string[] genericGoodNutritionScore = data.AnswerArray("GenericGoodNutritionScore"); %>
                <label>Total</label>
                <input type="text" class="shortest" readonly="readonly" value="0" id="<%= Model.TypeName %>_GenericGoodNutritionScore" name="<%= Model.TypeName %>_GenericGoodNutritionScore" />
                <ul class="score-list">
                    <li>56 &#8211; 100: High Nutritional Risk;</li>
                    <li>26 &#8211; 55: Moderate Nutritional Risk;</li>
                    <li>0 &#8211; 25: Good Nutritional Status</li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionalStatusComments">Nutritional Status Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNutritionalStatusComments", data.AnswerOrEmptyString("GenericNutritionalStatusComments"),10, 50, new { @id = Model.TypeName + "_GenericNutritionalStatusComments" })%></div>
            </div>
            <div class="row">
                <%  string[] genericNutritionDiffect = data.AnswerArray("GenericNutritionDiffect"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericNutritionDiffect", "", new { @id = Model.TypeName + "_GenericNutritionDiffectHidden" })%>
                <ul class="checkgroup two-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_GenericNutritionDiffect1' name='{0}_GenericNutritionDiffect' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionDiffect.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutritionDiffect1">Non-compliant with prescribed diet</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='{0}_GenericNutritionDiffect2' name='{0}_GenericNutritionDiffect' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionDiffect.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutritionDiffect2">Over/under weight by 10%</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMealsPreparedBy">Meals prepared by</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericMealsPreparedBy", data.AnswerOrEmptyString("GenericMealsPreparedBy"), 10, 50, new { @id = Model.TypeName + "_GenericMealsPreparedBy" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Enter Physician&#8217;s Orders or Diet Requirements (Locator #16)</legend>
        <%  string[] nutritionalReqs = data.AnswerArray("485NutritionalReqs"); %>
        <%= Html.Hidden(Model.TypeName + "_485NutritionalReqs", "", new { @id = Model.TypeName + "_485NutritionalReqsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Regular">
                            <%= string.Format("<input id='{0}_485NutritionalReqs1' name='{0}_485NutritionalReqs' value='1' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs1">Regular</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Mechanical Soft">
                            <%= string.Format("<input id='{0}_485NutritionalReqs2' name='{0}_485NutritionalReqs' value='2' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs2">Mechanical Soft</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Heart Healthy">
                            <%= string.Format("<input id='{0}_485NutritionalReqs3' name='{0}_485NutritionalReqs' value='3' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs3">Heart Healthy</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Low Cholesterol">
                            <%= string.Format("<input id='{0}_485NutritionalReqs4' name='{0}_485NutritionalReqs' value='4' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs4">Low Cholesterol</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Low Fat">
                            <%= string.Format("<input id='{0}_485NutritionalReqs5' name='{0}_485NutritionalReqs' value='5' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs5">Low Fat</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Sodium">
                            <%= string.Format("<input id='{0}_485NutritionalReqs6' name='{0}_485NutritionalReqs' value='6' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs6">Sodium</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_485NutritionalReqsSodiumAmount">Amount</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsSodiumAmount", data.AnswerOrDefault("485NutritionalReqsSodiumAmount", "Low"), new { @id = Model.TypeName + "_485NutritionalReqsSodiumAmount", @maxlength = "10", @title = "(485 Locator 16) Nutritional Requirements, Sodium Amount" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, No Added Salt">
                            <%= string.Format("<input id='{0}_485NutritionalReqs7' name='{0}_485NutritionalReqs' value='7' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs7">No Added Salt</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Calorie ADA Diet">
                            <%= string.Format("<input id='{0}_485NutritionalReqs8' name='{0}_485NutritionalReqs' value='8' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("8").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs8">Calorie ADA Diet</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485NutritionalReqsCalorieADADietAmount" class="fl">Amount</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsCalorieADADietAmount", data.AnswerOrDefault("485NutritionalReqsCalorieADADietAmount", "Low"), new { @id = Model.TypeName + "_485NutritionalReqsCalorieADADietAmount", @maxlength = "10", @title = "(485 Locator 16) Nutritional Requirements, Calorie ADA Diet Amount" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, No Concentrated Sweets">
                            <%= string.Format("<input id='{0}_485NutritionalReqs9' name='{0}_485NutritionalReqs' value='9' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs9">No Concentrated Sweets</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Coumadin Diet">
                            <%= string.Format("<input id='{0}_485NutritionalReqs10' name='{0}_485NutritionalReqs' value='10' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("10").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs10">Coumadin Diet</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Renal Diet">
                            <%= string.Format("<input id='{0}_485NutritionalReqs11' name='{0}_485NutritionalReqs' value='11' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("11").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs11">Renal Diet</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Other">
                            <%= string.Format("<input id='{0}_485NutritionalReqs12' name='{0}_485NutritionalReqs' value='12' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("12").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs12">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485NutritionalReqsPhyDietOtherName" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsPhyDietOtherName", data.AnswerOrEmptyString("485NutritionalReqsPhyDietOtherName"), new { @id = Model.TypeName + "_485NutritionalReqsPhyDietOtherName", @maxlength = "20", status = "(485 Locator 16) Nutritional Requirements, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Supplement">
                            <%= string.Format("<input id='{0}_485NutritionalReqs13' name='{0}_485NutritionalReqs' value='13' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("13").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs13">Supplement</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485NutritionalReqsSupplementType" class="fl">Type</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsSupplementType", data.AnswerOrEmptyString("485NutritionalReqsSupplementType"), new { @id = Model.TypeName + "_485NutritionalReqsSupplementType", @maxlength = "20", @title = "(485 Locator 16) Nutritional Requirements, Supplement Type" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Fluid Restriction">
                            <%= string.Format("<input id='{0}_485NutritionalReqs14' name='{0}_485NutritionalReqs' value='14' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("14").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs14">Fluid Restriction</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485NutritionalReqsFluidResAmount" class="fl">Amount</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsFluidResAmount", data.AnswerOrEmptyString("485NutritionalReqsFluidResAmount"), new { @id = Model.TypeName + "_485NutritionalReqsFluidResAmount", @class = "shorter numeric", @maxlength = "5", @title = "(485 Locator 16) Nutritional Requirements, Fluid Restriction, Amount" })%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsFluidResAmount">ml/24 hours</label>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, TPN">
                            <%= string.Format("<input id='{0}_485NutritionalReqs16' name='{0}_485NutritionalReqs' value='16' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("16").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs16">TPN</label>
                        </div>
                        <div class="more ac">
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsTPNAmount", data.AnswerOrEmptyString("485NutritionalReqsTPNAmount"), new { @id = Model.TypeName + "_485NutritionalReqsTPNAmount", @class = "shorter", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, TPN, Amount" })%>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsTPNAmount">@ml/hr</label>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsTPNVia">via</label>
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsTPNVia", data.AnswerOrEmptyString("485NutritionalReqsTPNVia"), new { @id = Model.TypeName + "_485NutritionalReqsTPNVia", @class = "short", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, TPN, Via" })%>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition">
                            <%= string.Format("<input id='{0}_485NutritionalReqs15' name='{0}_485NutritionalReqs' value='15' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("15").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs15">Enteral Nutrition</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralDesc" class="fl">Formula</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralDesc", data.AnswerOrEmptyString("485NutritionalReqsEnteralDesc"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralDesc", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Formula" })%></div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralAmount" class="fl">Amount</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralAmount", data.AnswerOrEmptyString("485NutritionalReqsEnteralAmount"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralAmount", @class = "shorter numeric", @maxlength = "5", @title = "(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Amount" })%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralAmount">ml/hr</label>
                            </div>
                            <div class="clr"></div>
                            <label>Per</label>
                            <%  string[] nutritionalReqsEnteral = data.AnswerArray("485NutritionalReqsEnteral"); %>
                            <%= Html.Hidden(Model.TypeName + "_485NutritionalReqsEnteral", "", new { @id = Model.TypeName + "_485NutritionalReqsEnteralHidden" })%>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per PEG">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralPer1' name='{0}_485NutritionalReqsEnteral' value='1' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("1").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer1">PEG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per NG">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralPer2' name='{0}_485NutritionalReqsEnteral' value='2' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("2").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer2">NG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per Dobhoff">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralPer3' name='{0}_485NutritionalReqsEnteral' value='3' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("3").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer3">Dobhoff</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per Other">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralPer4' name='{0}_485NutritionalReqsEnteral' value='4' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("4").ToChecked())%>
                                        <span>
                                            <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer4">Other</label>
                                            <div id="<%= Model.TypeName %>_485NutritionalReqsEnteralPer4More" class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralOtherName", data.AnswerOrEmptyString("485NutritionalReqsEnteralOtherName"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralOtherName", @maxlength = "20" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                            </ul>
                            <label>Via</label>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Pump">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralVia1' name='{0}_485NutritionalReqsEnteral' value='5' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("5").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia1">Pump</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Gravity">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralVia2' name='{0}_485NutritionalReqsEnteral' value='6' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("6").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia2">Gravity</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Bolus">
                                        <%= string.Format("<input id='{0}_485NutritionalReqsEnteralVia3' name='{0}_485NutritionalReqsEnteral' value='7' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("7").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia3">Bolus</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Nutrition.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>