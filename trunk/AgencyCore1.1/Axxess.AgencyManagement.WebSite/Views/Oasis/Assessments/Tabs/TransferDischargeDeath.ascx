<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasis" + Model.TypeName + "DeathForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden("categoryType", AssessmentCategory.TransferDischargeDeath.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>    
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset class="oasis">
        <legend>
        <%  if (Model.AssessmentTypeNum.ToInteger() == 6 || Model.AssessmentTypeNum.ToInteger() == 7) { %>
            Transfer
        <%  } else if (Model.AssessmentTypeNum.ToInteger() == 8) { %>
            Death
        <%  } else { %>
            Discharge
        <%  } %>
        </legend>
        <%  if (Model.AssessmentTypeNum != "08") { %>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M2400">
                <label>
                    <a class="green" onclick="Oasis.Help('M2400')">(M2400)</a>
                    Intervention Synopsis: Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?
                </label>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">a.</span>
                    <span class="margin">Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400DiabeticFootCare", "", new { @id = Model.TypeName + "_M2400DiabeticFootCareHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DiabeticFootCare", "00", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DiabeticFootCare", "01", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("01"), "1", "Yes") %>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DiabeticFootCare", "NA", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("NA"), "NA", "Patient is not diabetic or is bilateral amputee")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">b.</span>
                    <span class="margin">Falls prevention interventions</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400FallsPreventionInterventions", "", new { @id = Model.TypeName + "_M2400FallsPreventionInterventionsHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400FallsPreventionInterventions", "00", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400FallsPreventionInterventions", "01", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400FallsPreventionInterventions", "NA", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("NA"), "NA", "Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">c.</span>
                    <span class="margin">Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400DepressionIntervention", "", new { @id = Model.TypeName + "_M2400DepressionInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DepressionIntervention", "00", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DepressionIntervention", "01", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400DepressionIntervention", "NA", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("NA"), "NA", "Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">d.</span>
                    <span class="margin">Intervention(s) to monitor and mitigate pain</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400PainIntervention", "", new { @id = Model.TypeName + "_M2400PainInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PainIntervention", "00", data.AnswerOrEmptyString("M2400PainIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PainIntervention", "01", data.AnswerOrEmptyString("M2400PainIntervention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PainIntervention", "NA", data.AnswerOrEmptyString("M2400PainIntervention").Equals("NA"), "NA", "Formal assessment did not indicate pain since the last OASIS assessment")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">e.</span>
                    <span class="margin">Intervention(s) to prevent pressure ulcers</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400PressureUlcerIntervention", "", new { @id = Model.TypeName + "_M2400PressureUlcerInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerIntervention", "00", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerIntervention", "01", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerIntervention", "NA", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("NA"), "NA", "Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">f.</span>
                    <span class="margin">Pressure ulcer treatment based on principles of moist wound healing</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2400PressureUlcerTreatment", "", new { @id = Model.TypeName + "_M2400PressureUlcerTreatmentHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerTreatment", "00", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerTreatment", "01", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2400PressureUlcerTreatment", "NA", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("NA"), "NA", "Dressings that support the principles of moist wound healing not indicated for this patient&#8217;s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing")%>
                </ul>
            </div>
            <div class="row no-input">
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2400')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2410">
                <label>
                    <a class="green" onclick="Oasis.Help('M2410')">(M2410)</a>
                    To which Inpatient Facility has the patient been admitted?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2410TypeOfInpatientFacility", "", new { @id = Model.TypeName + "_M2410TypeOfInpatientFacilityHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2410TypeOfInpatientFacility", "01", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("01"), "1", "Hospital")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2410TypeOfInpatientFacility", "02", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("02"), "2", "Rehabilitation facility")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2410TypeOfInpatientFacility", "03", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("03"), "3", "Nursing home")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2410TypeOfInpatientFacility", "04", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("04"), "4", "Hospice")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2410TypeOfInpatientFacility", "NA", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("NA"), "NA", "No inpatient facility admission")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2410')">?</a></div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M2420">
                <label>
                    <a class="green" onclick="Oasis.Help('M2420')">(M2420)</a>
                    Discharge Disposition: Where is the patient after discharge from your agency?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2420DischargeDisposition", "", new { @id = Model.TypeName + "_M2420DischargeDispositionHidden" })%>
                <ul class="one-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2420DischargeDisposition", "01", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("01"), "1", "Patient remained in the community (without formal assistive services)")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2420DischargeDisposition", "02", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("02"), "2", "Patient remained in the community (with formal assistive services)")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2420DischargeDisposition", "03", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("03"), "3", "Patient transferred to a non-institutional hospice")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2420DischargeDisposition", "04", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("04"), "4", "Unknown because patient moved to a geographic location not served by this agency")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2420DischargeDisposition", "UK", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("UK"), "UK", "Other unknown")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2420')">?</a></div>
                </div>
            </div>
            <%  } %>
	        <%  if (Model.AssessmentTypeNum.ToInteger() == 6 || Model.AssessmentTypeNum.ToInteger() == 7) { %>
            <div class="row" id="<%= Model.TypeName %>_M2430">
                <label>
                    <a class="green" onclick="Oasis.Help('M2430')">(M2430)</a>
                    Reason for Hospitalization: For what reason(s) did the patient require hospitalization?
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMed", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMedHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationMed", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMed").Equals("1"), "1", "Improper medication administration, medication side effects, toxicity, anaphylaxis")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationFall", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationFallHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationFall", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationFall").Equals("1"), "2", "Injury caused by fall")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationInfection", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationInfectionHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationInfection", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationInfection").Equals("1"), "3", "Respiratory infection (e.g., pneumonia, bronchitis)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationOtherRP", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationOtherRPHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationOtherRP", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationOtherRP").Equals("1"), "4", "Other respiratory problem")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHeartFail", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHeartFailHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationHeartFail", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartFail").Equals("1"), "5", "Heart failure (e.g., fluid overload)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationCardiac", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationCardiacHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationCardiac", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationCardiac").Equals("1"), "6", "Cardiac dysrhythmia (irregular heartbeat)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMyocardial", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMyocardialHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationMyocardial", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMyocardial").Equals("1"), "7", "Myocardial infarction or chest pain")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHeartDisease", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHeartDiseaseHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationHeartDisease", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartDisease").Equals("1"), "8", "Other heart disease")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationStroke", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationStrokeHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationStroke", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationStroke").Equals("1"), "9", "Stroke (CVA) or TIA")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHypo", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHypoHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationHypo", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationHypo").Equals("1"), "10", "Hypo/Hyperglycemia, diabetes out of control")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationGI", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationGIHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationGI", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationGI").Equals("1"), "11", "GI bleeding, obstruction, constipation, impaction")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationDehMal", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationDehMalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationDehMal", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationDehMal").Equals("1"), "12", "Dehydration, malnutrition")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUrinaryInf", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUrinaryInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationUrinaryInf", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUrinaryInf").Equals("1"), "13", "Urinary tract infection")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationIV", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationIVHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationIV", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationIV").Equals("1"), "14", "IV catheter-related infection or complication")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationWoundInf", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationWoundInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationWoundInf", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationWoundInf").Equals("1"), "15", "Wound infection or deterioration")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUncontrolledPain", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUncontrolledPainHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationUncontrolledPain", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUncontrolledPain").Equals("1"), "16", "Uncontrolled pain")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMental", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMentalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationMental", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationMental").Equals("1"), "17", "Acute mental/behavioral health problem")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationDVT", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationDVTHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationDVT", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationDVT").Equals("1"), "18", "Deep vein thrombosis, pulmonary embolus")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationScheduled", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationScheduledHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationScheduled", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationScheduled").Equals("1"), "19", "Scheduled treatment or procedure")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationOther", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationOtherHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationOther", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationOther").Equals("1"), "20", "Other than above reasons")%>
                    <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUK", string.Empty, new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUKHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2430ReasonForHospitalizationUK", "1", data.AnswerOrEmptyString("M2430ReasonForHospitalizationUK").Equals("1"), "UK", "Reason unknown")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2430')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2440">
                <label>
                    <a class="green" onclick="Oasis.Help('M2440')">(M2440)</a>
                    For what Reason(s) was the patient Admitted to a Nursing Home?
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedTherapy", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedTherapyHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedTherapy", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedTherapy").Equals("1"), "1", "Therapy services")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedRespite", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedRespiteHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedRespite", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedRespite").Equals("1"), "2", "Respite care")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedHospice", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedHospiceHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedHospice", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedHospice").Equals("1"), "3", "Hospice care")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedPermanent", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedPermanentHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedPermanent", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedPermanent").Equals("1"), "4", "Permanent placement")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedUnsafe", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedUnsafeHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedUnsafe", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnsafe").Equals("1"), "5", "Unsafe for care at home")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedOther", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedOtherHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedOther", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedOther").Equals("1"), "6", "Other")%>
                    <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedUnknown", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedUnknownHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2440ReasonPatientAdmittedUnknown", "1", data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnknown").Equals("1"), "UK", "Unknown")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2440')">?</a></div>
                </div>
            </div>
	        <%  } %>
        </div>
        <%  } %>
        <div class="clr"></div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0903">
                <label for="<%= Model.TypeName %>_M0903LastHomeVisitDate" class="fl">
                    <a class="green" onclick="Oasis.Help('M0903')">(M0903)</a>
                    Date of Last (Most Recent) Home Visit
                </label>
                <div class="fr">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0903LastHomeVisitDate" value="<%= data.AnswerOrEmptyString("M0903LastHomeVisitDate") %>" id="<%= Model.TypeName %>_M0903LastHomeVisitDate" />
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0903')">?</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0906">
                <label for="<%= Model.TypeName %>_M0906DischargeDate" class="fl">
                    <a class="green" onclick="Oasis.Help('M0906')">(M0906)</a>
                    Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.
                </label>
                <div class="fr">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0906DischargeDate" value="<%= data.AnswerOrEmptyString("M0906DischargeDate") %>" id="<%= Model.TypeName %>_M0906DischargeDate" />
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0906')">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNarrativeTemplate">Narrative</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.TypeName + "_GenericDischargeNarrativeTemplate") %>
                    <%= Html.TextArea(Model.TypeName + "_GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), new { @id = Model.TypeName + "_GenericDischargeNarrative", @status = "(Optional) Narrative" })%>
                </div>
            </div>
        </div>
    </fieldset>
        <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>