<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var area = Model.Service.ToArea(); %>
<%  var service = area.ToEnum(AgencyServices.HomeHealth); %>
<div class="wrapper main">
<%  if (Model.IsUserCanSeeSticky) { %>
	<%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.Id, Model.PatientId, Model.EpisodeId, Model.StatusComment, service)) %>
<%  } %>
<%  if (Model.IsUserCanLoadPrevious) { %>
    <fieldset>
        <legend>Previous Assessments</legend>
        <div class="wide-column">
            <div class="row narrower ac">
                <label for="<%= Model.TypeName %>_PreviousAssessments" class="fl">Select Assessment</label>
                <div class="fr"><%= Html.PreviousAssessments((int)service, Model.PatientId, Model.Id, Model.AssessmentTypeNum.ToInteger(), Model.ScheduleDate, new { @id = Model.TypeName + "_PreviousAssessments" })%></div>
                <div class="clr"/>
                <div class="button"><a id="<%= Model.TypeName %>_PreviousAssessmentButton">Load Assessment</a></div>
            </div>
        </div>
    </fieldset>
<%  } %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "DemographicsForm" })) { %>
    <%  var data = Model.ToDictionary(); %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", AssessmentCategory.Demographics.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset class="oasis loc485">
        <legend>Patient Information</legend>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0020">
                <label for="<%= Model.TypeName %>_M0020PatientIdNumber" class="fl">
                    <a class="green" onclick="Oasis.Help('M0020')" status="More Information about M0020">(M0020)</a>
                    ID Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0020PatientIdNumber", data.AnswerOrEmptyString("M0020PatientIdNumber"), new { @id = Model.TypeName + "_M0020PatientIdNumber", @status = "(OASIS M0020) ID Number", @maxlength = "15" }) %>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0020')" status="More Information about M0020">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0040">
                <label for="<%= Model.TypeName %>_M0040FirstName" class="fl">
                    <a class="green" onclick="Oasis.Help('M0040')" status="More Information about M0040">(M0040)</a>
                    First Name
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040FirstName", data.AnswerOrEmptyString("M0040FirstName"), new { @id = Model.TypeName + "_M0040FirstName", @status = "(OASIS M0040) First Name" }) %>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0040')" status="More Information about M0040">?</a></div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_M0040MI" class="fl">
                    <a class="green" onclick="Oasis.Help('M0040')" status="More Information about M0040">(M0040)</a>
                    MI
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040MI", data.AnswerOrEmptyString("M0040MI"), new { @id = Model.TypeName + "_M0040MI", @status = "(OASIS M0040) Middle Initial", @class = "shortest", @maxlength = "1" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0040')" status="More Information about M0040">?</a></div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_M0040LastName" class="fl">
                    <a class="green" onclick="Oasis.Help('M0040')" status="More Information about M0040">(M0040)</a>
                    Last Name
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040LastName", data.AnswerOrEmptyString("M0040LastName"), new { @id = Model.TypeName + "_M0040LastName", @status = "(OASIS M0040) Last Name" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0040')" status="More Information about M0040">?</a></div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_M0040Suffix" class="fl">
                    <a class="green" onclick="Oasis.Help('M0040')" status="More Information about M0040">(M0040)</a>
                    Suffix
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040Suffix", data.AnswerOrEmptyString("M0040Suffix"), new { @id = Model.TypeName + "_M0040Suffix", @status = "(OASIS M0040) Suffix", @class = "shortest", @maxlength = "4" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0040')" status="More Information about M0040">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0050">
                <label for="<%= Model.TypeName %>_M0050PatientState" class="fl">
                    <a class="green" onclick="Oasis.Help('M0050')" status="More Information about M0050">(M0050)</a>
                    State
                </label>
                <div class="fr oasis">
                    <%= Html.States(Model.TypeName + "_M0050PatientState", data.AnswerOrEmptyString("M0050PatientState"), new { @id = Model.TypeName + "_M0050PatientState", @status = "(OASIS M0050) State", @class = "address-state" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0050')" status="More Information about M0050">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0060">
                <label for="<%= Model.TypeName %>_M0060PatientZipCode" class="fl">
                    <a class="green" onclick="Oasis.Help('M0060')" status="More Information about M0060">(M0060)</a>
                    Zip
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0060PatientZipCode", data.AnswerOrEmptyString("M0060PatientZipCode"), new { @id = Model.TypeName + "_M0060PatientZipCode", @status = "(OASIS M0060) Zip Code", @class = "numeric shorter", @maxlength = "9" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0060')" status="More Information about M0060">?</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0063">
                <label for="<%= Model.TypeName %>_M0063PatientMedicareNumber" class="fl">
                    <a class="green" onclick="Oasis.Help('M0063')" status="More Information about M0063">(M0063)</a>
                    Medicare Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0063PatientMedicareNumber", data.AnswerOrEmptyString("M0063PatientMedicareNumber"), new { @id = Model.TypeName + "_M0063PatientMedicareNumber", @status = "(OASIS M0063) Medicare Number", @maxlength = "12" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0063PatientMedicareNumberUnknown", " ", new { @id = Model.TypeName + "_M0063PatientMedicareNumberUnknownHidden" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0063')" status="More Information about M0063">?</a></div>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.TypeName + "_M0063PatientMedicareNumberUnknown", "1", data.AnswerOrEmptyString("M0063PatientMedicareNumberUnknown").Equals("1"), "NA", "No Medicare") %>
                    </ul>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0064">
                <label for="<%= Model.TypeName %>_M0064PatientSSN" class="fl">
                    <a class="green" onclick="Oasis.Help('M0064')" status="More Information about M0064">(M0064)</a>
                    Social Security Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0064PatientSSN", data.AnswerOrEmptyString("M0064PatientSSN"), new { @id = Model.TypeName + "_M0064PatientSSN", @status = "(OASIS M0064) Social Security Number", @maxlength = "9" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0064PatientSSNUnknown", " ", new { @id = Model.TypeName + "_M0064PatientSSNUnknownHidden" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0064')" status="More Information about M0064">?</a></div>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.TypeName + "_M0064PatientSSNUnknown", "1", data.AnswerOrEmptyString("M0064PatientSSNUnknown").Equals("1"), "UK", "Unknown")%>
                    </ul>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0065">
                <label for="<%= Model.TypeName %>_M0065PatientMedicaidNumber" class="fl">
                    <a class="green" onclick="Oasis.Help('M0065')" status="More Information about M0065">(M0065)</a>
                    Medicaid Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0065PatientMedicaidNumber", data.AnswerOrEmptyString("M0065PatientMedicaidNumber"), new { @id = Model.TypeName + "_M0065PatientMedicaidNumber", @status = "(OASIS M0065) Medicaid Number", @maxlength = "14" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0065PatientMedicaidNumberUnknown", " ", new { @id = Model.TypeName + "_M0065PatientMedicaidNumberUnknownHidden" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0065')" title="More Information about M0065">?</a></div>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.TypeName + "_M0065PatientMedicaidNumberUnknown", "1", data.AnswerOrEmptyString("M0065PatientMedicaidNumberUnknown").Equals("1"), "NA", "No Medicaid")%>
                    </ul>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0066">
                <label for="<%= Model.TypeName %>_M0066PatientDoB" class="fl">
                    <a class="green" onclick="Oasis.Help('M0066')" status="More Information about M0066">(M0066)</a>
                    Birth Date
                </label>
                <div class="fr oasis">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0066PatientDoB" value="<%= data.AnswerOrEmptyString("M0066PatientDoB") %>" id="<%= Model.TypeName %>_M0066PatientDoB" status="(OASIS M0066) Birth Date" />
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0066')" title="More Information about M0066">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0069">
                <label class="fl">
                    <a class="green" onclick="Oasis.Help('M0069')" status="More Information about M0069">(M0069)</a>
                    Gender
                </label>
                <%= Html.Hidden(Model.TypeName + "_M0069Gender", " ", new { }) %>
                <div class="fr oasis">
                    <%  var M0069Gender = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Male", Value = "1" },
                            new SelectListItem { Text = "Female", Value = "2" }
                        }, "Value", "Text", data.AnswerOrEmptyString("M0069Gender")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M0069Gender", M0069Gender)%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0069')" status="More Information about M0069">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis loc485">
        <legend>Episode Information</legend>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0030">
                <label for="<%= Model.TypeName %>_M0030SocDate" class="fl">
                    <a class="green" onclick="Oasis.Help('M0030')" status="More Information about M0030">(M0030)</a>
                    Start of Care Date
                </label>
                <div class="fr oasis">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0030SocDate" value="<%= data.AnswerOrEmptyString("M0030SocDate") %>" id="<%= Model.TypeName %>_M0030SocDate" status="(OASIS M0030) Start of Care Date" />
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0030')" status="More Information about M0030">?</a></div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericEpisodeStartDate" class="fl">Episode Start Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericEpisodeStartDate" value="<%= data.AnswerOrEmptyString("GenericEpisodeStartDate") %>" id="<%= Model.TypeName %>_GenericEpisodeStartDate" status="(Optional) Episode Start Date" /></div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0032">
                <label for="<%= Model.TypeName %>_M0032ROCDate" class="fl">
                    <a class="green" onclick="Oasis.Help('M0032')" status="More Information about M0032">(M0032)</a>
                    Resumption of Care Date
                </label>
                <div class="fr oasis">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0032ROCDate" value="<%= data.AnswerOrEmptyString("M0032ROCDate") %>" id="<%= Model.TypeName %>_M0032ROCDate" status="(OASIS M0032) Resumption of Care Date" />
                    <%= Html.Hidden(Model.TypeName + "_M0032ROCDateNotApplicable", " ", new { @id = Model.TypeName + "_M0032ROCDateNotApplicableHidden" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0032')" status="More Information about M0032">?</a></div>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.TypeName + "_M0032ROCDateNotApplicable", "1", data.AnswerOrEmptyString("M0032ROCDateNotApplicable").Equals("1"), "NA", "Not Applicable")%>
                    </ul>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0090">
                <label for="<%= Model.TypeName %>_M0090AssessmentCompleted" class="fl">
                    <a class="green" onclick="Oasis.Help('M0090')" status="More Information about M0090">(M0090)</a>
                    Date Assessment Completed
                </label>
                <div class="fr oasis">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0090AssessmentCompleted" value="<%= data.AnswerOrEmptyString("M0090AssessmentCompleted") %>" id="<%= Model.TypeName %>_M0090AssessmentCompleted" status="(OASIS M0090) Date Assessment Completed" />
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0090')" status="More Information about M0090">?</a></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0080">
                <label for="<%= Model.TypeName %>_M0080DisciplinePerson" class="fl">
                    <a class="green" onclick="Oasis.Help('M0080')" status="More Information about M0080">(M0080)</a>
                    Discipline of Person Completing Assessment
                </label>
                <div class="fr oasis">
                    <%= Html.Hidden(Model.TypeName + "_M0080DisciplinePerson")%>
                    <%  var DisciplinePersonCompletingAssessment = new SelectList(new[] {
                            new SelectListItem { Text = "1 - RN", Value = "01" },
                            new SelectListItem { Text = "2 - PT", Value = "02" },
                            new SelectListItem { Text = "3 - SLP/ST", Value = "03"},
                            new SelectListItem { Text = "4 - OT", Value = "04" }
                        }, "Value", "Text", data.AnswerOrDefault("M0080DisciplinePerson", "01")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M0080DisciplinePerson", DisciplinePersonCompletingAssessment)%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0080')" status="More Information about M0080">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0010">
                <label for="<%= Model.TypeName %>_M0010CertificationNumber" class="fl">
                    <a class="green" onclick="Oasis.Help('M0010')" status="More Information about M0010">(M0010)</a>
                    CMS Certification Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0010CertificationNumber", data.AnswerOrEmptyString("M0010CertificationNumber"), new { @id = Model.TypeName + "_M0010CertificationNumber", @status = "(OASIS M0010) CMS Certification Number" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0010')" status="More Information about M0010">?</a></div>
                </div>
            </div>
            <div class="row hidden" id="<%= Model.TypeName %>_M0014">
                <label for="<%= Model.TypeName %>_M0014BranchState" class="fl">
                    <a class="green" onclick="Oasis.Help('M0014')" status="More Information about M0014">(M0014)</a>
                    Branch State
                </label>
                <div class="fr oasis">
                    <%= Html.States( Model.TypeName + "_M0014BranchState", data.AnswerOrEmptyString("M0014BranchState"), new { @id = Model.TypeName + "_M0014BranchState", @status = "(OASIS M0014) Branch State", @class = "address-state" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0014')" status="More Information about M0014">?</a></div>
                </div>
            </div>
            <div class="row hidden" id="<%= Model.TypeName %>_M0016">
                <label for="<%= Model.TypeName %>_M0016BranchId" class="fl">
                    <a class="green" onclick="Oasis.Help('M0016')" status="More Information about M0016">(M0016)</a>
                    Branch ID Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0016BranchId", data.AnswerOrEmptyString("M0016BranchId"), new { @id = Model.TypeName + "_M0016BranchId", @status = "(OASIS M0016) Branch ID Number" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0016')" status="More Information about M0016">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0018">
                <label for="<%= Model.TypeName %>_M0018NationalProviderId" class="fl">
                    <a class="green" onclick="Oasis.Help('M0018')" status="More Information about M0018">(M0018)</a>
                    Physician NPI Number
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0018NationalProviderId", data.AnswerOrEmptyString("M0018NationalProviderId"), new { @id = Model.TypeName + "_M0018NationalProviderId", @status = "(OASIS M0018) National Provider Identifier" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0018NationalProviderIdUnknown", " ", new { @id = Model.TypeName + "_M0018NationalProviderIdUnknownHidden" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0018')" status="More Information about M0018">?</a></div>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupOption(Model.TypeName + "_M0018NationalProviderIdUnknown", "1", data.AnswerOrEmptyString("M0018NationalProviderIdUnknown").Equals("1"), "UK", "Unknown")%>
                    </ul>
                </div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
	<div class="inline-fieldset two-wide">
		<div>
	    <%  } %>
			<fieldset class="oasis">
			    <legend>Assessment Information</legend>
			    <div class="wide-column" id="<%= Model.TypeName %>_M0100">
			        <%= Html.Hidden(Model.TypeName + "_M0100AssessmentType", Model.AssessmentTypeNum, new { @id = Model.TypeName + "_M0100AssessmentTypeHidden" })%>
			        <div class="row">
			            <label>
    			            <a class="green" onclick="Oasis.Help('M0100')" status="More Information about M0100">(M0100)</a>
	    		            This assessment is currently being completed for the following reason
	    		        </label>
			            <ul class="checkgroup one-wide">
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("01"), "1", "Start of care &#8212; further visits planned")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("03"), "3", "Resumption of care (after inpatient stay)")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("04"), "4", "Recertification (follow-up) reassessment")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("05"), "5", "Other follow-up")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("06"), "6", "Transferred to an inpatient facility &#8212; patient not discharged from agency")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("07"), "7", "Transferred to an inpatient facility &#8212; patient discharged from agency")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("08"), "8", "Death at home")%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M0100AssessmentType", "", Model.AssessmentTypeNum.Equals("09"), "9", "Discharge from agency")%>
			            </ul>
                        <div class="fr oasis">
                            <div class="button oasis-help"><a onclick="Oasis.Help('M0100')" status="More Information about M0100">?</a></div>
                        </div>
			        </div>
                </div>
            </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
        </div>
        <div>
	        <fieldset class="oasis">
	            <legend>Dates/Timing</legend>
	            <div class="wide-column">
	    <%  } %>
	    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
	                <div class="row" id="<%= Model.TypeName %>_M0102">
	                    <label for="<%= Model.TypeName %>_M0102PhysicianOrderedDate">
                            <a class="green" onclick="Oasis.Help('M0102')" status="More Information about M0102">(M0102)</a>
                            Date of Physician-ordered Start/Resumption of Care
                        </label>
                        <p>
                            If the physician indicated a specific start of care (resumption of care) date when the patient was
                            referred for home health services, record the date specified.
                        </p>
                        <div class="fr oasis" id="<%= Model.TypeName %>_M0102PhysicianOrderedDateDiv">
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0102PhysicianOrderedDate" value="<%= data.AnswerOrEmptyString("M0102PhysicianOrderedDate") %>" id="<%= Model.TypeName %>_M0102PhysicianOrderedDate" status="(OASIS M0102) Date of Physician-ordered Start/Resumption of Care" />
                            <%= Html.Hidden(Model.TypeName + "_M0102PhysicianOrderedDateNotApplicable", " ", new { @id = Model.TypeName + "_M0102PhysicianOrderedDateNotApplicableHidden" })%>
                            <div class="button oasis-help"><a onclick="Oasis.Help('M0102')" status="More Information about M0102">?</a></div>
                        </div>
                        <div class="clr"></div>
                        <div class="fr">
                            <ul class="checkgroup one-wide">
                                <%= Html.CheckgroupOption(Model.TypeName + "_M0102PhysicianOrderedDateNotApplicable", "1", data.AnswerOrEmptyString("M0102PhysicianOrderedDateNotApplicable").Equals("1"), "NA", "No specific SOC date ordered")%>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="<%= Model.TypeName %>_M0104">
                        <label for="<%= Model.TypeName %>_M0104ReferralDate">
                            <a class="green" onclick="Oasis.Help('M0104')" status="More Information about M0104">(M0104)</a>
                            Date of Referral
                        </label>
                        <p>
                            Indicate the date that the written or verbal referral for initiation or resumption of care was
                            received by the HHA.
                        </p>
                        <div class="fr oasis">
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0104ReferralDate" value="<%= data.AnswerOrEmptyString("M0104ReferralDate") %>" id="<%= Model.TypeName %>_M0104ReferralDate" status="(OASIS M0104) Date of Referral" />
                            <div class="button oasis-help"><a onclick="Oasis.Help('M0104')" status="More Information about M0104">?</a></div>
                        </div>
                    </div>
        <%  } %>
	    <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
                    <div class="row" id="<%= Model.TypeName %>_M0110">
                        <label for="<%= Model.TypeName %>_M0110EpisodeTiming">
                            <a class="green" onclick="Oasis.Help('M0110')" status="More Information about M0110">(M0110)</a>
                            Episode Timing
                        </label>
                        <p>
                            Is the Medicare home health payment episode for which this assessment will define a case mix group
                            an &#8220;early&#8221; episode or a &#8220;later&#8221; episode in the patient&#8217;s current
                            sequence of adjacent Medicare home health payment episodes?
                        </p>
                        <div class="fr oasis">
                            <%= Html.Hidden(Model.TypeName + "_M0110EpisodeTiming")%>
                            <%  var EpisodeTiming = new SelectList(new[] {
                                    new SelectListItem { Text = "Early", Value = "01" },
                                    new SelectListItem { Text = "Later", Value = "02" },
                                    new SelectListItem { Text = "Unknown", Value = "UK"},
                                    new SelectListItem { Text = "Not Applicable/No Medicare case mix group", Value = "NA" }
                                }, "Value", "Text", data.AnswerOrDefault("M0110EpisodeTiming", "01")); %>
                            <%= Html.DropDownList(Model.TypeName + "_M0110EpisodeTiming", EpisodeTiming)%>
                            <div class="button oasis-help"><a onclick="Oasis.Help('M0110')" status="More Information about M0110">?</a></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
	    <%  } %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum != "08") { %>
    <fieldset class="oasis">
        <legend>Race/Ethnicity</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M0140">
                <label>
                    <a class="green" onclick="Oasis.Help('M0140')" status="More Information about M0140">(M0140)</a>
                    Race/Ethnicity
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceAMorAN", " ", new { @id = Model.TypeName + "_M0140RaceAMorANHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceAMorAN", "1", data.AnswerOrEmptyString("M0140RaceAMorAN").Equals("1"), "1", "American Indian or Alaska Native")%>
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceAsia", " ", new { @id = Model.TypeName + "_M0140RaceAsiaHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceAsia", "1", data.AnswerOrEmptyString("M0140RaceAsia").Equals("1"), "2", "Asian")%>
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceBalck", " ", new { @id = Model.TypeName + "_M0140RaceBalckHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceBalck", "1", data.AnswerOrEmptyString("M0140RaceBalck").Equals("1"), "3", "Black or African-American")%>
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceHispanicOrLatino", " ", new { @id = Model.TypeName + "_M0140RaceHispanicOrLatinoHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceHispanicOrLatino", "1", data.AnswerOrEmptyString("M0140RaceHispanicOrLatino").Equals("1"), "4", "Hispanic or Latino")%>
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceNHOrPI", " ", new { @id = Model.TypeName + "_M0140RaceNHOrPIHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceNHOrPI", "1", data.AnswerOrEmptyString("M0140RaceNHOrPI").Equals("1"), "5", "Native Hawaiian or Pacific Islander")%>
                    <%= Html.Hidden(Model.TypeName + "_M0140RaceWhite", " ", new { @id = Model.TypeName + "_M0140RaceWhiteHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0140RaceWhite", "1", data.AnswerOrEmptyString("M0140RaceWhite").Equals("1"), "6", "White")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0140')" status="More Information about M0140">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset class="oasis">
        <legend>Payment Source</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M0150">
                <label>
                    <a class="green" onclick="Oasis.Help('M0150')" status="More Information about M0150">(M0150)</a>
                    Current payment sources for home care
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceNone", " ", new { @id = Model.TypeName + "_M0150PaymentSourceNoneHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceNone", "1", data.AnswerOrEmptyString("M0150PaymentSourceNone").Equals("1"), "0", "None; no charge for current services", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCREFFS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCREFFSHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceMCREFFS", "1", data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").Equals("1"), "1", "Medicare <em>(Traditional Fee-for-Service)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCREHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCREHMOHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceMCREHMO", "1", data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").Equals("1"), "2", "Medicare <em>(HMO/Managed Care/Advantage Plan)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCAIDFFS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCAIDFFSHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceMCAIDFFS", "1", data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").Equals("1"), "3", "Medicaid <em>(Traditional Fee-for-Service)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMACIDHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMACIDHMOHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceMACIDHMO", "1", data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").Equals("1"), "4", "Medicaid <em>(HMO/Managed Care)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceWRKCOMP", " ", new { @id = Model.TypeName + "_M0150PaymentSourceWRKCOMPHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceWRKCOMP", "1", data.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").Equals("1"), "5", "Workers&#8217; Compensation", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceTITLPRO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceTITLPROHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceTITLPRO", "1", data.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").Equals("1"), "6", "Title Programs <em>(e.g., Title III, V, or XX)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceOTHGOVT", " ", new { @id = Model.TypeName + "_M0150PaymentSourceOTHGOVTHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceOTHGOVT", "1", data.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").Equals("1"), "7", "Other Government <em>(e.g., TriCare, VA, etc.)</em>", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourcePRVINS", " ", new { @id = Model.TypeName + "_M0150PaymentSourcePRVINSHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourcePRVINS", "1", data.AnswerOrEmptyString("M0150PaymentSourcePRVINS").Equals("1"), "8", "Private Insurance", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourcePRVHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourcePRVHMOHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourcePRVHMO", "1", data.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").Equals("1"), "9", "Private HMO/Managed Care", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceSelfPay", " ", new { @id = Model.TypeName + "_M0150PaymentSourceSelfPayHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceSelfPay", "1", data.AnswerOrEmptyString("M0150PaymentSourceSelfPay").Equals("1"), "10", "Self-Pay", new { @class = "M0150" })%>
                    <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceUnknown", " ", new { @id = Model.TypeName + "_M0150PaymentSourceUnknownHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M0150PaymentSourceUnknown", "1", data.AnswerOrEmptyString("M0150PaymentSourceUnknown").Equals("1"), "UK", "Unknown", new { @class = "M0150" })%>
                    <li class="option">
                        <div class="wrapper" status="(M0150) Payment Source, Other">
                            <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceOtherSRS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceOtherSRSHidden" })%>
                            <%= string.Format("<input id='{0}_M0150PaymentSourceOtherSRS' name='{0}_M0150PaymentSourceOtherSRS' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_M0150PaymentSourceOtherSRS">
                                <span class="fl">11 &#8211;</span>
                                <span class="margin">Other</span>
                            </label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_M0150PaymentSourceOther" class="fl">Specify</label>
                            <div class="fr oasis"><%= Html.TextBox(Model.TypeName + "_M0150PaymentSourceOther", data.AnswerOrEmptyString("M0150PaymentSourceOther"), new { @id = Model.TypeName + "_M0150PaymentSourceOther" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M0150')" status="More Information about M0150">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>