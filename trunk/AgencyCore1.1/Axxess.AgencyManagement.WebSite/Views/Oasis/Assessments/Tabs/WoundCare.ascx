<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main"> 
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "WoundCareForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_WoundNumber", ViewData["WoundNumber"]) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("assessment", Model.TypeName) %>  
    <%= Html.Hidden("categoryType", AssessmentCategory.Integumentary.ToString()) %> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" }) %>
    <fieldset>
        <legend>Wound <%= ViewData["WoundNumber"] %></legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericLocation<%= ViewData["WoundNumber"] %>" class="fl">Location</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLocation" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericLocation" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericLocation" + ViewData["WoundNumber"], @maxlength = "30", @class = "required", @status = "(Optional) Wound Location" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOnsetDate<%= ViewData["WoundNumber"] %>" class="fl">Onset Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericOnsetDate<%= ViewData["WoundNumber"] %>" value="<%= data.AnswerOrEmptyString("GenericOnsetDate" + ViewData["WoundNumber"]) %>" id="<%= Model.TypeName %>_GenericOnsetDate<%= ViewData["WoundNumber"] %>" title="(Optional) Wound Onset Date" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWoundType<%= ViewData["WoundNumber"] %>" class="fl">Wound Type</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericWoundType" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericWoundType" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericWoundType" + ViewData["WoundNumber"], @class = "WoundType", @maxlength = "30", @status = "(Optional) Wound Type" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPressureUlcerStage<%= ViewData["WoundNumber"] %>" class="fl">Pressure Ulcer Stage</label>
                <div class="fr">
                    <%  var pressureUlcerStage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "I", Value = "I" },
                            new SelectListItem { Text = "II", Value = "II" },
                            new SelectListItem { Text = "III", Value = "III" },
                            new SelectListItem { Text = "IV", Value = "IV" },
                            new SelectListItem { Text = "Unstageable", Value = "Unstageable" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPressureUlcerStage" + ViewData["WoundNumber"], "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPressureUlcerStage" + ViewData["WoundNumber"], pressureUlcerStage, new { @id = Model.TypeName + "_GenericPressureUlcerStage" + ViewData["WoundNumber"], @status = "(Optional) Pressure Ulcer Stage" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Measurements</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericMeasurementLength<%= ViewData["WoundNumber"] %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementLength" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericMeasurementLength" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericMeasurementLength" + ViewData["WoundNumber"], @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericMeasurementWidth<%= ViewData["WoundNumber"] %>"><em>Width</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementWidth" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericMeasurementWidth" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericMeasurementWidth" + ViewData["WoundNumber"], @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Width" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericMeasurementDepth<%= ViewData["WoundNumber"] %>"><em>Depth</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementDepth" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericMeasurementDepth" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericMeasurementDepth" + ViewData["WoundNumber"], @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Depth" })%>
                    cm
                </div>
            </div>
            <div class="row">
                <label class="fl">Wound Bed</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericWoundBedGranulation<%= ViewData["WoundNumber"] %>"><em>Granulation</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedGranulation" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericWoundBedGranulation" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericWoundBedGranulation" + ViewData["WoundNumber"], @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Granulation" })%>
                    %<br />
                    <label for="<%= Model.TypeName %>_GenericWoundBedSlough<%= ViewData["WoundNumber"] %>"><em>Slough</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedSlough" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericWoundBedSlough" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericWoundBedSlough" + ViewData["WoundNumber"], @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Slough" })%>
                    %<br />
                    <label for="<%= Model.TypeName %>_GenericWoundBedEschar<%= ViewData["WoundNumber"] %>"><em>Eschar</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedEschar" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericWoundBedEschar" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericWoundBedEschar" + ViewData["WoundNumber"], @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Eschar" })%>
                    %
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericSurroundingTissue<%= ViewData["WoundNumber"] %>" class="fl">Surrounding Tissue</label>
                <div class="fr">
                    <%  var surroundingTissue = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Pink", Value = "Pink" },
                            new SelectListItem { Text = "Dry", Value = "Dry" },
                            new SelectListItem { Text = "Pale", Value = "Pale" },
                            new SelectListItem { Text = "Moist", Value = "Moist" },
                            new SelectListItem { Text = "Excoriated", Value = "Excoriated" },
                            new SelectListItem { Text = "Calloused", Value = "Calloused" },
                            new SelectListItem { Text = "Normal", Value = "Normal" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericSurroundingTissue" + ViewData["WoundNumber"], "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSurroundingTissue" + ViewData["WoundNumber"], surroundingTissue, new { @id = Model.TypeName + "_GenericSurroundingTissue" + ViewData["WoundNumber"], @status = "(Optional) Wound Surrounding Tissue" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDrainage<%= ViewData["WoundNumber"] %>" class="fl">Drainage</label>
                <div class="fr">
                    <%  var drainage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Serous", Value = "Serous" },
                            new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" },
                            new SelectListItem { Text = "Sanguineous", Value = "Sanguineous" },
                            new SelectListItem { Text = "Purulent", Value = "Purulent" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDrainage" + ViewData["WoundNumber"], "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDrainage" + ViewData["WoundNumber"], drainage, new { @id = Model.TypeName + "_GenericDrainage" + ViewData["WoundNumber"], @status = "(Optional) Wound Drainage" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDrainageAmount<%= ViewData["WoundNumber"] %>" class="fl">Drainage Amount</label>
                <div class="fr">
                    <%  var drainageAmount = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Minimal", Value = "Minimal" },
                            new SelectListItem { Text = "Moderate", Value = "Moderate" },
                            new SelectListItem { Text = "Heavy", Value = "Heavy" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDrainageAmount" + ViewData["WoundNumber"], "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDrainageAmount" + ViewData["WoundNumber"], drainageAmount, new { @id = Model.TypeName + "_GenericDrainageAmount" + ViewData["WoundNumber"], @status = "(Optional) Wound Drainage Amount" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOdor<%= ViewData["WoundNumber"] %>" class="fl">Odor</label>
                <div class="fr">
                    <%  var odor = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericOdor" + ViewData["WoundNumber"], "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericOdor" + ViewData["WoundNumber"], odor, new { @id = Model.TypeName + "_GenericOdor" + ViewData["WoundNumber"], @status = "(Optional) Wound Odor" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Tunneling</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericTunnelingLength<%= ViewData["WoundNumber"] %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericTunnelingLength" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericTunnelingLength" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericTunnelingLength" + ViewData["WoundNumber"], @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Tunneling Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericTunnelingTime<%= ViewData["WoundNumber"] %>"><em>Time</em></label>
                    <%  var tunnelingTime = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericTunnelingTime" + ViewData["WoundNumber"]));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericTunnelingTime" + ViewData["WoundNumber"], tunnelingTime, new { @id = Model.TypeName + "_GenericTunnelingTime" + ViewData["WoundNumber"], @class = "shorter", @status = "(Optional) Tunneling Time" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Undermining</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericUnderminingLength<%= ViewData["WoundNumber"] %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericUnderminingLength" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericUnderminingLength" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericUnderminingLength" + ViewData["WoundNumber"], @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Undermining Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericUnderminingTimeFrom<%= ViewData["WoundNumber"] %>"><em>Time</em></label>
                    <%  var underminingTimeFrom = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeFrom" + ViewData["WoundNumber"]));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeFrom" + ViewData["WoundNumber"], underminingTimeFrom, new { @id = Model.TypeName + "_GenericUnderminingTimeFrom" + ViewData["WoundNumber"], @class = "shorter", @status = "(Optional) Undermining Time Start" })%>
                    to
                    <%  var underminingTimeTo = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeTo" + ViewData["WoundNumber"]));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeTo" + ViewData["WoundNumber"], underminingTimeTo, new { @id = Model.TypeName + "_GenericUnderminingTimeTo" + ViewData["WoundNumber"], @class = "shorter", @status = "(Optional) Undermining Time End" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Device</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericDeviceType<%= ViewData["WoundNumber"] %>"><em>Type</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDeviceType" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericDeviceType" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericDeviceType" + ViewData["WoundNumber"], @class = "short DeviceType", @maxlength = "30", @status = "(Optional) Device Type " })%>
                    <br />
                    <label for="<%= Model.TypeName %>_GenericDeviceSetting<%= ViewData["WoundNumber"] %>"><em>Setting</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDeviceSetting" + ViewData["WoundNumber"], data.AnswerOrEmptyString("GenericDeviceSetting" + ViewData["WoundNumber"]), new { @id = Model.TypeName + "_GenericDeviceSetting" + ViewData["WoundNumber"], @class = "short", @maxlength = "30", @status = "(Optional) Device Setting " })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericUploadFile<%= ViewData["WoundNumber"] %>" class="fl">Attachment</label>
                <div class="fr">
                    <% var assetId = data.AnswerOrEmptyGuid("GenericUploadFile" + ViewData["WoundNumber"]); %>
                <%  if (assetId.IsEmpty()) { %>
                    <input type="file" name="<%= Model.TypeName %>_GenericUploadFile<%= ViewData["WoundNumber"] %>" value="Upload" title="(Optional) Photo of Wound" />
                <%  } else { %>
                    <%= Html.Hidden(Model.TypeName + "_GenericUploadFile" + ViewData["WoundNumber"], assetId)%>
                    <%= Html.Asset(assetId)%><a class="link" onclick="Oasis.DeleteAsset($(this),'<%= Model.TypeName %>','GenericUploadFile<%= ViewData["WoundNumber"] %>','<%= assetId %>')">Delete</a>
                <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveExit',function(){})">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
</div>
<%  } %>