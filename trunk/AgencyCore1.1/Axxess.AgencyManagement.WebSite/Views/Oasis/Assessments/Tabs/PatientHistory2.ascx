﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PatientHistoryForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", AssessmentCategory.PatientHistory.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
		        <legend>Physician Visit</legend>
				<div class="column">
				    <div class="row">
    					<label for="<%= Model.TypeName %>_GenericLastVisitDate" class="fl">Last Physician Visit Date</label>
	    				<div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericLastVisitDate" value="<%= data.AnswerOrEmptyString("GenericLastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("GenericLastVisitDate") : string.Empty %>"  id="<%= Model.TypeName %>_GenericLastVisitDate" /></div>
	    			</div>
				</div>
			</fieldset>
			<fieldset class="loc485">
				<legend>Allergies (Locator #17)</legend>
	    <%  var allergyProfile = (Model != null && Model.AllergyProfile.IsNotNullOrEmpty()) ? Model.AllergyProfile.ToObject<AllergyProfile>() : new AllergyProfile(); %>
				<div class="wide-column">
				    <div class="row">
				        <ul class="buttons ac">
	    <%  if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
	                        <li><a onclick="Allergy.New('<%= allergyProfile.Id %>','<%= Model.TypeName %>')">Add Allergy</a></li>
	    <%  } %>
	                        <li><a onclick="Allergy.Refresh('<%= allergyProfile.Id %>','<%= Model.TypeName %>')">Refresh Allergies</a></li>
	                    </ul>
	                    <div id="<%= Model.TypeName %>_AllergyProfileList">
	    <%  allergyProfile.Type = Model.TypeName.ToString(); %>
	    <%  Html.RenderPartial("~/Views/AllergyProfile/List.ascx",allergyProfile); %>
	                    </div>
	                </div>
	            </div>
	        </fieldset>
	    </div>
	    <div>
			<fieldset class="vital-signs">
				<legend>Vital Signs</legend>
				<%  Html.RenderPartial("~/Views/VitalSign/Content.ascx", Model != null && Model.VitalSignLog != null ? Model.VitalSignLog : new VitalSignLog(true)); %>
			</fieldset>
        </div>
    </div>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>