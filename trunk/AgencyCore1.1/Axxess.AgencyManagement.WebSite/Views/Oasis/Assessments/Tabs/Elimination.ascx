<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "EliminationForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Elimination.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>GU</legend>
        <%  string[] genericGU = data.AnswerArray("GenericGU"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericGU", "", new { @id = Model.TypeName + "_GenericGUHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "1", genericGU.Contains("1"), "WNL (Within Normal Limits)") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "2", genericGU.Contains("2"), "Incontinence") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "3", genericGU.Contains("3"), "Bladder Distention") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "4", genericGU.Contains("4"), "Discharge") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "5", genericGU.Contains("5"), "Frequency") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "6", genericGU.Contains("6"), "Dysuria") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "7", genericGU.Contains("7"), "Retention") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "8", genericGU.Contains("8"), "Urgency") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "9", genericGU.Contains("9"), "Oliguria") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericGU", "13", genericGU.Contains("13"), "Nocturia") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrourinary, Catheter/Device">
                            <%= string.Format("<input id='{0}_GenericGU10' name='{0}_GenericGU' value='10' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("10").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericGU10">Catheter/Device</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericGUCatheterList" class="fl">Type</label>
                            <div class="fr">
                                <%  var genericGUCatheterList = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "N/A", Value = "1" },
                                        new SelectListItem { Text = "Foley Catheter ", Value = "2" },
                                        new SelectListItem { Text = "Condom Catheter", Value = "3" },
                                        new SelectListItem { Text = "Suprapubic Catheter", Value = "4" },
                                        new SelectListItem { Text = "Urostomy", Value = "5" },
                                        new SelectListItem { Text = "Other", Value = "6" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericGUCatheterList", "0")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericGUCatheterList", genericGUCatheterList, new { @status = "(Optional) Gastrourinary, Catheter/Device Type" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericGUCatheterLastChanged" class="fl">Last Changed</label>
                            <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericGUCatheterLastChanged" value="<%= data.AnswerOrEmptyString("GenericGUCatheterLastChanged") %>" id="<%= Model.TypeName %>_GenericGUCatheterLastChanged" title="(Optional) Gastrourinary, Catheter/Device, Date Last Changed" /></div>
                            <div class="clr"></div>
                            <div class="ac">
                                <%= Html.TextBox(Model.TypeName + "_GenericGUCatheterFrequency", data.AnswerOrEmptyString("GenericGUCatheterFrequency"), new { @id = Model.TypeName + "_GenericGUCatheterFrequency", @class = "shorter", @maxlength = "5", @status = "(Optional) Gastrourinary, Catheter/Device, Frequency" })%>
                                <label for="<%= Model.TypeName %>_GenericGUCatheterFrequency">Fr</label>
                                <%= Html.TextBox(Model.TypeName + "_GenericGUCatheterAmount", data.AnswerOrEmptyString("GenericGUCatheterAmount"), new { @id = Model.TypeName + "_GenericGUCatheterAmount", @class = "shorter", @maxlength = "5", @status = "(Optional) Gastrourinary, Catheter/Device, Amount" })%>
                                <label for="<%= Model.TypeName %>_GenericGUCatheterAmount">cc</label>
                            </div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrourinary, Urine">
                            <%= string.Format("<input id='{0}_GenericGU11' name='{0}_GenericGU' value='11' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("11").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericGU11">Urine</label>
                        </div>
                        <div class="more">
                            <%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericGUUrine", "", new { @id = Model.TypeName + "_GenericGUUrineHidden" })%>
                            <ul class="checkgroup one-wide">
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericGUUrine", "6", genericGUUrine.Contains("6"), "Clear")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericGUUrine", "1", genericGUUrine.Contains("1"), "Cloudy")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericGUUrine", "2", genericGUUrine.Contains("2"), "Odorous")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericGUUrine", "3", genericGUUrine.Contains("3"), "Sediment")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericGUUrine", "4", genericGUUrine.Contains("4"), "Hematuria")%>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Gastrourinary, Urine, Other">
                                        <%= string.Format("<input id='{0}_GenericGUUrine5' name='{0}_GenericGUUrine' value='5' type='checkbox' {1} />", Model.TypeName, genericGUUrine.Contains("5").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericGUUrine5">Other</label>
                                            <div id="<%= Model.TypeName %>_GenericGUUrine5More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericGUOtherText", data.AnswerOrEmptyString("GenericGUOtherText"), new { @id = Model.TypeName + "_GenericGUOtherText", @maxlength = "20", @status = "(Optional) Gastrourinary, Urine, Specify Other" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                           </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrourinary, External Genitalia">
                            <%= string.Format("<input id='{0}_GenericGU12' name='{0}_GenericGU' value='12' type='checkbox' {1} />", Model.TypeName, genericGU.Contains("12").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericGU12">External Genitalia</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericGUNormal" class="fl">Normal?</label>
                            <div class="fr">
                                <%  var GenericGUNormal = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Normal", Value = "1" },
                                        new SelectListItem { Text = "Abnormal ", Value = "0" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericGUNormal")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericGUNormal", GenericGUNormal, new { @id = Model.TypeName + "_GenericGUNormal", @status = "(Optional) Gastrourinary, External Genitalia, Normal" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericGUClinicalAssessment" class="fl">As per</label>
                            <div class="fr">
                                <%  var GenericGUClinicalAssessment = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Clinician Assessment", Value = "1" },
                                        new SelectListItem { Text = "Pt/CG Report ", Value = "0" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericGUClinicalAssessment")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericGUClinicalAssessment", GenericGUClinicalAssessment, new { @id = Model.TypeName + "_GenericGUClinicalAssessment", @status = "(Optional) Gastrourinary, External Genitalia, As per" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Dialysis</legend>
        <div class="wide-column">
            <div class="row narrower">
                <label class="fl">Is patient on dialysis?</label>
                <div class="fr">
                    <%  var GenericPatientOnDialysis = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No ", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericPatientOnDialysis")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPatientOnDialysis", GenericPatientOnDialysis, new { @id = Model.TypeName + "_GenericPatientOnDialysis", @status = "(Optional) Dialysis" })%>
                </div>
            </div>
            <div id="<%= Model.TypeName %>_Dialysis">
                <div class="row">
                    <%  string[] genericDialysis = data.AnswerArray("GenericDialysis"); %>
                    <%= Html.Hidden(Model.TypeName + "_GenericDialysis", "", new { @id = Model.TypeName + "_GenericDialysisHidden" })%>
                    <label>Dialysis Type</label>
                    <ul class="checkgroup">
                        <%= Html.CheckgroupOption(Model.TypeName + "_GenericDialysis", "1", genericDialysis.Contains("1"), "Peritoneal Dialysis") %>
                        <%= Html.CheckgroupOption(Model.TypeName + "_GenericDialysis", "2", genericDialysis.Contains("2"), "CCPD (Continuous Cyclic Peritoneal Dialysis)") %>
                        <%= Html.CheckgroupOption(Model.TypeName + "_GenericDialysis", "3", genericDialysis.Contains("3"), "IPD (Intermittent Peritoneal Dialysis)") %>
                        <%= Html.CheckgroupOption(Model.TypeName + "_GenericDialysis", "4", genericDialysis.Contains("4"), "CAPD (Continuous Ambulatory Peritoneal Dialysis)") %>
                        <li class="option">
                            <div class="wrapper" status="(Optional) Dialysis Type, Hemodialysis">
                                <%= string.Format("<input id='{0}_GenericDialysis5' name='{0}_GenericDialysis' value='5' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("5").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDialysis5">Hemodialysis</label>
                            </div>
                            <div class="more">
                                <%  string[] genericDialysisHemodialysis = data.AnswerArray("GenericDialysisHemodialysis"); %>
                                <%= Html.Hidden(Model.TypeName + "_GenericDialysisHemodialysis", "", new { @id = Model.TypeName + "_GenericDialysisHemodialysisHidden" })%>
                                <ul class="checkgroup one-wide">
                                    <li class="option">
                                        <div class="wrapper" status="(Optional) Dialysis Type, Hemodialysis, AV Graft/Fistula Site">
                                            <%= string.Format("<input id='{0}_GenericDialysisHemodialysis1' name='{0}_GenericDialysisHemodialysis' value='1' type='checkbox' {1} />", Model.TypeName, genericDialysisHemodialysis.Contains("1").ToChecked()) %>
                                            <span>
                                                <label for="<%= Model.TypeName %>_GenericDialysisHemodialysis1">AV Graft/ Fistula Site</label>
                                                <div id="<%= Model.TypeName %>_GenericDialysisHemodialysis1More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDialysisHemodialysisGriftAV", data.AnswerOrEmptyString("GenericDialysisHemodialysisGriftAV"), new { @id = Model.TypeName + "_GenericDialysisHemodialysisGriftAV", @maxlength = "25", @status = "(Optional) Dialysis Type, Hemodialysis, AV Graft/Fistula Site" })%></div>
                                            </span>
                                            <div class="clr"></div>
                                        </div>
                                    </li>
                                    <li class="option">
                                        <div class="wrapper" status="(Optional) Dialysis Type, Hemodialysis, Central Venous Catheter Access Site">
                                            <%= string.Format("<input id='{0}_GenericDialysisHemodialysis2' name='{0}_GenericDialysisHemodialysis' value='2' type='checkbox' {1} />", Model.TypeName, genericDialysisHemodialysis.Contains("2").ToChecked()) %>
                                            <span>
                                                <label for="<%= Model.TypeName %>_GenericDialysisHemodialysis2">Central Venous Catheter Access Site</label>
                                                <div id="<%= Model.TypeName %>_GenericDialysisHemodialysis2More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDialysisHemodialysisCentralVenousAV", data.AnswerOrEmptyString("GenericDialysisHemodialysisCentralVenousAV"), new { @id = Model.TypeName + "_GenericDialysisHemodialysisCentralVenousAV", @maxlength = "25", @status = "(Optional) Dialysis Type, Hemodialysis, Central Venous Catheter Access Site" })%></div>
                                            </span>
                                            <div class="clr"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="option">
                            <div class="wrapper" status="(Optional) Catheter Site Free of Infection">
                                <%= string.Format("<input id='{0}_GenericDialysis6' name='{0}_GenericDialysis' value='6' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("6").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDialysis6">Catheter site free from signs and symptoms of infection</label>
                            </div>
                        </li>
                        <li class="option">
                            <div class="wrapper" status="(Optional) Dialysis Type, Other">
                                <%= string.Format("<input title='(Optional) Dialysis Type, Other' id='{0}_GenericDialysis7' name='{0}_GenericDialysis' value='7' type='checkbox' {1} />", Model.TypeName, genericDialysis.Contains("7").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericDialysis7">Other</label>
                            </div>
                            <div class="more">
                                <label for="<%= Model.TypeName %>_GenericDialysisOtherDesc" class="fl">Specify</label>
                                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDialysisOtherDesc", data.AnswerOrEmptyString("GenericDialysisOtherDesc"), new { @id = Model.TypeName + "_GenericDialysisOtherDesc", @maxlength = "25", @status = "(Optional) Dialysis Type, Specify Other" })%></div>
                                <div class="clr"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="<%= Model.TypeName %>_GenericDialysisCenter">Dialysis Center</label>
                    <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericDialysisCenter", data.AnswerOrEmptyString("GenericDialysisCenter"), 3, 100, new { @id = Model.TypeName + "_GenericDialysisCenter", @maxlength = "25", @status = "(Optional) Dialysis Center" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Urinary</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1600">
                <label>
                    <a status="More Information about M1600" class="green" onclick="Oasis.Help('M1600')">(M1600)</a>
                    Has this patient been treated for a Urinary Tract Infection in the past 14 days?
                </label>    
                <%= Html.Hidden(Model.TypeName + "_M1600UrinaryTractInfection", "", new { @id = Model.TypeName + "_M1600UrinaryTractInfectionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1600UrinaryTractInfection", "00", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1600UrinaryTractInfection", "01", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("01"), "1", "Yes") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1600UrinaryTractInfection", "NA", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("NA"), "NA", "Patient on prophylactic treatment") %>
                <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1600UrinaryTractInfection", "UK", data.AnswerOrEmptyString("M1600UrinaryTractInfection").Equals("UK"), "UK", "Unknown") %>
                <%  } %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1600')" title="More Information about M1600">?</a></div>
                </div>
            </div>
            <%  } %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1610">
                <label>
                    <a status="More Information about M1610" class="green" onclick="Oasis.Help('M1610')">(M1610)</a>
                    Urinary Incontinence or Urinary Catheter Presence:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1610UrinaryIncontinence", "", new { @id = Model.TypeName + "_M1610UrinaryIncontinenceHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1610UrinaryIncontinence", "00", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("00"), "0", "No incontinence or catheter <em>(includes anuria or ostomy for urinary drainage)</em>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1610UrinaryIncontinence", "01", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("01"), "1", "Patient is incontinent")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1610UrinaryIncontinence", "02", data.AnswerOrEmptyString("M1610UrinaryIncontinence").Equals("02"), "2", "Patient requires a urinary catheter <em>(i.e., external, indwelling, intermittent, suprapubic)</em>")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1610')" stauts="More Information about M1610">?</a></div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1615">
                <label>
                    <a status="More Information about M1615" class="green" onclick="Oasis.Help('M1615')">(M1615)</a>
                    When does Urinary Incontinence occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "", new { @id = Model.TypeName + "_M1615UrinaryIncontinenceOccurHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "00", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("00"), "0", "Timed-voiding defers incontinence")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "01", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("01"), "1", "Occasional stress incontinence")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "02", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("02"), "2", "During the night only")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "03", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("03"), "3", "During the day only")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1615UrinaryIncontinenceOccur", "04", data.AnswerOrEmptyString("M1615UrinaryIncontinenceOccur").Equals("04"), "4", "During the day and night")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1615')" status="More Information about M1615">?</a></div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Bowels</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1620">
                <label>
                    <a status="More Information about M1620" class="green" onclick="Oasis.Help('M1620')">(M1620)</a>
                    Bowel Incontinence Frequency:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1620BowelIncontinenceFrequency", "", new { @id = Model.TypeName + "_M1620BowelIncontinenceFrequencyHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "00", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("00"), "0", "Very rarely or never has bowel incontinence")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "01", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("01"), "1", "Less than once weekly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "02", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("02"), "2", "One to three times weekly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "03", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("03"), "3", "Four to six times weekly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "04", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("04"), "4", "On a daily basis")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "05", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("05"), "5", "More often than once daily")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "NA", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("NA"), "NA", "Patient has ostomy for bowel elimination")%>
        <%  if (Model.AssessmentTypeNum.ToInteger() != 4 && Model.AssessmentTypeNum.ToInteger() != 9) { %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1620BowelIncontinenceFrequency", "UK", data.AnswerOrEmptyString("M1620BowelIncontinenceFrequency").Equals("UK"), "UK", "Unknown")%>
        <%  } %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1620')" title="More Information about M1620">?</a></div>
                </div>
            </div>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1630">
                <label>
                    <a status="More Information about M1630" class="green" onclick="Oasis.Help('M1630')">(M1630)</a>
                    Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days):
                </label>
                <div class="subrow">
                    <label>
                        a) was related to an inpatient facility stay, or<br />
                        b) necessitated a change in medical or treatment regimen?
                    </label>
                </div>
                <%= Html.Hidden(Model.TypeName + "_M1630OstomyBowelElimination", "", new { @id = Model.TypeName + "_M1630OstomyBowelEliminationHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1630OstomyBowelElimination", "00", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("00"), "0", "Patient does not have an ostomy for bowel elimination.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1630OstomyBowelElimination", "01", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("01"), "1", "Patient&#8217;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1630OstomyBowelElimination", "02", data.AnswerOrEmptyString("M1630OstomyBowelElimination").Equals("02"), "2", "The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1630')" status="More Information about M1630">?</a></div>
                </div>
            </div>
        <%  } %>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>GI</legend>
        <%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericDigestive", "", new { @id = Model.TypeName + "_GenericDigestiveHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestive", "1", genericDigestive.Contains("1"), "WNL (Within Normal Limits)") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Bowel Sounds">
                            <%= string.Format("<input id='{0}_GenericDigestive2' name='{0}_GenericDigestive' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestive2">Bowel Sounds</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericDigestiveBowelSoundsType">Specify</label>
                            <div class="fr">
                                <%  var bowelSounds = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Present/WNL x4 quadrants", Value = "1" },
                                        new SelectListItem { Text = "Hyperactive", Value = "2" },
                                        new SelectListItem { Text = "Hypoactive", Value = "3" },
                                        new SelectListItem { Text = "Absent", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveBowelSoundsType", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = Model.TypeName + "_GenericDigestiveBowelSoundsType", @status = "(Optional) Gastrointestinal, Bowel Sounds" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Abdominal Palpation">
                            <%= string.Format("<input id='{0}_GenericDigestive3' name='{0}_GenericDigestive' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestive3">Abdominal Palpation</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericAbdominalPalpation">Specify</label>
                            <div class="fr">
                                <%  var abdominalPalpation = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Soft/WNL", Value = "1" },
                                        new SelectListItem { Text = "Firm", Value = "2" },
                                        new SelectListItem { Text = "Tender", Value = "3" },
                                        new SelectListItem { Text = "Other", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericAbdominalPalpation", "0")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericAbdominalPalpation", abdominalPalpation, new { @id = Model.TypeName + "_GenericAbdominalPalpation", @status = "(Optional) Gastrointestinal, Abdominal Palpation" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestive", "4", genericDigestive.Contains("4"), "Bowel Incontinence") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestive", "5", genericDigestive.Contains("5"), "Nausea") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestive", "6", genericDigestive.Contains("6"), "Vomiting") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestive", "7", genericDigestive.Contains("7"), "GERD") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Abd Girth">
                            <%= string.Format("<input id='{0}_GenericDigestive8' name='{0}_GenericDigestive' value='8' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("8").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestive8">Abd Girth</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericDigestiveAbdGirthLength">Length</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveAbdGirthLength", data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength"), new { @id = Model.TypeName + "_GenericDigestiveAbdGirthLength", @maxlength = "5", @status = "(Optional) Gastrointestinal, Abd Girth Length" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label>Elimination</label>
                <%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBM", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMHidden" })%>
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Elimination, Last BM">
                            <%= string.Format("<input id='{0}_GenericDigestive11' name='{0}_GenericDigestive' value='11' type='checkbox' {1} />", Model.TypeName, genericDigestive.Contains("11").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestive11">Last BM</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMDate" class="fl">Date</label>
                            <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" id="<%= Model.TypeName %>_GenericDigestiveLastBMDate" status="(Optional) Gastrointestinal, Elimination, Last BM Date" /></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestiveLastBM", "1", genericDigestiveLastBM.Contains("1"), "WNL (Within Normal Limits)")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Elimination, Last BM, Abnormal Stool">
                            <%= string.Format("<input id='{0}_GenericDigestiveLastBM2' name='{0}_GenericDigestiveLastBM' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBM2">Abnormal Stool</label>
                        </div>
                        <div class="more">
                            <%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "", new { @id = Model.TypeName + "_GenericDigestiveLastBMAbnormalStoolHidden" })%>
                            <ul class="checkgroup one-wide">
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "1", genericDigestiveLastBMAbnormalStool.Contains("1"), "Gray")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "2", genericDigestiveLastBMAbnormalStool.Contains("2"), "Tarry")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "3", genericDigestiveLastBMAbnormalStool.Contains("3"), "Fresh Blood")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericDigestiveLastBMAbnormalStool", "4", genericDigestiveLastBMAbnormalStool.Contains("4"), "Black")%>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Elimination, Constipation">
                            <%= string.Format("<input id='{0}_GenericDigestiveLastBM3' name='{0}_GenericDigestiveLastBM' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBM3">Constipation</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMConstipationType" class="fl">Type</label>
                            <div class="fr">
                                <%  var GenericDigestiveLastBMConstipationType = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Chronic", Value = "Chronic" },
                                        new SelectListItem { Text = "Acute ", Value = "Acute" },
                                        new SelectListItem { Text = "Occasional ", Value = "Occasional" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveLastBMConstipationType", GenericDigestiveLastBMConstipationType, new { @id = Model.TypeName + "_GenericDigestiveLastBMConstipationType", @status = "Gastrointestinal, Elimination, Constipation, Type" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Elimination, Diarrhea">
                            <%= string.Format("<input id='{0}_GenericDigestiveLastBM4' name='{0}_GenericDigestiveLastBM' value='4' type='checkbox' {1} />", Model.TypeName, genericDigestiveLastBM.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBM4">Diarrhea</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericDigestiveLastBMConstipationType" class="fl">Type</label>
                            <div class="fr">
                                <%  var GenericDigestiveLastBMDiarrheaType = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Chronic", Value = "Chronic" },
                                        new SelectListItem { Text = "Acute ", Value = "Acute" },
                                        new SelectListItem { Text = "Occasional ", Value = "Occasional" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", GenericDigestiveLastBMDiarrheaType, new { @id = Model.TypeName + "_GenericDigestiveLastBMDiarrheaType", @status = "Gastrointestinal, Elimination, Diarrhea, Type" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label>Ostomy</label>
                <%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDigestiveOstomy", "", new { @id = Model.TypeName + "_GenericDigestiveOstomyHidden" })%>
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Ostomy Type">
                            <%= string.Format("<input id='{0}_GenericDigestiveOstomy1' name='{0}_GenericDigestiveOstomy' value='1' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveOstomy1">Ostomy Type</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericDigestiveOstomyType">Specify</label>
                            <div class="fr">
                                <%  var ostomy = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "N/A", Value = "1" },
                                        new SelectListItem { Text = "Ileostomy ", Value = "2" },
                                        new SelectListItem { Text = "Colostomy", Value = "3" },
                                        new SelectListItem { Text = "Other", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveOstomyType", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDigestiveOstomyType", ostomy, new { @id = Model.TypeName + "_GenericDigestiveOstomyType", @status = "(Optional) Gastrointestinal, Ostomy Type" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Ostomy, Stoma Appearance">
                            <%= string.Format("<input id='{0}_GenericDigestiveOstomy2' name='{0}_GenericDigestiveOstomy' value='2' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveOstomy2">Stoma Appearance</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericDigestiveStomaAppearance">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveStomaAppearance", data.AnswerOrEmptyString("GenericDigestiveStomaAppearance"), new { @id = Model.TypeName + "_GenericDigestiveStomaAppearance", @class = "shorter", @maxlength = "15", @status = "(Optional) Gastrointestinal, Ostomy, Stoma Appearance" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Gastrointestinal, Ostomy, Surrounding Skin">
                            <%= string.Format("<input id='{0}_GenericDigestiveOstomy3' name='{0}_GenericDigestiveOstomy' value='3' type='checkbox' {1} />", Model.TypeName, genericDigestiveOstomy.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDigestiveOstomy3">Surrounding Skin</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericDigestiveOstomy3More">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDigestiveSurSkinType", data.AnswerOrEmptyString("GenericDigestiveSurSkinType"), new { @id = Model.TypeName + "_GenericDigestiveSurSkinType", @maxlength = "15", @status = "(Optional) Gastrointestinal, Ostomy, Surrounding Skin" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericGUDigestiveComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericGUDigestiveComments", data.AnswerOrEmptyString("GenericGUDigestiveComments"), new { @id = Model.TypeName + "_GenericGUDigestiveComments", @status = "(Optional) Gastrointestinal Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Elimination.ascx", Model); %>
    <%  } %>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>