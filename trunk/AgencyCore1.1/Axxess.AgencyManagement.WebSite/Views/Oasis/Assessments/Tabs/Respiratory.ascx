<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "RespiratoryForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Respiratory.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Respiratory</legend>
        <%  string[] respiratoryCondition = data.AnswerArray("GenericRespiratoryCondition"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericRespiratoryCondition", "", new { @id = Model.TypeName + "_GenericRespiratoryConditionHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericRespiratoryCondition", "1", respiratoryCondition.Contains("1"), "WNL (Within Normal Limits)") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Respiratory, Lung Sounds">
                            <%= string.Format("<input id='{0}_GenericRespiratoryCondition2' name='{0}_GenericRespiratoryCondition' value='2' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericRespiratoryCondition2">Lung Sounds</label>
                        </div>
                        <div class="more">
                            <%  string[] respiratorySounds = data.AnswerArray("GenericRespiratorySounds"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericRespiratorySounds", "", new { @id = Model.TypeName + "_GenericRespiratorySoundsHidden" })%>
                            <ul class="checkgroup two-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, CTA">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds1' name='{0}_GenericRespiratorySounds' value='1' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("1").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds1">CTA</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds1More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsCTAText", data.AnswerOrEmptyString("GenericLungSoundsCTAText"), new { @id = Model.TypeName + "_GenericLungSoundsCTAText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, CTA", @class = "short" }) %></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Rales">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds2' name='{0}_GenericRespiratorySounds' value='2' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("2").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds2">Rales</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds2More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsRalesText", data.AnswerOrEmptyString("GenericLungSoundsRalesText"), new { @id = Model.TypeName + "_GenericLungSoundsRalesText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Rales", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Rhonchi">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds3' name='{0}_GenericRespiratorySounds' value='3' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("3").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds3">Rhonchi</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds3More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsRhonchiText", data.AnswerOrEmptyString("GenericLungSoundsRhonchiText"), new { @id = Model.TypeName + "_GenericLungSoundsRhonchiText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Rhonchi", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Wheezes">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds4' name='{0}_GenericRespiratorySounds' value='4' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("4").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds4">Wheezes</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds4More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsWheezesText", data.AnswerOrEmptyString("GenericLungSoundsWheezesText"), new { @id = Model.TypeName + "_GenericLungSoundsWheezesText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Wheezes", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Crackles">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds5' name='{0}_GenericRespiratorySounds' value='5' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("5").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds5">Crackles</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds5More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsCracklesText", data.AnswerOrEmptyString("GenericLungSoundsCracklesText"), new { @id = Model.TypeName + "_GenericLungSoundsCracklesText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Crackles", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Diminished">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds6' name='{0}_GenericRespiratorySounds' value='6' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("6").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds6">Diminished</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds6More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsDiminishedText", data.AnswerOrEmptyString("GenericLungSoundsDiminishedText"), new { @id = Model.TypeName + "_GenericLungSoundsDiminishedText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Diminished", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Absent">
                                        <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Absent' id='{0}_GenericRespiratorySounds7' name='{0}_GenericRespiratorySounds' value='7' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("7").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds7">Absent</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds7More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsAbsentText", data.AnswerOrEmptyString("GenericLungSoundsAbsentText"), new { @id = Model.TypeName + "_GenericLungSoundsAbsentText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Absent", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Respiratory, Lung Sounds, Stridor">
                                        <%= string.Format("<input id='{0}_GenericRespiratorySounds8' name='{0}_GenericRespiratorySounds' value='8' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("8").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericRespiratorySounds8">Stridor</label>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds8More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLungSoundsStridorText", data.AnswerOrEmptyString("GenericLungSoundsStridorText"), new { @id = Model.TypeName + "_GenericLungSoundsStridorText", @maxlength = "20", @status = "(Optional) Respiratory, Lung Sounds, Stridor", @class = "short" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Respiratory, Cough">
                            <%= string.Format("<input id='{0}_GenericRespiratoryCondition3' name='{0}_GenericRespiratoryCondition' value='3' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericRespiratoryCondition3">Cough</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericCoughList" class="fl">Productive?</label>
                            <div class="fr">
                                <%  var coughList = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "N/A", Value = "1" },
                                        new SelectListItem { Text = "Productive", Value = "1" },
                                        new SelectListItem { Text = "Nonproductive", Value = "3" },
                                        new SelectListItem { Text = "Other", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericCoughList", "0")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCoughList", coughList, new { @status = "(Optional) Respiratory, Cough, Productive" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericCoughDescribe" class="fl">Describe</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericCoughDescribe", data.AnswerOrEmptyString("GenericCoughDescribe"), new { @id = Model.TypeName + "_GenericCoughDescribe", @maxlength = "20", @status = "(Optional) Respiratory, Cough, Describe" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Respiratory, Oxygen">
                            <%= string.Format("<input title='(Optional) Respiratory, Oxygen' id='{0}_GenericRespiratoryCondition4' name='{0}_GenericRespiratoryCondition' value='4' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericRespiratoryCondition4">O<sub>2</sub></label>
                        </div>
                        <div class="more ac">
                            <label for="<%= Model.TypeName %>_Generic02AtText">At</label>
                            <%= Html.TextBox(Model.TypeName + "_Generic02AtText", data.AnswerOrEmptyString("Generic02AtText"), new { @id = Model.TypeName + "_Generic02AtText", @class = "short", @maxlength = "20", @status = "(Optional) Respiratory, Oxygen At" }) %>
                            <label for="<%= Model.TypeName %>_GenericLPMVia">LPM via</label>
                            <%  var via = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Nasal Cannula", Value = "1" },
                                    new SelectListItem { Text = "Mask", Value = "2" },
                                    new SelectListItem { Text = "Trach", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericLPMVia", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericLPMVia", via, new { @id = Model.TypeName + "_GenericLPMVia", @class = "short", @status = "(Optional) Respiratory, Oxygen Via" })%>
                            <%  var oxygenRate = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Continuous", Value = "1" },
                                    new SelectListItem { Text = "Intermittent", Value = "2" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericOxygenRate", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericOxygenRate", oxygenRate, new { @id = Model.TypeName + "_GenericOxygenRate", @class = "short", @status = "(Optional) Respiratory, Oxygen Rate" })%>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Respiratory, Oxygen Saturation">
                            <%= string.Format("<input id='{0}_GenericRespiratoryCondition5' name='{0}_GenericRespiratoryCondition' value='5' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericRespiratoryCondition5">O<sub>2</sub> Sat</label>
                        </div>
                        <div class="more ac">
                            <%= Html.TextBox(Model.TypeName + "_Generic02SatText", data.AnswerOrEmptyString("Generic02SatText"), new { @id = Model.TypeName + "_Generic02SatText", @class = "short", @maxlength = "20", @status = "(Optional) Respiratory, Oxygen Saturation Level" })%>
                            <label for="<%= Model.TypeName %>_Generic02SatList">On</label>
                            <%  var satList = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Room Air", Value = "Room Air" },
                                    new SelectListItem { Text = "02", Value = "02" }
                                }, "Value", "Text", data.AnswerOrDefault("Generic02SatList", "0")); %>
                            <%= Html.DropDownList(Model.TypeName + "_Generic02SatList", satList, new { @status = "(Optional) Respiratory, Oxygen Saturation, Room Air/O2", @class = "short" })%>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Respiratory, Nebulizer">
                            <%= string.Format("<input id='{0}_GenericRespiratoryCondition6' name='{0}_GenericRespiratoryCondition' value='6' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericRespiratoryCondition6">Nebulizer</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNebulizerText" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNebulizerText", data.AnswerOrEmptyString("GenericNebulizerText"), new { @id = Model.TypeName + "_GenericNebulizerText", @maxlength = "20", @status = "(Optional) Respiratory, Nebulizer" }) %></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericRespiratoryCondition", "7", respiratoryCondition.Contains("7"), "Tracheostomy") %>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericRespiratoryComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericRespiratoryComments", data.AnswerOrEmptyString("GenericRespiratoryComments"), new { @id = Model.TypeName + "_GenericRespiratoryComments", @status = "(Optional) Respiratory Comments" })%></div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Dyspneic</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1400">
                <label>
                    <a status="More Information about M1400" class="green" onclick="Oasis.Help('M1400')">(M1400)</a>
                    When is the patient dyspneic or noticeably Short of Breath?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1400PatientDyspneic", "", new { @id = Model.TypeName + "_M1400PatientDyspneicHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1400PatientDyspneic", "00", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("00"), "0", "Patient is not short of breath</")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1400PatientDyspneic", "01", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("01"), "1", "When walking more than 20 feet, climbing stairs")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1400PatientDyspneic", "02", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("02"), "2", "With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1400PatientDyspneic", "03", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("03"), "3", "With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1400PatientDyspneic", "04", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("04"), "4", "At rest (during day or night)")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1400')" status="More Information about M1400">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Treatments</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1410">
                <label>
                    <a status="More Information about M1410" class="green" onclick="Oasis.Help('M1410')">(M1410)</a>
                    Respiratory Treatments utilized at home
                </label>
                <ul class="checkgroup two-wide">
                    <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsOxygen", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsOxygenHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1410HomeRespiratoryTreatmentsOxygen", "1", data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsOxygen").Equals("1"), "1", "Oxygen (intermittent or continuous)", new { @class = "M1410" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsVentilator", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsVentilatorHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1410HomeRespiratoryTreatmentsVentilator", "1", data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsVentilator").Equals("1"), "2", "Ventilator (continually or at night)", new { @class = "M1410" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsContinuous", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsContinuousHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1410HomeRespiratoryTreatmentsContinuous", "1", data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsContinuous").Equals("1"), "3", "Continuous/Bi-level positive airway pressure", new { @class = "M1410" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsNone", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsNoneHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1410HomeRespiratoryTreatmentsNone", "1", data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsNone").Equals("1"), "4", "None of the above", new { @class = "M1410" })%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1410')" title="More Information about M1410">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Respiratory.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>