<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "AdlForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.AdlIadl.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset class="loc485">
	    <legend>Activities Permitted (Locator #18.B)</legend>
        <%  string[] activitiesPermitted = data.AnswerArray("485ActivitiesPermitted"); %>
        <%= Html.Hidden(Model.TypeName + "_485ActivitiesPermitted", "", new { @id = Model.TypeName + "_485ActivitiesPermittedHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "1", activitiesPermitted.Contains("1"), "Complete bed rest") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "2", activitiesPermitted.Contains("2"), "Bed rest with BRP") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "3", activitiesPermitted.Contains("3"), "Up as tolerated") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "4", activitiesPermitted.Contains("4"), "Transfer bed-chair") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "5", activitiesPermitted.Contains("5"), "Exercise prescribed") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "6", activitiesPermitted.Contains("6"), "Partial weight bearing") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "7", activitiesPermitted.Contains("7"), "Independent at home") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "8", activitiesPermitted.Contains("8"), "Crutches") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "9", activitiesPermitted.Contains("9"), "Cane") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "A", activitiesPermitted.Contains("A"), "Wheelchair") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "B", activitiesPermitted.Contains("B"), "Walker") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ActivitiesPermitted", "C", activitiesPermitted.Contains("C"), "No Restrictions") %>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 18B) Activities Permitted, Other">
                            <%= string.Format("<input id='{0}_485ActivitiesPermittedD' name='{0}_485ActivitiesPermitted' value='D' type='checkbox' {1} />", Model.TypeName, activitiesPermitted.Contains("D").ToChecked())%>
                            <label for="<%= Model.TypeName %>_485ActivitiesPermittedD">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485ActivitiesPermittedOther">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_485ActivitiesPermittedOther", data.AnswerOrEmptyString("485ActivitiesPermittedOther"), new { @id = Model.TypeName + "_485ActivitiesPermittedOther" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Musculoskeletal</legend>
        <%  string[] genericMusculoskeletal = data.AnswerArray("GenericMusculoskeletal"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericMusculoskeletal", "", new { @id = Model.TypeName + "_GenericMusculoskeletalHidden" })%>
	    <div class="wide-column">
            <div class="row">
                <%= Html.Hidden(Model.TypeName + "_GenericMusculoskeletal", "", new { @id = Model.TypeName + "_GenericMusculoskeletalHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericMusculoskeletal", "1", genericMusculoskeletal.Contains("1"), "WNL (Within Normal Limits)")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Grip Strength">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal2' name='{0}_GenericMusculoskeletal' value='2' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal2">Grip Strength</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletalHandGrips" class="fl">Level</label>
                            <div class="fr">
                                <%  var handGrips = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Strong", Value = "1" },
                                        new SelectListItem { Text = "Weak", Value = "2" },
                                        new SelectListItem { Text = "Other", Value = "3" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalHandGrips", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalHandGrips", handGrips, new { @id = Model.TypeName + "_GenericMusculoskeletalHandGrips", @status = "(Optional) Musculoskeletal, Grip Strength Level" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletalHandGripsPosition" class="fl">Position</label>
                            <div class="fr">
                                <%  var GenericMusculoskeletalHandGripsPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericMusculoskeletalHandGripsPosition"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalHandGripsPosition", GenericMusculoskeletalHandGripsPosition, new { @id = Model.TypeName + "_GenericMusculoskeletalHandGripsPosition", @status = "(Optional) Musculoskeletal, Grip Strength Position" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Impaired Motor Skill">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal3' name='{0}_GenericMusculoskeletal' value='3' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal3">Impaired Motor Skill</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletalImpairedMotorSkills" class="fl">Specify</label>
                            <div class="fr">
                                <%  var impairedMotorSkills = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "N/A", Value = "1" },
                                        new SelectListItem { Text = "Fine", Value = "2" },
                                        new SelectListItem { Text = "Gross", Value = "3" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalImpairedMotorSkills", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalImpairedMotorSkills", impairedMotorSkills, new { @id = Model.TypeName + "_GenericMusculoskeletalImpairedMotorSkills", @status = "(Optional) Musculoskeletal, Impaired Motor Skill LeveL" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Limited ROM">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal4' name='{0}_GenericMusculoskeletal' value='4' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal4">Limited ROM</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericLimitedROMLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLimitedROMLocation", data.AnswerOrEmptyString("GenericLimitedROMLocation"), new { @id = Model.TypeName + "_GenericLimitedROMLocation", @maxlength = "24", @status = "(Optional) Musculoskeletal, Limited ROM Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Mobility">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal5' name='{0}_GenericMusculoskeletal' value='5' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal5">Mobility</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletalMobility" class="fl">Specify</label>
                            <div class="fr">
                                <%  var mobility = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "WNL (Within Normal Limits)", Value = "1" },
                                        new SelectListItem { Text = "Ambulatory", Value = "2" },
                                        new SelectListItem { Text = "Ambulatory w/assistance", Value = "3" },
                                        new SelectListItem { Text = "Chair fast", Value = "4" },
                                        new SelectListItem { Text = "Bedfast", Value = "5" },
                                        new SelectListItem { Text = "Non-ambulatory", Value = "6" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericMusculoskeletalMobility", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericMusculoskeletalMobility", mobility, new { @id = Model.TypeName + "_GenericMusculoskeletalMobility", @status = "(Optional) Musculoskeletal, Mobility Level" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Type Assistive Device">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal6' name='{0}_GenericMusculoskeletal' value='6' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal6">Type Assistive Device</label>
                        </div>
                        <div class="more">
                            <%  string[] genericAssistiveDevice = data.AnswerArray("GenericAssistiveDevice"); %>
                            <ul class="checkgroup one-wide">
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericAssistiveDevice", "1", genericAssistiveDevice.Contains("1"), "Cane")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericAssistiveDevice", "2", genericAssistiveDevice.Contains("2"), "Crutches")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericAssistiveDevice", "3", genericAssistiveDevice.Contains("3"), "Walker")%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_GenericAssistiveDevice", "4", genericAssistiveDevice.Contains("4"), "Wheelchair")%>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Musculoskeletal, Type Assistive Device, Other">
                                        <%= string.Format("<input id='{0}_GenericAssistiveDevice5' name='{0}_GenericAssistiveDevice' value='5' type='checkbox' {1} />", Model.TypeName, genericAssistiveDevice.Contains("5").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericAssistiveDevice5">Other</label>
                                            <div id="<%= Model.TypeName %>_GenericAssistiveDevice5More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericAssistiveDeviceOther", data.AnswerOrEmptyString("GenericAssistiveDeviceOther"), new { @id = Model.TypeName + "_GenericAssistiveDeviceOther", @class = "short", @status = "(Optional) Musculoskeletal, Type Assistive Device, Specify Other" })%></div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Contracture">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal7' name='{0}_GenericMusculoskeletal' value='7' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal7">Contracture</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericContractureLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericContractureLocation", data.AnswerOrEmptyString("GenericContractureLocation"), new { @id = Model.TypeName + "_GenericContractureLocation", @maxlength = "24", @status = "(Optional) Musculoskeletal, Contracture Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericMusculoskeletal", "8", genericMusculoskeletal.Contains("8"), "Weakness")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Joint Pain">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal9' name='{0}_GenericMusculoskeletal' value='9' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal9">Joint Pain</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericJointPainLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericJointPainLocation", data.AnswerOrEmptyString("GenericJointPainLocation"), new { @id = Model.TypeName + "_GenericJointPainLocation", @maxlength = "24", @status = "(Optional) Musculoskeletal, Joint Pain Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericMusculoskeletal", "10", genericMusculoskeletal.Contains("10"), "Poor Balance")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericMusculoskeletal", "11", genericMusculoskeletal.Contains("11"), "Joint Stiffness")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Amputation">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal12' name='{0}_GenericMusculoskeletal' value='12' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("12").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal12">Amputation</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericAmputationLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericAmputationLocation", data.AnswerOrEmptyString("GenericAmputationLocation"), new { @id = Model.TypeName + "_GenericAmputationLocation", @maxlength = "24", @status = "(Optional) Musculoskeletal, Amputation Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Musculoskeletal, Weight Bearing Restrictions">
                            <%= string.Format("<input id='{0}_GenericMusculoskeletal13' name='{0}_GenericMusculoskeletal' value='13' type='checkbox' {1} />", Model.TypeName, genericMusculoskeletal.Contains("13").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericMusculoskeletal13">Weight Bearing Restrictions</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericWeightBearingRestriction" class="fl">Restriction</label>
                            <div class="fr">
                                <%  var GenericWeightBearingRestriction = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Full", Value = "0" },
                                        new SelectListItem { Text = "Partial", Value = "1" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericWeightBearingRestriction"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericWeightBearingRestriction", GenericWeightBearingRestriction, new { @id = Model.TypeName + "_GenericWeightBearingRestriction", @status = "(Optional) Musculoskeletal, Weight Bearing Restrictions" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericWeightBearingRestrictionLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericWeightBearingRestrictionLocation", data.AnswerOrEmptyString("GenericWeightBearingRestrictionLocation"), new { @id = Model.TypeName + "_GenericWeightBearingRestrictionLocation", @maxlength = "24", @status = "(Optional) Musculoskeletal, Weight Bearing Restrictions Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMusculoskeletalComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericMusculoskeletalComments", data.AnswerOrEmptyString("GenericMusculoskeletalComments"), new { @id = Model.TypeName + "_GenericMusculoskeletalComments", @status = "(Optional) Musculoskeletal, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Grooming</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1800">
                <label>
                    <a status="More Information about M1800" class="green" onclick="Oasis.Help('M1800')">(M1800)</a>
                    Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1800Grooming", "", new { @id = Model.TypeName + "_M1800GroomingHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1800Grooming", "00", data.AnswerOrEmptyString("M1800Grooming").Equals("00"), "0", "Able to groom self unaided, with or without the use of assistive devices or adapted methods.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1800Grooming", "01", data.AnswerOrEmptyString("M1800Grooming").Equals("01"), "1", "Grooming utensils must be placed within reach before able to complete grooming activities.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1800Grooming", "02", data.AnswerOrEmptyString("M1800Grooming").Equals("02"), "2", "Someone must assist the patient to groom self.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1800Grooming", "03", data.AnswerOrEmptyString("M1800Grooming").Equals("03"), "3", "Patient depends entirely upon someone else for grooming needs.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1800')" status="More Information about M1800">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum != "11" && Model.AssessmentTypeNum != "14") { %>
    <fieldset class="oasis">
        <legend>Dressing Ability</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1810">
                <label>
                    <a status="More Information about M1810" class="green" onclick="Oasis.Help('M1810')">(M1810)</a>
                    Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "", new { @id = Model.TypeName + "_M1810CurrentAbilityToDressUpperHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "00", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("00"), "0", "Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "01", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("01"), "1", "Able to dress upper body without assistance if clothing is laid out or handed to the patient.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "02", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("02"), "2", "Someone must help the patient put on upper body clothing.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1810CurrentAbilityToDressUpper", "03", data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").Equals("03"), "3", "Patient depends entirely upon another person to dress the upper body.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1810')" status="More Information about M1810">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1820">
                <label>
                    <a status="More Information about M1820" class="green" onclick="Oasis.Help('M1820')">(M1820)</a>
                    Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1820CurrentAbilityToDressLower", "", new { @id = Model.TypeName + "_M1820CurrentAbilityToDressLowerHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1820CurrentAbilityToDressLower", "00", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("00"), "0", "Able to obtain, put on, and remove clothing and shoes without assistance.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1820CurrentAbilityToDressLower", "01", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("01"), "1", "Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1820CurrentAbilityToDressLower", "02", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("02"), "2", "Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1820CurrentAbilityToDressLower", "03", data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").Equals("03"), "3", "Patient depends entirely upon another person to dress lower body.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1820')" status="More Information about M1820">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Bath/Toilet</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1830">
                <label>
                    <a status="More Information about M1830" class="green" onclick="Oasis.Help('M1830')">(M1830)</a>
                    Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "", new { @id = Model.TypeName + "_M1830CurrentAbilityToBatheEntireBodyHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "00", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("00"), "0", "Able to bathe self in shower or tub independently, including getting in and out of tub/shower.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "01", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("01"), "1", "With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "02", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("02"), "2", "Able to bathe in shower or tub with the intermittent assistance of another person:<ul><li><span class=\"fl\">(a)</span><span class=\"margin\">for intermittent supervision or encouragement or reminders, OR</span></li><li><span class=\"fl\">(b)</span><span class=\"margin\">to get in and out of the shower or tub, OR</span></li><li><span class=\"fl\">(c)</span><span class=\"margin\">for washing difficult to reach areas.</span></li></ul>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "03", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("03"), "3", "Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "04", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("04"), "4", "Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "05", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("05"), "5", "Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1830CurrentAbilityToBatheEntireBody", "06", data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").Equals("06"), "6", "Unable to participate effectively in bathing and is bathed totally by another person.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1830')" status="More Information about M1830">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1840">
                <label>
                    <a status="More Information about M1840" class="green" onclick="Oasis.Help('M1840')">(M1840)</a>
                    Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1840ToiletTransferring", "", new { @id = Model.TypeName + "_M1840ToiletTransferringHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1840ToiletTransferring", "00", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("00"), "0", "Able to get to and from the toilet and transfer independently with or without a device.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1840ToiletTransferring", "01", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("01"), "1", "When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1840ToiletTransferring", "02", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("02"), "2", "Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1840ToiletTransferring", "03", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("03"), "3", "Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1840ToiletTransferring", "04", data.AnswerOrEmptyString("M1840ToiletTransferring").Equals("04"), "4", "Is totally dependent in toileting.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1840')" status="More Information about M1840">?</a></div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1845">
                <label>
                    <a status="More Information about M1845" class="green" onclick="Oasis.Help('M1845')">(M1845)</a>
                    Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1845ToiletingHygiene", "", new { @id = Model.TypeName + "_M1845ToiletingHygieneHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1845ToiletingHygiene", "00", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("00"), "0", "Able to manage toileting hygiene and clothing management without assistance.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1845ToiletingHygiene", "01", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("01"), "1", "Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1845ToiletingHygiene", "02", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("02"), "2", "Someone must help the patient to maintain toileting hygiene and/or adjust clothing.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1845ToiletingHygiene", "03", data.AnswerOrEmptyString("M1845ToiletingHygiene").Equals("03"), "3", "Patient depends entirely upon another person to maintain toileting hygiene.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1845')" status="More Information about M1845">?</a></div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Transferring</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1850">
                <label>
                    <a status="More Information about M1850" class="green" onclick="Oasis.Help('M1850')">(M1850)</a>
                    Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1850Transferring", "", new { @id = Model.TypeName + "_M1850TransferringHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "00", data.AnswerOrEmptyString("M1850Transferring").Equals("00"), "0", "Able to independently transfer.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "01", data.AnswerOrEmptyString("M1850Transferring").Equals("01"), "1", "Able to transfer with minimal human assistance or with use of an assistive device.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "02", data.AnswerOrEmptyString("M1850Transferring").Equals("02"), "2", "Able to bear weight and pivot during the transfer process but unable to transfer self.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "03", data.AnswerOrEmptyString("M1850Transferring").Equals("03"), "3", "Unable to transfer self and is unable to bear weight or pivot when transferred by another person.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "04", data.AnswerOrEmptyString("M1850Transferring").Equals("04"), "4", "Bedfast, unable to transfer but is able to turn and position self in bed.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1850Transferring", "05", data.AnswerOrEmptyString("M1850Transferring").Equals("05"), "5", "Bedfast, unable to transfer and is unable to turn and position self.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1850')" status="More Information about M1850">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Ambulation/Locomotion</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1860">
                <label>
                    <a status="More Information about M1860" class="green" onclick="Oasis.Help('M1860')">(M1860)</a>
                    Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1860AmbulationLocomotion", "", new { @id = Model.TypeName + "_M1860AmbulationLocomotionHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "00", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("00"), "0", "Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "01", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("01"), "1", "With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "02", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("02"), "2", "Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "03", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("03"), "3", "Able to walk only with the supervision or assistance of another person at all times.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "04", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("04"), "4", "Chairfast, unable to ambulate but is able to wheel self independently.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "05", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("05"), "5", "Chairfast, unable to ambulate and is unable to wheel self.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1860AmbulationLocomotion", "06", data.AnswerOrEmptyString("M1860AmbulationLocomotion").Equals("06"), "6", "Bedfast, unable to ambulate or be up in a chair.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1860')" status="More Information about M1860">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Feeding/Eating</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1870">
                <label>
                    <a status="More Information about M1870" class="green" onclick="Oasis.Help('M1870')">(M1870)</a>
                    Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1870FeedingOrEating", "", new { @id = Model.TypeName + "_M1870FeedingOrEatingHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "00", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("00"), "0", "Able to independently feed self.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "01", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("01"), "1", "Able to feed self independently but requires:<ul><li><span class=\"fl\">(a)</span><span class=\"margin\">meal set-up; OR</span></li><li><span class=\"fl\">(b)</span><span class=\"margin\">intermittent assistance or supervision from another person; OR</span></li><li><span class=\"fl\">(c)</span><span class=\"margin\">a liquid, pureed or ground meat diet.</span></li></ul>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "02", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("02"), "2", "Unable to feed self and must be assisted or supervised throughout the meal/snack.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "03", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("03"), "3", "Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "04", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("04"), "4", "Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1870FeedingOrEating", "05", data.AnswerOrEmptyString("M1870FeedingOrEating").Equals("05"), "5", "Unable to take in nutrients orally or by tube feeding.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1870')" status="More Information about M1870">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1880">
                <label>
                    <a status="More Information about M1880" class="green" onclick="Oasis.Help('M1880')">(M1880)</a>
                    Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "", new { @id = Model.TypeName + "_M1880AbilityToPrepareLightMealHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "00", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("00"), "0", "<ul><li><span class=\"fl\">(a)</span><span class=\"margin\">Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</span></li><li><span class=\"fl\">(b)</span><span class=\"margin\">Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</span></li></ul>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "01", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("01"), "1", "Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1880AbilityToPrepareLightMeal", "02", data.AnswerOrEmptyString("M1880AbilityToPrepareLightMeal").Equals("02"), "2", "Unable to prepare any light meals or reheat any delivered meals.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1880')" status="More Information about M1880">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Telephone Ability</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1890">
                <label>
                    <a status="More Information about M1890" class="green" onclick="Oasis.Help('M1890')">(M1890)</a>
                    Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1890AbilityToUseTelephone", "", new { @id = Model.TypeName + "_M1890AbilityToUseTelephoneHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "00", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("00"), "0", "Able to dial numbers and answer calls appropriately and as desired.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "01", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("01"), "1", "Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "02", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("02"), "2", "Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "03", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("03"), "3", "Able to answer the telephone only some of the time or is able to carry on only a limited conversation.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "04", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("04"), "4", "Unable to answer the telephone at all but can listen if assisted with equipment.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "05", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("05"), "5", "Totally unable to use the telephone.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1890AbilityToUseTelephone", "NA", data.AnswerOrEmptyString("M1890AbilityToUseTelephone").Equals("NA"), "NA", "Patient does not have a telephone.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1890')" status="More Information about M1890">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Adl.ascx", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1900">
                <label>
                    <a status="More Information about M1900" class="green" onclick="Oasis.Help('M1900')">(M1900)</a>
                    Prior Functioning ADL/IADL: Indicate the patient&#8217;s usual ability with everyday activities prior to this current illness, exacerbation, or injury. Check only one box in each row.
                </label>
                <div class="subrow">
                    <label class="fl" for="<%= Model.TypeName %>">a. Self-Care (e.g., grooming, dressing, and bathing)</label>
                    <div class="fr">
                        <%  var M1900SelfCareFunctioning = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "0 - Independent", Value = "00" },
                                new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                                new SelectListItem { Text = "2 - Dependent", Value = "02" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1900SelfCareFunctioning"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1900SelfCareFunctioning", M1900SelfCareFunctioning, new { @id = Model.TypeName + "_M1900SelfCareFunctioning", @status = "(OASIS M1900) Prior Functioning ADL/IADL, Self-Care" })%>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="subrow">
                    <label class="fl" for="<%= Model.TypeName %>">b. Ambulation</label>
                    <div class="fr">
                        <%  var M1900Ambulation = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "0 - Independent", Value = "00" },
                                new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                                new SelectListItem { Text = "2 - Dependent", Value = "02" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1900Ambulation"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1900Ambulation", M1900Ambulation, new { @id = Model.TypeName + "_M1900Ambulation", @status = "(OASIS M1900) Prior Functioning ADL/IADL, Ambulation" })%>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="subrow">
                    <label class="fl" for="<%= Model.TypeName %>">c. Transfer</label>
                    <div class="fr">
                        <%  var M1900Transfer = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "0 - Independent", Value = "00" },
                                new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                                new SelectListItem { Text = "2 - Dependent", Value = "02" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1900Transfer"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1900Transfer", M1900Transfer, new { @id = Model.TypeName + "_M1900Transfer", @status = "(OASIS M1900) Prior Functioning ADL/IADL, Transfer" })%>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="subrow">
                    <label class="fl" for="<%= Model.TypeName %>">d. Household tasks (e.g., light meal preparation, laundry, shopping)</label>
                    <div class="fr">
                        <%  var M1900HouseHoldTasks = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "0 - Independent", Value = "00" },
                                new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                                new SelectListItem { Text = "2 - Dependent", Value = "02" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1900HouseHoldTasks"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1900HouseHoldTasks", M1900HouseHoldTasks, new { @id = Model.TypeName + "_M1900HouseHoldTasks", @status = "(OASIS M1900) Prior Functioning ADL/IADL, Household Tasks" })%>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1900')" status="More Information about M1900">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Fall Assessment</legend>
        <div id="<%= Model.TypeName %>_FallAssessment" class="wide-column">
            <div class="row no-input">
                <label>
                    Select all that apply
                    <em>One point is assessed for each one selected</em>
                </label>
                <ul class="checkgroup one-wide">
                    <%= Html.Hidden(Model.TypeName + "_GenericAge65Plus", "", new { @id = Model.TypeName + "_GenericAge65PlusHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericAge65Plus", "1", data.AnswerOrEmptyString("GenericAge65Plus").Equals("1"), "Age 65+")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericHypotensionDiagnosis", "", new { @id = Model.TypeName + "_GenericHypotensionDiagnosisHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHypotensionDiagnosis", "1", data.AnswerOrEmptyString("GenericHypotensionDiagnosis").Equals("1"), "Diagnosis (3 or more co-existing)<br /><em>Assess for hypotension.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericPriorFalls", "", new { @id = Model.TypeName + "_GenericPriorFallsHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPriorFalls", "1", data.AnswerOrEmptyString("GenericPriorFalls").Equals("1"), "Prior history of falls within 3 months<br /><em>Fall definition: &#8220;An unintentional change in position resulting in coming to rest on the ground or at a lower level.&#8221;</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericFallIncontinence", "", new { @id = Model.TypeName + "_GenericFallIncontinenceHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericFallIncontinence", "1", data.AnswerOrEmptyString("GenericFallIncontinence").Equals("1"), "Incontinence<br /><em>Inability to make it to the bathroom or commode in timely manner. Includes frequency, urgency, and/or nocturia.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericVisualImpairment", "", new { @id = Model.TypeName + "_GenericVisualImpairmentHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericVisualImpairment", "1", data.AnswerOrEmptyString("GenericVisualImpairment").Equals("1"), "Visual impairment<br /><em>Includes macular degeneration, diabetic retinopathies, visual field loss, age related changes, decline in visual acuity, accommodation, glare tolerance, depth perception, and night vision or not wearing prescribed glasses or having the correct prescription.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericImpairedFunctionalMobility", "", new { @id = Model.TypeName + "_GenericImpairedFunctionalMobilityHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericImpairedFunctionalMobility", "1", data.AnswerOrEmptyString("GenericImpairedFunctionalMobility").Equals("1"), "Impaired functional mobility<br /><em>May include patients who need help with IADL&#8217;s or ADL&#8217;s or have gait or transfer problems, arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination or improper use of assistive devices.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericEnvHazards", "", new { @id = Model.TypeName + "_GenericEnvHazardsHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEnvHazards", "1", data.AnswerOrEmptyString("GenericEnvHazards").Equals("1"), "Environmental hazards<br /><em>May include poor illumination, equipment tubing, inappropriate footwear, pets, hard to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and exits.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericPolyPharmacy", "", new { @id = Model.TypeName + "_GenericPolyPharmacyHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPolyPharmacy", "1", data.AnswerOrEmptyString("GenericPolyPharmacy").Equals("1"), "Poly Pharmacy (4 or more prescriptions)<br /><em>Drugs highly associated with fall risk include but are not limited to, sedatives, anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids, anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericPainAffectingFunction", "", new { @id = Model.TypeName + "_GenericPainAffectingFunctionHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPainAffectingFunction", "1", data.AnswerOrEmptyString("GenericPainAffectingFunction").Equals("1"), "Pain affecting level of function<br /><em>Pain often affects an individual&#8217;s desire or ability to move or pain can be a factor in depression or compliance with safety recommendations.</em>")%>
                    <%= Html.Hidden(Model.TypeName + "_GenericCognitiveImpairment", "", new { @id = Model.TypeName + "_GenericCognitiveImpairmentHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericCognitiveImpairment", "1", data.AnswerOrEmptyString("GenericCognitiveImpairment").Equals("1"), "Cognitive impairment<br /><em>Could include patients with dementia, Alzheimer&#8217;s or stroke patients or patients who are confused, use poor judgment, have decreased comprehension, impulsivity, memory deficits. Consider patient&#8217;s ability to adhere to the plan of care.</em>")%>
                </ul>
            </div>
            <div class="row narrowest">
                <label class="fl">Total</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485FallAssessmentScore", data.AnswerOrEmptyString("485FallAssessmentScore"), new { @id = Model.TypeName + "_485FallAssessmentScore", @class = "shortest", @maxlength = "10" })%></div>
            </div>
            <div class="row ac">
                <em>A score of 4 or more is considered at risk for falling</em>
            </div>
        </div>
    </fieldset>
        <%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
    <fieldset id="<%= Model.TypeName %>_Tinetti">
        <legend>Tinetti Assessment</legend>    
        <div class="column">
            <div class="row no-input">
                <label>
                    Balance Test<br />
                    With the patient seated in a hard, armless chair, test on the following maneuvers
                </label>
            </div>
            <div class="row no-input">
                <label>1. Sitting balance</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSitting", "0", data.AnswerOrEmptyString("GenericTinettiSitting").Equals("0"), "0", "Leans or slides in chair", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSitting", "1", data.AnswerOrEmptyString("GenericTinettiSitting").Equals("1"), "1", "Steady, safe", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>2. Arises</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiArises", "0", data.AnswerOrEmptyString("GenericTinettiArises").Equals("0"), "0", "Unable without Help", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiArises", "1", data.AnswerOrEmptyString("GenericTinettiArises").Equals("1"), "1", "Able, uses arms to help", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiArises", "2", data.AnswerOrEmptyString("GenericTinettiArises").Equals("2"), "2", "Able without using arms", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>3. Attempts to arise</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiAttemptsToArise", "0", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("0"), "0", "Unable without Help", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiAttemptsToArise", "1", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("1"), "1", "Able, requires greater than 1 attempt", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiAttemptsToArise", "2", data.AnswerOrEmptyString("GenericTinettiAttemptsToArise").Equals("2"), "2", "Able to rise, 1 attempt", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>4. Immediate standing balance (first 5 seconds)</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiImmediateStanding", "0", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("0"), "0", "Unsteady (swaggers, moves feet, trunk sway)", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiImmediateStanding", "1", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("1"), "1", "Steady but uses walker or other support", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiImmediateStanding", "2", data.AnswerOrEmptyString("GenericTinettiImmediateStanding").Equals("2"), "2", "Steady without walker or other support", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>5. Standing balance</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiStanding", "0", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("0"), "0", "Unsteady", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiStanding", "1", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("1"), "1", "Steady but wide stance (medial heels greater than 4 inches apart) and uses cane or other support", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiStanding", "2", data.AnswerOrEmptyString("GenericTinettiStanding").Equals("2"), "2", "Narrow stance without support", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>6. Nudged (subject at max position with feet as close together as possible, examiner pushes lightly on subject&#8217;s sternum with palm of hand 3 times)</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiNudged", "0", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("0"), "0", "Begins to fall", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiNudged", "1", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("1"), "1", "Staggers, grabs, catches self", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiNudged", "2", data.AnswerOrEmptyString("GenericTinettiNudged").Equals("2"), "2", "Steady", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>7. Eyes closed (at maximum position #6)</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiEyesClosed", "0", data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("0"), "0", "Unsteady", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiEyesClosed", "1", data.AnswerOrEmptyString("GenericTinettiEyesClosed").Equals("1"), "1", "Steady", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>8. Turning 360 degrees</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTurningContinuous", "0", data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("0"), "0", "Discontinuous steps", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTurningContinuous", "1", data.AnswerOrEmptyString("GenericTinettiTurningContinuous").Equals("1"), "1", "Continuous steps", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTurningSteady", "0", data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("0"), "0", "Unsteady (grabs, swaggers)", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTurningSteady", "1", data.AnswerOrEmptyString("GenericTinettiTurningSteady").Equals("1"), "1", "Steady", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>9. Sitting down</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSittingDown", "0", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("0"), "0", "Unsafe (misjudged distance, falls into chair)", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSittingDown", "1", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("1"), "1", "Uses arms or not a smooth motion", new { @class = "TinettiBalance" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSittingDown", "2", data.AnswerOrEmptyString("GenericTinettiSittingDown").Equals("2"), "2", "Safe, smooth motion", new { @class = "TinettiBalance" })%>
                </ul>
            </div>
            <div class="row">
                <label class="fl">Balance Total</label>
                <div class="fr"><input type="text" name="<%= Model.TypeName %>_TinettiBalanceTotal" value="" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row no-input">
                <label>
                    Gait Test<br />
                    Patient stands, walks down hallway or across the room, first at &#8220;unusual&#8221; pace, then back at &#8220;rapid, but safe&#8221; pace (using usual walking aids)
                </label>
            </div>
            <div class="row no-input">
                <label>10. Initiation of gait (immediately after told to &#8220;go&#8221;)</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiGait", "0", data.AnswerOrEmptyString("GenericTinettiGait").Equals("0"), "0", "Any hesitancy or multiple attempts to start", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiGait", "1", data.AnswerOrEmptyString("GenericTinettiGait").Equals("1"), "1", "No hesitancy", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>11. Step length and height</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiRightPassLeft", "0", data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("0"), "0", "a. Right swing foot does not pass left stance foot with step", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiRightPassLeft", "1", data.AnswerOrEmptyString("GenericTinettiRightPassLeft").Equals("1"), "1", "b. Right foot passes left stance foot", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiRightClear", "0", data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("0"), "0", "c. Right foot does not clear floor completely with step", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiRightClear", "1", data.AnswerOrEmptyString("GenericTinettiRightClear").Equals("1"), "1", "d. Right foot completely clears floor", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiLeftPassRight", "0", data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("0"), "0", "e. Left swing foot does not pass right stance foot with step", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiLeftPassRight", "1", data.AnswerOrEmptyString("GenericTinettiLeftPassRight").Equals("1"), "1", "f. Left foot passes right stance foot", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiLeftClear", "0", data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("0"), "0", "g. Left foot does not clear floor completely with step", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiLeftClear", "1", data.AnswerOrEmptyString("GenericTinettiLeftClear").Equals("1"), "1", "h. Left foot completely clears floor", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>12. Step symmetry</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSymmetry", "0", data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("0"), "0", "Right and left step length not equal (estimate)", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiSymmetry", "1", data.AnswerOrEmptyString("GenericTinettiSymmetry").Equals("1"), "1", "Right and left step appear equal", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>13. Step continually</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiContinually", "0", data.AnswerOrEmptyString("GenericTinettiContinually").Equals("0"), "0", "Stopping or discontinuity between steps", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiContinually", "1", data.AnswerOrEmptyString("GenericTinettiContinually").Equals("1"), "1", "Steps appear continuous", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>14. Path (estimated in relation to floor tiles, 12-inch diameter; observe excursion of 1 foot over about 10 feet of the course)</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiPath", "0", data.AnswerOrEmptyString("GenericTinettiPath").Equals("0"), "0", "Marked deviation", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiPath", "1", data.AnswerOrEmptyString("GenericTinettiPath").Equals("1"), "1", "Mild/moderate deviation or uses walking aid", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiPath", "2", data.AnswerOrEmptyString("GenericTinettiPath").Equals("2"), "2", "Straight without walking aid", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>15. Trunk</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTrunk", "0", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("0"), "0", "Marked sway or uses walking aid", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTrunk", "1", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("1"), "1", "No sway but flexion of knees or back, or spreads arms out while walking", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiTrunk", "2", data.AnswerOrEmptyString("GenericTinettiTrunk").Equals("2"), "2", "No sway, no flexion, no use of arms, and no use of walking aid", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row no-input">
                <label>16. Walking stance</label>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiWalking", "0", data.AnswerOrEmptyString("GenericTinettiWalking").Equals("0"), "0", "Heels apart", new { @class = "TinettiGait" })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericTinettiWalking", "1", data.AnswerOrEmptyString("GenericTinettiWalking").Equals("1"), "1", "Heels almost touching while walking", new { @class = "TinettiGait" })%>
                </ul>
            </div>
            <div class="row">
                <label class="fl">Gait Total</label>
                <div class="fr"><input type="text" name="<%= Model.TypeName %>_TinettiGaitTotal" value="" /></div>
            </div>
            <div class="row no-input">
                <label>Key for Scoring</label>
                <ul class="tinetti_score">
                    <li>Maximum score is 28</li>
                    <li>Score below 19 &#8220;High risk of falling&#8221;</li>
                    <li>Score in 19&#8211;24 range &#8220;Greater chance of falling&#8221; but is not at &#8220;High risk&#8221;</li>
                    <li>Score in 24&#8211;28 range &#8220;Low risk of falling&#8221;</li>
                </ul>
            </div>
            <div class="row">
                <label class="fl">Total (Balance and Gait)</label>
                <div class="fr"><input type="text" name="<%= Model.TypeName %>_TinettiTotal" value="" /></div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset>
        <legend>Timed Up and Go</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>" class="fl">Timed Up and Go Findings</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericTimedUpAndFindings", data.AnswerOrEmptyString("GenericTimedUpAndFindings"), new { @id = Model.TypeName + "_GenericTimedUpAndFindings", @maxlength = "3" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row ac">
                <em>&#60; 10 seconds= Normal, &#60; 14 seconds= not a falls risk, &#62; 14 seconds = increased risk for falls</em>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Fall Risk Assessment</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1910">
                <label>
                    <a status="More Information about M1910" class="green" onclick="Oasis.Help('M1910')">(M1910)</a>
                    Has this patient had a multi-factor Fall Risk Assessment (such as falls history, use of multiple medications, mental impairment, toileting frequency, general mobility/transferring impairment, environmental hazards)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1910FallRiskAssessment", "", new { @id = Model.TypeName + "_M1910FallRiskAssessmentHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1910FallRiskAssessment", "00", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("00"), "0", "No multi-factor falls risk assessment conducted.") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1910FallRiskAssessment", "01", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("01"), "1", "Yes, and it does not indicate a risk for falls.") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1910FallRiskAssessment", "02", data.AnswerOrEmptyString("M1910FallRiskAssessment").Equals("02"), "2", "Yes, and it indicates a risk for falls.") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1910')" status="More Information about M1910">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Iadl.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>