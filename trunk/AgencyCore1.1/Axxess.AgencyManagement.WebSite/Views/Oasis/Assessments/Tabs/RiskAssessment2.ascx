﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 4 && Model.AssessmentTypeNum.ToInteger() % 10 != 5) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "RiskAssessmentForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", AssessmentCategory.RiskAssessment.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
	<fieldset>
	    <legend>Immunizations</legend>
	    <div class="column">
	        <div class="row">
	            <label class="fl">Pneumonia</label>
	            <%= Html.Hidden(Model.TypeName + "_485Pnemonia", " ", new { @id = Model.TypeName + "_485PnemoniaHidden" })%>
	            <div class="fr">
	                <%  var Pnemonia = new SelectList(new[] {
					        new SelectListItem { Text = "", Value = "" },
					        new SelectListItem { Text = "Yes", Value = "Yes" },
					        new SelectListItem { Text = "No", Value = "No"},
					        new SelectListItem { Text = "Unknown", Value = "UK" }
					    }, "Value", "Text", data.AnswerOrEmptyString("485Pnemonia")); %>
				    <%= Html.DropDownList(Model.TypeName + "_485Pnemonia", Pnemonia, new { @class = "shorter" })%>
				    <input type="text" class="date-picker short" name="<%= Model.TypeName %>_485PnemoniaDate" value="<%= data.AnswerOrEmptyString("485PnemoniaDate") %>" id="<%= Model.TypeName %>_485PnemoniaDate" />
			    </div>
			</div>
			<div class="row">
			    <label class="fl">Flu</label>
			    <%= Html.Hidden(Model.TypeName + "_485Flu")%>
				<div class="fr">
					<%  var Flu = new SelectList(new[] {
						    new SelectListItem { Text = "", Value = "" },
						    new SelectListItem { Text = "Yes", Value = "Yes" },
						    new SelectListItem { Text = "No", Value = "No"},
						    new SelectListItem { Text = "Unknown", Value = "UK" }
						}, "Value", "Text", data.AnswerOrEmptyString("485Flu")); %>
					<%= Html.DropDownList(Model.TypeName + "_485Flu", Flu, new { @class = "shorter" })%>
					<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485FluDate" value="<%= data.AnswerOrEmptyString("485FluDate") %>" id="<%= Model.TypeName %>_485FluDate" />
				</div>
			</div>
			<div class="row">
			    <label class="fl">TB</label>
			    <%= Html.Hidden(Model.TypeName + "_485TB")%>
				<div class="fr">
					<%  var TB = new SelectList(new[] {
						    new SelectListItem { Text = "", Value = "" },
						    new SelectListItem { Text = "Yes", Value = "Yes" },
						    new SelectListItem { Text = "No", Value = "No"},
						    new SelectListItem { Text = "Unknown", Value = "UK" }
						}, "Value", "Text", data.AnswerOrEmptyString("485TB")); %>
					<%= Html.DropDownList(Model.TypeName + "_485TB", TB, new { @class = "shorter" })%>
					<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485TBDate" value="<%= data.AnswerOrEmptyString("485TBDate") %>" id="<%= Model.TypeName %>_485TBDate" />
				</div>
			</div>
			<div class="row">
				<label class="fl">TB Exposure</label>
				<%= Html.Hidden(Model.TypeName + "_485TBExposure")%>
				<div class="fr">
					<%  var TBExposure = new SelectList(new[] {
						    new SelectListItem { Text = "", Value = "" },
						    new SelectListItem { Text = "Yes", Value = "Yes" },
						    new SelectListItem { Text = "No", Value = "No"},
						    new SelectListItem { Text = "Unknown", Value = "UK" }
						}, "Value", "Text", data.AnswerOrEmptyString("485TBExposure")); %>
					<%= Html.DropDownList(Model.TypeName + "_485TBExposure", TBExposure, new { @class = "shorter" })%>
					<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485TBExposureDate" value="<%= data.AnswerOrEmptyString("485TBExposureDate") %>" id="<%= Model.TypeName %>_485TBExposureDate" />
				</div>
			</div>
        </div>
	    <div class="column">
			<div class="row ac">Additional Immunizations</div>
			<div class="row">
				<div class="fl"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization1Name", data.AnswerOrEmptyString("485AdditionalImmunization1Name"), new { @id = Model.TypeName + "_485AdditionalImmunization1Name" }) %></div>
				<%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization1")%>
				<div class="fr">
					<%  var AdditionalImmunization1 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Yes", Value = "Yes" },
						new SelectListItem { Text = "No", Value = "No"},
						new SelectListItem { Text = "Unknown", Value = "UK" }
						}, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization1")); %>
					<%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization1", AdditionalImmunization1, new { @class = "shorter" })%>
					<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485AdditionalImmunization1Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization1Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization1Date" />
				</div>
			</div>
			<div class="row">
				<div class="fl"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization2Name", data.AnswerOrEmptyString("485AdditionalImmunization2Name"), new { @id = Model.TypeName + "_485AdditionalImmunization2Name" }) %></div>
				<%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization2")%>
				<div class="fr">
					<%  var AdditionalImmunization2 = new SelectList(new[] {
						new SelectListItem { Text = "", Value = "" },
						new SelectListItem { Text = "Yes", Value = "Yes" },
						new SelectListItem { Text = "No", Value = "No"},
						new SelectListItem { Text = "Unknown", Value = "UK" }
						}, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization2")); %>
					<%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization2", AdditionalImmunization2, new { @class = "shorter" })%>
					<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485AdditionalImmunization2Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization2Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization2Date" />
				</div>
			</div>
		</div>
		<div class="wide-column">
			<div class="row">
				<label for="<%= Model.TypeName %>_485ImmunizationComments">Comments</label>
				<div class="ac"><%= Html.TextArea(Model.TypeName + "_485ImmunizationComments", data.AnswerOrEmptyString("485ImmunizationComments"), new { @id = Model.TypeName + "_485ImmunizationComments" }) %></div>
			</div>
		</div>
	</fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Risk for Hospitalization</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1032">
                <label>
                    <a title="More Information about M1032" class="green" onclick="Oasis.Help('M1032')">(M1032)</a>
                    Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization?
                    <em>(Mark all that apply)</em>
                </label>
                <ul class="checkgroup one-wide">
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskRecentDecline", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskRecentDeclineHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskRecentDecline", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskRecentDecline").Equals("1"), "1", "Recent decline in mental, emotional, or behavioral status", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskMultipleHosp", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskMultipleHospHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskMultipleHosp", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskMultipleHosp").Equals("1"), "2", "Multiple hospitalizations (2 or more) in the past 12 months", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskHistoryOfFall", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskHistoryOfFallHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskHistoryOfFall", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskHistoryOfFall").Equals("1"), "3", "History of falls (2 or more falls, or any fall with an injury, in the past year)", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskMedications", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskMedicationsHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskMedications", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskMedications").Equals("1"), "4", "Taking five or more medications", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskFrailty", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskFrailtyHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskFrailty", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskFrailty").Equals("1"), "5", "Frailty indicators, e.g., weight loss, self-reported exhaustion", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskOther", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskOtherHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskOther", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskOther").Equals("1"), "6", "Other", new { @class = "M1032" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskNone", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskNoneHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1032HospitalizationRiskNone", "1", data.AnswerOrEmptyString("M1032HospitalizationRiskNone").Equals("1"), "7", "None of the above", new { @class = "M1032" })%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1032')" title="More Information about M1032">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Overall Status</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1034">
                <label>
                    <a title="More Information about M1034" class="green" onclick="Oasis.Help('M1034')">(M1034)</a>
                    Overall Status: Which description best fits the patient&#8217;s overall status?
                    <em>(Check one)</em>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1034OverallStatus", "", new { @id = Model.TypeName + "_M1034OverallStatusHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1034OverallStatus", "00", data.AnswerOrEmptyString("M1034OverallStatus").Equals("00"), "0", "The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient&#8217;s age).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1034OverallStatus", "01", data.AnswerOrEmptyString("M1034OverallStatus").Equals("01"), "1", "The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications and death (beyond those typical of the patient&#8217;s age).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1034OverallStatus", "02", data.AnswerOrEmptyString("M1034OverallStatus").Equals("02"), "2", "The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1034OverallStatus", "03", data.AnswerOrEmptyString("M1034OverallStatus").Equals("03"), "3", "The patient has serious progressive conditions that could lead to death within a year.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1034OverallStatus", "UK", data.AnswerOrEmptyString("M1034OverallStatus").Equals("UK"), "UK", "The patient&#8217;s situation is unknown or unclear.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1034')" title="More Information about M1034">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Risk Factors</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1036">
                <label>
                    <a title="More Information about M1036" class="green" onclick="Oasis.Help('M1036')">(M1036)</a>
                    Risk Factors, either present or past, likely to affect current health status and/or outcome:
                    <em>(Mark all that apply)</em>
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsSmoking", "", new { @id = Model.TypeName + "_M1036RiskFactorsSmokingHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsSmoking", "1", data.AnswerOrEmptyString("M1036RiskFactorsSmoking").Equals("1"), "1", "Smoking", new { @class = "M1036" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsObesity", "", new { @id = Model.TypeName + "_M1036RiskFactorsObesityHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsObesity", "1", data.AnswerOrEmptyString("M1036RiskFactorsObesity").Equals("1"), "2", "Obesity", new { @class = "M1036" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsAlcoholism", "", new { @id = Model.TypeName + "_M1036RiskFactorsAlcoholismHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsAlcoholism", "1", data.AnswerOrEmptyString("M1036RiskFactorsAlcoholism").Equals("1"), "3", "Alcohol dependency", new { @class = "M1036" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsDrugs", "", new { @id = Model.TypeName + "_M1036RiskFactorsDrugsHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsDrugs", "1", data.AnswerOrEmptyString("M1036RiskFactorsDrugs").Equals("1"), "4", "Drug dependency", new { @class = "M1036" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsNone", "", new { @id = Model.TypeName + "_M1036RiskFactorsNoneHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsNone", "1", data.AnswerOrEmptyString("M1036RiskFactorsNone").Equals("1"), "5", "None of the above", new { @class = "M1036" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsUnknown", "", new { @id = Model.TypeName + "__M1036RiskFactorsUnknownHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1036RiskFactorsUnknown", "1", data.AnswerOrEmptyString("M1036RiskFactorsUnknown").Equals("1"), "UK", "Unknown", new { @class = "M1036" })%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1036')" title="More Information about M1036">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() > 5) { %>
    <fieldset class="oasis">
        <legend>Vaccinations</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1040">
                <label>
                    <a class="green" onclick="Oasis.Help('M1040')">(M1040)</a>
                    Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&#8217;s influenza season (October 1 through March 31) during this episode of care?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1040InfluenzaVaccine", "", new { @id = Model.TypeName + "_M1040InfluenzaVaccineHidden" })%>
                <ul class="two-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1040InfluenzaVaccine", "00", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1040InfluenzaVaccine", "01", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("01"), "1", "Yes") %>
                </ul>
                <ul class="one-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1040InfluenzaVaccine", "NA", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("NA"), "NA", "Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season.") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1040')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1045">
                <label>
                    <a class="green" onclick="Oasis.Help('M1045')">(M1045)</a>
                    Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "", new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReasonHidden" })%>
                <ul class="one-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "01", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("01"), "1", "Received from another health care provider (e.g., physician)") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "02", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("02"), "2", "Received from your agency previously during this year&#8217;s flu season") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "03", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("03"), "3", "Offered and declined") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "04", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("04"), "4", "Assessed and determined to have medical contraindication(s)") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "05", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("05"), "5", "Not indicated; patient does not meet age/condition guidelines for influenza vaccine") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "06", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("06"), "6", "Inability to obtain vaccine due to declared shortage") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "07", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("07"), "7", "None of the above") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1045')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1050">
                <label>
                    <a class="green" onclick="Oasis.Help('M1050')">(M1050)</a>
                    Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1050PneumococcalVaccine", "", new { @id = Model.TypeName + "_M1050PneumococcalVaccineHidden" })%>
                <ul class="two-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1050PneumococcalVaccine", "0", data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("0"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1050PneumococcalVaccine", "1", data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("1"), "1", "Yes") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1050')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1055">
                <label>
                    <a class="green" onclick="Oasis.Help('M1055')">(M1055)</a>
                    Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1055PPVNotReceivedReason", "", new { @id = Model.TypeName + "_M1055PPVNotReceivedReasonHidden" })%>
                <ul class="one-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1055PPVNotReceivedReason", "01", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("01"), "1", "Patient has received PPV in the past") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1055PPVNotReceivedReason", "02", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("02"), "2", "Offered and declined") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1055PPVNotReceivedReason", "03", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("03"), "3", "Assessed and determined to have medical contraindication(s)") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1055PPVNotReceivedReason", "04", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("04"), "4", "Not indicated; patient does not meet age/condition guidelines for PPV") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1055PPVNotReceivedReason", "05", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("05"), "5", "None of the above") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1055')">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/RiskAssessment.ascx", Model); %>
    <% Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>