﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main"> 
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "IntegumentaryForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>  
    <%= Html.Hidden("categoryType", AssessmentCategory.Integumentary.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset id="<%= Model.TypeName %>_BradenScale">
        <legend>Braden Scale</legend>
        <div class="wide-column">
            <div class="row no-input">
                <p><strong>Sensory Perception</strong> &#8211; Ability to respond meaningfully to pressure-related discomfort</p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleSensory", data.AnswerOrEmptyString("485BradenScaleSensory"), new { @id = Model.TypeName + "_485BradenScaleSensoryHidden" }) %>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleSensory", "1", data.AnswerOrEmptyString("485BradenScaleSensory").Equals("1"), "1", "Completely Limited", new { @tooltip = "Unresponsive (does not moan, flinch, or grasp) to painful stimuli, due to diminished level of consciousness or sedation or limited ability to feel pain over most of body." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleSensory", "2", data.AnswerOrEmptyString("485BradenScaleSensory").Equals("2"), "2", "Very Limited", new { @tooltip = "Responds only to painful stimuli. Cannot communicate discomfort except by moaning or restlessness or has a sensory impairment which limits the ability to feel pain or discomfort over &frac12; of body." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleSensory", "3", data.AnswerOrEmptyString("485BradenScaleSensory").Equals("3"), "3", "Slightly Limited", new { @tooltip = "Responds to verbal commands, but cannot always communicate discomfort or the need to be turned or has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleSensory", "4", data.AnswerOrEmptyString("485BradenScaleSensory").Equals("4"), "4", "No Impairment", new { @tooltip = "Responds to verbal commands. Has no sensory deficit which would limit ability to feel or voice pain or discomfort." })%>
                </ul>
            </div>
            <div class="row no-input">
                <p><strong>Moisture</strong> &#8211; Degree to which skin is exposed to moisture</p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleMoisture", data.AnswerOrEmptyString("485BradenScaleMoisture"), new { @id = Model.TypeName + "_485BradenScaleMoistureHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMoisture", "1", data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("1"), "1", "Constantly Moist", new { @tooltip = "Skin is kept moist almost constantly by perspiration, urine, etc. Dampness is detected every time patient is moved or turned." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMoisture", "2", data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("2"), "2", "Often Moist", new { @tooltip = "Skin is often, but not always moist. Linen must be changed as often as 3 times in 24 hours." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMoisture", "3", data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("3"), "3", "Occasionally Moist", new { @tooltip = "Skin is occasionally moist, requiring an extra linen change approximately once a day." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMoisture", "4", data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("4"), "4", "Rarely Moist", new { @tooltip = "Skin is usually dry; Linen only requires changing at routine intervals." })%>
                </ul>
            </div>
            <div class="row no-input">
                <p><strong>Activity</strong> &#8211; Degree of physical activity</p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleActivity", data.AnswerOrEmptyString("485BradenScaleActivity"), new { @id = Model.TypeName + "_485BradenScaleActivityHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleActivity", "1", data.AnswerOrEmptyString("485BradenScaleActivity").Equals("1"), "1", "Bedfast", new { @tooltip = "Confined to bed." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleActivity", "2", data.AnswerOrEmptyString("485BradenScaleActivity").Equals("2"), "2", "Chairfast", new { @tooltip = "Ability to walk severely limited or non-existent. Cannot bear own weight and/or must be assisted into chair or wheelchair." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleActivity", "3", data.AnswerOrEmptyString("485BradenScaleActivity").Equals("3"), "3", "Walks Occasionally", new { @tooltip = "Walks occasionally during day, but for very short distances, with or without assistance. Spends majority of day in bed or chair." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleActivity", "4", data.AnswerOrEmptyString("485BradenScaleActivity").Equals("4"), "4", "Walks Frequently", new { @tooltip = "Walks outside bedroom twice a day and inside room at least once every two hours during waking hours." })%>
                </ul>
            </div>
            <div class="row no-input">
                <p><strong>Mobility</strong> &#8211; Ability to change and control body position</p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleMobility", data.AnswerOrEmptyString("485BradenScaleMobility"), new { @id = Model.TypeName + "_485BradenScaleMobilityHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMobility", "1", data.AnswerOrEmptyString("485BradenScaleMobility").Equals("1"), "1", "Completely Immobile", new { @tooltip = "Does not make even slight changes in body or extremity position without assistance." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMobility", "2", data.AnswerOrEmptyString("485BradenScaleMobility").Equals("2"), "2", "Very Limited", new { @tooltip = "Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMobility", "3", data.AnswerOrEmptyString("485BradenScaleMobility").Equals("3"), "3", "Slightly Limited", new { @tooltip = "Makes frequent though slight changes in body or extremity position independently." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleMobility", "4", data.AnswerOrEmptyString("485BradenScaleMobility").Equals("4"), "4", "No Limitation", new { @tooltip = "Makes major and frequent changes in position without assistance." })%>
                </ul>
            </div>
            <div class="row no-input">
                <p><strong>Nutrition</strong> &#8211; Usual food intake pattern</p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleNutrition", data.AnswerOrEmptyString("485BradenScaleNutrition"), new { @id = Model.TypeName + "_485BradenScaleNutritionHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleNutrition", "1", data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("1"), "1", "Very Poor", new { @tooltip = "Never eats a complete meal. Rarely eats more than &#8531; of any food offered. Eats 2 servings or less of protein (meat or dairy products) per day. Takes fluids poorly. Does not take a liquid dietary supplement or is NPO and/or maintained on clear liquids or IV for more than 5 days." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleNutrition", "2", data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("2"), "2", "Probably Inadequate", new { @tooltip = "Rarely eats a complete meal and generally eats only about &frac12; of any food offered. Protein intake includes only 3 servings of meat or dairy products per day. Occasionally will take a dietary supplement or receives less than optimum amount of liquid diet or tube feeding." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleNutrition", "3", data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("3"), "3", "Adequate", new { @tooltip = "Eats over half of most meals. Eats a total of 4 servings of protein (meat, dairy products) per day. Occasionally will refuse a meal, but will usually take a supplement when offered or is on a tube feeding or TPN regimen which probably meets most of nutritional needs." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleNutrition", "4", data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("4"), "4", "Excellent", new { @tooltip = "Eats most of every meal. Never refuses a meal. Usually eats a total of 4 or more servings of meat and dairy products. Occasionally eats between meals. Does not require supplementation." })%>
                </ul>
            </div>
            <div class="row no-input">
                <p><strong>Friction &#38; Shear</strong></p>
                <%= Html.Hidden(Model.TypeName + "_485BradenScaleFriction", data.AnswerOrEmptyString("485BradenScaleFriction"), new { @id = Model.TypeName + "_485BradenScaleFrictionHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleFriction", "1", data.AnswerOrEmptyString("485BradenScaleFriction").Equals("1"), "1", "Problem", new { @tooltip = "Requires moderate to maximum assistance in moving. Complete lifting without sliding against sheets is impossible. Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance. Spasticity, contractures or agitation leads to almost constant friction." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleFriction", "2", data.AnswerOrEmptyString("485BradenScaleFriction").Equals("2"), "2", "Potential Problem", new { @tooltip = "Moves feebly or requires minimum assistance. During a move skin probably slides to some extent against sheets, chair, restraints or other devices. Maintains relatively good position in chair or bed most of the time but occasionally slides down." })%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_485BradenScaleFriction", "3", data.AnswerOrEmptyString("485BradenScaleFriction").Equals("3"), "3", "No Apparent Problem", new { @tooltip = "Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move. Maintains good position in bed or chair." })%>
                </ul>
            </div>
            <div class="row narrowest no-input ac">
                <label for="<%= Model.TypeName %>_485BradenScaleTotal" class="strong">Total</label>
                <%= Html.TextBox(Model.TypeName + "_485BradenScaleTotal", data.AnswerOrEmptyString("485BradenScaleTotal"), new { @id = Model.TypeName + "_485BradenScaleTotal", @class = "shortest", @readonly = "readonly" })%>
                <ul class="braden-score">
                    <li>19 or above: Not at Risk;</li>
                    <li>15-18: At risk;</li>
                    <li>13-14: Moderate risk;</li>
                    <li>10-12: High risk;</li>
                    <li>9 or below: Very high risk</li>
                </ul>
                <em>Copyright. Barbara Braden and Nancy Bergstrom, 1988. Reprinted with permission. All Rights Reserved.</em>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset>
        <legend>Integumentary Status</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericSkinTurgor" class="fl">Skin Turgor</label>
                <div class="fr">
                    <%  var GenericSkinTurgor = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Good", Value = "Good" },
                            new SelectListItem { Text = "Fair", Value = "Fair" },
                            new SelectListItem { Text = "Poor", Value = "Poor" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericSkinTurgor")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSkinTurgor", GenericSkinTurgor, new { @id = Model.TypeName + "_GenericSkinTurgor", @status = "(Optional) Skin Turgor" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Skin Temperature</label>
                <div class="fr">
                    <%  var skinTemp = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Warm", Value = "1" },
                            new SelectListItem { Text = "Cool", Value = "2" },
                            new SelectListItem { Text = "Clammy", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericSkinTemp", "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSkinTemp", skinTemp, new { @id = Model.TypeName + "_GenericSkinTemp", @status = "(Optional) Skin Temperature" })%>
                </div>
            </div>
            <div class="row">
                <label>Skin Color</label>
                <%  string[] skinColor = data.AnswerArray("GenericSkinColor"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSkinColor", "", new { @id = Model.TypeName + "_GenericSkinColorHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinColor", "1", skinColor.Contains("1"), "Pink/WNL") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinColor", "2", skinColor.Contains("2"), "Pallor") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinColor", "3", skinColor.Contains("3"), "Jaundice") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinColor", "4", skinColor.Contains("4"), "Cyanotic") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Skin Color, Other">
                            <%= string.Format("<input id='{0}_GenericSkinColor5' name='{0}_GenericSkinColor' value='5' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericSkinColor5">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericSkinColorOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericSkinColorOther", data.AnswerOrEmptyString("GenericSkinColorOther"), new { @id = Model.TypeName + "_GenericSkinColorOther", @maxlength = "20", @status = "(Optional) Skin Color, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label>Condition</label>
                <%  string[] skinCondition = data.AnswerArray("GenericSkinCondition"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSkinCondition", "", new { @id = Model.TypeName + "_GenericSkinConditionHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinCondition", "1", skinCondition.Contains("1"), "Dry")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinCondition", "2", skinCondition.Contains("2"), "Wound")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinCondition", "3", skinCondition.Contains("3"), "Ulcer")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinCondition", "4", skinCondition.Contains("4"), "Incision")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSkinCondition", "5", skinCondition.Contains("5"), "Rash")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Skin Condition, Other">
                            <%= string.Format("<input id='{0}_GenericSkinCondition6' name='{0}_GenericSkinCondition' value='6' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericSkinCondition6">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericSkinConditionOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericSkinConditionOther", data.AnswerOrEmptyString("GenericSkinConditionOther"), new { @id = Model.TypeName + "_GenericSkinConditionOther", @maxlength = "20", @status = "(Optional) Skin Condition, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericInstructedControlInfections" class="fl">Instructed on measures to control infections?</label>
                <div class="fr">
                    <%  var GenericInstructedControlInfections = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericInstructedControlInfections")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericInstructedControlInfections", GenericInstructedControlInfections, new { @id = Model.TypeName + "_GenericInstructedControlInfections", @status = "(Optional) Instructed on Infection Control" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNails" class="fl">Nail Condition</label>
                <%= Html.Hidden(Model.TypeName + "_GenericNails", "", new { @id = Model.TypeName + "_GenericNailsHidden" })%>
                <div class="fr">
                    <ul class="checkgroup one-wide">
                        <%= Html.CheckgroupRadioOption(Model.TypeName + "_GenericNails", "Good", data.AnswerOrEmptyString("GenericNails").Equals("Good"), "Good")%>
                        <li class="option">
                            <div class="wrapper">
                                <%= Html.RadioButton(Model.TypeName + "_GenericNails", "Problem", data.AnswerOrEmptyString("GenericNails").Equals("Problem"), new { @id = Model.TypeName + "_GenericNailsProblem", @status = "(Optional) Nail Condition, Problem" })%>
                                <label for="<%= Model.TypeName %>_GenericNailsProblem">Problems</label>
                            </div>
                            <div class="more">
                                <label for="<%= Model.TypeName %>_GenericNailsProblemOther" class="fl">Specify</label>
                                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNailsProblemOther", data.AnswerOrEmptyString("GenericNailsProblemOther"), new { @id = Model.TypeName + "_GenericNailsProblemOther", @maxlength = "200", @status = "(Optional) Nail Condition, Specify Problem" })%></div>
                                <div class="clr"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPressureRelievingDevice" class="fl">Is patient using pressure-relieving device(s)?</label>
                <div class="fr">
                    <%  var GenericPressureRelievingDevice = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericPressureRelievingDevice")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPressureRelievingDevice", GenericPressureRelievingDevice, new { @id = Model.TypeName + "_GenericPressureRelievingDevice", @status = "(Optional) Pressure-Relieving Device" })%>
                </div>
                <div class="clr"></div>
                <div id="<%= Model.TypeName %>_GenericPressureRelievingDeviceMore" class="fr">
                    <label for="<%= Model.TypeName %>_GenericPressureRelievingDeviceType">Type</label>
                    <%  var pressureRelievingDeviceType = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Low Air Mattress", Value = "1" },
                            new SelectListItem { Text = "Gel Cushion", Value = "2" },
                            new SelectListItem { Text = "Egg Crate", Value = "3" },
                            new SelectListItem { Text = "Other (Specify)", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPressureRelievingDeviceType", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPressureRelievingDeviceType", pressureRelievingDeviceType, new { @id = Model.TypeName + "_GenericPressureRelievingDeviceType", @class = "loc", @status = "(Optional) Pressure-Relieving Device" })%>
                </div>
                <div class="clr"></div>
                <div id="<%= Model.TypeName %>_GenericPressureRelievingDeviceTypeMore" class="fr">
                    <label for="<%= Model.TypeName %>_GenericPressureRelievingDeviceType">Specify</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericPressureRelievingDeviceTypeOther", data.AnswerOrEmptyString("GenericPressureRelievingDeviceTypeOther"), new { @id = Model.TypeName + "_GenericPressureRelievingDeviceTypeOther", @class = "loc", @maxlength = "20", @status = "(Optional) Pressure-Relieving Device, Specify Other" })%>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIntegumentaryStatusComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericIntegumentaryStatusComments", data.AnswerOrEmptyString("GenericIntegumentaryStatusComments"), new { @id = Model.TypeName + "_GenericIntegumentaryStatusComments", @status = "(Optional) Integumentary Comments" })%></div>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Ulcers</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1300">
                <label>
                    <a status="More Information about M1300" class="green" onclick="Oasis.Help('M1300')">(M1300)</a>
                    Pressure Ulcer Assessment: Was this patient assessed for Risk of Developing Pressure Ulcers?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1300PressureUlcerAssessment", "", new { @id = Model.TypeName + "_M1300PressureUlcerAssessmentHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1300PressureUlcerAssessment", "00", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("00"), "0", "No assessment conducted")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1300PressureUlcerAssessment", "01", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("01"), "1", "Yes, based on an evaluation of clinical factors, e.g., mobility, incontinence, nutrition, etc., without use of standardized tool")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1300PressureUlcerAssessment", "02", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("02"), "2", "Yes, using a standardized tool, e.g., Braden, Norton, other")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1300')" status="More Information about M1300">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1302">
                <label>
                    <a status="More Information about M1302" class="green" onclick="Oasis.Help('M1302')">(M1302)</a>
                    Does this patient have a Risk of Developing Pressure Ulcers?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "", new { @id = Model.TypeName + "_M1302RiskDevelopingPressureUlcersHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "0", data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("0"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "1", data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("1"), "1", "Yes")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1302')" title="More Information about M1302">?</a></div>
                </div>
            </div>
            <%  } %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1306">
                <label>
                    <a status="More Information about M1306" class="green" onclick="Oasis.Help('M1306')">(M1306)</a>
                    Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &#8220;unstageable&#8221;?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1306UnhealedPressureUlcers", "", new { @id = Model.TypeName + "_M1306UnhealedPressureUlcersHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1306UnhealedPressureUlcers", "0", data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("0"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1306UnhealedPressureUlcers", "1", data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("1"), "1", "Yes")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1306')" title="More Information about M1306">?</a></div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
            <div class="row no-input <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1307">
                <label class="strong">
                    <a status="More Information about M1307"  class="green" onclick="Oasis.Help('M1307')">(M1307)</a>
                    The Oldest Non-epithelialized Stage II Pressure Ulcer that is present at discharge
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcerDate", "", new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcerDateHidden" })%>
                <ul class="one-wide checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "01", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("01"), "1", "Was present at the most recent SOC/ROC assessment")%>
                    <li class="option">
                        <div class="wrapper" status="(OASIS M1307) Oldest Non-epithelialized Stage II Pressure Ulcer, Developed since SOC/ROC">
                            <%= Html.RadioButton(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "02", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("02"), new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer02" })%>
                            <label for="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer02">
                                <span class="fl">2 &#8211;</span>
                                <span class="margin">Developed since the most recent SOC/ROC assessment</span>
                            </label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcerDate" class="fl">Record date pressure ulcer was first identified</label>
                            <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcerDate" value="<%= data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcerDate") %>" /></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "NA", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("NA"), "NA", "No non-epithelialized Stage II pressure ulcers are present at discharge")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1307')">?</a></div>
                </div>
            </div>
            <%  } %>
            <div class="row no-imput <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1308">
                <label class="strong">
                    <a status="More Information about M1308" class="green" onclick="Oasis.Help('M1308')">(M1308)</a>
                    Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: (Enter &#8220;0&#8221; if none; excludes Stage I pressure ulcers)
                </label>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                        <span class="fl">a.</span>
                        <span class="margin">Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Stage II Ulcers" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Stage II Ulcers at Admission" })%>
                    </div>
            <%  } %>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>" class="fl">
                        <span class="fl">b.</span>
                        <span class="margin">Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Stage III Ulcers" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Stage III Ulcers at Admission" })%>
                    </div>
            <%  } %>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>" class="fl">
                        <span class="fl">c.</span>
                        <span class="margin">Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageFourUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageFourUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Stage IV Ulcers" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageIVUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageIVUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Stage IV Ulcers at Admission" })%>
                    </div>
            <%  } %>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>" class="fl">
                        <span class="fl">d.1</span>
                        <span class="margin">Unstageable: Known or likely but unstageable due to non-removable dressing or device</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Unstageable Ulcers (Due to Non-removable Dressing/Device)" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Unstageable Ulcers at Admission (Due to Non-removable Dressing/Device)" })%>
                    </div>
            <%  } %>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>" class="fl">
                        <span class="fl">d.2</span>
                        <span class="margin">Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Unstageable Ulcers (Due to Coverage of Wound)" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Unstageable Ulcers at Admission (Due to Coverage of Wound)" })%>
                    </div>
            <%  } %>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>" class="fl">
                        <span class="fl">d.3</span>
                        <span class="margin">Unstageable: Suspected deep tissue injury in evolution.</span>
                    </label>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerCurrent" class="fl">
                            <em>Number Currently Present</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Present Number Unstageable Ulcers (Suspected Deep Tissue Injury)" })%>
                    </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                    <div class="clr"></div>
                    <div class="fr">
                        <label for="<%= Model.TypeName %>_M1308NumberNonEpithelializedStageTwoUlcerAdmission" class="fl">
                            <em>Number that were Present on Admission (Most Recent SOC/ROC)</em>
                        </label>
                        <%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", @class = "shorter numeric", @maxlength = "5", @status = "(OASIS M1308) Number Unstageable Ulcers at Admission (Suspected Deep Tissue Injury)" })%>
                    </div>
            <%  } %>
                </div>
                <div class="clr"></div>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1308')" title="More Information about M1308">?</a></div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1310">
                <label>Directions for M1310, M1312, and M1314 &#8211; If the patient has one or more unhealed (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or IV pressure ulcer with the largest surface dimension (length x width) and record in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.</label>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>_M1310PressureUlcerLength" class="fl">
                        <a status="More Information about M1310" class="green" onclick="Oasis.Help('M1310')">(M1310)</a>
                        Pressure Ulcer Length: Longest length &#8220;head-to-toe&#8221;
                    </label>
                    <div class="fr oasis">
                        <%= Html.TextBox(Model.TypeName + "_M1310PressureUlcerLength", data.AnswerOrEmptyString("M1310PressureUlcerLength"), new { @id = Model.TypeName + "_M1310PressureUlcerLength", @maxlength = "2", @class = "shortest numeric", @status = "(OASIS M1310) Pressure Ulcer Length" })%>
                        .
                        <%= Html.TextBox(Model.TypeName + "_M1310PressureUlcerLengthDecimal", data.AnswerOrEmptyString("M1310PressureUlcerLengthDecimal"), new { @id = Model.TypeName + "_M1310PressureUlcerLengthDecimal", @maxlength = "1", @class = "shortest numeric", @status = "(OASIS M1310) Pressure Ulcer Length" })%>
                        cm
                        <div class="button oasis-help"><a onclick="Oasis.Help('M1310')" title="More Information about M1310">?</a></div>
                    </div>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>_M1312PressureUlcerWidth" class="fl">
                        <a status="More Information about M1312" class="green" onclick="Oasis.Help('M1312')">(M1312)</a>
                        Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular to the length
                    </label>
                    <div class="fr oasis">
                        <%= Html.TextBox(Model.TypeName + "_M1312PressureUlcerWidth", data.AnswerOrEmptyString("M1312PressureUlcerWidth"), new { @id = Model.TypeName + "_M1312PressureUlcerWidth", @maxlength = "2", @class = "shortest numeric", @status = "(OASIS M1312) Pressure Ulcer Width" })%>
                        .
                        <%= Html.TextBox(Model.TypeName + "_M1312PressureUlcerWidthDecimal", data.AnswerOrEmptyString("M1312PressureUlcerWidthDecimal"), new { @id = Model.TypeName + "_M1312PressureUlcerWidthDecimal", @maxlength = "1", @class = "shortest numeric", @status = "(OASIS M1312) Pressure Ulcer Width" })%>
                        cm
                        <div class="button oasis-help"><a onclick="Oasis.Help('M1312')" title="More Information about M1312">?</a></div>
                    </div>
                </div>
                <div class="sub row">
                    <label for="<%= Model.TypeName %>_M1314PressureUlcerDepth" class="fl">
                        <a status="More Information about M1314" class="green" onclick="Oasis.Help('M1314')">(M1314)</a>
                        Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface to the deepest area
                    </label>
                    <div class="fr oasis">
                        <%= Html.TextBox(Model.TypeName + "_M1314PressureUlcerDepth", data.AnswerOrEmptyString("M1314PressureUlcerDepth"), new { @id = Model.TypeName + "_M1314PressureUlcerDepth", @maxlength = "2", @class = "shortest numeric", @status = "(OASIS M1314) Pressure Ulcer Depth" })%>
                        .
                        <%= Html.TextBox(Model.TypeName + "_M1314PressureUlcerDepthDecimal", data.AnswerOrEmptyString("M1314PressureUlcerDepthDecimal"), new { @id = Model.TypeName + "_M1314PressureUlcerDepthDecimal", @maxlength = "1", @class = "shortest numeric", @status = "(OASIS M1314) Pressure Ulcer Depth" })%>
                        cm
                        <div class="button oasis-help"><a onclick="Oasis.Help('M1314')" title="More Information about M1314">?</a></div>
                    </div>
                </div>
            </div>
            <div class="row no-input <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1320">
                <label class="strong">
                    <a status="More Information about M1320" class="green" onclick="Oasis.Help('M1320')">(M1320)</a>
                    Status of Most Problematic (Observable) Pressure Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "", new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatusHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "00", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("00"), "0", "Newly epithelialized")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "01", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("01"), "1", "Fully granulating")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "02", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("02"), "2", "Early/partial granulation")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "03", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("03"), "3", "Not healing")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "NA", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("NA"), "NA", "No observable pressure ulcer")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1320')" title="More Information about M1320">?</a></div>
                </div>
            </div>
            <%  } %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1322">
                <label>
                    <a status="More Information about M1322" class="green" onclick="Oasis.Help('M1322')">(M1322)</a>
                    Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "", new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcerHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "00", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("00"), "0", "Zero")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "01", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("01"), "1", "One")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "02", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("02"), "2", "Two")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "03", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("03"), "3", "Three")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "04", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("04"), "4", "Four or more")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1322')" status="More Information about M1322">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1324">
                <label>
                    <a status="More Information about M1324" class="green" onclick="Oasis.Help('M1324')">(M1324)</a>
                    Stage of Most Problematic Unhealed (Observable) Pressure Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1324MostProblematicUnhealedStage", "", new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStageHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1324MostProblematicUnhealedStage", "01", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("01"), "1", "Stage I")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1324MostProblematicUnhealedStage", "02", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("02"), "2", "Stage II")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1324MostProblematicUnhealedStage", "03", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("03"), "3", "Stage III")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1324MostProblematicUnhealedStage", "04", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("04"), "4", "Stage IV")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1324MostProblematicUnhealedStage", "NA", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("NA"), "NA", "No observable pressure ulcer or unhealed pressure ulcer")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1324')" title="More Information about M1324">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1330">
                <label>
                    <a statud="More Information about M1330" class="green" onclick="Oasis.Help('M1330')">(M1330)</a>
                    Does this patient have a Stasis Ulcer?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1330StasisUlcer", "", new { @id = Model.TypeName + "_M1330StasisUlcerHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1330StasisUlcer", "00", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1330StasisUlcer", "01", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("01"), "1", "Yes, patient has BOTH observable and unobservable stasis ulcers")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1330StasisUlcer", "02", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("02"), "2", "Yes, patient has observable stasis ulcers ONLY")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1330StasisUlcer", "03", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("03"), "3", "Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1330')" title="More Information about M1330">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1332">
                <label>
                    <a status="More Information about M1332" class="green" onclick="Oasis.Help('M1332')">(M1332)</a>
                    Current Number of (Observable) Stasis Ulcer(s)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "", new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcerHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "01", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("01"), "1", "One")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "02", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("02"), "2", "Two")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "03", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("03"), "3", "Three")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "04", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("04"), "4", "Four or More")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1332')" title="More Information about M1332">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1334">
                <label>
                    <a status="More Information about M1334" class="green" onclick="Oasis.Help('M1334')">(M1334)</a>
                    Status of Most Problematic (Observable) Stasis Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1334StasisUlcerStatus", "", new { @id = Model.TypeName + "_M1334StasisUlcerStatusHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1334StasisUlcerStatus", "00", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("00"), "0", "Newly epithelialized")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1334StasisUlcerStatus", "01", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("01"), "1", "Fully granulating")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1334StasisUlcerStatus", "02", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("02"), "2", "Early/partial granulation")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1334StasisUlcerStatus", "03", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("03"), "3", "Not healing")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1334')" title="More Information about M1334">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Surgical Wounds</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1340">
                <label class="strong">
                    <a status="More Information about M1340" class="green" onclick="Oasis.Help('M1340')">(M1340)</a>
                    Does this patient have a Surgical Wound?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1340SurgicalWound", "", new { @id = Model.TypeName + "_M1340SurgicalWoundHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1340SurgicalWound", "00", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1340SurgicalWound", "01", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("01"), "1", "Yes, patient has at least one (observable) surgical wound")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1340SurgicalWound", "02", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("02"), "2", "Surgical wound known but not observable due to non-removable dressing")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1340')" title="More Information about M1340">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1342">
                <label>
                    <a status="More Information about M1342" class="green" onclick="Oasis.Help('M1342')">(M1342)</a>
                    Status of Most Problematic (Observable) Surgical Wound
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1342SurgicalWoundStatus", "", new { @id = Model.TypeName + "_M1342SurgicalWoundStatusHidden" })%>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1342SurgicalWoundStatus", "00", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("00"), "0", "Newly epithelialized")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1342SurgicalWoundStatus", "01", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("01"), "1", "Fully granulating")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1342SurgicalWoundStatus", "02", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("02"), "2", "Early/partial granulation")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1342SurgicalWoundStatus", "03", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("03"), "3", "Not healing")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1342')" title="More Information about M1342">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Open Wounds</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1350">
                <label>
                    <a status="More Information about M1350" class="green" onclick="Oasis.Help('M1350')">(M1350)</a>
                    Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1350SkinLesionOpenWound", "", new { @id = Model.TypeName + "_M1350SkinLesionOpenWoundHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1350SkinLesionOpenWound", "0", data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("0"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1350SkinLesionOpenWound", "1", data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("1"), "1", "Yes")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1350')" title="More Information about M1350">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset>
        <legend>Wound Graph</legend>
        <div class="wide-column">
            <div class="row ac"><img alt="Body Outline" src="/Images/body_outline.gif" /></div>
        </div>
        <%  for (int i = 1; i < 6; i++) { %>
        <div class="column">
            <div class="row ac"><strong>Wound <%= i %></strong></div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericLocation<%= i %>" class="fl">Location</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLocation" + i, data.AnswerOrEmptyString("GenericLocation" + i), new { @id = Model.TypeName + "_GenericLocation" + i, @maxlength = "30", @status = "(Optional) Wound Location" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOnsetDate<%= i %>" class="fl">Onset Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericOnsetDate<%= i %>" value="<%= data.AnswerOrEmptyString("GenericOnsetDate" + i) %>" id="<%= Model.TypeName %>_GenericOnsetDate<%= i %>" title="(Optional) Wound Onset Date" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWoundType<%= i %>" class="fl">Wound Type</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericWoundType" + i, data.AnswerOrEmptyString("GenericWoundType" + i), new { @id = Model.TypeName + "_GenericWoundType" + i, @class = "WoundType", @maxlength = "30", @status = "(Optional) Wound Type" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPressureUlcerStage<%= i %>" class="fl">Pressure Ulcer Stage</label>
                <div class="fr">
                    <%  var pressureUlcerStage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "I", Value = "I" },
                            new SelectListItem { Text = "II", Value = "II" },
                            new SelectListItem { Text = "III", Value = "III" },
                            new SelectListItem { Text = "IV", Value = "IV" },
                            new SelectListItem { Text = "Unstageable", Value = "Unstageable" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPressureUlcerStage" + i, "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPressureUlcerStage" + i, pressureUlcerStage, new { @id = Model.TypeName + "_GenericPressureUlcerStage" + i, @status = "(Optional) Pressure Ulcer Stage" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Measurements</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericMeasurementLength<%= i %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementLength" + i, data.AnswerOrEmptyString("GenericMeasurementLength" + i), new { @id = Model.TypeName + "_GenericMeasurementLength" + i, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericMeasurementWidth<%= i %>"><em>Width</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementWidth" + i, data.AnswerOrEmptyString("GenericMeasurementWidth" + i), new { @id = Model.TypeName + "_GenericMeasurementWidth" + i, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Width" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericMeasurementDepth<%= i %>"><em>Depth</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMeasurementDepth" + i, data.AnswerOrEmptyString("GenericMeasurementDepth" + i), new { @id = Model.TypeName + "_GenericMeasurementDepth" + i, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Depth" })%>
                    cm
                </div>
            </div>
            <div class="row">
                <label class="fl">Wound Bed</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericWoundBedGranulation<%= i %>"><em>Granulation</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedGranulation" + i, data.AnswerOrEmptyString("GenericWoundBedGranulation" + i), new { @id = Model.TypeName + "_GenericWoundBedGranulation" + i, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Granulation" })%>
                    %<br />
                    <label for="<%= Model.TypeName %>_GenericWoundBedSlough<%= i %>"><em>Slough</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedSlough" + i, data.AnswerOrEmptyString("GenericWoundBedSlough" + i), new { @id = Model.TypeName + "_GenericWoundBedSlough" + i, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Slough" })%>
                    %<br />
                    <label for="<%= Model.TypeName %>_GenericWoundBedEschar<%= i %>"><em>Eschar</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWoundBedEschar" + i, data.AnswerOrEmptyString("GenericWoundBedEschar" + i), new { @id = Model.TypeName + "_GenericWoundBedEschar" + i, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Eschar" })%>
                    %
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericSurroundingTissue<%= i %>" class="fl">Surrounding Tissue</label>
                <div class="fr">
                    <%  var surroundingTissue = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Pink", Value = "Pink" },
                            new SelectListItem { Text = "Dry", Value = "Dry" },
                            new SelectListItem { Text = "Pale", Value = "Pale" },
                            new SelectListItem { Text = "Moist", Value = "Moist" },
                            new SelectListItem { Text = "Excoriated", Value = "Excoriated" },
                            new SelectListItem { Text = "Calloused", Value = "Calloused" },
                            new SelectListItem { Text = "Normal", Value = "Normal" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericSurroundingTissue" + i, "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSurroundingTissue" + i, surroundingTissue, new { @id = Model.TypeName + "_GenericSurroundingTissue" + i, @status = "(Optional) Wound Surrounding Tissue" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDrainage<%= i %>" class="fl">Drainage</label>
                <div class="fr">
                    <%  var drainage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Serous", Value = "Serous" },
                            new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" },
                            new SelectListItem { Text = "Sanguineous", Value = "Sanguineous" },
                            new SelectListItem { Text = "Purulent", Value = "Purulent" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDrainage" + i, "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDrainage" + i, drainage, new { @id = Model.TypeName + "_GenericDrainage" + i, @status = "(Optional) Wound Drainage" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDrainageAmount<%= i %>" class="fl">Drainage Amount</label>
                <div class="fr">
                    <%  var drainageAmount = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Minimal", Value = "Minimal" },
                            new SelectListItem { Text = "Moderate", Value = "Moderate" },
                            new SelectListItem { Text = "Heavy", Value = "Heavy" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDrainageAmount" + i, "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDrainageAmount" + i, drainageAmount, new { @id = Model.TypeName + "_GenericDrainageAmount" + i, @status = "(Optional) Wound Drainage Amount" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOdor<%= i %>" class="fl">Odor</label>
                <div class="fr">
                    <%  var odor = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericOdor" + i, "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericOdor" + i, odor, new { @id = Model.TypeName + "_GenericOdor" + i, @status = "(Optional) Wound Odor" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Tunneling</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericTunnelingLength<%= i %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericTunnelingLength" + i, data.AnswerOrEmptyString("GenericTunnelingLength" + i), new { @id = Model.TypeName + "_GenericTunnelingLength" + i, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Tunneling Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericTunnelingTime<%= i %>"><em>Time</em></label>
                    <%  var tunnelingTime = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericTunnelingTime" + i));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericTunnelingTime" + i, tunnelingTime, new { @id = Model.TypeName + "_GenericTunnelingTime" + i, @class = "shorter", @status = "(Optional) Tunneling Time" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Undermining</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericUnderminingLength<%= i %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericUnderminingLength" + i, data.AnswerOrEmptyString("GenericUnderminingLength" + i), new { @id = Model.TypeName + "_GenericUnderminingLength" + i, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Undermining Length" })%>
                    cm<br />
                    <label for="<%= Model.TypeName %>_GenericUnderminingTimeFrom<%= i %>"><em>Time</em></label>
                    <%  var underminingTimeFrom = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeFrom" + i));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeFrom" + i, underminingTimeFrom, new { @id = Model.TypeName + "_GenericUnderminingTimeFrom" + i, @class = "shorter", @status = "(Optional) Undermining Time Start" })%>
                    to
                    <%  var underminingTimeTo = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeTo" + i));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeTo" + i, underminingTimeTo, new { @id = Model.TypeName + "_GenericUnderminingTimeTo" + i, @class = "shorter", @status = "(Optional) Undermining Time End" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Device</label>
                <div class="fr ar">
                    <label for="<%= Model.TypeName %>_GenericDeviceType<%= i %>"><em>Type</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDeviceType" + i, data.AnswerOrEmptyString("GenericDeviceType" + i), new { @id = Model.TypeName + "_GenericDeviceType" + i, @class = "short DeviceType", @maxlength = "30", @status = "(Optional) Device Type " })%>
                    <br />
                    <label for="<%= Model.TypeName %>_GenericDeviceSetting<%= i %>"><em>Setting</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDeviceSetting" + i, data.AnswerOrEmptyString("GenericDeviceSetting" + i), new { @id = Model.TypeName + "_GenericDeviceSetting" + i, @class = "short", @maxlength = "30", @status = "(Optional) Device Setting " })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericUploadFile<%= i %>" class="fl">Attachment</label>
                <div class="fr">
                    <% var assetId = data.AnswerOrEmptyGuid("GenericUploadFile" + i); %>
            <%  if (assetId.IsEmpty()) { %>
                    <input type="file" name="<%= Model.TypeName %>_GenericUploadFile<%= i %>" value="Upload" title="(Optional) Photo of Wound" />
            <%  } else { %>
                    <%= Html.Hidden(Model.TypeName + "_GenericUploadFile" + i, assetId)%>
                    <%= Html.Asset(assetId)%><a class="link" onclick="Oasis.DeleteAsset($(this),'<%= Model.TypeName %>','GenericUploadFile<%= i %>','<%= assetId %>')">Delete</a>
            <%  } %>
                </div>
            </div>
        </div>
        <%  } %>
        <div class="wide-column">
            <div class="row">
                <label for="">Treatment Performed</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericTreatmentPerformed", data.AnswerOrEmptyString("GenericTreatmentPerformed"), new { @id = Model.TypeName + "_GenericTreatmentPerformed", @status = "(Optional) Treatment Performed" })%></div>
            </div>
            <div class="row">
                <label for="">Narrative</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), new { @id = Model.TypeName + "_GenericNarrative", @status = "(Optional) Narrative" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>