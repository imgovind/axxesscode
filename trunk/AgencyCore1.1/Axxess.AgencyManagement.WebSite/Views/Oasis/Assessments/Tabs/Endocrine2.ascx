﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "EndocrineForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Endocrine.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Endocrine</legend>
        <%= Html.Hidden(Model.TypeName + "_GenericEndocrineWithinNormalLimits", "", new { @id = Model.TypeName + "_GenericEndocrineWithinNormalLimitsHidden" })%>
        <div class="column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine, Within Normal Limits">
                            <%= string.Format("<input id='{0}_GenericEndocrineWithinNormalLimits' name='{0}_GenericEndocrineWithinNormalLimits' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericEndocrineWithinNormalLimits").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericEndocrineWithinNormalLimits">WNL (Within Normal Limits)</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl">Is patient diabetic?</label>
                <div class="fr">
                    <%  var GenericPatientDiabetic = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericPatientDiabetic"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPatientDiabetic", GenericPatientDiabetic, new { @id = Model.TypeName + "_GenericPatientDiabetic", @title = "(Optional) Diabetic" })%>
                </div>
            </div>
        </div>
        <div id="<%= Model.TypeName %>_GenericPatientDiabeticMore" class="wide-column">
            <div class="row">
                <label>Diabetic Management</label>
                <%  string[] diabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDiabeticCareDiabeticManagement", "", new { @id = Model.TypeName + "_GenericDiabeticCareDiabeticManagementHidden" })%>
                <ul class="checkgroup four-wide">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Diabetic Management, Diet">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement1' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='1' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement1">Diet</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Diabetic Management, Oral Hypoglycemic">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement2' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='2' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement2">Oral Hypoglycemic</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Diabetic Management, Exercise">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement3' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='3' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement3">Exercise</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Diabetic Management, Insulin">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement4' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='4' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement4">Insulin</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insulin</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericInsulinDependent" class="fl">Insulin dependent?</label>
                <div class="fr">
                    <%  var GenericInsulinDependent = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericInsulinDependent"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericInsulinDependent", GenericInsulinDependent, new { @id = Model.TypeName + "_GenericInsulinDependent", @title = "(Optional) Insulin Dependent" })%>
                </div>
                <div class="clr"></div>
                <div id="<%= Model.TypeName %>_GenericInsulinDependentMore" class="fr">
                    <label for="<%= Model.TypeName %>_GenericInsulinDependencyDuration"><em>How long?</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericInsulinDependencyDuration", data.AnswerOrEmptyString("GenericInsulinDependencyDuration"), new { @id = Model.TypeName + "_GenericInsulinDependencyDuration", @maxlength = "10", @title = "(Optional) Insulin Dependent, How Long" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label>Insulin Administered by</label>
                <%  string[] diabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDiabeticCareInsulinAdministeredby", "", new { @id = Model.TypeName + "_GenericDiabeticCareInsulinAdministeredbyHidden" })%>
                <ul class="checkgroup two-wide">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Insulin Administered By, Not Applicable">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby1' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='1' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby1">N/A</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Insulin Administered by Patient">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby2' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='2' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby2">Patient</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Insulin Administered by Caregiver">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby3' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='3' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby3">Caregiver</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Insulin Administered by Skilled Nurse">
                            <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby4' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='4' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby4">SN</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Glucometer</legend>
        <div class="column">
           <div class="row">
                <label for="<%= Model.TypeName %>_GenericGlucometerUseIndependent" class="fl">Patient independent with glucometer use?</label>
                <div class="fr">
                    <%  var GenericGlucometerUseIndependent = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericGlucometerUseIndependent"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericGlucometerUseIndependent", GenericGlucometerUseIndependent, new { @id = Model.TypeName + "_GenericGlucometerUseIndependent", @title = "(Optional) Patient Independent with Glucometer" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericCareGiverGlucometerUse" class="fl">Caregiver independent with glucometer use?</label>
                <div class="fr">
                    <%  var GenericCareGiverGlucometerUse = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" },
                            new SelectListItem { Text = "NA, No Caregiver", Value = "2" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericCareGiverGlucometerUse"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericCareGiverGlucometerUse", GenericCareGiverGlucometerUse, new { @id = Model.TypeName + "_GenericCareGiverGlucometerUse", @title = "(Optional) Caregiver Independent with Glucometer" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Conditions</legend>
        <div class="wide-column">
            <div class="row">
                <label>Does patient have any of the following?</label>
                <%  string[] patientEdocrineProblem = data.AnswerArray("GenericPatientEdocrineProblem"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericPatientEdocrineProblem", new { @id = Model.TypeName + "_GenericPatientEdocrineProblemHidden" })%>
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Polyuria">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem1' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='1' {1} />", Model.TypeName, patientEdocrineProblem.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem1">Polyuria</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Polydipsia">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem2' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='2' {1} />", Model.TypeName, patientEdocrineProblem.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem2">Polydipsia</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Polyphagia">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem3' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='3' {1} />", Model.TypeName, patientEdocrineProblem.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem3">Polyphagia</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Neuropathy">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem4' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='4' {1} />", Model.TypeName, patientEdocrineProblem.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem4">Neuropathy</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Radiculopathy">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem5' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='5' {1} />", Model.TypeName, patientEdocrineProblem.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem5">Radiculopathy</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Endocrine Patient Conditions, Thyroid Problems">
                            <%= string.Format("<input id='{0}_GenericPatientEdocrineProblem6' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='6' {1} />", Model.TypeName, patientEdocrineProblem.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem6">Thyroid problems</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Blood Sugar</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarLevelText" class="fl">Blood Sugar</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_GenericBloodSugarLevelText", data.AnswerOrEmptyString("GenericBloodSugarLevelText"), new { @id = Model.TypeName + "_GenericBloodSugarLevelText", @class = "short numeric", @maxlength = "5", @title = "(Optional) Blood Sugar Level" })%>
                    <%= Html.Hidden(Model.TypeName + "_GenericBloodSugarLevel", "", new { @id = Model.TypeName + "_GenericBloodSugarLevelHidden" })%>
                    mg/dl<br />
                    <%  var GenericBloodSugarLevel = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Random", Value = "Random" },
                            new SelectListItem { Text = "Fasting", Value = "Fasting" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericBloodSugarLevel"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericBloodSugarLevel", GenericBloodSugarLevel, new { @id = Model.TypeName + "_GenericBloodSugarLevel", @title = "(Optional) Blood Sugar" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarCheckedBy" class="fl">Blood sugar checked by</label>
                <div class="fr">
                    <%  var diabeticCarePerformedby = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Patient", Value = "1" },
                            new SelectListItem { Text = "SN", Value = "2" },
                            new SelectListItem { Text = "Caregiver", Value = "3" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarCheckedBy", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = Model.TypeName + "_GenericBloodSugarCheckedBy", @title = "(Optional) Blood Sugar Checked By" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarSiteText" class="fl">Site</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_GenericBloodSugarSiteText", data.AnswerOrEmptyString("GenericBloodSugarSiteText"), new { @id = Model.TypeName + "_GenericBloodSugarSiteText", @class = "short", @title = "(Optional) Blood Sugar Test Site" })%>
                    <%  var GenericBloodSugarSite = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Left", Value = "Left" },
                            new SelectListItem { Text = "Right", Value = "Right" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericBloodSugarSite"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericBloodSugarSite", GenericBloodSugarSite, new { @id = Model.TypeName + "_GenericBloodSugarSite", @title = "(Optional) Blood Sugar Test Site", @class = "shorter" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <%= Html.TextArea(Model.TypeName + "_GenericEndocrineComments", data.AnswerOrEmptyString("GenericEndocrineComments"), new { @id = Model.TypeName + "_GenericEndocrineComments", @title = "(Optional) Endocrine Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>