<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main"> 
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SupportiveAssistanceForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.SupportiveAssistance.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
        <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
    <fieldset class="oasis">
        <legend>Patient Living Situation</legend>
        <%= Html.Hidden(Model.TypeName + "_M1100LivingSituation", "", new { @id = Model.TypeName + "_M1100LivingSituationHidden" })%>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1100">
                <label class="fl">
                    <a status="More Information about M1100" class="green" onclick="Oasis.Help('M1100')">(M1100)</a>
                    Patient Living Situation: Which of the following best describes the patient&#8217;s residential circumstance and availability of assistance?
                </label>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1100')" title="More Information about M1100">?</a></div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericLivingSituation" class="fl">Living Situation</label>
                <div class="fr">
            <%  string GenericLivingSituation = data.AnswerOrEmptyString("GenericLivingSituation"); %>
            <%  if (data.AnswerOrEmptyString("M1100LivingSituation").IsNotNullOrEmpty() && GenericLivingSituation.IsNullOrEmpty()) GenericLivingSituation = (data.AnswerOrEmptyString("M1100LivingSituation").ToInteger() < 6 ? "a" : "") + (data.AnswerOrEmptyString("M1100LivingSituation").ToInteger() > 5 && data.AnswerOrEmptyString("M1100LivingSituation").ToInteger() < 11 ? "b" : "") + (data.AnswerOrEmptyString("M1100LivingSituation").ToInteger() > 10 ? "c" : ""); %>
                    <%  var GenericLivingSituationSelect = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "a. Patient lives alone", Value = "a" },
                            new SelectListItem { Text = "b. Patient lives with other person(s) in the home", Value = "b"},
                            new SelectListItem { Text = "c. Patient lives in congregate situation (e.g., assisted living)", Value = "c"}
                        }, "Value", "Text", GenericLivingSituation); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericLivingSituation", GenericLivingSituationSelect, new { @id = Model.TypeName + "_GenericLivingSituation" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_GenericLivingSituationMore">
                <label for="<%= Model.TypeName %>_M1100LivingSituation" class="fl">Level of Assistance</label>
                <div class="fr">
                    <%  var M1100LivingSituationA = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "01 - Around the Clock", Value = "01" },
                            new SelectListItem { Text = "02 - Regular Daytime", Value = "02" },
                            new SelectListItem { Text = "03 - Regular Nighttime", Value = "03" },
                            new SelectListItem { Text = "04 - Occasional/Short-term", Value = "04" },
                            new SelectListItem { Text = "05 - No Assistance Available", Value = "05" }
                        }, "Value", "Text", data.AnswerOrDefault("M1100LivingSituation", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M1100LivingSituation", M1100LivingSituationA, new { @id = Model.TypeName + "_M1100LivingSituation", @class = "a" })%>
                    <%  var M1100LivingSituationB = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "06 - Around the Clock", Value = "06" },
                            new SelectListItem { Text = "07 - Regular Daytime", Value = "07" },
                            new SelectListItem { Text = "08 - Regular Nighttime", Value = "08" },
                            new SelectListItem { Text = "09 - Occasional/Short-term", Value = "09" },
                            new SelectListItem { Text = "10 - No Assistance Available", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("M1100LivingSituation", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M1100LivingSituation", M1100LivingSituationB, new { @id = Model.TypeName + "_M1100LivingSituation", @class = "b" })%>
                    <%  var M1100LivingSituationC = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "11 - Around the Clock", Value = "11" },
                            new SelectListItem { Text = "12 - Regular Daytime", Value = "12" },
                            new SelectListItem { Text = "13 - Regular Nighttime", Value = "13" },
                            new SelectListItem { Text = "14 - Occasional/Short-term", Value = "14" },
                            new SelectListItem { Text = "15 - No Assistance Available", Value = "15" },
                        }, "Value", "Text", data.AnswerOrDefault("M1100LivingSituation", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M1100LivingSituation", M1100LivingSituationC, new { @id = Model.TypeName + "_M1100LivingSituation", @class = "c" })%>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset>
        <legend>Social Service Screening</legend>
        <div class="column">
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_GenericCommunityResourceInfoNeeded">Community resource info needed to manage care</label>
                <div class="fr">
                    <%  var GenericCommunityResourceInfoNeeded = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericCommunityResourceInfoNeeded", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericCommunityResourceInfoNeeded", GenericCommunityResourceInfoNeeded, new { @id = Model.TypeName + "_GenericCommunityResourceInfoNeeded", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_GenericAlteredAffect">Altered affect, e.g., express sadness or anxiety, grief </label>
                <div class="fr">
                    <%  var GenericAlteredAffect = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericAlteredAffect", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericAlteredAffect", GenericAlteredAffect, new { @id = Model.TypeName + "_GenericAlteredAffect", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_GenericSuicidalIdeation">Suicidal ideation</label>
                <div class="fr">
                    <%  var GenericSuicidalIdeation = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericSuicidalIdeation", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSuicidalIdeation", GenericSuicidalIdeation, new { @id = Model.TypeName + "_GenericSuicidalIdeation", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fl">
                    <label for="<%= Model.TypeName %>_GenericMSWIndicatedForWhat">MSW referral indicated for</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericMSWIndicatedForWhat", data.AnswerOrEmptyString("GenericMSWIndicatedForWhat"), new { @id = Model.TypeName + "_GenericMSWIndicatedForWhat", @maxlength = "150", @status = "(Optional) MSW Referral Reason" }) %>
                </div>
                <div class="fr">
                    <%  var GenericMSWIndicatedFor = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericMSWIndicatedFor", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericMSWIndicatedFor", GenericMSWIndicatedFor, new { @id = Model.TypeName + "_GenericMSWIndicatedFor", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_GenericCoordinatorNotified">Coordinator notified</label>
                <div class="fr">
                    <%  var GenericCoordinatorNotified = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericCoordinatorNotified", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericCoordinatorNotified", GenericCoordinatorNotified, new { @id = Model.TypeName + "_GenericCoordinatorNotified", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_GenericAbilityHandleFinance">Ability of Patient to Handle Finances</label>
                <div class="fr">
                    <%  var GenericAbilityHandleFinance = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Independent", Value = "Independent" },
                            new SelectListItem { Text = "Dependent", Value = "Dependent" },
                            new SelectListItem { Text = "Needs assistance", Value = "Needs assistance" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericAbilityHandleFinance", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericAbilityHandleFinance", GenericAbilityHandleFinance, new { @id = Model.TypeName + "_GenericAbilityHandleFinance", @class = "short" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label>Suspected Abuse/Neglect</label>
                <%  string[] genericSuspected = data.AnswerArray("GenericSuspected"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSuspected", "", new { @id = Model.TypeName + "_GenericSuspectedHidden" })%>
                <ul class="checkgroup three-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "1", genericSuspected.Contains("1"), "Unexplained bruises") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "2", genericSuspected.Contains("2"), "Inadequate food") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "3", genericSuspected.Contains("3"), "Fearful of family member") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "4", genericSuspected.Contains("4"), "Exploitation of funds") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "5", genericSuspected.Contains("5"), "Sexual abuse") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "6", genericSuspected.Contains("6"), "Neglect") %>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericSuspected", "7", genericSuspected.Contains("7"), "Left unattended if constant supervision is needed") %>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericAlteredAffectComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericAlteredAffectComments", data.AnswerOrEmptyString("GenericAlteredAffectComments"), new { @id = Model.TypeName + "_GenericAlteredAffectComments", @status = "(Optional) Social Service Comments" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericOrgProvidingAssistanceNames">Supportive Assistance: Names of organizations providing assistance</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericOrgProvidingAssistanceNames", data.AnswerOrEmptyString("GenericOrgProvidingAssistanceNames"), new { @id = Model.TypeName + "_GenericOrgProvidingAssistanceNames", @status = "(Optional) Supportive Assistance Organizations" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Safety/Sanitation Hazards</legend>
        <div class="wide-column">
            <div class="row no-input">
                <label>Safety/Sanitation Hazards affecting patient</label>
        <%  string[] genericHazardsIdentified = data.AnswerArray("GenericHazardsIdentified"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericNoHazardsIdentified", "", new { @id = Model.TypeName + "_GenericNoHazardsIdentifiedHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "0", genericHazardsIdentified.Contains("0"), "No hazards identified")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "1", genericHazardsIdentified.Contains("1"), "Stairs")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "2", genericHazardsIdentified.Contains("2"), "Narrow or obstructed walkway")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "3", genericHazardsIdentified.Contains("3"), "No gas/ electric appliance")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "4", genericHazardsIdentified.Contains("4"), "No running water, plumbing")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "5", genericHazardsIdentified.Contains("5"), "Insect/ rodent infestation")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "6", genericHazardsIdentified.Contains("6"), "Cluttered/ soiled living area")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "8", genericHazardsIdentified.Contains("8"), "Lack of fire safety devices")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHazardsIdentified", "7", genericHazardsIdentified.Contains("7"), "Inadequate lighting, heating and cooling")%>
                    <li class="option">
                        <div class="wrapper" title="(Optional) Safety/Sanitation Hazards, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericHazardsIdentified9' name='{0}_GenericHazardsIdentified' value='9' {1} />", Model.TypeName, genericHazardsIdentified.Contains("9").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericHazardsIdentified9">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericOtherHazards" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericOtherHazards", data.AnswerOrEmptyString("GenericOtherHazards"), new { @id = Model.TypeName + "_GenericOtherHazards", @maxlength = "30", @status = "(Optional) Safety/Sanitation Hazards, Specify Other" }) %></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHazardsComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericHazardsComments", data.AnswerOrEmptyString("GenericHazardsComments"), new { @id = Model.TypeName + "_GenericHazardsComments", @status = "(Optional) Safety/Sanitation Hazards, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Cultural</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPrimaryLanguage" class="fl">Primary language</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericPrimaryLanguage", data.AnswerOrEmptyString("GenericPrimaryLanguage"), new { @id = Model.TypeName + "_GenericPrimaryLanguage", @status = "(Optional) Primary Language" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericCulturalPractices" class="fl">Does patient have cultural practices that influence health care?</label>
                <div class="fr">
                    <%  var GenericCulturalPractices = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "1" },
                            new SelectListItem { Text = "No", Value = "0" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericCulturalPractices", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericCulturalPractices", GenericCulturalPractices, new { @id = Model.TypeName + "_GenericCulturalPractices", @class = "shorter" })%>
                </div>
                <div class="clear"></div>
                <div id="<%= Model.TypeName %>_GenericCulturalPracticesYesMore">
                    <label for="<%= Model.TypeName %>_GenericCulturalPracticesDetails">Please explain</label>
                    <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericCulturalPracticesDetails", data.AnswerOrEmptyString("GenericCulturalPracticesDetails"), new { @id = Model.TypeName + "_GenericCulturalPracticesDetails", @status = "(Optional) Cultural Influences on Health Care" })%></div>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
        <%  string[] useOfInterpreter = data.AnswerArray("GenericUseOfInterpreter"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericUseOfInterpreter", "", new { @id = Model.TypeName + "_GenericUseOfInterpreterHidden" })%>
                <label>Use of interpreter</label>
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericUseOfInterpreter", "1", useOfInterpreter.Contains("1"), "Family")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericUseOfInterpreter", "2", useOfInterpreter.Contains("2"), "Friend")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericUseOfInterpreter", "3", useOfInterpreter.Contains("3"), "Professional")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Interpreter, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericUseOfInterpreter4' name='{0}_GenericUseOfInterpreter' value='4' {1} />", Model.TypeName, useOfInterpreter.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericUseOfInterpreter4">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericUseOfInterpreterOtherDetails" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericUseOfInterpreterOtherDetails", data.AnswerOrEmptyString("GenericUseOfInterpreterOtherDetails"), new { @class = "oe", @id = Model.TypeName + "_GenericUseOfInterpreterOtherDetails", @maxlength = "20", @status = "(Optional) Interpreter, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Homebound</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIsHomeBound" class="fl">Is The Patient Homebound?</label>
                <div class="fr">
                    <%  var GenericIsHomeBound = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIsHomeBound", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIsHomeBound", GenericIsHomeBound, new { @id = Model.TypeName + "_GenericIsHomeBound", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div id="<%= Model.TypeName %>_GenericIsHomeBoundMore" class="wide-column">
        <%  string[] homeBoundReason = data.AnswerArray("GenericHomeBoundReason"); %>
            <%= Html.Hidden(Model.TypeName + "_GenericHomeBoundReason", "", new { @id = Model.TypeName + "_GenericHomeBoundReasonHidden" })%>
            <div class="row">
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "1", homeBoundReason.Contains("1"), "Exhibits considerable &#38; taxing effort to leave home")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "2", homeBoundReason.Contains("2"), "Requires the assistance of another to get up and move safely")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "3", homeBoundReason.Contains("3"), "Severe Dyspnea")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "4", homeBoundReason.Contains("4"), "Unable to safely leave home unassisted")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "5", homeBoundReason.Contains("5"), "Unsafe to leave home due to cognitive or psychiatric impairments")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericHomeBoundReason", "6", homeBoundReason.Contains("6"), "Unable to leave home due to medical restriction(s)")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Homebound, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericHomeBoundReason7' name='{0}_GenericHomeBoundReason' value='7' {1} />", Model.TypeName, homeBoundReason.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericHomeBoundReason7">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericOtherHomeBoundDetails" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericOtherHomeBoundDetails", data.AnswerOrEmptyString("GenericOtherHomeBoundDetails"), new { @id = Model.TypeName + "_GenericOtherHomeBoundDetails", @maxlength = "60", @status = "(Optional) Homebound, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHomeBoundComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericHomeBoundComments", data.AnswerOrEmptyString("GenericHomeBoundComments"), new { @id = Model.TypeName + "_GenericHomeBoundComments", @status = "(Optional) Homebound, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Safety Measures (Locator #15)</legend>
        <%  string[] safetyMeasure = data.AnswerArray("485SafetyMeasures"); %>
        <%= Html.Hidden(Model.TypeName + "_485SafetyMeasures", "", new { @id = Model.TypeName + "_485SafetyMeasuresHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup three-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "1", safetyMeasure.Contains("1"), "Anticoagulant Precautions")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "2", safetyMeasure.Contains("2"), "Emergency Plan Developed")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "3", safetyMeasure.Contains("3"), "Fall Precautions")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "4", safetyMeasure.Contains("4"), "Keep Pathway Clear")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "5", safetyMeasure.Contains("5"), "Keep Side Rails Up")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "6", safetyMeasure.Contains("6"), "Neutropenic Precautions")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "7", safetyMeasure.Contains("7"), "O2 Precautions")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "8", safetyMeasure.Contains("8"), "Proper Position During Meals")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "9", safetyMeasure.Contains("9"), "Safety in ADLs")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "10", safetyMeasure.Contains("10"), "Seizure Precautions")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "11", safetyMeasure.Contains("11"), "Sharps Safety")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "12", safetyMeasure.Contains("12"), "Slow Position Change")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "13", safetyMeasure.Contains("13"), "Standard Precautions/ Infection Control")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "14", safetyMeasure.Contains("14"), "Support During Transfer and Ambulation")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "15", safetyMeasure.Contains("15"), "Use of Assistive Devices")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "16", safetyMeasure.Contains("16"), "Instructed on safe utilities management")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "17", safetyMeasure.Contains("17"), "Instructed on mobility safety")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "18", safetyMeasure.Contains("18"), "Instructed on DME &#38; electrical safety")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "19", safetyMeasure.Contains("19"), "Instructed on sharps container")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "20", safetyMeasure.Contains("20"), "Instructed on medical gas")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "21", safetyMeasure.Contains("21"), "Instructed on disaster/ emergency plan")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "22", safetyMeasure.Contains("22"), "Instructed on safety measures")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485SafetyMeasures", "23", safetyMeasure.Contains("23"), "Instructed on proper handling of biohazard waste")%>
                </ul>
            </div>
            <div class="row narrower">
                <label for="<%= Model.TypeName %>_485TriageEmergencyCode" class="fl">Emergency Triage Code</label>
                <div class="fr">
                    <%  var triageEmergencyCode = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("485TriageEmergencyCode", "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485TriageEmergencyCode", triageEmergencyCode, new { @status = "(485 Locator 15) Safety Measures, Emergency Triage Code" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485OtherSafetyMeasures">Other</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485OtherSafetyMeasures", data.AnswerOrEmptyString("485OtherSafetyMeasures"), new { @id = Model.TypeName + "_485OtherSafetyMeasures", @status = "(485 Locator 15) Safety Measures, Other" })%></div>
            </div>
        </div>
    </fieldset>
        <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/SupportiveAssistance.ascx", Model); %>
        <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>