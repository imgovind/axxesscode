<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10< 5 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "BehaviourialForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden("categoryType", AssessmentCategory.NeuroBehavioral.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Neurological</legend>
        <%  string[] genericNeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalStatus", " ", new { @id = Model.TypeName + "_GenericNeurologicalStatusHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Neurological, LOC">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus1' name='{0}_GenericNeurologicalStatus' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("1").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus1">LOC</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalLOC">Condition</label>
                            <div class="fr">
                                <%  var LOC = new SelectList(new[]{
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Alert", Value = "1" },
                                        new SelectListItem { Text = "Lethargic", Value = "2" },
                                        new SelectListItem { Text = "Comatose", Value = "3" },
                                        new SelectListItem { Text = "Disoriented", Value = "4" },
                                        new SelectListItem { Text = "Forgetful", Value = "5"},
                                        new SelectListItem { Text = "Other", Value = "6" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalLOC", "0"));    %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalLOC", LOC, new { @id = Model.TypeName + "_GenericNeurologicalLOC", @status = "(Optional) Neurological, LOC Condition", @class = "short" })%>
                            </div>
                            <div class="clr"></div>
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalOriented">Oriented to</label>
                            <%  string[] genericNeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalOriented", " ", new { @id = Model.TypeName + "_GenericNeurologicalOrientedHidden" })%>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, LOC, Orientation, Person">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalOriented1' name='{0}_GenericNeurologicalOriented' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("1").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalOriented1">Person</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, LOC, Orientation, Place">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalOriented2' name='{0}_GenericNeurologicalOriented' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("2").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalOriented2">Place</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, LOC, Orientation, Time">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalOriented3' name='{0}_GenericNeurologicalOriented' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalOriented.Contains("3").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalOriented3">Time</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Neurological, Pupils">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus2' name='{0}_GenericNeurologicalStatus' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("2").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus2">Pupils</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalPupils">Condition</label>
                            <div class="fr">
                                <%  var pupils = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "PERRLA/WNL (Within Normal Limits)", Value = "1" },
                                        new SelectListItem { Text = "Sluggish", Value = "2" },
                                        new SelectListItem { Text = "Non-Reactive", Value = "3" },
                                        new SelectListItem { Text = "Other", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalPupils", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalPupils", pupils, new { @id = Model.TypeName + "_GenericNeurologicalPupils", @status = "(Optional) Neurological, Pupils Condition", @class = "short" })%>
                            </div>
                            <div class="clr"></div>
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalPupilsPosition">Position</label>
                            <div class="fr">
                                <%  var GenericNeurologicalPupilsPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalPupilsPosition", GenericNeurologicalPupilsPosition, new { @id = Model.TypeName + "_GenericNeurologicalPupilsPosition", @status = "(Optional) Neurological, Pupils Condition" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Neurological, Vision">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus3' name='{0}_GenericNeurologicalStatus' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("3").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus3">Vision</label>
                        </div>
                        <div class="more">
                            <%  string[] genericNeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNeurologicalVisionStatus", " ", new { @id = Model.TypeName + "_GenericNeurologicalVisionStatus" })%>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Within Normal Limits">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus1' name='{0}_GenericNeurologicalVisionStatus' value='1' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("1").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus1">WNL (Within Normal Limits)</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Blurred Vision">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus2' name='{0}_GenericNeurologicalVisionStatus' value='2' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("2").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus2">Blurred Vision</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Cataracts">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus3' name='{0}_GenericNeurologicalVisionStatus' value='3' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("3").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus3">Cataracts</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Corrective Lenses">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus4' name='{0}_GenericNeurologicalVisionStatus' value='4' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("4").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus4">Corrective Lenses</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Glaucoma">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus5' name='{0}_GenericNeurologicalVisionStatus' value='5' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("5").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus5">Glaucoma</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Neurological, Vision, Legally Blind">
                                        <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus6' name='{0}_GenericNeurologicalVisionStatus' value='6' type='checkbox' {1} />", Model.TypeName, genericNeurologicalVisionStatus.Contains("6").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericNeurologicalVisionStatus6">Legally Blind</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" statur="(Optional) Neurological, Speech">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus4' name='{0}_GenericNeurologicalStatus' value='4' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("4").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus4">Speech</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalSpeech">Specify</label>
                            <div class="fr">
                                <%  var speech = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Clear", Value = "1" },
                                        new SelectListItem { Text = "Slurred", Value = "2" },
                                        new SelectListItem { Text = "Aphasic", Value = "3" },
                                        new SelectListItem { Text = "Other", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalSpeech", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNeurologicalSpeech", speech, new { @id = Model.TypeName + "_GenericNeurologicalSpeech", @status = "(Optional) Neurological, Specify Speech" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Neurological, Paralysis">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus5' name='{0}_GenericNeurologicalStatus' value='5' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus5">Paralysis</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalParalysisLocation">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNeurologicalParalysisLocation", data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation"), new { @id = Model.TypeName + "_GenericNeurologicalParalysisLocation", @maxlength = "20", @status = "(Optional) Neurological, Paralysis Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNeurologicalStatus", "6", genericNeurologicalStatus.Contains("6"), "Quadriplegia") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNeurologicalStatus", "7", genericNeurologicalStatus.Contains("7"), "Paraplegia") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNeurologicalStatus", "8", genericNeurologicalStatus.Contains("8"), "Seizures") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Neurological, Tremors">
                            <%= string.Format("<input id='{0}_GenericNeurologicalStatus9' name='{0}_GenericNeurologicalStatus' value='9' type='checkbox' {1} />", Model.TypeName, genericNeurologicalStatus.Contains("9").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNeurologicalStatus9">Tremors</label>
                        </div>
                        <div class="more">
                            <label class="fl" for="<%= Model.TypeName %>_GenericNeurologicalTremorsLocation">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNeurologicalTremorsLocation", data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation"), new { @id = Model.TypeName + "_GenericNeurologicalTremorsLocation", @maxlength = "20", @status = "(Optional) Neurological, Tremors Location" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNeurologicalStatus", "10", genericNeurologicalStatus.Contains("10"), "Dizziness") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNeurologicalStatus", "11", genericNeurologicalStatus.Contains("11"), "Headache") %>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNeuroEmoBehaviorComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNeuroEmoBehaviorComments", data.AnswerOrEmptyString("GenericNeuroEmoBehaviorComments"), new { @id = Model.TypeName + "_GenericNeuroEmoBehaviorComments", @status = "(Optional) Neurological, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Mental Status (Locator #19)</legend>
        <%  string[] mentalStatus = data.AnswerArray("485MentalStatus"); %>
        <%= Html.Hidden(Model.TypeName + "_485MentalStatus", " ", new { @id = Model.TypeName + "_485MentalStatusHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "1", mentalStatus.Contains("1"), "Oriented")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "2", mentalStatus.Contains("2"), "Comatose")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "3", mentalStatus.Contains("3"), "Forgetful")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "5", mentalStatus.Contains("5"), "Depressed")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "6", mentalStatus.Contains("6"), "Disoriented")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "7", mentalStatus.Contains("7"), "Lethargic")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485MentalStatus1", "4", mentalStatus.Contains("4"), "Agitated")%>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 19) Mental Status, Other">
                            <%= string.Format("<input id='{0}_485MentalStatus8' name='{0}_485MentalStatus' value='8' type='checkbox' {1} />", Model.TypeName, mentalStatus.Contains("8").ToChecked())%>
                            <label for="<%= Model.TypeName %>_485MentalStatus8">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485MentalStatusOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485MentalStatusOther", data.AnswerOrEmptyString("485MentalStatusOther"), new { @id = Model.TypeName + "_485MentalStatusOther", @maxlength = "24", @status = "(485 Locator 19) Mental Status, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485MentalStatusComments">Additional Orders</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485MentalStatusComments", data.AnswerOrEmptyString("485MentalStatusComments"), new { @id = Model.TypeName + "_485MentalStatusComments", @status = "(485 Locator 19) Mental Status, Additional Orders" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Behavior Status</legend>
        <%  string[] genericBehaviour = data.AnswerArray("GenericBehaviour"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericBehaviour", " ", new { @id = Model.TypeName + "_GenericBehaviourHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "1", genericBehaviour.Contains("1"), "WNL (Within Normal Limits)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "2", genericBehaviour.Contains("2"), "Withdrawn")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "3", genericBehaviour.Contains("3"), "Expresses Depression")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "4", genericBehaviour.Contains("4"), "Impaired Decision Making")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "5", genericBehaviour.Contains("5"), "Difficulty coping with Altered Health Status")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "6", genericBehaviour.Contains("6"), "Combative")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericBehaviour", "7", genericBehaviour.Contains("7"), "Irritability")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Behavior Status, Other">
                            <%= string.Format("<input id='{0}_GenericBehaviour8' name='{0}_GenericBehaviour' value='8' type='checkbox' {1} />", Model.TypeName, genericBehaviour.Contains("8").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericBehaviour8">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericBehaviourOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericBehaviourOther", data.AnswerOrEmptyString("GenericBehaviourOther"), new { @id = Model.TypeName + "_GenericBehaviourOther", @maxlength = "20", @status = "(Optional) Behavior Status, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBehaviourComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericBehaviourComments", data.AnswerOrEmptyString("GenericBehaviourComments"), new { @id = Model.TypeName + "_GenericBehaviourComments", @status = "(Optional) Behavior Status, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Psychosocial</legend>
        <%  string[] genericPsychosocial = data.AnswerArray("GenericPsychosocial"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericPsychosocial", " ", new { @id = Model.TypeName + "_GenericPsychosocialHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPsychosocial", "1", genericPsychosocial.Contains("1"), "WNL (No Issues Identified)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPsychosocial", "2", genericPsychosocial.Contains("1"), "Poor Home Environment")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPsychosocial", "3", genericPsychosocial.Contains("1"), "No emotional support from family/CG")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericPsychosocial", "4", genericPsychosocial.Contains("1"), "Verbalize desire for spiritual support")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Psychosocial, Other">
                            <%= string.Format("<input id='{0}_GenericPsychosocial5' name='{0}_GenericPsychosocial' value='5' type='checkbox' {1} />", Model.TypeName, genericPsychosocial.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericPsychosocial5">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericPsychosocialOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericPsychosocialOther", data.AnswerOrEmptyString("GenericPsychosocialOther"), new { @id = Model.TypeName + "_GenericPsychosocialOther", @maxlength = "20", @status = "(Optional) Psychosocial, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPsychosocialComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericPsychosocialComments", data.AnswerOrEmptyString("GenericPsychosocialComments"), new { @id = Model.TypeName + "_GenericPsychosocialComments", @status = "(Optional) Psychosocial, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Strengths</legend>
        <%  string[] patientStrengths = data.AnswerArray("485PatientStrengths"); %>
        <%= Html.Hidden(Model.TypeName + "_485PatientStrengths", " ", new { @id = Model.TypeName + "_485PatientStrengthsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485PatientStrengths", "1", patientStrengths.Contains("1"), "Motivated Learner")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485PatientStrengths", "2", patientStrengths.Contains("2"), "Strong Support System")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485PatientStrengths", "3", patientStrengths.Contains("3"), "Absence of Multiple Diagnosis")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485PatientStrengths", "4", patientStrengths.Contains("4"), "Enhanced Socioeconomic Status")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Patient Strengths, Other">
                            <%= string.Format("<input id='{0}_485PatientStrengths5' name='{0}_485PatientStrengths' value='5' type='checkbox' {1} />", Model.TypeName, patientStrengths.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_485PatientStrengths5">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485PatientStrengthOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_485PatientStrengthOther", data.AnswerOrEmptyString("485PatientStrengthOther"), new { @id = Model.TypeName + "_485PatientStrengthOther", @maxlength = "70", @status = "(Optional) Patient Strengths, Specify Other" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
    <fieldset class="oasis">
        <legend>Cognition</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1700">
                <label>
                    <a status="More Information about M1700" class="green" onclick="Oasis.Help('M1700')">(M1700)</a>
                    Cognitive Functioning: Patient&#8217;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1700CognitiveFunctioning", "", new { @id = Model.TypeName + "_M1700CognitiveFunctioningHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1700CognitiveFunctioning", "00", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("00"), "0", "Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1700CognitiveFunctioning", "01", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("01"), "1", "Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1700CognitiveFunctioning", "02", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("02"), "2", "Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1700CognitiveFunctioning", "03", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("03"), "3", "Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1700CognitiveFunctioning", "04", data.AnswerOrEmptyString("M1700CognitiveFunctioning").Equals("04"), "4", "Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1700')" status="More Information about M1700">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1710">
                <label class="strong">
                    <a status="More Information about M1710" class="green" onclick="Oasis.Help('M1710')">(M1710)</a>
                    When Confused (Reported or Observed Within the Last 14 Days)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1710WhenConfused", "", new { @id = Model.TypeName + "_M1710WhenConfusedHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "00", data.AnswerOrEmptyString("M1710WhenConfused").Equals("00"), "0", "Never")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "01", data.AnswerOrEmptyString("M1710WhenConfused").Equals("01"), "1", "In new or complex situations only")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "02", data.AnswerOrEmptyString("M1710WhenConfused").Equals("02"), "2", "On awakening or at night only")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "03", data.AnswerOrEmptyString("M1710WhenConfused").Equals("03"), "3", "During the day and evening, but not constantly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "04", data.AnswerOrEmptyString("M1710WhenConfused").Equals("04"), "4", "Constantly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1710WhenConfused", "NA", data.AnswerOrEmptyString("M1710WhenConfused").Equals("NA"), "NA", "Patient nonresponsive")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1710');" title="More Information about M1710">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1720">
                <label>
                    <a status="More Information about M1720" class="green" onclick="Oasis.Help('M1720')">(M1720)</a>
                    When Anxious (Reported or Observed Within the Last 14 Days)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1720WhenAnxious", "", new { @id = Model.TypeName + "_M1720WhenAnxiousHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1720WhenAnxious", "00", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("00"), "0", "None of the time")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1720WhenAnxious", "01", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("01"), "1", "Less often than daily")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1720WhenAnxious", "02", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("02"), "2", "Daily, but not constantly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1720WhenAnxious", "03", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("03"), "3", "All of the time")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1720WhenAnxious", "NA", data.AnswerOrEmptyString("M1720WhenAnxious").Equals("NA"), "NA", "Patient nonresponsive")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1720');" title="More Information about M1720">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 9) { %>
    <fieldset class="oasis">
        <legend>Depression Screening</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1730">
                <label>
                    <a status="More Information about M1730" class="green" onclick="Oasis.Help('M1730')">(M1730)</a>
                    Depression Screening: Has the patient been screened for depression, using a standardized depression screening tool?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1730DepressionScreening", "", new { @id = Model.TypeName + "_M1730DepressionScreeningHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1730DepressionScreening", "00", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1730DepressionScreening", "01", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("01"), "1", "Yes, patient was screened using the PHQ-2&#169;* scale. (Instructions for this two-question tool: Ask patient: &#8220;Over the last two weeks, how often have you been bothered by any of the following problems&#8221;)")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1730DepressionScreening", "02", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("02"), "2", "Yes, with a different standardized assessment-and the patient meets criteria for further evaluation for depression.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1730DepressionScreening", "03", data.AnswerOrEmptyString("M1730DepressionScreening").Equals("03"), "3", "Yes, patient was screened with a different standardized assessment-and the patient does not meet criteria for further evaluation for depression.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1730');" status="More Information about M1730">?</a></div>
                </div>
            </div>
        </div>
        <div id="<%= Model.TypeName %>_Phq2">
            <div class="wide-column">
                <div class="row ac no-input" id="<%= Model.TypeName %>_Phq2">
                    <label>PHQ-2&#169;*</label>
                    <br />
                    <em>Copyright&#169; Pfizer Inc. All rights reserved. Reproduced with permission.</em>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningInterest" class="fl">a) Little interest or pleasure in doing things</label>
                    <div class="fr">
                        <%  var M1730DepressionScreeningInterest = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "Not at all (0 - 1 day)", Value = "00" },
                                new SelectListItem { Text = "Several days (2 - 6 days)", Value = "01" },
                                new SelectListItem { Text = "More than half of the days (7 - 11 days)", Value = "02" },
                                new SelectListItem { Text = "Nearly every day (12 - 14 days)", Value = "03" },
                                new SelectListItem { Text = "N/A Unable to respond", Value = "NA" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1730DepressionScreeningInterest"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1730DepressionScreeningInterest", M1730DepressionScreeningInterest, new { @id = Model.TypeName + "_M1730DepressionScreeningInterest", @status = "(Optional) PHQ-2, Interest/Pleasure" })%>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="<%= Model.TypeName %>_M1730DepressionScreeningHopeless" class="fl">b) Feeling down, depressed, or hopeless?</label>
                    <div class="fr">
                        <%  var M1730DepressionScreeningHopeless = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "" },
                                new SelectListItem { Text = "Not at all (0 - 1 day)", Value = "00" },
                                new SelectListItem { Text = "Several days (2 - 6 days)", Value = "01" },
                                new SelectListItem { Text = "More than half of the days (7 - 11 days)", Value = "02" },
                                new SelectListItem { Text = "Nearly every day (12 - 14 days)", Value = "03" },
                                new SelectListItem { Text = "N/A Unable to respond", Value = "NA" }
                            }, "Value", "Text", data.AnswerOrEmptyString("M1730DepressionScreeningHopeless"));%>
                        <%= Html.DropDownList(Model.TypeName + "_M1730DepressionScreeningHopeless", M1730DepressionScreeningHopeless, new { @id = Model.TypeName + "_M1730DepressionScreeningHopeless", @status = "(Optional) PHQ-2, Depression/Despair" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset class="oasis">
        <legend>Cognition</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1740">
                <label>
                    <a status="More Information about M1740" class="green" onclick="Oasis.Help('M1740')">(M1740)</a>
                    Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed)
                </label>
                <ul class="checkgroup one-wide">
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficitHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit").Equals("1"), "1", "Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsImpDes", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsImpDesHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsImpDes", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsImpDes").Equals("1"), "2", "Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsVerbal", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsVerbalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsVerbal", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsVerbal").Equals("1"), "3", "Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsPhysical", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsPhysicalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsPhysical", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsPhysical").Equals("1"), "4", "Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsSIB", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsSIBHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsSIB", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsSIB").Equals("1"), "5", "Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsDelusional", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsDelusionalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsDelusional", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsDelusional").Equals("1"), "6", "Delusional, hallucinatory, or paranoid behavior", new { @class = "M1740" })%>
                    <%= Html.Hidden(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsNone", " ", new { @id = Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsNoneHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1740CognitiveBehavioralPsychiatricSymptomsNone", "1", data.AnswerOrEmptyString("M1740CognitiveBehavioralPsychiatricSymptomsNone").Equals("1"), "7", "None of the above behaviors demonstrated", new { @class = "M1740" })%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1740');" title="More Information about M1740">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M1745">
                <label>
                    <a status="More Information about M1745" class="green" onclick="Oasis.Help('M1745')">(M1745)</a>
                    Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "", new { @id = Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequencyHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("00"), "0", "Never")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("01"), "1", "Less than once a month")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("02"), "2", "Once a month")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("03"), "3", "Several times each month")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("04"), "4", "Several times a week")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.AnswerOrEmptyString("M1745DisruptiveBehaviorSymptomsFrequency").Equals("05"), "5", "At least daily")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1745');" title="More Information about M1745">?</a></div>
                </div>
            </div>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M1750">
                <label>
                    <a status="More Information about M1750" class="green" onclick="Oasis.Help('M1750')">(M1750)</a>
                    Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1750PsychiatricNursingServicing", "", new { @id = Model.TypeName + "_M1750PsychiatricNursingServicingHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1750PsychiatricNursingServicing", "0", data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("0"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1750PsychiatricNursingServicing", "1", data.AnswerOrEmptyString("M1750PsychiatricNursingServicing").Equals("1"), "1", "Yes")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1750')" status="More Information about M1750">?</a></div>
                </div>
            </div>
        <%  } %>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/NeuroBehavioral.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>