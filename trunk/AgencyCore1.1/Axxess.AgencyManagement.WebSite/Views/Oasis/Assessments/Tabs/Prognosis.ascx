<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PrognosisForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Prognosis.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset class="loc485">
        <legend>Prognosis (Locator #20)</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup five-wide">
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 20) Prognosis, Guarded">
                            <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Guarded", data.AnswerOrEmptyString("485Prognosis").Equals("Guarded"), new { @id = Model.TypeName + "_485PrognosisGuarded" })%>
                            <label for="<%= Model.TypeName %>_485PrognosisGuarded">Guarded</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 20) Prognosis, Poor">
                            <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Poor", data.AnswerOrEmptyString("485Prognosis").Equals("Poor"), new { @id = Model.TypeName + "_485PrognosisPoor" })%>
                            <label for="<%= Model.TypeName %>_485PrognosisPoor">Poor</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 20) Prognosis, Fair">
                            <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Fair", data.AnswerOrEmptyString("485Prognosis").Equals("Fair"), new { @id = Model.TypeName + "_485PrognosisFair" })%>
                            <label for="<%= Model.TypeName %>_485PrognosisFair">Fair</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 20) Prognosis, Good">
                            <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Good", data.AnswerOrEmptyString("485Prognosis").Equals("Good"), new { @id = Model.TypeName + "_485PrognosisGood" })%>
                            <label for="<%= Model.TypeName %>_485PrognosisGood">Good</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(485 Locator 20) Prognosis, Excellent">
                            <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Excellent", data.AnswerOrEmptyString("485Prognosis").Equals("Excellent"), new { @id = Model.TypeName + "_485PrognosisExcellent" })%>
                            <label for="<%= Model.TypeName %>_485PrognosisExcellent">Excellent</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Functional Limitations (Locator #18.A)</legend>
        <%  string[] functionLimitations = data.AnswerArray("485FunctionLimitations"); %>
        <%= Html.Hidden(Model.TypeName + "_485FunctionLimitations", "", new { @id = Model.TypeName + "_485FunctionLimitationsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Amputation'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations1' name='{0}_485FunctionLimitations' value='1' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations1">Amputation</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Incontinence'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations2' name='{0}_485FunctionLimitations' value='2' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations2">Bowel/Bladder Incontinence</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Contrature'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations3' name='{0}_485FunctionLimitations' value='3' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations3">Contracture</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Hearing'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations4' name='{0}_485FunctionLimitations' value='4' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations4">Hearing</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Paralysis'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations5' name='{0}_485FunctionLimitations' value='5' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations5">Paralysis</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Endurance'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations6' name='{0}_485FunctionLimitations' value='6' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations6">Endurance</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Ambulation'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations7' name='{0}_485FunctionLimitations' value='7' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations7">Ambulation</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Speech'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations8' name='{0}_485FunctionLimitations' value='8' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("8").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations8">Speech</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Legally Blind'>
                            <%= string.Format("<input id='{0}_485FunctionLimitations9' name='{0}_485FunctionLimitations' value='9' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitations9">Legally Blind</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Dyspnea'>
                            <%= string.Format("<input id='{0}_485FunctionLimitationsA' name='{0}_485FunctionLimitations' value='A' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("A").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitationsA">Dyspnea with Minimal Exertion</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" title='(485 Locator 18A) Functional Limitations, Other'>
                            <%= string.Format("<input id='{0}_485FunctionLimitationsB' name='{0}_485FunctionLimitations' value='B' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("B").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485FunctionLimitationsB">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485FunctionLimitationsOther">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_485FunctionLimitationsOther", data.AnswerOrEmptyString("485FunctionLimitationsOther"), new { @id = Model.TypeName + "_485FunctionLimitationsOther", @title = "(485 Locator 18A) Functional Limitations, Specify Other" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Advanced Directives</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485AdvancedDirectives" class="fl">Are there any Advanced Directives?</label>
                <div class="fr">
                    <%  var AdvancedDirectives = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"}
                        }, "Value", "Text", data.AnswerOrDefault("485AdvancedDirectives", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485AdvancedDirectives", AdvancedDirectives, new { @id = Model.TypeName + "_485AdvancedDirectives", @class = "shorter" })%>
                </div>
            </div>
            <div class="row no-input">
                <label>Intent</label>
                <%  string[] advancedDirectivesIntent = data.AnswerArray("485AdvancedDirectivesIntent"); %>
                <%= Html.Hidden(Model.TypeName + "_485AdvancedDirectivesIntent", "", new { @id = Model.TypeName + "_485AdvancedDirectivesIntentHidden" })%>
                <ul class="checkgroup two-wide">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Advanced Directives Intent, Do Not Resuscitate">
                            <%= string.Format("<input id='{0}_485AdvancedDirectivesIntentDNR' name='{0}_485AdvancedDirectivesIntent' value='DNR' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("DNR").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentDNR">DNR</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Advanced Directives Intent, Living Will">
                            <%= string.Format("<input id='{0}_485AdvancedDirectivesIntentLivingWill' name='{0}_485AdvancedDirectivesIntent' value='Living Will' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Living Will").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentLivingWill">Living Will</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Advanced Directives Intent, Medical Power of Attorney">
                            <%= string.Format("<input id='{0}_485AdvancedDirectivesIntentMPOA' name='{0}_485AdvancedDirectivesIntent' value='Medical Power of Attorney' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Medical Power of Attorney").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentMPOA">Medical Power of Attorney</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Advanced Directives Intent, Other">
                            <%= string.Format("<input id='{0}_485AdvancedDirectivesIntentOther' name='{0}_485AdvancedDirectivesIntent' value='Other' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Other").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentOther">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentOther" class="fl">Specify</label>
                            <%= Html.TextBox(Model.TypeName + "_485AdvancedDirectivesIntentOther", data.AnswerOrEmptyString("485AdvancedDirectivesIntentOther"), new { @id = Model.TypeName + "_485AdvancedDirectivesIntentOther", @maxlength="50", @status = "(Optional) Advanced Directives Intent, Specify Other", @class = "fr" }) %>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485AdvancedDirectivesCopyOnFile" class="fl">Copy on file at agency?</label>
                <div class="fr">
                    <%  var AdvancedDirectivesCopyOnFile = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"}
                        }, "Value", "Text", data.AnswerOrDefault("485AdvancedDirectivesCopyOnFile", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485AdvancedDirectivesCopyOnFile", AdvancedDirectivesCopyOnFile, new { @id = Model.TypeName + "_485AdvancedDirectivesCopyOnFile", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485AdvancedDirectivesWrittenAndVerbal" class="fl">Patient was provided written and verbal information on Advance Directives</label>
                <div class="fr">
                    <%  var AdvancedDirectivesWrittenAndVerbal = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"}
                        }, "Value", "Text", data.AnswerOrDefault("485AdvancedDirectivesWrittenAndVerbal", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbal", AdvancedDirectivesWrittenAndVerbal, new { @id = Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbal", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPatientDNR" class="fl">Is the Patient DNR (Do Not Resuscitate)?</label>
                <div class="fr">
                    <%  var GenericPatientDNR = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"}
                        }, "Value", "Text", data.AnswerOrDefault("GenericPatientDNR", "")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPatientDNR", GenericPatientDNR, new { @id = Model.TypeName + "_GenericPatientDNR", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485AdvancedDirectivesComment">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485AdvancedDirectivesComment", data.AnswerOrEmptyString("485AdvancedDirectivesComment"), new { @id = Model.TypeName + "_485AdvancedDirectivesComment", @title = "(Optional) Advanced Directives Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
